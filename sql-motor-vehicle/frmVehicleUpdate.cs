//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Manufacturer;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Enums;
using SharedApplication.MotorVehicle.Models;
using TWSharedLibrary;
using Color = System.Drawing.Color;

namespace TWMV0000
{
	public partial class frmVehicleUpdate : BaseForm
	{
        private const string NUMERIC_FORMAT = "#,##0.00";
        private int intPartyType1 = 0;
        private int intPartyType2 = 0;
        private int intPartyType3 = 0;
        private string strReg1FirstName = "";
        private string strReg1LastName = "";
        private string strReg1MI = "";
        private string strReg1Designation = "";
        private string strOwner1 = "";
        private string strReg2FirstName = "";
        private string strReg2LastName = "";
        private string strReg2MI = "";
        private string strReg2Designation = "";
        private string strOwner2 = "";
        private string strReg3FirstName = "";
        private string strReg3LastName = "";
        private string strReg3MI = "";
        private string strReg3Designation = "";
        private string strOwner3 = "";
		private IBlueBookRepository blueBookRepository;

        private IBlueBookRepository BlueBookRepository
        {
            get
            {
                if (blueBookRepository == null)
                {
                    blueBookRepository = StaticSettings.GlobalCommandDispatcher.Send(new GetBlueBookRepository()).Result;
                }

                return blueBookRepository;
            }
        }
        public frmVehicleUpdate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            Label31 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label31.AddControlArrayElement(Label31_0, 0);
            Label31.AddControlArrayElement(Label31_11, 1);
            Label31.AddControlArrayElement(Label31_12, 2);
            Label31.AddControlArrayElement(Label31_13, 3);
            Label31.AddControlArrayElement(Label31_14, 4);
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVehicleUpdate InstancePtr
		{
			get
			{
				return (frmVehicleUpdate)Sys.GetInstance(typeof(frmVehicleUpdate));
			}
		}

		protected frmVehicleUpdate _InstancePtr = null;
		//=========================================================
		bool bolAddNewRecord;
		// vbPorter upgrade warning: lngGFColor As int	OnWrite(Color)
		int lngGFColor;
		// Got Focus Color
		// vbPorter upgrade warning: lngLFColor As int	OnWrite(Color)
		int lngLFColor;
		// Lost Focus Color
		// vbPorter upgrade warning: lngLFColorICM As int	OnWrite(Color)
		int lngLFColorICM;
		// Lost Focus Color for ICMN box
		int lngLFColorGray;
		// Second Lost Focus Color
		string strOriginalMVR = "";
		bool blnFinalCheck;
		private bool boolLoading = false;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;

        private Color _LFColor;
        private Color _GFColor;
        private Color _LFColorGray;

        // Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cboClass_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadSubclassCombo(cboClass.Text);
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtMessage.Text) != "" && cboMessageCodes.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a message code to go with your message before you continue.", "Invalid Message Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == "")
			{
				MessageBox.Show("You must input a message to go with your code before you continue.", "No Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			fraAdditionalInfo.Visible = false;
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void cmdErase_Click(object sender, System.EventArgs e)
		{
			cboMessageCodes.SelectedIndex = -1;
			txtMessage.Text = "";
		}

		private void cmdMoreInfo_Click(object sender, System.EventArgs e)
		{
			fraAdditionalInfo.Visible = true;
		}

		private void CmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			clsDRWrapper rsClass = new clsDRWrapper();
			clsDRWrapper rsLongTerm = new clsDRWrapper();
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			bool blnMissing = false;
			int x;
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (MotorVehicle.Statics.InfoForCorrect)
			{
				blnMissing = false;
				if (Conversion.Val(txtCredit.Text) != 0 && fecherFoundation.Strings.Trim(txtCreditNumber.Text) == "")
				{
					blnMissing = true;
				}
				if (fecherFoundation.Strings.Trim(txtMVR3.Text) == "")
				{
					blnMissing = true;
				}
				if (!Information.IsDate(txtExpireDate.Text))
				{
					blnMissing = true;
				}
				if (!Information.IsDate(txtEffectiveDate.Text))
				{
					blnMissing = true;
				}
				if (!Information.IsDate(txtExciseTaxDate.Text) && (cboClass.Text != "TL" || Strings.Left(cboSubClass.Text, 2) != "L9"))
				{
					blnMissing = true;
				}
				if (Conversion.Val(txtMilRate.Text) == 0)
				{
					blnMissing = true;
				}
				if (Conversion.Val(txtPrice.Text) == 0)
				{
					blnMissing = true;
				}
				if (Conversion.Val(txtRegRate.Text) == 0)
				{
					blnMissing = true;
				}
				if (Conversion.Val(txtRegFeeCharged.Text) == 0)
				{
					blnMissing = true;
				}
				if (Conversion.Val(txtAmountOfTax.Text) == 0)
				{
					blnMissing = true;
				}
				if (Conversion.Val(txtAgentFee.Text) == 0)
				{
					blnMissing = true;
				}
				if (blnMissing)
				{
					answer = MessageBox.Show("Required fields still may be missing information. Do you wish to continue?", "Missing Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer != DialogResult.Yes)
					{
						return;
					}
				}
			}
			if (MotorVehicle.Statics.InfoForReReg || MotorVehicle.Statics.InfoForDuplicate)
			{
				blnMissing = false;
				if (!Information.IsDate(txtExpireDate.Text))
				{
					blnMissing = true;
				}
				if (!Information.IsDate(txtEffectiveDate.Text))
				{
					blnMissing = true;
				}
				if (!Information.IsDate(txtExciseTaxDate.Text))
				{
					blnMissing = true;
				}
				if (blnMissing)
				{
					answer = MessageBox.Show("Required fields still may be missing information. Do you wish to continue?", "Missing Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer != DialogResult.Yes)
					{
						return;
					}
				}
			}
			if (Information.IsDate(txtExpireDate.Text))
			{
				string strDate = "";
				switch (FCConvert.ToDateTime(txtExpireDate.Text).Month)
				{
					case 12:
						{
							strDate = "01/1/" + FCConvert.ToString(FCConvert.ToDateTime(txtExpireDate.Text).Year + 1);
							break;
						}
					default:
						{
							strDate = FCConvert.ToString(FCConvert.ToDateTime(txtExpireDate.Text).Month + 1) + "/1/" + FCConvert.ToString(FCConvert.ToDateTime(txtExpireDate.Text).Year);
							break;
						}
				}
				//end switch
				strDate = Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(strDate)), "MM/dd/yyyy");
				if (Strings.Format(strDate, "MM/dd/yyyy") != Strings.Format(txtExpireDate.Text, "MM/dd/yyyy"))
				{
					txtExpireDate.Text = strDate;
				}
			}
			// Save Data
			if (txtStatus.Text != "T")
			{
				rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + cboClass.Text + "' AND SystemCode = '" + Strings.Left(cboSubClass.Text, 2) + "'");
				if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsClass.Get_Fields_String("renewaldate")) == "F")
					{
						MotorVehicle.Statics.FixedMonth = true;
						MotorVehicle.Statics.gintFixedMonthMonth = FCConvert.ToInt16(rsClass.Get_Fields_Int16("FixedMonth"));
					}
					else
					{
						MotorVehicle.Statics.FixedMonth = false;
						MotorVehicle.Statics.gintFixedMonthMonth = 0;
					}
					if (FCConvert.ToString(rsClass.Get_Fields_String("ExciseRequired")) != "Y")
					{
						// do nothing
					}
					else
					{
						if (Conversion.Val(txtPrice.Text) == 0)
						{
							MessageBox.Show("You must enter a price for this vehicle before you may save.", "Invalid Price", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtPrice.Focus();
							return;
						}
					}
				}
				else
				{
					if ((cboClass.Text == "TL" || cboClass.Text == "CL") && (Strings.Left(cboSubClass.Text, 2) == "L2" || Strings.Left(cboSubClass.Text, 2) == "L4"))
					{
						// do nothing
					}
					else
					{
						if (Conversion.Val(txtPrice.Text) == 0)
						{
							MessageBox.Show("You must enter a price for this vehicle before you may save.", "Invalid Price", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtPrice.Focus();
							return;
						}
					}
				}
				if (txtStatus.Text == "A")
				{
					// kk02272017 tromv-1254  If plate is "NEW" then Status has to stay at "P"
					if (fecherFoundation.Strings.Trim(txtPlate.Text) == "NEW")
					{
						MessageBox.Show("Status must be Pending if a plate has not been assigned.", "Invalid Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtStatus.Focus();
						return;
					}
					if (!Information.IsDate(txtExpireDate.Text))
					{
						MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtExpireDate.Focus();
						return;
					}
					else if (MotorVehicle.Statics.FixedMonth)
					{
						if (MotorVehicle.IsMotorcycle(cboClass.Text))
						{
							if (FCConvert.ToDouble(txtExpireDate.Text) < FCConvert.ToDateTime("3/1/2013").ToOADate())
							{
								// do nothing
							}
							else
							{
								if (fecherFoundation.DateAndTime.DateValue(txtExpireDate.Text).Month != MotorVehicle.Statics.gintFixedMonthMonth)
								{
									MessageBox.Show("This type of vehicle requires an expiration date ending in " + Strings.Format(FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth) + "/1/2011", "MMMM") + ".  You must enter a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									txtExpireDate.Focus();
									return;
								}
							}
						}
						else
						{
							if (fecherFoundation.DateAndTime.DateValue(txtExpireDate.Text).Month != MotorVehicle.Statics.gintFixedMonthMonth)
							{
								MessageBox.Show("This type of vehicle requires an expiration date ending in " + Strings.Format(FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth) + "/1/2011", "MMMM") + ".  You must enter a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtExpireDate.Focus();
								return;
							}
						}
					}
					if (!Information.IsDate(txtEffectiveDate.Text))
					{
						MessageBox.Show("You must have a valid effective date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtEffectiveDate.Focus();
						return;
					}
					// vbPorter upgrade warning: NM As int	OnWrite(long)
					int NM = 0;
					NM = FCConvert.ToInt32((fecherFoundation.DateAndTime.DateDiff("m", FCConvert.ToDateTime(txtEffectiveDate.Text), FCConvert.ToDateTime(txtExpireDate.Text))));
					if (cboClass.Text == "TL" && Strings.Left(cboSubClass.Text, 2) == "L9")
					{
						if (NM > (MotorVehicle.Statics.gintLongTermYearsAllowed * 12))
						{
							MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtExpireDate.Focus();
							return;
						}
					}
					else if (cboClass.Text == "CI" || cboClass.Text == "CS" || cboClass.Text == "MM")
					{
						// 
						if (NM > (120))
						{
							MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtExpireDate.Focus();
							return;
						}
					}
					else if (cboClass.Text == "ST")
					{
						// 
						if (NM > (72))
						{
							MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtExpireDate.Focus();
							return;
						}
					}
					else
					{
						if (FCConvert.ToString(rsClass.Get_Fields("TwoYear")) == "Y")
						{
							if (NM > 24)
							{
								MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtExpireDate.Focus();
								return;
							}
							else if (NM > 12)
							{
								MotorVehicle.Statics.rsPlateForReg.Set_Fields("TwoYear", true);
							}
						}
						else
						{
							if (NM > 12)
							{
								MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtExpireDate.Focus();
								return;
							}
						}
					}
				}
				if (fecherFoundation.Strings.Trim(txtresidenceCode.Text) == "")
				{
					MessageBox.Show("You must enter legal residence information before you may save.", "Invalid Legal Residence", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtresidenceCode.Focus();
					return;
				}
				if (fecherFoundation.Strings.Trim(txtLegalResStreet.Text) == "")
				{
					if (MessageBox.Show("You have not entered a legal residence street address.  Do you wish to do so now?", "Enter Legal Residence?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						txtLegalResStreet.Focus();
						return;
					}
				}
				if (!VerifyCountry())
				{
					return;
				}
				if (fecherFoundation.Strings.Trim(txtPlate.Text) == "")
				{
					MessageBox.Show("You must enter a plate number before you may save.", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtPlate.Focus();
					return;
				}
				if (txtReg1ICM.Text == "N")
				{
					MessageBox.Show("You must enter information for the primary registrant before you may save.", "Invalid Primary Registrant", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtReg1ICM.Focus();
					return;
				}
				else if (lblRegistrant1.Text == "")
				{
					MessageBox.Show("You must enter information for the primary registrant before you may save.", "Invalid Primary Registrant", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtReg1PartyID.Focus();
					return;
				}
				if (FCConvert.ToDecimal(txtRegFeeCharged.Text) != 0 && FCConvert.ToDecimal(txtRegRate.Text) == 0)
				{
					if (MessageBox.Show("You have entered an amount into fees but not into  the rate.  Is this correct?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						txtRegRate.Focus();
						return;
					}
				}
				blnFinalCheck = true;
				VerifyVIN();
				blnFinalCheck = false;
				if ((txtReg1ICM.Text == "C" && txtReg1LR.Text != "R") || (txtReg2ICM.Text == "C" && txtReg2LR.Text != "R") || (txtReg3ICM.Text == "C" && txtReg3LR.Text != "R"))
				{
					if (fecherFoundation.Strings.Trim(txtTaxIDNumber.Text) == "")
					{
						MessageBox.Show("You must enter a Tax ID Number before you may proceed.", "Tax ID Required", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTaxIDNumber.Focus();
						return;
					}
				}
				else
				{
					if (fecherFoundation.Strings.Trim(txtTaxIDNumber.Text) != "")
					{
						MessageBox.Show("You may not enter a Tax ID Number for this registration.", "No Tax ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTaxIDNumber.Focus();
						return;
					}
				}
			}
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillNewValue(this);
			}
			// Thsi function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("New To System / Update", txtPlate.Text, txtVin.Text, MotorVehicle.Statics.OpID);
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClass();
			FillControlInformationClassFromGrid();
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillOldValue(this);
			}
			// ----------------------------------------------------------------
			if (bolAddNewRecord == true)
				MotorVehicle.Statics.rsPlateForReg.AddNew();
			else
				MotorVehicle.Statics.rsPlateForReg.Edit();
			if (bolAddNewRecord)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OpID", MotorVehicle.Statics.OpID);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("DateUpdated", DateTime.Now);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OldMVR3", 0);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExcisePaidMVR3", 0);
			}
			if (fecherFoundation.Strings.Trim(txtBattle.Text) == "")
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Battle", "X");
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Battle", txtBattle.Text);
			}
			if (cmbtNo.Text == "Yes")
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Mail", true);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Mail", false);
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("EMailAddress", fecherFoundation.Strings.Trim(txtEMail.Text));
			if (chkOverrideFleetEmail.CheckState == Wisej.Web.CheckState.Checked)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OverrideFleetEmail", true);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OverrideFleetEmail", false);
			}
			if ((cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) != "") || (cboMessageCodes.SelectedIndex == -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == ""))
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("MessageFlag", cboMessageCodes.SelectedIndex + 1);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserMessage", fecherFoundation.Strings.Trim(txtMessage.Text));
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserField1", fecherFoundation.Strings.Trim(txtUserField1.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserReason1", fecherFoundation.Strings.Trim(txtUserReason1.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserField2", fecherFoundation.Strings.Trim(txtUserField2.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserReason2", fecherFoundation.Strings.Trim(txtUserReason2.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserField3", fecherFoundation.Strings.Trim(txtUserField3.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserReason3", fecherFoundation.Strings.Trim(txtUserReason3.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Status", txtStatus.Text);
			if (txtStatus.Text == "P")
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ETO", true);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ETO", false);
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("MillYear", FCConvert.ToString(Conversion.Val(txtRBYear.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("OpID", txtOpID.Text);
			// .fields("Trailer")  = txtTrailer.Text
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("plate", txtPlate.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Class", cboClass.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Subclass", Strings.Left(cboSubClass.Text, 2));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Odometer", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtMileage.Text))));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Vin", txtVin.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Year", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtYear.Text))));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("make", txtMake.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("model", txtModel.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("color1", txtColor1.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("color2", txtColor2.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Style", txtStyle.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Tires", FCConvert.ToString(Conversion.Val(txtTires.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Axles", FCConvert.ToString(Conversion.Val(txtAxles.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(txtNetWeight.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("registeredWeightNew", FCConvert.ToString(Conversion.Val(txtRegWeight.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Fuel", txtFuel.Text);
			if (Conversion.Val(txtPrice.Text) == 0)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("BasePrice", 0);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("BasePrice", FCConvert.ToInt32(FCConvert.ToDouble(txtPrice.Text)));
			}
			// 
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TaxIDNumber", fecherFoundation.Strings.Trim(txtTaxIDNumber.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("LeaseCode1", txtReg1LR.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("LeaseCode2", txtReg2LR.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("LeaseCode3", txtReg3LR.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("OwnerCode1", txtReg1ICM.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyID1", FCConvert.ToString(Conversion.Val(txtReg1PartyID.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyName1", lblRegistrant1.Text);
			if (txtReg1ICM.Text == "I")
			{
				if (!txtReg1DOB.IsEmpty)
				{
					MotorVehicle.Statics.rsPlateForReg.Set_Fields("DOB1", txtReg1DOB.Text);
				}
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("DOB1", null);
			}
            if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1") == "I")
            {
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Owner1", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1LastName", strReg1LastName);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1FirstName", strReg1FirstName);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1MI", strReg1MI);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1Designation", strReg1Designation);
            }
            else
            {
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Owner1", lblRegistrant1.Text);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1LastName", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1FirstName", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1MI", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg1Designation", "");
            }
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("OwnerCode2", txtReg2ICM.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyID2", FCConvert.ToString(Conversion.Val(txtReg2PartyID.Text)));
			if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID2")) > 0)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyName2", lblRegistrant2.Text);
			}
			if (txtReg2ICM.Text == "I")
			{
				if (!txtReg2DOB.IsEmpty)
				{
					MotorVehicle.Statics.rsPlateForReg.Set_Fields("DOB2", txtReg2DOB.Text);
				}
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("DOB2", null);
			}

            if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2") == "I")
            {
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Owner2", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2LastName", strReg2LastName);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2FirstName", strReg2FirstName);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2MI", strReg2MI);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2Designation", strReg2Designation);
            }
            else
            {
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Owner2", lblRegistrant2.Text);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2LastName", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2FirstName", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2MI", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg2Designation", "");
            }
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("OwnerCode3", txtReg3ICM.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyID3", FCConvert.ToString(Conversion.Val(txtReg3PartyID.Text)));
			if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID3")) > 0)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyName3", lblRegistrant3.Text);
			}
			if (txtReg3ICM.Text == "I")
			{
				if (!txtReg3DOB.IsEmpty)
				{
					MotorVehicle.Statics.rsPlateForReg.Set_Fields("DOB3", txtReg3DOB.Text);
				}
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("DOB3", null);
			}

            if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3") == "I")
            {
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Owner3", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3LastName", strReg3LastName);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3FirstName", strReg3FirstName);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3MI", strReg3MI);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3Designation", strReg3Designation);
            }
            else
            {
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Owner3", lblRegistrant3.Text);
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3LastName", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3FirstName", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3MI", "");
                MotorVehicle.Statics.rsPlateForReg.Set_Fields("Reg3Designation", "");
            }
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UnitNumber", txtUnitNumber.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("DOTNumber", txtDOTNumber.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Address", txtAddress1.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Address2", txtAddress2.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("City", txtCity.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("State", txtState.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Zip", txtZip.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Country", txtCountry.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Residence", txtLegalres.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ResidenceAddress", txtLegalResStreet.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ResidenceState", txtResState.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ResidenceCountry", txtResCountry.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ResidenceCode", txtresidenceCode.Text);
			if (Information.IsDate(txtExpireDate.Text))
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExpireDate", txtExpireDate.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("AgentFee", txtAgentFee.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseTaxCharged", FCConvert.ToDecimal(txtAmountOfTax.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseCreditMVR3", FCConvert.ToString(Conversion.Val(txtCreditNumber.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("MVR3", FCConvert.ToString(Conversion.Val(txtMVR3.Text)));
			// Val(txtCreditNumber.Text)    'kk07142016  Don't use the MVR3 as the credit no
			// Matthew **********
			if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseCreditFull") + " ") == 0)
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseCreditFull", 0);
			// *******************
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseCreditUsed", FCConvert.ToDecimal(txtCredit.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseTransferCharge", txtTransferCharge.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseHalfRate", FCConvert.CBool(chkExciseHalfRate.CheckState == Wisej.Web.CheckState.Checked));
			// kk01282016 tromv-979  Need to be able to correctly indicate HR
			if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ExciseHalfRate")))
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtAmountOfTax.Text) * 2);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtAmountOfTax.Text));
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("LocalPaid", FCConvert.ToDecimal(txtExciseTaxBalance.Text) + FCConvert.ToDecimal(txtAgentFee.Text));
			if (Information.IsDate(txtExciseTaxDate.Text))
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExciseTaxDate", txtExciseTaxDate.Text);
			}
			else
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "TL" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "L9")
				{
					MessageBox.Show("The Excise Tax date needs to be filled in.", "Date Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					MotorVehicle.Statics.rsPlateForReg.CancelUpdate();
					txtExciseTaxDate.Focus();
					return;
				}
			}
			if (Information.IsDate(txtEffectiveDate.Text))
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("EffectiveDate", txtEffectiveDate.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("SalesTax", FCConvert.ToDecimal(txtSalesTax.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TitleFee", txtTitleFee.Text);
			// kk01282916 tromv-979  Zero plate fees that aren't accounted for in StatePaid; esp for RapidRenewal records
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("InitialFee", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PlateFeeNew", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PlateFeeReReg", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("StatePaid", FCConvert.ToDecimal(txtRegFeeCharged.Text) + FCConvert.ToDecimal(txtSalesTax.Text) + FCConvert.ToDecimal(txtTitleFee.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("RegHalf", FCConvert.CBool(chkRegHalf.CheckState == Wisej.Web.CheckState.Checked));
			// kk01282016 tromv-979  Need to be able to correctly indicate HR
			if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("RegHalf")))
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("TransferCreditFull", FCConvert.ToDecimal(txtRegCredit.Text) * 2);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("TransferCreditFull", FCConvert.ToDecimal(txtRegCredit.Text));
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TransferCreditUsed", FCConvert.ToDecimal(txtRegCredit.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtRegRate.Text));
			if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("RegHalf")))
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtRegRate.Text) * 2);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtRegRate.Text));
			}
			if (Conversion.Val(fecherFoundation.Strings.Trim(txtFleetNumber.Text)) == 0)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNumber", 0);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetOld", false);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNew", false);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNumber", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtFleetNumber.Text))));
				if (!FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("FleetOld")))
				{
					MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNew", true);
				}
			}

            MotorVehicle.Statics.rsPlateForReg.Set_Fields("ReasonCodes", "");
            MotorVehicle.Statics.rsPlateForReg.Set_Fields("PrintPriorities", "");

			if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("LongTermTrailerRegID")) != 0 && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "L9" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")) != "NEW")
			{
				rsLongTerm.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("LongTermTrailerRegID"));
				if (rsLongTerm.EndOfFile() != true && rsLongTerm.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsLongTerm.Get_Fields_String("Plate")) == "NEW")
					{
						rsLongTerm.Edit();
						rsLongTerm.Set_Fields("Plate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
						rsLongTerm.Update();
					}
				}
			}

            MotorVehicle.Statics.rsPlateForReg.Update();
			if (MotorVehicle.Statics.FromInventory)
			{
				frmInventoryStatus.InstancePtr.Show(App.MainForm);
			}
			if (MotorVehicle.Statics.InfoForCorrect)
			{
				MotorVehicle.Statics.blnTryCorrectAgain = true;
				MotorVehicle.Statics.blnTryReRegAgain = false;
				MotorVehicle.Statics.blnTryDuplicateAgain = false;
				frmPlateInfo.InstancePtr.blnCameFromVehicleUpdate = true;
			}
			else if (MotorVehicle.Statics.InfoForReReg)
			{
				MotorVehicle.Statics.blnTryCorrectAgain = false;
				MotorVehicle.Statics.blnTryReRegAgain = true;
				MotorVehicle.Statics.blnTryDuplicateAgain = false;
				frmPlateInfo.InstancePtr.blnCameFromVehicleUpdate = true;
			}
			else if (MotorVehicle.Statics.InfoForDuplicate)
			{
				MotorVehicle.Statics.blnTryCorrectAgain = false;
				MotorVehicle.Statics.blnTryReRegAgain = false;
				MotorVehicle.Statics.blnTryDuplicateAgain = true;
				frmPlateInfo.InstancePtr.blnCameFromVehicleUpdate = true;
			}
			else
			{
				MotorVehicle.Statics.blnTryCorrectAgain = false;
				MotorVehicle.Statics.blnTryReRegAgain = false;
				MotorVehicle.Statics.blnTryDuplicateAgain = false;
				frmPlateInfo.InstancePtr.blnCameFromVehicleUpdate = false;
			}
			FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");

            if (MotorVehicle.Statics.FromInventory)
            {
                frmInventoryStatus.InstancePtr.Show(App.MainForm);
            }
            else
            {
                frmPlateInfo.InstancePtr.ShowForm();
            }
            Close();
        }

		public void CmdSave_Click()
		{
			CmdSave_Click(cmdProcessSave, new System.EventArgs());
		}

		public bool VerifyCountry()
		{
			bool VerifyCountry = true;

			bool badCountry = (txtCountry.Text != "" && txtCountry.Text != "US");
			bool badResCountry = (txtResCountry.Text != "" && txtResCountry.Text != "US");

			if (badCountry || badResCountry)
			{
				if (MessageBox.Show("A country code other than US has been entered. Is this correct?",
					    "Check Country", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					VerifyCountry = false;
					txtResCountry.TabStop = badCountry;
					txtCountry.TabStop = badResCountry;
					if (badCountry)
					{
						txtCountry.Focus();
					}
					else
					{
						txtResCountry.Focus();
					}
				}
			}
			return VerifyCountry;
		}

		private void frmVehicleUpdate_Activated(object sender, System.EventArgs e)
		{
			System.Diagnostics.Debug.Print("Activated");
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.VEHICLEUPDATE))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
		}

		private void frmVehicleUpdate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmVehicleUpdate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				//if (CmdQuit.Enabled == true)
				//{
				//	CmdQuit_Click();
				//}
				//else
				{
					cmdDone_Click();
				}
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmVehicleUpdate_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((FCGlobal.Screen.ActiveControl is TextBoxBase) && 
                ((FCGlobal.Screen.ActiveControl as TextBoxBase).MaxLength == (FCGlobal.Screen.ActiveControl as TextBoxBase).SelectionStart && 
                (FCGlobal.Screen.ActiveControl as TextBoxBase).MaxLength != 0) &&
                KeyCode != Keys.Return &&
                KeyCode != Keys.Down &&
                KeyCode != Keys.Up && 
                KeyCode != Keys.Left &&
                KeyCode != Keys.Tab)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			/*? On Error GoTo 0 */
		}

		private void frmVehicleUpdate_Load(object sender, System.EventArgs e)
		{
			System.Diagnostics.Debug.Print("Loading Start");

			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this, false);
			lngGFColor = ColorTranslator.ToOle(Color.Yellow);
			lngLFColorGray = ColorTranslator.ToOle(Color.White);
			lngLFColor = ColorTranslator.ToOle(txtVin.BackColor);
			lngLFColorICM = ColorTranslator.ToOle(txtReg1ICM.BackColor);
			SetCustomFormColors();
			LoadClassCombo();
			boolLoading = true;
			InitializeForm();
			boolLoading = false;

			System.Diagnostics.Debug.Print("Loading End");
		}

		private void frmVehicleUpdate_Resize(object sender, System.EventArgs e)
		{
			modGNBas.SaveWindowSize(this);
            fraAdditionalInfo.CenterToContainer(this.ClientArea);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGNBas.SaveWindowSize(this);
			MotorVehicle.Statics.InfoForCorrect = false;
			MotorVehicle.Statics.InfoForReReg = false;
			MotorVehicle.Statics.InfoForDuplicate = false;
		}

        private bool HasCurrentRegNameInfo(int registrantIndex)
        {
            switch (registrantIndex)
            {
				case 1:
                    if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1") == "I")
                    {
                        if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1LastName").Trim() != "" &&
                            MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1FirstName").Trim() != "")
                        {
                            return true;
                        }
					}
                    else
                    {
                        if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner1").Trim() != "")
                        {
                            return true;
                        }
					}
					break;
                case 2:
					if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2") == "I")
                    {
                        if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2LastName").Trim() != "" &&
                            MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2FirstName").Trim() != "")
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner2").Trim() != "")
                        {
                            return true;
                        }
                    }
					break;
				case 3:
					if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3") == "I")
                    {
                        if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3LastName").Trim() != "" &&
                            MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3FirstName").Trim() != "")
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner3").Trim() != "")
                        {
                            return true;
                        }
                    }
					break;
            }

            return false;
        }

		private void Fill_Owner_Information()
		{
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("TaxIDNumber")))
			{
				txtTaxIDNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("TaxIDNumber"));
			}
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1"))) != "")
			{
				txtReg1ICM.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1")));
			}
			else
			{
				txtReg1ICM.Text = "I";
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2")) != "")
			{
				txtReg2ICM.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2"));
			}
			else
			{
				txtReg2ICM.Text = "N";
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3")) != "")
			{
				txtReg3ICM.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3"));
			}
			else
			{
				txtReg3ICM.Text = "N";
			}

            var leaseCode1 = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("LeaseCode1"));
            if (leaseCode1 != "") txtReg1LR.Text = leaseCode1;  
            
            var leaseCode2 = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("LeaseCode2"));
            if (leaseCode2 != "") txtReg2LR.Text = leaseCode2;    
            
            var leaseCode3 = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("LeaseCode3"));
            if (leaseCode3 != "") txtReg3LR.Text = leaseCode3;

 
			if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1")) != 0)
			{
				txtReg1PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1"));
				GetPartyInfo(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1"), 1);
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1")) == "C" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1")) == "M")
			{
				txtReg1DOB.Visible = false;
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1") == "!")
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("Owner1")) != "")
				{
					txtReg1DOB.Visible = false;
				}
				else
				{
					txtReg1DOB.Visible = true;
				}
			}
			else
			{
				txtReg1DOB.Visible = true;
			}

            if (HasCurrentRegNameInfo(1))
            {
                if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1") == "I")
                {
                    strReg1LastName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1LastName");
                    strReg1FirstName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1FirstName");
                    strReg1MI = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1MI");
                    strReg1Designation = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1Designation");
                    lblRegistrant1.Text = FormatIndividualName(1);
                }
				else
                {
                    lblRegistrant1.Text = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner1");
                }
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2")) != "N" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2")) != "")
			{
				txtReg2PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID2"));
				GetPartyInfo(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID2"), 2);
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2")) == "C" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2")) == "M" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2")) == "D")
			{
				txtReg2DOB.Visible = false;
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2") == "!")
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("Owner2")) != "")
				{
					txtReg2DOB.Visible = false;
				}
				else
				{
					txtReg2DOB.Visible = true;
				}
			}
			else
			{
				txtReg2DOB.Visible = true;
			}

            if (HasCurrentRegNameInfo(2))
            {
                if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2") == "I")
                {
                    strReg2LastName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2LastName");
                    strReg2FirstName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2FirstName");
                    strReg2MI = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2MI");
                    strReg2Designation = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2Designation");
                    lblRegistrant2.Text = FormatIndividualName(2);
                }
                else
                {
                    lblRegistrant2.Text = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner2");
                }
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3")) != "N" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3")) != "")
			{
				txtReg3PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID3"));
				GetPartyInfo(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID3"), 3);
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3")) == "C" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3")) == "M" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3")) == "D")
			{
				txtReg3DOB.Visible = false;
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3") == "!")
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("Owner3")) != "")
				{
					txtReg3DOB.Visible = false;
				}
				else
				{
					txtReg3DOB.Visible = true;
				}
			}
			else
			{
				txtReg3DOB.Visible = true;
			}

            if (HasCurrentRegNameInfo(3))
            {
                if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3") == "I")
                {
                    strReg3LastName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3LastName");
                    strReg3FirstName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3FirstName");
                    strReg3MI = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3MI");
                    strReg3Designation = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3Designation");
                    lblRegistrant3.Text = FormatIndividualName(3);
                }
                else
                {
                    lblRegistrant3.Text = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner3");
                }
            }

            var screenDateFormat = "MM/dd/yyyy";
            txtReg1DOB.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("DOB1"), screenDateFormat);
			txtReg2DOB.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("DOB2"), screenDateFormat);
			txtReg3DOB.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("DOB3"), screenDateFormat);
			txtAddress1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Address")));
			txtAddress2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Address2")));
			txtCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("City")));
			txtState.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("State"));
			txtZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Zip"));
			txtCountry.Text =
					fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Country"))).Length <= 2 ?
					fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Country"))) :
					"";
			txtLegalres.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Residence")));
			txtLegalResStreet.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ResidenceAddress")));
            var residenceCode = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ResidenceCode"));

            if (residenceCode.Length < 5)
			{
				txtresidenceCode.Text = "0" + fecherFoundation.Strings.Trim(residenceCode);
			}
			else
			{
				txtresidenceCode.Text = fecherFoundation.Strings.Trim(residenceCode);
			}
			txtResCountry.Text =
					fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ResidenceCountry"))).Length <= 2 ?
					fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ResidenceCountry"))) :
					"";
			txtResState.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ResidenceState"));
			if (fecherFoundation.Strings.Trim(txtCountry.Text) == "")
				txtCountry.Text = "US";
			if (fecherFoundation.Strings.Trim(txtResCountry.Text) == "")
				txtResCountry.Text = "US";
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			CmdSave_Click();
		}

		private void txtAddress1_Enter(object sender, System.EventArgs e)
        {
            _GFColor = ColorTranslator.FromOle(lngGFColor);
            txtAddress1.BackColor = _GFColor;
        }

		private void txtAddress1_Leave(object sender, System.EventArgs e)
        {
            _LFColor = ColorTranslator.FromOle(lngLFColor);
            txtAddress1.BackColor = _LFColor;
        }

		private void txtAddress2_Enter(object sender, System.EventArgs e)
		{
			txtAddress2.BackColor = _GFColor;
		}

		private void txtAddress2_Leave(object sender, System.EventArgs e)
		{
			txtAddress2.BackColor = _LFColor;
		}

		private void txtAgentFee_Enter(object sender, System.EventArgs e)
		{
			txtAgentFee.BackColor = _GFColor;
		}

		private void txtAgentFee_Leave(object sender, System.EventArgs e)
		{
            _LFColorGray = ColorTranslator.FromOle(lngLFColorGray);
            txtAgentFee.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtAgentFee.Text) == "")
			{
				txtAgentFee.Text = "0.00";
			}
		}

		private void txtAmountOfTax_Enter(object sender, System.EventArgs e)
		{
			txtAmountOfTax.BackColor = _GFColor;
		}

		private void txtAmountOfTax_Leave(object sender, System.EventArgs e)
		{
			txtAmountOfTax.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtAmountOfTax.Text) == "")
			{
				txtAmountOfTax.Text = "0.00";
			}

            var taxMinusCredit = FCConvert.ToDecimal(txtAmountOfTax.Text) - FCConvert.ToDecimal(txtCredit.Text);

            txtSubtotal.Text = Strings.Format(taxMinusCredit, NUMERIC_FORMAT);
			if (FCConvert.ToDouble(Strings.Format(taxMinusCredit, NUMERIC_FORMAT)) < 0)
				txtSubtotal.Text = FCConvert.ToString(0);
			txtExciseTaxBalance.Text = Strings.Format(FCConvert.ToDecimal(txtSubtotal.Text) + FCConvert.ToDecimal(txtTransferCharge.Text), NUMERIC_FORMAT);
		}

		private void txtAxles_Enter(object sender, System.EventArgs e)
		{
			txtAxles.BackColor = _GFColor;
		}

		private void txtAxles_Leave(object sender, System.EventArgs e)
		{
			txtAxles.BackColor = _LFColor;
		}

		private void txtCity_Enter(object sender, System.EventArgs e)
		{
			txtCity.BackColor = _GFColor;
		}

		private void txtCity_Leave(object sender, System.EventArgs e)
		{
			txtCity.BackColor = _LFColor;
		}

		private void cboClass_Enter(object sender, System.EventArgs e)
		{
			cboClass.BackColor = _GFColor;
		}

		private void cboClass_Leave(object sender, System.EventArgs e)
		{
			cboClass.BackColor = _LFColor;
			SetToolTips();
		}

		private void txtColor1_Enter(object sender, System.EventArgs e)
		{
			txtColor1.BackColor = _GFColor;
		}

		private void txtColor1_Leave(object sender, System.EventArgs e)
		{
			txtColor1.BackColor = _LFColor;
		}

		private void txtColor2_Enter(object sender, System.EventArgs e)
		{
			txtColor2.BackColor = _GFColor;
		}

		private void txtColor2_Leave(object sender, System.EventArgs e)
		{
			txtColor2.BackColor = _LFColor;
		}

		private void txtCredit_Enter(object sender, System.EventArgs e)
		{
			txtCredit.BackColor = _GFColor;
		}

		private void txtCredit_Leave(object sender, System.EventArgs e)
		{
			txtCredit.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtCredit.Text) == "")
			{
				txtCredit.Text = "0.00";
			}
			txtSubtotal.Text = Strings.Format(FCConvert.ToDecimal(txtAmountOfTax.Text) - FCConvert.ToDecimal(txtCredit.Text), NUMERIC_FORMAT);
			txtExciseTaxBalance.Text = Strings.Format(FCConvert.ToDecimal(txtSubtotal.Text) + FCConvert.ToDecimal(txtTransferCharge.Text), NUMERIC_FORMAT);
		}

		private void txtCreditNumber_Enter(object sender, System.EventArgs e)
		{
			txtCreditNumber.BackColor = _GFColor;
		}

		private void txtCreditNumber_Leave(object sender, System.EventArgs e)
		{
			txtCreditNumber.BackColor = _LFColorGray;
		}

		private void txtMVR3_Enter(object sender, System.EventArgs e)
		{
			txtMVR3.BackColor = _GFColor;
		}

		private void txtMVR3_Leave(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			if (fecherFoundation.Strings.Trim(txtMVR3.Text) != fecherFoundation.Strings.Trim(strOriginalMVR) && Conversion.Val(strOriginalMVR) != 0)
			{
				ans = MessageBox.Show("Are you sure you wish to change the MVR3 Number for this Vehicle?", "Change MVR3 Number?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					txtMVR3.BackColor = _LFColorGray;
				}
				else
				{
					txtMVR3.SelectionStart = 0;
                    if (txtMVR3.Text.Length > 0)
                    {
                        txtMVR3.SelectionLength = txtMVR3.Text.Length;
                    }

                    txtMVR3.Focus();
				}
			}
			else
			{
				txtMVR3.BackColor = _LFColorGray;
			}
		}

		private void txtDOTNumber_Enter(object sender, System.EventArgs e)
		{
			txtDOTNumber.BackColor = _GFColor;
		}

		private void txtDOTNumber_Leave(object sender, System.EventArgs e)
		{
			txtDOTNumber.BackColor = _LFColor;
		}

		private void txtTaxIDNumber_Enter(object sender, System.EventArgs e)
		{
			txtTaxIDNumber.BackColor = _GFColor;
		}

		private void txtTaxIDNumber_Leave(object sender, System.EventArgs e)
		{
			txtTaxIDNumber.BackColor = _LFColor;
		}

		private void txtEffectiveDate_Enter(object sender, System.EventArgs e)
		{
			txtEffectiveDate.BackColor = _GFColor;
		}

		private void txtEffectiveDate_Leave(object sender, System.EventArgs e)
		{
			txtEffectiveDate.BackColor = _LFColorGray;
		}

		private void txtExciseTaxBalance_Enter(object sender, System.EventArgs e)
		{
			txtExciseTaxBalance.BackColor = _GFColor;
		}

		private void txtExciseTaxBalance_Leave(object sender, System.EventArgs e)
		{
			txtExciseTaxBalance.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtExciseTaxBalance.Text) == "")
			{
				txtExciseTaxBalance.Text = "0.00";
			}
		}

		private void txtExciseTaxDate_Enter(object sender, System.EventArgs e)
		{
			txtExciseTaxDate.BackColor = _GFColor;
		}

		private void txtExciseTaxDate_Leave(object sender, System.EventArgs e)
		{
			txtExciseTaxDate.BackColor = _LFColorGray;
		}

		private void txtExpireDate_Enter(object sender, System.EventArgs e)
		{
			txtExpireDate.BackColor = _GFColor;
		}

		private void txtExpireDate_Leave(object sender, System.EventArgs e)
		{
			txtExpireDate.BackColor = _LFColorGray;
		}

		private void txtExpireDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(txtExpireDate.Text))
			{
				string strDate = "";
				switch (FCConvert.ToDateTime(txtExpireDate.Text).Month)
				{
					case 12:
						{
							strDate = "1/1/" + FCConvert.ToString(FCConvert.ToDateTime(txtExpireDate.Text).Year + 1);
							break;
						}
					default:
						{
							strDate = FCConvert.ToString(FCConvert.ToDateTime(txtExpireDate.Text).Month + 1) + "/1/" + FCConvert.ToString(FCConvert.ToDateTime(txtExpireDate.Text).Year);
							break;
						}
				}
				//end switch
				strDate = Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(strDate)), "MM/dd/yyyy");
				if (Strings.Format(strDate, "MM/dd/yyyy") != Strings.Format(txtExpireDate.Text, "MM/dd/yyyy"))
				{
					txtExpireDate.Text = strDate;
				}
			}
		}

		private void txtFuel_Enter(object sender, System.EventArgs e)
		{
			txtFuel.BackColor = _GFColor;
		}

		private void txtFuel_Leave(object sender, System.EventArgs e)
		{
			txtFuel.BackColor = _LFColor;
		}

		private void txtLegalres_Enter(object sender, System.EventArgs e)
		{
			txtLegalres.BackColor = _GFColor;
		}

		private void txtLegalres_Leave(object sender, System.EventArgs e)
		{
			txtLegalres.BackColor = _LFColor;
		}

		private void txtLegalres_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			if (fecherFoundation.Strings.Trim(txtresidenceCode.Text) == "")
			{
				rs.OpenRecordset("SELECT * FROM Residence WHERE Town = '" + fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(txtLegalres.Text)) + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					txtresidenceCode.Text = FCConvert.ToString(rs.Get_Fields("Code"));
				}
			}
		}

		private void txtLegalResStreet_Enter(object sender, System.EventArgs e)
		{
			txtLegalResStreet.BackColor = _GFColor;
		}

		private void txtLegalResStreet_Leave(object sender, System.EventArgs e)
		{
			txtLegalResStreet.BackColor = _LFColor;
		}

		private void txtMake_Enter(object sender, System.EventArgs e)
		{
			txtMake.BackColor = _GFColor;
		}

		private void txtMake_Leave(object sender, System.EventArgs e)
		{
			txtMake.BackColor = _LFColor;
		}

		private void txtMileage_Enter(object sender, System.EventArgs e)
		{
			txtMileage.BackColor = _GFColor;
		}

		private void txtMileage_Leave(object sender, System.EventArgs e)
		{
			txtMileage.BackColor = _LFColorGray;
		}

		private void txtModel_Enter(object sender, System.EventArgs e)
		{
			txtModel.BackColor = _GFColor;
		}

		private void txtModel_Leave(object sender, System.EventArgs e)
		{
			txtModel.BackColor = _LFColor;
		}

		private void txtNetWeight_Enter(object sender, System.EventArgs e)
		{
			txtNetWeight.BackColor = _GFColor;
		}

		private void txtNetWeight_Leave(object sender, System.EventArgs e)
		{
			txtNetWeight.BackColor = _LFColor;
		}

		private void txtOpID_Enter(object sender, System.EventArgs e)
		{
			txtOpID.BackColor = _GFColor;
		}

		private void txtOpID_Leave(object sender, System.EventArgs e)
		{
			txtOpID.BackColor = _LFColorGray;
		}

		private void txtPlate_Enter(object sender, System.EventArgs e)
		{
			txtPlate.BackColor = _GFColor;
		}

		private void txtPlate_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii >= 65 && KeyAscii <= 90) || (KeyAscii >= 97 && KeyAscii <= 122) || KeyAscii == 8 || KeyAscii == 45 || KeyAscii == 38 || KeyAscii == 32)
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtPlate_Leave(object sender, System.EventArgs e)
		{
			txtPlate.BackColor = _LFColor;
		}

		private void txtPrice_Enter(object sender, System.EventArgs e)
		{
			txtPrice.BackColor = _GFColor;
		}

		private void txtPrice_Leave(object sender, System.EventArgs e)
		{
			txtPrice.BackColor = _LFColorGray;
		}

		private void txtRBYear_Enter(object sender, System.EventArgs e)
		{
			txtRBYear.BackColor = _GFColor;
		}

		private void txtRBYear_Leave(object sender, System.EventArgs e)
		{
			txtRBYear.BackColor = _LFColorGray;
			if ((FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "CL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "C5") || (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "L6"))
			{
                if (Conversion.Val(txtRBYear.Text) == 1)
				{
					txtMilRate.Text = ".0250";
				}
				else if (Conversion.Val(txtRBYear.Text) == 2)
				{
					txtMilRate.Text = ".0200";
				}
				else if (Conversion.Val(txtRBYear.Text) == 3)
				{
					txtMilRate.Text = ".0160";
				}
				else if (Conversion.Val(txtRBYear.Text) == 4)
				{
					txtMilRate.Text = ".0120";
				}
				else if (Conversion.Val(txtRBYear.Text) == 5)
				{
					txtMilRate.Text = ".0120";
				}
				else if (Conversion.Val(txtRBYear.Text) == 6)
				{
					txtMilRate.Text = ".0120";
				}
			}
			else
			{
				if (Conversion.Val(txtRBYear.Text) == 1)
				{
					txtMilRate.Text = ".0240";
				}
				else if (Conversion.Val(txtRBYear.Text) == 2)
				{
					txtMilRate.Text = ".0175";
				}
				else if (Conversion.Val(txtRBYear.Text) == 3)
				{
					txtMilRate.Text = ".0135";
				}
				else if (Conversion.Val(txtRBYear.Text) == 4)
				{
					txtMilRate.Text = ".0100";
				}
				else if (Conversion.Val(txtRBYear.Text) == 5)
				{
					txtMilRate.Text = ".0065";
				}
				else if (Conversion.Val(txtRBYear.Text) == 6)
				{
					txtMilRate.Text = ".0040";
				}
			}
		}

		private void txtRBYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var _rbYear = txtRBYear.Text;

            if (_rbYear != "1" && _rbYear != "2" && _rbYear != "3" && _rbYear != "4" && _rbYear != "5" && _rbYear != "6")
			{
				MessageBox.Show("You may only have a value of 1 - 6 in this field.", "Invalid Mil Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}
        }

		private void txtReg1DOB_Enter(object sender, System.EventArgs e)
		{
			txtReg1DOB.BackColor = _GFColor;
		}

		private void txtReg1DOB_Leave(object sender, System.EventArgs e)
		{
			txtReg1DOB.BackColor = _LFColor;
			if (Form.ActiveForm.Name == this.Name)
			{
				// in case the focus has switched to a modal form like the party screen in the middle of code
				if (txtReg2ICM.Text == "N")
				{
					txtAddress1.Focus();
				}
			}
		}

		private void txtReg1ICM_TextChanged(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.UCase(txtReg1ICM.Text) == "I")
			{
				txtReg1DOB.Visible = true;
				txtReg1PartyID.Enabled = true;
				imgReg1Search.Enabled = true;
				imgReg1Edit.Enabled = true;
                imgEditRegistrant1.Enabled = true;
            }
			else if (fecherFoundation.Strings.UCase(txtReg1ICM.Text) == "N")
			{
				txtReg1DOB.Visible = false;
				txtReg1PartyID.Text = "";
				txtReg1PartyID.Enabled = false;
				imgReg1Search.Enabled = false;
				imgReg1Edit.Enabled = false;
                imgEditRegistrant1.Enabled = false;
				ClearPartyInfo(1);
			}
			else
			{
				txtReg1DOB.Visible = false;
				txtReg1PartyID.Enabled = true;
				imgReg1Search.Enabled = true;
				imgReg1Edit.Enabled = true;
                imgEditRegistrant1.Enabled = true;
			}
			if (txtReg1ICM.Text == "!")
			{
				txtReg1ICM.Text = "I";
			}
			if (txtReg1PartyID.Text == "")
			{
				imgReg1Edit.Enabled = false;
                imgEditRegistrant1.Enabled = false;
			}
			this.Refresh();
		}

		public void txtReg1ICM_Change()
		{
			txtReg1ICM_TextChanged(txtReg1ICM, new System.EventArgs());
		}

		private void txtReg1ICM_DoubleClick(object sender, System.EventArgs e)
		{
            txtReg1ICM.Text = GetNextICMType(txtReg1ICM.Text);
		}

		private void txtReg1ICM_Enter(object sender, System.EventArgs e)
		{
			txtReg1ICM.BackColor = _GFColor;
		}

		private void txtReg1ICM_Leave(object sender, System.EventArgs e)
		{
			txtReg1ICM.BackColor = ColorTranslator.FromOle(lngLFColorICM);
		}

		private void txtReg1ICM_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Validate_ICM_Code(txtReg1ICM.Text) == false)
			{
				MotorVehicle.Beep(1000, 250);
				e.Cancel = true;
			}
		}

		private void txtReg1LR_DoubleClick(object sender, System.EventArgs e)
		{
            txtReg1LR.Text = GetNextRegistrantType(txtReg1LR.Text);
		}

		private void txtReg1LR_Enter(object sender, System.EventArgs e)
		{
			txtReg1LR.BackColor = _GFColor;
		}

		private void txtReg1LR_Leave(object sender, System.EventArgs e)
		{
			txtReg1LR.BackColor = ColorTranslator.FromOle(lngLFColorICM);
		}

		private void txtReg1LR_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Validate_LR_Code(txtReg1LR.Text) == false)
			{
				MotorVehicle.Beep(1000, 250);
				e.Cancel = true;
			}
		}

		private void txtReg1PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg1PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolLoading)
			{
				ValidateReg1PartyID();
			}
		}

        private void ValidateReg1PartyID()
        {
	        if (Information.IsNumeric(txtReg1PartyID.Text) && Conversion.Val(txtReg1PartyID.Text) > 0)
            {

                GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtReg1PartyID.Text)), 1, MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1") == txtReg1PartyID.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ClearPartyInfo(1);
            }
        }

		private void txtReg2PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg2PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolLoading)
			{
				ValidateReg2PartyID();
			}
		}

        private void ValidateReg2PartyID()
        {
            if (Information.IsNumeric(txtReg2PartyID.Text) && Conversion.Val(txtReg2PartyID.Text) > 0)
            {
                GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtReg2PartyID.Text)), 2, MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID2") == txtReg2PartyID.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ClearPartyInfo(2);
            }
        }

		private void txtReg3PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg3PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolLoading)
			{
				ValidateReg3PartyID();
			}
		}

        private void ValidateReg3PartyID()
        {
            if (Information.IsNumeric(txtReg3PartyID.Text) && Conversion.Val(txtReg3PartyID.Text) > 0)
            {
                GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtReg3PartyID.Text)), 3, MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID3") == txtReg3PartyID.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ClearPartyInfo(3);
            }
        }
		public void ClearPartyInfo(int intReg)
		{
			switch (intReg)
			{
				case 1:
					{
						lblRegistrant1.Text = "";
						txtReg1DOB.Text = "";
						imgReg1Edit.Enabled = false;
                        imgEditRegistrant1.Enabled = false;
                        imgReg1Search.Enabled = txtReg1ICM != "N";
                        txtReg1PartyID.Enabled = false;
						strReg1FirstName = "";
                        strReg1LastName = "";
                        strReg1MI = "";
                        strReg1Designation = "";
						break;
					}
				case 2:
					{
						lblRegistrant2.Text = "";
						txtReg2DOB.Text = "";
						imgReg2Edit.Enabled = false;
                        imgEditRegistrant2.Enabled = false;
                        imgReg2Search.Enabled = txtReg2ICM != "N";
                        txtReg2PartyID.Enabled = false;
                        strReg2FirstName = "";
                        strReg2LastName = "";
                        strReg2MI = "";
                        strReg2Designation = "";
						break;
					}
				case 3:
					{
						lblRegistrant3.Text = "";
						txtReg3DOB.Text = "";
						imgReg3Edit.Enabled = false;
                        imgEditRegistrant3.Enabled = false;
                        imgReg3Search.Enabled = txtReg3ICM != "N";
                        txtReg3PartyID.Enabled = false;
                        strReg3FirstName = "";
                        strReg3LastName = "";
                        strReg3MI = "";
                        strReg3Designation = "";
						break;
					}
				default:
					{
						// do nothing
						break;
					}
			}
			//end switch
		}

		public bool GetPartyInfo(int intPartyID, int intReg, CentralPartyNameAddressCheck ignoreName = CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(intPartyID);
            
			if (!(pInfo == null))
			{
                pAdd = pInfo.GetPrimaryAddress();
                
				switch (intReg)
				{
					case 1:
						{
							
							imgReg1Edit.Enabled = true;
                            imgEditRegistrant1.Enabled = true;
                            
                            
                            bool updateInfo = false;
                            if (ignoreName == CentralPartyNameAddressCheck.UseCentralPartyInfo)
                            {
                                updateInfo = true;
							}
                            else if (ignoreName == CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent)
                            {
                                if (lblRegistrant1.Text.ToUpper() != pInfo.FullNameMiddleInitial.ToUpper()
                                    || ((txtReg1LR != "R") && (pAdd.Address1.ToUpper() != txtAddress1.Text.ToUpper()
                                    || pAdd.Address2.ToUpper() != txtAddress2.Text.ToUpper()
                                    || pAdd.City.ToUpper() != txtCity.Text.ToUpper()
                                    || pAdd.State.ToUpper() != txtState.Text.ToUpper()
                                    || pAdd.Zip.ToUpper() != txtZip.Text.ToUpper())))
                                {
                                    updateInfo = (MessageBox.Show(
                                                      "Name and / or address on the central party record is different than the information on the registration.  Would you like to update the registration with the information from the party?",
                                                      "Update Registration Information?", MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question) == DialogResult.Yes);
                                }
                            }
                            else
                            {
                                if ((MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1") == "I" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1FirstName") == "") ||
                                    (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1") != "I" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner1") == ""))
                                {
                                    updateInfo = true;
                                }
                                else if (lblRegistrant1.Text == "")
                                {
                                    if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("PartyName1") == "")
                                    {
                                        updateInfo = true;
                                    }
                                    else
                                    {
                                        lblRegistrant1.Text =
                                            MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("PartyName1").ToUpper();
                                        strReg1FirstName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1FirstName").ToUpper();
                                        strReg1LastName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1LastName").ToUpper();
                                        strReg1MI = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1MI").ToUpper();
                                        strReg1Designation = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg1Designation").ToUpper();
                                    }
                                }
                            }

							if (updateInfo)
                            {
                                intPartyType1 = pInfo.PartyType;
                                lblRegistrant1.Text = fecherFoundation.Strings.UCase(pInfo.FullNameMiddleInitial);
                                strReg1FirstName = Strings.UCase(pInfo.FirstName);
                                strReg1LastName = Strings.UCase(pInfo.LastName);
                                strReg1MI = pInfo.MiddleName.Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper();
								strReg1Designation = Strings.UCase(pInfo.Designation);

                                if (txtReg1LR != "R")
                                {
                                    FillAddressFields(intPartyID);
                                }
							}
                            
							break;
						}
					case 2:
						{
							
							imgReg2Edit.Enabled = true;
							imgEditRegistrant2.Enabled = true;

							
                            bool updateInfo = false;
                            if (ignoreName == CentralPartyNameAddressCheck.UseCentralPartyInfo)
                            {
                                updateInfo = true;
                            }
                            else if (ignoreName == CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent)
                            {
								if (lblRegistrant2.Text.ToUpper() != pInfo.FullNameMiddleInitial.ToUpper()
                                    || ((txtReg1LR == "R") && (pAdd.Address1.ToUpper() != txtAddress1.Text.ToUpper()
                                   || pAdd.Address2.ToUpper() != txtAddress2.Text.ToUpper()
                                   || pAdd.City.ToUpper() != txtCity.Text.ToUpper()
                                   || pAdd.State.ToUpper() != txtState.Text.ToUpper()
                                   || pAdd.Zip.ToUpper() != txtZip.Text.ToUpper())))
								{
                                    updateInfo = (MessageBox.Show(
                                                      "Name and / or address on the central party record is different than the information on the registration.  Would you like to update the registration with the information from the party?",
                                                      "Update Registration Information?", MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question) == DialogResult.Yes);
                                }
                            }
                            else
                            {
								if ((MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2") == "I" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2FirstName") == "") ||
                                    (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2") != "I" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner2") == ""))
								{
                                    updateInfo = true;
                                }
                                else if (lblRegistrant2.Text == "")
                                {
                                    if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("PartyName2") == "")
                                    {
                                        updateInfo = true;
                                    }
                                    else
                                    {
                                        lblRegistrant2.Text =
                                            MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("PartyName2");
                                        strReg2FirstName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2FirstName").ToUpper();
                                        strReg2LastName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2LastName").ToUpper();
                                        strReg2MI = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2MI").ToUpper();
                                        strReg2Designation = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg2Designation").ToUpper();
                                    }
                                }
                            }

							if (updateInfo)
                            {

                                lblRegistrant2.Text = fecherFoundation.Strings.UCase(pInfo.FullNameMiddleInitial);
                                intPartyType2 = pInfo.PartyType;
                                strReg2FirstName = Strings.UCase(pInfo.FirstName);
                                strReg2LastName = Strings.UCase(pInfo.LastName);
                                strReg2MI = pInfo.MiddleName.Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper();
								strReg2Designation = Strings.UCase(pInfo.Designation);

                                if (txtReg1LR == "R")
                                {
                                    FillAddressFields(intPartyID);
                                }
							}
					
							break;
						}
					case 3:
						{
							imgReg3Edit.Enabled = true;
							imgEditRegistrant3.Enabled = true;

							
                            bool updateInfo = false;
                            if (ignoreName == CentralPartyNameAddressCheck.UseCentralPartyInfo)
                            {
                                updateInfo = true;
                            }
                            else if (ignoreName == CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent)
                            {
                                if (lblRegistrant3.Text.ToUpper() != pInfo.FullNameMiddleInitial.ToUpper())
                                {
                                    updateInfo = (MessageBox.Show(
                                                      "Name and / or address on the central party record is different than the information on the registration.  Would you like to update the registration with the information from the party?",
                                                      "Update Registration Information?", MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question) == DialogResult.Yes);
								}
                            }
							else
                            {
								if ((MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3") == "I" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3FirstName") == "") ||
                                    (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode3") != "I" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Owner3") == ""))
								{
                                    updateInfo = true;
                                }
                                else if (lblRegistrant3.Text == "")
                                {
                                    if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("PartyName3") == "")
                                    {
                                        updateInfo = true;
                                    }
                                    else
                                    {
                                        lblRegistrant3.Text =
                                            MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("PartyName3");
                                        strReg3FirstName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3FirstName").ToUpper();
                                        strReg3LastName = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3LastName").ToUpper();
                                        strReg3MI = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3MI").ToUpper();
                                        strReg3Designation = MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Reg3Designation").ToUpper();
                                    }
                                }
                            }

							if (updateInfo)
                            {
                                lblRegistrant3.Text = fecherFoundation.Strings.UCase(pInfo.FullNameMiddleInitial);
                                intPartyType3 = pInfo.PartyType;
                                strReg3FirstName = Strings.UCase(pInfo.FirstName);
                                strReg3LastName = Strings.UCase(pInfo.LastName);
                                strReg3MI = pInfo.MiddleName.Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper();
								strReg3Designation = Strings.UCase(pInfo.Designation);
                            }
                            
							break;
						}
				}
			}

            return false;
        }

		private void txtReg2LR_DoubleClick(object sender, System.EventArgs e)
		{
            txtReg2LR.Text = GetNextRegistrantType(txtReg2LR.Text);
		}

		private void txtReg2LR_Enter(object sender, System.EventArgs e)
		{
			txtReg2LR.BackColor = _GFColor;
		}

		private void txtReg2LR_Leave(object sender, System.EventArgs e)
		{
			txtReg2LR.BackColor = ColorTranslator.FromOle(lngLFColorICM);
		}

		private void txtReg2LR_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Validate_LR_Code(txtReg2LR.Text) == false)
			{
				MotorVehicle.Beep(1000, 250);
				e.Cancel = true;
			}
		}

		private void txtReg3LR_DoubleClick(object sender, System.EventArgs e)
        {
            txtReg3LR.Text = GetNextRegistrantType(txtReg3LR.Text);
        }

        private string GetNextRegistrantType(string currentType)
        {
            string nextType = "N";

            switch (currentType)
            {
                case "N":
                    nextType = "E";

                    break;
                case "E":
                    nextType = "R";

                    break;
                case "R":
                    nextType = "T";

                    break;
                case "T":
                    nextType = "P";

                    break;
                case "P":
                    nextType = "N";

                    break;
            }

            return nextType;
        }

        private void txtReg3LR_Enter(object sender, System.EventArgs e)
		{
			txtReg3LR.BackColor = _GFColor;
		}

		private void txtReg3LR_Leave(object sender, System.EventArgs e)
		{
			txtReg3LR.BackColor = ColorTranslator.FromOle(lngLFColorICM);
		}

		private void txtReg3LR_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Validate_LR_Code(txtReg3LR.Text) == false)
			{
				MotorVehicle.Beep(1000, 250);
				e.Cancel = true;
			}
		}

		private void txtReg2DOB_Enter(object sender, System.EventArgs e)
		{
			txtReg2DOB.BackColor = _GFColor;
		}

		private void txtReg2DOB_Leave(object sender, System.EventArgs e)
		{
			txtReg2DOB.BackColor = _LFColor;
		}

		private void txtReg2ICM_TextChanged(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.UCase(txtReg2ICM.Text) == "I")
			{
				txtReg2DOB.Visible = true;
				txtReg2PartyID.Enabled = true;
				imgReg2Search.Enabled = true;
				imgReg2Edit.Enabled = true;
                imgEditRegistrant2.Enabled = true;
			}
			else if (fecherFoundation.Strings.UCase(txtReg2ICM.Text) == "N")
			{
				txtReg2DOB.Visible = false;
				txtReg2PartyID.Text = "";
				txtReg2PartyID.Enabled = false;
				ClearPartyInfo(2);
				imgReg2Search.Enabled = false;
				imgReg2Edit.Enabled = false;
                imgEditRegistrant2.Enabled = false;
			}
			else
			{
				txtReg2DOB.Visible = false;
				txtReg2PartyID.Enabled = true;
				imgReg2Search.Enabled = true;
				imgReg2Edit.Enabled = true;
                imgEditRegistrant2.Enabled = true;
			}
			if (txtReg2ICM.Text == "!")
			{
				txtReg2ICM.Text = "N";
			}
			if (txtReg2PartyID.Text == "")
			{
				imgReg2Edit.Enabled = false;
                imgEditRegistrant2.Enabled = false;
			}
		}

		public void txtReg2ICM_Change()
		{
			txtReg2ICM_TextChanged(txtReg2ICM, new System.EventArgs());
		}

		private void txtReg2ICM_DoubleClick(object sender, System.EventArgs e)
		{
            txtReg2ICM.Text = GetNextICMType(txtReg2ICM.Text);
		}

		private void txtReg2ICM_Enter(object sender, System.EventArgs e)
		{
			txtReg2ICM.BackColor = _GFColor;
		}

		private void txtReg2ICM_Leave(object sender, System.EventArgs e)
		{
			txtReg2ICM.BackColor = ColorTranslator.FromOle(lngLFColorICM);
		}

		private void txtReg2ICM_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Validate_ICM_Code(txtReg2ICM.Text) == false)
			{
				MotorVehicle.Beep(1000, 250);
				e.Cancel = true;
			}
		}

		private void txtReg3DOB_Enter(object sender, System.EventArgs e)
		{
			txtReg3DOB.BackColor = _GFColor;
		}

		private void txtReg3DOB_Leave(object sender, System.EventArgs e)
		{
			txtReg3DOB.BackColor = _LFColor;
			txtAddress1.Focus();
		}

		private void txtReg3ICM_TextChanged(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.UCase(txtReg3ICM.Text) == "I")
			{
				txtReg3DOB.Visible = true;
				txtReg3PartyID.Enabled = true;
				imgReg3Search.Enabled = true;
				imgReg3Edit.Enabled = true;
                imgEditRegistrant3.Enabled = true;
			}
			else if (fecherFoundation.Strings.UCase(txtReg3ICM.Text) == "N")
			{
				txtReg3DOB.Visible = false;
				txtReg3PartyID.Text = "";
				txtReg3PartyID.Enabled = false;
				ClearPartyInfo(3);
				imgReg3Search.Enabled = false;
				imgReg3Edit.Enabled = false;
                imgEditRegistrant3.Enabled = false;
			}
			else
			{
				txtReg3DOB.Visible = false;
				txtReg3PartyID.Enabled = true;
				imgReg3Search.Enabled = true;
				imgReg3Edit.Enabled = true;
                imgEditRegistrant3.Enabled = true;
			}
			if (txtReg3ICM.Text == "!")
			{
				txtReg3ICM.Text = "N";
			}
			if (txtReg3PartyID.Text == "")
			{
				imgReg3Edit.Enabled = false;
                imgEditRegistrant3.Enabled = false;
			}
		}

		public void txtReg3ICM_Change()
		{
			txtReg3ICM_TextChanged(txtReg3ICM, new System.EventArgs());
		}

		private void txtReg3ICM_DoubleClick(object sender, System.EventArgs e)
        {
            txtReg3ICM.Text = GetNextICMType(txtReg3ICM.Text);
        }

        private string GetNextICMType(string currentType)
        {
            string nextType = "N";

            switch (currentType)
            {
                case "I":
                    nextType = "C";

                    break;
                case "C":
                    nextType = "M";

                    break;
                case "M":
                    nextType = "D";

                    break;
                case "D":
                    nextType = "N";

                    break;
                case "N":
                    nextType = "I";

                    break;
            }

            return nextType;
        }

        private void txtReg3ICM_Enter(object sender, System.EventArgs e)
		{
			txtReg3ICM.BackColor = _GFColor;
		}

		private void txtReg3ICM_Leave(object sender, System.EventArgs e)
		{
			txtReg3ICM.BackColor = ColorTranslator.FromOle(lngLFColorICM);
		}

		private void txtReg3ICM_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Validate_ICM_Code(txtReg3ICM.Text) == false)
			{
				MotorVehicle.Beep(1000, 250);
				e.Cancel = true;
			}
		}

		private bool Validate_ICM_Code(string str1)
		{
			bool Validate_ICM_Code = false;
			Validate_ICM_Code = false;
			if (str1 == "I")
				Validate_ICM_Code = true;
			if (str1 == "C")
				Validate_ICM_Code = true;
			if (str1 == "M")
				Validate_ICM_Code = true;
			if (str1 == "D")
				Validate_ICM_Code = true;
			if (str1 == "N")
				Validate_ICM_Code = true;
			return Validate_ICM_Code;
		}

		private bool Validate_LR_Code(string str1)
		{
			bool Validate_LR_Code = false;
			Validate_LR_Code = false;
			if (str1 == "N")
				Validate_LR_Code = true;
			if (str1 == "E")
				Validate_LR_Code = true;
			if (str1 == "R")
				Validate_LR_Code = true;
			if (str1 == "T")
				Validate_LR_Code = true;
			if (str1 == "P")
				Validate_LR_Code = true;
			return Validate_LR_Code;
		}

		private void txtRegCredit_Enter(object sender, System.EventArgs e)
		{
			txtRegCredit.BackColor = _GFColor;
		}

		private void txtRegCredit_Leave(object sender, System.EventArgs e)
		{
			txtRegCredit.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtRegCredit.Text) == "")
			{
				txtRegCredit.Text = "0.00";
			}
			if (FCConvert.ToDecimal(txtRegFeeCharged.Text) == 0 || FCConvert.ToDecimal(txtRegFeeCharged.Text) == FCConvert.ToDecimal(txtRegRate.Text))
			{
				if (Information.IsNumeric(txtRegCredit.Text))
				{
					if (FCConvert.ToDecimal(txtRegCredit.Text) > FCConvert.ToDecimal(txtRegRate.Text))
					{
						// do nothing
					}
					else
					{
						txtRegFeeCharged.Text = Strings.Format(FCConvert.ToDecimal(txtRegRate.Text) - FCConvert.ToDecimal(txtRegCredit.Text), NUMERIC_FORMAT);
					}
				}
				else
				{
					txtRegFeeCharged.Text = Strings.Format(FCConvert.ToDecimal(txtRegRate.Text), NUMERIC_FORMAT);
				}
			}
		}

		private void txtRegFeeCharged_Enter(object sender, System.EventArgs e)
		{
			txtRegFeeCharged.BackColor = _GFColor;
		}

		private void txtRegFeeCharged_Leave(object sender, System.EventArgs e)
		{
			txtRegFeeCharged.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtRegFeeCharged.Text) == "")
			{
				txtRegFeeCharged.Text = "0.00";
			}
		}

		private void txtRegRate_Enter(object sender, System.EventArgs e)
		{
			txtRegRate.BackColor = _GFColor;
		}

		private void txtRegRate_Leave(object sender, System.EventArgs e)
		{
			txtRegRate.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtRegRate.Text) == "")
			{
				txtRegRate.Text = "0.00";
			}
			if (FCConvert.ToDecimal(txtRegFeeCharged.Text) == 0)
			{
				if (Information.IsNumeric(txtRegCredit.Text))
				{
					if (FCConvert.ToDecimal(txtRegCredit.Text) > FCConvert.ToDecimal(txtRegRate.Text))
					{
						// do nothing
					}
					else
					{
						txtRegFeeCharged.Text = Strings.Format(FCConvert.ToDecimal(txtRegRate.Text) - FCConvert.ToDecimal(txtRegCredit.Text), NUMERIC_FORMAT);
					}
				}
				else
				{
					txtRegFeeCharged.Text = Strings.Format(FCConvert.ToDecimal(txtRegRate.Text), NUMERIC_FORMAT);
				}
			}
		}

		private void txtRegWeight_Enter(object sender, System.EventArgs e)
		{
			txtRegWeight.BackColor = _GFColor;
		}

		private void txtRegWeight_Leave(object sender, System.EventArgs e)
		{
			txtRegWeight.BackColor = _LFColor;
		}

		private void txtresidenceCode_Enter(object sender, System.EventArgs e)
		{
			txtresidenceCode.BackColor = _GFColor;
		}

		private void txtresidenceCode_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtresidenceCode_Leave(object sender, System.EventArgs e)
		{
			txtresidenceCode.BackColor = _LFColor;
		}

		private void txtResState_Enter(object sender, System.EventArgs e)
		{
			txtResState.BackColor = _GFColor;
			txtResState.SelectionStart = 0;
		}

		private void txtResState_Leave(object sender, System.EventArgs e)
		{
			txtResState.BackColor = _LFColor;
		}

		private void txtResCountry_Enter(object sender, System.EventArgs e)
		{
			txtResCountry.BackColor = _GFColor;
			txtResCountry.SelectionStart = 0;
		}

		private void txtResCountry_Leave(object sender, System.EventArgs e)
		{
			txtResCountry.BackColor = _LFColor;
		}

		private void txtSalesTax_Enter(object sender, System.EventArgs e)
		{
			txtSalesTax.BackColor = _GFColor;
		}

		private void txtSalesTax_Leave(object sender, System.EventArgs e)
		{
			txtSalesTax.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtSalesTax.Text) == "")
			{
				txtSalesTax.Text = "0.00";
			}
		}

		private void txtSecondTaxIDNumber_KeyPress(ref int KeyAscii)
		{
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
		}

		private void txtState_Enter(object sender, System.EventArgs e)
		{
			txtState.BackColor = _GFColor;
		}

		private void txtState_Leave(object sender, System.EventArgs e)
		{
			txtState.BackColor = _LFColor;
		}

		private void txtStatus_Enter(object sender, System.EventArgs e)
		{
			txtStatus.BackColor = _GFColor;
		}

        private void txtStatus_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string text = txtStatus.Text;
            if (text != "A" && text != "P" && text != "T")
            {
                MessageBox.Show("You may only enter A - Active, P - Pending, or T - Terminated.", "Invalid Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void txtStatus_Leave(object sender, System.EventArgs e)
		{
			txtStatus.BackColor = _LFColorGray;
		}

		private void txtStyle_Enter(object sender, System.EventArgs e)
		{
			txtStyle.BackColor = _GFColor;
		}

		private void txtStyle_Leave(object sender, System.EventArgs e)
		{
			txtStyle.BackColor = _LFColor;
		}

		private void cboSubClass_Enter(object sender, System.EventArgs e)
		{
			cboSubClass.BackColor = _GFColor;
		}

		private void cboSubClass_Leave(object sender, System.EventArgs e)
		{
			cboSubClass.BackColor = _LFColor;
		}

		private void txtSubtotal_Enter(object sender, System.EventArgs e)
		{
			txtSubtotal.BackColor = _GFColor;
		}

		private void txtSubtotal_Leave(object sender, System.EventArgs e)
		{
			txtSubtotal.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtSubtotal.Text) == "")
			{
				txtSubtotal.Text = "0.00";
			}
		}

		private void txtTires_Enter(object sender, System.EventArgs e)
		{
			txtTires.BackColor = _GFColor;
		}

		private void txtTires_Leave(object sender, System.EventArgs e)
		{
			txtTires.BackColor = _LFColor;
		}

		private void txtTitleFee_Enter(object sender, System.EventArgs e)
		{
			txtTitleFee.BackColor = _GFColor;
		}

		private void txtTitleFee_Leave(object sender, System.EventArgs e)
		{
			txtTitleFee.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtTitleFee.Text) == "")
			{
				txtTitleFee.Text = "0.00";
			}
		}

		private void txtTransferCharge_Enter(object sender, System.EventArgs e)
		{
			txtTransferCharge.BackColor = _GFColor;
		}

		private void txtTransferCharge_Leave(object sender, System.EventArgs e)
		{
			txtTransferCharge.BackColor = _LFColorGray;
			if (fecherFoundation.Strings.Trim(txtTransferCharge.Text) == "")
			{
				txtTransferCharge.Text = "0.00";
			}
			txtExciseTaxBalance.Text = Strings.Format(FCConvert.ToDecimal(txtSubtotal.Text) + FCConvert.ToDecimal(txtTransferCharge.Text), NUMERIC_FORMAT);
		}

		private void txtUnitNumber_Enter(object sender, System.EventArgs e)
		{
			txtUnitNumber.BackColor = _GFColor;
		}

		private void txtUnitNumber_Leave(object sender, System.EventArgs e)
		{
			txtUnitNumber.BackColor = _LFColor;
		}

		private void txtVin_Enter(object sender, System.EventArgs e)
		{
			txtVin.BackColor = _GFColor;
		}

		private void txtVin_Leave(object sender, System.EventArgs e)
		{
			txtVin.BackColor = _LFColor;
		}

		private void txtVin_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strYear;
            if (VerifyVIN() == false)
				e.Cancel = true;
            if ((fecherFoundation.Strings.Trim(txtMake.Text) != "" &&
                 fecherFoundation.Strings.Trim(txtYear.Text) != "") ||
                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CV")
            {
	            return;
            }

            strYear = GetYearFromVin(txtVin.Text);
            if (txtVin.Text.Length == 17)
            {
	            var vehicle = this.BlueBookRepository.VehicleInformation.GetVehicleInformation(txtVin.Text).FirstOrDefault();
	            if (vehicle != null)
	            {
		            strYear = vehicle.ModelYear.ToString();
		            string strMake = vehicle.ManufacturerName.Left(4).ToUpper();
		            string strType = GetVehicleTypeCode(vehicle.ClassificationId, vehicle.CategoryId);
		            txtMake.Text = modGlobalFunctions.ReturnCorrectedMakeCode(strType, vehicle.ManufacturerName, strMake);
		            txtModel.Text = vehicle.ModelName.Left(6).ToUpper();
	            }
            }
            txtYear.Text = strYear;
		}

		private void txtYear_Enter(object sender, System.EventArgs e)
		{
			txtYear.BackColor = _GFColor;
		}

		private void txtYear_Leave(object sender, System.EventArgs e)
		{
			txtYear.BackColor = _LFColor;
		}

		private void txtZip_Enter(object sender, System.EventArgs e)
		{
			txtZip.BackColor = _GFColor;
		}

		private void txtZip_Leave(object sender, System.EventArgs e)
		{
			txtZip.BackColor = _LFColor;
		}

		private void txtCountry_Enter(object sender, System.EventArgs e)
		{
			txtCountry.BackColor = _GFColor;
		}

		private void txtCountry_Leave(object sender, System.EventArgs e)
		{
			txtCountry.BackColor = _LFColor;
		}

		//FC:FINAL:MSH - i.issue #1770: don't need to use 'ref' to avoid exceptions on executing method(value won't be changed and)
		//private void FillBooster(ref int lngID)
		private void FillBooster(int lngID)
		{
			clsDRWrapper rs = new clsDRWrapper();
			Frame2.Visible = true;
			//FC:FINAL:MSH - i.issue #1770: wrong list indexes
			//Label31[11].Visible = true;
			//Label31[12].Visible = true;
			//Label31[13].Visible = true;
			//Label31[14].Visible = true;
			Label31[1].Visible = true;
			Label31[2].Visible = true;
			Label31[3].Visible = true;
			Label31[4].Visible = true;
			txtPermit.Visible = true;
			txtBoosterEffective.Visible = true;
			txtBoosterExpiration.Visible = true;
			txtBoosterWeight.Visible = true;
			rs.OpenRecordset("SELECT * FROM Booster WHERE ID = " + FCConvert.ToString(lngID));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				txtPermit.Text = FCConvert.ToString(rs.Get_Fields_Int32("Permit"));
				txtBoosterEffective.Text = FCConvert.ToString(rs.Get_Fields("EffectiveDate"));
				txtBoosterExpiration.Text = FCConvert.ToString(rs.Get_Fields("ExpireDate"));
				txtBoosterWeight.Text = FCConvert.ToString(rs.Get_Fields_Int32("BoostedWeight"));
			}
		}

		private void HideBooster()
		{
			txtPermit.Text = "";
			txtBoosterEffective.Text = "";
			txtBoosterExpiration.Text = "";
			txtBoosterWeight.Text = "";
			Frame2.Visible = false;
			//FC:FINAL:MSH - i.issue #1770: wrong list indexes
			//Label31[11].Visible = false;
			//Label31[12].Visible = false;
			//Label31[13].Visible = false;
			//Label31[14].Visible = false;
			Label31[1].Visible = false;
			Label31[2].Visible = false;
			Label31[3].Visible = false;
			Label31[4].Visible = false;
			txtPermit.Visible = false;
			txtBoosterEffective.Visible = false;
			txtBoosterExpiration.Visible = false;
			txtBoosterWeight.Visible = false;
		}

		public void SetToolTips()
		{
			clsDRWrapper rsSubClass = new clsDRWrapper();
			txtStatus.ToolTipText = "A - Active   P - Pending   T - Terminated";
			ToolTip1.SetToolTip(txtRBYear, "1 - .024   2 - .0175   3 - .0135   4 - .01   5 - .0065   6 - .004");
		}

		private void SetUserFields()
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel1")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField1.Visible = true;
					txtUserField1.Visible = true;
					lblInfo1.Visible = true;
					txtUserReason1.Visible = true;
					lblUserField1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1")));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel2")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField2.Visible = true;
					txtUserField2.Visible = true;
					lblInfo2.Visible = true;
					txtUserReason2.Visible = true;
					lblUserField2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2")));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel3")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField3.Visible = true;
					txtUserField3.Visible = true;
					lblInfo3.Visible = true;
					txtUserReason3.Visible = true;
					lblUserField3.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3")));
				}
			}
			rsDefaultInfo.Reset();
		}

		private void FillExtraInfo()
		{
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Battle")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Battle"))) == "")
				{
					txtBattle.Text = "X";
				}
				else
				{
					txtBattle.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Battle"));
				}
			}
			if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("Mail")))
			{
				cmbtNo.Text = "Yes";
			}
			else
			{
				cmbtNo.Text = "No";
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("EMailAddress")))
			{
				txtEMail.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("EMailAddress"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserMessage")) && !fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag")))
			{
				if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag")) > 0)
				{
					cboMessageCodes.SelectedIndex = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag")) - 1);
					txtMessage.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserMessage"));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField1")))
			{
				txtUserField1.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField1"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason1")))
			{
				txtUserReason1.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason1"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField2")))
			{
				txtUserField2.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField2"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason2")))
			{
				txtUserReason2.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason2"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField3")))
			{
				txtUserField3.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField3"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason3")))
			{
				txtUserReason3.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason3"));
			}
		}

		public bool VerifyVIN()
		{
			bool VerifyVIN = false;
			VerifyVIN = true;
			if (blnFinalCheck)
			{
				if ((MotorVehicle.Statics.Class == "MH") || (MotorVehicle.Statics.Class == "RV") || (MotorVehicle.Statics.Class == "WX") || (MotorVehicle.Statics.Class == "TC") || (MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "CL") || (MotorVehicle.Statics.Class == "TL"))
				{
					// do nothing
				}
				else
				{
					if (Conversion.Val(txtYear.Text) > 1980 || txtYear.Text == "")
					{
						if (CheckDigit(txtVin.Text) == false)
						{
							// Bp = Beep(1000, 250)
							// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
							DialogResult answer;
							answer = MessageBox.Show("The VIN,  " + txtVin.Text + "  is Invalid for this Vehicle. Should it be used?", "Invalid VIN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (answer == DialogResult.No)
							{
								txtVin.TabStop = true;
								VerifyVIN = false;
								txtVin.Focus();
								return VerifyVIN;
							}
							else
							{
								VerifyVIN = true;
							}
						}
					}
				}
			}
			return VerifyVIN;
		}

		public bool CheckDigit(string hold)
		{
			bool CheckDigit = false;
			// vbPorter upgrade warning: cd As int	OnWriteFCConvert.ToInt32(
			int[] cd = new int[17 + 1];
			int fnx;
			string OneDigit = "";
			float cdvalue;
			float cdOperand;
			float cdOperand2;
			CheckDigit = false;
			for (fnx = 1; fnx <= 17; fnx++)
			{
				OneDigit = Strings.Mid(hold, fnx, 1);
				if (string.Compare(OneDigit, "0") < 0)
					return CheckDigit;
				if (string.Compare(OneDigit, "9") > 0)
				{
					if (string.Compare(OneDigit, "A") < 0)
						return CheckDigit;
					if (string.Compare(OneDigit, "Z") > 0)
						return CheckDigit;
					if (string.Compare(OneDigit, "I") < 0)
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 64);
					}
					else if (string.Compare(OneDigit, "S") < 0)
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 73);
					}
					else
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 81);
					}
				}
				else
				{
					cd[fnx] = FCConvert.ToInt32(Math.Round(Conversion.Val(OneDigit)));
				}
			}
			// fnx
			cdvalue = cd[1] * 8;
			cdvalue += (cd[2] * 7);
			cdvalue += (cd[3] * 6);
			cdvalue += (cd[4] * 5);
			cdvalue += (cd[5] * 4);
			cdvalue += (cd[6] * 3);
			cdvalue += (cd[7] * 2);
			cdvalue += (cd[8] * 10);
			cdvalue += (cd[10] * 9);
			cdvalue += (cd[11] * 8);
			cdvalue += (cd[12] * 7);
			cdvalue += (cd[13] * 6);
			cdvalue += (cd[14] * 5);
			cdvalue += (cd[15] * 4);
			cdvalue += (cd[16] * 3);
			cdvalue += (cd[17] * 2);
			if (cd[9] == FCUtils.iMod(cdvalue, 11))
				CheckDigit = true;
			if (FCUtils.iMod(cdvalue, 11) == 10 && Strings.Mid(hold, 9, 1) == "X")
				CheckDigit = true;
			return CheckDigit;
		}

		private void SetCustomFormColors()
		{
			Label10.BackColor = Color.White;
			Label11.BackColor = Color.White;
			Label12.BackColor = Color.White;
			Label13.BackColor = Color.White;
			Label14.BackColor = Color.White;
			Label15.BackColor = Color.White;
			Label16.BackColor = Color.White;
			Label17.BackColor = Color.White;
			Label18.BackColor = Color.White;
			Label19.BackColor = Color.White;
			Label20.BackColor = Color.White;
			Label28.BackColor = Color.White;
			//FC:FINAL:MSH - i.issue #1770: remove label, which overlapps another fields (in original is used as border area for textbox)
			//Label41.BackColor = Color.White;
			Label23.BackColor = Color.White;
			Label24.BackColor = Color.White;
			//FC:FINAL:MSH - i.issue #1770: remove label, which overlapps another fields (in original is used as border area for textbox)
			//Label25.BackColor = Color.White;
			//FC:FINAL:MSH - i.issue #1770: remove label, which overlapps another fields (in original is used as border area for textbox)
			//Label26.BackColor = Color.White;
			Label27.BackColor = Color.White;
			Label29.BackColor = Color.White;
			Label33.BackColor = Color.White;
			Label38.BackColor = Color.White;
			Label47.BackColor = Color.White;
			//FC:FINAL:MSH - i.issue #1893: remove gray BackColors from textboxes
			//txtEffectiveDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtExpireDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtMileage.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtPrice.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtRBYear.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtMilRate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtAgentFee.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtAmountOfTax.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtCredit.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtTransferCharge.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtExciseTaxBalance.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtSubtotal.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtExciseTaxDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtCreditNumber.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtMVR3.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtRegRate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtRegCredit.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtRegFeeCharged.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtOpID.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtSalesTax.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtTitleFee.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//txtStatus.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		}

		private void LoadClassCombo()
		{
			clsDRWrapper rsClass = new clsDRWrapper();
			cboClass.Clear();
			rsClass.OpenRecordset("SELECT DISTINCT BMVCode FROM Class ORDER BY BMVCode");
			if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
			{
				do
				{
					cboClass.AddItem(rsClass.Get_Fields_String("BMVCode"));
					rsClass.MoveNext();
				}
				while (rsClass.EndOfFile() != true);
			}
		}

		private void LoadSubclassCombo(string strClass)
		{
			clsDRWrapper rsSub = new clsDRWrapper();
			cboSubClass.Clear();
			rsSub.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + strClass + "' ORDER BY SystemCode");
			if (rsSub.EndOfFile() != true && rsSub.BeginningOfFile() != true)
			{
				do
				{
					cboSubClass.AddItem(rsSub.Get_Fields_String("SystemCode") + " - " + rsSub.Get_Fields_String("Description"));
					rsSub.MoveNext();
				}
				while (rsSub.EndOfFile() != true);
				cboSubClass.SelectedIndex = 0;
			}
		}

		//FC:FINAL:MSH - i.issue #1770: don't need to use 'ref' to avoid exceptions on executing method(value won't be changed and)
		//private void SetClassCombo(ref string strClass)
		private void SetClassCombo(string strClass)
		{
			int counter;
			for (counter = 0; counter <= cboClass.Items.Count - 1; counter++)
			{
				if (cboClass.Items[counter].ToString() == strClass)
				{
					cboClass.SelectedIndex = counter;
					break;
				}
			}
		}

		//FC:FINAL:MSH - i.issue #1770: don't need to use 'ref' to avoid exceptions on executing method(value won't be changed and)
		//private void SetSubClassCombo(ref string strSub)
		private void SetSubClassCombo(string strSub)
		{
			int counter;
			for (counter = 0; counter <= cboSubClass.Items.Count - 1; counter++)
			{
				if (Strings.Left(cboSubClass.Items[counter].ToString(), 2) == strSub)
				{
					cboSubClass.SelectedIndex = counter;
					break;
				}
			}
		}

		private void cboSubClass_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSubClass.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboClass";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Class";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboMessageCodes";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Message Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboSubClass";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Sub-Class";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			//FC:FINAL:MSH - i.issue #1770: radioButtons were replaced by combobox
			//clsControlInfo[intTotalNumberOfControls].ControlName = "optYes";
			//clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.OptionButton;
			clsControlInfo[intTotalNumberOfControls].ControlName = "cmbtNo";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Mail Reminder";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAddress1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAddress2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAgentExempt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.Label;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Agent Fee Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAgentFee";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Agent Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAmountOfTax";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Amount of Tax";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtAxles";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Axles";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtBattle";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.Label;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Battle Decal";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtBoosterEffective";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Booster Effective Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtBoosterExpiration";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Booster Expiration Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtBoosterWeight";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Booster Weight";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCity";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "City";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtColor1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Color 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtColor2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Color 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCountry";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Country";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCredit";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Excise Credit";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCreditNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Credit MVR3 Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMVR3";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "MVR3 Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDOTNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "DOT Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtEffectiveDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Effective Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtEmail";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "E-Mail";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtExciseExempt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.Label;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Excise Tax Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtExciseTaxBalance";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Excise Balance";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtExciseTaxDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Excise Tax Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtExpireDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtFleetNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Fleet / Group Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "lblFleetName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.Label;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Fleet / Group  Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtForced";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.Label;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Forced Plate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtFuel";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Fuel";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtLegalRes";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Legal Residence City";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtLegalResStreet";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Legal Residence Address";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMake";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Make";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMessage";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Message";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMileage";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Mileage";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMilRate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Mil Rate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtModel";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Model";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMonthStickerNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Month Sticker Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtNetWeight";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Net Weight";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtOpID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Operator ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtPermit";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Permit";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtPlate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Plate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtPrice";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Price";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRBYear";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Year";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg1PartyID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 1 Party ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg1DOB";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 1 DOB";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg1ICM";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 1 Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg1LR";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 1 Lease Flag";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg2PartyID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 2 Party ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg2DOB";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 2 DOB";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg2ICM";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 2 Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg2LR";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 2 Lease Flag";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg3PartyID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 3 Party ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg3DOB";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 3 DOB";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg3ICM";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 3 Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg3LR";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Reg 3 Lease Flag";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRegCredit";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Registration Credit";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRegFeeCharged";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Registration Charge";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRegistrationType";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Registration Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRegRate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Registration Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtRegWeight";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "GVW";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtResCountry";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Legal Residence Country";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtResidenceCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Residence Code";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtResState";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Legal Residence State";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtSalesTax";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Sales Tax";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtState";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStateExempt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.Label;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Registration Fee Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Status";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtStyle";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Style";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtSubTotal";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Local Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtTaxIDNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Tax ID Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtTires";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Tires";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtTitleFee";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Title Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtTransferCharge";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Transfer Charge";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtTransferExempt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.Label;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Transfer Fee Exempt";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtUnitNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Unit #";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserField1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Field 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserField2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Field 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserField3";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Field 3";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserReason1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Reason 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserReason2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Reason 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserReason3";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Reason 3";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtVIN";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "VIN";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtYear";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Year";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtYearStickerNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Year Sticker Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1770: initialize object to avoid missing data and exceptions
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtZip";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Zip Code";
			intTotalNumberOfControls += 1;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{

		}

		private void chkOverrideFleetEmail_CheckedChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (chkOverrideFleetEmail.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtEMail.Enabled = true;
			}
			else
			{
				rsTemp.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = 0 AND Type <> 'L' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)));
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					txtEMail.Text = FCConvert.ToString(rsTemp.Get_Fields_String("EmailAddress"));
				}
				txtEMail.Enabled = false;
			}
		}

		private void imgFleetComment_Click(object sender, System.EventArgs e)
		{
			mnuFleetGroupComment_Click();
		}

		private void mnuFleetGroupComment_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtFleetNumber.Text) > 0)
			{
				if (!frmCommentOld.InstancePtr.Init("MV", "FleetMaster", "Comment", "FleetNumber", FCConvert.ToInt32(FCConvert.ToDouble(txtFleetNumber.Text)), boolModal: true))
				{
					imgFleetComment.Visible = true;
				}
				else
				{
					imgFleetComment.Visible = false;
				}
			}
		}

		public void mnuFleetGroupComment_Click()
		{
			mnuFleetGroupComment_Click(cmdFleetGroupComment, new System.EventArgs());
		}

		private void txtFleetNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			VerifyFleetInfo();
		}

		public bool VerifyFleetInfo()
		{
			clsDRWrapper rsFT = new clsDRWrapper();
			string tempdate = "";
			clsDRWrapper rsClassInfo = new clsDRWrapper();
			clsDRWrapper rsOverride = new clsDRWrapper();
			string strMI = "";

			if (txtFleetNumber.Text.ToIntegerValue() != 0)
			{
				rsFT.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = 0 AND Type <> 'L' AND FleetNumber = " + txtFleetNumber.Text.ToIntegerValue());
				if (rsFT.EndOfFile() != true && rsFT.BeginningOfFile() != true)
				{
					if (rsFT.Get_Fields_Int32("ExpiryMonth") != 99)
					{
						rsClassInfo.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
						if (rsClassInfo.EndOfFile() != true && rsClassInfo.BeginningOfFile() != true)
						{
							if (FCConvert.ToString(rsClassInfo.Get_Fields_String("RenewalDate")) == "F")
							{
								if (Conversion.Val(rsFT.Get_Fields_Int32("ExpiryMonth")) != MotorVehicle.Statics.gintFixedMonthMonth)
								{
									MessageBox.Show("You may not add a vehicle with a fixed renewal date to a fleet that has an expiration month other than the required expiration month", "Invalid Expiration Month", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									txtFleetNumber.Text = "";
									return false;
								}
							}
						}

						if (MessageBox.Show("Warning - Fees will NOT be prorated." + "\r\n" + "If fees should prorate, please add vehicle to fleet during registration process ONLY.  Do you wish to continue?", "Add Vehicle to Fleet / Group?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						{
							txtFleetNumber.Text = "";
							return false;
						}
					}

					lblFleetName.Text = MotorVehicle.GetPartyNameMiddleInitial(rsFT.Get_Fields_Int32("PartyID1"));
					
					if (FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode1")) == "")
					{
						txtReg1ICM.Text = "I";
					}
					else
					{
						txtReg1ICM.Text = FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode1"));
					}
					if (FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode2")) == "")
					{
						txtReg2ICM.Text = "N";
					}
					else
					{
						txtReg2ICM.Text = FCConvert.ToString(rsFT.Get_Fields_String("Ownercode2"));
					}
					if (FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode3")) == "")
					{
						txtReg3ICM.Text = "N";
					}
					else
					{
						txtReg3ICM.Text = FCConvert.ToString(rsFT.Get_Fields_String("Ownercode3"));
					}
					if ((modGlobalConstants.Statics.MuniName.ToUpper() == "STAAB AGENCY") && rsFT.Get_Fields("State").ToString().Trim() != "ME")
					{
						rsOverride.OpenRecordset("SELECT * FROM GroupAddressOverride");
						if (rsOverride.EndOfFile() != true && rsOverride.BeginningOfFile() != true)
						{
							if (!MotorVehicle.Statics.blnSuspended)
							{
								if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("Address")))
									txtAddress1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields_String("Address")));
							}
							else
							{
								txtAddress1.Text = "REGISTRATION SUSPENDED";
							}
							if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("Address2")))
								txtAddress2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields_String("Address2")));
							if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("City")))
								txtCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields_String("City")));
							if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields("State")))
								txtState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields("State")));
							if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("Zip")))
							{
								if (FCConvert.ToString(rsOverride.Get_Fields_String("Zip")).Length < 5)
								{
									txtZip.Text = "0" + rsOverride.Get_Fields_String("Zip");
								}
								else
								{
									txtZip.Text = FCConvert.ToString(rsOverride.Get_Fields_String("Zip"));
								}
							}
							if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("Country")))
								txtCountry.Text =
										fecherFoundation.Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields_String("Country"))).Length <= 2 ?
										fecherFoundation.Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields_String("Country"))) :
										"";
						}
					}
					else
					{
						if (!MotorVehicle.Statics.blnSuspended)
						{
							if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Address")))
								txtAddress1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Address")));
						}
						else
						{
							txtAddress1.Text = "REGISTRATION SUSPENDED";
						}
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Address2")))
							txtAddress2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Address2")));
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("City")))
							txtCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("City")));
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields("State")))
							txtState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields("State")));
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Zip")))
						{
							if (FCConvert.ToString(rsFT.Get_Fields_String("Zip")).Length < 5)
							{
								txtZip.Text = "0" + rsFT.Get_Fields_String("Zip");
							}
							else
							{
								txtZip.Text = FCConvert.ToString(rsFT.Get_Fields_String("Zip"));
							}
						}
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Country")))
							txtCountry.Text =
									fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Country"))).Length <= 2 ?
									fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Country"))) :
									"";
					}
					txtEMail.Text = FCConvert.ToString(rsFT.Get_Fields_String("EmailAddress"));
					txtEMail.Enabled = false;
					chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
					chkOverrideFleetEmail.Visible = true;
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Comment"))) != "")
					{
						imgFleetComment.Visible = true;
					}
					else
					{
						imgFleetComment.Visible = false;
					}
					cmdFleetGroupComment.Enabled = true;
					txtReg1PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID1"));
					GetPartyInfo(rsFT.Get_Fields_Int32("PartyID1"), 1, MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber") == txtFleetNumber.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
					if (rsFT.Get_Fields_Int32("PartyID2") != 0)
					{
						txtReg2PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID2"));
						GetPartyInfo(rsFT.Get_Fields_Int32("PartyID2"), 2, MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber") == txtFleetNumber.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
					}
					if (rsFT.Get_Fields_Int32("PartyID3") != 0)
					{
						txtReg3PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID3"));
						GetPartyInfo(rsFT.Get_Fields_Int32("PartyID3"), 3, MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber") == txtFleetNumber.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
					}
					txtTaxIDNumber.Text = FCConvert.ToString(rsFT.Get_Fields_String("TaxID"));
					string vbPorterVar = txtReg1ICM.Text;
					if (vbPorterVar == "I")
					{
						txtReg1DOB.Text = Strings.Format(rsFT.Get_Fields("DOB1"), "MM/dd/yyyy");
					}
					else
					{
						txtReg1DOB.Text = "";
					}
					string vbPorterVar1 = txtReg2ICM.Text;
					if (vbPorterVar1 == "I")
					{
						txtReg2DOB.Text = Strings.Format(rsFT.Get_Fields("DOB2"), "MM/dd/yyyy");
					}
					else
					{
						txtReg2DOB.Text = "";
					}
					string vbPorterVar2 = txtReg3ICM.Text;
					if (vbPorterVar2 == "I")
					{
						txtReg3DOB.Text = Strings.Format(rsFT.Get_Fields("DOB3"), "MM/dd/yyyy");
					}
					else
					{
						txtReg3DOB.Text = "";
					}

					return true;
				}
				else
				{
					MessageBox.Show("There was no match found for Fleet / Group Master Number " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Invalid Fleet / Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtFleetNumber.Text = "";
					chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
					chkOverrideFleetEmail.Visible = false;
					txtEMail.Text = "";
					txtEMail.Enabled = true;
					imgFleetComment.Visible = false;
					cmdFleetGroupComment.Enabled = false;
					return false;
				}
			}

			return false;
		}

        private void FillAddressFields(int partyId)
        {
	        cPartyController pCont = new cPartyController();
	        cParty pInfo;
	        cPartyAddress pAdd;

	        pInfo = pCont.GetParty(partyId);
	        if (!(pInfo == null))
	        {
		        pAdd = pInfo.GetPrimaryAddress();
		        if (pAdd != null)
		        {
			        txtAddress1.Text = pAdd.Address1;
			        txtAddress2.Text = pAdd.Address2;
			        txtCity.Text = pAdd.City;
			        txtState.Text = pAdd.State;
			        txtZip.Text = pAdd.Zip;
		        }
	        }
        }

        private void InitializeForm()
        {
	        clsDRWrapper rsFlInfo = new clsDRWrapper();
	        clsDRWrapper rsBoost = new clsDRWrapper();
	        int counter;

	        SetUserFields();
			FillExtraInfo();
			if (MotorVehicle.Statics.NewToTheSystem != true)
			{

				// found a record fill screen

				bolAddNewRecord = false;

				// Fill Vehicle Information

				txtStatus.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Status"));
				txtRBYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MillYear"));
				if ((FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "CL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "C5") || (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "L6"))
				{
					if (Conversion.Val(txtRBYear.Text) == 1)
					{
						txtMilRate.Text = ".0250";
					}
					else if (Conversion.Val(txtRBYear.Text) == 2)
					{
						txtMilRate.Text = ".0200";
					}
					else if (Conversion.Val(txtRBYear.Text) == 3)
					{
						txtMilRate.Text = ".0160";
					}
					else if (Conversion.Val(txtRBYear.Text) == 4)
					{
						txtMilRate.Text = ".0120";
					}
					else if (Conversion.Val(txtRBYear.Text) == 5)
					{
						txtMilRate.Text = ".0120";
					}
					else if (Conversion.Val(txtRBYear.Text) == 6)
					{
						txtMilRate.Text = ".0120";
					}
				}
				else
				{
					if (Conversion.Val(txtRBYear.Text) == 1)
					{
						txtMilRate.Text = ".0240";
					}
					else if (Conversion.Val(txtRBYear.Text) == 2)
					{
						txtMilRate.Text = ".0175";
					}
					else if (Conversion.Val(txtRBYear.Text) == 3)
					{
						txtMilRate.Text = ".0135";
					}
					else if (Conversion.Val(txtRBYear.Text) == 4)
					{
						txtMilRate.Text = ".0100";
					}
					else if (Conversion.Val(txtRBYear.Text) == 5)
					{
						txtMilRate.Text = ".0065";
					}
					else if (Conversion.Val(txtRBYear.Text) == 6)
					{
						txtMilRate.Text = ".0040";
					}
				}
				txtOpID.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OpID"));
				chkOverrideFleetEmail.Visible = false;
				chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
				txtEMail.Enabled = true;
				imgFleetComment.Visible = false;
				cmdFleetGroupComment.Enabled = false;



				if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber")))
				{
					if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber")) != 0)
					{
						rsFlInfo.OpenRecordset("SELECT * FROM FleetMaster WHERE ISNULL(Deleted,0) = 0 AND FleetNumber = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber"));
						if (rsFlInfo.EndOfFile() != true && rsFlInfo.BeginningOfFile() != true)
						{
							rsFlInfo.MoveLast();
							rsFlInfo.MoveFirst();
							txtFleetNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber"));
							lblFleetName.Text = rsFlInfo.Get_Fields("Owner1Name").ToString();
							chkOverrideFleetEmail.Visible = true;
							if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("OverrideFleetEmail")))
							{
								chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								txtEMail.Enabled = false;
							}
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFlInfo.Get_Fields_String("Comment"))) != "")
							{
								imgFleetComment.Visible = true;
							}
							else
							{
								imgFleetComment.Visible = false;
							}
							cmdFleetGroupComment.Enabled = true;
							txtFleetNumber.Enabled = true;
						}
					}
				}

				txtPlate.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
				SetClassCombo(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "!!")
				{

					//FC:FINAL:MSh - i.issue #1770: don't need to use 'ref' (value won't be changed)

					//SetSubClassCombo(ref MotorVehicle.Statics.Subclass);

					SetSubClassCombo(MotorVehicle.Statics.Subclass);
				}
				else
				{
					SetSubClassCombo(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass"));
				}
				if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("Odometer")) == false)
				{
					txtMileage.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("Odometer"));
				}
				else
				{
					txtMileage.Text = "";
				}
				if (FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MonthStickerNumber")) != 0)
				{
					txtMonthStickerNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MonthStickerNumber"));
				}
				if (FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("YearStickerNumber")) != 0)
				{
					txtYearStickerNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("YearStickerNumber"));
				}
				txtMonthStickerNumber.Enabled = false;
				txtYearStickerNumber.Enabled = false;
				txtVin.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Vin"));
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("Year"));
				txtMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("make"));
				txtModel.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("model"));
				txtColor1.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("color1"));
				txtColor2.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("color2"));
				txtStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Style"));
				txtTires.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("Tires"));
				txtAxles.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("Axles"));
				txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("NetWeight"));
				txtRegWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("RegisteredWeightNew"));
				txtFuel.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Fuel"));
				txtPrice.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("BasePrice"), "#,###");
				txtDOTNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("DOTNumber"));
				if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ForcedPlate")))
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ForcedPlate")) == "" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ForcedPlate")) == "N")
					{
						txtForced.Text = "N";
					}
					else
					{
						txtForced.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ForcedPlate"));
					}
				}
				else
				{
					txtForced.Text = "N";
				}



				if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MVR3") > 0)

				{

					txtMVR3.Text = MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MVR3").ToString();

					strOriginalMVR = MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MVR3").ToString();

				}

				// Fill Owner Information

				Fill_Owner_Information();
				// Fill Fees Information

				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ExciseExempt")))
				{
					txtExciseExempt.Text = "Y";
				}
				else
				{
					txtExciseExempt.Text = "N";
				}
				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("AgentExempt")))
				{
					txtAgentExempt.Text = "Y";
				}
				else
				{
					txtAgentExempt.Text = "N";
				}
				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("RegExempt")))
				{
					txtStateExempt.Text = "Y";
				}
				else
				{
					txtStateExempt.Text = "N";
				}
				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("TransferExempt")))
				{
					txtTransferExempt.Text = "Y";
				}
				else
				{
					txtTransferExempt.Text = "N";
				}
				txtExpireDate.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExpireDate"), "MM/dd/yyyy");
				txtAgentFee.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("AgentFee"))), NUMERIC_FORMAT);
				txtAmountOfTax.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseTaxCharged"))), NUMERIC_FORMAT);

				if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ExciseCreditMVR3")))
				{

					// kk07142016  Separate MVR3 from Excise Credit MVR3

					txtCreditNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ExciseCreditMVR3"));
				}
				txtCredit.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseCreditUsed"))), NUMERIC_FORMAT);

				//FC:FINAL:MSH - i.issue #1770: '-' operator can't be applied for types string and string

				//txtRegFeeCharged.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("StatePaid")))) - FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("SalesTax")))) - FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TitleFee")))), NUMERIC_FORMAT);

				txtRegFeeCharged.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("StatePaid")) - Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("SalesTax")) - Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TitleFee")), NUMERIC_FORMAT);
				txtRegCredit.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("TransferCreditUsed"))), NUMERIC_FORMAT);
				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("RegHalf")))
				{

					// kk01282016 tromv-979   Need to be able to correctly indicate Half Rate

					chkRegHalf.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkRegHalf.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtRegRate.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("RegRateCharge"))), NUMERIC_FORMAT);
				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ExciseHalfRate")))
				{

					// kk01282016 tromv-979   Need to be able to correctly indicate Half Rate

					chkExciseHalfRate.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkExciseHalfRate.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseTaxCharged") > MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseCreditUsed"))
				{

					//FC:FINAL:MSH - i.issue #1770: '-' operator can't be applied for types string and string

					//txtSubtotal.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExciseTaxCharged")))) - FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExciseCreditUsed")))), NUMERIC_FORMAT);

					txtSubtotal.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseTaxCharged")) - Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseCreditUsed")), NUMERIC_FORMAT);
				}
				else
				{
					txtSubtotal.Text = Strings.Format(0, NUMERIC_FORMAT);
				}
				txtTransferCharge.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseTransferCharge"))), NUMERIC_FORMAT);
				txtExciseTaxBalance.Text = Strings.Format(FCConvert.ToSingle(FCConvert.ToDouble(txtSubtotal.Text)) + FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseTransferCharge")))), NUMERIC_FORMAT);
				txtExciseTaxDate.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExciseTaxDate"), "MM/dd/yyyy");
				txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("EffectiveDate"), "MM/dd/yyyy");
				txtSalesTax.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("SalesTax"))), NUMERIC_FORMAT);
				txtTitleFee.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TitleFee"))), NUMERIC_FORMAT);
			}
			else
			{

				// no record user is adding

				bolAddNewRecord = true;
				txtPlate.Text = frmPlateInfo.InstancePtr.txtPlateType[0].Text;

				// Plate1

				SetClassCombo(MotorVehicle.Statics.Class);
				txtState.Text = "ME";
				txtStatus.Text = "A";
				txtCountry.Text = "US";
				txtResCountry.Text = "US";
				SetSubClassCombo(MotorVehicle.Statics.Subclass);
				txtAgentFee.Text = "0.00";
				txtAmountOfTax.Text = "0.00";
				txtCredit.Text = "0.00";
				txtSubtotal.Text = "0.00";
				txtTransferCharge.Text = "0.00";
				txtExciseTaxBalance.Text = "0.00";
				txtSalesTax.Text = "0.00";
				txtTitleFee.Text = "0.00";
				txtRegRate.Text = "0.00";
				txtRegCredit.Text = "0.00";
				txtRegFeeCharged.Text = "0.00";
				chkRegHalf.CheckState = Wisej.Web.CheckState.Unchecked;
				chkExciseHalfRate.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UnitNumber")))
			{
				txtUnitNumber.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UnitNumber")));
			}
			rsBoost.OpenRecordset("SELECT * FROM Booster WHERE isnull(Voided, 0) = 0 and Inactive = 0 AND ExpireDate >= '" + DateTime.Today.ToShortDateString() + "' AND VehicleID = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID") + " ORDER BY BoostedWeight DESC", "TWMV0000.vb1");
			if (rsBoost.EndOfFile() != true && rsBoost.BeginningOfFile() != true)
			{
				FillBooster(rsBoost.Get_Fields_Int32("ID"));
			}
			else
			{
				HideBooster();
			}
			txtPermit.Enabled = false;
			txtBoosterEffective.Enabled = false;
			txtBoosterExpiration.Enabled = false;
			txtBoosterWeight.Enabled = false;
			if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType")) == "RRR")
			{
				txtRegistrationType.Text = "Re-Registration Regular";
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "NRR")
			{
				txtRegistrationType.Text = "New-Registration Regular";
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "NRT")
			{
				txtRegistrationType.Text = "New-Registration Transfer";
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "RRT")
			{
				txtRegistrationType.Text = "Re-Registration Transfer";
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "DPS")
			{
				txtRegistrationType.Text = "Duplicate Stickers";
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "DPR")
			{
				txtRegistrationType.Text = "Duplicate Registration";
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "ECO")
			{
				txtRegistrationType.Text = "E-Correct";
			}
			else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "LPS")
			{
				txtRegistrationType.Text = "Lost Plate Stickers";
			}
			txtRegistrationType.Enabled = false;
			txtReg2ICM_Change();
			txtReg1ICM_Change();
			txtReg3ICM_Change();
			frmPlateInfo.InstancePtr.Unload();
			SetToolTips();
			if (MotorVehicle.Statics.InfoForCorrect)
			{
				Label36.ForeColor = Color.Red;
				Label35.ForeColor = Color.Red;
				Label37.ForeColor = Color.Red;
				Label7.ForeColor = Color.Red;
				Label46.ForeColor = Color.Red;
				lblFees.ForeColor = Color.Red;
				Label5.ForeColor = Color.Red;
				Label34.ForeColor = Color.Red;
				lblExciseTax.ForeColor = Color.Red;
				lblMVR3.ForeColor = Color.Red;
			}
			if (MotorVehicle.Statics.InfoForReReg || MotorVehicle.Statics.InfoForDuplicate)
			{
				Label7.ForeColor = Color.Red;
				Label5.ForeColor = Color.Red;
				Label34.ForeColor = Color.Red;
			}
			FillControlInformationClass();
			FillControlInformationClassFromGrid();

			// This will initialize the old data so we have somethign to compare the new data against when you save

			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}

			// ---------------------------------------------------------------------
        }

		private void imgReg1Search_Click(object sender, EventArgs e)
		{
            int lngID;
            lngID = frmCentralPartySearch.InstancePtr.Init();
            if (lngID > 0)
            {
                var showWarning = lngID != txtReg1PartyID.Text.ToIntegerValue();
                txtReg1PartyID.Text = FCConvert.ToString(lngID);

                GetPartyInfo(lngID, 1, CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ValidateReg1PartyID();
            }
            imgReg1Search.Focus();
		}

		private void imgReg2Search_Click(object sender, EventArgs e)
		{
            int lngID;
            lngID = frmCentralPartySearch.InstancePtr.Init();
            if (lngID > 0)
            {
                var showWarning = lngID != txtReg2PartyID.Text.ToIntegerValue();
                txtReg2PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 2, CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ValidateReg2PartyID();
            }
            imgReg2Search.Focus();
		}

		private void imgReg3Search_Click(object sender, EventArgs e)
		{
            int lngID;
            lngID = frmCentralPartySearch.InstancePtr.Init();
            if (lngID > 0)
            {
                var showWarning = lngID != txtReg3PartyID.Text.ToIntegerValue();
                txtReg3PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 3, CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ValidateReg3PartyID();
            }
            imgReg3Search.Focus();
		}

		private void imgFleetGroupSearch_Click(object sender, EventArgs e)
		{
            MotorVehicle.Statics.bolFromVehicleUpdate = true;
            frmFleetMaster.InstancePtr.Unload();
            frmFleetMaster.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            MotorVehicle.Statics.bolFromVehicleUpdate = false;
		}

		private void imgReg1Edit_Click(object sender, EventArgs e)
		{
            int lngID;
            int originalPartyId;

			if (fecherFoundation.Strings.Trim(txtReg1PartyID.Text) == "")
                return;
            lngID = FCConvert.ToInt32(txtReg1PartyID.Text);
            if (lngID < 1)
                return;
            originalPartyId = lngID;
			imgReg1Edit.Enabled = false;
            lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
            if (lngID > 0)
            {

                txtReg1PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 1, originalPartyId == lngID ? CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            imgReg1Edit.Enabled = true;
            imgReg1Edit.Focus();
		}

		private void imgReg2Edit_Click(object sender, EventArgs e)
		{
            int lngID;
            int originalPartyId;

			if (fecherFoundation.Strings.Trim(txtReg2PartyID.Text) == "")
                return;
            lngID = FCConvert.ToInt32(txtReg2PartyID.Text);
            if (lngID < 1)
                return;
            originalPartyId = lngID;
			imgReg2Edit.Enabled = false;
            lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
            if (lngID > 0)
            {
                txtReg2PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 2, originalPartyId == lngID ? CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            imgReg2Edit.Enabled = true;
            imgReg2Edit.Focus();
		}

		private void imgReg3Edit_Click(object sender, EventArgs e)
		{
            int lngID;
            int originalPartyId;

			if (fecherFoundation.Strings.Trim(txtReg3PartyID.Text) == "")
                return;
            lngID = FCConvert.ToInt32(txtReg3PartyID.Text);
            if (lngID < 1)
                return;
            originalPartyId = lngID;
			imgReg3Edit.Enabled = false;
            lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
            if (lngID > 0)
            {
                txtReg3PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 3, originalPartyId == lngID ? CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            imgReg3Edit.Enabled = true;
            imgReg3Edit.Focus();
		}

		private void imgCopyToMailingAddress_Click(object sender, EventArgs e)
		{
            if (txtReg1LR.Text == "R")
            {
                FillAddressFields(txtReg2PartyID.Text.ToIntegerValue());
            }
            else
            {
                FillAddressFields(txtReg1PartyID.Text.ToIntegerValue());
            }
		}

		private void imgCopyToLegalAddress_Click(object sender, EventArgs e)
		{
            txtLegalResStreet.Text = txtAddress1.Text;
            txtLegalres.Text = txtCity.Text;
            txtResState.Text = txtState.Text;
            txtResCountry.Text = txtCountry.Text;
		}

		private void imgEditRegistrant1_Click(object sender, EventArgs e)
		{
            UpdateRegistrantName(1);
		}

		private void imgEditRegistrant2_Click(object sender, EventArgs e)
		{
            UpdateRegistrantName(2);
		}

		private void imgEditRegistrant3_Click(object sender, EventArgs e)
		{
            UpdateRegistrantName(3);
		}

		private void UpdateRegistrantName(int registrant)
        {
            var regNameInfo = GetCurrentRegNameInfo(registrant);
            
			var result = StaticSettings.GlobalCommandDispatcher.Send(new EditRegistrantName
			{
				CurrentInfo = regNameInfo
			}).Result;

            UpdateRegistrantInfo(registrant, result);
        }

        private void UpdateRegistrantInfo(int registrant, RegistrantNameEditInfo updateInfo)
        {
			switch (registrant)
			{
				case 1:
					if (updateInfo.Type == RegistrantType.Individual)
					{
                        strReg1FirstName = updateInfo.FirstName;
                        strReg1LastName = updateInfo.LastName;
                        strReg1MI= updateInfo.MiddleInitial;
                        strReg1Designation = updateInfo.Designation;
                        lblRegistrant1.Text = FormatIndividualName(registrant);
					}
					else
					{
                        lblRegistrant1.Text = updateInfo.CompanyName;
					}
					break;
				case 2:
                    if (updateInfo.Type == RegistrantType.Individual)
                    {
                        strReg2FirstName = updateInfo.FirstName;
                        strReg2LastName = updateInfo.LastName;
                        strReg2MI = updateInfo.MiddleInitial;
                        strReg2Designation = updateInfo.Designation;
                        lblRegistrant2.Text = FormatIndividualName(registrant);
                    }
                    else
                    {
                        lblRegistrant2.Text = updateInfo.CompanyName;
                    }

					break;
				case 3:
                    if (updateInfo.Type == RegistrantType.Individual)
                    {
                        strReg3FirstName = updateInfo.FirstName;
                        strReg3LastName = updateInfo.LastName;
                        strReg3MI = updateInfo.MiddleInitial;
                        strReg3Designation = updateInfo.Designation;
                        lblRegistrant3.Text = FormatIndividualName(registrant);
                    }
                    else
                    {
                        lblRegistrant3.Text = updateInfo.CompanyName;
                    }
					break;
			}
		}

		private RegistrantNameEditInfo GetCurrentRegNameInfo(int registrant)
        {
            var regNameInfo = new RegistrantNameEditInfo();
			string registrantType;

			switch (registrant)
			{
				case 1:
					registrantType = txtReg1ICM.Text;
					regNameInfo.Type = registrantType == "I" ? RegistrantType.Individual : RegistrantType.Company;
					if (regNameInfo.Type == RegistrantType.Individual)
					{
						regNameInfo.FirstName = strReg1FirstName;
						regNameInfo.LastName = strReg1LastName;
						regNameInfo.MiddleInitial = strReg1MI;
						regNameInfo.Designation = strReg1Designation;
						regNameInfo.CompanyName = "";
					}
					else
					{
						regNameInfo.FirstName = "";
						regNameInfo.LastName = "";
						regNameInfo.MiddleInitial = "";
						regNameInfo.Designation = "";
						regNameInfo.CompanyName = lblRegistrant1.Text;
					}
					break;
				case 2:
					registrantType = txtReg2ICM.Text;
					regNameInfo.Type = registrantType == "I" ? RegistrantType.Individual : RegistrantType.Company;
					if (regNameInfo.Type == RegistrantType.Individual)
					{
						regNameInfo.FirstName = strReg2FirstName;
						regNameInfo.LastName = strReg2LastName;
						regNameInfo.MiddleInitial = strReg2MI;
						regNameInfo.Designation = strReg2Designation;
						regNameInfo.CompanyName = "";
					}
					else
					{
						regNameInfo.FirstName = "";
						regNameInfo.LastName = "";
						regNameInfo.MiddleInitial = "";
						regNameInfo.Designation = "";
						regNameInfo.CompanyName = lblRegistrant2.Text;
					}

					break;
				case 3:
					registrantType = txtReg3ICM.Text;
					regNameInfo.Type = registrantType == "I" ? RegistrantType.Individual : RegistrantType.Company;
					if (regNameInfo.Type == RegistrantType.Individual)
					{
						regNameInfo.FirstName = strReg3FirstName;
						regNameInfo.LastName = strReg3LastName;
						regNameInfo.MiddleInitial = strReg3MI;
						regNameInfo.Designation = strReg3Designation;
						regNameInfo.CompanyName = "";
					}
					else
					{
						regNameInfo.FirstName = "";
						regNameInfo.LastName = "";
						regNameInfo.MiddleInitial = "";
						regNameInfo.Designation = "";
						regNameInfo.CompanyName = lblRegistrant3.Text;
					}
					break;
			}

            return regNameInfo;
        }
		private string FormatIndividualName(int registrant)
		{
            string firstName = "";
            string middleInitial = "";
            string lastName = "";
            string designation = "";

			switch (registrant)
            {
				case 1:
                    firstName = strReg1FirstName;
                    middleInitial = strReg1MI;
                    lastName = strReg1LastName;
                    designation = strReg1Designation;
                    break;
				case 2:
                    firstName = strReg2FirstName;
                    middleInitial = strReg2MI;
                    lastName = strReg2LastName;
                    designation = strReg2Designation;
                    break;
				case 3:
                    firstName = strReg3FirstName;
                    middleInitial = strReg3MI;
                    lastName = strReg3LastName;
                    designation = strReg3Designation;
                    break;
			}

			return Strings.Trim(Strings.Trim(Strings.Trim(Strings.Trim(firstName) + " " + middleInitial) + " " + lastName) + " " + designation);
		}


		private int ReturnOldestPossibleVINYear(string strVin)

		{

			int intYear = 0;



			if (fecherFoundation.Strings.Trim(strVin) != "" || strVin.Length >= 17)

			{

				var yearIndicator = Strings.Mid(strVin, 10, 1);



				switch (yearIndicator)

				{
					case "A":
						intYear = 1980;     // 1980/2010
						break;
					case "B":
						intYear = 1981;
						break;
					case "C":
						intYear = 1982;
						break;
					case "D":
						intYear = 1983;
						break;
					case "E":
						intYear = 1984;
						break;

					case "F":
						intYear = 1985;
						break;

					case "G":
						intYear = 1986;
						break;

					case "H":
						intYear = 1987;
						break;

					case "J":
						intYear = 1988;
						break;

					case "K":
						intYear = 1989;
						break;

					case "L":
						intYear = 1990;     // 1990/2020
						break;

					case "M":
						intYear = 1991;
						break;

					case "N":
						intYear = 1992;
						break;

					case "P":
						intYear = 1993;
						break;

					case "R":
						intYear = 1994;
						break;

					case "S":
						intYear = 1995;
						break;

					case "T":
						intYear = 1996;
						break;

					case "V":
						intYear = 1997;
						break;

					case "W":
						intYear = 1998;
						break;

					case "X":

						intYear = 1999;

						break;

					case "Y":
						intYear = 2000;     // 2000/2030

						break;

					case "1":

						intYear = 2001;

						break;

					case "2":

						intYear = 2002;

						break;

					case "3":

						intYear = 2003;

						break;

					case "4":

						intYear = 2004;

						break;

					case "5":

						intYear = 2005;

						break;

					case "6":

						intYear = 2006;

						break;

					case "7":

						intYear = 2007;

						break;

					case "8":

						intYear = 2008;

						break;

					case "9":

						intYear = 2009;     // 2009/2039

						break;

				}

			}



			return intYear;

		}

		private string GetYearFromVin(string strVin)

		{

			string GetYearFromVin = "";

			// Choose correct Model Year based on the 7th and 10th char of the VIN

			// If char 7 is a number, char 10 is 1980-2009

			// If it is a letter, char to is 2010-2039

			int intYear;

			intYear = ReturnOldestPossibleVINYear(strVin);



			if (intYear > 0)

			{

				// kk05122016  Check for year way in the future

				if (intYear + 30 <= DateTime.Now.Year + 2)

				{

					intYear += 30;

				}

			}



			if (intYear > 0)

			{

				GetYearFromVin = Strings.Format(intYear, "0000");

			}

			else

			{

				GetYearFromVin = "";

			}



			return GetYearFromVin;

		}



		private string GetVehicleTypeCode(int classificationId, int categoryId)
        {
	        switch (classificationId)
	        {
		        case (int)VehicleClassificationNumber.CommercialTrucks:
			        return "H";
		        case (int)VehicleClassificationNumber.PassengerVehicles:
			        return "C";
		        case (int)VehicleClassificationNumber.Powersport:
			        return "M";
		        case (int)VehicleClassificationNumber.CommercialTrailers:
			        return "X";
		        case (int)VehicleClassificationNumber.RecreationalVehicles:
			        return "R";
		        case (int)VehicleClassificationNumber.TruckBodies:
			        return "B";
	        }

	        if (categoryId == (int)VehicleCategoryNumber.MotorCycles)
	        {
		        return "M";
	        }
	        return "";
        }
	}
}
