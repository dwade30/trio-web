//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReconDouble.
	/// </summary>
	public partial class rptReconDouble : BaseSectionReport
	{
		public rptReconDouble()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Reconcilliation Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptReconDouble_ReportEnd;
		}

        private void RptReconDouble_ReportEnd(object sender, EventArgs e)
        {
			rs.DisposeOf();
            rs1.DisposeOf();
            rsInvAdjust.DisposeOf();
            rsNotFound.DisposeOf();

		}

        public static rptReconDouble InstancePtr
		{
			get
			{
				return (rptReconDouble)Sys.GetInstance(typeof(rptReconDouble));
			}
		}

		protected rptReconDouble _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReconDouble	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lng1;
		int lng2;
		int lngPK;
		int lngEndPK;
		//clsDRWrapper rsA = new clsDRWrapper();
		int temp;
		int lngSOP;
		int lngAR;
		int lngVI;
		int lngAA;
		int lngEOP;
		int lngPK1;
		int lngPK2;
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		clsDRWrapper rs1 = new clsDRWrapper();
		clsDRWrapper rsInvAdjust = new clsDRWrapper();
		string strLine = "";
		bool boolFirstTime;
		bool blnFound;
		// vbPorter upgrade warning: intY1 As int	OnWriteFCConvert.ToInt32(
		int intY1;
		clsDRWrapper rsNotFound = new clsDRWrapper();
		int intDone;
		int intYY;
		string strTC = "";
		// Inventory Type Check
		string strMM = "";
		int intPageNumber;

		private string strMMYY1(ref string str1)
		{
			string strMMYY1 = "";
			strMMYY1 = "";
			if (Strings.Mid(str1, 1, 1) == "M")
			{
				str1 = Strings.Mid(str1, 2, 2);
				if (str1 == "01")
					strMMYY1 = "JAN";
				if (str1 == "02")
					strMMYY1 = "FEB";
				if (str1 == "03")
					strMMYY1 = "MAR";
				if (str1 == "04")
					strMMYY1 = "APR";
				if (str1 == "05")
					strMMYY1 = "MAY";
				if (str1 == "06")
					strMMYY1 = "JUN";
				if (str1 == "07")
					strMMYY1 = "JUL";
				if (str1 == "08")
					strMMYY1 = "AUG";
				if (str1 == "09")
					strMMYY1 = "SEP";
				if (str1 == "10")
					strMMYY1 = "OCT";
				if (str1 == "11")
					strMMYY1 = "NOV";
				if (str1 == "12")
					strMMYY1 = "DEC";
			}
			else
			{
				strMMYY1 = Strings.Mid(str1, 2, 2);
			}
			return strMMYY1;
		}

		private void strStickers(string strMonth)
		{
			clsDRWrapper rsGG = new clsDRWrapper();
			clsDRWrapper rsHH = new clsDRWrapper();
			clsDRWrapper rsII = new clsDRWrapper();
			clsDRWrapper rsJJ = new clsDRWrapper();
			clsDRWrapper rsKK = new clsDRWrapper();
			int lngGG;
			int lngHH;
			int lngII;
			int lngJJ;
			int lngKK;
			string strMMYY = "";
			lngGG = 0;
			lngHH = 0;
			lngII = 0;
			lngJJ = 0;
			lngKK = 0;
			// 
			rsGG.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE Type = '" + strMonth + "' And PeriodCloseoutID = " + FCConvert.ToString(lngEndPK));
			rsHH.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strMonth + "' And AdjustmentCode = 'R' And PeriodCloseoutID >" + FCConvert.ToString(lngPK) + "AND PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsHH.EndOfFile() != true && rsHH.BeginningOfFile() != true)
			{
				rsHH.MoveLast();
				rsHH.MoveFirst();
				while (!rsHH.EndOfFile())
				{
					lngHH += rsHH.Get_Fields_Int32("QuantityAdjusted");
					rsHH.MoveNext();
				}
			}
			rsII.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strMonth + "' And AdjustmentCode = 'I' And PeriodCloseoutID >" + FCConvert.ToString(lngPK) + "AND PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsII.EndOfFile() != true && rsII.BeginningOfFile() != true)
			{
				rsII.MoveLast();
				rsII.MoveFirst();
				while (!rsII.EndOfFile())
				{
					lngII += rsII.Get_Fields_Int32("QuantityAdjusted");
					rsII.MoveNext();
				}
			}
			rsJJ.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strMonth + "' And AdjustmentCode = 'A' And PeriodCloseoutID >" + FCConvert.ToString(lngPK) + "AND PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsJJ.EndOfFile() != true && rsJJ.BeginningOfFile() != true)
			{
				rsJJ.MoveLast();
				rsJJ.MoveFirst();
				while (!rsJJ.EndOfFile())
				{
					lngJJ += rsJJ.Get_Fields_Int32("QuantityAdjusted");
					rsJJ.MoveNext();
				}
			}
			rsKK.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE Type = '" + strMonth + "' And PeriodCloseoutID = " + FCConvert.ToString(lngPK));
			if (rsKK.EndOfFile() != true && rsKK.BeginningOfFile() != true)
			{
				rsKK.MoveLast();
				rsKK.MoveFirst();
				lngKK = 0;
				while (!rsKK.EndOfFile())
				{
					lngKK += rsKK.Get_Fields_Int32("Number");
					rsKK.MoveNext();
				}
			}
			if (rsGG.EndOfFile() != true && rsGG.BeginningOfFile() != true)
			{
				rsGG.MoveLast();
				rsGG.MoveFirst();
				lngGG = 0;
				while (!rsGG.EndOfFile())
				{
					lngGG += rsGG.Get_Fields_Int32("Number");
					rsGG.MoveNext();
				}
			}
			string str1 = Strings.Mid(strMonth, 2, 1) + Strings.Mid(strMonth, 4, 2);
			txtMonth.Text = strMMYY1(ref str1);
			txtOnHand.Text = Strings.Format(lngKK, "@@@@@@@@");
			txtRecorded.Text = Strings.Format(lngHH, "@@@@@@@@@@");
			txtIssues.Text = Strings.Format(lngII, "@@@@@@@@@@");
			txtAdjusted.Text = Strings.Format(lngJJ, "@@@@@@@@@@");
			txtEnd.Text = Strings.Format(lngGG, "@@@@@@@@@@");
			if (lngKK + lngHH - lngII - lngJJ != lngGG)
			{
				txtEnd.Text = txtEnd.Text + "  **";
			}
			lngSOP += lngKK;
			lngAR += lngHH;
			lngVI += lngII;
			lngAA += lngJJ;
			lngEOP += lngGG;
            rsGG.DisposeOf();
            rsHH.DisposeOf();
            rsII.DisposeOf();
            rsJJ.DisposeOf();
            rsKK.DisposeOf();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!rs.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
			txtMonth.Text = "";
			txtOnHand.Text = "";
			txtRecorded.Text = "";
			txtIssues.Text = "";
			txtAdjusted.Text = "";
			txtEnd.Text = "";
			txtItem.Text = "";
			txtItem1.Text = "";
			txtItem2.Text = "";
			txtItem3.Text = "";
			switch (intDone)
			{
				case 0:
					{
						strStickers("SMD01");
						intDone += 1;
						break;
					}
				case 1:
					{
						strStickers("SMD02");
						intDone += 1;
						break;
					}
				case 2:
					{
						strStickers("SMD03");
						intDone += 1;
						break;
					}
				case 3:
					{
						strStickers("SMD04");
						intDone += 1;
						break;
					}
				case 4:
					{
						strStickers("SMD05");
						intDone += 1;
						break;
					}
				case 5:
					{
						strStickers("SMD06");
						intDone += 1;
						break;
					}
				case 6:
					{
						strStickers("SMD07");
						intDone += 1;
						break;
					}
				case 7:
					{
						strStickers("SMD08");
						intDone += 1;
						break;
					}
				case 8:
					{
						strStickers("SMD09");
						intDone += 1;
						break;
					}
				case 9:
					{
						strStickers("SMD10");
						intDone += 1;
						break;
					}
				case 10:
					{
						strStickers("SMD11");
						intDone += 1;
						break;
					}
				case 11:
					{
						strStickers("SMD12");
						intDone += 1;
						break;
					}
				case 12:
					{
						if (intYY <= intY1 + 4)
						{
							// For intYY = intY1 - 4 To intY1 + 4
							if (rs.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2)))
							{
								blnFound = true;
								strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
								// LineCount = LineCount + 1
							}
							else
							{
								rsInvAdjust.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2));
								if (rsInvAdjust.NoMatch == false)
								{
									blnFound = true;
									strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
								}
							}
							intYY += 1;
							rs.MoveFirst();
						}
						else
						{
							intDone += 1;
							intY1 = DateTime.Today.Year;
							intYY = intY1 - 4;
						}
						break;
					}
				case 13:
					{
						if (intYY <= intY1 + 4)
						{
							if (blnFound == false)
							{
								rsNotFound.OpenRecordset("SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK) + " ORDER BY InventoryType, Low");
								if (rsNotFound.EndOfFile() != true && rsNotFound.BeginningOfFile() != true)
								{
									// For intYY = intY1 - 4 To intY1 + 4
									if (rsNotFound.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2)))
									{
										strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
									}
									else
									{
										rsInvAdjust.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2));
										if (rsInvAdjust.NoMatch == false)
										{
											strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
										}
									}
								}
							}
							intYY += 1;
							rs.MoveFirst();
						}
						else
						{
							intDone += 1;
						}
						break;
					}
				case 14:
					{
						// TOTOL LINE
						txtMonth.Text = "TOTAL--  ";
						txtOnHand.Text = Strings.Format(lngSOP, "@@@@@@@@");
						txtRecorded.Text = Strings.Format(lngAR, "@@@@@@@@@@");
						txtIssues.Text = Strings.Format(lngVI, "@@@@@@@@@@");
						txtAdjusted.Text = Strings.Format(lngAA, "@@@@@@@@@@");
						txtEnd.Text = Strings.Format(lngEOP, "@@@@@@@@@@");
						intDone += 1;
						rs.MoveFirst();
						txtItem.Text = "ITEMIZED LISTING----------------";
						break;
					}
				case 15:
					{
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						if (strTC != "SD")
							goto NextTag2;
						if (rs.EndOfFile() != true)
						{
							txtItem1.Text = Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@");
							txtItem1.Text = txtItem1.Text + "  -  ";
							txtItem1.Text = txtItem1.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");
							rs.MoveNext();
						}
						// 
						if (rs.EndOfFile() != true)
						{
							strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
							strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
							if (strTC != "SD")
								goto GotOne3;
							txtItem2.Text = Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@");
							txtItem2.Text = txtItem2.Text + "  -  ";
							txtItem2.Text = txtItem2.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");
							rs.MoveNext();
						}
						// 
						if (rs.EndOfFile() != true)
						{
							strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
							strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
							if (strTC != "SD")
								goto GotOne3;
							txtItem3.Text = Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@");
							txtItem3.Text = txtItem3.Text + "  -  ";
							txtItem3.Text = txtItem3.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");
						}
					GotOne3:
						;
					NextTag2:
						;
						if (rs.EndOfFile() != true)
							rs.MoveNext();
						break;
					}
			}
			//end switch
			
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				txtDescTitle.Text = "Double Stickers---------------------";
			}
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngPK = lngPK1;
				lng1 = lngPK1;
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngEndPK = lngPK2;
				lng2 = lngPK2;
			}
			// 
			strSQL = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " ORDER BY InventoryType, Low";
			rs.OpenRecordset(strSQL);
			rsInvAdjust.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " ORDER BY InventoryType, Low");
			rs.MoveFirst();
			lngSOP = 0;
			lngAR = 0;
			lngVI = 0;
			lngAA = 0;
			lngEOP = 0;
			intDone = 0;
			intY1 = 0;
			intY1 = DateTime.Today.Year;
			blnFound = false;
			intYY = intY1 - 4;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (txtItem.Text != "")
			{
				txtItem.Top += 180 / 1440F;
				txtItem1.Top += 180 / 1440F;
				txtItem2.Top += 180 / 1440F;
				txtItem3.Top += 180 / 1440F;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			frmReport.InstancePtr.lngInventoryCounter += 1;
			lblPage.Text = "Page " + FCConvert.ToString(frmReport.InstancePtr.lngInventoryCounter);
		}
	}
}
