//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptInventoryStatus.
	/// </summary>
	public partial class rptInventoryStatus : BaseSectionReport
	{
		public rptInventoryStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptInventoryStatus_ReportEnd;
		}

        private void RptInventoryStatus_ReportEnd(object sender, EventArgs e)
        {
            rsPrint.DisposeOf();
        }

        public static rptInventoryStatus InstancePtr
		{
			get
			{
				return (rptInventoryStatus)Sys.GetInstance(typeof(rptInventoryStatus));
			}
		}

		protected rptInventoryStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptInventoryStatus	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strSQL = "";
		string strSQLItems = "";
		//clsDRWrapper rsPlates = new clsDRWrapper();
		//clsDRWrapper rsItems = new clsDRWrapper();
		clsDRWrapper rsPrint = new clsDRWrapper();
        FCFileSystem ff = new FCFileSystem();
		string InventoryPrinter = "";
		string TitleType = "";
		int NumberOfRecords;
		int[] Stickers = new int[6 + 1];
		int MaxElements;
		int fnx;
		int lngStart;
		int lngStop;
		int lngRow;
		int lngFirst;
		int lngLast;
		string strPrefix = "";
		string strSuffix = "";
		string strStatus = "";
		string strInventoryOpID = "";
		int Difference;
		int TB;
		int intGroup;
		bool boolDidSection;
		int PageCounter;
		string strOriginalPrefix = "";
		string strStartPlate = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDidSection == true)
			{
				eArgs.EOF = true;
				return;
			}
			if (!rsPrint.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				rsPrint.MovePrevious();
				Difference = 0;
				if (FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number")) != lngFirst || rsPrint.RecordCount() == 1)
				{
					boolDidSection = true;
					Difference = (rsPrint.Get_Fields_Int32("Number") - lngFirst) + 1;
					txtType.Text = Strings.Mid(FCConvert.ToString(rsPrint.Get_Fields("Code")), 4, 2);
					txtDate.Text = Strings.Format(rsPrint.Get_Fields_DateTime("InventoryDate"), "MM/dd/yyyy");
					txtOpID.Text = FCConvert.ToString(rsPrint.Get_Fields_String("OpID"));
					txtLow.Text = strStartPlate;
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					txtHigh.Text = FCConvert.ToString(rsPrint.Get_Fields_String("FormattedInventory"));
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					txtStatus.Text = FCConvert.ToString(rsPrint.Get_Fields_String("Status"));
					if (FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) == 0)
					{
						txtGroup.Text = "All";
					}
					else
					{
						txtGroup.Text = FCConvert.ToString(rsPrint.Get_Fields_Int16("Group"));
					}
					txtCount.Text = FCConvert.ToString(Difference);
					eArgs.EOF = false;
					return;
				}
				else
				{
					Difference = (rsPrint.Get_Fields_Int32("Number") - lngFirst) + 1;
					txtType.Text = Strings.Mid(FCConvert.ToString(rsPrint.Get_Fields("Code")), 4, 2);
					txtDate.Text = Strings.Format(rsPrint.Get_Fields_DateTime("InventoryDate"), "MM/dd/yyyy");
					txtOpID.Text = FCConvert.ToString(rsPrint.Get_Fields_String("OpID"));
					txtLow.Text = strStartPlate;
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					txtHigh.Text = FCConvert.ToString(rsPrint.Get_Fields_String("FormattedInventory"));
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					txtStatus.Text = FCConvert.ToString(rsPrint.Get_Fields_String("Status"));
					if (FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) == 0)
					{
						txtGroup.Text = "All";
					}
					else
					{
						txtGroup.Text = FCConvert.ToString(rsPrint.Get_Fields_Int16("Group"));
					}
					txtCount.Text = Difference.ToString();
					boolDidSection = true;
					eArgs.EOF = false;
					return;
				}
			}
			txtType.Text = "";
			txtDate.Text = "";
			txtOpID.Text = "";
			txtLow.Text = "";
			txtHigh.Text = "";
			txtStatus.Text = "";
			txtGroup.Text = "";
			txtCount.Text = "";
			while (!rsPrint.EndOfFile())
			{
				if (strPrefix.Length > FCConvert.ToString(rsPrint.Get_Fields_String("prefix")).Length && Strings.Right(strPrefix, 1) == "0")
				{
					strPrefix = Strings.Left(strPrefix, (strPrefix.Length - (strPrefix.Length - FCConvert.ToString(rsPrint.Get_Fields_String("prefix")).Length)));
				}
				if (FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number")) == lngLast && (strPrefix == FCConvert.ToString(rsPrint.Get_Fields_String("prefix")) && strSuffix == FCConvert.ToString(rsPrint.Get_Fields_String("Suffix"))) && intGroup == FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) && strStatus == FCConvert.ToString(rsPrint.Get_Fields_String("Status")) && strInventoryOpID == FCConvert.ToString(rsPrint.Get_Fields_String("OpID")))
				{
					lngLast += 1;
				}
				else if (FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number")) == lngLast && fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("prefix")) == true && fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("Suffix")) == true && intGroup == FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) && strStatus == FCConvert.ToString(rsPrint.Get_Fields_String("Status")) && strInventoryOpID == FCConvert.ToString(rsPrint.Get_Fields_String("OpID")))
				{
					lngLast += 1;
				}
				else
				{
					rsPrint.MovePrevious();
					Difference = (rsPrint.Get_Fields_Int32("Number") - lngFirst) + 1;
					txtType.Text = Strings.Mid(FCConvert.ToString(rsPrint.Get_Fields("Code")), 4, 2);
					txtDate.Text = Strings.Format(rsPrint.Get_Fields_DateTime("InventoryDate"), "MM/dd/yyyy");
					txtOpID.Text = FCConvert.ToString(rsPrint.Get_Fields_String("OpID"));
					txtLow.Text = strStartPlate;
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					txtHigh.Text = FCConvert.ToString(rsPrint.Get_Fields_String("FormattedInventory"));
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					txtStatus.Text = FCConvert.ToString(rsPrint.Get_Fields_String("Status"));
					if (FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) == 0)
					{
						txtGroup.Text = "All";
					}
					else
					{
						txtGroup.Text = FCConvert.ToString(rsPrint.Get_Fields_Int16("Group"));
					}
					txtCount.Text = FCConvert.ToString(Difference);
					rsPrint.MoveNext();
					lngFirst = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
					lngLast = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
					intGroup = FCConvert.ToInt16(rsPrint.Get_Fields_Int16("Group"));
					strStatus = FCConvert.ToString(rsPrint.Get_Fields_String("Status"));
					strInventoryOpID = FCConvert.ToString(rsPrint.Get_Fields_String("OpID"));
					if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("prefix")) == true)
						strPrefix = "";
					else
						strPrefix = FCConvert.ToString(rsPrint.Get_Fields_String("prefix"));
					if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("Suffix")) == true)
						strSuffix = "";
					else
						strSuffix = FCConvert.ToString(rsPrint.Get_Fields_String("Suffix"));
					strOriginalPrefix = strPrefix;
					strStartPlate = FCConvert.ToString(rsPrint.Get_Fields_String("FormattedInventory"));
					// .MovePrevious
					eArgs.EOF = false;
					return;
				}
				rsPrint.MoveNext();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int Index;
			// vbPorter upgrade warning: intAnswer As int	OnWrite(DialogResult)
			DialogResult intAnswer = 0;
			// vbPorter upgrade warning: strMonth As string	OnWrite(int, string)
			string strMonth = "";
			boolDidSection = false;
			this.Document.Printer.PrinterName = frmInventoryStatus.InstancePtr.InventoryPrinter;
			Index = frmInventoryStatus.InstancePtr.intIndex;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label11.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label8.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			string Month = "";
			switch (Index)
			{
				case 0:
					{
						// TitleType = "MVR3 Forms"
						if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Number";
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no MVR3 forms in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 1:
					{
						if (frmInventoryStatus.InstancePtr.lstPlates.SelectedIndex != -1 && !frmInventoryStatus.InstancePtr.blnAllInventory)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected plates?", "Print Selected Plates?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = Trim(Mid(lstPlates.List(lstPlates.ListIndex), 3, Len(lstPlates.List(lstPlates.ListIndex)) - 2)) & " Plates"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
									strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(frmInventoryStatus.InstancePtr.lstPlates.Items[frmInventoryStatus.InstancePtr.lstPlates.SelectedIndex].Text, 1, 2) + "' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
								else
								{
									//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
									strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(frmInventoryStatus.InstancePtr.lstPlates.Items[frmInventoryStatus.InstancePtr.lstPlates.SelectedIndex].Text, 1, 2) + "' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
							}
							else
							{
								// TitleType = "Plates"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
							}
						}
						else
						{
							// TitleType = "Plates"
							if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Plates in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 2:
					{
						if (frmInventoryStatus.InstancePtr.lstItems.SelectedIndex != -1 && !frmInventoryStatus.InstancePtr.blnAllInventory)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Double Year Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYD" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYD" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Double Year Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Double Year Stickers"
							if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Double Year Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 3:
					{
						if (frmInventoryStatus.InstancePtr.lstItems.SelectedIndex != -1 && !frmInventoryStatus.InstancePtr.blnAllInventory)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								strMonth = FCConvert.ToString(frmInventoryStatus.InstancePtr.lstItems.SelectedIndex + 1);
								if (Conversion.Val(strMonth) < 10)
								{
									strMonth = "0" + strMonth;
								}
								// TitleType = lstItems.List(lstItems.ListIndex) & " Double Month Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMD" + strMonth + "'" + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMD" + strMonth + "'" + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Double Month Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Double Month Stickers"
							if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Double Month Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 4:
					{
						if (frmInventoryStatus.InstancePtr.lstItems.SelectedIndex != -1 && !frmInventoryStatus.InstancePtr.blnAllInventory)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Single Year Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYS" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYS" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Single Year Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Single Year Stickers"
							if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Single Year Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 5:
					{
						if (frmInventoryStatus.InstancePtr.lstItems.SelectedIndex != -1 && !frmInventoryStatus.InstancePtr.blnAllInventory)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								strMonth = FCConvert.ToString(frmInventoryStatus.InstancePtr.lstItems.SelectedIndex + 1);
								if (Conversion.Val(strMonth) < 10)
								{
									strMonth = "0" + strMonth;
								}
								// TitleType = lstItems.List(lstItems.ListIndex) & " Single Month Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMS" + strMonth + "'" + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMS" + strMonth + "'" + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Single Month Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Single Month Stickers"
							if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Single Month Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 6:
					{
						if (frmInventoryStatus.InstancePtr.lstItems.SelectedIndex != -1 && !frmInventoryStatus.InstancePtr.blnAllInventory)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Decals"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'DYS" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'DYS" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Decals"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Decals"
							if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Decals in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 7:
					{
						// TitleType = "Boosters"
						if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'BX' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'BX' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Number";
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Booster Plates in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 8:
					{
						// TitleType = "Boosters"
						if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Number";
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no MVR10 forms in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 9:
					{
						if (frmInventoryStatus.InstancePtr.lstItems.SelectedIndex != -1 && !frmInventoryStatus.InstancePtr.blnAllInventory)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Double Year Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYC" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYC" + Strings.Mid(frmInventoryStatus.InstancePtr.lstItems.Items[frmInventoryStatus.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'" + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Double Year Stickers"
								if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Double Year Stickers"
							if (frmInventoryStatus.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' " + frmInventoryStatus.InstancePtr.StrSearch + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' " + frmInventoryStatus.InstancePtr.StrSearch + " AND [Group] = " + FCConvert.ToString(frmInventoryStatus.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventoryStatus.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Combination Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
			}
			//end switch
		}

		private void PrintRoutine()
		{
			if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
			{
				rsPrint.MoveLast();
				rsPrint.MoveFirst();
				lngFirst = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
				lngLast = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
				intGroup = FCConvert.ToInt16(rsPrint.Get_Fields_Int16("Group"));
				strStatus = FCConvert.ToString(rsPrint.Get_Fields_String("Status"));
				strInventoryOpID = FCConvert.ToString(rsPrint.Get_Fields_String("OpID"));
				if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("prefix")) == true)
					strPrefix = "";
				else
					strPrefix = FCConvert.ToString(rsPrint.Get_Fields_String("prefix"));
				if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("Suffix")) == true)
					strSuffix = "";
				else
					strSuffix = FCConvert.ToString(rsPrint.Get_Fields_String("Suffix"));
				strOriginalPrefix = strPrefix;
				strStartPlate = FCConvert.ToString(rsPrint.Get_Fields_String("FormattedInventory"));
				TB = 3;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblHeading.Text = frmInventoryStatus.InstancePtr.strHeading;
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptInventoryStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptInventoryStatus properties;
			//rptInventoryStatus.Caption	= "Inventory Listing";
			//rptInventoryStatus.Icon	= "rptInventoryStatus.dsx":0000";
			//rptInventoryStatus.Left	= 0;
			//rptInventoryStatus.Top	= 0;
			//rptInventoryStatus.Width	= 11880;
			//rptInventoryStatus.Height	= 8595;
			//rptInventoryStatus.StartUpPosition	= 3;
			//rptInventoryStatus.SectionData	= "rptInventoryStatus.dsx":058A;
			//End Unmaped Properties
		}
	}
}
