//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmTransitPlate : BaseForm
	{
		public frmTransitPlate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTransitPlate InstancePtr
		{
			get
			{
				return (frmTransitPlate)Sys.GetInstance(typeof(frmTransitPlate));
			}
		}

		protected frmTransitPlate _InstancePtr = null;
		//=========================================================
		bool BadVIN;

		private void cboCodesList_Leave(object sender, System.EventArgs e)
		{
			if (this.ActiveControl.GetName() != "cmdCancel" && this.ActiveControl.GetName() != "cmdOK" && this.ActiveControl.GetName() != "txtStyle")
			{
				if (fraCodes.Visible == true)
				{
					cboCodesList.Focus();
				}
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraCodes.Visible = false;
			txtMake.Focus();
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			fraCodes.Visible = false;
			txtMake.Text = Strings.Left(cboCodesList.Text, 4);
			txtMake.Focus();
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private bool VerifyResCode()
		{
			bool VerifyResCode = false;
			VerifyResCode = true;
			string rescode = "";
			clsDRWrapper rsResCode = new clsDRWrapper();
			// 
			if (Conversion.Val(txtResidenceCode.Text) != 0 && Conversion.Val(txtResidenceCode.Text) != 99999)
			{
				rescode = fecherFoundation.Strings.Trim(txtResidenceCode.Text);
				MotorVehicle.Statics.strSql = "SELECT * FROM Residence";
				rsResCode.OpenRecordset(MotorVehicle.Statics.strSql);
				if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
				{
					rsResCode.MoveLast();
					rsResCode.MoveFirst();
					if (rsResCode.FindFirstRecord("Code", rescode))
					{
						// do nothing
					}
					else
					{
						MessageBox.Show(" The residence code is not in the BMV table." + "\r\n" + "Please input a valid Residence Code.", "Residence Code Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtResidenceCode.Text = "00000";
						VerifyResCode = false;
						txtResidenceCode.TabStop = true;
					}
				}
				else
				{
					MessageBox.Show("There are no entries in the BMV residence code table.", "Empty Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					VerifyResCode = false;
				}
			}
			else
			{
				MessageBox.Show("Residence code is required", "Residence Code Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				VerifyResCode = false;
			}
			return VerifyResCode;
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string strApplicant;
			if (Conversion.Val(fecherFoundation.Strings.Trim(txtCustomerNumber.Text)) == 0)
			{
				MessageBox.Show("You must fill in the applicant before you can save.", "Invalid Applicant", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtCustomerNumber.Focus();
				return;
			}
			if (txtYear.Text == "")
			{
				MessageBox.Show("You must fill in the year of the vehicle before you can save.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtYear.Focus();
				return;
			}
			if (txtMake.Text == "")
			{
				MessageBox.Show("You must fill in the make of the vehicle before you can save.", "Invalid Make", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtMake.Focus();
				return;
			}
			if (txtModel.Text == "")
			{
				MessageBox.Show("You must fill in the model of the vehicle before you can save.", "Invalid Model", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtModel.Focus();
				return;
			}
			if (txtVIN.Text == "")
			{
				MessageBox.Show("You must fill in the VIN # of the vehicle before you can save.", "Invalid VIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVIN.Focus();
				return;
			}
			if (txtPlate.Text == "")
			{
				MessageBox.Show("You must fill in the plate number before you can save.", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPlate.Focus();
				return;
			}
			if (txtOrigin.Text == "")
			{
				MessageBox.Show("You must fill in the point of origin before you can save.", "Invalid Point of Origin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtOrigin.Focus();
				return;
			}
			if (txtPlate.Text == "")
			{
				MessageBox.Show("You must fill in the destination before you can save.", "Invalid Destination", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPlate.Focus();
				return;
			}
			if (!VerifyResCode())
			{
				return;
			}
			strApplicant = MotorVehicle.GetPartyNameMiddleInitial(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtCustomerNumber.Text)))));
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				MotorVehicle.Write_PDS_Work_Record_Stuff();
			}
			rs.OpenRecordset("SELECT * FROM TransitPlates");
			rs.AddNew();
			rs.Set_Fields("plate", txtPlate.Text);
			rs.Set_Fields("EffectiveDate", txtEffectiveDate.Text);
			rs.Set_Fields("ExpireDate", txtExpireDate.Text);
			rs.Set_Fields("PartyID", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtCustomerNumber.Text))));
			if (fecherFoundation.Strings.Trim(txtDOB.Text) != "" && Information.IsDate(txtDOB.Text))
			{
				rs.Set_Fields("DOB", txtDOB.Text);
			}
			else
			{
				rs.Set_Fields("DOB", null);
			}
			rs.Set_Fields("RoundTrip", chkRoundTrip.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("MunicipalVehicle", chkMunicipalVehicle.CheckState == Wisej.Web.CheckState.Checked);
			rs.Set_Fields("ResidenceCode", txtResidenceCode.Text);
			rs.Set_Fields("make", txtMake.Text);
			rs.Set_Fields("Year", txtYear.Text);
			rs.Set_Fields("model", txtModel.Text);
			rs.Set_Fields("Vin", txtVIN.Text);
			if (BadVIN)
			{
				rs.Set_Fields("InvalidVIN", true);
			}
			else
			{
				rs.Set_Fields("InvalidVIN", false);
			}
			rs.Set_Fields("Origin", txtOrigin.Text);
			rs.Set_Fields("Destination", txtDestination.Text);
			rs.Set_Fields("DateUpdated", DateTime.Now);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (chkMunicipalVehicle.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("LocalPaid", 0);
				rs.Set_Fields("StatePaid", 0);
			}
			else
			{
				rs.Set_Fields("LocalPaid", rsDefaultInfo.Get_Fields_Decimal("AgentFeeTransit"));
				if (chkRoundTrip.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					rs.Set_Fields("StatePaid", rsDefaultInfo.Get_Fields_Decimal("TransitPlateFee"));
				}
				else
				{
					if (Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("TransitPlateRoundTrip")) != 0)
					{
						rs.Set_Fields("StatePaid", FCConvert.ToString(Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("TransitPlateRoundTrip"))));
					}
					else
					{
						rs.Set_Fields("StatePaid", 25);
					}
				}
			}
			rs.Update();
			rs.OpenRecordset("SELECT * FROM ExceptionReport");
			rs.AddNew();
			rs.Set_Fields("Type", "6PM");
			rs.Set_Fields("ExceptionReportDate", DateTime.Today);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("PermitNumber", fecherFoundation.Strings.Trim(txtPlate.Text));
			if (chkRoundTrip.CheckState == Wisej.Web.CheckState.Checked)
			{
				rs.Set_Fields("PermitType", "TRANSIT RT");
				if (chkMunicipalVehicle.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("Fee", 0);
				}
				else
				{
					if (Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("TransitPlateRoundTrip")) != 0)
					{
						rs.Set_Fields("Fee", FCConvert.ToString(Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("TransitPlateRoundTrip"))));
					}
					else
					{
						rs.Set_Fields("Fee", 25);
					}
				}
			}
			else
			{
				rs.Set_Fields("PermitType", "TRANSIT OW");
				if (chkMunicipalVehicle.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs.Set_Fields("Fee", 0);
				}
				else
				{
					rs.Set_Fields("Fee", rsDefaultInfo.Get_Fields_Decimal("TransitPlateFee"));
				}
			}
			rs.Set_Fields("ApplicantName", fecherFoundation.Strings.Trim(strApplicant));
			rs.Set_Fields("MVR3Printed", 0);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			rs.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'PXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPlate.Text)));
			rs.Edit();
			rs.Set_Fields("Status", "I");
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("MVR3", 0);
			rs.Update();
			rs.OpenRecordset("SELECT * FROM InventoryAdjustments");
			rs.AddNew();
			rs.Set_Fields("AdjustmentCode", "I");
			rs.Set_Fields("DateOfAdjustment", DateTime.Today);
			rs.Set_Fields("QuantityAdjusted", 1);
			rs.Set_Fields("InventoryType", "PXSXX");
			rs.Set_Fields("Low", txtPlate.Text);
			rs.Set_Fields("High", txtPlate.Text);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("reason", "Issued - " + strApplicant);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			MessageBox.Show("Be sure to complete the MV-159 Form.", "Fill out MV-159 Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				//MDIParent.InstancePtr.GetMainMenu();
				MDIParent.InstancePtr.Menu18();
			}
			else
			{
				Close();
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdProcessSave, new System.EventArgs());
		}

		private void frmTransitPlate_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtExpireDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", 9, DateTime.Now), "MM/dd/yyyy");
			// txtExpireDate.Enabled = False
			// txtEffectiveDate.Enabled = False
			txtPlate.Focus();
			this.Refresh();
		}

		private void frmTransitPlate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmTransitPlate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransitPlate properties;
			//frmTransitPlate.FillStyle	= 0;
			//frmTransitPlate.ScaleWidth	= 9045;
			//frmTransitPlate.ScaleHeight	= 7410;
			//frmTransitPlate.LinkTopic	= "Form2";
			//frmTransitPlate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsDefaults = new clsDRWrapper();
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			if (!modRegionalTown.IsRegionalTown())
			{
				rsDefaults.OpenRecordset("SELECT * FROM DefaultInfo");
				if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
				{
					txtResidenceCode.Text = FCConvert.ToString(rsDefaults.Get_Fields_String("ResidenceCode"));
				}
			}
		}

		private void frmTransitPlate_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}

            fraCodes.CenterToContainer(this.ClientArea);
        }

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
			//frmPlateInfo.InstancePtr.Show(App.MainForm);
			frmPlateInfo.InstancePtr.ShowForm();
		}

		private void frmTransitPlate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraCodes.Visible == true)
				{
					cmdCancel.Focus();
					cmdCancel_Click();
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void txtEffectiveDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(txtEffectiveDate.Text))
			{
				txtExpireDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", 9, fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text)), "MM/dd/yyyy");
			}
			else
			{
				e.Cancel = true;
				MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtExpireDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsDate(txtExpireDate.Text))
			{
				if (fecherFoundation.DateAndTime.DateValue(txtExpireDate.Text).ToOADate() <= fecherFoundation.DateAndTime.DateAdd("d", 9, DateTime.Now).ToOADate())
				{
					// do nothing
				}
				else
				{
					e.Cancel = true;
					MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				e.Cancel = true;
				MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void txtMake_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rsMake = new clsDRWrapper();
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			if (txtMake.Text != "")
			{
				rsMake.OpenRecordset("SELECT * FROM Make WHERE Code = '" + txtMake.Text + "'");
				if (rsMake.EndOfFile() != true && rsMake.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					// answer = MsgBox("This is not a valid Make code.  Do you wish to use it?", vbQuestion + vbYesNo, "Invalid Make")
					// If answer = vbNo Then
					// Cancel = True
					// txtMake.Text = ""
					// End If
					answer = MessageBox.Show("That is not a valid make.  Would you like to see a list of the Valid Makes?", "Invalid Make", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer == DialogResult.Yes)
					{
						e.Cancel = true;
						txtMake.Text = "";
						FillCodes();
						fraCodes.Visible = true;
						//FC:FINAL:DDU:#i1962 - fixed show of Make Codes
						fraCodes.BringToFront();
						fraCodes.CenterToContainer(this.ClientArea);
						cboCodesList.Focus();
					}
				}
			}
		}

		private void txtPlate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			if (fecherFoundation.Strings.Trim(txtPlate.Text) != "")
			{
				rs.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'PXSXX' AND Status = 'A' AND Number = " + FCConvert.ToString(Conversion.Val(txtPlate.Text)));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					e.Cancel = true;
					MessageBox.Show("There are no transit plates with this number in inventory.", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtPlate.SelectionLength = 1;
				}
			}
		}

		private void txtVIN_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			if (txtVIN.Text != "")
			{
				if (CheckDigit(txtVIN.Text) == false)
				{
					answer = MessageBox.Show("This is an invalid VIN.  Do you wish to use it anyway?", "Use this VIN?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer == DialogResult.Yes)
					{
						BadVIN = true;
						return;
					}
					else
					{
						e.Cancel = true;
						txtVIN.Text = "";
					}
				}
				else
				{
					BadVIN = false;
				}
			}
		}

		private void txtYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// kk08032016 tromv-1171  Don't validate the year, not sure where 80 years old came from
			// If txtYear <> "" Then
			// If txtYear.Text < Year(Now) - 80 Or txtYear.Text > Year(Now) + 1 Then
			// Cancel = True
			// txtYear.Text = ""
			// MsgBox "The year you have entered is incorrect.", vbExclamation, "Invalid Year"
			// End If
			// End If
		}

		public bool CheckDigit(string hold)
		{
			bool CheckDigit = false;
			// vbPorter upgrade warning: cd As int	OnWriteFCConvert.ToInt32(
			int[] cd = new int[17 + 1];
			int fnx;
			string OneDigit = "";
			float cdvalue;
			float cdOperand;
			float cdOperand2;
			CheckDigit = false;
			for (fnx = 1; fnx <= 17; fnx++)
			{
				OneDigit = Strings.Mid(hold, fnx, 1);
				if (string.Compare(OneDigit, "0") < 0)
					return CheckDigit;
				if (string.Compare(OneDigit, "9") > 0)
				{
					if (string.Compare(OneDigit, "A") < 0)
						return CheckDigit;
					if (string.Compare(OneDigit, "Z") > 0)
						return CheckDigit;
					if (string.Compare(OneDigit, "I") < 0)
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 64);
					}
					else if (string.Compare(OneDigit, "S") < 0)
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 73);
					}
					else
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 81);
					}
				}
				else
				{
					cd[fnx] = FCConvert.ToInt32(Math.Round(Conversion.Val(OneDigit)));
				}
			}
			// fnx
			cdvalue = cd[1] * 8;
			cdvalue += (cd[2] * 7);
			cdvalue += (cd[3] * 6);
			cdvalue += (cd[4] * 5);
			cdvalue += (cd[5] * 4);
			cdvalue += (cd[6] * 3);
			cdvalue += (cd[7] * 2);
			cdvalue += (cd[8] * 10);
			cdvalue += (cd[10] * 9);
			cdvalue += (cd[11] * 8);
			cdvalue += (cd[12] * 7);
			cdvalue += (cd[13] * 6);
			cdvalue += (cd[14] * 5);
			cdvalue += (cd[15] * 4);
			cdvalue += (cd[16] * 3);
			cdvalue += (cd[17] * 2);
			if (cd[9] == FCUtils.iMod(cdvalue, 11))
				CheckDigit = true;
			if (FCUtils.iMod(cdvalue, 11) == 10 && Strings.Mid(hold, 9, 1) == "X")
				CheckDigit = true;
			return CheckDigit;
		}

		private void FillCodes()
		{
			clsDRWrapper tempRS = new clsDRWrapper();
			tempRS.OpenRecordset("SELECT * FROM Make ORDER BY Code");
			if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
			{
				tempRS.MoveLast();
				tempRS.MoveFirst();
				cboCodesList.Clear();
				while (tempRS.EndOfFile() != true)
				{
					cboCodesList.AddItem(tempRS.Get_Fields("Code") + " - " + tempRS.Get_Fields_String("make"));
					tempRS.MoveNext();
				}
			}
		}

		private void txtCustomerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCustomerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtCustomerNumber.Text) && Conversion.Val(txtCustomerNumber.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtCustomerNumber.Text)));
				// tromvs-56 03.20.2017 kjr fix overflow error
			}
			else
			{
				ClearPartyInfo();
			}
		}

		public void ClearPartyInfo()
		{
			lblCustomerInfo.Text = "";
			cmdEdit.Enabled = false;
		}

		public void GetPartyInfo(int intPartyID)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				lblCustomerInfo.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
				pAdd = pInfo.GetAddress("MV", intPartyID);
				if (!(pAdd == null))
				{
					lblCustomerInfo.Text = lblCustomerInfo.Text + "\r\n" + pAdd.GetFormattedAddress();
				}
				cmdEdit.Enabled = true;
			}
		}

		private void cmdEdit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtCustomerNumber.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}
	}
}
