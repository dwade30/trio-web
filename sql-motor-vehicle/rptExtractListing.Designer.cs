﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptExtractListing.
	/// </summary>
	partial class rptExtractListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExtractListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAgent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldClass,
				this.fldYear,
				this.fldMake,
				this.fldModel,
				this.fldReg,
				this.fldExcise,
				this.fldAgent,
				this.fldOwner,
				this.fldPost,
				this.fldTotal,
				this.fldEmail
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label5,
				this.Label6,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Line1,
				this.Label15,
				this.Label16,
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label17
			});
			this.PageHeader.Height = 1.135417F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label5.Text = "Label5";
			this.Label5.Top = 0.375F;
			this.Label5.Width = 10.46875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label6.Text = "CL / PLATE";
			this.Label6.Top = 0.9375F;
			this.Label6.Width = 0.78125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.78125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label8.Text = "YEAR";
			this.Label8.Top = 0.9375F;
			this.Label8.Width = 0.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.25F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label9.Text = "MAKE";
			this.Label9.Top = 0.9375F;
			this.Label9.Width = 0.4375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.75F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label10.Text = "MODEL";
			this.Label10.Top = 0.9375F;
			this.Label10.Width = 0.5625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.4375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label11.Text = "REG";
			this.Label11.Top = 0.9375F;
			this.Label11.Width = 0.375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "EXCISE";
			this.Label12.Top = 0.9375F;
			this.Label12.Width = 0.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.5F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "AGENT";
			this.Label13.Top = 0.9375F;
			this.Label13.Width = 0.5F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.1875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label14.Text = "OWNER";
			this.Label14.Top = 0.9375F;
			this.Label14.Width = 2.28125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.125F;
			this.Line1.Width = 10.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10.46875F;
			this.Line1.Y1 = 1.125F;
			this.Line1.Y2 = 1.125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.03125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label15.Text = "POST";
			this.Label15.Top = 0.9375F;
			this.Label15.Width = 0.4375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 4.46875F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label16.Text = "TOTAL";
			this.Label16.Top = 0.9375F;
			this.Label16.Width = 0.625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Extract Listing";
			this.Label1.Top = 0F;
			this.Label1.Width = 10.46875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 9.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 9.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 7.625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label17.Text = "E-MAIL ADDRESS";
			this.Label17.Top = 0.9375F;
			this.Label17.Width = 1.5F;
			// 
			// fldClass
			// 
			this.fldClass.Height = 0.1875F;
			this.fldClass.Left = 0F;
			this.fldClass.Name = "fldClass";
			this.fldClass.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldClass.Text = "Field1";
			this.fldClass.Top = 0F;
			this.fldClass.Width = 0.75F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 0.78125F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.375F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.1875F;
			this.fldMake.Left = 1.25F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 0F;
			this.fldMake.Width = 0.375F;
			// 
			// fldModel
			// 
			this.fldModel.Height = 0.1875F;
			this.fldModel.Left = 1.75F;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldModel.Text = "Field1";
			this.fldModel.Top = 0F;
			this.fldModel.Width = 0.5625F;
			// 
			// fldReg
			// 
			this.fldReg.Height = 0.1875F;
			this.fldReg.Left = 2.34375F;
			this.fldReg.Name = "fldReg";
			this.fldReg.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldReg.Text = "Field1";
			this.fldReg.Top = 0F;
			this.fldReg.Width = 0.46875F;
			// 
			// fldExcise
			// 
			this.fldExcise.Height = 0.1875F;
			this.fldExcise.Left = 2.875F;
			this.fldExcise.Name = "fldExcise";
			this.fldExcise.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExcise.Text = "Field1";
			this.fldExcise.Top = 0F;
			this.fldExcise.Width = 0.5625F;
			// 
			// fldAgent
			// 
			this.fldAgent.Height = 0.1875F;
			this.fldAgent.Left = 3.5625F;
			this.fldAgent.Name = "fldAgent";
			this.fldAgent.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAgent.Text = "Field1";
			this.fldAgent.Top = 0F;
			this.fldAgent.Width = 0.375F;
			// 
			// fldOwner
			// 
			this.fldOwner.Height = 0.1875F;
			this.fldOwner.Left = 5.1875F;
			this.fldOwner.Name = "fldOwner";
			this.fldOwner.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldOwner.Text = "Field1";
			this.fldOwner.Top = 0F;
			this.fldOwner.Width = 2.28125F;
			// 
			// fldPost
			// 
			this.fldPost.Height = 0.1875F;
			this.fldPost.Left = 4F;
			this.fldPost.Name = "fldPost";
			this.fldPost.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldPost.Text = "Field1";
			this.fldPost.Top = 0F;
			this.fldPost.Width = 0.46875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 4.46875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.625F;
			// 
			// fldEmail
			// 
			this.fldEmail.Height = 0.1875F;
			this.fldEmail.Left = 7.625F;
			this.fldEmail.Name = "fldEmail";
			this.fldEmail.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldEmail.Text = "Field1";
			this.fldEmail.Top = 0F;
			this.fldEmail.Width = 2.125F;
			// 
			// rptExtractListing
			//
			// 
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExcise;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAgent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
