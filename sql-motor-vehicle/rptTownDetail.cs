//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports.DataProcessing;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTownDetail.
	/// </summary>
	public partial class rptTownDetail : BaseSectionReport
	{
		public rptTownDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Town Detail Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptTownDetail_ReportEnd;
		}

        private void RptTownDetail_ReportEnd(object sender, EventArgs e)
        {
			rs.DisposeOf();
            rsTDS.DisposeOf();
            rsCategory.DisposeOf();
            rsInventoryMaster.DisposeOf();

		}

        public static rptTownDetail InstancePtr
		{
			get
			{
				return (rptTownDetail)Sys.GetInstance(typeof(rptTownDetail));
			}
		}

		protected rptTownDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTownDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		Decimal curException;
		Decimal curSubTotal;
		Decimal curGrand;
		int TotalUnits;
		// vbPorter upgrade warning: TotalAmt As Decimal	OnWrite(int, Decimal)
		Decimal TotalAmt;
		string strTitle = "";
		clsDRWrapper rsTDS = new clsDRWrapper();
		clsDRWrapper rsCategory = new clsDRWrapper();
		clsDRWrapper rsInventoryMaster = new clsDRWrapper();
		int HOLDSCT;
		string HoldCL = "";
		string HoldSC = "";
		int HOLDWG;
		int HoldRG;
		int HoldLV;
		string strString = "";
		int intPageNumber;
		bool boolExcise;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!rsTDS.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
			txtMain.Text = "";
			txtDollars.Text = "";
			txtUnits.Text = "";
			txtStar.Text = "";
			txtEmpty.Visible = false;
			if (HOLDSCT != 0)
			{
				// Debug.Print .fields("Level
				if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) != HOLDSCT || FCConvert.ToString(rsTDS.Get_Fields_String("Class")) != HoldCL || FCConvert.ToString(rsTDS.Get_Fields_String("Subclass")) != HoldSC || Conversion.Val(rsTDS.Get_Fields_Int32("WeightGroup")) != HOLDWG || FCConvert.ToInt32(rsTDS.Get_Fields_Int32("RateGroup")) != HoldRG || FCConvert.ToInt32(rsTDS.Get_Fields_Int32("GroupLevel")) != HoldLV)
				{
					// Debug.Print .fields("Level
					if (HoldLV == 1)
					{
						// rtf1.Text = rtf1.Text & vbCrLf
						rsCategory.FindFirstRecord("CategoryCode", HOLDSCT);

						string vbPorterVar = rsCategory.Get_Fields_String("Heading");
						if (vbPorterVar == "PASSENGER FULL")
						{
							txtMain.Text = "PASSENGER GRAND TOTAL";
						}
						else if (vbPorterVar == "TRACTOR/SPEC MOBILE")
						{
							txtMain.Text = "TRACTOR/SPECIAL MOBILE EQUIPMENT GRAND TOTAL";
						}
						else if ((vbPorterVar == "MOTOR HOME") || (vbPorterVar == "COMMERCIAL") || (vbPorterVar == "TRAILER") || (vbPorterVar == "FARM TRUCK"))
						{
							txtMain.Text = rsCategory.Get_Fields_String("Heading") + " GRAND TOTAL";
						}
						else
						{
							txtMain.Text = rsCategory.Get_Fields_String("Heading") + " TOTAL";
						}
					}
					else if (HoldLV == 2)
					{
						txtEmpty.Visible = false;
						txtMain.Text = GetLevel2Text();
					}
					else if (HoldLV == 3)
					{
						txtEmpty.Visible = false;
						txtMain.Text = GetLevel3Text();
					}
					else if (HoldLV == 4)
					{
						txtEmpty.Visible = false;
						if (HoldCL == "SE")
						{
							frmReport.InstancePtr.rsGVW.FindFirstRecord2("Type, High", "SE," + FCConvert.ToString(HOLDWG), ",");
						}
						else
						{
							frmReport.InstancePtr.rsClass.FindFirstRecord2("SystemCode, BMVCode", HoldSC + "," + HoldCL, ",");
							// kk01102018 tromvs-96  Add BMVCode clause to query
							if (frmReport.InstancePtr.rsClass.Get_Fields_Decimal("RegistrationFee") == 0.01m || HOLDSCT == 40 && (HoldCL == "DV" || HoldCL == "VX"))
							{
								// kk01102018 tromvs-96  Force DV and VX > 10000 lbs to show weight
								frmReport.InstancePtr.rsGVW.FindFirstRecord2("Type, High", "TK," + FCConvert.ToString(HOLDWG), ",");
							}
							else if (frmReport.InstancePtr.rsClass.Get_Fields_Decimal("RegistrationFee") == 0.02m)
							{
								frmReport.InstancePtr.rsGVW.FindFirstRecord2("Type, High", "FM," + FCConvert.ToString(HOLDWG), ",");
							}
							else if (frmReport.InstancePtr.rsClass.Get_Fields_Decimal("RegistrationFee") == 0.03m)
							{
								frmReport.InstancePtr.rsGVW.FindFirstRecord2("Type, High", "F2," + FCConvert.ToString(HOLDWG), ",");
							}
						}
						string strWeight = "";
						strWeight = Strings.Format(frmReport.InstancePtr.rsGVW.Get_Fields("Low"), "#,##0") + " - " + Strings.Format(frmReport.InstancePtr.rsGVW.Get_Fields("High"), "#,##0") + " LBS";
						strString = Strings.StrDup(30, " ");
						txtMain.Text = strString + strWeight;
					}
					else if (HoldLV == 5)
					{
						strString = Strings.StrDup(40, " ");
						if (HoldRG == 0)
						{
							strTitle = strString + "No Fee.................";
						}
						else if (HoldRG == 1)
						{
							strTitle = strString + "Regular................";
						}
						else if (HoldRG == 5)
						{
							if (HoldCL == "TL" || HoldCL == "CL" || HoldCL == "SE")
							{
								strTitle = strString + "Half Year..............";
							}
							else
							{
								strTitle = strString + "Half Rate..............";
							}
						}
						else if (HoldRG == 10)
						{
							if (HoldCL == "TL" || HoldCL == "CL" || HoldCL == "SE")
							{
								strTitle = strString + "One Year...............";
							}
							else
							{
								strTitle = strString + "Full Rate..............";
							}
						}
						else if (HoldRG == 15)
						{
							strTitle = strString + "One and One Half Years.";
						}
						else if (HoldRG == 20)
						{
							strTitle = strString + "Two Year...............";
						}
						else if (HoldRG == 25)
						{
							strTitle = strString + "          $40.00 Credit";
						}
						else if (HoldRG == 101)
						{
							strTitle = strString + "PRO-RATE - JANUARY.....";
						}
						else if (HoldRG == 102)
						{
							strTitle = strString + "PRO-RATE - FEBRUARY....";
						}
						else if (HoldRG == 103)
						{
							strTitle = strString + "PRO-RATE - MARCH.......";
						}
						else if (HoldRG == 104)
						{
							strTitle = strString + "PRO-RATE - APRIL.......";
						}
						else if (HoldRG == 105)
						{
							strTitle = strString + "PRO-RATE - MAY.........";
						}
						else if (HoldRG == 106)
						{
							strTitle = strString + "PRO-RATE - JUNE........";
						}
						else if (HoldRG == 107)
						{
							strTitle = strString + "PRO-RATE - JULY........";
						}
						else if (HoldRG == 108)
						{
							strTitle = strString + "PRO-RATE - AUGUST......";
						}
						else if (HoldRG == 109)
						{
							strTitle = strString + "PRO-RATE - SEPTEMBER...";
						}
						else if (HoldRG == 110)
						{
							strTitle = strString + "PRO-RATE - OCTOBER.....";
						}
						else if (HoldRG == 111)
						{
							strTitle = strString + "PRO-RATE - NOVEMBER....";
						}
						else if (HoldRG == 112)
						{
							strTitle = strString + "PRO-RATE - DECEMBER....";
						}
						else
						{
							strTitle = strString + "Uncoded................";
						}
						txtMain.Text = strTitle;
						// kk07212016          txtEmpty.Visible = True
					}
					else if (HoldLV == 9 && FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 195)
					{
						txtEmpty.Visible = true;
						// Exception Report Items HERE
						txtMain.Text = "EXCEPTION REPORT ADJUSTMENTS TOTAL";
						txtUnits.Text = Strings.Format(Strings.Format(rsTDS.Get_Fields("Units"), "##,###"), "@@@@@@");
						txtDollars.Text = Strings.Format(Strings.FormatCurrency(rsTDS.Get_Fields("Fee"), 2, VbTriState.vbFalse, VbTriState.vbTrue), "@@@@@@@@@@@@@@");
						curException = FCConvert.ToDecimal(rsTDS.Get_Fields("Fee"));
						goto ExceptionTag;
					}
					else if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 190 || FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 200)
					{
						goto Sub_Grand;
					}
					txtUnits.Text = Strings.Format(Strings.Format(TotalUnits, "##,###"), "@@@@@@");
					txtDollars.Text = Strings.Format(Strings.FormatCurrency(TotalAmt, 2, VbTriState.vbTrue, VbTriState.vbTrue), "@@@@@@@@@@@@@@");
					if (HoldLV == 1)
					{
						txtStar.Text = "****";
					}
					else if (HoldLV == 2)
					{
						txtStar.Text = "*** ";
					}
					else if (HoldLV == 3)
					{
						txtStar.Text = "**  ";
					}
					else if (HoldLV == 4)
					{
						txtStar.Text = "*   ";
					}
					else
					{
						txtStar.Text = "    ";
					}
					ExceptionTag:
					;
					TotalUnits = 0;
					TotalAmt = 0;
				}
			}
			// 
			Sub_Grand:
			;
			HOLDSCT = FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory"));
			HoldCL = fecherFoundation.Strings.Trim(rsTDS.Get_Fields_String("Class") + "");
			HoldSC = fecherFoundation.Strings.Trim(rsTDS.Get_Fields_String("Subclass") + "");
			HOLDWG = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(rsTDS.Get_Fields_Int32("WeightGroup") + ""))));
			HoldRG = FCConvert.ToInt32(rsTDS.Get_Fields_Int32("RateGroup"));
			HoldLV = FCConvert.ToInt32(rsTDS.Get_Fields_Int32("GroupLevel"));
			if (HoldLV == 1)
			{
				if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) != 185 || MotorVehicle.Statics.gboolAllowLongTermTrailers)
				{
					curSubTotal += rsTDS.Get_Fields_Decimal("Fee");
					curGrand += rsTDS.Get_Fields_Decimal("Fee");
				}
				else
				{
					// Do not add Excise Tax
				}
			}
			TotalUnits += rsTDS.Get_Fields_Int32("Units");
			TotalAmt += rsTDS.Get_Fields_Decimal("Fee");
			if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 190 && boolExcise == true)
			{
				// rtf1.Text = rtf1.Text & vbCrLf
				// strLine = String(80, " ")
				txtMain.Text = "SUBTOTAL";
				txtUnits.Text = Strings.Format(Strings.Format(0, "##,###"), "@@@@@@");
				txtDollars.Text = Strings.Format(Strings.FormatCurrency(curSubTotal, 2, VbTriState.vbFalse, VbTriState.vbTrue), "@@@@@@@@@@@@@@");
				// rtf1.Text = rtf1.Text & strLine
				// rtf1.Text = rtf1.Text & vbCrLf
			}
			else if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 200)
			{
				// rtf1.Text = rtf1.Text & vbCrLf
				// strLine = String(80, " ")
				txtMain.Text = "ADJUSTED DETAIL REPORT GRAND TOTAL";
				txtUnits.Text = Strings.Format(Strings.Format(0, "##,###"), "@@@@@@");
				txtDollars.Text = Strings.Format(Strings.FormatCurrency(curGrand + curException, 2, VbTriState.vbFalse, VbTriState.vbTrue), "@@@@@@@@@@@@@@");
				// rtf1.Text = rtf1.Text & strLine
				// rtf1.Text = rtf1.Text & vbCrLf
			}
			if (txtMain.Text == "EXCISE TAX TOTAL")
			{
				boolExcise = true;
				goto Skip;
			}
			// Sub And Grand TOTALS
			// On Error GoTo 0
			rsTDS.MoveNext();
			Skip:
			;
			// Loop
			// End If
		}

		private string GetLevel2Text()
		{
			string returnText = "";
			switch (HoldCL)
			{
				case "AG":
					returnText = "          AGRICULTURE PLATE GRAND TOTAL";
					break;
				case "BY":
					returnText = "          BREAST CANCER PLATE GRAND TOTAL";
					break;
				case "BZ":
					returnText = "          BLACK BEAR PLATE GRAND TOTAL";
					break;
				case "DY":
					returnText = "          DUNE BUGGY";
					break;
				case "LS":
					returnText = "          LOW SPEED VEHICLE";
					break;
				case "MQ":
					returnText = "          ANTIQUE MOTORCYCLE";
					break;
				case "NG":
					returnText = "          NATIONAL GUARD";
					break;
				case "PC":
					returnText = "          PASSENGER PLATE GRAND TOTAL";
					break;
				case "RT":
					returnText = "          RENTAL PLATE GRAND TOTAL";
					break;
				case "SC":
					returnText = "          STOCK CAR";
					break;
				case "TZ":
					returnText = "          SPECIAL MOBILE EQUIPMENT";
					break;
				case "X1":
					returnText = "          TRANSIT (ONE WAY)";
					break;
				case "X2":
					returnText = "          TRANSIT (ROUND TRIP)";
					break;
				default:
					if (!rsInventoryMaster.FindFirstRecord("MonthYearClass", HoldCL))
					{
						returnText = "          Uncoded" + Strings.Format(HoldCL, "@@@");
					}
					else
					{
						returnText = "          " + rsInventoryMaster.Get_Fields_String("Description").TrimEnd() + " PLATE GRAND TOTAL";
					}
					break;
			}
			return returnText;
		}

		private string GetLevel3Text()
		{
			string returnText = "";
			if (HoldCL == "TR")
			{
				if (HoldSC == "RT")
				{
					frmReport.InstancePtr.rsClass.FindFirstRecord2("SystemCode, BMVCode", "T4,TR", ",");
				}
				else if (HoldSC == "FT")
				{
					frmReport.InstancePtr.rsClass.FindFirstRecord2("SystemCode, BMVCode", "T5,TR", ",");
				}
			}
			else if (HoldCL == "TZ")
			{
				frmReport.InstancePtr.rsClass.FindFirstRecord2("SystemCode, BMVCode", HoldSC + ",TR", ",");
			}
			else if (HoldCL == "BZ")
			{
				frmReport.InstancePtr.rsClass.FindFirst("SystemCode = '" + HoldSC.Substring(0, 2) + "' and BMVCode = 'BB'");
			}
			else if (HoldCL == "BY")
			{
				frmReport.InstancePtr.rsClass.FindFirst("SystemCode = '" + HoldSC.Substring(0, 2) + "' and BMVCode = 'BC'");
			}
			else if (HoldCL == "AG")
			{
				frmReport.InstancePtr.rsClass.FindFirst("SystemCode = '" + HoldSC.Substring(0, 2) + "' and BMVCode = 'AG'");
			}
			else
			{
				frmReport.InstancePtr.rsClass.FindFirstRecord2("SystemCode, BMVCode", HoldSC + "," + HoldCL, ",");
			}
			if (HoldSC.Length > 2 && HoldSC.EndsWith("R"))
			{
				returnText = Strings.StrDup(20, " ") + "RENTAL " + frmReport.InstancePtr.rsClass.Get_Fields_String("Description");
			}
			else
			{
				returnText = Strings.StrDup(20, " ") + "REGULAR " + frmReport.InstancePtr.rsClass.Get_Fields_String("Description");
			}

			return returnText;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			intPageNumber = 0;
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							int lngPK1 = 0;
							MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(MotorVehicle.Statics.strSql);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				// txtVendorID.Text = "TRIO"
				// txtMuni.Text = .fields("Town
				// txtTownCounty.Text = Format(.fields("ResidenceCode"), "00000")
				// txtAgent.Text = Trim$(.fields("ReportAgent"))
				// txtVersion.Text = Format(App.Major, "00") & "." & App.Minor & "." & App.Revision
				// txtPhone.Text = .fields("ReportTelephone
				// lblPage.Caption = "Page " & rptDiskVerification.PageNumber
				// txtDate.Text = Format(Now, "MM/dd/yyyy")
				// txtProcess.Text = strProcessDate
				// txtAuthType.Text = strLevel
				// txtDateReceived.Text = "___/___/___"
			}
			HOLDSCT = 0;
			HoldCL = "";
			HoldSC = "";
			HOLDWG = 0;
			HoldRG = 0;
			HoldLV = 0;
			rsInventoryMaster.OpenRecordset("SELECT * FROM InventoryMaster");
			rsTDS.OpenRecordset("SELECT * FROM TownDetailSummary ORDER BY SummaryCategory, Class, SubClass, WeightGroup, RateGroup, GroupLevel");
			rsCategory.OpenRecordset("SELECT * FROM TownSummaryHeadings");
			if (rsTDS.EndOfFile() != true && rsTDS.BeginningOfFile() != true)
			{
				rsTDS.MoveLast();
				rsTDS.MoveFirst();
			}
			rs1.DisposeOf();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void rptTownDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTownDetail properties;
			//rptTownDetail.Caption	= "Town Detail Report";
			//rptTownDetail.Icon	= "rptTownDetail.dsx":0000";
			//rptTownDetail.Left	= 0;
			//rptTownDetail.Top	= 0;
			//rptTownDetail.Width	= 11880;
			//rptTownDetail.Height	= 8595;
			//rptTownDetail.StartUpPosition	= 3;
			//rptTownDetail.SectionData	= "rptTownDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
