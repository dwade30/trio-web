//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmUseTax : BaseForm
	{
		public frmUseTax()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label1.AddControlArrayElement(Label1_0, 0);
            Label1.AddControlArrayElement(Label1_1, 1);

            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmUseTax InstancePtr
		{
			get
			{
				return (frmUseTax)Sys.GetInstance(typeof(frmUseTax));
			}
		}

		protected frmUseTax _InstancePtr = null;
		//=========================================================
		clsDRWrapper rs1 = new clsDRWrapper();
		string str1;
		int NewBackColor = ColorTranslator.ToOle(Color.Yellow);
		// &HD2FFFF
		// vbPorter upgrade warning: OldBackColor As int	OnWrite(Color)
		int OldBackColor;
		bool blnEditFlag;

		private enum enumExempt
		{
			ExemptOrg = 1,
			PrevUse = 2,
			PaidOtherJur = 3,
			AmputeeVet = 4,
			ShortTermRental = 5,
			InterstateCommerce = 6,
			Other = 7,
		}

		private void chkFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!blnEditFlag)
			{
				if (chkFleet.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (MotorVehicle.Statics.VehiclesCovered < 2)
					{
						MotorVehicle.Statics.VehiclesCovered = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("How many vehicles will be covered by this Use Tax form?", "Number of Vehicles?", null))));
					}
					if (MotorVehicle.Statics.VehiclesCovered < 2)
					{
						MotorVehicle.Statics.FleetUseTax = false;
						chkFleet.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					else
					{
						MotorVehicle.Statics.FleetUseTax = true;
						MessageBox.Show("Please enter the combined price of all the vehicles in the Price field.", "Price for All Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					MotorVehicle.Statics.VehiclesCovered = 1;
					MotorVehicle.Statics.FleetUseTax = false;
				}
			}
		}

		private void chkRegistrant1_CheckedChanged(object sender, System.EventArgs e)
		{
			object ans;
			string strSql = "";
			clsDRWrapper rsCP = new clsDRWrapper();
			if (chkRegistrant1.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (!MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					if (frmDataInput.InstancePtr.txtReg1ICM.Text == "C" || frmDataInput.InstancePtr.txtReg1ICM.Text == "M")
                    {
                        txtUTCPurchaserFName.Text = frmDataInput.InstancePtr.strReg1FirstName;
                    }
					else
                    {
                        txtUTCPurchaserFName.Text = frmDataInput.InstancePtr.strReg1FirstName.Trim() + " " +
                                                    frmDataInput.InstancePtr.strReg1MI.Trim();
                        txtUTCPurchaserLName.Text = frmDataInput.InstancePtr.strReg1LastName.Trim() + " " + frmDataInput.InstancePtr.strReg1Designation.Trim();
					}
					txtUTCPurcahserAddress.Text = frmDataInput.InstancePtr.txtAddress1.Text;
					txtUTCPurchaserCity.Text = frmDataInput.InstancePtr.txtCity.Text;
					txtUTCPurchaserState.Text = frmDataInput.InstancePtr.txtState.Text;
					txtUTCPurchaserZip.Text = frmDataInput.InstancePtr.txtZip.Text;
				}
				else
				{
					txtUTCPurchaserFName.Text = frmDataInputLongTermTrailers.InstancePtr.lblRegistrant1.Text;
					txtUTCPurchaserLName.Text = "";
					txtUTCPurcahserAddress.Text = frmDataInputLongTermTrailers.InstancePtr.txtAddress1.Text;
					txtUTCPurchaserCity.Text = frmDataInputLongTermTrailers.InstancePtr.txtCity.Text;
					txtUTCPurchaserState.Text = frmDataInputLongTermTrailers.InstancePtr.txtState.Text;
					txtUTCPurchaserZip.Text = frmDataInputLongTermTrailers.InstancePtr.txtZip.Text;
				}
			}
			else
			{
				txtUTCPurchaserFName.Text = "";
				txtUTCPurchaserLName.Text = "";
				txtUTCPurcahserAddress.Text = "";
				txtUTCPurchaserCity.Text = "";
				txtUTCPurchaserState.Text = "";
				txtUTCPurchaserZip.Text = "";
			}
			txtUTCPrice.Focus();
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As object	OnWrite(DialogResult)
			DialogResult ans;
			ans = MessageBox.Show("Would you like to save this Use Tax Form?", "Save Use Tax", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.No)
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);
				MotorVehicle.Statics.NeedToPrintUseTax = false;
				frmUseTax.InstancePtr.Close();
				return;
			}
			else
			{
				SaveInfo();
			}
		}

		public void cmdReturn_Click()
		{
			cmdReturn_Click(cmdReturn, new System.EventArgs());
		}

		private void frmUseTax_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper ctaRS = new clsDRWrapper();
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			blnEditFlag = false;
			if (DateTime.Today.ToOADate() >= FCConvert.ToDateTime("10/1/2013").ToOADate())
			{
				txtRate.Text = "0.055";
			}
			else
			{
				txtRate.Text = "0.050";
			}
			if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") == true)
			{
				if (MotorVehicle.Statics.lngUTCAddNewID != 0)
				{
					FillScreen();
					if (MotorVehicle.Statics.NeedToPrintUseTax)
					{
						cmbUseTaxPrint.Text = "This Form NEEDS to be printed";
					}
					else
					{
						cmbUseTaxPrint.Text = "This Form does NOT need to be printed";
					}
				}
				else
				{
					MotorVehicle.Statics.FleetUseTax = false;
				}
			}
			else
			{
				MotorVehicle.Statics.FleetUseTax = false;
				// kk01252016 tromv-1080  Default to Type TRAILER if LTT
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					txtPurchasedType.Text = "TRAILER";
				}
				if (MotorVehicle.Statics.RegistrationType == "NRT" && MotorVehicle.Statics.PlateType == 1)
				{
					answer = MessageBox.Show("Bring over vehicle info from transfer?", "Vehicle Info", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer == DialogResult.Yes)
					{
						// kk01252016 tromv-1080  Default to Type TRAILER if LTT
						if (!MotorVehicle.Statics.gboolFromLongTermDataEntry)
						{
							txtTradedType.Text = "VEHICLE";
						}
						else
						{
							txtTradedType.Text = "TRAILER";
						}
						txtUTCMakeTraded.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.TradedMake);
						txtUTCModelTraded.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.TradedModel);
						txtUTCYearTraded.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.TradedYear);
						txtUTCVINTraded.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.TradedVIN);
					}
				}
			}
			ctaRS.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
			if (ctaRS.EndOfFile() != true && ctaRS.BeginningOfFile() != true)
			{
				txtUTCDate.Text = Strings.Format(ctaRS.Get_Fields("PurchaseDate"), "MM/dd/yyyy");
				txtUTCSellerName.Text = FCConvert.ToString(ctaRS.Get_Fields_String("SellerName"));
				txtUTCSellerAddress.Text = FCConvert.ToString(ctaRS.Get_Fields_String("SellerAddress1"));
				txtUTCSellerCity.Text = FCConvert.ToString(ctaRS.Get_Fields_String("SellerCity"));
				txtUTCSellerState.Text = FCConvert.ToString(ctaRS.Get_Fields_String("SellerState"));
				txtUTCSellerZip.Text = FCConvert.ToString(ctaRS.Get_Fields_String("SellerZipAndZip4"));
				txtUTCLienName.Text = FCConvert.ToString(ctaRS.Get_Fields_String("LH1Name"));
				txtUTCLienAddress.Text = FCConvert.ToString(ctaRS.Get_Fields_String("LH1Address1"));
				txtUTCLienCity.Text = FCConvert.ToString(ctaRS.Get_Fields_String("LH1City"));
				txtUTCLienState.Text = FCConvert.ToString(ctaRS.Get_Fields_String("LH1State"));
				txtUTCLienZip.Text = FCConvert.ToString(ctaRS.Get_Fields_String("LH1ZipAndZip4"));
			}
			cmbUseTaxPrint.Text = "This Form NEEDS to be printed";
			;
		}

		private void frmUseTax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmUseTax.InstancePtr.Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				if (FCGlobal.Screen.ActiveControl is Global.T2KOverTypeBox || FCGlobal.Screen.ActiveControl is FCTextBox)
				{
					KeyAscii = KeyAscii - 32;
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmUseTax_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUseTax properties;
			//frmUseTax.ScaleWidth	= 9330;
			//frmUseTax.ScaleHeight	= 7155;
			//frmUseTax.LinkTopic	= "Form1";
			//End Unmaped Properties
			frmUseTax.InstancePtr.TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal - frmUseTax.InstancePtr.HeightOriginal) / 2.0);
			frmUseTax.InstancePtr.LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal - frmUseTax.InstancePtr.WidthOriginal) / 2.0);
			if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") == false)
			{
				MotorVehicle.Statics.NeedToPrintUseTax = false;
			}
			txtTradedType.Text = "NONE";
			txtUTCLienName.Text = "NONE";
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this, false);
			FillExemptCombo();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdReturn_Click();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtUTCPrice.Text) != 0)
			{
				CalculateTax();
			}
			SaveInfo();
		}

		private void txtA1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtA1.BackColor);
			txtA1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtA1_Leave(object sender, System.EventArgs e)
		{
			txtA1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtB1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtB1.BackColor);
			txtB1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtB1_Leave(object sender, System.EventArgs e)
		{
			txtB1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtB2_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtB2.BackColor);
			txtB2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtB2_Leave(object sender, System.EventArgs e)
		{
			txtB2.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtB3_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtB3.BackColor);
			txtB3.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtB3_Leave(object sender, System.EventArgs e)
		{
			txtB3.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtC1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtC1.BackColor);
			txtC1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtC1_Leave(object sender, System.EventArgs e)
		{
			txtC1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtC2_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtC2.BackColor);
			txtC2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtC2_Leave(object sender, System.EventArgs e)
		{
			txtC2.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtC2_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			CalculateTax();
		}

		private void txtD1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtD1.BackColor);
			txtD1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtD1_Leave(object sender, System.EventArgs e)
		{
			txtD1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtE1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtE1.BackColor);
			txtE1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtE1_Leave(object sender, System.EventArgs e)
		{
			txtE1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtX1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtX1.BackColor);
			txtX1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtX1_Leave(object sender, System.EventArgs e)
		{
			txtX1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtPurchasedType_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtPurchasedType.BackColor);
			txtPurchasedType.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtPurchasedType_Leave(object sender, System.EventArgs e)
		{
			txtPurchasedType.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtRate_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtRate.BackColor);
			txtRate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
			{
				KeyAscii = (Keys)0;
			}
			else if (KeyAscii == Keys.Delete)
			{
				if (Strings.InStr(1, txtRate.Text, ".", CompareConstants.vbBinaryCompare) > 0)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtRate_Leave(object sender, System.EventArgs e)
		{
			txtRate.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtUTCPrice.Text) != 0)
			{
				CalculateTax();
			}
		}

		private void txtTradedType_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtTradedType.BackColor);
			txtTradedType.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtTradedType_Leave(object sender, System.EventArgs e)
		{
			txtTradedType.BackColor = ColorTranslator.FromOle(OldBackColor);
			if (txtTradedType.Text == "NONE" && this.ActiveControl.GetName() != "chkRegistrant1")
			{
				txtUTCDate.Focus();
			}
		}

		private void txtUTCAllowance_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCAllowance.BackColor);
			txtUTCAllowance.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCAllowance_Leave(object sender, System.EventArgs e)
		{
			txtUTCAllowance.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCAllowance_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtTradedType.Text != "" && Conversion.Val(txtUTCAllowance.Text) != 0)
			{
				CalculateTax();
			}
		}

		private void txtUTCDate_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCDate.BackColor);
			txtUTCDate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCDate_Leave(object sender, System.EventArgs e)
		{
			txtUTCDate.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void cmbExemptCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kk05082018 tromvs-98  Changed Exempt Code from textbox to drop-down list
			if (blnEditFlag)
			{
				return;
			}
			if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.ExemptOrg))
			{
				// Code = A
				lblA1.Visible = true;
				txtA1.Visible = true;
				lblB1.Visible = false;
				lblB2.Visible = false;
				lblB3.Visible = false;
				txtB1.Visible = false;
				txtB2.Visible = false;
				txtB3.Visible = false;
				lblC1.Visible = false;
				lblC2.Visible = false;
				txtC1.Visible = false;
				txtC2.Visible = false;
				lblD1.Visible = false;
				txtD1.Visible = false;
				lblE1.Visible = false;
				txtE1.Visible = false;
				lblX1.Visible = false;
				txtX1.Visible = false;
				CalculateTax();
				txtA1.Focus();
			}
			else if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.PrevUse))
			{
				// Code = B
				lblA1.Visible = false;
				txtA1.Visible = false;
				lblB1.Visible = true;
				lblB2.Visible = true;
				lblB3.Visible = true;
				txtB1.Visible = true;
				txtB2.Visible = true;
				txtB3.Visible = true;
				lblC1.Visible = false;
				lblC2.Visible = false;
				txtC1.Visible = false;
				txtC2.Visible = false;
				lblD1.Visible = false;
				txtD1.Visible = false;
				lblE1.Visible = false;
				txtE1.Visible = false;
				lblX1.Visible = false;
				txtX1.Visible = false;
				CalculateTax();
				txtB1.Focus();
			}
			else if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.PaidOtherJur))
			{
				// Code = C
				lblA1.Visible = false;
				txtA1.Visible = false;
				lblB1.Visible = false;
				lblB2.Visible = false;
				lblB3.Visible = false;
				txtB1.Visible = false;
				txtB2.Visible = false;
				txtB3.Visible = false;
				lblC1.Visible = true;
				lblC2.Visible = true;
				txtC1.Visible = true;
				txtC2.Visible = true;
				lblD1.Visible = false;
				txtD1.Visible = false;
				lblE1.Visible = false;
				txtE1.Visible = false;
				lblX1.Visible = false;
				txtX1.Visible = false;
				CalculateTax();
				txtC1.Focus();
			}
			else if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.AmputeeVet))
			{
				// Code = D
				lblA1.Visible = false;
				txtA1.Visible = false;
				lblB1.Visible = false;
				lblB2.Visible = false;
				lblB3.Visible = false;
				txtB1.Visible = false;
				txtB2.Visible = false;
				txtB3.Visible = false;
				lblC1.Visible = false;
				lblC2.Visible = false;
				txtC1.Visible = false;
				txtC2.Visible = false;
				if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
				{
					lblD1.Visible = true;
					txtD1.Visible = true;
				}
				else
				{
					lblD1.Visible = false;
					txtD1.Visible = false;
				}
				lblE1.Visible = false;
				txtE1.Visible = false;
				lblX1.Visible = false;
				txtX1.Visible = false;
				CalculateTax();
				if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
				{
					txtD1.Focus();
				}
				else
				{
					txtUTCSellerName.Focus();
				}
			}
			else if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.ShortTermRental))
			{
				lblA1.Visible = false;
				txtA1.Visible = false;
				lblB1.Visible = false;
				lblB2.Visible = false;
				lblB3.Visible = false;
				txtB1.Visible = false;
				txtB2.Visible = false;
				txtB3.Visible = false;
				lblC1.Visible = false;
				lblC2.Visible = false;
				txtC1.Visible = false;
				txtC2.Visible = false;
				lblD1.Visible = false;
				txtD1.Visible = false;
				lblE1.Visible = true;
				txtE1.Visible = true;
				lblX1.Visible = false;
				txtX1.Visible = false;
				CalculateTax();
				txtE1.Focus();
			}
			else if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.InterstateCommerce))
			{
				// Code F
				lblA1.Visible = false;
				txtA1.Visible = false;
				lblB1.Visible = false;
				lblB2.Visible = false;
				lblB3.Visible = false;
				txtB1.Visible = false;
				txtB2.Visible = false;
				txtB3.Visible = false;
				lblC1.Visible = false;
				lblC2.Visible = false;
				txtC1.Visible = false;
				txtC2.Visible = false;
				lblD1.Visible = false;
				txtD1.Visible = false;
				lblE1.Visible = false;
				txtE1.Visible = false;
				lblX1.Visible = false;
				txtX1.Visible = false;
				CalculateTax();
				txtUTCSellerName.Focus();
			}
			else if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.Other))
			{
				// Code G (E on older forms)
				lblA1.Visible = false;
				txtA1.Visible = false;
				lblB1.Visible = false;
				lblB2.Visible = false;
				lblB3.Visible = false;
				txtB1.Visible = false;
				txtB2.Visible = false;
				txtB3.Visible = false;
				lblC1.Visible = false;
				lblC2.Visible = false;
				txtC1.Visible = false;
				txtC2.Visible = false;
				lblD1.Visible = false;
				txtD1.Visible = false;
				lblE1.Visible = false;
				txtE1.Visible = false;
				if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
				{
					lblX1.Visible = true;
					txtX1.Visible = true;
				}
				else
				{
					lblX1.Visible = false;
					txtX1.Visible = false;
				}
				CalculateTax();
				if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
				{
					txtX1.Focus();
				}
				else
				{
					txtUTCSellerName.Focus();
				}
			}
			else
			{
				lblA1.Visible = false;
				txtA1.Visible = false;
				lblB1.Visible = false;
				lblB2.Visible = false;
				lblB3.Visible = false;
				txtB1.Visible = false;
				txtB2.Visible = false;
				txtB3.Visible = false;
				lblC1.Visible = false;
				lblC2.Visible = false;
				txtC1.Visible = false;
				txtC2.Visible = false;
				lblD1.Visible = false;
				txtD1.Visible = false;
				lblE1.Visible = false;
				txtE1.Visible = false;
				lblX1.Visible = false;
				txtX1.Visible = false;
				CalculateTax();
				txtUTCSellerName.Focus();
			}
		}

		private void txtUTCLienAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCLienAddress.BackColor);
			txtUTCLienAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCLienAddress_Leave(object sender, System.EventArgs e)
		{
			txtUTCLienAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCLienCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCLienCity.BackColor);
			txtUTCLienCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCLienCity_Leave(object sender, System.EventArgs e)
		{
			txtUTCLienCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCLienName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCLienName.BackColor);
			txtUTCLienName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCLienName_Leave(object sender, System.EventArgs e)
		{
			txtUTCLienName.BackColor = ColorTranslator.FromOle(OldBackColor);
			if (txtUTCLienName.Text == "NONE")
			{
				cmdReturn.Focus();
			}
		}

		private void txtUTCLienState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCLienState.BackColor);
			txtUTCLienState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCLienState_Leave(object sender, System.EventArgs e)
		{
			txtUTCLienState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCLienZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCLienZip.BackColor);
			txtUTCLienZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCLienZip_Leave(object sender, System.EventArgs e)
		{
			txtUTCLienZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCMakeTraded_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCMakeTraded.BackColor);
			txtUTCMakeTraded.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCMakeTraded_Leave(object sender, System.EventArgs e)
		{
			txtUTCMakeTraded.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCModelTraded_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCModelTraded.BackColor);
			txtUTCModelTraded.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCModelTraded_Leave(object sender, System.EventArgs e)
		{
			txtUTCModelTraded.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCNetAmount_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCNetAmount.BackColor);
			txtUTCNetAmount.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCNetAmount_Leave(object sender, System.EventArgs e)
		{
			txtUTCNetAmount.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCNetAmount_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtUTCNetAmount.Text) != 0)
			{
				CalculateTax();
			}
		}

		private void txtUTCPrice_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCPrice.BackColor);
			txtUTCPrice.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCPrice_Leave(object sender, System.EventArgs e)
		{
			txtUTCPrice.BackColor = ColorTranslator.FromOle(OldBackColor);
			if (txtTradedType.Text == "NONE")
			{
				txtUTCNetAmount.Focus();
			}
		}

		private void txtUTCPrice_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtUTCPrice.Text) != 0)
			{
				CalculateTax();
			}
		}

		private void txtUTCPurcahserAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCPurcahserAddress.BackColor);
			txtUTCPurcahserAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCPurcahserAddress_Leave(object sender, System.EventArgs e)
		{
			txtUTCPurcahserAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCPurchaserCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCPurchaserCity.BackColor);
			txtUTCPurchaserCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCPurchaserCity_Leave(object sender, System.EventArgs e)
		{
			txtUTCPurchaserCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCPurchaserFName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCPurchaserFName.BackColor);
			txtUTCPurchaserFName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCPurchaserFName_Leave(object sender, System.EventArgs e)
		{
			txtUTCPurchaserFName.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCPurchaserLName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCPurchaserLName.BackColor);
			txtUTCPurchaserLName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCPurchaserLName_Leave(object sender, System.EventArgs e)
		{
			txtUTCPurchaserLName.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCPurchaserState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCPurchaserState.BackColor);
			txtUTCPurchaserState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCPurchaserState_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode != Keys.Left && KeyCode != Keys.Right)
			{
				if (fecherFoundation.Strings.Trim(txtUTCPurchaserState.Text).Length >= 2)
				{
					txtUTCPurchaserZip.Focus();
				}
			}
		}

		private void txtUTCPurchaserState_Leave(object sender, System.EventArgs e)
		{
			txtUTCPurchaserState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCPurchaserZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCPurchaserZip.BackColor);
			txtUTCPurchaserZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCPurchaserZip_Leave(object sender, System.EventArgs e)
		{
			txtUTCPurchaserZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerAddress.BackColor);
			txtUTCSellerAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerAddress_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerCity.BackColor);
			txtUTCSellerCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerCity_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerName.BackColor);
			txtUTCSellerName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerName_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerName.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerState.BackColor);
			txtUTCSellerState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerState_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerZip.BackColor);
			txtUTCSellerZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerZip_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSSN_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSSN.BackColor);
			txtUTCSSN.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSSN_Leave(object sender, System.EventArgs e)
		{
			txtUTCSSN.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCUseTax_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCUseTax.BackColor);
			txtUTCUseTax.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCUseTax_Leave(object sender, System.EventArgs e)
		{
			txtUTCUseTax.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCVINTraded_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCVINTraded.BackColor);
			txtUTCVINTraded.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCVINTraded_Leave(object sender, System.EventArgs e)
		{
			txtUTCVINTraded.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCYearTraded_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCYearTraded.BackColor);
			txtUTCYearTraded.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCYearTraded_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
			}
			else if (KeyAscii == Keys.Return)
			{
			}
			else if (KeyAscii == Keys.Back)
			{
			}
			else if (KeyAscii == Keys.Escape)
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillScreen()
		{
			str1 = "SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID);
			rs1.OpenRecordset(str1);
			fecherFoundation.Information.Err().Clear();
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				txtTradedType.Text = FCConvert.ToString(rs1.Get_Fields_String("TradedType"));
				txtUTCMakeTraded.Text = FCConvert.ToString(rs1.Get_Fields_String("TradedMake"));
				txtUTCModelTraded.Text = FCConvert.ToString(rs1.Get_Fields_String("TradedModel"));
				txtUTCYearTraded.Text = FCConvert.ToString(rs1.Get_Fields_String("TradedYear"));
				txtUTCVINTraded.Text = FCConvert.ToString(rs1.Get_Fields_String("TradedVIN"));
				txtPurchasedType.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchasedType"));
				txtUTCDate.Text = Strings.Format(rs1.Get_Fields("TradedDate"), "MM/dd/yyyy");
				txtUTCSellerName.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerName"));
				txtUTCSellerAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerAddress1"));
				txtUTCSellerCity.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerCity"));
				txtUTCSellerState.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerState"));
				if (fecherFoundation.FCUtils.IsNull(rs1.Get_Fields_String("SellerZipAndZip4")) == false)
				{
					txtUTCSellerZip.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerZipAndZip4"));
				}
				txtUTCLienName.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1Name"));
				txtUTCLienAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1Address1"));
				txtUTCLienCity.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1City"));
				txtUTCLienState.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1State"));
				if (fecherFoundation.FCUtils.IsNull(rs1.Get_Fields_String("LH1ZipAndZip4")) == false)
				{
					txtUTCLienZip.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1ZipAndZip4"));
				}
				txtUTCPrice.Text = Strings.Format(rs1.Get_Fields_Decimal("PurchasePrice"), "#,##0.00");
				txtUTCAllowance.Text = Strings.Format(rs1.Get_Fields_Decimal("Allowance"), "#,##0.00");
				txtUTCNetAmount.Text = FCConvert.ToString(rs1.Get_Fields("Rate"));
				txtUTCUseTax.Text = Strings.Format(rs1.Get_Fields_Decimal("TaxAmount"), "#,##0.00");
				string vbPorterVar = rs1.Get_Fields_String("ExemptCode");
				// kk05082018  tromvs-98  Rework form for new use tax certificate
				if (vbPorterVar == "A")
				{
					cmbExemptCode.SelectedIndex = FCConvert.ToInt32(enumExempt.ExemptOrg);
					// txtA1.Visible = True
					txtA1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptNumber"));
				}
				else if (vbPorterVar == "B")
				{
					cmbExemptCode.SelectedIndex = FCConvert.ToInt32(enumExempt.PrevUse);
					// txtB1.Visible = True
					// txtB2.Visible = True
					// txtB3.Visible = True
					txtB1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptPreviousState"));
					txtB2.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptRegistrationNumber"));
					txtB3.Text = Strings.Format(rs1.Get_Fields("ExemptDate"), "MM/dd/yyyy");
				}
				else if (vbPorterVar == "C")
				{
					cmbExemptCode.SelectedIndex = FCConvert.ToInt32(enumExempt.PaidOtherJur);
					// txtC1.Visible = True
					// txtC2.Visible = True
					txtC1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptState"));
					txtC2.Text = Strings.Format(rs1.Get_Fields_Decimal("ExemptAmountPaid"), "#,##0.00");
				}
				else if (vbPorterVar == "D")
				{
					cmbExemptCode.SelectedIndex = FCConvert.ToInt32(enumExempt.AmputeeVet);
					if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
					{
						// txtD1.Visible = True
						txtD1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptVACNumber"));
					}
				}
				else if (vbPorterVar == "E")
				{
					if (MotorVehicle.Statics.intUseTaxFormVersion >= 3)
					{
						cmbExemptCode.SelectedIndex = FCConvert.ToInt32(enumExempt.ShortTermRental);
						// txtE1.Visible = True
						txtE1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptSalesTaxNumber"));
					}
				}
				else if (vbPorterVar == "F")
				{
					if (MotorVehicle.Statics.intUseTaxFormVersion >= 3)
					{
						cmbExemptCode.SelectedIndex = FCConvert.ToInt32(enumExempt.InterstateCommerce);
					}
				}
				else if (vbPorterVar == "X")
				{
					cmbExemptCode.SelectedIndex = FCConvert.ToInt32(enumExempt.Other);
					if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
					{
						// txtX1.Visible = True
						txtX1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptDescription"));
					}
				}
				else
				{
					cmbExemptCode.SelectedIndex = 0;
				}
				CalculateTax();
				txtUTCSSN.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserSSN"));
				txtUTCPurchaserFName.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserFName"));
				// kk05082018  tromvs-98  Split purchaser name into FName & LName on new Use Tax Cert
				txtUTCPurchaserLName.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserLName"));
				txtUTCPurcahserAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserAddress"));
				txtUTCPurchaserCity.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserCity"));
				txtUTCPurchaserState.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserState"));
				if (fecherFoundation.FCUtils.IsNull(rs1.Get_Fields_String("PurchaserZipAndZip4")) == false)
				{
					txtUTCPurchaserZip.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserZipAndZip4"));
				}
			}
			blnEditFlag = true;
			if (MotorVehicle.Statics.FleetUseTax)
			{
				chkFleet.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkFleet.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			blnEditFlag = false;
		}

		private void CalculateTax()
		{
			double dblPrice;
			double dblAllowance;
			double dblNetPrice;
			double dblRate;
			double dblTax;
			dblPrice = 0;
			dblAllowance = 0;
			dblNetPrice = 0;
			dblRate = 0;
			dblTax = 0;
			if (Conversion.Val(txtUTCPrice.Text) != 0)
				dblPrice = FCConvert.ToDouble(txtUTCPrice.Text);
			if (Conversion.Val(txtUTCAllowance.Text) != 0)
				dblAllowance = FCConvert.ToDouble(txtUTCAllowance.Text);
			dblNetPrice = dblPrice - dblAllowance;
			if (dblNetPrice < 0)
			{
				dblNetPrice = 0;
			}
			txtUTCNetAmount.Text = Strings.Format(Strings.Format(dblNetPrice, "#,###.00"), "@@@@@@@@@@");
			if (Conversion.Val(Strings.Right(txtRate.Text, 2)) != 0)
			{
				dblRate = FCConvert.ToDouble(txtRate.Text);
			}
			else if (Conversion.Val(txtRate.Text) != 0)
			{
				dblRate = FCConvert.ToDouble(txtRate.Text);
			}
			// If txtUTCExempt.Text <> "N" And Trim(txtUTCExempt.Text) <> "" Then
			// If txtUTCExempt.Text <> "C" Then
			if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) != FCConvert.ToDouble("0"))
			{
				// kk05082018 tromvs-98
				if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) != FCConvert.ToInt32(enumExempt.PaidOtherJur))
				{
					// Code C
					txtUTCUseTax.Text = "0.00";
				}
				else
				{
					if (fecherFoundation.Strings.Trim(txtC2.Text) != "")
					{
						if (FCConvert.ToDecimal(dblRate * dblNetPrice) > FCConvert.ToDecimal(txtC2.Text))
						{
							txtUTCUseTax.Text = Strings.Format(Strings.Format(FCConvert.ToDecimal(dblRate * dblNetPrice) - FCConvert.ToDecimal(txtC2.Text), "#,###.00"), "@@@@@@@@@@");
						}
						else
						{
							txtUTCUseTax.Text = "0.00";
						}
					}
					else
					{
						txtUTCUseTax.Text = "0.00";
					}
				}
			}
			else
			{
				if (dblRate != 0)
				{
					txtUTCUseTax.Text = Strings.Format(Strings.Format(dblRate * dblNetPrice, "#,###.00"), "@@@@@@@@@@");
				}
			}
		}

		private void txtUTCYearTraded_Leave(object sender, System.EventArgs e)
		{
			txtUTCYearTraded.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void frmUseTax_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void SaveInfo()
		{
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				// If txtUTCExempt = "" Or txtUTCExempt = "C" Then                       'kk05082018 tromvs-98
				if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == 0 || cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) == FCConvert.ToInt32(enumExempt.PaidOtherJur))
				{
					// None or Code C
					if (txtUTCPrice.Text == "" || Conversion.Val(txtUTCPrice.Text) == 0)
					{
						MessageBox.Show("You must enter a Price before you can save this information.", "Invalid Price", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtUTCPrice.Focus();
						return;
					}
				}
				if (txtTradedType.Text != "NONE")
				{
					if (txtUTCAllowance.Text == "")
					{
						MessageBox.Show("You must enter an Allowance for the vehicle being traded before you can save this information.  If there is no vehicle being traded enter NONE as the type for the traded vehicle.", "Invalid Allowance", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtUTCAllowance.Focus();
						return;
					}
				}
				else if (fecherFoundation.Strings.Trim(txtUTCMakeTraded.Text) != "" || fecherFoundation.Strings.Trim(txtUTCModelTraded.Text) != "" || fecherFoundation.Strings.Trim(txtUTCYearTraded.Text) != "" || fecherFoundation.Strings.Trim(txtUTCVINTraded.Text) != "")
				{
					MessageBox.Show("You must enter a type for the traded vehicle before you may save.", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtTradedType.Focus();
					return;
				}
				// If txtUTCExempt.Text <> "B" Then              'kk05022018 tromv-1361
				if (cmbExemptCode.ItemData(cmbExemptCode.SelectedIndex) != FCConvert.ToInt32(enumExempt.PrevUse))
				{
					// Not Code B
					if (!Information.IsDate(txtUTCDate.Text))
					{
						MessageBox.Show("You must enter a valid purchase date before you may save this information.", "Invalid Purchase Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtUTCDate.Focus();
						return;
					}
				}
				if (fecherFoundation.Strings.Trim(txtUTCSellerName.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerAddress.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerCity.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerState.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerZip.Text) == "")
				{
					ans = MessageBox.Show("Some seller information has not been filled out.  Do you wish to continue?", "Missing Seller Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						// do nothing
					}
					else
					{
						if (fecherFoundation.Strings.Trim(txtUTCSellerName.Text) == "")
						{
							txtUTCSellerName.Focus();
						}
						else if (fecherFoundation.Strings.Trim(txtUTCSellerAddress.Text) == "")
						{
							txtUTCSellerAddress.Focus();
						}
						else if (fecherFoundation.Strings.Trim(txtUTCSellerCity.Text) == "")
						{
							txtUTCSellerCity.Focus();
						}
						else if (fecherFoundation.Strings.Trim(txtUTCSellerState.Text) == "")
						{
							txtUTCSellerState.Focus();
						}
						else
						{
							txtUTCSellerZip.Focus();
						}
						return;
					}
				}
				frmUseTax.InstancePtr.MousePointer = MousePointerConstants.vbHourglass;
				clsDRWrapper rs1 = new clsDRWrapper();
				// kk05092018  tromvs-98  Rework so we're not changing record #1 if UseTaxDone is true
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") == true && MotorVehicle.Statics.lngUTCAddNewID != 0)
				{
					rs1.OpenRecordset("SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID));
					if (rs1.EndOfFile())
					{
						rs1.AddNew();
					}
				}
				else
				{
					rs1.OpenRecordset("SELECT * FROM UseTax WHERE ID = -1");
					rs1.AddNew();
					MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", true);
				}
				rs1.Set_Fields("TradedType", txtTradedType.Text);
				rs1.Set_Fields("TradedMake", txtUTCMakeTraded.Text);
				rs1.Set_Fields("TradedModel", txtUTCModelTraded.Text);
				rs1.Set_Fields("TradedYear", txtUTCYearTraded.Text);
				rs1.Set_Fields("TradedVIN", txtUTCVINTraded.Text);
				rs1.Set_Fields("PurchasedType", txtPurchasedType.Text);
				if (Information.IsDate(txtUTCDate.Text))
					rs1.Set_Fields("TradedDate", Strings.Format(txtUTCDate.Text, "MM/dd/yyyy"));
				rs1.Set_Fields("DateUTCProcessed", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
				rs1.Set_Fields("SellerName", txtUTCSellerName.Text);
				rs1.Set_Fields("SellerAddress1", txtUTCSellerAddress.Text);
				rs1.Set_Fields("SellerCity", txtUTCSellerCity.Text);
				rs1.Set_Fields("SellerState", txtUTCSellerState.Text);
				rs1.Set_Fields("SellerZipAndZip4", txtUTCSellerZip.Text);
				rs1.Set_Fields("LH1Name", txtUTCLienName.Text);
				rs1.Set_Fields("LH1Address1", txtUTCLienAddress.Text);
				rs1.Set_Fields("LH1City", txtUTCLienCity.Text);
				rs1.Set_Fields("LH1State", txtUTCLienState.Text);
				rs1.Set_Fields("LH1ZipAndZip4", txtUTCLienZip.Text);
				if (Conversion.Val(txtUTCPrice.Text) != 0)
				{
					rs1.Set_Fields("PurchasePrice", FCConvert.ToDecimal(txtUTCPrice.Text));
				}
				else
				{
					rs1.Set_Fields("PurchasePrice", 0);
				}
				if (Conversion.Val(txtUTCAllowance.Text) != 0)
				{
					rs1.Set_Fields("Allowance", FCConvert.ToInt32(FCConvert.ToDouble(txtUTCAllowance.Text)));
				}
				else
				{
					rs1.Set_Fields("Allowance", 0);
				}
				if (Conversion.Val(txtUTCNetAmount.Text) != 0)
				{
					rs1.Set_Fields("Rate", FCConvert.ToDecimal(txtUTCNetAmount.Text));
				}
				else
				{
					rs1.Set_Fields("Rate", 0);
				}
				if (Conversion.Val(txtUTCUseTax.Text) != 0)
				{
					rs1.Set_Fields("TaxAmount", FCConvert.ToDecimal(txtUTCUseTax.Text));
				}
				else
				{
					rs1.Set_Fields("TaxAmount", 0);
				}
				switch ((enumExempt)cmbExemptCode.SelectedIndex)
				{
				// kk05082018  tromvs-98
					case enumExempt.ExemptOrg:
						{
							rs1.Set_Fields("ExemptCode", "A");
							rs1.Set_Fields("ExemptNumber", txtA1.Text);
							break;
						}
					case enumExempt.PrevUse:
						{
							rs1.Set_Fields("ExemptCode", "B");
							rs1.Set_Fields("ExemptPreviousState", txtB1.Text);
							rs1.Set_Fields("ExemptRegistrationNumber", txtB2.Text);
							if (Information.IsDate(Strings.Format(txtB3.Text, "MM/dd/yyyy")))
							{
								rs1.Set_Fields("ExemptDate", Strings.Format(txtB3.Text, "MM/dd/yyyy"));
							}
							break;
						}
					case enumExempt.PaidOtherJur:
						{
							rs1.Set_Fields("ExemptCode", "C");
							rs1.Set_Fields("ExemptState", txtC1.Text);
							if (fecherFoundation.Strings.Trim(txtC2.Text) == "")
							{
								rs1.Set_Fields("ExemptAmountPaid", 0);
							}
							else
							{
								rs1.Set_Fields("ExemptAmountPaid", FCConvert.ToDecimal(txtC2.Text));
							}
							break;
						}
					case enumExempt.AmputeeVet:
						{
							rs1.Set_Fields("ExemptCode", "D");
							break;
						}
					case enumExempt.ShortTermRental:
						{
							rs1.Set_Fields("ExemptCode", "E");
							rs1.Set_Fields("ExemptSalesTaxNumber", txtE1.Text);
							break;
						}
					case enumExempt.InterstateCommerce:
						{
							rs1.Set_Fields("ExemptCode", "F");
							break;
						}
					case enumExempt.Other:
						{
							rs1.Set_Fields("ExemptCode", "X");
							if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
							{
								rs1.Set_Fields("ExemptDescription", txtX1.Text);
							}
							break;
						}
					default:
						{
							rs1.Set_Fields("ExemptCode", "");
							break;
						}
				}
				//end switch
				rs1.Set_Fields("PurchaserSSN", txtUTCSSN.Text);
				rs1.Set_Fields("PurchaserFName", txtUTCPurchaserFName.Text);
				// kk05082018  tromvs-98  Split purchaser name into FName & LName on new Use Tax Cert
				rs1.Set_Fields("PurchaserLName", txtUTCPurchaserLName.Text);
				rs1.Set_Fields("PurchaserAddress", txtUTCPurcahserAddress.Text);
				rs1.Set_Fields("PurchaserCity", txtUTCPurchaserCity.Text);
				rs1.Set_Fields("PurchaserState", txtUTCPurchaserState.Text);
				rs1.Set_Fields("PurchaserZipAndZip4", txtUTCPurchaserZip.Text);
				MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", true);
				if (Conversion.Val(txtUTCUseTax.Text) != 0)
				{
					if (MotorVehicle.Statics.FleetUseTax)
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", Strings.Format(FCUtils.iDiv(FCConvert.ToDecimal(txtUTCUseTax.Text), MotorVehicle.Statics.VehiclesCovered), "#.00"));
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", FCConvert.ToDecimal(txtUTCUseTax.Text));
					}
				}
				else
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
				}
				rs1.Update();
				MotorVehicle.Statics.lngUTCAddNewID = FCConvert.ToInt32(rs1.Get_Fields_Int32("ID"));
				MotorVehicle.Statics.NeedToPrintUseTax = cmbUseTaxPrint.Text == "This Form NEEDS to be printed";
				;
				//App.DoEvents();
				// rs1.Close
				rs1.Reset();
				frmUseTax.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
				frmUseTax.InstancePtr.Close();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				frmUseTax.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("There was an error processing this Use Tax Certificate.  (Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " was encountered, " + fecherFoundation.Information.Err(ex).Description + ".", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				fecherFoundation.Information.Err(ex).Clear();
				frmUseTax.InstancePtr.Close();
			}
		}

		private void FillExemptCombo()
		{
			cmbExemptCode.AddItem("None");
			cmbExemptCode.ItemData(0, 0);
			cmbExemptCode.AddItem("A. Exempt Organization");
			cmbExemptCode.ItemData(1, FCConvert.ToInt32(enumExempt.ExemptOrg));
			cmbExemptCode.AddItem("B. Prev Use Outside of Maine");
			cmbExemptCode.ItemData(2, FCConvert.ToInt32(enumExempt.PrevUse));
			cmbExemptCode.AddItem("C. Tax Paid Another Jurisdiction");
			cmbExemptCode.ItemData(3, FCConvert.ToInt32(enumExempt.PaidOtherJur));
			cmbExemptCode.AddItem("D. Amputee Veteran");
			cmbExemptCode.ItemData(4, FCConvert.ToInt32(enumExempt.AmputeeVet));
			if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
			{
				cmbExemptCode.AddItem("E. Other");
				cmbExemptCode.ItemData(5, FCConvert.ToInt32(enumExempt.Other));
			}
			else
			{
				cmbExemptCode.AddItem("E. int Term Rental");
				cmbExemptCode.ItemData(5, FCConvert.ToInt32(enumExempt.ShortTermRental));
				cmbExemptCode.AddItem("F. Interstate Commerce");
				cmbExemptCode.ItemData(6, FCConvert.ToInt32(enumExempt.InterstateCommerce));
				cmbExemptCode.AddItem("G. Other");
				cmbExemptCode.ItemData(7, FCConvert.ToInt32(enumExempt.Other));
			}
			blnEditFlag = true;
			cmbExemptCode.SelectedIndex = 0;
			blnEditFlag = false;
		}
	}
}
