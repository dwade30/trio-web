//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmReminder.
	/// </summary>
	partial class frmReminder
	{
		public fecherFoundation.FCComboBox cmbEmailNo;
		public fecherFoundation.FCLabel lblEmailNo;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public System.Collections.Generic.List<T2KOverTypeBox> txtTownAddress;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame fraTownInfo;
		public fecherFoundation.FCButton cmdTownInfoCancel;
		public fecherFoundation.FCButton cmdTownInfoOK;
		public Global.T2KOverTypeBox txtTownName;
		public Global.T2KOverTypeBox txtTownAddress_0;
		public Global.T2KOverTypeBox txtTownAddress_1;
		public Global.T2KOverTypeBox txtTownAddress_2;
		public Global.T2KOverTypeBox txtTownAddress_3;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCComboBox cboReminderType;
		public fecherFoundation.FCFrame fraEmailOptions;
		public fecherFoundation.FCButton cmdEmailOK;
		public fecherFoundation.FCButton cmdEmailCancel;
		public fecherFoundation.FCCheckBox chkPersonalSignature;
		public fecherFoundation.FCCheckBox chkCompanySignature;
		public fecherFoundation.FCCheckBox chkDisclaimerNotice;
		public fecherFoundation.FCButton cmdEmailReminders;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCRichTextBox txtMessage;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCFrame fraLabelType;
		public fecherFoundation.FCButton cmdLabelOK;
		public fecherFoundation.FCButton cmdLabelCancel;
		public fecherFoundation.FCComboBox cboLabelType;
		public fecherFoundation.FCLabel lblLabelDescription;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraPostage;
		public fecherFoundation.FCButton Command2;
		public fecherFoundation.FCButton Command1;
		public Global.T2KBackFillDecimal txtPostage;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCButton cmdExtract;
		public fecherFoundation.FCComboBox cboYear;
		public fecherFoundation.FCComboBox cboMonth;
		public fecherFoundation.FCButton cmdPrintLabels;
		public fecherFoundation.FCButton cmdPrintReminders;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCButton cmdPrintList;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCGrid vsVehicles;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblMonth;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileEditTownInfo;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbEmailNo = new fecherFoundation.FCComboBox();
			this.lblEmailNo = new fecherFoundation.FCLabel();
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.lblAll = new fecherFoundation.FCLabel();
			this.fraTownInfo = new fecherFoundation.FCFrame();
			this.cmdTownInfoCancel = new fecherFoundation.FCButton();
			this.cmdTownInfoOK = new fecherFoundation.FCButton();
			this.txtTownName = new Global.T2KOverTypeBox();
			this.txtTownAddress_0 = new Global.T2KOverTypeBox();
			this.txtTownAddress_1 = new Global.T2KOverTypeBox();
			this.txtTownAddress_2 = new Global.T2KOverTypeBox();
			this.txtTownAddress_3 = new Global.T2KOverTypeBox();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.cboReminderType = new fecherFoundation.FCComboBox();
			this.fraEmailOptions = new fecherFoundation.FCFrame();
			this.cmdEmailOK = new fecherFoundation.FCButton();
			this.cmdEmailCancel = new fecherFoundation.FCButton();
			this.chkPersonalSignature = new fecherFoundation.FCCheckBox();
			this.chkCompanySignature = new fecherFoundation.FCCheckBox();
			this.chkDisclaimerNotice = new fecherFoundation.FCCheckBox();
			this.cmdEmailReminders = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtMessage = new fecherFoundation.FCRichTextBox();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.fraLabelType = new fecherFoundation.FCFrame();
			this.cmdLabelOK = new fecherFoundation.FCButton();
			this.cmdLabelCancel = new fecherFoundation.FCButton();
			this.cboLabelType = new fecherFoundation.FCComboBox();
			this.lblLabelDescription = new fecherFoundation.FCLabel();
			this.cmdClear = new fecherFoundation.FCButton();
			this.fraPostage = new fecherFoundation.FCFrame();
			this.Command2 = new fecherFoundation.FCButton();
			this.Command1 = new fecherFoundation.FCButton();
			this.txtPostage = new Global.T2KBackFillDecimal();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.cmdExtract = new fecherFoundation.FCButton();
			this.cboYear = new fecherFoundation.FCComboBox();
			this.cboMonth = new fecherFoundation.FCComboBox();
			this.cmdPrintLabels = new fecherFoundation.FCButton();
			this.cmdPrintReminders = new fecherFoundation.FCButton();
			this.cmdSelect = new fecherFoundation.FCButton();
			this.cmdPrintList = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.vsVehicles = new fecherFoundation.FCGrid();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.lblMonth = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileEditTownInfo = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileEditTownInfo = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).BeginInit();
			this.fraTownInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdTownInfoCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTownInfoOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraEmailOptions)).BeginInit();
			this.fraEmailOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmailOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmailCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPersonalSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCompanySignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDisclaimerNotice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmailReminders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraLabelType)).BeginInit();
			this.fraLabelType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdLabelOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLabelCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPostage)).BeginInit();
			this.fraPostage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPostage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExtract)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabels)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintReminders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileEditTownInfo)).BeginInit();
			this.SuspendLayout();
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraLabelType);
			this.ClientArea.Controls.Add(this.fraPostage);
			this.ClientArea.Controls.Add(this.fraEmailOptions);
			this.ClientArea.Controls.Add(this.fraTownInfo);
			this.ClientArea.Controls.Add(this.cboReminderType);
			this.ClientArea.Controls.Add(this.cmdEmailReminders);
			this.ClientArea.Controls.Add(this.cmbEmailNo);
			this.ClientArea.Controls.Add(this.lblEmailNo);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmdClear);
			this.ClientArea.Controls.Add(this.cmbAll);
			this.ClientArea.Controls.Add(this.lblAll);
			this.ClientArea.Controls.Add(this.cmdExtract);
			this.ClientArea.Controls.Add(this.cboYear);
			this.ClientArea.Controls.Add(this.cboMonth);
			this.ClientArea.Controls.Add(this.cmdPrintLabels);
			this.ClientArea.Controls.Add(this.cmdPrintReminders);
			this.ClientArea.Controls.Add(this.cmdSelect);
			this.ClientArea.Controls.Add(this.cmdPrintList);
			this.ClientArea.Controls.Add(this.cmdSearch);
			this.ClientArea.Controls.Add(this.vsVehicles);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblYear);
			this.ClientArea.Controls.Add(this.lblMonth);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileEditTownInfo);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileEditTownInfo, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(208, 30);
			this.HeaderText.Text = "Reminder Notices";
			// 
			// cmbEmailNo
			// 
			this.cmbEmailNo.Items.AddRange(new object[] {
            "Owners with NO E-mail Address",
            "Owners with E-mail Address",
            "Both"});
			this.cmbEmailNo.Location = new System.Drawing.Point(246, 80);
			this.cmbEmailNo.Name = "cmbEmailNo";
			this.cmbEmailNo.Size = new System.Drawing.Size(369, 41);
			this.cmbEmailNo.TabIndex = 41;
			this.cmbEmailNo.Text = "Both";
			// 
			// lblEmailNo
			// 
			this.lblEmailNo.AutoSize = true;
			this.lblEmailNo.Location = new System.Drawing.Point(30, 94);
			this.lblEmailNo.Name = "lblEmailNo";
			this.lblEmailNo.Size = new System.Drawing.Size(185, 16);
			this.lblEmailNo.TabIndex = 42;
			this.lblEmailNo.Text = "E-MAIL ADDRESS OPTIONS";
			// 
			// cmbAll
			// 
			this.cmbAll.Items.AddRange(new object[] {
            "All Vehicles",
            "Vehicles with Calculated Fees"});
			this.cmbAll.Location = new System.Drawing.Point(246, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(369, 40);
			this.cmbAll.TabIndex = 43;
			this.cmbAll.Text = "All Vehicles";
			// 
			// lblAll
			// 
			this.lblAll.AutoSize = true;
			this.lblAll.Location = new System.Drawing.Point(30, 44);
			this.lblAll.Name = "lblAll";
			this.lblAll.Size = new System.Drawing.Size(146, 16);
			this.lblAll.TabIndex = 44;
			this.lblAll.Text = "VEHICLES INCLUDED";
			// 
			// fraTownInfo
			// 
			this.fraTownInfo.BackColor = System.Drawing.Color.White;
			this.fraTownInfo.Controls.Add(this.cmdTownInfoCancel);
			this.fraTownInfo.Controls.Add(this.cmdTownInfoOK);
			this.fraTownInfo.Controls.Add(this.txtTownName);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_0);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_1);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_2);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_3);
			this.fraTownInfo.Controls.Add(this.Label1_0);
			this.fraTownInfo.Controls.Add(this.Label3);
			this.fraTownInfo.Location = new System.Drawing.Point(30, 304);
			this.fraTownInfo.Name = "fraTownInfo";
			this.fraTownInfo.Size = new System.Drawing.Size(502, 340);
			this.fraTownInfo.TabIndex = 40;
			this.fraTownInfo.Text = "Enter Town Information";
			this.fraTownInfo.Visible = false;
			// 
			// cmdTownInfoCancel
			// 
			this.cmdTownInfoCancel.AppearanceKey = "actionButton";
			this.cmdTownInfoCancel.Location = new System.Drawing.Point(114, 280);
			this.cmdTownInfoCancel.Name = "cmdTownInfoCancel";
			this.cmdTownInfoCancel.Size = new System.Drawing.Size(94, 40);
			this.cmdTownInfoCancel.TabIndex = 42;
			this.cmdTownInfoCancel.Text = "Cancel";
			this.cmdTownInfoCancel.Click += new System.EventHandler(this.cmdTownInfoCancel_Click);
			// 
			// cmdTownInfoOK
			// 
			this.cmdTownInfoOK.AppearanceKey = "actionButton";
			this.cmdTownInfoOK.Location = new System.Drawing.Point(20, 280);
			this.cmdTownInfoOK.Name = "cmdTownInfoOK";
			this.cmdTownInfoOK.Size = new System.Drawing.Size(64, 40);
			this.cmdTownInfoOK.TabIndex = 41;
			this.cmdTownInfoOK.Text = "OK";
			this.cmdTownInfoOK.Click += new System.EventHandler(this.cmdTownInfoOK_Click);
			// 
			// txtTownName
			// 
			this.txtTownName.Location = new System.Drawing.Point(142, 30);
			this.txtTownName.MaxLength = 35;
			this.txtTownName.Name = "txtTownName";
			this.txtTownName.Size = new System.Drawing.Size(340, 40);
			this.txtTownName.TabIndex = 43;
			// 
			// txtTownAddress_0
			// 
			this.txtTownAddress_0.Location = new System.Drawing.Point(142, 80);
			this.txtTownAddress_0.MaxLength = 35;
			this.txtTownAddress_0.Name = "txtTownAddress_0";
			this.txtTownAddress_0.Size = new System.Drawing.Size(340, 40);
			this.txtTownAddress_0.TabIndex = 44;
			// 
			// txtTownAddress_1
			// 
			this.txtTownAddress_1.Location = new System.Drawing.Point(142, 130);
			this.txtTownAddress_1.MaxLength = 35;
			this.txtTownAddress_1.Name = "txtTownAddress_1";
			this.txtTownAddress_1.Size = new System.Drawing.Size(340, 40);
			this.txtTownAddress_1.TabIndex = 45;
			// 
			// txtTownAddress_2
			// 
			this.txtTownAddress_2.Location = new System.Drawing.Point(142, 180);
			this.txtTownAddress_2.MaxLength = 35;
			this.txtTownAddress_2.Name = "txtTownAddress_2";
			this.txtTownAddress_2.Size = new System.Drawing.Size(340, 40);
			this.txtTownAddress_2.TabIndex = 46;
			// 
			// txtTownAddress_3
			// 
			this.txtTownAddress_3.Location = new System.Drawing.Point(142, 230);
			this.txtTownAddress_3.MaxLength = 35;
			this.txtTownAddress_3.Name = "txtTownAddress_3";
			this.txtTownAddress_3.Size = new System.Drawing.Size(340, 40);
			this.txtTownAddress_3.TabIndex = 47;
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(20, 44);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(46, 15);
			this.Label1_0.TabIndex = 49;
			this.Label1_0.Text = "NAME";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 94);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(56, 15);
			this.Label3.TabIndex = 48;
			this.Label3.Text = "ADDRESS";
			// 
			// cboReminderType
			// 
			this.cboReminderType.BackColor = System.Drawing.SystemColors.Window;
			this.cboReminderType.Items.AddRange(new object[] {
            "Postcard",
            "8.5 X 11"});
			this.cboReminderType.Location = new System.Drawing.Point(195, 754);
			this.cboReminderType.Name = "cboReminderType";
			this.cboReminderType.Size = new System.Drawing.Size(188, 40);
			this.cboReminderType.TabIndex = 38;
			// 
			// fraEmailOptions
			// 
			this.fraEmailOptions.BackColor = System.Drawing.Color.White;
			this.fraEmailOptions.Controls.Add(this.cmdEmailOK);
			this.fraEmailOptions.Controls.Add(this.cmdEmailCancel);
			this.fraEmailOptions.Controls.Add(this.chkPersonalSignature);
			this.fraEmailOptions.Controls.Add(this.chkCompanySignature);
			this.fraEmailOptions.Controls.Add(this.chkDisclaimerNotice);
			this.fraEmailOptions.Location = new System.Drawing.Point(253, 304);
			this.fraEmailOptions.Name = "fraEmailOptions";
			this.fraEmailOptions.Size = new System.Drawing.Size(227, 201);
			this.fraEmailOptions.TabIndex = 32;
			this.fraEmailOptions.Text = "E-Mail Options";
			this.fraEmailOptions.Visible = false;
			// 
			// cmdEmailOK
			// 
			this.cmdEmailOK.AppearanceKey = "actionButton";
			this.cmdEmailOK.Location = new System.Drawing.Point(20, 141);
			this.cmdEmailOK.Name = "cmdEmailOK";
			this.cmdEmailOK.Size = new System.Drawing.Size(65, 40);
			this.cmdEmailOK.TabIndex = 37;
			this.cmdEmailOK.Text = "OK";
			this.cmdEmailOK.Click += new System.EventHandler(this.cmdEmailOK_Click);
			// 
			// cmdEmailCancel
			// 
			this.cmdEmailCancel.AppearanceKey = "actionButton";
			this.cmdEmailCancel.Location = new System.Drawing.Point(115, 141);
			this.cmdEmailCancel.Name = "cmdEmailCancel";
			this.cmdEmailCancel.Size = new System.Drawing.Size(92, 40);
			this.cmdEmailCancel.TabIndex = 36;
			this.cmdEmailCancel.Text = "Cancel";
			this.cmdEmailCancel.Click += new System.EventHandler(this.cmdEmailCancel_Click);
			// 
			// chkPersonalSignature
			// 
			this.chkPersonalSignature.Location = new System.Drawing.Point(20, 30);
			this.chkPersonalSignature.Name = "chkPersonalSignature";
			this.chkPersonalSignature.Size = new System.Drawing.Size(140, 23);
			this.chkPersonalSignature.TabIndex = 35;
			this.chkPersonalSignature.Text = "Personal Signature";
			// 
			// chkCompanySignature
			// 
			this.chkCompanySignature.Location = new System.Drawing.Point(20, 67);
			this.chkCompanySignature.Name = "chkCompanySignature";
			this.chkCompanySignature.Size = new System.Drawing.Size(144, 23);
			this.chkCompanySignature.TabIndex = 34;
			this.chkCompanySignature.Text = "Company Signature";
			// 
			// chkDisclaimerNotice
			// 
			this.chkDisclaimerNotice.Location = new System.Drawing.Point(20, 104);
			this.chkDisclaimerNotice.Name = "chkDisclaimerNotice";
			this.chkDisclaimerNotice.Size = new System.Drawing.Size(130, 23);
			this.chkDisclaimerNotice.TabIndex = 33;
			this.chkDisclaimerNotice.Text = "Disclaimer Notice";
			// 
			// cmdEmailReminders
			// 
			this.cmdEmailReminders.AppearanceKey = "actionButton";
			this.cmdEmailReminders.Enabled = false;
			this.cmdEmailReminders.Location = new System.Drawing.Point(709, 704);
			this.cmdEmailReminders.Name = "cmdEmailReminders";
			this.cmdEmailReminders.Size = new System.Drawing.Size(180, 40);
			this.cmdEmailReminders.TabIndex = 31;
			this.cmdEmailReminders.Text = "E-mail Reminders";
			this.cmdEmailReminders.Click += new System.EventHandler(this.cmdEmailReminders_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtMessage);
			this.Frame1.Location = new System.Drawing.Point(30, 131);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(585, 163);
			this.Frame1.TabIndex = 24;
			this.Frame1.Text = "Reminder Notice Message";
			// 
			// txtMessage
			// 
			this.txtMessage.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.txtMessage.Location = new System.Drawing.Point(20, 30);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(545, 113);
			this.txtMessage.TabIndex = 25;
			// 
			// dlg1
			// 
			this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1.FontName = "Microsoft Sans Serif";
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			// 
			// fraLabelType
			// 
			this.fraLabelType.BackColor = System.Drawing.Color.White;
			this.fraLabelType.Controls.Add(this.cmdLabelOK);
			this.fraLabelType.Controls.Add(this.cmdLabelCancel);
			this.fraLabelType.Controls.Add(this.cboLabelType);
			this.fraLabelType.Controls.Add(this.lblLabelDescription);
			this.fraLabelType.Location = new System.Drawing.Point(253, 304);
			this.fraLabelType.Name = "fraLabelType";
			this.fraLabelType.Size = new System.Drawing.Size(329, 230);
			this.fraLabelType.TabIndex = 20;
			this.fraLabelType.Text = "Select Address Label Type";
			this.fraLabelType.Visible = false;
			// 
			// cmdLabelOK
			// 
			this.cmdLabelOK.AppearanceKey = "actionButton";
			this.cmdLabelOK.Location = new System.Drawing.Point(20, 170);
			this.cmdLabelOK.Name = "cmdLabelOK";
			this.cmdLabelOK.Size = new System.Drawing.Size(65, 40);
			this.cmdLabelOK.TabIndex = 23;
			this.cmdLabelOK.Text = "OK";
			this.cmdLabelOK.Click += new System.EventHandler(this.cmdLabelOK_Click);
			// 
			// cmdLabelCancel
			// 
			this.cmdLabelCancel.AppearanceKey = "actionButton";
			this.cmdLabelCancel.Location = new System.Drawing.Point(115, 170);
			this.cmdLabelCancel.Name = "cmdLabelCancel";
			this.cmdLabelCancel.Size = new System.Drawing.Size(93, 40);
			this.cmdLabelCancel.TabIndex = 22;
			this.cmdLabelCancel.Text = "Cancel";
			this.cmdLabelCancel.Click += new System.EventHandler(this.cmdLabelCancel_Click);
			// 
			// cboLabelType
			// 
			this.cboLabelType.BackColor = System.Drawing.SystemColors.Window;
			this.cboLabelType.Location = new System.Drawing.Point(20, 30);
			this.cboLabelType.Name = "cboLabelType";
			this.cboLabelType.Size = new System.Drawing.Size(289, 40);
			this.cboLabelType.TabIndex = 21;
			this.cboLabelType.SelectedIndexChanged += new System.EventHandler(this.cboLabelType_SelectedIndexChanged);
			// 
			// lblLabelDescription
			// 
			this.lblLabelDescription.Location = new System.Drawing.Point(20, 80);
			this.lblLabelDescription.Name = "lblLabelDescription";
			this.lblLabelDescription.Size = new System.Drawing.Size(289, 80);
			this.lblLabelDescription.TabIndex = 26;
			// 
			// cmdClear
			// 
			this.cmdClear.AppearanceKey = "actionButton";
			this.cmdClear.Enabled = false;
			this.cmdClear.Location = new System.Drawing.Point(300, 654);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(108, 40);
			this.cmdClear.TabIndex = 18;
			this.cmdClear.Text = "Clear All";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fraPostage
			// 
			this.fraPostage.BackColor = System.Drawing.Color.White;
			this.fraPostage.Controls.Add(this.Command2);
			this.fraPostage.Controls.Add(this.Command1);
			this.fraPostage.Controls.Add(this.txtPostage);
			this.fraPostage.Controls.Add(this.Label1_1);
			this.fraPostage.Location = new System.Drawing.Point(253, 304);
			this.fraPostage.Name = "fraPostage";
			this.fraPostage.Size = new System.Drawing.Size(338, 175);
			this.fraPostage.TabIndex = 10;
			this.fraPostage.Text = "Postage Fee";
			this.fraPostage.Visible = false;
			// 
			// Command2
			// 
			this.Command2.AppearanceKey = "actionButton";
			this.Command2.Location = new System.Drawing.Point(113, 115);
			this.Command2.Name = "Command2";
			this.Command2.Size = new System.Drawing.Size(94, 40);
			this.Command2.TabIndex = 14;
			this.Command2.Text = "Cancel";
			this.Command2.Click += new System.EventHandler(this.Command2_Click);
			// 
			// Command1
			// 
			this.Command1.AppearanceKey = "actionButton";
			this.Command1.Location = new System.Drawing.Point(20, 115);
			this.Command1.Name = "Command1";
			this.Command1.Size = new System.Drawing.Size(63, 40);
			this.Command1.TabIndex = 13;
			this.Command1.Text = "OK";
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// txtPostage
			// 
			this.txtPostage.Location = new System.Drawing.Point(20, 65);
			this.txtPostage.MaxLength = 6;
			this.txtPostage.Name = "txtPostage";
			this.txtPostage.Size = new System.Drawing.Size(298, 22);
			this.txtPostage.TabIndex = 12;
			// 
			// Label1_1
			// 
			this.Label1_1.Location = new System.Drawing.Point(20, 30);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(298, 25);
			this.Label1_1.TabIndex = 11;
			this.Label1_1.Text = "PLEASE ENTER THE POSTAGE FEE, IF ANY, YOU WILL BE ADDING TO THE TOTAL REGISTRATIO" +
    "N COST";
			this.Label1_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cmdExtract
			// 
			this.cmdExtract.AppearanceKey = "actionButton";
			this.cmdExtract.Enabled = false;
			this.cmdExtract.Location = new System.Drawing.Point(30, 704);
			this.cmdExtract.Name = "cmdExtract";
			this.cmdExtract.Size = new System.Drawing.Size(149, 40);
			this.cmdExtract.TabIndex = 9;
			this.cmdExtract.Text = "Extract to File";
			this.cmdExtract.Click += new System.EventHandler(this.cmdExtract_Click);
			// 
			// cboYear
			// 
			this.cboYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboYear.Location = new System.Drawing.Point(769, 80);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(130, 40);
			this.cboYear.TabIndex = 6;
			// 
			// cboMonth
			// 
			this.cboMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboMonth.Location = new System.Drawing.Point(769, 30);
			this.cboMonth.Name = "cboMonth";
			this.cboMonth.Size = new System.Drawing.Size(130, 40);
			this.cboMonth.TabIndex = 5;
			// 
			// cmdPrintLabels
			// 
			this.cmdPrintLabels.AppearanceKey = "actionButton";
			this.cmdPrintLabels.Enabled = false;
			this.cmdPrintLabels.Location = new System.Drawing.Point(546, 704);
			this.cmdPrintLabels.Name = "cmdPrintLabels";
			this.cmdPrintLabels.Size = new System.Drawing.Size(133, 40);
			this.cmdPrintLabels.TabIndex = 4;
			this.cmdPrintLabels.Text = "Print Labels";
			this.cmdPrintLabels.Click += new System.EventHandler(this.cmdPrintLabels_Click);
			// 
			// cmdPrintReminders
			// 
			this.cmdPrintReminders.AppearanceKey = "actionButton";
			this.cmdPrintReminders.Enabled = false;
			this.cmdPrintReminders.Location = new System.Drawing.Point(349, 704);
			this.cmdPrintReminders.Name = "cmdPrintReminders";
			this.cmdPrintReminders.Size = new System.Drawing.Size(167, 40);
			this.cmdPrintReminders.TabIndex = 3;
			this.cmdPrintReminders.Text = "Print Reminders";
			this.cmdPrintReminders.Click += new System.EventHandler(this.cmdPrintReminders_Click);
			// 
			// cmdSelect
			// 
			this.cmdSelect.AppearanceKey = "actionButton";
			this.cmdSelect.Enabled = false;
			this.cmdSelect.Location = new System.Drawing.Point(155, 654);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(115, 40);
			this.cmdSelect.TabIndex = 2;
			this.cmdSelect.Text = "Select All";
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// cmdPrintList
			// 
			this.cmdPrintList.AppearanceKey = "actionButton";
			this.cmdPrintList.Enabled = false;
			this.cmdPrintList.Location = new System.Drawing.Point(209, 704);
			this.cmdPrintList.Name = "cmdPrintList";
			this.cmdPrintList.Size = new System.Drawing.Size(110, 40);
			this.cmdPrintList.TabIndex = 1;
			this.cmdPrintList.Text = "Print List";
			this.cmdPrintList.Click += new System.EventHandler(this.cmdPrintList_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Location = new System.Drawing.Point(30, 654);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(95, 40);
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// vsVehicles
			// 
			this.vsVehicles.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsVehicles.Cols = 16;
			this.vsVehicles.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsVehicles.ExtendLastCol = true;
			this.vsVehicles.FixedCols = 0;
			this.vsVehicles.Location = new System.Drawing.Point(30, 304);
			this.vsVehicles.Name = "vsVehicles";
			this.vsVehicles.RowHeadersVisible = false;
			this.vsVehicles.Rows = 17;
			this.vsVehicles.ShowFocusCell = false;
			this.vsVehicles.Size = new System.Drawing.Size(932, 311);
			this.vsVehicles.TabIndex = 19;
			this.vsVehicles.Visible = false;
			this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
			this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 768);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(123, 15);
			this.Label2.TabIndex = 39;
			this.Label2.Text = "REMINDER TYPE";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(645, 94);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(69, 15);
			this.lblYear.TabIndex = 8;
			this.lblYear.Text = "YEAR";
			this.lblYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblMonth
			// 
			this.lblMonth.Location = new System.Drawing.Point(645, 44);
			this.lblMonth.Name = "lblMonth";
			this.lblMonth.Size = new System.Drawing.Size(69, 15);
			this.lblMonth.TabIndex = 7;
			this.lblMonth.Text = "MONTH";
			this.lblMonth.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileEditTownInfo,
            this.mnuSep,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileEditTownInfo
			// 
			this.mnuFileEditTownInfo.Index = 0;
			this.mnuFileEditTownInfo.Name = "mnuFileEditTownInfo";
			this.mnuFileEditTownInfo.Text = "Edit Town Info";
			this.mnuFileEditTownInfo.Click += new System.EventHandler(this.mnuFileEditTownInfo_Click);
			// 
			// mnuSep
			// 
			this.mnuSep.Index = 1;
			this.mnuSep.Name = "mnuSep";
			this.mnuSep.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFileEditTownInfo
			// 
			this.cmdFileEditTownInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileEditTownInfo.Location = new System.Drawing.Point(946, 29);
			this.cmdFileEditTownInfo.Name = "cmdFileEditTownInfo";
			this.cmdFileEditTownInfo.Size = new System.Drawing.Size(104, 24);
			this.cmdFileEditTownInfo.Text = "Edit Town Info";
			this.cmdFileEditTownInfo.Click += new System.EventHandler(this.mnuFileEditTownInfo_Click);
			// 
			// frmReminder
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReminder";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reminder Notices";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmReminder_Load);
			this.Activated += new System.EventHandler(this.frmReminder_Activated);
			this.Resize += new System.EventHandler(this.frmReminder_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReminder_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).EndInit();
			this.fraTownInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdTownInfoCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTownInfoOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraEmailOptions)).EndInit();
			this.fraEmailOptions.ResumeLayout(false);
			this.fraEmailOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmailOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmailCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPersonalSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCompanySignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDisclaimerNotice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmailReminders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraLabelType)).EndInit();
			this.fraLabelType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdLabelOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLabelCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPostage)).EndInit();
			this.fraPostage.ResumeLayout(false);
			this.fraPostage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPostage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExtract)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabels)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintReminders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileEditTownInfo)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdFileEditTownInfo;
    }
}