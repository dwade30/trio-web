//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmExcisePaid.
	/// </summary>
	partial class frmExcisePaid
	{
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCTextBox txtExciseReceiptNumber;
		public Global.T2KBackFillDecimal txtExciseTaxPaid;
		public fecherFoundation.FCTextBox txtExciseTaxNumber;
		public fecherFoundation.FCTextBox txtExciseResCode;
		public Global.T2KDateBox txtExcisePaidDate;
		public Global.T2KBackFillDecimal txtExciseCreditAmount;
		public Global.T2KBackFillDecimal txtExciseSubTotal;
		public Global.T2KBackFillDecimal txtExciseTransferCharge;
		public Global.T2KBackFillDecimal txtExciseTotal;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblResState;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			//FC:FINAL:DDU:#2175 - hold disposing of form, we need just to hide and use it's controls data
			if (dontDispose)
			{
				return;
			}
			else
			{
				if (_InstancePtr == this)
				{
					_InstancePtr = null;
					Sys.ClearInstance(this);
				}
				if (disposing)
				{
					if (components != null)
					{
						components.Dispose();
					}
				}
				base.Dispose(disposing);
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdDone = new fecherFoundation.FCButton();
            this.txtExciseReceiptNumber = new fecherFoundation.FCTextBox();
            this.txtExciseTaxPaid = new Global.T2KBackFillDecimal();
            this.txtExciseTaxNumber = new fecherFoundation.FCTextBox();
            this.txtExciseResCode = new fecherFoundation.FCTextBox();
            this.txtExcisePaidDate = new Global.T2KDateBox();
            this.txtExciseCreditAmount = new Global.T2KBackFillDecimal();
            this.txtExciseSubTotal = new Global.T2KBackFillDecimal();
            this.txtExciseTransferCharge = new Global.T2KBackFillDecimal();
            this.txtExciseTotal = new Global.T2KBackFillDecimal();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblResState = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExcisePaidDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseCreditAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTransferCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdDone);
            this.BottomPanel.Location = new System.Drawing.Point(0, 540);
            this.BottomPanel.Size = new System.Drawing.Size(489, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtExciseReceiptNumber);
            this.ClientArea.Controls.Add(this.txtExciseTaxPaid);
            this.ClientArea.Controls.Add(this.txtExciseTaxNumber);
            this.ClientArea.Controls.Add(this.txtExciseResCode);
            this.ClientArea.Controls.Add(this.txtExcisePaidDate);
            this.ClientArea.Controls.Add(this.txtExciseCreditAmount);
            this.ClientArea.Controls.Add(this.txtExciseSubTotal);
            this.ClientArea.Controls.Add(this.txtExciseTransferCharge);
            this.ClientArea.Controls.Add(this.txtExciseTotal);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblResState);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Size = new System.Drawing.Size(489, 480);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(489, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(279, 30);
            this.HeaderText.Text = "Excise Tax Already Paid";
            // 
            // cmdDone
            // 
            this.cmdDone.AppearanceKey = "acceptButton";
            this.cmdDone.Location = new System.Drawing.Point(201, 30);
            this.cmdDone.Name = "cmdDone";
            this.cmdDone.Size = new System.Drawing.Size(80, 48);
            this.cmdDone.TabIndex = 17;
            this.cmdDone.Text = "Save";
            this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
            // 
            // txtExciseReceiptNumber
            // 
            this.txtExciseReceiptNumber.AutoSize = false;
            this.txtExciseReceiptNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtExciseReceiptNumber.LinkItem = null;
            this.txtExciseReceiptNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtExciseReceiptNumber.LinkTopic = null;
            this.txtExciseReceiptNumber.Location = new System.Drawing.Point(207, 280);
            this.txtExciseReceiptNumber.Name = "txtExciseReceiptNumber";
            this.txtExciseReceiptNumber.Size = new System.Drawing.Size(113, 40);
            this.txtExciseReceiptNumber.TabIndex = 5;
            this.txtExciseReceiptNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseReceiptNumber_KeyPress);
            // 
            // txtExciseTaxPaid
            // 
            this.txtExciseTaxPaid.Location = new System.Drawing.Point(207, 180);
            this.txtExciseTaxPaid.MaxLength = 8;
            this.txtExciseTaxPaid.Name = "txtExciseTaxPaid";
            this.txtExciseTaxPaid.Size = new System.Drawing.Size(113, 40);
            this.txtExciseTaxPaid.TabIndex = 3;
            this.txtExciseTaxPaid.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtExciseTaxPaid.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseTaxPaid_KeyPressEvent);
            this.txtExciseTaxPaid.Validating += new System.ComponentModel.CancelEventHandler(this.txtExciseTaxPaid_Validate);
            // 
            // txtExciseTaxNumber
            // 
            this.txtExciseTaxNumber.AutoSize = false;
            this.txtExciseTaxNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtExciseTaxNumber.LinkItem = null;
            this.txtExciseTaxNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtExciseTaxNumber.LinkTopic = null;
            this.txtExciseTaxNumber.Location = new System.Drawing.Point(207, 130);
            this.txtExciseTaxNumber.Name = "txtExciseTaxNumber";
            this.txtExciseTaxNumber.Size = new System.Drawing.Size(113, 40);
            this.txtExciseTaxNumber.TabIndex = 2;
            this.txtExciseTaxNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseTaxNumber_KeyPress);
            // 
            // txtExciseResCode
            // 
            this.txtExciseResCode.AutoSize = false;
            this.txtExciseResCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtExciseResCode.LinkItem = null;
            this.txtExciseResCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtExciseResCode.LinkTopic = null;
            this.txtExciseResCode.Location = new System.Drawing.Point(207, 80);
            this.txtExciseResCode.Name = "txtExciseResCode";
            this.txtExciseResCode.Size = new System.Drawing.Size(113, 40);
            this.txtExciseResCode.TabIndex = 1;
            this.txtExciseResCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseResCode_KeyPress);
            this.txtExciseResCode.Enter += new System.EventHandler(this.txtExciseResCode_Enter);
            this.txtExciseResCode.Leave += new System.EventHandler(this.txtExciseResCode_Leave);
            // 
            // txtExcisePaidDate
            // 
            this.txtExcisePaidDate.Location = new System.Drawing.Point(207, 30);
            this.txtExcisePaidDate.Mask = "##/##/####";
            this.txtExcisePaidDate.Name = "txtExcisePaidDate";
            this.txtExcisePaidDate.Size = new System.Drawing.Size(115, 40);
            this.txtExcisePaidDate.TabIndex = 0;
            this.txtExcisePaidDate.Text = "  /  /";
            this.txtExcisePaidDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExcisePaidDate_KeyPressEvent);
            // 
            // txtExciseCreditAmount
            // 
            this.txtExciseCreditAmount.Location = new System.Drawing.Point(207, 230);
            this.txtExciseCreditAmount.MaxLength = 8;
            this.txtExciseCreditAmount.Name = "txtExciseCreditAmount";
            this.txtExciseCreditAmount.Size = new System.Drawing.Size(113, 40);
            this.txtExciseCreditAmount.TabIndex = 4;
            this.txtExciseCreditAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtExciseCreditAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseCreditAmount_KeyPressEvent);
            this.txtExciseCreditAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtExciseCreditAmount_Validate);
            // 
            // txtExciseSubTotal
            // 
            this.txtExciseSubTotal.Location = new System.Drawing.Point(207, 330);
            this.txtExciseSubTotal.MaxLength = 8;
            this.txtExciseSubTotal.Name = "txtExciseSubTotal";
            this.txtExciseSubTotal.Size = new System.Drawing.Size(113, 40);
            this.txtExciseSubTotal.TabIndex = 6;
            this.txtExciseSubTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtExciseSubTotal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseSubTotal_KeyPressEvent);
            // 
            // txtExciseTransferCharge
            // 
            this.txtExciseTransferCharge.Location = new System.Drawing.Point(207, 380);
            this.txtExciseTransferCharge.MaxLength = 8;
            this.txtExciseTransferCharge.Name = "txtExciseTransferCharge";
            this.txtExciseTransferCharge.Size = new System.Drawing.Size(113, 40);
            this.txtExciseTransferCharge.TabIndex = 7;
            this.txtExciseTransferCharge.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtExciseTransferCharge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseTransferCharge_KeyPressEvent);
            this.txtExciseTransferCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtExciseTransferCharge_Validate);
            // 
            // txtExciseTotal
            // 
            this.txtExciseTotal.Location = new System.Drawing.Point(207, 430);
            this.txtExciseTotal.MaxLength = 8;
            this.txtExciseTotal.Name = "txtExciseTotal";
            this.txtExciseTotal.Size = new System.Drawing.Size(113, 40);
            this.txtExciseTotal.TabIndex = 8;
            this.txtExciseTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtExciseTotal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtExciseTotal_KeyPressEvent);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(140, 15);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "DATE EXCISE TAX PAID";
            // 
            // lblResState
            // 
            this.lblResState.Location = new System.Drawing.Point(340, 94);
            this.lblResState.Name = "lblResState";
            this.lblResState.Size = new System.Drawing.Size(121, 15);
            this.lblResState.TabIndex = 19;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 444);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(111, 15);
            this.Label9.TabIndex = 16;
            this.Label9.Text = "EXCISE TAX PAID";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(30, 394);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(120, 15);
            this.Label8.TabIndex = 15;
            this.Label8.Text = "TRANSFER CHARGE";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 344);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(70, 15);
            this.Label7.TabIndex = 14;
            this.Label7.Text = "SUBTOTAL";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 294);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(150, 15);
            this.Label6.TabIndex = 13;
            this.Label6.Text = "CREDIT RECEIPT NUMBER";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 244);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(111, 15);
            this.Label5.TabIndex = 12;
            this.Label5.Text = "CREDIT AMOUNT";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 194);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(111, 15);
            this.Label4.TabIndex = 11;
            this.Label4.Text = "AMOUNT OF TAX";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 144);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(140, 15);
            this.Label3.TabIndex = 10;
            this.Label3.Text = "TAX RECEIPT NUMBER";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(150, 15);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "RES. CODE WHERE PAID";
            // 
            // frmExcisePaid
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(489, 648);
            this.FillColor = 0;
            this.Name = "frmExcisePaid";
            this.Text = "Excise Tax Already Paid";
            this.Load += new System.EventHandler(this.frmExcisePaid_Load);
            this.Activated += new System.EventHandler(this.frmExcisePaid_Activated);
            this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExcisePaidDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseCreditAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTransferCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTotal)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}