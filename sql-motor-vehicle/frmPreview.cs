using fecherFoundation;
using Global;
using SharedApplication.Extensions;
using System;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Xml.Schema;
using SharedApplication.MotorVehicle.Commands;
using TWSharedLibrary;
using Wisej.Web;


namespace TWMV0000
{
    public partial class frmPreview : BaseForm
    {
        public frmPreview()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();

            this.Telemetry.TrackTiming("Test timing event",456);
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null ) _InstancePtr = this;
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmPreview InstancePtr
        {
            get { return (frmPreview)Sys.GetInstance(typeof(frmPreview)); }
        }

        protected frmPreview _InstancePtr = null;

        //=========================================================
        int NewBackColor = ColorTranslator.ToOle(Color.Yellow);
        string MVR3HoldNumber = "";
        clsDRWrapper rsLastMVR3 = new clsDRWrapper();
        bool ReasonMessageNeeded;
        bool PrintPriorityMessageNeeded;
        string rr;
        string RRText;
        string reason;
        int intLastMVR;
        clsDRWrapper rsReason = new clsDRWrapper();
        clsDRWrapper rsPP = new clsDRWrapper();
        string CurrentMVR3 = "";
        string PP1;
        string PP2;
        string PP3;
        int OldColor;
        bool blnTaxRecieptShown;
        string strMVR3Number = "";
        bool blnEndRegistration;
        clsPrintCodes oPrtCodes = new clsPrintCodes();
        private bool boolPreReg;
        bool blnLabelClicked = false;
        private bool blnMVR3Done = false;
        private bool blnCTADone = false;
        private bool blnUseTaxDone = false;
        int lngNoFeeDupRegMVR3Number;

        public bool IsPreReg
        {
            set { boolPreReg = value; }
            get
            {
                bool IsPreReg = false;
                IsPreReg = boolPreReg;

                return IsPreReg;
            }
        }

        public bool DisableReturnButton { get; set; } = false;
        public bool DisableUpdateReasonCodes { get; set; } = false;

        private void LoadReasonCodes()
        {
            if (MotorVehicle.Statics.RegistrationType == "DUPREG" && MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OpID") != "R/R" && MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("TransactionType") != "99" && (MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("ReasonCodes") != "" || MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PrintPriorities") != ""))
            {
                LoadReasonCodesForDup();
            }
            else
            {
                BuildReasonCode();
            }
        }

        private void InitializeForm()
        {
            if (Text == "MVR3 Preview")
            {
                Text = "MVR3 Preview  -  " + MotorVehicle.Statics.RegistrationType;
            }

            blnEndRegistration = false;

            if (MotorVehicle.Statics.RebuildingLine == true)
            {
                MotorVehicle.Statics.RebuildingLine = false;

                return;
            }

            if (OldColor == 0xFFFFFF) OldColor = ColorTranslator.ToOle(txtMessage.BackColor);


            LoadEverything();
            LoadReasonCodes();

            if (blnEndRegistration)
            {
                Close();

                return;
            }

            switch (MotorVehicle.Statics.RegistrationType)
            {
                case "LOST":
                    txtMessage.Enabled = false;
                    txtReasonsOnTheBottom.Enabled = false;

                    MotorVehicle.Statics.rsFinal.Set_Fields("TitleDone", false);
                    MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);
                    OldColor = ColorTranslator.ToOle(txtMVR3Input.BackColor);
                    frmWait.InstancePtr.Unload();

                    break;

                case "DUPREG":
                    {
                        txtMessage.Enabled = false;
                        txtReasonsOnTheBottom.Enabled = true;
                        txtReasonsOnTheBottom.ReadOnly = true;

                        if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO") == true)
                        {
                            MotorVehicle.Statics.ETO = true;
                        }

                        break;
                    }
            }

            int tempPrinter = StaticSettings.GlobalCommandDispatcher.Send(new GetPrinterGroup()).Result;
            MotorVehicle.Statics.strSql = "SELECT LastMVRNumber FROM LastMVRNumber WHERE PrinterNumber = " + FCConvert.ToString(tempPrinter);
            rsLastMVR3.OpenRecordset(MotorVehicle.Statics.strSql);

            if (rsLastMVR3.EndOfFile())
            {
                MotorVehicle.Statics.strSql = "SELECT TOP 1 * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' ORDER BY Number";
            }
            else
            {
                intLastMVR = FCConvert.ToInt32(rsLastMVR3.Get_Fields_Int32("LastMVRNumber"));
                MotorVehicle.Statics.strSql = "SELECT TOP 1 * FROM Inventory WHERE Code = 'MXS00' AND Number = " + FCConvert.ToString(intLastMVR);
                rsLastMVR3.OpenRecordset(MotorVehicle.Statics.strSql);

                if (rsLastMVR3.EndOfFile())
                {
                    MotorVehicle.Statics.strSql = "SELECT TOP 1 * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' ORDER BY Number";
                    intLastMVR = -1;
                }
                else
                {
                    MotorVehicle.Statics.strSql = "SELECT TOP 1 * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' and Number > " + FCConvert.ToString(intLastMVR) + " ORDER BY Number";
                }
            }

            rsLastMVR3.OpenRecordset(MotorVehicle.Statics.strSql);

            if (rsLastMVR3.EndOfFile() != true && rsLastMVR3.BeginningOfFile() != true)
            {
                var tempRS = new clsDRWrapper();

                try
                {
                    MVR3HoldNumber = FCConvert.ToString(rsLastMVR3.Get_Fields_Int32("Number"));
                    tempRS.OpenRecordset("SELECT * FROM WMV");

                    if (Conversion.Val(txtMVR3Input.Text) == 0)
                    {
                        if (MVR3HoldNumber.Length <= tempRS.Get_Fields_Int16("MVR3Digits"))
                        {
                            txtMVR3Input.Text = "";
                        }
                        else
                        {
                            txtMVR3Input.Text = Conversion.Val(tempRS.Get_Fields_Int16("MVR3Digits")) == 0
                                ? MVR3HoldNumber
                                : Strings.Mid(MVR3HoldNumber, 1, MVR3HoldNumber.Length - tempRS.Get_Fields_Int16("MVR3Digits"));
                        }
                    }

                    strMVR3Number = txtMVR3Input.Text;
                }
                finally
                {
                    tempRS.DisposeOf();
                }
            }

            MotorVehicle.Statics.RebuildingLine = false;

            txtMVR3Input.Focus();
            txtMVR3Input_GotFocus();

            if (txtMVR3Input.Text.Length > 0)
            {
                txtMVR3Input.SelectionStart = txtMVR3Input.Text.Length;
            }
        }

        private void frmPreview_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

            if (Shift == 0)
            {
                if (KeyCode == Keys.F10)
                {
                    KeyCode = (Keys)0;
                }
            }
        }

        private void frmPreview_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        public void LoadEverything()
        {

            if (!Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", 0);
            }

            // 
            if (MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (MotorVehicle.Statics.TempYear == "")
                {
                    MotorVehicle.Statics.TempYear = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
                }

                if (MotorVehicle.Statics.TempMonth == "")
                {
                    MotorVehicle.Statics.TempMonth = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "MM");
                }
            }
        }

        private void frmPreview_Load(object sender, System.EventArgs e)
        {
            modGNBas.GetWindowSize(this);
            modGlobalFunctions.SetTRIOColors(this, false);

            MotorVehicle.Statics.blnPrintingDupReg = false;
            MotorVehicle.Statics.gboolFromBatchRegistration = false;
            MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", "");

            rsReason.OpenRecordset("SELECT * FROM ReasonCodes");

            if (rsReason.EndOfFile() != true && rsReason.BeginningOfFile() != true)
            {
                rsReason.MoveLast();
                rsReason.MoveFirst();
            }
            else
            {
                MessageBox.Show("The Reason Code table is Empty! Contact System Support.", "No Table Data'", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            rsPP.OpenRecordset("SELECT * FROM PrintPriority");

            if (rsPP.EndOfFile() != true && rsPP.BeginningOfFile() != true)
            {
                rsPP.MoveLast();
                rsPP.MoveFirst();
            }
            else
            {
                MessageBox.Show("The Print Priority table is Empty! Contact System Support.", "No Table Data'", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            btnPrintCTA.Enabled = MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") && MotorVehicle.Statics.NeedToPrintCTA;

            btnPrintUseTax.Enabled = MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") && MotorVehicle.Statics.NeedToPrintUseTax;

            cmdSave.Enabled = false;

            if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "GVW" || MotorVehicle.Statics.RegistrationType == "CORR")
            {
                btnHoldRegistration.Enabled = false;
            }
            else
            {
                btnHoldRegistration.Enabled = !MotorVehicle.Statics.HoldRegistrationDollar;
            }

            if (DisableReturnButton)
            {
                cmdReturn.Enabled = false;
            }

            if (DisableUpdateReasonCodes)
            {
                cmdReasonCodes.Enabled = false;
            }

            InitializeForm();
            rptMVR3Laser rpt = new rptMVR3Laser();
            rpt.IsPreReg = IsPreReg;
            rpt.RunReport();
            arvMVR3Preview.ReportSource = rpt;
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            DialogResult ans = 0;
            clsDRWrapper ArchiveRecord = new clsDRWrapper();
            clsDRWrapper rsTitle = new clsDRWrapper();

            if (e.CloseReason == FCCloseReason.FormControlMenu)
            {
                if (!blnEndRegistration)
                {
                    ans = MessageBox.Show("Are you sure you want to end this registration?", "Quit Registration", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                else
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") && MotorVehicle.Statics.lngCTAAddNewID != 0)
                    {
                        rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));

                        if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                        {
                            if (Conversion.Val(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
                            {
                                MotorVehicle.Statics.rsDoubleTitle.OpenRecordset("SELECT * FROM DoubleCTA WHERE ID = " + rsTitle.Get_Fields_String("DoubleNumber"));

                                if (MotorVehicle.Statics.rsDoubleTitle.EndOfFile() != true && MotorVehicle.Statics.rsDoubleTitle.BeginningOfFile() != true)
                                {
                                    MotorVehicle.Statics.rsDoubleTitle.Delete();
                                    MotorVehicle.Statics.rsDoubleTitle.Update();
                                }
                            }

                            rsTitle.Delete();
                            rsTitle.Update();
                        }
                    }

                    MotorVehicle.Statics.rsFinal.Reset();
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Delete();
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord.Reset();
                    MotorVehicle.Statics.rsFinalCompare.Reset();
                    MotorVehicle.Statics.rsPlateForReg.Reset();
                    MotorVehicle.Statics.rsSecondPlate.Reset();
                    MotorVehicle.Statics.rsThirdPlate.Reset();
                    MotorVehicle.Statics.rsDoubleTitle.Reset();
                    frmDataInput.InstancePtr.Unload();
                }

                if (ans == DialogResult.Yes)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") && MotorVehicle.Statics.lngCTAAddNewID != 0)
                    {
                        rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));

                        if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                        {
                            if (Conversion.Val(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
                            {
                                MotorVehicle.Statics.rsDoubleTitle.OpenRecordset("SELECT * FROM DoubleCTA WHERE ID = " + rsTitle.Get_Fields_String("DoubleNumber"));

                                if (MotorVehicle.Statics.rsDoubleTitle.EndOfFile() != true && MotorVehicle.Statics.rsDoubleTitle.BeginningOfFile() != true)
                                {
                                    MotorVehicle.Statics.rsDoubleTitle.Delete();
                                    MotorVehicle.Statics.rsDoubleTitle.Update();
                                }
                            }

                            rsTitle.Delete();
                            rsTitle.Update();
                        }
                    }

                    MotorVehicle.Statics.rsFinal.Reset();
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Delete();
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord.Reset();
                    MotorVehicle.Statics.rsFinalCompare.Reset();
                    MotorVehicle.Statics.rsPlateForReg.Reset();
                    MotorVehicle.Statics.rsSecondPlate.Reset();
                    MotorVehicle.Statics.rsThirdPlate.Reset();
                    MotorVehicle.Statics.rsDoubleTitle.Reset();
                    frmDataInput.InstancePtr.Unload();
                }
                else
                {
                    e.Cancel = true;

                    return;
                }
            }

            rsLastMVR3.Reset();
            rsReason.Reset();
            MVR3HoldNumber = "";
            ReasonMessageNeeded = false;
            PrintPriorityMessageNeeded = false;
            rr = "";
            RRText = "";
            reason = "";
            PP1 = "";
            PP2 = "";
            PP3 = "";
            OldColor = 0;

            if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST")
            {
                frmDataInput.InstancePtr.Unload();
            }
        }

        private void cmdSave_Click(object sender, System.EventArgs e)
        {
            if (!MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("eto") && MotorVehicle.Statics.TownLevel != 9 && MotorVehicle.Statics.TownLevel != 6 && !IsPreReg)
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate").ToString().Trim() != "")
                {
                    if (!ValidateExciseDate(DateTime.Today.ToShortDateString(), MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate").ToString("MM/dd/yy")))
                    {
                        MessageBox.Show("The excise tax date cannot be greater than the validation or transaction date", "Invalid Excise Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        return;
                    }
                }
            }

            if ((PrintPriorityMessageNeeded || ReasonMessageNeeded) && txtMessage.Text.Trim() == "")
            {
	            MessageBox.Show("You must enter a message before you may continue.", "Enter Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
	            return;
            }

			if (MotorVehicle.Statics.RegistrationType == "DUPREG")
            {
                MessageBox.Show("Please be sure to fill out an MV-11 form.", "MV-11 Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (MotorVehicle.Statics.RegistrationType == "RRR" && !MotorVehicle.Statics.NewToTheSystem && (((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) != "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode2")) == "N") || (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode2")) != "N" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode2"))) != "")) || ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) != "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode3")) == "N") || (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode3")) != "N" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode2"))) != ""))))
            {
                if (MotorVehicle.Statics.DemographicInfo == true)
                {
                    MessageBox.Show("Please be sure to fill out an MV-138 form.", "MV-138 Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            if ((MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "RRT") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N")
            {
                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AQ" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "SR" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CV")
                {
                    MessageBox.Show("Please be sure to fill out an MV-65 form.", "MV-65 Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            if (Strings.InStr(1, txtReasonsOnTheBottom.Text, "RS", CompareConstants.vbBinaryCompare) > 0)
            {
                MessageBox.Show("Please be sure to fill out an MVT-22 form.", "MVT-22 Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            cmdSave.Enabled = false;
            this.Refresh();
            SaveRegistration();
        }

        private void MakeCode(string x)
        {
            if (x == "M")
            {
                var monthStickerMonth = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth");

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge") > 1)
                {
                    MotorVehicle.Statics.strCodeM = "SMD";

                    if (monthStickerMonth < 10)
                    {
                        MotorVehicle.Statics.strCodeM += "0" + fecherFoundation.Strings.Trim(Conversion.Str(monthStickerMonth));
                    }
                    else
                    {
                        MotorVehicle.Statics.strCodeM += fecherFoundation.Strings.Trim(Conversion.Str(monthStickerMonth));
                    }
                }
                else
                {
                    MotorVehicle.Statics.strCodeM = "SMS";

                    if (monthStickerMonth < 10)
                    {
                        MotorVehicle.Statics.strCodeM += "0" + fecherFoundation.Strings.Trim(Conversion.Str(monthStickerMonth));
                    }
                    else
                    {
                        MotorVehicle.Statics.strCodeM += fecherFoundation.Strings.Trim(Conversion.Str(monthStickerMonth));
                    }
                }
            }
            else
            {
                var expiredDateString = Strings.Right(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")), 2);

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge") > 1)
                {
                    MotorVehicle.Statics.strCodeY = "SYD" + expiredDateString;
                }
                else
                {
                    MotorVehicle.Statics.strCodeY = "SYS" + expiredDateString;
                }
            }
        }

        private void MakeCodeP()
        {
            clsDRWrapper rs = new clsDRWrapper();

            try
            {
                rs.OpenRecordset("SELECT * FROM Inventory WHERE (FormattedInventory) = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'");

                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    MotorVehicle.Statics.strCodeP = FCConvert.ToString(rs.Get_Fields("Code"));
                }
            }
            finally
            {
                rs.DisposeOf();
            }
        }

        private void SaveRegistration()
        {
            clsDRWrapper rsAM = new clsDRWrapper();
            int fnx;
            clsDRWrapper rsTemp = new clsDRWrapper();
            double TitleFee = 0;
            int intLockCount = 0;
            DialogResult intChoice = 0;
            int intRndCount = 0;
            int x;
            int LastMVR3 = 0;
            clsDRWrapper rsMVR3 = new clsDRWrapper();
            clsDRWrapper rsID = new clsDRWrapper();
            clsDRWrapper ArchiveRecord = new clsDRWrapper();
            clsDRWrapper rsTestFleetCTA = new clsDRWrapper();
            clsDRWrapper rsMVRecord = new clsDRWrapper();
            string strVin = "";
            clsDRWrapper rsUpdate = new clsDRWrapper();
            clsDRWrapper CheckRS = new clsDRWrapper();
            clsDRWrapper rsHeld = new clsDRWrapper();
            clsDRWrapper rsTitle = new clsDRWrapper();
            string tempString = "";
            clsDRWrapper rsFees = new clsDRWrapper();
            clsDRWrapper rsInventory = new clsDRWrapper();

            try
            {
                // On Error GoTo ShowLineNumber
                fecherFoundation.Information.Err().Clear();

                Debug.WriteLine("Start Save" + "\t" + FCConvert.ToString(DateTime.Now));
                blnLabelClicked = true;

                MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", DateTime.Now);

                if (MotorVehicle.Statics.blnChangeToFleet)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull"));
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditFull", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditUsed", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", 0);
                }

                var regType = MotorVehicle.Statics.RegistrationType;

                if (regType != "DUPREG")
                {
                    tempString = "";

                    for (x = 1; x <= (Information.UBound(MotorVehicle.Statics.gPPriority, 1)); x++)
                    {
                        if (x == 1)
                        {
                            tempString = MotorVehicle.Statics.gPPriNumber[x] + "," + MotorVehicle.Statics.gPPriority[x];
                        }
                        else
                        {
                            tempString += "|" + MotorVehicle.Statics.gPPriNumber[x] + "," + MotorVehicle.Statics.gPPriority[x];
                        }
                    }

                    if (fecherFoundation.Strings.Trim(tempString) != "")
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("PrintPriorities", tempString);
                    }

                    if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRTextAll) != "")
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("ReasonCodes", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRTextAll));
                    }
                }

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PreEffectiveDate.ToOADate() > DateTime.Today.ToOADate())
                {
                    rsAM.OpenRecordset("SELECT * FROM PendingActivityMaster WHERE ID = 0");
                }
                else
                {
                    rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
                }

                if (regType == "CORR" || regType == "GVW")
                {
                    MotorVehicle.Statics.rsECorrect.OpenRecordset("SELECT * FROM tblMasterTemp WHERE ID = 0");
                    MotorVehicle.Statics.rsECorrect.AddNew();

                    for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID")
                        {
                            MotorVehicle.Statics.rsECorrect.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                        }
                    }

                    clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
                    clsDRWrapper temp1 = MotorVehicle.Statics.rsFinalCompare;
                    clsDRWrapper temp2 = MotorVehicle.Statics.rsECorrect;
                    MotorVehicle.CompareRecordsForECorrect(ref temp, ref temp1, ref temp2);
                    MotorVehicle.Statics.rsFinal = temp;
                    MotorVehicle.Statics.rsFinalCompare = temp1;
                    MotorVehicle.Statics.rsECorrect = temp2;
                    string tempstr = "";
                EnterReason:;

                    if (regType == "CORR")
                    {
                        tempstr = Interaction.InputBox("Please enter a reason for this correction", "Reason Needed", null);

                        if (tempstr == "")
                        {
                            MessageBox.Show("You must enter a reason before you can save this registration.", "Invalid Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            goto EnterReason;
                        }

                        MotorVehicle.Statics.rsECorrect.Set_Fields("InfoMessage", tempstr);
                    }

                    rsUpdate.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
                    rsUpdate.AddNew();

                    for (fnx = 1; fnx <= MotorVehicle.Statics.rsECorrect.FieldsCount - 1; fnx++)
                    {
                        if (MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx) == "OldMVR3")
                        {
                            rsUpdate.Set_Fields(MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx), 0);
                            /*? 43 */
                        }
                        else if (MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx) == "TransactionType")
                        {
                            if (regType == "GVW")
                            {
                                rsUpdate.Set_Fields(MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx), "GVW");
                            }
                            else
                            {
                                rsUpdate.Set_Fields(MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx), "ECO");
                            }
                        }
                        else if (MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx) == "TellerCloseoutID")
                        {
                            rsUpdate.Set_Fields(MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx), 0);
                        }
                        else
                        {
                            rsUpdate.Set_Fields(MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsECorrect.Get_FieldsIndexValue(fnx));
                        }
                    }

                    rsUpdate.Update();

                    MotorVehicle.Statics.rsECorrect.CancelUpdate();
                    frmWait.InstancePtr.Show();
                    frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Saving Data";
                    frmWait.InstancePtr.Refresh();

                    goto DoneFillingTag;
                }

                frmWait.InstancePtr.Show();
                frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Saving Data";
                frmWait.InstancePtr.Refresh();

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    rsUpdate.OpenRecordset("SELECT * FROM PendingActivityMaster WHERE ID = 0");
                }
                else
                {
                    rsUpdate.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
                }

                rsUpdate.AddNew();

                for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
                {
	                string fieldName = MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx);
	                TypeCode fieldType = (TypeCode)MotorVehicle.Statics.rsFinal.Get_FieldsIndexDataType(fnx);
	                if (fieldName == "OldMVR3")
                    {
                        rsUpdate.Set_Fields(fieldName, 0);
                    }
                    else if (fieldName == "TellerCloseoutID")
                    {
                        rsUpdate.Set_Fields(fieldName, 0);
                    }
                    else if (fieldName.ToLower() == "masterid")
                    {
						// do nothing
                    }
                    else if (fieldName == "ID")
                    {
                        // do nothing
                    }
                    else if (fieldName == "AgentFee")
                    {
                        if (regType == "DUPREG")
                        {
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI" || MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "DV")
                            {
                                rsUpdate.Set_Fields(fieldName, 0);
                            }
                            else
                            {
                                rsFees.OpenRecordset("SELECT * FROM DefaultInfo");

                                if (rsFees.EndOfFile() != true && rsFees.BeginningOfFile() != true)
                                {
                                    rsUpdate.Set_Fields(fieldName, rsFees.Get_Fields_Decimal("DupRegAgentFee"));
                                }
                                else
                                {
                                    rsUpdate.Set_Fields(fieldName, MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                                }
                            }
                        }
                        else
                        {
                            rsUpdate.Set_Fields(fieldName, MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                        }
                    }
                    else
                    {
	                    if (fieldType == TypeCode.Int16 || fieldType == TypeCode.Int32 || fieldType == TypeCode.Int64)
	                    {
		                    if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx).ToString() != "")
		                    {
			                    rsUpdate.Set_Fields(fieldName, MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
		                    }
	                    }
						else
	                    {
		                    rsUpdate.Set_Fields(fieldName, MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
	                    }
					}
                }

                rsUpdate.Update();

                Debug.WriteLine("Saved ActivityMaster" + "\t" + FCConvert.ToString(DateTime.Now));
            DoneFillingTag:;
                fecherFoundation.Information.Err().Clear();

                if (regType == "DUPREG")
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
                }

                string tempOldMVR3 = "";

                if ((regType == "RRR" || regType == "RRT") && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3")) == 0)
                {
                    tempOldMVR3 = Interaction.InputBox("Please enter the old MVR3 number", "Old MVR3 Number Needed", null);

                    if (tempOldMVR3 == "" || !Information.IsNumeric(tempOldMVR3))
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", 0);
						MotorVehicle.Statics.rsFinal.Set_Fields("PriorTaxReceipt", 0);
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", tempOldMVR3);
						MotorVehicle.Statics.rsFinal.Set_Fields("PriorTaxReceipt", tempOldMVR3);
                    }
                }

                rsID.OpenRecordset("SELECT * FROM ActivityMaster WHERE MVR3 = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") + " AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "'");

                if (rsID.EndOfFile() != true && rsID.BeginningOfFile() != true)
                {
                    MotorVehicle.Statics.TransactionID = FCConvert.ToString(rsID.Get_Fields_Int32("ID"));
                }
                else
                {
                    MotorVehicle.Statics.TransactionID = "";
                }

                if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
                {
                    MotorVehicle.Write_PDS_Work_Record_Stuff();
                }

                LastMVR3 = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") == true)
                {
                    rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));

                    if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                    {
                        rsTitle.MoveLast();
                        rsTitle.MoveFirst();

                        if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                        {
                            rsUpdate.OpenRecordset("SELECT * FROM PendingTitleApplications WHERE ID = 0");
                        }
                        else
                        {
                            rsUpdate.OpenRecordset("SELECT * FROM TitleApplications WHERE ID = 0");
                        }

                        if (MotorVehicle.Statics.FleetCTA)
                        {
                            if (MotorVehicle.Statics.VehiclesCovered > 1)
                            {
                                rsTestFleetCTA.OpenRecordset("SELECT * FROM TitleApplications WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber") + "'");

                                if (rsTestFleetCTA.EndOfFile() != true && rsTestFleetCTA.BeginningOfFile() != true)
                                {
                                    goto SkipCTA;
                                }
                            }

                            TitleFee = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered);
                        }
                        else
                        {
                            TitleFee = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"));
                        }

                        rsUpdate.AddNew();
                        rsUpdate.Set_Fields("CTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber").ToUpper());
                        rsUpdate.Set_Fields("Fee", TitleFee);
                        rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                        rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        rsUpdate.Set_Fields("RegistrationDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                        rsUpdate.Set_Fields("CustomerName", rsTitle.Get_Fields_String("Name1"));
                        rsUpdate.Set_Fields("OPID", MotorVehicle.Statics.UserID);
                        rsUpdate.Set_Fields("DoubleIfApplicable", rsTitle.Get_Fields_String("DoubleNumber"));
                        rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                        rsUpdate.Set_Fields("MSRP", rsTitle.Get_Fields_String("MSRP"));
                        rsUpdate.Update();
                    }

                    if (Conversion.Val(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
                    {
                        MotorVehicle.Statics.rsDoubleTitle.Update();
                    }
                }

                Debug.WriteLine("Saved Title" + "\t" + FCConvert.ToString(DateTime.Now));
            SkipCTA:

                if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber")) != "" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"))) != fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("GiftCertificateNumber")))
                {
                    rsUpdate.OpenRecordset("SELECT * FROM GiftCertificateRegister WHERE ID = 0");
                    rsUpdate.AddNew();
                    rsUpdate.Set_Fields("DateRedeemed", DateTime.Today);
                    rsUpdate.Set_Fields("CertificateNumber", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"))));
                    rsUpdate.Set_Fields("Name", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"))));
                    rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                    rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("Amount", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GiftCertificateAmount"));
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.OpID);
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Set_Fields("GiftCertOrVoucher", MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher"));
                    rsUpdate.Set_Fields("RegistrationID", MotorVehicle.Statics.TransactionID);
                    rsUpdate.Update();
                }

                Debug.WriteLine("Saved Gift Cert" + "\t" + FCConvert.ToString(DateTime.Now));

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO") == true)
                {
                    rsUpdate.OpenRecordset("SELECT * FROM ExciseTax WHERE ID = 0");
                    rsUpdate.AddNew();
                    rsUpdate.Set_Fields("MVR3Number", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                    rsUpdate.Set_Fields("DateofTransaction", DateTime.Today);
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Update();
                }

                Debug.WriteLine("Saved Title" + "\t" + FCConvert.ToString(DateTime.Now));

                rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    if (fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxDate", Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yyyy"));
                }

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
                {
                    if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
                    {
                        MakeCode("M");
                    }

                    rsUpdate.AddNew();

                    if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "P");
                        rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                    }
                    else
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "I");
                        rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    }

                    rsUpdate.Set_Fields("QuantityAdjusted", 1);
                    rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeM);
                    rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
                    rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Set_Fields("TellerCloseoutID", 0);
                    rsUpdate.Update();
                }

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") != 0)
                {
                    int tempPrinter = StaticSettings.GlobalCommandDispatcher.Send(new GetPrinterGroup()).Result;
                    rsMVR3.OpenRecordset("SELECT * FROM LastMVRNumber WHERE PrinterNumber = " + FCConvert.ToString(tempPrinter));

                    if (rsMVR3.EndOfFile() != true && rsMVR3.BeginningOfFile() != true)
                    {
                        rsMVR3.Edit();

                        if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") == true && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == true)
                        {
                            rsMVR3.Set_Fields("LastMVRNumber", lngNoFeeDupRegMVR3Number);
                        }
                        else
                        {
                            rsMVR3.Set_Fields("LastMVRNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        }
                    }
                    else
                    {
                        rsMVR3.AddNew();
                        rsMVR3.Set_Fields("PrinterNumber", tempPrinter);

                        if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") == true && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == true)
                        {
                            rsMVR3.Set_Fields("LastMVRNumber", lngNoFeeDupRegMVR3Number);
                        }
                        else
                        {
                            rsMVR3.Set_Fields("LastMVRNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        }
                    }

                    rsMVR3.Update();
                    rsUpdate.AddNew();

                    if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "P");
                        rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                    }
                    else
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "I");
                        rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    }

                    rsUpdate.Set_Fields("QuantityAdjusted", 1);
                    rsUpdate.Set_Fields("InventoryType", "MXS00");
                    rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Set_Fields("TellerCloseoutID", 0);
                    rsUpdate.Update();

                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") == true && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == true && regType != "DUPREG")
					{
                        rsUpdate.AddNew();

                        if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                        {
                            rsUpdate.Set_Fields("AdjustmentCode", "P");
                            rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                        }
                        else
                        {
                            rsUpdate.Set_Fields("AdjustmentCode", "I");
                            rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                        }

                        rsUpdate.Set_Fields("QuantityAdjusted", 1);
                        rsUpdate.Set_Fields("InventoryType", "MXS00");
                        rsUpdate.Set_Fields("Low", lngNoFeeDupRegMVR3Number);
                        rsUpdate.Set_Fields("High", lngNoFeeDupRegMVR3Number);
                        rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                        rsUpdate.Set_Fields("Reason", "No Fee Dup Reg Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + FCConvert.ToString(lngNoFeeDupRegMVR3Number));
                        rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                        rsUpdate.Set_Fields("TellerCloseoutID", 0);
                        rsUpdate.Update();
                    }
                }

                if ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
                {
                    if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
                    {
                        if (MotorVehicle.Statics.Class == "IU")
                        {
                            MotorVehicle.Statics.strCodeY = "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
                        }
                        else
                        {
                            MakeCode("Y");
                        }
                    }

                    rsUpdate.AddNew();

                    if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "P");
                        rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                    }
                    else
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "I");
                        rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    }

                    rsUpdate.Set_Fields("QuantityAdjusted", 1);
                    rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeY);

                    if (MotorVehicle.Statics.Class == "IU")
                    {
                        rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    }
                    else
                    {
                        rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                        rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                    }

                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Set_Fields("TellerCloseoutID", 0);
                    rsUpdate.Update();
                }

                if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" || MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "R") && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "N" && regType != "DUPREG" && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != "NEW" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "IU")
                {
                    if (MotorVehicle.Statics.strCodeP == "")
                    {
                        MakeCodeP();
                    }

                    rsUpdate.AddNew();

                    if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "P");
                        rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                    }
                    else
                    {
                        rsUpdate.Set_Fields("AdjustmentCode", "I");
                        rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    }

                    rsUpdate.Set_Fields("QuantityAdjusted", 1);
                    rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
                    rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Set_Fields("TellerCloseoutID", 0);
                    rsUpdate.Update();
                }

                Debug.WriteLine("Saved Inv Adj" + "\t" + FCConvert.ToString(DateTime.Now));

                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "T")
                {
                    string st1 = "";
                    rsUpdate.OpenRecordset("SELECT * FROM TemporaryPlateRegister WHERE ID = 0");
                    st1 = fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), true));
                    rsUpdate.AddNew();
                    rsUpdate.Set_Fields("TemporaryPlateNumber", MotorVehicle.Statics.TempPlateNumber);
                    rsUpdate.Set_Fields("OwnerCode1", MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1"));
                    rsUpdate.Set_Fields("Owner1", st1);
                    rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                    rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Update();
                }

                Debug.WriteLine("Saved Temp Plate" + "\t" + FCConvert.ToString(DateTime.Now));

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
                    {
                        if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
                        {
                            MakeCode("M");
                        }

                        rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeM + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();
                            rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                            }

                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "P");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
                    {
                        if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
                        {
                            MakeCode("M");
                        }

                        rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeM + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "I");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") != 0)
                    {
                        rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();
                            rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                            }

                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "P");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") != 0)
                    {
                        rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                            }

                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "I");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") == true && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == true)
                    {
                        rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + FCConvert.ToString(lngNoFeeDupRegMVR3Number));

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                            rsInventory.Set_Fields("MVR3", lngNoFeeDupRegMVR3Number);
                            rsInventory.Set_Fields("Status", "P");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") == true && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == true)
                    {
                        rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + FCConvert.ToString(lngNoFeeDupRegMVR3Number));

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                            rsInventory.Set_Fields("MVR3", lngNoFeeDupRegMVR3Number);
                            rsInventory.Set_Fields("Status", "I");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    if ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
                    {
                        if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
                        {
                            if (MotorVehicle.Statics.Class == "IU")
                            {
                                MotorVehicle.Statics.strCodeY = "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
                            }
                            else
                            {
                                MakeCode("Y");
                            }
                        }

                        if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU")
                        {
                            rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
                        }
                        else
                        {
                            rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                        }

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                            }

                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "P");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }
                else
                {
                    if ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
                    {
                        if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
                        {
                            if (MotorVehicle.Statics.Class == "IU")
                            {
                                MotorVehicle.Statics.strCodeY =
                                    "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
                            }
                            else
                            {
                                MakeCode("Y");
                            }
                        }

                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "IU")
                        {
                            rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
                        }
                        else
                        {
                            rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                        }

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                            }

                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "I");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" || MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "R") && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "N" && regType != "DUPREG" && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != "NEW" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "IU")
                    {
                        MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
                        rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                            }

                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "P");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }
                else
                {
                    if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" || MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "R") && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "N" && regType != "DUPREG" && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != "NEW" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "IU")
                    {
                        MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
                        rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);

                        if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                        {
                            rsInventory.Edit();

                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                            {
                                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                            }

                            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                            }

                            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                            rsInventory.Set_Fields("Status", "I");
                            rsInventory.Set_Fields("PeriodCloseoutID", 0);
                            rsInventory.Set_Fields("TellerCloseoutID", 0);
                            rsInventory.Update();
                        }
                    }
                }

                if (MotorVehicle.Statics.ForcedPlate == "T")
                {
                    rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE SUBSTRING(Code,4,5) = 'TM' AND Number = " + FCConvert.ToString(MotorVehicle.Statics.TempPlateNumber));

                    if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                    {
                        rsInventory.Edit();

                        if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                        {
                            rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                        }

                        rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text))
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                        }

                        rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        rsInventory.Set_Fields("Status", "I");
                        rsInventory.Set_Fields("PeriodCloseoutID", 0);
                        rsInventory.Set_Fields("TellerCloseoutID", 0);
                        rsInventory.Update();
                    }
                }

                Debug.WriteLine("Saved Inventory" + "\t" + FCConvert.ToString(DateTime.Now));

                CheckRS.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));

                if (CheckRS.EndOfFile() != true && CheckRS.BeginningOfFile() != true)
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")) != "")
                    {
                        rsHeld.OpenRecordset("SELECT * FROM Master WHERE Plate = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate") + "'");

                        if (rsHeld.EndOfFile() != true && rsHeld.BeginningOfFile() != true)
                        {
                            rsHeld.Edit();
                        }
                        else
                        {
                            rsHeld.AddNew();
                        }
                    }
                    else
                    {
                        rsHeld.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
                        rsHeld.AddNew();
                    }

                    for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID")
                        {
                            rsHeld.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                        }
                    }

                    rsHeld.Update();
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3"));
						ArchiveRecord.Set_Fields("PriorTaxReceipt", MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3"));

                        if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") != Conversion.Val(fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text)) && fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text) != "")
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text));
                        }

                        ArchiveRecord.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID2));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", ArchiveRecord.Get_Fields_Int32("MVR3"));
						ArchiveRecord.Set_Fields("PriorTaxReceipt", ArchiveRecord.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID3));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", ArchiveRecord.Get_Fields_Int32("MVR3"));
						ArchiveRecord.Set_Fields("PriorTaxReceipt", ArchiveRecord.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord = null;
                    ArchiveRecord = new clsDRWrapper();
                    ArchiveRecord.Execute("DELETE FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"), "TWMV0000.vb1");
                }
                else
                {
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
						ArchiveRecord.Set_Fields("PriorTaxReceipt", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));

                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != Conversion.Val(fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text)))
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", FCConvert.ToString(fecherFoundation.Strings.Trim(frmPreview.InstancePtr.txtMVR3Input.Text)));
                        }

                        ArchiveRecord.Set_Fields("MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID2));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
						ArchiveRecord.Set_Fields("PriorTaxReceipt", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Set_Fields("MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID3));

                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Set_Fields("PriorTaxReceipt", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
						ArchiveRecord.Set_Fields("MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Update();
                    }

                    ArchiveRecord.Reset();

                    if (MotorVehicle.Statics.RegisteringFleet)
                    {
                        var statePaidFinal = MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid");

                        var localPaidFinal = MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid");

                        if (regType == "NRR")
                        {
                            MotorVehicle.Statics.VehicleInformation[0, MotorVehicle.Statics.NumberOfVehicles] = "Registered";
                            MotorVehicle.Statics.VehicleInformation[1, MotorVehicle.Statics.NumberOfVehicles] = true;
                            MotorVehicle.Statics.VehicleInformation[2, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");
                            MotorVehicle.Statics.VehicleInformation[3, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("plate");
                            MotorVehicle.Statics.VehicleInformation[4, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
                            MotorVehicle.Statics.VehicleInformation[5, MotorVehicle.Statics.NumberOfVehicles] = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("Year")));
                            MotorVehicle.Statics.VehicleInformation[6, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
                            MotorVehicle.Statics.VehicleInformation[7, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");

                            if (MotorVehicle.Statics.GroupOrFleet == "G")
                            {
                                MotorVehicle.Statics.VehicleInformation[8, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate");
                            }
                            else
                            {
                                var unitNumberFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber");

                                if (fecherFoundation.Strings.Trim(unitNumberFinal) != "")
                                {
                                    MotorVehicle.Statics.VehicleInformation[8, MotorVehicle.Statics.NumberOfVehicles] = unitNumberFinal;
                                }
                            }

                            MotorVehicle.Statics.VehicleInformation[9, MotorVehicle.Statics.NumberOfVehicles] = statePaidFinal + localPaidFinal;
                        }
                        else
                        {
                            MotorVehicle.Statics.VehicleInformation[0, MotorVehicle.Statics.VehicleBeingRegistered] = "Registered";
                            MotorVehicle.Statics.VehicleInformation[9, MotorVehicle.Statics.VehicleBeingRegistered] = statePaidFinal + localPaidFinal;
                        }

                        MotorVehicle.Statics.VehiclesRegistered[MotorVehicle.Statics.VehicleBeingRegistered] = true;
                    }

                    strVin = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
                    MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
                    MotorVehicle.Statics.rsFinal.Update();
                }

                Debug.WriteLine("Saved Master" + "\t" + FCConvert.ToString(DateTime.Now));
                modRegistry.SaveRegistryKey("LastMVR3Used", FCConvert.ToString(LastMVR3));

                if (MotorVehicle.Statics.RecordIDToTerminate != 0)
                {
                    var recordIdToTerminate = FCConvert.ToString(MotorVehicle.Statics.RecordIDToTerminate);

                    if (regType == "RRT" && (MotorVehicle.Statics.PlateType == 2 || MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4))
                    {
                        rsTemp.Execute("DELETE FROM Master WHERE ID = " + recordIdToTerminate, "TWMV0000.vb1");
                    }
                    else
                    {
                        rsTemp.OpenRecordset("SELECT * FROM Master WHERE ID = " + recordIdToTerminate);

                        if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                        {
                            rsTemp.Edit();
                            rsTemp.Set_Fields("Status", "T");
                            rsTemp.Update();
                            rsTemp.Reset();
                        }
                    }
                }

                if ((regType == "NRR" || regType == "NRT") && MotorVehicle.Statics.blnMakeVehicleInactive)
                {
                    rsMVRecord.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + strVin + "'");

                    if (rsMVRecord.EndOfFile() != true && rsMVRecord.BeginningOfFile() != true)
                    {
                        rsMVRecord.Edit();
                        rsMVRecord.Set_Fields("Inactive", true);
                        rsMVRecord.Update();
                    }
                }

                MotorVehicle.Statics.blnMakeVehicleInactive = FCConvert.ToBoolean(0);
                Debug.WriteLine("Start Reset Variables" + "\t" + FCConvert.ToString(DateTime.Now));
                MotorVehicle.Statics.rsDoubleTitle.Reset();
                MotorVehicle.Statics.rsFinalCompare.Reset();

                MotorVehicle.Statics.MVR3saved = fecherFoundation.Information.Err().Number == 0;

                MotorVehicle.ClearAllVariables();
                frmWait.InstancePtr.Unload();
                frmDataInput.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();

                if (MotorVehicle.Statics.bolFromDosTrio || (MotorVehicle.Statics.bolFromWindowsCR || MotorVehicle.Statics.RegisteringFleet))
                {
                    if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
                    {
                        MDIParent.InstancePtr.Menu18();
                        Close();
                        App.MainForm.EndWaitMVModule();

                        return;
                    }
                    else
                    {
                        if (!MotorVehicle.Statics.RegisteringFleet)
                        { }
                    }
                }
                else
                {
                    frmPlateInfo.InstancePtr.ShowForm();
                }

                blnLabelClicked = false;
                Debug.WriteLine("End Save" + "\t" + FCConvert.ToString(DateTime.Now));

                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "An error occurred on line " + fecherFoundation.Information.Erl(), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                switch (fecherFoundation.Information.Err(ex).Number)
                {
                    case 3260:
                        {
                            intLockCount += 1;

                            if (intLockCount > 5)
                            {
                                intChoice = MessageBox.Show("Error Trying to Save.  This record is currently locked by another user.  Would you like to try to save again?", "Record Locked", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);

                                if (intChoice == DialogResult.Retry)
                                {
                                    intLockCount = 1;
                                }
                            }

                            intRndCount = (FCConvert.ToInt32(Math.Pow(intLockCount, 2)) * 4000);

                            for (x = 1; x <= intRndCount; x++)
                            { }

                            break;
                        }

                    default:
                        {
                            MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "A file called SaveErr.txt has been created in the " + Environment.CurrentDirectory + " directory of your server.  Please notify TRIO of your problem and E-mail the SaveErr.txt file so we may more quickly find the problem you are encountering.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                            break;
                        }
                }

                MotorVehicle.ClearAllVariables();
                frmDataInput.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();
                MotorVehicle.Statics.rsFinal = null;
                MotorVehicle.Statics.rsFinalCompare = null;

                if (!MotorVehicle.Statics.bolFromDosTrio && !MotorVehicle.Statics.bolFromWindowsCR)
                {
                    frmPlateInfo.InstancePtr.ShowForm();
                }
            }
            finally
            {
                rsAM.DisposeOf();
                rsTemp.DisposeOf();
                rsMVR3.DisposeOf();
                rsID.DisposeOf();
                ArchiveRecord.DisposeOf();
                rsTestFleetCTA.DisposeOf();
                rsMVRecord.DisposeOf();
                rsUpdate.DisposeOf();
                CheckRS.DisposeOf();
                rsHeld.DisposeOf();
                rsTitle.DisposeOf();
                rsFees.DisposeOf();
                rsInventory.DisposeOf();
            }
        }

        public void BuildReasonCode()
        {
            var rsTemp = new clsDRWrapper();
            double dblInitFee = 0;
            var pCont = new cPartyController();
            cParty pInfo = null;
            bool blnUnderAge;
            reason = "";

            var classCodeFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");
            var classCode = FCConvert.ToString(classCodeFinal);

            if (classCode == "VT" || classCode == "DS")
            {
                var battleFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Battle");
                var battle = fecherFoundation.Strings.Trim(FCConvert.ToString(battleFinal));

                if (battle != "" && battle != "X")
                {
                    reason += oPrtCodes.VeteranDecal;
                }
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RENTAL"))
            {
                reason += oPrtCodes.RentalVehicle;
            }

            var ownerCode2Final = MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2");

            if (MotorVehicle.Statics.DemographicInfo)
            {
                if (!MotorVehicle.Statics.NewToTheSystem)
                {
                    const string NEWLINE = "\r\n";

                    var addressCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Address");

                    var addressFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Address");

                    var isAugusta = fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AUGUSTA";

                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(addressFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(addressCompare)))
                    {
                        if (!MotorVehicle.Statics.blnSuspended)
                        {
                            if (isAugusta)
                            {
                                MessageBox.Show("Address Change" + NEWLINE + NEWLINE + "Old Address - " + addressCompare + NEWLINE + "New Address - " + addressFinal, "Address Change", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }

                            reason += oPrtCodes.AddressChange;
                        }
                    }
                    else
                    {
                        var address2Final = MotorVehicle.Statics.rsFinal.Get_Fields_String("Address2");

                        var address2Compare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Address2");

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(address2Final)) != fecherFoundation.Strings.Trim(FCConvert.ToString(address2Compare)))
                        {
                            if (!MotorVehicle.Statics.blnSuspended)
                            {
                                if (isAugusta)
                                {
                                    MessageBox.Show("Address 2 Change" + NEWLINE + NEWLINE + "Old Address 2 - " + address2Compare + NEWLINE + "New Address 2 - " + address2Final, "Address Change", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }

                                reason += oPrtCodes.AddressChange;
                            }
                        }
                        else
                        {
                            var cityFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("City");

                            var cityCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("City");

                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(cityFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(cityCompare)))
                            {
                                if (!MotorVehicle.Statics.blnSuspended)
                                {
                                    if (isAugusta)
                                    {
                                        MessageBox.Show($"City Change{NEWLINE}{NEWLINE}Old City - {cityCompare}{NEWLINE}New City - {cityFinal}", "Address Change", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }

                                    reason += oPrtCodes.AddressChange;
                                }
                            }
                            else
                            {
                                var stateFinal = MotorVehicle.Statics.rsFinal.Get_Fields("State");

                                var stateCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields("State");

                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(stateFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(stateCompare)))
                                {
                                    if (!MotorVehicle.Statics.blnSuspended)
                                    {
                                        if (isAugusta)
                                        {
                                            MessageBox.Show($"State Change{NEWLINE}{NEWLINE}Old State - " + stateCompare + NEWLINE + "New State - " + stateFinal, "Address Change", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        }

                                        reason += oPrtCodes.AddressChange;
                                    }
                                }
                                else
                                {
                                    var zipFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip");

                                    var zipCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Zip");

                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(zipFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(zipCompare)))
                                    {
                                        if (!MotorVehicle.Statics.blnSuspended)
                                        {
                                            if (isAugusta)
                                            {
                                                MessageBox.Show($"5 Digit Zip Change{NEWLINE}{NEWLINE}Old Zip - {zipCompare}{NEWLINE}New Zip - {zipFinal}", "Address Change", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            }

                                            reason += oPrtCodes.AddressChange;
                                        }
                                    }
                                    else
                                    {
                                        var countryFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Country");

                                        var countryCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Country");

                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(countryFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(countryCompare)))
                                        {
                                            if (isAugusta)
                                            {
                                                MessageBox.Show($"Country Change{NEWLINE}{NEWLINE}Old Country - {countryCompare}{NEWLINE}New Country - {countryFinal}", "Address Change", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            }

                                            reason += oPrtCodes.AddressChange;
                                        }

                                    }
                                }
                            }
                        }
                    }


                    var residenceAddressCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("ResidenceAddress");

                    var residenceAddressFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceAddress");

                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(residenceAddressFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(residenceAddressCompare)))
                    {
                        reason += oPrtCodes.AddressChange;
                    }
                    else
                    {
                        var cityFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence");

                        var cityCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Residence");

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(cityFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(cityCompare)))
                        {
                            reason += oPrtCodes.AddressChange;
                        }
                        else
                        {
                            var stateFinal = MotorVehicle.Statics.rsFinal.Get_Fields("ResidenceState");

                            var stateCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields("ResidenceState");

                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(stateFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(stateCompare)))
                            {
                               reason += oPrtCodes.AddressChange;
                            }
                            else
                            {
                                var zipFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");

                                var zipCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("ResidenceCode");

                                if (fecherFoundation.Strings.Trim(FCConvert.ToString(zipFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(zipCompare)))
                                {
                                    reason += oPrtCodes.AddressChange;
                                }
                                else
                                {
                                    var countryFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCountry");

                                    var countryCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("ResidenceCountry");

                                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(countryFinal)) != fecherFoundation.Strings.Trim(FCConvert.ToString(countryCompare)))
                                    {
                                        reason += oPrtCodes.AddressChange;
                                    }
                                }
                            }
                        }
                        
                    }

                    Owner1NameChange(pCont);
                    Owner2NameChange(pCont);
                    Owner3NameChange(pCont);
                }

                if ((MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
                        && MotorVehicle.Statics.blnAllowExciseRebates
                        && MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") < 0)
                {
                    reason += oPrtCodes.ExciseTaxRebate;
                }
            }

            var subClassFinal = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
            var subClassCompare = FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Subclass"));

            if (MotorVehicle.Statics.AddressChange && MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType == 1)
            {
                reason += oPrtCodes.AddressChange;
            }

            if (MotorVehicle.Statics.VehicleInfo == true)
            {
                if (MotorVehicle.Statics.RegistrationType != "NRT" && MotorVehicle.Statics.RegistrationType != "RRT")
                {
                    var color1Final = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color1")));
                    var color1Compare = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("color1")));
                    var color2Final = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2")));
                    var color2Compare = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("color2")));

                    if (color1Final != color1Compare || (color2Final != color2Compare && color2Compare != "")) reason += "RC";
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"))) != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Style")))) reason += "RH";
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make"))) != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("make")))) reason += "RM";
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("model"))) != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("model")))) reason += "RJ";
                    if (MotorVehicle.Statics.rsFinal.Get_Fields("Year") != MotorVehicle.Statics.rsFinalCompare.Get_Fields("Year")) reason += "RY";

                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"))) != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Vin"))))
                    {
                        reason += oPrtCodes.VINCorrection;
                    }
				}

				var tempClass = classCodeFinal;

				var registeredWeightNewFinal = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("registeredWeightNew"));

				var registeredWeightNewCompare = Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("registeredWeightNew"));

				switch (tempClass)
				{
					case "BH":
					case "VT":
					case "LB":
					case "BB":
					case "FD":
					case "DS":
					case "WB":
					case "TS":
					case "SW":
					case "BC":
					case "AW":
					case "CV":
					case "MO":
					case "PH":
					case "PO":
					case "PS":
					case "AG":
					case "CR":
					case "CD":
					case "EM":
						{
							if (registeredWeightNewFinal != registeredWeightNewCompare)
							{
								reason += oPrtCodes.RVWChange;
							}

							break;
						}

					default:
						{

							switch (subClassFinal)
							{
								case "L3":
								case "L4":
								    {

										if (subClassCompare == "L1" || subClassCompare == "L2") reason += oPrtCodes.RVWChange;

										break;
									}

								default:
									{
										if (registeredWeightNewFinal != registeredWeightNewCompare)
										{
											reason += oPrtCodes.RVWChange;
										}

										break;
									}
							}

							break;
						}
				}
			}

            object datechecker = null;

            if (MotorVehicle.Statics.DateInfo == true)
            {
                if (MotorVehicle.IsProratedMotorcycleExpirationDate(classCodeFinal, MotorVehicle.Statics.datMotorCycleEffective, MotorVehicle.Statics.datMotorCycleExpires, MotorVehicle.Statics.RegistrationType))
                {
                    reason += oPrtCodes.NewExpireDate;
                }
                else
                {
                    if (Check2Year() == false && MotorVehicle.Statics.ETO == false)
                    {
                        datechecker = Strings.Format(MotorVehicle.Statics.CompareDate, "MM/dd/yy");

                        if (Information.IsDate(datechecker))
                        {
                            if (FCConvert.ToDouble(Strings.Left(FCConvert.ToString(datechecker), 1)) == 0)
                            {
                                datechecker = Strings.Right(FCConvert.ToString(datechecker), FCConvert.ToString(datechecker).Length - 1);
                            }

                            if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR")
                            {
                                if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() != MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate())
                                {
                                    reason += oPrtCodes.NewExpireDate;
                                }
                            }
                            else
                            {
                                if (fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"))).ToOADate() != fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(datechecker)).ToOADate() || MotorVehicle.Statics.gblnDelayedReg)
                                {
                                    reason += oPrtCodes.NewExpireDate;
                                }
                            }
                        }
                    }
                }
            }

            switch (classCode)
            {
                case "IU":
                    reason += oPrtCodes.IslandUseOnly;

                    break;
                case "TX":
                    reason += oPrtCodes.ForHire;

                    break;
                case "MO":
                    reason += oPrtCodes.MedalOfHonor;

                    break;
                case "CV":
                    reason += oPrtCodes.ModifiedVehicle;

                    break;
                case "GS":
                    reason += oPrtCodes.GoldStarFamily;

                    break;

                case "CO":
                case "CC":
                case "TT":
                case "AC":
                case "AF":
                case "FM":
                case "LC":
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("registeredWeightNew") > 54999) reason += "RB";

                        break;
                    }

                case "TL":
                case "CL":
                    {
                        if (subClassFinal == "C1" || subClassFinal == "C3" || subClassFinal == "L1" || subClassFinal == "L3")
                        {
                            reason += oPrtCodes.CamperTrailer;
                        }

                        break;
                    }
            }

            if (subClassFinal == "C7" || subClassFinal == "L8")
            {
                reason += oPrtCodes.OfficeTrailer;
            }

            if (MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "NRT" || (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2)
                && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher")) != "V")
            {
                if (MotorVehicle.GetSpecialtyPlate(classCodeFinal) != "")
                {
                    reason += oPrtCodes.SpecialtyPlate;
                }
            }
            else
            {
                if (MotorVehicle.GetSpecialtyPlate(classCodeFinal) != "" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher")) != "V")
                {
                    reason += oPrtCodes.SpecialtyPlate;
                }
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher")) == "V")
            {
                reason += oPrtCodes.SpecialtyPlate;
            }

            switch (classCode)
            {
                case "TR":
                    {
                        switch (subClassFinal)
                        {
                            case "T1":
                                reason += oPrtCodes.SMEClassA;

                                break;
                            case "T2":
                                reason += oPrtCodes.SMEClassB10Ton;

                                break;
                            case "T4":
                                reason += oPrtCodes.RoadTractor;

                                break;
                            case "T5":
                                reason += oPrtCodes.FarmUseOnly;

                                break;
                        }

                        break;
                    }

                case "FM":
                case "AF":
                    reason += oPrtCodes.FarmUseOnly;

                    break;

                case "PC":
                    {
                        switch (subClassFinal)
                        {
                            case "P1":
                                reason += oPrtCodes.NotForHwyUse + oPrtCodes.StockCar;

                                break;
                            case "P4":
                                reason += oPrtCodes.NotForHwyUse + oPrtCodes.DuneBuggy;

                                break;
                            case "P5":
                            case "P6":
                                reason += oPrtCodes.RentalVehicle;

                                break;
                        }

                        break;
                    }
            }

            if (subClassFinal == "S2" && classCode == "LS")
            {
                reason += oPrtCodes.RentalVehicle;
            }

            if (subClassFinal == "B1" && classCode == "BU")
            {
                reason += oPrtCodes.NonCompensation;
            }

            if (subClassFinal == "A1" && classCode == "AM")
            {
                reason += oPrtCodes.Hearse;
            }

            if (classCode == "TL")
            {
                switch (subClassFinal)
                {
                    case "L2":
                    case "L4":
                        reason += oPrtCodes.NonExciseTrailer;

                        break;
                    case "L6":
                        reason += oPrtCodes.MobileHome;

                        break;
                    case "L7":
                        reason += oPrtCodes.NonExciseTrueTrailer;

                        break;
                }
            }

            if (classCode == "CL")
            {
                switch (subClassFinal)
                {
                    case "C2":
                    case "C4":
                        reason += oPrtCodes.NonExciseTrailer;

                        break;
                    case "C5":
                        reason += oPrtCodes.MobileHome;

                        break;
                    case "C6":
                        reason += oPrtCodes.NonExciseTrueTrailer;

                        break;
                }
            }

            var transactionTypeFinal = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType"));

            if (transactionTypeFinal == "DPR")
            {
                reason += MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee") != 0 ? "RD" + oPrtCodes.DupRegFee : "RD" + oPrtCodes.DupRegNoFee;
            }

            if (transactionTypeFinal == "LPS")
            {
                reason += MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee") != 0 ? "RL" + oPrtCodes.LostPltStkr : "RL" + oPrtCodes.LostPlate;
            }
            else
            {
                if (MotorVehicle.Statics.LostPlate)
                {
                    reason += "RL" + oPrtCodes.LostPlate;
                }
            }

            if (transactionTypeFinal != "DPR")
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee") != 0)
                {
                    reason += oPrtCodes.DupSticker;
                }
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("AmputeeVet") == true)
            {
                reason += "RZ";
            }

            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExcisePaidDate")))
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExcisePaidDate").Year > 1980) reason += "R4";
            }

            var fleetNumberFinal = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber");

            if (Conversion.Val(fleetNumberFinal) != 0)
            {
                rsTemp.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND FleetNumber = " + fleetNumberFinal);

                if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true
                                               && FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ExpiryMonth")) != 99)
                {
                    reason += oPrtCodes.FleetVehicle;
                }
            }

            var oldPlateFinal = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate"));

            var plateFinal = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));

            if (fecherFoundation.Strings.Trim(transactionTypeFinal) == "ECR" && MotorVehicle.Statics.PlateType == 2 && MotorVehicle.Statics.intCorrNewPlate)
            {
	            if (fecherFoundation.Strings.Trim(oldPlateFinal) != "" && oldPlateFinal != "00000000" &&
	                fecherFoundation.Strings.Trim(oldPlateFinal) != "")
	            {
		            if (fecherFoundation.Strings.Trim(plateFinal) != fecherFoundation.Strings.Trim(oldPlateFinal) ||
		                fecherFoundation.Strings.Trim(classCode) != fecherFoundation.Strings.Trim(
			                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldClass"))))
		            {
			            if (!MotorVehicle.Statics.LostPlate)
				            reason += "RP" + oPrtCodes.NewPlate;
			            else
				            reason += oPrtCodes.NewPlate;
		            }
	            }
	            else
	            {
		            if (!MotorVehicle.Statics.LostPlate)
			            reason += "RP" + oPrtCodes.NewPlate;
		            else
			            reason += oPrtCodes.NewPlate;
	            }
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "P")
            {
	            if (MotorVehicle.GetLowDigit())
	            {
		            reason += oPrtCodes.ReserveNumFeePd;
	            }

	            if (FCConvert.ToDouble(MotorVehicle.GetInitialPlateFees(classCodeFinal,
		                MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"),
		                MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TrailerVanity"),
		                MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwoYear"))) != 0)
	            {
		            reason += oPrtCodes.VanityFeePd;
				}
            }
            else
            {
	            if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee") != 0 ||
                    MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("VanityFlag") ||
	                MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TrailerVanity"))
	            {
		            if ((fecherFoundation.Strings.Trim(transactionTypeFinal) == "ECR" && MotorVehicle.Statics.PlateType == 2) || fecherFoundation.Strings.Trim(transactionTypeFinal) == "LPS" ||
		                (MotorVehicle.Statics.ETO && (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("VanityFlag") || MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TrailerVanity"))))
		            {
			            dblInitFee = FCConvert.ToDouble(MotorVehicle.GetInitialPlateFees(classCodeFinal, MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"),
				            MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TrailerVanity"), MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwoYear")));

			            switch (dblInitFee)
			            {
				            case 25:
					            reason += oPrtCodes.VanityFee25;

					            // "11"
					            break;
				            case 50:
					            reason += oPrtCodes.VanityFee50;

					            break;
				            default:
					            reason += oPrtCodes.VanityFee25;

					            break;
			            }
		            }
		            else
		            {
			            if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee") == 25)
			            {
				            reason += oPrtCodes.VanityFee25;
			            }
			            else
				            reason += MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee") == 50 ? oPrtCodes.VanityFee50 : oPrtCodes.VanityFee25;
		            }
	            }

	            var reservePlateFinal = MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate");
	            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ReservePlateYN") == true)
	            {
		            reason += reservePlateFinal == 0 ? oPrtCodes.ReserveNumFeePd : oPrtCodes.ReserveNumFee;
	            }
	            else
	            {
		            if (reservePlateFinal != 0)
		            {
			            reason += oPrtCodes.ReserveNumFee;
		            }
	            }
			}
			

            if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields("outofrotation")) != 0)
            {
                reason += oPrtCodes.ReserveNumFee;
            }

            if (transactionTypeFinal != "LPS" && !MotorVehicle.Statics.LostPlate)
            {
                if (fecherFoundation.Strings.Trim(oldPlateFinal) != "" && oldPlateFinal != "00000000" && fecherFoundation.Strings.Trim(oldPlateFinal) != "" && (MotorVehicle.Statics.RegistrationType != "NRR" || MotorVehicle.Statics.PlateType != 3))
                {
                    if (fecherFoundation.Strings.Trim(plateFinal) != fecherFoundation.Strings.Trim(oldPlateFinal) || fecherFoundation.Strings.Trim(classCode) != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldClass"))))
                    {
                        if (classCode != "TC" && classCode != "IU")
                        {
                            if (MotorVehicle.Statics.NewPlate == true)
                            {
                                reason += "RP" + oPrtCodes.PlateChange;
                            }
                            else if (Strings.Mid(MotorVehicle.Statics.RegistrationType, 1, 1) == "R" || (MotorVehicle.Statics.RegistrationType == "NRT" && MotorVehicle.Statics.PlateType > 1) || (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2))
                            {
                                reason += MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType == 1 ? "RP" : "RP" + oPrtCodes.PlateChange;
                            }
                        }
                        else if (classCodeFinal == "IU")
                        {
                            reason += "RP";
                        }
                    }
                }
            }

            var plateTypeFinal = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType"));

            var forcedPlateFinal = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate"));

            if ((Strings.Mid(transactionTypeFinal, 1, 2) == "NR" || Strings.Mid(transactionTypeFinal, 1, 2) == "RR") && (forcedPlateFinal == "N" || forcedPlateFinal == ""))
            {
                switch (plateTypeFinal)
                {
                    case "N" when FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) != "NEW":
                        reason += oPrtCodes.NewPlate;

                        break;
                    case "R":
                        reason += "RR" + oPrtCodes.ReplFee;

                        break;
                }

                if (!MotorVehicle.Statics.LostPlate && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee")) != 0)
                {
                    reason += "RR" + oPrtCodes.ReplFee;
                }
            }

            if (transactionTypeFinal == "NRR" && (forcedPlateFinal == "N" || forcedPlateFinal == "") && plateTypeFinal == "O")
            {
                reason += oPrtCodes.NewRegOldPlate;
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields("Transfer") == "True")
            {
                reason += MotorVehicle.Statics.RegistrationType == "CORR" ? "R4" : "R3" + oPrtCodes.Transfer;
            }

            blnUnderAge = false;
            DateTime age18dob = DateTime.Today.AddYears(-18);

            if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1"))) == "I")
            {
                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("DOB1")))
                {
                    var dateOfBirth1Final = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1");

                    if (dateOfBirth1Final > age18dob)
                    {
                        reason += "RU";
                        blnUnderAge = true;
                    }
                }
            }

            if (fecherFoundation.Strings.Trim(FCConvert.ToString(ownerCode2Final)) == "I" && !blnUnderAge)
            {
                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("DOB2")))
                {
                    var dateOfBirth2Final = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2");

                    if (dateOfBirth2Final > age18dob)
                    {
                        reason += "RU";
                        blnUnderAge = true;
                    }
                }
            }

            if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3"))) == "I" && !blnUnderAge)
            {
                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("DOB3")))
                {
                    var dateOfBirth3Final = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3");

                    if (dateOfBirth3Final > age18dob)
                    {
                        reason += "RU";
                        blnUnderAge = true;
                    }
                }
            }

            if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName)) == "MAINE MOTOR TRANSPORT")
            {
                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1")) != "")
                {
                    reason += oPrtCodes.OtherEntered;
                    txtMessage.Text = "UNIT# " + MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1");
                }
            }

            MotorVehicle.Statics.reason1 = reason;
            RebuildLine(reason);
        }


        #region Owner Name Changes

        private void Owner3NameChange(cPartyController pCont)
        {
            var ownerCodeFinal = ScrubOwnerCode(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3"));
            var ownerCodeCompare = ScrubOwnerCode(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode3"));
            var partyIdFinal = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID3");
            var partyIdCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3");

            string partyNameCompare = "";
            string partyNameFinal = "";
            if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode3") == "I")
            {
                partyNameCompare = FormatRegistrantName(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg3FirstName"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg3MI"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg3LastName"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg3Designation"));
            }
            else
            {
                partyNameCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Owner3");
            }

            if (partyNameCompare == "")
            {
                partyNameCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PartyName3");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3") == "I")
            {
                partyNameFinal = FormatRegistrantName(MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3FirstName"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3MI"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3LastName"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3Designation"));
            }
            else
            {
                partyNameFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Owner3");
            }

            var myReason = GetOwnerChangeReason(3, ownerCodeFinal, ownerCodeCompare, partyIdFinal, partyIdCompare, partyNameFinal, partyNameCompare);

            reason += myReason;

        }

        private void Owner2NameChange(cPartyController pCont)
        {
            var ownerCodeFinal = ScrubOwnerCode(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2"));
            var ownerCodeCompare = ScrubOwnerCode(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode2"));
            var partyIdFinal = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2");
            var partyIdCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2");

            string partyNameCompare = "";
            string partyNameFinal = "";
            if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode2") == "I")
            {
                partyNameCompare = FormatRegistrantName(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg2FirstName"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg2MI"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg2LastName"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg2Designation"));
            }
            else
            {
                partyNameCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Owner2");
            }

            if (partyNameCompare == "")
            {
                partyNameCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PartyName2");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2") == "I")
            {
                partyNameFinal = FormatRegistrantName(MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2FirstName"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2MI"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2LastName"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2Designation"));
            }
            else
            {
                partyNameFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Owner2");
            }

            var myReason = GetOwnerChangeReason(2, ownerCodeFinal, ownerCodeCompare, partyIdFinal, partyIdCompare, partyNameFinal, partyNameCompare);

            reason += myReason;
        }

        private void Owner1NameChange(cPartyController pCont)
        {
            // owner codes are not compared on owner 1, so they are hardcoded to the same value
            var ownerCodeFinal = "";
            var ownerCodeCompare = "";

            var partyIdFinal = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1");
            var partyIdCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID1");

            string partyNameCompare = "";
            string partyNameFinal = "";

            if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("OwnerCode1") == "I")
            {
                partyNameCompare = FormatRegistrantName(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg1FirstName"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg1MI"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg1LastName"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Reg1Designation"));
            }
            else
            {
                partyNameCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Owner1");
            }

            if (partyNameCompare == "")
            {
                partyNameCompare = MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PartyName1");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1") == "I")
            {
                partyNameFinal = FormatRegistrantName(MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1FirstName"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1MI"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1LastName"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1Designation"));
            }
            else
            {
                partyNameFinal = MotorVehicle.Statics.rsFinal.Get_Fields_String("Owner1");
            }

            var myReason = GetOwnerChangeReason(1, ownerCodeFinal, ownerCodeCompare, partyIdFinal, partyIdCompare, partyNameFinal, partyNameCompare);

            reason += myReason;
        }

        private string FormatRegistrantName(string firstName, string middleInitial, string lastName, string designation)
        {
            return Strings.Trim(Strings.Trim(Strings.Trim(Strings.Trim(firstName) + " " + middleInitial.Left(1)) + " " + lastName) + " " + designation);
        }
        private string GetOwnerChangeReason(int ownerNumber, string ownerCodeFinal, string ownerCodeCompare, int partyIdFinal, int partyIdCompare, string partyNameFinal, string partyNameCompare)
        {
            // what is the reason for the change?

            // owner deleted?
            var myReason = OwnerDeletedDescription(ownerCodeFinal, ownerCodeCompare);

            // no? is owner id different?
            if (myReason.Length == 0) myReason = OwnerPartyIdChangedDescription(ownerNumber, partyIdFinal, partyIdCompare);

            // no? owner name changed?
            if (myReason.Length == 0) myReason = OwnerPartyNameChangedDescription(partyNameFinal?.Trim(), partyNameCompare.Trim());

            return myReason;
        }

        private string OwnerDeletedDescription(string ownerCodeFinal, string ownerCodeCompare)
        {
            // if the final owner code is set, then they couldn't have been deleted now, could they?
            if (IsOwnerCodeSet(ownerCodeFinal)) return string.Empty;

            // held new plates is one example where the Compare fields are blank so set them to a place where it is apples to apples
            ownerCodeCompare = ScrubOwnerCode(ownerCodeCompare);

            var ret = string.Empty;

            if (IsOwnerCodeSet(ownerCodeCompare)) ret = oPrtCodes.NameDeleted; // they have gone from non-N to N, thus deleted

            return ret;
        }

        private string OwnerPartyIdChangedDescription(int ownerNumber, int partyIdFinal, int partyIdCompare)
        {
            if (partyIdFinal == partyIdCompare) return string.Empty;

            var ret = string.Empty;

            // if we are owner 1 and there is a previous, different, partyID then we have a name change
            if (ownerNumber == 1 && partyIdCompare > 0)
                ret = oPrtCodes.NameChanged;
            else
            // if we are owner 1, then there is no old partyID so this is an add and we don't put a msg in
            // if we are not owner 1, then we see if we are adding them or changing them
                if (ownerNumber != 1)
                ret = partyIdCompare > 0 ? oPrtCodes.NameChanged : oPrtCodes.NameAdded;

            return ret;
        }

        private string OwnerPartyNameChangedDescription(string partyNameFinal, string partyNameCompare)
        {
            if (partyNameFinal == null) partyNameFinal = string.Empty;
            if (partyNameCompare == null) partyNameCompare = string.Empty;

            var ret = string.Empty;

            if (partyNameFinal.ToUpper() != partyNameCompare.ToUpper() && partyNameCompare.Length > 0) ret = oPrtCodes.NameChanged;

            return ret;
        }

        private static string ScrubOwnerCode(string ownerCode)
        {
            return string.IsNullOrEmpty(ownerCode) ? "N" : ownerCode;
        }

        private static bool IsOwnerCodeSet(string ownerCode)
        {
            // "N" or blank/empty mean it is NOT set
            return !(ownerCode == "N" || string.IsNullOrEmpty(ownerCode));
        }

        #endregion

        public string PPText(string ppsub)
        {
            string PPText = "";
            int TotalStickers = 0;
            clsDRWrapper rsStickerFee = new clsDRWrapper();
            clsDRWrapper rsSpecialtyDescription = new clsDRWrapper();
            rsPP.FindFirstRecord("Code", ppsub);
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew")) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
            }
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeReReg")) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
            }
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("outofrotation")) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("outofrotation", 0);
            }
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate")) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlate", 0);
            }
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee")) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("InitialFee", 0);
            }

            var replacementFeeFinal = MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee");

            if (Conversion.Val(replacementFeeFinal) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", 0);
            }
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee")) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
            }
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee")) == 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", 0);
            }

            const string decimalFormat = "#.00";

            if (Conversion.Val(ppsub) == Conversion.Val(oPrtCodes.ReserveNumFee))
            {
                PPText = fecherFoundation.Strings.Trim(FCConvert.ToString(rsPP.Get_Fields_String("Description")) + Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("outofrotation")), decimalFormat));
            }
            else if (Conversion.Val(ppsub) == Conversion.Val(oPrtCodes.VeteranDecal))
            {
                PPText = oPrtCodes.GetVeteranDecalText(MotorVehicle.Statics.rsFinal.Get_Fields_String("Battle")) + " DECAL";
            }
            else
            {
                var ppDescription = fecherFoundation.Strings.Trim(FCConvert.ToString(rsPP.Get_Fields_String("Description")));

                if (Conversion.Val(ppsub) == Conversion.Val(oPrtCodes.ReplFee))
                {
                    PPText = ppDescription + Strings.Format(replacementFeeFinal, decimalFormat);
                }
                else if (Conversion.Val(ppsub) == Conversion.Val(oPrtCodes.DupSticker))
                {
                    PPText = ppDescription + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee"), decimalFormat);
                }
                else if (Conversion.Val(ppsub) == Conversion.Val(oPrtCodes.LostPlate))
                {
                    if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) != 0 || FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) != 0)
                    {
                        TotalStickers = FCConvert.ToInt16(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")));
                        rsStickerFee.OpenRecordset("SELECT * FROM DefaultInfo");
                        if (TotalStickers * rsStickerFee.Get_Fields_Decimal("Stickers") != MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee"))
                        {
                            PPText = ppDescription + Strings.Format(replacementFeeFinal - (TotalStickers * rsStickerFee.Get_Fields_Decimal("Stickers")), decimalFormat);
                        }
                        else
                        {
                            PPText = ppDescription + Strings.Format(replacementFeeFinal, decimalFormat);
                        }
                    }
                    else
                    {
                        PPText = ppDescription + Strings.Format(replacementFeeFinal, decimalFormat);
                    }
                }
                else if (Conversion.Val(ppsub) == Conversion.Val(oPrtCodes.LostPltStkr))
                {
                    if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) != 0 || FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) != 0)
                    {
                        TotalStickers = FCConvert.ToInt16(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")));
                        rsStickerFee.OpenRecordset("SELECT * FROM DefaultInfo");
                        if (TotalStickers * rsStickerFee.Get_Fields_Decimal("Stickers") != MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee"))
                        {
                            PPText = ppDescription + Strings.Format(replacementFeeFinal - (TotalStickers * rsStickerFee.Get_Fields_Decimal("Stickers")), decimalFormat);
                        }
                        else
                        {
                            PPText = ppDescription + Strings.Format(replacementFeeFinal, decimalFormat);
                        }
                    }
                    else
                    {
                        PPText = ppDescription + Strings.Format(replacementFeeFinal, decimalFormat);
                    }
                }
                else if (Conversion.Val(ppsub) == Conversion.Val(oPrtCodes.SpecialtyPlate))
                {
                    var classCodeFinal = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")));

                    if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 1)
                    {
                        PPText = classCodeFinal;
                    }
                    else
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher")) != "V")
                        {
                            PPText = classCodeFinal + " $" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeReReg"), decimalFormat);
                        }
                        else
                        {
                            PPText = classCodeFinal + " PD VCR# " + MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftcertificateNumber");
                        }
                    }
                }
                else
                {
                    PPText = ppDescription;
                }
            }

            return PPText;
        }

        public string SetRR(string rr)
        {
            string SetRR = "";
            try
            {
                fecherFoundation.Information.Err().Clear();
                switch (rr)
                {
                    case "R3" when MotorVehicle.Statics.RegistrationType == "DUPREG":
                        {
                            RRText = blnTaxRecieptShown ? "3" : "3 " + MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("OldMVR3").ToString();

                            break;
                        }

                    case "R3" when blnTaxRecieptShown:
                        RRText = "3";

                        break;
                    case "R3":
                        RRText = "3 " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3").ToString();

                        break;

                    case "R4" when MotorVehicle.Statics.RegistrationType == "DUPREG":
                        {
                            RRText = blnTaxRecieptShown ? "4" : "4 " + MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("OldMVR3").ToString();

                            break;
                        }

                    case "R4" when blnTaxRecieptShown:
                        RRText = "4";

                        break;
                    case "R4":
                        RRText = "4 " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3").ToString();

                        break;

                    case "RD" when Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3")) == 0:
                        {
                            RRText = blnTaxRecieptShown ? "D" : "D " + CurrentMVR3;

                            break;
                        }

                    case "RD" when blnTaxRecieptShown:
                        RRText = "D";

                        break;
                    case "RD":
                        RRText = "D " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3").ToString();

                        break;
                    case "R#":
                        RRText = "# " + Strings.Format((MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) * -1, "0.00");

                        break;
                    case "RL":
                        RRText = "L " + MotorVehicle.Statics.rsFinal.Get_Fields_String("OldClass") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")));

                        break;
                    case "RP":
                        RRText = "P " + MotorVehicle.Statics.rsFinal.Get_Fields_String("OldClass") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")));

                        break;
                    case "RR":
                        RRText = "R " + MotorVehicle.Statics.rsFinal.Get_Fields_String("OldClass") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")));

                        break;
                    case "RT":
                        RRText = "T " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")));

                        break;
                    default:
                        RRText = Strings.Mid(rr, 2, 1);

                        break;
                }
                return SetRR;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (fecherFoundation.Information.Err(ex).Number == 94)
                {
                    MessageBox.Show(" Either Old Plate or Old MVR3 is missing. Some data may not print on the MVR3.", "Missing Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show(" There was an error accessing the old data. Some of it may not print on the MVR3.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                fecherFoundation.Information.Err(ex).Clear();
            }
            return SetRR;
        }

        private void mnuProcessUpdate_Click(object sender, System.EventArgs e)
        {
            txtReasonsOnTheBottom_DblClick();
        }

        private void mnuReturn_Click(object sender, System.EventArgs e)
        {
            Close();
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3")) != 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3"));
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", 0);
            }

            txtMVR3Input.Text = "0";
            if (MotorVehicle.Statics.RegistrationType == "DUPREG")
            {
            }
            else if (MotorVehicle.Statics.RegistrationType == "LOST")
            {
                if (MotorVehicle.Statics.RegTypeLost == "Input")
                {
                    frmDataInput.InstancePtr.Show(App.MainForm);
                }
                else if (MotorVehicle.Statics.RegTypeLost == "Info")
                {
                }
            }
            else
            {
                frmDataInput.InstancePtr.Show(App.MainForm);
            }
            MotorVehicle.Statics.RebuildingLine = false;
        }

        private void txtMessage_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtMessage.BackColor);
            txtMessage.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtMessage_Leave(object sender, System.EventArgs e)
        {
            txtMessage.BackColor = ColorTranslator.FromOle(OldColor);
            RebuildLine(MotorVehicle.Statics.reason2);
        }

        private void txtMVR3Input_Enter(object sender, System.EventArgs e)
        {
            txtMVR3Input.SelectionStart = txtMVR3Input.Text.Length;
            OldColor = ColorTranslator.ToOle(txtMVR3Input.BackColor);
            txtMVR3Input.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        public void txtMVR3Input_GotFocus()
        {
            txtMVR3Input_Enter(txtMVR3Input, new System.EventArgs());
        }

        private void txtMVR3Input_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }
            else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.NumPad3))
            {
                KeyAscii = (Keys)0;
                txtMVR3Input.Text = "";
            }
            else
            {
                KeyAscii = (Keys)0;
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMVR3Input_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (Shift == 3 && KeyCode == Keys.F9)
            {
                MotorVehicle.Statics.F9 = true;
                frmInventory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            }
        }

        private void txtMVR3Input_Leave(object sender, System.EventArgs e)
        {
            if (OldColor == 0x0)
                OldColor = ColorTranslator.ToOle(txtMessage.BackColor);
            txtMVR3Input.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtMVR3Input_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            txtMVR3Input.Text = fecherFoundation.Strings.Trim(txtMVR3Input.Text.Replace("\t", "").Replace("\r\n", ""));
            if (txtMVR3Input.Text.Length == 7 || txtMVR3Input.Text.Length == 8)
            {
                if (ValidateMVR3(txtMVR3Input.Text.ToIntegerValue()))
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", txtMVR3Input.Text.ToIntegerValue());
                    RefreshMVR3Preview();
                }
                else
                {
                    e.Cancel = true;
                    txtMVR3Input.Text = strMVR3Number;
                    txtMVR3Input.SelectionStart = strMVR3Number.Length;
                }
            }
        }



        private void txtReasonsOnTheBottom_DoubleClick(object sender, System.EventArgs e)
        {
            if (MotorVehicle.Statics.RegistrationType != "DUPREG")
            {
                frmReasonCodes.InstancePtr.OriginalReasons = txtReasonsOnTheBottom.Text.Trim();
                frmReasonCodes.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                RebuildLine(MotorVehicle.Statics.reason2);
                frmPreview.InstancePtr.Refresh();
            }
        }

        public void txtReasonsOnTheBottom_DblClick()
        {
            txtReasonsOnTheBottom_DoubleClick(txtReasonsOnTheBottom, new System.EventArgs());
        }

        public void RebuildLine(string reason)
        {
            DialogResult ans = 0;
            int tempID;
            if (MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "GVW" || MotorVehicle.Statics.RegistrationType == "LOST")
            {
                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3")) == 0)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("PriorTaxReceipt", 0);
                }
                if (Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3")))) == 0)
                {
                EnterCurrentMVR3:
                    ;
                    CurrentMVR3 = Interaction.InputBox("Please enter the old MVR3 Number", "MVR3 Number", null);
                    if (CurrentMVR3 == "" || !Information.IsNumeric(CurrentMVR3) || CurrentMVR3.Length > 7)
                    {
                        ans = MessageBox.Show("You have entered an invalid MVR3 form number.  Do you wish to try again?", "Enter MVR3 Number?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.Yes)
                        {
                            goto EnterCurrentMVR3;
                        }
                        else
                        {
                            blnEndRegistration = true;
                            return;
                        }
                    }
                    else
                    {
                        tempID = FCConvert.ToInt32(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("ID"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", CurrentMVR3);
						MotorVehicle.Statics.rsFinal.Set_Fields("PriorTaxReceipt", CurrentMVR3);
                    }
                    if (!reason.Contains("R4"))
                    {
                        reason += "R4";
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "GVW" || MotorVehicle.Statics.RegistrationType == "LOST")
                    {
                        if (!reason.Contains("R4"))
                        {
                            reason += "R4";
                        }
                    }
                    else
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "ECO" || MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("EAP") == true)
                            reason += "R4";
                    }
                }
            }

			System.Collections.Generic.List<PPCode> ppCodes = new System.Collections.Generic.List<PPCode>();
			System.Collections.Generic.List<PPCode> rrCodes = new System.Collections.Generic.List<PPCode>();

			for (int i = 0; i < reason.Length; i+=2)
			{
				rr = reason.Substring(i, 2);
				if (rr.StartsWith("R"))
				{
					if (!rrCodes.Exists(p => p.Code == rr))
					{
						rsReason.FindFirstRecord("Code", rr.Substring(1));
						rrCodes.Add(new PPCode
						{
							Code = rr,
							Priority = rsReason.Get_Fields("Priority")
						});
					}
				}
				else
				{
					if (!ppCodes.Exists(p => p.Code == rr))
					{
						rsPP.FindFirstRecord("Code", rr);
						ppCodes.Add(new PPCode
						{
							Code = rr,
							Priority = rsPP.Get_Fields("Priority")
						});
					}
				}
			}

			MotorVehicle.Statics.gPPriority = new string[ppCodes.Count + 1];
			MotorVehicle.Statics.gPPriNumber = new string[ppCodes.Count + 1];

			reason = "";
			PrintPriorityMessageNeeded = false;
			ReasonMessageNeeded = false;
			int x = 1;
			foreach (PPCode pp in ppCodes.OrderBy(p => p.Priority))
			{
				reason += pp.Code;
				if (pp.Code == oPrtCodes.OtherEntered)
				{
					PrintPriorityMessageNeeded = true;
					MotorVehicle.Statics.gPPriority[x] = Strings.Left(txtMessage.Text, 15);
				}
				else
				{
					MotorVehicle.Statics.gPPriority[x] = PPText(pp.Code);
				}
				MotorVehicle.Statics.gPPriNumber[x] = pp.Code;
				x++;
			}

			foreach (PPCode rr in rrCodes.OrderBy(p => p.Priority))
			{
				reason += rr.Code;
				if (rr.Code == "R2")
				{
					ReasonMessageNeeded = true;
					MotorVehicle.Statics.gPPriority[x] = Strings.Left(txtMessage.Text, 15);
				}
				x++;
			}

            blnTaxRecieptShown = false;

			txtReasonsOnTheBottom.Text = reason;
            if (PrintPriorityMessageNeeded == true || ReasonMessageNeeded == true)
            {
                if (PrintPriorityMessageNeeded)
                {
                    txtMessage.MaxLength = 15;
                }
                else
                {
                    txtMessage.MaxLength = 24;
                }
                txtMessage.Enabled = true;
            }
            else
            {
                txtMessage.Enabled = false;
            }

            MotorVehicle.Statics.RRTextAll = "";
            if (rrCodes.Any())
            {
	            foreach (PPCode rr in rrCodes.OrderBy(p => p.Code))
	            {
		            if (rr.Code == "R2")
		            {
			            MotorVehicle.Statics.RRTextAll += Strings.Left(txtMessage.Text, 24) + ",";
		            }
		            else
		            {
			            SetRR(rr.Code);
			            MotorVehicle.Statics.RRTextAll += RRText + ",";
		            }
	            }
				if (Strings.Mid(MotorVehicle.Statics.RRTextAll, MotorVehicle.Statics.RRTextAll.Length, 1) == ",")
					MotorVehicle.Statics.RRTextAll = Strings.Mid(MotorVehicle.Statics.RRTextAll, 1, MotorVehicle.Statics.RRTextAll.Length - 1);
            }

			RefreshMVR3Preview();
        }

        private void txtReasonsOnTheBottom_Enter(object sender, System.EventArgs e)
        {
            if (MotorVehicle.Statics.RegistrationType != "DUPREG")
            {
                OldColor = ColorTranslator.ToOle(txtReasonsOnTheBottom.BackColor);
                txtReasonsOnTheBottom.BackColor = ColorTranslator.FromOle(NewBackColor);
            }
        }

        private void txtReasonsOnTheBottom_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
            {
                KeyAscii = KeyAscii - 32;
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtReasonsOnTheBottom_Leave(object sender, System.EventArgs e)
        {
            if (MotorVehicle.Statics.RegistrationType != "DUPREG")
            {
                if (ColorTranslator.FromOle(OldColor) != txtMessage.BackColor)
                {
                    txtReasonsOnTheBottom.BackColor = ColorTranslator.FromOle(OldColor);
                }
            }
        }

        private void txtReasonsOnTheBottom_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MotorVehicle.Statics.RegistrationType != "DUPREG")
            {
                MotorVehicle.Statics.reason2 = txtReasonsOnTheBottom.Text;
                RebuildLine(MotorVehicle.Statics.reason2);
                txtReasonsOnTheBottom.BackColor = ColorTranslator.FromOle(OldColor);
            }
        }

        private void RefreshMVR3Preview()
        {
            if (arvMVR3Preview.ReportSource != null)
            {
                if (!arvMVR3Preview.ReportSource.Disposing)
                {
                    arvMVR3Preview.ReportSource.Dispose();
                }
                arvMVR3Preview.Dispose();
                arvMVR3Preview = null;
                arvMVR3Preview = new ARViewer();
                arvMVR3Preview.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                arvMVR3Preview.Height = 378;
                arvMVR3Preview.Width = 969;
                arvMVR3Preview.ScrollBars = false;
                arvMVR3Preview.BorderStyle = BorderStyle.Solid;
                ClientArea.Controls.First(x => x.Name == "Frame1").Controls.Add(arvMVR3Preview);
                arvMVR3Preview.Left = 8;
                arvMVR3Preview.Top = 70;
                ClientArea.Controls.First(x => x.Name == "Frame1").Controls.SetChildIndex(arvMVR3Preview, 71);
            }

            rptMVR3Laser rpt = new rptMVR3Laser();
            rpt.IsPreReg = IsPreReg;
            rpt.RunReport();
            arvMVR3Preview.ReportSource = rpt;
        }

        public bool Check2Year()
        {
            bool Check2Year = false;
            object YY = null;

            if (fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate")) == false && MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate() != 0)
            {
                if (MotorVehicle.Statics.TwoYear == true)
                {
                    YY = fecherFoundation.DateAndTime.DateDiff("yyyy", MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"), MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
                    if (FCConvert.ToInt32(YY) == 2 || FCConvert.ToInt32(YY) == 1)
                    {
                        Check2Year = true;
                    }
                }
            }
            return Check2Year;
        }

        public void LoadReasonCodesForDup()
        {
            string[] arrCodes = null;
            string[] arrItem = null;
            string strTemp = "";
            string strDupText = "";
            int i, iCount;
            int intOrder = 0;
            bool boolDupAdded = false;
            MotorVehicle.Statics.PP3Text = "";
            MotorVehicle.Statics.PP2Text = "";
            MotorVehicle.Statics.PP1Text = "";
            if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PrintPriorities"))) != "")
            {
                arrCodes = Strings.Split(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PrintPriorities"))), "|", -1, CompareConstants.vbBinaryCompare);
                iCount = Information.UBound(arrCodes, 1) + 1;
                MotorVehicle.Statics.gPPriority = new string[iCount + 1 + 1];
                boolDupAdded = false;
                for (i = 1; i <= iCount; i++)
                {
                    arrItem = Strings.Split(arrCodes[i - 1], ",", -1, CompareConstants.vbBinaryCompare);
                    intOrder = FCConvert.ToInt32(arrItem[0]);
                    if (intOrder < Conversion.Val(oPrtCodes.DupRegFee))
                    {
                        MotorVehicle.Statics.gPPriority[i] = fecherFoundation.Strings.Trim(arrItem[1]);
                    }
                    else
                    {
                        if (!boolDupAdded)
                        {
                            if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee") != 0)
                            {
                                MotorVehicle.Statics.gPPriority[i] = oPrtCodes.ReturnPrintPriorityText(FCConvert.ToInt32(oPrtCodes.DupRegFee));
                            }
                            else
                            {
                                MotorVehicle.Statics.gPPriority[i] = oPrtCodes.ReturnPrintPriorityText(FCConvert.ToInt32(oPrtCodes.DupRegNoFee));
                            }
                            MotorVehicle.Statics.gPPriority[i + 1] = fecherFoundation.Strings.Trim(arrItem[1]);
                            boolDupAdded = true;
                        }
                        else
                        {
                            MotorVehicle.Statics.gPPriority[i + 1] = fecherFoundation.Strings.Trim(arrItem[1]);
                        }
                    }
                }
            }
            else
            {
                MotorVehicle.Statics.gPPriority = new string[1 + 1];
                iCount = 0;
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee") != 0)
                {
                    MotorVehicle.Statics.gPPriority[1] = oPrtCodes.ReturnPrintPriorityText(FCConvert.ToInt32(oPrtCodes.DupRegFee));
                }
                else
                {
                    MotorVehicle.Statics.gPPriority[1] = oPrtCodes.ReturnPrintPriorityText(FCConvert.ToInt32(oPrtCodes.DupRegNoFee));
                }
            }
            if (iCount > 2)
            {
                MotorVehicle.Statics.PP3Text = FCConvert.ToString(MotorVehicle.Statics.gPPriority[3]);
            }
            if (iCount > 1)
            {
                MotorVehicle.Statics.PP2Text = FCConvert.ToString(MotorVehicle.Statics.gPPriority[2]);
            }
            if (iCount > 0)
            {
                MotorVehicle.Statics.PP1Text = FCConvert.ToString(MotorVehicle.Statics.gPPriority[1]);
            }

            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3")) == 0 || blnTaxRecieptShown)
            {
                strDupText = "D";
            }
            else
            {
                strDupText = "D " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3").ToString();
            }
            MotorVehicle.Statics.RRTextAll = "";
            var alteredReasonCodes = RemoveReasonCodeA(fecherFoundation.Strings.Trim(
                FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("ReasonCodes"))));

            if (fecherFoundation.Strings.Trim(alteredReasonCodes) != "")
            {
                MotorVehicle.Statics.RRTextAll = alteredReasonCodes;
                if (Strings.Left(MotorVehicle.Statics.RRTextAll, 1) != "3")
                {
                    MotorVehicle.Statics.RRTextAll = fecherFoundation.Strings.Trim(strDupText) + "," + fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRTextAll);
                }
                else
                {
                    iCount = Strings.InStr(MotorVehicle.Statics.RRTextAll, ",", CompareConstants.vbBinaryCompare);
                    if (FCConvert.ToInt32(iCount) == 0)
                    {
                        MotorVehicle.Statics.RRTextAll = fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRTextAll) + "," + fecherFoundation.Strings.Trim(strDupText);
                    }
                    else
                    {
                        strTemp = Strings.Left(MotorVehicle.Statics.RRTextAll, FCConvert.ToInt32(iCount)) + "," + fecherFoundation.Strings.Trim(strDupText);
                        strTemp += Strings.Mid(MotorVehicle.Statics.RRTextAll, FCConvert.ToInt32(iCount));
                        MotorVehicle.Statics.RRTextAll = strTemp;
                    }
                }
            }
            else
            {
                MotorVehicle.Statics.RRTextAll = fecherFoundation.Strings.Trim(strDupText);
            }
        }

        private string RemoveReasonCodeA(string reasonCodes)
        {
            if (reasonCodes == "A")
            {
                return "";
            }
            else
            {
                reasonCodes = reasonCodes.Replace("A,", "");
                reasonCodes = reasonCodes.Replace(",A", "");
                return reasonCodes;
            }
        }
        private bool ValidateExciseDate(string strEffectiveDate, string strExciseDate)
        {
            bool ValidateExciseDate = false;
            bool boolReturn;
            ValidateExciseDate = true;
            boolReturn = true;
            if (Information.IsDate(strEffectiveDate))
            {
                if (Information.IsDate(strExciseDate))
                {
                    if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(strEffectiveDate), FCConvert.ToDateTime(strExciseDate)) > 0)
                    {
                        boolReturn = false;
                    }
                    if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Today, FCConvert.ToDateTime(strExciseDate)) > 0)
                    {
                        boolReturn = false;
                    }
                }
            }
            ValidateExciseDate = boolReturn;
            return ValidateExciseDate;
        }

        private void btnPrintCTA_Click(object sender, EventArgs e)
        {
            clsDRWrapper rsTitle = new clsDRWrapper();
            string NewCTA = "";

            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
            {
	            return;
            }
			GrapeCity.ActiveReports.Export.Word.Section.RtfExport ArTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
            blnLabelClicked = true;
            if (MotorVehicle.Statics.lngCTAAddNewID != 0)
            {
                rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
                if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                {
                    rsTitle.MoveLast();
                    rsTitle.MoveFirst();
                }
            }
            if (blnCTADone)
            {
                NewCTA = Interaction.InputBox("Please enter the new CTA Number to use.", "CTA Number", null);
                if (NewCTA == "")
                {
                    MessageBox.Show("You must enter a new CTA Number before you may print.", "Invalid CTA Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    blnLabelClicked = false;
                    return;
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("CTANumber", NewCTA);
                }
            }

            if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to level as possible) next to the line titled:" + "\r\n" + "THIS IS NOT A CERTIFICATE OF TITLE", 47, 1, MotorVehicle.Statics.gstrCTAPrinterName) == DialogResult.Cancel)
            {
                MotorVehicle.Statics.blnPrintCTA = false;
            }
            else
            {
                rptNewCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                rptNewCTA.InstancePtr.Unload();
            }

            if (MotorVehicle.Statics.blnPrintCTA == true)
            {
                if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) == 0 ||
                    (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0 &&
                     MotorVehicle.Statics.PrintDouble == false))
                {
                    blnCTADone = true;
                    imgCTAPrinted.Visible = true;
                    imgCTAPrinted.ImageSource = "icon-check";
                    // btnPrintCTA.ImageSource = "icon-check";
                    if (AllFormsPrinted())
                    {
                        cmdSave.Enabled = true;
                    }
                }
                else
                {
                    MotorVehicle.Statics.boolDoubleCTA = true;
                    MessageBox.Show("Click OK when ready to print the second CTA.", "Double CTA", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    if (modPrinterFunctions.PrintXsForAlignment(
                            "The X should have printed (with the bottoms as close to level as possible) next to the line titled:" +
                            "\r\n" + "THIS IS NOT A CERTIFICATE OF TITLE", 47, 1,
                            MotorVehicle.Statics.gstrCTAPrinterName) == DialogResult.Cancel)
                    {
                        MotorVehicle.Statics.blnPrintCTA = false;
                    }
                    else
                    {
                        rptNewCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                        rptNewCTA.InstancePtr.Unload();
                    }


                    if (MotorVehicle.Statics.blnPrintDoubleCTA == true)
                    {
                        blnCTADone = true;
                        imgCTAPrinted.Visible = true;
                        if (AllFormsPrinted())
                        {
                            cmdSave.Enabled = true;
                        }
                    }
                }

                fecherFoundation.Information.Err().Clear();
            }

            blnLabelClicked = false;
        }

        private bool AllFormsPrinted()
        {
            return ((btnPrintCTA.Enabled && blnCTADone) || !btnPrintCTA.Enabled) &&
                   ((btnPrintUseTax.Enabled && blnUseTaxDone) || !btnPrintUseTax.Enabled) && blnMVR3Done;
        }

        private bool ValidateMVR3(int xx)
        {
            bool ValidateMVR3 = false;
            DialogResult ans = 0;
            clsDRWrapper rsMVR3 = new clsDRWrapper();

            ValidateMVR3 = true;

            try
            {

                if (MotorVehicle.Statics.MVR3Group == 0)
                {
                    rsMVR3.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + "MXS00" + "'  AND Status = 'A' AND Number = " + xx.ToString());
                }
                else
                {
                    rsMVR3.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + "MXS00" + "'  AND Status = 'A' AND Number = " + xx.ToString() + " AND ([Group] = " + MotorVehicle.Statics.MVR3Group.ToString() + " OR [Group] = 0)");
                }
                if (rsMVR3.EndOfFile() != true && rsMVR3.BeginningOfFile() != true)
                {
                    if (MotorVehicle.Statics.MVR3Warning)
                    {
                        if (intLastMVR > 0)
                        {
                            if (xx != intLastMVR + 1)
                            {
                                ans = MessageBox.Show("The MVR3 Number you entered was not the next one expected.  The last one used was " + FCConvert.ToString(intLastMVR) + ".  The one you just entered was " + xx + ".  Is this correct?", "MVR3 Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (ans == DialogResult.No)
                                {
                                    ValidateMVR3 = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("There is no MVR3 with that number in inventory.", "No MVR3", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ValidateMVR3 = false;
                }
            }
            finally
            {
                rsMVR3.DisposeOf();
            }

            return ValidateMVR3;
        }

        private bool ValidateMVR3ForReprint(int xx)
        {
	        bool ValidateMVR3ForReprint = false;
			clsDRWrapper rsMVR3 = new clsDRWrapper();
			rsMVR3.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' AND Status <> 'I' AND Number = " + xx.ToString());
			if (!rsMVR3.EndOfFile())
			{
				if (txtMVR3Input.Text != xx.ToString() && lngNoFeeDupRegMVR3Number != xx)
				{
					ValidateMVR3ForReprint = true;
				}
			}
			rsMVR3.DisposeOf();
			return ValidateMVR3ForReprint;
        }

        private void PrintNoFeeDupReg()
        {
            int NewMVR3Number;
            string temp;
            clsDRWrapper rsMVR3 = new clsDRWrapper();
            temp = Interaction.InputBox("Please input the MVR3 Number for the No Fee Duplicate Registration." + "\r\n" + "\r\n" + "The last number used:  " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"), "Next MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") + 1));
            if (temp.Length > 8)
                return;
            NewMVR3Number = temp.ToIntegerValue();
            if (ValidateMVR3ForReprint(NewMVR3Number))
            {
	            lngNoFeeDupRegMVR3Number = NewMVR3Number;
            }
            else
	        {
		        MessageBox.Show("There is no MVR3 with that number in inventory.", "No MVR3", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", false);
                return;
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeMVR3Number", NewMVR3Number);
            intLastMVR = NewMVR3Number;
            PrintMVR3Form();

            if (MotorVehicle.Statics.PrintMVR3 == true)
            {
                if (fecherFoundation.Information.Err().Number == 0)
                {
                    blnMVR3Done = true;
                    cmdReturn.Enabled = false;
                    CloseBox = false;
                    mnuReturn.Enabled = false;
                    cmdReasonCodes.Enabled = false;
                    btnHoldRegistration.Enabled = false;
                    txtMVR3Input.Enabled = false;
                    txtReasonsOnTheBottom.Enabled = false;
                    txtMessage.Enabled = false;

                    if (AllFormsPrinted())
                    {
                        cmdSave.Enabled = true;
                    }
                }
            }
            rsMVR3.Reset();
        }

        private void PrintMVR3Form()
        {
            rptMVR3Laser rpt = new rptMVR3Laser();
            rpt.IsPreReg = IsPreReg;
            rpt.PrintReportOnDotMatrix("MVR3PrinterName");
            rpt.Unload();
        }

        private void btnPrintMVR3_Click(object sender, EventArgs e)
        {
            DialogResult answer = 0;
            string temp = "";
            int NewMVR3Number = 0;

            if (blnLabelClicked)
            {
                return;
            }
            else
            {
                blnLabelClicked = true;
            }
            btnHoldRegistration.Enabled = false;

            if ((PrintPriorityMessageNeeded || ReasonMessageNeeded) && txtMessage.Text.Trim() == "")
            {
	            MessageBox.Show("You must enter a message before you may continue.", "Enter Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
	            blnLabelClicked = false;
	            return;
			}

            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
            {
	            blnLabelClicked = false;
				return;
            }

			if (blnMVR3Done)
            {
	            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT" ||
	                MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "RRT")
	            {
		            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == false &&
		                MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RENTAL") == true)
		            {
			            answer = MessageBox.Show(
				            "Are you trying to print out the No Fee Duplicate Registration for this Rental Vehicle?",
				            "Print Duplicate?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			            if (answer == DialogResult.Yes)
			            {
				            MotorVehicle.Statics.blnPrintingDupReg = true;
				            MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", true);
				            PrintNoFeeDupReg();
				            blnLabelClicked = false;
				            return;
			            }
		            }
	            }

	            var lastMVR3 = lngNoFeeDupRegMVR3Number > MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")
                    ? lngNoFeeDupRegMVR3Number
                    : MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");
                temp = Interaction.InputBox("Please input the New MVR3 Number for the Reprint." + "\r\n" + "\r\n" + "The last number used:  " + lastMVR3, "Next MVR3", FCConvert.ToString(lastMVR3 + 1));
                if (temp.Length > 8)
                {
	                blnLabelClicked = false;
	                return;
				}
	            
                NewMVR3Number = temp.ToIntegerValue();
                if (NewMVR3Number != 0)
                {
                    if (ValidateMVR3ForReprint(NewMVR3Number))
                    {
                        MotorVehicle.VoidMVR3(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"), MotorVehicle.Statics.rsFinal.Get_Fields_String("OpID"));
                        MotorVehicle.Statics.OldMVR3Number = FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        intLastMVR = MotorVehicle.Statics.OldMVR3Number;
                        MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", NewMVR3Number);
                        txtMVR3Input.Text = FCConvert.ToString(NewMVR3Number);
                        RefreshMVR3Preview();
                    }
                    else
                    {
	                    MessageBox.Show("There is no MVR3 with that number in inventory.", "No MVR3", MessageBoxButtons.OK, MessageBoxIcon.Error);
						blnLabelClicked = false;
                        return;
                    }
                }
                else
                {
                    blnLabelClicked = false;
                    return;
                }
            }
            else
            {
                if (ValidateMVR3(txtMVR3Input.Text.Trim().ToIntegerValue()))
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", txtMVR3Input.Text.Trim().ToIntegerValue());
                }
                else
                {
                    blnLabelClicked = false;
                    return;
                }
            }

            PrintMVR3Form();

            if (MotorVehicle.Statics.PrintMVR3 == true)
            {
                if (fecherFoundation.Information.Err().Number == 0)
                {
                    blnMVR3Done = true;
                    cmdReturn.Enabled = false;
                    CloseBox = false;
                    mnuReturn.Enabled = false;
                    cmdReasonCodes.Enabled = false;
                    btnHoldRegistration.Enabled = false;
                    txtMVR3Input.Enabled = false;
                    txtReasonsOnTheBottom.Enabled = false;
                    txtMessage.Enabled = false;
                    imgMVR3Printed.Visible = true;
                    if (AllFormsPrinted())
                    {
                        cmdSave.Enabled = true;
                    }
                }
            }

            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT" ||
                MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "RRT")
            {
	            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == false &&
	                MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RENTAL") == true)
	            {
		            answer = MessageBox.Show(
			            "Would you like to print a No Fee Duplicate Registration for this Rental Vehicle?",
			            "Print Duplicate?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
		            if (answer == DialogResult.No)
		            {
			            // do nothing
		            }
		            else
		            {
			            MotorVehicle.Statics.blnPrintingDupReg = true;
			            MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", true);
			            PrintNoFeeDupReg();
		            }
	            }
            }
            blnLabelClicked = false;
        }

        private void btnHoldRegistration_Click(object sender, EventArgs e)
        {
            var rsTitleUseTax = new clsDRWrapper();
            var rsPlate = new clsDRWrapper();
            MotorVehicle.Statics.rsFinalCompare.Edit();
            FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
            var rsHold = new clsDRWrapper();
            var rsTitle = new clsDRWrapper();

            blnLabelClicked = true;
            cmdSave.Enabled = false;

            try
            {
                MotorVehicle.Statics.strSql = "SELECT * FROM HeldRegistrationMaster WHERE ID = 0";
                rsHold.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsHold.EndOfFile() != true && rsHold.BeginningOfFile() != true)
                {
                    rsHold.MoveFirst();
                    rsHold.MoveLast();
                    rsHold.AddNew();
                }
                else
                {
                    rsHold.AddNew();
                }
                for (MotorVehicle.Statics.fnx = 0; MotorVehicle.Statics.fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; MotorVehicle.Statics.fnx++)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "ID")
                    {
                        rsHold.Set_Fields("MasterID", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                    }
                    else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "MasterID")
                    {
                        rsHold.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                    }
                    else
                    {
                        rsHold.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));
                    }
                }
                rsHold.Set_Fields("Status", "H");
                rsHold.Set_Fields("MVR3", 0);
                rsHold.Set_Fields("MonthStickerCharge", 0);
                rsHold.Set_Fields("MonthStickerNoCharge", 0);
                rsHold.Set_Fields("MonthStickerMonth", 0);
                rsHold.Set_Fields("MonthStickerNumber", 0);
                rsHold.Set_Fields("YearStickerCharge", 0);
                rsHold.Set_Fields("YearStickerNoCharge", 0);
                rsHold.Set_Fields("YearStickerYear", 0);
                rsHold.Set_Fields("YearStickerNumber", 0);
                rsHold.Set_Fields("DateUpdated", DateTime.Today);
                rsHold.Update();
                rsHold.AddNew();
                MotorVehicle.Statics.rsFinalCompare.Set_Fields("Status", "FC");
                for (MotorVehicle.Statics.fnx = 0; MotorVehicle.Statics.fnx <= MotorVehicle.Statics.rsFinalCompare.FieldsCount - 1; MotorVehicle.Statics.fnx++)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "Plate" && (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.NewToTheSystem))
                    {
                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexValue(MotorVehicle.Statics.fnx))) == "")
                        {
                            rsHold.Set_Fields(MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        }
                        else
                        {
                            rsHold.Set_Fields(MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));
                        }
                    }
                    else
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "ID")
                        {
                            rsHold.Set_Fields("MasterID", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                        }
                        else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "MasterID")
                        {
                            rsHold.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("ID"));
                        }
                        else
                        {
                            rsHold.Set_Fields(MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));
                        }
                    }
                }
                rsHold.Set_Fields("DateUpdated", DateTime.Today);
                rsHold.Update();

                Hold_Plate_From_Inventory(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")));
                rsTitleUseTax.OpenRecordset("SELECT * FROM HeldTitleUseTax WHERE Plate = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'");
                if (rsTitleUseTax.EndOfFile() != true && rsTitleUseTax.BeginningOfFile() != true)
                {
                    rsTitleUseTax.MoveLast();
                    rsTitleUseTax.MoveFirst();
                    rsTitleUseTax.Edit();
                }
                else
                {
                    rsTitleUseTax.AddNew();
                }
                rsTitleUseTax.Set_Fields("HoldID", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                rsTitleUseTax.Set_Fields("plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
                rsTitleUseTax.Set_Fields("UseTaxID", MotorVehicle.Statics.lngUTCAddNewID);
                rsTitleUseTax.Set_Fields("TitleID", MotorVehicle.Statics.lngCTAAddNewID);
                rsTitleUseTax.Set_Fields("PrintTitle", MotorVehicle.Statics.NeedToPrintCTA ? 1 : 0);

                rsTitleUseTax.Set_Fields("PrintUseTax", MotorVehicle.Statics.NeedToPrintUseTax ? 1 : 0);

                rsTitleUseTax.Set_Fields("PrintTitleDouble", MotorVehicle.Statics.PrintDouble ? 1 : 0);
                rsTitleUseTax.Update();
                rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
                if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                {
                    if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
                    {
                        MotorVehicle.Statics.rsDoubleTitle.Update();
                    }
                }
                MotorVehicle.Statics.rsFinal.Reset();
                rsHold.Reset();
                MotorVehicle.Statics.rsFinalCompare.Reset();
                rsPlate.Reset();
                MotorVehicle.Statics.rsDoubleTitle.Reset();
            }
            finally
            {
                rsTitle.DisposeOf();
                rsTitleUseTax.DisposeOf();
                rsPlate.DisposeOf();
                rsHold.DisposeOf();
            }
            FCGlobal.Screen.MousePointer = 0;
            blnLabelClicked = false;
            frmSavePrint.InstancePtr.Close();
            frmPreview.InstancePtr.Unload();
            frmDataInput.InstancePtr.Unload();
        }

        private void Hold_Plate_From_Inventory(string plate)
        {
            clsDRWrapper rsPL = new clsDRWrapper();

            try
            {
                rsPL.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'A' AND FormattedInventory = '" + plate + "'");
                if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
                {
                    rsPL.MoveLast();
                    rsPL.MoveFirst();
                    rsPL.Edit();
                    rsPL.Set_Fields("Status", "H");
                    rsPL.Update();
                }
            }
            finally
            {
                rsPL.DisposeOf();
            }
        }

        private void btnPrintUseTax_Click(object sender, EventArgs e)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
	            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
	            {
		            return;
	            }

				blnLabelClicked = true;

				int intLeaseUseTaxFormVersion = 0;
                rsLoad.OpenRecordset("select * from WMV", "twmv0000.vb1");
                if (!rsLoad.EndOfFile())
                    intLeaseUseTaxFormVersion = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("LeaseUseTaxFormVersion"))));

                PrintUseTaxForm(intLeaseUseTaxFormVersion);
				
                if (MotorVehicle.Statics.blnPrintUseTax)
                {
                    blnUseTaxDone = true;
                    imgUseTaxPrinted.Visible = true;
                    if (AllFormsPrinted()) cmdSave.Enabled = true;
                }

                fecherFoundation.Information.Err().Clear();
            }
            finally
            {
                rsLoad.DisposeOf();
            }

            blnLabelClicked = false;
        }

        private static void PrintUseTaxForm(int intLeaseUseTaxFormVersion)
        {
            if (MotorVehicle.IsLease())
                PrintLeaseUseTaxForm(intLeaseUseTaxFormVersion);
            else
                PrintPurchaseUseTaxForm();
        }

        private static void PrintPurchaseUseTaxForm()
        {
            if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
            {
                MessageBox.Show("Old Versions of the Use Tax Certificate are no longer supported. Please update the Use Tax form settings.");
            }
            else
            {
                rptUseTaxCert2017.InstancePtr.TestPrint = false;
                rptUseTaxCert2017.InstancePtr.PageSettings.Duplex = Duplex.Vertical;
                rptUseTaxCert2017.InstancePtr.PrintUTCForm = modRegistry.GetRegistryKey("USETAX_PRINTFORM", "MOTOR_VEHICLE") == "Y";
                rptUseTaxCert2017.InstancePtr.Run();

                if (modRegistry.GetRegistryKey("USETAX_DUPLEX", "MOTOR_VEHICLE") == "Y")
                {
                    rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName");
                }
                else
                {
                    rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName", printerSettings: new DirectPrintingParams() { FromPage = 1, ToPage = 1 });
                    MessageBox.Show("Printing front side. When finished, insert the paper to print the back of the form and click OK.");
                    rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName", printerSettings: new DirectPrintingParams() { FromPage = 2, ToPage = 2 });
                }

                rptUseTaxCert2017.InstancePtr.Unload();
            }
        }

        private static void PrintLeaseUseTaxForm(int formVersion)
        {

            if (modPrinterFunctions.PrintXsForAlignment("This 'X' should have printed next to the line USE TAX CERTIFICATE." + "\r\n" + "If this is correct press 'Yes', to reprint the 'X' press 'No', to quit press 'Cancel'.", 52, 1, MotorVehicle.Statics.gstrCTAPrinterName) != DialogResult.Cancel)
            {
                if (formVersion == 1)
                {
                    rptLeaseUseTaxCertificateV2.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                    rptLeaseUseTaxCertificateV2.InstancePtr.Unload();
                }
                else
                {
                    rptLeaseUseTaxCertificate.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                    rptLeaseUseTaxCertificate.InstancePtr.Unload();
                }
            }
            else
            {
                MotorVehicle.Statics.blnPrintUseTax = false;
            }
        }

        private void txtMessage_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RefreshMVR3Preview();
        }

        private static void SetDoubleCTACustomer()
        {
	        string customer;

	        do
	        {
		        customer = Interaction.InputBox("You must enter the customer name", "Double CTA", null);

		        if (customer.Trim()
			            .Length
		            > 0)
			        break;

		        MessageBox.Show("You must enter a reason before you can save this registration.", "Invalid Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
	        } while (true);


        }

        private class PPCode
        {
			public string Code { get; set; }
			public string Priority { get; set; }
        }

	}
}
