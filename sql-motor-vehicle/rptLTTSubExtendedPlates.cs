//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTSubExtendedPlates.
	/// </summary>
	public partial class rptLTTSubExtendedPlates : BaseSectionReport
	{
		public rptLTTSubExtendedPlates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptLTTSubExtendedPlates_ReportEnd;
		}

        private void RptLTTSubExtendedPlates_ReportEnd(object sender, EventArgs e)
        {
			rsAM.DisposeOf();
            rs.DisposeOf();

		}

        public static rptLTTSubExtendedPlates InstancePtr
		{
			get
			{
				return (rptLTTSubExtendedPlates)Sys.GetInstance(typeof(rptLTTSubExtendedPlates));
			}
		}

		protected rptLTTSubExtendedPlates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLTTSubExtendedPlates	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;
		int lngPK1;
		int lngPK2;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsAM.EndOfFile())
				{
					lblNoInfo.Visible = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			else
			{
				rsAM.MoveNext();
				if (rsAM.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			if (frmReportLongTerm.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				MotorVehicle.Statics.strSql = "SELECT FleetMaster.CompanyCode, LongTermTrailerRegistrations.* FROM FleetMaster INNER JOIN LongTermTrailerRegistrations ON FleetMaster.FleetNumber = LongTermTrailerRegistrations.FleetNumber WHERE Status <> 'V' AND PeriodCloseoutID < 1 AND RegistrationType = 'RRR' AND Extension = true ORDER BY FleetMaster.CompanyCode, LongTermTrailerRegistrations.OldPlate";
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate BETWEEN '" + frmReportLongTerm.InstancePtr.cboStart.Text + "' AND '" + frmReportLongTerm.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(MotorVehicle.Statics.strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
				MotorVehicle.Statics.strSql = "SELECT FleetMaster.CompanyCode, LongTermTrailerRegistrations.* FROM FleetMaster INNER JOIN LongTermTrailerRegistrations ON FleetMaster.FleetNumber = LongTermTrailerRegistrations.FleetNumber WHERE Status <> 'V' AND RegistrationType = 'RRR' AND Extension = true AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY FleetMaster.CompanyCode, LongTermTrailerRegistrations.OldPlate";
			}
			rsAM.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldPlate As object	OnWrite(object, string)
			// vbPorter upgrade warning: fldCompanyCode As object	OnWrite(object, string)
			if (rsAM.EndOfFile() != true)
			{
				fldPlate.Text = rsAM.Get_Fields_String("OldPlate");
				fldCompanyCode.Text = rsAM.Get_Fields_String("CompanyCode");
			}
			else
			{
				fldPlate.Text = "";
				fldCompanyCode.Text = "";
			}
		}

		private void rptLTTSubExtendedPlates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLTTSubExtendedPlates properties;
			//rptLTTSubExtendedPlates.Caption	= "ActiveReport1";
			//rptLTTSubExtendedPlates.Left	= 0;
			//rptLTTSubExtendedPlates.Top	= 0;
			//rptLTTSubExtendedPlates.Width	= 12465;
			//rptLTTSubExtendedPlates.Height	= 5490;
			//rptLTTSubExtendedPlates.StartUpPosition	= 3;
			//rptLTTSubExtendedPlates.SectionData	= "rptLTTSubExtendedPlates.dsx":0000;
			//End Unmaped Properties
		}
	}
}
