//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using System.Drawing.Printing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptUseTaxCert2017.
	/// </summary>
	public partial class rptUseTaxCert2017 : BaseSectionReport
	{
		public rptUseTaxCert2017()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "STMV6102017";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptUseTaxCert2017_ReportEnd;
		}

        private void RptUseTaxCert2017_ReportEnd(object sender, EventArgs e)
        {
            rsUseTax.DisposeOf();
        }

        public static rptUseTaxCert2017 InstancePtr
		{
			get
			{
				return (rptUseTaxCert2017)Sys.GetInstance(typeof(rptUseTaxCert2017));
			}
		}

		protected rptUseTaxCert2017 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUseTaxCert2017	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public bool PrintUTCForm;
		public bool TestPrint;
		clsDRWrapper rsUseTax = new clsDRWrapper();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			string strTemp;
			if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.gstrUseTaxPrinterName) != "")
			{
				this.Document.Printer.PrinterName = MotorVehicle.Statics.gstrUseTaxPrinterName;
			}
			/*? For Each */
			foreach (FCPrinter p in FCGlobal.Printers)
			{
				if (p.DeviceName == MotorVehicle.Statics.gstrUseTaxPrinterName)
				{
					if (tempPrinter.DriverName == "EPSON24")
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 10cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
						}
					}
					break;
				}
			}
			/*? Next */
			strTemp = MotorVehicle.GetComputerMVSetting("UTCTopAdjustment");
			if (strTemp != "")
			{
				this.PageSettings.Margins.Top += FCConvert.ToSingle(Conversion.Val(strTemp) * 1440F) / 1440F;
			}
			strTemp = MotorVehicle.GetComputerMVSetting("UTCLeftAdjustment");
			if (strTemp != "")
			{
				this.PageSettings.Margins.Left += FCConvert.ToSingle(Conversion.Val(strTemp) * 1440F) / 1440F;
				if ((this.PrintWidth + this.PageSettings.Margins.Left + this.PageSettings.Margins.Right) > this.PageSettings.PaperWidth)
				{
					this.PageSettings.Margins.Right = this.PageSettings.PaperWidth - (this.PrintWidth + this.PageSettings.Margins.Left);
				}
			}
			if (TestPrint)
			{
				return;
			}
            //if (MotorVehicle.Statics.PrintDouble) 
               // this.PageSettings.Duplex = Duplex.Horizontal;
            if (this.Document.Printer.PrinterSettings.CanDuplex)  this.Document.Printer.PrinterSettings.Duplex = Duplex.Horizontal;

			rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID), "TWMV0000.vb1");
			if (rsUseTax.EndOfFile())
			{
				MotorVehicle.Statics.blnPrintUseTax = false;
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// this sub fills the information on the first page of the 2017 Use Tax Certificate
			double dblTaxAmount;
			double dblPurchasePrice;
			double dblAllowance;
			string SellerAddress;
			string strRegLName = "";
			string strRegFName = "";
			imgPDFpage1.Visible = PrintUTCForm;
			// imgPDFpage1.Height = 0
			if (TestPrint)
			{
				TestPrintPage1();
				return;
			}
			fldType1.Text = rsUseTax.Get_Fields_String("PurchasedType");
			fldType2.Text = rsUseTax.Get_Fields_String("TradedType");
			fldMake1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
			fldModel1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
			fldYear1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Year"));
			if (Conversion.Val(rsUseTax.Get_Fields_String("TradedYear")) != 0)
			{
				fldMake2.Text = rsUseTax.Get_Fields_String("TradedMake");
				fldModel2.Text = rsUseTax.Get_Fields_String("TradedModel");
				fldYear2.Text = rsUseTax.Get_Fields_String("TradedYear");
			}
			if (MotorVehicle.Statics.FleetUseTax)
			{
				fldVin1.Text = "S.A.L.";
			}
			else
			{
				fldVin1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
			}
			fldVin2.Text = rsUseTax.Get_Fields_String("TradedVIN");
			fldSellerName.Text = rsUseTax.Get_Fields_String("SellerName");
			if (Information.IsDate(rsUseTax.Get_Fields("TradedDate")))
			{
				fldDateofTransfer.Text = Strings.Format(rsUseTax.Get_Fields_DateTime("TradedDate"), "MM/dd/yyyy");
			}
			else
			{
				fldDateofTransfer.Text = "";
			}
			SellerAddress = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerAddress1"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerCity"))) + " " + rsUseTax.Get_Fields_String("SellerState") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerZipAndZip4")));
			fldSellerAddress.Text = fecherFoundation.Strings.Trim(SellerAddress);
			dblPurchasePrice = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("PurchasePrice")) == false)
				dblPurchasePrice = Conversion.Val(rsUseTax.Get_Fields_Decimal("PurchasePrice"));
			fldFullPurchasePrice.Text = Strings.Format(dblPurchasePrice, "#,###.00");
			dblAllowance = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("Allowance")) == false)
				dblAllowance = Conversion.Val(rsUseTax.Get_Fields_Decimal("Allowance"));
			fldAllowance.Text = Strings.Format(dblAllowance, "#,###.00");
			fldNetAmount.Text = Strings.Format(dblPurchasePrice - dblAllowance, "#,###.00");
			dblTaxAmount = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("TaxAmount")) == false)
				dblTaxAmount = Conversion.Val(rsUseTax.Get_Fields_Decimal("TaxAmount"));
			fldUseTax.Text = Strings.Format(dblTaxAmount, "#,##0.00");
			if (FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptCode")) != "X")
			{
				fldExemptType.Text = rsUseTax.Get_Fields_String("ExemptCode");
			}
			else
			{
				fldExemptType.Text = "G";
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I")
			{
				strRegFName = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Reg1FirstName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Reg1MI"))));
				strRegLName = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Reg1LastName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Reg1Designation"))));
			}
			else
			{
				strRegFName = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Owner1")));
				strRegLName = "";
			}
			if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strRegFName)) != fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserFName")))) || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(strRegLName)) != fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserLName")))))
			{
				fldDifferentOwner.Text = fecherFoundation.Strings.Trim(strRegFName) + " " + fecherFoundation.Strings.Trim(strRegLName);
			}
			fldLienHolderName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1Name")));
			fldLienHolderAddress.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1Address1"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1City"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1State"))) + " " + fecherFoundation.Strings.RTrim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1ZipAndZip4")));
			fldPurchaserFName.Text = rsUseTax.Get_Fields_String("PurchaserFName");
			fldPurchaserLName.Text = rsUseTax.Get_Fields_String("PurchaserLName");
			fldPurchaserSocialSec.Text = rsUseTax.Get_Fields_String("PurchaserSSN");
			fldPurchaserAddress.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserAddress")));
			fldPurchaserCity.Text = rsUseTax.Get_Fields_String("PurchaserCity");
			fldPurchaserState.Text = rsUseTax.Get_Fields_String("PurchaserState");
			fldPurchaserZip.Text = rsUseTax.Get_Fields_String("PurchaserZipAndZip4");
			if (MotorVehicle.Statics.FleetUseTax)
			{
				fldClassPlate.Text = "S.A.L.";
			}
			else
			{
				fldClassPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");
			}
			fldRegDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			fldTaxAmount.Text = Strings.Format(Strings.Format(dblTaxAmount, "###,###.00"), "@@@@@@@@@@");
			fldDatePaid1.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			string strExemptCode = "";
			imgPDFpage2.Visible = PrintUTCForm;
			if (TestPrint)
			{
				TestPrintPage2();
				return;
			}
			if (FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptCode")) != "X")
			{
				strExemptCode = FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptCode"));
			}
			else
			{
				strExemptCode = "G";
			}
			if (strExemptCode == "A")
			{
				// EXEMPT ORGANIZATION
				fldExemptTypeA.Text = "X";
				fldExemptNumber.Text = rsUseTax.Get_Fields_String("ExemptNumber");
			}
			else if (strExemptCode == "B")
			{
				// PREVIOUSLY USED OUTSIDE OF MAINE
				fldExemptTypeB.Text = "X";
				fldWhereRegistered.Text = rsUseTax.Get_Fields_String("ExemptPreviousState");
				fldRegNumber.Text = rsUseTax.Get_Fields_String("ExemptRegistrationNumber");
				fldDateOfOriginalReg.Text = Strings.Format(rsUseTax.Get_Fields_DateTime("ExemptDate"), "MM/dd/yyyy");
			}
			else if (strExemptCode == "C")
			{
				fldExemptTypeC.Text = "X";
				// TAX PAID IN ANOTHER JURISDICTION
				fldOtherStateTax.Text = rsUseTax.Get_Fields_String("ExemptState");
				fldOtherAmount.Text = Strings.Format(Strings.Format(rsUseTax.Get_Fields_Decimal("ExemptAmountPaid"), "###,###.00"), "@@@@@@@@@@");
			}
			else if (strExemptCode == "D")
			{
				// AMPUTEE VETERANS
				fldExemptTypeD.Text = "X";
			}
			else if (strExemptCode == "E")
			{
				// RENTAL - NEED THE SALES TAX REGISTRATION NUMBER
				fldExemptTypeE.Text = "X";
				fldRentalTaxReg.Text = rsUseTax.Get_Fields_String("ExemptSalesTaxNumber");
			}
			else if (strExemptCode == "F")
			{
				// INTERSTATE COMMERCE
				fldExemptTypeF.Text = "X";
			}
			else if (strExemptCode == "G")
			{
				// OTHER
				fldExemptTypeG.Text = "X";
			}
			MotorVehicle.Statics.blnPrintUseTax = true;
		}

		private void TestPrintPage1()
		{
			fldType1.Text = "AUTO";
			fldType2.Text = "AUTO";
			fldMake1.Text = "CHEV";
			fldModel1.Text = "EQUINO";
			fldYear1.Text = "2017";
			fldMake2.Text = "CHEV";
			fldModel2.Text = "EQUINO";
			fldYear2.Text = "2008";
			fldVin1.Text = "2GNAXSEV3J6314953";
			fldVin2.Text = "2GNFLGE31D6390933";
			fldSellerName.Text = "BILL DODGE AUTO GROUP";
			fldDateofTransfer.Text = "07/07/2020";
			fldSellerAddress.Text = "2 SAUNDERS WAY WESTBROOK ME 04092";
			fldFullPurchasePrice.Text = "25,500.00";
			fldAllowance.Text = "3,500.00";
			fldNetAmount.Text = "22,000.00";
			fldUseTax.Text = "1,210.00";
			fldExemptType.Text = "X";
			fldDifferentOwner.Text = "RONALD L MCDONALD JR";
			fldLienHolderName.Text = "CUMBERLAND COUNTY FCU";
			fldLienHolderAddress.Text = "222 PORTLAND RD FALMOUTH ME 04086";
			fldPurchaserFName.Text = "RONALD L";
			fldPurchaserLName.Text = "MCDONALD SR";
			fldPurchaserSocialSec.Text = "000-00-0000";
			fldPurchaserAddress.Text = "117 MIDDLE ST";
			fldPurchaserCity.Text = "CUMBERLAND";
			fldPurchaserState.Text = "ME";
			fldPurchaserZip.Text = "04077";
			fldClassPlate.Text = "SW 8888XX";
			fldRegDate.Text = "05/05/18";
			fldTaxAmount.Text = "1,210.00";
			fldDatePaid1.Text = "05/05/2018";
		}

		private void TestPrintPage2()
		{
			// A - EXEMPT ORGANIZATION
			fldExemptTypeA.Text = "X";
			fldExemptNumber.Text = "9999999";
			// B - PREVIOUSLY USED OUTSIDE OF MAINE
			fldExemptTypeB.Text = "X";
			fldWhereRegistered.Text = "NH";
			fldRegNumber.Text = "9999999";
			fldDateOfOriginalReg.Text = "08/08/2020";
			// C - TAX PAID IN ANOTHER JURISDICTION
			fldExemptTypeC.Text = "X";
			fldOtherStateTax.Text = "NH";
			fldOtherAmount.Text = "2,000.00";
			// D - AMPUTEE VETERANS
			fldExemptTypeD.Text = "X";
			// E - RENTAL - NEED THE SALES TAX REGISTRATION NUMBER
			fldExemptTypeE.Text = "X";
			fldRentalTaxReg.Text = "9999999";
			// F - INTERSTATE COMMERCE
			fldExemptTypeF.Text = "X";
			// G - OTHER
			fldExemptTypeG.Text = "X";
		}
	}
}
