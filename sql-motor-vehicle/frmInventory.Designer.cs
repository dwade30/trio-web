//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmInventory.
	/// </summary>
	partial class frmInventory
	{
		public fecherFoundation.FCComboBox cmbStart;
		public fecherFoundation.FCComboBox cmbInventoryType;
		public fecherFoundation.FCLabel lblInventoryType;
		public fecherFoundation.FCComboBox cmbSelection;
		public fecherFoundation.FCLabel lblSelection;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCListBox lstPlates;
		public fecherFoundation.FCListBox lstItems;
		public fecherFoundation.FCButton cmdPrintSelected;
		public fecherFoundation.FCButton cmdPrintAll;
		public fecherFoundation.FCButton cmdChange;
		public fecherFoundation.FCCommonDialog dlg1;
		public fecherFoundation.FCLabel lblItemType;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbStart = new fecherFoundation.FCComboBox();
            this.cmbInventoryType = new fecherFoundation.FCComboBox();
            this.lblInventoryType = new fecherFoundation.FCLabel();
            this.cmbSelection = new fecherFoundation.FCComboBox();
            this.lblSelection = new fecherFoundation.FCLabel();
            this.vs1 = new fecherFoundation.FCGrid();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.lstPlates = new fecherFoundation.FCListBox();
            this.lstItems = new fecherFoundation.FCListBox();
            this.cmdPrintSelected = new fecherFoundation.FCButton();
            this.cmdPrintAll = new fecherFoundation.FCButton();
            this.cmdChange = new fecherFoundation.FCButton();
            this.dlg1 = new fecherFoundation.FCCommonDialog();
            this.lblItemType = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChange)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 709);
            this.BottomPanel.Size = new System.Drawing.Size(784, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbStart);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.cmdAdd);
            this.ClientArea.Controls.Add(this.lstItems);
            this.ClientArea.Controls.Add(this.cmbInventoryType);
            this.ClientArea.Controls.Add(this.lblInventoryType);
            this.ClientArea.Controls.Add(this.cmdPrintSelected);
            this.ClientArea.Controls.Add(this.cmdPrintAll);
            this.ClientArea.Controls.Add(this.cmbSelection);
            this.ClientArea.Controls.Add(this.lblSelection);
            this.ClientArea.Controls.Add(this.cmdChange);
            this.ClientArea.Controls.Add(this.lblItemType);
            this.ClientArea.Controls.Add(this.lstPlates);
            this.ClientArea.Size = new System.Drawing.Size(804, 606);
            this.ClientArea.Controls.SetChildIndex(this.lstPlates, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblItemType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdChange, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSelection, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbSelection, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrintAll, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrintSelected, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblInventoryType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbInventoryType, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstItems, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdAdd, 0);
            this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbStart, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(804, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(247, 28);
            this.HeaderText.Text = "Inventory Maintenance";
            // 
            // cmbStart
            // 
            this.cmbStart.Items.AddRange(new object[] {
            "Start"});
            this.cmbStart.Location = new System.Drawing.Point(597, 30);
            this.cmbStart.Name = "cmbStart";
            this.cmbStart.Size = new System.Drawing.Size(162, 40);
            this.cmbStart.TabIndex = 1001;
            // 
            // cmbInventoryType
            // 
            this.cmbInventoryType.Items.AddRange(new object[] {
            "MVR3s",
            "Plates",
            "Double Year Stickers",
            "Double Month Stickers",
            "Single Year Stickers",
            "Single Month Stickers",
            "Decals",
            "Boosters",
            "MVR10s",
            "Combination Stickers"});
            this.cmbInventoryType.Location = new System.Drawing.Point(231, 30);
            this.cmbInventoryType.Name = "cmbInventoryType";
            this.cmbInventoryType.Size = new System.Drawing.Size(304, 40);
            this.cmbInventoryType.TabIndex = 33;
            this.cmbInventoryType.SelectedIndexChanged += new System.EventHandler(this.optInventoryType_CheckedChanged);
            // 
            // lblInventoryType
            // 
            this.lblInventoryType.AutoSize = true;
            this.lblInventoryType.Location = new System.Drawing.Point(30, 44);
            this.lblInventoryType.Name = "lblInventoryType";
            this.lblInventoryType.Size = new System.Drawing.Size(198, 15);
            this.lblInventoryType.TabIndex = 34;
            this.lblInventoryType.Text = "SELECT THE INVENTORY ITEM";
            // 
            // cmbSelection
            // 
            this.cmbSelection.Items.AddRange(new object[] {
            "All",
            "Group 1",
            "Group 2",
            "Group 3",
            "Group 4",
            "Group 5",
            "Group 6",
            "Group 7",
            "Group 8",
            "Group 9",
            "Group 10",
            "Group 11",
            "Group 12"});
            this.cmbSelection.Location = new System.Drawing.Point(231, 80);
            this.cmbSelection.Name = "cmbSelection";
            this.cmbSelection.Size = new System.Drawing.Size(304, 40);
            this.cmbSelection.TabIndex = 35;
            this.cmbSelection.Text = "All";
            this.cmbSelection.SelectedIndexChanged += new System.EventHandler(this.optSelection_CheckedChanged);
            // 
            // lblSelection
            // 
            this.lblSelection.AutoSize = true;
            this.lblSelection.Location = new System.Drawing.Point(30, 94);
            this.lblSelection.Name = "lblSelection";
            this.lblSelection.Size = new System.Drawing.Size(119, 15);
            this.lblSelection.TabIndex = 36;
            this.lblSelection.Text = "GROUPS TO VIEW";
            // 
            // vs1
            // 
            this.vs1.Cols = 8;
            this.vs1.ExtendLastCol = true;
            this.vs1.FixedCols = 0;
            this.vs1.Location = new System.Drawing.Point(30, 446);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersVisible = false;
            this.vs1.Rows = 1;
            this.vs1.ShowFocusCell = false;
            this.vs1.Size = new System.Drawing.Size(704, 263);
            this.vs1.TabIndex = 32;
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(30, 396);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(236, 40);
            this.cmdAdd.TabIndex = 24;
            this.cmdAdd.Text = "Add / Remove Inventory";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // lstPlates
            // 
            this.lstPlates.BackColor = System.Drawing.SystemColors.Window;
            this.lstPlates.Location = new System.Drawing.Point(30, 165);
            this.lstPlates.Name = "lstPlates";
            this.lstPlates.Size = new System.Drawing.Size(505, 171);
            this.lstPlates.Sorted = true;
            this.lstPlates.Sorting = Wisej.Web.SortOrder.Ascending;
            this.lstPlates.TabIndex = 8;
            this.lstPlates.DoubleClick += new System.EventHandler(this.lstPlates_DoubleClick);
            this.lstPlates.KeyPress += new Wisej.Web.KeyPressEventHandler(this.lstPlates_KeyPress);
            // 
            // lstItems
            // 
            this.lstItems.BackColor = System.Drawing.SystemColors.Window;
            this.lstItems.Location = new System.Drawing.Point(30, 165);
            this.lstItems.Name = "lstItems";
            this.lstItems.Size = new System.Drawing.Size(236, 171);
            this.lstItems.TabIndex = 9;
            this.lstItems.DoubleClick += new System.EventHandler(this.lstItems_DoubleClick);
            this.lstItems.KeyPress += new Wisej.Web.KeyPressEventHandler(this.lstItems_KeyPress);
            // 
            // cmdPrintSelected
            // 
            this.cmdPrintSelected.AppearanceKey = "actionButton";
            this.cmdPrintSelected.Location = new System.Drawing.Point(30, 346);
            this.cmdPrintSelected.Name = "cmdPrintSelected";
            this.cmdPrintSelected.Size = new System.Drawing.Size(236, 40);
            this.cmdPrintSelected.TabIndex = 27;
            this.cmdPrintSelected.Text = "Print Selected Inventory";
            this.cmdPrintSelected.Click += new System.EventHandler(this.cmdPrintSelected_Click);
            // 
            // cmdPrintAll
            // 
            this.cmdPrintAll.AppearanceKey = "actionButton";
            this.cmdPrintAll.Location = new System.Drawing.Point(286, 346);
            this.cmdPrintAll.Name = "cmdPrintAll";
            this.cmdPrintAll.Size = new System.Drawing.Size(184, 40);
            this.cmdPrintAll.TabIndex = 28;
            this.cmdPrintAll.Text = "Print All Inventory";
            this.cmdPrintAll.Click += new System.EventHandler(this.cmdPrintAll_Click);
            // 
            // cmdChange
            // 
            this.cmdChange.AppearanceKey = "actionButton";
            this.cmdChange.Location = new System.Drawing.Point(286, 396);
            this.cmdChange.Name = "cmdChange";
            this.cmdChange.Size = new System.Drawing.Size(240, 40);
            this.cmdChange.TabIndex = 26;
            this.cmdChange.Text = "Change Inventory Group";
            this.cmdChange.Click += new System.EventHandler(this.cmdChange_Click);
            // 
            // dlg1
            // 
            this.dlg1.Name = "dlg1";
            this.dlg1.Size = new System.Drawing.Size(0, 0);
            // 
            // lblItemType
            // 
            this.lblItemType.Location = new System.Drawing.Point(30, 130);
            this.lblItemType.Name = "lblItemType";
            this.lblItemType.Size = new System.Drawing.Size(153, 15);
            this.lblItemType.TabIndex = 31;
            // 
            // frmInventory
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(804, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmInventory";
            this.Text = " Inventory Maintenance";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmInventory_Load);
            this.Activated += new System.EventHandler(this.frmInventory_Activated);
            this.Resize += new System.EventHandler(this.frmInventory_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmInventory_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChange)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
    }
}