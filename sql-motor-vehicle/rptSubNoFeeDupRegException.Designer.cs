﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubNoFeeDupRegException.
	/// </summary>
	partial class rptSubNoFeeDupRegException
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSubNoFeeDupRegException));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldReciept = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistrant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldReciept)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldReciept,
            this.fldClassPlate,
            this.fldRegistrant,
            this.fldOpID,
            this.fldDate});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldReciept
			// 
			this.fldReciept.Height = 0.19F;
			this.fldReciept.Left = 0.0625F;
			this.fldReciept.Name = "fldReciept";
			this.fldReciept.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReciept.Text = "Field1";
			this.fldReciept.Top = 0.03125F;
			this.fldReciept.Width = 0.6875F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.19F;
			this.fldClassPlate.Left = 1.90625F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldClassPlate.Text = "Field1";
			this.fldClassPlate.Top = 0.03125F;
			this.fldClassPlate.Width = 1F;
			// 
			// fldRegistrant
			// 
			this.fldRegistrant.Height = 0.19F;
			this.fldRegistrant.Left = 3.375F;
			this.fldRegistrant.Name = "fldRegistrant";
			this.fldRegistrant.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldRegistrant.Text = "Field1";
			this.fldRegistrant.Top = 0.03125F;
			this.fldRegistrant.Width = 2.84375F;
			// 
			// fldOpID
			// 
			this.fldOpID.Height = 0.19F;
			this.fldOpID.Left = 6.53125F;
			this.fldOpID.Name = "fldOpID";
			this.fldOpID.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldOpID.Text = "Field1";
			this.fldOpID.Top = 0.03125F;
			this.fldOpID.Width = 0.5F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 0.9375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0.03125F;
			this.fldDate.Width = 0.75F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label2,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label10,
            this.Label11});
			this.GroupHeader1.Height = 0.6979167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.90625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label2.Text = "CLASS / PLATE";
			this.Label2.Top = 0.53125F;
			this.Label2.Width = 1.15625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label5.Text = "REGISTRANT\'S NAME";
			this.Label5.Top = 0.53125F;
			this.Label5.Width = 2.1875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.53125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.Label6.Text = "OPID";
			this.Label6.Top = 0.53125F;
			this.Label6.Width = 0.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label7.Text = "FORM #";
			this.Label7.Top = 0.53125F;
			this.Label7.Width = 0.6875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.75F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label10.Text = "---- NO FEE - DUPLICATE - SHORT TERM RENTAL ----";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 3.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.9375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label11.Text = "DATE";
			this.Label11.Top = 0.53125F;
			this.Label11.Width = 0.75F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label9,
            this.fldTotalCount});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label9.Text = "TOTAL NO FEE - DUPLICATE - SHORT TERM RENTAL:";
			this.Label9.Top = 0.0625F;
			this.Label9.Width = 3.75F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 3.875F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldTotalCount.Text = "Field1";
			this.fldTotalCount.Top = 0.0625F;
			this.fldTotalCount.Width = 0.75F;
			// 
			// rptSubNoFeeDupRegException
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.385417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldReciept)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReciept;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrant;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
	}
}
