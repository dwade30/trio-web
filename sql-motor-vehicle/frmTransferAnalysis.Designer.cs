//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTransferAnalysis.
	/// </summary>
	partial class frmTransferAnalysis
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblStateCreditPR;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblStateCreditHR;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCTextBox txtCreditMVR3Number;
		public fecherFoundation.FCButton cmdProcessCredit;
		public fecherFoundation.FCButton cmdQuitCredit;
		public Global.T2KBackFillDecimal txtLocalCredit;
		public Global.T2KBackFillDecimal txtStateCredit;
		public Global.T2KDateBox txtExpireDate;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraFinal;
		public fecherFoundation.FCCheckBox chkETO;
		public fecherFoundation.FCButton cmdProcessNew;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCLabel lblStateCreditPR_1;
		public fecherFoundation.FCLabel lblStateCreditPR_0;
		public fecherFoundation.FCLabel lblStateCreditPR_2;
		public fecherFoundation.FCLabel lblStateCreditPR_3;
		public fecherFoundation.FCLabel lblStateCreditHR_3;
		public fecherFoundation.FCLabel lblStateCreditHR_2;
		public fecherFoundation.FCLabel lblStateCreditHR_1;
		public fecherFoundation.FCLabel lblStateCreditHR_0;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel lblRegistrationTransfer;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel lblRegistrationTotal;
		public fecherFoundation.FCLabel lblRegistrationTax2Year;
		public fecherFoundation.FCLabel lblRegistrationCredit;
		public fecherFoundation.FCLabel lblRegistrationTax1Year;
		public fecherFoundation.FCLabel Label1_14;
		public fecherFoundation.FCLabel Label1_15;
		public fecherFoundation.FCLabel Label1_16;
		public fecherFoundation.FCLabel Label1_17;
		public fecherFoundation.FCLabel lblExciseTaxTotal;
		public fecherFoundation.FCLabel lblExciseTax2Year;
		public fecherFoundation.FCLabel lblExciseTaxCredit;
		public fecherFoundation.FCLabel lblExciseTax1Year;
		public fecherFoundation.FCLabel Label1_22;
		public fecherFoundation.FCLabel Label1_23;
		public fecherFoundation.FCLabel Label1_24;
		public fecherFoundation.FCLabel Label1_25;
		public fecherFoundation.FCLabel Label1_26;
		public fecherFoundation.FCLabel Label1_27;
		public fecherFoundation.FCLabel Label1_28;
		public fecherFoundation.FCLabel lblLocalTotal;
		public fecherFoundation.FCLabel lblLocalTax2Year;
		public fecherFoundation.FCLabel lblLocalTax1Year;
		public fecherFoundation.FCLabel Label1_32;
		public fecherFoundation.FCLabel Label1_33;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel Label1_34;
		public fecherFoundation.FCLabel Label1_35;
		public fecherFoundation.FCLabel Label1_36;
		public fecherFoundation.FCLabel lblLocalNR;
		public fecherFoundation.FCLabel lblRegNR;
		public fecherFoundation.FCLabel lblExciseNR;
		public fecherFoundation.FCLabel lblLocalT;
		public fecherFoundation.FCLabel lblRegT;
		public fecherFoundation.FCLabel lblExciseT;
		public fecherFoundation.FCLabel lblPayTodayNR;
		public fecherFoundation.FCLabel lblPerMonthNR;
		public fecherFoundation.FCLabel lblMonthsNR;
		public fecherFoundation.FCLabel lblPayTodayT;
		public fecherFoundation.FCLabel lblPerMonthT;
		public fecherFoundation.FCLabel lblMonthsT;
		public fecherFoundation.FCLabel lblPayLaterNR;
		public fecherFoundation.FCLabel lblPayLaterT;
		public fecherFoundation.FCLabel lblTotalNR;
		public fecherFoundation.FCLabel lblTotalT;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Command1 = new fecherFoundation.FCButton();
			this.txtCreditMVR3Number = new fecherFoundation.FCTextBox();
			this.cmdProcessCredit = new fecherFoundation.FCButton();
			this.cmdQuitCredit = new fecherFoundation.FCButton();
			this.txtLocalCredit = new Global.T2KBackFillDecimal();
			this.txtStateCredit = new Global.T2KBackFillDecimal();
			this.txtExpireDate = new Global.T2KDateBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraFinal = new fecherFoundation.FCFrame();
			this.chkETO = new fecherFoundation.FCCheckBox();
			this.cmdProcessNew = new fecherFoundation.FCButton();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.lblStateCreditPR_1 = new fecherFoundation.FCLabel();
			this.lblStateCreditPR_0 = new fecherFoundation.FCLabel();
			this.lblStateCreditPR_2 = new fecherFoundation.FCLabel();
			this.lblStateCreditPR_3 = new fecherFoundation.FCLabel();
			this.lblStateCreditHR_3 = new fecherFoundation.FCLabel();
			this.lblStateCreditHR_2 = new fecherFoundation.FCLabel();
			this.lblStateCreditHR_1 = new fecherFoundation.FCLabel();
			this.lblStateCreditHR_0 = new fecherFoundation.FCLabel();
			this.Label1_10 = new fecherFoundation.FCLabel();
			this.lblRegistrationTransfer = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.Label1_5 = new fecherFoundation.FCLabel();
			this.Label1_6 = new fecherFoundation.FCLabel();
			this.Label1_7 = new fecherFoundation.FCLabel();
			this.Label1_8 = new fecherFoundation.FCLabel();
			this.lblRegistrationTotal = new fecherFoundation.FCLabel();
			this.lblRegistrationTax2Year = new fecherFoundation.FCLabel();
			this.lblRegistrationCredit = new fecherFoundation.FCLabel();
			this.lblRegistrationTax1Year = new fecherFoundation.FCLabel();
			this.Label1_14 = new fecherFoundation.FCLabel();
			this.Label1_15 = new fecherFoundation.FCLabel();
			this.Label1_16 = new fecherFoundation.FCLabel();
			this.Label1_17 = new fecherFoundation.FCLabel();
			this.lblExciseTaxTotal = new fecherFoundation.FCLabel();
			this.lblExciseTax2Year = new fecherFoundation.FCLabel();
			this.lblExciseTaxCredit = new fecherFoundation.FCLabel();
			this.lblExciseTax1Year = new fecherFoundation.FCLabel();
			this.Label1_22 = new fecherFoundation.FCLabel();
			this.Label1_23 = new fecherFoundation.FCLabel();
			this.Label1_24 = new fecherFoundation.FCLabel();
			this.Label1_25 = new fecherFoundation.FCLabel();
			this.Label1_26 = new fecherFoundation.FCLabel();
			this.Label1_27 = new fecherFoundation.FCLabel();
			this.Label1_28 = new fecherFoundation.FCLabel();
			this.lblLocalTotal = new fecherFoundation.FCLabel();
			this.lblLocalTax2Year = new fecherFoundation.FCLabel();
			this.lblLocalTax1Year = new fecherFoundation.FCLabel();
			this.Label1_32 = new fecherFoundation.FCLabel();
			this.Label1_33 = new fecherFoundation.FCLabel();
			this.Label1_9 = new fecherFoundation.FCLabel();
			this.Label1_34 = new fecherFoundation.FCLabel();
			this.Label1_35 = new fecherFoundation.FCLabel();
			this.Label1_36 = new fecherFoundation.FCLabel();
			this.lblLocalNR = new fecherFoundation.FCLabel();
			this.lblRegNR = new fecherFoundation.FCLabel();
			this.lblExciseNR = new fecherFoundation.FCLabel();
			this.lblLocalT = new fecherFoundation.FCLabel();
			this.lblRegT = new fecherFoundation.FCLabel();
			this.lblExciseT = new fecherFoundation.FCLabel();
			this.lblPayTodayNR = new fecherFoundation.FCLabel();
			this.lblPerMonthNR = new fecherFoundation.FCLabel();
			this.lblMonthsNR = new fecherFoundation.FCLabel();
			this.lblPayTodayT = new fecherFoundation.FCLabel();
			this.lblPerMonthT = new fecherFoundation.FCLabel();
			this.lblMonthsT = new fecherFoundation.FCLabel();
			this.lblPayLaterNR = new fecherFoundation.FCLabel();
			this.lblPayLaterT = new fecherFoundation.FCLabel();
			this.lblTotalNR = new fecherFoundation.FCLabel();
			this.lblTotalT = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuitCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocalCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFinal)).BeginInit();
			this.fraFinal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkETO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(738, 0);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraFinal);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(738, 628);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(738, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(202, 30);
			this.HeaderText.Text = "Transfer Analysis";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// dlg1
			// 
			this.dlg1.Color = System.Drawing.Color.Black;
			this.dlg1.DefaultExt = null;
			this.dlg1.FilterIndex = ((short)(0));
			this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1.FontName = "Microsoft Sans Serif";
			this.dlg1.FontSize = 8.25F;
			this.dlg1.ForeColor = System.Drawing.Color.Black;
			this.dlg1.FromPage = 0;
			this.dlg1.Location = new System.Drawing.Point(0, 0);
			this.dlg1.Name = "dlg1";
			this.dlg1.PrinterSettings = null;
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			this.dlg1.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.dlg1, null);
			this.dlg1.ToPage = 0;
			// 
			// Frame1
			// 
			this.Frame1.BackColor = System.Drawing.Color.White;
			this.Frame1.Controls.Add(this.Command1);
			this.Frame1.Controls.Add(this.txtCreditMVR3Number);
			this.Frame1.Controls.Add(this.cmdProcessCredit);
			this.Frame1.Controls.Add(this.cmdQuitCredit);
			this.Frame1.Controls.Add(this.txtLocalCredit);
			this.Frame1.Controls.Add(this.txtStateCredit);
			this.Frame1.Controls.Add(this.txtExpireDate);
			this.Frame1.Controls.Add(this.Label5);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Location = new System.Drawing.Point(169, 149);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(479, 290);
			this.Frame1.TabIndex = 65;
			this.Frame1.Text = "Credit Amounts Need To Be Filled In";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// Command1
			// 
			this.Command1.AppearanceKey = "actionButton";
			this.Command1.Location = new System.Drawing.Point(236, 230);
			this.Command1.Name = "Command1";
			this.Command1.Size = new System.Drawing.Size(223, 40);
			this.Command1.TabIndex = 75;
			this.Command1.Text = "Continue W/O Analysis";
			this.ToolTip1.SetToolTip(this.Command1, null);
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
			// 
			// txtCreditMVR3Number
			// 
			this.txtCreditMVR3Number.MaxLength = 10;
			this.txtCreditMVR3Number.AutoSize = false;
			this.txtCreditMVR3Number.BackColor = System.Drawing.SystemColors.Window;
			this.txtCreditMVR3Number.LinkItem = null;
			this.txtCreditMVR3Number.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCreditMVR3Number.LinkTopic = null;
			this.txtCreditMVR3Number.Location = new System.Drawing.Point(326, 130);
			this.txtCreditMVR3Number.Name = "txtCreditMVR3Number";
			this.txtCreditMVR3Number.Size = new System.Drawing.Size(133, 40);
			this.txtCreditMVR3Number.TabIndex = 2;
			this.txtCreditMVR3Number.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtCreditMVR3Number, null);
			this.txtCreditMVR3Number.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCreditMVR3Number_KeyPress);
			this.txtCreditMVR3Number.Enter += new System.EventHandler(this.txtCreditMVR3Number_Enter);
			// 
			// cmdProcessCredit
			// 
			this.cmdProcessCredit.AppearanceKey = "actionButton";
			this.cmdProcessCredit.Location = new System.Drawing.Point(116, 230);
			this.cmdProcessCredit.Name = "cmdProcessCredit";
			this.cmdProcessCredit.Size = new System.Drawing.Size(100, 40);
			this.cmdProcessCredit.TabIndex = 4;
			this.cmdProcessCredit.Text = "Process";
			this.ToolTip1.SetToolTip(this.cmdProcessCredit, null);
			this.cmdProcessCredit.Click += new System.EventHandler(this.cmdProcessCredit_Click);
			// 
			// cmdQuitCredit
			// 
			this.cmdQuitCredit.AppearanceKey = "actionButton";
			this.cmdQuitCredit.Location = new System.Drawing.Point(20, 230);
			this.cmdQuitCredit.Name = "cmdQuitCredit";
			this.cmdQuitCredit.Size = new System.Drawing.Size(76, 40);
			this.cmdQuitCredit.TabIndex = 5;
			this.cmdQuitCredit.Text = "Quit";
			this.ToolTip1.SetToolTip(this.cmdQuitCredit, null);
			this.cmdQuitCredit.Click += new System.EventHandler(this.cmdQuitCredit_Click);
			// 
			// txtLocalCredit
			// 
			this.txtLocalCredit.Location = new System.Drawing.Point(326, 30);
			this.txtLocalCredit.MaxLength = 11;
			this.txtLocalCredit.Name = "txtLocalCredit";
			this.txtLocalCredit.Size = new System.Drawing.Size(133, 40);
			this.txtLocalCredit.TabIndex = 0;
			this.txtLocalCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtLocalCredit, "If the credit is half-rated, please input the original, un-halfrated amount.");
			this.txtLocalCredit.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLocalCredit_KeyPressEvent);
			// 
			// txtStateCredit
			// 
			this.txtStateCredit.Location = new System.Drawing.Point(326, 80);
			this.txtStateCredit.MaxLength = 11;
			this.txtStateCredit.Name = "txtStateCredit";
			this.txtStateCredit.Size = new System.Drawing.Size(133, 40);
			this.txtStateCredit.TabIndex = 1;
			this.txtStateCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtStateCredit, "If the credit is half-rated, please input the original, un-halfrated amount.");
			this.txtStateCredit.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStateCredit_KeyPressEvent);
			// 
			// txtExpireDate
			// 
			this.txtExpireDate.Location = new System.Drawing.Point(326, 180);
			this.txtExpireDate.Mask = "##/##/####";
			this.txtExpireDate.MaxLength = 10;
			this.txtExpireDate.Name = "txtExpireDate";
			this.txtExpireDate.Size = new System.Drawing.Size(133, 40);
			this.txtExpireDate.TabIndex = 3;
			this.txtExpireDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtExpireDate, null);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 194);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(277, 16);
			this.Label5.TabIndex = 69;
			this.Label5.Text = "ENTER THE EXPIRATION DATE FROM THE CREDIT";
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 144);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(234, 16);
			this.Label4.TabIndex = 68;
			this.Label4.Text = "ENTER THE MVR3 # FROM THE CREDIT";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 94);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(211, 16);
			this.Label3.TabIndex = 67;
			this.Label3.Text = "ENTER THE REGISTRATION CREDIT";
			this.ToolTip1.SetToolTip(this.Label3, "If the credit is half-rated, please input the original, un-halfrated amount.");
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(211, 16);
			this.Label2.TabIndex = 66;
			this.Label2.Text = "ENTER THE EXCISE TAX CREDIT";
			this.ToolTip1.SetToolTip(this.Label2, "If the credit is half-rated, please input the original, un-halfrated amount.");
			// 
			// fraFinal
			// 
			this.fraFinal.AppearanceKey = "groupBoxNoBorders";
			this.fraFinal.BackColor = System.Drawing.Color.White;
			this.fraFinal.Controls.Add(this.chkETO);
			this.fraFinal.Controls.Add(this.cmdProcessNew);
			this.fraFinal.Controls.Add(this.cmdQuit);
			this.fraFinal.Controls.Add(this.cmdPrint);
			this.fraFinal.Controls.Add(this.cmdProcess);
			this.fraFinal.Controls.Add(this.lblStateCreditPR_1);
			this.fraFinal.Controls.Add(this.lblStateCreditPR_0);
			this.fraFinal.Controls.Add(this.lblStateCreditPR_2);
			this.fraFinal.Controls.Add(this.lblStateCreditPR_3);
			this.fraFinal.Controls.Add(this.lblStateCreditHR_3);
			this.fraFinal.Controls.Add(this.lblStateCreditHR_2);
			this.fraFinal.Controls.Add(this.lblStateCreditHR_1);
			this.fraFinal.Controls.Add(this.lblStateCreditHR_0);
			this.fraFinal.Controls.Add(this.Label1_10);
			this.fraFinal.Controls.Add(this.lblRegistrationTransfer);
			this.fraFinal.Controls.Add(this.Label1_1);
			this.fraFinal.Controls.Add(this.Label1_2);
			this.fraFinal.Controls.Add(this.Label1_3);
			this.fraFinal.Controls.Add(this.Label1_4);
			this.fraFinal.Controls.Add(this.Label1_5);
			this.fraFinal.Controls.Add(this.Label1_6);
			this.fraFinal.Controls.Add(this.Label1_7);
			this.fraFinal.Controls.Add(this.Label1_8);
			this.fraFinal.Controls.Add(this.lblRegistrationTotal);
			this.fraFinal.Controls.Add(this.lblRegistrationTax2Year);
			this.fraFinal.Controls.Add(this.lblRegistrationCredit);
			this.fraFinal.Controls.Add(this.lblRegistrationTax1Year);
			this.fraFinal.Controls.Add(this.Label1_14);
			this.fraFinal.Controls.Add(this.Label1_15);
			this.fraFinal.Controls.Add(this.Label1_16);
			this.fraFinal.Controls.Add(this.Label1_17);
			this.fraFinal.Controls.Add(this.lblExciseTaxTotal);
			this.fraFinal.Controls.Add(this.lblExciseTax2Year);
			this.fraFinal.Controls.Add(this.lblExciseTaxCredit);
			this.fraFinal.Controls.Add(this.lblExciseTax1Year);
			this.fraFinal.Controls.Add(this.Label1_22);
			this.fraFinal.Controls.Add(this.Label1_23);
			this.fraFinal.Controls.Add(this.Label1_24);
			this.fraFinal.Controls.Add(this.Label1_25);
			this.fraFinal.Controls.Add(this.Label1_26);
			this.fraFinal.Controls.Add(this.Label1_27);
			this.fraFinal.Controls.Add(this.Label1_28);
			this.fraFinal.Controls.Add(this.lblLocalTotal);
			this.fraFinal.Controls.Add(this.lblLocalTax2Year);
			this.fraFinal.Controls.Add(this.lblLocalTax1Year);
			this.fraFinal.Controls.Add(this.Label1_32);
			this.fraFinal.Controls.Add(this.Label1_33);
			this.fraFinal.Controls.Add(this.Label1_9);
			this.fraFinal.Controls.Add(this.Label1_34);
			this.fraFinal.Controls.Add(this.Label1_35);
			this.fraFinal.Controls.Add(this.Label1_36);
			this.fraFinal.Controls.Add(this.lblLocalNR);
			this.fraFinal.Controls.Add(this.lblRegNR);
			this.fraFinal.Controls.Add(this.lblExciseNR);
			this.fraFinal.Controls.Add(this.lblLocalT);
			this.fraFinal.Controls.Add(this.lblRegT);
			this.fraFinal.Controls.Add(this.lblExciseT);
			this.fraFinal.Controls.Add(this.lblPayTodayNR);
			this.fraFinal.Controls.Add(this.lblPerMonthNR);
			this.fraFinal.Controls.Add(this.lblMonthsNR);
			this.fraFinal.Controls.Add(this.lblPayTodayT);
			this.fraFinal.Controls.Add(this.lblPerMonthT);
			this.fraFinal.Controls.Add(this.lblMonthsT);
			this.fraFinal.Controls.Add(this.lblPayLaterNR);
			this.fraFinal.Controls.Add(this.lblPayLaterT);
			this.fraFinal.Controls.Add(this.lblTotalNR);
			this.fraFinal.Controls.Add(this.lblTotalT);
			this.fraFinal.Controls.Add(this.Label1_0);
			this.fraFinal.ForeColor = System.Drawing.SystemColors.ScrollBar;
			this.fraFinal.Location = new System.Drawing.Point(30, 30);
			this.fraFinal.Name = "fraFinal";
			this.fraFinal.Size = new System.Drawing.Size(681, 547);
			this.fraFinal.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.fraFinal, null);
			// 
			// chkETO
			// 
			this.chkETO.ForeColor = System.Drawing.Color.FromName("@appWorkspace");
			this.chkETO.Location = new System.Drawing.Point(0, 470);
			this.chkETO.Name = "chkETO";
			this.chkETO.Size = new System.Drawing.Size(146, 27);
			this.chkETO.TabIndex = 76;
			this.chkETO.Text = "Excise Tax Only";
			this.ToolTip1.SetToolTip(this.chkETO, null);
			this.chkETO.CheckedChanged += new System.EventHandler(this.chkETO_CheckedChanged);
			// 
			// cmdProcessNew
			// 
			this.cmdProcessNew.AppearanceKey = "actionButton";
			this.cmdProcessNew.Location = new System.Drawing.Point(390, 507);
			this.cmdProcessNew.Name = "cmdProcessNew";
			this.cmdProcessNew.Size = new System.Drawing.Size(164, 40);
			this.cmdProcessNew.TabIndex = 70;
			this.cmdProcessNew.Text = "Process Re Reg";
			this.ToolTip1.SetToolTip(this.cmdProcessNew, null);
			this.cmdProcessNew.Click += new System.EventHandler(this.cmdProcessNew_Click);
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.Location = new System.Drawing.Point(98, 507);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(75, 40);
			this.cmdQuit.TabIndex = 7;
			this.cmdQuit.Text = "Quit";
			this.ToolTip1.SetToolTip(this.cmdQuit, null);
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "actionButton";
			this.cmdPrint.Location = new System.Drawing.Point(0, 507);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(78, 40);
			this.cmdPrint.TabIndex = 6;
			this.cmdPrint.Text = "Print";
			this.ToolTip1.SetToolTip(this.cmdPrint, null);
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "actionButton";
			this.cmdProcess.Location = new System.Drawing.Point(193, 507);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Size = new System.Drawing.Size(177, 40);
			this.cmdProcess.TabIndex = 8;
			this.cmdProcess.Text = "Process Transfer";
			this.ToolTip1.SetToolTip(this.cmdProcess, null);
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// lblStateCreditPR_1
			// 
			this.lblStateCreditPR_1.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditPR_1.Location = new System.Drawing.Point(67, 85);
			this.lblStateCreditPR_1.Name = "lblStateCreditPR_1";
			this.lblStateCreditPR_1.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditPR_1.TabIndex = 80;
			this.lblStateCreditPR_1.Text = "PR";
			this.ToolTip1.SetToolTip(this.lblStateCreditPR_1, null);
			this.lblStateCreditPR_1.Visible = false;
			// 
			// lblStateCreditPR_0
			// 
			this.lblStateCreditPR_0.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditPR_0.Location = new System.Drawing.Point(67, 60);
			this.lblStateCreditPR_0.Name = "lblStateCreditPR_0";
			this.lblStateCreditPR_0.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditPR_0.TabIndex = 79;
			this.lblStateCreditPR_0.Text = "PR";
			this.ToolTip1.SetToolTip(this.lblStateCreditPR_0, null);
			this.lblStateCreditPR_0.Visible = false;
			// 
			// lblStateCreditPR_2
			// 
			this.lblStateCreditPR_2.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditPR_2.Location = new System.Drawing.Point(291, 85);
			this.lblStateCreditPR_2.Name = "lblStateCreditPR_2";
			this.lblStateCreditPR_2.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditPR_2.TabIndex = 78;
			this.lblStateCreditPR_2.Text = "PR";
			this.ToolTip1.SetToolTip(this.lblStateCreditPR_2, null);
			this.lblStateCreditPR_2.Visible = false;
			// 
			// lblStateCreditPR_3
			// 
			this.lblStateCreditPR_3.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditPR_3.Location = new System.Drawing.Point(291, 60);
			this.lblStateCreditPR_3.Name = "lblStateCreditPR_3";
			this.lblStateCreditPR_3.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditPR_3.TabIndex = 77;
			this.lblStateCreditPR_3.Text = "PR";
			this.ToolTip1.SetToolTip(this.lblStateCreditPR_3, null);
			this.lblStateCreditPR_3.Visible = false;
			// 
			// lblStateCreditHR_3
			// 
			this.lblStateCreditHR_3.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditHR_3.Location = new System.Drawing.Point(291, 85);
			this.lblStateCreditHR_3.Name = "lblStateCreditHR_3";
			this.lblStateCreditHR_3.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditHR_3.TabIndex = 74;
			this.lblStateCreditHR_3.Text = "HR";
			this.ToolTip1.SetToolTip(this.lblStateCreditHR_3, null);
			this.lblStateCreditHR_3.Visible = false;
			// 
			// lblStateCreditHR_2
			// 
			this.lblStateCreditHR_2.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditHR_2.Location = new System.Drawing.Point(291, 60);
			this.lblStateCreditHR_2.Name = "lblStateCreditHR_2";
			this.lblStateCreditHR_2.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditHR_2.TabIndex = 73;
			this.lblStateCreditHR_2.Text = "HR";
			this.ToolTip1.SetToolTip(this.lblStateCreditHR_2, null);
			this.lblStateCreditHR_2.Visible = false;
			// 
			// lblStateCreditHR_1
			// 
			this.lblStateCreditHR_1.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditHR_1.Location = new System.Drawing.Point(67, 85);
			this.lblStateCreditHR_1.Name = "lblStateCreditHR_1";
			this.lblStateCreditHR_1.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditHR_1.TabIndex = 72;
			this.lblStateCreditHR_1.Text = "HR";
			this.ToolTip1.SetToolTip(this.lblStateCreditHR_1, null);
			this.lblStateCreditHR_1.Visible = false;
			// 
			// lblStateCreditHR_0
			// 
			this.lblStateCreditHR_0.BackColor = System.Drawing.SystemColors.Info;
			this.lblStateCreditHR_0.Location = new System.Drawing.Point(67, 60);
			this.lblStateCreditHR_0.Name = "lblStateCreditHR_0";
			this.lblStateCreditHR_0.Size = new System.Drawing.Size(19, 13);
			this.lblStateCreditHR_0.TabIndex = 71;
			this.lblStateCreditHR_0.Text = "HR";
			this.ToolTip1.SetToolTip(this.lblStateCreditHR_0, null);
			this.lblStateCreditHR_0.Visible = false;
			// 
			// Label1_10
			// 
			this.Label1_10.Location = new System.Drawing.Point(224, 110);
			this.Label1_10.Name = "Label1_10";
			this.Label1_10.Size = new System.Drawing.Size(82, 15);
			this.Label1_10.TabIndex = 64;
			this.Label1_10.Text = "TRANSFER";
			this.ToolTip1.SetToolTip(this.Label1_10, null);
			// 
			// lblRegistrationTransfer
			// 
			this.lblRegistrationTransfer.Location = new System.Drawing.Point(326, 110);
			this.lblRegistrationTransfer.Name = "lblRegistrationTransfer";
			this.lblRegistrationTransfer.Size = new System.Drawing.Size(102, 15);
			this.lblRegistrationTransfer.TabIndex = 63;
			this.lblRegistrationTransfer.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRegistrationTransfer, null);
			// 
			// Label1_1
			// 
			this.Label1_1.Location = new System.Drawing.Point(224, 85);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(82, 15);
			this.Label1_1.TabIndex = 61;
			this.Label1_1.Text = "CREDIT";
			this.ToolTip1.SetToolTip(this.Label1_1, null);
			// 
			// Label1_2
			// 
			this.Label1_2.Location = new System.Drawing.Point(224, 60);
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.Size = new System.Drawing.Size(82, 15);
			this.Label1_2.TabIndex = 60;
			this.Label1_2.Text = "1ST YEAR";
			this.ToolTip1.SetToolTip(this.Label1_2, null);
			// 
			// Label1_3
			// 
			this.Label1_3.Location = new System.Drawing.Point(224, 30);
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.Size = new System.Drawing.Size(204, 20);
			this.Label1_3.TabIndex = 59;
			this.Label1_3.Text = "REGISTRATION FEE";
			this.Label1_3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_3, null);
			// 
			// Label1_4
			// 
			this.Label1_4.Location = new System.Drawing.Point(0, 60);
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.Size = new System.Drawing.Size(82, 15);
			this.Label1_4.TabIndex = 58;
			this.Label1_4.Text = "1ST YEAR";
			this.ToolTip1.SetToolTip(this.Label1_4, null);
			// 
			// Label1_5
			// 
			this.Label1_5.Location = new System.Drawing.Point(448, 30);
			this.Label1_5.Name = "Label1_5";
			this.Label1_5.Size = new System.Drawing.Size(200, 20);
			this.Label1_5.TabIndex = 57;
			this.Label1_5.Text = "LOCAL FEE";
			this.Label1_5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_5, null);
			// 
			// Label1_6
			// 
			this.Label1_6.Location = new System.Drawing.Point(0, 30);
			this.Label1_6.Name = "Label1_6";
			this.Label1_6.Size = new System.Drawing.Size(204, 20);
			this.Label1_6.TabIndex = 56;
			this.Label1_6.Text = "EXCISE TAX";
			this.Label1_6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_6, null);
			// 
			// Label1_7
			// 
			this.Label1_7.Location = new System.Drawing.Point(0, 335);
			this.Label1_7.Name = "Label1_7";
			this.Label1_7.Size = new System.Drawing.Size(46, 15);
			this.Label1_7.TabIndex = 55;
			this.Label1_7.Text = "TOTAL";
			this.ToolTip1.SetToolTip(this.Label1_7, null);
			// 
			// Label1_8
			// 
			this.Label1_8.Location = new System.Drawing.Point(96, 215);
			this.Label1_8.Name = "Label1_8";
			this.Label1_8.Size = new System.Drawing.Size(151, 20);
			this.Label1_8.TabIndex = 54;
			this.Label1_8.Text = "TRANSFER";
			this.Label1_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_8, null);
			// 
			// lblRegistrationTotal
			// 
			this.lblRegistrationTotal.Location = new System.Drawing.Point(326, 160);
			this.lblRegistrationTotal.Name = "lblRegistrationTotal";
			this.lblRegistrationTotal.Size = new System.Drawing.Size(102, 15);
			this.lblRegistrationTotal.TabIndex = 53;
			this.lblRegistrationTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRegistrationTotal, null);
			// 
			// lblRegistrationTax2Year
			// 
			this.lblRegistrationTax2Year.Location = new System.Drawing.Point(326, 135);
			this.lblRegistrationTax2Year.Name = "lblRegistrationTax2Year";
			this.lblRegistrationTax2Year.Size = new System.Drawing.Size(102, 15);
			this.lblRegistrationTax2Year.TabIndex = 52;
			this.lblRegistrationTax2Year.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRegistrationTax2Year, null);
			// 
			// lblRegistrationCredit
			// 
			this.lblRegistrationCredit.Location = new System.Drawing.Point(326, 85);
			this.lblRegistrationCredit.Name = "lblRegistrationCredit";
			this.lblRegistrationCredit.Size = new System.Drawing.Size(102, 15);
			this.lblRegistrationCredit.TabIndex = 51;
			this.lblRegistrationCredit.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRegistrationCredit, null);
			// 
			// lblRegistrationTax1Year
			// 
			this.lblRegistrationTax1Year.Location = new System.Drawing.Point(326, 60);
			this.lblRegistrationTax1Year.Name = "lblRegistrationTax1Year";
			this.lblRegistrationTax1Year.Size = new System.Drawing.Size(102, 15);
			this.lblRegistrationTax1Year.TabIndex = 50;
			this.lblRegistrationTax1Year.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRegistrationTax1Year, null);
			// 
			// Label1_14
			// 
			this.Label1_14.Location = new System.Drawing.Point(224, 135);
			this.Label1_14.Name = "Label1_14";
			this.Label1_14.Size = new System.Drawing.Size(82, 15);
			this.Label1_14.TabIndex = 49;
			this.Label1_14.Text = "2ND YEAR";
			this.ToolTip1.SetToolTip(this.Label1_14, null);
			// 
			// Label1_15
			// 
			this.Label1_15.Location = new System.Drawing.Point(0, 300);
			this.Label1_15.Name = "Label1_15";
			this.Label1_15.Size = new System.Drawing.Size(102, 15);
			this.Label1_15.TabIndex = 48;
			this.Label1_15.Text = "LOCAL FEE";
			this.ToolTip1.SetToolTip(this.Label1_15, null);
			// 
			// Label1_16
			// 
			this.Label1_16.Location = new System.Drawing.Point(0, 275);
			this.Label1_16.Name = "Label1_16";
			this.Label1_16.Size = new System.Drawing.Size(102, 15);
			this.Label1_16.TabIndex = 47;
			this.Label1_16.Text = "REGISTRATION";
			this.ToolTip1.SetToolTip(this.Label1_16, null);
			// 
			// Label1_17
			// 
			this.Label1_17.Location = new System.Drawing.Point(0, 250);
			this.Label1_17.Name = "Label1_17";
			this.Label1_17.Size = new System.Drawing.Size(102, 15);
			this.Label1_17.TabIndex = 46;
			this.Label1_17.Text = "EXCISE";
			this.ToolTip1.SetToolTip(this.Label1_17, null);
			// 
			// lblExciseTaxTotal
			// 
			this.lblExciseTaxTotal.Location = new System.Drawing.Point(102, 135);
			this.lblExciseTaxTotal.Name = "lblExciseTaxTotal";
			this.lblExciseTaxTotal.Size = new System.Drawing.Size(102, 15);
			this.lblExciseTaxTotal.TabIndex = 45;
			this.lblExciseTaxTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblExciseTaxTotal, null);
			// 
			// lblExciseTax2Year
			// 
			this.lblExciseTax2Year.Location = new System.Drawing.Point(102, 110);
			this.lblExciseTax2Year.Name = "lblExciseTax2Year";
			this.lblExciseTax2Year.Size = new System.Drawing.Size(102, 15);
			this.lblExciseTax2Year.TabIndex = 44;
			this.lblExciseTax2Year.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblExciseTax2Year, null);
			// 
			// lblExciseTaxCredit
			// 
			this.lblExciseTaxCredit.Location = new System.Drawing.Point(102, 85);
			this.lblExciseTaxCredit.Name = "lblExciseTaxCredit";
			this.lblExciseTaxCredit.Size = new System.Drawing.Size(102, 15);
			this.lblExciseTaxCredit.TabIndex = 43;
			this.lblExciseTaxCredit.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblExciseTaxCredit, null);
			// 
			// lblExciseTax1Year
			// 
			this.lblExciseTax1Year.Location = new System.Drawing.Point(102, 60);
			this.lblExciseTax1Year.Name = "lblExciseTax1Year";
			this.lblExciseTax1Year.Size = new System.Drawing.Size(102, 15);
			this.lblExciseTax1Year.TabIndex = 42;
			this.lblExciseTax1Year.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblExciseTax1Year, null);
			// 
			// Label1_22
			// 
			this.Label1_22.Location = new System.Drawing.Point(0, 110);
			this.Label1_22.Name = "Label1_22";
			this.Label1_22.Size = new System.Drawing.Size(82, 15);
			this.Label1_22.TabIndex = 41;
			this.Label1_22.Text = "2ND YEAR";
			this.ToolTip1.SetToolTip(this.Label1_22, null);
			// 
			// Label1_23
			// 
			this.Label1_23.Location = new System.Drawing.Point(0, 85);
			this.Label1_23.Name = "Label1_23";
			this.Label1_23.Size = new System.Drawing.Size(82, 15);
			this.Label1_23.TabIndex = 40;
			this.Label1_23.Text = "CREDIT";
			this.ToolTip1.SetToolTip(this.Label1_23, null);
			// 
			// Label1_24
			// 
			this.Label1_24.Location = new System.Drawing.Point(448, 60);
			this.Label1_24.Name = "Label1_24";
			this.Label1_24.Size = new System.Drawing.Size(82, 15);
			this.Label1_24.TabIndex = 39;
			this.Label1_24.Text = "1ST YEAR";
			this.ToolTip1.SetToolTip(this.Label1_24, null);
			// 
			// Label1_25
			// 
			this.Label1_25.Location = new System.Drawing.Point(224, 160);
			this.Label1_25.Name = "Label1_25";
			this.Label1_25.Size = new System.Drawing.Size(46, 15);
			this.Label1_25.TabIndex = 38;
			this.Label1_25.Text = "TOTAL";
			this.ToolTip1.SetToolTip(this.Label1_25, null);
			// 
			// Label1_26
			// 
			this.Label1_26.Location = new System.Drawing.Point(0, 135);
			this.Label1_26.Name = "Label1_26";
			this.Label1_26.Size = new System.Drawing.Size(46, 15);
			this.Label1_26.TabIndex = 37;
			this.Label1_26.Text = "TOTAL";
			this.ToolTip1.SetToolTip(this.Label1_26, null);
			// 
			// Label1_27
			// 
			this.Label1_27.Location = new System.Drawing.Point(0, 370);
			this.Label1_27.Name = "Label1_27";
			this.Label1_27.Size = new System.Drawing.Size(102, 15);
			this.Label1_27.TabIndex = 36;
			this.Label1_27.Text = "MONTHS";
			this.ToolTip1.SetToolTip(this.Label1_27, null);
			// 
			// Label1_28
			// 
			this.Label1_28.Location = new System.Drawing.Point(267, 215);
			this.Label1_28.Name = "Label1_28";
			this.Label1_28.Size = new System.Drawing.Size(151, 20);
			this.Label1_28.TabIndex = 35;
			this.Label1_28.Text = "NEW REGISTRATION";
			this.Label1_28.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_28, null);
			// 
			// lblLocalTotal
			// 
			this.lblLocalTotal.Location = new System.Drawing.Point(550, 110);
			this.lblLocalTotal.Name = "lblLocalTotal";
			this.lblLocalTotal.Size = new System.Drawing.Size(98, 15);
			this.lblLocalTotal.TabIndex = 34;
			this.lblLocalTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblLocalTotal, null);
			// 
			// lblLocalTax2Year
			// 
			this.lblLocalTax2Year.Location = new System.Drawing.Point(550, 85);
			this.lblLocalTax2Year.Name = "lblLocalTax2Year";
			this.lblLocalTax2Year.Size = new System.Drawing.Size(98, 15);
			this.lblLocalTax2Year.TabIndex = 33;
			this.lblLocalTax2Year.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblLocalTax2Year, null);
			// 
			// lblLocalTax1Year
			// 
			this.lblLocalTax1Year.Location = new System.Drawing.Point(550, 60);
			this.lblLocalTax1Year.Name = "lblLocalTax1Year";
			this.lblLocalTax1Year.Size = new System.Drawing.Size(98, 15);
			this.lblLocalTax1Year.TabIndex = 32;
			this.lblLocalTax1Year.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblLocalTax1Year, "(Agent Fee)  +  (Transfer Fee)");
			// 
			// Label1_32
			// 
			this.Label1_32.Location = new System.Drawing.Point(448, 110);
			this.Label1_32.Name = "Label1_32";
			this.Label1_32.Size = new System.Drawing.Size(46, 15);
			this.Label1_32.TabIndex = 31;
			this.Label1_32.Text = "TOTAL";
			this.ToolTip1.SetToolTip(this.Label1_32, null);
			// 
			// Label1_33
			// 
			this.Label1_33.Location = new System.Drawing.Point(448, 85);
			this.Label1_33.Name = "Label1_33";
			this.Label1_33.Size = new System.Drawing.Size(82, 15);
			this.Label1_33.TabIndex = 30;
			this.Label1_33.Text = "2ND YEAR";
			this.ToolTip1.SetToolTip(this.Label1_33, null);
			// 
			// Label1_9
			// 
			this.Label1_9.BackColor = System.Drawing.SystemColors.Info;
			this.Label1_9.Location = new System.Drawing.Point(193, 185);
			this.Label1_9.Name = "Label1_9";
			this.Label1_9.Size = new System.Drawing.Size(250, 20);
			this.Label1_9.TabIndex = 29;
			this.Label1_9.Text = "---------- SUMMARY ----------";
			this.Label1_9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_9, null);
			// 
			// Label1_34
			// 
			this.Label1_34.Location = new System.Drawing.Point(0, 445);
			this.Label1_34.Name = "Label1_34";
			this.Label1_34.Size = new System.Drawing.Size(102, 15);
			this.Label1_34.TabIndex = 28;
			this.Label1_34.Text = "PAY LATER";
			this.ToolTip1.SetToolTip(this.Label1_34, null);
			// 
			// Label1_35
			// 
			this.Label1_35.Location = new System.Drawing.Point(0, 420);
			this.Label1_35.Name = "Label1_35";
			this.Label1_35.Size = new System.Drawing.Size(102, 15);
			this.Label1_35.TabIndex = 27;
			this.Label1_35.Text = "PAY TODAY";
			this.ToolTip1.SetToolTip(this.Label1_35, null);
			// 
			// Label1_36
			// 
			this.Label1_36.Location = new System.Drawing.Point(0, 395);
			this.Label1_36.Name = "Label1_36";
			this.Label1_36.Size = new System.Drawing.Size(102, 15);
			this.Label1_36.TabIndex = 26;
			this.Label1_36.Text = "PER MONTH";
			this.ToolTip1.SetToolTip(this.Label1_36, null);
			// 
			// lblLocalNR
			// 
			this.lblLocalNR.Location = new System.Drawing.Point(291, 300);
			this.lblLocalNR.Name = "lblLocalNR";
			this.lblLocalNR.Size = new System.Drawing.Size(102, 15);
			this.lblLocalNR.TabIndex = 25;
			this.lblLocalNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblLocalNR, null);
			// 
			// lblRegNR
			// 
			this.lblRegNR.Location = new System.Drawing.Point(291, 275);
			this.lblRegNR.Name = "lblRegNR";
			this.lblRegNR.Size = new System.Drawing.Size(102, 15);
			this.lblRegNR.TabIndex = 24;
			this.lblRegNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRegNR, null);
			// 
			// lblExciseNR
			// 
			this.lblExciseNR.Location = new System.Drawing.Point(291, 250);
			this.lblExciseNR.Name = "lblExciseNR";
			this.lblExciseNR.Size = new System.Drawing.Size(102, 15);
			this.lblExciseNR.TabIndex = 23;
			this.lblExciseNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblExciseNR, null);
			// 
			// lblLocalT
			// 
			this.lblLocalT.Location = new System.Drawing.Point(122, 300);
			this.lblLocalT.Name = "lblLocalT";
			this.lblLocalT.Size = new System.Drawing.Size(102, 15);
			this.lblLocalT.TabIndex = 22;
			this.lblLocalT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblLocalT, null);
			// 
			// lblRegT
			// 
			this.lblRegT.Location = new System.Drawing.Point(122, 275);
			this.lblRegT.Name = "lblRegT";
			this.lblRegT.Size = new System.Drawing.Size(102, 15);
			this.lblRegT.TabIndex = 21;
			this.lblRegT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRegT, null);
			// 
			// lblExciseT
			// 
			this.lblExciseT.Location = new System.Drawing.Point(122, 250);
			this.lblExciseT.Name = "lblExciseT";
			this.lblExciseT.Size = new System.Drawing.Size(102, 15);
			this.lblExciseT.TabIndex = 20;
			this.lblExciseT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblExciseT, null);
			// 
			// lblPayTodayNR
			// 
			this.lblPayTodayNR.Location = new System.Drawing.Point(291, 420);
			this.lblPayTodayNR.Name = "lblPayTodayNR";
			this.lblPayTodayNR.Size = new System.Drawing.Size(102, 15);
			this.lblPayTodayNR.TabIndex = 19;
			this.lblPayTodayNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblPayTodayNR, null);
			// 
			// lblPerMonthNR
			// 
			this.lblPerMonthNR.Location = new System.Drawing.Point(291, 395);
			this.lblPerMonthNR.Name = "lblPerMonthNR";
			this.lblPerMonthNR.Size = new System.Drawing.Size(102, 15);
			this.lblPerMonthNR.TabIndex = 18;
			this.lblPerMonthNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblPerMonthNR, null);
			// 
			// lblMonthsNR
			// 
			this.lblMonthsNR.Location = new System.Drawing.Point(291, 370);
			this.lblMonthsNR.Name = "lblMonthsNR";
			this.lblMonthsNR.Size = new System.Drawing.Size(102, 15);
			this.lblMonthsNR.TabIndex = 17;
			this.lblMonthsNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblMonthsNR, null);
			// 
			// lblPayTodayT
			// 
			this.lblPayTodayT.Location = new System.Drawing.Point(122, 420);
			this.lblPayTodayT.Name = "lblPayTodayT";
			this.lblPayTodayT.Size = new System.Drawing.Size(102, 15);
			this.lblPayTodayT.TabIndex = 16;
			this.lblPayTodayT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblPayTodayT, null);
			// 
			// lblPerMonthT
			// 
			this.lblPerMonthT.Location = new System.Drawing.Point(122, 395);
			this.lblPerMonthT.Name = "lblPerMonthT";
			this.lblPerMonthT.Size = new System.Drawing.Size(102, 15);
			this.lblPerMonthT.TabIndex = 15;
			this.lblPerMonthT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblPerMonthT, null);
			// 
			// lblMonthsT
			// 
			this.lblMonthsT.Location = new System.Drawing.Point(122, 370);
			this.lblMonthsT.Name = "lblMonthsT";
			this.lblMonthsT.Size = new System.Drawing.Size(102, 15);
			this.lblMonthsT.TabIndex = 14;
			this.lblMonthsT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblMonthsT, null);
			// 
			// lblPayLaterNR
			// 
			this.lblPayLaterNR.Location = new System.Drawing.Point(291, 445);
			this.lblPayLaterNR.Name = "lblPayLaterNR";
			this.lblPayLaterNR.Size = new System.Drawing.Size(102, 15);
			this.lblPayLaterNR.TabIndex = 13;
			this.lblPayLaterNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblPayLaterNR, null);
			// 
			// lblPayLaterT
			// 
			this.lblPayLaterT.Location = new System.Drawing.Point(122, 445);
			this.lblPayLaterT.Name = "lblPayLaterT";
			this.lblPayLaterT.Size = new System.Drawing.Size(102, 15);
			this.lblPayLaterT.TabIndex = 12;
			this.lblPayLaterT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblPayLaterT, null);
			// 
			// lblTotalNR
			// 
			this.lblTotalNR.Location = new System.Drawing.Point(291, 335);
			this.lblTotalNR.Name = "lblTotalNR";
			this.lblTotalNR.Size = new System.Drawing.Size(102, 15);
			this.lblTotalNR.TabIndex = 11;
			this.lblTotalNR.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblTotalNR, null);
			// 
			// lblTotalT
			// 
			this.lblTotalT.Location = new System.Drawing.Point(122, 335);
			this.lblTotalT.Name = "lblTotalT";
			this.lblTotalT.Size = new System.Drawing.Size(102, 15);
			this.lblTotalT.TabIndex = 10;
			this.lblTotalT.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblTotalT, null);
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(138, 0);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(377, 20);
			this.Label1_0.TabIndex = 62;
			this.Label1_0.Text = "---------- TRANSFER INFORMATION ----------";
			this.Label1_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 0;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// frmTransferAnalysis
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(738, 668);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTransferAnalysis";
			this.Text = "Transfer Analysis";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmTransferAnalysis_Load);
			this.Activated += new System.EventHandler(this.frmTransferAnalysis_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTransferAnalysis_KeyPress);
			this.Resize += new System.EventHandler(this.frmTransferAnalysis_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuitCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocalCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFinal)).EndInit();
			this.fraFinal.ResumeLayout(false);
			this.fraFinal.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkETO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}
