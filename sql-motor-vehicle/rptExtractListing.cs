﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptExtractListing.
	/// </summary>
	public partial class rptExtractListing : BaseSectionReport
	{
		public rptExtractListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Reminder List";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptExtractListing InstancePtr
		{
			get
			{
				return (rptExtractListing)Sys.GetInstance(typeof(rptExtractListing));
			}
		}

		protected rptExtractListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptExtractListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		int counter;
		// kk05162017 tromv-1115  Change from Integer to Long
		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmReminder.InstancePtr.txtPostage.Text = "";
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			counter = 1;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			this.Document.Printer.DefaultPageSettings.Landscape = true;
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			Label5.Text = "Registrations expiring in " + frmReminder.InstancePtr.cboMonth.Text + " " + frmReminder.InstancePtr.cboYear.Text;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			if (eArgs.EOF)
				return;
			//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
			//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			{
				fldClass.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 2) + " " + frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
				fldYear.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4);
				fldMake.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5);
				fldModel.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6).Left(6);
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) == "")
				{
					fldReg.Text = "0.00";
				}
				else
				{
					fldReg.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7);
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8) == "")
				{
					fldExcise.Text = "0.00";
				}
				else
				{
					fldExcise.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8);
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9) == "")
				{
					fldAgent.Text = "0.00";
				}
				else
				{
					fldAgent.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9);
				}
				fldPost.Text = Strings.Format(frmReminder.InstancePtr.txtPostage.Text, "#,##0.00");
				fldTotal.Text = Strings.Format(FCConvert.ToDecimal(fldReg.Text) + FCConvert.ToDecimal(fldExcise.Text) + FCConvert.ToDecimal(fldAgent.Text) + FCConvert.ToDecimal(fldPost.Text), "#,##0.00");
				fldOwner.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
				fldEmail.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 11);
			}
			else
			{
				counter += 1;
				while (counter < frmReminder.InstancePtr.vsVehicles.Rows)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					{
						fldClass.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 2) + " " + frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
						fldYear.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4);
						fldMake.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5);
						fldModel.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6).Left(6);
						fldReg.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7);
						fldExcise.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8);
						fldAgent.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9);
						fldPost.Text = Strings.Format(frmReminder.InstancePtr.txtPostage.Text, "#,##0.00");
						fldTotal.Text = Strings.Format(FCConvert.ToDecimal(fldReg.Text) + FCConvert.ToDecimal(fldExcise.Text) + FCConvert.ToDecimal(fldAgent.Text) + FCConvert.ToDecimal(fldPost.Text), "#,##0.00");
						fldOwner.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
						fldEmail.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 11);
						break;
					}
					else
					{
						counter += 1;
					}
				}
				if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
				{
					eArgs.EOF = true;
					return;
				}
			}
			counter += 1;
			eArgs.EOF = false;
			//Application.DoEvents();
		}

		private void rptExtractListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptExtractListing properties;
			//rptExtractListing.Caption	= "Reminder List";
			//rptExtractListing.Icon	= "rptExtractListing.dsx":0000";
			//rptExtractListing.Left	= 0;
			//rptExtractListing.Top	= 0;
			//rptExtractListing.Width	= 11880;
			//rptExtractListing.Height	= 8595;
			//rptExtractListing.StartUpPosition	= 3;
			//rptExtractListing.SectionData	= "rptExtractListing.dsx":058A;
			//End Unmaped Properties
		}
	}
}
