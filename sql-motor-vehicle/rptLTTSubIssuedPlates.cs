//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTSubIssuedPlates.
	/// </summary>
	public partial class rptLTTSubIssuedPlates : BaseSectionReport
	{
		public rptLTTSubIssuedPlates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptLTTSubIssuedPlates_ReportEnd;
		}

        private void RptLTTSubIssuedPlates_ReportEnd(object sender, EventArgs e)
        {
			rsAM.DisposeOf();
            rs.DisposeOf();

		}

        public static rptLTTSubIssuedPlates InstancePtr
		{
			get
			{
				return (rptLTTSubIssuedPlates)Sys.GetInstance(typeof(rptLTTSubIssuedPlates));
			}
		}

		protected rptLTTSubIssuedPlates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLTTSubIssuedPlates	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;
		int lngPK1;
		int lngPK2;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsAM.EndOfFile())
				{
					lblNoInfo.Visible = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			else
			{
				rsAM.MoveNext();
				if (rsAM.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			if (frmReportLongTerm.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				MotorVehicle.Statics.strSql = "SELECT FleetMaster.CompanyCode, LongTermTrailerRegistrations.* FROM FleetMaster INNER JOIN LongTermTrailerRegistrations ON FleetMaster.FleetNumber = LongTermTrailerRegistrations.FleetNumber WHERE Status <> 'V' AND PeriodCloseoutID < 1 AND OldPlate & '' <> Plate ORDER BY FleetMaster.CompanyCode, LongTermTrailerRegistrations.Plate";
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate BETWEEN '" + frmReportLongTerm.InstancePtr.cboStart.Text + "' AND '" + frmReportLongTerm.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(MotorVehicle.Statics.strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
				MotorVehicle.Statics.strSql = "SELECT FleetMaster.CompanyCode, LongTermTrailerRegistrations.* FROM FleetMaster INNER JOIN LongTermTrailerRegistrations ON FleetMaster.FleetNumber = LongTermTrailerRegistrations.FleetNumber WHERE Status <> 'V' AND OldPlate & '' <> Plate AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY FleetMaster.CompanyCode, LongTermTrailerRegistrations.Plate";
			}
			rsAM.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldPlate As object	OnWrite(object, string)
			// vbPorter upgrade warning: fldCompanyCode As object	OnWrite(object, string)
			if (rsAM.EndOfFile() != true)
			{
				fldPlate.Text = rsAM.Get_Fields_String("Plate");
				fldCompanyCode.Text = rsAM.Get_Fields_String("CompanyCode");
			}
			else
			{
				fldPlate.Text = "";
				fldCompanyCode.Text = "";
			}
		}

		private void rptLTTSubIssuedPlates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLTTSubIssuedPlates properties;
			//rptLTTSubIssuedPlates.Caption	= "ActiveReport1";
			//rptLTTSubIssuedPlates.Left	= 0;
			//rptLTTSubIssuedPlates.Top	= 0;
			//rptLTTSubIssuedPlates.Width	= 12465;
			//rptLTTSubIssuedPlates.Height	= 5490;
			//rptLTTSubIssuedPlates.StartUpPosition	= 3;
			//rptLTTSubIssuedPlates.SectionData	= "rptLTTSubIssuedPlates.dsx":0000;
			//End Unmaped Properties
		}
	}
}
