//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptVehicleClassList.
	/// </summary>
	public partial class rptVehicleClassList : BaseSectionReport
	{
		public rptVehicleClassList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Vehicle Class List";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptVehicleClassList_ReportEnd;
		}

        private void RptVehicleClassList_ReportEnd(object sender, EventArgs e)
        {
            rsInfo.DisposeOf();
        }

        public static rptVehicleClassList InstancePtr
		{
			get
			{
				return (rptVehicleClassList)Sys.GetInstance(typeof(rptVehicleClassList));
			}
		}

		protected rptVehicleClassList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVehicleClassList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		clsDRWrapper rsInfo = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label35 As object	OnWrite(string)
			// vbPorter upgrade warning: Label34 As object	OnWrite(string)
			// vbPorter upgrade warning: lblClass As object	OnWrite(string)
			// vbPorter upgrade warning: lblStatusDateRange As object	OnWrite(string)
			string strStatus = "";
			string strDates = "";
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			blnFirstRecord = true;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm tt");
			CreateSQL();
			if (frmVehicleClassList.InstancePtr.cmbMultiple.Text == "Multiple")
			{
				lblClass.Text = "Class: ";
				for (counter = 1; counter <= (frmVehicleClassList.InstancePtr.vsClass.Rows - 1); counter++)
				{
					if (FCConvert.ToBoolean(frmVehicleClassList.InstancePtr.vsClass.TextMatrix(counter, 0)) == true)
					{
						strDates += frmVehicleClassList.InstancePtr.vsClass.TextMatrix(counter, 1) + ", ";
					}
				}
				strDates = Strings.Left(strDates, strDates.Length - 2);
				lblClass.Text = lblClass.Text + strDates;
			}
			else
			{
				if (frmVehicleClassList.InstancePtr.cmbAllSubclass.Text == "All")
				{
					lblClass.Text = "Class: " + frmVehicleClassList.InstancePtr.cboClass.Text + "       Subclass: ALL";
				}
				else
				{
					lblClass.Text = "Class: " + frmVehicleClassList.InstancePtr.cboClass.Text + "       Subclass: " + Strings.Left(frmVehicleClassList.InstancePtr.cboSubclass.Text, 2);
				}
			}
			if (frmVehicleClassList.InstancePtr.cmbAllStatus.Text == "All")
			{
				strStatus = "Status: ALL";
			}
			else
			{
				strStatus = "Status: " + Strings.Left(frmVehicleClassList.InstancePtr.cboStatus.Text, 1);
			}
			if (frmVehicleClassList.InstancePtr.cmbAll.Text == "All")
			{
				strDates = "Dates: ALL";
			}
			else if (frmVehicleClassList.InstancePtr.cmbAll.Text == "Effective Date Range")
			{
				strDates = "Dates: Effective Date from " + frmVehicleClassList.InstancePtr.txtStartDate.Text + " To " + frmVehicleClassList.InstancePtr.txtEndDate.Text;
			}
			else if (frmVehicleClassList.InstancePtr.cmbAll.Text == "Excise Tax Date Range")
			{
				strDates = "Dates: Excise Tax Date from " + frmVehicleClassList.InstancePtr.txtStartDate.Text + " To " + frmVehicleClassList.InstancePtr.txtEndDate.Text;
			}
			else if (frmVehicleClassList.InstancePtr.cmbAll.Text == "Expiration Date Range")
			{
				strDates = "Dates: Expiration Date from " + frmVehicleClassList.InstancePtr.txtStartDate.Text + " To " + frmVehicleClassList.InstancePtr.txtEndDate.Text;
			}
			lblStatusDateRange.Text = strStatus + "       " + strDates;
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No records found that match the criteria.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
				return;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmVehicleClassList.InstancePtr.Show(App.MainForm);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldOwner As object	OnWrite(string)
			// vbPorter upgrade warning: fldExpires As object	OnWrite(string)
			// vbPorter upgrade warning: fldMake As object	OnWrite(string)
			// vbPorter upgrade warning: fldModel As object	OnWrite(string)
			// vbPorter upgrade warning: fldYear As object	OnWrite(string)
			// vbPorter upgrade warning: fldVIN As object	OnWrite(string)
			// vbPorter upgrade warning: fldClass As object	OnWrite(string)
			// vbPorter upgrade warning: fldSubclass As object	OnWrite(string)
			// vbPorter upgrade warning: fldMVR3 As object	OnWrite(string)
			fldPlate.Text = rsInfo.Get_Fields_String("Plate");
			fldOwner.Text = fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsInfo.Get_Fields_Int32("PartyID1"), true));
			fldExpires.Text = Strings.Format(rsInfo.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
			fldMake.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Make")));
			fldModel.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Model")));
			fldYear.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Year")));
			fldVIN.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("VIN")));
			fldClass.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Class")));
			fldSubclass.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("SubClass")));
			fldMVR3.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_Int32("MVR3")));
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotal As object	OnWrite(string)
			fldTotal.Text = Strings.Format(rsInfo.RecordCount(), "#,##0");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void CreateSQL()
		{
			string strSQL = "";
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (frmVehicleClassList.InstancePtr.cmbMultiple.Text == "Multiple")
			{
				strSQL = "SELECT * FROM Master WHERE (";
				for (counter = 1; counter <= (frmVehicleClassList.InstancePtr.vsClass.Rows - 1); counter++)
				{
					if (FCConvert.ToBoolean(frmVehicleClassList.InstancePtr.vsClass.TextMatrix(counter, 0)) == true)
					{
						strSQL += "Class = '" + frmVehicleClassList.InstancePtr.vsClass.TextMatrix(counter, 1) + "' OR ";
					}
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ") AND ";
			}
			else
			{
				if (frmVehicleClassList.InstancePtr.cmbAllSubclass.Text == "All")
				{
					strSQL = "SELECT * FROM Master WHERE Class = '" + frmVehicleClassList.InstancePtr.cboClass.Text + "' AND ";
				}
				else
				{
					strSQL = "SELECT * FROM Master WHERE (Class = '" + frmVehicleClassList.InstancePtr.cboClass.Text + "' AND Subclass = '" + Strings.Left(frmVehicleClassList.InstancePtr.cboSubclass.Text, 2) + "') AND ";
				}
			}
			if (frmVehicleClassList.InstancePtr.cmbAllStatus.Text == "Specific")
			{
				strSQL += "Status = '" + Strings.Left(frmVehicleClassList.InstancePtr.cboStatus.Text, 1) + "' AND ";
			}
			if (frmVehicleClassList.InstancePtr.cmbAll.Text == "Effective Date Range")
			{
				strSQL += "(EffectiveDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmVehicleClassList.InstancePtr.txtStartDate.Text)) + "' AND EffectiveDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmVehicleClassList.InstancePtr.txtEndDate.Text)) + "') AND ";
			}
			else if (frmVehicleClassList.InstancePtr.cmbAll.Text == "Excise Tax Date Range")
			{
				strSQL += "(ExciseTaxDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmVehicleClassList.InstancePtr.txtStartDate.Text)) + "' AND ExciseTaxDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmVehicleClassList.InstancePtr.txtEndDate.Text)) + "') AND ";
			}
			else if (frmVehicleClassList.InstancePtr.cmbAll.Text == "Expiration Date Range")
			{
				strSQL += "(ExpireDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmVehicleClassList.InstancePtr.txtStartDate.Text)) + "' AND ExpireDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(frmVehicleClassList.InstancePtr.txtEndDate.Text)) + "') AND ";
			}
			if (frmVehicleClassList.InstancePtr.cmbPlate.Text == "Plate")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 5) + " ORDER BY Plate";
			}
			else
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 5) + " ORDER BY ExpireDate, Plate";
			}
			rsInfo.OpenRecordset(strSQL);
		}

		private void rptVehicleClassList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptVehicleClassList properties;
			//rptVehicleClassList.Caption	= "Vehicle Class List";
			//rptVehicleClassList.Icon	= "rptVehicleClassList.dsx":0000";
			//rptVehicleClassList.Left	= 0;
			//rptVehicleClassList.Top	= 0;
			//rptVehicleClassList.Width	= 11880;
			//rptVehicleClassList.Height	= 8595;
			//rptVehicleClassList.StartUpPosition	= 3;
			//rptVehicleClassList.SectionData	= "rptVehicleClassList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
