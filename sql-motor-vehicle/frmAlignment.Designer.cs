//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmAlignment.
	/// </summary>
	partial class frmAlignment
	{
		public fecherFoundation.FCButton cmdCancelPrint;
		public fecherFoundation.FCButton cmdAlignDone;
		public fecherFoundation.FCButton cmdPrintXX;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdCancelPrint = new fecherFoundation.FCButton();
            this.cmdAlignDone = new fecherFoundation.FCButton();
            this.cmdPrintXX = new fecherFoundation.FCButton();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Line2 = new fecherFoundation.FCLine();
            this.Line1 = new fecherFoundation.FCLine();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAlignDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintXX)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.BackColor = System.Drawing.Color.FromName("@window");
            this.BottomPanel.Location = new System.Drawing.Point(0, 265);
            this.BottomPanel.Size = new System.Drawing.Size(579, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientArea.Controls.Add(this.cmdCancelPrint);
            this.ClientArea.Controls.Add(this.cmdAlignDone);
            this.ClientArea.Controls.Add(this.cmdPrintXX);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Line1);
            this.ClientArea.Controls.Add(this.Line2);
            this.ClientArea.Size = new System.Drawing.Size(579, 205);
            // 
            // TopPanel
            // 
            this.TopPanel.BackColor = System.Drawing.Color.FromName("@window");
            this.TopPanel.Size = new System.Drawing.Size(579, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(179, 30);
            this.HeaderText.Text = "Print Alignment";
            // 
            // cmdCancelPrint
            // 
            this.cmdCancelPrint.AppearanceKey = "actionButton";
            this.cmdCancelPrint.Location = new System.Drawing.Point(379, 135);
            this.cmdCancelPrint.Name = "cmdCancelPrint";
            this.cmdCancelPrint.Size = new System.Drawing.Size(172, 40);
            this.cmdCancelPrint.TabIndex = 6;
            this.cmdCancelPrint.Text = "Cancel Print Job";
            this.cmdCancelPrint.Click += new System.EventHandler(this.cmdCancelPrint_Click);
            // 
            // cmdAlignDone
            // 
            this.cmdAlignDone.AppearanceKey = "actionButton";
            this.cmdAlignDone.Location = new System.Drawing.Point(172, 135);
            this.cmdAlignDone.Name = "cmdAlignDone";
            this.cmdAlignDone.Size = new System.Drawing.Size(187, 40);
            this.cmdAlignDone.TabIndex = 5;
            this.cmdAlignDone.Text = "Alignment is Done";
            this.cmdAlignDone.Click += new System.EventHandler(this.cmdAlignDone_Click);
            // 
            // cmdPrintXX
            // 
            this.cmdPrintXX.AppearanceKey = "actionButton";
            this.cmdPrintXX.Location = new System.Drawing.Point(30, 135);
            this.cmdPrintXX.Name = "cmdPrintXX";
            this.cmdPrintXX.Size = new System.Drawing.Size(122, 40);
            this.cmdPrintXX.TabIndex = 4;
            this.cmdPrintXX.Text = "Print \'XX\'s";
            this.cmdPrintXX.Click += new System.EventHandler(this.cmdPrintXX_Click);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(203, 65);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(17, 15);
            this.Label4.TabIndex = 3;
            this.Label4.Text = "X";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(181, 65);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(9, 15);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "X";
            // 
            // Line2
            // 
            this.Line2.Location = new System.Drawing.Point(172, 58);
            this.Line2.Name = "Line2";
            this.Line2.Size = new System.Drawing.Size(50, 1);
            // 
            // Line1
            // 
            this.Line1.Location = new System.Drawing.Point(197, 43);
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(1, 30);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 100);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(292, 15);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "STATE OF MAINE VEHICLE REGISTRATION";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(531, 15);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "THE 2 \'XX\'S SHOULD PRINT BELOW THE CROSSHAIRS LOCATED AT THE TOP OF THE MVR3";
            // 
            // frmAlignment
            // 
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(579, 265);
            this.FillColor = -2147483643;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAlignment";
            this.ShowInTaskbar = false;
            this.Text = "MVR3 Print Alignment";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmAlignment_Load);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAlignDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintXX)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}