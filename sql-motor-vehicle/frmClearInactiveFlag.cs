﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmClearInactiveFlag : BaseForm
	{
		public frmClearInactiveFlag()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmClearInactiveFlag InstancePtr
		{
			get
			{
				return (frmClearInactiveFlag)Sys.GetInstance(typeof(frmClearInactiveFlag));
			}
		}

		protected frmClearInactiveFlag _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmClearInactiveFlag_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmClearInactiveFlag_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmClearInactiveFlag properties;
			//frmClearInactiveFlag.FillStyle	= 0;
			//frmClearInactiveFlag.ScaleWidth	= 3885;
			//frmClearInactiveFlag.ScaleHeight	= 1995;
			//frmClearInactiveFlag.LinkTopic	= "Form2";
			//frmClearInactiveFlag.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmClearInactiveFlag_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			if (fecherFoundation.Strings.Trim(txtPlate.Text) != "")
			{
				// kk05272016 tromv-1157  Change so we know if the record wasn't found and add a check for the stripped plate number
				// rs.Execute "UPDATE Master SET Inactive = 0 WHERE Plate = '" & UCase(Trim(txtPlate.Text)) & "'", "TWMV0000.vb1"
				rs.OpenRecordset("SELECT * FROM Master WHERE Plate = '" + fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(txtPlate.Text)) + "' OR PlateStripped = '" + fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(txtPlate.Text)) + "'", "TWMV0000.vb1");
				if (!rs.EndOfFile())
				{
					rs.Edit();
					rs.Set_Fields("Inactive", false);
					rs.Update();
					MessageBox.Show("Update completed", "Inactive Flag Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("The plate number was not found.", "Plate Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void txtPlate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// kk05272016 tromv-1157  Clean this up to allow "-", "&" Space and Backspace
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii >= Keys.A && KeyAscii <= Keys.Z) || KeyAscii == Keys.Back || KeyAscii == Keys.Insert || KeyAscii == Keys.Up || KeyAscii == Keys.Space)
			{
				// do nothing
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
