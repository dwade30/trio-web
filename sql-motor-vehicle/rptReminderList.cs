﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReminderList.
	/// </summary>
	public partial class rptReminderList : BaseSectionReport
	{
		public rptReminderList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Reminder List";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptReminderList InstancePtr
		{
			get
			{
				return (rptReminderList)Sys.GetInstance(typeof(rptReminderList));
			}
		}

		protected rptReminderList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		int counter;
		// kk05162017 tromv-1115  Change from Integer to Long
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			counter = 1;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label33.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label32.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			Label5.Text = frmReminder.InstancePtr.cboMonth.Text + " " + frmReminder.InstancePtr.cboYear.Text;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label34.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			if (eArgs.EOF)
				return;
			//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
			//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			{
				fldClass.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 2);
				fldPlate.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
				fldYear.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4);
				fldMake.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5);
				fldModel.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6);
				fldReg.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7);
				fldExcise.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8);
				fldAgent.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9);
				fldOwner.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
				fldEmail.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 11);
			}
			else
			{
				counter += 1;
				while (counter < frmReminder.InstancePtr.vsVehicles.Rows)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					{
						fldClass.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 2);
						fldPlate.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
						fldYear.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4);
						fldMake.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5);
						fldModel.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6);
						fldReg.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7);
						fldExcise.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8);
						fldAgent.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9);
						fldOwner.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
						fldEmail.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 11);
						break;
					}
					else
					{
						counter += 1;
					}
				}
				if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
				{
					eArgs.EOF = true;
					return;
				}
			}
			counter += 1;
			eArgs.EOF = false;
			//Application.DoEvents();
		}

		private void rptReminderList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderList properties;
			//rptReminderList.Caption	= "Reminder List";
			//rptReminderList.Icon	= "rptReminderList.dsx":0000";
			//rptReminderList.Left	= 0;
			//rptReminderList.Top	= 0;
			//rptReminderList.Width	= 11880;
			//rptReminderList.Height	= 8595;
			//rptReminderList.StartUpPosition	= 3;
			//rptReminderList.SectionData	= "rptReminderList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
