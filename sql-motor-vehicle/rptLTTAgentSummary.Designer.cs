﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTAgentSummary.
	/// </summary>
	partial class rptLTTAgentSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLTTAgentSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExtensionsUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExtensionAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTransfersLargeUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransfersSmallUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLostPlateUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDuplicateUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitleUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxPaidUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxNoFeeUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferLargeAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferSmallAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLostPlateAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDuplicateAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitleAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxPaidAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxNoFeeAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTransfers25YearUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransfer25YearAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTitle25YearUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle25YearsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRushTitleUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRushTitleAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionsUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersLargeUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersSmallUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferLargeAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferSmallAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfers25YearUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfer25YearAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label17,
				this.Label18,
				this.Label19,
				this.fldLTTLargeUnits,
				this.fldLTTSmallUnits,
				this.fldExtensionsUnits,
				this.fldLTTLargeAmount,
				this.fldLTTSmallAmount,
				this.fldExtensionAmount,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Line2,
				this.fldTransfersLargeUnits,
				this.fldTransfersSmallUnits,
				this.fldLostPlateUnits,
				this.fldDuplicateUnits,
				this.fldCorrectionUnits,
				this.fldTitleUnits,
				this.fldSalesTaxPaidUnits,
				this.fldSalesTaxNoFeeUnits,
				this.fldTotalUnits,
				this.fldTransferLargeAmount,
				this.fldTransferSmallAmount,
				this.fldLostPlateAmount,
				this.fldDuplicateAmount,
				this.fldCorrectionAmount,
				this.fldTitleAmount,
				this.fldSalesTaxPaidAmount,
				this.fldSalesTaxNoFeeAmount,
				this.fldTotalAmount,
				this.Label136,
				this.fldTransfers25YearUnits,
				this.fldTransfer25YearAmount,
				this.Label137,
				this.fldTitle25YearUnits,
				this.fldTitle25YearsAmount,
				this.Label138,
				this.fldRushTitleUnits,
				this.fldRushTitleAmount
			});
			this.Detail.Height = 3.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label12,
				this.Label13,
				this.Label16,
				this.Label1,
				this.lblPage,
				this.Line1
			});
			this.GroupHeader1.Height = 1.708333F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.3125F;
			this.SubReport2.Width = 7.46875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.15625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label12.Text = "Category";
			this.Label12.Top = 1.5F;
			this.Label12.Width = 2.09375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.46875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "Units";
			this.Label13.Top = 1.5F;
			this.Label13.Width = 0.84375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label16.Text = "Dollars";
			this.Label16.Top = 1.5F;
			this.Label16.Width = 1.125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.78125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label1.Text = "**** LTT AGENT SUMMARY REPORT****";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 3.625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.6875F;
			this.Line1.Width = 7.40625F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.6875F;
			this.Line1.Y2 = 1.6875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.1875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label17.Text = "LTT - Large";
			this.Label17.Top = 0F;
			this.Label17.Width = 2.09375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.1875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label18.Text = "LTT - Small";
			this.Label18.Top = 0.1875F;
			this.Label18.Width = 2.0625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.1875F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label19.Text = "Extensions";
			this.Label19.Top = 0.375F;
			this.Label19.Width = 2.0625F;
			// 
			// fldLTTLargeUnits
			// 
			this.fldLTTLargeUnits.Height = 0.1875F;
			this.fldLTTLargeUnits.Left = 2.46875F;
			this.fldLTTLargeUnits.Name = "fldLTTLargeUnits";
			this.fldLTTLargeUnits.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits.Text = "Field1";
			this.fldLTTLargeUnits.Top = 0F;
			this.fldLTTLargeUnits.Width = 0.84375F;
			// 
			// fldLTTSmallUnits
			// 
			this.fldLTTSmallUnits.Height = 0.1875F;
			this.fldLTTSmallUnits.Left = 2.5F;
			this.fldLTTSmallUnits.Name = "fldLTTSmallUnits";
			this.fldLTTSmallUnits.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits.Text = "Field2";
			this.fldLTTSmallUnits.Top = 0.1875F;
			this.fldLTTSmallUnits.Width = 0.8125F;
			// 
			// fldExtensionsUnits
			// 
			this.fldExtensionsUnits.Height = 0.1875F;
			this.fldExtensionsUnits.Left = 2.5F;
			this.fldExtensionsUnits.Name = "fldExtensionsUnits";
			this.fldExtensionsUnits.Style = "font-size: 9pt; text-align: right";
			this.fldExtensionsUnits.Text = "Field3";
			this.fldExtensionsUnits.Top = 0.375F;
			this.fldExtensionsUnits.Width = 0.8125F;
			// 
			// fldLTTLargeAmount
			// 
			this.fldLTTLargeAmount.Height = 0.1875F;
			this.fldLTTLargeAmount.Left = 3.625F;
			this.fldLTTLargeAmount.Name = "fldLTTLargeAmount";
			this.fldLTTLargeAmount.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount.Text = "Field13";
			this.fldLTTLargeAmount.Top = 0F;
			this.fldLTTLargeAmount.Width = 1.125F;
			// 
			// fldLTTSmallAmount
			// 
			this.fldLTTSmallAmount.Height = 0.1875F;
			this.fldLTTSmallAmount.Left = 3.625F;
			this.fldLTTSmallAmount.Name = "fldLTTSmallAmount";
			this.fldLTTSmallAmount.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount.Text = "Field14";
			this.fldLTTSmallAmount.Top = 0.1875F;
			this.fldLTTSmallAmount.Width = 1.125F;
			// 
			// fldExtensionAmount
			// 
			this.fldExtensionAmount.Height = 0.1875F;
			this.fldExtensionAmount.Left = 3.625F;
			this.fldExtensionAmount.Name = "fldExtensionAmount";
			this.fldExtensionAmount.Style = "font-size: 9pt; text-align: right";
			this.fldExtensionAmount.Text = "Field15";
			this.fldExtensionAmount.Top = 0.375F;
			this.fldExtensionAmount.Width = 1.125F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.1875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label20.Text = "Transfers - Large";
			this.Label20.Top = 0.75F;
			this.Label20.Width = 2.0625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.1875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label21.Text = "Transfers - Small";
			this.Label21.Top = 0.9375F;
			this.Label21.Width = 2.0625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.1875F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label22.Text = "Lost Plates";
			this.Label22.Top = 1.125F;
			this.Label22.Width = 2.0625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.1875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label23.Text = "Duplicates";
			this.Label23.Top = 1.3125F;
			this.Label23.Width = 2.0625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.1875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label24.Text = "Corrections";
			this.Label24.Top = 1.5F;
			this.Label24.Width = 2.0625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.1875F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label25.Text = "Titles";
			this.Label25.Top = 1.875F;
			this.Label25.Width = 2.0625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.1875F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label26.Text = "Sales Tax - Paid";
			this.Label26.Top = 2.25F;
			this.Label26.Width = 2.0625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.1875F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label27.Text = "Sales Tax - No Fee";
			this.Label27.Top = 2.4375F;
			this.Label27.Width = 2.0625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.1875F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.Label28.Text = "Total";
			this.Label28.Top = 2.6875F;
			this.Label28.Width = 2.0625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.1875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 2.625F;
			this.Line2.Width = 5F;
			this.Line2.X1 = 0.1875F;
			this.Line2.X2 = 5.1875F;
			this.Line2.Y1 = 2.625F;
			this.Line2.Y2 = 2.625F;
			// 
			// fldTransfersLargeUnits
			// 
			this.fldTransfersLargeUnits.Height = 0.1875F;
			this.fldTransfersLargeUnits.Left = 2.5F;
			this.fldTransfersLargeUnits.Name = "fldTransfersLargeUnits";
			this.fldTransfersLargeUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTransfersLargeUnits.Text = "Field4";
			this.fldTransfersLargeUnits.Top = 0.75F;
			this.fldTransfersLargeUnits.Width = 0.8125F;
			// 
			// fldTransfersSmallUnits
			// 
			this.fldTransfersSmallUnits.Height = 0.1875F;
			this.fldTransfersSmallUnits.Left = 2.5F;
			this.fldTransfersSmallUnits.Name = "fldTransfersSmallUnits";
			this.fldTransfersSmallUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTransfersSmallUnits.Text = "Field5";
			this.fldTransfersSmallUnits.Top = 0.9375F;
			this.fldTransfersSmallUnits.Width = 0.8125F;
			// 
			// fldLostPlateUnits
			// 
			this.fldLostPlateUnits.Height = 0.1875F;
			this.fldLostPlateUnits.Left = 2.5F;
			this.fldLostPlateUnits.Name = "fldLostPlateUnits";
			this.fldLostPlateUnits.Style = "font-size: 9pt; text-align: right";
			this.fldLostPlateUnits.Text = "Field6";
			this.fldLostPlateUnits.Top = 1.125F;
			this.fldLostPlateUnits.Width = 0.8125F;
			// 
			// fldDuplicateUnits
			// 
			this.fldDuplicateUnits.Height = 0.1875F;
			this.fldDuplicateUnits.Left = 2.5F;
			this.fldDuplicateUnits.Name = "fldDuplicateUnits";
			this.fldDuplicateUnits.Style = "font-size: 9pt; text-align: right";
			this.fldDuplicateUnits.Text = "Field7";
			this.fldDuplicateUnits.Top = 1.3125F;
			this.fldDuplicateUnits.Width = 0.8125F;
			// 
			// fldCorrectionUnits
			// 
			this.fldCorrectionUnits.Height = 0.1875F;
			this.fldCorrectionUnits.Left = 2.5F;
			this.fldCorrectionUnits.Name = "fldCorrectionUnits";
			this.fldCorrectionUnits.Style = "font-size: 9pt; text-align: right";
			this.fldCorrectionUnits.Text = "Field8";
			this.fldCorrectionUnits.Top = 1.5F;
			this.fldCorrectionUnits.Width = 0.8125F;
			// 
			// fldTitleUnits
			// 
			this.fldTitleUnits.Height = 0.1875F;
			this.fldTitleUnits.Left = 2.5F;
			this.fldTitleUnits.Name = "fldTitleUnits";
			this.fldTitleUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTitleUnits.Text = "Field9";
			this.fldTitleUnits.Top = 1.875F;
			this.fldTitleUnits.Width = 0.8125F;
			// 
			// fldSalesTaxPaidUnits
			// 
			this.fldSalesTaxPaidUnits.Height = 0.1875F;
			this.fldSalesTaxPaidUnits.Left = 2.5F;
			this.fldSalesTaxPaidUnits.Name = "fldSalesTaxPaidUnits";
			this.fldSalesTaxPaidUnits.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxPaidUnits.Text = "Field10";
			this.fldSalesTaxPaidUnits.Top = 2.25F;
			this.fldSalesTaxPaidUnits.Width = 0.8125F;
			// 
			// fldSalesTaxNoFeeUnits
			// 
			this.fldSalesTaxNoFeeUnits.Height = 0.1875F;
			this.fldSalesTaxNoFeeUnits.Left = 2.5F;
			this.fldSalesTaxNoFeeUnits.Name = "fldSalesTaxNoFeeUnits";
			this.fldSalesTaxNoFeeUnits.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxNoFeeUnits.Text = "Field11";
			this.fldSalesTaxNoFeeUnits.Top = 2.4375F;
			this.fldSalesTaxNoFeeUnits.Width = 0.8125F;
			// 
			// fldTotalUnits
			// 
			this.fldTotalUnits.Height = 0.1875F;
			this.fldTotalUnits.Left = 2.5F;
			this.fldTotalUnits.Name = "fldTotalUnits";
			this.fldTotalUnits.Style = "font-weight: bold; text-align: right";
			this.fldTotalUnits.Text = "Field12";
			this.fldTotalUnits.Top = 2.6875F;
			this.fldTotalUnits.Width = 0.8125F;
			// 
			// fldTransferLargeAmount
			// 
			this.fldTransferLargeAmount.Height = 0.1875F;
			this.fldTransferLargeAmount.Left = 3.625F;
			this.fldTransferLargeAmount.Name = "fldTransferLargeAmount";
			this.fldTransferLargeAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTransferLargeAmount.Text = "Field16";
			this.fldTransferLargeAmount.Top = 0.75F;
			this.fldTransferLargeAmount.Width = 1.125F;
			// 
			// fldTransferSmallAmount
			// 
			this.fldTransferSmallAmount.Height = 0.1875F;
			this.fldTransferSmallAmount.Left = 3.625F;
			this.fldTransferSmallAmount.Name = "fldTransferSmallAmount";
			this.fldTransferSmallAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTransferSmallAmount.Text = "Field17";
			this.fldTransferSmallAmount.Top = 0.9375F;
			this.fldTransferSmallAmount.Width = 1.125F;
			// 
			// fldLostPlateAmount
			// 
			this.fldLostPlateAmount.Height = 0.1875F;
			this.fldLostPlateAmount.Left = 3.625F;
			this.fldLostPlateAmount.Name = "fldLostPlateAmount";
			this.fldLostPlateAmount.Style = "font-size: 9pt; text-align: right";
			this.fldLostPlateAmount.Text = "Field18";
			this.fldLostPlateAmount.Top = 1.125F;
			this.fldLostPlateAmount.Width = 1.125F;
			// 
			// fldDuplicateAmount
			// 
			this.fldDuplicateAmount.Height = 0.1875F;
			this.fldDuplicateAmount.Left = 3.625F;
			this.fldDuplicateAmount.Name = "fldDuplicateAmount";
			this.fldDuplicateAmount.Style = "font-size: 9pt; text-align: right";
			this.fldDuplicateAmount.Text = "Field19";
			this.fldDuplicateAmount.Top = 1.3125F;
			this.fldDuplicateAmount.Width = 1.125F;
			// 
			// fldCorrectionAmount
			// 
			this.fldCorrectionAmount.Height = 0.1875F;
			this.fldCorrectionAmount.Left = 3.625F;
			this.fldCorrectionAmount.Name = "fldCorrectionAmount";
			this.fldCorrectionAmount.Style = "font-size: 9pt; text-align: right";
			this.fldCorrectionAmount.Text = "Field20";
			this.fldCorrectionAmount.Top = 1.5F;
			this.fldCorrectionAmount.Width = 1.125F;
			// 
			// fldTitleAmount
			// 
			this.fldTitleAmount.Height = 0.1875F;
			this.fldTitleAmount.Left = 3.625F;
			this.fldTitleAmount.Name = "fldTitleAmount";
			this.fldTitleAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTitleAmount.Text = "Field21";
			this.fldTitleAmount.Top = 1.875F;
			this.fldTitleAmount.Width = 1.125F;
			// 
			// fldSalesTaxPaidAmount
			// 
			this.fldSalesTaxPaidAmount.Height = 0.1875F;
			this.fldSalesTaxPaidAmount.Left = 3.625F;
			this.fldSalesTaxPaidAmount.Name = "fldSalesTaxPaidAmount";
			this.fldSalesTaxPaidAmount.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxPaidAmount.Text = "Field22";
			this.fldSalesTaxPaidAmount.Top = 2.25F;
			this.fldSalesTaxPaidAmount.Width = 1.125F;
			// 
			// fldSalesTaxNoFeeAmount
			// 
			this.fldSalesTaxNoFeeAmount.Height = 0.1875F;
			this.fldSalesTaxNoFeeAmount.Left = 3.625F;
			this.fldSalesTaxNoFeeAmount.Name = "fldSalesTaxNoFeeAmount";
			this.fldSalesTaxNoFeeAmount.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxNoFeeAmount.Text = "Field23";
			this.fldSalesTaxNoFeeAmount.Top = 2.4375F;
			this.fldSalesTaxNoFeeAmount.Width = 1.125F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 3.625F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-weight: bold; text-align: right";
			this.fldTotalAmount.Text = "Field24";
			this.fldTotalAmount.Top = 2.6875F;
			this.fldTotalAmount.Width = 1.125F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1875F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 0.1875F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label136.Text = "Transfers - 25 Year";
			this.Label136.Top = 0.5625F;
			this.Label136.Width = 2.0625F;
			// 
			// fldTransfers25YearUnits
			// 
			this.fldTransfers25YearUnits.Height = 0.1875F;
			this.fldTransfers25YearUnits.Left = 2.5F;
			this.fldTransfers25YearUnits.Name = "fldTransfers25YearUnits";
			this.fldTransfers25YearUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTransfers25YearUnits.Text = "Field4";
			this.fldTransfers25YearUnits.Top = 0.5625F;
			this.fldTransfers25YearUnits.Width = 0.8125F;
			// 
			// fldTransfer25YearAmount
			// 
			this.fldTransfer25YearAmount.Height = 0.1875F;
			this.fldTransfer25YearAmount.Left = 3.625F;
			this.fldTransfer25YearAmount.Name = "fldTransfer25YearAmount";
			this.fldTransfer25YearAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTransfer25YearAmount.Text = "Field16";
			this.fldTransfer25YearAmount.Top = 0.5625F;
			this.fldTransfer25YearAmount.Width = 1.125F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1875F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 0.1875F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label137.Text = "Titles - 25 Year";
			this.Label137.Top = 1.6875F;
			this.Label137.Width = 2.0625F;
			// 
			// fldTitle25YearUnits
			// 
			this.fldTitle25YearUnits.Height = 0.1875F;
			this.fldTitle25YearUnits.Left = 2.5F;
			this.fldTitle25YearUnits.Name = "fldTitle25YearUnits";
			this.fldTitle25YearUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTitle25YearUnits.Text = "Field9";
			this.fldTitle25YearUnits.Top = 1.6875F;
			this.fldTitle25YearUnits.Width = 0.8125F;
			// 
			// fldTitle25YearsAmount
			// 
			this.fldTitle25YearsAmount.Height = 0.1875F;
			this.fldTitle25YearsAmount.Left = 3.625F;
			this.fldTitle25YearsAmount.Name = "fldTitle25YearsAmount";
			this.fldTitle25YearsAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTitle25YearsAmount.Text = "Field21";
			this.fldTitle25YearsAmount.Top = 1.6875F;
			this.fldTitle25YearsAmount.Width = 1.125F;
			// 
			// Label138
			// 
			this.Label138.Height = 0.1875F;
			this.Label138.HyperLink = null;
			this.Label138.Left = 0.1875F;
			this.Label138.Name = "Label138";
			this.Label138.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label138.Text = "Rush Titles";
			this.Label138.Top = 2.0625F;
			this.Label138.Width = 2.0625F;
			// 
			// fldRushTitleUnits
			// 
			this.fldRushTitleUnits.Height = 0.1875F;
			this.fldRushTitleUnits.Left = 2.5F;
			this.fldRushTitleUnits.Name = "fldRushTitleUnits";
			this.fldRushTitleUnits.Style = "font-size: 9pt; text-align: right";
			this.fldRushTitleUnits.Text = "Field9";
			this.fldRushTitleUnits.Top = 2.0625F;
			this.fldRushTitleUnits.Width = 0.8125F;
			// 
			// fldRushTitleAmount
			// 
			this.fldRushTitleAmount.Height = 0.1875F;
			this.fldRushTitleAmount.Left = 3.625F;
			this.fldRushTitleAmount.Name = "fldRushTitleAmount";
			this.fldRushTitleAmount.Style = "font-size: 9pt; text-align: right";
			this.fldRushTitleAmount.Text = "Field21";
			this.fldRushTitleAmount.Top = 2.0625F;
			this.fldRushTitleAmount.Width = 1.125F;
			// 
			// rptLTTAgentSummary
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionsUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersLargeUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersSmallUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferLargeAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferSmallAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfers25YearUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfer25YearAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExtensionsUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExtensionAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfersLargeUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfersSmallUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLostPlateUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDuplicateUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitleUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxPaidUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxNoFeeUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferLargeAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferSmallAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLostPlateAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDuplicateAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitleAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxPaidAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxNoFeeAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfers25YearUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfer25YearAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle25YearUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle25YearsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRushTitleUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRushTitleAmount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
