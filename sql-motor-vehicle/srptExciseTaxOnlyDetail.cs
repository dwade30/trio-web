using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptExciseTaxOnlyDetail.
	/// </summary>
	public partial class srptExciseTaxOnlyDetail : GrapeCity.ActiveReports.SectionReport
	{
		private List<string> detailItems;
		private int currentIndex = 0;

		public srptExciseTaxOnlyDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public srptExciseTaxOnlyDetail(string title, List<string> detailItems) : this()
		{
			lblTitle.Text = title;
			this.detailItems = detailItems;
		}

		private void detail_Format(object sender, EventArgs e)
		{
			Field1.Text = "";
			Field2.Text = "";
			Field3.Text = "";
			Field4.Text = "";
			Field5.Text = "";
			Field1.Text = detailItems[currentIndex];
			currentIndex++;
			if (currentIndex < detailItems.Count)
			{
				Field2.Text = detailItems[currentIndex];
				currentIndex++;
			}
			if (currentIndex < detailItems.Count)
			{
				Field3.Text = detailItems[currentIndex];
				currentIndex++;
			}
			if (currentIndex < detailItems.Count)
			{
				Field4.Text = detailItems[currentIndex];
				currentIndex++;
			}
			if (currentIndex < detailItems.Count)
			{
				Field5.Text = detailItems[currentIndex];
				currentIndex++;
			}
		}

		private void srptExciseTaxOnlyDetail_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (currentIndex < detailItems.Count)
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}
	}
}
