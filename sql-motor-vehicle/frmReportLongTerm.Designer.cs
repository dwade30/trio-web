//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmReportLongTerm.
	/// </summary>
	partial class frmReportLongTerm
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCComboBox cmbInterim;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraClose;
		public fecherFoundation.FCFrame fraSetDates;
		public fecherFoundation.FCComboBox cboStart;
		public fecherFoundation.FCComboBox cboEnd;
		public fecherFoundation.FCButton cmdSaveDates;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCButton cmdCloseout;
		public fecherFoundation.FCButton cmdCloseoutPrint;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbReport = new fecherFoundation.FCComboBox();
			this.cmbInterim = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraClose = new fecherFoundation.FCFrame();
			this.fraSetDates = new fecherFoundation.FCFrame();
			this.cboStart = new fecherFoundation.FCComboBox();
			this.cboEnd = new fecherFoundation.FCComboBox();
			this.cmdSaveDates = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmdCloseout = new fecherFoundation.FCButton();
			this.cmdCloseoutPrint = new fecherFoundation.FCButton();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraClose)).BeginInit();
			this.fraClose.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSetDates)).BeginInit();
			this.fraSetDates.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseoutPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(483, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbInterim);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.fraClose);
			this.ClientArea.Size = new System.Drawing.Size(483, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(483, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(397, 30);
			this.HeaderText.Text = "Process End of Period - Long Term";
			// 
			// cmbReport
			// 
			this.cmbReport.AutoSize = false;
			this.cmbReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbReport.FormattingEnabled = true;
			this.cmbReport.Items.AddRange(new object[] {
            "LTT Plate Inventory",
            "Create Data File",
            "LTT Agent Inventory",
            "LTT Agent Detail",
            "LTT Agent Summary",
            "Full Set Of Reports",
            "Upload Data File To BMV"});
			this.cmbReport.Location = new System.Drawing.Point(20, 30);
			this.cmbReport.Name = "cmbReport";
			this.cmbReport.Size = new System.Drawing.Size(355, 40);
			this.cmbReport.TabIndex = 0;
			this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
			// 
			// cmbInterim
			// 
			this.cmbInterim.AutoSize = false;
			this.cmbInterim.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbInterim.FormattingEnabled = true;
			this.cmbInterim.Items.AddRange(new object[] {
            "Interim Reports",
            "Live Reports"});
			this.cmbInterim.Location = new System.Drawing.Point(30, 30);
			this.cmbInterim.Name = "cmbInterim";
			this.cmbInterim.Size = new System.Drawing.Size(254, 40);
			this.cmbInterim.TabIndex = 0;
			this.cmbInterim.Text = "Live Reports";
			this.cmbInterim.SelectedIndexChanged += new System.EventHandler(this.optInterim_CheckedChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmbReport);
			this.Frame1.Location = new System.Drawing.Point(30, 440);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(395, 90);
			this.Frame1.TabIndex = 13;
			this.Frame1.Text = "Listings";
			// 
			// fraClose
			// 
			this.fraClose.AppearanceKey = "groupBoxNoBorders";
			this.fraClose.Controls.Add(this.fraSetDates);
			this.fraClose.Controls.Add(this.cmdCloseout);
			this.fraClose.Controls.Add(this.cmdCloseoutPrint);
			this.fraClose.Location = new System.Drawing.Point(30, 90);
			this.fraClose.Name = "fraClose";
			this.fraClose.Size = new System.Drawing.Size(395, 330);
			this.fraClose.TabIndex = 1;
			this.fraClose.Text = " ";
			// 
			// fraSetDates
			// 
			this.fraSetDates.Controls.Add(this.cboStart);
			this.fraSetDates.Controls.Add(this.cboEnd);
			this.fraSetDates.Controls.Add(this.cmdSaveDates);
			this.fraSetDates.Controls.Add(this.Label1);
			this.fraSetDates.Controls.Add(this.Label2);
			this.fraSetDates.Location = new System.Drawing.Point(0, 120);
			this.fraSetDates.Name = "fraSetDates";
			this.fraSetDates.Size = new System.Drawing.Size(395, 210);
			this.fraSetDates.TabIndex = 4;
			this.fraSetDates.Text = "Set Reporting Dates";
			// 
			// cboStart
			// 
			this.cboStart.AutoSize = false;
			this.cboStart.BackColor = System.Drawing.SystemColors.Window;
			this.cboStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStart.FormattingEnabled = true;
			this.cboStart.Location = new System.Drawing.Point(157, 30);
			this.cboStart.Name = "cboStart";
			this.cboStart.Size = new System.Drawing.Size(218, 40);
			this.cboStart.TabIndex = 7;
			// 
			// cboEnd
			// 
			this.cboEnd.AutoSize = false;
			this.cboEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cboEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEnd.FormattingEnabled = true;
			this.cboEnd.Location = new System.Drawing.Point(157, 90);
			this.cboEnd.Name = "cboEnd";
			this.cboEnd.Size = new System.Drawing.Size(218, 40);
			this.cboEnd.TabIndex = 6;
			// 
			// cmdSaveDates
			// 
			this.cmdSaveDates.AppearanceKey = "actionButton";
			this.cmdSaveDates.Location = new System.Drawing.Point(20, 150);
			this.cmdSaveDates.Name = "cmdSaveDates";
			this.cmdSaveDates.Size = new System.Drawing.Size(212, 40);
			this.cmdSaveDates.TabIndex = 5;
			this.cmdSaveDates.Text = "Save Reporting Dates";
			this.cmdSaveDates.Click += new System.EventHandler(this.cmdSaveDates_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(80, 15);
			this.Label1.TabIndex = 9;
			this.Label1.Text = "START DATE";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(58, 17);
			this.Label2.TabIndex = 8;
			this.Label2.Text = "END DATE";
			// 
			// cmdCloseout
			// 
			this.cmdCloseout.AppearanceKey = "actionButton";
			this.cmdCloseout.Location = new System.Drawing.Point(0, 0);
			this.cmdCloseout.Name = "cmdCloseout";
			this.cmdCloseout.Size = new System.Drawing.Size(253, 40);
			this.cmdCloseout.TabIndex = 3;
			this.cmdCloseout.Text = "Close Off Reporting Period";
			this.cmdCloseout.Click += new System.EventHandler(this.cmdCloseout_Click);
			// 
			// cmdCloseoutPrint
			// 
			this.cmdCloseoutPrint.AppearanceKey = "actionButton";
			this.cmdCloseoutPrint.Location = new System.Drawing.Point(0, 60);
			this.cmdCloseoutPrint.Name = "cmdCloseoutPrint";
			this.cmdCloseoutPrint.Size = new System.Drawing.Size(209, 40);
			this.cmdCloseoutPrint.TabIndex = 2;
			this.cmdCloseoutPrint.Text = "Print Closeout Listing";
			this.cmdCloseoutPrint.Click += new System.EventHandler(this.cmdCloseoutPrint_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(174, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(120, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// frmReportLongTerm
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(483, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReportLongTerm";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Process End of Period - Long Term";
			this.Load += new System.EventHandler(this.frmReportLongTerm_Load);
			this.Activated += new System.EventHandler(this.frmReportLongTerm_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReportLongTerm_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraClose)).EndInit();
			this.fraClose.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSetDates)).EndInit();
			this.fraSetDates.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseoutPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdProcessSave;
    }
}