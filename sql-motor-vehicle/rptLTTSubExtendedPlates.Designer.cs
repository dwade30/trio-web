﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTSubExtendedPlates.
	/// </summary>
	partial class rptLTTSubExtendedPlates
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLTTSubExtendedPlates));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCompanyCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblNoInfo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCompanyCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPlate,
				this.fldCompanyCode
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label12,
				this.Label13,
				this.Line1,
				this.lblDescription
			});
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label14,
				this.Label15,
				this.Line2,
				this.Label16
			});
			this.GroupHeader1.Height = 0.5520833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblNoInfo
			});
			this.GroupFooter1.Height = 0.1875F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.15625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label12.Text = "Plate";
			this.Label12.Top = 0.34375F;
			this.Label12.Width = 1.34375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.8125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label13.Text = "Company Code";
			this.Label13.Top = 0.34375F;
			this.Label13.Width = 1.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.53125F;
			this.Line1.Width = 7.40625F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0.53125F;
			this.Line1.Y2 = 0.53125F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.21875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 2.65625F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Tahoma\'; font-size: 11pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.lblDescription.Text = "Issued Plates";
			this.lblDescription.Top = 0.09375F;
			this.lblDescription.Width = 2.1875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.15625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label14.Text = "Plate";
			this.Label14.Top = 0.34375F;
			this.Label14.Width = 1.34375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 1.8125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label15.Text = "Company Code";
			this.Label15.Top = 0.34375F;
			this.Label15.Width = 1.25F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.03125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.53125F;
			this.Line2.Width = 7.40625F;
			this.Line2.X1 = 0.03125F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.53125F;
			this.Line2.Y2 = 0.53125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.21875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.65625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 11pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label16.Text = "Extended Plates";
			this.Label16.Top = 0.09375F;
			this.Label16.Width = 2.1875F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 0.15625F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Text = "Field1";
			this.fldPlate.Top = 0F;
			this.fldPlate.Width = 1.34375F;
			// 
			// fldCompanyCode
			// 
			this.fldCompanyCode.Height = 0.1875F;
			this.fldCompanyCode.Left = 1.8125F;
			this.fldCompanyCode.Name = "fldCompanyCode";
			this.fldCompanyCode.Text = "Field2";
			this.fldCompanyCode.Top = 0F;
			this.fldCompanyCode.Width = 1.25F;
			// 
			// lblNoInfo
			// 
			this.lblNoInfo.Height = 0.1875F;
			this.lblNoInfo.HyperLink = null;
			this.lblNoInfo.Left = 0F;
			this.lblNoInfo.Name = "lblNoInfo";
			this.lblNoInfo.Style = "font-weight: bold; text-align: center";
			this.lblNoInfo.Text = "No Information";
			this.lblNoInfo.Top = 0F;
			this.lblNoInfo.Visible = false;
			this.lblNoInfo.Width = 3.21875F;
			// 
			// rptLTTSubExtendedPlates
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCompanyCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCompanyCode;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNoInfo;
	}
}
