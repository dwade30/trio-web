//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPurgeHeld.
	/// </summary>
	partial class frmPurgeHeld
	{
		public fecherFoundation.FCComboBox cmbOwner;
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCFrame fraSearch;
		public fecherFoundation.FCTextBox txtCompanyName;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCLabel lblCompanyName;
		public fecherFoundation.FCLabel lblFirstName;
		public fecherFoundation.FCLabel lblLastName;
		public fecherFoundation.FCButton cmdPurge;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCTextBox txtPlate;
		public fecherFoundation.FCButton cmdSearch;
		public FCGrid vsVehicles;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeHeld));
            this.cmbOwner = new fecherFoundation.FCComboBox();
            this.cmbName = new fecherFoundation.FCComboBox();
            this.lblName = new fecherFoundation.FCLabel();
            this.fraSearch = new fecherFoundation.FCFrame();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.lblCompanyName = new fecherFoundation.FCLabel();
            this.lblFirstName = new fecherFoundation.FCLabel();
            this.lblLastName = new fecherFoundation.FCLabel();
            this.txtCompanyName = new fecherFoundation.FCTextBox();
            this.cmdPurge = new fecherFoundation.FCButton();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.txtPlate = new fecherFoundation.FCTextBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.vsVehicles = new fecherFoundation.FCGrid();
            this.txtDate = new Global.T2KDateBox();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
            this.fraSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPurge);
            this.BottomPanel.Location = new System.Drawing.Point(0, 551);
            this.BottomPanel.Size = new System.Drawing.Size(674, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSearch);
            this.ClientArea.Controls.Add(this.cmbName);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.cmdSelect);
            this.ClientArea.Controls.Add(this.txtPlate);
            this.ClientArea.Controls.Add(this.cmdSearch);
            this.ClientArea.Controls.Add(this.vsVehicles);
            this.ClientArea.Controls.Add(this.txtDate);
            this.ClientArea.Controls.Add(this.lblInstructions);
            this.ClientArea.Size = new System.Drawing.Size(674, 491);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(674, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(286, 30);
            this.HeaderText.Text = "Purge Held Registrations";
            // 
            // cmbOwner
            // 
            this.cmbOwner.AutoSize = false;
            this.cmbOwner.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbOwner.FormattingEnabled = true;
            this.cmbOwner.Items.AddRange(new object[] {
			"Company",
			"Individual"});
            this.cmbOwner.Location = new System.Drawing.Point(20, 30);
            this.cmbOwner.Name = "cmbOwner";
            this.cmbOwner.Size = new System.Drawing.Size(249, 40);
            this.cmbOwner.TabIndex = 13;
            this.cmbOwner.Text = "Company";
            this.cmbOwner.SelectedIndexChanged += new System.EventHandler(this.cmbOwner_SelectedIndexChanged);
            // 
            // cmbName
            // 
            this.cmbName.AutoSize = false;
            this.cmbName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbName.FormattingEnabled = true;
            this.cmbName.Items.AddRange(new object[] {
            "Name",
            "Plate",
            "Date"});
            this.cmbName.Location = new System.Drawing.Point(157, 93);
            this.cmbName.Name = "cmbName";
            this.cmbName.Size = new System.Drawing.Size(149, 40);
            this.cmbName.TabIndex = 10;
            this.cmbName.Text = "Date";
            this.cmbName.SelectedIndexChanged += new System.EventHandler(this.cmbName_SelectedIndexChanged);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(30, 107);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(71, 15);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "PURGE BY";
            // 
            // fraSearch
            // 
            this.fraSearch.Controls.Add(this.cmbOwner);
            this.fraSearch.Controls.Add(this.txtFirstName);
            this.fraSearch.Controls.Add(this.txtLastName);
            this.fraSearch.Controls.Add(this.lblCompanyName);
            this.fraSearch.Controls.Add(this.lblFirstName);
            this.fraSearch.Controls.Add(this.lblLastName);
            this.fraSearch.Controls.Add(this.txtCompanyName);
            this.fraSearch.Location = new System.Drawing.Point(81, 139);
            this.fraSearch.Name = "fraSearch";
            this.fraSearch.Size = new System.Drawing.Size(453, 210);
            this.fraSearch.TabIndex = 9;
            this.fraSearch.Text = "Search Criteria";
            this.fraSearch.Visible = false;
            // 
            // txtFirstName
            // 
            this.txtFirstName.AutoSize = false;
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.LinkItem = null;
            this.txtFirstName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFirstName.LinkTopic = null;
            this.txtFirstName.Location = new System.Drawing.Point(184, 90);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(249, 40);
            this.txtFirstName.TabIndex = 11;
            this.txtFirstName.Visible = false;
            // 
            // txtLastName
            // 
            this.txtLastName.AutoSize = false;
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.LinkItem = null;
            this.txtLastName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLastName.LinkTopic = null;
            this.txtLastName.Location = new System.Drawing.Point(184, 150);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(249, 40);
            this.txtLastName.TabIndex = 10;
            this.txtLastName.Visible = false;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Location = new System.Drawing.Point(20, 104);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(110, 15);
            this.lblCompanyName.TabIndex = 17;
            this.lblCompanyName.Text = "COMPANY NAME";
            // 
            // lblFirstName
            // 
            this.lblFirstName.Location = new System.Drawing.Point(20, 104);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(80, 15);
            this.lblFirstName.TabIndex = 16;
            this.lblFirstName.Text = "FIRST NAME";
            this.lblFirstName.Visible = false;
            // 
            // lblLastName
            // 
            this.lblLastName.Location = new System.Drawing.Point(20, 164);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(80, 15);
            this.lblLastName.TabIndex = 15;
            this.lblLastName.Text = "LAST NAME";
            this.lblLastName.Visible = false;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.AutoSize = false;
            this.txtCompanyName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompanyName.LinkItem = null;
            this.txtCompanyName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCompanyName.LinkTopic = null;
            this.txtCompanyName.Location = new System.Drawing.Point(184, 90);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(249, 40);
            this.txtCompanyName.TabIndex = 12;
            // 
            // cmdPurge
            // 
            this.cmdPurge.AppearanceKey = "acceptButton";
            this.cmdPurge.Location = new System.Drawing.Point(288, 30);
            this.cmdPurge.Name = "cmdPurge";
            this.cmdPurge.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPurge.Size = new System.Drawing.Size(88, 48);
            this.cmdPurge.TabIndex = 8;
            this.cmdPurge.Text = "Purge";
            this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.AppearanceKey = "actionButton";
            this.cmdSelect.Location = new System.Drawing.Point(153, 429);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(115, 40);
            this.cmdSelect.TabIndex = 2;
            this.cmdSelect.Text = "Select All";
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            // 
            // txtPlate
            // 
            this.txtPlate.AutoSize = false;
            this.txtPlate.BackColor = System.Drawing.SystemColors.Window;
            this.txtPlate.LinkItem = null;
            this.txtPlate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPlate.LinkTopic = null;
            this.txtPlate.Location = new System.Drawing.Point(157, 154);
            this.txtPlate.Name = "txtPlate";
            this.txtPlate.Size = new System.Drawing.Size(149, 40);
            this.txtPlate.TabIndex = 1;
            this.txtPlate.Visible = false;
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Location = new System.Drawing.Point(30, 429);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(93, 40);
            this.cmdSearch.TabIndex = 0;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // vsVehicles
            // 
            this.vsVehicles.AllowSelection = false;
            this.vsVehicles.AllowUserToResizeColumns = false;
            this.vsVehicles.AllowUserToResizeRows = false;
            this.vsVehicles.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsVehicles.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorBkg = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorSel = System.Drawing.Color.Empty;
            this.vsVehicles.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsVehicles.Cols = 9;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsVehicles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsVehicles.ColumnHeadersHeight = 30;
            this.vsVehicles.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsVehicles.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsVehicles.DragIcon = null;
            this.vsVehicles.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsVehicles.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.FrozenCols = 0;
            this.vsVehicles.GridColor = System.Drawing.Color.Empty;
            this.vsVehicles.GridColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.Location = new System.Drawing.Point(30, 214);
            this.vsVehicles.Name = "vsVehicles";
            this.vsVehicles.OutlineCol = 0;
            this.vsVehicles.ReadOnly = true;
            this.vsVehicles.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsVehicles.RowHeightMin = 0;
            this.vsVehicles.Rows = 50;
            this.vsVehicles.ScrollTipText = null;
            this.vsVehicles.ShowColumnVisibilityMenu = false;
            this.vsVehicles.ShowFocusCell = false;
            this.vsVehicles.Size = new System.Drawing.Size(616, 196);
            this.vsVehicles.StandardTab = true;
            this.vsVehicles.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsVehicles.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsVehicles.TabIndex = 18;
            this.vsVehicles.Visible = false;
            this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
            this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
			this.vsVehicles.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(this.vsVehicles_ColumnHeaderClickEvent);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(157, 154);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(149, 40);
            this.txtDate.TabIndex = 19;
            this.txtDate.Text = "  /  /";
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(616, 48);
            this.lblInstructions.TabIndex = 20;
            this.lblInstructions.Text = resources.GetString("lblInstructions.Text");
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 0;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmPurgeHeld
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(674, 659);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPurgeHeld";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Purge Held Registrations";
            this.Load += new System.EventHandler(this.frmPurgeHeld_Load);
            this.Activated += new System.EventHandler(this.frmPurgeHeld_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeHeld_KeyPress);
            this.Resize += new System.EventHandler(this.frmPurgeHeld_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
            this.fraSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}