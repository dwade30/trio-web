//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmSingleLongTermCTA : BaseForm
	{
		public frmSingleLongTermCTA()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSingleLongTermCTA InstancePtr
		{
			get
			{
				return (frmSingleLongTermCTA)Sys.GetInstance(typeof(frmSingleLongTermCTA));
			}
		}

		protected frmSingleLongTermCTA _InstancePtr = null;
		//=========================================================
		clsDRWrapper rs1 = new clsDRWrapper();
		clsDRWrapper rsResCode = new clsDRWrapper();
		int NewBackColor = ColorTranslator.ToOle(Color.Yellow);
		// &HD2FFFF
		// vbPorter upgrade warning: OldBackColor As int	OnWrite(Color)
		int OldBackColor;
		bool blnPrinted;
		bool blnDoubleCompleted;

		private void chkFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkFleet.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (MotorVehicle.Statics.VehiclesCovered < 2)
				{
					MotorVehicle.Statics.VehiclesCovered = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("How many vehicles will be covered by this CTA Form?", "Number of Vehicles?", null))));
				}
				if (MotorVehicle.Statics.VehiclesCovered < 2)
				{
					MotorVehicle.Statics.FleetCTA = false;
					chkFleet.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				else
				{
					MotorVehicle.Statics.FleetCTA = true;
				}
			}
			else
			{
				MotorVehicle.Statics.FleetCTA = false;
				MotorVehicle.Statics.VehiclesCovered = 0;
			}
		}

		private void chkGetOwnerInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsFleet = new clsDRWrapper();
			if (chkGetOwnerInfo.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtUTCSellerName1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName1"));
				txtUTCSellerName2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName2"));
				txtUTCSellerAddress.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
				txtUTCSellerCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
				txtUTCSellerState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State"));
				txtUTCSellerZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")) != 0)
				{
					rsFleet.OpenRecordset("SELECT * FROM FleetMaster WHERE FleetNumber = " + FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"))), "TWMV0000.vb1");
					if (rsFleet.BeginningOfFile() != true && rsFleet.EndOfFile() != true)
					{
						if (FCConvert.ToString(rsFleet.Get_Fields("Owner1Code")) == "I")
						{
							txtCTADOB1.Text = Strings.Format(rsFleet.Get_Fields("DOB1"), "MM/dd/yyyy");
						}
						if (FCConvert.ToString(rsFleet.Get_Fields("Owner2Code")) == "I")
						{
							txtCTADOB2.Text = Strings.Format(rsFleet.Get_Fields("DOB2"), "MM/dd/yyyy");
						}
					}
				}
				if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence") != MotorVehicle.Statics.rsFinal.Get_Fields_String("City") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence")) != "")
				{
					txtCTAResTown.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
					txtCTAApplicantState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
				}
				txtUTCSellerName1.Focus();
			}
			else
			{
				txtUTCSellerName1.Text = "";
				txtUTCSellerName2.Text = "";
				txtUTCSellerAddress.Text = "";
				txtUTCSellerCity.Text = "";
				txtUTCSellerState.Text = "";
				txtUTCSellerZip.Text = "";
				txtCTADOB1.Text = "";
				txtCTADOB2.Text = "";
				txtCTAResAddress.Text = "";
				txtCTAResTown.Text = "";
				txtCTAApplicantState.Text = "";
				txtUTCSellerName1.Focus();
			}
		}

		private void chkNoFee_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkNoFee.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PH")
				{
					chkNoFee.CheckState = Wisej.Web.CheckState.Checked;
					MessageBox.Show("There is no title fee charged for a PH class vehicle.", "No Title Fee", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			txtDoubleCity.Text = "";
			txtDoubleCTANumber.Text = "";
			txtDoubleCylinders.Text = "";
			txtDoublePlate.Text = "";
			txtDoublePreviousState.Text = "";
			txtDoublePreviousTitle.Text = "";
			txtDoublePurchaseDate.Text = "";
			txtDoubleSellerAddress.Text = "";
			txtDoubleSellerName.Text = "";
			txtDoubleState.Text = "";
			txtDoubleZip.Text = "";
			cmbDoubleDealer.Text = "";
			cmbDoubleVehicleType.Text = "Used";
			fraDoubleInfo.Visible = false;
			fraApplicant.Enabled = true;
			fraVehicleInfo.Enabled = true;
			fraLienHolder.Enabled = true;
			Frame2.Enabled = true;
			cmbCTAPrint.Enabled = true;
			txtCTANumber.Enabled = true;
		}

		private bool SaveTitleToPrint()
		{
			bool SaveTitleToPrint = false;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: ans As object	OnWrite(DialogResult)
				DialogResult ans = 0;
				bool blnSecondLienHolder;
				clsDRWrapper rsTitleCost = new clsDRWrapper();
				clsDRWrapper rsTitleApp = new clsDRWrapper();
				SaveTitleToPrint = false;
				rsTitleCost.OpenRecordset("SELECT * FROM DefaultInfo", "TWMV0000.vb1");
				if (MotorVehicle.Statics.PlateType != 2)
				{
					if (cmbMSRP.Text != "New" && cmbMSRP.Text != "Used" && cmbMSRP.Text != "N/R")
					{
						MessageBox.Show("You must make an MSRP Selection before you may continue.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveTitleToPrint;
					}
					if (!Information.IsDate(txtCATDatePurchased.Text))
					{
						MessageBox.Show("You must input a valid Purchase Date before saving this information.", "Invalid Purchase Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtCATDatePurchased.Focus();
						return SaveTitleToPrint;
					}
					if (fecherFoundation.Strings.Trim(txtCTAPreviousNumber.Text) == "")
					{
						MessageBox.Show("You must input a valid Previous Title Number before saving this information.", "Invalid Previous Title Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtCTAPreviousNumber.Focus();
						return SaveTitleToPrint;
					}
					if (fecherFoundation.Strings.Trim(txtCTAStateOfOrigin.Text) == "")
					{
						MessageBox.Show("You must input a valid Previous State before saving this information.", "Invalid Previous State", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtCTAStateOfOrigin.Focus();
						return SaveTitleToPrint;
					}
					if (fecherFoundation.Strings.Trim(txtCTANumber.Text) == "")
					{
						MessageBox.Show("You must input a CTA Number before saving this information.", "Invalid CTA Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtCTANumber.Focus();
						return SaveTitleToPrint;
					}
					if (fecherFoundation.Strings.Trim(txtUTCSellerName1.Text) != "" && fecherFoundation.Strings.Trim(txtUTCSellerName2.Text) != "" && chkCTAJoint.CheckState == Wisej.Web.CheckState.Unchecked)
					{
						ans = MessageBox.Show("You have entered 2 owners but have not checked off joint ownership.  Are you sure you wish to continue without selecting this option?", "Joint Ownership?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.No)
						{
							chkCTAJoint.Focus();
							return SaveTitleToPrint;
						}
					}
				}
				if (chkDouble.CheckState == Wisej.Web.CheckState.Checked && fraDoubleInfo.Visible == false && !blnDoubleCompleted)
				{
					fraApplicant.Enabled = false;
					fraVehicleInfo.Enabled = false;
					fraLienHolder.Enabled = false;
					Frame2.Enabled = false;
					cmbCTAPrint.Enabled = false;
					txtCTANumber.Enabled = false;
					fraDoubleInfo.Visible = true;
					txtDoubleCTANumber.Focus();
					return SaveTitleToPrint;
				}
				else if (fraDoubleInfo.Visible == true)
				{
					return SaveTitleToPrint;
				}
				this.MousePointer = MousePointerConstants.vbHourglass;
				rs1.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
				if (MotorVehicle.Statics.lngCTAAddNewID != 0)
				{
					rs1.Edit();
				}
				else
				{
					rs1.AddNew();
				}
				rs1.Set_Fields("Name1", txtUTCSellerName1.Text);
				if (Information.IsDate(txtCTADOB1.Text))
					rs1.Set_Fields("DOB1", Strings.Format(txtCTADOB1.Text, "MM/dd/yyyy"));
				rs1.Set_Fields("name2", txtUTCSellerName2.Text);
				if (Information.IsDate(txtCTADOB2.Text))
					rs1.Set_Fields("DOB2", Strings.Format(txtCTADOB2.Text, "MM/dd/yyyy"));
				rs1.Set_Fields("Joint", chkCTAJoint.CheckState);
				rs1.Set_Fields("Address", txtUTCSellerAddress.Text);
				rs1.Set_Fields("City", txtUTCSellerCity.Text);
				rs1.Set_Fields("State", txtUTCSellerState.Text);
				rs1.Set_Fields("ZipAndZip4", txtUTCSellerZip.Text);
				rs1.Set_Fields("ResidenceAddress", txtCTAResAddress.Text);
				rs1.Set_Fields("ResidenceCity", txtCTAResTown.Text);
				rs1.Set_Fields("ResidenceState", txtCTAApplicantState.Text);
				rs1.Set_Fields("ResidenceZip", txtCTALegalZip.Text);
				rs1.Set_Fields("TelephoneNumber", txtCTATelNumber.Text);
				if (cmbVehicleType.Text == "New")
				{
					rs1.Set_Fields("NewUsedRebuilt", "N");
				}
				else if (cmbVehicleType.Text == "Used")
				{
					rs1.Set_Fields("NewUsedRebuilt", "U");
				}
				else if (cmbVehicleType.Text == "Rebuilt")
				{
					rs1.Set_Fields("NewUsedRebuilt", "R");
				}
				else
				{
					rs1.Set_Fields("NewUsedRebuilt", "");
				}
				if (cmbOdometer.Text == "Miles")
				{
					rs1.Set_Fields("OdometerMK", "M");
				}
				else if (cmbOdometer.Text == "Kilometers")
				{
					rs1.Set_Fields("OdometerMK", "K");
				}
				else
				{
					rs1.Set_Fields("OdometerMK", "");
				}
				if (!MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					rs1.Set_Fields("OdometerReading", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer"));
				}
				if (cmbOdometerCondition.Text == "Actual")
				{
					rs1.Set_Fields("OdometerCondition", "A");
				}
				else if (cmbOdometerCondition.Text == "Excess")
				{
					rs1.Set_Fields("OdometerCondition", "E");
				}
				else if (cmbOdometerCondition.Text == "Changed")
				{
					rs1.Set_Fields("OdometerCondition", "C");
				}
				else if (cmbOdometerCondition.Text == "Broken")
				{
					rs1.Set_Fields("OdometerCondition", "B");
				}
				else
				{
					rs1.Set_Fields("OdometerCondition", "");
				}
				if (cmbMSRP.Text == "New")
				{
					rs1.Set_Fields("MSRP", "N");
				}
				else if (cmbMSRP.Text == "Used")
				{
					rs1.Set_Fields("MSRP", "U");
				}
				else
				{
					rs1.Set_Fields("MSRP", "NR");
				}
				rs1.Set_Fields("NumberOfCylinders", FCConvert.ToString(Conversion.Val(txtCTANumberOfCylinders.Text)));
				if (Information.IsDate(Strings.Format(txtCATDatePurchased.Text, "MM/dd/yyyy")))
					rs1.Set_Fields("PurchaseDate", Strings.Format(txtCATDatePurchased.Text, "MM/dd/yyyy"));
				rs1.Set_Fields("PreviousTitle", txtCTAPreviousNumber.Text);
				rs1.Set_Fields("StateOfOrigin", txtCTAStateOfOrigin.Text);
				rs1.Set_Fields("SellerName", txtCTASellerName.Text);
				rs1.Set_Fields("SellerAddress1", txtCTASellerAddress.Text);
				rs1.Set_Fields("SellerCity", txtCTASellerCity.Text);
				rs1.Set_Fields("SellerState", txtCTASellerState.Text);
				rs1.Set_Fields("SellerZipAndZip4", txtCTASellerZip.Text);
				if (cmbDealer.Text == "D")
				{
					rs1.Set_Fields("DealerType", "D");
				}
				else if (cmbDealer.Text == "UC")
				{
					rs1.Set_Fields("DealerType", "UC");
				}
				else if (cmbDealer.Text == "MC")
				{
					rs1.Set_Fields("DealerType", "MC");
				}
				else
				{
					rs1.Set_Fields("DealerType", "");
				}
				rs1.Set_Fields("DealerPlate", txtCTADealerPlate.Text);
				rs1.Set_Fields("LH1Name", txtCTALienOneName.Text);
				rs1.Set_Fields("LH1Address1", txtCTALienOneAddress.Text);
				rs1.Set_Fields("LH1City", txtCTALienOneCity.Text);
				rs1.Set_Fields("LH1State", txtCTALienOneState.Text);
				rs1.Set_Fields("LH1ZipAndZip4", txtCTALienOneZip.Text);
				if (Information.IsDate(Strings.Format(txt1stLienDate.Text, "MM/dd/yyyy")))
					rs1.Set_Fields("LH1Date", Strings.Format(txt1stLienDate.Text, "MM/dd/yyyy"));
				rs1.Set_Fields("LH2Name", txtCTALienTwoName.Text);
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("LH2Name"))) != "" && fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("LH2Name")))) != "NONE")
				{
					blnSecondLienHolder = true;
				}
				else
				{
					blnSecondLienHolder = false;
				}
				rs1.Set_Fields("LH2Address1", txtCTALienTwoAddress.Text);
				rs1.Set_Fields("LH2City", txtCTALienTwoCity.Text);
				rs1.Set_Fields("LH2State", txtCTALienTwoState.Text);
				rs1.Set_Fields("lh2zipandzip4", txtCTALienTwoZip.Text);
				if (Information.IsDate(Strings.Format(txt2ndLienDate.Text, "MM/dd/yyyy")))
					rs1.Set_Fields("LH2Date", Strings.Format(txt2ndLienDate.Text, "MM/dd/yyyy"));
				rs1.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
				rs1.Set_Fields("plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
				rs1.Set_Fields("MVR3Number", 0);
				if (chkRushTitle.CheckState == Wisej.Web.CheckState.Checked)
				{
					rs1.Set_Fields("RushTitle", true);
				}
				else
				{
					rs1.Set_Fields("RushTitle", false);
				}
				if (chkDouble.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (MotorVehicle.Statics.rsDoubleTitle.EndOfFile() != true && MotorVehicle.Statics.rsDoubleTitle.BeginningOfFile() != true)
					{
						// do nothing
					}
					else
					{
						if (FCConvert.ToInt32(rs1.Get_Fields_String("DoubleNumber")) != 0)
						{
							MotorVehicle.Statics.rsDoubleTitle.OpenRecordset("SELECT * FROM DoubleCTA WHERE ID = " + rs1.Get_Fields_String("DoubleNumber"), "TWMV0000.vb1");
						}
					}
					MotorVehicle.Statics.rsDoubleTitle.Update();
				}
				if (chkDouble.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					rs1.Set_Fields("DoubleNumber", 0);
					MotorVehicle.Statics.rsDoubleTitle = null;
				}
				else
				{
					rs1.Set_Fields("DoubleNumber", MotorVehicle.Statics.rsDoubleTitle.Get_Fields_Int32("ID"));
				}
				rs1.Update();
				MotorVehicle.Statics.lngCTAAddNewID = FCConvert.ToInt32(rs1.Get_Fields_Int32("ID"));
				//App.DoEvents();
				this.MousePointer = MousePointerConstants.vbDefault;
				SaveTitleToPrint = true;
				return SaveTitleToPrint;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				this.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Certificate of Title Application was not successfully saved.  (Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " was encountered.  " + fecherFoundation.Information.Err(ex).Description + ".)", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				fecherFoundation.Information.Err(ex).Clear();
			}
			return SaveTitleToPrint;
		}

		private void cmdReturn_Click()
		{
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				object ans;
				bool blnSecondLienHolder;
				clsDRWrapper rsTitleCost = new clsDRWrapper();
				clsDRWrapper rsTitleApp = new clsDRWrapper();
				rsTitleCost.OpenRecordset("SELECT * FROM DefaultInfo");
				if (SaveTitleToPrint())
				{
					if (cmbCTAPrint.Text == "This Form NEEDS to be printed" && !blnPrinted)
					{
						MessageBox.Show("You must print your CTA before saving this information.", "Invalid CTA Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtUTCSellerName1.Focus();
						return;
					}
					this.MousePointer = MousePointerConstants.vbHourglass;
					rs1.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
					rsTitleApp.OpenRecordset("SELECT * FROM LongTermTitleApplications", "TWMV0000.vb1");
					rsTitleApp.AddNew();
					rsTitleApp.Set_Fields("CTANumber", fecherFoundation.Strings.Trim(txtCTANumber.Text).ToUpper());
					if (chkNoFee.CheckState == Wisej.Web.CheckState.Unchecked)
					{
						if (rsTitleCost.EndOfFile() != true && rsTitleCost.BeginningOfFile() != true)
						{
							rsTitleCost.MoveLast();
							rsTitleCost.MoveFirst();
							if (MotorVehicle.Statics.FleetCTA)
							{
								rsTitleApp.Set_Fields("Fee", rsTitleCost.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered);
								rsTitleApp.Set_Fields("Units", MotorVehicle.Statics.VehiclesCovered);
							}
							else
							{
								rsTitleApp.Set_Fields("Fee", rsTitleCost.Get_Fields("TitleFee"));
								rsTitleApp.Set_Fields("Units", 1);
							}
							if (chkDouble.CheckState == Wisej.Web.CheckState.Checked)
							{
								rsTitleApp.Set_Fields("Fee", rsTitleApp.Get_Fields("Fee") * 2);
							}
							if (chkRushTitle.CheckState == Wisej.Web.CheckState.Checked)
							{
								rsTitleApp.Set_Fields("RushFee", rsTitleCost.Get_Fields("RushTitleFee"));
							}
							else
							{
								rsTitleApp.Set_Fields("RushFee", 0);
							}
						}
					}
					else
					{
						rsTitleApp.Set_Fields("Fee", 0);
						rsTitleApp.Set_Fields("Units", 1);
						rsTitleApp.Set_Fields("RushFee", 0);
					}
					rsTitleApp.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
					rsTitleApp.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
					rsTitleApp.Set_Fields("RegistrationDate", DateTime.Today);
					rsTitleApp.Set_Fields("CustomerName", rs1.Get_Fields_String("Name1"));
					rsTitleApp.Set_Fields("OPID", MotorVehicle.Statics.UserID);
					rsTitleApp.Set_Fields("DoubleIfApplicable", rs1.Get_Fields_String("DoubleNumber"));
					rsTitleApp.Set_Fields("PeriodCloseoutID", 0);
					rsTitleApp.Set_Fields("MSRP", rs1.Get_Fields_String("MSRP"));
					rsTitleApp.Set_Fields("StandAlone", true);
					rsTitleApp.Update();
					//App.DoEvents();
					rs1.Reset();
					rsTitleCost.Reset();
					this.MousePointer = MousePointerConstants.vbDefault;
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				this.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Certificate of Title Application was not successfully saved.  (Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " was encountered.  " + fecherFoundation.Information.Err(ex).Description + ".)", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				fecherFoundation.Information.Err(ex).Clear();
			}
		}

		private void PrintCTA()
		{
			int ans;
			clsDRWrapper rsTitle = new clsDRWrapper();
			string NewCTA = "";
			string temp = "";
			GrapeCity.ActiveReports.Export.Word.Section.RtfExport ArTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
			if (MotorVehicle.Statics.lngCTAAddNewID != 0)
			{
				rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
				if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
				{
					rsTitle.MoveLast();
					rsTitle.MoveFirst();
				}
			}
			if (MotorVehicle.Statics.PlateType == 2)
			{
				//rptDuplicateCTA.InstancePtr.PrintReport(false);
                rptDuplicateCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
				rptDuplicateCTA.InstancePtr.Unload();
				if (cmbCTAPrint.Text == "This Form NEEDS to be printed")
				{
					if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) == 0 || (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0 && MotorVehicle.Statics.PrintDouble == false))
					{
						blnPrinted = true;
					}
					else
					{
						MotorVehicle.Statics.boolDoubleCTA = true;
						//rptDuplicateCTA.InstancePtr.PrintReport(false);
                        rptDuplicateCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
						rptDuplicateCTA.InstancePtr.Unload();
					}
				}
			}
			else
			{
				if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to level as possible) next to the line titled:" + "\r\n" + "THIS IS NOT A CERTIFICATE OF TITLE", 47, 1, MotorVehicle.Statics.gstrCTAPrinterName) == DialogResult.Cancel)
				{
					MotorVehicle.Statics.blnPrintCTA = false;
				}
				else
				{
                    rptNewCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                    rptNewCTA.InstancePtr.Unload();
				}
				if (cmbCTAPrint.Text == "This Form NEEDS to be printed")
				{
					if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) == 0 || (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0 && MotorVehicle.Statics.PrintDouble == false))
					{
						blnPrinted = true;
					}
					else
					{
						MotorVehicle.Statics.boolDoubleCTA = true;
						MessageBox.Show("Click OK when ready to print the second CTA.", "Double CTA", MessageBoxButtons.OK, MessageBoxIcon.Information);
						
						if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to level as possible) next to the line titled:" + "\r\n" + "THIS IS NOT A CERTIFICATE OF TITLE", 47, 1, MotorVehicle.Statics.gstrCTAPrinterName) == DialogResult.Cancel)
						{
							MotorVehicle.Statics.blnPrintCTA = false;
						}
						else
						{
                            rptNewCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                            rptNewCTA.InstancePtr.Unload();
						}
						if (MotorVehicle.Statics.blnPrintDoubleCTA == true)
						{
							blnPrinted = true;
						}
					}
				}
				/*? On Error Resume Next  *///Printer = null;
				/*? On Error GoTo 0 */
				fecherFoundation.Information.Err().Clear();
				
			}
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (txtDoubleCTANumber.Text == "")
			{
				MessageBox.Show("You must input a CTA Number before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoubleCTANumber.Focus();
				return;
			}
			if (txtDoubleSellerName.Text == "")
			{
				MessageBox.Show("You must input a Seller Name before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoubleSellerName.Focus();
				return;
			}
			if (txtDoubleSellerAddress.Text == "")
			{
				MessageBox.Show("You must input a Seller Address before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoubleSellerAddress.Focus();
				return;
			}
			if (txtDoubleCity.Text == "")
			{
				MessageBox.Show("You must input a Seller City before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoubleCity.Focus();
				return;
			}
			if (txtDoubleState.Text == "")
			{
				MessageBox.Show("You must input a Seller State before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoubleState.Focus();
				return;
			}
			if (txtDoubleZip.Text == "")
			{
				MessageBox.Show("You must input a Seller Zip Code before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoubleZip.Focus();
				return;
			}
			if (txtDoublePurchaseDate.IsEmpty || !Information.IsDate(txtDoublePurchaseDate.Text))
			{
				MessageBox.Show("You must input a Purchase Date before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoublePurchaseDate.Focus();
				return;
			}
			if (txtDoublePreviousTitle.Text == "")
			{
				MessageBox.Show("You must input a Previous Title before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoublePreviousTitle.Focus();
				return;
			}
			if (txtDoublePreviousState.Text == "")
			{
				MessageBox.Show("You must input a Previous State before you can save.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDoublePreviousState.Focus();
				return;
			}
			fraApplicant.Enabled = true;
			fraVehicleInfo.Enabled = true;
			fraLienHolder.Enabled = true;
			Frame2.Enabled = true;
			cmbCTAPrint.Enabled = true;
			txtCTANumber.Enabled = true;
			fraDoubleInfo.Visible = false;
			if (chkPrintDouble.CheckState == Wisej.Web.CheckState.Checked)
			{
				MotorVehicle.Statics.PrintDouble = true;
			}
			else
			{
				MotorVehicle.Statics.PrintDouble = false;
			}
			bool executeAddRecord = false;
			if (MotorVehicle.Statics.rsDoubleTitle.EndOfFile() != true && MotorVehicle.Statics.rsDoubleTitle.BeginningOfFile() != true)
			{
				if (MotorVehicle.Statics.lngCTAAddNewID != 0)
				{
					rs1.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
				}
				if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
				{
					if (Conversion.Val(rs1.Get_Fields_String("DoubleNumber")) == MotorVehicle.Statics.rsDoubleTitle.Get_Fields_Int32("ID"))
					{
						// do nothing
					}
					else
					{
						executeAddRecord = true;
						goto AddRecord;
					}
				}
				else
				{
					executeAddRecord = true;
					goto AddRecord;
				}
			}
			else
			{
				executeAddRecord = true;
				goto AddRecord;
			}
			AddRecord:
			;
			if (executeAddRecord)
			{
				MotorVehicle.Statics.rsDoubleTitle.OpenRecordset("SELECT * FROM DoubleCTA");
				MotorVehicle.Statics.rsDoubleTitle.AddNew();
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("CTANumber", txtDoubleCTANumber.Text.ToUpper	());
				executeAddRecord = false;
			}
			if (cmbDoubleVehicleType.Text == "New")
			{
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("Type", "N");
			}
			else if (cmbDoubleVehicleType.Text == "Used")
			{
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("Type", "U");
			}
			else
			{
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("Type", "R");
			}
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("SellerName", txtDoubleSellerName.Text);
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("SellerAddress", txtDoubleSellerAddress.Text);
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("SellerCity", txtDoubleCity.Text);
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("SellerState", txtDoubleState.Text);
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("SellerZip", txtDoubleZip.Text);
			if (cmbDoubleDealer.Text == "D")
			{
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("DealerType", "D");
			}
			else if (cmbDoubleDealer.Text == "UC")
			{
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("DealerType", "UC");
			}
			else
			{
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("DealerType", "MC");
			}
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("plate", txtDoublePlate.Text);
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("PurchaseDate", txtDoublePurchaseDate.Text);
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("Cylinders", FCConvert.ToString(Conversion.Val(txtDoubleCylinders.Text)));
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("PreviousTitle", txtDoublePreviousTitle.Text);
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("PreviousState", txtDoublePreviousState.Text);
			if (Information.IsDate(txtOriginalDOB.Text))
			{
				MotorVehicle.Statics.rsDoubleTitle.Set_Fields("DOB", txtOriginalDOB.Text);
			}
			MotorVehicle.Statics.rsDoubleTitle.Set_Fields("Customer", txtCTASellerName.Text);
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmSingleLongTermCTA_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTitle = new clsDRWrapper();
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			MotorVehicle.Statics.FleetCTA = false;
			MotorVehicle.Statics.VehiclesCovered = 0;
			chkGetOwnerInfo.Enabled = true;
			if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PH")
			{
				chkNoFee.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (MotorVehicle.Statics.PlateType == 2)
			{
				cmbIllegible.Visible = true;
				lblCTANumber.Visible = false;
				txtCTANumber.Visible = false;
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber")) != "")
				{
					rsTitle.OpenRecordset("SELECT * FROM Title WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber") + "'");
					if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
					{
						MotorVehicle.Statics.lngCTAAddNewID = FCConvert.ToInt32(rsTitle.Get_Fields_Int32("ID"));
						FillScreen();
					}
				}
			}
			else
			{
				cmbIllegible.Visible = false;
				lblCTANumber.Visible = true;
				txtCTANumber.Visible = true;
			}
		}

		private void FillScreen()
		{
			rs1.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
			fecherFoundation.Information.Err().Clear();
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				if (FCConvert.ToString(rs1.Get_Fields_String("MSRP")) == "N")
				{
					cmbMSRP.Text = "New";
				}
				else if (rs1.Get_Fields_String("MSRP") == "U")
				{
					cmbMSRP.Text = "Used";
				}
				else
				{
					cmbMSRP.Text = "N/R";
				}
				txtUTCSellerName1.Text = FCConvert.ToString(rs1.Get_Fields_String("Name1"));
				if (Information.IsDate(rs1.Get_Fields("DOB1")))
					txtCTADOB1.Text = Strings.Format(rs1.Get_Fields_DateTime("DOB1"), "MM/dd/yyyy");
				txtUTCSellerName2.Text = FCConvert.ToString(rs1.Get_Fields_String("name2"));
				if (Information.IsDate(rs1.Get_Fields("DOB2")))
					txtCTADOB2.Text = Strings.Format(rs1.Get_Fields_DateTime("DOB2"), "MM/dd/yyyy");
				chkCTAJoint.CheckState = (rs1.Get_Fields_Boolean("Joint") == true ? CheckState.Checked : CheckState.Unchecked);
				txtUTCSellerAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("Address"));
				txtUTCSellerCity.Text = FCConvert.ToString(rs1.Get_Fields_String("City"));
				txtUTCSellerState.Text = FCConvert.ToString(rs1.Get_Fields("State"));
				if (fecherFoundation.FCUtils.IsNull(rs1.Get_Fields_String("ZipAndZip4")) == false)
				{
					txtUTCSellerZip.Text = FCConvert.ToString(rs1.Get_Fields_String("ZipAndZip4"));
				}
				txtCTANumber.Text = FCConvert.ToString(rs1.Get_Fields_String("CTANumber"));
				if (FCConvert.ToBoolean(rs1.Get_Fields_Boolean("RushTitle")))
				{
					chkRushTitle.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkRushTitle.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtCTAResAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("ResidenceAddress"));
				txtCTAResTown.Text = FCConvert.ToString(rs1.Get_Fields_String("ResidenceCity"));
				txtCTAApplicantState.Text = FCConvert.ToString(rs1.Get_Fields_String("ResidenceState"));
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("ResidenceZip"))) != "")
				{
					txtCTALegalZip.Text = FCConvert.ToString(rs1.Get_Fields_String("ResidenceZip"));
				}
				txtCTATelNumber.Text = FCConvert.ToString(rs1.Get_Fields_String("TelephoneNumber"));
				if (FCConvert.ToString(rs1.Get_Fields_String("NewUsedRebuilt")) == "N")
				{
					cmbVehicleType.Text = "New";
				}
				else if (rs1.Get_Fields_String("NewUsedRebuilt") == "U")
				{
					cmbVehicleType.Text = "Used";
				}
				else if (rs1.Get_Fields_String("NewUsedRebuilt") == "R")
				{
					cmbVehicleType.Text = "N/R";
				}
				else
				{
					cmbVehicleType.Text = "";
				}
				if (FCConvert.ToString(rs1.Get_Fields_String("OdometerMK")) == "M")
				{
					cmbOdometer.Text = "Miles";
				}
				else if (rs1.Get_Fields_String("OdometerMK") == "K")
				{
					cmbOdometer.Text = "Kilometers";
				}
				else
				{
					cmbOdometer.Text = "";
				}
				if (FCConvert.ToString(rs1.Get_Fields_String("OdometerCondition")) == "A")
				{
					cmbOdometerCondition.Text = "Actual";
				}
				else if (rs1.Get_Fields_String("OdometerCondition") == "E")
				{
					cmbOdometerCondition.Text = "Excess";
				}
				else if (rs1.Get_Fields_String("OdometerCondition") == "C")
				{
					cmbOdometerCondition.Text = "Changed";
				}
				else if (rs1.Get_Fields_String("OdometerCondition") == "B")
				{
					cmbOdometerCondition.Text = "Broken";
				}
				else
				{
					cmbOdometerCondition.Text = "";
				}
				txtCTANumberOfCylinders.Text = FCConvert.ToString(rs1.Get_Fields_Int32("NumberOfCylinders"));
				if (Information.IsDate(rs1.Get_Fields("PurchaseDate")))
					txtCATDatePurchased.Text = Strings.Format(rs1.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy");
				txtCTAPreviousNumber.Text = FCConvert.ToString(rs1.Get_Fields_String("PreviousTitle"));
				txtCTAStateOfOrigin.Text = FCConvert.ToString(rs1.Get_Fields_String("StateOfOrigin"));
				txtCTASellerName.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerName"));
				txtCTASellerAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerAddress1"));
				txtCTASellerCity.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerCity"));
				txtCTASellerState.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerState"));
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("SellerZipAndZip4"))) != "")
				{
					txtCTASellerZip.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerZipAndZip4"));
				}
				if (FCConvert.ToString(rs1.Get_Fields_String("DealerType")) == "D")
				{
					cmbDealer.Text = "D";
				}
				else if (rs1.Get_Fields_String("DealerType") == "UC")
				{
					cmbDealer.Text = "UC";
				}
				else if (rs1.Get_Fields_String("DealerType") == "MC")
				{
					cmbDealer.Text = "MC";
				}
				else
				{
					cmbDealer.Text = "";
				}
				if (MotorVehicle.Statics.FleetCTA && MotorVehicle.Statics.VehiclesCovered > 1)
				{
					chkFleet.CheckState = Wisej.Web.CheckState.Checked;
				}
				txtCTADealerPlate.Text = FCConvert.ToString(rs1.Get_Fields_String("DealerPlate"));
				txtCTALienOneName.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1Name"));
				txtCTALienOneAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1Address1"));
				txtCTALienOneCity.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1City"));
				txtCTALienOneState.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1State"));
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("LH1ZipAndZip4"))) != "")
				{
					txtCTALienOneZip.Text = FCConvert.ToString(rs1.Get_Fields_String("LH1ZipAndZip4"));
				}
				if (Information.IsDate(rs1.Get_Fields("LH1Date")))
					txt1stLienDate.Text = Strings.Format(rs1.Get_Fields_DateTime("LH1Date"), "MM/dd/yyyy");
				txtCTALienTwoName.Text = FCConvert.ToString(rs1.Get_Fields_String("LH2Name"));
				txtCTALienTwoAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("LH2Address1"));
				txtCTALienTwoCity.Text = FCConvert.ToString(rs1.Get_Fields_String("LH2City"));
				txtCTALienTwoState.Text = FCConvert.ToString(rs1.Get_Fields_String("LH2State"));
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("lh2zipandzip4"))) != "")
				{
					txtCTALienTwoZip.Text = FCConvert.ToString(rs1.Get_Fields_String("lh2zipandzip4"));
				}
				if (Information.IsDate(rs1.Get_Fields("LH2Date")))
					txt2ndLienDate.Text = Strings.Format(rs1.Get_Fields_DateTime("LH2Date"), "MM/dd/yyyy");
				if (FCConvert.ToInt32(rs1.Get_Fields_String("DoubleNumber")) != 0)
				{
					chkDouble.CheckState = Wisej.Web.CheckState.Checked;
					if (MotorVehicle.Statics.PrintDouble)
					{
						chkPrintDouble.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPrintDouble.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					txtDoubleCTANumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("CTANumber"));
					if (FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields("Type")) == "N")
					{
						cmbDoubleVehicleType.Text = "New";
					}
					else if (MotorVehicle.Statics.rsDoubleTitle.Get_Fields("Type") == "U")
					{
						cmbDoubleVehicleType.Text = "Used";
					}
					else
					{
						cmbDoubleVehicleType.Text = "Rebuilt";
					}
					txtDoubleSellerName.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("SellerName"));
					txtDoubleSellerAddress.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("SellerAddress"));
					txtDoubleCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("SellerCity"));
					txtDoubleState.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("SellerState"));
					txtDoubleZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("SellerZip"));
					if (FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("DealerType")) == "D")
					{
						cmbDoubleDealer.Text = "D";
					}
					else if (MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("DealerType") == "UC")
					{
						cmbDoubleDealer.Text = "UC";
					}
					else
					{
						cmbDoubleDealer.Text = "MC";
					}
					txtDoublePlate.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("plate"));
					txtDoublePurchaseDate.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields("PurchaseDate"));
					txtDoubleCylinders.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields("Cylinders"));
					txtDoublePreviousTitle.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("PreviousTitle"));
					txtDoublePreviousState.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("PreviousState"));
					if (Information.IsDate(MotorVehicle.Statics.rsDoubleTitle.Get_Fields("DOB")))
					{
						txtOriginalDOB.Text = Strings.Format(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_DateTime("DOB"), "MM/dd/yyyy");
					}
					txtCTASellerName.Text = FCConvert.ToString(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("Customer"));
				}
			}
		}

		private void frmSingleLongTermCTA_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmSingleLongTermCTA_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuFileExit_Click();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				if (FCGlobal.Screen.ActiveControl is Global.T2KOverTypeBox || FCGlobal.Screen.ActiveControl is FCTextBox)
				{
					KeyAscii = KeyAscii - 32;
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSingleLongTermCTA_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSingleLongTermCTA properties;
			//frmSingleLongTermCTA.ScaleWidth	= 9300;
			//frmSingleLongTermCTA.ScaleHeight	= 7320;
			//frmSingleLongTermCTA.LinkTopic	= "Form1";
			//End Unmaped Properties
			cmbCTAPrint.Text = "This Form NEEDS to be printed";
			rsResCode.OpenRecordset("SELECT * FROM Residence");
			if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
			{
				rsResCode.MoveLast();
				rsResCode.MoveFirst();
			}
			chkRushTitle.Visible = true;
			blnPrinted = false;
			blnDoubleCompleted = false;
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this, false);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuFileExit_Click()
		{
			mnuFileExit_Click(mnuFileExit, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (fraDoubleInfo.Visible == true)
			{
				cmdSave_Click();
			}
			else
			{
				cmdReturn_Click();
			}
		}

		private void mnuPrintCTA_Click(object sender, System.EventArgs e)
		{
			if (chkDouble.CheckState == Wisej.Web.CheckState.Checked && fraDoubleInfo.Visible == false && !blnDoubleCompleted)
			{
				fraApplicant.Enabled = false;
				fraVehicleInfo.Enabled = false;
				fraLienHolder.Enabled = false;
				Frame2.Enabled = false;
				cmbCTAPrint.Enabled = false;
				txtCTANumber.Enabled = false;
				fraDoubleInfo.Visible = true;
				txtDoubleCTANumber.Focus();
				return;
			}
			else if (fraDoubleInfo.Visible == true)
			{
				return;
			}
			if (SaveTitleToPrint())
			{
				PrintCTA();
			}
		}

		private void txt1stLienDate_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txt1stLienDate.BackColor);
			txt1stLienDate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txt1stLienDate_Leave(object sender, System.EventArgs e)
		{
			txt1stLienDate.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txt2ndLienDate_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txt2ndLienDate.BackColor);
			txt2ndLienDate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txt2ndLienDate_Leave(object sender, System.EventArgs e)
		{
			txt2ndLienDate.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCATDatePurchased_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCATDatePurchased.BackColor);
			txtCATDatePurchased.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCATDatePurchased_Leave(object sender, System.EventArgs e)
		{
			txtCATDatePurchased.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTAApplicantState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTAApplicantState.BackColor);
			txtCTAApplicantState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTAApplicantState_Leave(object sender, System.EventArgs e)
		{
			txtCTAApplicantState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTADealerPlate_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTADealerPlate.BackColor);
			txtCTADealerPlate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTADealerPlate_Leave(object sender, System.EventArgs e)
		{
			txtCTADealerPlate.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTADOB1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTADOB1.BackColor);
			txtCTADOB1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTADOB1_Leave(object sender, System.EventArgs e)
		{
			txtCTADOB1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTADOB2_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTADOB2.BackColor);
			txtCTADOB2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTADOB2_Leave(object sender, System.EventArgs e)
		{
			txtCTADOB2.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALegalZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALegalZip.BackColor);
			txtCTALegalZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALegalZip_Leave(object sender, System.EventArgs e)
		{
			txtCTALegalZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienOneAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienOneAddress.BackColor);
			txtCTALienOneAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienOneAddress_Leave(object sender, System.EventArgs e)
		{
			txtCTALienOneAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienOneCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienOneCity.BackColor);
			txtCTALienOneCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienOneCity_Leave(object sender, System.EventArgs e)
		{
			txtCTALienOneCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienOneName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienOneName.BackColor);
			txtCTALienOneName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienOneName_Leave(object sender, System.EventArgs e)
		{
			txtCTALienOneName.BackColor = ColorTranslator.FromOle(OldBackColor);
			if (fecherFoundation.Strings.Trim(txtCTALienOneName.Text) == "")
			{
				txtCTALienOneName.Text = "NONE";
			}
			if (fecherFoundation.Strings.Trim(txtCTALienOneName.Text) == "NONE")
			{
				txtUTCSellerName1.Focus();
			}
		}

		private void txtCTALienOneState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienOneState.BackColor);
			txtCTALienOneState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienOneState_Leave(object sender, System.EventArgs e)
		{
			txtCTALienOneState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienOneZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienOneZip.BackColor);
			txtCTALienOneZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienOneZip_Leave(object sender, System.EventArgs e)
		{
			txtCTALienOneZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienTwoAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienTwoAddress.BackColor);
			txtCTALienTwoAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienTwoAddress_Leave(object sender, System.EventArgs e)
		{
			txtCTALienTwoAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienTwoCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienTwoCity.BackColor);
			txtCTALienTwoCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienTwoCity_Leave(object sender, System.EventArgs e)
		{
			txtCTALienTwoCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienTwoName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienTwoName.BackColor);
			txtCTALienTwoName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienTwoName_Leave(object sender, System.EventArgs e)
		{
			txtCTALienTwoName.BackColor = ColorTranslator.FromOle(OldBackColor);
			if (fecherFoundation.Strings.Trim(txtCTALienTwoName.Text) == "")
			{
				txtCTALienTwoName.Text = "NONE";
			}
			if (fecherFoundation.Strings.Trim(txtCTALienTwoName.Text) == "NONE")
			{
				txtUTCSellerName1.Focus();
			}
		}

		private void txtCTALienTwoState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienTwoState.BackColor);
			txtCTALienTwoState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienTwoState_Leave(object sender, System.EventArgs e)
		{
			txtCTALienTwoState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTALienTwoZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTALienTwoZip.BackColor);
			txtCTALienTwoZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTALienTwoZip_Leave(object sender, System.EventArgs e)
		{
			txtCTALienTwoZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTANumber_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTANumber.BackColor);
			txtCTANumber.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTANumber_Leave(object sender, System.EventArgs e)
		{
			txtCTANumber.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTANumberOfCylinders_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTANumberOfCylinders.BackColor);
			txtCTANumberOfCylinders.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTANumberOfCylinders_Leave(object sender, System.EventArgs e)
		{
			txtCTANumberOfCylinders.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTAPreviousNumber_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTAPreviousNumber.BackColor);
			txtCTAPreviousNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTAPreviousNumber_Leave(object sender, System.EventArgs e)
		{
			txtCTAPreviousNumber.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTAResAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTAResAddress.BackColor);
			txtCTAResAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTAResAddress_Leave(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtCTAResAddress.Text) != "")
			{
				// rsResCode.FindFirstRecord , , "Code = '" & Trim(txtCTAResAddress.Text) & "'"
				// txtCTAResTown.Text = rsResCode.Fields("Town")
				if (txtCTAResTown.Text != "")
					txtCTAApplicantState.Text = "ME";
			}
			txtCTAResAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTAResTown_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTAResTown.BackColor);
			txtCTAResTown.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTAResTown_Leave(object sender, System.EventArgs e)
		{
			txtCTAResTown.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTASellerAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTASellerAddress.BackColor);
			txtCTASellerAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTASellerAddress_Leave(object sender, System.EventArgs e)
		{
			txtCTASellerAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTASellerCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTASellerCity.BackColor);
			txtCTASellerCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTASellerCity_Leave(object sender, System.EventArgs e)
		{
			txtCTASellerCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTASellerName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTASellerName.BackColor);
			txtCTASellerName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTASellerName_Leave(object sender, System.EventArgs e)
		{
			txtCTASellerName.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTASellerState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTASellerState.BackColor);
			txtCTASellerState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTASellerState_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode != Keys.Left && KeyCode != Keys.Right)
			{
				if (fecherFoundation.Strings.Trim(txtCTASellerState.Text).Length >= 2)
				{
					txtCTASellerZip.Focus();
				}
			}
		}

		private void txtCTASellerState_Leave(object sender, System.EventArgs e)
		{
			txtCTASellerState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTASellerZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTASellerZip.BackColor);
			txtCTASellerZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTASellerZip_Leave(object sender, System.EventArgs e)
		{
			txtCTASellerZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTAStateOfOrigin_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTAStateOfOrigin.BackColor);
			txtCTAStateOfOrigin.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTAStateOfOrigin_Leave(object sender, System.EventArgs e)
		{
			txtCTAStateOfOrigin.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtCTATelNumber_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtCTATelNumber.BackColor);
			txtCTATelNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCTATelNumber_Leave(object sender, System.EventArgs e)
		{
			txtCTATelNumber.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoubleCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoubleCity.BackColor);
			txtDoubleCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoubleCity_Leave(object sender, System.EventArgs e)
		{
			txtDoubleCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoubleCTANumber_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoubleCTANumber.BackColor);
			txtDoubleCTANumber.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoubleCTANumber_Leave(object sender, System.EventArgs e)
		{
			txtDoubleCTANumber.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoubleCylinders_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoubleCylinders.BackColor);
			txtDoubleCylinders.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoubleCylinders_Leave(object sender, System.EventArgs e)
		{
			txtDoubleCylinders.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoublePlate_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoublePlate.BackColor);
			txtDoublePlate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoublePlate_Leave(object sender, System.EventArgs e)
		{
			txtDoublePlate.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoublePreviousState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoublePreviousState.BackColor);
			txtDoublePreviousState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoublePreviousState_Leave(object sender, System.EventArgs e)
		{
			txtDoublePreviousState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoublePreviousTitle_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoublePreviousTitle.BackColor);
			txtDoublePreviousTitle.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoublePreviousTitle_Leave(object sender, System.EventArgs e)
		{
			txtDoublePreviousTitle.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoublePurchaseDate_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoublePurchaseDate.BackColor);
			txtDoublePurchaseDate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoublePurchaseDate_Leave(object sender, System.EventArgs e)
		{
			txtDoublePurchaseDate.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoubleSellerAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoubleSellerAddress.BackColor);
			txtDoubleSellerAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoubleSellerAddress_Leave(object sender, System.EventArgs e)
		{
			txtDoubleSellerAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoubleSellerName_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoubleSellerName.BackColor);
			txtDoubleSellerName.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoubleSellerName_Leave(object sender, System.EventArgs e)
		{
			txtDoubleSellerName.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoubleState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoubleState.BackColor);
			txtDoubleState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoubleState_Leave(object sender, System.EventArgs e)
		{
			txtDoubleState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtDoubleZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtDoubleZip.BackColor);
			txtDoubleZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtDoubleZip_Leave(object sender, System.EventArgs e)
		{
			txtDoubleZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtOriginalDOB_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtOriginalDOB.BackColor);
			txtOriginalDOB.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtOriginalDOB_Leave(object sender, System.EventArgs e)
		{
			txtOriginalDOB.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerAddress_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerAddress.BackColor);
			txtUTCSellerAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerAddress_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerAddress.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerCity_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerCity.BackColor);
			txtUTCSellerCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerCity_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerCity.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerName1_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerName1.BackColor);
			txtUTCSellerName1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerName1_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerName1.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerName2_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerName2.BackColor);
			txtUTCSellerName2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerName2_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerName2.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerState_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerState.BackColor);
			txtUTCSellerState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerState_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerState.BackColor = ColorTranslator.FromOle(OldBackColor);
		}

		private void txtUTCSellerZip_Enter(object sender, System.EventArgs e)
		{
			OldBackColor = ColorTranslator.ToOle(txtUTCSellerZip.BackColor);
			txtUTCSellerZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUTCSellerZip_Leave(object sender, System.EventArgs e)
		{
			txtUTCSellerZip.BackColor = ColorTranslator.FromOle(OldBackColor);
		}
	}
}
