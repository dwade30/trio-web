﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptUnprocessedPlates.
	/// </summary>
	public partial class rptUnprocessedPlates : BaseSectionReport
	{
		public rptUnprocessedPlates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Bad Accounts List";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptUnprocessedPlates InstancePtr
		{
			get
			{
				return (rptUnprocessedPlates)Sys.GetInstance(typeof(rptUnprocessedPlates));
			}
		}

		protected rptUnprocessedPlates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUnprocessedPlates	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public string strBadPlates = "";

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As object	OnWrite(string)
			// vbPorter upgrade warning: Label3 As object	OnWrite(string)
			// vbPorter upgrade warning: Label7 As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSize(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldPlates As object	OnWrite(string)
			fldPlates.Text = strBadPlates;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As object	OnWrite(string)
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptUnprocessedPlates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUnprocessedPlates properties;
			//rptUnprocessedPlates.Caption	= "Bad Accounts List";
			//rptUnprocessedPlates.Icon	= "rptUnprocessedPlates.dsx":0000";
			//rptUnprocessedPlates.Left	= 0;
			//rptUnprocessedPlates.Top	= 0;
			//rptUnprocessedPlates.Width	= 11880;
			//rptUnprocessedPlates.Height	= 8595;
			//rptUnprocessedPlates.StartUpPosition	= 3;
			//rptUnprocessedPlates.SectionData	= "rptUnprocessedPlates.dsx":508A;
			//End Unmaped Properties
		}
	}
}
