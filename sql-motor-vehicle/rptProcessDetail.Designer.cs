﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptProcessDetail.
	/// </summary>
	partial class rptProcessDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptProcessDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblPlate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOwner = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYearMakeModel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExp = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExcise = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYMM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.rtfWarning = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.txtNotFound = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.rtf1 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYearMakeModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYMM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotFound)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPlate,
				this.txtOwner,
				this.txtYMM,
				this.txtExp,
				this.txtExcise,
				this.rtfWarning,
				this.txtNotFound,
				this.txtNotUpdated
			});
			this.Detail.Height = 0.375F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblPlate,
				this.lblOwner,
				this.lblYearMakeModel,
				this.lblExp,
				this.lblExcise,
				this.Line1,
				this.Label1,
				this.Label32,
				this.Label9,
				this.Label10,
				this.Label33,
				this.lblMode
			});
			this.PageHeader.Height = 0.90625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0.05208333F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.rtf1
			});
			this.GroupFooter1.Height = 0.53125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblPlate
			// 
			this.lblPlate.Height = 0.1875F;
			this.lblPlate.HyperLink = null;
			this.lblPlate.Left = 0.0625F;
			this.lblPlate.Name = "lblPlate";
			this.lblPlate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblPlate.Text = "--Plate--";
			this.lblPlate.Top = 0.6875F;
			this.lblPlate.Width = 0.5625F;
			// 
			// lblOwner
			// 
			this.lblOwner.Height = 0.1875F;
			this.lblOwner.HyperLink = null;
			this.lblOwner.Left = 0.96875F;
			this.lblOwner.Name = "lblOwner";
			this.lblOwner.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblOwner.Text = "Owner";
			this.lblOwner.Top = 0.6875F;
			this.lblOwner.Width = 0.5F;
			// 
			// lblYearMakeModel
			// 
			this.lblYearMakeModel.Height = 0.1875F;
			this.lblYearMakeModel.HyperLink = null;
			this.lblYearMakeModel.Left = 3.78125F;
			this.lblYearMakeModel.Name = "lblYearMakeModel";
			this.lblYearMakeModel.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblYearMakeModel.Text = "Year Make Model / Warnings";
			this.lblYearMakeModel.Top = 0.6875F;
			this.lblYearMakeModel.Width = 1.875F;
			// 
			// lblExp
			// 
			this.lblExp.Height = 0.1875F;
			this.lblExp.HyperLink = null;
			this.lblExp.Left = 5.75F;
			this.lblExp.Name = "lblExp";
			this.lblExp.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblExp.Text = "Exp Date";
			this.lblExp.Top = 0.6875F;
			this.lblExp.Width = 0.75F;
			// 
			// lblExcise
			// 
			this.lblExcise.Height = 0.1875F;
			this.lblExcise.HyperLink = null;
			this.lblExcise.Left = 6.59375F;
			this.lblExcise.Name = "lblExcise";
			this.lblExcise.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblExcise.Text = "Excise Tax";
			this.lblExcise.Top = 0.6875F;
			this.lblExcise.Width = 0.8125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.875F;
			this.Line1.Width = 7.375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0.875F;
			this.Line1.Y2 = 0.875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "TRIO Software  -  Rapid Renewal Process";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.46875F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label32.Text = "Label32";
			this.Label32.Top = 0.1875F;
			this.Label32.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.15625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 6.15625F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label33.Text = "Label33";
			this.Label33.Top = 0F;
			this.Label33.Width = 1.3125F;
			// 
			// lblMode
			// 
			this.lblMode.Height = 0.1875F;
			this.lblMode.HyperLink = null;
			this.lblMode.Left = 0F;
			this.lblMode.Name = "lblMode";
			this.lblMode.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblMode.Text = "Label1";
			this.lblMode.Top = 0.375F;
			this.lblMode.Width = 7.46875F;
			// 
			// txtPlate
			// 
			this.txtPlate.Height = 0.1875F;
			this.txtPlate.Left = 0.0625F;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtPlate.Text = null;
			this.txtPlate.Top = 0F;
			this.txtPlate.Width = 0.90625F;
			// 
			// txtOwner
			// 
			this.txtOwner.CanShrink = true;
			this.txtOwner.Height = 0.1875F;
			this.txtOwner.Left = 0.96875F;
			this.txtOwner.Name = "txtOwner";
			this.txtOwner.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtOwner.Text = null;
			this.txtOwner.Top = 0F;
			this.txtOwner.Width = 2.75F;
			// 
			// txtYMM
			// 
			this.txtYMM.CanShrink = true;
			this.txtYMM.Height = 0.1875F;
			this.txtYMM.Left = 3.78125F;
			this.txtYMM.Name = "txtYMM";
			this.txtYMM.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtYMM.Text = null;
			this.txtYMM.Top = 0F;
			this.txtYMM.Width = 1.5625F;
			// 
			// txtExp
			// 
			this.txtExp.Height = 0.1875F;
			this.txtExp.Left = 5.75F;
			this.txtExp.Name = "txtExp";
			this.txtExp.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.txtExp.Text = null;
			this.txtExp.Top = 0F;
			this.txtExp.Width = 0.75F;
			// 
			// txtExcise
			// 
			this.txtExcise.Height = 0.1875F;
			this.txtExcise.Left = 6.5F;
			this.txtExcise.Name = "txtExcise";
			this.txtExcise.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.txtExcise.Text = null;
			this.txtExcise.Top = 0F;
			this.txtExcise.Width = 0.9375F;
			// 
			// rtfWarning
			// 
			this.rtfWarning.CanShrink = true;
			//this.rtfWarning.Font = new System.Drawing.Font("Arial", 10F);
			this.rtfWarning.Height = 0.125F;
			this.rtfWarning.Left = 3.78125F;
			this.rtfWarning.Name = "rtfWarning";
			this.rtfWarning.RTF = resources.GetString("rtfWarning.RTF");
			this.rtfWarning.Top = 0.21875F;
			this.rtfWarning.Visible = false;
			this.rtfWarning.Width = 2.5625F;
			// 
			// txtNotFound
			// 
			this.txtNotFound.Height = 0.1875F;
			this.txtNotFound.Left = 0.0625F;
			this.txtNotFound.Name = "txtNotFound";
			this.txtNotFound.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtNotFound.Text = null;
			this.txtNotFound.Top = 0F;
			this.txtNotFound.Width = 2.59375F;
			// 
			// txtNotUpdated
			// 
			this.txtNotUpdated.Height = 0.1875F;
			this.txtNotUpdated.Left = 0.0625F;
			this.txtNotUpdated.Name = "txtNotUpdated";
			this.txtNotUpdated.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtNotUpdated.Text = null;
			this.txtNotUpdated.Top = 0.21875F;
			this.txtNotUpdated.Visible = false;
			this.txtNotUpdated.Width = 2.59375F;
			// 
			// rtf1
			// 
			this.rtf1.CanShrink = true;
			//this.rtf1.Font = new System.Drawing.Font("Arial", 10F);
			this.rtf1.Height = 0.375F;
			this.rtf1.Left = 1.46875F;
			this.rtf1.Name = "rtf1";
			this.rtf1.RTF = resources.GetString("rtf1.RTF");
			this.rtf1.Top = 0.15625F;
			this.rtf1.Width = 4.5625F;
			// 
			// rptProcessDetail
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYearMakeModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYMM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotFound)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYMM;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcise;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtfWarning;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotFound;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPlate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYearMakeModel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExp;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExcise;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMode;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtf1;
	}
}
