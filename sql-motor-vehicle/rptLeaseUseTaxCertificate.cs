//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLeaseUseTaxCertificate.
	/// </summary>
	public partial class rptLeaseUseTaxCertificate : BaseSectionReport
	{
		public rptLeaseUseTaxCertificate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Use Tax Certificate";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLeaseUseTaxCertificate InstancePtr
		{
			get
			{
				return (rptLeaseUseTaxCertificate)Sys.GetInstance(typeof(rptLeaseUseTaxCertificate));
			}
		}

		protected rptLeaseUseTaxCertificate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLeaseUseTaxCertificate	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Last Updated:              01/28/2002
		private bool boolIsPrintTest;

		public void PrintTest()
		{
			boolIsPrintTest = true;
			this.PrintReport(false);
		}

		private void GetInformation()
		{
			// this sub will get all of the information needed and fill the fields on the Form
			double dblTaxAmount = 0;
			double dblPurchasePrice = 0;
			double dblAllowance = 0;
			double dblPaymentAmount = 0;
			int lngNumberOfPayments = 0;
			double dblDownPayment = 0;
			clsDRWrapper rsUseTax = new clsDRWrapper();
			int TB;
			string SellerAddress = "";
			string ExmptCode = "";
			string Owner = "";
			string LienHolder1 = "";
			int x;
			if (!boolIsPrintTest)
			{
				// 
				rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID), "TWMV0000.vb1");
				if (rsUseTax.EndOfFile() != true && rsUseTax.BeginningOfFile() != true)
				{
					rsUseTax.MoveLast();
					rsUseTax.MoveFirst();
				}
				else
				{
					MotorVehicle.Statics.blnPrintUseTax = false;
					this.Cancel();
					return;
				}
				fldMake1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
				fldModel1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
				fldYear1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
				fldVin1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
				fldSellerName.Text = rsUseTax.Get_Fields_String("SellerName");
				if (Information.IsDate(rsUseTax.Get_Fields("TradedDate")))
				{
					fldDateofTransfer.Text = Strings.Format(rsUseTax.Get_Fields_DateTime("TradedDate"), "MM/dd/yyyy");
				}
				else
				{
					fldDateofTransfer.Text = "";
				}
				SellerAddress = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerAddress1"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerCity"))) + " " + rsUseTax.Get_Fields_String("SellerState") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerZipAndZip4")));
				fldSellerAddress.Text = fecherFoundation.Strings.Trim(SellerAddress);
				dblPaymentAmount = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Double("LeasePaymentAmount")) == false)
					dblPaymentAmount = rsUseTax.Get_Fields_Double("LeasePaymentAmount");
				fldLeasePayment.Text = Strings.Format(dblPaymentAmount, "#,###");
				lngNumberOfPayments = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Int32("NumberOfPayments")) == false)
					lngNumberOfPayments = FCConvert.ToInt32(rsUseTax.Get_Fields_Int32("NumberOfPayments"));
				fldNumberOfPayments.Text = Strings.Format(lngNumberOfPayments, "#,###");
				fldPaymentTotal.Text = Strings.Format(dblPaymentAmount * lngNumberOfPayments, "#,###");
				dblDownPayment = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Double("LeaseDownPayment")) == false)
					dblDownPayment = rsUseTax.Get_Fields_Double("LeaseDownPayment");
				fldDownPayment.Text = Strings.Format(dblDownPayment, "#,###");
				dblPurchasePrice = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("PurchasePrice")) == false)
					dblPurchasePrice = Conversion.Val(rsUseTax.Get_Fields_Decimal("PurchasePrice"));
				dblAllowance = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("Allowance")) == false)
					dblAllowance = Conversion.Val(rsUseTax.Get_Fields_Decimal("Allowance"));
				fldAllowance.Text = Strings.Format(dblAllowance, "#,###");
				fldTotalAmount.Text = Strings.Format(dblPurchasePrice + dblAllowance, "#,###");
				dblTaxAmount = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("TaxAmount")) == false)
					dblTaxAmount = Conversion.Val(rsUseTax.Get_Fields_Decimal("TaxAmount"));
				fldUseTax.Text = Strings.Format(dblTaxAmount, "#,##0.00");
				ExmptCode = FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptCode"));
				if (ExmptCode == "A")
				{
					fldExemptTypeA.Text = "A";
					fldExemptNumber.Text = rsUseTax.Get_Fields_String("ExemptNumber");
				}
				else if (ExmptCode == "B")
				{
					fldExemptTypeB.Text = "B";
				}
				else if (ExmptCode == "C")
				{
					fldExemptTypeC.Text = "C";
				}
				else if (ExmptCode == "D")
				{
					fldExemptTypeD.Text = "D";
					fldOtherReason.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptDescription")));
				}
				fldPurchaserName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserFName"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserLName")));
				// tromvs-98 07/2018
				fldPurchaserSocialSec.Text = rsUseTax.Get_Fields_String("PurchaserSSN");
				fldPurchaserAddress.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserAddress")));
				fldPurchaserCity.Text = rsUseTax.Get_Fields_String("PurchaserCity");
				fldPurchaserState.Text = rsUseTax.Get_Fields_String("PurchaserState");
				fldPurchaserZip.Text = rsUseTax.Get_Fields_String("PurchaserZipAndZip4");
				if (MotorVehicle.Statics.FleetUseTax)
				{
					fldClassPlate.Text = "S.A.L.";
				}
				else
				{
					fldClassPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");
				}
				fldRegDate.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
				fldTaxAmount.Text = Strings.Format(Strings.Format(dblTaxAmount, "###,###.00"), "@@@@@@@@@@");
			}
			else
			{
				fldMake1.Text = "MAKE";
				fldModel1.Text = "MODEL";
				fldYear1.Text = DateTime.Today.Year.ToString();
				fldVin1.Text = "123456789";
				fldSellerName.Text = "Seller Name";
				fldDateofTransfer.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				SellerAddress = "Address 1" + " " + "Seller City" + " " + "ST" + " " + "00000";
				fldSellerAddress.Text = fecherFoundation.Strings.Trim(SellerAddress);
				dblPaymentAmount = 500;
				fldLeasePayment.Text = Strings.Format(dblPaymentAmount, "#,###");
				lngNumberOfPayments = 36;
				fldNumberOfPayments.Text = Strings.Format(lngNumberOfPayments, "#,###");
				fldPaymentTotal.Text = Strings.Format(dblPaymentAmount * lngNumberOfPayments, "#,###");
				dblDownPayment = 1000;
				fldDownPayment.Text = Strings.Format(dblDownPayment, "#,###");
				dblPurchasePrice = 32000;
				dblAllowance = 350;
				fldAllowance.Text = Strings.Format(dblAllowance, "#,###");
				fldTotalAmount.Text = Strings.Format(dblPurchasePrice + dblAllowance, "#,###");
				dblTaxAmount = 1100;
				fldUseTax.Text = Strings.Format(dblTaxAmount, "#,##0.00");
				fldExemptTypeA.Text = "A";
				fldExemptNumber.Text = "12345";
				fldExemptTypeB.Text = "B";
				fldExemptTypeC.Text = "C";
				fldExemptTypeD.Text = "D";
				fldOtherReason.Text = "Other reason text";
				fldPurchaserName.Text = "Purchaser Name";
				fldPurchaserSocialSec.Text = "000990000";
				fldPurchaserAddress.Text = "Purchaser Address";
				fldPurchaserCity.Text = "Purchaser City";
				fldPurchaserState.Text = "ST";
				fldPurchaserZip.Text = "12345";
				fldClassPlate.Text = "PC 123456";
				fldRegDate.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
				fldTaxAmount.Text = Strings.Format(Strings.Format(dblTaxAmount, "###,###.00"), "@@@@@@@@@@");
			}
			rsUseTax.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			this.Document.Printer.PrinterName = MotorVehicle.Statics.gstrCTAPrinterName;
			/*? For Each */
			foreach (FCPrinter p in FCGlobal.Printers)
			{
				if (p.DeviceName == MotorVehicle.Statics.gstrCTAPrinterName)
				{
					if (tempPrinter.DriverName == "EPSON24")
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 10cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
						}
					}
					break;
				}
			}
			/*? Next */
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			GetInformation();
			MotorVehicle.Statics.blnPrintUseTax = true;
		}

		private void rptLeaseUseTaxCertificate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLeaseUseTaxCertificate properties;
			//rptLeaseUseTaxCertificate.Caption	= "Use Tax Certificate";
			//rptLeaseUseTaxCertificate.Icon	= "rptLeaseUseTaxCertificate.dsx":0000";
			//rptLeaseUseTaxCertificate.Left	= 0;
			//rptLeaseUseTaxCertificate.Top	= 0;
			//rptLeaseUseTaxCertificate.Width	= 20280;
			//rptLeaseUseTaxCertificate.Height	= 11115;
			//rptLeaseUseTaxCertificate.StartUpPosition	= 3;
			//rptLeaseUseTaxCertificate.SectionData	= "rptLeaseUseTaxCertificate.dsx":058A;
			//End Unmaped Properties
		}
	}
}
