﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptBMV.
	/// </summary>
	public partial class rptBMV : BaseSectionReport
	{
		public rptBMV()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "BMV Update Disk Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBMV InstancePtr
		{
			get
			{
				return (rptBMV)Sys.GetInstance(typeof(rptBMV));
			}
		}

		protected rptBMV _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBMV	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strType = "";
		int counter;
		int TotalRows;
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				counter += 1;
				if (counter >= TotalRows)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			bool blnNoRecords;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnNoRecords = false;
			blnFirstRecord = true;
			counter = 1;
			if (frmPrePrints.InstancePtr.tabVehicleInfo.SelectedIndex == 0)
			{
				strType = "A";
				lblVehicles.Text = "Vehicles Added";
				if (frmPrePrints.InstancePtr.vsAdded.Rows <= 1)
				{
					blnNoRecords = true;
				}
				else
				{
					TotalRows = frmPrePrints.InstancePtr.vsAdded.Rows;
				}
			}
			else if (frmPrePrints.InstancePtr.tabVehicleInfo.SelectedIndex == 1)
			{
				strType = "E";
				lblVehicles.Text = "Vehicles Edited";
				if (frmPrePrints.InstancePtr.vsEdited.Rows <= 1)
				{
					blnNoRecords = true;
				}
				else
				{
					TotalRows = frmPrePrints.InstancePtr.vsEdited.Rows;
				}
			}
			else
			{
				strType = "N";
				lblVehicles.Text = "Vehicles Not Updated";
				if (frmPrePrints.InstancePtr.vsNotUpdated.Rows <= 1)
				{
					blnNoRecords = true;
				}
				else
				{
					TotalRows = frmPrePrints.InstancePtr.vsNotUpdated.Rows;
				}
			}
			if (blnNoRecords)
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrHandler
				fecherFoundation.Information.Err().Clear();
				//Application.DoEvents();
				if (strType == "A")
				{
					txtPlate.Text = frmPrePrints.InstancePtr.vsAdded.TextMatrix(counter, 0);
					txtOwner.Text = frmPrePrints.InstancePtr.vsAdded.TextMatrix(counter, 1);
					txtYear.Text = frmPrePrints.InstancePtr.vsAdded.TextMatrix(counter, 2);
					txtMake.Text = frmPrePrints.InstancePtr.vsAdded.TextMatrix(counter, 3);
					txtModel.Text = frmPrePrints.InstancePtr.vsAdded.TextMatrix(counter, 4);
				}
				else if (strType == "E")
				{
					txtPlate.Text = frmPrePrints.InstancePtr.vsEdited.TextMatrix(counter, 0);
					txtOwner.Text = frmPrePrints.InstancePtr.vsEdited.TextMatrix(counter, 1);
					txtYear.Text = frmPrePrints.InstancePtr.vsEdited.TextMatrix(counter, 2);
					txtMake.Text = frmPrePrints.InstancePtr.vsEdited.TextMatrix(counter, 3);
					txtModel.Text = frmPrePrints.InstancePtr.vsEdited.TextMatrix(counter, 4);
				}
				else
				{
					txtPlate.Text = frmPrePrints.InstancePtr.vsNotUpdated.TextMatrix(counter, 0);
					txtOwner.Text = frmPrePrints.InstancePtr.vsNotUpdated.TextMatrix(counter, 1);
					txtYear.Text = frmPrePrints.InstancePtr.vsNotUpdated.TextMatrix(counter, 2);
					txtMake.Text = frmPrePrints.InstancePtr.vsNotUpdated.TextMatrix(counter, 3);
					txtModel.Text = frmPrePrints.InstancePtr.vsNotUpdated.TextMatrix(counter, 4);
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				this.Close();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			lblVehicleCount.Text = "Vehicle Count: " + FCConvert.ToString(TotalRows - 1);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptBMV_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBMV properties;
			//rptBMV.Caption	= "BMV Update Disk Report";
			//rptBMV.Icon	= "rptBMV.dsx":0000";
			//rptBMV.Left	= 0;
			//rptBMV.Top	= 0;
			//rptBMV.Width	= 11880;
			//rptBMV.Height	= 8595;
			//rptBMV.StartUpPosition	= 3;
			//rptBMV.SectionData	= "rptBMV.dsx":058A;
			//End Unmaped Properties
		}
	}
}
