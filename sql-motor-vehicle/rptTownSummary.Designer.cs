﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTownSummary.
	/// </summary>
	partial class rptTownSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTownSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCategory = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDollars = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBcategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBDollars = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDollars)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBcategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBDollars)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCategory,
				this.txtUnits,
				this.txtDollars,
				this.txtBcategory,
				this.txtBUnits,
				this.txtBDollars,
				this.Field1,
				this.Label20
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			//
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label12,
				this.Label14,
				this.lblCategory,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label1,
				this.lblPage,
				this.Field2,
				this.Field3,
				this.Label33
			});
			this.GroupHeader2.Height = 1.947917F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Height = 0F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label34,
				this.Label35,
				this.Label36
			});
			this.GroupFooter1.Height = 1.9375F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.4375F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.8125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label12.Text = "---- TOWN SUMMARY ----";
			this.Label12.Top = 1.5F;
			this.Label12.Width = 2.25F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label14.Text = "---- FOR BMV USE ONLY ----";
			this.Label14.Top = 1.5F;
			this.Label14.Width = 2.3125F;
			// 
			// lblCategory
			// 
			this.lblCategory.Height = 0.1875F;
			this.lblCategory.HyperLink = null;
			this.lblCategory.Left = 0.25F;
			this.lblCategory.Name = "lblCategory";
			this.lblCategory.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblCategory.Text = "Category";
			this.lblCategory.Top = 1.75F;
			this.lblCategory.Width = 0.8125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label15.Text = "Units";
			this.Label15.Top = 1.75F;
			this.Label15.Width = 0.5F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.6875F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label16.Text = "Dollars";
			this.Label16.Top = 1.75F;
			this.Label16.Width = 0.75F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.75F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label17.Text = "Category";
			this.Label17.Top = 1.75F;
			this.Label17.Width = 0.8125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.5625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label18.Text = "Units";
			this.Label18.Top = 1.75F;
			this.Label18.Width = 0.5F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 6.125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label19.Text = "Dollars";
			this.Label19.Top = 1.75F;
			this.Label19.Width = 0.8125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label1.Text = "**** TOWN SUMMARY ****";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.3125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 3.5F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field2.Text = "|";
			this.Field2.Top = 1.75F;
			this.Field2.Width = 0.125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 3.5F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field3.Text = "|";
			this.Field3.Top = 1.5625F;
			this.Field3.Width = 0.125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 1.9375F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// txtCategory
			// 
			this.txtCategory.Height = 0.125F;
			this.txtCategory.Left = 0.125F;
			this.txtCategory.Name = "txtCategory";
			this.txtCategory.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtCategory.Text = null;
			this.txtCategory.Top = 0.0625F;
			this.txtCategory.Width = 1.9375F;
			// 
			// txtUnits
			// 
			this.txtUnits.Height = 0.125F;
			this.txtUnits.Left = 2.125F;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.txtUnits.Text = null;
			this.txtUnits.Top = 0.0625F;
			this.txtUnits.Width = 0.4375F;
			// 
			// txtDollars
			// 
			this.txtDollars.Height = 0.125F;
			this.txtDollars.Left = 2.5625F;
			this.txtDollars.Name = "txtDollars";
			this.txtDollars.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.txtDollars.Text = null;
			this.txtDollars.Top = 0.0625F;
			this.txtDollars.Width = 0.875F;
			// 
			// txtBcategory
			// 
			this.txtBcategory.Height = 0.125F;
			this.txtBcategory.Left = 3.6875F;
			this.txtBcategory.Name = "txtBcategory";
			this.txtBcategory.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtBcategory.Text = null;
			this.txtBcategory.Top = 0.0625F;
			this.txtBcategory.Width = 1.9375F;
			// 
			// txtBUnits
			// 
			this.txtBUnits.Height = 0.125F;
			this.txtBUnits.Left = 5.6875F;
			this.txtBUnits.Name = "txtBUnits";
			this.txtBUnits.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtBUnits.Text = null;
			this.txtBUnits.Top = 0.0625F;
			this.txtBUnits.Width = 0.375F;
			// 
			// txtBDollars
			// 
			this.txtBDollars.Height = 0.125F;
			this.txtBDollars.Left = 6.125F;
			this.txtBDollars.Name = "txtBDollars";
			this.txtBDollars.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtBDollars.Text = null;
			this.txtBDollars.Top = 0.0625F;
			this.txtBDollars.Width = 0.375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.125F;
			this.Field1.Left = 3.5F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field1.Text = "|";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 0.125F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.15F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.09375F;
			this.Label20.MultiLine = false;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label20.Text = "ˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉ" + "ˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉˉ" + "";
			this.Label20.Top = 0F;
			this.Label20.Width = 7.125F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.1875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label21.Text = "ATTN: ACCOUNTING UNIT";
			this.Label21.Top = 1F;
			this.Label21.Width = 2.4375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.1875F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label22.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label22.Top = 1.1875F;
			this.Label22.Width = 2.4375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.1875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label23.Text = "101 HOSPITAL STREET";
			this.Label23.Top = 1.375F;
			this.Label23.Width = 2.4375F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.1875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label24.Text = "29 STATE HOUSE STATION";
			this.Label24.Top = 1.5625F;
			this.Label24.Width = 2.4375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.1875F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label25.Text = "AUGUSTA, ME 04333-0029";
			this.Label25.Top = 1.75F;
			this.Label25.Width = 2.4375F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.1875F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label26.Text = "Report completed by _____________________________________________________________" + "__";
			this.Label26.Top = 0.0625F;
			this.Label26.Width = 6.75F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 3.125F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label27.Text = "(   ) Bank Deposit";
			this.Label27.Top = 1F;
			this.Label27.Width = 2.1875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 3.125F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label28.Text = "(   ) Cash";
			this.Label28.Top = 1.1875F;
			this.Label28.Width = 2.1875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 3.125F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label29.Text = "(   ) Checks/Money Order";
			this.Label29.Top = 1.375F;
			this.Label29.Width = 2.1875F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.4375F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label30.Text = "PROCESSED BY BMV";
			this.Label30.Top = 1F;
			this.Label30.Width = 1.8125F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 5.4375F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label31.Text = "INITIALS _________";
			this.Label31.Top = 1.1875F;
			this.Label31.Width = 1.8125F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 5.4375F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label32.Text = "DATE __/__/__";
			this.Label32.Top = 1.375F;
			this.Label32.Width = 1.8125F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.1875F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label34.Text = "Mail one copy of all reports, data upload";
			this.Label34.Top = 0.4375F;
			this.Label34.Width = 2.5625F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.1875F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label35.Text = "verification, and remittance to:";
			this.Label35.Top = 0.625F;
			this.Label35.Width = 2.5625F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 3.125F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label36.Text = "TOTAL AMOUNT REMITTED TO BMV ____________";
			this.Label36.Top = 0.625F;
			this.Label36.Width = 3.625F;
			// 
			// rptTownSummary
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDollars)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBcategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBDollars)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDollars;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBcategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBDollars;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCategory;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
	}
}
