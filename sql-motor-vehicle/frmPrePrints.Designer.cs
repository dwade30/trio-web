//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPrePrints.
	/// </summary>
	partial class frmPrePrints
	{
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdChangeDrive;
		public fecherFoundation.FCFrame fraProcessing;
		public fecherFoundation.FCProgressBar prgProcessing;
		public fecherFoundation.FCLabel lblCounter;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCTabControl tabVehicleInfo;
		public fecherFoundation.FCTabPage tabVehicleInfo_Page1;
		public FCGrid vsAdded;
		public fecherFoundation.FCTabPage tabVehicleInfo_Page2;
		public FCGrid vsEdited;
		public fecherFoundation.FCTabPage tabVehicleInfo_Page3;
		public FCGrid vsNotUpdated;
		public fecherFoundation.FCLabel lblDrive;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdChangeDrive = new fecherFoundation.FCButton();
            this.fraProcessing = new fecherFoundation.FCFrame();
            this.prgProcessing = new fecherFoundation.FCProgressBar();
            this.lblCounter = new fecherFoundation.FCLabel();
            this.dlg1 = new fecherFoundation.FCCommonDialog();
            this.tabVehicleInfo = new fecherFoundation.FCTabControl();
            this.tabVehicleInfo_Page1 = new fecherFoundation.FCTabPage();
            this.vsAdded = new fecherFoundation.FCGrid();
            this.tabVehicleInfo_Page2 = new fecherFoundation.FCTabPage();
            this.vsEdited = new fecherFoundation.FCGrid();
            this.tabVehicleInfo_Page3 = new fecherFoundation.FCTabPage();
            this.vsNotUpdated = new fecherFoundation.FCGrid();
            this.lblDrive = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeDrive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraProcessing)).BeginInit();
            this.fraProcessing.SuspendLayout();
            this.tabVehicleInfo.SuspendLayout();
            this.tabVehicleInfo_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAdded)).BeginInit();
            this.tabVehicleInfo_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsEdited)).BeginInit();
            this.tabVehicleInfo_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsNotUpdated)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(914, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdChangeDrive);
            this.ClientArea.Controls.Add(this.fraProcessing);
            this.ClientArea.Controls.Add(this.tabVehicleInfo);
            this.ClientArea.Controls.Add(this.lblDrive);
            this.ClientArea.Size = new System.Drawing.Size(914, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(914, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(294, 30);
            this.HeaderText.Text = "Process BMV Update File";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Enabled = false;
            this.cmdProcess.Location = new System.Drawing.Point(267, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(100, 48);
            this.cmdProcess.TabIndex = 9;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "actionButton";
            this.cmdPrint.Location = new System.Drawing.Point(30, 564);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(168, 40);
            this.cmdPrint.TabIndex = 8;
            this.cmdPrint.Text = "Print Above List";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdChangeDrive
            // 
            this.cmdChangeDrive.AppearanceKey = "actionButton";
            this.cmdChangeDrive.Location = new System.Drawing.Point(30, 30);
            this.cmdChangeDrive.Name = "cmdChangeDrive";
            this.cmdChangeDrive.Size = new System.Drawing.Size(181, 40);
            this.cmdChangeDrive.TabIndex = 7;
            this.cmdChangeDrive.Text = "Select file";
            this.cmdChangeDrive.Click += new System.EventHandler(this.cmdChangeDrive_Click);
            // 
            // fraProcessing
            // 
            this.fraProcessing.Controls.Add(this.prgProcessing);
            this.fraProcessing.Controls.Add(this.lblCounter);
            this.fraProcessing.Location = new System.Drawing.Point(152, 229);
            this.fraProcessing.Name = "fraProcessing";
            this.fraProcessing.Size = new System.Drawing.Size(300, 102);
            this.fraProcessing.TabIndex = 0;
            this.fraProcessing.Text = "Processing File";
            this.fraProcessing.Visible = false;
            // 
            // prgProcessing
            // 
            this.prgProcessing.Location = new System.Drawing.Point(20, 66);
            this.prgProcessing.Name = "prgProcessing";
            this.prgProcessing.Size = new System.Drawing.Size(260, 16);
            this.prgProcessing.TabIndex = 1;
            // 
            // lblCounter
            // 
            this.lblCounter.Location = new System.Drawing.Point(20, 30);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(260, 16);
            this.lblCounter.TabIndex = 2;
            // 
            // dlg1
            // 
            this.dlg1.Color = System.Drawing.Color.Black;
            this.dlg1.DefaultExt = null;
            this.dlg1.FilterIndex = ((short)(0));
            this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dlg1.FontName = "Microsoft Sans Serif";
            this.dlg1.FontSize = 8.25F;
            this.dlg1.ForeColor = System.Drawing.Color.Black;
            this.dlg1.FromPage = 0;
            this.dlg1.Location = new System.Drawing.Point(0, 0);
            this.dlg1.Name = "dlg1";
            this.dlg1.PrinterSettings = null;
            this.dlg1.Size = new System.Drawing.Size(0, 0);
            this.dlg1.TabIndex = 0;
            this.dlg1.ToPage = 0;
            // 
            // tabVehicleInfo
            // 
            this.tabVehicleInfo.Controls.Add(this.tabVehicleInfo_Page1);
            this.tabVehicleInfo.Controls.Add(this.tabVehicleInfo_Page2);
            this.tabVehicleInfo.Controls.Add(this.tabVehicleInfo_Page3);
            this.tabVehicleInfo.Location = new System.Drawing.Point(30, 93);
            this.tabVehicleInfo.Name = "tabVehicleInfo";
            this.tabVehicleInfo.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.tabVehicleInfo.ShowFocusRect = false;
            this.tabVehicleInfo.Size = new System.Drawing.Size(1000, 453);
            this.tabVehicleInfo.TabIndex = 3;
            this.tabVehicleInfo.TabsPerRow = 0;
            this.tabVehicleInfo.Text = "Added";
            this.tabVehicleInfo.WordWrap = false;
            // 
            // tabVehicleInfo_Page1
            // 
            this.tabVehicleInfo_Page1.Controls.Add(this.vsAdded);
            this.tabVehicleInfo_Page1.Location = new System.Drawing.Point(1, 47);
            this.tabVehicleInfo_Page1.Name = "tabVehicleInfo_Page1";
            this.tabVehicleInfo_Page1.Text = "Added";
            // 
            // vsAdded
            // 
            this.vsAdded.AllowSelection = false;
            this.vsAdded.AllowUserToResizeColumns = false;
            this.vsAdded.AllowUserToResizeRows = false;
            this.vsAdded.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsAdded.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsAdded.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsAdded.BackColorBkg = System.Drawing.Color.Empty;
            this.vsAdded.BackColorFixed = System.Drawing.Color.Empty;
            this.vsAdded.BackColorSel = System.Drawing.Color.Empty;
            this.vsAdded.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsAdded.Cols = 5;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsAdded.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsAdded.ColumnHeadersHeight = 30;
            this.vsAdded.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsAdded.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsAdded.DragIcon = null;
            this.vsAdded.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsAdded.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsAdded.ExtendLastCol = true;
            this.vsAdded.FixedCols = 0;
            this.vsAdded.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsAdded.FrozenCols = 0;
            this.vsAdded.GridColor = System.Drawing.Color.Empty;
            this.vsAdded.GridColorFixed = System.Drawing.Color.Empty;
            this.vsAdded.Location = new System.Drawing.Point(20, 20);
            this.vsAdded.Name = "vsAdded";
            this.vsAdded.OutlineCol = 0;
            this.vsAdded.ReadOnly = true;
            this.vsAdded.RowHeadersVisible = false;
            this.vsAdded.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsAdded.RowHeightMin = 0;
            this.vsAdded.Rows = 1;
            this.vsAdded.ScrollTipText = null;
            this.vsAdded.ShowColumnVisibilityMenu = false;
            this.vsAdded.ShowFocusCell = false;
            this.vsAdded.Size = new System.Drawing.Size(961, 367);
            this.vsAdded.StandardTab = true;
            this.vsAdded.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsAdded.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsAdded.TabIndex = 4;
            // 
            // tabVehicleInfo_Page2
            // 
            this.tabVehicleInfo_Page2.Controls.Add(this.vsEdited);
            this.tabVehicleInfo_Page2.Location = new System.Drawing.Point(1, 47);
            this.tabVehicleInfo_Page2.Name = "tabVehicleInfo_Page2";
            this.tabVehicleInfo_Page2.Text = "Edited";
            // 
            // vsEdited
            // 
            this.vsEdited.AllowSelection = false;
            this.vsEdited.AllowUserToResizeColumns = false;
            this.vsEdited.AllowUserToResizeRows = false;
            this.vsEdited.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsEdited.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsEdited.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsEdited.BackColorBkg = System.Drawing.Color.Empty;
            this.vsEdited.BackColorFixed = System.Drawing.Color.Empty;
            this.vsEdited.BackColorSel = System.Drawing.Color.Empty;
            this.vsEdited.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsEdited.Cols = 5;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsEdited.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsEdited.ColumnHeadersHeight = 30;
            this.vsEdited.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsEdited.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsEdited.DragIcon = null;
            this.vsEdited.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsEdited.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsEdited.ExtendLastCol = true;
            this.vsEdited.FixedCols = 0;
            this.vsEdited.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsEdited.FrozenCols = 0;
            this.vsEdited.GridColor = System.Drawing.Color.Empty;
            this.vsEdited.GridColorFixed = System.Drawing.Color.Empty;
            this.vsEdited.Location = new System.Drawing.Point(20, 20);
            this.vsEdited.Name = "vsEdited";
            this.vsEdited.OutlineCol = 0;
            this.vsEdited.ReadOnly = true;
            this.vsEdited.RowHeadersVisible = false;
            this.vsEdited.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsEdited.RowHeightMin = 0;
            this.vsEdited.Rows = 1;
            this.vsEdited.ScrollTipText = null;
            this.vsEdited.ShowColumnVisibilityMenu = false;
            this.vsEdited.ShowFocusCell = false;
            this.vsEdited.Size = new System.Drawing.Size(961, 367);
            this.vsEdited.StandardTab = true;
            this.vsEdited.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsEdited.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsEdited.TabIndex = 5;
            // 
            // tabVehicleInfo_Page3
            // 
            this.tabVehicleInfo_Page3.Controls.Add(this.vsNotUpdated);
            this.tabVehicleInfo_Page3.Location = new System.Drawing.Point(1, 47);
            this.tabVehicleInfo_Page3.Name = "tabVehicleInfo_Page3";
            this.tabVehicleInfo_Page3.Text = "Not Updated";
            // 
            // vsNotUpdated
            // 
            this.vsNotUpdated.AllowSelection = false;
            this.vsNotUpdated.AllowUserToResizeColumns = false;
            this.vsNotUpdated.AllowUserToResizeRows = false;
            this.vsNotUpdated.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsNotUpdated.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsNotUpdated.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsNotUpdated.BackColorBkg = System.Drawing.Color.Empty;
            this.vsNotUpdated.BackColorFixed = System.Drawing.Color.Empty;
            this.vsNotUpdated.BackColorSel = System.Drawing.Color.Empty;
            this.vsNotUpdated.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsNotUpdated.Cols = 5;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsNotUpdated.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.vsNotUpdated.ColumnHeadersHeight = 30;
            this.vsNotUpdated.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsNotUpdated.DefaultCellStyle = dataGridViewCellStyle6;
            this.vsNotUpdated.DragIcon = null;
            this.vsNotUpdated.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsNotUpdated.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsNotUpdated.ExtendLastCol = true;
            this.vsNotUpdated.FixedCols = 0;
            this.vsNotUpdated.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsNotUpdated.FrozenCols = 0;
            this.vsNotUpdated.GridColor = System.Drawing.Color.Empty;
            this.vsNotUpdated.GridColorFixed = System.Drawing.Color.Empty;
            this.vsNotUpdated.Location = new System.Drawing.Point(20, 20);
            this.vsNotUpdated.Name = "vsNotUpdated";
            this.vsNotUpdated.OutlineCol = 0;
            this.vsNotUpdated.ReadOnly = true;
            this.vsNotUpdated.RowHeadersVisible = false;
            this.vsNotUpdated.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsNotUpdated.RowHeightMin = 0;
            this.vsNotUpdated.Rows = 1;
            this.vsNotUpdated.ScrollTipText = null;
            this.vsNotUpdated.ShowColumnVisibilityMenu = false;
            this.vsNotUpdated.ShowFocusCell = false;
            this.vsNotUpdated.Size = new System.Drawing.Size(961, 367);
            this.vsNotUpdated.StandardTab = true;
            this.vsNotUpdated.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsNotUpdated.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsNotUpdated.TabIndex = 6;
            // 
            // lblDrive
            // 
            this.lblDrive.Location = new System.Drawing.Point(241, 44);
            this.lblDrive.Name = "lblDrive";
            this.lblDrive.Size = new System.Drawing.Size(503, 30);
            this.lblDrive.TabIndex = 10;
            this.lblDrive.Text = "SELECTED FILE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 0;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // frmPrePrints
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPrePrints";
            this.Text = " Process BMV Update File";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPrePrints_Load);
            this.Activated += new System.EventHandler(this.frmPrePrints_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrePrints_KeyPress);
            this.Resize += new System.EventHandler(this.frmPrePrints_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeDrive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraProcessing)).EndInit();
            this.fraProcessing.ResumeLayout(false);
            this.tabVehicleInfo.ResumeLayout(false);
            this.tabVehicleInfo_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsAdded)).EndInit();
            this.tabVehicleInfo_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsEdited)).EndInit();
            this.tabVehicleInfo_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsNotUpdated)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}