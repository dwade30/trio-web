﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptUseTaxSummary.
	/// </summary>
	partial class rptUseTaxSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUseTaxSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCustomer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOPID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaidUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaidAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUnits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDollars = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.linTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblPaid = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNoFee = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldNoFeeUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoFeeAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNoInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaidUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaidAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDollars)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoFeeUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoFeeAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtType,
				this.txtFee,
				this.txtClass,
				this.txtPlate,
				this.txtDT,
				this.txtCustomer,
				this.txtOPID
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNoInfo
			});
			this.ReportFooter.Height = 0.625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label1,
				this.lblPage,
				this.Line1,
				this.Label18,
				this.Label33
			});
			this.GroupHeader1.Height = 1.822917F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPaidUnits,
				this.fldPaidAmount,
				this.lblTotals,
				this.lblType,
				this.lblUnits,
				this.lblDollars,
				this.linTotals,
				this.lblPaid,
				this.lblNoFee,
				this.fldNoFeeUnits,
				this.fldNoFeeAmount
			});
			this.GroupFooter1.Height = 0.9375F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.4375F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.1875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label12.Text = "TYPE";
			this.Label12.Top = 1.625F;
			this.Label12.Width = 1.0625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.3125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "FEE";
			this.Label13.Top = 1.625F;
			this.Label13.Width = 0.4375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.25F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label14.Text = "CUSTOMER";
			this.Label14.Top = 1.625F;
			this.Label14.Width = 1.0625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label15.Text = "DATE";
			this.Label15.Top = 1.625F;
			this.Label15.Width = 0.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.9375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.Label16.Text = "CLASS/PLATE";
			this.Label16.Top = 1.625F;
			this.Label16.Width = 1.125F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 6.875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label17.Text = "OPID";
			this.Label17.Top = 1.625F;
			this.Label17.Width = 0.4375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label1.Text = "**** USE TAX SUMMARY REPORT****";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.875F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0F;
			this.lblPage.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.8125F;
			this.Line1.Width = 7.375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.8125F;
			this.Line1.Y2 = 1.8125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.1875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label18.Text = "USE TAX";
			this.Label18.Top = 1.4375F;
			this.Label18.Width = 1.0625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.1875F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// txtType
			// 
			this.txtType.CanShrink = true;
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 0.25F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtType.Text = null;
			this.txtType.Top = 0F;
			this.txtType.Width = 0.875F;
			// 
			// txtFee
			// 
			this.txtFee.CanShrink = true;
			this.txtFee.Height = 0.1875F;
			this.txtFee.Left = 1.125F;
			this.txtFee.Name = "txtFee";
			this.txtFee.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.txtFee.Text = null;
			this.txtFee.Top = 0F;
			this.txtFee.Width = 0.75F;
			// 
			// txtClass
			// 
			this.txtClass.CanShrink = true;
			this.txtClass.Height = 0.1875F;
			this.txtClass.Left = 1.9375F;
			this.txtClass.Name = "txtClass";
			this.txtClass.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtClass.Text = null;
			this.txtClass.Top = 0F;
			this.txtClass.Width = 0.375F;
			// 
			// txtPlate
			// 
			this.txtPlate.CanShrink = true;
			this.txtPlate.Height = 0.1875F;
			this.txtPlate.Left = 2.3125F;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtPlate.Text = null;
			this.txtPlate.Top = 0F;
			this.txtPlate.Width = 0.875F;
			// 
			// txtDT
			// 
			this.txtDT.CanShrink = true;
			this.txtDT.Height = 0.1875F;
			this.txtDT.Left = 3.25F;
			this.txtDT.Name = "txtDT";
			this.txtDT.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtDT.Text = null;
			this.txtDT.Top = 0F;
			this.txtDT.Width = 0.8125F;
			// 
			// txtCustomer
			// 
			this.txtCustomer.CanShrink = true;
			this.txtCustomer.Height = 0.1875F;
			this.txtCustomer.Left = 4.21875F;
			this.txtCustomer.Name = "txtCustomer";
			this.txtCustomer.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtCustomer.Text = null;
			this.txtCustomer.Top = 0F;
			this.txtCustomer.Width = 2.46875F;
			// 
			// txtOPID
			// 
			this.txtOPID.CanShrink = true;
			this.txtOPID.Height = 0.1875F;
			this.txtOPID.Left = 6.84375F;
			this.txtOPID.Name = "txtOPID";
			this.txtOPID.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOPID.Text = null;
			this.txtOPID.Top = 0F;
			this.txtOPID.Width = 0.4375F;
			// 
			// fldPaidUnits
			// 
			this.fldPaidUnits.CanShrink = true;
			this.fldPaidUnits.Height = 0.1875F;
			this.fldPaidUnits.Left = 1.9375F;
			this.fldPaidUnits.Name = "fldPaidUnits";
			this.fldPaidUnits.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPaidUnits.Text = null;
			this.fldPaidUnits.Top = 0.5625F;
			this.fldPaidUnits.Visible = false;
			this.fldPaidUnits.Width = 0.6875F;
			// 
			// fldPaidAmount
			// 
			this.fldPaidAmount.CanShrink = true;
			this.fldPaidAmount.Height = 0.1875F;
			this.fldPaidAmount.Left = 2.75F;
			this.fldPaidAmount.Name = "fldPaidAmount";
			this.fldPaidAmount.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.fldPaidAmount.Text = null;
			this.fldPaidAmount.Top = 0.5625F;
			this.fldPaidAmount.Visible = false;
			this.fldPaidAmount.Width = 0.875F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.25F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "";
			this.lblTotals.Text = "Totals:";
			this.lblTotals.Top = 0.3125F;
			this.lblTotals.Visible = false;
			this.lblTotals.Width = 0.6875F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 1F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.3125F;
			this.lblType.Visible = false;
			this.lblType.Width = 0.6875F;
			// 
			// lblUnits
			// 
			this.lblUnits.Height = 0.1875F;
			this.lblUnits.HyperLink = null;
			this.lblUnits.Left = 1.9375F;
			this.lblUnits.Name = "lblUnits";
			this.lblUnits.Style = "";
			this.lblUnits.Text = "Units";
			this.lblUnits.Top = 0.3125F;
			this.lblUnits.Visible = false;
			this.lblUnits.Width = 0.6875F;
			// 
			// lblDollars
			// 
			this.lblDollars.Height = 0.1875F;
			this.lblDollars.HyperLink = null;
			this.lblDollars.Left = 2.75F;
			this.lblDollars.Name = "lblDollars";
			this.lblDollars.Style = "";
			this.lblDollars.Text = "Dollars";
			this.lblDollars.Top = 0.3125F;
			this.lblDollars.Visible = false;
			this.lblDollars.Width = 0.6875F;
			// 
			// linTotals
			// 
			this.linTotals.Height = 0F;
			this.linTotals.Left = 1F;
			this.linTotals.LineWeight = 1F;
			this.linTotals.Name = "linTotals";
			this.linTotals.Top = 0.5F;
			this.linTotals.Visible = false;
			this.linTotals.Width = 2.625F;
			this.linTotals.X1 = 1F;
			this.linTotals.X2 = 3.625F;
			this.linTotals.Y1 = 0.5F;
			this.linTotals.Y2 = 0.5F;
			// 
			// lblPaid
			// 
			this.lblPaid.Height = 0.1875F;
			this.lblPaid.HyperLink = null;
			this.lblPaid.Left = 1F;
			this.lblPaid.Name = "lblPaid";
			this.lblPaid.Style = "";
			this.lblPaid.Text = "PAID";
			this.lblPaid.Top = 0.5625F;
			this.lblPaid.Visible = false;
			this.lblPaid.Width = 0.6875F;
			// 
			// lblNoFee
			// 
			this.lblNoFee.Height = 0.1875F;
			this.lblNoFee.HyperLink = null;
			this.lblNoFee.Left = 1F;
			this.lblNoFee.Name = "lblNoFee";
			this.lblNoFee.Style = "";
			this.lblNoFee.Text = "NO FEE";
			this.lblNoFee.Top = 0.75F;
			this.lblNoFee.Visible = false;
			this.lblNoFee.Width = 0.6875F;
			// 
			// fldNoFeeUnits
			// 
			this.fldNoFeeUnits.CanShrink = true;
			this.fldNoFeeUnits.Height = 0.1875F;
			this.fldNoFeeUnits.Left = 1.9375F;
			this.fldNoFeeUnits.Name = "fldNoFeeUnits";
			this.fldNoFeeUnits.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldNoFeeUnits.Text = null;
			this.fldNoFeeUnits.Top = 0.75F;
			this.fldNoFeeUnits.Visible = false;
			this.fldNoFeeUnits.Width = 0.6875F;
			// 
			// fldNoFeeAmount
			// 
			this.fldNoFeeAmount.CanShrink = true;
			this.fldNoFeeAmount.Height = 0.1875F;
			this.fldNoFeeAmount.Left = 2.75F;
			this.fldNoFeeAmount.Name = "fldNoFeeAmount";
			this.fldNoFeeAmount.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.fldNoFeeAmount.Text = null;
			this.fldNoFeeAmount.Top = 0.75F;
			this.fldNoFeeAmount.Visible = false;
			this.fldNoFeeAmount.Width = 0.875F;
			// 
			// txtNoInfo
			// 
			this.txtNoInfo.CanShrink = true;
			this.txtNoInfo.Height = 0.3125F;
			this.txtNoInfo.Left = 1.96875F;
			this.txtNoInfo.Name = "txtNoInfo";
			this.txtNoInfo.Style = "font-family: \'Roman 10cpi\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.txtNoInfo.Text = null;
			this.txtNoInfo.Top = 0.15625F;
			this.txtNoInfo.Width = 2.6875F;
			// 
			// rptUseTaxSummary
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaidUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaidAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDollars)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoFeeUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoFeeAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCustomer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOPID;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoInfo;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaidUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaidAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUnits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDollars;
		private GrapeCity.ActiveReports.SectionReportModel.Line linTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaid;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNoFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoFeeUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoFeeAmount;
	}
}
