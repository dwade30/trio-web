﻿using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Receipting;

namespace TWMV0000
{
    public class MakeMotorVehicleTransactionHandler : CommandHandler<MakeMotorVehicleTransaction>
    {
	    private CommandDispatcher commandDispatcher;
	    private ISettingService settingService;
		public MakeMotorVehicleTransactionHandler(CommandDispatcher commandDispatcher, ISettingService settingService)
		{
			this.commandDispatcher = commandDispatcher;
			this.settingService = settingService;
		}

        protected override void Handle(MakeMotorVehicleTransaction command)
        {
			settingService.SaveSetting(SettingOwnerType.User, "EntryFlag", "CR");

            TWSharedLibrary.Variables.Statics.ShowOpIDForm = false;
            TWMV0000.MotorVehicle.Statics.bolFromWindowsCR  = true;
            TWMV0000.MotorVehicle.Statics.correlationId = command.CorrelationId;
            TWMV0000.MotorVehicle.Statics.TransactionCompleted = false;
			var lngWait = App.MainForm.OpenModule("TWMV0000", true, true);
            TWSharedLibrary.Variables.Statics.ShowOpIDForm = true;
            modGlobalConstants.Statics.gblnMVIsOpen = true;
        }
    }
}