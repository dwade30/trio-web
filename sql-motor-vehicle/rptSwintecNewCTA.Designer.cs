﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSwintecNewCTA.
	/// </summary>
	partial class rptSwintecNewCTA
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSwintecNewCTA));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJointOwnership = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLegalRes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLeased = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBodyType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPurchaseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFirstLienHolder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateofLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1Address = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1City = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1State = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecondLienHolder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Address = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2City = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2State = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSellerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlateNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDealer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMile = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKilometer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtActual = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotActual = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometerChanged = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometerBroken = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Date = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPreviousTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateofOrigin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtU = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRebuilt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmsrpamount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewMsrp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedMsrp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNRMSRP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJointOwnership)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalRes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBodyType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLienHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Address)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1City)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1State)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Zip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondLienHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Address)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2City)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2State)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Zip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSellerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlateNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDealer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKilometer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActual)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotActual)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerChanged)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerBroken)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Date)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreviousTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateofOrigin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRebuilt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmsrpamount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewMsrp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedMsrp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNRMSRP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName1,
				this.txtName2,
				this.txtJointOwnership,
				this.txtDOB1,
				this.txtDOB2,
				this.txtTelephone,
				this.txtAddress,
				this.txtCity,
				this.txtState,
				this.txtZip,
				this.txtLegalRes,
				this.txtLeased,
				this.txtYear,
				this.txtMake,
				this.txtModel,
				this.txtVIN,
				this.txtBodyType,
				this.txtPurchaseDate,
				this.txtFirstLienHolder,
				this.txtDateofLien,
				this.txtLien1Address,
				this.txtLien1City,
				this.txtLien1State,
				this.txtLien1Zip,
				this.txtSecondLienHolder,
				this.txtLien2Address,
				this.txtLien2City,
				this.txtLien2State,
				this.txtLien2Zip,
				this.txtSellerName,
				this.txtPlateNo,
				this.txtDealer,
				this.txtUsed,
				this.txtM,
				this.txtOdometer,
				this.txtMile,
				this.txtKilometer,
				this.txtActual,
				this.txtInExcess,
				this.txtNotActual,
				this.txtOdometerChanged,
				this.txtOdometerBroken,
				this.txtLien2Date,
				this.txtPreviousTitle,
				this.txtStateofOrigin,
				this.txtOther,
				this.txtNew,
				this.txtU,
				this.txtRebuilt,
				this.txtmsrpamount,
				this.txtNewMsrp,
				this.txtUsedMsrp,
				this.txtNRMSRP
			});
			this.Detail.Height = 8.125F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtName1
			// 
			this.txtName1.Height = 0.21875F;
			this.txtName1.Left = 0.5555556F;
			this.txtName1.MultiLine = false;
			this.txtName1.Name = "txtName1";
			this.txtName1.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtName1.Text = null;
			this.txtName1.Top = 0.0625F;
			this.txtName1.Width = 3F;
			// 
			// txtName2
			// 
			this.txtName2.Height = 0.21875F;
			this.txtName2.Left = 0.5555556F;
			this.txtName2.MultiLine = false;
			this.txtName2.Name = "txtName2";
			this.txtName2.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtName2.Text = null;
			this.txtName2.Top = 0.3125F;
			this.txtName2.Width = 3F;
			// 
			// txtJointOwnership
			// 
			this.txtJointOwnership.Height = 0.19F;
			this.txtJointOwnership.Left = 4.5625F;
			this.txtJointOwnership.MultiLine = false;
			this.txtJointOwnership.Name = "txtJointOwnership";
			this.txtJointOwnership.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtJointOwnership.Text = null;
			this.txtJointOwnership.Top = 0.3125F;
			this.txtJointOwnership.Width = 0.1875F;
			// 
			// txtDOB1
			// 
			this.txtDOB1.Height = 0.21875F;
			this.txtDOB1.Left = 5.0625F;
			this.txtDOB1.MultiLine = false;
			this.txtDOB1.Name = "txtDOB1";
			this.txtDOB1.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDOB1.Text = null;
			this.txtDOB1.Top = 0.0625F;
			this.txtDOB1.Width = 1F;
			// 
			// txtDOB2
			// 
			this.txtDOB2.Height = 0.1875F;
			this.txtDOB2.Left = 5.0625F;
			this.txtDOB2.MultiLine = false;
			this.txtDOB2.Name = "txtDOB2";
			this.txtDOB2.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDOB2.Text = null;
			this.txtDOB2.Top = 0.3125F;
			this.txtDOB2.Width = 1F;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Height = 0.21875F;
			this.txtTelephone.Left = 6.125F;
			this.txtTelephone.MultiLine = false;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtTelephone.Text = null;
			this.txtTelephone.Top = 0.0625F;
			this.txtTelephone.Width = 1.1875F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.21875F;
			this.txtAddress.Left = 0.3125F;
			this.txtAddress.MultiLine = false;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 0.625F;
			this.txtAddress.Width = 3.25F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.21875F;
			this.txtCity.Left = 0.3125F;
			this.txtCity.MultiLine = false;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtCity.Text = null;
			this.txtCity.Top = 0.9375F;
			this.txtCity.Width = 2F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.21875F;
			this.txtState.Left = 2.375F;
			this.txtState.MultiLine = false;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtState.Text = null;
			this.txtState.Top = 0.9375F;
			this.txtState.Width = 0.4375F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.21875F;
			this.txtZip.Left = 3F;
			this.txtZip.MultiLine = false;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtZip.Text = null;
			this.txtZip.Top = 0.9375F;
			this.txtZip.Width = 1.125F;
			// 
			// txtLegalRes
			// 
			this.txtLegalRes.Height = 0.21875F;
			this.txtLegalRes.Left = 0.3125F;
			this.txtLegalRes.MultiLine = false;
			this.txtLegalRes.Name = "txtLegalRes";
			this.txtLegalRes.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLegalRes.Text = null;
			this.txtLegalRes.Top = 1.28125F;
			this.txtLegalRes.Width = 3.78125F;
			// 
			// txtLeased
			// 
			this.txtLeased.Height = 0.21875F;
			this.txtLeased.Left = 0.3125F;
			this.txtLeased.MultiLine = false;
			this.txtLeased.Name = "txtLeased";
			this.txtLeased.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLeased.Text = null;
			this.txtLeased.Top = 1.59375F;
			this.txtLeased.Width = 3.25F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.21875F;
			this.txtYear.Left = 0.3125F;
			this.txtYear.MultiLine = false;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtYear.Text = null;
			this.txtYear.Top = 2F;
			this.txtYear.Width = 0.4375F;
			// 
			// txtMake
			// 
			this.txtMake.Height = 0.21875F;
			this.txtMake.Left = 0.875F;
			this.txtMake.MultiLine = false;
			this.txtMake.Name = "txtMake";
			this.txtMake.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtMake.Text = null;
			this.txtMake.Top = 2F;
			this.txtMake.Width = 0.75F;
			// 
			// txtModel
			// 
			this.txtModel.Height = 0.21875F;
			this.txtModel.Left = 1.75F;
			this.txtModel.MultiLine = false;
			this.txtModel.Name = "txtModel";
			this.txtModel.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtModel.Text = null;
			this.txtModel.Top = 2F;
			this.txtModel.Width = 0.8125F;
			// 
			// txtVIN
			// 
			this.txtVIN.Height = 0.21875F;
			this.txtVIN.Left = 2.625F;
			this.txtVIN.MultiLine = false;
			this.txtVIN.Name = "txtVIN";
			this.txtVIN.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtVIN.Text = null;
			this.txtVIN.Top = 2F;
			this.txtVIN.Width = 2.375F;
			// 
			// txtBodyType
			// 
			this.txtBodyType.Height = 0.21875F;
			this.txtBodyType.Left = 5.5F;
			this.txtBodyType.MultiLine = false;
			this.txtBodyType.Name = "txtBodyType";
			this.txtBodyType.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtBodyType.Text = null;
			this.txtBodyType.Top = 2F;
			this.txtBodyType.Width = 0.4375F;
			// 
			// txtPurchaseDate
			// 
			this.txtPurchaseDate.Height = 0.21875F;
			this.txtPurchaseDate.Left = 0.9375F;
			this.txtPurchaseDate.MultiLine = false;
			this.txtPurchaseDate.Name = "txtPurchaseDate";
			this.txtPurchaseDate.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtPurchaseDate.Text = null;
			this.txtPurchaseDate.Top = 2.46875F;
			this.txtPurchaseDate.Width = 1.03125F;
			// 
			// txtFirstLienHolder
			// 
			this.txtFirstLienHolder.Height = 0.21875F;
			this.txtFirstLienHolder.Left = 0.3125F;
			this.txtFirstLienHolder.MultiLine = false;
			this.txtFirstLienHolder.Name = "txtFirstLienHolder";
			this.txtFirstLienHolder.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtFirstLienHolder.Text = null;
			this.txtFirstLienHolder.Top = 3.46875F;
			this.txtFirstLienHolder.Width = 3.25F;
			// 
			// txtDateofLien
			// 
			this.txtDateofLien.Height = 0.21875F;
			this.txtDateofLien.Left = 4.75F;
			this.txtDateofLien.MultiLine = false;
			this.txtDateofLien.Name = "txtDateofLien";
			this.txtDateofLien.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDateofLien.Text = null;
			this.txtDateofLien.Top = 3.46875F;
			this.txtDateofLien.Width = 1.0625F;
			// 
			// txtLien1Address
			// 
			this.txtLien1Address.Height = 0.21875F;
			this.txtLien1Address.Left = 0.3125F;
			this.txtLien1Address.MultiLine = false;
			this.txtLien1Address.Name = "txtLien1Address";
			this.txtLien1Address.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1Address.Text = null;
			this.txtLien1Address.Top = 3.96875F;
			this.txtLien1Address.Width = 1.9375F;
			// 
			// txtLien1City
			// 
			this.txtLien1City.Height = 0.21875F;
			this.txtLien1City.Left = 2.3125F;
			this.txtLien1City.MultiLine = false;
			this.txtLien1City.Name = "txtLien1City";
			this.txtLien1City.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1City.Text = null;
			this.txtLien1City.Top = 3.96875F;
			this.txtLien1City.Width = 1.6875F;
			// 
			// txtLien1State
			// 
			this.txtLien1State.Height = 0.21875F;
			this.txtLien1State.Left = 4.125F;
			this.txtLien1State.MultiLine = false;
			this.txtLien1State.Name = "txtLien1State";
			this.txtLien1State.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1State.Text = null;
			this.txtLien1State.Top = 3.96875F;
			this.txtLien1State.Width = 0.75F;
			// 
			// txtLien1Zip
			// 
			this.txtLien1Zip.Height = 0.21875F;
			this.txtLien1Zip.Left = 4.9375F;
			this.txtLien1Zip.MultiLine = false;
			this.txtLien1Zip.Name = "txtLien1Zip";
			this.txtLien1Zip.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1Zip.Text = null;
			this.txtLien1Zip.Top = 3.96875F;
			this.txtLien1Zip.Width = 1F;
			// 
			// txtSecondLienHolder
			// 
			this.txtSecondLienHolder.Height = 0.21875F;
			this.txtSecondLienHolder.Left = 0.3125F;
			this.txtSecondLienHolder.MultiLine = false;
			this.txtSecondLienHolder.Name = "txtSecondLienHolder";
			this.txtSecondLienHolder.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtSecondLienHolder.Text = null;
			this.txtSecondLienHolder.Top = 4.46875F;
			this.txtSecondLienHolder.Width = 3.25F;
			// 
			// txtLien2Address
			// 
			this.txtLien2Address.Height = 0.21875F;
			this.txtLien2Address.Left = 0.3125F;
			this.txtLien2Address.MultiLine = false;
			this.txtLien2Address.Name = "txtLien2Address";
			this.txtLien2Address.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Address.Text = null;
			this.txtLien2Address.Top = 4.96875F;
			this.txtLien2Address.Width = 1.9375F;
			// 
			// txtLien2City
			// 
			this.txtLien2City.Height = 0.21875F;
			this.txtLien2City.Left = 2.3125F;
			this.txtLien2City.MultiLine = false;
			this.txtLien2City.Name = "txtLien2City";
			this.txtLien2City.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2City.Text = null;
			this.txtLien2City.Top = 4.96875F;
			this.txtLien2City.Width = 1.6875F;
			// 
			// txtLien2State
			// 
			this.txtLien2State.Height = 0.21875F;
			this.txtLien2State.Left = 4.125F;
			this.txtLien2State.MultiLine = false;
			this.txtLien2State.Name = "txtLien2State";
			this.txtLien2State.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2State.Text = null;
			this.txtLien2State.Top = 4.96875F;
			this.txtLien2State.Width = 0.75F;
			// 
			// txtLien2Zip
			// 
			this.txtLien2Zip.Height = 0.21875F;
			this.txtLien2Zip.Left = 4.9375F;
			this.txtLien2Zip.MultiLine = false;
			this.txtLien2Zip.Name = "txtLien2Zip";
			this.txtLien2Zip.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Zip.Text = null;
			this.txtLien2Zip.Top = 4.96875F;
			this.txtLien2Zip.Width = 1F;
			// 
			// txtSellerName
			// 
			this.txtSellerName.Height = 0.21875F;
			this.txtSellerName.Left = 0.3125F;
			this.txtSellerName.MultiLine = false;
			this.txtSellerName.Name = "txtSellerName";
			this.txtSellerName.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtSellerName.Text = null;
			this.txtSellerName.Top = 5.46875F;
			this.txtSellerName.Width = 3.6875F;
			// 
			// txtPlateNo
			// 
			this.txtPlateNo.Height = 0.21875F;
			this.txtPlateNo.Left = 4.5625F;
			this.txtPlateNo.MultiLine = false;
			this.txtPlateNo.Name = "txtPlateNo";
			this.txtPlateNo.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtPlateNo.Text = null;
			this.txtPlateNo.Top = 5.46875F;
			this.txtPlateNo.Width = 0.75F;
			// 
			// txtDealer
			// 
			this.txtDealer.Height = 0.19F;
			this.txtDealer.Left = 5.9375F;
			this.txtDealer.MultiLine = false;
			this.txtDealer.Name = "txtDealer";
			this.txtDealer.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtDealer.Text = null;
			this.txtDealer.Top = 5.28125F;
			this.txtDealer.Width = 0.1875F;
			// 
			// txtUsed
			// 
			this.txtUsed.Height = 0.19F;
			this.txtUsed.Left = 5.9375F;
			this.txtUsed.MultiLine = false;
			this.txtUsed.Name = "txtUsed";
			this.txtUsed.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtUsed.Text = null;
			this.txtUsed.Top = 5.4375F;
			this.txtUsed.Width = 0.1875F;
			// 
			// txtM
			// 
			this.txtM.Height = 0.19F;
			this.txtM.Left = 5.9375F;
			this.txtM.MultiLine = false;
			this.txtM.Name = "txtM";
			this.txtM.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtM.Text = null;
			this.txtM.Top = 5.59375F;
			this.txtM.Width = 0.1875F;
			// 
			// txtOdometer
			// 
			this.txtOdometer.Height = 0.21875F;
			this.txtOdometer.Left = 0.3125F;
			this.txtOdometer.MultiLine = false;
			this.txtOdometer.Name = "txtOdometer";
			this.txtOdometer.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtOdometer.Text = null;
			this.txtOdometer.Top = 2.96875F;
			this.txtOdometer.Width = 1.375F;
			// 
			// txtMile
			// 
			this.txtMile.Height = 0.19F;
			this.txtMile.Left = 2.1875F;
			this.txtMile.MultiLine = false;
			this.txtMile.Name = "txtMile";
			this.txtMile.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtMile.Text = null;
			this.txtMile.Top = 2.875F;
			this.txtMile.Width = 0.1875F;
			// 
			// txtKilometer
			// 
			this.txtKilometer.Height = 0.19F;
			this.txtKilometer.Left = 2.1875F;
			this.txtKilometer.MultiLine = false;
			this.txtKilometer.Name = "txtKilometer";
			this.txtKilometer.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtKilometer.Text = null;
			this.txtKilometer.Top = 3.15625F;
			this.txtKilometer.Width = 0.1875F;
			// 
			// txtActual
			// 
			this.txtActual.Height = 0.125F;
			this.txtActual.Left = 2.6875F;
			this.txtActual.MultiLine = false;
			this.txtActual.Name = "txtActual";
			this.txtActual.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtActual.Text = null;
			this.txtActual.Top = 2.875F;
			this.txtActual.Width = 0.1875F;
			// 
			// txtInExcess
			// 
			this.txtInExcess.Height = 0.125F;
			this.txtInExcess.Left = 2.6875F;
			this.txtInExcess.MultiLine = false;
			this.txtInExcess.Name = "txtInExcess";
			this.txtInExcess.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtInExcess.Text = null;
			this.txtInExcess.Top = 3F;
			this.txtInExcess.Width = 0.1875F;
			// 
			// txtNotActual
			// 
			this.txtNotActual.Height = 0.125F;
			this.txtNotActual.Left = 2.6875F;
			this.txtNotActual.MultiLine = false;
			this.txtNotActual.Name = "txtNotActual";
			this.txtNotActual.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtNotActual.Text = null;
			this.txtNotActual.Top = 3.125F;
			this.txtNotActual.Width = 0.1875F;
			// 
			// txtOdometerChanged
			// 
			this.txtOdometerChanged.Height = 0.19F;
			this.txtOdometerChanged.Left = 4.9375F;
			this.txtOdometerChanged.Name = "txtOdometerChanged";
			this.txtOdometerChanged.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOdometerChanged.Text = null;
			this.txtOdometerChanged.Top = 2.90625F;
			this.txtOdometerChanged.Width = 0.1875F;
			// 
			// txtOdometerBroken
			// 
			this.txtOdometerBroken.Height = 0.19F;
			this.txtOdometerBroken.Left = 4.9375F;
			this.txtOdometerBroken.Name = "txtOdometerBroken";
			this.txtOdometerBroken.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOdometerBroken.Text = null;
			this.txtOdometerBroken.Top = 3.0625F;
			this.txtOdometerBroken.Width = 0.1875F;
			// 
			// txtLien2Date
			// 
			this.txtLien2Date.Height = 0.21875F;
			this.txtLien2Date.Left = 4.75F;
			this.txtLien2Date.Name = "txtLien2Date";
			this.txtLien2Date.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Date.Text = null;
			this.txtLien2Date.Top = 4.46875F;
			this.txtLien2Date.Width = 1.0625F;
			// 
			// txtPreviousTitle
			// 
			this.txtPreviousTitle.Height = 0.21875F;
			this.txtPreviousTitle.Left = 2.0625F;
			this.txtPreviousTitle.Name = "txtPreviousTitle";
			this.txtPreviousTitle.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtPreviousTitle.Text = null;
			this.txtPreviousTitle.Top = 2.46875F;
			this.txtPreviousTitle.Width = 1.125F;
			// 
			// txtStateofOrigin
			// 
			this.txtStateofOrigin.Height = 0.21875F;
			this.txtStateofOrigin.Left = 3.375F;
			this.txtStateofOrigin.Name = "txtStateofOrigin";
			this.txtStateofOrigin.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtStateofOrigin.Text = null;
			this.txtStateofOrigin.Top = 2.46875F;
			this.txtStateofOrigin.Width = 0.8125F;
			// 
			// txtOther
			// 
			this.txtOther.Height = 0.21875F;
			this.txtOther.Left = 4.375F;
			this.txtOther.Name = "txtOther";
			this.txtOther.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtOther.Text = null;
			this.txtOther.Top = 2.46875F;
			this.txtOther.Width = 1.4375F;
			// 
			// txtNew
			// 
			this.txtNew.Height = 0.125F;
			this.txtNew.Left = 0.65625F;
			this.txtNew.MultiLine = false;
			this.txtNew.Name = "txtNew";
			this.txtNew.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtNew.Text = null;
			this.txtNew.Top = 2.40625F;
			this.txtNew.Width = 0.1875F;
			// 
			// txtU
			// 
			this.txtU.Height = 0.125F;
			this.txtU.Left = 0.65625F;
			this.txtU.MultiLine = false;
			this.txtU.Name = "txtU";
			this.txtU.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtU.Text = null;
			this.txtU.Top = 2.5625F;
			this.txtU.Width = 0.1875F;
			// 
			// txtRebuilt
			// 
			this.txtRebuilt.Height = 0.125F;
			this.txtRebuilt.Left = 0.65625F;
			this.txtRebuilt.MultiLine = false;
			this.txtRebuilt.Name = "txtRebuilt";
			this.txtRebuilt.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtRebuilt.Text = null;
			this.txtRebuilt.Top = 2.71875F;
			this.txtRebuilt.Width = 0.1875F;
			// 
			// txtmsrpamount
			// 
			this.txtmsrpamount.Height = 0.21875F;
			this.txtmsrpamount.Left = 4.5F;
			this.txtmsrpamount.Name = "txtmsrpamount";
			this.txtmsrpamount.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtmsrpamount.Text = null;
			this.txtmsrpamount.Top = 1.59375F;
			this.txtmsrpamount.Width = 1.375F;
			// 
			// txtNewMsrp
			// 
			this.txtNewMsrp.Height = 0.125F;
			this.txtNewMsrp.Left = 4.375F;
			this.txtNewMsrp.MultiLine = false;
			this.txtNewMsrp.Name = "txtNewMsrp";
			this.txtNewMsrp.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtNewMsrp.Text = null;
			this.txtNewMsrp.Top = 0.6875F;
			this.txtNewMsrp.Width = 0.1875F;
			// 
			// txtUsedMsrp
			// 
			this.txtUsedMsrp.Height = 0.125F;
			this.txtUsedMsrp.Left = 4.375F;
			this.txtUsedMsrp.MultiLine = false;
			this.txtUsedMsrp.Name = "txtUsedMsrp";
			this.txtUsedMsrp.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtUsedMsrp.Text = null;
			this.txtUsedMsrp.Top = 0.84375F;
			this.txtUsedMsrp.Width = 0.1875F;
			// 
			// txtNRMSRP
			// 
			this.txtNRMSRP.Height = 0.125F;
			this.txtNRMSRP.Left = 4.375F;
			this.txtNRMSRP.MultiLine = false;
			this.txtNRMSRP.Name = "txtNRMSRP";
			this.txtNRMSRP.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtNRMSRP.Text = null;
			this.txtNRMSRP.Top = 1F;
			this.txtNRMSRP.Width = 0.1875F;
			// 
			// rptSwintecNewCTA
			//
			// 
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.9375F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.374306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJointOwnership)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalRes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBodyType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLienHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Address)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1City)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1State)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Zip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondLienHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Address)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2City)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2State)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Zip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSellerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlateNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDealer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKilometer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActual)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotActual)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerChanged)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerBroken)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Date)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreviousTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateofOrigin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRebuilt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmsrpamount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewMsrp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedMsrp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNRMSRP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJointOwnership;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLegalRes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLeased;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBodyType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPurchaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirstLienHolder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateofLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1Address;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1City;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1State;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1Zip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondLienHolder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Address;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2City;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2State;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Zip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSellerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlateNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDealer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMile;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKilometer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtActual;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInExcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotActual;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometerChanged;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometerBroken;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Date;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPreviousTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateofOrigin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtU;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRebuilt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmsrpamount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewMsrp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedMsrp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNRMSRP;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
