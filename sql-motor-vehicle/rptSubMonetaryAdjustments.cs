//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubMonetaryAdjustments.
	/// </summary>
	public partial class rptSubMonetaryAdjustments : BaseSectionReport
	{
		public rptSubMonetaryAdjustments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Exception Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptSubMonetaryAdjustments_ReportEnd;
		}

        private void RptSubMonetaryAdjustments_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
			rsCategory.DisposeOf();
        }

        public static rptSubMonetaryAdjustments InstancePtr
		{
			get
			{
				return (rptSubMonetaryAdjustments)Sys.GetInstance(typeof(rptSubMonetaryAdjustments));
			}
		}

		protected rptSubMonetaryAdjustments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubMonetaryAdjustments	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsCategory = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rs.EndOfFile();
			}
			else
			{
				rs.MoveNext();
				eArgs.EOF = rs.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND Type = '5MA' ORDER BY ExceptionReportDate");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Type = '5MA' ORDER BY ExceptionReportDate");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rptNewExceptionReport.InstancePtr.boolData = true;
			}
			else
			{
				this.Close();
			}
			rsCategory.OpenRecordset("SELECT * FROM TownSummaryHeadings");
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldDate As object	OnWrite(string)
			// vbPorter upgrade warning: fldCategory As object	OnWrite(string)
			rptNewExceptionReport.InstancePtr.MonAdjTotal += 1;
			rptNewExceptionReport.InstancePtr.MonAdjMoneyTotal += FCConvert.ToSingle(rs.Get_Fields("Fee"));
			fldAmount.Text = Strings.Format(rs.Get_Fields("Fee"), "#,###.00");
			fldDate.Text = Strings.Format(rs.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yyyy");
			if (rsCategory.FindFirstRecord("CategoryCode", Conversion.Val(rs.Get_Fields_String("CategoryAdjusted"))))
			{
				fldCategory.Text = Strings.Format(rsCategory.Get_Fields_String("Heading"), "!@@@@@@@@@@@@@@@@@@@@");
			}
			fldExplanation.Text = rs.Get_Fields_String("reason");
			fldOpID.Text = rs.Get_Fields_String("OpID");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalCount As object	OnWrite
			// vbPorter upgrade warning: fldTotalMoney As object	OnWrite(string)
			fldTotalCount.Text = FCConvert.ToString(rptNewExceptionReport.InstancePtr.MonAdjTotal);
			fldTotalMoney.Text = Strings.Format(rptNewExceptionReport.InstancePtr.MonAdjMoneyTotal, "#,##0.00");
		}

		private void rptSubMonetaryAdjustments_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptSubMonetaryAdjustments properties;
			//rptSubMonetaryAdjustments.Caption	= "Exception Report";
			//rptSubMonetaryAdjustments.Icon	= "rptSubMonetaryAdjustments.dsx":0000";
			//rptSubMonetaryAdjustments.Left	= 0;
			//rptSubMonetaryAdjustments.Top	= 0;
			//rptSubMonetaryAdjustments.Width	= 11880;
			//rptSubMonetaryAdjustments.Height	= 8595;
			//rptSubMonetaryAdjustments.StartUpPosition	= 3;
			//rptSubMonetaryAdjustments.SectionData	= "rptSubMonetaryAdjustments.dsx":058A;
			//End Unmaped Properties
		}
	}
}
