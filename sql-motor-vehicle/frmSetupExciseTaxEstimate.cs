//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmSetupExciseTaxEstimate : BaseForm
	{
		public frmSetupExciseTaxEstimate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSetupExciseTaxEstimate InstancePtr
		{
			get
			{
				return (frmSetupExciseTaxEstimate)Sys.GetInstance(typeof(frmSetupExciseTaxEstimate));
			}
		}

		protected frmSetupExciseTaxEstimate _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public DateTime datStart;
		public DateTime datFinish;
		int SelectCol;
		int CodeCol;
		int DescriptionCol;

		private void frmSetupExciseTaxEstimate_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupExciseTaxEstimate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupExciseTaxEstimate properties;
			//frmSetupExciseTaxEstimate.FillStyle	= 0;
			//frmSetupExciseTaxEstimate.ScaleWidth	= 9045;
			//frmSetupExciseTaxEstimate.ScaleHeight	= 7140;
			//frmSetupExciseTaxEstimate.LinkTopic	= "Form2";
			//frmSetupExciseTaxEstimate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// vbPorter upgrade warning: ii As int	OnWriteFCConvert.ToInt32(
			int ii;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			for (ii = (DateTime.Now.Year - 5); ii <= (DateTime.Now.Year + 10); ii++)
			{
				cboStartYear.AddItem(ii.ToString());
				cboEndYear.AddItem(ii.ToString());
			}
			LoadClassCombo();
			LoadStyleGrid();
		}

		private void LoadStyleGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			SelectCol = 0;
			CodeCol = 1;
			DescriptionCol = 2;
			vsStyle.TextMatrix(0, CodeCol, "Code");
			vsStyle.TextMatrix(0, DescriptionCol, "Description");
			vsStyle.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsStyle.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			rsInfo.OpenRecordset("SELECT * FROM Style ORDER BY Code", "TWMV0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsStyle.Rows += 1;
					vsStyle.TextMatrix(vsStyle.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsStyle.TextMatrix(vsStyle.Rows - 1, CodeCol, FCConvert.ToString(rsInfo.Get_Fields("Code")));
					vsStyle.TextMatrix(vsStyle.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields_String("Style")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void optAllStyle_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			vsStyle.Enabled = false;
			for (counter = 1; counter <= (vsStyle.Rows - 1); counter++)
			{
				vsStyle.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void optSelectedStyle_CheckedChanged(object sender, System.EventArgs e)
		{
			vsStyle.Enabled = true;
			vsStyle.Focus();
		}

		private void vsStyle_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsStyle.Row > 0)
			{
				if (vsStyle.TextMatrix(vsStyle.Row, SelectCol) == "0")
				{
					vsStyle.TextMatrix(vsStyle.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsStyle.TextMatrix(vsStyle.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsStyle_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsStyle.Row > 0)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsStyle.TextMatrix(vsStyle.Row, SelectCol)) == false)
					if (FCConvert.CBool(vsStyle.TextMatrix(vsStyle.Row, SelectCol)) == false)
					{
						vsStyle.TextMatrix(vsStyle.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsStyle.TextMatrix(vsStyle.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void LoadClassCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboClass.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT BMVCode FROM Class ORDER BY BMVCode");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboClass.AddItem(rsInfo.Get_Fields_String("BMVCode"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void frmSetupExciseTaxEstimate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnSelected;
			if (cboStartMonth.SelectedIndex == -1 || cboStartYear.SelectedIndex == -1 || cboEndMonth.SelectedIndex == -1 || cboEndYear.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a beginning Year and Month and an ending Year and Month before you may continue.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			datStart = fecherFoundation.DateAndTime.DateValue(Conversion.Str(cboStartMonth.SelectedIndex + 1) + "/1/" + cboStartYear.Text);
			if (cboEndMonth.SelectedIndex == 11)
			{
				datFinish = fecherFoundation.DateAndTime.DateValue("1/1/" + Conversion.Str(Conversion.Val(cboEndYear.Text) + 1));
			}
			else
			{
				datFinish = fecherFoundation.DateAndTime.DateValue(Conversion.Str(cboEndMonth.SelectedIndex + 2) + "/1/" + cboEndYear.Text);
			}
			if (datFinish.ToOADate() < datStart.ToOADate())
			{
				MessageBox.Show("Your ending date must be later then your beginning date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			blnSelected = false;
			if (cmbAllStyle.Text == "Selected")
			{
				for (counter = 1; counter <= (vsStyle.Rows - 1); counter++)
				{
					if (FCConvert.CBool(vsStyle.TextMatrix(counter, SelectCol)))
					{
						blnSelected = true;
						break;
					}
				}
				if (!blnSelected)
				{
					MessageBox.Show("You must select at least one style before you may continue.", "Invalid Style Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			frmReportViewer.InstancePtr.Init(rptExciseTaxEstimate.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnSelected;
			if (cboStartMonth.SelectedIndex == -1 || cboStartYear.SelectedIndex == -1 || cboEndMonth.SelectedIndex == -1 || cboEndYear.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a beginning Year and Month and an ending Year and Month before you may continue.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			datStart = fecherFoundation.DateAndTime.DateValue(Conversion.Str(cboStartMonth.SelectedIndex + 1) + "/1/" + cboStartYear.Text);
			if (cboEndMonth.SelectedIndex == 11)
			{
				datFinish = fecherFoundation.DateAndTime.DateValue("1/1/" + Conversion.Str(Conversion.Val(cboEndYear.Text) + 1));
			}
			else
			{
				datFinish = fecherFoundation.DateAndTime.DateValue(Conversion.Str(cboEndMonth.SelectedIndex + 2) + "/1/" + cboEndYear.Text);
			}
			if (datFinish.ToOADate() < datStart.ToOADate())
			{
				MessageBox.Show("Your ending date must be later then your beginning date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			blnSelected = false;
			if (cmbAllStyle.Text == "Selected")
			{
				for (counter = 1; counter <= (vsStyle.Rows - 1); counter++)
				{
					if (FCConvert.CBool(vsStyle.TextMatrix(counter, SelectCol)))
					{
						blnSelected = true;
						break;
					}
				}
				if (!blnSelected)
				{
					MessageBox.Show("You must select at least one style before you may continue.", "Invalid Style Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			rptExciseTaxEstimate.InstancePtr.PrintReport(true);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecificClass.Enabled = false;
			cboClass.SelectedIndex = -1;
		}

		private void optSpecific_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecificClass.Enabled = true;
			cboClass.SelectedIndex = 0;
		}

		public void cmbSpecific_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbSpecific.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbSpecific.Text == "Specific")
			{
				optSpecific_CheckedChanged(sender, e);
			}
		}

		public void cmbAllStyle_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAllStyle.Text == "All")
			{
				optAllStyle_CheckedChanged(sender, e);
			}
			else if (cmbAllStyle.Text == "Selected")
			{
				optSelectedStyle_CheckedChanged(sender, e);
			}
		}
	}
}
