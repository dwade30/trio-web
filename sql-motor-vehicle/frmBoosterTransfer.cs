//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmBoosterTransfer : BaseForm
	{
		public frmBoosterTransfer()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBoosterTransfer InstancePtr
		{
			get
			{
				return (frmBoosterTransfer)Sys.GetInstance(typeof(frmBoosterTransfer));
			}
		}

		protected frmBoosterTransfer _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int IDCol;
		int OldPErmitCol;
		int NewPermitCol;
		int ExpiresCol;
		int BoostedWeightCol;
		int SelectCol;
		clsDRWrapper rsDefaultInfo = new clsDRWrapper();

		private void frmBoosterTransfer_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBoosterTransfer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBoosterTransfer properties;
			//frmBoosterTransfer.FillStyle	= 0;
			//frmBoosterTransfer.ScaleWidth	= 5880;
			//frmBoosterTransfer.ScaleHeight	= 3810;
			//frmBoosterTransfer.LinkTopic	= "Form2";
			//frmBoosterTransfer.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsBoosters = new clsDRWrapper();
			IDCol = 0;
			SelectCol = 1;
			OldPErmitCol = 2;
			BoostedWeightCol = 3;
			ExpiresCol = 4;
			NewPermitCol = 5;
			vsPermits.TextMatrix(0, SelectCol, "");
			vsPermits.TextMatrix(0, OldPErmitCol, "Old Permit");
			vsPermits.TextMatrix(0, BoostedWeightCol, "Boosted To");
			vsPermits.TextMatrix(0, ExpiresCol, "Expires");
			vsPermits.TextMatrix(0, NewPermitCol, "New Permit");
			vsPermits.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsPermits.ColHidden(IDCol, true);
			vsPermits.ColWidth(SelectCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.1));
			vsPermits.ColWidth(OldPErmitCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.22));
			vsPermits.ColWidth(BoostedWeightCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.2));
			vsPermits.ColWidth(ExpiresCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.25));
			vsPermits.ColWidth(NewPermitCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.01));
			vsPermits.ColAlignment(SelectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPermits.ColAlignment(OldPErmitCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPermits.ColAlignment(BoostedWeightCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPermits.ColAlignment(ExpiresCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPermits.ColAlignment(NewPermitCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			rsBoosters.OpenRecordset("SELECT * FROM Booster WHERE VehicleID = " + MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID") + " And Inactive = 0 and isnull(Voided, 0) = 0 and ExpireDate >= '" + DateTime.Today.ToShortDateString() + "' AND BoostedFrom >= " + MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("RegisteredWeightNew") + " ORDER BY BoostedWeight", "TWMV0000.vb1");
			if (rsBoosters.EndOfFile() != true && rsBoosters.BeginningOfFile() != true)
			{
				do
				{
					vsPermits.Rows += 1;
					vsPermits.TextMatrix(vsPermits.Rows - 1, IDCol, FCConvert.ToString(rsBoosters.Get_Fields_Int32("ID")));
					vsPermits.TextMatrix(vsPermits.Rows - 1, SelectCol, FCConvert.ToString(true));
					vsPermits.TextMatrix(vsPermits.Rows - 1, OldPErmitCol, FCConvert.ToString(rsBoosters.Get_Fields_Int32("Permit")));
					vsPermits.TextMatrix(vsPermits.Rows - 1, BoostedWeightCol, Strings.Format(rsBoosters.Get_Fields_Int32("BoostedWeight"), "#,##0"));
					vsPermits.TextMatrix(vsPermits.Rows - 1, ExpiresCol, Strings.Format(rsBoosters.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy"));
					rsBoosters.MoveNext();
				}
				while (rsBoosters.EndOfFile() != true);
			}
			vsPermits.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, OldPErmitCol, vsPermits.Rows - 1, ExpiresCol, 0x8000000F);
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			lblFeeAmount.Text = Strings.Format(8 + rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"), "$#,##0.00");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmBoosterTransfer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmBoosterTransfer_Resize(object sender, System.EventArgs e)
		{
			vsPermits.ColWidth(SelectCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.1));
			vsPermits.ColWidth(OldPErmitCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.22));
			vsPermits.ColWidth(BoostedWeightCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.2));
			vsPermits.ColWidth(ExpiresCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.25));
			vsPermits.ColWidth(NewPermitCol, FCConvert.ToInt32(vsPermits.WidthOriginal * 0.01));
			//vsPermits.Height = (vsPermits.Rows * vsPermits.RowHeight(0)) + 75;
		}

		private void ResizeGridHeight()
		{
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsBooster = new clsDRWrapper();
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnAtLEastOne;
			clsDRWrapper rsOldBooster = new clsDRWrapper();
			blnAtLEastOne = false;
			vsPermits.Col = SelectCol;
			for (counter = 1; counter <= (vsPermits.Rows - 1); counter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsPermits.TextMatrix(counter, SelectCol)) == true)
				if (FCConvert.CBool(vsPermits.TextMatrix(counter, SelectCol)) == true)
				{
					blnAtLEastOne = true;
					if (vsPermits.TextMatrix(counter, NewPermitCol) == "")
					{
						MessageBox.Show("You must enter a Permit Number for each permti you wish to transfer before you can Save.", "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			if (!blnAtLEastOne)
			{
				MessageBox.Show("You must transfer at least 1 permit before you may continue.", "Transfer Permit", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				MotorVehicle.Write_PDS_Work_Record_Stuff();
			}
			rsOldBooster.OpenRecordset("SELECT * FROM Booster");
			for (counter = 1; counter <= (vsPermits.Rows - 1); counter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsPermits.TextMatrix(counter, SelectCol)) == true)
				if (FCConvert.CBool(vsPermits.TextMatrix(counter, SelectCol)) == true)
				{
					rsOldBooster.FindFirstRecord("ID", vsPermits.TextMatrix(counter, IDCol));
					rs.OpenRecordset("SELECT * FROM Booster WHERE ID = 0");
					rs.AddNew();
					rs.Set_Fields("Permit", vsPermits.TextMatrix(counter, NewPermitCol));
					rs.Set_Fields("EffectiveDate", DateTime.Today);
					rs.Set_Fields("ExpireDate", vsPermits.TextMatrix(counter, ExpiresCol));
					rs.Set_Fields("BoostedFrom", rsOldBooster.Get_Fields_Int32("BoostedFrom"));
					rs.Set_Fields("BoostedWeight", vsPermits.TextMatrix(counter, BoostedWeightCol));
					rs.Set_Fields("StatePaid", FCConvert.ToDecimal(Conversion.Val(lblFeeAmount.Text) - Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"))));
					rs.Set_Fields("LocalPaid", rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"));
					rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
					rs.Set_Fields("plate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
					rs.Set_Fields("Class", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
					rs.Set_Fields("PeriodCloseoutID", 0);
					rs.Set_Fields("TellerCloseoutID", 0);
					rs.Set_Fields("DateUpdated", DateTime.Now);
					rs.Set_Fields("VehicleID", MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
					rs.Update();
					rsOldBooster.Edit();
					rsOldBooster.Set_Fields("Inactive", true);
					rsOldBooster.Update();
					rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE ID = 0");
					rs.AddNew();
					rs.Set_Fields("Type", "6PM");
					rs.Set_Fields("ExceptionReportDate", DateTime.Today);
					rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
					rs.Set_Fields("NewClass", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
					rs.Set_Fields("NewPlate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
					rs.Set_Fields("Fee", FCConvert.ToDecimal(Conversion.Val(lblFeeAmount.Text) - Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"))));
					rs.Set_Fields("PermitNumber", vsPermits.TextMatrix(counter, NewPermitCol));
					rs.Set_Fields("PermitType", "BOOSTER");
					rs.Set_Fields("ApplicantName", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1"))));
					rs.Set_Fields("MVR3Printed", 0);
					rs.Set_Fields("PeriodCloseoutID", 0);
					rs.Set_Fields("TellerCloseoutID", 0);
					rs.Update();
					rs.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(vsPermits.TextMatrix(counter, NewPermitCol))));
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						rs.MoveFirst();
						rs.Edit();
						rs.Set_Fields("Status", "I");
						rs.Set_Fields("PeriodCloseoutID", 0);
						rs.Set_Fields("TellerCloseoutID", 0);
						rs.Update();
					}
					rs.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
					rs.AddNew();
					rs.Set_Fields("AdjustmentCode", "I");
					rs.Set_Fields("DateOfAdjustment", DateTime.Today);
					rs.Set_Fields("QuantityAdjusted", 1);
					rs.Set_Fields("InventoryType", "BXSXX");
					rs.Set_Fields("Low", vsPermits.TextMatrix(counter, NewPermitCol));
					rs.Set_Fields("High", vsPermits.TextMatrix(counter, NewPermitCol));
					rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
					rs.Set_Fields("reason", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate") + " " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MVR3"));
					rs.Set_Fields("PeriodCloseoutID", 0);
					rs.Set_Fields("TellerCloseoutID", 0);
					rs.Update();
				}
			}
			MessageBox.Show("Be sure to complete the Booster Form.", "Fill out Booster Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				//MDIParent.InstancePtr.GetMainMenu();
				MDIParent.InstancePtr.Menu18();
			}
		}

		private void vsPermits_BeforeRowColChange(object sender, System.EventArgs e)
		{
			//if (e.newRow>0) {
			if (vsPermits.Col >= OldPErmitCol && vsPermits.Col <= ExpiresCol)
			{
				//if (e.oldCol>e.newCol) {
				//	//e.Cancel = true;
				//	vsPermits.Col = SelectCol;
				//} else {
				//e.Cancel = true;
				vsPermits.Col = NewPermitCol;
				//}
			}
			//}
		}

		private void vsPermits_ClickEvent(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (vsPermits.MouseRow > 0)
			{
				if (vsPermits.MouseCol == SelectCol)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsPermits.TextMatrix(vsPermits.MouseRow, SelectCol)) == true)
					if (FCConvert.CBool(vsPermits.TextMatrix(vsPermits.MouseRow, SelectCol)) == true)
					{
						vsPermits.TextMatrix(vsPermits.MouseRow, SelectCol, FCConvert.ToString(false));
						for (counter = (vsPermits.MouseRow + 1); counter <= (vsPermits.Rows - 1); counter++)
						{
							vsPermits.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
						}
					}
					else
					{
						vsPermits.TextMatrix(vsPermits.MouseRow, SelectCol, FCConvert.ToString(true));
						for (counter = (vsPermits.MouseRow - 1); counter >= 1; counter--)
						{
							vsPermits.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
						}
					}
				}
			}
		}

		private void vsPermits_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (vsPermits.Col == SelectCol && vsPermits.Row > 0)
			{
				if (KeyCode == Keys.Space)
				{
					KeyCode = 0;
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsPermits.TextMatrix(vsPermits.Row, SelectCol)) == true)
					if (FCConvert.CBool(vsPermits.TextMatrix(vsPermits.Row, SelectCol)) == true)
					{
						vsPermits.TextMatrix(vsPermits.Row, SelectCol, FCConvert.ToString(false));
						for (counter = (vsPermits.Row + 1); counter <= (vsPermits.Rows - 1); counter++)
						{
							vsPermits.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
						}
					}
					else
					{
						vsPermits.TextMatrix(vsPermits.Row, SelectCol, FCConvert.ToString(true));
						for (counter = (vsPermits.Row - 1); counter >= 1; counter--)
						{
							vsPermits.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
						}
					}
				}
			}
		}

		private void vsPermits_RowColChange(object sender, System.EventArgs e)
		{
			if (vsPermits.Row > 0)
			{
				if (vsPermits.Col >= OldPErmitCol && vsPermits.Col <= ExpiresCol)
				{
					vsPermits.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else if (vsPermits.Col == SelectCol)
				{
					vsPermits.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vsPermits.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsPermits.EditCell();
				}
			}
		}

		private void vsPermits_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			clsDRWrapper tempRS = new clsDRWrapper();
			if (vsPermits.Row > 0)
			{
				if (vsPermits.Col == NewPermitCol)
				{
					if (vsPermits.EditText != "")
					{
						if (MotorVehicle.Statics.PlateGroup == 0)
						{
							tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(vsPermits.EditText)) + " AND Status = 'A'");
						}
						else
						{
							tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(vsPermits.EditText)) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.PlateGroup) + " OR [Group] = 0)");
						}
						if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
						{
							return;
						}
						else
						{
							MessageBox.Show("This permit number was not found in Inventory.", "Invalid Permit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
						}
					}
				}
			}
		}
	}
}
