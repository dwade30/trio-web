//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmInventoryStatus.
	/// </summary>
	partial class frmInventoryStatus
	{
		public fecherFoundation.FCComboBox cmbSelection;
		public fecherFoundation.FCLabel lblSelection;
		public fecherFoundation.FCComboBox cmbStart;
		public fecherFoundation.FCComboBox cmbInventoryType;
		public fecherFoundation.FCLabel lblInventoryType;
		public fecherFoundation.FCComboBox cmbVoided;
		public fecherFoundation.FCLabel lblVoided;
		public fecherFoundation.FCFrame fraMVR10;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCFrame fraVehicle;
		public fecherFoundation.FCTextBox txtVIN;
		public fecherFoundation.FCTextBox txtColor;
		public fecherFoundation.FCTextBox txtMake;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCLabel lblVin;
		public fecherFoundation.FCLabel lblColor;
		public fecherFoundation.FCLabel lblMake;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCFrame fraApplicant;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtApplicant;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel lblApplicant;
		public fecherFoundation.FCTextBox txtPermit;
		public Global.T2KDateBox txtExpireDate;
		public Global.T2KDateBox txtEffectiveDate;
		public fecherFoundation.FCLabel lblPermit;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCListBox lstItems;
		public fecherFoundation.FCListBox lstPlates;
		public fecherFoundation.FCButton cmdPrintSelected;
		public fecherFoundation.FCButton cmdPrintAll;
		public fecherFoundation.FCButton cmdSearch;
		public FCCommonDialog dlg1_Save;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblItemType;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuMVR3;
		public fecherFoundation.FCToolStripMenuItem mnuPlates;
		public fecherFoundation.FCToolStripMenuItem mnuDoubleYear;
		public fecherFoundation.FCToolStripMenuItem mnuDoubleMonth;
		public fecherFoundation.FCToolStripMenuItem mnuSingleYear;
		public fecherFoundation.FCToolStripMenuItem mnuSingleMonth;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cmbSelection = new fecherFoundation.FCComboBox();
			this.lblSelection = new fecherFoundation.FCLabel();
			this.cmbStart = new fecherFoundation.FCComboBox();
			this.cmbInventoryType = new fecherFoundation.FCComboBox();
			this.lblInventoryType = new fecherFoundation.FCLabel();
			this.cmbVoided = new fecherFoundation.FCComboBox();
			this.lblVoided = new fecherFoundation.FCLabel();
			this.fraMVR10 = new fecherFoundation.FCFrame();
			this.cmdOk = new fecherFoundation.FCButton();
			this.fraVehicle = new fecherFoundation.FCFrame();
			this.txtVIN = new fecherFoundation.FCTextBox();
			this.txtColor = new fecherFoundation.FCTextBox();
			this.txtMake = new fecherFoundation.FCTextBox();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.lblVin = new fecherFoundation.FCLabel();
			this.lblColor = new fecherFoundation.FCLabel();
			this.lblMake = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.fraApplicant = new fecherFoundation.FCFrame();
			this.txtAddress = new fecherFoundation.FCTextBox();
			this.txtApplicant = new fecherFoundation.FCTextBox();
			this.lblAddress = new fecherFoundation.FCLabel();
			this.lblApplicant = new fecherFoundation.FCLabel();
			this.txtPermit = new fecherFoundation.FCTextBox();
			this.txtExpireDate = new Global.T2KDateBox();
			this.txtEffectiveDate = new Global.T2KDateBox();
			this.lblPermit = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lstItems = new fecherFoundation.FCListBox();
			this.lstPlates = new fecherFoundation.FCListBox();
			this.cmdPrintSelected = new fecherFoundation.FCButton();
			this.cmdPrintAll = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.dlg1_Save = new fecherFoundation.FCCommonDialog();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblItemType = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMVR3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPlates = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDoubleYear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDoubleMonth = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSingleYear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSingleMonth = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMVR10)).BeginInit();
			this.fraMVR10.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVehicle)).BeginInit();
			this.fraVehicle.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).BeginInit();
			this.fraApplicant.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintSelected)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 679);
			this.BottomPanel.Size = new System.Drawing.Size(888, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbStart);
			this.ClientArea.Controls.Add(this.cmbSelection);
			this.ClientArea.Controls.Add(this.lblSelection);
			this.ClientArea.Controls.Add(this.lstItems);
			this.ClientArea.Controls.Add(this.lstPlates);
			this.ClientArea.Controls.Add(this.cmbInventoryType);
			this.ClientArea.Controls.Add(this.lblInventoryType);
			this.ClientArea.Controls.Add(this.cmdPrintSelected);
			this.ClientArea.Controls.Add(this.cmdPrintAll);
			this.ClientArea.Controls.Add(this.cmbVoided);
			this.ClientArea.Controls.Add(this.lblVoided);
			this.ClientArea.Controls.Add(this.cmdSearch);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblItemType);
			this.ClientArea.Controls.Add(this.fraMVR10);
			this.ClientArea.Size = new System.Drawing.Size(908, 606);
			this.ClientArea.Controls.SetChildIndex(this.fraMVR10, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblItemType, 0);
			this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdSearch, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblVoided, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbVoided, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdPrintAll, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdPrintSelected, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblInventoryType, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbInventoryType, 0);
			this.ClientArea.Controls.SetChildIndex(this.lstPlates, 0);
			this.ClientArea.Controls.SetChildIndex(this.lstItems, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblSelection, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbSelection, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbStart, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(908, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(183, 28);
			this.HeaderText.Text = " Inventory Status";
			// 
			// cmbSelection
			// 
			this.cmbSelection.Items.AddRange(new object[] {
            "All",
            "Group 1",
            "Group 2",
            "Group 3",
            "Group 4",
            "Group 5",
            "Group 6",
            "Group 7",
            "Group 8",
            "Group 9",
            "Group 10",
            "Group 11",
            "Group 12"});
			this.cmbSelection.Location = new System.Drawing.Point(229, 180);
			this.cmbSelection.Name = "cmbSelection";
			this.cmbSelection.Size = new System.Drawing.Size(290, 40);
			this.cmbSelection.TabIndex = 42;
			this.cmbSelection.Text = "All";
			this.cmbSelection.SelectedIndexChanged += new System.EventHandler(this.optSelection_CheckedChanged);
			// 
			// lblSelection
			// 
			this.lblSelection.AutoSize = true;
			this.lblSelection.Location = new System.Drawing.Point(30, 194);
			this.lblSelection.Name = "lblSelection";
			this.lblSelection.Size = new System.Drawing.Size(118, 15);
			this.lblSelection.TabIndex = 43;
			this.lblSelection.Text = "GROUPS TO VIEW";
			// 
			// cmbStart
			// 
			this.cmbStart.Items.AddRange(new object[] {
            "Start"});
			this.cmbStart.Location = new System.Drawing.Point(229, 80);
			this.cmbStart.Name = "cmbStart";
			this.cmbStart.Size = new System.Drawing.Size(290, 40);
			this.cmbStart.TabIndex = 0;
			this.cmbStart.Visible = false;
			// 
			// cmbInventoryType
			// 
			this.cmbInventoryType.Items.AddRange(new object[] {
            "MVR3s",
            "Plates",
            "Double Year Stickers",
            "Double Month Stickers",
            "Single Year Stickers",
            "Single Month Stickers",
            "Decals",
            "Boosters",
            "MVR10s",
            "Combination Stickers"});
			this.cmbInventoryType.Location = new System.Drawing.Point(229, 30);
			this.cmbInventoryType.Name = "cmbInventoryType";
			this.cmbInventoryType.Size = new System.Drawing.Size(290, 40);
			this.cmbInventoryType.TabIndex = 44;
			this.cmbInventoryType.Text = "MVR3s";
			this.cmbInventoryType.SelectedIndexChanged += new System.EventHandler(this.optInventoryType_CheckedChanged);
			// 
			// lblInventoryType
			// 
			this.lblInventoryType.AutoSize = true;
			this.lblInventoryType.Location = new System.Drawing.Point(30, 44);
			this.lblInventoryType.Name = "lblInventoryType";
			this.lblInventoryType.Size = new System.Drawing.Size(198, 15);
			this.lblInventoryType.TabIndex = 45;
			this.lblInventoryType.Text = "SELECT THE INVENTORY ITEM";
			// 
			// cmbVoided
			// 
			this.cmbVoided.Items.AddRange(new object[] {
            "All",
            "Available",
            "Issued",
            "Voided",
            "Held",
            "Pending",
            "Deleted"});
			this.cmbVoided.Location = new System.Drawing.Point(229, 130);
			this.cmbVoided.Name = "cmbVoided";
			this.cmbVoided.Size = new System.Drawing.Size(290, 40);
			this.cmbVoided.TabIndex = 46;
			this.cmbVoided.Text = "All";
			this.cmbVoided.SelectedIndexChanged += new System.EventHandler(this.cmbVoided_SelectedIndexChanged);
			// 
			// lblVoided
			// 
			this.lblVoided.AutoSize = true;
			this.lblVoided.Location = new System.Drawing.Point(30, 144);
			this.lblVoided.Name = "lblVoided";
			this.lblVoided.Size = new System.Drawing.Size(113, 15);
			this.lblVoided.TabIndex = 47;
			this.lblVoided.Text = "STATUS TO VIEW";
			// 
			// fraMVR10
			// 
			this.fraMVR10.AppearanceKey = "groupBoxLeftBorder";
			this.fraMVR10.Controls.Add(this.cmdOk);
			this.fraMVR10.Controls.Add(this.fraVehicle);
			this.fraMVR10.Controls.Add(this.fraApplicant);
			this.fraMVR10.Controls.Add(this.txtPermit);
			this.fraMVR10.Controls.Add(this.txtExpireDate);
			this.fraMVR10.Controls.Add(this.txtEffectiveDate);
			this.fraMVR10.Controls.Add(this.lblPermit);
			this.fraMVR10.Controls.Add(this.Label2);
			this.fraMVR10.Controls.Add(this.Label1);
			this.fraMVR10.Location = new System.Drawing.Point(30, 30);
			this.fraMVR10.Name = "fraMVR10";
			this.fraMVR10.Size = new System.Drawing.Size(414, 649);
			this.fraMVR10.TabIndex = 41;
			this.fraMVR10.Text = "Mvr10 Information";
			this.fraMVR10.Visible = false;
			// 
			// cmdOk
			// 
			this.cmdOk.AppearanceKey = "actionButton";
			this.cmdOk.Location = new System.Drawing.Point(20, 590);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new System.Drawing.Size(66, 40);
			this.cmdOk.TabIndex = 62;
			this.cmdOk.Text = "OK";
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			// 
			// fraVehicle
			// 
			this.fraVehicle.Controls.Add(this.txtVIN);
			this.fraVehicle.Controls.Add(this.txtColor);
			this.fraVehicle.Controls.Add(this.txtMake);
			this.fraVehicle.Controls.Add(this.txtYear);
			this.fraVehicle.Controls.Add(this.lblVin);
			this.fraVehicle.Controls.Add(this.lblColor);
			this.fraVehicle.Controls.Add(this.lblMake);
			this.fraVehicle.Controls.Add(this.lblYear);
			this.fraVehicle.Location = new System.Drawing.Point(20, 330);
			this.fraVehicle.Name = "fraVehicle";
			this.fraVehicle.Size = new System.Drawing.Size(376, 241);
			this.fraVehicle.TabIndex = 48;
			this.fraVehicle.Text = "Vehicle Information";
			// 
			// txtVIN
			// 
			this.txtVIN.BackColor = System.Drawing.SystemColors.Window;
			this.txtVIN.Location = new System.Drawing.Point(154, 180);
			this.txtVIN.MaxLength = 18;
			this.txtVIN.Name = "txtVIN";
			this.txtVIN.Size = new System.Drawing.Size(203, 40);
			this.txtVIN.TabIndex = 52;
			// 
			// txtColor
			// 
			this.txtColor.BackColor = System.Drawing.SystemColors.Window;
			this.txtColor.Location = new System.Drawing.Point(154, 130);
			this.txtColor.MaxLength = 2;
			this.txtColor.Name = "txtColor";
			this.txtColor.Size = new System.Drawing.Size(203, 40);
			this.txtColor.TabIndex = 51;
			// 
			// txtMake
			// 
			this.txtMake.BackColor = System.Drawing.SystemColors.Window;
			this.txtMake.Location = new System.Drawing.Point(154, 80);
			this.txtMake.MaxLength = 4;
			this.txtMake.Name = "txtMake";
			this.txtMake.Size = new System.Drawing.Size(203, 40);
			this.txtMake.TabIndex = 50;
			// 
			// txtYear
			// 
			this.txtYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtYear.Location = new System.Drawing.Point(154, 30);
			this.txtYear.MaxLength = 4;
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(203, 40);
			this.txtYear.TabIndex = 49;
			// 
			// lblVin
			// 
			this.lblVin.Location = new System.Drawing.Point(20, 194);
			this.lblVin.Name = "lblVin";
			this.lblVin.Size = new System.Drawing.Size(46, 15);
			this.lblVin.TabIndex = 56;
			this.lblVin.Text = "VIN #";
			// 
			// lblColor
			// 
			this.lblColor.Location = new System.Drawing.Point(20, 144);
			this.lblColor.Name = "lblColor";
			this.lblColor.Size = new System.Drawing.Size(46, 15);
			this.lblColor.TabIndex = 55;
			this.lblColor.Text = "COLOR";
			// 
			// lblMake
			// 
			this.lblMake.Location = new System.Drawing.Point(20, 94);
			this.lblMake.Name = "lblMake";
			this.lblMake.Size = new System.Drawing.Size(46, 15);
			this.lblMake.TabIndex = 54;
			this.lblMake.Text = "MAKE";
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(20, 44);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(46, 15);
			this.lblYear.TabIndex = 53;
			this.lblYear.Text = "YEAR";
			// 
			// fraApplicant
			// 
			this.fraApplicant.Controls.Add(this.txtAddress);
			this.fraApplicant.Controls.Add(this.txtApplicant);
			this.fraApplicant.Controls.Add(this.lblAddress);
			this.fraApplicant.Controls.Add(this.lblApplicant);
			this.fraApplicant.Location = new System.Drawing.Point(20, 180);
			this.fraApplicant.Name = "fraApplicant";
			this.fraApplicant.Size = new System.Drawing.Size(376, 140);
			this.fraApplicant.TabIndex = 43;
			this.fraApplicant.Text = "Applicant Information";
			// 
			// txtAddress
			// 
			this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress.Location = new System.Drawing.Point(154, 80);
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Size = new System.Drawing.Size(203, 40);
			this.txtAddress.TabIndex = 45;
			// 
			// txtApplicant
			// 
			this.txtApplicant.BackColor = System.Drawing.SystemColors.Window;
			this.txtApplicant.Location = new System.Drawing.Point(154, 30);
			this.txtApplicant.Name = "txtApplicant";
			this.txtApplicant.Size = new System.Drawing.Size(203, 40);
			this.txtApplicant.TabIndex = 44;
			// 
			// lblAddress
			// 
			this.lblAddress.Location = new System.Drawing.Point(20, 94);
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Size = new System.Drawing.Size(60, 16);
			this.lblAddress.TabIndex = 47;
			this.lblAddress.Text = "ADDRESS";
			// 
			// lblApplicant
			// 
			this.lblApplicant.Location = new System.Drawing.Point(20, 44);
			this.lblApplicant.Name = "lblApplicant";
			this.lblApplicant.Size = new System.Drawing.Size(110, 16);
			this.lblApplicant.TabIndex = 46;
			this.lblApplicant.Text = "APPLICANT NAME";
			// 
			// txtPermit
			// 
			this.txtPermit.BackColor = System.Drawing.SystemColors.Window;
			this.txtPermit.Location = new System.Drawing.Point(174, 130);
			this.txtPermit.MaxLength = 6;
			this.txtPermit.Name = "txtPermit";
			this.txtPermit.Size = new System.Drawing.Size(111, 40);
			this.txtPermit.TabIndex = 42;
			// 
			// txtExpireDate
			// 
			this.txtExpireDate.Location = new System.Drawing.Point(172, 80);
			this.txtExpireDate.Mask = "##/##/####";
			this.txtExpireDate.Name = "txtExpireDate";
			this.txtExpireDate.Size = new System.Drawing.Size(115, 22);
			this.txtExpireDate.TabIndex = 57;
			this.txtExpireDate.TabStop = false;
			// 
			// txtEffectiveDate
			// 
			this.txtEffectiveDate.Location = new System.Drawing.Point(172, 30);
			this.txtEffectiveDate.Mask = "##/##/####";
			this.txtEffectiveDate.Name = "txtEffectiveDate";
			this.txtEffectiveDate.Size = new System.Drawing.Size(115, 22);
			this.txtEffectiveDate.TabIndex = 58;
			this.txtEffectiveDate.TabStop = false;
			// 
			// lblPermit
			// 
			this.lblPermit.Location = new System.Drawing.Point(20, 144);
			this.lblPermit.Name = "lblPermit";
			this.lblPermit.Size = new System.Drawing.Size(62, 16);
			this.lblPermit.TabIndex = 61;
			this.lblPermit.Text = "PERMIT #";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(110, 16);
			this.Label2.TabIndex = 60;
			this.Label2.Text = "EXPIRATION DATE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(100, 16);
			this.Label1.TabIndex = 59;
			this.Label1.Text = "EFFECTIVE DATE";
			// 
			// lstItems
			// 
			this.lstItems.BackColor = System.Drawing.SystemColors.Window;
			this.lstItems.Location = new System.Drawing.Point(539, 55);
			this.lstItems.Name = "lstItems";
			this.lstItems.Size = new System.Drawing.Size(184, 165);
			this.lstItems.TabIndex = 8;
			this.lstItems.DoubleClick += new System.EventHandler(this.lstItems_DoubleClick);
			// 
			// lstPlates
			// 
			this.lstPlates.BackColor = System.Drawing.SystemColors.Window;
			this.lstPlates.Location = new System.Drawing.Point(539, 55);
			this.lstPlates.Name = "lstPlates";
			this.lstPlates.Size = new System.Drawing.Size(319, 165);
			this.lstPlates.Sorted = true;
			this.lstPlates.Sorting = Wisej.Web.SortOrder.Ascending;
			this.lstPlates.TabIndex = 9;
			this.lstPlates.DoubleClick += new System.EventHandler(this.lstPlates_DoubleClick);
			// 
			// cmdPrintSelected
			// 
			this.cmdPrintSelected.AppearanceKey = "actionButton";
			this.cmdPrintSelected.Location = new System.Drawing.Point(30, 230);
			this.cmdPrintSelected.Name = "cmdPrintSelected";
			this.cmdPrintSelected.Size = new System.Drawing.Size(233, 40);
			this.cmdPrintSelected.TabIndex = 17;
			this.cmdPrintSelected.Text = "Print Selected Inventory";
			this.cmdPrintSelected.Click += new System.EventHandler(this.cmdPrintSelected_Click);
			// 
			// cmdPrintAll
			// 
			this.cmdPrintAll.AppearanceKey = "actionButton";
			this.cmdPrintAll.Location = new System.Drawing.Point(283, 230);
			this.cmdPrintAll.Name = "cmdPrintAll";
			this.cmdPrintAll.Size = new System.Drawing.Size(184, 40);
			this.cmdPrintAll.TabIndex = 18;
			this.cmdPrintAll.Text = "Print All Inventory";
			this.cmdPrintAll.Click += new System.EventHandler(this.cmdPrintAll_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Enabled = false;
			this.cmdSearch.Location = new System.Drawing.Point(487, 230);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(93, 40);
			this.cmdSearch.TabIndex = 19;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// dlg1_Save
			// 
			this.dlg1_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1_Save.FontName = "Microsoft Sans Serif";
			this.dlg1_Save.Name = "dlg1_Save";
			this.dlg1_Save.Size = new System.Drawing.Size(0, 0);
			this.dlg1_Save.TabIndex = 0;
			// 
			// dlg1
			// 
			this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1.FontName = "Microsoft Sans Serif";
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			this.dlg1.TabIndex = 0;
			// 
			// vs1
			// 
			this.vs1.Cols = 8;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedCols = 0;
			this.vs1.Location = new System.Drawing.Point(30, 280);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 1;
			this.vs1.ShowFocusCell = false;
			this.vs1.Size = new System.Drawing.Size(736, 286);
			this.vs1.TabIndex = 16;
			this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
			// 
			// lblItemType
			// 
			this.lblItemType.Location = new System.Drawing.Point(539, 30);
			this.lblItemType.Name = "lblItemType";
			this.lblItemType.Size = new System.Drawing.Size(153, 15);
			this.lblItemType.TabIndex = 37;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "";
			// 
			// mnuMVR3
			// 
			this.mnuMVR3.Index = -1;
			this.mnuMVR3.Name = "mnuMVR3";
			this.mnuMVR3.Text = "MVR3";
			// 
			// mnuPlates
			// 
			this.mnuPlates.Index = -1;
			this.mnuPlates.Name = "mnuPlates";
			this.mnuPlates.Text = "Plates";
			// 
			// mnuDoubleYear
			// 
			this.mnuDoubleYear.Index = -1;
			this.mnuDoubleYear.Name = "mnuDoubleYear";
			this.mnuDoubleYear.Text = "Double Year Stickers";
			// 
			// mnuDoubleMonth
			// 
			this.mnuDoubleMonth.Index = -1;
			this.mnuDoubleMonth.Name = "mnuDoubleMonth";
			this.mnuDoubleMonth.Text = "Double Month Stickers";
			// 
			// mnuSingleYear
			// 
			this.mnuSingleYear.Index = -1;
			this.mnuSingleYear.Name = "mnuSingleYear";
			this.mnuSingleYear.Text = "Single Year Stickers";
			// 
			// mnuSingleMonth
			// 
			this.mnuSingleMonth.Index = -1;
			this.mnuSingleMonth.Name = "mnuSingleMonth";
			this.mnuSingleMonth.Text = "Single Month Stickers";
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = -1;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmInventoryStatus
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(908, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmInventoryStatus";
			this.Text = " Inventory Status";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmInventoryStatus_Load);
			this.Activated += new System.EventHandler(this.frmInventoryStatus_Activated);
			this.Resize += new System.EventHandler(this.frmInventoryStatus_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmInventoryStatus_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMVR10)).EndInit();
			this.fraMVR10.ResumeLayout(false);
			this.fraMVR10.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVehicle)).EndInit();
			this.fraVehicle.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).EndInit();
			this.fraApplicant.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintSelected)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
