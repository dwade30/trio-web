//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Receipting;
using System;
using System.IO;
using SharedApplication.Telemetry;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
    public class MDIParent// : BaseForm
    {
        private readonly ITelemetryService _telemetryService;
        public FCCommonDialog CommonDialog1;
        public MDIParent()
        {
            
            //InitializeComponent();
            InitializeComponentEx();
        }

        public MDIParent(ITelemetryService telemetryService) : base()
        {
            _telemetryService = telemetryService;
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
                _InstancePtr = this;
            CommonDialog1 = new FCCommonDialog();
        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static MDIParent InstancePtr
        {
            get
            {
                return (MDIParent)Sys.GetInstance(typeof(MDIParent));
            }
        }

        public void Init(bool addExitMenu = false)
        {
            modGlobalFunctions.LoadTRIOColors();
            App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.NavigationMenu.OriginName = "Motor Vehicle";
            //FC:FINAL:MSH - change status bar text(same with issue #1886)
            App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
            App.MainForm.menuTree.ImageList = CreateImageList();
            GetMainMenu(addExitMenu);
        }

        protected MDIParent _InstancePtr = null;
        //=========================================================
        // Last Updated:  6/27/01
        private int[] LabelNumber = new int[200 + 1];
        //int CurRow;
        int lngFCode;

        public ImageList CreateImageList()
        {
            var imageList = new ImageList { ImageSize = new System.Drawing.Size(18, 18) };
            imageList.Images.AddRange(new ImageListEntry[] {
                new ImageListEntry("menutree-registration-menu", "registration-menu"),
                new ImageListEntry("menutree-printing", "printing"),
                new ImageListEntry("menutree-daily-audit-report", "daily-audit-report"),
                new ImageListEntry("menutree-load-of-back-information", "load-of-back-information"),
                new ImageListEntry("menutree-inventory-maintenance", "inventory-maintenance"),
                new ImageListEntry("menutree-exception-report-items", "exception-report-items"),
                new ImageListEntry("menutree-vehicle-history", "vehicle-history"),
                new ImageListEntry("menutree-table-file-setup", "table-file-setup"),
                new ImageListEntry("menutree-extract-remote-reader", "extract-remote-reader"),
                new ImageListEntry("menutree-blue-book", "blue-book"),
                new ImageListEntry("menutree-inventory-status", "inventory-status"),
                new ImageListEntry("menutree-rapid-renewal", "rapid-renewal"),
                new ImageListEntry("menutree-new-to-system-update", "new-to-system-update"),
                new ImageListEntry("menutree-teller-closeout", "teller-closeout"),
                new ImageListEntry("menutree-gift-certificates", "gift-certificates"),
                new ImageListEntry("menutree-teller-closeout", "teller-closeout"),
                new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
                new ImageListEntry("menutree-registration-menu-active", "registration-menu-active"),
                new ImageListEntry("menutree-printing-active", "printing-active"),
                new ImageListEntry("menutree-daily-audit-report-active", "daily-audit-report-active"),
                new ImageListEntry("menutree-load-of-back-information-active", "load-of-back-information-active"),
                new ImageListEntry("menutree-inventory-maintenance-active", "inventory-maintenance-active"),
                new ImageListEntry("menutree-exception-report-items-active", "exception-report-items-active"),
                new ImageListEntry("menutree-vehicle-history-active", "vehicle-history-active"),
                new ImageListEntry("menutree-table-file-setup-active", "table-file-setup-active"),
                new ImageListEntry("menutree-extract-remote-reader-active", "extract-remote-reader-active"),
                new ImageListEntry("menutree-blue-book-active", "blue-book-active"),
                new ImageListEntry("menutree-inventory-status-active", "inventory-status-active"),
                new ImageListEntry("menutree-rapid-renewal-active", "rapid-renewal-active"),
                new ImageListEntry("menutree-new-to-system-update-active", "new-to-system-update-active"),
                new ImageListEntry("menutree-teller-closeout-active", "teller-closeout-active"),
                new ImageListEntry("menutree-gift-certificates-active", "gift-certificates-active"),
                new ImageListEntry("menutree-teller-closeout-active", "teller-closeout-active"),
                new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
                new ImageListEntry("menutree-street-maintenance", "street-maintenance")
            });
            return imageList;
        }

        public void ClearLabels()
        {

            // this function will clear all of the text in the label captions
        }

        private void SetMenuOptions(string strMenu)
        {

            ClearLabels();
            // Call CloseChildForms
            //GRID.Tag = (System.Object)(strMenu);
            GetMainMenu();
            //AutoSizeMenu(ref GRID, 20);
        }

        public void Menu1()
        {
            FCFileSystem fs = new FCFileSystem();
            // vbPorter upgrade warning: ans As object	OnWrite(DialogResult)
            DialogResult ans = 0;
            string strRec1 = "";
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            switch (vbPorterVar)
            {
                case "Main" when MotorVehicle.Statics.gboolReRegBatchStarted:

                    // kk03252015 tromv-999
                    return;
                case "Main" when !MotorVehicle.Statics.RegisteringFleet:
                    {
                        if (MotorVehicle.JobComplete("frmDataInput"))
                        {
                            MotorVehicle.ClearAllVariables();
                        }
                        FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                        //frmPlateInfo.InstancePtr.Show(App.MainForm);
                        frmPlateInfo.InstancePtr.ShowForm();
                        FCGlobal.Screen.MousePointer = 0;

                        break;
                    }

                case "Main":
                    {
                        ans = MessageBox.Show("You cannot do this while Registering a Fleet of Vehicles.  Do you wish to end your Fleet Registration?", "Invalid Menu Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.Yes)
                        {
                            MotorVehicle.ClearAllVariables();
                            MotorVehicle.Statics.RegisteringFleet = false;
                            FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                            //frmPlateInfo.InstancePtr.Show(App.MainForm);
                            frmPlateInfo.InstancePtr.ShowForm();
                            FCGlobal.Screen.MousePointer = 0;
                        }
                        else
                        {
                            return;
                        }

                        break;
                    }

                case "File":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmSettings.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPrintVIN.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Purge":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPurgeHeld.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Custom" when fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" :
                    mnuCustomAddressOverride_Click(null, null);

                    break;
                case "Custom" when fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC" ||
                                   fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER":
                    mnuCustomLongTermEndofPeriod_Click(null, null);

                    break;

                case "Custom":
                    {
                        if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" ||
                            fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER" ||
                            fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
                        {
                            mnuCustomPrintQueue_Click(null, null);
                        }

                        break;
                    }

                default:
                    break;
            }
        }

        public void Menu10()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    MotorVehicle.GetRedbookValues(false, false);
                    //frmDataInput.InstancePtr.Unload();
                    //this.Focus();
                    break;
                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmReportViewer.InstancePtr.Init(rptTellerCouseOutAuditReport.InstancePtr);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
            }
        }

        public void Menu11()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmInventoryStatus.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "File":
                    GetMainMenu();

                    break;
                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmFleetGroupLabels.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                default:
                    break;
            }
        }

        public void Menu12()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    {
                        clsDRWrapper rsUser = new clsDRWrapper();

                        try
                        {
                            rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
                            if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                if (MotorVehicle.Statics.OpID != "988")
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                        finally
                        {
                            rsUser.DisposeOf();
                        }

                        FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                        menuRapidRenewal.InstancePtr.Show(App.MainForm);
                        FCGlobal.Screen.MousePointer = 0;

                        break;
                    }

                case "File":
                    break;
                case "Report":
                    {
                        FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                        if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
                        {
                            frmFleetExpiringListMMTA.InstancePtr.Show(App.MainForm);
                        }
                        else
                        {
                            frmFleetExpiringList.InstancePtr.Show(App.MainForm);
                        }
                        FCGlobal.Screen.MousePointer = 0;

                        break;
                    }

                default:
                    break;
            }
        }

        public void Menu13()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main" when MotorVehicle.Statics.gboolReRegBatchStarted:

                    // kk03252015 tromv-999
                    return;
                case "Main":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    //frmPlateInfo.InstancePtr.Show(App.MainForm);
                    frmPlateInfo.InstancePtr.ShowForm();
                    //App.DoEvents();
                    frmPlateInfo.InstancePtr.OKFlag = true;
                    frmPlateInfo.InstancePtr.cmbRegType1.Text = "New To System / Update";
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "File":
                    break;
                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmReportViewer.InstancePtr.Init(rptAuditReport.InstancePtr);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                default:
                    break;
            }
        }

        public void Menu14()
        {

            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    {
                        clsDRWrapper rsUser = new clsDRWrapper();

                        try
                        {
                            rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
                            if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                if (MotorVehicle.Statics.OpID != "988")
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                        finally
                        {
                            rsUser.DisposeOf();
                        }
                        FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                        frmTellerCloseout.InstancePtr.Show(App.MainForm);
                        FCGlobal.Screen.MousePointer = 0;

                        break;
                    }

                case "File":
                    break;
                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmSetupAuditArchiveReport.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                default:
                    break;
            }
        }

        public void Menu15()
        {

            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmGiftCertificates.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "File":
                    break;
                case "Report":
                    GetMainMenu();

                    break;
                default:
                    break;
            }
        }

        public void Menu16()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":

                    //GetFileMenu();
                    break;
                case "File":
                    break;
                case "Report":
                    break;
                default:
                    break;
            }
        }

        public void Menu18()
        {

            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    {
                        foreach (FCForm f in FCGlobal.Statics.Forms)
                        {
                            if (f.Name == "frmPlateInfo")
                            {
                                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.bolFromWindowsCR)
                                {
                                    frmPlateInfo.InstancePtr.cmdCancel_Click();
                                    return;
                                }
                                break;
                            }
                        }
                        modBlockEntry.WriteYY();
                        if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
                        {
                            // Do Nothing
                        }
                        else
                        {
                            //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                        }

                        FCUtils.CloseFormsOnProject();
                        App.MainForm.EndWaitMVModule();
                        if (MotorVehicle.Statics.correlationId != new Guid())
                        {
                            if (!MotorVehicle.Statics.TransactionCompleted)
                            {
                                StaticSettings.GlobalEventPublisher.Publish(new TransactionCancelled(MotorVehicle.Statics.correlationId,null ));
                            }
                            else
                            {
                                MotorVehicle.Statics.correlationId = new Guid();
                                MotorVehicle.Statics.TransactionCompleted = false;
                            }
                            App.MainForm.OpenModule("TWCR0000", false);
                        }

                        break;
                    }

                case "File":
                    break;
                case "Report":
                    break;
                default:
                    break;
            }
        }

        public void Menu17()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    break;
                case "File":
                    break;
                case "Report":
                    break;
                default:
                    break;
            }
        }

        public void Menu2()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmReport.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;

                case "File":
                    CDBS();

                    break;

                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPrintFleet.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;

                case "Purge":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPurgeTerminated.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;

                case "Custom" when fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT":
                    mnuCustomPrintQueue_Click(null, null);

                    break;

                case "Custom":
                    {
                        if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION" ||
                            fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER")
                        {
                            mnuCustomLongTermEndofPeriod_Click(null, null);
                        }

                        break;
                    }

                default:
                    break;
            }
        }

        public void Menu3()
        {

            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            switch (vbPorterVar)
            {
                case "Main":
                    {
                        clsDRWrapper rsUser = new clsDRWrapper();

                        try
                        {
                            rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
                            if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                if (MotorVehicle.Statics.OpID != "988")
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                        finally
                        {
                            rsUser.DisposeOf();
                        }
                        FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                        frmPrePrints.InstancePtr.blnBMVDataRefresh = false;
                        frmPrePrints.InstancePtr.Show(App.MainForm);
                        FCGlobal.Screen.MousePointer = 0;

                        break;
                    }

                case "File":
                    {
                        clsDRWrapper rsUser = new clsDRWrapper();

                        try
                        {
                            rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
                            if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                if (MotorVehicle.Statics.OpID != "988")
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                        finally
                        {
                            rsUser.DisposeOf();
                        }
                        //GetPurgeMenu();
                        break;
                    }

                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmReminder.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Purge":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPurgeExpired.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;

                case "Custom":
                    {
                        if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT")
                        {
                            mnuCustomLongTermEndofPeriod_Click(null, null);
                        }

                        break;
                    }

                default:
                    break;
            }
        }

        public void Menu4()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            switch (vbPorterVar)
            {
                case "Main":
                    {
                        clsDRWrapper tempRS = new clsDRWrapper();

                        try
                        {
                            tempRS.OpenRecordset("SELECT * FROM Operators WHERE Code = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
                            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
                            {
                                tempRS.MoveLast();
                                tempRS.MoveFirst();
                                if (FCConvert.ToInt32(tempRS.Get_Fields_String("Level")) == 1 || FCConvert.ToInt32(tempRS.Get_Fields_String("Level")) == 2)
                                {
                                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                                    frmInventory.InstancePtr.Show(App.MainForm);
                                    FCGlobal.Screen.MousePointer = 0;
                                }
                                else
                                {
                                    MessageBox.Show("Your Operator Level is not high enough to access this option.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                                frmInventory.InstancePtr.Show(App.MainForm);
                                FCGlobal.Screen.MousePointer = 0;
                            }
                        }
                        finally
                        {
                            tempRS.DisposeOf();
                        }

                        break;
                    }

                case "File":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmEditTransferAnalysisReport.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmTellerReport.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Purge":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPurgeArchive.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;

                case "Custom":
                    {
                       
                        break;
                    }

                default:
                    break;
            }
        }

        public void Menu5()
        {

            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    {
                        clsDRWrapper rsUser = new clsDRWrapper();

                        try
                        {
                            rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
                            if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            else
                            {
                                if (MotorVehicle.Statics.OpID != "988")
                                {
                                    MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                        finally
                        {
                            rsUser.DisposeOf();
                        }
                        FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                        frmExceptionReportItems.InstancePtr.Show(App.MainForm);
                        FCGlobal.Screen.MousePointer = 0;

                        break;
                    }

                case "File":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmSpecialResCodes.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmVehicleClassList.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Purge":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPurgeReports.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                default:
                    break;
            }
        }

        public void Menu6()
        {
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;

            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            switch (vbPorterVar)
            {
                case "Main":

                    //GetReportingMenu();
                    break;
                case "File":
                    {
                        clsDRWrapper rsInfo = new clsDRWrapper();

                        try
                        {
                            ans = MessageBox.Show("Please select what you would like all your mail reminders set to.", "Set Mail Reminders", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                            if (ans == DialogResult.Yes)
                            {
                                rsInfo.Execute("UPDATE Master SET Mail = 1", "TWMV0000.vb1");
                            }
                            else if (ans == DialogResult.No)
                            {
                                rsInfo.Execute("UPDATE Master SET Mail = 0", "TWMV0000.vb1");
                            }
                            else
                            {
                                return;
                            }
                        }
                        finally
                        {
                            rsInfo.DisposeOf();
                        }
                        MessageBox.Show("Mail Reminders Set.", "Mail Reminders Set", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        break;
                    }

                case "Report":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmTransitPlateInformation.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Purge":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmPurgeAuditArchive.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                default:
                    break;
            }
        }

        public void Menu7()
        {
            object lngCode = 0;

            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            switch (vbPorterVar)
            {
                case "Main":
                    frmVehicleHistory.InstancePtr.Show(App.MainForm);

                    break;

                case "File":
                    {
                        if (frmInput.InstancePtr.Init(ref lngCode, "MV One Day Code", "Call TRIO for today's code", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true))
                        {
                            if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngCode), "MV"))
                            {
                                FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                                frmClearInactiveFlag.InstancePtr.Show(App.MainForm);
                                FCGlobal.Screen.MousePointer = 0;
                            }
                            else
                            {
                                MessageBox.Show("Invalid Code", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }

                        break;
                    }

                case "Report":
                    frmSetupExciseTaxEstimate.InstancePtr.Show(App.MainForm);

                    break;
                case "Purge":

                    //GetFileMenu();
                    break;
                default:
                    break;
            }
        }

        public void Menu8()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            switch (vbPorterVar)
            {
                case "Main":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmTableMaintenance.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "File":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmExport.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "Report":
                    frmLD79Estimate.InstancePtr.Show(App.MainForm);

                    break;
                default:
                    break;
            }
        }

        public void Menu9()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            switch (vbPorterVar)
            {
                case "Main":
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                    frmFleetMaster.InstancePtr.Show(App.MainForm);
                    FCGlobal.Screen.MousePointer = 0;

                    break;
                case "File":
                    rptMVR3Laser.InstancePtr.boolTest = true;
                    rptMVR3Laser.InstancePtr.PrintReportOnDotMatrix("MVR3PrinterName");
                    rptMVR3Laser.InstancePtr.Unload();

                    break;
                case "Report":
                    frmVehicleAddressList.InstancePtr.Show(App.MainForm);

                    break;
                default:
                    break;
            }
        }

        private void MDIForm_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if (e.CloseReason == FCCloseReason.FormControlMenu)
            {
                modBlockEntry.WriteYY();
                if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
                {
                    // Do Nothing
                }
                else
                {
                    //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                }

                FCUtils.CloseFormsOnProject();
            }
            Application.Exit();
        }

        private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWBD0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWCR0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuCentralParties_Click(object sender, System.EventArgs e)
        {
            frmCentralPartySearch.InstancePtr.Show(App.MainForm);
        }

        private void mnuClerkHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWCK0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWCE0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuCustomAddressOverride_Click(object sender, System.EventArgs e)
        {
            FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
            frmGroupAddressOverride.InstancePtr.Show(App.MainForm);
            FCGlobal.Screen.MousePointer = 0;
        }

        private void mnuCustomLongTermEndofPeriod_Click(object sender, System.EventArgs e)
        {
            FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
            frmReportLongTerm.InstancePtr.Show(App.MainForm);
            FCGlobal.Screen.MousePointer = 0;
        }

        private void mnuCustomLongTermImportCorrectionsBatch_Click(object sender, System.EventArgs e)
        {
            object[] lngIDs = null;
            // - "AutoDim"
            string strCurDir;
            string strFilePath = "";
            int lngCounter;
            string strBadPlates;
            string strLine = "";
            // vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
            DialogResult answer = 0;
            clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
            int lngFormCheck;
            clsDRWrapper rsFleet = new clsDRWrapper();
            clsDRWrapper rsLongTermActivityRecord = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                strCurDir = Environment.CurrentDirectory;
                fecherFoundation.Information.Err().Clear();

                // CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //- CommonDialog1.CancelError = true;
                /*? On Error Resume Next  */
                CommonDialog1.Filter = "*.*";
                CommonDialog1.FileName = "*.*";
                CommonDialog1.Show();

                if (fecherFoundation.Information.Err().Number == 0)
                {
                    strFilePath = CommonDialog1.FileName;
                }
                else
                {
                    FCFileSystem.ChDrive(strCurDir);
                    Environment.CurrentDirectory = strCurDir;

                    return;
                }

                FCFileSystem.ChDrive(strCurDir);
                Environment.CurrentDirectory = strCurDir;
                frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Importing Data...");
                MotorVehicle.Statics.lngRecords = 0;
                strBadPlates = "";
                lngIDs = new object[0 + 1];
                MotorVehicle.Statics.RegistrationType = "CORR";
                FCFileSystem.FileOpen(1, strFilePath, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);

                while (FCFileSystem.EOF(1) != true)
                {
                    strLine = FCFileSystem.LineInput(1);
                    MotorVehicle.Statics.rsFinal.OpenRecordset("SELECT * FROM Master WHERE PlateStripped = '" + fecherFoundation.Strings.Trim(strLine) + "'", "TWMV0000.vb1");

                    if (MotorVehicle.Statics.rsFinal.EndOfFile() != true && MotorVehicle.Statics.rsFinal.BeginningOfFile() != true)
                    {
                        if (rsFleet.RecordCount() == 0)
                        {
                            rsFleet.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = 0 AND Type = 'L' AND FleetNumber = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"), "TWMV0000.vb1");
                        }

                        MotorVehicle.Statics.rsFinal.Edit();
                        MotorVehicle.Statics.rsFinal.Set_Fields("Address", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_String("Address"))));
                        MotorVehicle.Statics.rsFinal.Set_Fields("City", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_String("City"))));
                        MotorVehicle.Statics.rsFinal.Set_Fields("State", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields("State"))));

                        if (!fecherFoundation.FCUtils.IsNull(rsFleet.Get_Fields_String("Zip")))
                        {
                            if (FCConvert.ToString(rsFleet.Get_Fields_String("Zip")).Length < 5)
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("Zip", "0" + rsFleet.Get_Fields_String("Zip"));
                            }
                            else
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("Zip", rsFleet.Get_Fields_String("Zip"));
                            }
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("Zip", "");
                        }

                        MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode1", "C");
                        MotorVehicle.Statics.rsFinal.Set_Fields("PartyID1", rsFleet.Get_Fields_Int32("PartyID1"));

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields("Owner2Name"))) != "")
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "C");
                            MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_Int32("PartyID2"))));
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "N");
                            MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", 0);
                        }

                        MotorVehicle.Statics.rsFinal.Update();
                        Array.Resize(ref lngIDs, MotorVehicle.Statics.lngRecords + 1);
                        lngIDs[MotorVehicle.Statics.lngRecords] = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.lngRecords += 1;
                    }
                    else
                    {
                        strBadPlates += fecherFoundation.Strings.Trim(strLine) + "\r\n";
                    }
                }

                MotorVehicle.Statics.gboolFromImport = true;

                if (MotorVehicle.Statics.lngRecords > 0)
                {
                    if (MotorVehicle.Statics.blnLongTermLaserForms)
                    {
                        rptMVRT10Laser.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                    }
                    else
                    {
                        frmWait.InstancePtr.Unload();

                        if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to the top as possible) next to the line titled:" + "\r\n" + "STATE OF MAINE", 23, 1, MotorVehicle.Statics.MVR10PrinterName) == DialogResult.Cancel)
                        {
                            return;
                        }
                        else
                        {
                            frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Importing Data...");

                            rptMVRT10.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                        }
                    }

                    if (!MotorVehicle.Statics.blnLongTermLaserForms)
                    {
                        rptMVRT10.InstancePtr.Unload();
                    }
                    else
                    {
                        rptMVRT10Laser.InstancePtr.Unload();
                    }

                    for (lngCounter = 0; lngCounter <= MotorVehicle.Statics.lngRecords - 1; lngCounter++)
                    {
                        MotorVehicle.Statics.rsFinal.OpenRecordset("SELECT * FROM Master WHERE ID = " + lngIDs[lngCounter], "TWMV0000.vb1");
                        clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
                        MotorVehicle.TransferRecordToArchive(ref temp, 0);
                        MotorVehicle.Statics.rsFinal = temp;
                        MotorVehicle.Statics.rsFinal.Edit();
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldClass", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", "ECO");
                        MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.OpID);
                        MotorVehicle.Statics.rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations");
                        MotorVehicle.Statics.rsAM.AddNew();
                        MotorVehicle.Statics.rsAM.Set_Fields("RegistrationType", "ECO");
                        MotorVehicle.Statics.rsAM.Set_Fields("DateUpdated", DateTime.Now);
                        MotorVehicle.Statics.rsAM.Set_Fields("TitleDone", false);
                        MotorVehicle.Statics.rsAM.Set_Fields("UseTaxDone", false);
                        MotorVehicle.Statics.rsAM.Set_Fields("InfoMessage", "ADDRESS CHANGE");
                        MotorVehicle.Statics.rsAM.Set_Fields("Make", MotorVehicle.Statics.rsFinal.Get_Fields_String("Make"));
                        MotorVehicle.Statics.rsAM.Set_Fields("Unit", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
                        MotorVehicle.Statics.rsAM.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
                        MotorVehicle.Statics.rsAM.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
                        MotorVehicle.Statics.rsAM.Set_Fields("Color", MotorVehicle.Statics.rsFinal.Get_Fields_String("Color1") + MotorVehicle.Statics.rsFinal.Get_Fields_String("Color2"));
                        MotorVehicle.Statics.rsAM.Set_Fields("MaineReregistration", false);
                        MotorVehicle.Statics.rsAM.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        MotorVehicle.Statics.rsAM.Set_Fields("VIN", MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN"));
                        MotorVehicle.Statics.rsAM.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
                        MotorVehicle.Statics.rsAM.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"))));
                        MotorVehicle.Statics.rsAM.Set_Fields("OwnerName", MotorVehicle.Statics.rsFinal.Get_Fields("Owner1"));
                        MotorVehicle.Statics.rsAM.Set_Fields("Address1", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
                        MotorVehicle.Statics.rsAM.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
                        MotorVehicle.Statics.rsAM.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
                        MotorVehicle.Statics.rsAM.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
                        MotorVehicle.Statics.rsAM.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate"));
                        MotorVehicle.Statics.rsAM.Set_Fields("FleetNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
                        MotorVehicle.Statics.rsAM.Set_Fields("LegalResAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
                        MotorVehicle.Statics.rsAM.Set_Fields("LegalResCity", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
                        MotorVehicle.Statics.rsAM.Set_Fields("LegalResState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
                        MotorVehicle.Statics.rsAM.Set_Fields("LegalResZip", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResZip"));
                        MotorVehicle.Statics.rsAM.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
                        MotorVehicle.Statics.rsAM.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
                        rsLongTermActivityRecord.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + MotorVehicle.Statics.rsFinal.Get_Fields("LongTermTrailerRegKey"));

                        if (rsLongTermActivityRecord.EndOfFile() != true && rsLongTermActivityRecord.BeginningOfFile() != true)
                        {
                            MotorVehicle.Statics.rsAM.Set_Fields("NumberOfYears", rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears"));
                        }

                        MotorVehicle.Statics.rsAM.Set_Fields("UniqueIdentifier", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("LongTermTrailerRegUniqueIdentifier"));
                        MotorVehicle.Statics.rsAM.Set_Fields("RegistrationFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"));
                        MotorVehicle.Statics.rsAM.Set_Fields("RegistrationCredit", 0);
                        MotorVehicle.Statics.rsAM.Set_Fields("TitleFee", 0);
                        MotorVehicle.Statics.rsAM.Set_Fields("RushTitleFee", 0);
                        MotorVehicle.Statics.rsAM.Set_Fields("SalesTax", 0);
                        MotorVehicle.Statics.rsAM.Set_Fields("StateFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"));
                        MotorVehicle.Statics.rsAM.Set_Fields("AgentFee", 0);
                        MotorVehicle.Statics.rsAM.Set_Fields("AgentFeeTitle", 0);
                        MotorVehicle.Statics.rsAM.Set_Fields("TransferFee", 0);
                        MotorVehicle.Statics.rsAM.Set_Fields("Status", "A");
                        MotorVehicle.Statics.rsAM.Set_Fields("TimePayment", false);
                        MotorVehicle.Statics.rsAM.Set_Fields("TwentyFiveYearPlate", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwentyFiveYearPlate"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", MotorVehicle.Statics.rsAM.Get_Fields_DateTime("DateUpdated"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveKey", MotorVehicle.Statics.FinalCompareID);
                        MotorVehicle.Statics.rsAM.Update();
                        MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegID", MotorVehicle.Statics.rsAM.Get_Fields_Int32("ID"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
                        MotorVehicle.Statics.rsFinal.Update();
                    }

                    frmWait.InstancePtr.Unload();

                    //App.DoEvents();
                    MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + "Registrations have been processed.", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    frmWait.InstancePtr.Unload();

                    //App.DoEvents();
                    MessageBox.Show("No registrations were found to import.", "No Registrations", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                MotorVehicle.Statics.gboolFromImport = false;

                if (fecherFoundation.Strings.Trim(strBadPlates) != "")
                {
                    rptUnprocessedPlates.InstancePtr.strBadPlates = strBadPlates;
                    frmReportViewer.InstancePtr.Init(rptUnprocessedPlates.InstancePtr);
                }

                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MotorVehicle.Statics.gboolFromImport = false;
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsLongTermActivityRecord.DisposeOf();
                rsFleet.DisposeOf();
            }
        }

        private void mnuCustomPrintQueue_Click(object sender, System.EventArgs e)
        {
            FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
            frmPrintQueue.InstancePtr.Show(App.MainForm);
            FCGlobal.Screen.MousePointer = 0;
        }

        private void mnuFExit_Click(object sender, System.EventArgs e)
        {

            modBlockEntry.WriteYY();
            Application.Exit();
        }


       

        public void GetMainMenu(bool addExitMenu = false)
        {
            bool boolDisable = false;
            string strTemp = "";
            //ClearLabels();
            string menu = "Main";
            string imageKey = "";
            App.MainForm.Caption = "MOTOR VEHICLE";
            App.MainForm.ApplicationIcon = "icon-motor-vehicle-registration";
            //GRID.Tag = (System.Object)("Main");
            // set the caption for the current menu labels
            int menuItemsCount = addExitMenu ? 18 : 17;
            for (int CurRow = 1; CurRow <= menuItemsCount; CurRow++)
            {
                strTemp = "";
                boolDisable = false;
                switch (CurRow)
                {
                    case 1:
                        {
                            strTemp = "Registration Menu";
                            imageKey = "registration-menu";
                            lngFCode = 0;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Process End of Period";
                            imageKey = "daily-audit-report";
                            lngFCode = MotorVehicle.ENDOFPERIOD;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Process BMV Update File";
                            imageKey = "load-of-back-information";
                            lngFCode = MotorVehicle.BMVUPDATEDISK;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Inventory Maintenance";
                            imageKey = "inventory-maintenance";
                            lngFCode = MotorVehicle.INVENTORYMAINTENANCE;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Exception Report Items";
                            imageKey = "exception-report-items";
                            lngFCode = MotorVehicle.ExceptionReport;
                            break;
                        }
                    case 6:
                        {
                            strTemp = "Printing";
                            imageKey = "printing";
                            lngFCode = MotorVehicle.REPORTING;
                            break;
                        }
                    case 7:
                        {
                            strTemp = "Vehicle History";
                            imageKey = "vehicle-history";
                            lngFCode = 0;
                            break;
                        }
                    case 8:
                        {
                            strTemp = "Table / Option Processing";
                            imageKey = "table-file-setup";
                            lngFCode = MotorVehicle.TABLEPROCESSING;
                            break;
                        }
                    case 9:
                        {
                            strTemp = "Fleet Master Add / Update";
                            imageKey = "extract-remote-reader";
                            lngFCode = MotorVehicle.FLEETMASTER;
                            break;
                        }
                    case 10:
                        {
                            strTemp = "Blue Book";
                            imageKey = "blue-book";
                            lngFCode = MotorVehicle.REDBOOK; 
                            break;
                        }
                    case 11:
                        {
                            strTemp = "Inventory Status";
                            imageKey = "inventory-status";
                            lngFCode = MotorVehicle.INVENTORYSTATUS;
                            break;
                        }
                    case 12:
                        {
                            strTemp = "Rapid Renewal";
                            imageKey = "rapid-renewal";
                            if (!MotorVehicle.Statics.AllowRapidRenewal)
                            {
                                lngFCode = 999999;
                            }
                            else
                            {
                                lngFCode = MotorVehicle.RAPIDRENEWAL;
                            }
                            break;
                        }
                    case 13:
                        {
                            strTemp = "New To System / Update";
                            imageKey = "new-to-system-update";
                            lngFCode = MotorVehicle.VEHICLEUPDATE;
                            break;
                        }
                    case 14:
                        {
                            strTemp = "Teller Closeout";
                            imageKey = "teller-closeout";
                            if (!MotorVehicle.Statics.blnUseTellerCloseout)
                            {
                                lngFCode = 999999;
                            }
                            else
                            {
                                lngFCode = MotorVehicle.TELLERCLOSEOUT;
                            }
                            break;
                        }
                    case 15:
                        {
                            strTemp = "Gift Certificates";
                            imageKey = "gift-certificates";
                            lngFCode = 0;
                            break;
                        }
                    case 16:
                        {
                            strTemp = "File Maintenance";
                            imageKey = "file-maintenance";
                            lngFCode = MotorVehicle.FILEMAINTENANCE;
                            break;
                        }
                    case 17:
                        {
                            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
                            {
                                strTemp = "Custom Programs";
                                imageKey = "street-maintenance";
                                lngFCode = 0;
                            }
                            break;
                        }
                    case 18:
                        {
                            strTemp = "Exit Motor Vehicle";
                            lngFCode = 0;
                            break;
                        }

                }
                //GRID.TextMatrix((CurRow), 1, strTemp);
                if (lngFCode != 0 && lngFCode != 999999 && MotorVehicle.Statics.OpID != "988" && ((MotorVehicle.Statics.bolFromDosTrio && MotorVehicle.Statics.OpID != "") || !MotorVehicle.Statics.bolFromDosTrio))
                {
                    boolDisable = !modSecurity.ValidPermissions(null, lngFCode, false);
                }
                else if (lngFCode == 999999)
                {
                    boolDisable = true;
                }
                if (strTemp != "")
                {

                    FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 1, imageKey);
                    switch (CurRow)
                    {
                        case 6:
                            GetReportingMenu(newItem);
                            break;
                        case 16:
                            GetFileMenu(newItem);
                            break;
                        case 17:
                            GetCustomMenu(newItem);
                            break;
                    }
                }
            }
            // set the int array so that the alpha characters can be mapped to numbers
            //AutoSizeMenu(ref GRID, 20);
            App.MainForm.Text = "TRIO Software - Motor Vehicle [Main]";
        }

        private void GetCustomMenu(FCMenuItem parent)
        {
            string strTemp = "";
            bool add = false;
            string menu = "Custom";
            for (int CurRow = 1; CurRow <= 4; CurRow++)
            {
                add = false;
                switch (CurRow)
                {
                    case 1:
                        {
                            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT")
                            {
                                strTemp = "Group Address Override";
                                add = true;
                            }
                            else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER")
                            {
                                strTemp = "Process End of Period - Long Term";
                                add = true;
                            }
                            else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
                            {
                                strTemp = "Print Queue";
                                add = true;
                            }
                            break;
                        }
                    case 2:
                        {
                            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT")
                            {
                                strTemp = "Print Queue";
                                add = true;
                            }
                            else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION" ||
                                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER")
                            {
                                strTemp = "Process End of Period - Long Term";
                                add = true;
                            }
                            break;
                        }
                    case 3:
                        {
                            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT")
                            {
                                strTemp = "Process End of Period - Long Term";
                                add = true;
                            }
                            break;
                        }
                    case 4:
                        {
                            
                            break;
                        }
                }
                if (add)
                {
                    FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + CurRow, menu, true, 2);
                }
            }
        }

        private void GetFileMenu(FCMenuItem parent)
        {
            string strTemp = "";
            string menu = "File";
            bool boolDisable = false;
            string imageKey = "";

            for (int CurRow = 1; CurRow <= 9; CurRow++)
            {
                boolDisable = false;
                switch (CurRow)
                {
                    case 1:
                        {
                            strTemp = "Customize";
                            lngFCode = MotorVehicle.MOTORVEHICLESETTINGS;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Check Database Structure";
                            lngFCode = 0;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Purge";
                            lngFCode = MotorVehicle.PURGE;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Transfer Analysis Comment";
                            lngFCode = 0;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Special Res Code Setup";
                            lngFCode = 0;
                            break;
                        }
                    case 6:
                        {
                            strTemp = "Set All Mail Reminders";
                            lngFCode = 0;
                            break;
                        }
                    case 7:
                        {
                            strTemp = "Clear Inactive Flag";
                            lngFCode = 0;
                            break;
                        }
                    case 8:
                        {
                            strTemp = "Data Export";
                            lngFCode = 0;
                            break;
                        }
                    case 9:
                        {
                            strTemp = "Test MVR-3E (Laser)";
                            lngFCode = 0;
                            break;
                        }
                }
                //GRID.TextMatrix((CurRow), 1, strTemp);
                if (lngFCode != 0 && MotorVehicle.Statics.OpID != "988" && MotorVehicle.Statics.OpID != "")
                {
                    boolDisable = !modSecurity.ValidPermissions(null, lngFCode, false);
                }
                parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
                switch (CurRow)
                {
                    case 3:
                        GetPurgeMenu(parent);
                        break;
                }
            }
            //AutoSizeMenu(ref GRID, 20);
            App.MainForm.Text = "TRIO Software - Motor Vehicle [File Maintenance]";
        }

        private void GetReportingMenu(FCMenuItem parent)
        {

            bool boolDisable = false;
            //GRID.Tag = (System.Object)("Report");
            string menu = "Report";
            string strTemp = "";
            for (int CurRow = 1; CurRow <= 14; CurRow++)
            {
                boolDisable = false;
                switch (CurRow)
                {
                    case 1:
                        {
                            strTemp = "Print VIN List";
                            lngFCode = MotorVehicle.VINLIST;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Fleet Registration List";
                            lngFCode = MotorVehicle.FLEETLIST;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Reminder Notices";
                            lngFCode = MotorVehicle.REMINDERS;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Teller Report";
                            lngFCode = MotorVehicle.TELLERREPORT;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Vehicle Class List";
                            lngFCode = 0;
                            break;
                        }
                    case 6:
                        {
                            strTemp = "Transit Plate Information";
                            lngFCode = 0;
                            break;
                        }
                    case 7:
                        {
                            strTemp = "Excise Tax Report";
                            lngFCode = 0;
                            break;
                        }
                    case 8:
                        {
                            strTemp = "Excise Rate Comparison";
                            lngFCode = 0;
                            break;
                        }
                    case 9:
                        {
                            strTemp = "Vehicle Address List";
                            lngFCode = 0;
                            break;
                        }
                    case 10:
                        {
                            strTemp = "Teller Closeout Audit Report";
                            lngFCode = !MotorVehicle.Statics.blnUseTellerCloseout ? 999999 : 0;
                            break;
                        }
                    case 11:
                        {
                            strTemp = "Fleet / Group Labels";
                            lngFCode = 0;
                            break;
                        }
                    case 12:
                        {
                            strTemp = "Fleet Reminder List";
                            lngFCode = 0;
                            break;
                        }
                    case 13:
                        {
                            strTemp = "Audit Report";
                            lngFCode = MotorVehicle.AUDITREPORT;
                            break;
                        }
                    case 14:
                        {
                            strTemp = "Audit Archive Report";
                            lngFCode = MotorVehicle.AUDITARCHIVEREPORT;
                            break;
                        }
                }

                if (lngFCode != 0 && MotorVehicle.Statics.OpID != "988" && MotorVehicle.Statics.OpID != "")
                {
                    boolDisable = !modSecurity.ValidPermissions(null, lngFCode, false);
                }
                else if (lngFCode == 999999)
                {
                    boolDisable = true;
                }
                parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
            }
            //AutoSizeMenu(ref GRID, 20);
            App.MainForm.Text = "TRIO Software - Motor Vehicle [Printing]";
        }

        private void GetPurgeMenu(FCMenuItem parent)
        {

            string strTemp = "";
            bool boolDisable = false;
            string menu = "Purge";
            for (int CurRow = 1; CurRow <= 6; CurRow++)
            {
                boolDisable = false;
                switch (CurRow)
                {
                    case 1:
                        {
                            strTemp = "Purge Held Registrations";
                            lngFCode = 0;
                            break;
                        }
                    case 2:
                        {
                            strTemp = "Purge Deleted Registrations";
                            lngFCode = 0;
                            break;
                        }
                    case 3:
                        {
                            strTemp = "Purge Expired Registrations";
                            lngFCode = 0;
                            break;
                        }
                    case 4:
                        {
                            strTemp = "Purge Archive Information";
                            lngFCode = 0;
                            break;
                        }
                    case 5:
                        {
                            strTemp = "Purge Reporting Information";
                            lngFCode = 0;
                            break;
                        }
                    case 6:
                        {
                            strTemp = "Purge Audit Information";
                            lngFCode = 0;
                            break;
                        }
                }
                if (lngFCode != 0 && MotorVehicle.Statics.OpID != "988" && MotorVehicle.Statics.OpID != "")
                {
                    boolDisable = !modSecurity.ValidPermissions(null, lngFCode, false);
                }
                parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 3);
            }
            LabelNumber[Strings.Asc("1")] = 1;
            LabelNumber[Strings.Asc("2")] = 2;
            LabelNumber[Strings.Asc("3")] = 3;
            LabelNumber[Strings.Asc("4")] = 4;
            LabelNumber[Strings.Asc("5")] = 5;
            LabelNumber[Strings.Asc("6")] = 6;
            LabelNumber[Strings.Asc("X")] = 7;
            App.MainForm.Text = "TRIO Software - Motor Vehicle [Purge]";
        }

        // vbPorter upgrade warning: Grid As vsFlexGrid	OnWrite(FCGridCtl.vsFlexGrid)
        public void AutoSizeMenu(ref FCGrid Grid, int intLabels)
        {
            int i;
            // vbPorter upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
            int FullRowHeight = 0;
            // this will store the height of the labels with text
            // .RowHeight(0) = 0                   'the first row in the grid is height = 0
            FullRowHeight = FCConvert.ToInt32(Grid.HeightOriginal / intLabels);
            // find the average height of the row
            for (i = 1; i <= 20; i++)
            {
                // this just assigns the row height
                Grid.RowHeight(i, FullRowHeight);
            }
            Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void DisableMenuOption(ref bool boolState, int intRow)
        {
            boolState = !boolState;
            //GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, modGlobalFunctions.SetMenuOptionColor(ref boolState));
        }

        private void DisableMenuOption2(bool boolState, int intRow)
        {
            DisableMenuOption(ref boolState, intRow);
        }

        private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWPY0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWPP0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWRE0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuRedBookHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWRB0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWTB0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWTC0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWUT0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
        {
            string strDir;
            strDir = App.Path;
            if (Strings.Right(strDir, 1) != "\\")
            {
                strDir += "\\";
            }
            //modGlobalFunctions.HelpShell("TWVR0000.HLP", AppWinStyle.MaximizedFocus, strDir);
        }

        private void CDBS()
        {
            cMotorVehicleDB mDB = new cMotorVehicleDB();
            cVersionInfo tVer;
            if (mDB.CheckVersion())
            {
                MessageBox.Show("Database Structure Check completed successfully.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error Number " + FCConvert.ToString(mDB.LastErrorNumber) + "  " + mDB.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            tVer = mDB.GetVersion();
            MessageBox.Show("Current DB Version is " + tVer.VersionString);
            // kk12022016 tromv-1194  Always run a check of the Residence (geocodes) table
            MotorVehicle.UpdateResidenceTable();
        }
    }
}
