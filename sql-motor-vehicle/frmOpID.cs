//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmOpID : BaseForm
	{
		public frmOpID()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmOpID InstancePtr
		{
			get
			{
				return (frmOpID)Sys.GetInstance(typeof(frmOpID));
			}
		}

		protected frmOpID _InstancePtr = null;
		//=========================================================
		string strOPID = "";
		public bool blnAgentCheck;
		string strAgent = "";

		private void frmOpID_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsAgent = new clsDRWrapper();
			rsAgent.OpenRecordset("SELECT * FROM Operators WHERE Level = '1'", "SystemSettings");
			if (rsAgent.EndOfFile() != true && rsAgent.BeginningOfFile() != true)
			{
				strAgent = fecherFoundation.Strings.UCase(FCConvert.ToString(rsAgent.Get_Fields("Code")));
			}
			if (MotorVehicle.Statics.OpID != "")
			{
				blnAgentCheck = true;
				Label1.Text = "Please enter the Primary Agent's Operator ID";
			}
			else
			{
				blnAgentCheck = false;
			}
			txtOpId.Focus();
		}

		private void frmOpID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			MotorVehicle.Statics.OpID = fecherFoundation.Strings.Trim(txtOpId.Text);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (!blnAgentCheck)
				{
					modBlockEntry.WriteYY();
					if (MotorVehicle.Statics.bolFromDosTrio == false && MotorVehicle.Statics.bolFromWindowsCR == false)
					{
                        //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                        //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
                        App.MainForm.OpenModule("TWGNENTY");
						Close();
					}
					else
					{
						Close();
					}
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				MotorVehicle.Statics.OpID = strOPID;
				if (MotorVehicle.Statics.OpID.Length != 3)
				{
                    MotorVehicle.Statics.OpID = "";
                    MessageBox.Show("Operator ID must be 3 characters in length.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtOpId.Text = "";
					txtOpId.Focus();
				}
				else
				{
					if (blnAgentCheck)
					{
						if (fecherFoundation.Strings.UCase(MotorVehicle.Statics.OpID) != strAgent)
						{
							MessageBox.Show("This is not the Primary Agent's Operator ID.", "Invalid Agent Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							// & vbNewLine & vbNewLine & "Correct Agent ID:  " & strAgent & vbNewLine & "You Typed:            " & UCase(OpID)
							strOPID = "";
							MotorVehicle.Statics.OpID = "";
							txtOpId.Text = "";
							txtOpId.Focus();
							return;
						}
					}
					Close();
				}
				strOPID = "";
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmOpID_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetTRIOColors(this, false);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
		}

		private void txtOpId_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			else if (KeyAscii == Keys.Back)
			{
				if (strOPID.Length > 1)
				{
					strOPID = Strings.Mid(strOPID, 1, strOPID.Length - 1);
				}
				else if (strOPID.Length == 1)
				{
					strOPID = "";
				}
			}
			else
			{
				if (strOPID.Length >= 3)
				{
					// Do Nothing
				}
				else
				{
					strOPID += FCConvert.ToString(Convert.ToChar(KeyAscii));
					KeyAscii = Keys.Separator;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
