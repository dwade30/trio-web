﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTransferAnalysisReport.
	/// </summary>
	partial class rptTransferAnalysisReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTransferAnalysisReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldExciseFirstYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseSecondYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldRegistrationFirstYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistrationCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistrationSecondYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistrationTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRegistrationTransfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldLocalFirstYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocalSecondYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTransferExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferRegistration = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegRegistration = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRegType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTransferMonths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferPerMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferPayToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferPayLater = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegMonths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegPerMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegPayToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegPayLater = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTransferCommentLine = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseFirstYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseSecondYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationFirstYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationSecondYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalFirstYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalSecondYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferRegistration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegRegistration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferMonths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferPerMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferPayToday)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferPayLater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegMonths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegPerMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegPayToday)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegPayLater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label36,
				this.Label37,
				this.Line1,
				this.Label38,
				this.Line2,
				this.Label39,
				this.Line3,
				this.Label40,
				this.Label41,
				this.Label42,
				this.Label43,
				this.Line4,
				this.fldExciseFirstYear,
				this.fldExciseCredit,
				this.fldExciseSecondYear,
				this.fldExciseTotal,
				this.Label44,
				this.Label45,
				this.Label46,
				this.Label47,
				this.Line5,
				this.fldRegistrationFirstYear,
				this.fldRegistrationCredit,
				this.fldRegistrationSecondYear,
				this.fldRegistrationTotal,
				this.Label48,
				this.fldRegistrationTransfer,
				this.Label49,
				this.Label51,
				this.Label52,
				this.Line6,
				this.fldLocalFirstYear,
				this.fldLocalSecondYear,
				this.fldLocalTotal,
				this.Label53,
				this.Label54,
				this.Label55,
				this.Label56,
				this.Label57,
				this.Line7,
				this.fldTransferExcise,
				this.fldTransferRegistration,
				this.fldTransferLocal,
				this.fldTransferTotal,
				this.fldRegExcise,
				this.fldRegRegistration,
				this.fldRegLocal,
				this.fldRegTotal,
				this.Label58,
				this.lblRegType,
				this.Line8,
				this.Line9,
				this.Label59,
				this.Label60,
				this.Label61,
				this.Label62,
				this.fldTransferMonths,
				this.fldTransferPerMonth,
				this.fldTransferPayToday,
				this.fldTransferPayLater,
				this.fldRegMonths,
				this.fldRegPerMonth,
				this.fldRegPayToday,
				this.fldRegPayLater,
				this.txtTransferCommentLine
			});
			this.Detail.Height = 4.875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label34,
				this.Label9,
				this.Label10,
				this.Label35
			});
			this.PageHeader.Height = 0.5104167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.53125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Courier New\'; font-size: 12pt; font-weight: bold; text-align: cente" + "r; ddo-char-set: 1";
			this.Label1.Text = "Motor Vehicle - Transfer Analysis";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.90625F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.03125F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Label34.Text = "Label34";
			this.Label34.Top = 0.1875F;
			this.Label34.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.03125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Courier New\'; ddo-char-set: 1";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.4375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 5.4375F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label35.Text = "Label35";
			this.Label35.Top = 0F;
			this.Label35.Width = 1.3125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.9166667F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: center; ddo-char-set: " + "1";
			this.Label36.Text = "----- T R A N S F E R   I N F O R M A T I O N -----";
			this.Label36.Top = 0F;
			this.Label36.Width = 4.875F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 1F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: center; ddo-char-set: " + "1";
			this.Label37.Text = "Excise Tax";
			this.Label37.Top = 0.34375F;
			this.Label37.Width = 1.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.96875F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.53125F;
			this.Line1.Width = 1.375F;
			this.Line1.X1 = 0.96875F;
			this.Line1.X2 = 2.34375F;
			this.Line1.Y1 = 0.53125F;
			this.Line1.Y2 = 0.53125F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 2.6875F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: center; ddo-char-set: " + "1";
			this.Label38.Text = "Registration Fee";
			this.Label38.Top = 0.34375F;
			this.Label38.Width = 1.6875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.84375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.53125F;
			this.Line2.Width = 1.375F;
			this.Line2.X1 = 2.84375F;
			this.Line2.X2 = 4.21875F;
			this.Line2.Y1 = 0.53125F;
			this.Line2.Y2 = 0.53125F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 4.75F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: center; ddo-char-set: " + "1";
			this.Label39.Text = "Local Fee";
			this.Label39.Top = 0.34375F;
			this.Label39.Width = 1.3125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.71875F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.53125F;
			this.Line3.Width = 1.375F;
			this.Line3.X1 = 4.71875F;
			this.Line3.X2 = 6.09375F;
			this.Line3.Y1 = 0.53125F;
			this.Line3.Y2 = 0.53125F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 0.8125F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label40.Text = "1st Year";
			this.Label40.Top = 0.5625F;
			this.Label40.Width = 0.8125F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1875F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.8125F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label41.Text = "Credit";
			this.Label41.Top = 0.75F;
			this.Label41.Width = 0.8125F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0.8125F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label42.Text = "2nd Year";
			this.Label42.Top = 0.9375F;
			this.Label42.Width = 0.8125F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 0.8125F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label43.Text = "Total";
			this.Label43.Top = 1.15625F;
			this.Label43.Width = 0.8125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.8125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.125F;
			this.Line4.Width = 1.6875F;
			this.Line4.X1 = 0.8125F;
			this.Line4.X2 = 2.5F;
			this.Line4.Y1 = 1.125F;
			this.Line4.Y2 = 1.125F;
			// 
			// fldExciseFirstYear
			// 
			this.fldExciseFirstYear.Height = 0.1875F;
			this.fldExciseFirstYear.Left = 1.6875F;
			this.fldExciseFirstYear.Name = "fldExciseFirstYear";
			this.fldExciseFirstYear.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldExciseFirstYear.Text = "Field1";
			this.fldExciseFirstYear.Top = 0.5625F;
			this.fldExciseFirstYear.Width = 0.78125F;
			// 
			// fldExciseCredit
			// 
			this.fldExciseCredit.Height = 0.1875F;
			this.fldExciseCredit.Left = 1.6875F;
			this.fldExciseCredit.Name = "fldExciseCredit";
			this.fldExciseCredit.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldExciseCredit.Text = "Field1";
			this.fldExciseCredit.Top = 0.75F;
			this.fldExciseCredit.Width = 0.78125F;
			// 
			// fldExciseSecondYear
			// 
			this.fldExciseSecondYear.Height = 0.1875F;
			this.fldExciseSecondYear.Left = 1.6875F;
			this.fldExciseSecondYear.Name = "fldExciseSecondYear";
			this.fldExciseSecondYear.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldExciseSecondYear.Text = "Field1";
			this.fldExciseSecondYear.Top = 0.9375F;
			this.fldExciseSecondYear.Width = 0.78125F;
			// 
			// fldExciseTotal
			// 
			this.fldExciseTotal.Height = 0.1875F;
			this.fldExciseTotal.Left = 1.6875F;
			this.fldExciseTotal.Name = "fldExciseTotal";
			this.fldExciseTotal.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldExciseTotal.Text = "Field1";
			this.fldExciseTotal.Top = 1.15625F;
			this.fldExciseTotal.Width = 0.78125F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 2.6875F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label44.Text = "1st Year";
			this.Label44.Top = 0.5625F;
			this.Label44.Width = 0.8125F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 2.6875F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label45.Text = "Credit";
			this.Label45.Top = 0.75F;
			this.Label45.Width = 0.8125F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 2.6875F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label46.Text = "2nd Year";
			this.Label46.Top = 1.125F;
			this.Label46.Width = 0.8125F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 2.6875F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label47.Text = "Total";
			this.Label47.Top = 1.34375F;
			this.Label47.Width = 0.8125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 2.6875F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.3125F;
			this.Line5.Width = 1.6875F;
			this.Line5.X1 = 2.6875F;
			this.Line5.X2 = 4.375F;
			this.Line5.Y1 = 1.3125F;
			this.Line5.Y2 = 1.3125F;
			// 
			// fldRegistrationFirstYear
			// 
			this.fldRegistrationFirstYear.Height = 0.1875F;
			this.fldRegistrationFirstYear.Left = 3.5625F;
			this.fldRegistrationFirstYear.Name = "fldRegistrationFirstYear";
			this.fldRegistrationFirstYear.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegistrationFirstYear.Text = "Field1";
			this.fldRegistrationFirstYear.Top = 0.5625F;
			this.fldRegistrationFirstYear.Width = 0.78125F;
			// 
			// fldRegistrationCredit
			// 
			this.fldRegistrationCredit.Height = 0.1875F;
			this.fldRegistrationCredit.Left = 3.5625F;
			this.fldRegistrationCredit.Name = "fldRegistrationCredit";
			this.fldRegistrationCredit.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegistrationCredit.Text = "Field1";
			this.fldRegistrationCredit.Top = 0.75F;
			this.fldRegistrationCredit.Width = 0.78125F;
			// 
			// fldRegistrationSecondYear
			// 
			this.fldRegistrationSecondYear.Height = 0.1875F;
			this.fldRegistrationSecondYear.Left = 3.5625F;
			this.fldRegistrationSecondYear.Name = "fldRegistrationSecondYear";
			this.fldRegistrationSecondYear.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegistrationSecondYear.Text = "Field1";
			this.fldRegistrationSecondYear.Top = 1.125F;
			this.fldRegistrationSecondYear.Width = 0.78125F;
			// 
			// fldRegistrationTotal
			// 
			this.fldRegistrationTotal.Height = 0.1875F;
			this.fldRegistrationTotal.Left = 3.5625F;
			this.fldRegistrationTotal.Name = "fldRegistrationTotal";
			this.fldRegistrationTotal.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegistrationTotal.Text = "Field1";
			this.fldRegistrationTotal.Top = 1.34375F;
			this.fldRegistrationTotal.Width = 0.78125F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 2.6875F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label48.Text = "Transfer";
			this.Label48.Top = 0.9375F;
			this.Label48.Width = 0.8125F;
			// 
			// fldRegistrationTransfer
			// 
			this.fldRegistrationTransfer.Height = 0.1875F;
			this.fldRegistrationTransfer.Left = 3.5625F;
			this.fldRegistrationTransfer.Name = "fldRegistrationTransfer";
			this.fldRegistrationTransfer.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegistrationTransfer.Text = "Field1";
			this.fldRegistrationTransfer.Top = 0.9375F;
			this.fldRegistrationTransfer.Width = 0.78125F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1875F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 4.59375F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label49.Text = "1st Year";
			this.Label49.Top = 0.5625F;
			this.Label49.Width = 0.8125F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 4.59375F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label51.Text = "2nd Year";
			this.Label51.Top = 0.75F;
			this.Label51.Width = 0.8125F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 4.59375F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label52.Text = "Total";
			this.Label52.Top = 0.96875F;
			this.Label52.Width = 0.8125F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 4.59375F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 0.9375F;
			this.Line6.Width = 1.6875F;
			this.Line6.X1 = 4.59375F;
			this.Line6.X2 = 6.28125F;
			this.Line6.Y1 = 0.9375F;
			this.Line6.Y2 = 0.9375F;
			// 
			// fldLocalFirstYear
			// 
			this.fldLocalFirstYear.Height = 0.1875F;
			this.fldLocalFirstYear.Left = 5.46875F;
			this.fldLocalFirstYear.Name = "fldLocalFirstYear";
			this.fldLocalFirstYear.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldLocalFirstYear.Text = "Field1";
			this.fldLocalFirstYear.Top = 0.5625F;
			this.fldLocalFirstYear.Width = 0.78125F;
			// 
			// fldLocalSecondYear
			// 
			this.fldLocalSecondYear.Height = 0.1875F;
			this.fldLocalSecondYear.Left = 5.46875F;
			this.fldLocalSecondYear.Name = "fldLocalSecondYear";
			this.fldLocalSecondYear.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldLocalSecondYear.Text = "Field1";
			this.fldLocalSecondYear.Top = 0.75F;
			this.fldLocalSecondYear.Width = 0.78125F;
			// 
			// fldLocalTotal
			// 
			this.fldLocalTotal.Height = 0.1875F;
			this.fldLocalTotal.Left = 5.46875F;
			this.fldLocalTotal.Name = "fldLocalTotal";
			this.fldLocalTotal.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldLocalTotal.Text = "Field1";
			this.fldLocalTotal.Top = 0.96875F;
			this.fldLocalTotal.Width = 0.78125F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 0.90625F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: center; ddo-char-set: " + "1";
			this.Label53.Text = "----- S U M M A R Y -----";
			this.Label53.Top = 1.90625F;
			this.Label53.Width = 4.875F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1875F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 1.8125F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label54.Text = "Excise";
			this.Label54.Top = 2.5F;
			this.Label54.Width = 0.8125F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1875F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 1.8125F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label55.Text = "Registration";
			this.Label55.Top = 2.6875F;
			this.Label55.Width = 1.125F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1875F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 1.8125F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label56.Text = "Local Fee";
			this.Label56.Top = 2.875F;
			this.Label56.Width = 0.8125F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1875F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 1.8125F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label57.Text = "Total";
			this.Label57.Top = 3.09375F;
			this.Label57.Width = 0.8125F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 1.78125F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 3.0625F;
			this.Line7.Width = 3.03125F;
			this.Line7.X1 = 1.78125F;
			this.Line7.X2 = 4.8125F;
			this.Line7.Y1 = 3.0625F;
			this.Line7.Y2 = 3.0625F;
			// 
			// fldTransferExcise
			// 
			this.fldTransferExcise.Height = 0.1875F;
			this.fldTransferExcise.Left = 3.0625F;
			this.fldTransferExcise.Name = "fldTransferExcise";
			this.fldTransferExcise.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldTransferExcise.Text = "Field1";
			this.fldTransferExcise.Top = 2.5F;
			this.fldTransferExcise.Width = 0.78125F;
			// 
			// fldTransferRegistration
			// 
			this.fldTransferRegistration.Height = 0.1875F;
			this.fldTransferRegistration.Left = 3.0625F;
			this.fldTransferRegistration.Name = "fldTransferRegistration";
			this.fldTransferRegistration.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldTransferRegistration.Text = "Field1";
			this.fldTransferRegistration.Top = 2.6875F;
			this.fldTransferRegistration.Width = 0.78125F;
			// 
			// fldTransferLocal
			// 
			this.fldTransferLocal.Height = 0.1875F;
			this.fldTransferLocal.Left = 3.0625F;
			this.fldTransferLocal.Name = "fldTransferLocal";
			this.fldTransferLocal.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldTransferLocal.Text = "Field1";
			this.fldTransferLocal.Top = 2.875F;
			this.fldTransferLocal.Width = 0.78125F;
			// 
			// fldTransferTotal
			// 
			this.fldTransferTotal.Height = 0.1875F;
			this.fldTransferTotal.Left = 3.0625F;
			this.fldTransferTotal.Name = "fldTransferTotal";
			this.fldTransferTotal.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldTransferTotal.Text = "Field1";
			this.fldTransferTotal.Top = 3.09375F;
			this.fldTransferTotal.Width = 0.78125F;
			// 
			// fldRegExcise
			// 
			this.fldRegExcise.Height = 0.1875F;
			this.fldRegExcise.Left = 4.03125F;
			this.fldRegExcise.Name = "fldRegExcise";
			this.fldRegExcise.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegExcise.Text = "Field5";
			this.fldRegExcise.Top = 2.5F;
			this.fldRegExcise.Width = 0.78125F;
			// 
			// fldRegRegistration
			// 
			this.fldRegRegistration.Height = 0.1875F;
			this.fldRegRegistration.Left = 4.03125F;
			this.fldRegRegistration.Name = "fldRegRegistration";
			this.fldRegRegistration.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegRegistration.Text = "Field1";
			this.fldRegRegistration.Top = 2.6875F;
			this.fldRegRegistration.Width = 0.78125F;
			// 
			// fldRegLocal
			// 
			this.fldRegLocal.Height = 0.1875F;
			this.fldRegLocal.Left = 4.03125F;
			this.fldRegLocal.Name = "fldRegLocal";
			this.fldRegLocal.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegLocal.Text = "Field1";
			this.fldRegLocal.Top = 2.875F;
			this.fldRegLocal.Width = 0.78125F;
			// 
			// fldRegTotal
			// 
			this.fldRegTotal.Height = 0.1875F;
			this.fldRegTotal.Left = 4.03125F;
			this.fldRegTotal.Name = "fldRegTotal";
			this.fldRegTotal.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegTotal.Text = "Field1";
			this.fldRegTotal.Top = 3.09375F;
			this.fldRegTotal.Width = 0.78125F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 3F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right; ddo-char-set: 1" + "";
			this.Label58.Text = "Transfer";
			this.Label58.Top = 2.28125F;
			this.Label58.Width = 0.8125F;
			// 
			// lblRegType
			// 
			this.lblRegType.Height = 0.1875F;
			this.lblRegType.HyperLink = null;
			this.lblRegType.Left = 3.96875F;
			this.lblRegType.Name = "lblRegType";
			this.lblRegType.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right; ddo-char-set: 1" + "";
			this.lblRegType.Text = "New Reg";
			this.lblRegType.Top = 2.28125F;
			this.lblRegType.Width = 0.8125F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 3.03125F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 2.46875F;
			this.Line8.Width = 0.78125F;
			this.Line8.X1 = 3.8125F;
			this.Line8.X2 = 3.03125F;
			this.Line8.Y1 = 2.46875F;
			this.Line8.Y2 = 2.46875F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 4F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 2.46875F;
			this.Line9.Width = 0.78125F;
			this.Line9.X1 = 4.78125F;
			this.Line9.X2 = 4F;
			this.Line9.Y1 = 2.46875F;
			this.Line9.Y2 = 2.46875F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1875F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 1.8125F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label59.Text = "Months";
			this.Label59.Top = 3.59375F;
			this.Label59.Width = 0.8125F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1875F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 1.8125F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.Label60.Text = "Per Month";
			this.Label60.Top = 3.78125F;
			this.Label60.Width = 1.125F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1875F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 1.8125F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label61.Text = "Pay Today";
			this.Label61.Top = 3.96875F;
			this.Label61.Width = 0.8125F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1875F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 1.8125F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "font-family: \'Courier New\'; text-align: left; ddo-char-set: 1";
			this.Label62.Text = "Pay Later";
			this.Label62.Top = 4.15625F;
			this.Label62.Width = 0.8125F;
			// 
			// fldTransferMonths
			// 
			this.fldTransferMonths.Height = 0.1875F;
			this.fldTransferMonths.Left = 3.0625F;
			this.fldTransferMonths.Name = "fldTransferMonths";
			this.fldTransferMonths.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldTransferMonths.Text = "Field1";
			this.fldTransferMonths.Top = 3.59375F;
			this.fldTransferMonths.Width = 0.78125F;
			// 
			// fldTransferPerMonth
			// 
			this.fldTransferPerMonth.Height = 0.1875F;
			this.fldTransferPerMonth.Left = 3.0625F;
			this.fldTransferPerMonth.Name = "fldTransferPerMonth";
			this.fldTransferPerMonth.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right; ddo-char-set: 1" + "";
			this.fldTransferPerMonth.Text = "Field1";
			this.fldTransferPerMonth.Top = 3.78125F;
			this.fldTransferPerMonth.Width = 0.78125F;
			// 
			// fldTransferPayToday
			// 
			this.fldTransferPayToday.Height = 0.1875F;
			this.fldTransferPayToday.Left = 3.0625F;
			this.fldTransferPayToday.Name = "fldTransferPayToday";
			this.fldTransferPayToday.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldTransferPayToday.Text = "Field1";
			this.fldTransferPayToday.Top = 3.96875F;
			this.fldTransferPayToday.Width = 0.78125F;
			// 
			// fldTransferPayLater
			// 
			this.fldTransferPayLater.Height = 0.1875F;
			this.fldTransferPayLater.Left = 3.0625F;
			this.fldTransferPayLater.Name = "fldTransferPayLater";
			this.fldTransferPayLater.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldTransferPayLater.Text = "Field1";
			this.fldTransferPayLater.Top = 4.15625F;
			this.fldTransferPayLater.Width = 0.78125F;
			// 
			// fldRegMonths
			// 
			this.fldRegMonths.Height = 0.1875F;
			this.fldRegMonths.Left = 4.03125F;
			this.fldRegMonths.Name = "fldRegMonths";
			this.fldRegMonths.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegMonths.Text = "Field5";
			this.fldRegMonths.Top = 3.59375F;
			this.fldRegMonths.Width = 0.78125F;
			// 
			// fldRegPerMonth
			// 
			this.fldRegPerMonth.Height = 0.1875F;
			this.fldRegPerMonth.Left = 4.03125F;
			this.fldRegPerMonth.Name = "fldRegPerMonth";
			this.fldRegPerMonth.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right; ddo-char-set: 1" + "";
			this.fldRegPerMonth.Text = "Field1";
			this.fldRegPerMonth.Top = 3.78125F;
			this.fldRegPerMonth.Width = 0.78125F;
			// 
			// fldRegPayToday
			// 
			this.fldRegPayToday.Height = 0.1875F;
			this.fldRegPayToday.Left = 4.03125F;
			this.fldRegPayToday.Name = "fldRegPayToday";
			this.fldRegPayToday.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegPayToday.Text = "Field1";
			this.fldRegPayToday.Top = 3.96875F;
			this.fldRegPayToday.Width = 0.78125F;
			// 
			// fldRegPayLater
			// 
			this.fldRegPayLater.Height = 0.1875F;
			this.fldRegPayLater.Left = 4.03125F;
			this.fldRegPayLater.Name = "fldRegPayLater";
			this.fldRegPayLater.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 1";
			this.fldRegPayLater.Text = "Field1";
			this.fldRegPayLater.Top = 4.15625F;
			this.fldRegPayLater.Width = 0.78125F;
			// 
			// txtTransferCommentLine
			// 
			this.txtTransferCommentLine.CanShrink = true;
			//this.txtTransferCommentLine.Font = new System.Drawing.Font("Arial", 10F);
			this.txtTransferCommentLine.Height = 0.19F;
			this.txtTransferCommentLine.Left = 0.96875F;
			this.txtTransferCommentLine.Name = "txtTransferCommentLine";
			this.txtTransferCommentLine.RTF = resources.GetString("txtTransferCommentLine.RTF");
			this.txtTransferCommentLine.Top = 4.71875F;
			this.txtTransferCommentLine.Width = 4.78125F;
			// 
			// rptTransferAnalysisReport
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.71875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseFirstYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseSecondYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationFirstYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationSecondYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalFirstYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalSecondYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferRegistration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegRegistration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferMonths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferPerMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferPayToday)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferPayLater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegMonths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegPerMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegPayToday)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegPayLater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseFirstYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseSecondYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationFirstYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationSecondYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocalFirstYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocalSecondYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferExcise;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferRegistration;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegExcise;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegRegistration;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegType;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferMonths;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferPerMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferPayToday;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferPayLater;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegMonths;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegPerMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegPayToday;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegPayLater;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox txtTransferCommentLine;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
