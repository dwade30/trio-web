//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptMVRT10.
	/// </summary>
	public partial class rptMVRT10 : BaseSectionReport
	{
		public rptMVRT10()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "MVRT-10";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMVRT10 InstancePtr
		{
			get
			{
				return (rptMVRT10)Sys.GetInstance(typeof(rptMVRT10));
			}
		}

		protected rptMVRT10 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMVRT10	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		bool blnFirstRecord;
		bool blnSetPageSize;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: cnt As int	OnWriteFCConvert.ToInt32(
			int cnt;
			if (MotorVehicle.Statics.gboolFromBatchRegistration)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= frmBatchProcessing.InstancePtr.intNumberToPrint)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			else if (MotorVehicle.Statics.gboolFromReRegBatchRegistration)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= frmLongTermReRegBatch.InstancePtr.intNumberToPrint)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			else if (MotorVehicle.Statics.gboolFromImport)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= MotorVehicle.Statics.lngRecords)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			else if (MotorVehicle.Statics.gboolFromPrintQueue)
			{
				for (cnt = intCounter + 1; cnt <= (frmPrintQueue.InstancePtr.vsPrintQueue.Rows - 1); cnt++)
				{
					if (FCConvert.CBool(frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(cnt, frmPrintQueue.InstancePtr.intSelectCol)))
					{
						intCounter = cnt;
						break;
					}
				}
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					if (intCounter != cnt)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			foreach (FCPrinter p in FCGlobal.Printers)
			{
				// kk04112017 Drop support for Win95/Sysinfo control
				// If MDIParent.SysInfo1.OSPlatform > 1 Then
				if (p.DeviceName == MotorVehicle.Statics.MVR10PrinterName)
				{
					if (tempPrinter.DriverName == "EPSON24")
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 12cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
						}
					}
					break;
				}
			}

			if (MotorVehicle.Statics.gboolFromBatchRegistration || MotorVehicle.Statics.gboolFromReRegBatchRegistration || MotorVehicle.Statics.gboolFromPrintQueue || MotorVehicle.Statics.gboolFromImport)
			{
				blnFirstRecord = true;
				blnSetPageSize = true;
				intCounter = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{

			string c = "";
			Decimal SubTotal;
			Decimal ExciseBal;
			string MVTyp = "";
			string Info = "";
			string CL1 = "";
			string CL2 = "";
			string PRINTOWNER2 = "";
			double Mrate;
			Decimal Credit;
			Decimal RegFee = 0;
			string Town = "";
			string SD = "";
			string printfile = "";
			DateTime datStartYear;
			clsDRWrapper rsLong = new clsDRWrapper();
			clsDRWrapper rsDefaults = new clsDRWrapper();
			clsDRWrapper rsLongTermActivityRecord = new clsDRWrapper();
			object strStart = 0;
			clsDRWrapper rsFleet = new clsDRWrapper();
			MotorVehicle.Statics.PrintMVR3 = false;
			fecherFoundation.Information.Err().Clear();
            try
            {
                // On Error GoTo ErrorTag
                fecherFoundation.Information.Err().Clear();
                rsDefaults.OpenRecordset("SELECT * FROM DefaultInformation", "TWMV0000.vb1");
                // Check to see if errors have occured
                if (!MotorVehicle.Statics.gboolFromBatchRegistration &&
                    !MotorVehicle.Statics.gboolFromReRegBatchRegistration && !MotorVehicle.Statics.gboolFromPrintQueue)
                {
                    if (fecherFoundation.Information.Err().Number != 0)
                    {
                        MotorVehicle.PrinterError("MVR3 Printer");
                        MotorVehicle.Statics.PrintMVR3 = false;
                        rptMVRT10.InstancePtr.Cancel();
                        return;
                    }

                    MotorVehicle.NewSetReportForm(this);
                }
                else
                {
                    if (blnSetPageSize)
                    {
                        blnSetPageSize = false;
                        if (fecherFoundation.Information.Err().Number != 0)
                        {
                            MotorVehicle.PrinterError("MVR3 Printer");
                            MotorVehicle.Statics.PrintMVR3 = false;
                            rptMVRT10.InstancePtr.Cancel();
                            return;
                        }

                        MotorVehicle.NewSetReportForm(this);
                    }
                }

                bool executeCheckAgain2 = false;
                bool firstIf = false;
                bool executeCheckAgain20 = false;
                bool thirdIf = false;
                if (!MotorVehicle.Statics.gboolFromReRegBatchRegistration && !MotorVehicle.Statics.gboolFromPrintQueue)
                {
                    firstIf = true;
                    if (MotorVehicle.Statics.gboolFromImport)
                    {
                        MotorVehicle.Statics.rsFinal.OpenRecordset(
                            "SELECT * FROM Master WHERE ID = " +
                            FCConvert.ToString(MotorVehicle.Statics.lngIDs[intCounter]), "TWMV0000.vb1");
                    }

                    if (MotorVehicle.Statics.gboolFromBatchRegistration)
                    {
                        fldVIN.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intVINCol);
                        fldYear.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intYearCol);
                        fldMake.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intMakeCol);
                        fldUnit.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intUnitCol);
                        fldPlate.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intPlateCol);
                        fldStyle.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intStyleCol);
                        fldCTANumber.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intCTACol);
                    }
                    else
                    {
                        fldMake.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
                        fldYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) !=
                            "...." &&
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) != "    ")
                        {
                            fldUnit.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber");
                        }

                        if (fecherFoundation.Strings.Trim(
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))) != "NEW")
                        {
                            fldPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");
                        }

                        fldStyle.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Style");
                        fldVIN.Text =
                            Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")), 17);
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber")).Length >
                            1)
                        {
                            fldCTANumber.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber");
                        }
                    }

                    fldColor1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("color1");
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2")) != "NA")
                    {
                        fldColor2.Text = "/" + MotorVehicle.Statics.rsFinal.Get_Fields_String("color2");
                    }

                    // DJW@01122012 - BMV told us to only print 2000 if the vehicle weight was less than or equal to it and if it was more to not print it on the form
                    // DJW@04102013 - TROMV-733 BMV does not want any weight to print on this form ever
                    // DJW@04292013 - TROMV 741 BMV wants seperatenotice to show for 2000 lb trialers
                    fld2000lbsNotice.Visible = false;
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != true)
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != 0 &&
                            Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) <= 2000)
                        {
                            // fldNetWeight.Text = Format(Format(2000, "######"), "@@@@@@")
                            fld2000lbsNotice.Visible = true;
                        }
                    }

                    if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) ==
                            "RRR")
                        {
                            fldReRegYes.Visible = true;
                            fldReRegNo.Visible = false;
                        }
                        else
                        {
                            fldReRegYes.Visible = false;
                            fldReRegNo.Visible = true;
                        }
                    }
                    else if (MotorVehicle.Statics.RegistrationType == "LOST")
                    {
                        fldReRegYes.Visible = false;
                        fldReRegNo.Visible = true;
                    }
                    else
                    {
                        if (MotorVehicle.Statics.gboolFromImport)
                        {
                            fldReRegYes.Visible = false;
                            fldReRegNo.Visible = true;
                        }
                        else
                        {
                            if (frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text ==
                                "YES - Expires this year" ||
                                frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text ==
                                "YES - Expires next year")
                            {
                                fldReRegYes.Visible = true;
                                fldReRegNo.Visible = false;
                            }
                            else
                            {
                                fldReRegYes.Visible = false;
                                fldReRegNo.Visible = true;
                            }
                        }
                    }

                    fldOwner1.Text =
                        MotorVehicle.GetPartyNameMiddleInitial(
                            MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"));
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1") > 0)
                    {
                        fldOwner2.Text =
                            MotorVehicle.GetPartyNameMiddleInitial(
                                MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
                    }
                    else
                    {
                        fldOwner2.Text = "";
                    }

                    if (!MotorVehicle.Statics.blnSuspended)
                    {
                        fldAddress.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Address");
                    }
                    else
                    {
                        fldAddress.Text = "REGISTRATION SUSPENDED";
                    }

                    fldCity.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("City");
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State")) != "CC")
                    {
                        fldState.Text = MotorVehicle.Statics.rsFinal.Get_Fields("State");
                    }

                    fldZip.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip");
                    fldResidenceCity.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence");
                    fldResidenceState.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState");
                    fldResidenceCode.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
                    if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                    {
                        RegFee = FCConvert.ToDecimal(
                            MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"));
                    }
                    else if (MotorVehicle.Statics.RegistrationType == "LOST")
                    {
                        RegFee = FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee"));
                    }
                    else if (MotorVehicle.Statics.RegistrationType == "CORR")
                    {
                        RegFee = 0;
                    }
                    else
                    {
                        if (fecherFoundation.FCUtils.IsNull(
                            MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) == true)
                            MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", 0);
                        if (MotorVehicle.Statics.rsFinal.IsFieldNull("RegRateCharge"))
                            MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", 0);
                        if (fecherFoundation.FCUtils.IsNull(
                            MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) == true)
                            MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
                        if (MotorVehicle.Statics.rsFinal.IsFieldNull("TransferCreditUsed"))
                            MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
                        if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")) ==
                            true)
                            MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", 0);
                        RegFee = FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") -
                                                     MotorVehicle.Statics.rsFinal.Get_Fields_Decimal(
                                                         "TransferCreditUsed"));
                        if (RegFee < 0)
                            RegFee = 0;
                        RegFee += Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee"));
                    }

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "LPS" ||
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "NRT" ||
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "DPR" ||
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "ECO" ||
                        MotorVehicle.Statics.gboolLongRegExtension || MotorVehicle.Statics.gboolFromImport ||
                        MotorVehicle.Statics.gboolLongRegExtensionSamePlate)
                    {
                        if (MotorVehicle.Statics.gboolFromImport)
                        {
                            rsLongTermActivityRecord.OpenRecordset(
                                "SELECT * FROM LongTermTrailerRegistrations WHERE ID = " +
                                MotorVehicle.Statics.rsFinal.Get_Fields_Int32("LongTermTrailerRegID"));
                        }
                        else
                        {
                            rsLongTermActivityRecord.OpenRecordset(
                                "SELECT * FROM LongTermTrailerRegistrations WHERE ID = " +
                                MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("LongTermTrailerRegID"));
                        }

                        if (rsLongTermActivityRecord.EndOfFile() != true &&
                            rsLongTermActivityRecord.BeginningOfFile() != true)
                        {
                            if (rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears") > 0)
                            {
                                if (MotorVehicle.Statics.gboolLongRegExtension ||
                                    MotorVehicle.Statics.gboolLongRegExtensionSamePlate)
                                {
                                    fldDescription.Text = "Start Year: " + FCConvert.ToString(
                                        MotorVehicle.Statics.rsFinal
                                            .Get_Fields_DateTime("ExpireDate").Year -
                                        rsLongTermActivityRecord
                                            .Get_Fields_Int32("NumberOfYears") -
                                        (frmDataInputLongTermTrailers.InstancePtr.cboLength
                                            .ItemData(frmDataInputLongTermTrailers.InstancePtr
                                                .cboLength.SelectedIndex)));
                                }
                                else
                                {
                                    fldDescription.Text =
                                        "Start Year: " + FCConvert.ToString(
                                            rsLongTermActivityRecord.Get_Fields_DateTime("ExpireDate").Year -
                                            rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears"));
                                }
                            }
                            else
                            {
                                executeCheckAgain2 = true;
                                goto CheckAgain2;
                            }
                        }
                        else
                        {
                            executeCheckAgain2 = true;
                            goto CheckAgain2;
                        }
                    }
                    else
                    {
                        if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
                        {
                            if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")
                                .Month) >= 10)
                            {
                                datStartYear = FCConvert.ToDateTime(
                                    "3/1/" + FCConvert.ToString(FCConvert.ToInt32(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate").Year + 1)));
                            }
                            else
                            {
                                datStartYear = FCConvert.ToDateTime(
                                    "3/1/" + FCConvert.ToString(MotorVehicle.Statics.rsFinal
                                        .Get_Fields_DateTime("EffectiveDate").Year));
                            }

                            if (MotorVehicle.Statics.RegistrationType == "RRR")
                            {
                                if (frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text ==
                                    "YES - Expires next year")
                                {
                                    fldDescription.Text =
                                        "Start Year: " + Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"), 4);
                                }
                                else
                                {
                                    fldDescription.Text =
                                        "Start Year: " + Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
                                }
                            }
                            else if (MotorVehicle.Statics.RegistrationType == "NRR")
                            {
                                fldDescription.Text =
                                    "Start Year: " +
                                    Strings.Right(
                                        Strings.Format(
                                            MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"),
                                            "MM/dd/yyyy"), 4);
                            }
                            else
                            {
                                fldDescription.Text =
                                    "Start Year: " + Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
                            }
                        }
                        else
                        {
                            fldDescription.Text = "Start Year: ";
                        }
                    }

                    goto CheckAgain2;
                }
                else if (MotorVehicle.Statics.gboolFromReRegBatchRegistration)
                {
                    rsLong.OpenRecordset(
                        "SELECT * FROM Master WHERE ID = " +
                        frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                            frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                            frmLongTermReRegBatch.InstancePtr.SelectIDCol), "TWMV0000.vb1");
                    if (rsLong.EndOfFile() != true && rsLong.BeginningOfFile() != true)
                    {
                        rsFleet.OpenRecordset(
                            "SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " +
                            rsFleet.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                            rsFleet.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE Deleted <> 1 AND Type = 'L' AND FleetNumber = " +
                            rsLong.Get_Fields_Int32("FleetNumber") + " ORDER BY p.FullName");
                        fldPlate.Text = frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                            frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                            frmLongTermReRegBatch.InstancePtr.SelectNewPlateCol);
                        fldCTANumber.Text = "";
                        fldMake.Text = rsLong.Get_Fields_String("make");
                        fldYear.Text = rsLong.Get_Fields_String("Year");
                        if (FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "...." &&
                            FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "    ")
                        {
                            fldUnit.Text = rsLong.Get_Fields_String("UnitNumber");
                        }

                        fldStyle.Text = rsLong.Get_Fields_String("Style");
                        fldVIN.Text = Strings.Left(FCConvert.ToString(rsLong.Get_Fields_String("Vin")), 17);
                        fldColor1.Text = rsLong.Get_Fields_String("color1");
                        if (FCConvert.ToString(rsLong.Get_Fields_String("color2")) != "NA")
                        {
                            fldColor2.Text = "/" + rsLong.Get_Fields_String("color2");
                        }

                        // DJW@01122012 - BMV told us to only print 2000 if the vehicle weight was less than or equal to it and if it was more to not print it on the form
                        // DJW@04102013 - TROMV-733 BMV does not want any weight to print on this form ever
                        // DJW@04292013 - TROMV 741 BMV wants seperatenotice to show for 2000 lb trialers
                        fld2000lbsNotice.Visible = false;
                        if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields("NetWeight")) != true)
                        {
                            if (Conversion.Val(rsLong.Get_Fields("NetWeight")) != 0 &&
                                Conversion.Val(rsLong.Get_Fields("NetWeight")) <= 2000)
                            {
                                // fldNetWeight.Text = Format(Format(2000, "######"), "@@@@@@")
                                fld2000lbsNotice.Visible = true;
                            }
                        }

                        // 
                        fldReRegYes.Visible = true;
                        fldReRegNo.Visible = false;
                        fldOwner1.Text =
                            fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields("Party1FullName")));
                        fldOwner2.Text =
                            fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields("Party2FullName")));
                        if (!MotorVehicle.Statics.blnSuspended)
                        {
                            fldAddress.Text = rsFleet.Get_Fields_String("Address");
                        }
                        else
                        {
                            fldAddress.Text = "REGISTRATION SUSPENDED";
                        }

                        fldCity.Text = rsFleet.Get_Fields_String("City");
                        if (FCConvert.ToString(rsFleet.Get_Fields("State")) != "CC")
                        {
                            fldState.Text = rsFleet.Get_Fields_String("State");
                        }

                        fldZip.Text = rsFleet.Get_Fields_String("Zip");
                        if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                            "MAINE MOTOR TRANSPORT")
                        {
                            fldResidenceCity.Text = "MMTA SERVICES INC. 44004";
                        }
                        else if (fecherFoundation.Strings.UCase(
                                     fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                                 "ACE REGISTRATION SERVICES LLC")
                        {
                            fldResidenceCity.Text = "ACE REGISTRATION";
                        }
                        else if (fecherFoundation.Strings.UCase(
                                     fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                                 "COUNTRYWIDE TRAILER")
                        {
                            fldResidenceCity.Text = "COUNTRYWIDE TRAILER 44018";
                        }
                        else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                                 "AB LEDUE ENTERPRISES")
                        {
                            fldResidenceCity.Text = "AB LEDUE ENTERPRISES";
                        }
                        else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER")
                        {
                            fldResidenceCity.Text = "MAINE TRAILER ME 44005";
                        }
                        else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                                 "HASKELL REGISTRATION")
                        {
                            fldResidenceCity.Text = "HASKELL REGISTRATION 44002";
                        }
                        else
                        {
                            fldResidenceCity.Text = rsLong.Get_Fields_String("Residence");
                        }

                        if (fecherFoundation.Strings.UCase(
                                fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                            "ACE REGISTRATION SERVICES LLC")
                        {
                            fldResidenceState.Text = "ME";
                        }
                        else if (fecherFoundation.Strings.UCase(
                                     fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                                 "ACE REGISTRATION SERVICES LLC")
                        {
                            fldResidenceState.Text = "ME";
                        }
                        else
                        {
                            fldResidenceState.Text = rsLong.Get_Fields_String("ResidenceState");
                        }

                        if (fecherFoundation.Strings.UCase(
                                fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                            "ACE REGISTRATION SERVICES LLC")
                        {
                            fldResidenceCode.Text = "44021";
                        }
                        else if (fecherFoundation.Strings.UCase(
                                     fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                                 "ACE REGISTRATION SERVICES LLC")
                        {
                            fldResidenceCode.Text = "44003";
                        }
                        else
                        {
                            fldResidenceCode.Text = rsLong.Get_Fields_String("ResidenceCode");
                        }

                        // fldRegFee.Text = Format(Format(.vsRegLength.TextMatrix(.intStartRow + intCounter, .SelectTotalCol) - curAgentFee, "#,##0.00"), "@@@@@@@")
                        // fldRegCredit.Text = Format(Format(0, "###0.00"), "@@@@@@@")
                        // fldFees.Text = Format(Format(.vsRegLength.TextMatrix(.intStartRow + intCounter, .SelectTotalCol) - curAgentFee, "#,##0.00"), "@@@@@@@")
                        // datStartYear = "3/1/" & Year(.vsRegLength.TextMatrix(.intStartRow + intCounter, .SelectCurrentExpireDateCol))
                        if (DateTime.Today.Month >= 10)
                        {
                            datStartYear = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
                        }
                        else
                        {
                            datStartYear = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year));
                        }

                        fldTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                        fldDescription.Text =
                            "Start Year: " + Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"), 4);
                        fldOldPlate.Text = frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                            frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                            frmLongTermReRegBatch.InstancePtr.SelectPlateCol);
                        lblTransaction.Text = "";
                        fldExpires.Text =
                            Strings.Format(
                                frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                                    frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                                    frmLongTermReRegBatch.InstancePtr.SelectExpiresCol), "MM/dd/yyyy");
                        if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                            "MAINE MOTOR TRANSPORT" ||
                            fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                            "HASKELL REGISTRATION" ||
                            fecherFoundation.Strings.UCase(
                                fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                            "ACE REGISTRATION SERVICES LLC" ||
                            fecherFoundation.Strings.UCase(
                                fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "MAINE TRAILER")
                        {
                            frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                                frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                                frmLongTermReRegBatch.InstancePtr.SelectUniqueIdentifierCol,
                                FCConvert.ToString(FCConvert.ToInt32(FCUtils.Rnd() * 999999) + 1));
                            fldValidationLine1.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt");
                            fldValidationLine1.Text = fldValidationLine1 + " " +
                                                      rsDefaults.Get_Fields_String("ResidenceCode") + "#" +
                                                      Strings.Format(
                                                          frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                                                              frmLongTermReRegBatch.InstancePtr.intStartRow +
                                                              intCounter,
                                                              frmLongTermReRegBatch.InstancePtr
                                                                  .SelectUniqueIdentifierCol), "000000") + " " +
                                                      MotorVehicle.Statics.OpID;
                            fldValidationLine2Description.Text = "LTT";
                            fldValidationLine2.Text = Strings.Format(
                                FCConvert.ToDecimal(frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                                    frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                                    frmLongTermReRegBatch.InstancePtr.SelectTotalCol)) -
                                frmLongTermReRegBatch.InstancePtr.curAgentFee, "#,##0.00");
                        }
                    }
                    else
                    {
                        MessageBox.Show(
                            "Error printing MVR-10.  Can't find vehicle with key " +
                            frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                                frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                                frmLongTermReRegBatch.InstancePtr.SelectVINCol), "Unable to Find Vehicle",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MotorVehicle.Statics.PrintMVR3 = false;
                        rptMVRT10.InstancePtr.Cancel();
                        this.Close();
                    }
                }
                else
                {
                    rsLong.OpenRecordset(
                        "SELECT * FROM Master WHERE ID = " +
                        frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter,
                            frmPrintQueue.InstancePtr.intNumberCol), "TWMV0000.vb1");
                    if (rsLong.EndOfFile() != true && rsLong.BeginningOfFile() != true)
                    {
                        thirdIf = true;
                        fldPlate.Text =
                            frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter,
                                frmPrintQueue.InstancePtr.intPlateCol);
                        if (FCConvert.ToString(rsLong.Get_Fields_String("TitleNumber")).Length > 1)
                        {
                            fldCTANumber.Text = rsLong.Get_Fields_String("TitleNumber");
                        }
                        else
                        {
                            fldCTANumber.Text = "";
                        }

                        fldMake.Text = rsLong.Get_Fields_String("make");
                        fldYear.Text = rsLong.Get_Fields_String("Year");
                        if (FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "...." &&
                            FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "    ")
                        {
                            fldUnit.Text = rsLong.Get_Fields_String("UnitNumber");
                        }
                        else
                        {
                            fldUnit.Text = "";
                        }

                        fldStyle.Text = rsLong.Get_Fields_String("Style");
                        fldVIN.Text = Strings.Left(FCConvert.ToString(rsLong.Get_Fields_String("Vin")), 17);
                        fldColor1.Text = rsLong.Get_Fields_String("color1");
                        if (FCConvert.ToString(rsLong.Get_Fields_String("color2")) != "NA")
                        {
                            fldColor2.Text = "/" + rsLong.Get_Fields_String("color2");
                        }
                        else
                        {
                            fldColor2.Text = "";
                        }

                        // DJW@01122012 - BMV told us to only print 2000 if the vehicle weight was less than or equal to it and if it was more to not print it on the form
                        // DJW@04102013 - TROMV-733 BMV does not want any weight to print on this form ever
                        // DJW@04292013 - TROMV 741 BMV wants seperatenotice to show for 2000 lb trialers
                        fld2000lbsNotice.Visible = false;
                        if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields("NetWeight")) != true)
                        {
                            if (Conversion.Val(rsLong.Get_Fields("NetWeight")) != 0 &&
                                Conversion.Val(rsLong.Get_Fields("NetWeight")) <= 2000)
                            {
                                // fldNetWeight.Text = Format(Format(2000, "######"), "@@@@@@")
                                fld2000lbsNotice.Visible = true;
                            }
                        }

                        if (FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "RRR")
                        {
                            fldReRegYes.Visible = true;
                            fldReRegNo.Visible = false;
                        }
                        else
                        {
                            fldReRegYes.Visible = false;
                            fldReRegNo.Visible = true;
                        }

                        fldOwner1.Text = MotorVehicle.GetPartyNameMiddleInitial(rsLong.Get_Fields_Int32("PartyID1"));
                        if (rsLong.Get_Fields_Int32("PartyID1") > 0)
                        {
                            fldOwner2.Text =
                                MotorVehicle.GetPartyNameMiddleInitial(rsLong.Get_Fields_Int32("PartyID2"));
                        }
                        else
                        {
                            fldOwner2.Text = "";
                        }

                        if (!MotorVehicle.Statics.blnSuspended)
                        {
                            fldAddress.Text = rsLong.Get_Fields_String("Address");
                        }
                        else
                        {
                            fldAddress.Text = "REGISTRATION SUSPENDED";
                        }

                        fldCity.Text = rsLong.Get_Fields_String("City");
                        if (FCConvert.ToString(rsLong.Get_Fields("State")) != "CC")
                        {
                            fldState.Text = rsLong.Get_Fields_String("State");
                        }

                        fldZip.Text = rsLong.Get_Fields_String("Zip");
                        fldResidenceCity.Text = rsLong.Get_Fields_String("Residence");
                        fldResidenceState.Text = rsLong.Get_Fields_String("ResidenceState");
                        fldResidenceCode.Text = rsLong.Get_Fields_String("ResidenceCode");
                        if (FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "DPR")
                        {
                            RegFee = FCConvert.ToDecimal(rsLong.Get_Fields_Decimal("DuplicateRegistrationFee"));
                        }
                        else if (rsLong.Get_Fields("TransactionType") == "LPS")
                        {
                            RegFee = FCConvert.ToDecimal(rsLong.Get_Fields_Decimal("ReplacementFee"));
                        }
                        else if (rsLong.Get_Fields("TransactionType") == "ECO")
                        {
                            RegFee = 0;
                        }
                        else
                        {
                            if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields_Decimal("RegRateCharge")) == true)
                                rsLong.Set_Fields("RegRateCharge", 0);
                            if (rsLong.IsFieldNull("RegRateCharge"))
                                rsLong.Set_Fields("RegRateCharge", 0);
                            if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields_Decimal("TransferCreditUsed")) ==
                                true)
                                rsLong.Set_Fields("TransferCreditUsed", 0);
                            if (rsLong.IsFieldNull("TransferCreditUsed"))
                                rsLong.Set_Fields("TransferCreditUsed", 0);
                            if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields("TransferFee")) == true)
                                rsLong.Set_Fields("TransferFee", 0);
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsLong.Get_Fields("TransferFee"))) ==
                                "")
                                rsLong.Set_Fields("TransferFee", 0);
                            RegFee = FCConvert.ToDecimal(rsLong.Get_Fields_Decimal("RegRateCharge") -
                                                         rsLong.Get_Fields_Decimal("TransferCreditUsed"));
                            if (RegFee < 0)
                                RegFee = 0;
                            RegFee += Conversion.Val(rsLong.Get_Fields("TransferFee"));
                        }

                        if (FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "LPS" ||
                            FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "NRT" ||
                            FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "DPR" ||
                            FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "ECO")
                        {
                            rsLongTermActivityRecord.OpenRecordset(
                                "SELECT * FROM LongTermTrailerRegistrations WHERE ID = " +
                                rsLong.Get_Fields("LongTermTrailerRegKey"));
                            if (rsLongTermActivityRecord.EndOfFile() != true &&
                                rsLongTermActivityRecord.BeginningOfFile() != true)
                            {
                                if (rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears") > 0)
                                {
                                    fldDescription.Text =
                                        "Start Year: " + FCConvert.ToString(
                                            rsLongTermActivityRecord.Get_Fields_DateTime("ExpireDate").Year -
                                            rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears"));
                                }
                                else
                                {
                                    executeCheckAgain20 = true;
                                    goto CheckAgain20;
                                }
                            }
                            else
                            {
                                executeCheckAgain20 = true;
                                goto CheckAgain20;
                            }
                        }
                        else
                        {
                            if (Information.IsDate(rsLong.Get_Fields("EffectiveDate")))
                            {
                                if (FCConvert.ToInt32(rsLong.Get_Fields_DateTime("EffectiveDate").Month) >= 10)
                                {
                                    datStartYear = FCConvert.ToDateTime(
                                        "3/1/" + FCConvert.ToString(
                                            FCConvert.ToInt32(rsLong.Get_Fields_DateTime("EffectiveDate").Year + 1)));
                                }
                                else
                                {
                                    datStartYear = FCConvert.ToDateTime(
                                        "3/1/" + FCConvert.ToString(rsLong.Get_Fields_DateTime("EffectiveDate").Year));
                                }

                                if (FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "RRR")
                                {
                                    rsLongTermActivityRecord.OpenRecordset(
                                        "SELECT * FROM LongTermTrailerRegistrations WHERE ID = " +
                                        rsLong.Get_Fields("LongTermTrailerRegKey"));
                                    if (rsLongTermActivityRecord.EndOfFile() != true &&
                                        rsLongTermActivityRecord.BeginningOfFile() != true)
                                    {
                                        if (rsLongTermActivityRecord.Get_Fields_DateTime("ExpireDate").Year -
                                            rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears") ==
                                            datStartYear.Year)
                                        {
                                            fldDescription.Text =
                                                "Start Year: " +
                                                Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"), 4);
                                        }
                                        else
                                        {
                                            fldDescription.Text =
                                                "Start Year: " +
                                                Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
                                        }
                                    }
                                    else
                                    {
                                        fldDescription.Text =
                                            "Start Year: " + Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"),
                                                4);
                                    }
                                }
                                else if (rsLong.Get_Fields("TransactionType") == "NRR")
                                {
                                    fldDescription.Text =
                                        "Start Year: " +
                                        Strings.Right(
                                            Strings.Format(rsLong.Get_Fields_DateTime("EffectiveDate"), "MM/dd/yyyy"),
                                            4);
                                }
                                else
                                {
                                    fldDescription.Text =
                                        "Start Year: " + Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
                                }
                            }
                            else
                            {
                                fldDescription.Text = "Start Year: ";
                            }
                        }

                        goto CheckAgain20;
                    }
                    else
                    {
                        MessageBox.Show(
                            "Error printing MVR-10.  Can't find vehicle with key " +
                            frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter,
                                frmPrintQueue.InstancePtr.intNumberCol), "Unable to Find Vehicle", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        MotorVehicle.Statics.PrintMVR3 = false;
                        rptMVRT10.InstancePtr.Cancel();
                        this.Close();
                    }
                }

                CheckAgain2: ;
                if (executeCheckAgain2)
                {
                    frmInput.InstancePtr.Init(ref strStart, "Enter Start Year",
                        "Please enter the start year to be printed on the registration.");
                    if (!Information.IsNumeric(strStart) || FCConvert.ToInt32(strStart) <= 0)
                    {
                        MessageBox.Show("You must enter a numeric value for the start year.", "Invalid Start Year",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        executeCheckAgain2 = true;
                        goto CheckAgain2;
                    }
                    else
                    {
                        MotorVehicle.Statics.glngNumberOfYears = FCConvert.ToInt32(
                            MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year -
                            FCConvert.ToInt32(strStart));
                        fldDescription.Text = "Start Year: " + FCConvert.ToString(strStart);
                    }

                    executeCheckAgain2 = false;
                    firstIf = true;
                }

                if (firstIf)
                {
                    fldTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                    if (MotorVehicle.Statics.gboolFromImport)
                    {
                        lblTransaction.Text = fecherFoundation.Strings.Trim(
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("InfoMessage")));
                        fldOldPlate.Text = "";
                    }
                    else
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "DPR")
                        {
                            lblTransaction.Text = "DUPLICATE";
                            fldOldPlate.Text = "";
                        }
                        else if (MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType") == "ECO")
                        {
                            lblTransaction.Text = fecherFoundation.Strings.Trim(
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("InfoMessage")));
                            fldOldPlate.Text = "";
                        }
                        else if (MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType") == "LPS")
                        {
                            lblTransaction.Text = "LOST PLATE";
                            fldOldPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate");
                        }
                        else if (MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType") == "RRR")
                        {
                            fldOldPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate");
                            lblTransaction.Text = "";
                        }
                        else if (MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType") == "NRT")
                        {
                            lblTransaction.Text = "TRANSFER";
                            fldOldPlate.Text = "";
                        }
                        else
                        {
                            fldOldPlate.Text = "";
                            lblTransaction.Text = "";
                        }
                    }

                    fldExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"),
                        "MM/dd/yyyy");
                    if (
                        fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                        "MAINE MOTOR TRANSPORT" ||
                        fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION" ||
                        fecherFoundation.Strings.UCase(
                            fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                        "ACE REGISTRATION SERVICES LLC" ||
                        fecherFoundation.Strings.UCase(
                            fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "MAINE TRAILER")
                    {
                        if (Conversion.Val(
                                MotorVehicle.Statics.rsFinal.Get_Fields_Int32("LongTermTrailerRegUniqueIdentifier")) ==
                            0)
                        {
                            if (MotorVehicle.Statics.gboolFromImport)
                            {
                                MotorVehicle.Statics.rsFinal.Edit();
                                MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegUniqueIdentifier",
                                    FCConvert.ToInt32(FCUtils.Rnd() * 999999) + 1);
                                MotorVehicle.Statics.rsFinal.Update();
                            }
                            else
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegUniqueIdentifier",
                                    FCConvert.ToInt32(FCUtils.Rnd() * 999999) + 1);
                            }
                        }

                        fldValidationLine1.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt");
                        fldValidationLine1.Text = fldValidationLine1 + " " +
                                                  rsDefaults.Get_Fields_String("ResidenceCode") + "#" +
                                                  Strings.Format(
                                                      MotorVehicle.Statics.rsFinal.Get_Fields_Int32(
                                                          "LongTermTrailerRegUniqueIdentifier"), "000000") + " " +
                                                  MotorVehicle.Statics.OpID;
                        fldValidationLine2Description.Text = "LTT";
                        if (MotorVehicle.Statics.gboolFromImport)
                        {
                            fldValidationLine2Description.Text = "CORRECTION";
                        }
                        else
                        {
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "DPR")
                            {
                                fldValidationLine2Description.Text = "DUPLICATE";
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType") == "ECO")
                            {
                                fldValidationLine2Description.Text = "CORRECTION";
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType") == "LPS")
                            {
                                fldValidationLine2Description.Text = "LOST PLATE";
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType") == "NRT")
                            {
                                fldValidationLine2Description.Text = "TRANSFER";
                            }
                            else
                            {
                                fldValidationLine2Description.Text = "LTT";
                            }
                        }

                        fldValidationLine2.Text = Strings.Format(RegFee, "#,##0.00");
                    }

                    firstIf = false;
                }

                CheckAgain20: ;
                if (executeCheckAgain20)
                {
                    frmInput.InstancePtr.Init(ref strStart, "Enter Start Year",
                        "Please enter the start year to be printed on the registration.");
                    if (!Information.IsNumeric(strStart) || FCConvert.ToInt32(strStart) <= 0)
                    {
                        MessageBox.Show("You must enter a numeric value for the start year.", "Invalid Start Year",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        executeCheckAgain2 = true;
                        goto CheckAgain2;
                    }
                    else
                    {
                        MotorVehicle.Statics.glngNumberOfYears = FCConvert.ToInt32(
                            MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year -
                            FCConvert.ToInt32(strStart));
                        fldDescription.Text = "Start Year: " + FCConvert.ToString(strStart);
                    }

                    executeCheckAgain20 = false;
                    thirdIf = true;
                }

                if (thirdIf)
                {
                    fldTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                    if (FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "DPR")
                    {
                        lblTransaction.Text = "DUPLICATE";
                        fldOldPlate.Text = "";
                    }
                    else if (rsLong.Get_Fields("TransactionType") == "ECO")
                    {
                        lblTransaction.Text =
                            fecherFoundation.Strings.Trim(FCConvert.ToString(rsLong.Get_Fields_String("InfoMessage")));
                        fldOldPlate.Text = "";
                    }
                    else if (rsLong.Get_Fields("TransactionType") == "LPS")
                    {
                        lblTransaction.Text = "LOST PLATE";
                        fldOldPlate.Text = rsLong.Get_Fields_String("OldPlate");
                    }
                    else if (rsLong.Get_Fields("TransactionType") == "RRR")
                    {
                        fldOldPlate.Text = rsLong.Get_Fields_String("OldPlate");
                        lblTransaction.Text = "";
                    }
                    else if (rsLong.Get_Fields("TransactionType") == "NRT")
                    {
                        lblTransaction.Text = "TRANSFER";
                        fldOldPlate.Text = "";
                    }
                    else
                    {
                        lblTransaction.Text = "";
                        fldOldPlate.Text = "";
                    }

                    fldExpires.Text = Strings.Format(rsLong.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
                    if (
                        fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                        "MAINE MOTOR TRANSPORT" ||
                        fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION" ||
                        fecherFoundation.Strings.UCase(
                            fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                        "ACE REGISTRATION SERVICES LLC" ||
                        fecherFoundation.Strings.UCase(
                            fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "MAINE TRAILER")
                    {
                        if (Conversion.Val(frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter,
                            frmPrintQueue.InstancePtr.intUniqueIdentifierCol)) == 0)
                        {
                            frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter,
                                frmPrintQueue.InstancePtr.intUniqueIdentifierCol,
                                FCConvert.ToString(FCConvert.ToInt32(FCUtils.Rnd() * 999999) + 1));
                        }

                        fldValidationLine1.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt");
                        fldValidationLine1.Text = fldValidationLine1 + " " +
                                                  rsDefaults.Get_Fields_String("ResidenceCode") + "#" +
                                                  Strings.Format(
                                                      frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter,
                                                          frmPrintQueue.InstancePtr.intUniqueIdentifierCol), "000000") +
                                                  " " + MotorVehicle.Statics.OpID;
                        fldValidationLine2Description.Text = "LTT";
                        if (FCConvert.ToString(rsLong.Get_Fields("TransactionType")) == "DPR")
                        {
                            fldValidationLine2Description.Text = "DUPLICATE";
                        }
                        else if (rsLong.Get_Fields("TransactionType") == "ECO")
                        {
                            fldValidationLine2Description.Text = "CORRECTION";
                        }
                        else if (rsLong.Get_Fields("TransactionType") == "LPS")
                        {
                            fldValidationLine2Description.Text = "LOST PLATE";
                        }
                        else if (rsLong.Get_Fields("TransactionType") == "NRT")
                        {
                            fldValidationLine2Description.Text = "TRANSFER";
                        }
                        else
                        {
                            fldValidationLine2Description.Text = "LTT";
                        }

                        fldValidationLine2.Text = Strings.Format(RegFee, "#,##0.00");
                    }

                    thirdIf = false;
                }

                fecherFoundation.Information.Err().Clear();
                if (fecherFoundation.Information.Err().Number == 0)
                {
                    MotorVehicle.Statics.PrintMVR3 = true;
                    if (MotorVehicle.Statics.gboolFromBatchRegistration)
                    {
                        frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(
                            frmBatchProcessing.InstancePtr.intStartRow + intCounter,
                            frmBatchProcessing.InstancePtr.intPrintedCol, FCConvert.ToString(true));
                    }
                    else if (MotorVehicle.Statics.gboolFromReRegBatchRegistration)
                    {
                        frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(
                            frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter,
                            frmLongTermReRegBatch.InstancePtr.SelectPrintedCol, FCConvert.ToString(true));
                    }
                    else if (MotorVehicle.Statics.gboolFromPrintQueue)
                    {
                        frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter,
                            frmPrintQueue.InstancePtr.intPrintedStatusCol, "Yes");
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (fecherFoundation.Information.Err(ex).Number != 0)
                {
                    MotorVehicle.PrinterError("MVR10 Printer");
                    MotorVehicle.Statics.PrintMVR3 = false;
                    this.Cancel();
                    this.Close();
                }
            }
            finally
            {
                rsLong.DisposeOf();
                rsDefaults.DisposeOf();
                rsLongTermActivityRecord.DisposeOf();
                rsFleet.DisposeOf();
            }
		}
	}
}
