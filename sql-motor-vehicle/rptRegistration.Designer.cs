﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptRegistration.
	/// </summary>
	partial class rptRegistration
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRegistration));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblClass = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMVR3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMVR3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtClass,
				this.txtPlate,
				this.txtMVR3,
				this.txtStatus,
				this.txtMO,
				this.txtYr,
				this.txtFees
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblClass,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Line1,
				this.Label1,
				this.Label32,
				this.Label9,
				this.Label10,
				this.Label33
			});
			this.PageHeader.Height = 0.6875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblClass
			// 
			this.lblClass.Height = 0.19F;
			this.lblClass.HyperLink = null;
			this.lblClass.Left = 0.0625F;
			this.lblClass.Name = "lblClass";
			this.lblClass.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblClass.Text = "CLASS";
			this.lblClass.Top = 0.5F;
			this.lblClass.Width = 0.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.8125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "PLATE";
			this.Label2.Top = 0.5F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.75F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label3.Text = "MVR3";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 0.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label4.Text = "STATUS";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 0.6875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.6875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label5.Text = "MO STKR";
			this.Label5.Top = 0.5F;
			this.Label5.Width = 0.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.75F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label6.Text = "YR STKR";
			this.Label6.Top = 0.5F;
			this.Label6.Width = 0.75F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.8125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label7.Text = "FEES";
			this.Label7.Top = 0.5F;
			this.Label7.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.65625F;
			this.Line1.Width = 6.46875F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 0.65625F;
			this.Line1.Y2 = 0.65625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Registration Listing";
			this.Label1.Top = 0F;
			this.Label1.Width = 6.5F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label32.Text = "Label32";
			this.Label32.Top = 0.1875F;
			this.Label32.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 5.1875F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label33.Text = "Label33";
			this.Label33.Top = 0F;
			this.Label33.Width = 1.3125F;
			// 
			// txtClass
			// 
			this.txtClass.Height = 0.1875F;
			this.txtClass.Left = 0.0625F;
			this.txtClass.Name = "txtClass";
			this.txtClass.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.txtClass.Text = "Field1";
			this.txtClass.Top = 0F;
			this.txtClass.Width = 0.6875F;
			// 
			// txtPlate
			// 
			this.txtPlate.Height = 0.1875F;
			this.txtPlate.Left = 0.875F;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.txtPlate.Text = "Field1";
			this.txtPlate.Top = 0F;
			this.txtPlate.Width = 0.6875F;
			// 
			// txtMVR3
			// 
			this.txtMVR3.Height = 0.1875F;
			this.txtMVR3.Left = 1.75F;
			this.txtMVR3.Name = "txtMVR3";
			this.txtMVR3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.txtMVR3.Text = "Field1";
			this.txtMVR3.Top = 0F;
			this.txtMVR3.Width = 0.6875F;
			// 
			// txtStatus
			// 
			this.txtStatus.Height = 0.1875F;
			this.txtStatus.Left = 2.6875F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.txtStatus.Text = "Field1";
			this.txtStatus.Top = 0F;
			this.txtStatus.Width = 0.6875F;
			// 
			// txtMO
			// 
			this.txtMO.Height = 0.1875F;
			this.txtMO.Left = 3.6875F;
			this.txtMO.Name = "txtMO";
			this.txtMO.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.txtMO.Text = "Field1";
			this.txtMO.Top = 0F;
			this.txtMO.Width = 0.6875F;
			// 
			// txtYr
			// 
			this.txtYr.Height = 0.1875F;
			this.txtYr.Left = 4.75F;
			this.txtYr.Name = "txtYr";
			this.txtYr.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.txtYr.Text = "Field1";
			this.txtYr.Top = 0F;
			this.txtYr.Width = 0.6875F;
			// 
			// txtFees
			// 
			this.txtFees.Height = 0.1875F;
			this.txtFees.Left = 5.6875F;
			this.txtFees.Name = "txtFees";
			this.txtFees.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.txtFees.Text = "Field1";
			this.txtFees.Top = 0F;
			this.txtFees.Width = 0.6875F;
			// 
			// rptRegistration
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.75F;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMVR3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMVR3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYr;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFees;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblClass;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
