﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubNonMonetary.
	/// </summary>
	partial class rptSubNonMonetary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSubNonMonetary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldReciept = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExplanation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldReciept)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExplanation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldReciept,
            this.fldClassPlate,
            this.fldExplanation,
            this.fldOpID,
            this.fldDate});
			this.Detail.Height = 0.22125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldReciept
			// 
			this.fldReciept.Height = 0.19F;
			this.fldReciept.Left = 1.09375F;
			this.fldReciept.Name = "fldReciept";
			this.fldReciept.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReciept.Text = "Field1";
			this.fldReciept.Top = 0.03125F;
			this.fldReciept.Width = 0.71875F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.19F;
			this.fldClassPlate.Left = 1.96875F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldClassPlate.Text = "Field1";
			this.fldClassPlate.Top = 0.03125F;
			this.fldClassPlate.Width = 1F;
			// 
			// fldExplanation
			// 
			this.fldExplanation.Height = 0.19F;
			this.fldExplanation.Left = 3.4375F;
			this.fldExplanation.Name = "fldExplanation";
			this.fldExplanation.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldExplanation.Text = "Field1";
			this.fldExplanation.Top = 0.03125F;
			this.fldExplanation.Width = 2.84375F;
			// 
			// fldOpID
			// 
			this.fldOpID.Height = 0.19F;
			this.fldOpID.Left = 6.59375F;
			this.fldOpID.Name = "fldOpID";
			this.fldOpID.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldOpID.Text = "Field1";
			this.fldOpID.Top = 0.03125F;
			this.fldOpID.Width = 0.5F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 0F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldDate.Top = 0.03125F;
			this.fldDate.Width = 0.844F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label2,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label10,
            this.Label11,
            this.label3});
			this.GroupHeader1.Height = 0.9375001F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.094F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.Label1.Text = "TAX RCPT";
			this.Label1.Top = 0.591F;
			this.Label1.Width = 0.71875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.969F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label2.Text = "CLASS / PLATE";
			this.Label2.Top = 0.74725F;
			this.Label2.Width = 1.15625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.43775F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label5.Text = "EXPLANATION-------------";
			this.Label5.Top = 0.74725F;
			this.Label5.Width = 2.1875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.594F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.Label6.Text = "OPID";
			this.Label6.Top = 0.74725F;
			this.Label6.Width = 0.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 1.094F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label7.Text = "NUMBER";
			this.Label7.Top = 0.74725F;
			this.Label7.Width = 0.71875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.09375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label10.Text = "---- NON-MONETARY CORRECTIONS ----";
			this.Label10.Top = 0.15625F;
			this.Label10.Width = 2.78125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.0002500415F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label11.Text = "DATE";
			this.Label11.Top = 0.74725F;
			this.Label11.Width = 0.71875F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label9,
            this.fldTotalCount});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.03125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label9.Text = "TOTAL NON-MONETARY CORRECTIONS:";
			this.Label9.Top = 0.0625F;
			this.Label9.Width = 2.5F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 2.59375F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldTotalCount.Text = "Field1";
			this.fldTotalCount.Top = 0.0625F;
			this.fldTotalCount.Width = 0.75F;
			// 
			// label3
			// 
			this.label3.Height = 0.1875F;
			this.label3.HyperLink = null;
			this.label3.Left = 1.234F;
			this.label3.Name = "label3";
			this.label3.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.label3.Text = "(ALL FORMS MUST BE SENT TO BUREAU OF MOTOR VEHICLES) ";
			this.label3.Top = 0.344F;
			this.label3.Width = 4.50025F;
			// 
			// rptSubNonMonetary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.385417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldReciept)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExplanation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReciept;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExplanation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label label3;
	}
}
