//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptMVR3.
	/// </summary>
	public partial class rptMVR3 : FCSectionReport
	{
		public rptMVR3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "MVR3";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMVR3 InstancePtr
		{
			get
			{
				return (rptMVR3)Sys.GetInstance(typeof(rptMVR3));
			}
		}

		protected rptMVR3 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMVR3	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		bool blnFirstRecord;
		public bool blnTest;
		private bool boolIsPreReg;

		public bool IsPreReg
		{
			set
			{
				boolIsPreReg = value;
			}
			get
			{
				bool IsPreReg = false;
				IsPreReg = boolIsPreReg;
				return IsPreReg;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (MotorVehicle.Statics.gboolFromBatchRegistration)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= frmBatchProcessing.InstancePtr.intNumberToPrint)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			if (eArgs.EOF != true)
			{
				if (MotorVehicle.Statics.gboolFromBatchRegistration)
				{
					BatchValidationLine(FCConvert.ToInt32(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3Col)));
				}
			}
		}
		// vbPorter upgrade warning: strMVR3 As int	OnWrite(string)
		private void BatchValidationLine(int strMVR3)
		{
			clsDRWrapper rsFees = new clsDRWrapper();
			string strFee = "";
			clsDRWrapper rsClass = new clsDRWrapper();
			if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO") == false)
			{
				if (MotorVehicle.Statics.ResidenceCode.Length < 5)
				{
					MotorVehicle.Statics.Validation = "0" + MotorVehicle.Statics.ResidenceCode;
				}
				else
				{
					MotorVehicle.Statics.Validation = MotorVehicle.Statics.ResidenceCode;
				}
				MotorVehicle.Statics.Validation += "/" + Strings.Format(DateTime.Now, "MMddyy");
				if (MotorVehicle.Statics.blnPrintingDupReg)
				{
					MotorVehicle.Statics.Validation += "/" + FCConvert.ToString(modGlobalRoutines.PadToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("NoFeeMVR3Number"), 8));
				}
				else
				{
					MotorVehicle.Statics.Validation += "/" + FCConvert.ToString(modGlobalRoutines.PadToString(strMVR3, 8));
				}
				if (MotorVehicle.Statics.TownLevel != 9)
				{
					if (MotorVehicle.Statics.RegistrationType == "LOST")
					{
						MotorVehicle.Statics.Validation += "/$" + Strings.Format((MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee")) * 100, "0000000");
					}
					else if (MotorVehicle.Statics.RegistrationType == "DUPREG")
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "DV")
						{
							MotorVehicle.Statics.Validation += "/$" + "0000000";
						}
						else
						{
							rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
							if (rsFees.EndOfFile() != true && rsFees.BeginningOfFile() != true)
							{
								rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
								if (FCConvert.ToInt32(rsClass.Get_Fields("RegistrationFee")) != 0)
								{
									strFee = fecherFoundation.Strings.Trim(Conversion.Str((rsFees.Get_Fields_Decimal("DupRegAgentFee") + rsFees.Get_Fields_Decimal("DuplicateRegistration")) * 100));
								}
								else
								{
									strFee = fecherFoundation.Strings.Trim(Conversion.Str(rsFees.Get_Fields_Decimal("DupRegAgentFee") * 100));
								}
								if (strFee.Length == 3)
								{
									MotorVehicle.Statics.Validation += "/$" + "0000" + strFee;
								}
								else
								{
									MotorVehicle.Statics.Validation += "/$" + "000" + strFee;
								}
							}
							else
							{
								MotorVehicle.Statics.Validation += "/$" + "0000300";
							}
						}
					}
					else if (MotorVehicle.Statics.ExciseAP)
					{
						MotorVehicle.Statics.Validation += "/$" + Strings.Format(MotorVehicle.Statics.GrandTotal * 100, "0000000");
					}
					else if (MotorVehicle.Statics.RegistrationType == "CORR")
					{
						MotorVehicle.Statics.Validation += "/$" + Strings.Format((MotorVehicle.Statics.ECTotal * 100), "0000000");
					}
					else
					{
						if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "PROCUREMENT SPR" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER")
						{
							MotorVehicle.Statics.Validation += "/$" + Strings.Format((MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") + MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid") - MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee")) * 100, "0000000");
						}
						else
						{
							MotorVehicle.Statics.Validation += "/$" + Strings.Format((MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") + MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid")) * 100, "0000000");
						}
					}
				}
			}
			else
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP")
				{
					MotorVehicle.Statics.Validation = "Excise Tax Only - IRP";
				}
				else
				{
					MotorVehicle.Statics.Validation = "Excise Tax Receipt";
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			// set printer for report to use
			// MsgBox "4"
			/*? For Each */
			foreach (FCPrinter p in FCGlobal.Printers)
			{
				// kk04112017 Drop support for Win95/Sysinfo control
				// If MDIParent.SysInfo1.OSPlatform > 1 Then
				if (p.DeviceName == MotorVehicle.Statics.MVR3PrinterName)
				{
					if (tempPrinter.DriverName == "EPSON24")
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 12cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
						}
					}
					break;
				}
			}
			// Else
			// If tempFCGlobal.FCGlobal.Printer.DeviceName = MVR3PrinterName Then
			// rptMVR3.FCGlobal.FCGlobal.Printer.DeviceName = tempFCGlobal.FCGlobal.Printer.DeviceName
			// If tempPrinter.DriverName = "EPSON24" Then
			// For x = 0 To Me.Detail.Controls.Count - 1
			// Me.Detail.Controls[x].Font.Name = "Courier 12cpi"
			// Next
			// End If
			// Exit For
			// End If
			// End If
			/*? Next */// MsgBox "5"
			//rptMVR3.InstancePtr.Printer.RenderMode = 1;
			// MsgBox "6"
			if (MotorVehicle.Statics.gboolFromBatchRegistration)
			{
				blnFirstRecord = true;
				intCounter = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldValidation3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldFuel As object	OnWrite(string)
			// vbPorter upgrade warning: fldOwner3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldDOB3 As object	OnWrite(string)
			string c = "";
			// vbPorter upgrade warning: SubTotal As Decimal	OnWrite(Decimal, double, int)
			Decimal SubTotal = 0;
			// vbPorter upgrade warning: ExciseBal As Decimal	OnWrite(double, int, Decimal)
			Decimal ExciseBal;
			string MVTyp = "";
			string Info = "";
			string CL1 = "";
			string CL2 = "";
			string PRINTOWNER1 = "";
			string PRINTOWNER2 = "";
			string PRINTOWNER3 = "";
			string strLessor = "";
			double Mrate = 0;
			// vbPorter upgrade warning: Credit As Decimal	OnWriteFCConvert.ToDouble(
			Decimal Credit;
			// vbPorter upgrade warning: RegFee As Decimal	OnWrite(double, int, Decimal)
			Decimal RegFee;
			string Town = "";
			string SD = "";
			string printfile = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			MotorVehicle.Statics.PrintMVR3 = false;
			fecherFoundation.Information.Err().Clear();
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				// Check Printer Alignment
				// MsgBox "7"
				if (!MotorVehicle.Statics.gboolFromBatchRegistration)
				{
					if (blnTest)
					{
						if (MotorVehicle.CheckPrintAlignment() == false)
						{
							MotorVehicle.Statics.PrintMVR3 = false;
							rptMVR3.InstancePtr.Cancel();
							return;
						}
					}
					else
					{
						if (!frmSavePrint.InstancePtr.blnExport)
						{
							if (MotorVehicle.CheckPrintAlignment() == false)
							{
								MotorVehicle.Statics.PrintMVR3 = false;
								rptMVR3.InstancePtr.Cancel();
								return;
							}
						}
					}
					// Check to see if errors have occured
					if (fecherFoundation.Information.Err().Number != 0)
					{
						MotorVehicle.PrinterError("MVR3 Printer");
						MotorVehicle.Statics.PrintMVR3 = false;
						rptMVR3.InstancePtr.Cancel();
						return;
					}
					// MsgBox "8"
					// kk04112017 Drop support for Win95/Sysinfo control
					// If MDIParent.SysInfo1.OSPlatform > 1 Then
					//if (MotorVehicle.blnWindowsXP) {
					//	modCustomPageSize.SelectForm("MVR3", this.Handle.ToInt32(), 215900, 140000);
					//} else if (MotorVehicle.blnWindows7 && MotorVehicle.Statics.blnLQPrinter) {
					//	modCustomPageSize.SelectForm("MVR3", this.Handle.ToInt32(), 215900, 139700);
					//} else {
					//	modCustomPageSize.SelectForm("MVR3", this.Handle.ToInt32(), 215900, 140250);
					//}
					MotorVehicle.NewSetReportForm(this);
					// Else
					// set size of paper to the size of an MVR3 form
					// rptMVR3.Printer.PaperSize = 255
					// rptMVR3.Printer.PaperHeight = 8145
					// rptMVR3.Printer.PaperWidth = 12240
					// End If
				}
				else
				{
					if (intCounter == 0)
					{
						if (MotorVehicle.CheckPrintAlignment() == false)
						{
							MotorVehicle.Statics.PrintMVR3 = false;
							rptMVR3.InstancePtr.Cancel();
							return;
						}
					}
					// Check to see if errors have occured
					if (fecherFoundation.Information.Err().Number != 0)
					{
						MotorVehicle.PrinterError("MVR3 Printer");
						MotorVehicle.Statics.PrintMVR3 = false;
						rptMVR3.InstancePtr.Cancel();
						return;
					}
					// 
					// kk04112017 Drop support for Win95/Sysinfo control
					// If MDIParent.SysInfo1.OSPlatform > 1 Then
					//if (MotorVehicle.blnWindowsXP) {
					//	modCustomPageSize.SelectForm("MVR3", this.Handle.ToInt32(), 215900, 140000);
					//} else if (MotorVehicle.blnWindows7 && MotorVehicle.Statics.blnLQPrinter) {
					//	modCustomPageSize.SelectForm("MVR3", this.Handle.ToInt32(), 215900, 139700);
					//} else {
					//	modCustomPageSize.SelectForm("MVR3", this.Handle.ToInt32(), 215900, 140250);
					//}
					MotorVehicle.NewSetReportForm(this);
					// Else
					// set size of paper to the size of an MVR3 form
					// rptMVR3.Printer.PaperSize = 255
					// rptMVR3.Printer.PaperHeight = 8145
					// rptMVR3.Printer.PaperWidth = 12240
					// End If
				}
				// MVPrinter.Write Chr(27) + "0"
				// MsgBox "9"
				// fill in information to be printed
				if (!blnTest)
				{
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
					{
						// do nothing
					}
					else
					{
						if (MotorVehicle.Statics.gboolFromBatchRegistration)
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP")
							{
								MotorVehicle.Statics.Validation = "Excise Tax Only - IRP\\" + frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3Col);
							}
							else
							{
								MotorVehicle.Statics.Validation = "Excise Tax Receipt\\" + frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3Col);
							}
						}
						else
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP")
							{
								MotorVehicle.Statics.Validation = "Excise Tax Only - IRP\\" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");
							}
							else
							{
								MotorVehicle.Statics.Validation = "Excise Tax Receipt\\" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");
							}
						}
					}
					string countychecker = "";
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode")).Length < 5)
					{
						countychecker = "0" + MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
					}
					else
					{
						countychecker = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
					}
					if (MotorVehicle.Statics.blnPrintingDupReg)
					{
						fldCheckDigits.Text = Strings.Right(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("NoFeeMVR3Number").ToString(), 2);
					}
					else
					{
						if (MotorVehicle.Statics.gboolFromBatchRegistration)
						{
							fldCheckDigits.Text = Strings.Right(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3Col), 2);
						}
						else
						{
							fldCheckDigits.Text = Strings.Right(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3").ToString(), 2);
						}
					}
					if (Strings.Left(countychecker, 2) == "05")
					{
						c = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
						if (c != "AQ" && c != "CL" && c != "CV" && c != "HC" && c != "IU" && c != "LS" && c != "MC" && c != "MM" && c != "MP" && c != "MQ" && c != "MX" && c != "PM" && c != "SE" && c != "SR" && c != "TC" && c != "TL" && c != "TR" && c != "TT" && c != "VM" && c != "XV")
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel")) != "D" && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("Year")) > 1973)
							{
								fldCumberlandCounty.Visible = true;
							}
							else
							{
								fldCumberlandCounty.Visible = false;
							}
						}
						else
						{
							fldCumberlandCounty.Visible = false;
						}
					}
					else
					{
						fldCumberlandCounty.Visible = false;
					}
					if (!MotorVehicle.Statics.gboolFromBatchRegistration)
					{
						if (MotorVehicle.Statics.blnPrintingDupReg)
						{
							fldReason1.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP2Text);
						}
						else
						{
							fldReason1.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP3Text);
						}
						// 6
						if (MotorVehicle.Statics.blnPrintingDupReg)
						{
							fldReason2.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP1Text);
						}
						else
						{
							fldReason2.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP2Text);
						}
						if (MotorVehicle.Statics.blnPrintingDupReg)
						{
							fldReason3.Text = "DUP REG NO FEE";
						}
						else
						{
							fldReason3.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP1Text);
						}
					}
					else
					{
						if (fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col)) != "")
						{
							if (Strings.Left(MotorVehicle.Statics.PP1Text, 4) == "UNIT")
							{
								MotorVehicle.Statics.PP1Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
							}
							else if (Strings.Left(MotorVehicle.Statics.PP2Text, 4) == "UNIT")
							{
								MotorVehicle.Statics.PP2Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
							}
							else if (Strings.Left(MotorVehicle.Statics.PP3Text, 4) == "UNIT")
							{
								MotorVehicle.Statics.PP3Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
							}
							else
							{
								if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP1Text) == "")
								{
									MotorVehicle.Statics.PP1Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
								}
								else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP2Text) == "")
								{
									MotorVehicle.Statics.PP2Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
								}
								else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP3Text) == "")
								{
									MotorVehicle.Statics.PP3Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
								}
							}
						}
						else
						{
							if (Strings.Left(MotorVehicle.Statics.PP1Text, 4) == "UNIT")
							{
								MotorVehicle.Statics.PP1Text = "";
							}
							else if (Strings.Left(MotorVehicle.Statics.PP2Text, 4) == "UNIT")
							{
								MotorVehicle.Statics.PP2Text = "";
							}
							else if (Strings.Left(MotorVehicle.Statics.PP3Text, 4) == "UNIT")
							{
								MotorVehicle.Statics.PP3Text = "";
							}
						}
						fldReason1.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP3Text);
						fldReason2.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP2Text);
						fldReason3.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP1Text);
					}
					MVTyp = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")), 3);
					MotorVehicle.Statics.RegType = "..";
					if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO") == false)
					{
						if (MVTyp == "NRR" || MVTyp == "NRT")
						{
							MotorVehicle.Statics.RegType = "E-New-Reg ";
						}
						else if (MVTyp == "RRR" || MVTyp == "RRT")
						{
							// kk07182016 Can't hardcode Dup to be E-Re-Reg    Or RegistrationType = "DUPREG" Then
							MotorVehicle.Statics.RegType = "E-Re-reg";
						}
						else if (MVTyp == "ECO" || MVTyp == "ECR" || MVTyp == "LPS" || MVTyp == "GVW")
						{
							MotorVehicle.Statics.RegType = "E-Correct";
						}
						else if (MVTyp == "DPR")
						{
							// kk07182016  Get reg type from original
							if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "NRR" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "NRT")
							{
								MotorVehicle.Statics.RegType = "E-New-Reg";
							}
							else if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRR" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRT")
							{
								MotorVehicle.Statics.RegType = "E-Re-reg";
							}
							else if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "ECO" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "ECR" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "LPS" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "GVW")
							{
								MotorVehicle.Statics.RegType = "E-Correct";
							}
						}
					}
					else
					{
						if (MVTyp == "NRR" || MVTyp == "NRT")
						{
							MotorVehicle.Statics.RegType = "New-Reg ";
						}
						else if (MVTyp == "RRR" || MVTyp == "RRT")
						{
							MotorVehicle.Statics.RegType = "Re-reg";
						}
						else if (MVTyp == "ECO")
						{
							MotorVehicle.Statics.RegType = "Correct";
						}
					}
					fldValidation1.Text = MotorVehicle.Statics.RegType;
					// 8
					if (MotorVehicle.Statics.blnPrintingDupReg)
					{
						if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg")))
						{
							// kk04202018 tromvs-90  Need to rebuild validation for no fee duplicate
							fldValidation2.Text = Strings.Left(MotorVehicle.Statics.Validation, 15) + FCConvert.ToString(modGlobalRoutines.PadToString(FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("NoFeeMVR3Number"))), 8)) + "/$000000000";
						}
						else
						{
							fldValidation2.Text = Strings.Left(MotorVehicle.Statics.Validation, MotorVehicle.Statics.Validation.Length - 7) + "0000000";
						}
					}
					else
					{
						fldValidation2.Text = MotorVehicle.Statics.Validation;
					}
					// 9
					if (MotorVehicle.Statics.blnPrintingDupReg)
					{
						if (MotorVehicle.Statics.RRTextAll == "")
						{
							fldValidation3.Text = "D " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");
						}
						else
						{
							fldValidation3.Text = MotorVehicle.Statics.RRTextAll + ",D " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");
						}
					}
					else
					{
						fldValidation3.Text = MotorVehicle.Statics.RRTextAll;
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) != true)
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer")) != 0)
						{
							lblMileage.Visible = true;
							fldMileage.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer"), "#,###,###"), "@@@@@@@@@");
						}
					}
					else
					{
						lblMileage.Visible = false;
					}
					fldClass.Text = "";
					fldPlate.Text = "";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))) != "NEW")
					{
						fldClass.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
						if (MotorVehicle.Statics.gboolFromBatchRegistration)
						{
							fldPlate.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPlateCol);
						}
						else
						{
							fldPlate.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
						}
					}
					else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP")
					{
						// kk08252016 tromv-1202  Class should print on NEW ETO for AP
						fldClass.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
					}
					// 11
					// kk07182016  Always print Effective Date, Always print Expiration Date Except NEW, ETO and NOT AP
					// kk10012016  tromvs-57  Per Sue McCormick BMV - No Effective Date on ETO - NRT, NRR or RRT unless AP
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) && (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP")
					{
						fldEffective.Text = "";
					}
					else
					{
						fldEffective.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"), "MM/dd/yyyy");
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == true && MotorVehicle.Statics.RegistrationType == "NRR" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP")
					{
						fldExpires.Text = "";
					}
					else
					{
						fldExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
					}
					// If rsFinal.Fields("ETO") And RegistrationType = "NRR" And rsFinal.Fields("FleetNumber") = 0 Then
					// do nothing
					// Else
					// If Not rsFinal.Fields("ETO") Then
					// If TownLevel = 9 And RegistrationType = "NRR" Then
					// do nothing
					// Else
					// fldEffective.Text = Format(rsFinal.Fields("EffectiveDate"), "MM/dd/yyyy")
					// End If
					// Else
					// If RegistrationType = "RRR" Or RegistrationType = "RRT" Then
					// fldEffective.Text = Format(rsFinal.Fields("EffectiveDate"), "MM/dd/yyyy")
					// End If
					// End If
					// fldExpires.Text = Format(rsFinal.Fields("ExpireDate"), "MM/dd/yyyy")
					// End If
					// MsgBox "10"
					if (MotorVehicle.Statics.gboolFromBatchRegistration)
					{
						fldVIN.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intVINCol);
						fldYear.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intYearCol);
						fldMake.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMakeCol);
						fldModel.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intModelCol);
						fldStyle.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intStyleCol);
						fldAxles.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intAxlesCol);
					}
					else
					{
						fldVIN.Text = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")), 17);
						fldYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
						fldMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make"));
						fldModel.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("model"));
						fldStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles")) != true)
						{
							if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles")) != 0)
							{
								fldAxles.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles"));
							}
						}
					}
					fldColor1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("color1");
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2")) != "NA")
					{
						fldColor2.Text = "/" + MotorVehicle.Statics.rsFinal.Get_Fields_String("color2");
					}
					if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires")) != true)
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires")) != 0)
						{
							fldTires.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires"));
						}
					}
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "SE" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP")
					{
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != true)
						{
							if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != 0)
							{
								fldNetWeight.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"), "######"), "@@@@@@");
							}
						}
					}
					if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")) != true)
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")) != 0)
						{
							// CL1$ = rsFinal.Fields("Subclass")
							// CL2$ = rsFinal.Fields("Class")
							// If CL1$ = "F2" Or CL1$ = "T1" Or CL1$ = "T3" Or CL1$ = "L1" Or CL1$ = "L2" Or CL1$ = "L7" Or CL1$ = "C1" Or CL1$ = "C2" Or CL1$ = "C6" Or CL1$ = "V2" Then
							// fldRegisteredWeight.Text = Format(Format(rsFinal.Fields("RegisteredWeightNew"), "######"), "@@@@@@")
							// ElseIf CL2$ = "RV" Or CL2$ = "AP" Or CL2$ = "CO" Or CL2$ = "CC" Or CL2$ = "FM" Or CL2$ = "MH" Or CL2$ = "WX" Or CL2$ = "CI" Then
							fldRegisteredWeight.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew"), "######"), "@@@@@@");
							// End If
						}
					}
					string vbPorterVar = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");
					if ((vbPorterVar == "SE") || (vbPorterVar == "TC") || (vbPorterVar == "TL") || (vbPorterVar == "CL") || (vbPorterVar == "HC"))
					{
						fldFuel.Text = "";
					}
					else
					{
						fldFuel.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel"));
					}
					PRINTOWNER1 = MotorVehicle.GetPartyName(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), true);
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "E")
					{
						PRINTOWNER1 += " -Lessee";
					}
					else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "T" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I")
					{
						PRINTOWNER1 += " -Trustee";
					}
					else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "P")
					{
						PRINTOWNER1 += " -Personal Representative";
					}
					if (PRINTOWNER1.Length > 39)
						PRINTOWNER1 = Strings.Mid(PRINTOWNER1, 1, 39);
					fldOwner1.Text = PRINTOWNER1;
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I")
					{
						fldDOB1.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1"), "MM/dd/yyyy");
					}
					else
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber")) != 0)
						{
							fldDOB1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber"));
						}
					}
					// kk07182016  Change format if Sale Price was used as base
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("BaseIsSalePrice")))
					{
						fldBase.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"), "###,###,###.00"), "@@@@@@@@@@@@@@");
					}
					else
					{
						fldBase.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"), "###,###,###"), "@@@@@@@@@@@");
					}
					// 21
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) != "N")
					{
						// MAXLN = 39
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "D")
						{
							PRINTOWNER2 = "DBA ";
						}
						else
						{
							PRINTOWNER2 = "";
						}
						PRINTOWNER2 += fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"), true));
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "E")
						{
							PRINTOWNER2 += " -Lessee";
						}
						else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "T" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "I")
						{
							PRINTOWNER2 += " -Trustee";
						}
						else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "P")
						{
							PRINTOWNER2 += " -Personal Representative";
						}
						if (PRINTOWNER2.Length > 39)
							PRINTOWNER2 = Strings.Mid(PRINTOWNER2, 1, 39);
						fldOwner2.Text = PRINTOWNER2;
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "I")
						{
							fldDOB2.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2"), "MM/dd/yyyy");
						}
						else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1") == "I")
						{
							if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber")) == false && Convert.ToBoolean(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber") + "")))
							{
								fldDOB2.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber");
							}
						}
					}
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) != "N")
					{
						// MAXLN = 39
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "D")
						{
							PRINTOWNER3 = "DBA ";
						}
						else
						{
							PRINTOWNER3 = "";
						}
						PRINTOWNER3 += fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID3"), true));
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "E")
						{
							PRINTOWNER3 += " -Lessee";
						}
						else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "T" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "I")
						{
							PRINTOWNER3 += " -Trustee";
						}
						else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3") == "P")
						{
							PRINTOWNER3 += " -Personal Representative";
						}
						if (PRINTOWNER3.Length > 39)
							PRINTOWNER3 = Strings.Mid(PRINTOWNER3, 1, 39);
						fldOwner3.Text = PRINTOWNER3;
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "I")
						{
							fldDOB3.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3"), "MM/dd/yyyy");
						}
						else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I" && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "I" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "N"))
						{
							if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber")) == false && Convert.ToBoolean(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber") + "")))
							{
								fldDOB3.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber"));
							}
						}
					}
					Mrate = 0;
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "1")
						Mrate = 0.024;
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "2")
						Mrate = 0.0175;
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "3")
						Mrate = 0.0135;
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "4")
						Mrate = 0.01;
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "5")
						Mrate = 0.0065;
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "6")
						Mrate = 0.004;
					fldMil.Text = Strings.Format(Strings.Format(Mrate, "####0.0000"), "@@@@@@@@@@");
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
					{
						if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") != MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") || (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"))) > 0)
						{
							if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) != 0)
							{
								if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")) == true)
								{
									if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "NRT")
									{
										fldRegFee.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00"), "@@@@@@@");
										fldRegFeeHR.Text = "HR";
									}
									else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "ECR")
									{
										if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class"))
										{
											// XXXX                                boolShowFees = True
											fldRegFee.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00"), "@@@@@@@");
											fldRegFeeHR.Text = "HR";
										}
									}
									else
									{
										fldRegFee.Text = "";
										// fldRegFeeHR.Text = "HR"
									}
								}
								else
								{
									// kk07182016 per BMV - don't print P/R on Rate
									// If rsFinal.Fields("RegProrate") = True Then
									// fldRegFeeHR.Text = "P/R"
									// End If
									fldRegFee.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00"), "@@@@@@@");
								}
							}
							else
							{
								if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PH" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PM")
								{
									fldRegFee.Text = "EXEMPT";
								}
							}
						}
					}
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "R")
					{
						strLessor = fldOwner1.Text;
						fldOwner1.Text = fldOwner2.Text;
						fldOwner2.Text = fldOwner3.Text;
						fldDOB1.Text = fldDOB2.Text;
						fldDOB2.Text = fldDOB3.Text;
						fldOwner3.Text = "";
						fldDOB3.Text = "";
					}
					else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2") == "R")
					{
						strLessor = fldOwner2.Text;
						fldOwner2.Text = fldOwner3.Text;
						fldDOB2.Text = fldDOB3.Text;
						fldOwner3.Text = "";
						fldDOB3.Text = "";
					}
					else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3") == "R")
					{
						strLessor = fldOwner3.Text;
						fldOwner3.Text = "";
						fldDOB3.Text = "";
					}
					else
					{
						strLessor = "";
					}
					if (strLessor != "")
					{
						if (fecherFoundation.Strings.Trim(strLessor).Length > 33)
						{
							fldLessor.Text = Strings.Mid(strLessor, 1, 33);
						}
						else
						{
							fldLessor.Text = strLessor;
						}
					}
					if (MotorVehicle.Statics.gboolFromBatchRegistration)
					{
						if (frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUnitCol).Length > 12)
						{
							fldUnit.Text = Strings.Left(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUnitCol), 12);
						}
						else
						{
							fldUnit.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUnitCol);
						}
					}
					else
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) != "...." && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) != "    ")
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")).Length > 12)
							{
								fldUnit.Text = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")), 12);
							}
							else
							{
								fldUnit.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
							}
						}
					}
					if (Strings.Mid(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber")), 1, 1) != "." && fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber")) != true)
					{
						fldDOTNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber"));
					}
					if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee")) != 0 && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "PROCUREMENT SPR" && modGlobalConstants.Statics.MuniName != "MAINE TRAILER" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ACE REGISTRATION SERVICES LLC" && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "COUNTRYWIDE TRAILER")
					{
						fldAgentFee.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee"), "###0.00"), "@@@@@@@");
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
					{
						// State registration credit amount
						Credit = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")));
						// commercial credit is automatically added into the state credit.   09/11
						if (Credit != 0)
						{
							if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")) == true)
							{
								if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "NRT")
								{
									fldRegCreditHR.Text = "HR";
									fldRegCredit.Text = Strings.Format(Strings.Format(Credit, "###0.00"), "@@@@@@@");
								}
								else
								{
									if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class"))
									{
										// XXX                            boolShowFees = True
										fldRegCreditHR.Text = "HR";
										fldRegCredit.Text = Strings.Format(Strings.Format(Credit, "###0.00"), "@@@@@@@");
									}
									else
									{
										// fldRegCreditHR.Text = "HR"
										fldRegCredit.Text = "";
									}
								}
							}
							else
							{
								if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegProrate")) == true)
								{
									fldRegCreditHR.Text = "P/R";
								}
								fldRegCredit.Text = Strings.Format(Strings.Format(Credit, "###0.00"), "@@@@@@@");
							}
						}
					}
					// MsgBox "11"
					if (MotorVehicle.Statics.ExciseAP)
					{
						// Year(rsFinal.fields("ExcisePaidDate")) > 1980 Then
						fldExciseHR.Text = "AP";
					}
					else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")) == true && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
					{
						fldExciseHR.Text = "HR";
					}
					else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")) == true)
					{
						fldExciseHR.Text = "P/R";
					}
					else if (frmPreview.InstancePtr.lblExciseDiff.Visible == true)
					{
						fldExciseHR.Text = "*";
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseExempt")) == true && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
					{
						if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "PROCUREMENT SPR")
						{
							fldExcise.Text = "EXEMPT";
						}
					}
					else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer") != "U")
					{
						if (MotorVehicle.Statics.ExciseAP)
						{
							fldExcise.Text = Strings.Format(frmPreview.InstancePtr.txtExciseTax.Text, "@@@@@@@@@");
						}
						else
						{
							fldExcise.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"), "##,##0.00"), "@@@@@@@@@");
						}
					}
					else
					{
						fldExcise.Text = "    N/A";
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
					{
						// local excise credit amount
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) == true)
							MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", 0);
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) == true)
							MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")) == true)
							MotorVehicle.Statics.rsFinal.Set_Fields("GIFTCERTIFICATEAMOUNT", 0);
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")) == true)
							MotorVehicle.Statics.rsFinal.Set_Fields("CommercialCredit", 0);
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")) == true)
							MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", 0);
						RegFee = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")));
						if (RegFee < 0)
							RegFee = 0;
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher")) != "V")
						{
							RegFee += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")));
						}
						else
						{
							RegFee += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")));
						}
						if (RegFee != 0)
						{
							if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")) == true)
							{
								fldFeesHR.Text = "HR";
							}
							else
							{
								if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegProrate")) == true)
								{
									fldFeesHR.Text = "P/R";
								}
							}
						}
						fldFees.Text = Strings.Format(Strings.Format(RegFee, "#,##0.00"), "@@@@@@@");
					}
					// kk07182016  Correct printing for suspended reg
					if (!MotorVehicle.Statics.blnSuspended)
					{
						fldAddress.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
						fldAddress2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address2"));
						fldCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State")) != "CC")
						{
							fldState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State"));
						}
						fldZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
					}
					else
					{
						fldAddress.Text = "REGISTRATION SUSPENDED";
						fldAddress2.Text = "CONTACT: (207) 624-9000 EXT 52143";
						fldCity.Text = "DO NOT ISSUE";
						fldState.Text = "";
						fldZip.Text = "";
					}
					if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) != 0)
					{
						if (MotorVehicle.Statics.ExciseAP)
						{
							fldExciseCreditHR.Text = "AP";
						}
						else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")) == true && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
						{
							fldExciseCreditHR.Text = "HR";
						}
						else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")) == true)
						{
							fldExciseCreditHR.Text = "P/R";
						}
						// kk04142017 tromv-1116  Add handling for duplicate reg
						if (MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "NRT" || (MotorVehicle.Statics.RegistrationType == "DUPREG" && (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "NRT")))
						{
							if (MotorVehicle.Statics.blnAllowExciseRebates)
							{
								// kk04142017 tromv-1116  Only show # if credit was rebated
								if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged")) < Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")))
								{
									fldExciseCreditHR.Text = "#" + fldExciseCreditHR.Text;
								}
							}
						}
						// kk04142017 tromv-1116  Force credit to equal the Excise Tax if rebate was done
						if (Strings.Left(fldExciseCreditHR.Text, 1) == "#")
						{
							fldExciseCredit.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"), "##,##0.00"), "@@@@@@@@@");
						}
						else
						{
							fldExciseCredit.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "##,##0.00"), "@@@@@@@@@");
						}
					}
					// kk01172018 tromv-1292  Reworked to handle No Fee and Dealer sales tax
					if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax")) != 0 || ((MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT") && (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone")) || FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("DealerSalesTax")) || FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("SalesTaxExempt")))))
					{
						if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("DealerSalesTax")))
						{
							fldSalesTax.Text = Strings.Format("DLR", "@@@@@@@@@");
						}
						else if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax")) == 0)
						{
							fldSalesTax.Text = Strings.Format("NO FEE", "@@@@@@@@@");
						}
						else
						{
							fldSalesTax.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"), "##,##0.00"), "@@@@@@@@@");
						}
					}
					if (MotorVehicle.Statics.ExciseAP)
					{
						if (Information.IsNumeric(frmPreview.InstancePtr.txtSubTotal.Text))
						{
							SubTotal = FCConvert.ToDecimal(frmPreview.InstancePtr.txtSubTotal.Text);
						}
					}
					else
					{
						SubTotal = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")));
					}
					if (SubTotal < 0)
						SubTotal = 0;
					if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U" && SubTotal != 0) || Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) != 0)
					{
						if (MotorVehicle.Statics.ExciseAP)
						{
							fldSubtotalHR.Text = "AP";
						}
						else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")) == true && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
						{
							fldSubtotalHR.Text = "HR";
						}
						else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")) == true)
						{
							fldSubtotalHR.Text = "P/R";
						}
						fldSubtotal.Text = Strings.Format(Strings.Format(SubTotal, "##,##0.00"), "@@@@@@@@@");
					}
					// kk01162018 tromv-1292  Reworked to handle No Fee cta on MVR3
					if (MVTyp == "RRR" || MVTyp == "RRT" || MVTyp == "NRR" || MVTyp == "NRT" || MVTyp == "ECO" || MVTyp == "DPR")
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee")) != 0)
						{
							fldTitleFee.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"), "#,##0.00"), "@@@@@@@@@");
						}
						else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeCTA")) && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee")) == 0 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber")).Length > 1 || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber")).Length > 1))
						{
							fldTitleFee.Text = Strings.Format("NO FEE", "@@@@@@@@@");
						}
					}
					if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge")))
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge")) != 0)
						{
							fldTransferFee.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "##,##0.00"), "@@@@@@@@@");
						}
					}
					if (MotorVehicle.Statics.gboolFromBatchRegistration)
					{
						fldCTANumber.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intCTACol);
					}
					else
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber"))) == "")
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber")).Length > 1)
							{
								fldCTANumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber"));
							}
						}
						else
						{
							fldCTANumber.Top = 4770 / 1440F;
							fldDoubleCTA.Top = 4995 / 1440F;
							fldDoubleCTA.Visible = true;
							fldCTANumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber"));
							fldDoubleCTA.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber"));
						}
					}
					// MsgBox "12"
					fldResidenceAddress.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceAddress"));
					fldResidenceCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
					fldResidenceState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode")) != "00000")
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode")).Length < 5)
						{
							fldResidenceCode.Text = "0" + FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
						}
						else
						{
							fldResidenceCode.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
						}
					}
					else
					{
						fldResidenceCode.Text = " N/A ";
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false && MotorVehicle.Statics.TownLevel != 9 && MotorVehicle.Statics.TownLevel != 6)
					{
						fldValidated.Visible = true;
					}
					else
					{
						fldValidated.Visible = false;
					}
					ExciseBal = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")));
					if (ExciseBal < 0)
						ExciseBal = 0;
					ExciseBal += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge")));
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false && MotorVehicle.Statics.TownLevel != 9 && MotorVehicle.Statics.TownLevel != 6)
					{
						fldRegistration.Visible = true;
					}
					else
					{
						fldRegistration.Visible = false;
					}
					if (MotorVehicle.Statics.ExciseAP)
					{
						fldExciseTotalHR.Text = "AP";
					}
					else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")) == true && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
					{
						fldExciseTotalHR.Text = "HR";
					}
					else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")) == true)
					{
						fldExciseTotalHR.Text = "P/R";
					}
					if (MotorVehicle.Statics.ExciseAP)
					{
						fldBalance.Text = Strings.Format(frmPreview.InstancePtr.txtBalance.Text, "@@@@@@@@@");
					}
					else
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U" && ExciseBal != 0)
						{
							fldBalance.Text = Strings.Format(Strings.Format(ExciseBal, "##,##0.00"), "@@@@@@@@@");
						}
					}
					if (MotorVehicle.Statics.RegistrationType == "DUPREG")
					{
						fldOpID.Text = frmPreview.InstancePtr.txtUserID.Text;
					}
					else
					{
						fldOpID.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("InsuranceInitials");
					}
					if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT")
					{
						Town = "MMTA SERVICES INC";
					}
					else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HANCOCK COUNTY")
					{
						Town = "FLETCHERS LANDING";
					}
					else
					{
						Town = modGlobalConstants.Statics.MuniName;
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false && MotorVehicle.Statics.TownLevel != 9 && MotorVehicle.Statics.TownLevel != 6)
					{
						fldRegistrationCity.Text = Town;
					}
					if (MotorVehicle.Statics.gboolFromBatchRegistration)
					{
						fldMonthSticker.Text = Strings.Left(frmPreview.InstancePtr.lblNumberofMonth.Text, 3) + FCConvert.ToString(MotorVehicle.strPadZeros(fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMonthStickerCol)), 7));
					}
					else
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber")) != 0)
						{
							fldMonthSticker.Text = frmPreview.InstancePtr.lblNumberofMonth;
						}
					}
					// 12/18/12 Per Natalie @ BMV Credi tNumber can only be shown for transfers not Excise AP transactions
					if (MVTyp == "RRT" || MVTyp == "NRT" || MVTyp == "ECR" || MVTyp == "ECO")
					{
						// Or ExciseAP
						if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3")) != true && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3")) != 0)
						{
							fldCreditNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3"));
						}
						else
						{
							fldCreditNumber.Text = "";
						}
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false && MotorVehicle.Statics.TownLevel != 9 && MotorVehicle.Statics.TownLevel != 6)
					{
						if (MotorVehicle.Statics.ResidenceCode.Length < 5)
						{
							fldRegistrationCode.Text = "0" + MotorVehicle.Statics.ResidenceCode;
						}
						else
						{
							fldRegistrationCode.Text = MotorVehicle.Statics.ResidenceCode;
						}
						if (!IsPreReg)
						{
							fldRegistrationDate.Text = Strings.Format(DateTime.Now, "MMddyyyy");
						}
						else
						{
							fldRegistrationDate.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"), "MMddyyyy");
						}
					}
					if (MotorVehicle.Statics.gboolFromBatchRegistration)
					{
						fldYearSticker.Text = Strings.Left(frmPreview.InstancePtr.lblNumberofYear.Text, 3) + FCConvert.ToString(MotorVehicle.strPadZeros(fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intYearStickerCol)), 7));
					}
					else
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber")) != 0)
						{
							fldYearSticker.Text = frmPreview.InstancePtr.lblNumberofYear.Text;
						}
					}
					if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
					{
						if (MVTyp == "NRR" || MVTyp == "NRT")
						{
							if (MotorVehicle.Statics.gboolFromBatchRegistration)
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber"))).Length > 0)
								{
									fldPriorTitle.Text = fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPriorTitleCol)) + " " + frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPriorStateCol);
								}
							}
							else
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber"))).Length > 0)
								{
									fldPriorTitle.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber"))) + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleState");
								}
							}
						}
						else if (MVTyp == "ECO" || MVTyp == "ECR")
						{
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber"))).Length > 0)
							{
								if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber"))) != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PriorTitleNumber"))))
								{
									fldPriorTitle.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber"))) + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleState");
								}
							}
						}
					}
					if (MotorVehicle.Statics.ExciseAP)
					{
						// kk07182016  Print AP with Excise Date for Excise Already Paid
						fldExciseTaxDate.Text = "AP " + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"), "MM/dd/yyyy");
					}
					else
					{
						fldExciseTaxDate.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"), "MM/dd/yyyy");
					}
					// MsgBox "13"
					fecherFoundation.Information.Err().Clear();
					if (fecherFoundation.Information.Err().Number == 0)
					{
						MotorVehicle.Statics.PrintMVR3 = true;
						if (MotorVehicle.Statics.gboolFromBatchRegistration)
						{
							frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPrintedCol, FCConvert.ToString(true));
							if (frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol) != "")
							{
								rsTemp.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + FCConvert.ToString(Conversion.Val(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol))));
								if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
								{
									rsTemp.Edit();
									rsTemp.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
									rsTemp.Set_Fields("OpID", MotorVehicle.Statics.UserID);
									rsTemp.Set_Fields("MVR3", frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol));
									rsTemp.Set_Fields("Status", "V");
									rsTemp.Update();
								}
								else
								{
									// no record
								}
								rsTemp.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
								rsTemp.AddNew();
								rsTemp.Set_Fields("DateOfAdjustment", DateTime.Today);
								rsTemp.Set_Fields("High", frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol));
								rsTemp.Set_Fields("Low", frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol));
								rsTemp.Set_Fields("OpID", MotorVehicle.Statics.rsFinal.Get_Fields_String("OpID"));
								rsTemp.Set_Fields("reason", "Printer Alignment/Jam Problem");
								rsTemp.Set_Fields("PeriodCloseoutID", 0);
								rsTemp.Set_Fields("QuantityAdjusted", 1);
								rsTemp.Set_Fields("InventoryType", "MXS00");
								rsTemp.Set_Fields("AdjustmentCode", "V");
								rsTemp.Update();
								frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol, "");
							}
						}
					}
				}
				else
				{
					fldCheckDigits.Text = "XX";
					fldReason1.Text = "Print Priority 1";
					fldReason2.Text = "Print Priority 2";
					fldReason3.Text = "Print Priority 3";
					fldValidation1.Text = "Valid 1";
					fldValidation2.Text = "Valid 2";
					fldValidation3.Text = "Valid 3";
					lblMileage.Visible = true;
					fldMileage.Text = Strings.Format(Strings.Format(20000, "#,###,###"), "@@@@@@@@@");
					fldClass.Text = "PC";
					//FC:FINAL:DDU:#2299 - removed by client choice
					//fldPlate.Text = "TEST";
					fldEffective.Text = "01/01/1900";
					fldExpires.Text = "01/31/1901";
					fldVIN.Text = "4532534GFGSD5534G";
					fldYear.Text = "1990";
					fldMake.Text = "FORD";
					fldModel.Text = "MUSTNG";
					fldStyle.Text = "2D";
					fldAxles.Text = "2";
					fldColor1.Text = "RD";
					fldColor2.Text = "/NA";
					fldTires.Text = "4";
					fldNetWeight.Text = "XXXXX";
					fldRegisteredWeight.Text = "XXXX";
					fldFuel.Text = "G";
					fldOwner1.Text = "OWNER 1";
					fldDOB1.Text = "01/01/1900";
					fldBase.Text = Strings.Format(Strings.Format(20000, "###,###,###"), "@@@@@@@@@@@");
					fldOwner2.Text = "OWNER 2";
					fldDOB2.Text = "01/01/1900";
					fldOwner3.Text = "OWNER 3";
					fldDOB3.Text = "01/01/1900";
					fldMil.Text = Strings.Format(Strings.Format(0.004, "##,##0.0000"), "@@@@@@@@@@");
					fldRegFee.Text = Strings.Format(Strings.Format(35, "#,##0.00"), "@@@@@@@");
					fldLessor.Text = "LESSOR";
					fldUnit.Text = "UNIT";
					fldDOTNumber.Text = "DOT NUM";
					fldAgentFee.Text = Strings.Format(Strings.Format(3, "#,##0.00"), "@@@@@@@");
					fldRegCredit.Text = Strings.Format(Strings.Format(10, "###0.00"), "@@@@@@@");
					fldExcise.Text = Strings.Format("100.00", "@@@@@@@@@");
					fldFees.Text = Strings.Format(Strings.Format(35, "#,##0.00"), "@@@@@@@");
					fldAddress.Text = "ADDRESS 1";
					fldAddress2.Text = "ADDRESS 2";
					fldExciseCredit.Text = Strings.Format(Strings.Format(10, "##,##0.00"), "@@@@@@@@@");
					fldSalesTax.Text = Strings.Format(Strings.Format(200, "##,##0.00"), "@@@@@@@@@");
					fldCity.Text = "CITY";
					fldState.Text = "ME";
					fldZip.Text = "99999";
					fldSubtotal.Text = Strings.Format(Strings.Format(100, "##,##0.00"), "@@@@@@@@@");
					fldTitleFee.Text = Strings.Format(Strings.Format(21, "#,##0.00"), "@@@@@@@@@");
					fldTransferFee.Text = Strings.Format(Strings.Format(5, "##,##0.00"), "@@@@@@@@@");
					fldCTANumber.Text = "4324545";
					fldResidenceAddress.Text = "RES ADDRESS";
					fldResidenceCity.Text = "RES CITY";
					fldResidenceState.Text = "ME";
					fldResidenceCode.Text = "99999";
					fldBalance.Text = Strings.Format(Strings.Format(100, "##,##0.00"), "@@@@@@@@@");
					fldOpID.Text = "XXX";
					fldRegistrationCity.Text = "XXXXXXXXXXX";
					fldMonthSticker.Text = "XXXXXX";
					fldCreditNumber.Text = "XXXXXX";
					fldRegistrationCode.Text = "99999";
					fldRegistrationDate.Text = "01/01/1900";
					fldYearSticker.Text = "XXXXXX";
					fldPriorTitle.Text = "XXXXXXXXXXX";
					fldExciseTaxDate.Text = "01/01/1900";
				}
				// MsgBox "14"
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				if (fecherFoundation.Information.Err(ex).Number != 0)
				{
					MotorVehicle.PrinterError("MVR3 Printer" + modGlobalConstants.Statics.Mvr3Printer);
					MotorVehicle.Statics.PrintMVR3 = false;
					rptMVR3.InstancePtr.Cancel();
					this.Close();
				}
			}
		}

		private void rptMVR3_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMVR3 properties;
			//rptMVR3.Caption	= "MVR3";
			//rptMVR3.Icon	= "rptMVR3.dsx":0000";
			//rptMVR3.Left	= 0;
			//rptMVR3.Top	= 0;
			//rptMVR3.Width	= 11880;
			//rptMVR3.Height	= 8595;
			//rptMVR3.StartUpPosition	= 3;
			//rptMVR3.SectionData	= "rptMVR3.dsx":058A;
			//End Unmaped Properties
		}
	}
}
