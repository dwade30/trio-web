﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmCreditInfo : BaseForm
	{
		public frmCreditInfo()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            Label2 = new System.Collections.Generic.List<FCLabel>();
            Label2.AddControlArrayElement(Label2_0, 0);
            Label2.AddControlArrayElement(Label2_1, 1);
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCreditInfo InstancePtr
		{
			get
			{
				return (frmCreditInfo)Sys.GetInstance(typeof(frmCreditInfo));
			}
		}

		protected frmCreditInfo _InstancePtr = null;
		//=========================================================
		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmCreditInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreditInfo properties;
			//frmCreditInfo.ScaleWidth	= 3885;
			//frmCreditInfo.ScaleHeight	= 2925;
			//frmCreditInfo.LinkTopic	= "Form1";
			//End Unmaped Properties
			frmCreditInfo.InstancePtr.TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal - frmCreditInfo.InstancePtr.HeightOriginal) / 2.0);
			frmCreditInfo.InstancePtr.LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal - frmCreditInfo.InstancePtr.HeightOriginal) / 2.0);
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				e.Cancel = true;
				return;
			}
		}
	}
}
