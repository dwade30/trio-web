//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPrintVIN.
	/// </summary>
	partial class frmPrintVIN
	{
		public fecherFoundation.FCComboBox cmbAnnual;
		public fecherFoundation.FCLabel lblAnnual;
		public fecherFoundation.FCComboBox cmbFleet;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCFrame fraCTA;
		public fecherFoundation.FCTextBox txtCTANumber;
		public fecherFoundation.FCLabel lblCTANumber;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCFrame fraCriteria;
		public fecherFoundation.FCTextBox txtModel;
		public fecherFoundation.FCComboBox cboFleets;
		public fecherFoundation.FCComboBox cboMakes;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCLabel lblFleet;
		public fecherFoundation.FCLabel lblMake;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public FCGrid vsVehicles;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbAnnual = new fecherFoundation.FCComboBox();
            this.lblAnnual = new fecherFoundation.FCLabel();
            this.cmbFleet = new fecherFoundation.FCComboBox();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.fraCTA = new fecherFoundation.FCFrame();
            this.txtCTANumber = new fecherFoundation.FCTextBox();
            this.lblCTANumber = new fecherFoundation.FCLabel();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.fraCriteria = new fecherFoundation.FCFrame();
            this.txtModel = new fecherFoundation.FCTextBox();
            this.cboFleets = new fecherFoundation.FCComboBox();
            this.cboMakes = new fecherFoundation.FCComboBox();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.lblFleet = new fecherFoundation.FCLabel();
            this.lblMake = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.vsVehicles = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCTA)).BeginInit();
            this.fraCTA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).BeginInit();
            this.fraCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.cmdSelect);
            this.ClientArea.Controls.Add(this.cmdSearch);
            this.ClientArea.Controls.Add(this.cmbFleet);
            this.ClientArea.Controls.Add(this.vsVehicles);
            this.ClientArea.Controls.Add(this.fraCTA);
            this.ClientArea.Controls.Add(this.fraCriteria);
            this.ClientArea.Size = new System.Drawing.Size(1078, 520);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(98, 30);
            this.HeaderText.Text = "VIN List";
            // 
            // cmbAnnual
            // 
            this.cmbAnnual.AutoSize = false;
            this.cmbAnnual.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAnnual.FormattingEnabled = true;
            this.cmbAnnual.Items.AddRange(new object[] {
            "Annual",
            "Long Term"});
            this.cmbAnnual.Location = new System.Drawing.Point(444, 10);
            this.cmbAnnual.Name = "cmbAnnual";
            this.cmbAnnual.Size = new System.Drawing.Size(200, 40);
            this.cmbAnnual.TabIndex = 6;
            this.cmbAnnual.Text = "Annual";
            this.cmbAnnual.ToolTipText = null;
            this.cmbAnnual.SelectedIndexChanged += new System.EventHandler(this.cmbAnnual_SelectedIndexChanged);
            // 
            // lblAnnual
            // 
            this.lblAnnual.AutoSize = true;
            this.lblAnnual.Location = new System.Drawing.Point(368, 24);
            this.lblAnnual.Name = "lblAnnual";
            this.lblAnnual.Size = new System.Drawing.Size(40, 15);
            this.lblAnnual.TabIndex = 7;
            this.lblAnnual.Text = "TYPE";
            this.lblAnnual.ToolTipText = null;
            // 
            // cmbFleet
            // 
            this.cmbFleet.AutoSize = false;
            this.cmbFleet.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFleet.FormattingEnabled = true;
            this.cmbFleet.Items.AddRange(new object[] {
            "Fleet/Make/Model/Year",
            "CTA Number"});
            this.cmbFleet.Location = new System.Drawing.Point(139, 30);
            this.cmbFleet.Name = "cmbFleet";
            this.cmbFleet.Size = new System.Drawing.Size(299, 40);
            this.cmbFleet.TabIndex = 19;
            this.cmbFleet.Text = "CTA Number";
            this.cmbFleet.ToolTipText = null;
            this.cmbFleet.SelectedIndexChanged += new System.EventHandler(this.cmbFleet_SelectedIndexChanged);
            // 
            // cmdSelect
            // 
            this.cmdSelect.AppearanceKey = "actionButton";
            this.cmdSelect.Enabled = false;
            this.cmdSelect.Location = new System.Drawing.Point(146, 622);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(114, 40);
            this.cmdSelect.TabIndex = 9;
            this.cmdSelect.Text = "Select All";
            this.cmdSelect.ToolTipText = null;
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            // 
            // fraCTA
            // 
            this.fraCTA.AppearanceKey = "groupBoxNoBorders";
            this.fraCTA.Controls.Add(this.txtCTANumber);
            this.fraCTA.Controls.Add(this.lblCTANumber);
            this.fraCTA.Location = new System.Drawing.Point(30, 80);
            this.fraCTA.Name = "fraCTA";
            this.fraCTA.Size = new System.Drawing.Size(408, 40);
            this.fraCTA.TabIndex = 18;
            // 
            // txtCTANumber
            // 
            this.txtCTANumber.AutoSize = false;
            this.txtCTANumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTANumber.LinkItem = null;
            this.txtCTANumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCTANumber.LinkTopic = null;
            this.txtCTANumber.Location = new System.Drawing.Point(109, 0);
            this.txtCTANumber.Name = "txtCTANumber";
            this.txtCTANumber.Size = new System.Drawing.Size(299, 40);
            this.txtCTANumber.TabIndex = 2;
            this.txtCTANumber.ToolTipText = null;
            // 
            // lblCTANumber
            // 
            this.lblCTANumber.Location = new System.Drawing.Point(0, 14);
            this.lblCTANumber.Name = "lblCTANumber";
            this.lblCTANumber.Size = new System.Drawing.Size(85, 15);
            this.lblCTANumber.TabIndex = 19;
            this.lblCTANumber.Text = "CTA NUMBER";
            this.lblCTANumber.ToolTipText = null;
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Enabled = false;
            this.cmdPrint.Location = new System.Drawing.Point(405, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(80, 48);
            this.cmdPrint.TabIndex = 10;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.ToolTipText = null;
            this.cmdPrint.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Location = new System.Drawing.Point(30, 622);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(96, 40);
            this.cmdSearch.TabIndex = 8;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.ToolTipText = null;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // fraCriteria
            // 
            this.fraCriteria.AppearanceKey = "groupBoxNoBorders";
            this.fraCriteria.Controls.Add(this.txtModel);
            this.fraCriteria.Controls.Add(this.cmbAnnual);
            this.fraCriteria.Controls.Add(this.lblAnnual);
            this.fraCriteria.Controls.Add(this.cboFleets);
            this.fraCriteria.Controls.Add(this.cboMakes);
            this.fraCriteria.Controls.Add(this.txtYear);
            this.fraCriteria.Controls.Add(this.lblFleet);
            this.fraCriteria.Controls.Add(this.lblMake);
            this.fraCriteria.Controls.Add(this.Label2);
            this.fraCriteria.Controls.Add(this.Label3);
            this.fraCriteria.Location = new System.Drawing.Point(0, 70);
            this.fraCriteria.Name = "fraCriteria";
            this.fraCriteria.Size = new System.Drawing.Size(989, 103);
            this.fraCriteria.TabIndex = 13;
            this.fraCriteria.Visible = false;
            // 
            // txtModel
            // 
            this.txtModel.AutoSize = false;
            this.txtModel.BackColor = System.Drawing.SystemColors.Window;
            this.txtModel.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtModel.LinkItem = null;
            this.txtModel.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtModel.LinkTopic = null;
            this.txtModel.Location = new System.Drawing.Point(444, 60);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(200, 40);
            this.txtModel.TabIndex = 5;
            this.txtModel.ToolTipText = null;
            this.txtModel.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtModel_KeyPress);
            // 
            // cboFleets
            // 
            this.cboFleets.AutoSize = false;
            this.cboFleets.BackColor = System.Drawing.SystemColors.Window;
            this.cboFleets.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboFleets.FormattingEnabled = true;
            this.cboFleets.Location = new System.Drawing.Point(132, 10);
            this.cboFleets.Name = "cboFleets";
            this.cboFleets.Size = new System.Drawing.Size(200, 40);
            this.cboFleets.TabIndex = 3;
            this.cboFleets.ToolTipText = null;
            // 
            // cboMakes
            // 
            this.cboMakes.AutoSize = false;
            this.cboMakes.BackColor = System.Drawing.SystemColors.Window;
            this.cboMakes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboMakes.FormattingEnabled = true;
            this.cboMakes.Location = new System.Drawing.Point(132, 60);
            this.cboMakes.Name = "cboMakes";
            this.cboMakes.Size = new System.Drawing.Size(200, 40);
            this.cboMakes.TabIndex = 4;
            this.cboMakes.ToolTipText = null;
            // 
            // txtYear
            // 
            this.txtYear.AutoSize = false;
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.LinkItem = null;
            this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtYear.LinkTopic = null;
            this.txtYear.Location = new System.Drawing.Point(746, 60);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(200, 40);
            this.txtYear.TabIndex = 6;
            this.txtYear.ToolTipText = null;
            // 
            // lblFleet
            // 
            this.lblFleet.Location = new System.Drawing.Point(30, 24);
            this.lblFleet.Name = "lblFleet";
            this.lblFleet.Size = new System.Drawing.Size(50, 15);
            this.lblFleet.TabIndex = 17;
            this.lblFleet.Text = "FLEET";
            this.lblFleet.ToolTipText = null;
            // 
            // lblMake
            // 
            this.lblMake.Location = new System.Drawing.Point(30, 74);
            this.lblMake.Name = "lblMake";
            this.lblMake.Size = new System.Drawing.Size(50, 15);
            this.lblMake.TabIndex = 16;
            this.lblMake.Text = "MAKE";
            this.lblMake.ToolTipText = null;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(368, 74);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(50, 15);
            this.Label2.TabIndex = 15;
            this.Label2.Text = "MODEL";
            this.Label2.ToolTipText = null;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(681, 74);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(45, 15);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "YEAR";
            this.Label3.ToolTipText = null;
            // 
            // vsVehicles
            // 
            this.vsVehicles.AllowSelection = false;
            this.vsVehicles.AllowUserToResizeColumns = false;
            this.vsVehicles.AllowUserToResizeRows = false;
            this.vsVehicles.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsVehicles.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsVehicles.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorBkg = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorSel = System.Drawing.Color.Empty;
            this.vsVehicles.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsVehicles.Cols = 5;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsVehicles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsVehicles.ColumnHeadersHeight = 30;
            this.vsVehicles.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsVehicles.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsVehicles.DragIcon = null;
            this.vsVehicles.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsVehicles.ExtendLastCol = true;
            this.vsVehicles.FixedCols = 0;
            this.vsVehicles.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.FrozenCols = 0;
            this.vsVehicles.GridColor = System.Drawing.Color.Empty;
            this.vsVehicles.GridColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.Location = new System.Drawing.Point(30, 180);
            this.vsVehicles.Name = "vsVehicles";
            this.vsVehicles.OutlineCol = 0;
            this.vsVehicles.ReadOnly = true;
            this.vsVehicles.RowHeadersVisible = false;
            this.vsVehicles.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsVehicles.RowHeightMin = 0;
            this.vsVehicles.Rows = 13;
            this.vsVehicles.ScrollTipText = null;
            this.vsVehicles.ShowColumnVisibilityMenu = false;
            this.vsVehicles.ShowFocusCell = false;
            this.vsVehicles.Size = new System.Drawing.Size(1001, 432);
            this.vsVehicles.StandardTab = true;
            this.vsVehicles.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsVehicles.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsVehicles.TabIndex = 7;
            this.vsVehicles.Visible = false;
            this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
            this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePreview,
            this.mnuFilePrint,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 0;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePreview.Text = "Print / Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 1;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(30, 44);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(79, 15);
            this.fcLabel1.TabIndex = 20;
            this.fcLabel1.Text = "SEARCH BY";
            this.fcLabel1.ToolTipText = null;
            // 
            // frmPrintVIN
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPrintVIN";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "VIN List";
            this.Load += new System.EventHandler(this.frmPrintVIN_Load);
            this.Activated += new System.EventHandler(this.frmPrintVIN_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintVIN_KeyPress);
            this.Resize += new System.EventHandler(this.frmPrintVIN_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCTA)).EndInit();
            this.fraCTA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).EndInit();
            this.fraCriteria.ResumeLayout(false);
            this.fraCriteria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCLabel fcLabel1;
	}
}