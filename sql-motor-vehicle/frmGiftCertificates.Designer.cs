//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmGiftCertificates.
	/// </summary>
	partial class frmGiftCertificates
	{
		public fecherFoundation.FCTabControl tabProcess;
		public fecherFoundation.FCTabPage tabProcess_Page1;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblGiftCertificateNumber;
		public fecherFoundation.FCLabel lblApplicant;
		public Global.T2KBackFillDecimal txtAmount;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCTextBox txtGiftCertificateNumber;
		public fecherFoundation.FCTextBox txtPurchaser;
		public fecherFoundation.FCTabPage tabProcess_Page2;
		public fecherFoundation.FCButton cmdDelete;
		public FCGrid vsCertificates;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.tabProcess = new fecherFoundation.FCTabControl();
            this.tabProcess_Page1 = new fecherFoundation.FCTabPage();
            this.lblAmount = new fecherFoundation.FCLabel();
            this.lblGiftCertificateNumber = new fecherFoundation.FCLabel();
            this.lblApplicant = new fecherFoundation.FCLabel();
            this.txtAmount = new Global.T2KBackFillDecimal();
            this.cmdSave = new fecherFoundation.FCButton();
            this.txtGiftCertificateNumber = new fecherFoundation.FCTextBox();
            this.txtPurchaser = new fecherFoundation.FCTextBox();
            this.tabProcess_Page2 = new fecherFoundation.FCTabPage();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.vsCertificates = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.tabProcess.SuspendLayout();
            this.tabProcess_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.tabProcess_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCertificates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 469);
            this.BottomPanel.Size = new System.Drawing.Size(781, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.tabProcess);
            this.ClientArea.Size = new System.Drawing.Size(781, 409);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(781, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(181, 30);
            this.HeaderText.Text = "Gift Certificates";
            // 
            // tabProcess
            // 
            this.tabProcess.Controls.Add(this.tabProcess_Page1);
            this.tabProcess.Controls.Add(this.tabProcess_Page2);
            this.tabProcess.Location = new System.Drawing.Point(30, 30);
            this.tabProcess.Name = "tabProcess";
            this.tabProcess.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.tabProcess.ShowFocusRect = false;
            this.tabProcess.Size = new System.Drawing.Size(723, 368);
            this.tabProcess.TabIndex = 0;
            this.tabProcess.TabsPerRow = 0;
            this.tabProcess.Text = "Issue Gift Certificate";
            this.tabProcess.WordWrap = false;
            this.tabProcess.SelectedIndexChanged += new System.EventHandler(this.tabProcess_SelectedIndexChanged);
            // 
            // tabProcess_Page1
            // 
            this.tabProcess_Page1.Controls.Add(this.lblAmount);
            this.tabProcess_Page1.Controls.Add(this.lblGiftCertificateNumber);
            this.tabProcess_Page1.Controls.Add(this.lblApplicant);
            this.tabProcess_Page1.Controls.Add(this.txtAmount);
            this.tabProcess_Page1.Controls.Add(this.cmdSave);
            this.tabProcess_Page1.Controls.Add(this.txtGiftCertificateNumber);
            this.tabProcess_Page1.Controls.Add(this.txtPurchaser);
            this.tabProcess_Page1.Location = new System.Drawing.Point(1, 47);
            this.tabProcess_Page1.Name = "tabProcess_Page1";
            this.tabProcess_Page1.Text = "Issue Gift Certificate";
            // 
            // lblAmount
            // 
            this.lblAmount.Location = new System.Drawing.Point(20, 164);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(60, 16);
            this.lblAmount.TabIndex = 6;
            this.lblAmount.Text = "AMOUNT";
            // 
            // lblGiftCertificateNumber
            // 
            this.lblGiftCertificateNumber.Location = new System.Drawing.Point(20, 44);
            this.lblGiftCertificateNumber.Name = "lblGiftCertificateNumber";
            this.lblGiftCertificateNumber.Size = new System.Drawing.Size(120, 16);
            this.lblGiftCertificateNumber.TabIndex = 8;
            this.lblGiftCertificateNumber.Text = "GIFT CERTIFICATE #";
            // 
            // lblApplicant
            // 
            this.lblApplicant.Location = new System.Drawing.Point(20, 104);
            this.lblApplicant.Name = "lblApplicant";
            this.lblApplicant.Size = new System.Drawing.Size(120, 16);
            this.lblApplicant.TabIndex = 9;
            this.lblApplicant.Text = "PURCHASER NAME";
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(200, 150);
            this.txtAmount.MaxLength = 9;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(203, 40);
            this.txtAmount.TabIndex = 3;
            this.txtAmount.TabStop = false;
            this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "actionButton";
            this.cmdSave.Location = new System.Drawing.Point(20, 210);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(78, 40);
            this.cmdSave.TabIndex = 4;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // txtGiftCertificateNumber
            // 
            this.txtGiftCertificateNumber.AutoSize = false;
            this.txtGiftCertificateNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGiftCertificateNumber.LinkItem = null;
            this.txtGiftCertificateNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtGiftCertificateNumber.LinkTopic = null;
            this.txtGiftCertificateNumber.Location = new System.Drawing.Point(200, 30);
            this.txtGiftCertificateNumber.Name = "txtGiftCertificateNumber";
            this.txtGiftCertificateNumber.Size = new System.Drawing.Size(203, 40);
            this.txtGiftCertificateNumber.TabIndex = 1;
            // 
            // txtPurchaser
            // 
            this.txtPurchaser.AutoSize = false;
            this.txtPurchaser.BackColor = System.Drawing.SystemColors.Window;
            this.txtPurchaser.LinkItem = null;
            this.txtPurchaser.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPurchaser.LinkTopic = null;
            this.txtPurchaser.Location = new System.Drawing.Point(200, 90);
            this.txtPurchaser.Name = "txtPurchaser";
            this.txtPurchaser.Size = new System.Drawing.Size(203, 40);
            this.txtPurchaser.TabIndex = 2;
            // 
            // tabProcess_Page2
            // 
            this.tabProcess_Page2.Controls.Add(this.cmdDelete);
            this.tabProcess_Page2.Controls.Add(this.vsCertificates);
            this.tabProcess_Page2.Location = new System.Drawing.Point(1, 47);
            this.tabProcess_Page2.Name = "tabProcess_Page2";
            this.tabProcess_Page2.Text = "Delete Gift Certificate";
            // 
            // cmdDelete
            // 
            this.cmdDelete.AppearanceKey = "actionButton";
            this.cmdDelete.Location = new System.Drawing.Point(20, 258);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(90, 40);
            this.cmdDelete.TabIndex = 7;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // vsCertificates
            // 
            this.vsCertificates.AllowSelection = false;
            this.vsCertificates.AllowUserToResizeColumns = false;
            this.vsCertificates.AllowUserToResizeRows = false;
            this.vsCertificates.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsCertificates.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsCertificates.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsCertificates.BackColorBkg = System.Drawing.Color.Empty;
            this.vsCertificates.BackColorFixed = System.Drawing.Color.Empty;
            this.vsCertificates.BackColorSel = System.Drawing.Color.Empty;
            this.vsCertificates.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsCertificates.Cols = 5;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsCertificates.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsCertificates.ColumnHeadersHeight = 30;
            this.vsCertificates.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsCertificates.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsCertificates.DragIcon = null;
            this.vsCertificates.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsCertificates.ExtendLastCol = true;
            this.vsCertificates.FixedCols = 0;
            this.vsCertificates.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsCertificates.FrozenCols = 0;
            this.vsCertificates.GridColor = System.Drawing.Color.Empty;
            this.vsCertificates.GridColorFixed = System.Drawing.Color.Empty;
            this.vsCertificates.Location = new System.Drawing.Point(20, 30);
            this.vsCertificates.Name = "vsCertificates";
            this.vsCertificates.OutlineCol = 0;
            this.vsCertificates.ReadOnly = true;
            this.vsCertificates.RowHeadersVisible = false;
            this.vsCertificates.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsCertificates.RowHeightMin = 0;
            this.vsCertificates.Rows = 1;
            this.vsCertificates.ScrollTipText = null;
            this.vsCertificates.ShowColumnVisibilityMenu = false;
            this.vsCertificates.ShowFocusCell = false;
            this.vsCertificates.Size = new System.Drawing.Size(682, 209);
            this.vsCertificates.StandardTab = true;
            this.vsCertificates.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsCertificates.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsCertificates.TabIndex = 5;
            this.vsCertificates.DoubleClick += new System.EventHandler(this.vsCertificates_DblClick);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(317, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(130, 48);
            this.cmdProcessSave.TabIndex = 0;
            this.cmdProcessSave.Text = "Save & Exit";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmGiftCertificates
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(781, 577);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGiftCertificates";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Gift Certificates";
            this.Load += new System.EventHandler(this.frmGiftCertificates_Load);
            this.Activated += new System.EventHandler(this.frmGiftCertificates_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGiftCertificates_KeyPress);
            this.Resize += new System.EventHandler(this.frmGiftCertificates_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.tabProcess.ResumeLayout(false);
            this.tabProcess_Page1.ResumeLayout(false);
            this.tabProcess_Page1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.tabProcess_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCertificates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessSave;
	}
}