﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptVoidedMVR3.
	/// </summary>
	partial class rptVoidedMVR3
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptVoidedMVR3));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInventoryType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtNoInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInventoryType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5,
            this.fldInventoryType});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.03125F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field1.Text = "Field1";
			this.Field1.Top = 0F;
			this.Field1.Width = 1F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 1.625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field2.Text = "Field2";
			this.Field2.Top = 0F;
			this.Field2.Width = 0.75F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 2.625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Field3.Text = "Field3";
			this.Field3.Top = 0F;
			this.Field3.Width = 1.5F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 4.34375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field4.Text = "Field4";
			this.Field4.Top = 0F;
			this.Field4.Width = 0.6875F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 5.21875F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Field5.Text = "Field5";
			this.Field5.Top = 0F;
			this.Field5.Width = 2.25F;
			// 
			// fldInventoryType
			// 
			this.fldInventoryType.Height = 0.1875F;
			this.fldInventoryType.Left = 1.03125F;
			this.fldInventoryType.Name = "fldInventoryType";
			this.fldInventoryType.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.fldInventoryType.Text = "BOOST";
			this.fldInventoryType.Top = 0F;
			this.fldInventoryType.Width = 0.5F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtNoInfo});
			this.ReportFooter.Height = 0.6041667F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// txtNoInfo
			// 
			this.txtNoInfo.CanShrink = true;
			this.txtNoInfo.Height = 0.3125F;
			this.txtNoInfo.Left = 1.90625F;
			this.txtNoInfo.Name = "txtNoInfo";
			this.txtNoInfo.Style = "font-family: \'Roman 10cpi\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.txtNoInfo.Text = null;
			this.txtNoInfo.Top = 0.09375F;
			this.txtNoInfo.Width = 2.6875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label17,
            this.SubReport2,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label1,
            this.lblPage,
            this.Line1,
            this.Label33});
			this.GroupHeader1.Height = 1.822917F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// Label17
			// 
			this.Label17.Height = 0.3125F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.0625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label17.Text = "INV TYPE";
			this.Label17.Top = 1.5F;
			this.Label17.Width = 0.5F;
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.5F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.3125F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label12.Text = "DATE ADJUSTED";
			this.Label12.Top = 1.5F;
			this.Label12.Width = 0.8125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.3125F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label13.Text = "QUANTITY ADJUSTED";
			this.Label13.Top = 1.5F;
			this.Label13.Width = 0.8440001F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.156F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.25F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label14.Text = "EXPLANATION";
			this.Label14.Top = 1.656F;
			this.Label14.Width = 1.0625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.3125F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.25F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label15.Text = "OPERATOR INITIALS";
			this.Label15.Top = 1.5F;
			this.Label15.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.3125F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.81275F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label16.Text = "RANGE ADJUSTED";
			this.Label16.Top = 1.5F;
			this.Label16.Width = 1.1245F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.6875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label1.Text = "**** VOIDED FORMS REPORT ****";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.8125F;
			this.Line1.Width = 7.375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.8125F;
			this.Line1.Y2 = 1.8125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 1.75F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// rptVoidedMVR3
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInventoryType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInventoryType;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoInfo;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
