﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary;

namespace TWMV0000
{
    public partial class rptDiskVerification : BaseSectionReport
	{
		public rptDiskVerification()
		{
            InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Disk Verification Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDiskVerification InstancePtr
		{
			get
			{
				return (rptDiskVerification)Sys.GetInstance(typeof(rptDiskVerification));
			}
		}

		protected rptDiskVerification _InstancePtr = null;
        protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		int TOTCNTR;
		Decimal TOTAMT;
		string CLCAT = "";
		//clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		FCFixedString strLine = new FCFixedString(80);
		string strHeading = "";
		string strMuni = "";
		//clsDRWrapper rs1 = new clsDRWrapper();
		public int intPageNumber;
		bool blnReportDone;
		bool blnFooterDone;
		bool blnNoData;
		int counter;

		private bool checkEOF()
		{
			bool checkEOF = false;
			if (counter > (FCFileSystem.LOF(12) / Marshal.SizeOf(MotorVehicle.Statics.BMVRecord)))
			{
				checkEOF = true;
			}
			else
			{
				checkEOF = false;
			}
			return checkEOF;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            string CL = "";
            string PL = "";
            string TR = "";
            string FeeString = "";
            Decimal FeeCurr;
			string OWN = "";
			string MST = "";
			string YST = "";
			string MM = "";
			string YY = "";
			FCFixedString Rec1 = new FCFixedString(251);
			counter += 1;
			if (blnFooterDone == true || blnNoData)
			{
				FCFileSystem.FileClose();
				this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				eArgs.EOF = true;
				return;
			}
			else if (!checkEOF())
			{
				eArgs.EOF = false;
				SubReport1.Report = null;
			}
			else
			{
				FCFileSystem.FileClose();
				eArgs.EOF = false;
				this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				SubReport1.Report = rptDiskVerificationSummary.InstancePtr;
				blnFooterDone = true;
				txtCL.Visible = false;
				txtPlate.Visible = false;
				txtTXRCPT.Visible = false;
				txtFee.Visible = false;
				txtOwner.Visible = false;
				txtYear.Visible = false;
				txtStkr.Visible = false;
			}
			if (!blnReportDone)
			{
				FCFileSystem.FileGet(12, ref MotorVehicle.Statics.BMVRecord, counter);
				CL = MotorVehicle.Statics.BMVRecord.Class;
				PL = MotorVehicle.Statics.BMVRecord.plate;
				TR = MotorVehicle.Statics.BMVRecord.MVR3;
				FeeString = FCConvert.ToString(FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.RateFee) - FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.CreditFee));
				FeeCurr = FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.RateFee) - FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.CreditFee);
				if (FeeCurr < 0)
					FeeCurr = 0;
				if (MotorVehicle.Statics.BMVRecord.DOB1 == "99999999")
				{
					OWN = Strings.Left(MotorVehicle.Statics.BMVRecord.Owner1, 39);
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1837: use custon constructor for initializing FixedString variables
					MotorVehicle.Statics.BMVRecordIndividualOwner = new MotorVehicle.IndividualOwner(0);
					MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 1, 50);
					MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 51, 20);
					MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 71, 1);
					MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 72, 3);
					OWN = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName) + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName) + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial) + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
					if (OWN.Length > 39)
					{
						OWN = Strings.Left(OWN, 39);
					}
				}
				MST = "       ";
				YST = Strings.Format(MotorVehicle.Statics.BMVRecord.YearStickerNumber, "00000000");
				MM = "  ";
				YY = Strings.Mid(MotorVehicle.Statics.BMVRecord.ExpirationDate, 3, 2);
				// 
				txtCL.Text = CL;
				txtPlate.Text = PL;
				txtTXRCPT.Text = TR;
				txtFee.Text = Strings.Format(Strings.Format(FeeCurr, "  ##0.00 "), "@@@@@@@@@");
				txtOwner.Text = fecherFoundation.Strings.Trim(OWN);
				txtYear.Text = YY;
				txtStkr.Text = YST;
				TOTCNTR += 1;
				TOTAMT += FeeCurr;

				CLCAT = CL + FeeString;

				counter += 1;
				if (checkEOF() && !blnFooterDone)
				{
					blnReportDone = true;
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				else
				{
					counter -= 1;
				}
			}

		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strReportDrive;
            FCFileSystem fs = new FCFileSystem();
			string strFilePath = "";
			strReportDrive = "";
	
			strReportDrive = Path.Combine(FCFileSystem.Statics.UserDataFolder,"BMVReports");
            if (!Directory.Exists(strReportDrive))
			{
				Directory.CreateDirectory(strReportDrive);
			}
            strFilePath = Path.Combine(strReportDrive, frmReport.InstancePtr.strResCode + "in.dat");
			if (FCFileSystem.FileExists(strFilePath))
            {
                var filePath = Path.Combine("BMVReports", frmReport.InstancePtr.strResCode + "in.dat");
				FCFileSystem.FileOpen(12, filePath, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.BMVRecord));
				if (FCFileSystem.LOF(12) == 0)
				{
					blnNoData = true;
				}
			}
			else
			{
				blnNoData = true;
			}
			blnFooterDone = false;
			blnReportDone = false;
			counter = 0;

		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = rptSubReportHeading.InstancePtr;
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			if (blnFooterDone)
			{
				Label1.Text = "**** FILE VERIFICATION SUMMARY REPORT ****";
				Label12.Visible = false;
				Label13.Visible = false;
				Label14.Visible = false;
				Label15.Visible = false;
				Label16.Visible = false;
				Label17.Visible = false;
				Label18.Visible = false;
				Line1.Visible = false;
			}
        }

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (blnNoData == true)
			{
				txtNoInfo.Text = "No Information";
			}
		}

		private void rptDiskVerification_Load(object sender, System.EventArgs e)
		{
        }
	}
}
