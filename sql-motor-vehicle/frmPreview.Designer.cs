//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPreview.
	/// </summary>
	partial class frmPreview
	{
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcessUpdate;
		public fecherFoundation.FCToolStripMenuItem mnuReturn;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessUpdate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuReturn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.cmdReasonCodes = new fecherFoundation.FCButton();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCPanel();
			this.imgMVR3Printed = new fecherFoundation.FCPictureBox();
			this.imgUseTaxPrinted = new fecherFoundation.FCPictureBox();
			this.imgCTAPrinted = new fecherFoundation.FCPictureBox();
			this.btnHoldRegistration = new fecherFoundation.FCButton();
			this.btnPrintMVR3 = new fecherFoundation.FCButton();
			this.btnPrintUseTax = new fecherFoundation.FCButton();
			this.btnPrintCTA = new fecherFoundation.FCButton();
			this.arvMVR3Preview = new Global.ARViewer();
			this.txtMVR3Input = new fecherFoundation.FCTextBox();
			this.txtReasonsOnTheBottom = new fecherFoundation.FCTextBox();
			this.txtMessage = new fecherFoundation.FCTextBox();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label21 = new fecherFoundation.FCLabel();
			this.Label22 = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReasonCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgMVR3Printed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgUseTaxPrinted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCTAPrinted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnHoldRegistration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrintMVR3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrintUseTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrintCTA)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 465);
			this.BottomPanel.Size = new System.Drawing.Size(1014, 113);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(1014, 405);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdReturn);
			this.TopPanel.Controls.Add(this.cmdReasonCodes);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdReasonCodes, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdReturn, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(181, 30);
			this.HeaderText.Text = " MVR3 Preview";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessUpdate,
            this.mnuReturn,
            this.mnuSeperator,
            this.mnuPrint});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuProcessUpdate
			// 
			this.mnuProcessUpdate.Index = 0;
			this.mnuProcessUpdate.Name = "mnuProcessUpdate";
			this.mnuProcessUpdate.Text = "Update Reason Codes";
			this.mnuProcessUpdate.Click += new System.EventHandler(this.mnuProcessUpdate_Click);
			// 
			// mnuReturn
			// 
			this.mnuReturn.Index = 1;
			this.mnuReturn.Name = "mnuReturn";
			this.mnuReturn.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuReturn.Text = "Return to Input";
			this.mnuReturn.Click += new System.EventHandler(this.mnuReturn_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 2;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 3;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "";
			// 
			// cmdReasonCodes
			// 
			this.cmdReasonCodes.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdReasonCodes.Location = new System.Drawing.Point(782, 29);
			this.cmdReasonCodes.Name = "cmdReasonCodes";
			this.cmdReasonCodes.Size = new System.Drawing.Size(155, 24);
			this.cmdReasonCodes.TabIndex = 1;
			this.cmdReasonCodes.Text = "Update Reason Codes";
			this.cmdReasonCodes.Click += new System.EventHandler(this.mnuProcessUpdate_Click);
			// 
			// cmdReturn
			// 
			this.cmdReturn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdReturn.Location = new System.Drawing.Point(943, 29);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdReturn.Size = new System.Drawing.Size(107, 24);
			this.cmdReturn.TabIndex = 2;
			this.cmdReturn.Text = "Return to Input";
			this.cmdReturn.Click += new System.EventHandler(this.mnuReturn_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(369, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(160, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.imgMVR3Printed);
			this.Frame1.Controls.Add(this.imgUseTaxPrinted);
			this.Frame1.Controls.Add(this.imgCTAPrinted);
			this.Frame1.Controls.Add(this.btnHoldRegistration);
			this.Frame1.Controls.Add(this.btnPrintMVR3);
			this.Frame1.Controls.Add(this.btnPrintUseTax);
			this.Frame1.Controls.Add(this.btnPrintCTA);
			this.Frame1.Controls.Add(this.arvMVR3Preview);
			this.Frame1.Controls.Add(this.txtMVR3Input);
			this.Frame1.Controls.Add(this.txtReasonsOnTheBottom);
			this.Frame1.Controls.Add(this.txtMessage);
			this.Frame1.Controls.Add(this.Label6);
			this.Frame1.Controls.Add(this.Label21);
			this.Frame1.Controls.Add(this.Label22);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(994, 537);
			// 
			// imgMVR3Printed
			// 
			this.imgMVR3Printed.Anchor = Wisej.Web.AnchorStyles.Left;
			this.imgMVR3Printed.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgMVR3Printed.ForeColor = System.Drawing.Color.FromArgb(70, 193, 126);
			this.imgMVR3Printed.ImageSource = "icon-check";
			this.imgMVR3Printed.Location = new System.Drawing.Point(656, 472);
			this.imgMVR3Printed.Name = "imgMVR3Printed";
			this.imgMVR3Printed.Size = new System.Drawing.Size(29, 32);
			this.imgMVR3Printed.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgMVR3Printed.Visible = false;
			// 
			// imgUseTaxPrinted
			// 
			this.imgUseTaxPrinted.Anchor = Wisej.Web.AnchorStyles.Left;
			this.imgUseTaxPrinted.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgUseTaxPrinted.ForeColor = System.Drawing.Color.FromArgb(70, 193, 126);
			this.imgUseTaxPrinted.ImageSource = "icon-check";
			this.imgUseTaxPrinted.Location = new System.Drawing.Point(480, 472);
			this.imgUseTaxPrinted.Name = "imgUseTaxPrinted";
			this.imgUseTaxPrinted.Size = new System.Drawing.Size(29, 32);
			this.imgUseTaxPrinted.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgUseTaxPrinted.Visible = false;
			// 
			// imgCTAPrinted
			// 
			this.imgCTAPrinted.Anchor = Wisej.Web.AnchorStyles.Left;
			this.imgCTAPrinted.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgCTAPrinted.ForeColor = System.Drawing.Color.FromArgb(70, 193, 126);
			this.imgCTAPrinted.ImageSource = "icon-check";
			this.imgCTAPrinted.Location = new System.Drawing.Point(304, 472);
			this.imgCTAPrinted.Name = "imgCTAPrinted";
			this.imgCTAPrinted.Size = new System.Drawing.Size(29, 32);
			this.imgCTAPrinted.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgCTAPrinted.Visible = false;
			// 
			// btnHoldRegistration
			// 
			this.btnHoldRegistration.AppearanceKey = "actionButton";
			this.btnHoldRegistration.Location = new System.Drawing.Point(691, 468);
			this.btnHoldRegistration.Name = "btnHoldRegistration";
			this.btnHoldRegistration.Size = new System.Drawing.Size(135, 40);
			this.btnHoldRegistration.TabIndex = 75;
			this.btnHoldRegistration.Text = "Hold";
			this.btnHoldRegistration.Click += new System.EventHandler(this.btnHoldRegistration_Click);
			// 
			// btnPrintMVR3
			// 
			this.btnPrintMVR3.AppearanceKey = "actionButton";
			this.btnPrintMVR3.Location = new System.Drawing.Point(515, 468);
			this.btnPrintMVR3.Name = "btnPrintMVR3";
			this.btnPrintMVR3.Size = new System.Drawing.Size(135, 40);
			this.btnPrintMVR3.TabIndex = 74;
			this.btnPrintMVR3.Text = "Print MVR3";
			this.btnPrintMVR3.TextImageRelation = Wisej.Web.TextImageRelation.TextBeforeImage;
			this.btnPrintMVR3.Click += new System.EventHandler(this.btnPrintMVR3_Click);
			// 
			// btnPrintUseTax
			// 
			this.btnPrintUseTax.AppearanceKey = "actionButton";
			this.btnPrintUseTax.Location = new System.Drawing.Point(339, 468);
			this.btnPrintUseTax.Name = "btnPrintUseTax";
			this.btnPrintUseTax.Size = new System.Drawing.Size(135, 40);
			this.btnPrintUseTax.TabIndex = 73;
			this.btnPrintUseTax.Text = "Print Use Tax";
			this.btnPrintUseTax.TextImageRelation = Wisej.Web.TextImageRelation.TextBeforeImage;
			this.btnPrintUseTax.Click += new System.EventHandler(this.btnPrintUseTax_Click);
			// 
			// btnPrintCTA
			// 
			this.btnPrintCTA.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.btnPrintCTA.AppearanceKey = "actionButton";
			this.btnPrintCTA.Location = new System.Drawing.Point(163, 468);
			this.btnPrintCTA.Name = "btnPrintCTA";
			this.btnPrintCTA.Size = new System.Drawing.Size(135, 40);
			this.btnPrintCTA.TabIndex = 72;
			this.btnPrintCTA.Text = "Print CTA";
			this.btnPrintCTA.TextImageRelation = Wisej.Web.TextImageRelation.TextBeforeImage;
			this.btnPrintCTA.Click += new System.EventHandler(this.btnPrintCTA_Click);
			// 
			// arvMVR3Preview
			// 
			this.arvMVR3Preview.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.arvMVR3Preview.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.arvMVR3Preview.Location = new System.Drawing.Point(8, 70);
			this.arvMVR3Preview.Name = "arvMVR3Preview";
			this.arvMVR3Preview.ScrollBars = false;
			this.arvMVR3Preview.Size = new System.Drawing.Size(969, 378);
			this.arvMVR3Preview.TabIndex = 71;
			// 
			// txtMVR3Input
			// 
			this.txtMVR3Input.BackColor = System.Drawing.SystemColors.Window;
			this.txtMVR3Input.Location = new System.Drawing.Point(163, 11);
			this.txtMVR3Input.MaxLength = 8;
			this.txtMVR3Input.Name = "txtMVR3Input";
			this.txtMVR3Input.Size = new System.Drawing.Size(108, 40);
			this.txtMVR3Input.TabIndex = 8;
			this.txtMVR3Input.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMVR3Input.Enter += new System.EventHandler(this.txtMVR3Input_Enter);
			this.txtMVR3Input.Leave += new System.EventHandler(this.txtMVR3Input_Leave);
			this.txtMVR3Input.Validating += new System.ComponentModel.CancelEventHandler(this.txtMVR3Input_Validating);
			this.txtMVR3Input.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMVR3Input_KeyPress);
			this.txtMVR3Input.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMVR3Input_KeyUp);
			// 
			// txtReasonsOnTheBottom
			// 
			this.txtReasonsOnTheBottom.BackColor = System.Drawing.SystemColors.Window;
			this.txtReasonsOnTheBottom.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtReasonsOnTheBottom.Location = new System.Drawing.Point(385, 11);
			this.txtReasonsOnTheBottom.LockedOriginal = true;
			this.txtReasonsOnTheBottom.Name = "txtReasonsOnTheBottom";
			this.txtReasonsOnTheBottom.ReadOnly = true;
			this.txtReasonsOnTheBottom.Size = new System.Drawing.Size(224, 40);
			this.txtReasonsOnTheBottom.TabIndex = 9;
			this.txtReasonsOnTheBottom.Enter += new System.EventHandler(this.txtReasonsOnTheBottom_Enter);
			this.txtReasonsOnTheBottom.Leave += new System.EventHandler(this.txtReasonsOnTheBottom_Leave);
			this.txtReasonsOnTheBottom.DoubleClick += new System.EventHandler(this.txtReasonsOnTheBottom_DoubleClick);
			this.txtReasonsOnTheBottom.Validating += new System.ComponentModel.CancelEventHandler(this.txtReasonsOnTheBottom_Validating);
			this.txtReasonsOnTheBottom.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReasonsOnTheBottom_KeyPress);
			// 
			// txtMessage
			// 
			this.txtMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage.Enabled = false;
			this.txtMessage.Location = new System.Drawing.Point(753, 11);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(224, 40);
			this.txtMessage.TabIndex = 10;
			this.txtMessage.Enter += new System.EventHandler(this.txtMessage_Enter);
			this.txtMessage.Leave += new System.EventHandler(this.txtMessage_Leave);
			this.txtMessage.Validating += new System.ComponentModel.CancelEventHandler(this.txtMessage_Validating);
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(8, 25);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(149, 16);
			this.Label6.TabIndex = 70;
			this.Label6.Text = "INPUT MVR3 NUMBER";
			// 
			// Label21
			// 
			this.Label21.Location = new System.Drawing.Point(330, 25);
			this.Label21.Name = "Label21";
			this.Label21.Size = new System.Drawing.Size(45, 16);
			this.Label21.TabIndex = 69;
			this.Label21.Text = "CODES";
			// 
			// Label22
			// 
			this.Label22.Location = new System.Drawing.Point(673, 25);
			this.Label22.Name = "Label22";
			this.Label22.Size = new System.Drawing.Size(83, 16);
			this.Label22.TabIndex = 68;
			this.Label22.Text = "MESSAGE";
			// 
			// frmPreview
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.Cursor = Wisej.Web.Cursors.Default;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPreview";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = " MVR3 Preview";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmPreview_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPreview_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPreview_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReasonCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgMVR3Printed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgUseTaxPrinted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCTAPrinted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnHoldRegistration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrintMVR3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrintUseTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrintCTA)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdReasonCodes;
		private FCButton cmdReturn;
		private FCButton cmdSave;
		public FCPanel Frame1;
		public FCButton btnHoldRegistration;
		public FCButton btnPrintMVR3;
		public FCButton btnPrintUseTax;
		private FCButton btnPrintCTA;
		public ARViewer arvMVR3Preview;
		public FCTextBox txtMVR3Input;
		public FCTextBox txtReasonsOnTheBottom;
		public FCTextBox txtMessage;
		public FCLabel Label6;
		public FCLabel Label21;
		public FCLabel Label22;
        public FCPictureBox imgCTAPrinted;
        public FCPictureBox imgMVR3Printed;
        public FCPictureBox imgUseTaxPrinted;
    }
}
