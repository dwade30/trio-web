//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPurgeTerminated.
	/// </summary>
	partial class frmPurgeTerminated
	{
		public fecherFoundation.FCComboBox cmbDate;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCComboBox cmbOwner;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtPlate;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCButton cmdPurge;
		public fecherFoundation.FCFrame fraSearch;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtCompanyName;
		public fecherFoundation.FCLabel lblLastName;
		public fecherFoundation.FCLabel lblFirstName;
		public fecherFoundation.FCLabel lblCompanyName;
		public FCGrid vsVehicles;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCLabel lblInstructions;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeTerminated));
			this.cmbDate = new fecherFoundation.FCComboBox();
			this.lblDate = new fecherFoundation.FCLabel();
			this.cmbOwner = new fecherFoundation.FCComboBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.txtPlate = new fecherFoundation.FCTextBox();
			this.cmdSelect = new fecherFoundation.FCButton();
			this.cmdPurge = new fecherFoundation.FCButton();
			this.fraSearch = new fecherFoundation.FCFrame();
			this.txtLastName = new fecherFoundation.FCTextBox();
			this.txtFirstName = new fecherFoundation.FCTextBox();
			this.txtCompanyName = new fecherFoundation.FCTextBox();
			this.lblLastName = new fecherFoundation.FCLabel();
			this.lblFirstName = new fecherFoundation.FCLabel();
			this.lblCompanyName = new fecherFoundation.FCLabel();
			this.vsVehicles = new fecherFoundation.FCGrid();
			this.txtDate = new Global.T2KDateBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
			this.fraSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 565);
			this.BottomPanel.Size = new System.Drawing.Size(901, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSearch);
			this.ClientArea.Controls.Add(this.cmdSelect);
			this.ClientArea.Controls.Add(this.cmbDate);
			this.ClientArea.Controls.Add(this.lblDate);
			this.ClientArea.Controls.Add(this.cmdPurge);
			this.ClientArea.Controls.Add(this.fraSearch);
			this.ClientArea.Controls.Add(this.vsVehicles);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Controls.Add(this.txtPlate);
			this.ClientArea.Size = new System.Drawing.Size(901, 505);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(901, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(320, 30);
			this.HeaderText.Text = "Purge Deleted Registrations";
			// 
			// cmbDate
			// 
			this.cmbDate.AutoSize = false;
			this.cmbDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDate.FormattingEnabled = true;
			this.cmbDate.Items.AddRange(new object[] {
            "Date",
            "Plate",
            "Name"});
			this.cmbDate.Location = new System.Drawing.Point(155, 78);
			this.cmbDate.Name = "cmbDate";
			this.cmbDate.Size = new System.Drawing.Size(174, 40);
			this.cmbDate.TabIndex = 14;
			this.cmbDate.Text = "Date";
			this.cmbDate.SelectedIndexChanged += new System.EventHandler(this.cmbDate_SelectedIndexChanged);
			// 
			// lblDate
			// 
			this.lblDate.AutoSize = true;
			this.lblDate.Location = new System.Drawing.Point(30, 92);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(71, 15);
			this.lblDate.TabIndex = 15;
			this.lblDate.Text = "PURGE BY";
			// 
			// cmbOwner
			// 
			this.cmbOwner.AutoSize = false;
			this.cmbOwner.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOwner.FormattingEnabled = true;
			this.cmbOwner.Items.AddRange(new object[] {
            "Company",
            "Individual"});
			this.cmbOwner.Location = new System.Drawing.Point(20, 30);
			this.cmbOwner.Name = "cmbOwner";
			this.cmbOwner.Size = new System.Drawing.Size(201, 40);
			this.cmbOwner.TabIndex = 11;
			this.cmbOwner.Text = "Company";
			this.cmbOwner.SelectedIndexChanged += new System.EventHandler(this.optOwner_CheckedChanged);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Location = new System.Drawing.Point(30, 462);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(93, 40);
			this.cmdSearch.TabIndex = 12;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// txtPlate
			// 
			this.txtPlate.AutoSize = false;
			this.txtPlate.BackColor = System.Drawing.SystemColors.Window;
			this.txtPlate.LinkItem = null;
			this.txtPlate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPlate.LinkTopic = null;
			this.txtPlate.Location = new System.Drawing.Point(155, 138);
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Size = new System.Drawing.Size(180, 40);
			this.txtPlate.TabIndex = 4;
			this.txtPlate.Visible = false;
			// 
			// cmdSelect
			// 
			this.cmdSelect.AppearanceKey = "actionButton";
			this.cmdSelect.Location = new System.Drawing.Point(143, 462);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(117, 40);
			this.cmdSelect.TabIndex = 13;
			this.cmdSelect.Text = "Select All";
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// cmdPurge
			// 
			this.cmdPurge.AppearanceKey = "actionButton";
			this.cmdPurge.Location = new System.Drawing.Point(280, 462);
			this.cmdPurge.Name = "cmdPurge";
			this.cmdPurge.Size = new System.Drawing.Size(87, 40);
			this.cmdPurge.TabIndex = 14;
			this.cmdPurge.Text = "Purge";
			this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
			// 
			// fraSearch
			// 
			this.fraSearch.Controls.Add(this.txtLastName);
			this.fraSearch.Controls.Add(this.cmbOwner);
			this.fraSearch.Controls.Add(this.txtFirstName);
			this.fraSearch.Controls.Add(this.txtCompanyName);
			this.fraSearch.Controls.Add(this.lblLastName);
			this.fraSearch.Controls.Add(this.lblFirstName);
			this.fraSearch.Controls.Add(this.lblCompanyName);
			this.fraSearch.Location = new System.Drawing.Point(305, 164);
			this.fraSearch.Name = "fraSearch";
			this.fraSearch.Size = new System.Drawing.Size(416, 210);
			this.fraSearch.TabIndex = 0;
			this.fraSearch.Text = "Search Criteria";
			this.fraSearch.Visible = false;
			// 
			// txtLastName
			// 
			this.txtLastName.AutoSize = false;
			this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtLastName.LinkItem = null;
			this.txtLastName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLastName.LinkTopic = null;
			this.txtLastName.Location = new System.Drawing.Point(185, 150);
			this.txtLastName.Name = "txtLastName";
			this.txtLastName.Size = new System.Drawing.Size(211, 40);
			this.txtLastName.TabIndex = 10;
			this.txtLastName.Visible = false;
			// 
			// txtFirstName
			// 
			this.txtFirstName.AutoSize = false;
			this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFirstName.LinkItem = null;
			this.txtFirstName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFirstName.LinkTopic = null;
			this.txtFirstName.Location = new System.Drawing.Point(185, 90);
			this.txtFirstName.Name = "txtFirstName";
			this.txtFirstName.Size = new System.Drawing.Size(211, 40);
			this.txtFirstName.TabIndex = 9;
			this.txtFirstName.Visible = false;
			// 
			// txtCompanyName
			// 
			this.txtCompanyName.AutoSize = false;
			this.txtCompanyName.BackColor = System.Drawing.SystemColors.Window;
			this.txtCompanyName.LinkItem = null;
			this.txtCompanyName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCompanyName.LinkTopic = null;
			this.txtCompanyName.Location = new System.Drawing.Point(185, 150);
			this.txtCompanyName.Name = "txtCompanyName";
			this.txtCompanyName.Size = new System.Drawing.Size(211, 40);
			this.txtCompanyName.TabIndex = 8;
			// 
			// lblLastName
			// 
			this.lblLastName.Location = new System.Drawing.Point(20, 164);
			this.lblLastName.Name = "lblLastName";
			this.lblLastName.Size = new System.Drawing.Size(74, 16);
			this.lblLastName.TabIndex = 18;
			this.lblLastName.Text = "LAST NAME";
			this.lblLastName.Visible = false;
			// 
			// lblFirstName
			// 
			this.lblFirstName.Location = new System.Drawing.Point(20, 104);
			this.lblFirstName.Name = "lblFirstName";
			this.lblFirstName.Size = new System.Drawing.Size(74, 15);
			this.lblFirstName.TabIndex = 17;
			this.lblFirstName.Text = "FIRST NAME";
			this.lblFirstName.Visible = false;
			// 
			// lblCompanyName
			// 
			this.lblCompanyName.Location = new System.Drawing.Point(20, 164);
			this.lblCompanyName.Name = "lblCompanyName";
			this.lblCompanyName.Size = new System.Drawing.Size(98, 16);
			this.lblCompanyName.TabIndex = 16;
			this.lblCompanyName.Text = "COMPANY NAME";
			// 
			// vsVehicles
			// 
			this.vsVehicles.AllowSelection = false;
			this.vsVehicles.AllowUserToResizeColumns = false;
			this.vsVehicles.AllowUserToResizeRows = false;
			this.vsVehicles.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsVehicles.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsVehicles.BackColorBkg = System.Drawing.Color.Empty;
			this.vsVehicles.BackColorFixed = System.Drawing.Color.Empty;
			this.vsVehicles.BackColorSel = System.Drawing.Color.Empty;
			this.vsVehicles.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsVehicles.Cols = 9;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsVehicles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsVehicles.ColumnHeadersHeight = 30;
			this.vsVehicles.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsVehicles.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsVehicles.DragIcon = null;
			this.vsVehicles.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsVehicles.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsVehicles.FrozenCols = 0;
			this.vsVehicles.GridColor = System.Drawing.Color.Empty;
			this.vsVehicles.GridColorFixed = System.Drawing.Color.Empty;
			this.vsVehicles.Location = new System.Drawing.Point(30, 198);
			this.vsVehicles.Name = "vsVehicles";
			this.vsVehicles.OutlineCol = 0;
			this.vsVehicles.ReadOnly = true;
			this.vsVehicles.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsVehicles.RowHeightMin = 0;
			this.vsVehicles.Rows = 50;
			this.vsVehicles.ScrollTipText = null;
			this.vsVehicles.ShowColumnVisibilityMenu = false;
			this.vsVehicles.ShowFocusCell = false;
			this.vsVehicles.Size = new System.Drawing.Size(843, 244);
			this.vsVehicles.StandardTab = true;
			this.vsVehicles.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsVehicles.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsVehicles.TabIndex = 11;
			this.vsVehicles.Visible = false;
			this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
			this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(155, 138);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.MaxLength = 10;
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 5;
			this.txtDate.Text = "  /  /";
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(843, 28);
			this.lblInstructions.TabIndex = 20;
			this.lblInstructions.Text = resources.GetString("lblInstructions.Text");
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmPurgeTerminated
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(901, 673);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPurgeTerminated";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purge Deleted Registrations";
			this.Load += new System.EventHandler(this.frmPurgeTerminated_Load);
			this.Activated += new System.EventHandler(this.frmPurgeTerminated_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeTerminated_KeyPress);
			this.Resize += new System.EventHandler(this.frmPurgeTerminated_Resize);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
			this.fraSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}