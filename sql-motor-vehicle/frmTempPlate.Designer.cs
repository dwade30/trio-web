//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTempPlate.
	/// </summary>
	partial class frmTempPlate
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTextBox txtTempNumber;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOK = new fecherFoundation.FCButton();
            this.txtTempNumber = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 255);
            this.BottomPanel.Size = new System.Drawing.Size(249, 108);
            this.BottomPanel.Visible = false;
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdCancel);
            this.ClientArea.Controls.Add(this.cmdOK);
            this.ClientArea.Controls.Add(this.txtTempNumber);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(249, 195);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(249, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(125, 131);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(93, 40);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(30, 131);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(65, 40);
            this.cmdOK.TabIndex = 2;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // txtTempNumber
            // 
            this.txtTempNumber.AutoSize = false;
            this.txtTempNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtTempNumber.LinkItem = null;
            this.txtTempNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTempNumber.LinkTopic = null;
            this.txtTempNumber.Location = new System.Drawing.Point(30, 72);
            this.txtTempNumber.Name = "txtTempNumber";
            this.txtTempNumber.Size = new System.Drawing.Size(188, 40);
            this.txtTempNumber.TabIndex = 0;
            this.txtTempNumber.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(188, 30);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "PLEASE INPUT THE TEMPORARY PLATE NUMBER";
            // 
            // frmTempPlate
            // 
            this.AcceptButton = this.cmdOK;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(249, 363);
            this.ControlBox = false;
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
            this.Name = "frmTempPlate";
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmTempPlate_Load);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}