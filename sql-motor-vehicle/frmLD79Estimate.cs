﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmLD79Estimate : BaseForm
	{
		public frmLD79Estimate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            txtProposedYear = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtProposedYear.AddControlArrayElement(txtProposedYear_0, 0);
            txtProposedYear.AddControlArrayElement(txtProposedYear_1, 1);
            txtProposedYear.AddControlArrayElement(txtProposedYear_2, 2);
            txtProposedYear.AddControlArrayElement(txtProposedYear_3, 3);
            txtProposedYear.AddControlArrayElement(txtProposedYear_4, 4);
            txtProposedYear.AddControlArrayElement(txtProposedYear_5, 5);

            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
			this.txtProposedYear = new System.Collections.Generic.List<FCTextBox>();
			txtProposedYear.AddRange(new FCTextBox[] {
			this.txtProposedYear_0,
			this.txtProposedYear_1,
			this.txtProposedYear_2,
			this.txtProposedYear_3,
			this.txtProposedYear_4,
			this.txtProposedYear_5});
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmLD79Estimate InstancePtr
		{
			get
			{
				return (frmLD79Estimate)Sys.GetInstance(typeof(frmLD79Estimate));
			}
		}

		protected frmLD79Estimate _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmLD79Estimate_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmLD79Estimate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLD79Estimate properties;
			//frmLD79Estimate.FillStyle	= 0;
			//frmLD79Estimate.ScaleWidth	= 5880;
			//frmLD79Estimate.ScaleHeight	= 3810;
			//frmLD79Estimate.LinkTopic	= "Form2";
			//frmLD79Estimate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmLD79Estimate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			int counter;
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtEndDate.Focus();
				return;
			}
			else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			for (counter = 0; counter <= 5; counter++)
			{
				if (!Information.IsNumeric(txtProposedYear[counter].Text))
				{
					MessageBox.Show("You must input mil rate values for each year in proposed before you may continue.", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			frmReportViewer.InstancePtr.Init(rptLD79Estimate.InstancePtr);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void txtProposedYear_KeyPress(int Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtProposedYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int index = txtProposedYear.GetIndex((FCTextBox)sender);
			txtProposedYear_KeyPress(index, sender, e);
		}
	}
}
