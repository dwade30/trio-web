//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWMV0000
{
	public partial class frmReminder : BaseForm
	{
		public frmReminder()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.txtTownAddress = new System.Collections.Generic.List<T2KOverTypeBox>();
            this.txtTownAddress.AddControlArrayElement(this.txtTownAddress_0, 0);
            this.txtTownAddress.AddControlArrayElement(this.txtTownAddress_1, 1);
            this.txtTownAddress.AddControlArrayElement(this.txtTownAddress_2, 2);
            this.txtTownAddress.AddControlArrayElement(this.txtTownAddress_3, 3);

            Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label1.AddControlArrayElement(Label1_0, 0);
            Label1.AddControlArrayElement(Label1_1, 1);
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReminder InstancePtr
		{
			get
			{
				return (frmReminder)Sys.GetInstance(typeof(frmReminder));
			}
		}

		protected frmReminder _InstancePtr = null;
		//=========================================================
		int ClassCol;
		int PlateCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int OwnerCol;
		int RegCol;
		int ExciseCol;
		int AgentCol;
		int PrintCol;
		int IDCol;
		int EmailAddressCol;
		int AddressCol;
		int Address2Col;
		int Address3Col;
		bool SecondResize;
		clsDRWrapper rsVehicles = new clsDRWrapper();
		bool FeeWrong;
		clsPrintLabel labLabelTypes = new clsPrintLabel();
		clsDRWrapper rsDefaultInfo = new clsDRWrapper();
		int EmailProcessedCol;

		private void cboLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intIndex;
			intIndex = labLabelTypes.Get_IndexFromID(cboLabelType.ItemData(cboLabelType.SelectedIndex));
			lblLabelDescription.Text = labLabelTypes.Get_Description(intIndex);
		}

		private void cmdEmailCancel_Click(object sender, System.EventArgs e)
		{
			fraEmailOptions.Visible = false;
			chkPersonalSignature.CheckState = Wisej.Web.CheckState.Unchecked;
			chkCompanySignature.CheckState = Wisej.Web.CheckState.Unchecked;
			chkDisclaimerNotice.CheckState = Wisej.Web.CheckState.Unchecked;
		}

		public void cmdEmailCancel_Click()
		{
			cmdEmailCancel_Click(cmdEmailCancel, new System.EventArgs());
		}

		private void cmdEmailOK_Click(object sender, System.EventArgs e)
		{
			string strEmail = "";
			string strSubject = "";
			string strBody = "";
			int counter;
			// vbPorter upgrade warning: Amount As double	OnWriteFCConvert.ToDecimal(
			double Amount = 0;
			Decimal amount1 = 0;
			Decimal amount2 = 0;
			Decimal amount3 = 0;
			string strMuniTitle = "";
			int counter2;
			for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
			{
				vsVehicles.TextMatrix(counter, EmailProcessedCol, FCConvert.ToString(false));
			}
			if (fecherFoundation.Strings.Trim(modGlobalConstants.Statics.gstrCityTown) == "")
			{
				strMuniTitle = modGlobalConstants.Statics.MuniName;
			}
			else
			{
				strMuniTitle = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
			}
			counter = 1;
			while (counter < vsVehicles.Rows)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, 1)) == true && FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, EmailProcessedCol)) == false)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, 1)) == true && FCConvert.CBool(vsVehicles.TextMatrix(counter, EmailProcessedCol)) == false)
				{
					strEmail = vsVehicles.TextMatrix(counter, EmailAddressCol);
					strSubject = strMuniTitle + " - Vehicle Registration Reminder";
					strBody = vsVehicles.TextMatrix(counter, OwnerCol) + "," + "\r\n" + "\r\n";
					strBody += "This is a reminder from " + strMuniTitle + " that the following registration(s) will expire at the end of " + Strings.Format(Conversion.Str(cboMonth.SelectedIndex + 1) + "/1/" + cboYear.Text, "MMMM") + ":" + "\r\n" + "\r\n";
					strBody += "<html></br></br><table><thead><tr><th align=left style=\"width: 100px; font color:#2A7FFF\">Plate</th><th align=left style=\"width: 100px; font color:#2A7FFF\">Year</th><th align=left style=\"width: 100px; font color:#2A7FFF\">Make</th><th align=left style=\"width: 100px; font color:#2A7FFF\">Model</th><th align=right style=\"width: 100px; font color:#2A7FFF\">Amount</th></tr></thead>";
					// strBody = strBody & "Plate     " & vbTab & "Year" & vbTab & vbTab & "Make" & vbTab & vbTab & "Model   " & vbTab & "                   Amount" & vbNewLine
					// strBody = strBody & "---------------------------------------------------------------------------------------------" & vbNewLine
					strBody += "<tbody><tr><td>" + vsVehicles.TextMatrix(counter, PlateCol) + "</td><td>" + vsVehicles.TextMatrix(counter, YearCol) + "</td><td>" + vsVehicles.TextMatrix(counter, MakeCol) + "</td><td>" + vsVehicles.TextMatrix(counter, ModelCol) + "</td>";
					// strBody = strBody & vsVehicles.TextMatrix(counter, PlateCol) & String(10 - Len(vsVehicles.TextMatrix(counter, PlateCol)), " ") & vbTab & vsVehicles.TextMatrix(counter, YearCol) & String(4 - Len(vsVehicles.TextMatrix(counter, YearCol)), " ") & vbTab & vbTab & vsVehicles.TextMatrix(counter, MakeCol) & String(4 - Len(vsVehicles.TextMatrix(counter, MakeCol)), " ") & vbTab & vbTab & vsVehicles.TextMatrix(counter, ModelCol) & String(((6 - Len(vsVehicles.TextMatrix(counter, ModelCol))) * 2), " ") & vbTab
					if (vsVehicles.TextMatrix(counter, 7) != "")
					{
						amount1 = FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, 7));
					}
					if (vsVehicles.TextMatrix(counter, 8) != "")
					{
						amount2 = FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, 8));
					}
					if (vsVehicles.TextMatrix(counter, 9) != "")
					{
						amount3 = FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, 9));
					}
					if (vsVehicles.TextMatrix(counter, 7) != "")
					{
						Amount = FCConvert.ToDouble(amount1 + amount2 + amount3);
						strBody += "<td align=right>" + Strings.Format(Amount, "$#,##0.00") + "</td></tr>";
						// strBody = strBody & String(25 - Len(Format(Amount, "$#,##0.00")), " ") & Format(Amount, "$#,##0.00") & vbNewLine
					}
					else
					{
						strBody += "<td align=right>Call Office for Amount</td></tr>";
						// strBody = strBody & "   Call Office for Amount" & vbNewLine
					}
					vsVehicles.TextMatrix(counter, EmailProcessedCol, FCConvert.ToString(true));
					for (counter2 = counter + 1; counter2 <= vsVehicles.Rows - 1; counter2++)
					{
						//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
						//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter2, 1)) == true && FCConvert.ToBoolean(vsVehicles.TextMatrix(counter2, EmailProcessedCol)) == false && strEmail == vsVehicles.TextMatrix(counter2, EmailAddressCol))
						if (FCConvert.CBool(vsVehicles.TextMatrix(counter2, 1)) == true && FCConvert.CBool(vsVehicles.TextMatrix(counter2, EmailProcessedCol)) == false && strEmail == vsVehicles.TextMatrix(counter2, EmailAddressCol))
						{
							// strBody = strBody & vsVehicles.TextMatrix(counter2, PlateCol) & String(10 - Len(vsVehicles.TextMatrix(counter2, PlateCol)), " ") & vbTab & vsVehicles.TextMatrix(counter2, YearCol) & String(4 - Len(vsVehicles.TextMatrix(counter2, YearCol)), " ") & vbTab & vbTab & vsVehicles.TextMatrix(counter2, MakeCol) & String(4 - Len(vsVehicles.TextMatrix(counter2, MakeCol)), " ") & vbTab & vbTab & vsVehicles.TextMatrix(counter2, ModelCol) & String(((6 - Len(vsVehicles.TextMatrix(counter2, ModelCol))) * 2), " ") & vbTab
							strBody += "<tbody><tr><td>" + vsVehicles.TextMatrix(counter2, PlateCol) + "</td><td>" + vsVehicles.TextMatrix(counter2, YearCol) + "</td><td>" + vsVehicles.TextMatrix(counter2, MakeCol) + "</td><td>" + vsVehicles.TextMatrix(counter2, ModelCol) + "</td>";
							if (vsVehicles.TextMatrix(counter2, 7) != "")
							{
								amount1 = FCConvert.ToDecimal(vsVehicles.TextMatrix(counter2, 7));
							}
							if (vsVehicles.TextMatrix(counter2, 8) != "")
							{
								amount2 = FCConvert.ToDecimal(vsVehicles.TextMatrix(counter2, 8));
							}
							if (vsVehicles.TextMatrix(counter2, 9) != "")
							{
								amount3 = FCConvert.ToDecimal(vsVehicles.TextMatrix(counter2, 9));
							}
							if (vsVehicles.TextMatrix(counter2, 7) != "")
							{
								Amount = FCConvert.ToDouble(amount1 + amount2 + amount3);
								strBody += "<td align=right>" + Strings.Format(Amount, "$#,##0.00") + "</td></tr>";
								// strBody = strBody & String(25 - Len(Format(Amount, "$#,##0.00")), " ") & Format(Amount, "$#,##0.00") & vbNewLine
							}
							else
							{
								strBody += "<td align=right>Call Office for Amount</td></tr>";
								// strBody = strBody & "   Call Office for Amount" & vbNewLine
							}
							vsVehicles.TextMatrix(counter2, EmailProcessedCol, FCConvert.ToString(true));
						}
					}
					strBody += "</tbody></table></br></br></html>" + "\r\n" + "\r\n";
					// strBody = "<html><table><thead><tr style=""bgcolor:silver""><th align=left style=""width: 100px; font color:#2A7FFF"">Plate</th><th>Year</th><th>Make</th><th>Model</th><th>Amount</th></tr></thead><tbody><tr><td>Plate</td><td>Year</td><td>Make</td><td>Model</td><td>Amount</td></tbody></table></html>"
					strBody += txtMessage.Text + "\r\n" + "\r\n";
					frmEMail.InstancePtr.Init("", strEmail, strSubject, strBody, chkDisclaimerNotice.CheckState == Wisej.Web.CheckState.Checked, false, false, true, true, false, chkPersonalSignature.CheckState == Wisej.Web.CheckState.Checked, chkCompanySignature.CheckState == Wisej.Web.CheckState.Checked, true, showAsModalForm: true);
				}
				counter += 1;
			}
			cmdEmailCancel_Click();
			MessageBox.Show("E-mail reminders have been successfully sent.", "E-mail Sent", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void cmdEmailReminders_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnVehicles;
			blnVehicles = false;
			counter = 1;
			while (counter < vsVehicles.Rows)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, 1)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, 1)) == true)
				{
					blnVehicles = true;
					break;
				}
				counter += 1;
			}
			if (blnVehicles)
			{
				fraEmailOptions.Visible = true;
			}
			else
			{
				MessageBox.Show("You must select at least 1 vehicle before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdExtract_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnVehicles;
			blnVehicles = false;
			counter = 1;
			while (counter < vsVehicles.Rows)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, 1)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, 1)) == true)
				{
					blnVehicles = true;
					break;
				}
				counter += 1;
			}
			if (blnVehicles)
			{
				fraPostage.Visible = true;
				txtPostage.Focus();
			}
			else
			{
				MessageBox.Show("You must select at least 1 vehicle before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdLabelCancel_Click(object sender, System.EventArgs e)
		{
			fraLabelType.Visible = false;
		}

		public void cmdLabelCancel_Click()
		{
			cmdLabelCancel_Click(cmdLabelCancel, new System.EventArgs());
		}

		private void cmdLabelOK_Click(object sender, System.EventArgs e)
		{
			int counter;
			string strPrinter = "";
			int NumFonts;
			bool boolUseFont;
			int intCPI;
			int x;
			string strFont = "";
			int intReturn;
			string strOldPrinter = "";
			MotorVehicle.Statics.rsReport = rsVehicles;
			
			strPrinter = FCGlobal.Printer.DeviceName;
			MotorVehicle.Statics.strSql = "";
			NumFonts = FCGlobal.Printer.FontCount;
			boolUseFont = false;
			intCPI = 10;
			for (x = 0; x <= NumFonts - 1; x++)
			{
				strFont = FCGlobal.Printer.Fonts[x];
				if (fecherFoundation.Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
				{
					strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
					if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
					{
						boolUseFont = true;
						strFont = FCGlobal.Printer.Fonts[x];
						break;
					}
				}
			}
			// x
			// SHOW THE REPORT
			if (!boolUseFont)
				strFont = "";
			cmdLabelCancel_Click();
			rptCustomLabels.InstancePtr.Init(ref MotorVehicle.Statics.strSql, "REMINDERLABELS", cboLabelType.ItemData(cboLabelType.SelectedIndex), ref strPrinter, ref strFont, true);
		}

		private void cmdPrintLabels_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnVehicles;
			blnVehicles = false;
			counter = 1;
			while (counter < vsVehicles.Rows)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, 1)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, 1)) == true)
				{
					blnVehicles = true;
					break;
				}
				counter += 1;
			}
			if (blnVehicles)
			{
				cboLabelType.SelectedIndex = 0;
				fraLabelType.Visible = true;
			}
			else
			{
				MessageBox.Show("You must select at least 1 vehicle before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdPrintList_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnVehicles;
			blnVehicles = false;
			counter = 1;
			while (counter < vsVehicles.Rows)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, 1)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, 1)) == true)
				{
					blnVehicles = true;
					break;
				}
				counter += 1;
			}
			if (blnVehicles)
			{
				// DuplexPrintReport rptReminderList
				// Unload rptReminderList
				frmReportViewer.InstancePtr.Init(rptReminderList.InstancePtr);
			}
			else
			{
				MessageBox.Show("You must select at least 1 vehicle before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdPrintReminders_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			int counter;
			bool blnVehicles;
			clsDRWrapper rs = new clsDRWrapper();
			blnVehicles = false;
			counter = 1;
			while (counter < vsVehicles.Rows)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, 1)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, 1)) == true)
				{
					blnVehicles = true;
					break;
				}
				counter += 1;
			}
			if (blnVehicles)
			{
				rs.OpenRecordset("SELECT * FROM WMV", "TWMV0000.vb1");
				if (rs.BeginningOfFile() != true && rs.EndOfFile() != true)
				{
					rs.Edit();
					rs.Set_Fields("ReminderType", cboReminderType.SelectedIndex);
					rs.Set_Fields("ReminderMessage", fecherFoundation.Strings.Trim(txtMessage.Text));
					rs.Update();
				}
				if (cboReminderType.SelectedIndex == 1)
				{
					rptReminderForm.InstancePtr.Init(this.Modal);
				}
				else
				{
					ans = MessageBox.Show("Are you printing these reminder notices on pre-printed forms?", "Pre Printed Forms?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						rptReminderNotice.InstancePtr.blnShowHeadings = false;
					}
					else
					{
						rptReminderNotice.InstancePtr.blnShowHeadings = true;
					}
					MotorVehicle.Statics.rsReport = rsVehicles;
					frmReportViewer.InstancePtr.Init(rptReminderNotice.InstancePtr);
				}
				// rptReminderNotice.PrintReport True
				// Unload rptReminderNotice
				//App.DoEvents();
				//Support.ZOrder(this, 0);
			}
			else
			{
				MessageBox.Show("You must select at least 1 vehicle before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string temp = "";
			DateTime DateCheck;
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			if (cboMonth.SelectedIndex == -1)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("You must select a month before you may search for vehicles.", "Invalid Month", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboMonth.Focus();
				return;
			}
			else if (cboYear.SelectedIndex == -1)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("You must select a year before you may search for vehicles.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboYear.Focus();
				return;
			}
			else
			{
				temp = Conversion.Str(cboMonth.SelectedIndex + 1) + "/01/" + cboYear.Text;
				DateCheck = FindLastDay(temp);
				if (cmbEmailNo.Text == "Both")
				{
					rsVehicles.OpenRecordset("SELECT c.*, p.FullName as Reg1FullName, q.FullName as Reg2FullName, r.FullName as Reg3FullName FROM Master as c LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as q ON c.PartyID2 = q.ID LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as r ON c.PartyID3 = r.ID WHERE ExpireDate = '" + FCConvert.ToString(DateCheck) + "' AND Status <> 'T' AND Mail = 1 ORDER BY p.FullNameLF");
				}
				else if (cmbEmailNo.Text == "Owners with E-mail Address")
				{
					rsVehicles.OpenRecordset("SELECT c.*, p.FullName as Reg1FullName, q.FullName as Reg2FullName, r.FullName as Reg3FullName FROM Master as c LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as q ON c.PartyID2 = q.ID LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as r ON c.PartyID3 = r.ID WHERE ExpireDate = '" + FCConvert.ToString(DateCheck) + "' AND rtrim(isnull(EMailAddress, '')) <> '' AND Status <> 'T' AND Mail = 1 ORDER BY p.FullNameLF");
				}
				else
				{
					rsVehicles.OpenRecordset("SELECT c.*, p.FullName as Reg1FullName, q.FullName as Reg2FullName, r.FullName as Reg3FullName FROM Master as c LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as q ON c.PartyID2 = q.ID LEFT JOIN " + rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyNameView as r ON c.PartyID3 = r.ID WHERE ExpireDate = '" + FCConvert.ToString(DateCheck) + "' AND rtrim(isnull(EMailAddress, '')) = '' AND Status <> 'T' AND Mail = 1 ORDER BY p.FullNameLF");
				}
				if (rsVehicles.EndOfFile() != true && rsVehicles.BeginningOfFile() != true)
				{
					rsVehicles.MoveLast();
					rsVehicles.MoveFirst();
					FillGrid();
					frmWait.InstancePtr.Unload();
					vsVehicles.Visible = true;
                    fraTownInfo.BringToFront();                    
					cmdSelect.Enabled = true;
					cmdClear.Enabled = true;
					cmdPrintLabels.Enabled = true;
					cmdPrintList.Enabled = true;
					cmdPrintReminders.Enabled = true;
					cmdExtract.Enabled = true;
					if (cmbEmailNo.Text == "Owners with E-mail Address")
					{
						cmdEmailReminders.Enabled = true;
					}
					else
					{
						cmdEmailReminders.Enabled = false;
					}
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No Records were found that expired in this month.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void cmdTownInfoCancel_Click(object sender, System.EventArgs e)
		{
			fraTownInfo.Visible = false;
		}

		private void cmdTownInfoOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM WMV", "TWMV0000.vb1");
			if (rs.BeginningOfFile() != true && rs.EndOfFile() != true)
			{
				rs.Edit();
				rs.Set_Fields("TownName", fecherFoundation.Strings.Trim(txtTownName.Text));
				rs.Set_Fields("TownAddress1", fecherFoundation.Strings.Trim(txtTownAddress[0].Text));
				rs.Set_Fields("TownAddress2", fecherFoundation.Strings.Trim(txtTownAddress[1].Text));
				rs.Set_Fields("TownAddress3", fecherFoundation.Strings.Trim(txtTownAddress[2].Text));
				rs.Set_Fields("TownAddress4", fecherFoundation.Strings.Trim(txtTownAddress[3].Text));
				rs.Update();
			}
			fraTownInfo.Visible = false;
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			int counter;
			// kk05162017 tromv-1115  Change from Integer to Long
			int counter2;
			string strOwner = "";
			string strOwner2 = "";
			string strOwner3 = "";
			string strLessor = "";
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans;
			FCFileSystem.FileOpen(1, "Reminders.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
			for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, PrintCol)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, PrintCol)) == true)
				{
					rsVehicles.FindFirstRecord("ID", Conversion.Val(vsVehicles.TextMatrix(counter, 0)));
					strOwner = fecherFoundation.Strings.UCase(FCConvert.ToString(rsVehicles.Get_Fields("Reg1FullName")));
					strOwner2 = fecherFoundation.Strings.UCase(FCConvert.ToString(rsVehicles.Get_Fields("Reg2FullName")));
					strOwner3 = fecherFoundation.Strings.UCase(FCConvert.ToString(rsVehicles.Get_Fields("Reg3FullName")));
					if (FCConvert.ToString(rsVehicles.Get_Fields_String("LeaseCode1")) == "R")
					{
						strLessor = strOwner;
						strOwner = strOwner2;
						strOwner2 = strOwner3;
					}
					else if (rsVehicles.Get_Fields_String("LeaseCode2") == "R")
					{
						strLessor = strOwner2;
						strOwner2 = strOwner3;
					}
					else if (rsVehicles.Get_Fields_String("LeaseCode3") == "R")
					{
						strLessor = strOwner3;
						strOwner3 = "";
					}
					else
					{
						strLessor = "";
					}
					for (counter2 = ClassCol; counter2 <= OwnerCol; counter2++)
					{
						if (counter2 == OwnerCol)
						{
							FCFileSystem.Write(1, strOwner);
						}
						else
						{
							FCFileSystem.Write(1, vsVehicles.TextMatrix(counter, counter2));
						}
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Vin")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_String("Vin"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Address")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_String("Address"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Address2")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_String("Address2"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("City")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_String("City"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields("State")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields("State"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Zip")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_String("Zip"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					FCFileSystem.Write(1, strOwner2);
					FCFileSystem.Write(1, strOwner3);
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_Int32("BasePrice")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_Int32("BasePrice"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_Int32("MillYear")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_Int32("MillYear"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (vsVehicles.TextMatrix(counter, RegCol) == "" || vsVehicles.TextMatrix(counter, ExciseCol) == "" || vsVehicles.TextMatrix(counter, AgentCol) == "")
					{
						FCFileSystem.Write(1, "");
					}
					else
					{
						if (fecherFoundation.Strings.Trim(txtPostage.Text) == "")
						{
							FCFileSystem.Write(1, Strings.Format(FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, RegCol)) + FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, ExciseCol)) + FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, AgentCol)), "#,##0.00"));
						}
						else
						{
							FCFileSystem.Write(1, Strings.Format(FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, RegCol)) + FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, ExciseCol)) + FCConvert.ToDecimal(vsVehicles.TextMatrix(counter, AgentCol)) + FCConvert.ToDecimal(txtPostage.Text), "#,##0.00"));
						}
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_Int32("FleetNumber")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_Int32("FleetNumber"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rsVehicles.Get_Fields_DateTime("ExpireDate")))
					{
						FCFileSystem.Write(1, Strings.Format(rsVehicles.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("color1")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_String("color1"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Residence")))
					{
						FCFileSystem.Write(1, rsVehicles.Get_Fields_String("Residence"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					FCFileSystem.Write(1, strLessor);
					if (fecherFoundation.Strings.Trim(txtPostage.Text) == "")
					{
						FCFileSystem.Write(1, "0.00");
					}
					else
					{
						FCFileSystem.Write(1, txtPostage.Text);
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("EMailAddress")))
					{
						FCFileSystem.WriteLine(1, rsVehicles.Get_Fields_String("EMailAddress"));
					}
					else
					{
						FCFileSystem.WriteLine(1, "");
					}
				}
			}
			FCFileSystem.FileClose(1);
			fraPostage.Visible = false;
			//FC:FINAL:DDU:#2201 - download the file where user wants
			//FC:FINAL:DDU:#2201 - changed question
			ans = MessageBox.Show(/*The data has been extracted to the " + Path.Combine(Application.StartupPath, "Reminders.txt") + " file.  */"Do you wish to get an extract listing?", "Extract Listing", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "Reminders.txt"), "Reminders.txt");
			if (ans == DialogResult.Yes)
			{
				frmReportViewer.InstancePtr.Init(rptExtractListing.InstancePtr);
			}
		}

		private void Command2_Click(object sender, System.EventArgs e)
		{
			txtPostage.Text = "";
			fraPostage.Visible = false;
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
			{
				vsVehicles.TextMatrix(counter, PrintCol, FCConvert.ToString(false));
			}
		}

		private void frmReminder_Activated(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.REMINDERS))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			IDCol = 0;
			PrintCol = 1;
			ClassCol = 2;
			PlateCol = 3;
			YearCol = 4;
			MakeCol = 5;
			ModelCol = 6;
			RegCol = 7;
			ExciseCol = 8;
			AgentCol = 9;
			EmailAddressCol = 11;
			OwnerCol = 10;
			EmailProcessedCol = 12;
			AddressCol = 13;
			Address2Col = 14;
			Address3Col = 15;
			vsVehicles.TextMatrix(0, ClassCol, "Class");
			vsVehicles.TextMatrix(0, PlateCol, "Plate");
			vsVehicles.TextMatrix(0, YearCol, "Year");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			vsVehicles.TextMatrix(0, RegCol, "Reg");
			vsVehicles.TextMatrix(0, ExciseCol, "Excise");
			vsVehicles.TextMatrix(0, AgentCol, "Agent");
			vsVehicles.TextMatrix(0, EmailAddressCol, "E-mail Address");
			vsVehicles.TextMatrix(0, OwnerCol, "Owner");
            vsVehicles.ColWidth(ClassCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.05)));
            vsVehicles.ColWidth(PlateCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.09)));
            vsVehicles.ColWidth(YearCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.05)));
            vsVehicles.ColWidth(MakeCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.06)));
            vsVehicles.ColWidth(ModelCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.09)));
            vsVehicles.ColWidth(RegCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.05)));
            vsVehicles.ColWidth(ExciseCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.06)));
            vsVehicles.ColWidth(AgentCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.06)));
            vsVehicles.ColWidth(OwnerCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.21)));
            vsVehicles.ColWidth(EmailAddressCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.22)));
            vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, OwnerCol, 4);
			vsVehicles.ColDataType(PrintCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVehicles.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsVehicles.ColAlignment(OwnerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsVehicles.ColWidth(IDCol, 0);
			vsVehicles.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //FC:FINAL:PB: - issue #2847 force alignments
            vsVehicles.ColAlignment(ClassCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColAlignment(MakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColAlignment(RegCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsVehicles.ColAlignment(ExciseCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsVehicles.ColAlignment(AgentCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsVehicles.ColHidden(EmailProcessedCol, true);
			vsVehicles.ColHidden(AddressCol, true);
			vsVehicles.ColHidden(Address2Col, true);
			vsVehicles.ColHidden(Address3Col, true);
			vsVehicles.ColDataType(EmailProcessedCol, FCGrid.DataTypeSettings.flexDTBoolean);
			for (counter = (DateTime.Today.Year - 5); counter <= (DateTime.Today.Year + 5); counter++)
			{
				cboYear.AddItem(counter.ToString());
			}
			if (DateTime.Today.Month == 12)
			{
				cboMonth.SelectedIndex = 0;
			}
			else
			{
				cboMonth.SelectedIndex = DateTime.Today.Month;
			}
			cboYear.SelectedIndex = 5;
			this.Refresh();
		}

		private void frmReminder_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReminder properties;
			//frmReminder.FillStyle	= 0;
			//frmReminder.ScaleWidth	= 9045;
			//frmReminder.ScaleHeight	= 6930;
			//frmReminder.LinkTopic	= "Form2";
			//frmReminder.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM WMV", "TWMV0000.vb1");
			if (rs.BeginningOfFile() != true && rs.EndOfFile() != true)
			{
				txtTownName.Text = FCConvert.ToString(rs.Get_Fields_String("TownName"));
				txtTownAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("TownAddress1"));
				txtTownAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("TownAddress2"));
				txtTownAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("TownAddress3"));
				txtTownAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("TownAddress4"));
				cboReminderType.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_String("ReminderType")));
				txtMessage.Text = FCConvert.ToString(rs.Get_Fields_String("ReminderMessage"));
			}
			else
			{
				cboReminderType.SelectedIndex = 0;
			}
			FillLabelTypeCombo();
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
		}

		private void frmReminder_Resize(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: temp As int	OnWriteFCConvert.ToInt32(
			int temp;
			int counter2;
			//vsVehicles.AutoSize(1, EmailAddressCol);
			//FC:FINAL:DDU - Managed by designer part
			//temp = 0;
			//for (counter2 = 0; counter2 <= EmailAddressCol; counter2++)
			//{
			//	temp += vsVehicles.ColWidth(counter2);
			//}
			//if (vsVehicles.Rows + 1 <= 15)
			//{
			//	vsVehicles.Height = ((vsVehicles.Rows + 1) * vsVehicles.RowHeight(0)) + 75;
			//}
			//else
			//{
			//	vsVehicles.Height = (15 * vsVehicles.RowHeight(0)) + 75;
			//}
			//if (temp + 400 > this.WidthOriginal)
			//{
			//	vsVehicles.Width = this.WidthOriginal - 100;
			//}
			//else
			//{
			//	vsVehicles.Width = temp + 400;
			//}
			if (SecondResize == false)
			{
				SecondResize = true;
				Form_Resize();
			}
			else
			{
				SecondResize = false;
			}
           
            fraEmailOptions.CenterToContainer(this.ClientArea);
            fraLabelType.CenterToContainer(this.ClientArea);
            fraPostage.CenterToContainer(this.ClientArea);
        }

		public void Form_Resize()
		{
			frmReminder_Resize(this, new System.EventArgs());
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmReminder_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileEditTownInfo_Click(object sender, System.EventArgs e)
		{
			fraTownInfo.Visible = true;
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void vsVehicles_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsVehicles.Row > 0)
			{
				if (vsVehicles.TextMatrix(vsVehicles.Row, PrintCol) != "-1")
				{
					vsVehicles.TextMatrix(vsVehicles.Row, PrintCol, "-1");
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, PrintCol, "0");
				}
			}
		}

		private void vsVehicles_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsVehicles.Row > 0)
			{
				if (KeyCode == Keys.Space)
				{
					KeyCode = 0;
					//FC:FINAL:MSH - wrong input value for FCConvert.ToBoolean (same with i.issue #1883)
					//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, PrintCol)) != true)
					if (FCUtils.CBool(vsVehicles.TextMatrix(vsVehicles.Row, PrintCol)) != true)
					{
						vsVehicles.TextMatrix(vsVehicles.Row, PrintCol, FCConvert.ToString(true));
					}
					else
					{
						vsVehicles.TextMatrix(vsVehicles.Row, PrintCol, FCConvert.ToString(false));
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(string)
		public DateTime FindLastDay(string Newdate)
		{
			DateTime FindLastDay = System.DateTime.Now;
			// vbPorter upgrade warning: xdate As object	OnWrite(DateTime)
			object xdate;
			xdate = (fecherFoundation.DateAndTime.DateAdd("m", 1, FCConvert.ToDateTime(Newdate)));
			xdate = fecherFoundation.DateAndTime.DateAdd("d", -1, (DateTime)xdate);
			FindLastDay = FCConvert.ToDateTime(Strings.Format(xdate, "MM/dd/yyyy"));
			return FindLastDay;
		}

		private void FillGrid()
		{
			int counter;
			int temp;
			// vbPorter upgrade warning: Agent As double	OnWriteFCConvert.ToDecimal(
			double Agent = 0;
			// vbPorter upgrade warning: ExciseTax As double	OnWriteFCConvert.ToDecimal(
			double ExciseTax = 0;
			// vbPorter upgrade warning: RegFee As double	OnWriteFCConvert.ToDecimal(
			double RegFee = 0;
			counter = 1;
			vsVehicles.Rows = 1;
			vsVehicles.Rows = rsVehicles.RecordCount() + 1;
			if (cmbAll.Text == "All Vehicles")
			{
				do
				{
					vsVehicles.TextMatrix(counter, 1, FCConvert.ToString(false));
					vsVehicles.TextMatrix(counter, EmailProcessedCol, FCConvert.ToString(false));
					vsVehicles.TextMatrix(counter, 0, FCConvert.ToString(rsVehicles.Get_Fields_Int32("ID")));
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Class")))
					{
						vsVehicles.TextMatrix(counter, ClassCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Class"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("plate")))
					{
						vsVehicles.TextMatrix(counter, PlateCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("plate"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields("Year")))
					{
						vsVehicles.TextMatrix(counter, YearCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("Year"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("make")))
					{
						vsVehicles.TextMatrix(counter, MakeCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("make"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("model")))
					{
						vsVehicles.TextMatrix(counter, ModelCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("model"))));
					}
					if (FCConvert.ToString(rsVehicles.Get_Fields_String("Subclass")) == "!!")
					{
						// do nothing
					}
					else
					{
						FeeWrong = false;
						Agent = FCConvert.ToDouble(CalcAgentFee(ref rsVehicles));
						ExciseTax = FCConvert.ToDouble(CalcExciseFee(ref rsVehicles));
						RegFee = FCConvert.ToDouble(CalcStateFee(ref rsVehicles));
						if (FeeWrong)
						{
							// do nothing
						}
						else
						{
							vsVehicles.TextMatrix(counter, RegCol, Strings.Format(RegFee, "#,##0.00"));
							vsVehicles.TextMatrix(counter, ExciseCol, Strings.Format(ExciseTax, "#,##0.00"));
							vsVehicles.TextMatrix(counter, AgentCol, Strings.Format(Agent, "#,##0.00"));
						}
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("EMailAddress")))
					{
						vsVehicles.TextMatrix(counter, EmailAddressCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("EMailAddress"))));
					}
					if (FCConvert.ToString(rsVehicles.Get_Fields_String("LeaseCode1")) != "R")
					{
						vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("Reg1FullName")))));
					}
					else
					{
						vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("Reg2FullName")))));
					}
					vsVehicles.TextMatrix(counter, AddressCol, FCConvert.ToString(rsVehicles.Get_Fields_String("Address")));
					vsVehicles.TextMatrix(counter, Address2Col, FCConvert.ToString(rsVehicles.Get_Fields_String("Address2")));
					vsVehicles.TextMatrix(counter, Address3Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Zip"))));
					counter += 1;
					rsVehicles.MoveNext();
				}
				while (rsVehicles.EndOfFile() != true);
			}
			else
			{
				do
				{
					if (FCConvert.ToString(rsVehicles.Get_Fields_String("Subclass")) == "!!")
					{
						goto NotCalculated;
					}
					else
					{
						FeeWrong = false;
						Agent = FCConvert.ToDouble(CalcAgentFee(ref rsVehicles));
						ExciseTax = FCConvert.ToDouble(CalcExciseFee(ref rsVehicles));
						RegFee = FCConvert.ToDouble(CalcStateFee(ref rsVehicles));
						if (FeeWrong)
						{
							goto NotCalculated;
						}
						else
						{
							vsVehicles.TextMatrix(counter, RegCol, Strings.Format(RegFee, "#,##0.00"));
							vsVehicles.TextMatrix(counter, ExciseCol, Strings.Format(ExciseTax, "#,##0.00"));
							vsVehicles.TextMatrix(counter, AgentCol, Strings.Format(Agent, "#,##0.00"));
						}
					}
					vsVehicles.TextMatrix(counter, 1, FCConvert.ToString(false));
					vsVehicles.TextMatrix(counter, 0, FCConvert.ToString(rsVehicles.Get_Fields_Int32("ID")));
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Class")))
					{
						vsVehicles.TextMatrix(counter, ClassCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Class"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("plate")))
					{
						vsVehicles.TextMatrix(counter, PlateCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("plate"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields("Year")))
					{
						vsVehicles.TextMatrix(counter, YearCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("Year"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("make")))
					{
						vsVehicles.TextMatrix(counter, MakeCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("make"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("model")))
					{
						vsVehicles.TextMatrix(counter, ModelCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("model"))));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("EMailAddress")))
					{
						vsVehicles.TextMatrix(counter, EmailAddressCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("EMailAddress"))));
					}
					if (FCConvert.ToString(rsVehicles.Get_Fields_String("LeaseCode1")) != "R")
					{
						vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("Reg1FullName")))));
					}
					else
					{
						vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("Reg2FullName")))));
					}
					vsVehicles.TextMatrix(counter, AddressCol, FCConvert.ToString(rsVehicles.Get_Fields_String("Address")));
					vsVehicles.TextMatrix(counter, Address2Col, FCConvert.ToString(rsVehicles.Get_Fields_String("Address2")));
					vsVehicles.TextMatrix(counter, Address3Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Zip"))));
					counter += 1;
					NotCalculated:
					;
					rsVehicles.MoveNext();
				}
				while (rsVehicles.EndOfFile() != true);
				vsVehicles.Rows = counter;
			}
			Form_Resize();
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(Decimal, int)MotorVehicle.GetSpecialtyPlate(rsVehicles.Get_Fields_String("Class"))
		private Decimal CalcStateFee(ref clsDRWrapper rs)
		{
			Decimal CalcStateFee = 0;
			Decimal RegFee;
			Decimal InitFee;
			Decimal CommCred;
			// vbPorter upgrade warning: SpecialFee As Decimal	OnWrite
			Decimal SpecialFee;
			int tempweight = 0;

            var specialtyPlate = MotorVehicle.GetSpecialtyPlate(rsVehicles.Get_Fields_String("Class"));

            if (specialtyPlate == "BH" || (specialtyPlate == "BB") || (specialtyPlate == "LB") || (specialtyPlate == "CR") || (specialtyPlate == "UM") || (specialtyPlate == "AG") || (specialtyPlate == "AC") || (specialtyPlate == "AF") || (specialtyPlate == "TS") || (specialtyPlate == "BC") || (specialtyPlate == "AW") || (specialtyPlate == "LC"))
			{
				SpecialFee = 15;
			}
			else if (MotorVehicle.GetSpecialtyPlate(rsVehicles.Get_Fields_String("Class")) == "SW")
			{
				SpecialFee = 20;
			}
			else
			{
				SpecialFee = 0;
			}
			if (FCConvert.ToString(rsVehicles.Get_Fields_String("Class")) == "SE")
			{
				if (fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields("NetWeight")))
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(rsVehicles.Get_Fields("NetWeight"));
				}
			}
			else
			{
				if (fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_Int32("RegisteredWeightNew")))
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(Math.Round(Conversion.Val(rsVehicles.Get_Fields_Int32("RegisteredWeightNew"))));
				}
			}
			RegFee = GetReg(rsVehicles.Get_Fields_String("Class"), rsVehicles.Get_Fields_String("Subclass"), tempweight);
			if (MotorVehicle.IsMotorcycle(rsVehicles.Get_Fields_String("Class")))
			{
				if ((cboMonth.SelectedIndex > 2 && cboYear.Text == "2012") || (cboMonth.SelectedIndex < 2 && cboYear.Text == "2013"))
				{
					RegFee = Prorate(ref RegFee, DateDifference());
				}
			}
			InitFee = MotorVehicle.GetInitialPlateFees2(rsVehicles.Get_Fields_String("Class"), rsVehicles.Get_Fields_String("Plate"));
			CommCred = GetComm();
			CalcStateFee = RegFee - CommCred;
			if (CalcStateFee < 0)
				CalcStateFee = 0;
			CalcStateFee += InitFee + SpecialFee;
			return CalcStateFee;
		}

		public int DateDifference()
		{
			int DateDifference = 0;
			int x;
			x = fecherFoundation.DateAndTime.DateDiff("m", (DateTime)rsVehicles.Get_Fields_DateTime("ExpireDate"), FCConvert.ToDateTime("3/31/2013")) + 1;
			if (x < 0)
			{
				x *= -1;
			}
			else if (x == 0)
			{
				x = 1;
			}
			else if (x > 12)
			{
				x = 12;
			}
			DateDifference = x;
			return DateDifference;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
		public Decimal Prorate(ref Decimal curAmount, int intMonths)
		{
			Decimal Prorate = 0;
			Decimal total;
			total = FCConvert.ToDecimal(curAmount) / 12;
			total *= intMonths;
			Prorate = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(total), 2));
			return Prorate;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(string)
		private Decimal CalcExciseFee(ref clsDRWrapper rs)
		{
			Decimal CalcExciseFee = 0;
			// vbPorter upgrade warning: MilYear As int	OnWriteFCConvert.ToInt32(
			int MilYear = 0;
			float MilRate = 0;
			double Base = 0;
			// vbPorter upgrade warning: Excise As Decimal	OnWrite(int, string, Decimal)
			Decimal Excise = 0;
			clsDRWrapper rsExciseExempt = new clsDRWrapper();
			rsExciseExempt.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + rs.Get_Fields_String("Class") + "' AND SystemCode = '" + rs.Get_Fields_String("Subclass") + "'", "TWMV0000.vb1");
			bool executeCalcTax = false;
			if (rsExciseExempt.EndOfFile() != true && rsExciseExempt.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(rsExciseExempt.Get_Fields_String("ExciseRequired")) == "N")
				{
					Excise = 0;
				}
				else
				{
					executeCalcTax = true;
					goto CalcTax;
				}
			}
			else
			{
				executeCalcTax = true;
				goto CalcTax;
			}
			CalcTax:
			;
			if (executeCalcTax)
			{
				if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("MillYear")))
				{
					if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) > 0)
					{
						MilYear = rs.Get_Fields_Int32("MillYear") + 1;
					}
					else
					{
						MilYear = ((DateTime.Today.Year - rs.Get_Fields("Year")) + 1);
					}
				}
				else
				{
					MilYear = ((DateTime.Today.Year - rs.Get_Fields("Year")) + 1);
				}
				if (MilYear > 6)
				{
					MilYear = 6;
				}
				switch (MilYear)
				{
					case 1:
						{
							MilRate = 0.024f;
							break;
						}
					case 2:
						{
							MilRate = 0.0175f;
							break;
						}
					case 3:
						{
							MilRate = 0.0135f;
							break;
						}
					case 4:
						{
							MilRate = 0.01f;
							break;
						}
					case 5:
						{
							MilRate = 0.0065f;
							break;
						}
					case 6:
						{
							MilRate = 0.004f;
							break;
						}
				}
				//end switch
				Base = rs.Get_Fields_Int32("BasePrice");
				if (Base == 0)
				{
					FeeWrong = true;
				}
				Excise = FCConvert.ToDecimal(Strings.Format(MilRate * Base, "#0.00"));
				if (MotorVehicle.IsMotorcycle(rsVehicles.Get_Fields_String("Class")))
				{
					if ((cboMonth.SelectedIndex > 2 && cboYear.Text == "2012") || (cboMonth.SelectedIndex < 2 && cboYear.Text == "2013"))
					{
						Excise = Prorate(ref Excise, DateDifference());
					}
				}
				if (Excise < 5)
					Excise = 5;
				executeCalcTax = false;
			}
			CalcExciseFee = FCConvert.ToDecimal(Strings.Format(Excise, "#0.00"));
			return CalcExciseFee;
		}

		private Decimal CalcAgentFee(ref clsDRWrapper rs)
		{
			Decimal CalcAgentFee = 0;
			int MilYear;
			float MilRate;
			double Base;
			Decimal Excise;
			float AgentFee;
			string tempRes = "";
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ResidenceCode")))
			{
				if (rs.Get_Fields_String("ResidenceCode").Length < 5)
				{
					tempRes = "0" + rs.Get_Fields_String("ResidenceCode");
				}
				else
				{
					tempRes = rs.Get_Fields_String("ResidenceCode");
				}
				if (tempRes == FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode")))
				{
					CalcAgentFee = FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegLocal"));
				}
				else
				{
					CalcAgentFee = FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegOOT"));
				}
			}
			else
			{
				FeeWrong = true;
			}
			return CalcAgentFee;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite
		private Decimal GetReg(string Class, string Subclass, int weight)
		{
			Decimal GetReg = 0;
			int WT;
			clsDRWrapper rsClassFee = new clsDRWrapper();
			clsDRWrapper rsGVWFee = new clsDRWrapper();
			GetReg = 0;
			WT = weight;
			if ( Class != "BH" && FCConvert.ToString(Class) != "AM" && FCConvert.ToString(Class) != "BU" && FCConvert.ToString(Class) != "CL" && FCConvert.ToString(Class) != "DV" && FCConvert.ToString(Class) != "FD" && FCConvert.ToString(Class) != "IU" && FCConvert.ToString(Class) != "TL" && FCConvert.ToString(Class) != "MC" && FCConvert.ToString(Class) != "PC" && FCConvert.ToString(Class) != "TR" && FCConvert.ToString(Class) != "VT" && FCConvert.ToString(Class) != "BB" && FCConvert.ToString(Class) != "LB" && FCConvert.ToString(Class) != "TS" && FCConvert.ToString(Class) != "SW" && FCConvert.ToString(Class) != "BC" && FCConvert.ToString(Class) != "AW" && FCConvert.ToString(Class) != "XV" && FCConvert.ToString(Class) != "VX" && FCConvert.ToString(Class) != "CV" && FCConvert.ToString(Class) != "MO" && FCConvert.ToString(Class) != "PH" && FCConvert.ToString(Class) != "PO" && FCConvert.ToString(Class) != "PS" && FCConvert.ToString(Class) != "AG" && FCConvert.ToString(Class) != "CR" && FCConvert.ToString(Class) != "CD" && FCConvert.ToString(Class) != "DS" && FCConvert.ToString(Class) != "WB" && FCConvert.ToString(Class) != "CM" && FCConvert.ToString(Class) != "DX" && FCConvert.ToString(Class) != "GS" && FCConvert.ToString(Class) != "UM" && FCConvert.ToString(Class) != "EM")
			{
				// tromvs-97 6.25.18 add EM
				Subclass = Class;
			}
			MotorVehicle.Statics.strSql = "SELECT * FROM CLASS WHERE BMVCode = '" + Class + "' And SystemCode =  '" + Subclass + "'";
			rsClassFee.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsClassFee.EndOfFile() != true && rsClassFee.BeginningOfFile() != true)
			{
				rsClassFee.MoveLast();
				rsClassFee.MoveFirst();
				if (fecherFoundation.FCUtils.IsNull(rsClassFee.Get_Fields("RegistrationFee")) == false)
				{
					if (Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.01 || Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.02 || Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.03)
					{
						string tempClass = "";
						tempClass = "FM";
						if (Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.01)
						{
							tempClass = "TK";
						}
						else if (Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.03 && WT > 54000 && FCConvert.ToString(Class) != "SE")
						{
							tempClass = "F2";
						}
						else if (Class == "SE")
						{
							tempClass = "SE";
						}
						MotorVehicle.Statics.strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = '" + tempClass + "' AND Low <= " + FCConvert.ToString(WT) + " AND High >= " + FCConvert.ToString(WT);
						rsGVWFee.OpenRecordset(MotorVehicle.Statics.strSql);
						if (rsGVWFee.EndOfFile() != true && rsGVWFee.BeginningOfFile() != true)
						{
							rsGVWFee.MoveLast();
							rsGVWFee.MoveFirst();
							GetReg = FCConvert.ToDecimal(Conversion.Val(rsGVWFee.Get_Fields("Fee")));
							if (GetReg == 0)
								GetReg = 35;
							rsClassFee.Reset();
							rsGVWFee.Reset();
							return GetReg;
						}
					}
					GetReg = FCConvert.ToDecimal(Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")));
				}
				else
				{
					GetReg = 0;
				}
			}
			else
			{
				FeeWrong = true;
			}
			rsClassFee.Reset();
			rsGVWFee.Reset();
			return GetReg;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite
		private Decimal GetComm()
		{
			Decimal GetComm = 0;
			if (Conversion.Val(rsVehicles.Get_Fields_String("plate")) > 799999 && Conversion.Val(rsVehicles.Get_Fields_String("plate")) < 900000)
			{
				if (Conversion.Val(rsVehicles.Get_Fields_Int32("registeredWeightNew") + "") > 23000)
				{
					if (Strings.Mid(FCConvert.ToString(rsVehicles.Get_Fields_String("Style")), 2, 1) == "5")
					{
						GetComm = 40;
					}
					else
					{
						GetComm = 0;
					}
				}
			}
			return GetComm;
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
			{
				vsVehicles.TextMatrix(counter, PrintCol, FCConvert.ToString(true));
			}
		}

		private void FillLabelTypeCombo()
		{
			int counter;
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_Visible(counter))
				{
					cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
					cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256)
					{
						cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
						cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
					}
				}
			}
		}
	}
}
