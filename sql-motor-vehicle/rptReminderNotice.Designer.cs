﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReminderNotice.
	/// </summary>
	partial class rptReminderNotice
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderNotice));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.fldPlate,
				this.fldYear,
				this.fldMake,
				this.fldModel,
				this.fldExpires,
				this.fldAmount,
				this.fldName,
				this.fldAddress,
				this.fldCityStateZip,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Line1,
				this.Line2,
				this.fldMessage
			});
			this.Detail.Height = 4F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label1.Text = "Plate";
			this.Label1.Top = 0.5F;
			this.Label1.Width = 0.5625F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 2.5625F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPlate.Text = "Field1";
			this.fldPlate.Top = 0.5F;
			this.fldPlate.Width = 0.75F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 0.5625F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 1F;
			this.fldYear.Width = 0.4375F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.1875F;
			this.fldMake.Left = 1F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 1F;
			this.fldMake.Width = 0.5625F;
			// 
			// fldModel
			// 
			this.fldModel.Height = 0.1875F;
			this.fldModel.Left = 1.625F;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldModel.Text = "Field1";
			this.fldModel.Top = 1F;
			this.fldModel.Width = 0.875F;
			// 
			// fldExpires
			// 
			this.fldExpires.Height = 0.1875F;
			this.fldExpires.Left = 0.5625F;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExpires.Text = "Field1";
			this.fldExpires.Top = 1.5F;
			this.fldExpires.Width = 1.03125F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 1.71875F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 1.5F;
			this.fldAmount.Width = 3.4375F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1.5F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldName.Text = "Field1";
			this.fldName.Top = 2F;
			this.fldName.Width = 4.3125F;
			// 
			// fldAddress
			// 
			this.fldAddress.Height = 0.1875F;
			this.fldAddress.Left = 1.5F;
			this.fldAddress.Name = "fldAddress";
			this.fldAddress.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldAddress.Text = "Field1";
			this.fldAddress.Top = 2.1875F;
			this.fldAddress.Width = 4.3125F;
			// 
			// fldCityStateZip
			// 
			this.fldCityStateZip.Height = 0.1875F;
			this.fldCityStateZip.Left = 1.5F;
			this.fldCityStateZip.Name = "fldCityStateZip";
			this.fldCityStateZip.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldCityStateZip.Text = "Field1";
			this.fldCityStateZip.Top = 2.375F;
			this.fldCityStateZip.Width = 4.3125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.96875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Roman 6cpi\'; font-weight: bold; text-align: center; ddo-char-set: 1" + "";
			this.Label2.Text = "REGISTRATION REMINDER";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 4.0625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.5F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label3.Text = "IF YOU INTEND TO REGISTER YOUR:";
			this.Label3.Top = 0.78125F;
			this.Label3.Width = 2.65625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label4.Text = "YOUR REGISTRATION EXPIRES:";
			this.Label4.Top = 1.28125F;
			this.Label4.Width = 2.65625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.6875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Roman 6cpi\'; font-weight: bold; text-align: center; ddo-char-set: 1" + "";
			this.Label5.Text = "TO:";
			this.Label5.Top = 2F;
			this.Label5.Width = 0.6875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.5F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.1875F;
			this.Line1.Width = 2.65625F;
			this.Line1.X1 = 0.5F;
			this.Line1.X2 = 3.15625F;
			this.Line1.Y1 = 1.1875F;
			this.Line1.Y2 = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.46875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.6875F;
			this.Line2.Width = 2.65625F;
			this.Line2.X1 = 0.46875F;
			this.Line2.X2 = 3.125F;
			this.Line2.Y1 = 1.6875F;
			this.Line2.Y2 = 1.6875F;
			// 
			// fldMessage
			// 
			this.fldMessage.CanGrow = false;
			this.fldMessage.Height = 1.25F;
			this.fldMessage.Left = 0.125F;
			this.fldMessage.Name = "fldMessage";
			this.fldMessage.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldMessage.Text = null;
			this.fldMessage.Top = 2.6875F;
			this.fldMessage.Width = 5.8125F;
			// 
			// rptReminderNotice
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMessage;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
