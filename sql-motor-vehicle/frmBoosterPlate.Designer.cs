//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmBoosterPlate.
	/// </summary>
	partial class frmBoosterPlate
	{
		public fecherFoundation.FCComboBox cmbBoostOriginalWeight;
		public fecherFoundation.FCLabel lblBoostOriginalWeight;
        public fecherFoundation.FCLabel lblActiveBoosters;
		public FCGrid vsBooster;
		public fecherFoundation.FCTextBox txtBoostedWeight;
		public fecherFoundation.FCTextBox txtCurrentWeight;
		public fecherFoundation.FCTextBox txtPermit;
		public fecherFoundation.FCComboBox cboLength;
		public fecherFoundation.FCButton cmdCalculate;
		public Global.T2KDateBox txtExpireDate;
		public Global.T2KDateBox txtEffectiveDate;
		public fecherFoundation.FCLabel lblFeeAmount;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblPermit;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblOldWeightGroup;
		public fecherFoundation.FCLabel lblNewWeightGroup;
		public fecherFoundation.FCLabel lblFee;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbBoostOriginalWeight = new fecherFoundation.FCComboBox();
			this.lblBoostOriginalWeight = new fecherFoundation.FCLabel();
			this.lblActiveBoosters = new fecherFoundation.FCLabel();
			this.vsBooster = new fecherFoundation.FCGrid();
			this.txtBoostedWeight = new fecherFoundation.FCTextBox();
			this.txtCurrentWeight = new fecherFoundation.FCTextBox();
			this.txtPermit = new fecherFoundation.FCTextBox();
			this.cboLength = new fecherFoundation.FCComboBox();
			this.cmdCalculate = new fecherFoundation.FCButton();
			this.txtExpireDate = new Global.T2KDateBox();
			this.txtEffectiveDate = new Global.T2KDateBox();
			this.lblFeeAmount = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblPermit = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblOldWeightGroup = new fecherFoundation.FCLabel();
			this.lblNewWeightGroup = new fecherFoundation.FCLabel();
			this.lblFee = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBooster)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 703);
			this.BottomPanel.Size = new System.Drawing.Size(552, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsBooster);
			this.ClientArea.Controls.Add(this.cmbBoostOriginalWeight);
			this.ClientArea.Controls.Add(this.lblBoostOriginalWeight);
			this.ClientArea.Controls.Add(this.lblActiveBoosters);
			this.ClientArea.Controls.Add(this.txtBoostedWeight);
			this.ClientArea.Controls.Add(this.txtCurrentWeight);
			this.ClientArea.Controls.Add(this.txtPermit);
			this.ClientArea.Controls.Add(this.cboLength);
			this.ClientArea.Controls.Add(this.cmdCalculate);
			this.ClientArea.Controls.Add(this.txtExpireDate);
			this.ClientArea.Controls.Add(this.txtEffectiveDate);
			this.ClientArea.Controls.Add(this.lblFeeAmount);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblPermit);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.lblOldWeightGroup);
			this.ClientArea.Controls.Add(this.lblNewWeightGroup);
			this.ClientArea.Controls.Add(this.lblFee);
			this.ClientArea.Size = new System.Drawing.Size(572, 628);
			this.ClientArea.Controls.SetChildIndex(this.lblFee, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblNewWeightGroup, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblOldWeightGroup, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblPermit, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblFeeAmount, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtEffectiveDate, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtExpireDate, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdCalculate, 0);
			this.ClientArea.Controls.SetChildIndex(this.cboLength, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtPermit, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtCurrentWeight, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtBoostedWeight, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblActiveBoosters, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblBoostOriginalWeight, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbBoostOriginalWeight, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsBooster, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(572, 60);
			this.TopPanel.TabIndex = 0;
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(175, 30);
			this.HeaderText.Text = "Booster Permit";
			// 
			// cmbBoostOriginalWeight
			// 
			this.cmbBoostOriginalWeight.Items.AddRange(new object[] {
            "Boost Registered Vehicle Weight",
            "Boost Active Booster"});
			this.cmbBoostOriginalWeight.Location = new System.Drawing.Point(139, 30);
			this.cmbBoostOriginalWeight.Name = "cmbBoostOriginalWeight";
			this.cmbBoostOriginalWeight.Size = new System.Drawing.Size(381, 40);
			this.cmbBoostOriginalWeight.TabIndex = 1;
			this.cmbBoostOriginalWeight.Text = "Boost Registered Vehicle Weight";
			this.cmbBoostOriginalWeight.SelectedIndexChanged += new System.EventHandler(this.cmbBoostOriginalWeight_SelectedIndexChanged);
			// 
			// lblBoostOriginalWeight
			// 
			this.lblBoostOriginalWeight.AutoSize = true;
			this.lblBoostOriginalWeight.Location = new System.Drawing.Point(30, 44);
			this.lblBoostOriginalWeight.Name = "lblBoostOriginalWeight";
			this.lblBoostOriginalWeight.Size = new System.Drawing.Size(55, 16);
			this.lblBoostOriginalWeight.Text = "BOOST";
			// 
			// lblActiveBoosters
			// 
			this.lblActiveBoosters.AutoSize = true;
			this.lblActiveBoosters.Location = new System.Drawing.Point(30, 80);
			this.lblActiveBoosters.Name = "lblActiveBoosters";
			this.lblActiveBoosters.Size = new System.Drawing.Size(134, 16);
			this.lblActiveBoosters.TabIndex = 1001;
			this.lblActiveBoosters.Text = "ACTIVE BOOSTERS";
			// 
			// vsBooster
			// 
			this.vsBooster.Cols = 4;
			this.vsBooster.FixedCols = 0;
			this.vsBooster.Location = new System.Drawing.Point(30, 100);
			this.vsBooster.Name = "vsBooster";
			this.vsBooster.RowHeadersVisible = false;
			this.vsBooster.Rows = 1;
			this.vsBooster.ShowFocusCell = false;
			this.vsBooster.Size = new System.Drawing.Size(490, 155);
			this.vsBooster.TabIndex = 2;
			this.vsBooster.CurrentCellChanged += new System.EventHandler(this.vsBooster_RowColChange);
			// 
			// txtBoostedWeight
			// 
			this.txtBoostedWeight.BackColor = System.Drawing.SystemColors.Window;
			this.txtBoostedWeight.Location = new System.Drawing.Point(201, 603);
			this.txtBoostedWeight.MaxLength = 6;
			this.txtBoostedWeight.Name = "txtBoostedWeight";
			this.txtBoostedWeight.Size = new System.Drawing.Size(167, 40);
			this.txtBoostedWeight.TabIndex = 16;
			this.txtBoostedWeight.Validating += new System.ComponentModel.CancelEventHandler(this.txtBoostedWeight_Validating);
			// 
			// txtCurrentWeight
			// 
			this.txtCurrentWeight.BackColor = System.Drawing.SystemColors.Window;
			this.txtCurrentWeight.Location = new System.Drawing.Point(201, 543);
			this.txtCurrentWeight.MaxLength = 5;
			this.txtCurrentWeight.Name = "txtCurrentWeight";
			this.txtCurrentWeight.Size = new System.Drawing.Size(167, 40);
			this.txtCurrentWeight.TabIndex = 14;
			// 
			// txtPermit
			// 
			this.txtPermit.BackColor = System.Drawing.SystemColors.Window;
			this.txtPermit.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
			this.txtPermit.Location = new System.Drawing.Point(201, 423);
			this.txtPermit.MaxLength = 6;
			this.txtPermit.Name = "txtPermit";
			this.txtPermit.Size = new System.Drawing.Size(167, 40);
			this.txtPermit.TabIndex = 10;
			this.txtPermit.Validating += new System.ComponentModel.CancelEventHandler(this.txtPermit_Validating);
			// 
			// cboLength
			// 
			this.cboLength.BackColor = System.Drawing.SystemColors.Window;
			this.cboLength.Items.AddRange(new object[] {
            "1 Month",
            "2 Months",
            "3 Months",
            "4 Months",
            "5 Months",
            "6 Months",
            "7 Months",
            "8 Months"});
			this.cboLength.Location = new System.Drawing.Point(201, 483);
			this.cboLength.Name = "cboLength";
			this.cboLength.Size = new System.Drawing.Size(167, 40);
			this.cboLength.TabIndex = 12;
			this.cboLength.Validating += new System.ComponentModel.CancelEventHandler(this.cboLength_Validating);
			// 
			// cmdCalculate
			// 
			this.cmdCalculate.AppearanceKey = "actionButton";
			this.cmdCalculate.Location = new System.Drawing.Point(30, 663);
			this.cmdCalculate.Name = "cmdCalculate";
			this.cmdCalculate.Size = new System.Drawing.Size(152, 40);
			this.cmdCalculate.TabIndex = 17;
			this.cmdCalculate.Text = "Calculate Fee";
			this.cmdCalculate.Click += new System.EventHandler(this.cmdCalculate_Click);
			// 
			// txtExpireDate
			// 
			this.txtExpireDate.Location = new System.Drawing.Point(201, 325);
			this.txtExpireDate.Mask = "##/##/####";
			this.txtExpireDate.Name = "txtExpireDate";
			this.txtExpireDate.Size = new System.Drawing.Size(115, 22);
			this.txtExpireDate.TabIndex = 6;
			this.txtExpireDate.TabStop = false;
			// 
			// txtEffectiveDate
			// 
			this.txtEffectiveDate.Location = new System.Drawing.Point(201, 265);
			this.txtEffectiveDate.Mask = "##/##/####";
			this.txtEffectiveDate.Name = "txtEffectiveDate";
			this.txtEffectiveDate.Size = new System.Drawing.Size(115, 22);
			this.txtEffectiveDate.TabIndex = 4;
			this.txtEffectiveDate.TabStop = false;
			this.txtEffectiveDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtEffectiveDate_Validate);
			// 
			// lblFeeAmount
			// 
			this.lblFeeAmount.Location = new System.Drawing.Point(79, 385);
			this.lblFeeAmount.Name = "lblFeeAmount";
			this.lblFeeAmount.Size = new System.Drawing.Size(66, 18);
			this.lblFeeAmount.TabIndex = 8;
			this.lblFeeAmount.Text = "$0.00";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 279);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(100, 16);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "EFFECTIVE DATE";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 339);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(110, 16);
			this.Label2.TabIndex = 5;
			this.Label2.Text = "EXPIRATION DATE";
			// 
			// lblPermit
			// 
			this.lblPermit.Location = new System.Drawing.Point(30, 437);
			this.lblPermit.Name = "lblPermit";
			this.lblPermit.Size = new System.Drawing.Size(60, 16);
			this.lblPermit.TabIndex = 9;
			this.lblPermit.Text = "PERMIT #";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 497);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(100, 16);
			this.Label3.TabIndex = 11;
			this.Label3.Text = "PERMIT LENGTH";
			// 
			// lblOldWeightGroup
			// 
			this.lblOldWeightGroup.Location = new System.Drawing.Point(30, 557);
			this.lblOldWeightGroup.Name = "lblOldWeightGroup";
			this.lblOldWeightGroup.Size = new System.Drawing.Size(124, 16);
			this.lblOldWeightGroup.TabIndex = 13;
			this.lblOldWeightGroup.Text = "CURRENT WEIGHT";
			// 
			// lblNewWeightGroup
			// 
			this.lblNewWeightGroup.Location = new System.Drawing.Point(30, 617);
			this.lblNewWeightGroup.Name = "lblNewWeightGroup";
			this.lblNewWeightGroup.Size = new System.Drawing.Size(134, 16);
			this.lblNewWeightGroup.TabIndex = 15;
			this.lblNewWeightGroup.Text = "BOOSTED WEIGHT";
			// 
			// lblFee
			// 
			this.lblFee.Location = new System.Drawing.Point(30, 385);
			this.lblFee.Name = "lblFee";
			this.lblFee.Size = new System.Drawing.Size(27, 16);
			this.lblFee.TabIndex = 7;
			this.lblFee.Text = "FEE";
			this.lblFee.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(243, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmBoosterPlate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(572, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBoosterPlate";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = " Booster Permit";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmBoosterPlate_Load);
			this.Activated += new System.EventHandler(this.frmBoosterPlate_Activated);
			this.Resize += new System.EventHandler(this.frmBoosterPlate_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBoosterPlate_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBoosterPlate_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBooster)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
