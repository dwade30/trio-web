using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for InventoryMaster.
	/// </summary>
	public partial class rptInventoryMaster : BaseSectionReport
	{
		private bool showMVR3 = false;
		private bool showSingle = false;
		private bool showDouble = false;
		private bool showCombination = false;
		private bool showPlate = false;
		private bool showPermit = false;

		public rptInventoryMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Reconcilliation Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptInventoryMaster InstancePtr
		{
			get
			{
				return (rptInventoryMaster)Sys.GetInstance(typeof(rptInventoryMaster));
			}
		}

		protected rptInventoryMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(bool showMVR3, bool showSingle, bool showDouble, bool showPlate, bool showCombination, bool showPermit)
		{
			this.showMVR3 = showMVR3;
			this.showSingle = showSingle;
			this.showCombination = showCombination;
			this.showDouble = showDouble;
			this.showPermit = showPermit;
			this.showPlate = showPlate;
		}

		private void detail_Format(object sender, EventArgs e)
		{
			if (showMVR3)
			{
				srptMVR3.Report = new rptReconMVR3();
			}
			else
			{
				srptMVR3.Report = null;
				srptMVR3.Top = 0;
				srptMVR3.Visible = false;
			}

			if (showSingle)
			{
				srptSingle.Report = new rptReconSingle();
			}
			else
			{
				srptSingle.Report = null;
				srptSingle.Top = 0;
				srptSingle.Visible = false;
			}

			if (showCombination)
			{
				srptCombination.Report = new rptReconCombination();
			}
			else
			{
				srptCombination.Report = null;
				srptCombination.Top = 0;
				srptCombination.Visible = false;
			}

			if (showDouble)
			{
				srptDouble.Report = new rptReconDouble();
			}
			else
			{
				srptDouble.Report = null;
				srptDouble.Top = 0;
				srptDouble.Visible = false;
			}

			if (showPlate)
			{
				srptPlate.Report = new rptReconPlates();
			}
			else
			{
				srptPlate.Report = null;
				srptPlate.Top = 0;
				srptPlate.Visible = false;
			}

			if (showPermit)
			{
				srptPermit.Report = new rptReconPermits();
			}
			else
			{
				srptPermit.Report = null;
				srptPermit.Top = 0;
				srptPermit.Visible = false;
			}
		}
	}
}
