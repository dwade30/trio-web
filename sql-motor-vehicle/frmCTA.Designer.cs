//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmCTA.
	/// </summary>
	partial class frmCTA
	{
		public fecherFoundation.FCComboBox cmbDoubleVehicleType;
		public fecherFoundation.FCLabel lblDoubleVehicleType;
		public fecherFoundation.FCComboBox cmbDoubleDealer;
		public fecherFoundation.FCComboBox cmbCTAPrint;
		public fecherFoundation.FCComboBox cmbMSRP;
		public fecherFoundation.FCLabel lblMSRP;
		public fecherFoundation.FCComboBox cmbVehicleType;
		public fecherFoundation.FCLabel lblVehicleType;
		public fecherFoundation.FCComboBox cmbOdometer;
		public fecherFoundation.FCComboBox cmbOdometerCondition;
		public fecherFoundation.FCLabel lblOdometerCondition;
		public fecherFoundation.FCComboBox cmbDealer;
		public fecherFoundation.FCFrame fraDoubleInfo;
		public fecherFoundation.FCCheckBox chkPrintDouble;
		public fecherFoundation.FCFrame fraDoubleOwner;
		public Global.T2KDateBox txtOriginalDOB;
		public fecherFoundation.FCLabel lblDOB;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCFrame fraDoubleVehicleInfo;
		public fecherFoundation.FCTextBox txtDoublePreviousState;
		public Global.T2KBackFillWhole txtDoubleCylinders;
		public Global.T2KDateBox txtDoublePurchaseDate;
		public Global.T2KOverTypeBox txtDoublePreviousTitle;
		public Global.T2KOverTypeBox txtDoubleCTANumber;
		public fecherFoundation.FCLabel Label38;
		public fecherFoundation.FCLabel Label37;
		public fecherFoundation.FCLabel Label36;
		public fecherFoundation.FCLabel Label35;
		public fecherFoundation.FCLabel Label34;
		public fecherFoundation.FCFrame fraDoubleSellerInfo;
		public fecherFoundation.FCTextBox txtDoubleSellerName;
		public fecherFoundation.FCTextBox txtDoubleZip;
		public fecherFoundation.FCTextBox txtDoubleState;
		public fecherFoundation.FCTextBox txtDoubleCity;
		public fecherFoundation.FCTextBox txtDoubleSellerAddress;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtDoublePlate;
		public fecherFoundation.FCLabel Label30;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCLabel Label32;
		public fecherFoundation.FCLabel Label33;
		public fecherFoundation.FCCheckBox chkRushTitle;
		public fecherFoundation.FCCheckBox chkFleet;
		public Global.T2KOverTypeBox txtCTANumber;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCCheckBox chkCTASupp;
		public fecherFoundation.FCCheckBox chkGetOwnerInfo;
		public fecherFoundation.FCTextBox txtUTCSellerAddress;
		public fecherFoundation.FCTextBox txtUTCSellerCity;
		public fecherFoundation.FCTextBox txtUTCSellerState;
		public fecherFoundation.FCTextBox txtUTCSellerZip;
		public fecherFoundation.FCTextBox txtUTCSellerName1;
		public fecherFoundation.FCTextBox txtUTCSellerName2;
		public fecherFoundation.FCCheckBox chkCTAJoint;
		public fecherFoundation.FCTextBox txtCTAResAddress;
		public fecherFoundation.FCTextBox txtCTALegalZip;
		public fecherFoundation.FCTextBox txtCTAApplicantState;
		public fecherFoundation.FCTextBox txtCTAResTown;
		public Global.T2KDateBox txtCTADOB2;
		public Global.T2KPhoneNumberBox txtCTATelNumber;
		public Global.T2KDateBox txtCTADOB1;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCFrame fraVehicleInfo;
		public fecherFoundation.FCTextBox txtCTAStateOfOrigin;
		public Global.T2KBackFillWhole txtCTANumberOfCylinders;
		public Global.T2KDateBox txtCATDatePurchased;
		public Global.T2KOverTypeBox txtCTAPreviousNumber;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCFrame fraLienHolder;
		public fecherFoundation.FCTextBox txtCTALienOneName;
		public fecherFoundation.FCTextBox txtCTALienOneZip;
		public fecherFoundation.FCTextBox txtCTALienOneState;
		public fecherFoundation.FCTextBox txtCTALienOneCity;
		public fecherFoundation.FCTextBox txtCTALienOneAddress;
		public fecherFoundation.FCTextBox txtCTALienTwoName;
		public fecherFoundation.FCTextBox txtCTALienTwoZip;
		public fecherFoundation.FCTextBox txtCTALienTwoState;
		public fecherFoundation.FCTextBox txtCTALienTwoCity;
		public fecherFoundation.FCTextBox txtCTALienTwoAddress;
		public Global.T2KDateBox txt1stLienDate;
		public Global.T2KDateBox txt2ndLienDate;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtCTASellerName;
		public fecherFoundation.FCTextBox txtCTASellerZip;
		public fecherFoundation.FCTextBox txtCTASellerState;
		public fecherFoundation.FCTextBox txtCTASellerCity;
		public fecherFoundation.FCTextBox txtCTASellerAddress;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtCTADealerPlate;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCCheckBox chkDouble;
		public fecherFoundation.FCCheckBox chkNoFee;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeparator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbDoubleVehicleType = new fecherFoundation.FCComboBox();
            this.lblDoubleVehicleType = new fecherFoundation.FCLabel();
            this.cmbDoubleDealer = new fecherFoundation.FCComboBox();
            this.cmbCTAPrint = new fecherFoundation.FCComboBox();
            this.cmbMSRP = new fecherFoundation.FCComboBox();
            this.lblMSRP = new fecherFoundation.FCLabel();
            this.cmbVehicleType = new fecherFoundation.FCComboBox();
            this.lblVehicleType = new fecherFoundation.FCLabel();
            this.cmbOdometer = new fecherFoundation.FCComboBox();
            this.cmbOdometerCondition = new fecherFoundation.FCComboBox();
            this.lblOdometerCondition = new fecherFoundation.FCLabel();
            this.cmbDealer = new fecherFoundation.FCComboBox();
            this.fraDoubleInfo = new fecherFoundation.FCFrame();
            this.chkPrintDouble = new fecherFoundation.FCCheckBox();
            this.fraDoubleOwner = new fecherFoundation.FCFrame();
            this.txtOriginalDOB = new Global.T2KDateBox();
            this.lblDOB = new fecherFoundation.FCLabel();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.fraDoubleVehicleInfo = new fecherFoundation.FCFrame();
            this.txtDoublePreviousState = new fecherFoundation.FCTextBox();
            this.txtDoubleCylinders = new Global.T2KBackFillWhole();
            this.txtDoublePurchaseDate = new Global.T2KDateBox();
            this.txtDoublePreviousTitle = new Global.T2KOverTypeBox();
            this.txtDoubleCTANumber = new Global.T2KOverTypeBox();
            this.Label38 = new fecherFoundation.FCLabel();
            this.Label37 = new fecherFoundation.FCLabel();
            this.Label36 = new fecherFoundation.FCLabel();
            this.Label35 = new fecherFoundation.FCLabel();
            this.Label34 = new fecherFoundation.FCLabel();
            this.fraDoubleSellerInfo = new fecherFoundation.FCFrame();
            this.txtDoubleSellerName = new fecherFoundation.FCTextBox();
            this.txtDoubleZip = new fecherFoundation.FCTextBox();
            this.txtDoubleState = new fecherFoundation.FCTextBox();
            this.txtDoubleCity = new fecherFoundation.FCTextBox();
            this.txtDoubleSellerAddress = new fecherFoundation.FCTextBox();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtDoublePlate = new fecherFoundation.FCTextBox();
            this.Label30 = new fecherFoundation.FCLabel();
            this.Label31 = new fecherFoundation.FCLabel();
            this.Label32 = new fecherFoundation.FCLabel();
            this.Label33 = new fecherFoundation.FCLabel();
            this.chkRushTitle = new fecherFoundation.FCCheckBox();
            this.chkFleet = new fecherFoundation.FCCheckBox();
            this.txtCTANumber = new Global.T2KOverTypeBox();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.chkCTASupp = new fecherFoundation.FCCheckBox();
            this.fraApplicant = new fecherFoundation.FCFrame();
            this.chkGetOwnerInfo = new fecherFoundation.FCCheckBox();
            this.txtUTCSellerAddress = new fecherFoundation.FCTextBox();
            this.txtUTCSellerCity = new fecherFoundation.FCTextBox();
            this.txtUTCSellerState = new fecherFoundation.FCTextBox();
            this.txtUTCSellerZip = new fecherFoundation.FCTextBox();
            this.txtUTCSellerName1 = new fecherFoundation.FCTextBox();
            this.txtUTCSellerName2 = new fecherFoundation.FCTextBox();
            this.chkCTAJoint = new fecherFoundation.FCCheckBox();
            this.txtCTAResAddress = new fecherFoundation.FCTextBox();
            this.txtCTALegalZip = new fecherFoundation.FCTextBox();
            this.txtCTAApplicantState = new fecherFoundation.FCTextBox();
            this.txtCTAResTown = new fecherFoundation.FCTextBox();
            this.txtCTADOB2 = new Global.T2KDateBox();
            this.txtCTATelNumber = new Global.T2KPhoneNumberBox();
            this.txtCTADOB1 = new Global.T2KDateBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.fraVehicleInfo = new fecherFoundation.FCFrame();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.txtCTAStateOfOrigin = new fecherFoundation.FCTextBox();
            this.txtCTANumberOfCylinders = new Global.T2KBackFillWhole();
            this.txtCATDatePurchased = new Global.T2KDateBox();
            this.txtCTAPreviousNumber = new Global.T2KOverTypeBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.fraLienHolder = new fecherFoundation.FCFrame();
            this.txtCTALienOneName = new fecherFoundation.FCTextBox();
            this.txtCTALienOneZip = new fecherFoundation.FCTextBox();
            this.txtCTALienOneState = new fecherFoundation.FCTextBox();
            this.txtCTALienOneCity = new fecherFoundation.FCTextBox();
            this.txtCTALienOneAddress = new fecherFoundation.FCTextBox();
            this.txtCTALienTwoName = new fecherFoundation.FCTextBox();
            this.txtCTALienTwoZip = new fecherFoundation.FCTextBox();
            this.txtCTALienTwoState = new fecherFoundation.FCTextBox();
            this.txtCTALienTwoCity = new fecherFoundation.FCTextBox();
            this.txtCTALienTwoAddress = new fecherFoundation.FCTextBox();
            this.txt1stLienDate = new Global.T2KDateBox();
            this.txt2ndLienDate = new Global.T2KDateBox();
            this.Label29 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtCTASellerName = new fecherFoundation.FCTextBox();
            this.txtCTASellerZip = new fecherFoundation.FCTextBox();
            this.txtCTASellerState = new fecherFoundation.FCTextBox();
            this.txtCTASellerCity = new fecherFoundation.FCTextBox();
            this.txtCTASellerAddress = new fecherFoundation.FCTextBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtCTADealerPlate = new fecherFoundation.FCTextBox();
            this.Label28 = new fecherFoundation.FCLabel();
            this.Label25 = new fecherFoundation.FCLabel();
            this.Label26 = new fecherFoundation.FCLabel();
            this.Label27 = new fecherFoundation.FCLabel();
            this.chkDouble = new fecherFoundation.FCCheckBox();
            this.chkNoFee = new fecherFoundation.FCCheckBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeparator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFileSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleInfo)).BeginInit();
            this.fraDoubleInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintDouble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleOwner)).BeginInit();
            this.fraDoubleOwner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleVehicleInfo)).BeginInit();
            this.fraDoubleVehicleInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCylinders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoublePurchaseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoublePreviousTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCTANumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleSellerInfo)).BeginInit();
            this.fraDoubleSellerInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRushTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFleet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTANumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCTASupp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).BeginInit();
            this.fraApplicant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetOwnerInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCTAJoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTADOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTATelNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTADOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraVehicleInfo)).BeginInit();
            this.fraVehicleInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTANumberOfCylinders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCATDatePurchased)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTAPreviousNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraLienHolder)).BeginInit();
            this.fraLienHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt1stLienDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt2ndLienDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDouble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 1558);
            this.BottomPanel.Size = new System.Drawing.Size(828, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraDoubleInfo);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbCTAPrint);
            this.ClientArea.Controls.Add(this.chkRushTitle);
            this.ClientArea.Controls.Add(this.chkFleet);
            this.ClientArea.Controls.Add(this.txtCTANumber);
            this.ClientArea.Controls.Add(this.cmdReturn);
            this.ClientArea.Controls.Add(this.chkCTASupp);
            this.ClientArea.Controls.Add(this.fraApplicant);
            this.ClientArea.Controls.Add(this.fraVehicleInfo);
            this.ClientArea.Controls.Add(this.fraLienHolder);
            this.ClientArea.Controls.Add(this.chkDouble);
            this.ClientArea.Controls.Add(this.chkNoFee);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Size = new System.Drawing.Size(848, 628);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkNoFee, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkDouble, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraLienHolder, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraVehicleInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraApplicant, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkCTASupp, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdReturn, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCTANumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkFleet, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkRushTitle, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbCTAPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraDoubleInfo, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(848, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(394, 30);
            this.HeaderText.Text = "Certificate of Title Application";
            // 
            // cmbDoubleVehicleType
            // 
            this.cmbDoubleVehicleType.Items.AddRange(new object[] {
            "New",
            "Used",
            "Rebuilt"});
            this.cmbDoubleVehicleType.Location = new System.Drawing.Point(183, 30);
            this.cmbDoubleVehicleType.Name = "cmbDoubleVehicleType";
            this.cmbDoubleVehicleType.Size = new System.Drawing.Size(131, 40);
            this.cmbDoubleVehicleType.TabIndex = 1;
            this.cmbDoubleVehicleType.Text = "Used";
            // 
            // lblDoubleVehicleType
            // 
            this.lblDoubleVehicleType.AutoSize = true;
            this.lblDoubleVehicleType.Location = new System.Drawing.Point(20, 44);
            this.lblDoubleVehicleType.Name = "lblDoubleVehicleType";
            this.lblDoubleVehicleType.Size = new System.Drawing.Size(76, 15);
            this.lblDoubleVehicleType.TabIndex = 12;
            this.lblDoubleVehicleType.Text = "VEHICLE IS";
            // 
            // cmbDoubleDealer
            // 
            this.cmbDoubleDealer.Items.AddRange(new object[] {
            "MC",
            "UC",
            "D",
            "None"});
            this.cmbDoubleDealer.Location = new System.Drawing.Point(20, 30);
            this.cmbDoubleDealer.Name = "cmbDoubleDealer";
            this.cmbDoubleDealer.Size = new System.Drawing.Size(200, 40);
            this.cmbDoubleDealer.TabIndex = 3;
            this.cmbDoubleDealer.Text = "None";
            // 
            // cmbCTAPrint
            // 
            this.cmbCTAPrint.Items.AddRange(new object[] {
            "This Form NEEDS to be printed",
            "This Form does NOT need to be printed"});
            this.cmbCTAPrint.Location = new System.Drawing.Point(30, 1518);
            this.cmbCTAPrint.Name = "cmbCTAPrint";
            this.cmbCTAPrint.Size = new System.Drawing.Size(460, 40);
            this.cmbCTAPrint.TabIndex = 12;
            this.cmbCTAPrint.Text = "This Form NEEDS to be printed";
            // 
            // cmbMSRP
            // 
            this.cmbMSRP.Items.AddRange(new object[] {
            "New",
            "Used",
            "N/R"});
            this.cmbMSRP.Location = new System.Drawing.Point(189, 130);
            this.cmbMSRP.Name = "cmbMSRP";
            this.cmbMSRP.Size = new System.Drawing.Size(150, 40);
            this.cmbMSRP.TabIndex = 5;
            // 
            // lblMSRP
            // 
            this.lblMSRP.AutoSize = true;
            this.lblMSRP.Location = new System.Drawing.Point(20, 144);
            this.lblMSRP.Name = "lblMSRP";
            this.lblMSRP.Size = new System.Drawing.Size(42, 15);
            this.lblMSRP.TabIndex = 4;
            this.lblMSRP.Text = "MSRP";
            // 
            // cmbVehicleType
            // 
            this.cmbVehicleType.Items.AddRange(new object[] {
            "Rebuilt",
            "Used",
            "New"});
            this.cmbVehicleType.Location = new System.Drawing.Point(189, 30);
            this.cmbVehicleType.Name = "cmbVehicleType";
            this.cmbVehicleType.Size = new System.Drawing.Size(150, 40);
            this.cmbVehicleType.TabIndex = 1;
            this.cmbVehicleType.Text = "Used";
            // 
            // lblVehicleType
            // 
            this.lblVehicleType.AutoSize = true;
            this.lblVehicleType.Location = new System.Drawing.Point(20, 44);
            this.lblVehicleType.Name = "lblVehicleType";
            this.lblVehicleType.Size = new System.Drawing.Size(76, 15);
            this.lblVehicleType.TabIndex = 3;
            this.lblVehicleType.Text = "VEHICLE IS";
            // 
            // cmbOdometer
            // 
            this.cmbOdometer.Items.AddRange(new object[] {
            "Kilometers",
            "Miles"});
            this.cmbOdometer.Location = new System.Drawing.Point(189, 80);
            this.cmbOdometer.Name = "cmbOdometer";
            this.cmbOdometer.Size = new System.Drawing.Size(150, 40);
            this.cmbOdometer.TabIndex = 3;
            this.cmbOdometer.Text = "Miles";
            // 
            // cmbOdometerCondition
            // 
            this.cmbOdometerCondition.Items.AddRange(new object[] {
            "Broken",
            "Changed",
            "Excess",
            "Actual"});
            this.cmbOdometerCondition.Location = new System.Drawing.Point(189, 180);
            this.cmbOdometerCondition.Name = "cmbOdometerCondition";
            this.cmbOdometerCondition.Size = new System.Drawing.Size(150, 40);
            this.cmbOdometerCondition.TabIndex = 7;
            this.cmbOdometerCondition.Text = "Actual";
            // 
            // lblOdometerCondition
            // 
            this.lblOdometerCondition.AutoSize = true;
            this.lblOdometerCondition.Location = new System.Drawing.Point(20, 194);
            this.lblOdometerCondition.Name = "lblOdometerCondition";
            this.lblOdometerCondition.Size = new System.Drawing.Size(156, 15);
            this.lblOdometerCondition.TabIndex = 6;
            this.lblOdometerCondition.Text = "ODOMETER CONDITION";
            // 
            // cmbDealer
            // 
            this.cmbDealer.Items.AddRange(new object[] {
            "MC",
            "UC",
            "D",
            "None"});
            this.cmbDealer.Location = new System.Drawing.Point(20, 30);
            this.cmbDealer.Name = "cmbDealer";
            this.cmbDealer.Size = new System.Drawing.Size(211, 40);
            this.cmbDealer.TabIndex = 3;
            this.cmbDealer.Text = "None";
            // 
            // fraDoubleInfo
            // 
            this.fraDoubleInfo.AppearanceKey = "groupBoxLeftBorder";
            this.fraDoubleInfo.BackColor = System.Drawing.Color.White;
            this.fraDoubleInfo.Controls.Add(this.chkPrintDouble);
            this.fraDoubleInfo.Controls.Add(this.fraDoubleOwner);
            this.fraDoubleInfo.Controls.Add(this.cmdCancel);
            this.fraDoubleInfo.Controls.Add(this.cmdSave);
            this.fraDoubleInfo.Controls.Add(this.fraDoubleVehicleInfo);
            this.fraDoubleInfo.Controls.Add(this.fraDoubleSellerInfo);
            this.fraDoubleInfo.FormatCaption = false;
            this.fraDoubleInfo.Location = new System.Drawing.Point(30, 30);
            this.fraDoubleInfo.Name = "fraDoubleInfo";
            this.fraDoubleInfo.Size = new System.Drawing.Size(800, 1527);
            this.fraDoubleInfo.TabIndex = 1;
            this.fraDoubleInfo.Text = "Double CTA Information";
            this.fraDoubleInfo.Visible = false;
            // 
            // chkPrintDouble
            // 
            this.chkPrintDouble.Location = new System.Drawing.Point(20, 829);
            this.chkPrintDouble.Name = "chkPrintDouble";
            this.chkPrintDouble.Size = new System.Drawing.Size(162, 22);
            this.chkPrintDouble.TabIndex = 3;
            this.chkPrintDouble.Text = "Print the Double CTA?";
            // 
            // fraDoubleOwner
            // 
            this.fraDoubleOwner.Controls.Add(this.txtOriginalDOB);
            this.fraDoubleOwner.Controls.Add(this.lblDOB);
            this.fraDoubleOwner.Location = new System.Drawing.Point(20, 30);
            this.fraDoubleOwner.Name = "fraDoubleOwner";
            this.fraDoubleOwner.Size = new System.Drawing.Size(317, 90);
            this.fraDoubleOwner.TabIndex = 4;
            this.fraDoubleOwner.Text = "Owner Information";
            // 
            // txtOriginalDOB
            // 
            this.txtOriginalDOB.Location = new System.Drawing.Point(184, 30);
            this.txtOriginalDOB.Mask = "##/##/####";
            this.txtOriginalDOB.MaxLength = 10;
            this.txtOriginalDOB.Name = "txtOriginalDOB";
            this.txtOriginalDOB.Size = new System.Drawing.Size(115, 22);
            this.txtOriginalDOB.TabIndex = 1;
            this.txtOriginalDOB.Enter += new System.EventHandler(this.txtOriginalDOB_Enter);
            this.txtOriginalDOB.Leave += new System.EventHandler(this.txtOriginalDOB_Leave);
            // 
            // lblDOB
            // 
            this.lblDOB.Location = new System.Drawing.Point(20, 44);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(140, 17);
            this.lblDOB.TabIndex = 2;
            this.lblDOB.Text = "ORIGINAL OWNER DOB";
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(120, 866);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 40);
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "actionButton";
            this.cmdSave.Location = new System.Drawing.Point(20, 866);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(80, 40);
            this.cmdSave.TabIndex = 4;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // fraDoubleVehicleInfo
            // 
            this.fraDoubleVehicleInfo.Controls.Add(this.txtDoublePreviousState);
            this.fraDoubleVehicleInfo.Controls.Add(this.cmbDoubleVehicleType);
            this.fraDoubleVehicleInfo.Controls.Add(this.lblDoubleVehicleType);
            this.fraDoubleVehicleInfo.Controls.Add(this.txtDoubleCylinders);
            this.fraDoubleVehicleInfo.Controls.Add(this.txtDoublePurchaseDate);
            this.fraDoubleVehicleInfo.Controls.Add(this.txtDoublePreviousTitle);
            this.fraDoubleVehicleInfo.Controls.Add(this.txtDoubleCTANumber);
            this.fraDoubleVehicleInfo.Controls.Add(this.Label38);
            this.fraDoubleVehicleInfo.Controls.Add(this.Label37);
            this.fraDoubleVehicleInfo.Controls.Add(this.Label36);
            this.fraDoubleVehicleInfo.Controls.Add(this.Label35);
            this.fraDoubleVehicleInfo.Controls.Add(this.Label34);
            this.fraDoubleVehicleInfo.Location = new System.Drawing.Point(20, 480);
            this.fraDoubleVehicleInfo.Name = "fraDoubleVehicleInfo";
            this.fraDoubleVehicleInfo.Size = new System.Drawing.Size(334, 339);
            this.fraDoubleVehicleInfo.TabIndex = 2;
            this.fraDoubleVehicleInfo.Text = "Vehicle Information";
            // 
            // txtDoublePreviousState
            // 
            this.txtDoublePreviousState.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoublePreviousState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoublePreviousState.Location = new System.Drawing.Point(183, 280);
            this.txtDoublePreviousState.MaxLength = 2;
            this.txtDoublePreviousState.Name = "txtDoublePreviousState";
            this.txtDoublePreviousState.Size = new System.Drawing.Size(131, 40);
            this.txtDoublePreviousState.TabIndex = 11;
            this.txtDoublePreviousState.Enter += new System.EventHandler(this.txtDoublePreviousState_Enter);
            this.txtDoublePreviousState.Leave += new System.EventHandler(this.txtDoublePreviousState_Leave);
            // 
            // txtDoubleCylinders
            // 
            this.txtDoubleCylinders.Location = new System.Drawing.Point(183, 130);
            this.txtDoubleCylinders.MaxLength = 2;
            this.txtDoubleCylinders.Name = "txtDoubleCylinders";
            this.txtDoubleCylinders.Size = new System.Drawing.Size(131, 22);
            this.txtDoubleCylinders.TabIndex = 5;
            this.txtDoubleCylinders.Enter += new System.EventHandler(this.txtDoubleCylinders_Enter);
            this.txtDoubleCylinders.Leave += new System.EventHandler(this.txtDoubleCylinders_Leave);
            // 
            // txtDoublePurchaseDate
            // 
            this.txtDoublePurchaseDate.Location = new System.Drawing.Point(183, 180);
            this.txtDoublePurchaseDate.Mask = "##/##/####";
            this.txtDoublePurchaseDate.MaxLength = 10;
            this.txtDoublePurchaseDate.Name = "txtDoublePurchaseDate";
            this.txtDoublePurchaseDate.Size = new System.Drawing.Size(115, 22);
            this.txtDoublePurchaseDate.TabIndex = 7;
            this.txtDoublePurchaseDate.Enter += new System.EventHandler(this.txtDoublePurchaseDate_Enter);
            this.txtDoublePurchaseDate.Leave += new System.EventHandler(this.txtDoublePurchaseDate_Leave);
            // 
            // txtDoublePreviousTitle
            // 
            this.txtDoublePreviousTitle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoublePreviousTitle.Location = new System.Drawing.Point(183, 230);
            this.txtDoublePreviousTitle.MaxLength = 20;
            this.txtDoublePreviousTitle.Name = "txtDoublePreviousTitle";
            this.txtDoublePreviousTitle.Size = new System.Drawing.Size(131, 40);
            this.txtDoublePreviousTitle.TabIndex = 9;
            this.txtDoublePreviousTitle.Enter += new System.EventHandler(this.txtDoublePreviousTitle_Enter);
            this.txtDoublePreviousTitle.Leave += new System.EventHandler(this.txtDoublePreviousTitle_Leave);
            // 
            // txtDoubleCTANumber
            // 
            this.txtDoubleCTANumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoubleCTANumber.Location = new System.Drawing.Point(183, 80);
            this.txtDoubleCTANumber.MaxLength = 9;
            this.txtDoubleCTANumber.Name = "txtDoubleCTANumber";
            this.txtDoubleCTANumber.Size = new System.Drawing.Size(131, 40);
            this.txtDoubleCTANumber.TabIndex = 3;
            this.txtDoubleCTANumber.Enter += new System.EventHandler(this.txtDoubleCTANumber_Enter);
            this.txtDoubleCTANumber.Leave += new System.EventHandler(this.txtDoubleCTANumber_Leave);
            // 
            // Label38
            // 
            this.Label38.Location = new System.Drawing.Point(20, 94);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(50, 15);
            this.Label38.TabIndex = 2;
            this.Label38.Text = "CTA#";
            // 
            // Label37
            // 
            this.Label37.Location = new System.Drawing.Point(20, 294);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(120, 15);
            this.Label37.TabIndex = 10;
            this.Label37.Text = "STATE OF ORIGIN";
            // 
            // Label36
            // 
            this.Label36.Location = new System.Drawing.Point(20, 244);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(110, 17);
            this.Label36.TabIndex = 8;
            this.Label36.Text = "PREVIOUS TITLE #";
            // 
            // Label35
            // 
            this.Label35.Location = new System.Drawing.Point(20, 194);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(100, 17);
            this.Label35.TabIndex = 6;
            this.Label35.Text = "PURCHASE DATE";
            // 
            // Label34
            // 
            this.Label34.Location = new System.Drawing.Point(20, 144);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(165, 17);
            this.Label34.TabIndex = 4;
            this.Label34.Text = "NUMBER OF CYLINDERS";
            // 
            // fraDoubleSellerInfo
            // 
            this.fraDoubleSellerInfo.AppearanceKey = "groupBoxLeftBorder";
            this.fraDoubleSellerInfo.Controls.Add(this.txtDoubleSellerName);
            this.fraDoubleSellerInfo.Controls.Add(this.txtDoubleZip);
            this.fraDoubleSellerInfo.Controls.Add(this.txtDoubleState);
            this.fraDoubleSellerInfo.Controls.Add(this.txtDoubleCity);
            this.fraDoubleSellerInfo.Controls.Add(this.txtDoubleSellerAddress);
            this.fraDoubleSellerInfo.Controls.Add(this.Frame5);
            this.fraDoubleSellerInfo.Controls.Add(this.Label31);
            this.fraDoubleSellerInfo.Controls.Add(this.Label32);
            this.fraDoubleSellerInfo.Controls.Add(this.Label33);
            this.fraDoubleSellerInfo.Location = new System.Drawing.Point(20, 130);
            this.fraDoubleSellerInfo.Name = "fraDoubleSellerInfo";
            this.fraDoubleSellerInfo.Size = new System.Drawing.Size(394, 340);
            this.fraDoubleSellerInfo.TabIndex = 1;
            this.fraDoubleSellerInfo.Text = "Seller Information";
            // 
            // txtDoubleSellerName
            // 
            this.txtDoubleSellerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoubleSellerName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoubleSellerName.Location = new System.Drawing.Point(110, 30);
            this.txtDoubleSellerName.MaxLength = 30;
            this.txtDoubleSellerName.Name = "txtDoubleSellerName";
            this.txtDoubleSellerName.Size = new System.Drawing.Size(264, 40);
            this.txtDoubleSellerName.TabIndex = 1;
            this.txtDoubleSellerName.Enter += new System.EventHandler(this.txtDoubleSellerName_Enter);
            this.txtDoubleSellerName.Leave += new System.EventHandler(this.txtDoubleSellerName_Leave);
            // 
            // txtDoubleZip
            // 
            this.txtDoubleZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoubleZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoubleZip.Location = new System.Drawing.Point(304, 130);
            this.txtDoubleZip.MaxLength = 15;
            this.txtDoubleZip.Name = "txtDoubleZip";
            this.txtDoubleZip.Size = new System.Drawing.Size(70, 40);
            this.txtDoubleZip.TabIndex = 7;
            this.txtDoubleZip.Enter += new System.EventHandler(this.txtDoubleZip_Enter);
            this.txtDoubleZip.Leave += new System.EventHandler(this.txtDoubleZip_Leave);
            // 
            // txtDoubleState
            // 
            this.txtDoubleState.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoubleState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoubleState.Location = new System.Drawing.Point(244, 130);
            this.txtDoubleState.MaxLength = 2;
            this.txtDoubleState.Name = "txtDoubleState";
            this.txtDoubleState.Size = new System.Drawing.Size(50, 40);
            this.txtDoubleState.TabIndex = 6;
            this.txtDoubleState.Enter += new System.EventHandler(this.txtDoubleState_Enter);
            this.txtDoubleState.Leave += new System.EventHandler(this.txtDoubleState_Leave);
            // 
            // txtDoubleCity
            // 
            this.txtDoubleCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoubleCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoubleCity.Location = new System.Drawing.Point(110, 130);
            this.txtDoubleCity.MaxLength = 20;
            this.txtDoubleCity.Name = "txtDoubleCity";
            this.txtDoubleCity.Size = new System.Drawing.Size(124, 40);
            this.txtDoubleCity.TabIndex = 5;
            this.txtDoubleCity.Enter += new System.EventHandler(this.txtDoubleCity_Enter);
            this.txtDoubleCity.Leave += new System.EventHandler(this.txtDoubleCity_Leave);
            // 
            // txtDoubleSellerAddress
            // 
            this.txtDoubleSellerAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoubleSellerAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoubleSellerAddress.Location = new System.Drawing.Point(110, 80);
            this.txtDoubleSellerAddress.MaxLength = 20;
            this.txtDoubleSellerAddress.Name = "txtDoubleSellerAddress";
            this.txtDoubleSellerAddress.Size = new System.Drawing.Size(264, 40);
            this.txtDoubleSellerAddress.TabIndex = 3;
            this.txtDoubleSellerAddress.Enter += new System.EventHandler(this.txtDoubleSellerAddress_Enter);
            this.txtDoubleSellerAddress.Leave += new System.EventHandler(this.txtDoubleSellerAddress_Leave);
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtDoublePlate);
            this.Frame5.Controls.Add(this.cmbDoubleDealer);
            this.Frame5.Controls.Add(this.Label30);
            this.Frame5.Location = new System.Drawing.Point(20, 180);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(240, 140);
            this.Frame5.TabIndex = 8;
            this.Frame5.Text = "Dealer Type";
            // 
            // txtDoublePlate
            // 
            this.txtDoublePlate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoublePlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoublePlate.Location = new System.Drawing.Point(100, 80);
            this.txtDoublePlate.MaxLength = 20;
            this.txtDoublePlate.Name = "txtDoublePlate";
            this.txtDoublePlate.Size = new System.Drawing.Size(120, 40);
            this.txtDoublePlate.TabIndex = 2;
            this.txtDoublePlate.Enter += new System.EventHandler(this.txtDoublePlate_Enter);
            this.txtDoublePlate.Leave += new System.EventHandler(this.txtDoublePlate_Leave);
            // 
            // Label30
            // 
            this.Label30.Location = new System.Drawing.Point(20, 94);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(60, 16);
            this.Label30.TabIndex = 1;
            this.Label30.Text = "PLATE #";
            // 
            // Label31
            // 
            this.Label31.Location = new System.Drawing.Point(20, 44);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(46, 14);
            this.Label31.TabIndex = 9;
            this.Label31.Text = "NAME ";
            // 
            // Label32
            // 
            this.Label32.Location = new System.Drawing.Point(20, 138);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(40, 15);
            this.Label32.TabIndex = 4;
            this.Label32.Text = "C/S/Z";
            // 
            // Label33
            // 
            this.Label33.Location = new System.Drawing.Point(20, 92);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(60, 15);
            this.Label33.TabIndex = 2;
            this.Label33.Text = "ADDRESS";
            // 
            // chkRushTitle
            // 
            this.chkRushTitle.Location = new System.Drawing.Point(30, 615);
            this.chkRushTitle.Name = "chkRushTitle";
            this.chkRushTitle.Size = new System.Drawing.Size(91, 22);
            this.chkRushTitle.TabIndex = 8;
            this.chkRushTitle.Text = "Rush Title";
            // 
            // chkFleet
            // 
            this.chkFleet.Location = new System.Drawing.Point(30, 578);
            this.chkFleet.Name = "chkFleet";
            this.chkFleet.Size = new System.Drawing.Size(160, 22);
            this.chkFleet.TabIndex = 7;
            this.chkFleet.Text = "For Multiple Vehicles?";
            this.chkFleet.CheckedChanged += new System.EventHandler(this.chkFleet_CheckedChanged);
            // 
            // txtCTANumber
            // 
            this.txtCTANumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTANumber.Location = new System.Drawing.Point(94, 417);
            this.txtCTANumber.MaxLength = 9;
            this.txtCTANumber.Name = "txtCTANumber";
            this.txtCTANumber.Size = new System.Drawing.Size(150, 40);
            this.txtCTANumber.TabIndex = 3;
            this.txtCTANumber.Enter += new System.EventHandler(this.txtCTANumber_Enter);
            this.txtCTANumber.Leave += new System.EventHandler(this.txtCTANumber_Leave);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(510, 1518);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(168, 40);
            this.cmdReturn.TabIndex = 13;
            this.cmdReturn.Text = "Return To Input";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // chkCTASupp
            // 
            this.chkCTASupp.Location = new System.Drawing.Point(30, 467);
            this.chkCTASupp.Name = "chkCTASupp";
            this.chkCTASupp.Size = new System.Drawing.Size(350, 22);
            this.chkCTASupp.TabIndex = 4;
            this.chkCTASupp.TabStop = false;
            this.chkCTASupp.Text = "Check here if Supplemental Application (Fee Required)";
            // 
            // fraApplicant
            // 
            this.fraApplicant.Controls.Add(this.chkGetOwnerInfo);
            this.fraApplicant.Controls.Add(this.txtUTCSellerAddress);
            this.fraApplicant.Controls.Add(this.txtUTCSellerCity);
            this.fraApplicant.Controls.Add(this.txtUTCSellerState);
            this.fraApplicant.Controls.Add(this.txtUTCSellerZip);
            this.fraApplicant.Controls.Add(this.txtUTCSellerName1);
            this.fraApplicant.Controls.Add(this.txtUTCSellerName2);
            this.fraApplicant.Controls.Add(this.chkCTAJoint);
            this.fraApplicant.Controls.Add(this.txtCTAResAddress);
            this.fraApplicant.Controls.Add(this.txtCTALegalZip);
            this.fraApplicant.Controls.Add(this.txtCTAApplicantState);
            this.fraApplicant.Controls.Add(this.txtCTAResTown);
            this.fraApplicant.Controls.Add(this.txtCTADOB2);
            this.fraApplicant.Controls.Add(this.txtCTATelNumber);
            this.fraApplicant.Controls.Add(this.txtCTADOB1);
            this.fraApplicant.Controls.Add(this.Label9);
            this.fraApplicant.Controls.Add(this.Label8);
            this.fraApplicant.Controls.Add(this.Label16);
            this.fraApplicant.Controls.Add(this.Label15);
            this.fraApplicant.Controls.Add(this.Label14);
            this.fraApplicant.Controls.Add(this.Label3);
            this.fraApplicant.Controls.Add(this.Label4);
            this.fraApplicant.Controls.Add(this.Label5);
            this.fraApplicant.Controls.Add(this.Label6);
            this.fraApplicant.Controls.Add(this.Label7);
            this.fraApplicant.FormatCaption = false;
            this.fraApplicant.Location = new System.Drawing.Point(30, 30);
            this.fraApplicant.Name = "fraApplicant";
            this.fraApplicant.Size = new System.Drawing.Size(667, 377);
            this.fraApplicant.TabIndex = 1;
            this.fraApplicant.Text = "Applicant(s)";
            // 
            // chkGetOwnerInfo
            // 
            this.chkGetOwnerInfo.Location = new System.Drawing.Point(20, 30);
            this.chkGetOwnerInfo.Name = "chkGetOwnerInfo";
            this.chkGetOwnerInfo.Size = new System.Drawing.Size(202, 22);
            this.chkGetOwnerInfo.TabIndex = 0;
            this.chkGetOwnerInfo.Text = "Get Owner From Registration";
            this.chkGetOwnerInfo.CheckedChanged += new System.EventHandler(this.chkGetOwnerInfo_CheckedChanged);
            // 
            // txtUTCSellerAddress
            // 
            this.txtUTCSellerAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerAddress.Location = new System.Drawing.Point(108, 167);
            this.txtUTCSellerAddress.MaxLength = 35;
            this.txtUTCSellerAddress.Name = "txtUTCSellerAddress";
            this.txtUTCSellerAddress.Size = new System.Drawing.Size(264, 40);
            this.txtUTCSellerAddress.TabIndex = 10;
            this.txtUTCSellerAddress.Enter += new System.EventHandler(this.txtUTCSellerAddress_Enter);
            this.txtUTCSellerAddress.Leave += new System.EventHandler(this.txtUTCSellerAddress_Leave);
            // 
            // txtUTCSellerCity
            // 
            this.txtUTCSellerCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerCity.Location = new System.Drawing.Point(108, 217);
            this.txtUTCSellerCity.MaxLength = 20;
            this.txtUTCSellerCity.Name = "txtUTCSellerCity";
            this.txtUTCSellerCity.Size = new System.Drawing.Size(124, 40);
            this.txtUTCSellerCity.TabIndex = 13;
            this.txtUTCSellerCity.Enter += new System.EventHandler(this.txtUTCSellerCity_Enter);
            this.txtUTCSellerCity.Leave += new System.EventHandler(this.txtUTCSellerCity_Leave);
            // 
            // txtUTCSellerState
            // 
            this.txtUTCSellerState.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerState.Location = new System.Drawing.Point(242, 217);
            this.txtUTCSellerState.MaxLength = 2;
            this.txtUTCSellerState.Name = "txtUTCSellerState";
            this.txtUTCSellerState.Size = new System.Drawing.Size(50, 40);
            this.txtUTCSellerState.TabIndex = 14;
            this.txtUTCSellerState.Enter += new System.EventHandler(this.txtUTCSellerState_Enter);
            this.txtUTCSellerState.Leave += new System.EventHandler(this.txtUTCSellerState_Leave);
            // 
            // txtUTCSellerZip
            // 
            this.txtUTCSellerZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerZip.Location = new System.Drawing.Point(302, 217);
            this.txtUTCSellerZip.MaxLength = 15;
            this.txtUTCSellerZip.Name = "txtUTCSellerZip";
            this.txtUTCSellerZip.Size = new System.Drawing.Size(70, 40);
            this.txtUTCSellerZip.TabIndex = 15;
            this.txtUTCSellerZip.Enter += new System.EventHandler(this.txtUTCSellerZip_Enter);
            this.txtUTCSellerZip.Leave += new System.EventHandler(this.txtUTCSellerZip_Leave);
            // 
            // txtUTCSellerName1
            // 
            this.txtUTCSellerName1.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerName1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerName1.Location = new System.Drawing.Point(108, 67);
            this.txtUTCSellerName1.MaxLength = 30;
            this.txtUTCSellerName1.Name = "txtUTCSellerName1";
            this.txtUTCSellerName1.Size = new System.Drawing.Size(264, 40);
            this.txtUTCSellerName1.TabIndex = 2;
            this.txtUTCSellerName1.Enter += new System.EventHandler(this.txtUTCSellerName1_Enter);
            this.txtUTCSellerName1.Leave += new System.EventHandler(this.txtUTCSellerName1_Leave);
            // 
            // txtUTCSellerName2
            // 
            this.txtUTCSellerName2.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerName2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerName2.Location = new System.Drawing.Point(108, 117);
            this.txtUTCSellerName2.MaxLength = 30;
            this.txtUTCSellerName2.Name = "txtUTCSellerName2";
            this.txtUTCSellerName2.Size = new System.Drawing.Size(264, 40);
            this.txtUTCSellerName2.TabIndex = 6;
            this.txtUTCSellerName2.Enter += new System.EventHandler(this.txtUTCSellerName2_Enter);
            this.txtUTCSellerName2.Leave += new System.EventHandler(this.txtUTCSellerName2_Leave);
            // 
            // chkCTAJoint
            // 
            this.chkCTAJoint.Location = new System.Drawing.Point(393, 173);
            this.chkCTAJoint.Name = "chkCTAJoint";
            this.chkCTAJoint.Size = new System.Drawing.Size(108, 22);
            this.chkCTAJoint.TabIndex = 11;
            this.chkCTAJoint.Text = "Joint Owners";
            // 
            // txtCTAResAddress
            // 
            this.txtCTAResAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTAResAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTAResAddress.Location = new System.Drawing.Point(236, 267);
            this.txtCTAResAddress.MaxLength = 20;
            this.txtCTAResAddress.Name = "txtCTAResAddress";
            this.txtCTAResAddress.Size = new System.Drawing.Size(274, 40);
            this.txtCTAResAddress.TabIndex = 20;
            this.txtCTAResAddress.Enter += new System.EventHandler(this.txtCTAResAddress_Enter);
            this.txtCTAResAddress.Leave += new System.EventHandler(this.txtCTAResAddress_Leave);
            // 
            // txtCTALegalZip
            // 
            this.txtCTALegalZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALegalZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALegalZip.Location = new System.Drawing.Point(430, 317);
            this.txtCTALegalZip.MaxLength = 15;
            this.txtCTALegalZip.Name = "txtCTALegalZip";
            this.txtCTALegalZip.Size = new System.Drawing.Size(80, 40);
            this.txtCTALegalZip.TabIndex = 24;
            this.txtCTALegalZip.Enter += new System.EventHandler(this.txtCTALegalZip_Enter);
            this.txtCTALegalZip.Leave += new System.EventHandler(this.txtCTALegalZip_Leave);
            // 
            // txtCTAApplicantState
            // 
            this.txtCTAApplicantState.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTAApplicantState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTAApplicantState.Location = new System.Drawing.Point(370, 317);
            this.txtCTAApplicantState.MaxLength = 2;
            this.txtCTAApplicantState.Name = "txtCTAApplicantState";
            this.txtCTAApplicantState.Size = new System.Drawing.Size(50, 40);
            this.txtCTAApplicantState.TabIndex = 23;
            this.txtCTAApplicantState.Enter += new System.EventHandler(this.txtCTAApplicantState_Enter);
            this.txtCTAApplicantState.Leave += new System.EventHandler(this.txtCTAApplicantState_Leave);
            // 
            // txtCTAResTown
            // 
            this.txtCTAResTown.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTAResTown.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTAResTown.Location = new System.Drawing.Point(236, 317);
            this.txtCTAResTown.MaxLength = 20;
            this.txtCTAResTown.Name = "txtCTAResTown";
            this.txtCTAResTown.Size = new System.Drawing.Size(124, 40);
            this.txtCTAResTown.TabIndex = 22;
            this.txtCTAResTown.Enter += new System.EventHandler(this.txtCTAResTown_Enter);
            this.txtCTAResTown.Leave += new System.EventHandler(this.txtCTAResTown_Leave);
            // 
            // txtCTADOB2
            // 
            this.txtCTADOB2.Location = new System.Drawing.Point(534, 117);
            this.txtCTADOB2.Mask = "##/##/####";
            this.txtCTADOB2.MaxLength = 10;
            this.txtCTADOB2.Name = "txtCTADOB2";
            this.txtCTADOB2.Size = new System.Drawing.Size(115, 22);
            this.txtCTADOB2.TabIndex = 8;
            this.txtCTADOB2.Enter += new System.EventHandler(this.txtCTADOB2_Enter);
            this.txtCTADOB2.Leave += new System.EventHandler(this.txtCTADOB2_Leave);
            // 
            // txtCTATelNumber
            // 
            this.txtCTATelNumber.Location = new System.Drawing.Point(503, 217);
            this.txtCTATelNumber.MaxLength = 13;
            this.txtCTATelNumber.Name = "txtCTATelNumber";
            this.txtCTATelNumber.Size = new System.Drawing.Size(144, 22);
            this.txtCTATelNumber.TabIndex = 17;
            this.txtCTATelNumber.Enter += new System.EventHandler(this.txtCTATelNumber_Enter);
            this.txtCTATelNumber.Leave += new System.EventHandler(this.txtCTATelNumber_Leave);
            // 
            // txtCTADOB1
            // 
            this.txtCTADOB1.Location = new System.Drawing.Point(534, 67);
            this.txtCTADOB1.Mask = "##/##/####";
            this.txtCTADOB1.MaxLength = 10;
            this.txtCTADOB1.Name = "txtCTADOB1";
            this.txtCTADOB1.Size = new System.Drawing.Size(115, 22);
            this.txtCTADOB1.TabIndex = 4;
            this.txtCTADOB1.Enter += new System.EventHandler(this.txtCTADOB1_Enter);
            this.txtCTADOB1.Leave += new System.EventHandler(this.txtCTADOB1_Leave);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(150, 281);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(55, 15);
            this.Label9.TabIndex = 19;
            this.Label9.Text = "ADDRESS";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(150, 331);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(35, 15);
            this.Label8.TabIndex = 21;
            this.Label8.Text = "C/S/Z";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(20, 229);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(35, 15);
            this.Label16.TabIndex = 12;
            this.Label16.Text = "C/S/Z";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(20, 181);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(60, 17);
            this.Label15.TabIndex = 9;
            this.Label15.Text = "ADDRESS";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 81);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(60, 17);
            this.Label14.TabIndex = 1;
            this.Label14.Text = "NAME #1";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 131);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 17);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "NAME #2";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(393, 234);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(80, 14);
            this.Label4.TabIndex = 16;
            this.Label4.Text = "TEL. NUMBER ";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 267);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(110, 41);
            this.Label5.TabIndex = 18;
            this.Label5.Text = "LEGAL RESIDENCE   (IF OTHER THAN ADDRESS ABOVE)";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(393, 81);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(120, 15);
            this.Label6.TabIndex = 3;
            this.Label6.Text = "DOB FOR NAME #1";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(393, 131);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(120, 15);
            this.Label7.TabIndex = 7;
            this.Label7.Text = "DOB FOR NAME #2";
            // 
            // fraVehicleInfo
            // 
            this.fraVehicleInfo.Controls.Add(this.fcLabel1);
            this.fraVehicleInfo.Controls.Add(this.cmbVehicleType);
            this.fraVehicleInfo.Controls.Add(this.lblVehicleType);
            this.fraVehicleInfo.Controls.Add(this.cmbMSRP);
            this.fraVehicleInfo.Controls.Add(this.lblMSRP);
            this.fraVehicleInfo.Controls.Add(this.cmbOdometer);
            this.fraVehicleInfo.Controls.Add(this.cmbOdometerCondition);
            this.fraVehicleInfo.Controls.Add(this.lblOdometerCondition);
            this.fraVehicleInfo.Controls.Add(this.txtCTAStateOfOrigin);
            this.fraVehicleInfo.Controls.Add(this.txtCTANumberOfCylinders);
            this.fraVehicleInfo.Controls.Add(this.txtCATDatePurchased);
            this.fraVehicleInfo.Controls.Add(this.txtCTAPreviousNumber);
            this.fraVehicleInfo.Controls.Add(this.Label10);
            this.fraVehicleInfo.Controls.Add(this.Label11);
            this.fraVehicleInfo.Controls.Add(this.Label12);
            this.fraVehicleInfo.Controls.Add(this.Label13);
            this.fraVehicleInfo.Location = new System.Drawing.Point(30, 652);
            this.fraVehicleInfo.Name = "fraVehicleInfo";
            this.fraVehicleInfo.Size = new System.Drawing.Size(359, 440);
            this.fraVehicleInfo.TabIndex = 9;
            this.fraVehicleInfo.Text = "Vehicle Information";
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(20, 94);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(79, 15);
            this.fcLabel1.TabIndex = 2;
            this.fcLabel1.Text = "ODOMETER";
            // 
            // txtCTAStateOfOrigin
            // 
            this.txtCTAStateOfOrigin.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTAStateOfOrigin.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTAStateOfOrigin.Location = new System.Drawing.Point(189, 380);
            this.txtCTAStateOfOrigin.MaxLength = 2;
            this.txtCTAStateOfOrigin.Name = "txtCTAStateOfOrigin";
            this.txtCTAStateOfOrigin.Size = new System.Drawing.Size(150, 40);
            this.txtCTAStateOfOrigin.TabIndex = 15;
            this.txtCTAStateOfOrigin.Enter += new System.EventHandler(this.txtCTAStateOfOrigin_Enter);
            this.txtCTAStateOfOrigin.Leave += new System.EventHandler(this.txtCTAStateOfOrigin_Leave);
            // 
            // txtCTANumberOfCylinders
            // 
            this.txtCTANumberOfCylinders.Location = new System.Drawing.Point(189, 230);
            this.txtCTANumberOfCylinders.MaxLength = 2;
            this.txtCTANumberOfCylinders.Name = "txtCTANumberOfCylinders";
            this.txtCTANumberOfCylinders.Size = new System.Drawing.Size(150, 22);
            this.txtCTANumberOfCylinders.TabIndex = 9;
            this.txtCTANumberOfCylinders.Enter += new System.EventHandler(this.txtCTANumberOfCylinders_Enter);
            this.txtCTANumberOfCylinders.Leave += new System.EventHandler(this.txtCTANumberOfCylinders_Leave);
            // 
            // txtCATDatePurchased
            // 
            this.txtCATDatePurchased.Location = new System.Drawing.Point(189, 280);
            this.txtCATDatePurchased.Mask = "##/##/####";
            this.txtCATDatePurchased.MaxLength = 10;
            this.txtCATDatePurchased.Name = "txtCATDatePurchased";
            this.txtCATDatePurchased.Size = new System.Drawing.Size(115, 22);
            this.txtCATDatePurchased.TabIndex = 11;
            this.txtCATDatePurchased.Enter += new System.EventHandler(this.txtCATDatePurchased_Enter);
            this.txtCATDatePurchased.Leave += new System.EventHandler(this.txtCATDatePurchased_Leave);
            // 
            // txtCTAPreviousNumber
            // 
            this.txtCTAPreviousNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTAPreviousNumber.Location = new System.Drawing.Point(189, 330);
            this.txtCTAPreviousNumber.MaxLength = 20;
            this.txtCTAPreviousNumber.Name = "txtCTAPreviousNumber";
            this.txtCTAPreviousNumber.Size = new System.Drawing.Size(150, 40);
            this.txtCTAPreviousNumber.TabIndex = 13;
            this.txtCTAPreviousNumber.Enter += new System.EventHandler(this.txtCTAPreviousNumber_Enter);
            this.txtCTAPreviousNumber.Leave += new System.EventHandler(this.txtCTAPreviousNumber_Leave);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 244);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(140, 15);
            this.Label10.TabIndex = 8;
            this.Label10.Text = "NUMBER OF CYLINDERS";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 294);
            this.Label11.Name = "Label11";
            this.Label11.TabIndex = 10;
            this.Label11.Text = "PURCHASE DATE";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 344);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(110, 15);
            this.Label12.TabIndex = 12;
            this.Label12.Text = "PREVIOUS TITLE #";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 394);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(140, 15);
            this.Label13.TabIndex = 14;
            this.Label13.Text = "STATE OF ORIGIN";
            // 
            // fraLienHolder
            // 
            this.fraLienHolder.Controls.Add(this.txtCTALienOneName);
            this.fraLienHolder.Controls.Add(this.txtCTALienOneZip);
            this.fraLienHolder.Controls.Add(this.txtCTALienOneState);
            this.fraLienHolder.Controls.Add(this.txtCTALienOneCity);
            this.fraLienHolder.Controls.Add(this.txtCTALienOneAddress);
            this.fraLienHolder.Controls.Add(this.txtCTALienTwoName);
            this.fraLienHolder.Controls.Add(this.txtCTALienTwoZip);
            this.fraLienHolder.Controls.Add(this.txtCTALienTwoState);
            this.fraLienHolder.Controls.Add(this.txtCTALienTwoCity);
            this.fraLienHolder.Controls.Add(this.txtCTALienTwoAddress);
            this.fraLienHolder.Controls.Add(this.txt1stLienDate);
            this.fraLienHolder.Controls.Add(this.txt2ndLienDate);
            this.fraLienHolder.Controls.Add(this.Label29);
            this.fraLienHolder.Controls.Add(this.Label1);
            this.fraLienHolder.Controls.Add(this.Label17);
            this.fraLienHolder.Controls.Add(this.Label18);
            this.fraLienHolder.Controls.Add(this.Label19);
            this.fraLienHolder.Controls.Add(this.Label20);
            this.fraLienHolder.Controls.Add(this.Label21);
            this.fraLienHolder.Controls.Add(this.Label22);
            this.fraLienHolder.Controls.Add(this.Label23);
            this.fraLienHolder.Controls.Add(this.Label24);
            this.fraLienHolder.FormatCaption = false;
            this.fraLienHolder.Location = new System.Drawing.Point(30, 1113);
            this.fraLienHolder.Name = "fraLienHolder";
            this.fraLienHolder.Size = new System.Drawing.Size(648, 395);
            this.fraLienHolder.TabIndex = 11;
            this.fraLienHolder.Text = "Lienholder(s)";
            // 
            // txtCTALienOneName
            // 
            this.txtCTALienOneName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienOneName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienOneName.Location = new System.Drawing.Point(107, 57);
            this.txtCTALienOneName.MaxLength = 30;
            this.txtCTALienOneName.Name = "txtCTALienOneName";
            this.txtCTALienOneName.Size = new System.Drawing.Size(268, 40);
            this.txtCTALienOneName.TabIndex = 2;
            this.txtCTALienOneName.Text = "NONE";
            this.txtCTALienOneName.Enter += new System.EventHandler(this.txtCTALienOneName_Enter);
            this.txtCTALienOneName.Leave += new System.EventHandler(this.txtCTALienOneName_Leave);
            // 
            // txtCTALienOneZip
            // 
            this.txtCTALienOneZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienOneZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienOneZip.Location = new System.Drawing.Point(301, 157);
            this.txtCTALienOneZip.MaxLength = 15;
            this.txtCTALienOneZip.Name = "txtCTALienOneZip";
            this.txtCTALienOneZip.Size = new System.Drawing.Size(74, 40);
            this.txtCTALienOneZip.TabIndex = 10;
            this.txtCTALienOneZip.Enter += new System.EventHandler(this.txtCTALienOneZip_Enter);
            this.txtCTALienOneZip.Leave += new System.EventHandler(this.txtCTALienOneZip_Leave);
            // 
            // txtCTALienOneState
            // 
            this.txtCTALienOneState.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienOneState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienOneState.Location = new System.Drawing.Point(241, 157);
            this.txtCTALienOneState.MaxLength = 2;
            this.txtCTALienOneState.Name = "txtCTALienOneState";
            this.txtCTALienOneState.Size = new System.Drawing.Size(50, 40);
            this.txtCTALienOneState.TabIndex = 9;
            this.txtCTALienOneState.Enter += new System.EventHandler(this.txtCTALienOneState_Enter);
            this.txtCTALienOneState.Leave += new System.EventHandler(this.txtCTALienOneState_Leave);
            // 
            // txtCTALienOneCity
            // 
            this.txtCTALienOneCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienOneCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienOneCity.Location = new System.Drawing.Point(107, 157);
            this.txtCTALienOneCity.MaxLength = 20;
            this.txtCTALienOneCity.Name = "txtCTALienOneCity";
            this.txtCTALienOneCity.Size = new System.Drawing.Size(124, 40);
            this.txtCTALienOneCity.TabIndex = 8;
            this.txtCTALienOneCity.Enter += new System.EventHandler(this.txtCTALienOneCity_Enter);
            this.txtCTALienOneCity.Leave += new System.EventHandler(this.txtCTALienOneCity_Leave);
            // 
            // txtCTALienOneAddress
            // 
            this.txtCTALienOneAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienOneAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienOneAddress.Location = new System.Drawing.Point(107, 107);
            this.txtCTALienOneAddress.MaxLength = 25;
            this.txtCTALienOneAddress.Name = "txtCTALienOneAddress";
            this.txtCTALienOneAddress.Size = new System.Drawing.Size(268, 40);
            this.txtCTALienOneAddress.TabIndex = 6;
            this.txtCTALienOneAddress.Enter += new System.EventHandler(this.txtCTALienOneAddress_Enter);
            this.txtCTALienOneAddress.Leave += new System.EventHandler(this.txtCTALienOneAddress_Leave);
            // 
            // txtCTALienTwoName
            // 
            this.txtCTALienTwoName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienTwoName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienTwoName.Location = new System.Drawing.Point(107, 239);
            this.txtCTALienTwoName.MaxLength = 30;
            this.txtCTALienTwoName.Name = "txtCTALienTwoName";
            this.txtCTALienTwoName.Size = new System.Drawing.Size(268, 40);
            this.txtCTALienTwoName.TabIndex = 13;
            this.txtCTALienTwoName.Enter += new System.EventHandler(this.txtCTALienTwoName_Enter);
            this.txtCTALienTwoName.Leave += new System.EventHandler(this.txtCTALienTwoName_Leave);
            // 
            // txtCTALienTwoZip
            // 
            this.txtCTALienTwoZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienTwoZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienTwoZip.Location = new System.Drawing.Point(301, 339);
            this.txtCTALienTwoZip.MaxLength = 15;
            this.txtCTALienTwoZip.Name = "txtCTALienTwoZip";
            this.txtCTALienTwoZip.Size = new System.Drawing.Size(74, 40);
            this.txtCTALienTwoZip.TabIndex = 21;
            this.txtCTALienTwoZip.Enter += new System.EventHandler(this.txtCTALienTwoZip_Enter);
            this.txtCTALienTwoZip.Leave += new System.EventHandler(this.txtCTALienTwoZip_Leave);
            // 
            // txtCTALienTwoState
            // 
            this.txtCTALienTwoState.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienTwoState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienTwoState.Location = new System.Drawing.Point(241, 339);
            this.txtCTALienTwoState.MaxLength = 2;
            this.txtCTALienTwoState.Name = "txtCTALienTwoState";
            this.txtCTALienTwoState.Size = new System.Drawing.Size(50, 40);
            this.txtCTALienTwoState.TabIndex = 20;
            this.txtCTALienTwoState.Enter += new System.EventHandler(this.txtCTALienTwoState_Enter);
            this.txtCTALienTwoState.Leave += new System.EventHandler(this.txtCTALienTwoState_Leave);
            // 
            // txtCTALienTwoCity
            // 
            this.txtCTALienTwoCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienTwoCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienTwoCity.Location = new System.Drawing.Point(107, 339);
            this.txtCTALienTwoCity.MaxLength = 20;
            this.txtCTALienTwoCity.Name = "txtCTALienTwoCity";
            this.txtCTALienTwoCity.Size = new System.Drawing.Size(124, 40);
            this.txtCTALienTwoCity.TabIndex = 19;
            this.txtCTALienTwoCity.Enter += new System.EventHandler(this.txtCTALienTwoCity_Enter);
            this.txtCTALienTwoCity.Leave += new System.EventHandler(this.txtCTALienTwoCity_Leave);
            // 
            // txtCTALienTwoAddress
            // 
            this.txtCTALienTwoAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTALienTwoAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTALienTwoAddress.Location = new System.Drawing.Point(107, 289);
            this.txtCTALienTwoAddress.MaxLength = 25;
            this.txtCTALienTwoAddress.Name = "txtCTALienTwoAddress";
            this.txtCTALienTwoAddress.Size = new System.Drawing.Size(268, 40);
            this.txtCTALienTwoAddress.TabIndex = 17;
            this.txtCTALienTwoAddress.Enter += new System.EventHandler(this.txtCTALienTwoAddress_Enter);
            this.txtCTALienTwoAddress.Leave += new System.EventHandler(this.txtCTALienTwoAddress_Leave);
            // 
            // txt1stLienDate
            // 
            this.txt1stLienDate.Location = new System.Drawing.Point(515, 57);
            this.txt1stLienDate.Mask = "##/##/####";
            this.txt1stLienDate.MaxLength = 10;
            this.txt1stLienDate.Name = "txt1stLienDate";
            this.txt1stLienDate.Size = new System.Drawing.Size(115, 22);
            this.txt1stLienDate.TabIndex = 4;
            this.txt1stLienDate.Enter += new System.EventHandler(this.txt1stLienDate_Enter);
            this.txt1stLienDate.Leave += new System.EventHandler(this.txt1stLienDate_Leave);
            // 
            // txt2ndLienDate
            // 
            this.txt2ndLienDate.Location = new System.Drawing.Point(515, 239);
            this.txt2ndLienDate.Mask = "##/##/####";
            this.txt2ndLienDate.MaxLength = 10;
            this.txt2ndLienDate.Name = "txt2ndLienDate";
            this.txt2ndLienDate.Size = new System.Drawing.Size(115, 22);
            this.txt2ndLienDate.TabIndex = 15;
            this.txt2ndLienDate.Enter += new System.EventHandler(this.txt2ndLienDate_Enter);
            this.txt2ndLienDate.Leave += new System.EventHandler(this.txt2ndLienDate_Leave);
            // 
            // Label29
            // 
            this.Label29.Location = new System.Drawing.Point(395, 253);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(90, 17);
            this.Label29.TabIndex = 14;
            this.Label29.Text = "2ND LIEN DATE";
            this.Label29.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(395, 71);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(90, 17);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "1ST LIEN DATE";
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(20, 71);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(46, 14);
            this.Label17.TabIndex = 1;
            this.Label17.Text = "NAME";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(20, 121);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(70, 17);
            this.Label18.TabIndex = 5;
            this.Label18.Text = "ADDRESS";
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(20, 171);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(35, 14);
            this.Label19.TabIndex = 7;
            this.Label19.Text = "C/S/Z";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(20, 253);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(46, 14);
            this.Label20.TabIndex = 12;
            this.Label20.Text = "NAME ";
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(20, 303);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(78, 15);
            this.Label21.TabIndex = 16;
            this.Label21.Text = "ADDRESS";
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(20, 353);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(35, 14);
            this.Label22.TabIndex = 18;
            this.Label22.Text = "C/S/Z";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(20, 30);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(120, 17);
            this.Label23.TabIndex = 22;
            this.Label23.Text = "LIENHOLDER ONE";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(20, 210);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(120, 17);
            this.Label24.TabIndex = 11;
            this.Label24.Text = "LIENHOLDER TWO";
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxLeftBorder";
            this.Frame2.Controls.Add(this.txtCTASellerName);
            this.Frame2.Controls.Add(this.txtCTASellerZip);
            this.Frame2.Controls.Add(this.txtCTASellerState);
            this.Frame2.Controls.Add(this.txtCTASellerCity);
            this.Frame2.Controls.Add(this.txtCTASellerAddress);
            this.Frame2.Controls.Add(this.Frame3);
            this.Frame2.Controls.Add(this.Label25);
            this.Frame2.Controls.Add(this.Label26);
            this.Frame2.Controls.Add(this.Label27);
            this.Frame2.Location = new System.Drawing.Point(409, 652);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(391, 337);
            this.Frame2.TabIndex = 10;
            this.Frame2.Text = "Seller Information";
            // 
            // txtCTASellerName
            // 
            this.txtCTASellerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTASellerName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTASellerName.Location = new System.Drawing.Point(115, 30);
            this.txtCTASellerName.MaxLength = 30;
            this.txtCTASellerName.Name = "txtCTASellerName";
            this.txtCTASellerName.Size = new System.Drawing.Size(256, 40);
            this.txtCTASellerName.TabIndex = 1;
            this.txtCTASellerName.Enter += new System.EventHandler(this.txtCTASellerName_Enter);
            this.txtCTASellerName.Leave += new System.EventHandler(this.txtCTASellerName_Leave);
            // 
            // txtCTASellerZip
            // 
            this.txtCTASellerZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTASellerZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTASellerZip.Location = new System.Drawing.Point(309, 130);
            this.txtCTASellerZip.MaxLength = 15;
            this.txtCTASellerZip.Name = "txtCTASellerZip";
            this.txtCTASellerZip.Size = new System.Drawing.Size(62, 40);
            this.txtCTASellerZip.TabIndex = 7;
            this.txtCTASellerZip.Enter += new System.EventHandler(this.txtCTASellerZip_Enter);
            this.txtCTASellerZip.Leave += new System.EventHandler(this.txtCTASellerZip_Leave);
            // 
            // txtCTASellerState
            // 
            this.txtCTASellerState.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTASellerState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTASellerState.Location = new System.Drawing.Point(249, 130);
            this.txtCTASellerState.MaxLength = 2;
            this.txtCTASellerState.Name = "txtCTASellerState";
            this.txtCTASellerState.Size = new System.Drawing.Size(50, 40);
            this.txtCTASellerState.TabIndex = 6;
            this.txtCTASellerState.Enter += new System.EventHandler(this.txtCTASellerState_Enter);
            this.txtCTASellerState.Leave += new System.EventHandler(this.txtCTASellerState_Leave);
            this.txtCTASellerState.KeyUp += new Wisej.Web.KeyEventHandler(this.txtCTASellerState_KeyUp);
            // 
            // txtCTASellerCity
            // 
            this.txtCTASellerCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTASellerCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTASellerCity.Location = new System.Drawing.Point(115, 130);
            this.txtCTASellerCity.MaxLength = 20;
            this.txtCTASellerCity.Name = "txtCTASellerCity";
            this.txtCTASellerCity.Size = new System.Drawing.Size(124, 40);
            this.txtCTASellerCity.TabIndex = 5;
            this.txtCTASellerCity.Enter += new System.EventHandler(this.txtCTASellerCity_Enter);
            this.txtCTASellerCity.Leave += new System.EventHandler(this.txtCTASellerCity_Leave);
            // 
            // txtCTASellerAddress
            // 
            this.txtCTASellerAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTASellerAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTASellerAddress.Location = new System.Drawing.Point(115, 80);
            this.txtCTASellerAddress.MaxLength = 20;
            this.txtCTASellerAddress.Name = "txtCTASellerAddress";
            this.txtCTASellerAddress.Size = new System.Drawing.Size(256, 40);
            this.txtCTASellerAddress.TabIndex = 3;
            this.txtCTASellerAddress.Enter += new System.EventHandler(this.txtCTASellerAddress_Enter);
            this.txtCTASellerAddress.Leave += new System.EventHandler(this.txtCTASellerAddress_Leave);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtCTADealerPlate);
            this.Frame3.Controls.Add(this.cmbDealer);
            this.Frame3.Controls.Add(this.Label28);
            this.Frame3.Location = new System.Drawing.Point(20, 184);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(251, 140);
            this.Frame3.TabIndex = 8;
            this.Frame3.Text = "Dealer Type";
            // 
            // txtCTADealerPlate
            // 
            this.txtCTADealerPlate.BackColor = System.Drawing.SystemColors.Window;
            this.txtCTADealerPlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTADealerPlate.Location = new System.Drawing.Point(100, 80);
            this.txtCTADealerPlate.MaxLength = 20;
            this.txtCTADealerPlate.Name = "txtCTADealerPlate";
            this.txtCTADealerPlate.Size = new System.Drawing.Size(131, 40);
            this.txtCTADealerPlate.TabIndex = 2;
            this.txtCTADealerPlate.Enter += new System.EventHandler(this.txtCTADealerPlate_Enter);
            this.txtCTADealerPlate.Leave += new System.EventHandler(this.txtCTADealerPlate_Leave);
            // 
            // Label28
            // 
            this.Label28.Location = new System.Drawing.Point(20, 94);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(50, 17);
            this.Label28.TabIndex = 1;
            this.Label28.Text = "PLATE #";
            // 
            // Label25
            // 
            this.Label25.Location = new System.Drawing.Point(20, 44);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(46, 14);
            this.Label25.TabIndex = 9;
            this.Label25.Text = "NAME ";
            // 
            // Label26
            // 
            this.Label26.Location = new System.Drawing.Point(20, 136);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(40, 15);
            this.Label26.TabIndex = 4;
            this.Label26.Text = "C/S/Z";
            // 
            // Label27
            // 
            this.Label27.Location = new System.Drawing.Point(20, 94);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(60, 17);
            this.Label27.TabIndex = 2;
            this.Label27.Text = "ADDRESS";
            // 
            // chkDouble
            // 
            this.chkDouble.Location = new System.Drawing.Point(30, 504);
            this.chkDouble.Name = "chkDouble";
            this.chkDouble.Size = new System.Drawing.Size(110, 22);
            this.chkDouble.TabIndex = 5;
            this.chkDouble.TabStop = false;
            this.chkDouble.Text = "Double CTA?";
            // 
            // chkNoFee
            // 
            this.chkNoFee.Location = new System.Drawing.Point(30, 541);
            this.chkNoFee.Name = "chkNoFee";
            this.chkNoFee.Size = new System.Drawing.Size(82, 22);
            this.chkNoFee.TabIndex = 6;
            this.chkNoFee.Text = "No Fee?";
            this.chkNoFee.CheckedChanged += new System.EventHandler(this.chkNoFee_CheckedChanged);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 431);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(40, 17);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "CTA#";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuFileSeparator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 0;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSave.Text = "Save & Exit";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSeparator
            // 
            this.mnuFileSeparator.Index = 1;
            this.mnuFileSeparator.Name = "mnuFileSeparator";
            this.mnuFileSeparator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdFileSave
            // 
            this.cmdFileSave.AppearanceKey = "acceptButton";
            this.cmdFileSave.Location = new System.Drawing.Point(343, 30);
            this.cmdFileSave.Name = "cmdFileSave";
            this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileSave.Size = new System.Drawing.Size(127, 48);
            this.cmdFileSave.TabIndex = 0;
            this.cmdFileSave.Text = "Save & Exit";
            this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmCTA
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(848, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCTA";
            this.Text = "Certificate of Title Application ";
            this.Load += new System.EventHandler(this.frmCTA_Load);
            this.Activated += new System.EventHandler(this.frmCTA_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCTA_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCTA_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleInfo)).EndInit();
            this.fraDoubleInfo.ResumeLayout(false);
            this.fraDoubleInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintDouble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleOwner)).EndInit();
            this.fraDoubleOwner.ResumeLayout(false);
            this.fraDoubleOwner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleVehicleInfo)).EndInit();
            this.fraDoubleVehicleInfo.ResumeLayout(false);
            this.fraDoubleVehicleInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCylinders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoublePurchaseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoublePreviousTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCTANumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDoubleSellerInfo)).EndInit();
            this.fraDoubleSellerInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkRushTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFleet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTANumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCTASupp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).EndInit();
            this.fraApplicant.ResumeLayout(false);
            this.fraApplicant.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkGetOwnerInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCTAJoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTADOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTATelNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTADOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraVehicleInfo)).EndInit();
            this.fraVehicleInfo.ResumeLayout(false);
            this.fraVehicleInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTANumberOfCylinders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCATDatePurchased)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTAPreviousNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraLienHolder)).EndInit();
            this.fraLienHolder.ResumeLayout(false);
            this.fraLienHolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt1stLienDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt2ndLienDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDouble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdFileSave;
		public FCLabel fcLabel1;
		public FCFrame fraApplicant;
	}
}
