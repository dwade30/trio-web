//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for menuRapidRenewal.
	/// </summary>
	partial class menuRapidRenewal
	{
		public fecherFoundation.FCComboBox cmbLoadData;
		public fecherFoundation.FCLabel lblLoadData;
		public fecherFoundation.FCFrame fraFileOptions;
		public fecherFoundation.FCButton cmdProcessAll;
		public fecherFoundation.FCButton cmdDownload;
		public fecherFoundation.FCListBox lstMessages;
		public fecherFoundation.FCProgressBar ProgressBar1;
		public fecherFoundation.FCGrid vsDetail;
		public fecherFoundation.FCGrid vsFinancial;
		public fecherFoundation.FCLabel lblPercent;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCComboBox cboEndMonth;
		public fecherFoundation.FCComboBox cboEndYear;
		public fecherFoundation.FCComboBox cboStartYear;
		public fecherFoundation.FCComboBox cboStartMonth;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame fraRapidRenewal;
		public fecherFoundation.FCCheckBox chkCreateJournal;
		public/*AxDevPowerFTP.AxFTP*/Chilkat.Ftp2 FTP1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbLoadData = new fecherFoundation.FCComboBox();
            this.lblLoadData = new fecherFoundation.FCLabel();
            this.fraFileOptions = new fecherFoundation.FCFrame();
            this.cmdProcessAll = new fecherFoundation.FCButton();
            this.cmdDownload = new fecherFoundation.FCButton();
            this.lstMessages = new fecherFoundation.FCListBox();
            this.vsDetail = new fecherFoundation.FCGrid();
            this.vsFinancial = new fecherFoundation.FCGrid();
            this.lblPercent = new fecherFoundation.FCLabel();
            this.ProgressBar1 = new fecherFoundation.FCProgressBar();
            this.fraRange = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.cboEndMonth = new fecherFoundation.FCComboBox();
            this.cboEndYear = new fecherFoundation.FCComboBox();
            this.cboStartYear = new fecherFoundation.FCComboBox();
            this.cboStartMonth = new fecherFoundation.FCComboBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.fraRapidRenewal = new fecherFoundation.FCFrame();
            this.chkCreateJournal = new fecherFoundation.FCCheckBox();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFileOptions)).BeginInit();
            this.fraFileOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDownload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFinancial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
            this.fraRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRapidRenewal)).BeginInit();
            this.fraRapidRenewal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateJournal)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 590);
            this.BottomPanel.Size = new System.Drawing.Size(638, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraRange);
            this.ClientArea.Controls.Add(this.fraFileOptions);
            this.ClientArea.Controls.Add(this.cmbLoadData);
            this.ClientArea.Controls.Add(this.lblLoadData);
            this.ClientArea.Controls.Add(this.fraRapidRenewal);
            this.ClientArea.Size = new System.Drawing.Size(658, 628);
            this.ClientArea.Controls.SetChildIndex(this.fraRapidRenewal, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblLoadData, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbLoadData, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraFileOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraRange, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(658, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(167, 28);
            this.HeaderText.Text = "Rapid Renewal";
            // 
            // cmbLoadData
            // 
            this.cmbLoadData.Items.AddRange(new object[] {
            "Create Rapid Renewal Data File",
            "Process Financial File",
            "Process Detail File"});
            this.cmbLoadData.Location = new System.Drawing.Point(151, 30);
            this.cmbLoadData.Name = "cmbLoadData";
            this.cmbLoadData.Size = new System.Drawing.Size(311, 40);
            this.cmbLoadData.TabIndex = 15;
            this.cmbLoadData.Text = "Create Rapid Renewal Data File";
            this.cmbLoadData.SelectedIndexChanged += new System.EventHandler(this.cmbLoadData_SelectedIndexChanged);
            // 
            // lblLoadData
            // 
            this.lblLoadData.AutoSize = true;
            this.lblLoadData.Location = new System.Drawing.Point(30, 44);
            this.lblLoadData.Name = "lblLoadData";
            this.lblLoadData.Size = new System.Drawing.Size(55, 15);
            this.lblLoadData.TabIndex = 16;
            this.lblLoadData.Text = "SELECT";
            // 
            // fraFileOptions
            // 
            this.fraFileOptions.Controls.Add(this.cmdProcessAll);
            this.fraFileOptions.Controls.Add(this.cmdDownload);
            this.fraFileOptions.Controls.Add(this.lstMessages);
            this.fraFileOptions.Controls.Add(this.vsDetail);
            this.fraFileOptions.Controls.Add(this.vsFinancial);
            this.fraFileOptions.Controls.Add(this.lblPercent);
            this.fraFileOptions.Location = new System.Drawing.Point(30, 90);
            this.fraFileOptions.Name = "fraFileOptions";
            this.fraFileOptions.Size = new System.Drawing.Size(497, 403);
            this.fraFileOptions.TabIndex = 14;
            this.fraFileOptions.Text = "File Options";
            this.fraFileOptions.Visible = false;
            // 
            // cmdProcessAll
            // 
            this.cmdProcessAll.AppearanceKey = "actionButton";
            this.cmdProcessAll.Location = new System.Drawing.Point(174, 343);
            this.cmdProcessAll.Name = "cmdProcessAll";
            this.cmdProcessAll.Size = new System.Drawing.Size(128, 40);
            this.cmdProcessAll.TabIndex = 21;
            this.cmdProcessAll.Text = "Process All";
            this.cmdProcessAll.Click += new System.EventHandler(this.cmdProcessAll_Click);
            // 
            // cmdDownload
            // 
            this.cmdDownload.AppearanceKey = "actionButton";
            this.cmdDownload.Location = new System.Drawing.Point(20, 343);
            this.cmdDownload.Name = "cmdDownload";
            this.cmdDownload.Size = new System.Drawing.Size(134, 40);
            this.cmdDownload.TabIndex = 16;
            this.cmdDownload.Text = "Process File";
            this.cmdDownload.Click += new System.EventHandler(this.cmdDownload_Click);
            // 
            // lstMessages
            // 
            this.lstMessages.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
            this.lstMessages.Location = new System.Drawing.Point(20, 349);
            this.lstMessages.Name = "lstMessages";
            this.lstMessages.Size = new System.Drawing.Size(457, 34);
            this.lstMessages.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.lstMessages, "FTP Status Messages");
            this.lstMessages.Visible = false;
            // 
            // vsDetail
            // 
            this.vsDetail.ColumnHeadersVisible = false;
            this.vsDetail.ExtendLastCol = true;
            this.vsDetail.FixedCols = 0;
            this.vsDetail.FixedRows = 0;
            this.vsDetail.Location = new System.Drawing.Point(20, 30);
            this.vsDetail.Name = "vsDetail";
            this.vsDetail.RowHeadersVisible = false;
            this.vsDetail.Rows = 0;
            this.vsDetail.ShowFocusCell = false;
            this.vsDetail.Size = new System.Drawing.Size(457, 218);
            this.vsDetail.TabIndex = 19;
            // 
            // vsFinancial
            // 
            this.vsFinancial.ColumnHeadersVisible = false;
            this.vsFinancial.ExtendLastCol = true;
            this.vsFinancial.FixedCols = 0;
            this.vsFinancial.FixedRows = 0;
            this.vsFinancial.Location = new System.Drawing.Point(20, 30);
            this.vsFinancial.Name = "vsFinancial";
            this.vsFinancial.RowHeadersVisible = false;
            this.vsFinancial.Rows = 0;
            this.vsFinancial.ShowFocusCell = false;
            this.vsFinancial.Size = new System.Drawing.Size(457, 218);
            this.vsFinancial.TabIndex = 20;
            this.vsFinancial.Visible = false;
            // 
            // lblPercent
            // 
            this.lblPercent.Location = new System.Drawing.Point(20, 268);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(163, 15);
            this.lblPercent.TabIndex = 18;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(20, 303);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(290, 20);
            this.ProgressBar1.Step = 1;
            this.ProgressBar1.TabIndex = 17;
            // 
            // fraRange
            // 
            this.fraRange.Controls.Add(this.cmdCancel);
            this.fraRange.Controls.Add(this.cmdProcess);
            this.fraRange.Controls.Add(this.cboEndMonth);
            this.fraRange.Controls.Add(this.cboEndYear);
            this.fraRange.Controls.Add(this.cboStartYear);
            this.fraRange.Controls.Add(this.cboStartMonth);
            this.fraRange.Controls.Add(this.lblTo);
            this.fraRange.Location = new System.Drawing.Point(30, 90);
            this.fraRange.Name = "fraRange";
            this.fraRange.Size = new System.Drawing.Size(600, 150);
            this.fraRange.TabIndex = 15;
            this.fraRange.Text = "Please Select The Range Of Records To Extract";
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(144, 90);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 40);
            this.cmdCancel.TabIndex = 7;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "actionButton";
            this.cmdProcess.Location = new System.Drawing.Point(20, 90);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(104, 40);
            this.cmdProcess.TabIndex = 6;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // cboEndMonth
            // 
            this.cboEndMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboEndMonth.Location = new System.Drawing.Point(327, 30);
            this.cboEndMonth.Name = "cboEndMonth";
            this.cboEndMonth.Size = new System.Drawing.Size(140, 40);
            this.cboEndMonth.TabIndex = 3;
            // 
            // cboEndYear
            // 
            this.cboEndYear.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndYear.Location = new System.Drawing.Point(487, 30);
            this.cboEndYear.Name = "cboEndYear";
            this.cboEndYear.Size = new System.Drawing.Size(90, 40);
            this.cboEndYear.TabIndex = 4;
            // 
            // cboStartYear
            // 
            this.cboStartYear.BackColor = System.Drawing.SystemColors.Window;
            this.cboStartYear.Location = new System.Drawing.Point(180, 30);
            this.cboStartYear.Name = "cboStartYear";
            this.cboStartYear.Size = new System.Drawing.Size(90, 40);
            this.cboStartYear.TabIndex = 2;
            // 
            // cboStartMonth
            // 
            this.cboStartMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboStartMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboStartMonth.Location = new System.Drawing.Point(20, 30);
            this.cboStartMonth.Name = "cboStartMonth";
            this.cboStartMonth.Size = new System.Drawing.Size(140, 40);
            this.cboStartMonth.TabIndex = 1;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(290, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(19, 24);
            this.lblTo.TabIndex = 5;
            this.lblTo.Text = "TO";
            // 
            // fraRapidRenewal
            // 
            this.fraRapidRenewal.Controls.Add(this.chkCreateJournal);
            this.fraRapidRenewal.Location = new System.Drawing.Point(30, 513);
            this.fraRapidRenewal.Name = "fraRapidRenewal";
            this.fraRapidRenewal.Size = new System.Drawing.Size(208, 77);
            this.fraRapidRenewal.TabIndex = 9;
            this.fraRapidRenewal.Text = "Financial Options";
            this.fraRapidRenewal.Visible = false;
            // 
            // chkCreateJournal
            // 
            this.chkCreateJournal.Location = new System.Drawing.Point(20, 30);
            this.chkCreateJournal.Name = "chkCreateJournal";
            this.chkCreateJournal.Size = new System.Drawing.Size(143, 22);
            this.chkCreateJournal.TabIndex = 10;
            this.chkCreateJournal.Text = "Create C/R Journal";
            // 
            // menuRapidRenewal
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(658, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "menuRapidRenewal";
            this.Text = "Rapid Renewal";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.menuRapidRenewal_Load);
            this.Activated += new System.EventHandler(this.menuRapidRenewal_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.menuRapidRenewal_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.menuRapidRenewal_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFileOptions)).EndInit();
            this.fraFileOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDownload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFinancial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
            this.fraRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRapidRenewal)).EndInit();
            this.fraRapidRenewal.ResumeLayout(false);
            this.fraRapidRenewal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateJournal)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}