﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTAgentInventory.
	/// </summary>
	public partial class rptLTTAgentInventory : BaseSectionReport
	{
		public rptLTTAgentInventory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Agent Inventory";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLTTAgentInventory InstancePtr
		{
			get
			{
				return (rptLTTAgentInventory)Sys.GetInstance(typeof(rptLTTAgentInventory));
			}
		}

		protected rptLTTAgentInventory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLTTAgentInventory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			SubReport2.Report = new rptSubReportHeadingLongTerm();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			subIssued.Report = new rptLTTSubIssuedPlates();
			subLost.Report = new rptLTTSubLostPlates();
			subExtended.Report = new rptLTTSubExtendedPlates();
		}

		private void rptLTTAgentInventory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLTTAgentInventory properties;
			//rptLTTAgentInventory.Caption	= "Agent Inventory";
			//rptLTTAgentInventory.Left	= 0;
			//rptLTTAgentInventory.Top	= 0;
			//rptLTTAgentInventory.Width	= 20280;
			//rptLTTAgentInventory.Height	= 11115;
			//rptLTTAgentInventory.StartUpPosition	= 3;
			//rptLTTAgentInventory.SectionData	= "rptLTTAgentInventory.dsx":0000;
			//End Unmaped Properties
		}
	}
}
