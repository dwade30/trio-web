//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Enums;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.Receipting;
using SharedApplication.Telemetry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
    public class MotorVehicle
    {
        public static ITelemetryService Telemetry = StaticSettings.GlobalTelemetryService;

        //=========================================================
        // ***** PRINTER ISSUES *********************
        [DllImport("user32", EntryPoint = "SendMessageA")]
        public static extern int SendMessageAsString(int hwnd, int wMsg, int wParam, string lParam);

        public const int lngMAX_CHAR_PER_LINE = 80;
        public const int lngGETLINE = 196;
        public const int lngGETLINECOUNT = 186;
        // ******************************************
        public enum cmdButtonEnum
        {
            cmdCompactNormal = 0,
            cmdCompactVersion = 1,
        }
        //
        public struct POINTAPI
        {
            public int x;
            public int y;
        };
        //
        // In a Modules Declarations section
        // #If Win32 Then
        // Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal
        // hWndInsertAfter As Long, ByVal x As Long, ByVal Y As Long, ByVal cx
        // As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
        // Global success As Long 'For holding the return value, Must be long in 32-Bit
        // #Else
        // Declare Function SetWindowPos Lib "user" (ByVal hwnd As Integer,
        // ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal Y As
        // Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As
        // Integer) As Integer
        // Global success As Integer 'For holding the return value, Must be Integer in 16-Bit
        // #End If
        public struct PlateCounter
        {
            public string Class;
            public int AdjustmentCount;
        };

        public struct OriginalPartyInfo
        {
            public string Owner1Name;
            public string Owner2Name;
            public string Owner3Name;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct OutgoingBMVDiskData
        {
            //FC:FINAL:SBE - #i2010 - use char[] instead of string when struct is used to read from file using FCFileSystem.FileGet
            //https://stackoverflow.com/questions/14631893/marshalled-c-string-missing-character
            // vbPorter upgrade warning: Class As FixedString	OnWrite(string)	OnRead(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] ClassCharArray;
            public string Class
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ClassCharArray);
                }
                set
                {
                    ClassCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: plate As FixedString	OnWrite(string)	OnRead(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] plateCharArray;
            public string plate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(plateCharArray);
                }
                set
                {
                    plateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: MVR3 As FixedString	OnWrite(string)	OnRead(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] MVR3CharArray;
            public string MVR3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MVR3CharArray);
                }
                set
                {
                    MVR3CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: RegFlag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RegFlagCharArray;
            public string RegFlag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RegFlagCharArray);
                }
                set
                {
                    RegFlagCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: ExciseExemptFlag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] ExciseExemptFlagCharArray;
            public string ExciseExemptFlag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExciseExemptFlagCharArray);
                }
                set
                {
                    ExciseExemptFlagCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: Status As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] StatusCharArray;
            public string Status
            {
                get
                {
                    return FCUtils.FixedStringFromArray(StatusCharArray);
                }
                set
                {
                    StatusCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: FeeExemptFlag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] FeeExemptFlagCharArray;
            public string FeeExemptFlag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FeeExemptFlagCharArray);
                }
                set
                {
                    FeeExemptFlagCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: YearStickerNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] YearStickerNumberCharArrayl;
            public string YearStickerNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(YearStickerNumberCharArrayl);
                }
                set
                {
                    YearStickerNumberCharArrayl = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: MonthStickerNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] MonthStickerNumberCharArray;
            public string MonthStickerNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MonthStickerNumberCharArray);
                }
                set
                {
                    MonthStickerNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: EffectiveDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] EffectiveDateCharArray;
            public string EffectiveDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(EffectiveDateCharArray);
                }
                set
                {
                    EffectiveDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: ExpirationDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] ExpirationDateCharArray;
            public string ExpirationDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExpirationDateCharArray);
                }
                set
                {
                    ExpirationDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: VIN As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
            public char[] VINCharArray;
            public string VIN
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VINCharArray);
                }
                set
                {
                    VINCharArray = FCUtils.FixedStringToArray(value, 17);
                }
            }
            // vbPorter upgrade warning: Year As FixedString	OnWrite(string)	OnRead(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] YearCharArray;
            public string Year
            {
                get
                {
                    return FCUtils.FixedStringFromArray(YearCharArray);
                }
                set
                {
                    YearCharArray = FCUtils.FixedStringToArray(value, 4);
                }
            }
            // vbPorter upgrade warning: make As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] makeCharArray;
            public string make
            {
                get
                {
                    return FCUtils.FixedStringFromArray(makeCharArray);
                }
                set
                {
                    makeCharArray = FCUtils.FixedStringToArray(value, 4);
                }
            }
            // vbPorter upgrade warning: model As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] modelCharArray;
            public string model
            {
                get
                {
                    return FCUtils.FixedStringFromArray(modelCharArray);
                }
                set
                {
                    modelCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: Style As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] StyleCharArray;
            public string Style
            {
                get
                {
                    return FCUtils.FixedStringFromArray(StyleCharArray);
                }
                set
                {
                    StyleCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Color1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] Color1CharArray;
            public string Color1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Color1CharArray);
                }
                set
                {
                    Color1CharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Color2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] Color2CharArray;
            public string Color2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Color2CharArray);
                }
                set
                {
                    Color2CharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Axles As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] AxlesCharArray;
            public string Axles
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AxlesCharArray);
                }
                set
                {
                    AxlesCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Tires As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public char[] TiresCharArray;
            public string Tires
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TiresCharArray);
                }
                set
                {
                    TiresCharArray = FCUtils.FixedStringToArray(value, 3);
                }
            }
            // vbPorter upgrade warning: NetWeight As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] NetWeightCharArray;
            public string NetWeight
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NetWeightCharArray);
                }
                set
                {
                    NetWeightCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: RegisteredWeight As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] RegisteredWeightCharArray;
            public string RegisteredWeight
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RegisteredWeightCharArray);
                }
                set
                {
                    RegisteredWeightCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: Fuel As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] FuelCharArray;
            public string Fuel
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FuelCharArray);
                }
                set
                {
                    FuelCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: DOB1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] DOB1CharArray;
            public string DOB1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DOB1CharArray);
                }
                set
                {
                    DOB1CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: LeaseFlag1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] LeaseFlag1CharArray;
            public string LeaseFlag1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LeaseFlag1CharArray);
                }
                set
                {
                    LeaseFlag1CharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: TrustFlag1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] TrustFlag1CharArray;
            public string TrustFlag1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TrustFlag1CharArray);
                }
                set
                {
                    TrustFlag1CharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: Owner1 As FixedString	OnWrite(string)	OnRead(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100)]
            public char[] Owner1CharArray;
            public string Owner1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Owner1CharArray);
                }
                set
                {
                    Owner1CharArray = FCUtils.FixedStringToArray(value, 100);
                }
            }
            // vbPorter upgrade warning: DOB2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] DOB2CharArray;
            public string DOB2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DOB2CharArray);
                }
                set
                {
                    DOB2CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: LeaseFlag2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] LeaseFlag2CharArray;
            public string LeaseFlag2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LeaseFlag2CharArray);
                }
                set
                {
                    LeaseFlag2CharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: TrustFlag2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] TrustFlag2CharArray;
            public string TrustFlag2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TrustFlag2CharArray);
                }
                set
                {
                    TrustFlag2CharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: Owner2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100)]
            public char[] Owner2CharArray;
            public string Owner2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Owner2CharArray);
                }
                set
                {
                    Owner2CharArray = FCUtils.FixedStringToArray(value, 100);
                }
            }
            // vbPorter upgrade warning: DOB3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] DOB3CharArray;
            public string DOB3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DOB3CharArray);
                }
                set
                {
                    DOB3CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: LeaseFlag3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] LeaseFlag3CharArray;
            public string LeaseFlag3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LeaseFlag3CharArray);
                }
                set
                {
                    LeaseFlag3CharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: TrustFlag3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] TrustFlag3CharArray;
            public string TrustFlag3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TrustFlag3CharArray);
                }
                set
                {
                    TrustFlag3CharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: Owner3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100)]
            public char[] Owner3CharArray;
            public string Owner3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Owner3CharArray);
                }
                set
                {
                    Owner3CharArray = FCUtils.FixedStringToArray(value, 100);
                }
            }
            // vbPorter upgrade warning: Mileage As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] MileageCharArray;
            public string Mileage
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MileageCharArray);
                }
                set
                {
                    MileageCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: BasePrice As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] BasePriceCharArray;
            public string BasePrice
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BasePriceCharArray);
                }
                set
                {
                    BasePriceCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: SalePrice As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] SalePriceCharArray;
            public string SalePrice
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SalePriceCharArray);
                }
                set
                {
                    SalePriceCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // vbPorter upgrade warning: ExciseTax As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] ExciseTaxCharArray;
            public string ExciseTax
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExciseTaxCharArray);
                }
                set
                {
                    ExciseTaxCharArray = FCUtils.FixedStringToArray(value, 9);
                }
            }
            // vbPorter upgrade warning: MilYear As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] MilYearCharArray;
            public string MilYear
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MilYearCharArray);
                }
                set
                {
                    MilYearCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: TaxIDNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] TaxIDNumberCharArray;
            public string TaxIDNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TaxIDNumberCharArray);
                }
                set
                {
                    TaxIDNumberCharArray = FCUtils.FixedStringToArray(value, 12);
                }
            }
            // vbPorter upgrade warning: DOTNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] DotNumberCharArray;
            public string DOTNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DotNumberCharArray);
                }
                set
                {
                    DotNumberCharArray = FCUtils.FixedStringToArray(value, 9);
                }
            }
            // vbPorter upgrade warning: OldClass As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] OldClassCharArray;
            public string OldClass
            {
                get
                {
                    return FCUtils.FixedStringFromArray(OldClassCharArray);
                }
                set
                {
                    OldClassCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: OldPlate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] OldPlateCharArray;
            public string OldPlate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(OldPlateCharArray);
                }
                set
                {
                    OldPlateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Battle As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] BattleCharArray;
            public string Battle
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BattleCharArray);
                }
                set
                {
                    BattleCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Address1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public char[] Address1CharArray;
            public string Address1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Address1CharArray);
                }
                set
                {
                    Address1CharArray = FCUtils.FixedStringToArray(value, 50);
                }
            }
            // vbPorter upgrade warning: Address2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public char[] Address2CharArray;
            public string Address2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Address2CharArray);
                }
                set
                {
                    Address2CharArray = FCUtils.FixedStringToArray(value, 50);
                }
            }
            // vbPorter upgrade warning: City As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public char[] CityCharArray;
            public string City
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CityCharArray);
                }
                set
                {
                    CityCharArray = FCUtils.FixedStringToArray(value, 50);
                }
            }
            // vbPorter upgrade warning: State As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] StateCharArray;
            public string State
            {
                get
                {
                    return FCUtils.FixedStringFromArray(StateCharArray);
                }
                set
                {
                    StateCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Zip As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
            public char[] ZipCharArray;
            public string Zip
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ZipCharArray);
                }
                set
                {
                    ZipCharArray = FCUtils.FixedStringToArray(value, 15);
                }
            }
            // vbPorter upgrade warning: Country As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] CountryCharArray;
            public string Country
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CountryCharArray);
                }
                set
                {
                    CountryCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: ResidenceCity As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public char[] ResidenceCityCharArray;
            public string ResidenceCity
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ResidenceCityCharArray);
                }
                set
                {
                    ResidenceCityCharArray = FCUtils.FixedStringToArray(value, 50);
                }
            }
            // vbPorter upgrade warning: ResidenceState As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] ResidenceStateCharArray;
            public string ResidenceState
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ResidenceStateCharArray);
                }
                set
                {
                    ResidenceStateCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: ResidenceCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] ResidenceCodeCharArray;
            public string ResidenceCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ResidenceCodeCharArray);
                }
                set
                {
                    ResidenceCodeCharArray = FCUtils.FixedStringToArray(value, 5);
                }
            }
            // vbPorter upgrade warning: ResidenceAddress As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public char[] ResidenceAddressCharArray;
            public string ResidenceAddress
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ResidenceAddressCharArray);
                }
                set
                {
                    ResidenceAddressCharArray = FCUtils.FixedStringToArray(value, 50);
                }
            }
            // vbPorter upgrade warning: ResidenceCountry As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] ResidenceCountryCharArray;
            public string ResidenceCountry
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ResidenceCountryCharArray);
                }
                set
                {
                    ResidenceCountryCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: DBAName As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public char[] DBANameCharArray;
            public string DBAName
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DBANameCharArray);
                }
                set
                {
                    DBANameCharArray = FCUtils.FixedStringToArray(value, 50);
                }
            }
            // vbPorter upgrade warning: TransactionDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
            public char[] TransactionDateCharArray;
            public string TransactionDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransactionDateCharArray);
                }
                set
                {
                    TransactionDateCharArray = FCUtils.FixedStringToArray(value, 17);
                }
            }
            // vbPorter upgrade warning: Unit As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] UnitCharArray;
            public string Unit
            {
                get
                {
                    return FCUtils.FixedStringFromArray(UnitCharArray);
                }
                set
                {
                    UnitCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: VanityFlag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] VanityFlagCharArray;
            public string VanityFlag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VanityFlagCharArray);
                }
                set
                {
                    VanityFlagCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: OldMVR3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] OldMVR3CharArray;
            public string OldMVR3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(OldMVR3CharArray);
                }
                set
                {
                    OldMVR3CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: SpecialtyFee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] SpecialtyFeeCharArray;
            public string SpecialtyFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SpecialtyFeeCharArray);
                }
                set
                {
                    SpecialtyFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: TransferFee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] TransferFeeCharArray;
            public string TransferFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransferFeeCharArray);
                }
                set
                {
                    TransferFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }

            // vbPorter upgrade warning: VanityFee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] VanityFeeCharArray;
            public string VanityFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VanityFeeCharArray);
                }
                set
                {
                    VanityFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: OtherFee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] OtherFeeCharArray;
            public string OtherFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(OtherFeeCharArray);
                }
                set
                {
                    OtherFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: RateFee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] RateFeeCharArray;
            public string RateFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RateFeeCharArray);
                }
                set
                {
                    RateFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: CreditFee As FixedString	OnWrite(string, FixedString)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] CreditFeeCharArray;
            public string CreditFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CreditFeeCharArray);
                }
                set
                {
                    CreditFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: Filler3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] Filler3CharArray;
            public string Filler3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Filler3CharArray);
                }
                set
                {
                    Filler3CharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: SalesTaxFee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] SalesTaxFeeCharArray;
            public string SalesTaxFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SalesTaxFeeCharArray);
                }
                set
                {
                    SalesTaxFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: TitleFee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] TitleFeeCharArray;
            public string TitleFee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TitleFeeCharArray);
                }
                set
                {
                    TitleFeeCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: TownResidenceCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] TownResidenceCodeCharArray;
            public string TownResidenceCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TownResidenceCodeCharArray);
                }
                set
                {
                    TownResidenceCodeCharArray = FCUtils.FixedStringToArray(value, 5);
                }
            }
            // vbPorter upgrade warning: CTA1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] CTA1CharArray;
            public string CTA1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CTA1CharArray);
                }
                set
                {
                    CTA1CharArray = FCUtils.FixedStringToArray(value, 9);
                }
            }
            // vbPorter upgrade warning: CTA2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] CTA2CharArray;
            public string CTA2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CTA2CharArray);
                }
                set
                {
                    CTA2CharArray = FCUtils.FixedStringToArray(value, 9);
                }
            }
            // vbPorter upgrade warning: PriorTitle As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public char[] PriorTitleCharArray;
            public string PriorTitle
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PriorTitleCharArray);
                }
                set
                {
                    PriorTitleCharArray = FCUtils.FixedStringToArray(value, 20);
                }
            }
            // vbPorter upgrade warning: PriorState As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] PriorStateCharArray;
            public string PriorState
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PriorStateCharArray);
                }
                set
                {
                    PriorStateCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: RentalFlag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RentalFlagCharArray;
            public string RentalFlag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RentalFlagCharArray);
                }
                set
                {
                    RentalFlagCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: ExciseTaxDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] ExciseTaxDateCharArray;
            public string ExciseTaxDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExciseTaxDateCharArray);
                }
                set
                {
                    ExciseTaxDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Filler1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 194)]
            public char[] Filler1CharArray;
            public string Filler1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Filler1CharArray);
                }
                set
                {
                    Filler1CharArray = FCUtils.FixedStringToArray(value, 194);
                }
            }
            // vbPorter upgrade warning: PlateStripped As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] PlateStrippedCharArray;
            public string PlateStripped
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PlateStrippedCharArray);
                }
                set
                {
                    PlateStrippedCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: Filler2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] Filler2CharArray;
            public string Filler2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Filler2CharArray);
                }
                set
                {
                    Filler2CharArray = FCUtils.FixedStringToArray(value, 5);
                }
            }
            // vbPorter upgrade warning: CRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] CRLFCharArray;
            public string CRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CRLFCharArray);
                }
                set
                {
                    CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
        };

        public struct IndividualOwner
        {
            // vbPorter upgrade warning: LastName As FixedString	OnWrite(string)
            public FCFixedString LastName;
            // vbPorter upgrade warning: FirstName As FixedString	OnWrite(string)
            public FCFixedString FirstName;
            // vbPorter upgrade warning: MiddleInitial As FixedString	OnWrite(string)
            public FCFixedString MiddleInitial;
            // vbPorter upgrade warning: Suffix As FixedString	OnWrite(string)
            public FCFixedString Suffix;
            public FCFixedString Filler;

            public IndividualOwner(int unusedParam)
            {
                LastName = new FCFixedString(50);
                FirstName = new FCFixedString(20);
                MiddleInitial = new FCFixedString(20);
                Suffix = new FCFixedString(3);
                Filler = new FCFixedString(7);
            }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RapidRenewalData
        {
            // vbPorter upgrade warning: plate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] plateCharArray;
            public string plate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(plateCharArray);
                }
                set
                {
                    plateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Class As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] ClassCharArray;
            public string Class
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ClassCharArray);
                }
                set
                {
                    ClassCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Subclass As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] SubclassCharArray;
            public string Subclass
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SubclassCharArray);
                }
                set
                {
                    SubclassCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: ExciseTax As FixedString	OnWrite(string, int)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] ExciseTaxCharArray;
            public string ExciseTax
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExciseTaxCharArray);
                }
                set
                {
                    ExciseTaxCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: BaseValue As FixedString	OnWrite(string, int)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] BaseValueCharArray;
            public string BaseValue
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BaseValueCharArray);
                }
                set
                {
                    BaseValueCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: MilRate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] MilRateCharArray;
            public string MilRate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MilRateCharArray);
                }
                set
                {
                    MilRateCharArray = FCUtils.FixedStringToArray(value, 4);
                }
            }
            // vbPorter upgrade warning: VIN As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
            public char[] VINCharArray;
            public string VIN
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VINCharArray);
                }
                set
                {
                    VINCharArray = FCUtils.FixedStringToArray(value, 17);
                }
            }
            // vbPorter upgrade warning: Year As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] YearCharArray;
            public string Year
            {
                get
                {
                    return FCUtils.FixedStringFromArray(YearCharArray);
                }
                set
                {
                    YearCharArray = FCUtils.FixedStringToArray(value, 4);
                }
            }
            // vbPorter upgrade warning: make As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] makeCharArray;
            public string make
            {
                get
                {
                    return FCUtils.FixedStringFromArray(makeCharArray);
                }
                set
                {
                    makeCharArray = FCUtils.FixedStringToArray(value, 4);
                }
            }
            // vbPorter upgrade warning: model As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] modelCharArray;
            public string model
            {
                get
                {
                    return FCUtils.FixedStringFromArray(modelCharArray);
                }
                set
                {
                    modelCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: Style As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] StyleCharArray;
            public string Style
            {
                get
                {
                    return FCUtils.FixedStringFromArray(StyleCharArray);
                }
                set
                {
                    StyleCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Color1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] Color1CharArray;
            public string Color1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Color1CharArray);
                }
                set
                {
                    Color1CharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Color2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] Color2CharArray;
            public string Color2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Color2CharArray);
                }
                set
                {
                    Color2CharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Axles As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] AxlesCharArray;
            public string Axles
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AxlesCharArray);
                }
                set
                {
                    AxlesCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: NetWeight As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] NetWeightCharArray;
            public string NetWeight
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NetWeightCharArray);
                }
                set
                {
                    NetWeightCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: RegisteredWeight As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] RegisteredWeightCharArray;
            public string RegisteredWeight
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RegisteredWeightCharArray);
                }
                set
                {
                    RegisteredWeightCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: Fuel As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] FuelCharArray;
            public string Fuel
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FuelCharArray);
                }
                set
                {
                    FuelCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: DOB1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] DOB1CharArray;
            public string DOB1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DOB1CharArray);
                }
                set
                {
                    DOB1CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Owner1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 39)]
            public char[] Owner1CharArray;
            public string Owner1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Owner1CharArray);
                }
                set
                {
                    Owner1CharArray = FCUtils.FixedStringToArray(value, 39);
                }
            }
            // vbPorter upgrade warning: Address1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 72)]
            public char[] Address1CharArray;
            public string Address1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Address1CharArray);
                }
                set
                {
                    Address1CharArray = FCUtils.FixedStringToArray(value, 72);
                }
            }
            // vbPorter upgrade warning: Address2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 72)]
            public char[] Address2CharArray;
            public string Address2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Address2CharArray);
                }
                set
                {
                    Address2CharArray = FCUtils.FixedStringToArray(value, 72);
                }
            }
            // vbPorter upgrade warning: City As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public char[] CityCharArray;
            public string City
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CityCharArray);
                }
                set
                {
                    CityCharArray = FCUtils.FixedStringToArray(value, 20);
                }
            }
            // vbPorter upgrade warning: State As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] StateCharArray;
            public string State
            {
                get
                {
                    return FCUtils.FixedStringFromArray(StateCharArray);
                }
                set
                {
                    StateCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Zip As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] ZipCharArray;
            public string Zip
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ZipCharArray);
                }
                set
                {
                    ZipCharArray = FCUtils.FixedStringToArray(value, 9);
                }
            }
            // vbPorter upgrade warning: DOB2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] DOB2CharArray;
            public string DOB2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DOB2CharArray);
                }
                set
                {
                    DOB2CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Owner2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 39)]
            public char[] Owner2CharArray;
            public string Owner2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Owner2CharArray);
                }
                set
                {
                    Owner2CharArray = FCUtils.FixedStringToArray(value, 39);
                }
            }
            // vbPorter upgrade warning: DOB3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] DOB3CharArray;
            public string DOB3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DOB3CharArray);
                }
                set
                {
                    DOB3CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Owner3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 39)]
            public char[] Owner3CharArray;
            public string Owner3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Owner3CharArray);
                }
                set
                {
                    Owner3CharArray = FCUtils.FixedStringToArray(value, 39);
                }
            }
            // vbPorter upgrade warning: ExciseExempt As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] ExciseExemptCharArray;
            public string ExciseExempt
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExciseExemptCharArray);
                }
                set
                {
                    ExciseExemptCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: RegExempt As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RegExemptCharArray;
            public string RegExempt
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RegExemptCharArray);
                }
                set
                {
                    RegExemptCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: OldStickerNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] OldStickerNumberCharArray;
            public string OldStickerNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(OldStickerNumberCharArray);
                }
                set
                {
                    OldStickerNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: TransactionDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] TransactionDateCharArray;
            public string TransactionDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransactionDateCharArray);
                }
                set
                {
                    TransactionDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: MailToAddress1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 72)]
            public char[] MailToAddress1CharArray;
            public string MailToAddress1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MailToAddress1CharArray);
                }
                set
                {
                    MailToAddress1CharArray = FCUtils.FixedStringToArray(value, 72);
                }
            }
            // vbPorter upgrade warning: MailToAddress2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 72)]
            public char[] MailToAddress2CharArray;
            public string MailToAddress2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MailToAddress2CharArray);
                }
                set
                {
                    MailToAddress2CharArray = FCUtils.FixedStringToArray(value, 72);
                }
            }
            // vbPorter upgrade warning: MailToCity As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public char[] MailToCityCharArray;
            public string MailToCity
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MailToCityCharArray);
                }
                set
                {
                    MailToCityCharArray = FCUtils.FixedStringToArray(value, 20);
                }
            }
            // vbPorter upgrade warning: MailToState As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] MailToStateCharArray;
            public string MailToState
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MailToStateCharArray);
                }
                set
                {
                    MailToStateCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: MailToZip As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] MailToZipCharArray;
            public string MailToZip
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MailToZipCharArray);
                }
                set
                {
                    MailToZipCharArray = FCUtils.FixedStringToArray(value, 9);
                }
            }
            // vbPorter upgrade warning: OldMVR3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] OldMVR3CharArray;
            public string OldMVR3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(OldMVR3CharArray);
                }
                set
                {
                    OldMVR3CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: MVR3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] MVR3CharArray;
            public string MVR3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MVR3CharArray);
                }
                set
                {
                    MVR3CharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: InsuranceCompany As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 60)]
            public char[] InsuranceCompanyCharArray;
            public string InsuranceCompany
            {
                get
                {
                    return FCUtils.FixedStringFromArray(InsuranceCompanyCharArray);
                }
                set
                {
                    InsuranceCompanyCharArray = FCUtils.FixedStringToArray(value, 60);
                }
            }
            // vbPorter upgrade warning: PolicyNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public char[] PolicyNumberCharArray;
            public string PolicyNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PolicyNumberCharArray);
                }
                set
                {
                    PolicyNumberCharArray = FCUtils.FixedStringToArray(value, 20);
                }
            }
            // vbPorter upgrade warning: PolicyExpiresDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] PolicyExpiresDateCharArray;
            public string PolicyExpiresDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PolicyExpiresDateCharArray);
                }
                set
                {
                    PolicyExpiresDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: EffectiveDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] EffectiveDateCharArray;
            public string EffectiveDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(EffectiveDateCharArray);
                }
                set
                {
                    EffectiveDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: ExpirationDate As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] ExpirationDateCharArray;
            public string ExpirationDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ExpirationDateCharArray);
                }
                set
                {
                    ExpirationDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: Mileage As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] MileageCharArray;
            public string Mileage
            {
                get
                {
                    return FCUtils.FixedStringFromArray(MileageCharArray);
                }
                set
                {
                    MileageCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: ResidenceCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] ResidenceCodeCharArray;
            public string ResidenceCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ResidenceCodeCharArray);
                }
                set
                {
                    ResidenceCodeCharArray = FCUtils.FixedStringToArray(value, 5);
                }
            }
            // vbPorter upgrade warning: stickernumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] stickernumberCharArray;
            public string stickernumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(stickernumberCharArray);
                }
                set
                {
                    stickernumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            // vbPorter upgrade warning: CRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] CRLFCharArray;
            public string CRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CRLFCharArray);
                }
                set
                {
                    CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
        };

        public struct RapidRenewalIndividualOwner
        {
            // vbPorter upgrade warning: LastName As FixedString	OnWrite(string)
            public FCFixedString LastName;
            // vbPorter upgrade warning: FirstName As FixedString	OnWrite(string)
            public FCFixedString FirstName;
            // vbPorter upgrade warning: MiddleInitial As FixedString	OnWrite(string)
            public FCFixedString MiddleInitial;
            // vbPorter upgrade warning: Suffix As FixedString	OnWrite(string)
            public FCFixedString Suffix;

            public RapidRenewalIndividualOwner(int unusedParam)
            {
                LastName = new FCFixedString(20);
                FirstName = new FCFixedString(15);
                MiddleInitial = new FCFixedString(1);
                Suffix = new FCFixedString(3);
            }
        };

        [DllImport("kernel32")]
        public static extern int WaitForSingleObject(int hHandle, int dwMilliseconds);

        [DllImport("kernel32")]
        public static extern int OpenProcess(uint dwDesiredAccess, int bInheritHandle, int dwProcessID);

        [DllImport("kernel32")]
        public static extern int CloseHandle(int hObject);

        [DllImport("kernel32")]
        public static extern int Beep(int dwFreq, int dwDuration);
        // Security Constants
        public const string DEFAULTDATABASE = "TWMV0000.vb1";
        public const int ENDOFPERIOD = 3;
        public const int INTERIMREPORTS = 4;
        public const int CLOSEPERIOD = 5;
        public const int VERIFICATIONREPORT = 6;
        public const int BMVUPDATEDISK = 7;
        public const int INVENTORYMAINTENANCE = 8;
        public const int ADDINVENTORY = 9;
        public const int REMOVEINVENTORY = 10;
        public const int CHANGEINVENTORYGROUP = 11;
        public const int ExceptionReport = 12;
        public const int REPORTING = 13;
        public const int VINLIST = 14;
        public const int FLEETLIST = 15;
        public const int REMINDERS = 16;
        public const int TELLERREPORT = 17;
        public const int TABLEPROCESSING = 18;
        public const int STYLECODES = 19;
        public const int STATECODES = 20;
        public const int COUNTYCODES = 21;
        public const int REASONCODES = 22;
        public const int MAKECODES = 23;
        public const int FEES = 24;
        public const int OPERATORCODES = 25;
        public const int CLASSCODES = 26;
        public const int COLORCODES = 27;
        public const int DEFAULTVALUES = 28;
        public const int FLEETMASTER = 29;
        public const int ADDFLEET = 30;
        public const int AddGroup = 31;
        public const int DELETEFLEETGROUP = 32;
        public const int REDBOOK = 34;
        public const int FILEMAINTENANCE = 35;
        public const int DATABASECLEANUP = 36;
        public const int MOTORVEHICLESETTINGS = 37;
        public const int EXTRACT = 38;
        public const int PURGE = 39;
        public const int INVENTORYSTATUS = 40;
        public const int RAPIDRENEWAL = 41;
        public const int MONTHLYLOAD = 42;
        public const int FINANCIALPROCESSING = 43;
        public const int VEHICLEUPDATE = 44;
        public const int TELLERCLOSEOUT = 45;
        public const int AUDITREPORT = 48;
        public const int AUDITARCHIVEREPORT = 47;
        public const int AUTOPOSTRAPIDRENEWAL = 49;
        public const int INFINITE = -1;
        public const uint SYNCHRONIZE = 0x100000;

        public enum Months
        {
            JANUARY = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12,
        }

        public static void ClearBMVRecord()
        {
            Statics.BMVRecord.Address1 = Strings.StrDup(50, " ");
            Statics.BMVRecord.Address2 = Strings.StrDup(50, " ");
            Statics.BMVRecord.Axles = Strings.StrDup(2, " ");
            Statics.BMVRecord.BasePrice = Strings.StrDup(8, " ");
            Statics.BMVRecord.Battle = Strings.StrDup(8, " ");
            Statics.BMVRecord.City = Strings.StrDup(50, " ");
            Statics.BMVRecord.Class = Strings.StrDup(2, " ");
            Statics.BMVRecord.Color1 = Strings.StrDup(2, " ");
            Statics.BMVRecord.Color2 = Strings.StrDup(2, " ");
            Statics.BMVRecord.Country = Strings.StrDup(2, " ");
            Statics.BMVRecord.CreditFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.CRLF = Strings.StrDup(2, " ");
            Statics.BMVRecord.CTA1 = Strings.StrDup(9, " ");
            Statics.BMVRecord.CTA2 = Strings.StrDup(9, " ");
            Statics.BMVRecord.DBAName = Strings.StrDup(50, " ");
            Statics.BMVRecord.DOB1 = Strings.StrDup(8, " ");
            Statics.BMVRecord.DOB2 = Strings.StrDup(8, " ");
            Statics.BMVRecord.DOB3 = Strings.StrDup(8, " ");
            Statics.BMVRecord.DOTNumber = Strings.StrDup(9, " ");
            Statics.BMVRecord.EffectiveDate = Strings.StrDup(8, " ");
            Statics.BMVRecord.ExciseExemptFlag = Strings.StrDup(1, " ");
            Statics.BMVRecord.ExciseTax = Strings.StrDup(9, " ");
            Statics.BMVRecord.ExciseTaxDate = Strings.StrDup(8, " ");
            Statics.BMVRecord.ExpirationDate = Strings.StrDup(8, " ");
            Statics.BMVRecord.FeeExemptFlag = Strings.StrDup(1, " ");
            Statics.BMVRecord.Filler1 = Strings.StrDup(194, " ");
            Statics.BMVRecord.Filler2 = Strings.StrDup(4, " ");
            Statics.BMVRecord.Fuel = Strings.StrDup(1, " ");
            Statics.BMVRecord.LeaseFlag1 = Strings.StrDup(1, " ");
            Statics.BMVRecord.LeaseFlag2 = Strings.StrDup(1, " ");
            Statics.BMVRecord.LeaseFlag3 = Strings.StrDup(1, " ");
            Statics.BMVRecord.make = Strings.StrDup(4, " ");
            Statics.BMVRecord.Mileage = Strings.StrDup(6, " ");
            Statics.BMVRecord.MilYear = Strings.StrDup(1, " ");
            Statics.BMVRecord.model = Strings.StrDup(6, " ");
            Statics.BMVRecord.MonthStickerNumber = Strings.StrDup(8, " ");
            Statics.BMVRecord.MVR3 = Strings.StrDup(8, " ");
            Statics.BMVRecord.NetWeight = Strings.StrDup(6, " ");
            Statics.BMVRecord.OldClass = Strings.StrDup(2, " ");
            Statics.BMVRecord.OldMVR3 = Strings.StrDup(8, " ");
            Statics.BMVRecord.OldPlate = Strings.StrDup(8, " ");
            Statics.BMVRecord.OtherFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.Owner1 = Strings.StrDup(100, " ");
            Statics.BMVRecord.Owner2 = Strings.StrDup(100, " ");
            Statics.BMVRecord.Owner3 = Strings.StrDup(100, " ");
            Statics.BMVRecord.plate = Strings.StrDup(8, " ");
            Statics.BMVRecord.PlateStripped = Strings.StrDup(7, " ");
            Statics.BMVRecord.PriorState = Strings.StrDup(2, " ");
            Statics.BMVRecord.PriorTitle = Strings.StrDup(20, " ");
            Statics.BMVRecord.RateFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.RegFlag = Strings.StrDup(1, " ");
            Statics.BMVRecord.RegisteredWeight = Strings.StrDup(6, " ");
            Statics.BMVRecord.Filler3 = Strings.StrDup(7, " ");
            Statics.BMVRecord.RentalFlag = Strings.StrDup(1, " ");
            Statics.BMVRecord.ResidenceAddress = Strings.StrDup(50, " ");
            Statics.BMVRecord.ResidenceCity = Strings.StrDup(50, " ");
            Statics.BMVRecord.ResidenceCountry = Strings.StrDup(2, " ");
            Statics.BMVRecord.SalePrice = Strings.StrDup(11, " ");
            Statics.BMVRecord.ResidenceCode = Strings.StrDup(5, " ");
            Statics.BMVRecord.ResidenceState = Strings.StrDup(2, " ");
            Statics.BMVRecord.SalesTaxFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.SpecialtyFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.State = Strings.StrDup(2, " ");
            Statics.BMVRecord.Status = Strings.StrDup(1, " ");
            Statics.BMVRecord.Style = Strings.StrDup(2, " ");
            Statics.BMVRecord.TaxIDNumber = Strings.StrDup(12, " ");
            Statics.BMVRecord.Tires = Strings.StrDup(3, " ");
            Statics.BMVRecord.TitleFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.TownResidenceCode = Strings.StrDup(5, " ");
            Statics.BMVRecord.TransactionDate = Strings.StrDup(17, " ");
            Statics.BMVRecord.TransferFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.TrustFlag1 = Strings.StrDup(1, " ");
            Statics.BMVRecord.TrustFlag2 = Strings.StrDup(1, " ");
            Statics.BMVRecord.TrustFlag3 = Strings.StrDup(1, " ");
            Statics.BMVRecord.Unit = Strings.StrDup(10, " ");
            Statics.BMVRecord.VanityFee = Strings.StrDup(7, " ");
            Statics.BMVRecord.VanityFlag = Strings.StrDup(1, " ");
            Statics.BMVRecord.VIN = Strings.StrDup(17, " ");
            Statics.BMVRecord.Year = Strings.StrDup(4, " ");
            Statics.BMVRecord.YearStickerNumber = Strings.StrDup(8, " ");
            Statics.BMVRecord.Zip = Strings.StrDup(15, " ");
        }

        public static void ClearRRRecord()
        {
            Statics.RRInfoRecord.Address1 = Strings.StrDup(72, " ");
            Statics.RRInfoRecord.Address2 = Strings.StrDup(72, " ");
            Statics.RRInfoRecord.Axles = Strings.StrDup(1, " ");
            Statics.RRInfoRecord.BaseValue = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.City = Strings.StrDup(20, " ");
            Statics.RRInfoRecord.Class = Strings.StrDup(2, " ");
            Statics.RRInfoRecord.Color1 = Strings.StrDup(2, " ");
            Statics.RRInfoRecord.Color2 = Strings.StrDup(2, " ");
            Statics.RRInfoRecord.CRLF = Strings.StrDup(2, " ");
            Statics.RRInfoRecord.DOB1 = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.DOB2 = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.DOB3 = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.EffectiveDate = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.ExciseExempt = Strings.StrDup(1, " ");
            Statics.RRInfoRecord.ExciseTax = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.ExpirationDate = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.Fuel = Strings.StrDup(1, " ");
            Statics.RRInfoRecord.InsuranceCompany = Strings.StrDup(60, " ");
            Statics.RRInfoRecord.MailToAddress1 = Strings.StrDup(72, " ");
            Statics.RRInfoRecord.MailToAddress2 = Strings.StrDup(72, " ");
            Statics.RRInfoRecord.MailToCity = Strings.StrDup(20, " ");
            Statics.RRInfoRecord.MailToState = Strings.StrDup(2, " ");
            Statics.RRInfoRecord.MailToZip = Strings.StrDup(9, " ");
            Statics.RRInfoRecord.make = Strings.StrDup(4, " ");
            Statics.RRInfoRecord.Mileage = Strings.StrDup(6, " ");
            Statics.RRInfoRecord.MilRate = Strings.StrDup(4, " ");
            Statics.RRInfoRecord.model = Strings.StrDup(6, " ");
            Statics.RRInfoRecord.MVR3 = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.NetWeight = Strings.StrDup(6, " ");
            Statics.RRInfoRecord.OldMVR3 = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.OldStickerNumber = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.Owner1 = Strings.StrDup(39, " ");
            Statics.RRInfoRecord.Owner2 = Strings.StrDup(39, " ");
            Statics.RRInfoRecord.Owner3 = Strings.StrDup(39, " ");
            Statics.RRInfoRecord.plate = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.PolicyExpiresDate = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.PolicyNumber = Strings.StrDup(20, " ");
            Statics.RRInfoRecord.RegisteredWeight = Strings.StrDup(6, " ");
            Statics.RRInfoRecord.RegExempt = Strings.StrDup(1, " ");
            Statics.RRInfoRecord.ResidenceCode = Strings.StrDup(5, " ");
            Statics.RRInfoRecord.State = Strings.StrDup(2, " ");
            Statics.RRInfoRecord.stickernumber = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.Style = Strings.StrDup(2, " ");
            Statics.RRInfoRecord.Subclass = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.TransactionDate = Strings.StrDup(8, " ");
            Statics.RRInfoRecord.VIN = Strings.StrDup(17, " ");
            Statics.RRInfoRecord.Year = Strings.StrDup(4, " ");
            Statics.RRInfoRecord.Zip = Strings.StrDup(9, " ");
        }

        public static MotorVehicleReturnValue Main(bool addExitMenu = false)
        {
            Telemetry.TrackHit($"MotorVehicle.cs.Main(addExitMenu = {addExitMenu})");
            if (Statics.startingMotorVehicle)
            {
                return MotorVehicleReturnValue.NoStatus;
            }
            Statics.startingMotorVehicle = true;
            string CurrentDirectory;
            clsDRWrapper rsMV = new clsDRWrapper();
            clsDRWrapper rsModules = new clsDRWrapper();
            clsDRWrapper rsVersion = new clsDRWrapper();
            clsDRWrapper rsOPID = new clsDRWrapper();
            clsDRWrapper rsUpdate = new clsDRWrapper();

            string strTemp;
            // kk05262015 troges-39  Change to single config file
            if (!FCConvert.ToBoolean(modGlobalFunctions.LoadSQLConfig("MotorVehicle")))
            {
                MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return MotorVehicleReturnValue.Failure;
            }
            Statics.bolFromWindowsCR = false;
            Statics.bolFromDosTrio = false;
            // DJW@20090205: Add increase to file locks
            //FCDBEngine.SetOption(dbMaxLocksPerFile, 200000);
            //FC:FINAL:IPI - not needed on web
            //if (App.PrevInstance() == true)
            //{
            //	MessageBox.Show("You already have an instance of Motor Vehicle running.  Please check your Program Manager for multiple instances of MV or TWMV0000.", "Program Ending", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //	return;
            //}
            modGlobalFunctions.UpdateUsedModules();
            if (!modGlobalConstants.Statics.gboolMV)
            {
                Telemetry.TrackEvent("Module Expired - MV");

                MessageBox.Show("Motor Vehicle has expired.  Please call TRIO.", "Module Expired", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            Statics.rsMVConnection.OpenRecordset("SELECT * FROM State", "TWMV0000.vb1");
            if (Strings.Right(App.Path, 1) == "\\")
                App.HelpFile = App.Path + "TWMV0000.hlp";
            else
                App.HelpFile = App.Path + "\\TWMV0000.hlp";
            Statics.cWSName = "NewWorkspace";
            // name of the workspace
            Statics.cWSUser = "admin";
            // the default
            Statics.cWSPassword = "";
            // the default, using TRIO causes a problem
            //wsNew = FCDBEngine.CreateWorkspace(cWSName, cWSUser, cWSPassword, dbUseJet);
            //FCDBEngine.Workspaces.Append(wsNew);
            modReplaceWorkFiles.GetGlobalVariables();
            var muniName = Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName));

            if (muniName == "MAINE TRAILER"
                || muniName == "MAINE MOTOR TRANSPORT"
                || muniName == "ACE REGISTRATION SERVICES LLC"
                || muniName == "COUNTRYWIDE TRAILER"
                || muniName == "HASKELL REGISTRATION"
                || muniName == "COUNTRYWIDE TRAILER"
                || muniName == "AB LEDUE ENTERPRISES")
            {
                Statics.gboolAllowLongTermTrailers = true;
                Statics.gintLongTermYearsAllowed = 24;
                Statics.gblnLongTermTimePaymentsAllowed = true;
                switch (muniName)
                {

                    case "MAINE MOTOR TRANSPORT":
                        Statics.gstrLongTermCompanyCode = "DH";

                        break;
                    case "MAINE TRAILER":
                        Statics.gstrLongTermCompanyCode = "MT";

                        break;
                    case "ACE REGISTRATION SERVICES LLC":
                        Statics.gstrLongTermCompanyCode = "AT";

                        break;
                    case "HASKELL REGISTRATION":
                        Statics.gstrLongTermCompanyCode = "BM";

                        break;
                    case "COUNTRYWIDE TRAILER":
                        Statics.gstrLongTermCompanyCode = "SM";

                        break;
                    default:
                        Statics.gstrLongTermCompanyCode = "";

                        break;
                }
            }
            else
            {
                Statics.gboolAllowLongTermTrailers = false;
            }
            // kk11192015 tromvs-55 October 2015 Legislative changes
            AddAntiqueMotorcycleCategory();
            UpdateAntiqueAutoFee();
            RemoveSMEBFClass();
            AddPassengerTruckCategory();
            rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'TransitPlates' AND COLUMN_NAME = 'ResidenceCode'", "MotorVehicle");
            if (rsUpdate.RecordCount() == 0) rsUpdate.Execute("ALTER TABLE TransitPlates ADD ResidenceCode nvarchar(255) NULL", "MotorVehicle");
            UpdateReasonCodes();
            SetPrintPriorityCodes();
            CheckVersion();
        NonUpdateTag:


            try
            {
                // On Error GoTo ErrorTag
                Information.Err().Clear();
                FixPCSubclass2016();

                // kk11142016 tromv-1235  Run fix for subclasses updated in error
                modNewAccountBox.GetFormats();
                if (modGlobalConstants.Statics.gboolBD) modValidateAccount.SetBDFormats();
                rsMV.OpenRecordset("SELECT * FROM WMV");
                rsModules.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
                CheckMVR3Number();

                if (FCConvert.ToString(modRegistry.GetRegistryKey("ShowReminders")) != "")
                    Statics.ShowReminders = FCConvert.ToBoolean(modRegistry.GetRegistryKey("ShowReminders"));
                else
                    Statics.ShowReminders = false;

                CurrentDirectory = Environment.CurrentDirectory;
                if (Strings.Right(CurrentDirectory, 1) != "\\") CurrentDirectory += "\\";

                // MsgBox CurrentDirectory
                // gstrNetworkFlag = IIf(GetRegistryKey("NetworkFlag") <> vbNullString, gstrReturn, "Y")
                Statics.TownLevel = FCConvert.ToInt16(rsModules.Get_Fields_String("MV_STATELEVEL"));
                if (FCConvert.ToString(rsMV.Get_Fields_String("UseTellerCloseout")) == "Y")
                    Statics.blnUseTellerCloseout = true;
                else
                    Statics.blnUseTellerCloseout = false;

                // MsgBox "6"
                if (!FCUtils.IsNull(rsModules.Get_Fields_String("MV_Rentals")))
                {
                    if (FCUtils.CBool(rsModules.Get_Fields_String("MV_Rentals")) == true)
                        Statics.AllowRentals = true;
                    else
                        Statics.AllowRentals = false;
                }
                else
                {
                    Statics.AllowRentals = false;
                }

                if (rsModules.Get_Fields_Boolean("RR") == true && rsModules.Get_Fields_DateTime("RRDate").ToOADate() >= DateTime.Today.ToOADate())
                    Statics.AllowRapidRenewal = true;
                else
                    Statics.AllowRapidRenewal = false;

                if (!FCUtils.IsNull(rsMV.Get_Fields_Boolean("PrintUseTax")))
                {
                    if (rsMV.Get_Fields_Boolean("PrintUseTax") == false)
                        Statics.blnTownPrintsUseTax = false;
                    else
                        Statics.blnTownPrintsUseTax = true;
                }
                else
                {
                    Statics.blnTownPrintsUseTax = true;
                }

                if (!FCUtils.IsNull(rsMV.Get_Fields_Boolean("LongTermPrintUseTaxCTA")))
                {
                    if (rsMV.Get_Fields_Boolean("LongTermPrintUseTaxCTA") == false)
                        Statics.blnTownPrintsLongTermUseTaxCTA = false;
                    else
                        Statics.blnTownPrintsLongTermUseTaxCTA = true;
                }
                else
                {
                    Statics.blnTownPrintsLongTermUseTaxCTA = true;
                }

                if (!FCUtils.IsNull(rsMV.Get_Fields_Boolean("LongTermAutoPopulateUnit")))
                {
                    if (rsMV.Get_Fields_Boolean("LongTermAutoPopulateUnit") == false)
                        Statics.blnLongTermAutoPopulateUnit = false;
                    else
                        Statics.blnLongTermAutoPopulateUnit = true;
                }
                else
                {
                    Statics.blnLongTermAutoPopulateUnit = true;
                }

                strTemp = GetComputerMVSetting("MVRT10UseLaser");

                if (strTemp == "")
                {
                    if (!FCUtils.IsNull(rsMV.Get_Fields_Boolean("LongTermLaserForms")))
                    {
                        if (FCConvert.ToBoolean(rsMV.Get_Fields_Boolean("Longtermlaserforms")))
                            strTemp = "True";
                        else
                            strTemp = "False";
                    }
                    else
                    {
                        strTemp = "False";
                    }
                }

                Statics.blnLongTermLaserForms = FCConvert.CBool(strTemp);

                if (!FCUtils.IsNull(rsMV.Get_Fields_Int16("UseTaxFormVersion")))
                    Statics.intUseTaxFormVersion = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMV.Get_Fields_Int16("UseTaxFormVersion"))));
                else
                    Statics.intUseTaxFormVersion = 0;

                if (!FCUtils.IsNull(rsMV.Get_Fields_Boolean("AllowExciseRebates")))
                {
                    if (rsMV.Get_Fields_Boolean("AllowExciseRebates") == true)
                        Statics.blnAllowExciseRebates = true;
                    else
                        Statics.blnAllowExciseRebates = false;
                }
                else
                {
                    Statics.blnAllowExciseRebates = false;
                }

                // MsgBox "7"
                if (!FCUtils.IsNull(rsMV.Get_Fields_String("MVR3Warning")))
                {
                    if (FCConvert.ToString(rsMV.Get_Fields_String("MVR3Warning")) == "N" || FCConvert.ToString(rsMV.Get_Fields_String("MVR3Warning")).ToLower() == "false")
                        Statics.MVR3Warning = false;
                    else
                        Statics.MVR3Warning = true;
                }
                else
                {
                    Statics.MVR3Warning = false;
                }

                
                modGlobalFunctions.GetGlobalRegistrySetting();

                // MsgBox "9"
                modReplaceWorkFiles.EntryFlagFile();
                if (modGlobalConstants.Statics.gstrEntryFlag == "CR") Statics.bolFromWindowsCR = true;

                modReplaceWorkFiles.EntryFlagFile(true, "  ");

                // MsgBox "10"
                GetWorkRecords();

                // is this from dos
                // MsgBox "11"
                if (!Statics.bolFromDosTrio)
                {
                    modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
                    modGlobalConstants.Statics.clsSecurityClass.Init("MV");
                }

                // MsgBox "12"
                clsDRWrapper rsMiscInfo = new clsDRWrapper();
                rsMiscInfo.OpenRecordset("SELECT * FROM DefaultInfo");

                if (rsMiscInfo.EndOfFile() != true && rsMiscInfo.BeginningOfFile() != true)
                {
                    rsMiscInfo.MoveLast();
                    rsMiscInfo.MoveFirst();

                    Statics.gboolCorrectionExtraFee = rsMiscInfo.Get_Fields_Boolean("ExtraFeeForCorrection") == true;

                    if (!FCUtils.IsNull(rsMiscInfo.Get_Fields_String("ReportAgent"))) Statics.ReportAgent = FCConvert.ToString(rsMiscInfo.Get_Fields_String("ReportAgent"));

                    if (!FCUtils.IsNull(rsMiscInfo.Get_Fields_String("ReportState"))) Statics.ReportState = FCConvert.ToString(rsMiscInfo.Get_Fields_String("ReportState"));

                    if (!FCUtils.IsNull(rsMiscInfo.Get_Fields_String("ReportTelephone"))) Statics.ReportTelephone = FCConvert.ToString(rsMiscInfo.Get_Fields_String("ReportTelephone"));

                    if (!FCUtils.IsNull(rsMiscInfo.Get_Fields_String("ReportTown"))) Statics.ReportTown = FCConvert.ToString(rsMiscInfo.Get_Fields_String("ReportTown"));

                    if (!FCUtils.IsNull(rsMiscInfo.Get_Fields_String("ReportZip"))) Statics.ReportZip = FCConvert.ToString(rsMiscInfo.Get_Fields_String("ReportZip"));

                    if (!FCUtils.IsNull(rsMiscInfo.Get_Fields_String("ResidenceCode")))
                    {
                        if (FCConvert.ToString(rsMiscInfo.Get_Fields_String("ResidenceCode")) == "")
                            MessageBox.Show("You are missing some default information.  Please go to Table Option Processing and fill it in before processing any registrations.", "Missing Default Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else
                            Statics.ResidenceCode = FCConvert.ToString(rsMiscInfo.Get_Fields_String("ResidenceCode"));
                    }
                }

                MDIParent.InstancePtr.Init(addExitMenu);
            TryOverTag:;

                /*? On Error GoTo 0 */
                //FC:FINAL:AM:#2379 - don't ask for operator id again if opened from CR
                if (String.IsNullOrEmpty(Statics.OpID) || Variables.Statics.ShowOpIDForm)
                {
                    Statics.OpID = "";
                    FCGlobal.Screen.MousePointer = 0;
                    frmOpID.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                }

                if (Statics.OpID == "988")
                {
                    Statics.OperatorLevel = 1;
                    Statics.PlateGroup = 0;
                    Statics.StickerGroup = 0;
                    Statics.MVR3Group = 0;
                    modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
                    modGlobalConstants.Statics.clsSecurityClass.Init("MV");

                    goto UnloadTag;
                }

                if (Statics.OpID != "")
                {
                    rsOPID.OpenRecordset("SELECT * FROM Operators", "SystemSettings");

                    if (rsOPID.EndOfFile() != true && rsOPID.BeginningOfFile() != true)
                    {
                        rsOPID.MoveLast();
                        rsOPID.MoveFirst();
                    }
                    else
                    {
                        MessageBox.Show("You do not have any Operator ID's in your Database.", "No Operators", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        modBlockEntry.WriteYY();

                        //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                        //Interaction.Shell("TWGNENTY.EXE", ProcessWindowStyle.Normal, false, -1);
                        App.MainForm.OpenModule("TWGNENTY");

                        return MotorVehicleReturnValue.Failure;
                    }

                    if (!rsOPID.FindFirstRecord("Code", Strings.UCase(Statics.OpID)))
                    {
                        MessageBox.Show("That is an invalid Operator ID.", "Invalid ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        //FC:FINAL:AM:#2364 - reset the operator ID
                        Statics.OpID = "";

                        goto TryOverTag;
                    }
                    else
                    {
                        Statics.OperatorLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOPID.Get_Fields_String("Level"))));
                        Statics.PlateGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOPID.Get_Fields_Int16("PlateGroup"))));
                        Statics.StickerGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOPID.Get_Fields_Int16("StickerGroup"))));
                        Statics.MVR3Group = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOPID.Get_Fields_Int16("MVR3Group"))));
                    }
                }
                else
                {
                    if (Statics.bolFromWindowsCR)
                    {
                        MDIParent.InstancePtr.Menu18();
                        App.MainForm.EndWaitMVModule();
                    }
                    return MotorVehicleReturnValue.Failure;
                }

            UnloadTag:;
                if (Statics.bolFromWindowsCR) MDIParent.InstancePtr.Menu1();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCGlobal.Screen.MousePointer = 0;
                modBlockEntry.WriteYY();

                MessageBox.Show(ex.Message, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Error);
                App.MainForm.OpenModule("TWGNENTY");
            }
            finally
            {
                rsUpdate.DisposeOf();
                rsMV.DisposeOf();
                rsModules.DisposeOf();
                rsOPID.DisposeOf();
                rsVersion.DisposeOf();
                Statics.startingMotorVehicle = false;
            }
            return MotorVehicleReturnValue.Success;
        }

        public static void VoidMVR3(int intMVR3Number, string operatorId)
        {
            clsDRWrapper rsV = new clsDRWrapper();
            rsV.OpenRecordset("SELECT * FROM InventoryAdjustments");
            rsV.AddNew();
            rsV.Set_Fields("DateOfAdjustment", DateTime.Today);
            rsV.Set_Fields("High", intMVR3Number);
            rsV.Set_Fields("Low", intMVR3Number);
            rsV.Set_Fields("OpID", operatorId);
            rsV.Set_Fields("reason", "Printer Alignment/Jam Problem");
            rsV.Set_Fields("PeriodCloseoutID", 0);
            rsV.Set_Fields("QuantityAdjusted", 1);
            rsV.Set_Fields("InventoryType", "MXS00");
            rsV.Set_Fields("AdjustmentCode", "V");
            rsV.Update();

            rsV.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + intMVR3Number);
            if (rsV.EndOfFile() != true && rsV.BeginningOfFile() != true)
            {
                rsV.Edit();
                rsV.Set_Fields("InventoryDate", DateTime.Today);
                rsV.Set_Fields("OpID", operatorId);
                rsV.Set_Fields("MVR3", intMVR3Number);
                rsV.Set_Fields("Status", "V");
                rsV.Update();
            }
        }

        public static string GetPartyName(int lngID, bool blnLastFirst = false)
        {
            string GetPartyName = "";
            clsDRWrapper rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(lngID), "CentralParties");
            if (rsTemp.BeginningOfFile() != true && rsTemp.EndOfFile() != true)
            {
                if (blnLastFirst == true)
                {
                    GetPartyName = Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields_String("FullNameLF")));
                }
                else
                {
                    GetPartyName = Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields_String("FullName")));
                }
            }
            else
            {
                GetPartyName = "";
            }
            return GetPartyName;
        }

        public static string GetPartyNameMiddleInitial(int lngID, bool blnLastFirst = false)
        {
            clsDRWrapper rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(lngID), "CentralParties");
            if (rsTemp.BeginningOfFile() != true && rsTemp.EndOfFile() != true)
            {
                if (blnLastFirst)
                {
                    return Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields_String("FullNameLFMiddleInitial")));
                }
                else
                {
                    return Strings.UCase(FCConvert.ToString(rsTemp.Get_Fields_String("FullNameMiddleInitial")));
                }
            }
            else
            {
                return "";
            }
        }
        public static void GetPlateRange(string record)
        {
            string HoldString;
            string string1 = new string(' ', 8);
            string string2 = new string(' ', 8);
            string prefix = new string(' ', 8);
            string Suffix = new string(' ', 8);
            string numeric = new string(' ', 8);
            string temp = "";
            int x;
            int y;
            int fnx;
            int counter;
            // 
            // first string
            HoldString = record;
            string1 = Strings.Mid(HoldString, 1, 8);
            string1 = Strings.Trim(string1.Replace("-", "")).PadRight(8);
            string2 = Strings.Mid(HoldString, 9, 8);
            string2 = Strings.Trim(string2.Replace("-", "")).PadRight(8);

            for (x = 1; x <= 8; x++)
            {
                temp = Strings.Mid(string1, x, 1);
                if (Information.IsNumeric(temp) == false)
                {
                    if (Convert.ToByte(temp[0]) < 65 || Convert.ToByte(temp[0]) > 90)
                    {
                        if (Convert.ToByte(temp[0]) < 97 || Convert.ToByte(temp[0]) > 122)
                        {
                            goto jumptag1;
                        }
                    }
                }
                if (Information.IsNumeric(temp) == false)
                {
                    if (Conversion.Val(numeric) == 0)
                    {
                        Strings.MidSet(ref prefix, x, 1, temp);
                    }
                    else
                    {
                        Strings.MidSet(ref Suffix, x, 1, temp);
                    }
                }
                else
                {
                    if (Strings.Trim(Suffix) == "")
                    {
                        if (Conversion.Val(numeric) == 0 && Conversion.Val(temp) == 0)
                        {
                            Strings.MidSet(ref prefix, x, 1, temp);
                        }
                        else
                        {
                            Strings.MidSet(ref numeric, x, 1, temp);
                        }
                    }
                    else
                    {
                        //
                        if (x == string1.Trim().Length)
                        {
                            Strings.MidSet(ref Suffix, x, 1, temp);
                        }
                        else
                        {
                            prefix = (Strings.Trim(numeric) + Strings.Trim(Suffix)).PadRight(8);
                            Suffix = Strings.Space(8);
                            numeric = Strings.Space(8);
                            if (Conversion.Val(temp) == 0)
                            {
                                Strings.MidSet(ref prefix, x, 1, temp);
                            }
                            else
                            {
                                Strings.MidSet(ref numeric, x, 1, temp);
                            }
                        }
                    }
                }
            jumptag1:
                ;
            }
            // x
            Statics.Numeric1 = Strings.Trim(numeric);
            Statics.Suffix1 = Strings.Trim(Suffix);
            Statics.Prefix1 = Strings.Trim(prefix);
            Statics.LengthOfNumeric = Statics.Numeric1.Length;
            //FC:FINAL:MSH - in vb6 declaring variable in format 'As String * 8' will generate string with fixed length 8 
            //and this variable will be autocompleted with spaces if input string less than declaration length
            //numeric = "";
            numeric = Strings.Space(8);
            prefix = Strings.Space(8);
            Suffix = Strings.Space(8);
            // 
            for (x = 1; x <= 8; x++)
            {
                temp = Strings.Mid(string2, x, 1);
                if (Information.IsNumeric(temp) == false)
                {
                    if (Convert.ToByte(temp[0]) < 65 || Convert.ToByte(temp[0]) > 122)
                    {
                        if (Convert.ToByte(temp[0]) < 97 || Convert.ToByte(temp[0]) > 122)
                        {
                            goto jumptag2;
                        }
                    }
                }
                if (Information.IsNumeric(temp) == false)
                {
                    if (Conversion.Val(numeric) == 0)
                    {
                        Strings.MidSet(ref prefix, x, 1, temp);
                    }
                    else
                    {
                        Strings.MidSet(ref Suffix, x, 1, temp);
                    }
                }
                else
                {
                    if (Strings.Trim(Suffix) == "")
                    {
                        if (Conversion.Val(numeric) == 0 && Conversion.Val(temp) == 0)
                        {
                            Strings.MidSet(ref prefix, x, 1, temp);
                        }
                        else
                        {
                            Strings.MidSet(ref numeric, x, 1, temp);
                        }
                    }
                    else
                    {
                        //FC:FINAL:MSH - in vb6 declaring variable in format 'As String * 8' will generate string with fixed length 8 
                        //and this variable will be autocompleted with spaces if input string less than declaration length
                        //prefix = (fecherFoundation.Strings.Trim(numeric) + fecherFoundation.Strings.Trim(Suffix));
                        if (x == string2.Trim().Length)
                        {
                            Strings.MidSet(ref Suffix, x, 1, temp);
                        }
                        else
                        {

                            prefix = (Strings.Trim(numeric) + Strings.Trim(Suffix)).PadRight(8);
                            Suffix = Strings.Space(8);
                            numeric = Strings.Space(8);
                            if (Conversion.Val(temp) == 0)
                            {
                                Strings.MidSet(ref prefix, x, 1, temp);
                            }
                            else
                            {
                                Strings.MidSet(ref numeric, x, 1, temp);
                            }
                        }
                    }
                }
            jumptag2:
                ;
            }
            // x
            // 
            Statics.Numeric2 = Strings.Trim(numeric);
            Statics.Suffix2 = Strings.Trim(Suffix);
            Statics.Prefix2 = Strings.Trim(prefix);
        }

        public static void BuildFinalRS(ref clsDRWrapper rsOne, ref clsDRWrapper rsTwo, ref clsDRWrapper rsThree)
        {
            // TYPE-      VEHICLE   DEMOGRAPHIC  DATE & $$
            // FILLNAME      PLATE     INFO FROM   INFO FROM   INFO FROM
            // FillTypeOne   RRR-SP        1           1           1
            // FillTypeTwo   RRR-NP        2           2           2
            // FillTypeTwo   RRR-OP        2           2           2
            // FillTypeThree RRT-PFTV      2           1           1   (kk05182016 this should not be a plate change. plate stays with reg)
            // FillTypeFour  RRT-PFRRV     1           2           2   (           this should be a plate change. plate not staying with reg)
            // FillTypeFive  RRT-NP        2           3           3
            // FillTypeFive  RRT-OP        2           3           3
            // FillTypeSix   NRT-XF       n/a          1           1
            // FillTypeSeven NRT-NP(CL)   n/a          2           2
            // FillTypeSeven NRT-NP(LOST) n/a          2           2
            // FillTypeEight NRT-OP       n/a          3           3
            // FillTypeNine  NRR-NP       n/a         n/a        calc
            // FillTypeNine  NRR-OP       n/a         n/a        calc
            // FillTypeNine  NRR-RP       n/a         n/a        calc
            // 
            // platetype      s=same plate
            // n=new plate
            // o=old plate
            // r=replacement vehicle
            // t=from transfer vehicle
            // vbPorter upgrade warning: tempdate As string	OnWrite(string, DateTime)
            DateTime tempdate = default(DateTime);
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            clsDRWrapper rsDefault = new clsDRWrapper();
            int intMonth;
            int originalRVW = 0;
			Statics.FinalRecordIDNumber = 0;
            Statics.RecordIDToTerminate = 0;
            Statics.rsFinal.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
            Statics.rsFinal.AddNew();
            if (Statics.RegistrationType == "RRR")
            {
                // on 7/31/00 ajb added "Or PlateType = 3" to the below line
                // due to RRR with Old Plate not bringing in the info from
                // the old plate.  Old plate is not plateforreg it is secondplate
                // and the newtothesystemflag was being set to true if the plateforreg
                // was not in the system.
                if (Statics.NewToTheSystem == false || Statics.PlateType == 3)
                {
                    clsDRWrapper temp2 = Statics.rsArchive;
                    if (Statics.PlateType == 1)
                    {
                        FillTypeOne(ref rsOne);
                        Statics.FinalRecordIDNumber = FCConvert.ToInt32(rsOne.Get_Fields_Int32("ID"));
                        // No current record error
                        TransferRecordSetsA(ref temp2, ref rsOne);
                    }
                    else
                    {
                        FillTypeTwo(ref rsTwo);
                        Statics.FinalRecordIDNumber = FCConvert.ToInt32(rsTwo.Get_Fields_Int32("ID"));
                        TransferRecordSetsA(ref temp2, ref rsTwo);
                        if (Statics.PlateType == 3)
                        {
                            if (FCUtils.IsNull(rsOne.Get_Fields_Int32("ID")) == false)
                            {
                                Statics.RecordIDToTerminate = FCConvert.ToInt32(rsOne.Get_Fields_Int32("ID"));
                            }
                        }
                    }
                    Statics.rsArchive = temp2;
                }
                else
                {
                    if ((Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION") && Statics.Class == "TL" && Statics.Subclass == "L9")
                    {
                        if (Statics.Plate1.Length == 4)
                        {
                            tempdate = Convert.ToDateTime("3/1/" + Statics.Plate1);
                            tempdate = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(tempdate.ToString()));
                        }
                    }
                    else
                    {
                        object temp2 = tempdate;
                        frmInput.InstancePtr.Init(ref temp2, "Enter New Expiration Date", "This plate is new to the system please input the new one year expiration date in the format MM/DD/YYYY.", 1800, false, modGlobalConstants.InputDTypes.idtDate);
                        tempdate = Convert.ToDateTime(Strings.Format(temp2, "MM/dd/yyyy"));
                        tempdate = tempdate.LastDayOfMonth();
                    }
                    if (!IsValidExpireDateForNTS(tempdate, Statics.Class, Statics.Subclass))
                    {
                        Statics.rsFinal.DisposeOf();
                        return;
                    }
                    // kk09072016  tromvs-57  This is in Access version but breaks the SQL version. Taking it back out.
                    // rsOne.AddNew
                    // Call TransferRecordSetsB(Statics.rsFinal, rsOne)
                    Statics.rsFinal.Set_Fields("ExpireDate", tempdate);
                    if (Statics.PlateType == 2)
                    {
                        Statics.rsFinal.Set_Fields("OldPlate", Statics.Plate2);
                        Statics.rsFinal.Set_Fields("OldClass", Statics.Class2);
                    }
                }
                // Dave 6/20/05
                if (!FCUtils.IsNull(Statics.rsFinal.Get_Fields_Boolean("ETO")))
                {
                    if (Statics.rsFinal.Get_Fields_Boolean("ETO") && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "TX" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "AP" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "ST" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "CS" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Plate")) != "NEW")
                    {
                        ans = MessageBox.Show("Is this an Excise Already Paid transaction?", "Excise Already Paid?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.Yes)
                        {
                            if (Statics.blnSuspended || Statics.blnSR22)
                            {
                                MessageBox.Show("This registration may only be processed as an Excise Tax Only process.", "Invalid Registration Options", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                // Statics.rsFinal.Reset();
                                Statics.rsFinal.DisposeOf();
                                return;
                            }
                            else
                            {
                                Statics.PreviousETO = true;
                            }
                        }
                        else
                        {
                            Statics.PreviousETO = false;
                        }
                    }
                    else
                    {
                        Statics.PreviousETO = false;
                    }
                }
                //App.DoEvents();
                ClearNormalTransaction();
                Statics.rsFinal.Set_Fields("TransactionType", "RRR");
                Statics.rsFinal.Set_Fields("ReReg", "Y");
                Statics.rsFinal.Set_Fields("Transfer", false);
                if (Statics.PlateType == 1)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "S");
                }
                else if (Statics.PlateType == 2)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "N");
                }
                else
                {
                    Statics.rsFinal.Set_Fields("PlateType", "O");
                }
            }
            else if (Statics.RegistrationType == "RRT")
            {
                if (Statics.PlateType == 1)
                {
                    // Plate from Transfer
                    FillTypeThree(ref rsOne, ref rsTwo);
                }
                else if (Statics.PlateType == 2)
                {
                    // Plate from Re-Reg
                    FillTypeFour(ref rsOne, ref rsTwo);
                }
                else
                {
                    // Plate Change (New or Old plate)
                    FillTypeFive(ref rsTwo, ref rsThree);
                }
                if (Statics.PlateType == 2)
                {
                    if (FCUtils.IsNull(rsOne.Get_Fields_Int32("ID")) == false)
                    {
                        Statics.RecordIDToTerminate = FCConvert.ToInt32(rsOne.Get_Fields_Int32("ID"));
                    }
                }
                else if (Statics.PlateType == 2 || Statics.PlateType == 3)
                {
                    if (FCUtils.IsNull(rsTwo.Get_Fields_Int32("ID")) == false)
                    {
                        Statics.RecordIDToTerminate = FCConvert.ToInt32(rsTwo.Get_Fields_Int32("ID"));
                    }
                }
                // Call ClearVehicleInformation
                Statics.rsFinal.Set_Fields("TransactionType", "RRT");
                Statics.rsFinal.Set_Fields("ReReg", "Y");
                Statics.rsFinal.Set_Fields("Transfer", true);
                if (Statics.PlateType == 1)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "T");
                }
                else if (Statics.PlateType == 2)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "S");
                }
                else if (Statics.PlateType == 3)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "N");
                }
                else if (Statics.PlateType == 4)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "O");
                }
                if (!FCUtils.IsNull(Statics.rsFinal.Get_Fields_Boolean("ETO")))
                {
                    Statics.PreviousETO = FCConvert.ToBoolean(Statics.rsFinal.Get_Fields_Boolean("ETO"));
                }
                // Dave 3/13/06
                if (!Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")))
                {
                    Statics.NewToTheSystem = true;
                }
                if (Statics.NewToTheSystem)
                {
                    string temp2 = Interaction.InputBox("This plate is new to the system please input the new expiration date in the format MM/dd/yyyy.", "New To System", null);
                    tempdate = Convert.ToDateTime(Strings.Format(temp2, "MM/dd/yyyy"));
                    if (!IsValidExpireDateForNTS(tempdate, Statics.Class, Statics.Subclass))
                    {
                        // MessageBox.Show("This is an invalid date please try again.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Statics.rsFinal.DisposeOf();
                        return;
                    }
                    Statics.rsFinal.Set_Fields("ExpireDate", tempdate);
                }
            }
            else if (Statics.RegistrationType == "NRT")
            {
                if (Statics.PlateType == 1)
                {
                    if (!FCUtils.IsNull(rsOne.Get_Fields_Boolean("ETO")))
                    {
                        if (FCConvert.CBool(rsOne.Get_Fields_Boolean("ETO")))
                        {
                            ans = MessageBox.Show("Are you completing an Excise Already Paid Transaction?", "Completing an Excise Already Paid Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                Statics.PreviousETO = FCConvert.CBool(rsOne.Get_Fields_Boolean("ETO"));
                            }
                        }
                    }
					originalRVW = rsOne.Get_Fields_Int32("RegisteredWeightNew");
                    FillTypeSix(ref rsOne);
                }
                else if (Statics.PlateType == 4)
                {
                    if (!FCUtils.IsNull(rsTwo.Get_Fields_Boolean("ETO")))
                    {
                        if (FCConvert.CBool(rsTwo.Get_Fields_Boolean("ETO")))
                        {
                            ans = MessageBox.Show("Are you completing an Excise Already Paid Transaction?", "Completing an Excise Already Paid Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                Statics.PreviousETO = FCConvert.CBool(rsTwo.Get_Fields_Boolean("ETO"));
                            }
                        }
                    }
                    originalRVW = rsTwo.Get_Fields_Int32("RegisteredWeightNew");
                    FillTypeEight(ref rsTwo);
                }
                else
                {
                    if (!FCUtils.IsNull(rsTwo.Get_Fields_Boolean("ETO")))
                    {
                        if (FCConvert.CBool(rsTwo.Get_Fields_Boolean("ETO")))
                        {
                            ans = MessageBox.Show("Are you completing an Excise Already Paid Transaction?", "Completing an Excise Already Paid Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                Statics.PreviousETO = FCConvert.CBool(rsTwo.Get_Fields_Boolean("ETO"));
                            }
                        }
                    }
                    originalRVW = rsTwo.Get_Fields_Int32("RegisteredWeightNew");
                    FillTypeSeven(ref rsTwo);
                }
                Statics.rsFinal.Set_Fields("TransactionType", "NRT");
                Statics.rsFinal.Set_Fields("ReReg", "N");
                Statics.rsFinal.Set_Fields("Transfer", true);
                if (Statics.PlateType == 1)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "T");
                }
                else if (Statics.PlateType == 2)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "N");
                }
                else if (Statics.PlateType == 3)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "N");
                }
                else
                {
                    Statics.rsFinal.Set_Fields("PlateType", "O");
                }
                if (Statics.NewToTheSystem)
                {
                    object temp2 = null;
                    frmInput.InstancePtr.Init(ref temp2, "Enter Expiration Date", "This plate is new to the system please input the current expiration date in the format MM/DD/YYYY.", 1700, false, modGlobalConstants.InputDTypes.idtDate);
                    tempdate = Convert.ToDateTime(Strings.Format(temp2, "MM/dd/yyyy"));
                    if (!IsValidExpireDateForNTS(tempdate, Statics.Class, Statics.Subclass))
                    {
                        MessageBox.Show("This is an invalid date please try again.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Statics.rsFinal.DisposeOf();
                        return;
                    }
                    Statics.rsFinal.Set_Fields("ExpireDate", tempdate);
                }
            }
            else if (Statics.RegistrationType == "NRR")
            {
                // If PlateType <> 2 Then
                // Call FillTypeNine(rsTwo)
                // Else
                // Call FillTypeNine(rsOne)
                // End If
                if (Statics.PlateType != 2)
                {
                    if (Statics.rsPlateForReg.Name() != "SELECT * FROM Master WHERE ID = 0")
                    {
                        Statics.FinalRecordIDNumber = FCConvert.ToInt32(rsOne.Get_Fields_Int32("ID"));
                        // No current record error
                        FillTypeOne(ref rsOne);
                    }
                    else
                    {
                        FillTypeNine(ref rsTwo);
                    }
                }
                else
                {
                    FillTypeNine(ref rsOne);
                }
                if (Statics.ExciseAP == true)
                {
                    clsDRWrapper temp2 = Statics.rsFinal;
                    TransferRecordSetsA(ref temp2, ref rsTwo);
                    Statics.rsFinal = temp2;
                    // this next if/else block is used for ETAP when plate is not in system
                    if (Statics.Plate1 == FCConvert.ToString(Statics.rsFinal.Get_Fields_String("plate")))
                    {
                        Statics.rsFinal.Edit();
                    }
                    else
                    {
                        Statics.rsFinal.AddNew();
                    }
                }
                else
                {
                    if (!FCUtils.IsNull(Statics.rsFinal.Get_Fields_Boolean("ETO")) && Statics.PlateType == 1)
                    {
                        Statics.PreviousETO = FCConvert.ToBoolean(Statics.rsFinal.Get_Fields_Boolean("ETO"));
                    }
                    ClearNormalTransaction();
                }
                Statics.rsFinal.Set_Fields("TransactionType", "NRR");
                Statics.rsFinal.Set_Fields("ReReg", "N");
                Statics.rsFinal.Set_Fields("Transfer", false);
                if (Statics.PlateType == 1)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "N");
                }
                else if (Statics.PlateType == 2)
                {
                    Statics.rsFinal.Set_Fields("PlateType", "O");
                }
                else
                {
                    Statics.rsFinal.Set_Fields("PlateType", "N");
                    Statics.rsFinal.Set_Fields("OldPlate", Statics.Plate2);
                    Statics.rsFinal.Set_Fields("OldClass", Statics.Class2);
                }
            }
            else if (Statics.RegistrationType == "DUPREG")
            {
                FillTypeOne(ref rsOne);
                Statics.rsFinal.Set_Fields("UseTaxDone", false);
                Statics.rsFinal.Set_Fields("TitleDone", false);
                // DJW 092711 took out shceck and just set subclass because of all the new subclassee going into effect need to be updated
                // If rsFinal.Fields("Subclass") = "!!" Then rsFinal.Fields("Subclass") = Subclass
                Statics.rsFinal.Set_Fields("Subclass", Statics.Subclass);
                clsDRWrapper temp2 = Statics.rsFinalCompare;
                clsDRWrapper temp3 = Statics.rsFinal;
                TransferRecordSetsA(ref temp2, ref temp3);
                Statics.rsFinalCompare = temp2;
                Statics.rsFinal = temp3;
                rsDefault.OpenRecordset("SELECT * FROM DefaultInfo");
                if (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Subclass")) == "L9")
                {
                    if (FCUtils.IsNull(rsDefault.Get_Fields_Decimal("AgentFeeLongTermTrailerDupReg")) == false)
                    {
                        Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", FCConvert.ToString(Conversion.Val(rsDefault.Get_Fields_Decimal("AgentFeeLongTermTrailerDupReg"))));
                    }
                    if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee")) == true)
                        Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 4);
                    if (Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee") == 0)
                        Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 4);
                }
                else
                {
                    if (FCUtils.IsNull(rsDefault.Get_Fields_Decimal("DuplicateRegistration")) == false)
                    {
                        if (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "CI" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "PH" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "DV")
                        {
                            Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", rsDefault.Get_Fields_Decimal("DuplicateRegistration"));
                        }
                        else
                        {
                            Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
                        }
                    }

                    if (Statics.rsFinal.Get_Fields_String("OPID") == "R/R")
                    {
	                    Statics.rsFinal.Set_Fields("GiftCertOrVoucher", "");

	                    var intialFee = GetInitialPlateFees(Statics.Class, Statics.Plate1,
		                    Statics.rsFinal.Get_Fields_Boolean("TrailerVanity"),
		                    Statics.rsFinal.Get_Fields_Boolean("TwoYear"));
	                    Statics.rsFinal.Set_Fields("InitialFee", intialFee);

	                    decimal platefee = 0;
	                    if (IsSpecialtyPlate(Statics.Class))
	                    {
		                    platefee = GetSpecialtyPlateRenewalFee(Statics.Class);
	                    }

	                    Statics.rsFinal.Set_Fields("PlateFeeReReg", platefee);
                    }

                    if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee")) == true)
                        Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 3);
                    if (Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee") == 0 && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "CI" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "PH" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "DV")
                        Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 3);
                }
                rsDefault.Reset();
                if (Statics.NewToTheSystem == true)
                {
                    ClearNormalTransaction();
                    ClearVehicleInformation();
                }
                Statics.rsFinal.Set_Fields("TransactionType", "DPR");
                Statics.rsFinal.Set_Fields("OpID", Statics.UserID);
                if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT")
                {
                    Statics.rsFinal.Set_Fields("Residence", "MMTA SERVICES INC. 44004");
                }
                else if (Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName)) == "ACE REGISTRATION SERVICES LLC")
                {
                    Statics.rsFinal.Set_Fields("Residence", "ACE REGISTRATION");
                    Statics.rsFinal.Set_Fields("ResidenceState", "ME");
                    Statics.rsFinal.Set_Fields("ResidenceCode", "44021");
                }
                else if (Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName)) == "STAAB AGENCY")
                {
                    Statics.rsFinal.Set_Fields("Residence", "STAAB AGENCY");
                }
                else if (Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER")
                {
                    Statics.rsFinal.Set_Fields("Residence", "COUNTRYWIDE TRAILER 44018");
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES")
                {
                    Statics.rsFinal.Set_Fields("Residence", "AB LEDUE ENTERPRISES");
                    Statics.rsFinal.Set_Fields("ResidenceState", "ME");
                    Statics.rsFinal.Set_Fields("ResidenceCode", "44003");
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER")
                {
                    Statics.rsFinal.Set_Fields("Residence", "MAINE TRAILER ME 44005");
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
                {
                    Statics.rsFinal.Set_Fields("Residence", "HASKELL REGISTRATION 44002");
                }
                return;
            }
            else if (Statics.RegistrationType == "LOST")
            {
                // Routine is in frmPlateInfo and will go directly to frmPreview
            }
            else if (Statics.RegistrationType == "GVW")
            {
                clsDRWrapper temp2 = Statics.rsPlateForReg;
                FillTypeOne(ref temp2);
                Statics.rsPlateForReg = temp2;
                Statics.rsFinal.Set_Fields("InitialFee", 0);
                Statics.rsFinal.Set_Fields("TransferFee", 0);
                Statics.rsFinal.Set_Fields("Transfer", false);
                Statics.rsFinal.Set_Fields("TransactionType", "GVW");
            }
            else if (Statics.RegistrationType == "CORR")
            {
                if (Statics.NewPlate == true)
                {
                    clsDRWrapper temp2 = Statics.rsSecondPlate;
                    FillTypeTwo(ref temp2);
                    Statics.rsSecondPlate = temp2;
                    Statics.rsFinal.Set_Fields("TransactionType", "ECR");
                    Statics.rsFinal.Set_Fields("PlateType", "N");
                    Statics.rsFinal.Set_Fields("GiftCertificateAmount", 0);
                    Statics.rsFinal.Set_Fields("GiftCertificateNumber", 0);
                    Statics.rsFinal.Set_Fields("GiftCertOrVoucher", "");
                    Statics.rsFinal.Set_Fields("ReplacementFee", 0);
                }
                else
                {
                    FillTypeOne(ref rsOne);
                    if (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("TransactionType")) == "LPS" || FCConvert.ToString(Statics.rsFinal.Get_Fields_String("TransactionType")) == "DPS" || FCConvert.ToString(Statics.rsFinal.Get_Fields_String("TransactionType")) == "DPR")
                    {
                        Statics.rsFinal.Set_Fields("ReplacementFee", 0);
                        Statics.rsFinal.Set_Fields("StickerFee", 0);
                    }
                    Statics.rsFinal.Set_Fields("TransactionType", "ECO");
                }
                Statics.rsFinal.Set_Fields("NoFeeDupReg", false);
                // kk11042016 tromvs-69  Fix No Fee Dup prompt not popping up on Lost Plate on Rental
            }
            // 
            if (Statics.Class == "CO")
                Statics.Subclass = "CO";
            if (Statics.Class == "TT")
                Statics.Subclass = "TT";
            // 11/21/01
            Statics.rsFinal.Set_Fields("OpID", Strings.UCase(Statics.OpID));
            if (Statics.rsFinal.IsFieldNull("ProrateMonth"))
            {
                // kk08242016 tromvs-57  TransferRecordSetsB doesn't like "" in Int16 field
                Statics.rsFinal.Set_Fields("ProrateMonth", 0);
            }
            clsDRWrapper temp = Statics.rsFinalCompare;
            clsDRWrapper temp1 = Statics.rsFinal;
            TransferRecordSetsB(ref temp, ref temp1);
            Statics.rsFinalCompare = temp;
            Statics.rsFinal = temp1;
            if (Statics.rsFinalCompare.EndOfFile() != true && Statics.rsFinalCompare.BeginningOfFile() != true)
            {
	            if (Statics.RegistrationType == "NRT")
	            {
		            Statics.rsFinalCompare.Set_Fields("RegisteredWeightNew", originalRVW);
		            Statics.rsFinalCompare.Update();
	            }
                Statics.rsFinalCompare.MoveLast();
                Statics.rsFinalCompare.MoveFirst();
                Statics.CompareRecord = true;
            }
            else
            {
                MessageBox.Show("There was an Error adding a record to the Temporary Master Table.", "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

            Statics.rsFinal.Set_Fields("Subclass", Statics.Subclass);
            Statics.rsFinal.Set_Fields("Class", Statics.Class);
            Statics.rsFinal.Set_Fields("plate", Statics.Plate1);
            Statics.rsFinal.Set_Fields("ForcedPlate", Statics.ForcedPlate);
            var vanityPlateFee = GetInitialPlateFees(Statics.rsFinal.Get_Fields_String("Class"), Statics.rsFinal.Get_Fields_String("Plate"), false, false, true, false);
            Statics.rsFinal.Set_Fields("VanityFlag", vanityPlateFee != 0);

            if (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Subclass")) == "L9" && (FCConvert.ToString(Statics.rsFinalCompare.Get_Fields_String("Class")) != "TL" || FCConvert.ToString(Statics.rsFinalCompare.Get_Fields_String("Subclass")) != "L9"))
            {
                Statics.rsFinal.Set_Fields("FleetNumber", 0);
            }
            else if (FCConvert.ToString(Statics.rsFinalCompare.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(Statics.rsFinalCompare.Get_Fields_String("Subclass")) == "L9" && (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) != "TL" || FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Subclass")) != "L9"))
            {
                Statics.rsFinal.Set_Fields("FleetNumber", 0);
            }
            Statics.rsFinal.Set_Fields("PlateStripped", Strings.Trim(FCConvert.ToString(Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
        }

        private static bool IsValidExpireDateForNTS(DateTime expireDate, string regClass = "", string regSubClass = "")
        {
            bool isValid = true;
            if (expireDate.ToString() == "" || !Information.IsDate(expireDate))
            {
                MessageBox.Show("This is an invalid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isValid = false;
            }
            else if (DateAndTime.DateValue(expireDate.ToString()).ToOADate() < DateTime.Today.ToOADate())
            {
                MessageBox.Show("The date entered has already passed.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isValid = false;
            }
            else if (IsMotorcycle(regClass) && expireDate.Month != 3)
            {
                MessageBox.Show("This is an invalid date for " + regClass + " class.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isValid = false;
            }
            else if (Statics.FixedMonth && expireDate.Month != Statics.gintFixedMonthMonth)
            {
                MessageBox.Show("This is an invalid date for " + regClass + " class.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isValid = false;
            }
            else
            {
                DateTime earlyDate = expireDate.AddMonths(-18);
                if (earlyDate.ToOADate() > DateTime.Today.ToOADate())
                {
                    MessageBox.Show("This registration cannot be processed. The expiration date is greater than 6 months away from now.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    isValid = false;
                }
                else if (expireDate.Day != DateTime.DaysInMonth(expireDate.Year, expireDate.Month))
                {
                    MessageBox.Show("This is an invalid date. Please try again.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    isValid = false;
                }
            }
            return isValid;
        }

        public static void FillTypeOne(ref clsDRWrapper rsOne)
        {
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsOne);
            Statics.rsFinal = temp;
            if (Statics.rsFinal.EndOfFile() != true && Statics.rsFinal.BeginningOfFile() != true)
            {
                if (Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("plate"))) == "NEW" && Statics.ReplacePlate)
                {
                    Statics.rsFinal.Edit();
                }
                else
                {
                    string strPlate = "";
                    string strTestPlate = "";
                    bool blnLong = false;
                    if (FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "L9")
                    {
                        blnLong = true;
                    }
                    else
                    {
                        blnLong = false;
                    }
                    if (GetInitialPlateFees2(Statics.rsPlateForReg.Get_Fields_String("Class"), Statics.rsPlateForReg.Get_Fields_String("Plate"), blnLongTerm: blnLong) == 0)
                    {
                        strPlate = strPlate.Replace("-", "");
                        strPlate = Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))) + Strings.StrDup(8 - Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))).Length, " ") + Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))) + Strings.StrDup(8 - Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))).Length, " ");
                        GetPlateRange(strPlate);
                        if (FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "L9")
                        {
                            strTestPlate = CreateFormattedInventory(Statics.Prefix1, Statics.Numeric1, Statics.Suffix1, "PXDLT");
                        }
                        else
                        {
                            strTestPlate = CreateFormattedInventory(Statics.Prefix1, Statics.Numeric1, Statics.Suffix1, "PXD" + Statics.rsPlateForReg.Get_Fields_String("Class"));
                        }
                    }
                    else
                    {
                        strTestPlate = FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"));
                    }
                    if (Strings.Trim(Statics.Plate1) != Strings.Trim(Strings.UCase(strTestPlate)))
                    {
                        Statics.rsFinal.AddNew();
                    }
                    else
                    {
                        Statics.rsFinal.Edit();
                    }
                }
            }
            else
            {
                Statics.rsFinal.AddNew();
            }
            if (Statics.RegistrationType == "NRR")
            {
                Statics.VehicleInfo = false;
            }
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            if (Statics.RegistrationType != "CORR" && Statics.RegistrationType != "DUPSTK" || (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Subclass")) == "L9"))
            {
                // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
                Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
                Statics.rsFinal.Set_Fields("OldPlate", Statics.rsFinal.Get_Fields_String("plate"));
                Statics.rsFinal.Set_Fields("OldMVR3", FCConvert.ToString(Conversion.Val(Statics.rsFinal.Get_Fields_Int32("MVR3"))));
				Statics.rsFinal.Set_Fields("PriorTaxReceipt", FCConvert.ToString(Conversion.Val(Statics.rsFinal.Get_Fields_Int32("MVR3"))));
                Statics.rsFinal.Set_Fields("OldClass", Statics.rsFinal.Get_Fields_String("Class"));
            }
            if (Strings.Trim(FCConvert.ToString(Statics.rsFinal.Get_Fields_String("OldPlate"))) == "")
                Statics.rsFinal.Set_Fields("OldPlate", Strings.Trim(FCConvert.ToString(Statics.rsFinal.Get_Fields_String("plate"))));
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
            // DJW 09232011 Took this line out because the trailer should nto lose excise exempt just because you do a correction
            // .Fields("ExciseExempt") = False
        }

        public static void FillTypeTwo(ref clsDRWrapper rsTwo)
        {
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsTwo);
            Statics.rsFinal = temp;
            if (Statics.rsFinal.EndOfFile() != true && Statics.rsFinal.BeginningOfFile() != true)
            {
                string strPlate = "";
                string strTestPlate = "";
                if (GetInitialPlateFees2(Statics.rsPlateForReg.Get_Fields_String("Class"), Statics.rsPlateForReg.Get_Fields_String("Plate")) == 0)
                {
                    strPlate = strPlate.Replace("-", "");
                    strPlate = Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))) + Strings.StrDup(8 - Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))).Length, " ") + Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))) + Strings.StrDup(8 - Strings.Trim(FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"))).Length, " ");
                    GetPlateRange(strPlate);
                    strTestPlate = CreateFormattedInventory(Statics.Prefix1, Statics.Numeric1, Statics.Suffix1, "PXD" + Statics.rsPlateForReg.Get_Fields_String("Class"));
                }
                else
                {
                    strTestPlate = FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("Plate"));
                }
                if (Strings.Trim(Statics.Plate1) != Strings.Trim(strTestPlate) && Statics.RegistrationType != "CORR" && Statics.RegistrationType != "LOST" && (Statics.RegistrationType != "RRR" || (Statics.PlateType != 2 && Statics.PlateType != 3)))
                {
                    Statics.rsFinal.AddNew();
                }
                else
                {
                    Statics.rsFinal.Edit();
                }
            }
            else
            {
                Statics.rsFinal.AddNew();
            }
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            Statics.rsFinal.Set_Fields("OldPlate", Statics.rsFinal.Get_Fields_String("plate"));
            Statics.rsFinal.Set_Fields("OldMVR3", FCConvert.ToString(Conversion.Val(Statics.rsFinal.Get_Fields_Int32("MVR3"))));
			Statics.rsFinal.Set_Fields("PriorTaxReceipt", FCConvert.ToString(Conversion.Val(Statics.rsFinal.Get_Fields_Int32("MVR3"))));
            Statics.rsFinal.Set_Fields("OldClass", Statics.rsFinal.Get_Fields_String("Class"));
            Statics.rsFinal.Set_Fields("VanityFlag", false);
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
        }

        public static void FillTypeThree(ref clsDRWrapper rsOne, ref clsDRWrapper rsTwo)
        {
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsOne);
            Statics.rsFinal = temp;
            FillVehicleInformation(ref rsTwo);
            FillDemographicInfo(ref rsTwo);
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            // kk09272016  Need to track the plate from the re-reg vehicle but not flag as a plate change per Sue McCormick
            // kk05182016  The old plate is from the registration being transferred, not from the vehicle
            // .Fields("OldPlate") = rsOne.Fields("Plate")
            // .Fields("OldClass") = rsOne.Fields("Class")
            Statics.rsFinal.Set_Fields("OldMVR3", FCConvert.ToString(Conversion.Val(rsOne.Get_Fields_Int32("MVR3"))));
			Statics.rsFinal.Set_Fields("PriorTaxReceipt", FCConvert.ToString(Conversion.Val(rsOne.Get_Fields_Int32("MVR3"))));
            // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            //TROWMV-118 State asked us to keep Unit Number where previously we were blanking it out
            Statics.rsFinal.Set_Fields("UnitNumber", rsTwo.Get_Fields_String("UnitNumber"));
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
        }

        public static void FillTypeFour(ref clsDRWrapper rsOne, ref clsDRWrapper rsTwo)
        {
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsTwo);
            Statics.rsFinal = temp;
            FillVehicleInformation(ref rsOne);
            FillDemographicInfo(ref rsOne);
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            // kk05182016  The old plate is from the registration being transferred, not from the vehicle
            Statics.rsFinal.Set_Fields("OldPlate", rsTwo.Get_Fields_String("Plate"));
            Statics.rsFinal.Set_Fields("OldClass", rsTwo.Get_Fields_String("Class"));
            Statics.rsFinal.Set_Fields("OldMVR3", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("MVR3"))));
			Statics.rsFinal.Set_Fields("PriorTaxReceipt", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("MVR3"))));
            Statics.rsFinal.Set_Fields("Battle", rsOne.Get_Fields_String("Battle"));
            //TROWMV-118 State asked us to keep Unit Number where previously we were blanking it out
            Statics.rsFinal.Set_Fields("UnitNumber", rsOne.Get_Fields_String("UnitNumber"));
            // kk05182016  Get the Battle decal for the plate
            // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
        }

        public static void FillTypeFive(ref clsDRWrapper rsTwo, ref clsDRWrapper rsThree)
        {
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsThree);
            Statics.rsFinal = temp;
            if (Statics.rsFinal.RecordCount() > 0)
            {
                Statics.rsFinal.Edit();
            }
            else
            {
                Statics.rsFinal.AddNew();
            }
            FillVehicleInformation(ref rsTwo);
            FillDemographicInfo(ref rsTwo);
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
        }

        public static void FillTypeSix(ref clsDRWrapper rsOne)
        {
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsOne);
            Statics.rsFinal = temp;
            ClearVehicleInformation();
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            // 
            // THIS WAS ADDED 8/10/00 BAB,AJB
            // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            Statics.rsFinal.Set_Fields("OldPlate", Statics.rsFinal.Get_Fields_String("plate"));
            Statics.rsFinal.Set_Fields("OldMVR3", FCConvert.ToString(Conversion.Val(Statics.rsFinal.Get_Fields_Int32("MVR3"))));
			Statics.rsFinal.Set_Fields("PriorTaxReceipt", FCConvert.ToString(Conversion.Val(Statics.rsFinal.Get_Fields_Int32("MVR3"))));
            Statics.rsFinal.Set_Fields("OldClass", Statics.rsFinal.Get_Fields_String("Class"));
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
            // THIS WAS ADDED 8/10/00 BAB,AJB
            // 
        }

        public static void FillTypeSeven(ref clsDRWrapper rsTwo)
        {
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsTwo);
            Statics.rsFinal = temp;
            ClearVehicleInformation();
            if (Statics.RegistrationType == "NRT" && (Statics.PlateType == 3 || Statics.PlateType == 2))
            {
                Statics.rsFinal.Set_Fields("OldClass", Statics.rsFinal.Get_Fields_String("Class"));
                Statics.rsFinal.Set_Fields("OldPlate", Statics.rsFinal.Get_Fields_String("plate"));
            }
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
        }

        public static void FillTypeEight(ref clsDRWrapper rsTwo)
        {
            clsDRWrapper temp = Statics.rsFinal;
            TransferRecordSetsA(ref temp, ref rsTwo);
            Statics.rsFinal = temp;
            ClearVehicleInformation();
            if (Statics.RegistrationType == "NRT" && Statics.PlateType == 4)
            {
	            Statics.rsFinal.Set_Fields("OldClass", Statics.rsFinal.Get_Fields_String("Class"));
	            Statics.rsFinal.Set_Fields("OldPlate", Statics.rsFinal.Get_Fields_String("plate"));
            }
            Statics.VehicleInfo = true;
            Statics.DemographicInfo = true;
            Statics.DateInfo = true;
            // If .endoffile <> True And .beginningoffile <> True Then .Edit Else .AddNew
            // .fields("registeredWeightOld")  = .fields("RegisteredWeightNew
            Statics.rsFinal.Set_Fields("FleetOld", Statics.rsFinal.Get_Fields_Boolean("FleetNew"));
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("make")) == true)
                Statics.VehicleInfo = false;
            if (FCUtils.IsNull(Statics.rsFinal.Get_Fields_String("Address")) == true)
                Statics.DemographicInfo = false;
            if (Information.IsDate(Statics.rsFinal.Get_Fields("ExpireDate")) == false)
                Statics.DateInfo = false;
            Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
        }

        public static void FillTypeNine(ref clsDRWrapper rsTwo)
        {
            if (Statics.PlateType == 3 || Statics.PlateType == 2)
            {
                clsDRWrapper temp = Statics.rsFinal;
                TransferRecordSetsA(ref temp, ref rsTwo);
                Statics.rsFinal = temp;
                ClearVehicleInformation();
                Statics.VehicleInfo = false;
                Statics.DemographicInfo = true;
                Statics.rsFinal.Set_Fields("plate", Statics.Plate1);
            }
            if (Statics.ExciseAP == true)
            {
                if (FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("make")) != "" && FCUtils.IsNull(Statics.rsPlateForReg.Get_Fields_String("make")) != true)
                {
                    Statics.VehicleInfo = true;
                    Statics.DemographicInfo = true;
                }
            }
            else
            {
                Statics.VehicleInfo = false;
                if (Statics.PlateType != 2)
                {
                    Statics.DemographicInfo = false;
                }
                else
                {
                    Statics.DemographicInfo = true;
                }
            }
            Statics.DateInfo = false;
        }

        public static void WaitForTerm(ref int pID)
        {
            int pHND;
            int pCheck = 0;
            pHND = OpenProcess(SYNCHRONIZE, 0, pID);
            if (pHND != 0)
            {
                foreach (FCForm ff in FCGlobal.Statics.Forms)
                {
                    ff.Enabled = false;
                }
                // ff
                do
                {
                    pCheck = WaitForSingleObject(pHND, 0);
                    if (pCheck != 0x102)
                        break;
                    //App.DoEvents();
                }
                while (true);
                foreach (FCForm ff in FCGlobal.Statics.Forms)
                {
                    ff.Enabled = true;
                }
                // ff
                CloseHandle(pHND);
            }
        }

        public static void FillVehicleInformation(ref clsDRWrapper rsTwo)
        {
            if (Statics.rsFinal.EndOfFile() != true && Statics.rsFinal.BeginningOfFile() != true)
                Statics.rsFinal.Edit();
            else
                Statics.rsFinal.AddNew();
            Statics.rsFinal.Set_Fields("Odometer", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("Odometer"))));
            Statics.rsFinal.Set_Fields("Trailer", rsTwo.Get_Fields_String("Trailer"));
            Statics.rsFinal.Set_Fields("Vin", rsTwo.Get_Fields_String("Vin"));
            Statics.rsFinal.Set_Fields("color1", rsTwo.Get_Fields_String("color1"));
            Statics.rsFinal.Set_Fields("color2", rsTwo.Get_Fields_String("color2"));
            Statics.rsFinal.Set_Fields("Year", rsTwo.Get_Fields_Int32("Year"));
            Statics.rsFinal.Set_Fields("make", rsTwo.Get_Fields_String("make"));
            Statics.rsFinal.Set_Fields("model", rsTwo.Get_Fields_String("model"));
            Statics.rsFinal.Set_Fields("Style", rsTwo.Get_Fields_String("Style"));
            Statics.rsFinal.Set_Fields("Tires", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("Tires"))));
            Statics.rsFinal.Set_Fields("Axles", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("Axles"))));
            Statics.rsFinal.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("NetWeight"))));
            Statics.rsFinal.Set_Fields("registeredWeightNew", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("registeredWeightNew"))));
            Statics.rsFinal.Set_Fields("registeredWeightOld", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("RegisteredWeightOld"))));
            Statics.rsFinal.Set_Fields("Fuel", rsTwo.Get_Fields_String("Fuel"));
            Statics.rsFinal.Set_Fields("UnitNumber", "");
            Statics.rsFinal.Set_Fields("DOTNumber", rsTwo.Get_Fields_String("DOTNumber"));
            Statics.rsFinal.Set_Fields("Value", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Double("Value"))));
            Statics.rsFinal.Set_Fields("BasePrice", FCConvert.ToString(Conversion.Val(rsTwo.Get_Fields_Int32("BasePrice"))));
            Statics.rsFinal.Set_Fields("BaseIsSalePrice", rsTwo.Get_Fields_Boolean("BaseIsSalePrice"));
            Statics.rsFinal.Set_Fields("MillYear", rsTwo.Get_Fields_Int32("MillYear"));
            Statics.rsFinal.Set_Fields("TrailerVanity", rsTwo.Get_Fields_Boolean("TrailerVanity"));
            Statics.rsFinal.Set_Fields("CC300", rsTwo.Get_Fields_Boolean("CC300"));
            Statics.rsFinal.Set_Fields("OldClass", rsTwo.Get_Fields_String("Class"));
            Statics.rsFinal.Set_Fields("OldPlate", rsTwo.Get_Fields_String("plate"));
            Statics.rsFinal.Set_Fields("OldMVR3", rsTwo.Get_Fields_Int32("MVR3"));
			Statics.rsFinal.Set_Fields("PriorTaxReceipt", rsTwo.Get_Fields_Int32("MVR3"));
        }

        public static void ClearVehicleInformation()
        {
            // DO NOT ADD "OLDPLATE"
            if (Statics.rsFinal.EndOfFile() == true && Statics.rsFinal.BeginningOfFile() == true)
            {
                Statics.rsFinal.AddNew();
            }
            if (!Statics.PreviousETO)
            {
                Statics.rsFinal.Set_Fields("Odometer", 0);
                // rsOne.fields("Odometer
                Statics.rsFinal.Set_Fields("Trailer", "");
                // rsOne.fields("Trailer
                Statics.rsFinal.Set_Fields("Vin", "");
                // rsOne.fields("VIN
                Statics.rsFinal.Set_Fields("color1", "");
                // rsOne.fields("color1
                Statics.rsFinal.Set_Fields("color2", "");
                // rsOne.fields("color2
                Statics.rsFinal.Set_Fields("Year", 0);
                // rsOne.fields("Year
                Statics.rsFinal.Set_Fields("make", "");
                // rsOne.fields("Make
                Statics.rsFinal.Set_Fields("model", "");
                // rsOne.fields("Model
                Statics.rsFinal.Set_Fields("Style", "");
                // rsOne.fields("Stylr
                Statics.rsFinal.Set_Fields("Tires", 0);
                // rsOne.fields("Tires
                Statics.rsFinal.Set_Fields("Axles", 0);
                // rsOne.fields("Axles
                Statics.rsFinal.Set_Fields("NetWeight", 0);
                // rsOne.fields("NetWeight
                Statics.rsFinal.Set_Fields("registeredWeightNew", 0);
                // rsOne.fields("RegisteredWeightNew
                Statics.rsFinal.Set_Fields("registeredWeightOld", 0);
                // rsOne.fields("RegisteredWeightOld
                Statics.rsFinal.Set_Fields("Fuel", "");
                // rsOne.fields("Fuel
                Statics.rsFinal.Set_Fields("Value", 0);
                // rsOne.fields("Value
                Statics.rsFinal.Set_Fields("BasePrice", 0);
                // rsOne.fields("BasePrice
                Statics.rsFinal.Set_Fields("BaseIsSalePrice", false);
                Statics.rsFinal.Set_Fields("MillYear", 0);
                // rsOne.fields("MillYear
                Statics.rsFinal.Set_Fields("TrailerVanity", false);
                // rsOne.fields("TrailerVanity
                Statics.rsFinal.Set_Fields("CC300", false);
                // rsOne.fields("CC300
                Statics.rsFinal.Set_Fields("FleetNumber", 0);
                Statics.rsFinal.Set_Fields("FleetOld", 0);
                Statics.rsFinal.Set_Fields("FleetNew", 0);
                Statics.rsFinal.Set_Fields("UnitNumber", "");
                //if (Statics.RegistrationType == "NRT" && Statics.PlateType == 3)
                //{
                //	// do nothing
                //}
                //else
                //{
                //	Statics.rsFinal.Set_Fields("OldClass", "");
                //	// rsOne.fields("OldClass
                //}
            }
        }

        public static void ClearNormalTransaction()
        {
            Statics.rsFinal.Set_Fields("MonthStickerCharge", 0);
            Statics.rsFinal.Set_Fields("MonthStickerNoCharge", 0);
            Statics.rsFinal.Set_Fields("MonthStickerNumber", 0);
            Statics.rsFinal.Set_Fields("YearStickerCharge", 0);
            Statics.rsFinal.Set_Fields("YearStickerNoCharge", 0);
            Statics.rsFinal.Set_Fields("YearStickerNumber", 0);
            Statics.rsFinal.Set_Fields("YearStickerYear", 0);
            Statics.rsFinal.Set_Fields("ExciseHalfRate", false);
            Statics.rsFinal.Set_Fields("ExciseProrate", false);
            Statics.rsFinal.Set_Fields("AgentFee", 0);
            if (!Statics.PreviousETO)
            {
                Statics.rsFinal.Set_Fields("ExciseTaxFull", 0);
                Statics.rsFinal.Set_Fields("ExciseCreditFull", 0);
                Statics.rsFinal.Set_Fields("ExciseTransferCharge", 0);
                Statics.rsFinal.Set_Fields("ExcisePaidMVR3", 0);
                // Dave 04/29/03
                // .fields("MVR3")  = 0
                Statics.rsFinal.Set_Fields("ExciseTaxDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                Statics.rsFinal.Set_Fields("ExcisePaidDate", null);
            }
            else
            {
                Statics.rsFinal.Set_Fields("ExcisePaidMVR3", Statics.rsFinal.Get_Fields_Int32("MVR3"));
                Statics.rsFinal.Set_Fields("ExcisePaidDate", Statics.rsFinal.Get_Fields_DateTime("DateUpdated"));
            }
            if (!Statics.blnChangeToFleet && !Statics.PreviousETO)
            {
                Statics.rsFinal.Set_Fields("ExciseTaxCharged", 0);
            }
            Statics.rsFinal.Set_Fields("ExciseCreditUsed", 0);
            Statics.rsFinal.Set_Fields("ExciseCreditMVR3", 0);
            Statics.rsFinal.Set_Fields("RegHalf", false);
            Statics.rsFinal.Set_Fields("RegProrate", false);
            Statics.rsFinal.Set_Fields("ProrateMonth", 0);
            Statics.rsFinal.Set_Fields("RegRateFull", 0);
            Statics.rsFinal.Set_Fields("RegRateCharge", 0);
            Statics.rsFinal.Set_Fields("TransferCreditFull", 0);
            Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
            Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
            Statics.rsFinal.Set_Fields("CommercialCredit", 0);
            Statics.rsFinal.Set_Fields("InitialFee", 0);
            Statics.rsFinal.Set_Fields("outofrotation", 0);
            Statics.rsFinal.Set_Fields("ReservePlateYN", false);
            Statics.rsFinal.Set_Fields("ReservePlate", 0);
            Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
            Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
            Statics.rsFinal.Set_Fields("ReplacementFee", 0);
            Statics.rsFinal.Set_Fields("StickerFee", 0);
            Statics.rsFinal.Set_Fields("TransferFee", 0);
            Statics.rsFinal.Set_Fields("GIFTCERTIFICATEAMOUNT", 0);
            Statics.rsFinal.Set_Fields("GiftCertificateNumber", 0);
            Statics.rsFinal.Set_Fields("SalesTax", 0);
            Statics.rsFinal.Set_Fields("TitleFee", 0);
            Statics.rsFinal.Set_Fields("CTANumber", "");
            Statics.rsFinal.Set_Fields("DoubleCTANumber", "");
            Statics.rsFinal.Set_Fields("PriorTitleNumber", "");
            Statics.rsFinal.Set_Fields("PriorTitleState", "");
            Statics.rsFinal.Set_Fields("InfoCodes", "");
            Statics.rsFinal.Set_Fields("InfoMessage", "");
            Statics.rsFinal.Set_Fields("ECorrectInfoCodes", "");
            Statics.rsFinal.Set_Fields("ECorrectInfoMessage", "");
            Statics.rsFinal.Set_Fields("StatePaid", 0);
            Statics.rsFinal.Set_Fields("LocalPaid", 0);
            Statics.rsFinal.Set_Fields("ETO", false);
            Statics.rsFinal.Set_Fields("TwoYear", false);
            Statics.rsFinal.Set_Fields("OpID", "");
        }

        public static void TransferRecordSetsA(ref clsDRWrapper rs1, ref clsDRWrapper rs2)
        {
            Information.Err().Clear();
            string strNew;
            strNew = rs2.Name();
            rs1.OpenRecordset(strNew);
        Donetag:
            ;
            Information.Err().Clear();
            /*? On Error GoTo 0 */
        }

        public static void TransferRecordSetsB(ref clsDRWrapper rs1, ref clsDRWrapper rs2)
        {
            clsDRWrapper tmpRS = new clsDRWrapper();
            string strValues = "";
            int fnx;
            string strFields = "";
            cPartyController pCont = new cPartyController();
            cParty pInfo;
            Information.Err().Clear();
            try
            {
                // On Error GoTo ErrorTag
                Information.Err().Clear();
                tmpRS.OpenRecordset("SELECT * FROM tblMasterTemp WHERE ID = 0");
                tmpRS.AddNew();
                for (fnx = 1; fnx <= rs2.FieldsCount - 1; fnx++)
                {
                    tmpRS.Set_Fields(rs2.Get_FieldsIndexName(fnx), rs2.Get_Fields(rs2.Get_FieldsIndexName(fnx)));
                }
                tmpRS.Update();
                rs1.OpenRecordset("SELECT * FROM tblMasterTemp WHERE ID = " + tmpRS.Get_Fields_Int32("ID"));
                pInfo = pCont.GetParty(rs1.Get_Fields_Int32("PartyID1"));
                if (pInfo != null)
                {
                    Statics.orgOwnerInfo.Owner1Name = Strings.UCase(pInfo.FullName);
                }

                {
                    Statics.orgOwnerInfo.Owner1Name = "";
                }

                if (Conversion.Val(rs1.Get_Fields_Int32("PartyID2")) != 0)
                {
                    pInfo = pCont.GetParty(rs1.Get_Fields_Int32("PartyID2"));
                    if (pInfo != null)
                    {
                        Statics.orgOwnerInfo.Owner2Name = Strings.UCase(pInfo.FullName);
                    }
                    else
                    {
                        Statics.orgOwnerInfo.Owner2Name = "";
                    }
                }
                else
                {
                    Statics.orgOwnerInfo.Owner2Name = "";
                }
                if (Conversion.Val(rs1.Get_Fields_Int32("PartyID3")) != 0)
                {
                    pInfo = pCont.GetParty(rs1.Get_Fields_Int32("PartyID3"));
                    if (pInfo != null)
                    {
                        Statics.orgOwnerInfo.Owner3Name = Strings.UCase(pInfo.FullName);
                    }
                    else
                    {
                        Statics.orgOwnerInfo.Owner3Name = "";
                    }
                }
                else
                {
                    Statics.orgOwnerInfo.Owner3Name = "";
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("There is an error in the original Master Record for this Plate/Transaction.  Please call TRIO.  (" + Information.Err(ex).Description + FCConvert.ToString(Information.Err(ex).Number) + ")", "Call TRIO", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Information.Err(ex).Clear();
            }
        }

        public static void TransferRecordToArchive(ref clsDRWrapper rs2, int intPlate)
        {
            // pass this function the original record from the master record that willend to be RSFinal
            // 
            clsDRWrapper tmpRS = new clsDRWrapper();
            int fnx;
            int lngHeldID;
            try
            {
                // 
                // tmpRS.Execute "INSERT INTO ArchiveMaster (ID) Values (" & rs2.Fields("ID") & ")", "TWMV0000.vb1"
                tmpRS.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = 0");
                tmpRS.AddNew();
                // Dave 09/22/06   tmprs.addnew
                for (fnx = 1; fnx <= rs2.FieldsCount - 1; fnx++)
                {
                    // Dave 09/06/06
                    //FC:FINAL:MSH - i.issue #1766: replace Get_Fields by Get_Fields_String for converting data to string and avoid comparing exceptions
                    //if (rs2.Get_Fields(rs2.Get_FieldsIndexName(fnx)) != "" && rs2.Get_FieldsIndexName(fnx) != "ID")
                    if (rs2.Get_Fields_String(rs2.Get_FieldsIndexName(fnx)) != "" && rs2.Get_FieldsIndexName(fnx) != "ID")
                    {
                        tmpRS.Set_Fields(rs2.Get_FieldsIndexName(fnx), rs2.Get_Fields(rs2.Get_FieldsIndexName(fnx)));
                    }
                }
                tmpRS.Update();
                lngHeldID = FCConvert.ToInt32(tmpRS.Get_Fields_Int32("ID"));
                tmpRS.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(lngHeldID), "TWMV0000.vb1");
                if (tmpRS.EndOfFile() != true && tmpRS.BeginningOfFile() != true)
                {
                    tmpRS.MoveLast();
                    tmpRS.MoveFirst();
                    if (intPlate == 0)
                    {
                        Statics.FinalCompareID = FCConvert.ToInt32(tmpRS.Get_Fields_Int32("ID"));
                    }
                    else if (intPlate == 1)
                    {
                        Statics.FinalCompareID2 = FCConvert.ToInt32(tmpRS.Get_Fields_Int32("ID"));
                    }
                    else
                    {
                        Statics.FinalCompareID3 = FCConvert.ToInt32(tmpRS.Get_Fields_Int32("ID"));
                    }
                }
                else
                {
                    MessageBox.Show("Error Trying To Locate ArchiveMaster Record", "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("There is an error in the original Master Record for this Plate/Transaction.  Please call TRIO.  (" + Information.Err(ex).Description + FCConvert.ToString(Information.Err(ex).Number) + ")", "Call TRIO", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Information.Err(ex).Clear();
                return;
            }
        }

        public static void TransferRecordToPending(ref clsDRWrapper rs2)
        {
            // pass this function the original record from the master record that willend to be RSFinal
            // 
            clsDRWrapper tmpRS = new clsDRWrapper();
            int fnx;
            int lngHeldID;
            try
            {
                // 
                tmpRS.OpenRecordset("SELECT * FROM PendingActivityMaster WHERE ID = 0");
                tmpRS.AddNew();
                for (fnx = 1; fnx <= rs2.FieldsCount - 1; fnx++)
                {
                    //FC:FINAL:MSH - i.issue #1766: replace Get_Fields by Get_Fields_String for converting data to string and avoid comparing exceptions
                    //if (rs2.Get_Fields(rs2.Get_FieldsIndexName(fnx)) != "" && rs2.Get_FieldsIndexName(fnx) != "ID" && rs2.Get_FieldsIndexName(fnx) != "ArchiveID")
                    if (rs2.Get_Fields_String(rs2.Get_FieldsIndexName(fnx)) != "" && rs2.Get_FieldsIndexName(fnx) != "ID" && rs2.Get_FieldsIndexName(fnx) != "ArchiveID")
                    {
                        tmpRS.Set_Fields(rs2.Get_FieldsIndexName(fnx), rs2.Get_Fields(rs2.Get_FieldsIndexName(fnx)));
                    }
                    else if (rs2.Get_FieldsIndexName(fnx) == "ArchiveID")
                    {
                        tmpRS.Set_Fields(rs2.Get_FieldsIndexName(fnx), rs2.Get_Fields_Int32("ID"));
                    }
                }
                tmpRS.Update();
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("There is an error putting transaction into print queue.  Please call TRIO.  (" + Information.Err(ex).Description + FCConvert.ToString(Information.Err(ex).Number) + ")", "Call TRIO", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Information.Err(ex).Clear();
                throw;
            }
        }

        public static bool GetRedbookValues(bool blnSendValues = true, bool blnProcessingReg = true)
        {
            bool GetRedbookValues = false;
            try
            {
                // On Error GoTo endtag
                Information.Err().Clear();
                //FC:FINAL:AM:#i1808 - use cSettingController instead of the registry
                cSettingsController set = new cSettingsController();
                if (blnSendValues)
                {
                    //Interaction.SaveSetting("TWGNENTY", "REDBOOK", "MVFLAG", "MV");
                    //Interaction.SaveSetting("TWGNENTY", "REDBOOK", "VIN", frmDataInput.InstancePtr.txtVin.Text);
                    //Interaction.SaveSetting("TWGNENTY", "REDBOOK", "YEAR", frmDataInput.InstancePtr.txtYear.Text);
                    //Interaction.SaveSetting("TWGNENTY", "REDBOOK", "MAKE", frmDataInput.InstancePtr.txtMake.Text);
                    //Interaction.SaveSetting("TWGNENTY", "REDBOOK", "MODEL", frmDataInput.InstancePtr.txtModel.Text);
                    //Interaction.SaveSetting("TWGNENTY", "REDBOOK", "BASE", frmDataInput.InstancePtr.txtBase.Text);
                    //Interaction.SaveSetting("TWGNENTY", "REDBOOK", "MILYEAR", frmDataInput.InstancePtr.txtMilYear.Text);
                    set.SaveSetting("MV", "MVFLAG", "TWGNENTY", "REDBOOK");
                    set.SaveSetting(frmDataInput.InstancePtr.txtVin.Text, "VIN", "TWGNENTY", "REDBOOK");
                    set.SaveSetting(frmDataInput.InstancePtr.txtYear.Text, "YEAR", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting(frmDataInput.InstancePtr.txtMake.Text, "MAKE", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting(frmDataInput.InstancePtr.txtModel.Text, "MODEL", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting(frmDataInput.InstancePtr.txtBase.Text, "BASE", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting(frmDataInput.InstancePtr.txtMilYear.Text, "MILYEAR", "TWGNENTY", "REDBOOK"); ;
                    if (blnProcessingReg)
                    {
                        set.SaveSetting("Y", "PROCESSREG", "TWGNENTY", "REDBOOK"); ;
                    }
                    else
                    {
                        set.SaveSetting("N", "PROCESSREG", "TWGNENTY", "REDBOOK"); ;
                    }
                }
                else
                {
                    set.SaveSetting("MV", "MVFLAG", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting("", "VIN", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting("", "YEAR", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting("", "MAKE", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting("", "MODEL", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting("", "BASE", "TWGNENTY", "REDBOOK"); ;
                    set.SaveSetting("", "MILYEAR", "TWGNENTY", "REDBOOK"); ;
                    if (blnProcessingReg)
                    {
                        set.SaveSetting("Y", "PROCESSREG", "TWGNENTY", "REDBOOK"); ;
                    }
                    else
                    {
                        set.SaveSetting("N", "PROCESSREG", "TWGNENTY", "REDBOOK"); ;
                    }
                }

                // Shell to Redbook and wait for program to terminate used to be here
                if (!modSecurity.ValidPermissions(null, REDBOOK, false))
                {
                    GetRedbookValues = false;
                    frmWait.InstancePtr.Unload();
                    return GetRedbookValues;
                }

                frmWait.InstancePtr.Show();
                frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Loading Blue Book";
                frmWait.InstancePtr.Refresh();
                //App.DoEvents();
                Statics.Wait = 0;
                modBlockEntry.WriteYY();
                App.MainForm.OpenModule("TWRB0000");
                //FC:FINAL:DDU:#i2189 - open & manage  well the modules
                frmWait.InstancePtr.Hide();
                App.MainForm.WaitForRBModule();
                App.MainForm.EndWaitRBModule();
                //FC:FINAL:SBE - #2380 - in original application it is not needed to open again the MV module, and the static variable bolFromWindowsCR is not changed
                //On web version, we need to re-open the MV module, to reload the left navigation menu. Then we have to keep also the value of bolFromWindowsCR static variable
                if (MotorVehicle.Statics.bolFromWindowsCR)
                {
                    modReplaceWorkFiles.EntryFlagFile(true, "CR");
                }
                App.MainForm.OpenModule("TWMV0000", Variables.Statics.MVModuleWait ? true : false);
                if (Statics.Wait != 0)
                {
                    WaitForTerm(ref Statics.Wait);
                }
                modReplaceWorkFiles.EntryFlagFile(true, "  ");
                frmWait.InstancePtr.Unload();
                // Redbook has ended Get Values From Work Record
                GetRedbookValues = true;
                /*? On Error GoTo 0 */
                return GetRedbookValues;
            }
            catch (Exception ex)
            {
            endtag:
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                GetRedbookValues = false;
                frmWait.InstancePtr.Unload();
                Information.Err(ex).Clear();
            }
            return GetRedbookValues;
        }

        public static void ClearVariables()
        {
            Statics.rsFinal.Reset();
            Statics.rsFinalCompare.Reset();
            Statics.rsPlateForReg.Reset();
            Statics.rsSecondPlate.Reset();
            Statics.rsThirdPlate.Reset();
            Statics.rsDoubleTitle.Reset();
            Statics.NewPlate = false;
            Statics.NewToTheSystem = false;
            Statics.blnSuspended = false;
            Statics.blnSR22 = false;
            Statics.orgOwnerInfo.Owner1Name = "";
            Statics.orgOwnerInfo.Owner2Name = "";
            Statics.orgOwnerInfo.Owner3Name = "";
        }

        public static bool CheckNumbers(dynamic frm, string WhichObject, int Month = 0, int Year = 0, int MVR3 = 0)
        {
            bool CheckNumbers = false;
            clsDRWrapper rsYStickers = new clsDRWrapper();
            clsDRWrapper rsMStickers = new clsDRWrapper();
            string QtyM = "";
            string QtyY = "";
            CheckNumbers = false;
            if (Conversion.Val(frm.txtMonthCharge.Text) == 1)
            {
                QtyM = "S";
            }
            else if (Conversion.Val(frm.txtMonthCharge.Text) == 2)
            {
                QtyM = "D";
            }
            if (Conversion.Val(frm.txtMonthNoCharge.Text) == 1)
            {
                QtyM = "S";
            }
            else if (Conversion.Val(frm.txtMonthNoCharge.Text) == 2)
            {
                QtyM = "D";
            }
            if (Conversion.Val(frm.txtMonthCharge.Text) == 1 && Conversion.Val(frm.txtMonthNoCharge.Text) == 1)
                QtyM = "D";
            if (!IsMotorcycle(Statics.Class) || !Statics.FixedMonth)
            {
                if (Conversion.Val(frm.txtYearCharge.Text) == 1)
                {
                    QtyY = "S";
                }
                else if (Conversion.Val(frm.txtYearCharge.Text) == 2)
                {
                    QtyY = "D";
                }
                if (Conversion.Val(frm.txtYearNoCharge.Text) == 1)
                {
                    QtyY = "S";
                }
                else if (Conversion.Val(frm.txtYearNoCharge.Text) == 2)
                {
                    QtyY = "D";
                }
                if (Conversion.Val(frm.txtYearCharge.Text) == 1 && Conversion.Val(frm.txtYearNoCharge.Text) == 1)
                    QtyY = "D";
            }
            else
            {
                QtyY = "C";
            }
            if (WhichObject == "MS")
            {
                Statics.strCodeM = "SM" + QtyM + Strings.Mid(frm.txtExpires.Text, 1, 2);
                if (Statics.StickerGroup == 0)
                {
                    Statics.strSql = "SELECT * FROM Inventory WHERE code = '" + Statics.strCodeM + "' AND Number = " + FCConvert.ToString(Month) + " AND Status = 'A'";
                }
                else
                {
                    Statics.strSql = "SELECT * FROM Inventory WHERE code = '" + Statics.strCodeM + "' AND Number = " + FCConvert.ToString(Month) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(Statics.StickerGroup) + " OR [Group] = 0)";
                }
                rsMStickers.OpenRecordset(Statics.strSql);
                if (rsMStickers.EndOfFile() != true && rsMStickers.BeginningOfFile() != true)
                {
                    rsMStickers.MoveLast();
                    rsMStickers.MoveFirst();
                    CheckNumbers = true;
                }
            }
            else if (WhichObject == "YS")
            {
                Statics.strCodeY = "SY" + QtyY + Strings.Mid(frm.txtExpires.Text, 9, 2);
                if (Statics.StickerGroup == 0)
                {
                    Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + Statics.strCodeY + "' AND Number = " + FCConvert.ToString(Year) + " AND Status = 'A'";
                }
                else
                {
                    Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + Statics.strCodeY + "' AND Number = " + FCConvert.ToString(Year) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(Statics.StickerGroup) + " OR [Group] = 0)";
                }
                rsYStickers.OpenRecordset(Statics.strSql);
                if (rsYStickers.EndOfFile() != true && rsYStickers.BeginningOfFile() != true)
                {
                    rsYStickers.MoveLast();
                    rsYStickers.MoveFirst();
                    CheckNumbers = true;
                }
            }
            return CheckNumbers;
        }

        public static void PrinterError(string strPrinterName)
        {
            string str1;
            // vbPorter upgrade warning: ans As object	OnWrite(DialogResult)
            object ans;
            str1 = strPrinterName;
            if (Information.Err().Number == 24)
            {
                MessageBox.Show("The " + str1 + " has a Device Timeout.  Be sure the Printer is turned on and Online.", "Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else if (Information.Err().Number == 25)
            {
                MessageBox.Show("The " + str1 + " has a Device Fault.  Usually caused by a paper jam or a printer ribbon/cartridge running out of ink or jamming.", "Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else if (Information.Err().Number == 52)
            {
                MessageBox.Show("The " + str1 + " has an invalid Printer File Number.  Please call TRIO.", "Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else if (Information.Err().Number == 53)
            {
                MessageBox.Show("The " + str1 + " has an invalid Printer Port listing on the Printer Setup Screen.  (From Entry Menu Press 9 - 1 And Check " + str1 + ".)", "Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            else if (Information.Err().Number == 70)
            {
                MessageBox.Show("The " + str1 + " has been turned off or is no longer connected to a computer that you have permission to use.  Please reconnect and check the Printer Setup Screen.  (From Entry Menu Press 9 - 1 And Check " + str1 + ".)", "Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else if (Information.Err().Number == 71)
            {
                MessageBox.Show("The " + str1 + " has been turned off or is no longer connected to a computer.  Please reconnect and check the Printer Setup Screen.  (From Entry Menu Press 9 - 1 And Check " + str1 + ".)", "Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else if (Information.Err().Number == 75)
            {
                MessageBox.Show("The " + str1 + " has an invalid Printer Path.  Causes may include printer being turned off, or no longer being connected to the network.  Please check the Printer Setup Screen.  (From Entry Menu Press 9 - 1 And Check " + str1 + ".)", "Printer Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            // 
            ans = FCConvert.ToInt32(MessageBox.Show("There is an error printing to the " + str1 + ".  Please fix printer and press OK to resume or Cancel to Quit.  (" + FCConvert.ToString(Information.Err().Number) + "-" + Information.Err().Description + ")", "Printer Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand));
            // If ans = vbOK Then Resume
        }

        public static void CompareRecordsForECorrect(ref clsDRWrapper rsF, ref clsDRWrapper rsFC, ref clsDRWrapper rsAM)
        {
            // if the fees change then put the new fees in the final and the difference in activity master
            rsAM.Set_Fields("AgentFee", rsF.Get_Fields_Decimal("AgentFee") - rsFC.Get_Fields_Decimal("AgentFee"));
            if (MotorVehicle.Statics.PlateType == 2)
            {
                rsAM.Set_Fields("AgentFee", rsAM.Get_Fields_Decimal("AgentFee") + MotorVehicle.Statics.SpecialtyPlateCorrExtraFee);
            }
            rsAM.Set_Fields("ExciseCreditUsed", rsF.Get_Fields_Decimal("ExciseCreditUsed") - rsFC.Get_Fields_Decimal("ExciseCreditUsed"));
            rsAM.Set_Fields("ExciseTaxCharged", FCConvert.ToDouble(rsF.Get_Fields_Decimal("ExciseTaxCharged")) - Conversion.Val(rsFC.Get_Fields_Decimal("ExciseTaxCharged")));
            rsAM.Set_Fields("ExciseTransferCharge", FCConvert.ToDouble(rsF.Get_Fields_Decimal("ExciseTransferCharge")) - Conversion.Val(rsFC.Get_Fields_Decimal("ExciseTransferCharge")));
            rsAM.Set_Fields("TitleFee", FCConvert.ToDouble(rsF.Get_Fields_Decimal("TitleFee")) - Conversion.Val(rsFC.Get_Fields_Decimal("TitleFee")));
            rsAM.Set_Fields("SalesTax", rsF.Get_Fields_Decimal("SalesTax") - rsFC.Get_Fields_Decimal("SalesTax"));
			rsAM.Set_Fields("SalesTaxExempt", 0);
            rsAM.Set_Fields("ExciseTaxFull", FCConvert.ToDouble(rsF.Get_Fields_Decimal("ExciseTaxFull")) - Conversion.Val(rsFC.Get_Fields_Decimal("ExciseTaxFull")));
            if (Statics.PlateType == 1 && Statics.RegistrationType != "GVW")
            {
                rsAM.Set_Fields("RegRateCharge", rsF.Get_Fields_Decimal("RegRateCharge") - rsFC.Get_Fields_Decimal("RegRateCharge"));
                rsAM.Set_Fields("RegRateFull", rsF.Get_Fields_Decimal("RegRateFull") - rsFC.Get_Fields_Decimal("RegRateFull"));
                rsAM.Set_Fields("TransferCreditFull", rsF.Get_Fields_Decimal("TransferCreditFull") - rsFC.Get_Fields_Decimal("TransferCreditFull"));
                rsAM.Set_Fields("TransferCreditUsed", rsF.Get_Fields_Decimal("TransferCreditUsed") - rsFC.Get_Fields_Decimal("TransferCreditUsed"));
                rsAM.Set_Fields("CommercialCredit", FCConvert.ToDouble(rsF.Get_Fields_Decimal("CommercialCredit")) - Conversion.Val(rsFC.Get_Fields_Decimal("CommercialCredit")));
                rsAM.Set_Fields("InitialFee", FCConvert.ToDouble(rsF.Get_Fields_Decimal("InitialFee")) - Conversion.Val(rsFC.Get_Fields_Decimal("InitialFee")));
                rsAM.Set_Fields("outofrotation", FCConvert.ToDouble(rsF.Get_Fields("outofrotation")) - Conversion.Val(rsFC.Get_Fields("outofrotation")));
                rsAM.Set_Fields("ReservePlate", FCConvert.ToDouble(rsF.Get_Fields_Decimal("ReservePlate")) - Conversion.Val(rsFC.Get_Fields_Decimal("ReservePlate")));
                rsAM.Set_Fields("ReplacementFee", FCConvert.ToDouble(rsF.Get_Fields_Decimal("ReplacementFee")) - Conversion.Val(rsFC.Get_Fields_Decimal("ReplacementFee")));
                rsAM.Set_Fields("StickerFee", FCConvert.ToDouble(rsF.Get_Fields_Decimal("StickerFee")) - Conversion.Val(rsFC.Get_Fields_Decimal("StickerFee")));
                rsAM.Set_Fields("GIFTCERTIFICATEAMOUNT", FCConvert.ToDouble(rsF.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")) - Conversion.Val(rsFC.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")));
                rsAM.Set_Fields("TransferFee", FCConvert.ToDouble(rsF.Get_Fields_Decimal("TransferFee")) - Conversion.Val(rsFC.Get_Fields_Decimal("TransferFee")));
                rsAM.Set_Fields("DuplicateRegistrationFee", Conversion.Val(rsF.Get_Fields_Decimal("DuplicateRegistrationFee")) - Conversion.Val(rsFC.Get_Fields_Decimal("DuplicateRegistrationFee")));
                decimal statePaid = rsAM.Get_Fields_Decimal("SalesTax");
                statePaid += rsAM.Get_Fields_Decimal("TitleFee");
                statePaid += rsAM.Get_Fields_Decimal("RegRateCharge");
                statePaid -= rsAM.Get_Fields_Decimal("CommercialCredit");
                statePaid -= rsAM.Get_Fields_Decimal("TransferCreditUsed");
                statePaid += rsAM.Get_Fields_Decimal("InitialFee");
                statePaid += rsAM.Get_Fields_Decimal("OutOfRotation");
                statePaid += rsAM.Get_Fields_Decimal("ReservePlate");
                statePaid += rsAM.Get_Fields_Decimal("ReplacementFee");
                statePaid += rsAM.Get_Fields_Decimal("StickerFee");
                statePaid += rsAM.Get_Fields_Decimal("TransferFee");
                statePaid += rsAM.Get_Fields_Decimal("DuplicateRegistrationFee");
                rsAM.Set_Fields("StatePaid", statePaid);
            }
			if (rsAM.Get_Fields_String("Class") == rsAM.Get_Fields_String("OldClass"))
            {
                if (Statics.PlateType == 2)
                {
                    rsAM.Set_Fields("StatePaid", rsF.Get_Fields_Decimal("StatePaid") - rsF.Get_Fields_Decimal("PlateFeeNew") - rsF.Get_Fields_Decimal("PlateFeeReReg"));
                }
                if (Statics.PlateType == 1)
                {
                    rsAM.Set_Fields("PlateFeeNew", FCConvert.ToDouble(rsF.Get_Fields_Decimal("PlateFeeNew")) - Conversion.Val(rsFC.Get_Fields_Decimal("PlateFeeNew")));
                    rsAM.Set_Fields("PlateFeeReReg", FCConvert.ToDouble(rsF.Get_Fields_Decimal("PlateFeeReReg")) - Conversion.Val(rsFC.Get_Fields_Decimal("PlateFeeReReg")));
                }
            }
            else
            {
                if (Statics.PlateType == 1)
                {
                    rsAM.Set_Fields("PlateFeeNew", FCConvert.ToDouble(rsF.Get_Fields_Decimal("PlateFeeNew")) - Conversion.Val(rsFC.Get_Fields_Decimal("PlateFeeNew")));
                    rsAM.Set_Fields("PlateFeeReReg", FCConvert.ToDouble(rsF.Get_Fields_Decimal("PlateFeeReReg")) - Conversion.Val(rsFC.Get_Fields_Decimal("PlateFeeReReg")));
                }
            }
            rsAM.Set_Fields("LocalPaid", rsF.Get_Fields_Decimal("LocalPaid") - rsFC.Get_Fields_Decimal("LocalPaid"));
            if (MotorVehicle.Statics.PlateType == 2)
            {
                rsAM.Set_Fields("LocalPaid", rsAM.Get_Fields_Decimal("LocalPaid") + MotorVehicle.Statics.SpecialtyPlateCorrExtraFee);
            }
            if (rsF.Get_Fields_Int32("registeredWeightNew") != 0 && Conversion.Val(rsFC.Get_Fields_Int32("registeredWeightNew")) != 0)
            {
                if (Conversion.Val(rsF.Get_Fields_Int32("registeredWeightNew")) != Conversion.Val(rsFC.Get_Fields_Int32("registeredWeightNew")))
                {
                    rsF.Set_Fields("registeredWeightOld", Conversion.Val(rsFC.Get_Fields_Int32("RegisteredWeightNew")));
                }
            }
            if (rsF.Get_Fields_Int32("YearStickerNumber") == rsFC.Get_Fields_Int32("YearStickerNumber"))
            {
                rsAM.Set_Fields("YearStickerNumber", 0);
            }
            if (rsF.Get_Fields_String("CTANumber").ToUpper() == rsFC.Get_Fields_String("CTANumber").ToUpper())
            {
                rsAM.Set_Fields("CTANumber", "");
            }
            if (rsF.Get_Fields_String("PriorTitleNumber") == rsFC.Get_Fields_String("PriorTitleNumber"))
            {
                rsAM.Set_Fields("PriorTitleNumber", "");
            }
        }

        public static void CalculateECorrectFee(ref DialogResult intCorrSpecialty, ref DialogResult intCorrInitial)
        {
            double totalcredit;
            double ExciseCredit;
            double ExciseCharge;
            double OldCredit = 0;
            double NewCredit = 0;
            totalcredit = 0;
            if (Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged") == 0 && !FCConvert.ToBoolean(Statics.rsFinalCompare.Get_Fields_Boolean("ExciseExempt")))
            {
                Statics.rsFinalCompare.Edit();
                Statics.rsFinalCompare.Set_Fields("ExciseTaxCharged", ExciseTax(FCConvert.ToInt32(Conversion.Val(Statics.rsFinalCompare.Get_Fields_Int32("MillYear"))), FCConvert.ToInt32(Conversion.Val(Statics.rsFinalCompare.Get_Fields_Int32("BasePrice"))), FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Subclass"))));
                Statics.rsFinalCompare.Update();
            }
            if (Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) > Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged")))
            {
                NewCredit = FCConvert.ToDouble(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"));
            }
            else
            {
                NewCredit = FCConvert.ToDouble(Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
            }
            if (Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseCreditUsed")) > Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged")))
            {
                OldCredit = FCConvert.ToDouble(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged"));
            }
            else
            {
                OldCredit = FCConvert.ToDouble(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseCreditUsed"));
            }
            ExciseCredit = (OldCredit - NewCredit);
            ExciseCharge = FCConvert.ToDouble(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged"));
            Statics.ECTotal = FCConvert.ToDecimal(ExciseCharge + ExciseCredit);
            Statics.ECTotal += FCConvert.ToDecimal(FCConvert.ToDouble(Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTransferCharge")));
            Statics.ECTotal += FCConvert.ToDecimal(FCConvert.ToDouble(Statics.rsFinal.Get_Fields_Decimal("TitleFee")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("TitleFee")));
            Statics.ECTotal += FCConvert.ToDecimal(Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinalCompare.Get_Fields_Decimal("SalesTax"));
            if (Statics.PlateType == 2 || Statics.RegistrationType == "GVW")
            {
                Statics.ECTotal += Statics.rsFinal.Get_Fields_Decimal("RegRateCharge");
                totalcredit -= FCConvert.ToDouble(Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"));
                totalcredit -= Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"));
            }
            else
            {
                Statics.ECTotal += (Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") - Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"));
                totalcredit += FCConvert.ToDouble(Statics.rsFinalCompare.Get_Fields_Decimal("TransferCreditUsed") - Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"));
                totalcredit += (Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("CommercialCredit")) - Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")));
            }
            if ((totalcredit * -1) > FCConvert.ToDouble(Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")))
            {
                Statics.ECTotal -= Statics.rsFinal.Get_Fields_Decimal("RegRateCharge");
            }
            else
            {
                Statics.ECTotal -= FCConvert.ToDecimal(totalcredit * -1);
            }
            if (Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("InitialFee")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("InitialFee")) < 0)
            {
                // do nothing
            }
            else
            {
                Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("InitialFee")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("InitialFee")));
            }
            Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("ReservePlate")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("ReservePlate")));
            if (Statics.PlateType == 2)
            {
                if (!Statics.NewPlate)
                {
                    if (Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("PlateFeeNew")) < 0)
                    {
                        // do nothing
                    }
                    else
                    {
                        Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("PlateFeeNew")));
                    }
                }
                else
                {
                    if (intCorrSpecialty == DialogResult.Yes)
                    {
                        Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew")));
                    }
                }
                Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("StickerFee")));
                Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields("outofrotation")));
            }
            else
            {
                Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("PlateFeeNew")));
                Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("TransferFee")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("TransferFee")));
                Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields("outofrotation")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields("outofrotation")));
            }
            if (Statics.rsFinal.Get_Fields_String("Class") == Statics.rsFinal.Get_Fields_String("OldClass") && Statics.PlateType == 1)
            {
                Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("PlateFeeReReg")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("PlateFeeReReg")));
            }
            else
            {
                if (!Statics.NewPlate)
                {
                    if (Statics.PlateType == 2 && Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("PlateFeeReReg")) != 0)
                    {
                        if (Statics.intCorrExtendedSpecialtyPlate == DialogResult.Yes)
                        {
                            Statics.ECTotal += FCConvert.ToDecimal(Statics.rsFinal.Get_Fields_Decimal("PlateFeeReReg"));
                            // kk04172017 tromv-1241  Change this to charge Vanity on Extended Specialty Vanity
                            if (intCorrInitial == DialogResult.Yes)
                            {
                                Statics.ECTotal += FCConvert.ToDecimal(Statics.rsFinal.Get_Fields_Decimal("InitialFee"));
                            }
                        }
                    }
                }
            }
            Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("ReplacementFee")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("ReplacementFee")));
            // Tracker 18476 Took out because it causes errors in fees
            // ECTotal = ECTotal + (Val(Statics.rsFinal.Fields("StickerFee")) - Val(Statics.rsFinalCompare.Fields("StickerFee")))
            Statics.ECTotal -= FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")));
            Statics.ECTotal += FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("AgentFee")) - Conversion.Val(Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee")));
            Statics.ECTotal += Statics.SpecialtyPlateCorrExtraFee;
            if (Statics.ECTotal < 0)
                Statics.ECTotal = 0;
            frmDataInput.InstancePtr.lblTotalDue.Text = "Total Amount Due:  " + Strings.Format(Statics.ECTotal, "#,##0.00");
        }

        public static bool IsSpecialtyPlate(string regClass)
        {
            if (String.IsNullOrWhiteSpace(regClass))
            {
                return false;
            }

            return (regClass == "AG" || regClass == "AC" || regClass == "AF" || regClass == "AW" || regClass == "BB" ||
                    regClass == "BC" || regClass == "BH" || regClass == "CC" || regClass == "CD" || regClass == "CL" ||
                    regClass == "CR" || regClass == "RV" || regClass == "LB" || regClass == "LC" || regClass == "SW" ||
                    regClass == "TS" || regClass == "UM" || regClass == "PH" || regClass == "PM" || regClass == "VT" ||
                    regClass == "DS" || regClass == "WB");
        }

        public static decimal GetSpecialtyPlateRenewalFee(string regClass)
        {
	        decimal specialtyPlateFee = 0;
	        using (clsDRWrapper rsClass = new clsDRWrapper())
	        {
		        rsClass.OpenRecordset("SELECT TOP 1 OtherFeesRenew FROM Class " +
		                              "WHERE BMVCode = '" + regClass + "'");
		        if (!rsClass.EndOfFile())
		        {
			        specialtyPlateFee = rsClass.Get_Fields_Decimal("OtherFeesRenew");
		        }
	        }

	        return specialtyPlateFee;
        }

        // vbPorter upgrade warning: MillYear As object	OnWrite(object, int, double)
        // vbPorter upgrade warning: BasePrice As int	OnWrite(int, double, Decimal, string)
        // vbPorter upgrade warning: 'Return' As Decimal	OnWrite(int, double)
        public static Decimal ExciseTax(int MillYear, int BasePrice, string VehicleClass, string VehicleSubClass)
        {
            Decimal ExciseTax = 0;
            clsDRWrapper rsFees = new clsDRWrapper();
            clsDRWrapper rsClass = new clsDRWrapper();
            rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + VehicleClass + "' AND SystemCode = '" + VehicleSubClass + "'", "TWMV0000.vb1");
            if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
            {
                if (FCConvert.ToString(rsClass.Get_Fields_String("ExciseRequired")) == "N")
                {
                    ExciseTax = 0;
                    return ExciseTax;
                }
            }
            rsFees.OpenRecordset("SELECT * FROM DefaultInfo", "TWMV0000.vb1");
            if ((VehicleClass == "CL" && VehicleSubClass == "C5") || (VehicleClass == "TL" && VehicleSubClass == "L6"))
            {
                if (MillYear == 1)
                {
                    Statics.MillRate = 0.025;
                    ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(BasePrice * Statics.MillRate, 2));
                }
                else if (MillYear == 2)
                {
                    Statics.MillRate = 0.02;
                    ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(BasePrice * Statics.MillRate, 2));
                }
                else if (MillYear == 3)
                {
                    Statics.MillRate = 0.016;
                    ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(BasePrice * Statics.MillRate, 2));
                }
                else if (MillYear == 4)
                {
                    Statics.MillRate = 0.012;
                    ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(BasePrice * Statics.MillRate, 2));
                }
                else if (MillYear == 5)
                {
                    Statics.MillRate = 0.012;
                    ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(BasePrice * Statics.MillRate, 2));
                }
                else if (MillYear == 6)
                {
                    Statics.MillRate = 0.012;
                    ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(BasePrice * Statics.MillRate, 2));
                }
            }
            else
            {
                switch (MillYear)
                {
                    case 1:
                        {
                            Statics.MillRate = 0.024;
                            ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(FCConvert.ToDecimal(Conversion.Val(BasePrice * Statics.MillRate))), 2));
                            break;
                        }
                    case 2:
                        {
                            Statics.MillRate = 0.0175;
                            ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(FCConvert.ToDecimal(Conversion.Val(BasePrice * Statics.MillRate))), 2));
                            break;
                        }
                    case 3:
                        {
                            Statics.MillRate = 0.0135;
                            ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(FCConvert.ToDecimal(Conversion.Val(BasePrice * Statics.MillRate))), 2));
                            break;
                        }
                    case 4:
                        {
                            Statics.MillRate = 0.01;
                            ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(FCConvert.ToDecimal(Conversion.Val(BasePrice * Statics.MillRate))), 2));
                            break;
                        }
                    case 5:
                        {
                            Statics.MillRate = 0.0065;
                            ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(FCConvert.ToDecimal(Conversion.Val(BasePrice * Statics.MillRate))), 2));
                            break;
                        }
                    case 6:
                        {
                            Statics.MillRate = 0.004;
                            ExciseTax = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(FCConvert.ToDecimal(Conversion.Val(BasePrice * Statics.MillRate))), 2));
                            break;
                        }
                }
                //end switch
            }
            if (ExciseTax < rsFees.Get_Fields_Decimal("MinimumExcise"))
                ExciseTax = FCConvert.ToDecimal(rsFees.Get_Fields_Decimal("MinimumExcise"));
            return ExciseTax;
        }
        // vbPorter upgrade warning: frm As Form	OnWrite(frmDataInput)
        // vbPorter upgrade warning: Class As object	OnWrite(string)
        // vbPorter upgrade warning: Subclass As object	OnWrite(string)
        // vbPorter upgrade warning: 'Return' As Decimal	OnWrite
        public static Decimal GetRegRate(dynamic frm, string Class, string Subclass)
        {
            Decimal GetRegRate = 0;
            int WT = 0;
            clsDRWrapper rsClassFee = new clsDRWrapper();
            clsDRWrapper rsGVWFee = new clsDRWrapper();
            GetRegRate = 0;
            if (FCConvert.ToString(Class) != "SE")
            {
                WT = FCConvert.ToInt32(Math.Round(Conversion.Val(frm.txtRegWeight.Text)));
            }
            else
            {
                WT = FCConvert.ToInt32(Math.Round(Conversion.Val(frm.txtNetWeight.Text)));
            }
            Statics.strSql = "SELECT * FROM CLASS WHERE BMVCode = '" + Class + "' And SystemCode =  '" + Subclass + "'";
            rsClassFee.OpenRecordset(Statics.strSql);
            if (rsClassFee.EndOfFile() != true && rsClassFee.BeginningOfFile() != true)
            {
                rsClassFee.MoveLast();
                rsClassFee.MoveFirst();
                if ((Statics.RegistrationType == "RRR") || (Statics.RegistrationType == "RRT") || (Statics.RegistrationType == "NRR") || (Statics.RegistrationType == "NRT") || (Statics.RegistrationType == "GVW") || (Statics.RegistrationType == "CORR"))
                {
                    if (FCUtils.IsNull(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == false)
                    {
                        if (Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.01 || Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.02 || Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.03)
                        {
                            string tempClass = "";
                            tempClass = "FM";
                            if (Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.01)
                            {
                                tempClass = "TK";
                            }
                            else if (Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.03 && WT > 54000 && FCConvert.ToString(Class) != "SE")
                            {
                                tempClass = "F2";
                            }
                            else if (Class == "SE")
                            {
                                tempClass = "SE";
                            }
                            Statics.strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = '" + tempClass + "' AND Low <= " + FCConvert.ToString(WT) + " AND High >= " + FCConvert.ToString(WT);
                            rsGVWFee.OpenRecordset(Statics.strSql);
                            if (rsGVWFee.EndOfFile() != true && rsGVWFee.BeginningOfFile() != true)
                            {
                                rsGVWFee.MoveLast();
                                rsGVWFee.MoveFirst();
                                GetRegRate = FCConvert.ToDecimal(rsGVWFee.Get_Fields_Double("Fee"));
                                if (GetRegRate == 0)
                                    GetRegRate = 35;
                                return GetRegRate;
                            }
                        }
                        GetRegRate = FCConvert.ToDecimal(rsClassFee.Get_Fields_Decimal("RegistrationFee"));
                    }
                    else
                    {
                        GetRegRate = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("There is no class or subclass information for this vehicle, please verify you are entering a valid class / subclass by going into Table Options Processing and selecting the Class Codes tab.  If the code that is in your record is not listed on the Class Codes tab, please change that code to an appropriate, valid code.", "No Class Code Information Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return GetRegRate;
        }
        // vbPorter upgrade warning: 'Return' As Decimal	OnWrite
        public static Decimal GetRegistrationRate(int lngWeight, string Class, string Subclass)
        {
            Decimal GetRegistrationRate = 0;
            int WT;
            clsDRWrapper rsClassFee = new clsDRWrapper();
            clsDRWrapper rsGVWFee = new clsDRWrapper();
            GetRegistrationRate = 0;
            WT = lngWeight;
            Statics.strSql = "SELECT * FROM CLASS WHERE BMVCode = '" + Class + "' And SystemCode =  '" + Subclass + "'";
            rsClassFee.OpenRecordset(Statics.strSql);
            if (rsClassFee.EndOfFile() != true && rsClassFee.BeginningOfFile() != true)
            {
                rsClassFee.MoveLast();
                rsClassFee.MoveFirst();
                if (FCUtils.IsNull(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == false)
                {
                    if (Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.01 || Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.02 || Conversion.Val(rsClassFee.Get_Fields_Decimal("RegistrationFee")) == 0.03)
                    {
                        string tempClass = "";
                        tempClass = "FM";
                        if (rsClassFee.Get_Fields_Decimal("RegistrationFee") == 0.01m)
                        {
                            tempClass = "TK";
                        }
                        else if (rsClassFee.Get_Fields_Decimal("RegistrationFee") == 0.03m && WT > 54000 && FCConvert.ToString(Class) != "SE")
                        {
                            tempClass = "F2";
                        }
                        else if (Class == "SE")
                        {
                            tempClass = "SE";
                        }
                        Statics.strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = '" + tempClass + "' AND Low <= " + FCConvert.ToString(WT) + " AND High >= " + FCConvert.ToString(WT);
                        rsGVWFee.OpenRecordset(Statics.strSql);
                        if (rsGVWFee.EndOfFile() != true && rsGVWFee.BeginningOfFile() != true)
                        {
                            rsGVWFee.MoveLast();
                            rsGVWFee.MoveFirst();
                            GetRegistrationRate = FCConvert.ToDecimal(rsGVWFee.Get_Fields_Double("Fee"));
                            if (GetRegistrationRate == 0)
                                GetRegistrationRate = 35;
                            return GetRegistrationRate;
                        }
                    }
                    GetRegistrationRate = FCConvert.ToDecimal(rsClassFee.Get_Fields_Decimal("RegistrationFee"));
                }
                else
                {
                    GetRegistrationRate = 0;
                }
            }
            else
            {
                MessageBox.Show("There is no class or subclass information for this vehicle, please verify you are entering a valid class / subclass by going into Table Options Processing and selecting the Class Codes tab.  If the code that is in your record is not listed on the Class Codes tab, please change that code to an appropriate, valid code.", "No Class Code Information Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return GetRegistrationRate;
        }
        // vbPorter upgrade warning: frm As Form	OnWrite(frmPreview, frmDataInput)

        public static void ClearAllVariables()
        {
            Statics.HoldRegistrationDollar = false;
            Statics.fnx = 0;
            Statics.bolFromDataInput = false;
            Statics.RRTextAll = "";
            Statics.PP3Text = "";
            Statics.PP2Text = "";
            Statics.PP1Text = "";
            Statics.reason1 = "";
            Statics.reason2 = "";
            Statics.AlignmentOK = "";
            Statics.blnSuspended = false;
            Statics.blnSR22 = false;
            Statics.GrandTotal = 0;
            Statics.RegTypeLost = "";
            Statics.CompareRecord = false;
            Statics.F9 = false;
            Statics.ListRefresh = false;
            Statics.RegistrationType = "";
            Statics.RegType = "";
            Statics.NewToTheSystem = false;
            Statics.PlateType = 0;
            Statics.MonthStickers = 0;
            Statics.YearStickers = 0;
            Statics.MVR3 = 0;
            Statics.pIndex = 0;
            Statics.MillRate = 0;
            Statics.MillYear = 0;
            Statics.FixedMonth = false;
            Statics.gintFixedMonthMonth = 0;
            Statics.MonetaryECorrect = false;
            Statics.SpecialtyPlateCorrExtraFee = 0;
            Statics.ECTotal = 0;
            Statics.RememberedDate1 = "";
            Statics.RememberedDate2 = "";
            Statics.strSql = "";
            Statics.ListType = "";
            Statics.ListSubType = "";
            Statics.Numeric1 = "";
            Statics.Numeric2 = "";
            Statics.Prefix1 = "";
            Statics.Prefix2 = "";
            Statics.Suffix1 = "";
            Statics.Suffix2 = "";
            Statics.LengthOfNumeric = 0;
            Statics.ETO = false;
            Statics.FullExciseCredit = 0;
            Statics.FullExciseTax = 0;
            Statics.FullRegCredit = 0;
            Statics.lngNewID = 0;
            Statics.NewPlate = false;
            Statics.TPlate = 0;
            Statics.SetFinalCompare = false;
            Statics.VehicleInfo = false;
            Statics.DemographicInfo = false;
            Statics.DateInfo = false;
            Statics.FinalRecordIDNumber = 0;
            Statics.RecordIDToTerminate = 0;
            Statics.Plate1 = "";
            Statics.Plate2 = "";
            Statics.Plate3 = "";
            Statics.TempPlateNumber = 0;
            Statics.strCodeM = "";
            Statics.strCodeP = "";
            Statics.strCodeY = "";
            Statics.ExciseAP = false;
            Statics.ForcedPlate = "";
            Statics.ReturnFromListBox = "";
            Statics.Class = "";
            Statics.Subclass = "";
            Statics.Subclass2 = "";
            Statics.RememberedSubclass = "";
            Statics.Class1 = "";
            Statics.Class2 = "";
            Statics.Class3 = "";
            Statics.MVR3saved = false;
            Statics.DataFormLoaded = false;
            Statics.RebuildingLine = false;
            Statics.FromFeesScreen = false;
            Statics.ValuesFromRedbook = false;
            Statics.lngCTAAddNewID = 0;
            Statics.lngUTCAddNewID = 0;
            Statics.NeedToPrintCTA = false;
            Statics.NeedToPrintUseTax = false;
            Statics.ChickConversion = "";
            modGNBas.Statics.Response = "";
            Statics.TwoYear = false;
            //App.DoEvents();
            Statics.InfoForCorrect = false;
            Statics.InfoForReReg = false;
            Statics.InfoForDuplicate = false;
            Statics.TempYear = "";
            // kk07262016  Variables still in memory
            Statics.TempMonth = "";
        }

        public static void FillDemographicInfo(ref clsDRWrapper rsTwo)
        {
            // If .endoffile <> True And .beginningoffile <> True Then .Edit Else .AddNew
            Statics.rsFinal.Set_Fields("OwnerCode1", rsTwo.Get_Fields_String("OwnerCode1"));
            Statics.rsFinal.Set_Fields("OwnerCode2", rsTwo.Get_Fields_String("OwnerCode2"));
            Statics.rsFinal.Set_Fields("OwnerCode3", rsTwo.Get_Fields_String("OwnerCode3"));
            Statics.rsFinal.Set_Fields("LeaseCode1", rsTwo.Get_Fields_String("LeaseCode1"));
            Statics.rsFinal.Set_Fields("LeaseCode2", rsTwo.Get_Fields_String("LeaseCode2"));
            Statics.rsFinal.Set_Fields("LeaseCode3", rsTwo.Get_Fields_String("LeaseCode3"));
            Statics.rsFinal.Set_Fields("PartyID1", rsTwo.Get_Fields_Int32("PartyID1"));
            Statics.rsFinal.Set_Fields("PartyID2", rsTwo.Get_Fields_Int32("PartyID2"));
            Statics.rsFinal.Set_Fields("PartyID3", rsTwo.Get_Fields_Int32("PartyID3"));
            Statics.rsFinal.Set_Fields("PartyName1", rsTwo.Get_Fields_String("PartyName1"));
            // kk07142016 tromvs-57  Need the correct party names to check for name change
            Statics.rsFinal.Set_Fields("PartyName2", rsTwo.Get_Fields_String("PartyName2"));
            Statics.rsFinal.Set_Fields("PartyName3", rsTwo.Get_Fields_String("PartyName3"));
            if (Information.IsDate(rsTwo.Get_Fields("DOB1")))
            {
                Statics.rsFinal.Set_Fields("DOB1", rsTwo.Get_Fields_DateTime("DOB1"));
            }
            else
            {
                Statics.rsFinal.Set_Fields("DOB1", null);
            }
            if (Information.IsDate(rsTwo.Get_Fields("DOB2")))
            {
                Statics.rsFinal.Set_Fields("DOB2", rsTwo.Get_Fields_DateTime("DOB2"));
            }
            else
            {
                Statics.rsFinal.Set_Fields("DOB2", null);
            }
            if (Information.IsDate(rsTwo.Get_Fields("DOB3")))
            {
                Statics.rsFinal.Set_Fields("DOB3", rsTwo.Get_Fields_DateTime("DOB3"));
            }
            else
            {
                Statics.rsFinal.Set_Fields("DOB3", null);
            }
            Statics.rsFinal.Set_Fields("Address", rsTwo.Get_Fields_String("Address"));
            Statics.rsFinal.Set_Fields("Address2", rsTwo.Get_Fields_String("Address2"));
            Statics.rsFinal.Set_Fields("City", rsTwo.Get_Fields_String("City"));
            Statics.rsFinal.Set_Fields("State", rsTwo.Get_Fields_String("State"));
            Statics.rsFinal.Set_Fields("Zip", rsTwo.Get_Fields_String("Zip"));
            Statics.rsFinal.Set_Fields("Country", rsTwo.Get_Fields_String("Country"));
            Statics.rsFinal.Set_Fields("ResidenceCode", rsTwo.Get_Fields_String("ResidenceCode"));
            Statics.rsFinal.Set_Fields("Residence", rsTwo.Get_Fields_String("Residence"));
            Statics.rsFinal.Set_Fields("ResidenceState", rsTwo.Get_Fields_String("ResidenceState"));
            Statics.rsFinal.Set_Fields("ResidenceAddress", rsTwo.Get_Fields_String("ResidenceAddress"));
            Statics.rsFinal.Set_Fields("ResidenceCountry", rsTwo.Get_Fields_String("ResidenceCountry"));
        }
        // vbPorter upgrade warning: frm As Form	OnWrite(frmDataInput)
        public static void GetCommCredit(clsDRWrapper PlateOne, dynamic frm)
        {
            string strTempPlate;
            strTempPlate = Statics.Plate1.Replace("-", "");
            if (Conversion.Val(strTempPlate) > 799999 && Conversion.Val(strTempPlate) < 900000)
            {
                if (Conversion.Val(frm.txtRegWeight.Text) > 23000)
                {
                    if ((Statics.Class == "CO" || Statics.Class == "TT" || Statics.Class == "CC" || Statics.Class == "AP") && Statics.RegistrationType != "GVW")
                    {
                        //FC:FINAL:DDU:#i2172 - fixed error conversing
                        FCCheckBox temp = frm.chkHalfRateState;
                        if (!Statics.gblnAskedCurrentConfig)
                        {
                            Statics.gblnAskedCurrentConfig = true;
                            if (MessageBox.Show("Will this vehicle be used in the current configuration for the whole year?", "Commercial Tractor?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                Statics.gblnCurrentConfig = true;
                                if (temp.CheckState == CheckState.Checked)
                                {
                                    frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "20.00";
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "40.00";
                                }
                            }
                            else
                            {
                                Statics.gblnCurrentConfig = false;
                                frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "0.00";
                            }
                        }
                        else
                        {
                            if (Statics.gblnCurrentConfig == true)
                            {
                                if (temp.CheckState == CheckState.Checked)
                                {
                                    frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "20.00";
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "40.00";
                                }
                            }
                            else
                            {
                                frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "0.00";
                            }
                        }
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "0.00";
                    }
                }
            }
        }
        // vbPorter upgrade warning: Class As object	OnWrite(object, string)
        // vbPorter upgrade warning: 'Return' As Decimal	OnWrite(int, Decimal)
        public static Decimal GetInitialPlateFees(string Class, string strPlate, bool blnTrailerVanity = false, bool blnTwoYear = false, bool blnForce = false, bool blnLongTerm = false)
        {
            Decimal GetInitialPlateFees = 0;
            clsDRWrapper rsFee = new clsDRWrapper();
            // vbPorter upgrade warning: curInitFee As Decimal	OnWrite(Decimal, int)
            Decimal curInitFee;
            int LetterCount = 0;
            // vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
            int counter;
            rsFee.OpenRecordset("SELECT * FROM DefaultInfo");
            if (rsFee.EndOfFile() != true && rsFee.BeginningOfFile() != true)
            {
                curInitFee = FCConvert.ToDecimal(Conversion.Val(rsFee.Get_Fields_Decimal("InitialPlateFee")));
            }
            else
            {
                curInitFee = 0;
            }
            if (curInitFee == 0)
            {
                MessageBox.Show("You need to enter the amount to charge for a vanity plate fee in the Table Maintenance screen.", "No Vanity Plate Fee", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            strPlate = strPlate.Replace("-", "");
            strPlate = strPlate.Replace("&", "");
            strPlate = strPlate.Replace(" ", "");
            GetInitialPlateFees = 0;
            if (strPlate == "NEW")
                return GetInitialPlateFees;
            if (!blnForce)
            {
                if ((Statics.RegistrationType == "NRT" || Statics.RegistrationType == "RRT") && Statics.PlateType == 1)
                    return GetInitialPlateFees;
            }
            if ((Class == "CO") || (Class == "TT"))
            {
                if (strPlate.Length == 7 || Information.IsNumeric(Strings.Mid(strPlate, 1, 1)) == false)
                {
                    GetInitialPlateFees = curInitFee;
                }
            }
            else if ((Class == "CC") || (Class == "CD") || (Class == "CR") || (Class == "UM") || (Class == "PH") || (Class == "PM") || (Class == "DX") || (Class == "DV") || (Class == "RV") || (Class == "WX") || (Class == "MX") || (Class == "VT") || (Class == "XV") || (Class == "VX"))
            {
                if (FCConvert.ToString(Class) != "VT")
                {
                    // And Class <> "VM"
                    if (strPlate.Length < 6 && Information.IsNumeric(Strings.Mid(strPlate, 1, 1)) == false)
                    {
                        GetInitialPlateFees = curInitFee;
                    }
                }
                else
                {
                    if (strPlate.Length <= 6 && Information.IsNumeric(Strings.Mid(strPlate, 1, 1)) == false)
                    {
                        GetInitialPlateFees = curInitFee;
                    }
                }
            }
            else if (Class == "LB")
            {
                if (strPlate.Length != 6 || !Information.IsNumeric(Strings.Left(strPlate, 3)) || Information.IsNumeric(Strings.Mid(strPlate, 4, 3)))
                {
                    GetInitialPlateFees = curInitFee;
                }
            }
            else if (Class == "DS")
            {
                if (!Information.IsNumeric(Strings.Left(strPlate, 1)))
                {
                    GetInitialPlateFees = curInitFee;
                }
            }
            else if ((Class == "AG") || (Class == "AC") || (Class == "AF") || (Class == "TS") || (Class == "SW") || (Class == "BC") || (Class == "AW") || (Class == "LC") || (Class == "EM"))
            {
                // tromv-1377 6.19.18 add EM
                if (!Information.IsNumeric(Strings.Left(strPlate, 1)))
                {
                    GetInitialPlateFees = curInitFee;
                }
            }
            else if (Class == "FM")
            {
                if (!Information.IsNumeric(strPlate))
                {
                    GetInitialPlateFees = curInitFee;
                }
            }
            else if (Class == "TL")
            {
                if (!blnTrailerVanity && !blnLongTerm)
                {
                    for (int i = 0; i < strPlate.Length; i++)
                    {
                        if (Char.IsLetter(strPlate[i]))
                        {
                            if (i > 0 || strPlate.Length == 1)
                            {
                                blnTrailerVanity = true;
                                break;
                            }
                        }
                    }
                }
                if (blnTrailerVanity)
                {
                    if (blnTwoYear)
                    {
                        GetInitialPlateFees = curInitFee * 2;
                    }
                    else
                    {
                        GetInitialPlateFees = curInitFee;
                    }
                }
            }
            else if (Class == "CL")
            {
                if (!blnTrailerVanity && !blnLongTerm)
                {
                    for (int i = 0; i < strPlate.Length; i++)
                    {
                        if (Char.IsLetter(strPlate[i]))
                        {
                            if (i != strPlate.Length - 1 || strPlate.Length == 1 || (strPlate[i] != 'U' && strPlate[i] != 'V'))
                            {
                                blnTrailerVanity = true;
                                break;
                            }
                        }
                    }
                }
                if (blnTrailerVanity)
                {
                    if (blnTwoYear)
                    {
                        GetInitialPlateFees = curInitFee * 2;
                    }
                    else
                    {
                        GetInitialPlateFees = curInitFee;
                    }
                }
            }
            else if (Class == "GS")
            {
                // kk11192015 tromvs-55  Allow vanity GS plate
                if (!Information.IsNumeric(strPlate))
                {
                    GetInitialPlateFees = curInitFee;
                }
            }
            else if ((Class == "AM") || (Class == "BU") || (Class == "HC") || (Class == "PO") || (Class == "PS") || (Class == "TC") || (Class == "IU") || (Class == "LS") || (Class == "AU") || (Class == "CS") || (Class == "ST"))
            {
                GetInitialPlateFees = 0;
            }
            else
            {
                if ((Strings.Mid(Strings.Trim(strPlate), 1, 1) != "0" && Conversion.Val(Strings.Mid(Strings.Trim(strPlate), 1, 1)) == 0) || Strings.Trim(strPlate).Length == 7)
                {
                    GetInitialPlateFees = curInitFee;
                }
            }
            return GetInitialPlateFees;
        }

        public static Decimal GetInitialPlateFees2(string Class, string strPlate, bool blnTrailerVanity = false, bool blnTwoYear = false, bool blnForce = false, bool blnLongTerm = false)
        {
            return GetInitialPlateFees(Class, strPlate, blnTrailerVanity, blnTwoYear, blnForce, blnLongTerm);
        }
        // vbPorter upgrade warning: 'Return' As Decimal	OnWrite
        public static Decimal GetOutOfRotation(string Class, string Subclass)
        {
            Decimal curOutOfRotationFee = 25;
            clsDRWrapper rsDefaults = new clsDRWrapper();
            rsDefaults.OpenRecordset("SELECT OutOfRotation FROM DefaultInfo");
            if (!rsDefaults.EndOfFile())
            {
                curOutOfRotationFee = FCConvert.ToDecimal(Conversion.Val(rsDefaults.Get_Fields_Decimal("OutOfRotation")));
            }
            Decimal GetOutOfRotation = 0;
            GetOutOfRotation = 0;
            if (Statics.Plate1 == "NEW" || Statics.NewPlate == false || Statics.ForcedPlate == "S")
                return GetOutOfRotation;
            string xx = "";
            if (Class == "PC")
            {
                if (Conversion.Val(Statics.Plate1) > 0 && Conversion.Val(Statics.Plate1) < 10000)
                {
                    if (Information.IsNumeric(Strings.Right(Statics.Plate1, 1)) == true)
                    {
                        GetOutOfRotation = curOutOfRotationFee;
                        return GetOutOfRotation;
                    }
                }
                if (Statics.Plate1.Length == 3 && Information.IsNumeric(Strings.Left(Statics.Plate1, 2)) && !Information.IsNumeric(Strings.Right(Statics.Plate1, 1)))
                {
                    GetOutOfRotation = curOutOfRotationFee;
                }
                if (Statics.Plate1.Length == 4 && Information.IsNumeric(Strings.Left(Statics.Plate1, 2)) && Conversion.Val(Strings.Left(Statics.Plate1, 2)) > 0 && Conversion.Val(Strings.Left(Statics.Plate1, 2)) < 26)
                {
                    xx = Strings.Right(Statics.Plate1, 3);
                    if (Information.IsNumeric(Strings.Mid(xx, 2, 1)) == false && Information.IsNumeric(Strings.Mid(xx, 3, 1)) == false)
                    {
                        if (Information.IsNumeric(Strings.Mid(xx, 1, 1)) == true)
                            GetOutOfRotation = curOutOfRotationFee;
                    }
                }
            }
            else if (Class == "MC")
            {
                if (Statics.Plate1.Length < 3)
                {
                    if (Conversion.Val(Statics.Plate1) > 0 && Conversion.Val(Statics.Plate1) < 100 && Information.IsNumeric(Strings.Right(Statics.Plate1, 1)) == true)
                    {
                        GetOutOfRotation = curOutOfRotationFee;
                    }
                }
            }
            else if ((Class == "CO") || (Class == "TT"))
            {
                if (Conversion.Val(Statics.Plate1) > 0 && Conversion.Val(Statics.Plate1) < 10000 && Information.IsNumeric(Strings.Right(Statics.Plate1, 1)) == true)
                {
                    GetOutOfRotation = curOutOfRotationFee;
                }
            }
            else if (Class == "SW")
            {
                if (Information.IsNumeric(Statics.Plate1) && Statics.Plate1.Length <= 6)
                {
                    GetOutOfRotation = curOutOfRotationFee;
                }
            }
            return GetOutOfRotation;
        }

        public static bool GetLowDigit()
        {
            bool GetLowDigit = false;
            GetLowDigit = false;
            if (Conversion.Val(Statics.Plate1) > 0 && Conversion.Val(Statics.Plate1) < 100 && Strings.Trim(Statics.Plate1).Length < 3 && Statics.Class == "PC")
            {
                if (Information.IsNumeric(Strings.Right(Statics.Plate1, 1)) == true)
                {
                    GetLowDigit = true;
                }
            }
            return GetLowDigit;
        }

        public static string GetSpecialtyPlate(string strClass)
        {
            string GetSpecialtyPlate = "";
            if ((strClass == "CC") || (strClass == "CD") || (strClass == "CL") || (strClass == "CR") || (strClass == "RV"))
            {
                GetSpecialtyPlate = "CR";
            }
            else if (strClass == "BB")
            {
                GetSpecialtyPlate = "BB";
            }
            else if (strClass == "BH")
            {
                GetSpecialtyPlate = "BH";
            }
            else if (strClass == "UM")
            {
                GetSpecialtyPlate = "UM";
            }
            else if (strClass == "LB")
            {
                GetSpecialtyPlate = "LB";
            }
            else if (strClass == "LC")
            {
                GetSpecialtyPlate = "LC";
            }
            else if (strClass == "AG")
            {
                GetSpecialtyPlate = "AG";
            }
            else if (strClass == "AC")
            {
                GetSpecialtyPlate = "AC";
            }
            else if (strClass == "AF")
            {
                GetSpecialtyPlate = "AF";
            }
            else if (strClass == "TS")
            {
                GetSpecialtyPlate = "TS";
            }
            else if (strClass == "SW")
            {
                GetSpecialtyPlate = "SW";
            }
            else if (strClass == "BC")
            {
                GetSpecialtyPlate = "BC";
            }
            else if (strClass == "AW")
            {
                GetSpecialtyPlate = "AW";
            }
            else
            {
                GetSpecialtyPlate = "";
            }
            return GetSpecialtyPlate;
        }
        // vbPorter upgrade warning: frm As Form	OnWrite(frmDataInput)
        public static bool GetTransferCredit(dynamic frm)
        {
            bool GetTransferCredit = false;
            clsDRWrapper rsFees = new clsDRWrapper();
            clsDRWrapper rsClass = new clsDRWrapper();
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            // vbPorter upgrade warning: curStateCreditAmount As Decimal	OnWrite(double, Decimal)
            Decimal curStateCreditAmount;
            // vbPorter upgrade warning: curExcise As Decimal	OnWriteFCConvert.ToDouble(
            Decimal curExcise;
            rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
            rsFees.MoveLast();
            rsFees.MoveFirst();
            Information.Err().Clear();
            GetTransferCredit = false;
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf"))
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwoYear"))
                {
                    curStateCreditAmount = FCConvert.ToDecimal((Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") / 3) * 2));
                }
                else
                {
                    curStateCreditAmount = FCConvert.ToDecimal((Conversion.Val(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"))) * 0.5));
                }
            }
            else
            {
                curStateCreditAmount = (FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") - FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"))))));
            }
            if (!Statics.StateHalfRate)
            {
                if (Statics.RegistrationType == "CORR")
                {
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("TransferCreditFull"))), "#,##0.00");
                }
                else
                {
                    if (FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) == 0)
                    {
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(curStateCreditAmount, "#,##0.00");
                        // frmFeesInfo.txtFEECreditTransfer.Text = Format(Val(rsCredit.Fields("RegRateFull")) - Val(rsCredit.Fields("CommercialCredit")), "#,##0.00")
                    }
                    else
                    {
                        if (FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) != (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")) && FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) != ((MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")) * 0.5m))
                        {
                            ans = MessageBox.Show("The Registration Credit is different than the one calculated.  Do you wish to keep this Registration Credit?" + "\r\n" + "\r\n" + "Current Fee:      " + frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text + "\r\n" + "Calculated Fee: " + Strings.Format(Math.Round(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"))) - Math.Round(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"))), "#,##0.00"), "Keep Registration Credit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                // do nothing
                            }
                            else
                            {
                                // frmFeesInfo.txtFEECreditTransfer.Text = Format(Val(rsCredit.Fields("RegRateFull")) - Val(rsCredit.Fields("CommercialCredit")), "#,##0.00")
                                frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(curStateCreditAmount, "#,##0.00");
                            }
                        }
                        else
                        {
                            // frmFeesInfo.txtFEECreditTransfer.Text = Format(Val(rsCredit.Fields("RegRateFull")) - Val(rsCredit.Fields("CommercialCredit")), "#,##0.00")
                            frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(curStateCreditAmount, "#,##0.00");
                        }
                    }
                }
            }
            else
            {
                if (Statics.RegistrationType == "CORR")
                {
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("TransferCreditFull")) * 0.5)), "#,##0.00");
                }
                else
                {
                    if (FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) == 0)
                    {
                        // If rsCredit.Fields("TwoYear") Then
                        // If rsCredit.Fields("RegHalf") Then
                        // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") / 3) * 2), "#,##0.00")
                        // Else
                        // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") - Val(rsCredit.Fields("CommercialCredit"))) * 0.5), "#,##0.00")
                        // End If
                        // Else
                        // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") - Val(rsCredit.Fields("CommercialCredit"))) * 0.5), "#,##0.00")
                        // End If
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(curStateCreditAmount * FCConvert.ToDecimal(0.5), "#,##0.00");
                    }
                    else
                    {
                        if (FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) != FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"))) && FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) != FCConvert.ToDecimal((Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"))) * 0.5))
                        {
                            ans = MessageBox.Show("The Registration Credit is different than the one calculated.  Do you wish to keep this Registration Credit?" + "\r\n" + "\r\n" + "Current Fee:      " + frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text + "\r\n" + "Calculated Fee: " + Strings.Format(Math.Round(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"))) - Math.Round(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"))), "#,##0.00"), "Keep Registration Credit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                // do nothing
                            }
                            else
                            {
                                // If rsCredit.Fields("TwoYear") Then
                                // If rsCredit.Fields("RegHalf") Then
                                // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") / 3) * 2), "#,##0.00")
                                // Else
                                // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") - Val(rsCredit.Fields("CommercialCredit"))) * 0.5), "#,##0.00")
                                // End If
                                // Else
                                // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") - Val(rsCredit.Fields("CommercialCredit"))) * 0.5), "#,##0.00")
                                // End If
                                frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(curStateCreditAmount * FCConvert.ToDecimal(0.5), "#,##0.00");
                            }
                        }
                        else
                        {
                            // If rsCredit.Fields("TwoYear") Then
                            // If rsCredit.Fields("RegHalf") Then
                            // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") / 3) * 2), "#,##0.00")
                            // Else
                            // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") - Val(rsCredit.Fields("CommercialCredit"))) * 0.5), "#,##0.00")
                            // End If
                            // Else
                            // frmFeesInfo.txtFEECreditTransfer.Text = Format((Val(rsCredit.Fields("RegRateFull") - Val(rsCredit.Fields("CommercialCredit"))) * 0.5), "#,##0.00")
                            // End If
                            frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(curStateCreditAmount * FCConvert.ToDecimal(0.5), "#,##0.00");
                        }
                    }
                }
            }
            if (Statics.Class == "CI" || Statics.Class == "DV")
            {
                // do nothing
            }
            else
            {
                // kk07212016   $5 transfer when vehicle is Stock Car(PC-P1), Dune Buggy(PC-P4), Moped(MP)
                // or a Trailer with a 10.50 reg fee (TL-L1,L2,L6,L8 and CL-C1,C2,C5,C7)
                if (Statics.Class == "PC" && (Statics.Subclass == "P1" || Statics.Subclass == "P4") || Statics.Class == "MP")
                {
                    frmFeesInfo.InstancePtr.txtFEETransferFee.Text = Strings.Format(5, "#,##0.00");
                }
                else
                {
                    rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" + Statics.rsFinal.Get_Fields_String("Subclass") + "'");
                    if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
                    {
                        if (rsClass.Get_Fields_Decimal("RegistrationFee") != 0)
                        {
                            // kk07212016  Clean up dup code
                            // If ((Class = "TL" Or Class = "CL") And (Subclass = "L1" Or Subclass = "L2" Or Subclass = "C1" Or Subclass = "C2")) Or Class = "MP" Or (Class = "TR" And Subclass = "T5") Then
                            // frmFeesInfo.txtFEETransferFee.Text = Format(5, "#,##0.00")
                            // Else
                            if (rsClass.Get_Fields_Decimal("RegistrationFee") > 0.1m && rsClass.Get_Fields_Decimal("RegistrationFee") < rsFees.Get_Fields_Decimal("StateTransferFee"))
                            {
                                frmFeesInfo.InstancePtr.txtFEETransferFee.Text = Strings.Format(rsClass.Get_Fields_Decimal("RegistrationFee"), "#,##0.00");
                            }
                            else
                            {
                                frmFeesInfo.InstancePtr.txtFEETransferFee.Text = Strings.Format(rsFees.Get_Fields_Decimal("StateTransferFee"), "#,##0.00");
                            }
                            // End If
                        }
                        else
                        {
                            frmFeesInfo.InstancePtr.txtFEETransferFee.Text = Strings.Format(0, "#,##0.00");
                        }
                    }
                    else
                    {
                        // kk07212016  Clean up dup code
                        // If ((Class = "TL" Or Class = "CL") And (Subclass = "L1" Or Subclass = "L2" Or Subclass = "C1" Or Subclass = "C2")) Or Class = "MP" Or (Class = "TR" And Subclass = "T5") Then
                        // frmFeesInfo.txtFEETransferFee.Text = Format(5, "#,##0.00")
                        // Else
                        frmFeesInfo.InstancePtr.txtFEETransferFee.Text = Strings.Format(rsFees.Get_Fields_Decimal("StateTransferFee"), "#,##0.00");
                        // End If
                    }
                }
            }
            // Matthew *****
            // original
            // If rsCredit.fields("ExciseTaxFull")  >= rsCredit.fields("ExciseCreditFull")   Then
            // **************
            if (Strings.Trim(frm.txtTownCredit.Text) == "" || FCConvert.ToDecimal(frm.txtTownCredit.Text) == 0)
            {
                if (!Statics.LocalHalfRate)
                {
                    // kk07112016  Sync'd this with the Access version, but it doesn't make sense. Setting frm.txtTownCredit.Text to curExcise and then overwriting?  Need to look into this
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate") && IsMotorcycle(Statics.rsFinal.Get_Fields_String("Class")))
                    {
                        curExcise = FCConvert.ToDecimal(modGNBas.Round_8((Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) / (DateAndTime.DateDiff("m", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"), MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")) + 1)) * 12, 2));
                        // curExcise = Round(ProRate(curExcise, intMonths), 2)
                        frm.txtTownCredit.Text = Strings.Format(curExcise, "#,##0.00");
                        frm.txtTownCredit.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"), "#,##0.00");
                    }
                    else
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) >= Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull") + " "))
                        {
                            frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")), "#,##0.00");
                        }
                        else
                        {
                            // kk04132017 tromv-1116  Add check Excise Tax Rebate program
                            if (Statics.blnAllowExciseRebates)
                            {
                                if (DialogResult.Yes == MessageBox.Show("Was a rebate given for the previous excise credit?", "Excise Rebate", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                {
                                    frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")), "#,##0.00");
                                }
                                else
                                {
                                    frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")), "#,##0.00");
                                }
                            }
                            else
                            {
                                frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")), "#,##0.00");
                            }
                        }
                    }
                }
                else
                {
                    if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) >= Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull") + " "))
                    {
                        frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull") * 0.5m), "#,##0.00");
                    }
                    else
                    {
                        // kk04132017 tromv-1116  Add check Excise Tax Rebate program
                        if (Statics.blnAllowExciseRebates)
                        {
                            if (DialogResult.Yes == MessageBox.Show("Was a rebate given for the previous excise credit?", "Excise Rebate", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull") * 0.5m), "#,##0.00");
                                // Should we prompt to confirm?
                            }
                            else
                            {
                                frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull") * 0.5m), "#,##0.00");
                            }
                        }
                        else
                        {
                            frm.txtTownCredit.Text = Strings.Format(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull") * 0.5m), "#,##0.00");
                        }
                    }
                }
            }
            // matthew *******
            // origiinal
            frm.txtCreditNumber.Text = Statics.rsFinal.Get_Fields_String("ExciseCreditMVR3");
            // frm.txtCreditNumber.Text = rsCredit.fields("MVR3
            // ****************
            if (Information.Err().Number == 0)
                GetTransferCredit = true;
            return GetTransferCredit;
        }

        public static Decimal GetFullStateTransferCredit()
        {
            Decimal GetFullStateTransferCredit = 0;
            // vbPorter upgrade warning: curStateCreditAmount As Decimal	OnWrite(Decimal, double, int)
            Decimal curStateCreditAmount;
            if (Statics.RegistrationType == "CORR")
            {
                curStateCreditAmount = FCConvert.ToDecimal(Conversion.Val(Statics.rsFinal.Get_Fields_Decimal("TransferCreditFull")));
            }
            else if (Statics.RegistrationType == "GVW")
            {
                curStateCreditAmount = FCConvert.ToDecimal(Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"));
            }
            else
            {
                if (Statics.RegistrationType == "NRT" || Statics.RegistrationType == "RRT")
                {
                    if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("RegHalf"))
                    {
                        if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("TwoYear"))
                        {
                            curStateCreditAmount = FCConvert.ToDecimal((Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull") / 3) * 2));
                        }
                        else
                        {
                            curStateCreditAmount = FCConvert.ToDecimal((Conversion.Val(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("CommercialCredit"))) * 0.5));
                        }
                    }
                    else
                    {
                        curStateCreditAmount = (FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull") - FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("CommercialCredit"))))));
                    }
                }
                else
                {
                    curStateCreditAmount = 0;
                }
            }
            GetFullStateTransferCredit = curStateCreditAmount;
            return GetFullStateTransferCredit;
        }
        // vbPorter upgrade warning: 'Return' As Decimal	OnWrite(double, Decimal, int)
        public static Decimal GetFullExciseTransferCredit()
        {
            Decimal GetFullExciseTransferCredit = 0;
            if (Statics.RegistrationType == "NRT" || Statics.RegistrationType == "RRT")
            {
                if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxFull")) >= Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseCreditFull") + " "))
                {
                    if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("ExciseProrate"))
                    {
                        // DJW@05162013 TROMV-751 ChNGED 3/31/2013 TO BE THE ACTUAL EXPIRATION DAte bacause the fixed date caused negative credits if effective date was later than fixed date
                        GetFullExciseTransferCredit = FCConvert.ToDecimal(modGNBas.Round_8((Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxFull")) / (DateAndTime.DateDiff("m", MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("EffectiveDate"), MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate")) + 1)) * 12, 2));
                    }
                    else
                    {
                        GetFullExciseTransferCredit = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxFull")));
                    }
                }
                else
                {
                    GetFullExciseTransferCredit = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseCreditFull")));
                }
            }
            else
            {
                GetFullExciseTransferCredit = 0;
            }
            return GetFullExciseTransferCredit;
        }

        public static string ReturnYearStickerNumber()
        {
            int xx = FCConvert.ToInt16(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge")));
            int YY = FCConvert.ToInt16(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge")));
            string returnVal = "";

            if (MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber"))
                {
                    if (YY == 2)
                    {
                        returnVal = MotorVehicle.Statics.TempYear + "D " + FCConvert.ToString(MotorVehicle.strPadZeros(Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"))), 8));
                    }
                    else if (YY == 1)
                    {
                        returnVal = MotorVehicle.Statics.TempYear + "S " + FCConvert.ToString(MotorVehicle.strPadZeros(Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"))), 8));
                    }
                }

                if (returnVal != "" && MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) && MotorVehicle.Statics.FixedMonth)
                {
                    returnVal = MotorVehicle.Statics.TempYear + "C " + FCConvert.ToString(MotorVehicle.strPadZeros(Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"))), 8));
                }
            }
            else if (MotorVehicle.Statics.RegistrationType != "DUPREG")
            {
                if (!MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) || !MotorVehicle.Statics.FixedMonth)
                {
                    if (YY == 1)
                    {
                        returnVal = frmDataInput.InstancePtr.lblNumberofYear.Text + "S " + FCConvert.ToString(modGlobalRoutines.PadToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"), 8));
                    }
                    else if (YY == 2)
                    {
                        returnVal = frmDataInput.InstancePtr.lblNumberofYear.Text + "D " + FCConvert.ToString(modGlobalRoutines.PadToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"), 8));
                    }
                }
                else
                {
                    if (YY > 0)
                    {
                        returnVal = frmDataInput.InstancePtr.lblNumberofYear.Text + "C " + FCConvert.ToString(modGlobalRoutines.PadToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"), 8));
                    }
                }
            }

            return returnVal;
        }

        public static string ReturnMonthStickerNumber()
        {
            int xx = FCConvert.ToInt16(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge")));
            int YY = FCConvert.ToInt16(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge")));
            string returnVal = "";

            if (MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber"))
                {
                    if (xx == 2)
                    {
                        returnVal = MotorVehicle.Statics.TempMonth + "D " + FCConvert.ToString(MotorVehicle.strPadZeros(Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"))), 8));
                    }
                    else if (xx == 1)
                    {
                        returnVal = MotorVehicle.Statics.TempMonth + "S " + FCConvert.ToString(MotorVehicle.strPadZeros(Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"))), 8));
                    }
                }
            }
            else if (MotorVehicle.Statics.RegistrationType != "DUPREG")
            {
                if (xx == 1)
                {
                    return frmDataInput.InstancePtr.lblNumberofMonth.Text + "S " + FCConvert.ToString(modGlobalRoutines.PadToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"), 8));
                }
                else if (xx == 2)
                {
                    return frmDataInput.InstancePtr.lblNumberofMonth.Text + "D " + FCConvert.ToString(modGlobalRoutines.PadToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"), 8));
                }
            }

            return returnVal;

        }

        public static void Write_PDS_Work_Record_Stuff()
        {
            string strRec3 = new string('\0', 241);
            string strRec5A = new string('\0', 241);
            clsDRWrapper rsCheck = new clsDRWrapper();
            bool AddToExcise = false;
            string strM = "";
            string strY = "";
            bool blnTest;
            clsDRWrapper rsDefaultInfo = new clsDRWrapper();
            clsDRWrapper rsResCheck = new clsDRWrapper();
            // vbPorter upgrade warning: curCorrAmount As Decimal	OnWrite(Decimal, int)
            Decimal curCorrAmount = 0;
            Decimal ExciseCredit = 0;
            // vbPorter upgrade warning: ExciseCharge As Decimal	OnWriteFCConvert.ToDouble(
            Decimal ExciseCharge = 0;
            Decimal OldCredit = 0;
            Decimal NewCredit = 0;
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
                blnTest = modRegistry.GetKeyValues2(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Test", "");
                rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
                if (FCConvert.ToString(rsCheck.Get_Fields_String("ExciseVSAgent")) == "A")
                {
                    AddToExcise = false;
                    /*? 6 */
                }
                else
                {
                    AddToExcise = true;
                    /*? 8 */
                }
                if (Statics.bolFromWindowsCR)
                {
                    if (modRegionalTown.IsRegionalTown())
                    {
                        if (Statics.RegistrationType == "TRANSIT" || Statics.RegistrationType == "SPREG")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                            /*? 13 */
                        }
                        else if (Statics.RegistrationType == "BOOST")
                        {
                            rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Strings.Format(Statics.rsPlateForReg.Get_Fields_String("ResidenceCode"), "00000") + "'", "CentralData");
                            if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsResCheck.Get_Fields_Int32("TownNumber")) == 0)
                                {
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                                }
                                else
                                {
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(rsResCheck.Get_Fields_Int32("TownNumber")));
                                }
                                /*? 17 */
                            }
                            else
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                                /*? 19 */
                            }
                            /*? 20 */
                        }
                        else
                        {
                            rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Strings.Format(Statics.rsFinal.Get_Fields_String("ResidenceCode"), "00000") + "'", "CentralData");
                            if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsResCheck.Get_Fields_Int32("TownNumber")) == 0)
                                {
                                    MessageBox.Show("Residence Code: " + Strings.Format(Statics.rsFinal.Get_Fields_String("ResidenceCode"), "00000") + " not found", "Invalid Region", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                                }
                                else
                                {
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(rsResCheck.Get_Fields_Int32("TownNumber")));
                                }
                                /*? 24 */
                            }
                            else
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                                /*? 26 */
                            }
                            /*? 27 */
                        }
                        /*? 28 */
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                        /*? 30 */
                    }
                    // GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CR\MV\", "ResCase", strM
                    // MsgBox strM
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
                    if (Statics.RegistrationType == "DUPREG" || Statics.RegistrationType == "LOST" || Statics.RegistrationType == "DUPSTK" || Statics.RegistrationType == "TRANSIT" || Statics.RegistrationType == "BOOST" || Statics.RegistrationType == "SPREG" || Statics.RegistrationType == "GVW")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
                        /*? 34 */
                    }
                    else if (Statics.RegistrationType == "CORR")
                    {
                        if (Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") > Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"))
                        {
                            NewCredit = FCConvert.ToDecimal(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"));
                            /*? 37 */
                        }
                        else
                        {
                            NewCredit = FCConvert.ToDecimal(Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
                            /*? 39 */
                        }
                        if (Statics.rsFinalCompare.Get_Fields_Decimal("ExciseCreditUsed") > Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged"))
                        {
                            OldCredit = FCConvert.ToDecimal(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged"));
                            /*? 42 */
                        }
                        else
                        {
                            OldCredit = FCConvert.ToDecimal(Statics.rsFinalCompare.Get_Fields_Decimal("ExciseCreditUsed"));
                            /*? 44 */
                        }
                        ExciseCredit = (OldCredit - NewCredit);
                        ExciseCharge = FCConvert.ToDecimal((Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged")));
                        curCorrAmount = ExciseCharge + ExciseCredit;
                        if (curCorrAmount < 0)
                            curCorrAmount = 0;
                        if (modRegionalTown.IsRegionalTown())
                        {
                            rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Statics.rsFinal.Get_Fields_String("ResidenceCode") + "'", "CentralData");
                            if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(curCorrAmount, "0.00"));
                                /*? 53 */
                            }
                            else
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format(curCorrAmount, "0.00"));
                                /*? 55 */
                            }
                            /*? 56 */
                        }
                        else
                        {
                            if (Statics.rsFinal.Get_Fields_String("ResidenceCode") != rsDefaultInfo.Get_Fields_String("ResidenceCode"))
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format(curCorrAmount, "0.00"));
                                /*? 59 */
                            }
                            else
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(curCorrAmount, "0.00"));
                                /*? 61 */
                            }
                            /*? 62 */
                        }
                        /*? 63 */
                    }
                    else
                    {
                        if (Statics.ExciseAP)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
                            /*? 66 */
                        }
                        else
                        {
                            if (AddToExcise)
                            {
                                if (Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") < 0)
                                {
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0.00"));
                                    /*? 70 */
                                }
                                else
                                {
                                    if (modRegionalTown.IsRegionalTown())
                                    {
                                        rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Statics.rsFinal.Get_Fields_String("ResidenceCode") + "'", "CentralData");
                                        if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0.00"));
                                            /*? 75 */
                                        }
                                        else
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0.00"));
                                            /*? 77 */
                                        }
                                        /*? 78 */
                                    }
                                    else
                                    {
                                        if (Statics.rsFinal.Get_Fields_String("ResidenceCode") != rsDefaultInfo.Get_Fields_String("ResidenceCode"))
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0.00"));
                                            /*? 81 */
                                        }
                                        else
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0.00"));
                                            /*? 83 */
                                        }
                                        /*? 84 */
                                    }
                                    /*? 85 */
                                }
                                /*? 86 */
                            }
                            else
                            {
                                if (Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") < 0)
                                {
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
                                    /*? 89 */
                                }
                                else
                                {
                                    /*? 90 */
                                    if (modRegionalTown.IsRegionalTown())
                                    {
                                        rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Statics.rsFinal.Get_Fields_String("ResidenceCode") + "'", "CentralData");
                                        if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "0.00"));
                                            /*? 95 */
                                        }
                                        else
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "0.00"));
                                            /*? 97 */
                                        }
                                        /*? 98 */
                                    }
                                    else
                                    {
                                        if (Statics.rsFinal.Get_Fields_String("ResidenceCode") != rsDefaultInfo.Get_Fields_String("ResidenceCode"))
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "0.00"));
                                            /*? 101 */
                                        }
                                        else
                                        {
                                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "0.00"));
                                            /*? 103 */
                                        }
                                        /*? 104 */
                                    }
                                    /*? 105 */
                                }
                                /*? 106 */
                            }
                            /*? 107 */
                        }
                        /*? 108 */
                    }
                    if (Statics.RegistrationType == "DUPREG")
                    {
                        if (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) == "CI" || FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) == "DV" || FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) == "PH")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(0, "0.00"));
                            /*? 112 */
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DuplicateRegistration"), "0.00"));
                            // kk01082016 tromv-1125    Format(2, "0.00")
                            /*? 114 */
                        }
                        /*? 115 */
                    }
                    else if (Statics.RegistrationType == "DUPSTK")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("StickerFee"), "0.00"));
                        /*? 117 */
                    }
                    else if (Statics.RegistrationType == "CORR")
                    {
                        if (Statics.ECTotal < 0)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(0, "0.00"));
                            /*? 120 */
                        }
                        else
                        {
                            if (Statics.rsFinal.Get_Fields_Decimal("AgentFee") > Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"))
                            {
                                curCorrAmount = Statics.ECTotal - curCorrAmount - (Statics.rsFinal.Get_Fields_Decimal("AgentFee") - Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"));
                                /*? 123 */
                            }
                            else if (Statics.PlateType == 2)
                            {
                                curCorrAmount = Statics.ECTotal - curCorrAmount - MotorVehicle.Statics.SpecialtyPlateCorrExtraFee;
                            }
                            else
                            {
                                curCorrAmount = Statics.ECTotal - curCorrAmount;
                                /*? 125 */
                            }
                            if (Statics.rsFinal.Get_Fields_Decimal("TitleFee") > Statics.rsFinalCompare.Get_Fields_Decimal("TitleFee"))
                            {
                                curCorrAmount -= (Statics.rsFinal.Get_Fields_Decimal("TitleFee") - Statics.rsFinalCompare.Get_Fields_Decimal("TitleFee"));
                            }
                            if (Statics.rsFinal.Get_Fields_Decimal("SalesTax") > Statics.rsFinalCompare.Get_Fields_Decimal("SalesTax"))
                            {
                                curCorrAmount -= (Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinalCompare.Get_Fields_Decimal("SalesTax"));
                            }
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(curCorrAmount, "0.00"));
                            /*? 126 */
                        }
                        /*? 127 */
                    }
                    else if (Statics.RegistrationType == "LOST")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ReplacementFee") + Statics.rsFinal.Get_Fields_Decimal("StickerFee")), "0.00"));
                        /*? 129 */
                    }
                    else if (Statics.RegistrationType == "TRANSIT")
                    {
                        if (frmTransitPlate.InstancePtr.chkMunicipalVehicle.CheckState == CheckState.Unchecked)
                        {
                            if (frmTransitPlate.InstancePtr.chkRoundTrip.CheckState == CheckState.Unchecked)
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("TransitPlateFee"), "0.00"));
                            }
                            else
                            {
                                if (Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("TransitPlateRoundTrip") + "") == 0)
                                {
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(25, "0.00"));
                                }
                                else
                                {
                                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("TransitPlateRoundTrip"), "0.00"));
                                }
                            }
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", "0.00");
                        }
                        /*? 131 */
                    }
                    else if (Statics.RegistrationType == "BOOST")
                    {
                        if (Statics.PlateType == 1)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(frmBoosterPlate.InstancePtr.curStatePaid, "0.00"));
                            /*? 133 */
                        }
                        else if (Statics.PlateType == 2)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(8, "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DuplicateBoosterFee"), "0.00"));
                        }
                    }
                    else if (Statics.RegistrationType == "SPREG")
                    {
                        if (Statics.PlateType == 1)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("SpecialRegistrationFee"), "0.00"));
                        }
                        else if (Statics.PlateType == 2)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(8, "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(2, "0.00"));
                        }
                        /*? 135 */
                    }
                    else if (Statics.ETO)
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(0, "0.00"));
                    }
                    else if (Statics.RegistrationType == "GVW")
                    {
                        if (Statics.rsFinal.Get_Fields_Decimal("StatePaid") < 0)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(0, "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("StatePaid"), "0.00"));
                        }
                        /*? 137 */
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format((Statics.rsFinal.Get_Fields_Decimal("StatePaid") - Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinal.Get_Fields_Decimal("TitleFee")), "0.00"));
                        /*? 139 */
                    }
                    if (Statics.RegistrationType == "DUPREG")
                    {
                        if (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("Class")) == "CI")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DupRegAgentFee"), "0.00"));
                        }
                        // 141            SaveKeyValue HKEY_CURRENT_USER, REGISTRYKEY & "CR\MV\", "AgentFee", frmPreview.txtAgentFee.Text
                        /*? 142 */
                    }
                    else if (Statics.RegistrationType == "CORR")
                    {
                        if (Statics.rsFinal.Get_Fields_Decimal("AgentFee") > Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"))
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee") - Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"), "0.00"));
                            /*? 145 */
                        }
                        else if (Statics.PlateType == 2)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(MotorVehicle.Statics.SpecialtyPlateCorrExtraFee, "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
                            /*? 147 */
                        }
                        /*? 148 */
                    }
                    else if (Statics.RegistrationType == "LOST")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
                        /*? 150 */
                    }
                    else if (Statics.RegistrationType == "TRANSIT")
                    {
                        if (frmTransitPlate.InstancePtr.chkMunicipalVehicle.CheckState == CheckState.Unchecked)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeTransit"), "0.00"));
                            /*? 152 */
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", "0.00");
                        }
                    }
                    else if (Statics.RegistrationType == "BOOST")
                    {
                        if (Statics.PlateType == 1)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(frmBoosterPlate.InstancePtr.curLocalPaid, "0.00"));
                            /*? 154 */
                        }
                        else if (Statics.PlateType == 2)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"), "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"), "0.00"));
                        }
                    }
                    else if (Statics.RegistrationType == "SPREG")
                    {
                        if (Statics.PlateType == 1)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeSpecialRegPermit"), "0.00"));
                        }
                        else if (Statics.PlateType == 2)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeSpecialRegPermit"), "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
                        }
                        /*? 156 */
                    }
                    else if (Statics.RegistrationType == "DUPSTK")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
                    }
                    else if (Statics.RegistrationType == "GVW")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
                        /*? 158 */
                    }
                    else
                    {
                        if (AddToExcise || Statics.ExciseAP)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee"), "0.00"));
                            /*? 161 */
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee") + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0.00"));
                            /*? 163 */
                        }
                        /*? 164 */
                    }
                    if (Statics.RegistrationType == "DUPREG" || Statics.RegistrationType == "LOST" || Statics.RegistrationType == "DUPSTK" || Statics.RegistrationType == "TRANSIT" || Statics.RegistrationType == "BOOST" || Statics.RegistrationType == "SPREG" || Statics.RegistrationType == "GVW")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(0, "0.00"));
                        /*? 167 */
                    }
                    else if (Statics.RegistrationType == "CORR")
                    {
                        if (Statics.rsFinal.Get_Fields_Decimal("SalesTax") > Statics.rsFinalCompare.Get_Fields_Decimal("SalesTax"))
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinalCompare.Get_Fields_Decimal("SalesTax"), "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(0, "0.00"));
                        }
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("SalesTax"), "0.00"));
                        /*? 169 */
                    }
                    if (Statics.RegistrationType == "DUPREG" || Statics.RegistrationType == "LOST" || Statics.RegistrationType == "DUPSTK" || Statics.RegistrationType == "TRANSIT" || Statics.RegistrationType == "BOOST" || Statics.RegistrationType == "SPREG" || Statics.RegistrationType == "GVW")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(0, "0.00"));
                        /*? 172 */
                    }
                    else if (Statics.RegistrationType == "CORR")
                    {
                        if (Statics.rsFinal.Get_Fields_Decimal("TitleFee") > Statics.rsFinalCompare.Get_Fields_Decimal("TitleFee"))
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("TitleFee") - Statics.rsFinalCompare.Get_Fields_Decimal("TitleFee"), "0.00"));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(0, "0.00"));
                        }
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("TitleFee"), "0.00"));
                        /*? 174 */
                    }
                    if (Statics.RegistrationType != "TRANSIT" && Statics.RegistrationType != "BOOST" && Statics.RegistrationType != "SPREG")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", GetPartyNameMiddleInitial(Statics.rsFinal.Get_Fields_Int32("PartyID1"), true));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(Statics.rsFinal.Get_Fields_Int32("PartyID1")));
                        /*? 181 */
                    }
                    else if (Statics.RegistrationType == "TRANSIT")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", GetPartyNameMiddleInitial(FCConvert.ToInt32(Conversion.Val(Strings.Trim(frmTransitPlate.InstancePtr.txtCustomerNumber.Text))), true));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(Conversion.Val(Strings.Trim(frmTransitPlate.InstancePtr.txtCustomerNumber.Text))));
                        /*? 183 */
                    }
                    else if (Statics.RegistrationType == "SPREG")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", GetPartyNameMiddleInitial(FCConvert.ToInt32(Conversion.Val(Strings.Trim(frmSpecialPermit.InstancePtr.txtCustomerNumber.Text))), true));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(Conversion.Val(Strings.Trim(frmSpecialPermit.InstancePtr.txtCustomerNumber.Text))));
                        /*? 185 */
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", GetPartyNameMiddleInitial(Statics.rsPlateForReg.Get_Fields_Int32("PartyID1"), true));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_Int32("PartyID1")));
                        /*? 191 */
                    }
                    if (Statics.RegistrationType != "TRANSIT" && Statics.RegistrationType != "BOOST" && Statics.RegistrationType != "SPREG")
                    {
                        strM = MotorVehicle.ReturnMonthStickerNumber();
                        strY = MotorVehicle.ReturnYearStickerNumber();
                        /*? 195 */
                    }
                    else
                    {
                        strM = "";
                        strY = "";
                        /*? 198 */
                    }
                    if (Statics.RegistrationType == "DUPSTK")
                    {
                        if (Strings.Trim(Statics.TempMonth) != "")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Month", Statics.TempMonth);
                            /*? 202 */
                        }
                        if (Strings.Trim(Statics.TempYear) != "")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Year", Statics.TempYear);
                            /*? 205 */
                        }
                        /*? 206 */
                    }
                    else
                    {
                        if (Strings.Trim(strM) != "")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Month", Strings.Left(strM, 2) + Strings.Right(strM, strM.Length - Strings.InStr(1, strM, " ", CompareConstants.vbBinaryCompare)) + Strings.Mid(strM, 3, 1));
                            /*? 209 */
                        }
                        if (Strings.Trim(strY) != "")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Year", Strings.Left(strY, 2) + Strings.Right(strY, strY.Length - Strings.InStr(1, strY, " ", CompareConstants.vbBinaryCompare)) + Strings.Mid(strY, 3, 1));
                            /*? 212 */
                        }
                        /*? 213 */
                    }
                    if (Statics.RegistrationType != "TRANSIT" && Statics.RegistrationType != "BOOST" && Statics.RegistrationType != "SPREG" && Statics.RegistrationType != "DUPSTK")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", FCConvert.ToString(Statics.rsFinal.Get_Fields_String("plate")));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", FCConvert.ToString(Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", FCConvert.ToString(Statics.rsFinal.Get_Fields_String("TransactionType")));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ID", FCConvert.ToString(Statics.TransactionID));
                        /*? 219 */
                    }
                    else if (Statics.RegistrationType == "DUPSTK")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", FCConvert.ToString(Statics.rsFinal.Get_Fields_String("plate")));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", FCConvert.ToString(Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", "DPS");
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ID", Statics.TransactionID);
                        /*? 224 */
                    }
                    else if (Statics.RegistrationType == "TRANSIT")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", frmTransitPlate.InstancePtr.txtPlate.Text);
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", "");
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", "TRANSIT");
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ID", "");
                        /*? 229 */
                    }
                    else if (Statics.RegistrationType == "SPREG")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", "");
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", "");
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", "SPECIAL REG PERMIT");
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ID", "");
                        /*? 234 */
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_String("plate")));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", FCConvert.ToString(Statics.rsPlateForReg.Get_Fields_Int32("MVR3")));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", "BOOSTER");
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ID", "");
                        /*? 239 */
                    }
                    if ((Statics.RegisteringFleet || Statics.gboolFromBatchRegistration) && !Statics.PendingRegistration)
                    {
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", ref modReplaceWorkFiles.Statics.gstrReturn);
                        if (Strings.Trim(modReplaceWorkFiles.Statics.gstrReturn) != "" && Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) != 0)
                        {
                            Statics.curFleetExcise += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        }
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", ref modReplaceWorkFiles.Statics.gstrReturn);
                        if (Strings.Trim(modReplaceWorkFiles.Statics.gstrReturn) != "" && Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) != 0)
                        {
                            Statics.curFleetStatePaid += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        }
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", ref modReplaceWorkFiles.Statics.gstrReturn);
                        if (Strings.Trim(modReplaceWorkFiles.Statics.gstrReturn) != "" && Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) != 0)
                        {
                            Statics.curFleetAgentFee += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        }
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", ref modReplaceWorkFiles.Statics.gstrReturn);
                        if (Strings.Trim(modReplaceWorkFiles.Statics.gstrReturn) != "" && Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) != 0)
                        {
                            Statics.curFleetSalesTax += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        }
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", ref modReplaceWorkFiles.Statics.gstrReturn);
                        if (Strings.Trim(modReplaceWorkFiles.Statics.gstrReturn) != "" && Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn) != 0)
                        {
                            Statics.curFleetTitleFee += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        }
                        if (Statics.GroupOrFleet == "F")
                        {
                            Statics.strFleetNameAndNumber = "Fleet " + Statics.rsFinal.Get_Fields_Int32("FleetNumber");
                            /*? 253 */
                        }
                        else
                        {
                            Statics.strFleetNameAndNumber = "Group " + Statics.rsFinal.Get_Fields_Int32("FleetNumber");
                            /*? 255 */
                        }
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.strFleetOwner = Strings.Trim(modReplaceWorkFiles.Statics.gstrReturn);
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.lngFleetOwnerPartyID = FCConvert.ToString(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn));
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.lngFleetResidenceCase = FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn)));
                        /*? 260 */
                    }
                    /*? 261 */
                    if (MotorVehicle.Statics.correlationId != new Guid() && !Statics.RegisteringFleet)
                    {
                        CompleteMVTransaction();
                    }

                    return;
                    /*? 262 */
                }
                if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "ORLAND")
                {
                    FCFileSystem.FileOpen(11, "S:\\TSREAS55.NET", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), 241);
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
                {
                    FCFileSystem.FileOpen(11, "S:\\TSREAS55.NET", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), 241);
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "UNION")
                {
                    FCFileSystem.FileOpen(11, "S:\\TSREAS55.NET", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), 241);
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "CASCO")
                {
                    FCFileSystem.FileOpen(11, "S:\\TSREAS55.NET", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), 241);
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "HIRAM")
                {
                    FCFileSystem.FileOpen(11, "S:\\TSREAS55.NET", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), 241);
                }
                else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "CHINA")
                {
                    FCFileSystem.FileOpen(11, "S:\\TSREAS55.NET", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), 241);
                }
                else
                {
                    FCFileSystem.FileOpen(11, "C:\\TrioVB\\TSREAS55.NET", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), 241);
                }
                FCFileSystem.FileGet(11, ref strRec3, -1, true);
                FCFileSystem.FileGet(11, ref strRec5A, -1, true);
                // 
                Strings.MidSet(ref strRec3, 3, 21, Strings.StrDup(21, " "));
                Strings.MidSet(ref strRec5A, 3, 120, Strings.StrDup(120, " "));
                // 
                Strings.MidSet(ref strRec3, 3, 3, "MV3");
                if (Statics.RegistrationType == "DUPREG" || Statics.RegistrationType == "CORR" || Statics.RegistrationType == "LOST" || Statics.RegistrationType == "DUPSTK")
                {
                    Strings.MidSet(ref strRec3, 6, 7, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "00000.00")) * 100, "0000000"));
                }
                else
                {
                    if (Statics.ExciseAP)
                    {
                        Strings.MidSet(ref strRec3, 6, 7, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "00000.00")) * 100, "0000000"));
                    }
                    else
                    {
                        if (AddToExcise)
                        {
                            if (Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") < 0)
                            {
                                Strings.MidSet(ref strRec3, 6, 7, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "00000.00")) * 100, "0000000"));
                            }
                            else
                            {
                                Strings.MidSet(ref strRec3, 6, 7, Strings.Format(FCConvert.ToInt32(Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "00000.00")) * 100, "0000000"));
                            }
                        }
                        else
                        {
                            if (Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") < 0)
                            {
                                Strings.MidSet(ref strRec3, 6, 7, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "00000.00")) * 100, "0000000"));
                            }
                            else
                            {
                                Strings.MidSet(ref strRec3, 6, 7, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "00000.00")) * 100, "0000000"));
                            }
                        }
                    }
                }
                Strings.MidSet(ref strRec5A, 3, 3, "MV5");
                if (Statics.RegistrationType == "DUPREG" || Statics.RegistrationType == "CORR" || Statics.RegistrationType == "LOST" || Statics.RegistrationType == "DUPSTK")
                {
                    Strings.MidSet(ref strRec5A, 6, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "0000.00")) * 100, "000000"));
                }
                else
                {
                    if (Statics.ExciseAP)
                    {
                        Strings.MidSet(ref strRec5A, 6, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "0000.00")) * 100, "000000"));
                    }
                    else
                    {
                        if (AddToExcise)
                        {
                            if (Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") < 0)
                            {
                                Strings.MidSet(ref strRec5A, 6, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0000.00")) * 100, "000000"));
                            }
                            else
                            {
                                Strings.MidSet(ref strRec5A, 6, 6, Strings.Format(FCConvert.ToInt32(Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "0000.00")) * 100, "000000"));
                            }
                        }
                        else
                        {
                            if (Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed") < 0)
                            {
                                Strings.MidSet(ref strRec5A, 6, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "0000.00")) * 100, "000000"));
                            }
                            else
                            {
                                Strings.MidSet(ref strRec5A, 6, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "0000.00")) * 100, "000000"));
                            }
                        }
                    }
                }
                if (Statics.RegistrationType == "DUPREG")
                {
                    Strings.MidSet(ref strRec5A, 12, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(2, "0000.00")) * 100, "000000"));
                }
                else if (Statics.RegistrationType == "DUPSTK")
                {
                    Strings.MidSet(ref strRec5A, 12, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("StickerFee"), "0000.00")) * 100, "000000"));
                }
                else if (Statics.RegistrationType == "CORR")
                {
                    if (Statics.rsFinal.Get_Fields_Decimal("AgentFee") > Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"))
                    {
                        Strings.MidSet(ref strRec5A, 12, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.ECTotal - (Statics.rsFinal.Get_Fields_Decimal("AgentFee") - Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee")), "0000.00")) * 100, "000000"));
                    }
                    else
                    {
                        Strings.MidSet(ref strRec5A, 12, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.ECTotal, "0000.00")) * 100, "000000"));
                    }
                }
                else if (Statics.RegistrationType == "LOST")
                {
                    Strings.MidSet(ref strRec5A, 12, 6, Strings.Format(FCConvert.ToInt32(Strings.Format((Statics.rsFinal.Get_Fields_Decimal("ReplacementFee") + Statics.rsFinal.Get_Fields_Decimal("StickerFee")), "0000.00")) * 100, "000000"));
                }
                else if (Statics.ETO)
                {
                    Strings.MidSet(ref strRec5A, 12, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "0000.00")) * 100, "000000"));
                }
                else
                {
                    Strings.MidSet(ref strRec5A, 12, 6, Strings.Format(FCConvert.ToInt32(Strings.Format((Statics.rsFinal.Get_Fields_Decimal("StatePaid") - Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinal.Get_Fields_Decimal("TitleFee")), "0000.00")) * 100, "000000"));
                }
                if (Statics.RegistrationType == "DUPREG")
                {
                    Strings.MidSet(ref strRec5A, 18, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(1, "000.00")) * 100, "000000"));
                }
                else if (Statics.RegistrationType == "CORR")
                {
                    if (Statics.rsFinal.Get_Fields_Decimal("AgentFee") > Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"))
                    {
                        Strings.MidSet(ref strRec5A, 18, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee") - Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"), "000.00")) * 100, "000000"));
                    }
                    else
                    {
                        Strings.MidSet(ref strRec5A, 18, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "000.00")) * 100, "000000"));
                    }
                }
                else if (Statics.RegistrationType == "LOST")
                {
                    Strings.MidSet(ref strRec5A, 18, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "000.00")) * 100, "000000"));
                }
                else if (Statics.RegistrationType == "DUPSTK")
                {
                    Strings.MidSet(ref strRec5A, 18, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "000.00")) * 100, "000000"));
                }
                else
                {
                    if (AddToExcise)
                    {
                        Strings.MidSet(ref strRec5A, 18, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee"), "000.00")) * 100, "000000"));
                    }
                    else
                    {
                        Strings.MidSet(ref strRec5A, 18, 6, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee") + Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "000.00")) * 100, "000000"));
                    }
                }
                if (Statics.RegistrationType == "DUPREG" || Statics.RegistrationType == "LOST" || Statics.RegistrationType == "DUPSTK" || Statics.RegistrationType == "CORR")
                {
                    Strings.MidSet(ref strRec5A, 24, 7, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "0000.00")) * 100, "0000000"));
                }
                else
                {
                    Strings.MidSet(ref strRec5A, 24, 7, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("SalesTax"), "0000.00")) * 100, "0000000"));
                }
                if (Statics.RegistrationType == "DUPREG" || Statics.RegistrationType == "LOST" || Statics.RegistrationType == "DUPSTK" || Statics.RegistrationType == "CORR")
                {
                    Strings.MidSet(ref strRec5A, 31, 5, Strings.Format(FCConvert.ToInt32(Strings.Format(0, "000.00")) * 100, "00000"));
                }
                else
                {
                    Strings.MidSet(ref strRec5A, 31, 5, Strings.Format(FCConvert.ToInt32(Strings.Format(Statics.rsFinal.Get_Fields_Decimal("TitleFee"), "000.00")) * 100, "00000"));
                }
                Strings.MidSet(ref strRec5A, 53, 30, FCConvert.ToString(strPadSpaceR(GetPartyNameMiddleInitial(Statics.rsFinal.Get_Fields_Int32("PartyID1"), true), 30)));
                strM = MotorVehicle.ReturnMonthStickerNumber();
                strY = MotorVehicle.ReturnYearStickerNumber();
                if (Statics.RegistrationType == "DUPSTK")
                {
                    if (Strings.Trim(Statics.TempMonth) != "")
                    {
                        Strings.MidSet(ref strRec5A, 83, 10, Statics.TempMonth);
                    }
                    if (Strings.Trim(Statics.TempYear) != "")
                    {
                        Strings.MidSet(ref strRec5A, 93, 10, Statics.TempYear);
                    }
                }
                else
                {
                    if (Strings.Trim(strM) != "")
                    {
                        Strings.MidSet(ref strRec5A, 83, 10, Strings.Left(strM, 2) + Strings.Right(strM, strM.Length - Strings.InStr(1, strM, " ", CompareConstants.vbBinaryCompare)) + Strings.Mid(strM, 3, 1));
                    }
                    if (Strings.Trim(strY) != "")
                    {
                        Strings.MidSet(ref strRec5A, 93, 10, Strings.Left(strY, 2) + Strings.Right(strY, strY.Length - Strings.InStr(1, strY, " ", CompareConstants.vbBinaryCompare)) + Strings.Mid(strY, 3, 1));
                    }
                }
                Strings.MidSet(ref strRec5A, 103, 8, FCConvert.ToString(strPadSpaceR(Statics.rsFinal.Get_Fields_String("plate"), 8)));
                Strings.MidSet(ref strRec5A, 111, 8, FCConvert.ToString(strPadSpaceR(FCConvert.ToString(Statics.rsFinal.Get_Fields_Int32("MVR3")), 8)));
                // 
                FCFileSystem.FilePut(11, strRec3, -1, true);
                FCFileSystem.FilePut(11, strRec5A, -1, true);
                FCFileSystem.FileClose(11);

                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "An error occurred on line " + Information.Erl() + " while writing information to WCR.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(string)

        public static void CompleteMVTransaction()
        {
            try
            {
                var transaction = new MotorVehicleTransactionBase();
                string strTemp = "";
                string strRegKey = modGlobalConstants.REGISTRYKEY + "CR\\MV\\";

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Type", ref strTemp);
                transaction.RegistrationType = Strings.Trim(strTemp + " ");
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Type", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "ResCase", ref strTemp);
                transaction.ResidenceCode = FCConvert.ToString(Conversion.Val(strTemp));
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "ResCase", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Excise", ref strTemp);
                transaction.AddTransactionAmount(new TransactionAmountItem
                {
                    Name = "Amount1",
                    Description = "Excise Tax",
                    Total = strTemp.ToDecimalValue()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Excise", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "StatePaid", ref strTemp);
                transaction.AddTransactionAmount(new TransactionAmountItem
                {
                    Name = "Amount2",
                    Description = "State Paid",
                    Total = strTemp.ToDecimalValue()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "StatePaid", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "AgentFee", ref strTemp);
                transaction.AddTransactionAmount(new TransactionAmountItem
                {
                    Name = "Amount3",
                    Description = "Agent Fee",
                    Total = strTemp.ToDecimalValue()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "AgentFee", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "SalesTax", ref strTemp);
                transaction.AddTransactionAmount(new TransactionAmountItem
                {
                    Name = "Amount4",
                    Description = "Sales Tax",
                    Total = strTemp.ToDecimalValue()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "SalesTax", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "TitleFee", ref strTemp);
                transaction.AddTransactionAmount(new TransactionAmountItem
                {
                    Name = "Amount5",
                    Description = "Title Fee",
                    Total = strTemp.ToDecimalValue()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "TitleFee", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "StateExcise", ref strTemp);
                transaction.AddTransactionAmount(new TransactionAmountItem
                {
                    Name = "Amount6",
                    Description = "Other Excise",
                    Total = strTemp.ToDecimalValue()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "StateExcise", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Year", ref strTemp);
                if (strTemp != "")
                {
                    if (strTemp.Length == 10)
                    {
                        strTemp = Strings.Left(strTemp, 2) + Strings.Right(strTemp, 1) + Strings.Mid(strTemp, 3, 7);
                    }
                }
                transaction.Controls.Add(new ControlItem
                {
                    Name = "Control1",
                    Description = "Year Sticker",
                    Value = strTemp.Trim()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Year", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Month", ref strTemp);
                if (strTemp != "")
                {
                    if (strTemp.Length == 10)
                    {
                        strTemp = Strings.Left(strTemp, 2) + Strings.Right(strTemp, 1) + Strings.Mid(strTemp, 3, 7);
                    }
                }
                transaction.Controls.Add(new ControlItem
                {
                    Name = "Control2",
                    Description = "Month Sticker",
                    Value = strTemp.Trim()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Month", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "MVR3", ref strTemp);
                transaction.Controls.Add(new ControlItem
                {
                    Name = "Control3",
                    Description = "MVR3",
                    Value = strTemp.Trim()
                });
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "MVR3", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Owner", ref strTemp);
                transaction.PayerName = strTemp.Trim();
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Owner", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "PartyID", ref strTemp);
                transaction.PayerPartyId = strTemp.ToIntegerValue();
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "PartyID", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "ID", ref strTemp);
                transaction.RecordKey = strTemp.ToIntegerValue();
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "ID", "");

                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Plate", ref strTemp);
                transaction.Reference = strTemp.Trim();
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, strRegKey, "Plate", "");

                transaction.TransactionTypeCode = "99";
                transaction.TransactionTypeDescription = "Motor Vehicle";
                transaction.ActualDateTime = DateTime.Now;
                transaction.EffectiveDateTime = DateTime.Now;

                StaticSettings.GlobalEventPublisher.Publish(new TransactionCompleted
                {
                    CorrelationId = MotorVehicle.Statics.correlationId,
                    CompletedTransactions = new List<TransactionBase>
                {
                    transaction
                }
                });

                Statics.TransactionCompleted = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                Console.WriteLine(ex);
            }
        }

        public static object strPadSpace(string str1, int intSpaces)
        {
            object strPadSpace = null;
            if (intSpaces >= str1.Length)
            {
                strPadSpace = Strings.StrDup(intSpaces - str1.Length, " ") + str1;
            }
            else
            {
                strPadSpace = Strings.StrDup(intSpaces, " ");
            }
            return strPadSpace;
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(string)
        public static object strPadSpaceR(string str1, int intSpaces)
        {
            object strPadSpaceR = null;
            if (intSpaces >= Strings.Trim(str1).Length)
            {
                strPadSpaceR = str1 + Strings.StrDup(intSpaces - Strings.Trim(str1).Length, " ");
            }
            else
            {
                strPadSpaceR = Strings.StrDup(intSpaces, " ");
            }
            return strPadSpaceR;
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(string)
        public static object strPadZeros(string str1, int intSpaces)
        {
            object strPadZeros = null;
            if (intSpaces >= str1.Length)
            {
                strPadZeros = Strings.StrDup(intSpaces - str1.Length, "0") + str1;
            }
            else
            {
                strPadZeros = Strings.StrDup(intSpaces, "0");
            }
            return strPadZeros;
        }

        public static void RegisterNew()
        {
            string RegChoice = "";
            int SubChoice = 0;
            if (Statics.FromTransfer)
            {
                if (Statics.RegistrationType == "RRT")
                {
                    RegChoice = "Re-Registration Regular";
                    if (Statics.PlateType == 1)
                    {
                        SubChoice = 2;
                    }
                    else if (Statics.PlateType == 2)
                    {
                        SubChoice = 0;
                    }
                    else if (Statics.PlateType == 3)
                    {
                        SubChoice = 1;
                    }
                    else if (Statics.PlateType == 4)
                    {
                        SubChoice = 2;
                    }
                }
                else
                {
                    RegChoice = "New Registration Regular";
                    if (Statics.PlateType == 1)
                    {
                        SubChoice = 1;
                    }
                    else if (Statics.PlateType == 2)
                    {
                        SubChoice = 0;
                    }
                    else if (Statics.PlateType == 3)
                    {
                        SubChoice = 2;
                    }
                    else if (Statics.PlateType == 4)
                    {
                        SubChoice = 1;
                    }
                    Statics.NoRedbook = true;
                    Statics.Subclass = Statics.TransferSubclass;
                    Statics.RememberedSubclass = Statics.TransferSubclass;
                }
                //App.DoEvents();
                //frmPlateInfo.InstancePtr.Show(App.MainForm);
                frmPlateInfo.InstancePtr.ShowForm();
                frmPlateInfo.InstancePtr.OKFlag = true;
                //FC:FINAL:BB:#i2210 - changed comboBox SelectedIndex with SelectedItem
                //frmPlateInfo.InstancePtr.cmbRegType1.SelectedIndex = RegChoice;
                frmPlateInfo.InstancePtr.cmbRegType1.SelectedItem = RegChoice;
                frmPlateInfo.InstancePtr.cmbPlate.SelectedIndex = SubChoice;
                //XXXXXX   frmPlateInfo.InstancePtr.optPlate_Click(SubChoice);
                if (RegChoice == "Re-Registration Regular")
                {
                    if (SubChoice == 2)
                    {
                        frmPlateInfo.InstancePtr.txtPlateType[0].Text = Statics.Plate1;
                        frmPlateInfo.InstancePtr.txtPlateType[1].Text = Statics.Plate2;
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(0, false);
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(1, false);
                        frmPlateInfo.InstancePtr.cboClass.SelectedIndex = Statics.Plate1Sub;
                        frmPlateInfo.InstancePtr.cboClass2.SelectedIndex = Statics.Plate2Sub;
                    }
                    else if (SubChoice == 0)
                    {
                        frmPlateInfo.InstancePtr.txtPlateType[0].Text = Statics.Plate1;
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(0, false);
                        frmPlateInfo.InstancePtr.cboClass.SelectedIndex = Statics.Plate1Sub;
                    }
                    else if (SubChoice == 1)
                    {
                        frmPlateInfo.InstancePtr.txtPlateType[0].Text = Statics.Plate1;
                        frmPlateInfo.InstancePtr.txtPlateType[1].Text = Statics.Plate2;
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(0, false);
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(1, false);
                        frmPlateInfo.InstancePtr.cboClass.SelectedIndex = Statics.Plate1Sub;
                        frmPlateInfo.InstancePtr.cboClass2.SelectedIndex = Statics.Plate2Sub;
                    }
                }
                else
                {
                    if (SubChoice == 2)
                    {
                        frmPlateInfo.InstancePtr.txtPlateType[0].Text = Statics.Plate1;
                        frmPlateInfo.InstancePtr.txtPlateType[1].Text = Statics.Plate2;
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(0, false);
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(1, false);
                        frmPlateInfo.InstancePtr.cboClass.SelectedIndex = Statics.Plate1Sub;
                        frmPlateInfo.InstancePtr.cboClass2.SelectedIndex = Statics.Plate2Sub;
                    }
                    else if (SubChoice == 0)
                    {
                        frmPlateInfo.InstancePtr.txtPlateType[0].Text = Statics.Plate1;
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(0, false);
                        frmPlateInfo.InstancePtr.cboClass.SelectedIndex = Statics.Plate1Sub;
                    }
                    else if (SubChoice == 1)
                    {
                        frmPlateInfo.InstancePtr.txtPlateType[0].Text = Statics.Plate1;
                        frmPlateInfo.InstancePtr.txtPlateType_Validate(0, false);
                        frmPlateInfo.InstancePtr.cboClass.SelectedIndex = Statics.Plate1Sub;
                    }
                }
                frmPlateInfo.InstancePtr.cmdProcess.Enabled = true;
                // frmPlateInfo.cmdProcess.SetFocus
                // frmPlateInfo.cmdProcess_Click
            }
        }

        public static void GetWorkRecords()
        {
            try
            {
                // On Error GoTo ErrorRoutine
                Information.Err().Clear();
                Statics.gstrCTAPrinterName = StaticSettings.GlobalSettingService
                                                 .GetSettingValue(SettingOwnerType.Machine, "CTAPrinterName")
                                                 ?.SettingValue ?? "";
                // tromvs-98 07/2018
                Statics.gstrUseTaxPrinterName = StaticSettings.GlobalSettingService
                                                    .GetSettingValue(SettingOwnerType.Machine, "UseTaxPrinterName")
                                                    ?.SettingValue ?? "";
                // tromvs-98 07/2018
                Statics.MVR3PrinterName = StaticSettings.GlobalSettingService
                                              .GetSettingValue(SettingOwnerType.Machine, "MVR3PrinterName")
                                              ?.SettingValue ?? "";
                Statics.MVR10PrinterName = StaticSettings.GlobalSettingService
                                               .GetSettingValue(SettingOwnerType.Machine, "MVR10PrinterName")
                                               ?.SettingValue ?? "";
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (modGNBas.Statics.Bbort == "Y")
                    return;
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static bool SetTellerID(ref string str1, ref int lng1)
        {
            bool SetTellerID = false;
            SetTellerID = false;
            clsDRWrapper rs1 = new clsDRWrapper();
            rs1.OpenRecordset(str1);
            if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
            {
                rs1.MoveLast();
                rs1.MoveFirst();
                while (!rs1.EndOfFile())
                {
                    rs1.Edit();
                    rs1.Set_Fields("TellerCloseoutID", lng1);
                    rs1.Update();
                    rs1.MoveNext();
                }
                SetTellerID = true;
            }
            else
            {
                SetTellerID = true;
            }
            return SetTellerID;
        }
        // vbPorter upgrade warning: NewValue As object	OnWrite(object, string)
        public static void UpdateMVVariable(string strVariableName, object NewValue)
        {
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                Statics.rsVariables.OpenRecordset("SELECT * FROM WMV");
                if (Statics.rsVariables.EndOfFile())
                {
                    Statics.rsVariables.AddNew();
                }
                else
                {
                    Statics.rsVariables.Edit();
                }
                Statics.rsVariables.Set_Fields(strVariableName, NewValue);
                Statics.rsVariables.Update();
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
        public static string GetMVVariable(string strName)
        {
            string GetMVVariable = "";
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                Statics.rsVariables.OpenRecordset("SELECT * FROM WMV");
                if (Statics.rsVariables.EndOfFile())
                {
                    GetMVVariable = String.Empty;
                }
                else
                {
                    GetMVVariable = FCConvert.ToString(Statics.rsVariables.Get_Fields(strName));
                }
                if (FCUtils.IsNull(GetMVVariable))
                    GetMVVariable = "";
                return GetMVVariable;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetMVVariable;
        }

        public static bool JobComplete(string x)
        {
            bool JobComplete = false;
            foreach (FCForm ff in FCGlobal.Statics.Forms)
            {
                if (ff.Name == x)
                {
                    JobComplete = false;
                    return JobComplete;
                }
            }
            JobComplete = true;
            return JobComplete;
        }

        public static void Write_Fleet_PDS_Work_Record_Stuff()
        {
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "lngFleetResidenceCase", FCConvert.ToString(1));
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(Statics.curFleetExcise, "0.00"));
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(Statics.curFleetStatePaid, "0.00"));
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(Statics.curFleetAgentFee, "0.00"));
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(Statics.curFleetSalesTax, "0.00"));
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(Statics.curFleetTitleFee, "0.00"));
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", Statics.strFleetNameAndNumber);
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", Statics.strFleetOwner);
            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", Statics.lngFleetOwnerPartyID);

            if (MotorVehicle.Statics.correlationId != new Guid())
            {
                CompleteMVTransaction();
            }
        }

        public static object FixQuotes(string strValue)
        {
            object FixQuotes = null;
            FixQuotes = strValue.Replace("'", "''");
            return FixQuotes;
        }

        public static void CreateJournalEntry(ref int lngJournal, ref Decimal curAmount, ref Decimal curMerchantFee)
        {
            clsDRWrapper rsEntries = new clsDRWrapper();
            int counter;
            clsDRWrapper Master = new clsDRWrapper();
            clsDRWrapper rsDefaultInfo = new clsDRWrapper();
            clsDRWrapper rsFundInfo = new clsDRWrapper();
            string strFund;
            string strCashAcct;
            string strRRRevAcct;
            string strRRExpAcct;
            strRRRevAcct = "";
            strRRExpAcct = "";
            strCashAcct = "";
            strFund = "";
            rsDefaultInfo.OpenRecordset("SELECT * FROM WMV");
            if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
            {
                strRRRevAcct = Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("RRRevAcct")));
                strRRExpAcct = Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("RRExpAcct")));
            }
            if (strRRRevAcct == "")
            {
                MessageBox.Show("You must set up a Rapid Renewal revenue account before you may continue.", "Invalid Revenue Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (strRRExpAcct == "")
            {
                // MsgBox "You must set up a Rapid Renewal merchant fee account before you may continue.", vbInformation, "Invalid Merchant Fee Account"
                // Exit Sub
                strRRExpAcct = strRRRevAcct;
            }
            rsFundInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Mid(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("RRRevAcct")), 3, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + "' AND convert(int, isnull(Division, 0)) = 0", "TWBD0000.vb1");
            if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
            {
                strFund = Strings.Format(rsFundInfo.Get_Fields_String("Fund"), Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), "0"));
            }
            else
            {
                strFund = Strings.Format(1, Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), "0"));
            }
            rsDefaultInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
            if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
            {
                strCashAcct = Strings.Trim(rsDefaultInfo.Get_Fields_String("Account"));
            }
            if (strCashAcct == "" || Conversion.Val(strCashAcct) == 0)
            {
                MessageBox.Show("You must set up a Miscellaneous Cash account before you may continue.", "Invalid Miscellaneous Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (lngJournal != 0)
            {
                // do nothing
            }
            else
            {
                // get journal number
                if (modBudgetaryAccounting.LockJournal() == false)
                {
                    // add entries to incomplete journals table
                }
                else
                {
                    Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
                    if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
                    {
                        Master.MoveLast();
                        Master.MoveFirst();
                        lngJournal = Master.Get_Fields_Int32("JournalNumber") + 1;
                    }
                    else
                    {
                        lngJournal = 1;
                    }
                    Master.AddNew();
                    Master.Set_Fields("JournalNumber", lngJournal);
                    Master.Set_Fields("Status", "E");
                    Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                    Master.Set_Fields("StatusChangeDate", DateTime.Today);
                    Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " R/R Deposit");
                    Master.Set_Fields("Type", "CR");
                    Master.Set_Fields("Period", DateTime.Today.Month);
                    Master.Update();
                    Master.Reset();
                    modBudgetaryAccounting.UnlockJournal();
                }
            }
            if (lngJournal != 0)
            {
                rsEntries.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
                rsEntries.OmitNullsOnInsert = true;
                rsEntries.AddNew();
                rsEntries.Set_Fields("Type", "C");
                rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
                rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " R/R");
                rsEntries.Set_Fields("JournalNumber", lngJournal);
                rsEntries.Set_Fields("Account", strRRRevAcct);
                rsEntries.Set_Fields("Amount", curAmount * -1);
                rsEntries.Set_Fields("WarrantNumber", 0);
                rsEntries.Set_Fields("Period", DateTime.Today.Month);
                rsEntries.Set_Fields("RCB", "R");
                rsEntries.Set_Fields("Status", "E");
                rsEntries.Update();
                if (curMerchantFee != 0)
                {
                    rsEntries.AddNew();
                    rsEntries.Set_Fields("Type", "C");
                    rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
                    rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " R/R Merc Fee");
                    rsEntries.Set_Fields("JournalNumber", lngJournal);
                    rsEntries.Set_Fields("Account", strRRExpAcct);
                    rsEntries.Set_Fields("Amount", curMerchantFee);
                    rsEntries.Set_Fields("WarrantNumber", 0);
                    rsEntries.Set_Fields("Period", DateTime.Today.Month);
                    rsEntries.Set_Fields("RCB", "R");
                    rsEntries.Set_Fields("Status", "E");
                    rsEntries.Update();
                }
                rsEntries.AddNew();
                rsEntries.Set_Fields("Type", "C");
                rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
                rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " R/R");
                rsEntries.Set_Fields("JournalNumber", lngJournal);
                if (modAccountTitle.Statics.YearFlag)
                {
                    rsEntries.Set_Fields("Account", "G " + strFund + "-" + strCashAcct);
                }
                else
                {
                    rsEntries.Set_Fields("Account", "G " + strFund + "-" + strCashAcct + "-00");
                }
                rsEntries.Set_Fields("Amount", curAmount - curMerchantFee);
                rsEntries.Set_Fields("WarrantNumber", 0);
                rsEntries.Set_Fields("Period", DateTime.Today.Month);
                rsEntries.Set_Fields("RCB", "R");
                rsEntries.Set_Fields("Status", "E");
                rsEntries.Update();
            }
            else
            {
                rsEntries.OpenRecordset("SELECT * FROM TempJournalEntries WHERE ID = 0", "TWBD0000.vb1");
                rsEntries.AddNew();
                rsEntries.Set_Fields("Type", "C");
                rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
                rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " R/R");
                rsEntries.Set_Fields("JournalNumber", lngJournal);
                rsEntries.Set_Fields("Account", strRRRevAcct);
                rsEntries.Set_Fields("Amount", curAmount * -1);
                rsEntries.Set_Fields("WarrantNumber", 0);
                rsEntries.Set_Fields("Period", DateTime.Today.Month);
                rsEntries.Set_Fields("RCB", "R");
                rsEntries.Set_Fields("Status", "E");
                rsEntries.Update();
                if (curMerchantFee != 0)
                {
                    rsEntries.AddNew();
                    rsEntries.Set_Fields("Type", "C");
                    rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
                    rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " R/R Merc Fee");
                    rsEntries.Set_Fields("JournalNumber", lngJournal);
                    rsEntries.Set_Fields("Account", strRRExpAcct);
                    rsEntries.Set_Fields("Amount", curMerchantFee);
                    rsEntries.Set_Fields("WarrantNumber", 0);
                    rsEntries.Set_Fields("Period", DateTime.Today.Month);
                    rsEntries.Set_Fields("RCB", "R");
                    rsEntries.Set_Fields("Status", "E");
                    rsEntries.Update();
                }
                rsEntries.AddNew();
                rsEntries.Set_Fields("Type", "C");
                rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
                rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " R/R");
                rsEntries.Set_Fields("JournalNumber", lngJournal);
                if (modAccountTitle.Statics.YearFlag)
                {
                    rsEntries.Set_Fields("Account", "G " + strFund + "-" + strCashAcct);
                }
                else
                {
                    rsEntries.Set_Fields("Account", "G " + strFund + "-" + strCashAcct + "-00");
                }
                rsEntries.Set_Fields("Amount", curAmount - curMerchantFee);
                rsEntries.Set_Fields("WarrantNumber", 0);
                rsEntries.Set_Fields("Period", DateTime.Today.Month);
                rsEntries.Set_Fields("RCB", "R");
                rsEntries.Set_Fields("Status", "E");
                rsEntries.Update();
            }
        }

        public static void Write_PDS_Work_Record_Stuff_MVRT10()
        {
            clsDRWrapper rsCheck = new clsDRWrapper();
            clsDRWrapper rsDefaultInfo = new clsDRWrapper();
            clsDRWrapper rsResCheck = new clsDRWrapper();
            Decimal curCorrAmount;
            Decimal ExciseCredit;
            Decimal ExciseCharge;
            Decimal OldCredit;
            Decimal NewCredit;
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
                rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
                if (Statics.bolFromWindowsCR)
                {
                    if (modRegionalTown.IsRegionalTown())
                    {
                        if (Statics.RegistrationType == "TRANSIT" || Statics.RegistrationType == "SPREG")
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                        }
                        else if (Statics.RegistrationType == "BOOST")
                        {
                            rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Statics.rsPlateForReg.Get_Fields_String("ResidenceCode") + "'", "CentralData");
                            if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(rsResCheck.Get_Fields_Int32("TownNumber")));
                            }
                            else
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                            }
                        }
                        else
                        {
                            rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Statics.rsFinal.Get_Fields_String("ResidenceCode") + "'", "CentralData");
                            if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(rsResCheck.Get_Fields_Int32("TownNumber")));
                            }
                            else
                            {
                                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                            }
                        }
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                    }
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
                    if (FCConvert.ToString(Statics.rsFinal.Get_Fields_String("TransactionType")) == "DPR")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(0, "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"), "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(0, "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(0, "0.00"));
                    }
                    else if (Statics.rsFinal.Get_Fields_String("TransactionType") == "ECO")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(((Statics.rsFinal.Get_Fields_Decimal("StatePaid") - Statics.rsFinalCompare.Get_Fields_Decimal("StatePaid")) - (Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinalCompare.Get_Fields_Decimal("SalesTax")) - (Statics.rsFinal.Get_Fields_Decimal("TitleFee") - Statics.rsFinalCompare.Get_Fields_Decimal("TitleFee"))), "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee") - Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee") + Statics.rsFinal.Get_Fields_Decimal("AgentFeeCorrection"), "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinalCompare.Get_Fields_Decimal("SalesTax"), "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("TitleFee") - Statics.rsFinalCompare.Get_Fields_Decimal("TitleFee"), "0.00"));
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format((Statics.rsFinal.Get_Fields_Decimal("StatePaid") - Statics.rsFinal.Get_Fields_Decimal("SalesTax") - Statics.rsFinal.Get_Fields_Decimal("TitleFee")), "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("AgentFee"), "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("SalesTax"), "0.00"));
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(Statics.rsFinal.Get_Fields_Decimal("TitleFee"), "0.00"));
                    }
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", Strings.Trim(FCConvert.ToString(Statics.rsFinal.Get_Fields("Owner1"))));
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", Statics.rsFinal.Get_Fields_String("plate"));
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", "");
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", Statics.rsFinal.Get_Fields_String("TransactionType"));
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ID", "");
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", "");
                    if (Statics.gboolFromBatchRegistration || Statics.gboolFromReRegBatchRegistration)
                    {
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.curFleetExcise += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.curFleetStatePaid += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.curFleetAgentFee += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.curFleetSalesTax += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.curFleetTitleFee += FCConvert.ToDecimal(modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.strFleetNameAndNumber = "Batch Long Term Trailer Registration";
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.strFleetOwner = Strings.Trim(modReplaceWorkFiles.Statics.gstrReturn);
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.lngFleetOwnerPartyID = FCConvert.ToString(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn));
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", ref modReplaceWorkFiles.Statics.gstrReturn);
                        Statics.lngFleetResidenceCase = FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn)));
                    }
                }

                if (MotorVehicle.Statics.correlationId != new Guid())
                {
                    CompleteMVTransaction();
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "An error occurred on line " + Information.Erl() + " while writing information to WCR.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: rpt As ActiveReport	OnWrite(rptMVRT10, rptMVR3)
        public static void NewSetReportForm(FCSectionReport rpt)
        {
            int i;
            //FC:TODO
            // need to make sure activereports is using the 'MVR3' form
            //if (rpt.Document.Name=="MVRT-10") {
            //	for(i=0; i<=rpt.Document.Printer.PaperSizes.Count-1; i++) {
            //		if (rpt.Document.Printer.PaperSizes.Name[i]=="MVRT10") {
            //			rpt.Document.Printer.PaperSize = rpt.Document.Printer.PaperSizes[i];
            //		}
            //	}
            //} else {
            //	for(i=0; i<=rpt.Document.Printer.PaperSizes.Count-1; i++) {
            //		if (rpt.Document.Printer.PaperSizes.Name(i)=="MVR3") {
            //			rpt.Document.Printer.PaperSize = rpt.Document.Printer.PaperSizes[i];
            //		}
            //	}
            //}
        }

        public static void CheckMVR3Number()
        {
            string strSql = "";
            // vbPorter upgrade warning: strNumber As string	OnWrite(string, int)
            string strNumber = "";
            bool blnCheck = false;
            clsDRWrapper rsLastMVR3 = new clsDRWrapper();
            clsDRWrapper rsCheck = new clsDRWrapper();
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.OpenRecordset("SELECT * FROM WMV");
            if (FCConvert.ToInt32(tempRS.Get_Fields_Int16("MVR3Digits")) != 0)
            {
                return;
            }
            int tempPrinter = StaticSettings.GlobalCommandDispatcher.Send(new GetPrinterGroup()).Result;
            rsLastMVR3.OpenRecordset("SELECT * FROM LastMVRNumber WHERE PrinterNumber = " + FCConvert.ToString(tempPrinter));
            if (rsLastMVR3.EndOfFile())
            {
                blnCheck = true;
            }
            else
            {
                //FC:FINAL:MSH - i.issue #1766: wrong comparing
                //if (rsLastMVR3.Get_Fields("CheckDate") != DateTime.Today.ToOADate())
                if (rsLastMVR3.Get_Fields_DateTime("CheckDate").ToOADate() != DateTime.Today.ToOADate())
                {
                    blnCheck = true;
                }
            }
            if (blnCheck)
            {
                if (rsLastMVR3.EndOfFile())
                {
                    strNumber = "";
                }
                else
                {
                    strNumber = FCConvert.ToString(rsLastMVR3.Get_Fields_Int32("LastMVRNumber") + 1);
                }
            TryAgain:
                ;
                strNumber = Interaction.InputBox("Please verify that this is the next MVR3 number that should be used.  If the number shown is incorrect please enter the correct number.", "Enter MVR3 Number", strNumber);
                if (Strings.Trim(strNumber) == "")
                    goto TryAgain;
                rsCheck.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' AND Number = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(strNumber))));
                if (rsCheck.EndOfFile())
                {
                    MessageBox.Show("The MVR3 you entered has either already been issued or could not be found in inventory.  Please try again.", "Invalid MVR3 Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    goto TryAgain;
                }
                else
                {
                    if (rsLastMVR3.EndOfFile())
                    {
                        rsLastMVR3.AddNew();
                        rsLastMVR3.Set_Fields("PrinterNumber", tempPrinter);
                        rsLastMVR3.Set_Fields("LastMVRNumber", FCConvert.ToInt32(FCConvert.ToDouble(strNumber)) - 1);
                        rsLastMVR3.Set_Fields("CheckDate", DateTime.Today);
                    }
                    else
                    {
                        rsLastMVR3.Edit();
                        rsLastMVR3.Set_Fields("LastMVRNumber", FCConvert.ToInt32(FCConvert.ToDouble(strNumber)) - 1);
                        rsLastMVR3.Set_Fields("CheckDate", DateTime.Today);
                    }
                    rsLastMVR3.Update();
                }
            }
        }

        public static string CreateFormattedInventory(string strPre, string strNumber, string strSuff, string strType)
        {
            string CreateFormattedInventory = "";
            if (Strings.Left(strType, 2) != "PX" || strPre == "NEW")
            {
                CreateFormattedInventory = strPre + strNumber + strSuff;
            }
            else
            {
                if ((strType.Right(2) == "AC") || (strType.Right(2) == "AF") || (strType.Right(2) == "AW") || strType.Right(2) == "BB" || (strType.Right(2) == "BC") || strType.Right(2) == "BH" || (strType.Right(2) == "CV") || (strType.Right(2) == "LB") || (strType.Right(2) == "LS") || (strType.Right(2) == "TS") || (strType.Right(2) == "WB") || (strType.Right(2) == "LC") || (strType.Right(2) == "EM"))
                {
                    // tromvs-97 6.29.18
                    CreateFormattedInventory = strPre + strNumber + "-" + strSuff;
                }
                else if ((Strings.Right(strType, 2) == "SW") || (Strings.Right(strType, 2) == "AG"))
                {
                    if (Strings.Trim(strSuff) == "")
                    {
                        CreateFormattedInventory = strPre + strNumber + strSuff;
                    }
                    else
                    {
                        CreateFormattedInventory = strPre + strNumber + "-" + strSuff;
                    }
                }
                else if ((Strings.Right(strType, 2) == "AM") || (Strings.Right(strType, 2) == "AP") || (Strings.Right(strType, 2) == "AQ") || (Strings.Right(strType, 2) == "CI") || (Strings.Right(strType, 2) == "CM") || (Strings.Right(strType, 2) == "FM") || (Strings.Right(strType, 2) == "MC") || (Strings.Right(strType, 2) == "MM") || (Strings.Right(strType, 2) == "MP") || (Strings.Right(strType, 2) == "MQ") || (Strings.Right(strType, 2) == "SE") || (Strings.Right(strType, 2) == "SR") || (Strings.Right(strType, 2) == "TR") || (Strings.Right(strType, 2) == "TX"))
                {
                    // "BU"
                    if (Strings.Trim(strNumber).Length <= 4)
                    {
                        CreateFormattedInventory = strPre + strNumber + strSuff;
                    }
                    else if (Strings.Trim(strNumber).Length == 5)
                    {
                        CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 2) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                    }
                    else
                    {
                        CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 3) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                    }
                }
                else if ((Strings.Right(strType, 2) == "PC") || (Strings.Right(strType, 2) == "MH"))
                {
                    if (strSuff == "")
                    {
                        if (Strings.Trim(strNumber).Length <= 4)
                        {
                            CreateFormattedInventory = strPre + strNumber + strSuff;
                        }
                        else if (Strings.Trim(strNumber).Length == 5)
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 2) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                        else
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 3) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                    }
                    else
                    {
                        CreateFormattedInventory = strPre + strNumber + strSuff;
                    }
                }
                else if ((Strings.Right(strType, 2) == "CO") || (Strings.Right(strType, 2) == "TT"))
                {
                    if (strPre == "")
                    {
                        if (Strings.Trim(strNumber).Length <= 4)
                        {
                            CreateFormattedInventory = strPre + strNumber + strSuff;
                        }
                        else if (Strings.Trim(strNumber).Length == 5)
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 2) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                        else
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 3) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                    }
                    else
                    {
                        if (!Information.IsNumeric(strPre))
                        {
                            if (strPre.Length > 2)
                            {
                                CreateFormattedInventory = Strings.Left(strPre, 2) + "-" + Strings.Right(strPre, strPre.Length - 2) + strNumber + strSuff;
                            }
                            else
                            {
                                CreateFormattedInventory = strPre + "-" + strNumber + strSuff;
                            }
                        }
                        else
                        {
                            if (strPre.Length > 3)
                            {
                                CreateFormattedInventory = Strings.Left(strPre, 3) + "-" + Strings.Right(strPre, strPre.Length - 3) + strNumber + strSuff;
                            }
                            else
                            {
                                CreateFormattedInventory = strPre + "-" + strNumber + strSuff;
                            }
                        }
                    }
                }
                else if (Strings.Right(strType, 2) == "ST")
                {
                    if (strPre == "")
                    {
                        CreateFormattedInventory = strPre + strNumber + strSuff;
                    }
                    else
                    {
                        if (strPre.Length == 1)
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 2) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                        else
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 1) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                    }
                }
                else if (Strings.Right(strType, 2) == "TL")
                {
                    if (strPre == "")
                    {
                        if (Strings.Trim(strNumber).Length <= 4)
                        {
                            CreateFormattedInventory = strPre + strNumber + strSuff;
                        }
                        else if (Strings.Trim(strNumber).Length == 5)
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 2) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                        else if (Strings.Trim(strNumber).Length == 7 || (Strings.Trim(strNumber).Length == 6 && strSuff != ""))
                        {
                            CreateFormattedInventory = strPre + strNumber + strSuff;
                        }
                        else
                        {
                            CreateFormattedInventory = strPre + Strings.Left(Strings.Trim(strNumber), 3) + "-" + Strings.Right(Strings.Trim(strNumber), 3) + strSuff;
                        }
                    }
                    else
                    {
                        CreateFormattedInventory = strPre + strNumber + strSuff;
                    }
                }
                else if (Strings.Right(strType, 2) == "VT")
                {
                    if (strPre == "")
                    {
                        CreateFormattedInventory = strPre + strNumber + strSuff;
                    }
                    else
                    {
                        if (strPre.Length > 2)
                        {
                            CreateFormattedInventory = Strings.Left(strPre, 2) + "-" + Strings.Right(strPre, strPre.Length - 2) + strNumber + strSuff;
                        }
                        else
                        {
                            CreateFormattedInventory = strPre + "-" + strNumber + strSuff;
                        }
                    }
                }
                else if (Strings.Right(strType, 2) == "LT")
                {
                    CreateFormattedInventory = Strings.Left(strNumber, 2) + "-" + Strings.Right(strNumber, strNumber.Length - 2) + strSuff;
                }
                else
                {
                    CreateFormattedInventory = strPre + strNumber + strSuff;
                }
            }
            return CreateFormattedInventory;
        }

        public static bool IsMotorcycle(string strClass)
        {
            bool IsMotorcycle = false;
            if (strClass == "AU" || strClass == "MC" || strClass == "MX" || strClass == "PM" || strClass == "VM" || strClass == "XV")
            {
                IsMotorcycle = true;
            }
            else
            {
                IsMotorcycle = false;
            }
            return IsMotorcycle;
        }

        public static bool IsProratedMotorcycleExpirationDate(string strClass, DateTime datEffectiveDate, DateTime datExpiresDate, string strRegType)
        {
            bool IsProratedMotorcycleExpirationDate = false;
            if (IsMotorcycle(strClass))
            {
                if (strRegType == "RRR" && !Statics.gblnDelayedReg)
                {
                    if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2013").ToOADate() && datExpiresDate.ToOADate() < DateTime.Today.ToOADate())
                    {
                        if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2012").ToOADate())
                        {
                            IsProratedMotorcycleExpirationDate = false;
                        }
                        else
                        {
                            if (Statics.FixedMonth)
                            {
                                IsProratedMotorcycleExpirationDate = true;
                            }
                            else
                            {
                                IsProratedMotorcycleExpirationDate = false;
                            }
                        }
                    }
                    else
                    {
                        if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2012").ToOADate())
                        {
                            IsProratedMotorcycleExpirationDate = false;
                        }
                        else if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2013").ToOADate() && Statics.FixedMonth && datExpiresDate.Month != 3)
                        {
                            IsProratedMotorcycleExpirationDate = true;
                        }
                        else
                        {
                            IsProratedMotorcycleExpirationDate = false;
                        }
                    }
                }
                else if (strRegType == "CORR" && Statics.PlateType == 2)
                {
                    if (MotorVehicle.Statics.CorrectingNewRegMotorCycle)
                    {
                        if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate())
                        {
                            IsProratedMotorcycleExpirationDate = true;
                        }
                        else
                        {
                            IsProratedMotorcycleExpirationDate = false;
                        }
                    }
                }
                else if (strRegType == "NRR")
                {
                    if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate())
                    {
                        IsProratedMotorcycleExpirationDate = true;
                    }
                    else
                    {
                        IsProratedMotorcycleExpirationDate = false;
                    }
                }
                else if (Statics.gblnDelayedReg)
                {
                    if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate() && datEffectiveDate.ToOADate() <= FCConvert.ToDateTime("3/1/2013").ToOADate())
                    {
                        IsProratedMotorcycleExpirationDate = true;
                    }
                    else
                    {
                        IsProratedMotorcycleExpirationDate = false;
                    }
                }
                else if (strRegType == "NRT")
                {
                    if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate())
                    {
                        IsProratedMotorcycleExpirationDate = true;
                    }
                    else
                    {
                        IsProratedMotorcycleExpirationDate = false;
                    }
                }
                else if (strRegType == "RRT")
                {
                    if (datExpiresDate.ToOADate() > FCConvert.ToDateTime("3/31/2013").ToOADate() && datExpiresDate.Month != 3 && DateTime.Today.ToOADate() < FCConvert.ToDateTime("3/31/2013").ToOADate())
                    {
                        IsProratedMotorcycleExpirationDate = true;
                    }
                    else
                    {
                        IsProratedMotorcycleExpirationDate = false;
                    }
                }
                else
                {
                    IsProratedMotorcycleExpirationDate = false;
                }
            }
            else
            {
                IsProratedMotorcycleExpirationDate = false;
            }
            return IsProratedMotorcycleExpirationDate;
        }

        public static bool IsProratedMotorcycle(string strClass, DateTime datEffectiveDate, ref DateTime datExpiresDate, string strRegType, bool blnExpDateChange = false)
        {
            bool IsProratedMotorcycle = false;
            if (IsMotorcycle(strClass))
            {
                if ((strRegType == "RRR" && !Statics.gblnDelayedReg))
                {
                    if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2013").ToOADate() && datExpiresDate.ToOADate() < DateTime.Today.ToOADate())
                    {
                        if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2012").ToOADate())
                        {
                            IsProratedMotorcycle = false;
                        }
                        else
                        {
                            if (Statics.FixedMonth)
                            {
                                IsProratedMotorcycle = true;
                            }
                            else
                            {
                                IsProratedMotorcycle = false;
                            }
                        }
                    }
                    else
                    {
                        if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2012").ToOADate())
                        {
                            IsProratedMotorcycle = false;
                        }
                        else if (datExpiresDate.ToOADate() < FCConvert.ToDateTime("3/1/2013").ToOADate() && Statics.FixedMonth && datExpiresDate.Month != 3 && datExpiresDate.Month != 4)
                        {
                            IsProratedMotorcycle = true;
                        }
                        else
                        {
                            IsProratedMotorcycle = false;
                        }
                    }
                }
                else if (strRegType == "CORR")
                {
                    if (Statics.PlateType == 1)
                    {
                        if (Statics.rsFinalCompare.Get_Fields_Boolean("RegProrate") == true || Statics.rsFinalCompare.Get_Fields_Boolean("ExciseProrate") == true)
                        {
                            IsProratedMotorcycle = true;
                        }
                        else
                        {
                            IsProratedMotorcycle = false;
                        }
                    }
                    else if (Statics.PlateType == 2)
                    {
                        if (MotorVehicle.Statics.CorrectingNewRegMotorCycle)
                        {
                            if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate())
                            {
                                IsProratedMotorcycle = true;
                            }
                            else
                            {
                                IsProratedMotorcycle = false;
                            }
                        }
                    }
                    
                }
                else if (strRegType == "NRR")
                {
                    if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate())
                    {
                        IsProratedMotorcycle = true;
                    }
                    else
                    {
                        IsProratedMotorcycle = false;
                    }
                }
                else if (Statics.gblnDelayedReg)
                {
                    if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate() && datEffectiveDate.ToOADate() <= FCConvert.ToDateTime("3/1/2013").ToOADate())
                    {
                        IsProratedMotorcycle = true;
                    }
                    else
                    {
                        IsProratedMotorcycle = false;
                    }
                }
                else if (strRegType == "NRT")
                {
                    if (datEffectiveDate.Month != 3 && datEffectiveDate.ToOADate() >= FCConvert.ToDateTime("3/1/2012").ToOADate())
                    {
                        IsProratedMotorcycle = true;
                    }
                    else if (Statics.rsFinalCompare.Get_Fields_Boolean("RegProrate") == true)
                    {
                        IsProratedMotorcycle = true;
                    }
                    else
                    {
                        IsProratedMotorcycle = false;
                    }
                }
                else if (strRegType == "RRT")
                {
                    if (datExpiresDate.ToOADate() > FCConvert.ToDateTime("3/31/2013").ToOADate() && FCConvert.ToBoolean(datExpiresDate.Month) & DateTime.Today.ToOADate() < FCConvert.ToDateTime("3/31/2013").ToOADate())
                    {
                        IsProratedMotorcycle = true;
                    }
                    else if (Statics.rsFinalCompare.Get_Fields_Boolean("RegProrate") == true)
                    {
                        IsProratedMotorcycle = true;
                    }
                    else
                    {
                        IsProratedMotorcycle = false;
                    }
                }
                else
                {
                    IsProratedMotorcycle = false;
                }
            }
            else
            {
                IsProratedMotorcycle = false;
            }
            return IsProratedMotorcycle;
        }

        public static bool LockInventory(string strPath = "")
        {
            bool LockInventory = false;
            clsDRWrapper rsTemp = new clsDRWrapper();
            if (strPath != String.Empty)
            {
                rsTemp.GroupName = strPath;
            }
            rsTemp.OpenRecordset("SELECT * FROM InventoryLock", "TWMV0000.vb1");
            if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
            {
                if (rsTemp.Get_Fields_Boolean("MasterLock") == false)
                {
                    if (rsTemp.Edit())
                    {
                        rsTemp.Set_Fields("MasterLock", true);
                        rsTemp.Set_Fields("OpID", modGlobalConstants.Statics.gstrUserID);
                        if (rsTemp.Update(false))
                        {
                            LockInventory = true;
                        }
                        else
                        {
                            LockInventory = false;
                        }
                    }
                }
                else
                {
                    LockInventory = false;
                    Statics.strInventoryLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
                    rsTemp.Reset();
                    return LockInventory;
                }
            }
            else
            {
                rsTemp.AddNew();
                rsTemp.Set_Fields("MasterLock", true);
                rsTemp.Set_Fields("OpID", modGlobalConstants.Statics.gstrUserID);
                if (rsTemp.Update(false))
                {
                    LockInventory = true;
                }
                else
                {
                    LockInventory = false;
                }
            }
            rsTemp.OpenRecordset("SELECT * FROM InventoryLock", "TWMV0000.vb1");
            if (FCConvert.ToString(rsTemp.Get_Fields_String("OpID")) != modGlobalConstants.Statics.gstrUserID)
            {
                LockInventory = false;
                Statics.strInventoryLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
            }
            return LockInventory;
        }

        public static void UnlockInventory(string strPath = "")
        {
            clsDRWrapper rsTemp = new clsDRWrapper();
            if (strPath != String.Empty)
            {
                rsTemp.GroupName = strPath;
            }
            rsTemp.OpenRecordset("SELECT * FROM InventoryLock", "TWMV0000.vb1");
            rsTemp.Edit();
            rsTemp.Set_Fields("MasterLock", false);
            rsTemp.Set_Fields("OpID", "");
            rsTemp.Update(true);
        }
        // vbPorter upgrade warning: intYear As int	OnWriteFCConvert.ToInt32(
        public static string GetLongTermPlate(int intYear, bool blnShowMessage = true)
        {
            string GetLongTermPlate = "";
            clsDRWrapper rs = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                GetLongTermPlate = "";
                if (LockInventory() == false)
                {
                    UnlockInventory();
                    if (blnShowMessage)
                    {
                        MessageBox.Show("User " + Statics.strInventoryLockedBy + " is currently trying to retrieve a long termplate.  You must wait until they are finished to continue.", "Unable to Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    rs.OpenRecordset("SELECT * FROM Inventory WHERE convert(int, left([Number], 2)) = " + Strings.Right(Strings.Trim(intYear.ToString()), 2) + " AND Code = 'PXSLT' AND Status = 'A' AND Pending <> 1 ORDER BY convert(nvarchar,[Number]) + Suffix", "TWMV0000.vb1");
                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        GetLongTermPlate = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FormattedInventory")));
                        rs.Edit();
                        rs.Set_Fields("Pending", true);
                        rs.Update();
                        UnlockInventory();
                    }
                    else
                    {
                        UnlockInventory();
                        if (blnShowMessage)
                        {
                            MessageBox.Show("No long term plate found for the year " + FCConvert.ToString(intYear), "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                return GetLongTermPlate;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                UnlockInventory();
                if (blnShowMessage)
                {
                    MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            return GetLongTermPlate;
        }

        public static bool ReturnLongTermPlate(string strPlate)
        {
            bool ReturnLongTermPlate = false;
            clsDRWrapper rs = new clsDRWrapper();
            rs.OpenRecordset("SELECT * FROM Inventory WHERE Prefix + convert(nvarchar, Number) + Suffix = '" + strPlate.Replace("-", "") + "' AND Code = 'PXSLT' AND Status = 'A' AND Pending = 1", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
                rs.Set_Fields("Pending", false);
                rs.Update();
                ReturnLongTermPlate = true;
            }
            else
            {
                MessageBox.Show("Unable to return long term plate " + strPlate + " to inventory.", "Unable to Return Plate", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ReturnLongTermPlate = false;
            }
            return ReturnLongTermPlate;
        }

        public static bool LockFleet()
        {
            bool LockFleet = false;
            clsDRWrapper rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset("SELECT * FROM FleetLock", "TWMV0000.vb1");
            if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
            {
                if (rsTemp.Get_Fields_Boolean("MasterLock") == false)
                {
                    rsTemp.Edit();
                    rsTemp.Set_Fields("MasterLock", true);
                    rsTemp.Set_Fields("OpID", modGlobalConstants.Statics.gstrUserID);
                    if (rsTemp.Update(false))
                    {
                        LockFleet = true;
                    }
                    else
                    {
                        LockFleet = false;
                    }
                }
                else
                {
                    LockFleet = false;
                    modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
                    rsTemp.Reset();
                    return LockFleet;
                }
            }
            else
            {
                rsTemp.AddNew();
                rsTemp.Set_Fields("MasterLock", true);
                rsTemp.Set_Fields("OpID", modGlobalConstants.Statics.gstrUserID);
                if (rsTemp.Update(false))
                {
                    LockFleet = true;
                }
                else
                {
                    LockFleet = false;
                }
            }
            rsTemp.OpenRecordset("SELECT * FROM FleetLock", "TWMV0000.vb1");
            if (FCConvert.ToString(rsTemp.Get_Fields_String("OpID")) != modGlobalConstants.Statics.gstrUserID)
            {
                LockFleet = false;
                modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
            }
            rsTemp.Reset();
            return LockFleet;
        }

        public static void UnlockFleet()
        {
            clsDRWrapper rsTemp = new clsDRWrapper();
            rsTemp.OpenRecordset("SELECT * FROM FleetLock", "TWMV0000.vb1");
            rsTemp.Edit();
            rsTemp.Set_Fields("MasterLock", false);
            rsTemp.Set_Fields("OpID", "");
            rsTemp.Update(true);
            rsTemp.Reset();
        }

        public static void AddAntiqueMotorcycleCategory()
        {
            // kk11192015 tromv-55  Part of legislative changes for 10/2015
            clsDRWrapper rs = new clsDRWrapper();
            // Add new Antique Motorcycle category after the Antique category
            rs.OpenRecordset("SELECT * FROM TownSummaryHeadings WHERE CategoryCode = 17", "TWMV0000.vb1");
            if (rs.EndOfFile() != true)
            {
                // do nothing
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("Heading", "ANTIQUE MOTORCYCLE");
                rs.Set_Fields("CategoryCode", 17);
                rs.Update(true);
            }
            rs.Execute("UPDATE Class SET ShortDescription = 17 WHERE BMVCode = 'MQ' AND SystemCode = 'MQ'", "TWMV0000.vb1");
        }

        public static void UpdateAntiqueAutoFee()
        {
            // kk11192015 tromvs-55  Part of legislative changes for 10/2015
            clsDRWrapper rs = new clsDRWrapper();
            rs.Execute("UPDATE Class SET RegistrationFee = 30 WHERE BMVCode = 'AQ' AND SystemCode = 'AQ'", "TWMV0000.vb1");
        }

        public static void RemoveSMEBFClass()
        {
            // kk11192015 tromvs-55  Part of legislative changes for 10/2015
            clsDRWrapper rs = new clsDRWrapper();
            // Remove the SME B/F class
            rs.Execute("DELETE FROM Class WHERE BMVCode = 'TR' AND SystemCode = 'T3'", "TWMV0000.vb1");
            // Change SME Class B/T to just Class B
            rs.Execute("UPDATE Class SET Description ='SPECIAL MOBILE EQUIPMENT (CLASS B)' WHERE BMVCode = 'TR' AND SystemCode = 'T2'", "TWMV0000.vb1");
            // Update SME Class B/F so they can be reclassed as Class A
            rs.Execute("UPDATE Master SET Subclass = '!!' WHERE Class = 'TR' AND Subclass = 'T3'", "TWMV0000.vb1");
            rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'TR' AND Subclass = 'T3'", "TWMV0000.vb1");
        }

        public static void AddPassengerTruckCategory()
        {
            // kk11192015 tromvs-55  Part of legislative changes for 10/2015
            clsDRWrapper rs = new clsDRWrapper();
            // kk11102016 tromv-1235   This should only happen when the Passenger Truck category is being added
            // Change all existing PC: P3, P4, P5 and P6
            // rs.Execute "UPDATE Master SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Master SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Master SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Master SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1"
            // 
            // rs.Execute "UPDATE Master SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Master SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Master SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Master SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1"
            // rs.Execute "UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1"
            // rs.Execute "UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1"
            // Add new passenger truck category before the commercial category
            rs.OpenRecordset("SELECT * FROM TownSummaryHeadings WHERE CategoryCode = 39", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // do nothing
            }
            else
            {
                // Change all existing PC: P3, P4, P5 and P6     -  'kk11102016 tromv-1235   This should only happen when the Passenger Truck category is being added
                rs.Execute("UPDATE Master SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1");
                rs.Execute("UPDATE Master SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1");
                rs.Execute("UPDATE Master SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1");
                rs.Execute("UPDATE Master SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = 'P7' WHERE Class = 'PC' AND Subclass = 'P6'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = 'P6' WHERE Class = 'PC' AND Subclass = 'P5'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = 'P5' WHERE Class = 'PC' AND Subclass = 'P4'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P3'", "TWMV0000.vb1");
                rs.Execute("UPDATE Master SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1");
                rs.Execute("UPDATE Master SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1");
                rs.Execute("UPDATE Master SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1");
                rs.Execute("UPDATE Master SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1");
                rs.Execute("UPDATE ActivityMaster SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1");
                rs.Execute("UPDATE HeldRegistrationMaster SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'CM' AND Subclass = 'CM'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'DX' AND Subclass = 'DX'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'GS' AND Subclass = 'GS'", "TWMV0000.vb1");
                rs.Execute("UPDATE ArchiveMaster SET Subclass = '!!' WHERE Class = 'UM' AND Subclass = 'UM'", "TWMV0000.vb1");
                rs.AddNew();
                rs.Set_Fields("Heading", "PASSENGER TRUCK");
                rs.Set_Fields("CategoryCode", 39);
                rs.Update(true);
            }
            // Update the category on existing 10000 lbs gvw subclass of passenger type classes
            // kk01042018 tromvs-96 DS, DV, VT and VX Change to Commercial, Comment out here
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'AG' AND SystemCode = 'A2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'AW' AND SystemCode = 'A2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'BB' AND SystemCode = 'B2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'BC' AND SystemCode = 'B2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'CD' AND SystemCode = 'C2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'CR' AND SystemCode = 'C2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'CV' AND SystemCode = 'C2'", "TWMV0000.vb1");
            // rs.Execute "UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'DS' AND SystemCode = 'D2'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'DV' AND (SystemCode = 'D2' OR SystemCode = 'D4')", "TWMV0000.vb1"
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'FD' AND SystemCode = 'F2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'LB' AND SystemCode = 'L2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'MO' AND SystemCode = 'M2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'PH' AND SystemCode = 'P2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'PO' AND SystemCode = 'P2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'PS' AND SystemCode = 'P2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'SW' AND SystemCode = 'S2'", "TWMV0000.vb1");
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'TS' AND SystemCode = 'S2'", "TWMV0000.vb1");
            // rs.Execute "UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'VT' AND SystemCode = 'V2'", "TWMV0000.vb1"
            // rs.Execute "UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'VX' AND (SystemCode = 'V2' OR SystemCode = 'V4')", "TWMV0000.vb1"
            rs.Execute("UPDATE Class SET ShortDescription = 39 WHERE BMVCode = 'WB' AND SystemCode = 'W2'", "TWMV0000.vb1");
            // Check for Duplicate CV-C1 Class
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CV' AND SystemCode = 'C1'", "TWMV0000.vb1");
            if (rs.RecordCount() > 1)
            {
                rs.Delete();
                rs.Update();
            }
            // Divide the existing CM, DX, GS and UM Classes into 6000 and 10000 gvw
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CM' AND (SystemCode = 'CM' OR SystemCode = 'C1')", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                if (FCConvert.ToString(rs.Get_Fields_String("SystemCode")) == "CM")
                {
                    rs.Edit();
                    rs.Set_Fields("SystemCode", "C1");
                    rs.Set_Fields("Description", "COMBINATION (6000 LBS GVW)");
                    rs.Update();
                }
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "C1");
                rs.Set_Fields("BMVCode", "CM");
                rs.Set_Fields("Description", "COMBINATION (6000 LBS GVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 35);
                rs.Set_Fields("OtherFeesNew", 0);
                rs.Set_Fields("Otherfeesrenew", 0);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 1);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "N");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 5);
                rs.Set_Fields("IssueNewPlate", true);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CM' AND SystemCode = 'C2'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
            }
            else
            {
                rs.AddNew();
            }
            rs.Set_Fields("SystemCode", "C2");
            rs.Set_Fields("BMVCode", "CM");
            rs.Set_Fields("Description", "COMBINATION (10000 LBS GVW)");
            rs.Set_Fields("ExciseRequired", "Y");
            rs.Set_Fields("RegistrationFee", 37);
            rs.Set_Fields("OtherFeesNew", 0);
            rs.Set_Fields("Otherfeesrenew", 0);
            rs.Set_Fields("HalfRateAllowed", "Y");
            rs.Set_Fields("TwoYear", "N");
            rs.Set_Fields("LocationNew", 1);
            rs.Set_Fields("LocationTransfer", 1);
            rs.Set_Fields("LocationRenewal", 1);
            rs.Set_Fields("NumberOfPlateStickers", 2);
            rs.Set_Fields("RenewalDate", "S");
            rs.Set_Fields("TitleRequired", "R");
            rs.Set_Fields("Emission", "N");
            rs.Set_Fields("InsuranceRequired", "Y");
            rs.Set_Fields("ShortDescription", 39);
            rs.Set_Fields("IssueNewPlate", true);
            rs.Update();
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DX' AND (SystemCode = 'DX' OR SystemCode = 'D1')", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                if (FCConvert.ToString(rs.Get_Fields_String("SystemCode")) == "DX")
                {
                    rs.Edit();
                    rs.Set_Fields("SystemCode", "D1");
                    rs.Set_Fields("Description", "DISABLED (6000 LBS GVW)");
                    rs.Update();
                }
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "D1");
                rs.Set_Fields("BMVCode", "DX");
                rs.Set_Fields("Description", "DISABLED (6000 LBS GVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 35);
                rs.Set_Fields("OtherFeesNew", 0);
                rs.Set_Fields("Otherfeesrenew", 0);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 2);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "N");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 5);
                rs.Set_Fields("IssueNewPlate", true);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DX' AND SystemCode = 'D2'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // do nothing
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "D2");
                rs.Set_Fields("BMVCode", "DX");
                rs.Set_Fields("Description", "DISABLED (10000 LBS GVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 37);
                rs.Set_Fields("OtherFeesNew", 0);
                rs.Set_Fields("Otherfeesrenew", 0);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 2);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "N");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 39);
                rs.Set_Fields("IssueNewPlate", true);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'GS' AND (SystemCode = 'GS' OR SystemCode = 'G1')", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                if (FCConvert.ToString(rs.Get_Fields_String("SystemCode")) == "GS")
                {
                    rs.Edit();
                    rs.Set_Fields("SystemCode", "G1");
                    rs.Set_Fields("Description", "GOLD STAR FAMILY  (6000 LBS GVW)");
                    rs.Update();
                }
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "G1");
                rs.Set_Fields("BMVCode", "GS");
                rs.Set_Fields("Description", "GOLD STAR FAMILY  (6000 LBS GVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 35);
                rs.Set_Fields("OtherFeesNew", 0);
                rs.Set_Fields("Otherfeesrenew", 0);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 3);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "R");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 5);
                rs.Set_Fields("IssueNewPlate", false);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'GS' AND SystemCode = 'G2'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // do nothing
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "G2");
                rs.Set_Fields("BMVCode", "GS");
                rs.Set_Fields("Description", "GOLD STAR FAMILY (10000 LBS GVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 37);
                rs.Set_Fields("OtherFeesNew", 0);
                rs.Set_Fields("Otherfeesrenew", 0);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 3);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "R");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 39);
                rs.Set_Fields("IssueNewPlate", false);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'UM' AND (SystemCode = 'UM' OR SystemCode = 'U1')", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                if (FCConvert.ToString(rs.Get_Fields_String("SystemCode")) == "UM")
                {
                    rs.Edit();
                    rs.Set_Fields("SystemCode", "U1");
                    rs.Set_Fields("Description", "UNIVERSITY OF MAINE (6000 LBS GVW)");
                    rs.Update();
                }
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "U1");
                rs.Set_Fields("BMVCode", "UM");
                rs.Set_Fields("Description", "UNIVERSITY OF MAINE (6000 LBS GVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 35);
                rs.Set_Fields("OtherFeesNew", 20);
                rs.Set_Fields("Otherfeesrenew", 15);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 1);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "N");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 5);
                rs.Set_Fields("IssueNewPlate", false);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'UM' AND SystemCode = 'U2'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // do nothing
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "U2");
                rs.Set_Fields("BMVCode", "UM");
                rs.Set_Fields("Description", "UNIVERSITY OF MAINE (10000 LBS GVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 37);
                rs.Set_Fields("OtherFeesNew", 20);
                rs.Set_Fields("OtherFeesRenew", 15);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 1);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "N");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 39);
                rs.Set_Fields("IssueNewPlate", false);
                rs.Update();
            }
            // Now deal with the PC class - Passenger Vehicle 6000 and 10000, and National Guard 6000 and 10000
            // PC - P1 = STOCK CAR
            // P2 = PASSENGER VEHICLE
            // P3 = DUNE BUGGY (OFF ROAD)
            // P4 = RENTAL AT MUNICIPALITY
            // P5 = RENTAL AT BMV BRANCH
            // P6 = NATIONAL GUARD PLATE
            // Need to split:
            // PASSENGER VEHICLE -
            // PASSENGER (6000 LBS GVW)
            // PASSENGER (10000 LBS GVW)
            // NATIONAL GUARD PLATE -
            // NATIONAL GUARD (6000 LBS GVW)
            // NATIONAL GUARD (10000 LBS GVW)
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'PC' AND SystemCode = 'P8'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // Do nothing
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "P8");
                rs.Set_Fields("BMVCode", "PC");
                rs.Set_Fields("Description", "NATIONAL GUARD (10000 LBS RVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 37);
                rs.Set_Fields("OtherFeesNew", 0);
                rs.Set_Fields("OtherFeesRenew", 0);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 3);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "N");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 39);
                rs.Set_Fields("IssueNewPlate", true);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'PC' AND SystemCode = 'P7'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // Do nothing
            }
            else
            {
                rs.AddNew();
                rs.Set_Fields("SystemCode", "P7");
                rs.Set_Fields("BMVCode", "PC");
                rs.Set_Fields("Description", "NATIONAL GUARD (6000 LBS RVW)");
                rs.Set_Fields("ExciseRequired", "Y");
                rs.Set_Fields("RegistrationFee", 35);
                rs.Set_Fields("OtherFeesNew", 0);
                rs.Set_Fields("OtherFeesRenew", 0);
                rs.Set_Fields("HalfRateAllowed", "Y");
                rs.Set_Fields("TwoYear", "N");
                rs.Set_Fields("LocationNew", 3);
                rs.Set_Fields("LocationTransfer", 1);
                rs.Set_Fields("LocationRenewal", 1);
                rs.Set_Fields("NumberOfPlateStickers", 2);
                rs.Set_Fields("RenewalDate", "S");
                rs.Set_Fields("TitleRequired", "R");
                rs.Set_Fields("Emission", "N");
                rs.Set_Fields("InsuranceRequired", "Y");
                rs.Set_Fields("ShortDescription", 5);
                rs.Set_Fields("IssueNewPlate", true);
                rs.Update();
            }
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'PC' AND SystemCode = 'P6'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
            }
            else
            {
                rs.AddNew();
            }
            rs.Set_Fields("SystemCode", "P6");
            rs.Set_Fields("BMVCode", "PC");
            rs.Set_Fields("Description", "RENTAL AT BMV BRANCH");
            rs.Set_Fields("ExciseRequired", "Y");
            rs.Set_Fields("RegistrationFee", 50);
            rs.Set_Fields("OtherFeesNew", 0);
            rs.Set_Fields("OtherFeesRenew", 0);
            rs.Set_Fields("HalfRateAllowed", "Y");
            rs.Set_Fields("TwoYear", "N");
            rs.Set_Fields("LocationNew", 3);
            rs.Set_Fields("LocationTransfer", 1);
            rs.Set_Fields("LocationRenewal", 1);
            rs.Set_Fields("NumberOfPlateStickers", 2);
            rs.Set_Fields("RenewalDate", "S");
            rs.Set_Fields("TitleRequired", "R");
            rs.Set_Fields("Emission", "N");
            rs.Set_Fields("InsuranceRequired", "Y");
            rs.Set_Fields("ShortDescription", 25);
            rs.Set_Fields("IssueNewPlate", true);
            rs.Update();
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'PC' AND SystemCode = 'P5'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
            }
            else
            {
                rs.AddNew();
            }
            rs.Set_Fields("SystemCode", "P5");
            rs.Set_Fields("BMVCode", "PC");
            rs.Set_Fields("Description", "RENTAL AT MUNICIPALITY");
            rs.Set_Fields("ExciseRequired", "Y");
            rs.Set_Fields("RegistrationFee", 70);
            rs.Set_Fields("OtherFeesNew", 0);
            rs.Set_Fields("OtherFeesRenew", 0);
            rs.Set_Fields("HalfRateAllowed", "Y");
            rs.Set_Fields("TwoYear", "N");
            rs.Set_Fields("LocationNew", 1);
            rs.Set_Fields("LocationTransfer", 1);
            rs.Set_Fields("LocationRenewal", 1);
            rs.Set_Fields("NumberOfPlateStickers", 2);
            rs.Set_Fields("RenewalDate", "S");
            rs.Set_Fields("TitleRequired", "R");
            rs.Set_Fields("Emission", "N");
            rs.Set_Fields("InsuranceRequired", "Y");
            rs.Set_Fields("ShortDescription", 25);
            rs.Set_Fields("IssueNewPlate", true);
            rs.Update();
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'PC' AND SystemCode = 'P4'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
            }
            else
            {
                rs.AddNew();
            }
            rs.Set_Fields("SystemCode", "P4");
            rs.Set_Fields("BMVCode", "PC");
            rs.Set_Fields("Description", "DUNE BUGGY (OFF ROAD)");
            rs.Set_Fields("ExciseRequired", "Y");
            rs.Set_Fields("RegistrationFee", 7);
            rs.Set_Fields("OtherFeesNew", 0);
            rs.Set_Fields("OtherFeesRenew", 0);
            rs.Set_Fields("HalfRateAllowed", "Y");
            rs.Set_Fields("TwoYear", "N");
            rs.Set_Fields("LocationNew", 1);
            rs.Set_Fields("LocationTransfer", 1);
            rs.Set_Fields("LocationRenewal", 1);
            rs.Set_Fields("NumberOfPlateStickers", 2);
            rs.Set_Fields("RenewalDate", "S");
            rs.Set_Fields("TitleRequired", "R");
            rs.Set_Fields("Emission", "N");
            rs.Set_Fields("InsuranceRequired", "Y");
            rs.Set_Fields("ShortDescription", 5);
            rs.Set_Fields("IssueNewPlate", true);
            rs.Update();
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'PC' AND SystemCode = 'P3'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
            }
            else
            {
                rs.AddNew();
            }
            rs.Set_Fields("SystemCode", "P3");
            rs.Set_Fields("BMVCode", "PC");
            rs.Set_Fields("Description", "PASSENGER (10000 LBS RVW)");
            rs.Set_Fields("ExciseRequired", "Y");
            rs.Set_Fields("RegistrationFee", 37);
            rs.Set_Fields("OtherFeesNew", 0);
            rs.Set_Fields("OtherFeesRenew", 0);
            rs.Set_Fields("HalfRateAllowed", "Y");
            rs.Set_Fields("TwoYear", "N");
            rs.Set_Fields("LocationNew", 1);
            rs.Set_Fields("LocationTransfer", 1);
            rs.Set_Fields("LocationRenewal", 1);
            rs.Set_Fields("NumberOfPlateStickers", 2);
            rs.Set_Fields("RenewalDate", "S");
            rs.Set_Fields("TitleRequired", "R");
            rs.Set_Fields("Emission", "N");
            rs.Set_Fields("InsuranceRequired", "Y");
            rs.Set_Fields("ShortDescription", 39);
            rs.Set_Fields("IssueNewPlate", true);
            rs.Update();
            rs.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'PC' AND SystemCode = 'P2'", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
            }
            else
            {
                rs.AddNew();
            }
            rs.Set_Fields("SystemCode", "P2");
            rs.Set_Fields("BMVCode", "PC");
            rs.Set_Fields("Description", "PASSENGER (6000 LBS RVW)");
            rs.Set_Fields("ExciseRequired", "Y");
            rs.Set_Fields("RegistrationFee", 35);
            rs.Set_Fields("OtherFeesNew", 0);
            rs.Set_Fields("OtherFeesRenew", 0);
            rs.Set_Fields("HalfRateAllowed", "Y");
            rs.Set_Fields("TwoYear", "N");
            rs.Set_Fields("LocationNew", 1);
            rs.Set_Fields("LocationTransfer", 1);
            rs.Set_Fields("LocationRenewal", 1);
            rs.Set_Fields("NumberOfPlateStickers", 2);
            rs.Set_Fields("RenewalDate", "S");
            rs.Set_Fields("TitleRequired", "R");
            rs.Set_Fields("Emission", "N");
            rs.Set_Fields("InsuranceRequired", "Y");
            rs.Set_Fields("ShortDescription", 5);
            rs.Set_Fields("IssueNewPlate", true);
            rs.Update();
        }
        // Public Sub UpdateExceptionItemTypes()
        // Dim rs As New clsDRWrapper
        //
        // rs.Execute "UPDATE ExceptionReport SET Type = '6PM' WHERE Type = '4PM'", "TWMV0000.vb1"
        // End Sub
        // Public Sub DeleteNoFeeDuplicateExceptionRecords()
        // Dim rs As New clsDRWrapper
        //
        // rs.Execute "DELETE FROM ExceptionReport WHERE Type = '4CN' AND Reason = 'No Fee Duplicate; Rental Vehicle'", "TWMV0000.vb1"
        // End Sub
        public static void UpdateReasonCodes()
        {
            clsDRWrapper rsInfo = new clsDRWrapper();
            int counter;
            rsInfo.OpenRecordset("SELECT * FROM ReasonCodes ORDER BY Code");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                do
                {
                    rsInfo.Edit();
                    string vbPorterVar = rsInfo.Get_Fields("Code");
                    if (vbPorterVar == "3")
                    {
                        rsInfo.Set_Fields("Priority", "01");
                    }
                    else if (vbPorterVar == "D")
                    {
                        rsInfo.Set_Fields("Priority", "02");
                    }
                    else if (vbPorterVar == "4")
                    {
                        rsInfo.Set_Fields("Priority", "03");
                    }
                    else if (vbPorterVar == "P")
                    {
                        rsInfo.Set_Fields("Priority", "04");
                    }
                    else if (vbPorterVar == "R")
                    {
                        rsInfo.Set_Fields("Priority", "05");
                    }
                    else if (vbPorterVar == "L")
                    {
                        rsInfo.Set_Fields("Priority", "06");
                    }
                    else if (vbPorterVar == "#")
                    {
                        rsInfo.Set_Fields("Priority", "07");
                    }
                    else if (vbPorterVar == "Z")
                    {
                        rsInfo.Set_Fields("Priority", "08");
                    }
                    else if (vbPorterVar == "S")
                    {
                        rsInfo.Set_Fields("Priority", "09");
                    }
                    else if (vbPorterVar == "E")
                    {
                        rsInfo.Set_Fields("Priority", "10");
                    }
                    else if (vbPorterVar == "X")
                    {
                        rsInfo.Set_Fields("Priority", "11");
                    }
                    else if (vbPorterVar == "W")
                    {
                        rsInfo.Set_Fields("Priority", "12");
                    }
                    else if (vbPorterVar == "G")
                    {
                        rsInfo.Set_Fields("Priority", "13");
                    }
                    else if (vbPorterVar == "A")
                    {
                        rsInfo.Set_Fields("Priority", "14");
                    }
                    else if (vbPorterVar == "V")
                    {
                        rsInfo.Set_Fields("Priority", "15");
                    }
                    else if (vbPorterVar == "U")
                    {
                        rsInfo.Set_Fields("Priority", "16");
                    }
                    else if (vbPorterVar == "2")
                    {
                        // kk09302016  Force R2 to the end
                        rsInfo.Set_Fields("Priority", "999");
                    }
                    else
                    {
                        rsInfo.Set_Fields("Priority", "99");
                    }
                    rsInfo.Update();
                    rsInfo.MoveNext();
                }
                while (rsInfo.EndOfFile() != true);
            }
        }

        public static void SetPrintPriorityCodes()
        {
            clsDRWrapper rs = new clsDRWrapper();
            int counter;
            string strPriority = "";
            clsPrintCodes oPrtCodes = new clsPrintCodes();
            // kk07132016 tromv-57  Changed these over to use the clsPrtCodes class
            // rs.Execute "DELETE FROM PrintPriority WHERE convert(int, isnull(Priority, 0)) > 57", "TWMV0000.vb1"
            rs.Execute("DELETE FROM PrintPriority WHERE CONVERT(INT, Priority) > " + FCConvert.ToString(oPrtCodes.MaxPrintPriority), "TWMV0000.vb1");
            rs.OpenRecordset("SELECT * FROM PrintPriority");
            // For counter = 1 To 57
            for (counter = 1; counter <= oPrtCodes.MaxPrintPriority; counter++)
            {
                strPriority = Strings.Format(counter, "00");
                if (rs.FindFirstRecord("Priority", strPriority))
                {
                    rs.Edit();
                }
                else
                {
                    rs.AddNew();
                }
                rs.Set_Fields("Priority", Strings.Format(counter, "00"));
                rs.Set_Fields("Code", Strings.Format(counter, "00"));
                rs.Set_Fields("Description", oPrtCodes.ReturnPrintPriorityText(counter));
                // ReturnPrintPriorityText(counter)
                rs.Set_Fields("Include", oPrtCodes.ReturnPrintPriorityInclude(counter));
                // ReturnPrintPriorityInclude(counter)
                rs.Update();
            }
        }
        // kk07132016 tromvs-57  Moved Print Priority and Veteran Decals to clsPrintCodes
        // Public Function ReturnPrintPriorityInclude(intPrintPriority As Integer) As Boolean
        // Select Case intPrintPriority
        // Case 26, 35, 36, 38, 48, 56, 57
        // ReturnPrintPriorityInclude = True
        // Case Else
        // ReturnPrintPriorityInclude = False
        // End Select
        // End Function
        //
        // Public Function ReturnPrintPriorityText(intPriority As Integer) As String
        // Select Case intPriority
        // Case 1
        // ReturnPrintPriorityText = "OWN POWER ON HIGHWAY"
        // Case 2
        // ReturnPrintPriorityText = "DO NOT OPERATE UNDER"
        // Case 3
        // ReturnPrintPriorityText = "DUNE BUGGY/OFF ROAD"
        // Case 4
        // ReturnPrintPriorityText = "LBS WITHOUT PERMIT"
        // Case 5
        // ReturnPrintPriorityText = "NOT VALID OVER 20000"
        // Case 6
        // ReturnPrintPriorityText = "MODIFIED VEHICLE"
        // Case 7
        // ReturnPrintPriorityText = "RESERVE NO FEE $"
        // Case 8
        // ReturnPrintPriorityText = "RESERVE NO FEE PAID"
        // Case 9
        // ReturnPrintPriorityText = "VANITY PLATE FEE PD"
        // Case 10
        // ReturnPrintPriorityText = "TYPE OF VETERAN DECAL"
        // Case 11
        // ReturnPrintPriorityText = "VANITY PLATE $"
        // Case 12
        // ReturnPrintPriorityText = "VANITY PLATE $"
        // Case 13
        // ReturnPrintPriorityText = "REPLACEMENT PL $"
        // Case 14
        // ReturnPrintPriorityText = "DUP REG $"
        // Case 15
        // ReturnPrintPriorityText = "DUP STICKER $"
        // Case 16
        // ReturnPrintPriorityText = "DUP DECAL"
        // Case 17
        // ReturnPrintPriorityText = "LOST PLATE $"
        // Case 18
        // ReturnPrintPriorityText = "LOST PL/ST $"
        // Case 19
        // ReturnPrintPriorityText = "SPEC PL PD VCR# "
        // Case 20
        // ReturnPrintPriorityText = "SPECIALTY PL $"
        // Case 21
        // ReturnPrintPriorityText = "EXCISE TAX REBATE"
        // Case 22
        // ReturnPrintPriorityText = "PLATE CHANGE"
        // Case 23
        // ReturnPrintPriorityText = "FARM USE ONLY"
        // Case 24
        // ReturnPrintPriorityText = "TRANSFER"
        // Case 25
        // ReturnPrintPriorityText = "NEW PLATE"
        // Case 26
        // ReturnPrintPriorityText = "CHANGE OF ADDRESS"
        // Case 27
        // ReturnPrintPriorityText = "MUNICIPAL VEHICLE"
        // Case 28
        // ReturnPrintPriorityText = "NEW REG/OLD PLATE"
        // Case 29
        // ReturnPrintPriorityText = "NEW EXPIRATION DATE"
        // Case 30
        // ReturnPrintPriorityText = "RENTAL VEHICLE"
        // Case 31
        // ReturnPrintPriorityText = "REG VEH WEIGHT CHANGE"
        // Case 32
        // ReturnPrintPriorityText = "VIN CORRECTION"
        // Case 33
        // ReturnPrintPriorityText = "NAME ADDED"
        // Case 34
        // ReturnPrintPriorityText = "NAME DELETED"
        // Case 35
        // ReturnPrintPriorityText = "NAME CHANGED"
        // Case 36
        // ReturnPrintPriorityText = "SURVIVING SPOUSE"
        // Case 37
        // ReturnPrintPriorityText = "ISLAND USE ONLY"
        // Case 38
        // ReturnPrintPriorityText = "WHATEVER TYPED / OTHER"
        // Case 39
        // ReturnPrintPriorityText = "ROAD TRACTOR"
        // Case 40
        // ReturnPrintPriorityText = "TRUCK CAMPER"
        // Case 41
        // ReturnPrintPriorityText = "FLEET VEHICLE"
        // Case 42
        // ReturnPrintPriorityText = "HIRE"
        // Case 43
        // ReturnPrintPriorityText = "MEDAL OF HONOR"
        // Case 44
        // ReturnPrintPriorityText = "GOLD STAR FAMILY"
        // Case 45
        // ReturnPrintPriorityText = "STOCK CAR"
        // Case 46
        // ReturnPrintPriorityText = "HEARSE"
        // Case 47
        // ReturnPrintPriorityText = "MOBILE HOME"
        // Case 48
        // ReturnPrintPriorityText = "OFFICE TRAILER"
        // Case 49
        // ReturnPrintPriorityText = "CAMPER TRAILER"
        // Case 50
        // ReturnPrintPriorityText = "NON-EXCISE TRAILER"
        // Case 51
        // ReturnPrintPriorityText = "NON-EXCISE TRUE TLR"
        // Case 52
        // ReturnPrintPriorityText = "SPEC MOB EQUIP CL A"
        // Case 53
        // ReturnPrintPriorityText = "SPEC MOB EQUIP CL B/T"
        // Case 54
        // ReturnPrintPriorityText = "SPEC MOB EQUIP CL B/F"
        // Case 55
        // ReturnPrintPriorityText = "BUS- NON-COMPENSATION"
        // Case 56
        // ReturnPrintPriorityText = "FISH TRUCK"
        // Case 57
        // ReturnPrintPriorityText = "SNOW PLOW USE ONLY"
        // Case Else
        // ReturnPrintPriorityText = ""
        // End Select
        // End Function
        // Public Sub AddSTClass()
        // Dim tempRS As New clsDRWrapper
        //
        // tempRS.OpenRecordset "SELECT * FROM Class WHERE BMVCode = 'ST' AND SystemCode = 'ST'"
        // If tempRS.EndOfFile <> True And tempRS.BeginningOfFile <> True Then
        // DoEvents
        // tempRS.Close
        // Else
        // With tempRS
        // .AddNew
        // .Fields("SystemCode") = "ST"
        // .Fields("BMVCode") = "ST"
        // .Fields("Description") = "STATE"
        // .Fields("ExciseRequired") = "N"
        // .Fields("RegistrationFee") = 0
        // .Fields("OtherFeesNew") = 0
        // .Fields("Otherfeesrenew") = 0
        // .Fields("HalfRateAllowed") = "N"
        // .Fields("TwoYear") = "N"
        // .Fields("LocationNew") = 3
        // .Fields("LocationTransfer") = 3
        // .Fields("LocationRenewal") = 3
        // .Fields("NumberOfPlateStickers") = 0
        // .Fields("renewaldate") = "S"
        // .Fields("TitleRequired") = "R"
        // .Fields("Emission") = "R"
        // .Fields("InsuranceRequired") = "Y"
        // .Fields("ShortDescription") = 5
        // .Update
        // End With
        // DoEvents
        // tempRS.Close
        // End If
        // End Sub
        //
        // Public Sub AddCSClass()
        // Dim tempRS As New clsDRWrapper
        //
        // tempRS.OpenRecordset "SELECT * FROM Class WHERE BMVCode = 'CS' AND SystemCode = 'CS'"
        // If tempRS.EndOfFile <> True And tempRS.BeginningOfFile <> True Then
        // DoEvents
        // tempRS.Close
        // Else
        // With tempRS
        // .AddNew
        // .Fields("SystemCode") = "CS"
        // .Fields("BMVCode") = "CS"
        // .Fields("Description") = "COUNTY SHERIFF"
        // .Fields("ExciseRequired") = "N"
        // .Fields("RegistrationFee") = 0
        // .Fields("OtherFeesNew") = 0
        // .Fields("Otherfeesrenew") = 0
        // .Fields("HalfRateAllowed") = "N"
        // .Fields("TwoYear") = "N"
        // .Fields("LocationNew") = 3
        // .Fields("LocationTransfer") = 3
        // .Fields("LocationRenewal") = 3
        // .Fields("NumberOfPlateStickers") = 0
        // .Fields("renewaldate") = "S"
        // .Fields("TitleRequired") = "R"
        // .Fields("Emission") = "R"
        // .Fields("InsuranceRequired") = "Y"
        // .Fields("ShortDescription") = 5
        // .Update
        // End With
        // DoEvents
        // tempRS.Close
        // End If
        // End Sub
        // Public Sub UpdateDuneBuggyStockCarRegFees()
        // Dim rs As New clsDRWrapper
        //
        // kk11202015 changed from P3
        // rs.Execute "UPDATE Class SET RegistrationFee = 7 WHERE BMVCode = 'PC' AND (SystemCode = 'P4' OR SystemCode = 'P1')", "TWMV0000.vb1"
        // End Sub

        private static object CheckVersion()
        {
            object CheckVersion = null;
            cMotorVehicleDB mDB = new cMotorVehicleDB();
            if (!mDB.CheckVersion())
            {
                cVersionInfo tVer;
                tVer = mDB.GetVersion();
                MessageBox.Show("Error checking database." + "\r\n" + tVer.VersionString);
            }
            cSystemSettings sysSet = new cSystemSettings();
            sysSet.CheckVersion();
            return CheckVersion;
        }

        public static string GetComputerMVSetting(string strSettingName)
        {
            string GetComputerMVSetting = "";
            cSettingsController setCont = new cSettingsController();
            string strSetting;
            string strComputerName;
            int lngNameLen;
            lngNameLen = 1024;
            strComputerName = Strings.StrDup(lngNameLen, " ");
            modAPIsConst.GetComputerNameWrp(ref strComputerName, ref lngNameLen);
            if (Strings.Trim(strComputerName).Length > 0)
            {
                strComputerName = Strings.Replace(Strings.Trim(strComputerName), FCConvert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
            }
            strSetting = setCont.GetSettingValue(strSettingName, "MotorVehicle", "Machine", strComputerName, "");
            GetComputerMVSetting = strSetting;
            return GetComputerMVSetting;
        }
        // vbPorter upgrade warning: strSettingValue As string	OnWrite(double, bool, string)
        public static void SetComputerMVSetting(string strSettingName, string strSettingValue)
        {
            cSettingsController setCont = new cSettingsController();
            string strSetting = "";
            string strComputerName;
            int lngNameLen;
            lngNameLen = 1024;
            strComputerName = Strings.StrDup(lngNameLen, " ");
            modAPIsConst.GetComputerNameWrp(ref strComputerName, ref lngNameLen);
            if (Strings.Trim(strComputerName).Length > 0)
            {
                strComputerName = Strings.Replace(Strings.Trim(strComputerName), FCConvert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
            }
            setCont.SaveSetting(strSettingValue, strSettingName, "MotorVehicle", "Machine", strComputerName, "");
        }
        // kk11142016 tromv-1235  Run fix for subclasses updated in error
        private static void FixPCSubclass2016()
        {
            clsDRWrapper rsFix = new clsDRWrapper();
            rsFix.OpenRecordset("SELECT Top 1 ID FROM Master WHERE Class = 'PC' AND Subclass = 'P7' AND " + "( RegRateFull = 50.00 OR RegRateFull = 70.00 OR " + "  RegRateFull = 7.00 OR (RegRateFull = 37.00 AND RegisteredWeightNew > 6000)  )", "twmv0000.vb1");
            if (!rsFix.EndOfFile() && !rsFix.BeginningOfFile())
            {
                // P6 (Rental BMV)
                rsFix.Execute("UPDATE Master SET Subclass = 'P6', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 50.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE ActivityMaster SET Subclass = 'P6', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 50.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P6', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 50.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE ArchiveMaster SET Subclass = 'P6', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 50.00", "twmv0000.vb1");
                // P5 (Rental Municipal)
                rsFix.Execute("UPDATE Master SET Subclass = 'P5', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 70.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE ActivityMaster SET Subclass = 'P5', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 70.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P5', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 70.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE ArchiveMaster SET Subclass = 'P5', Rental = 1 WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 70.00", "twmv0000.vb1");
                // P4 (Dune Buggy)
                rsFix.Execute("UPDATE Master SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 7.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE ActivityMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 7.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 7.00", "twmv0000.vb1");
                rsFix.Execute("UPDATE ArchiveMaster SET Subclass = 'P4' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 7.00", "twmv0000.vb1");
                // P3 (Passenger 6001 - 10000 lbs)
                rsFix.Execute("UPDATE Master SET Subclass = 'P3' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 37.00 AND RegisteredWeightNew > 6000", "twmv0000.vb1");
                rsFix.Execute("UPDATE ActivityMaster SET Subclass = 'P3' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 37.00 AND RegisteredWeightNew > 6000", "twmv0000.vb1");
                rsFix.Execute("UPDATE HeldRegistrationMaster SET Subclass = 'P3' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 37.00 AND RegisteredWeightNew > 6000", "twmv0000.vb1");
                rsFix.Execute("UPDATE ArchiveMaster SET Subclass = 'P3' WHERE Class = 'PC' AND Subclass = 'P7' AND RegRateFull = 37.00 AND RegisteredWeightNew > 6000", "twmv0000.vb1");
                // Anything else will be left at P7 - National Guard under 6001 lbs
            }
        }

        public static void UpdateResidenceTable()
        {
            // kk12012016  tromv-1194  Create new update routine to check all geocodes
            clsDRWrapper tempRS = new clsDRWrapper();
            string strCode = "";
            string strDesc = "";
            clsGeoCodes oGeoCodes = new clsGeoCodes();
            try
            {
                // On Error GoTo ErrorHandler
                Information.Err().Clear();
                foreach (clsGeoCode oGC in oGeoCodes.OrganizedTowns)
                {
                    strDesc = Strings.UCase(oGC.Desc);
                    tempRS.OpenRecordset("SELECT * FROM Residence WHERE Code = '" + oGC.Code + "'");
                    if (!tempRS.EndOfFile() && !tempRS.BeginningOfFile())
                    {
                        if (Strings.UCase(Strings.Trim(FCConvert.ToString(tempRS.Get_Fields_String("Town")))) != strDesc)
                        {
                            tempRS.Edit();
                            tempRS.Set_Fields("Town", strDesc);
                            tempRS.Update();
                        }
                    }
                    else
                    {
                        tempRS.AddNew();
                        tempRS.Set_Fields("Code", oGC.Code);
                        tempRS.Set_Fields("Town", strDesc);
                        tempRS.Update();
                    }
                }
                // Not forcing the Unorganized Towns or Agents
                tempRS.OpenRecordset("SELECT * FROM Residence ORDER BY Code");
                while (!tempRS.EndOfFile())
                {
                    strCode = FCConvert.ToString(tempRS.Get_Fields("Code"));
                    strDesc = oGeoCodes.GetDescription(ref strCode);
                    if (strDesc != "")
                    {
                        if (Strings.Trim(strDesc) != Strings.Trim(FCConvert.ToString(tempRS.Get_Fields_String("Town"))))
                        {
                            //FC:FINAL:MSH - i.issue #1858: remove unnecessary code to avoid exceptions
                            //Debug.WriteLine(tempRS.Get_Fields("Code") + " - " + fecherFoundation.Strings.Trim(FCConvert.ToString(tempRS.Get_Fields("Town"))) + " -> " + strCode + " - " + strDesc);
                            tempRS.Edit();
                            tempRS.Set_Fields("Town", strDesc);
                            tempRS.Update();
                        }
                    }
                    else
                    {
                        tempRS.Delete();
                        tempRS.Update();
                    }
                    tempRS.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public class StaticVariables
        {
            public bool moduleInitialized = false;
            public bool blnPrintCTA;
            public bool blnPrintDoubleCTA;
            public string gstrCTAPrinterName = String.Empty;
            // tromvs-98 07/2018
            public string gstrUseTaxPrinterName = String.Empty;
            public bool boolDoubleCTA;
            public bool PrintMVR3;
            public bool blnPrintUseTax;
            public string TransactionID = "";
            public bool bolFromWindowsCR;

            public Guid correlationId = new Guid();

            public bool TransactionCompleted = false;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsVariables = new clsDRWrapper();
            public clsDRWrapper rsVariables_AutoInitialized = null;
            public clsDRWrapper rsVariables
            {
                get
                {
                    if (rsVariables_AutoInitialized == null)
                    {
                        rsVariables_AutoInitialized = new clsDRWrapper();
                    }
                    return rsVariables_AutoInitialized;
                }
                set
                {
                    rsVariables_AutoInitialized = value;
                }
            }
            public string strTellerSQL = "";
            public bool blnUseTellerCloseout;
            public bool blnTryCorrectAgain;
            public bool blnTryReRegAgain;
            public bool blnTryDuplicateAgain;
            public bool CorrectLongTermReg;
            public string FirstCorrectPlate = "";
            public string FirstCorrectClass = "";
            public int FirstCorrectID;
            public string SecondCorrectPlate = "";
            public string SecondCorrectClass = "";
            public int SecondCorrectID;
            public int CorrectPlateType;
            public bool ShowReminders;
            public bool FirstFleetVehicle;
            public bool blnForcedAnalysis;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsSaveVehicle = new clsDRWrapper();
            public clsDRWrapper rsSaveVehicle_AutoInitialized = null;
            public clsDRWrapper rsSaveVehicle
            {
                get
                {
                    if (rsSaveVehicle_AutoInitialized == null)
                    {
                        rsSaveVehicle_AutoInitialized = new clsDRWrapper();
                    }
                    return rsSaveVehicle_AutoInitialized;
                }
                set
                {
                    rsSaveVehicle_AutoInitialized = value;
                }
            }
            public int OldMVR3Number;
            public bool blnCheck;
            public int PlateGroup;
            public int StickerGroup;
            public int MVR3Group;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsTellers = new clsDRWrapper();
            public clsDRWrapper rsTellers_AutoInitialized = null;
            public clsDRWrapper rsTellers
            {
                get
                {
                    if (rsTellers_AutoInitialized == null)
                    {
                        rsTellers_AutoInitialized = new clsDRWrapper();
                    }
                    return rsTellers_AutoInitialized;
                }
                set
                {
                    rsTellers_AutoInitialized = value;
                }
            }
            public bool AllowRapidRenewal;
            public bool AllowRentals;
            public bool AddressChange;
            //public static DAO.Workspace wsNew;
            public string cWSName = String.Empty;
            // workspace name
            public string cWSUser = String.Empty;
            // user name. using the default
            public string cWSPassword = String.Empty;
            // password for the database
            public string FleetExpDate = "";
            public bool NoReplacementFee;
            public bool ReplacePlate;
            public string ReplacementPlateNumber = "";
            public string ReplacementPlateCode = "";
            public FCFileSystem fso = new FCFileSystem();
            public StreamWriter MVPrinter;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsECorrect = new clsDRWrapper();
            public clsDRWrapper rsECorrect_AutoInitialized = null;
            public clsDRWrapper rsECorrect
            {
                get
                {
                    if (rsECorrect_AutoInitialized == null)
                    {
                        rsECorrect_AutoInitialized = new clsDRWrapper();
                    }
                    return rsECorrect_AutoInitialized;
                }
                set
                {
                    rsECorrect_AutoInitialized = value;
                }
            }
            public string TradedMake = "";
            public string TradedModel = "";
            public string TradedYear = "";
            public string TradedVIN = "";
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsReport = new clsDRWrapper();
            public clsDRWrapper rsReport_AutoInitialized = null;
            public clsDRWrapper rsReport
            {
                get
                {
                    if (rsReport_AutoInitialized == null)
                    {
                        rsReport_AutoInitialized = new clsDRWrapper();
                    }
                    return rsReport_AutoInitialized;
                }
                set
                {
                    rsReport_AutoInitialized = value;
                }
            }
            public bool FleetCTA;
            public bool FleetUseTax;
            public int VehiclesCovered;
            public bool InfoForCorrect;
            public bool InfoForReReg;
            public bool InfoForDuplicate;
            // vbPorter upgrade warning: gobjFormName As object	OnRead(Form)
            public FCForm gobjFormName;
            // vbPorter upgrade warning: gobjLabel As object	OnWrite(FCLabel)
            public FCLabel gobjLabel;
            // vbPorter upgrade warning: NumberOfVehicles As int	OnWriteFCConvert.ToInt32(
            public int NumberOfVehicles;
            // vbPorter upgrade warning: RememberedIndex As int	OnWriteFCConvert.ToInt32(
            public int RememberedIndex;
            public string GroupOrFleet = "";
            // vbPorter upgrade warning: VehiclesRegistered As bool()	OnWrite(bool(), string)
            public bool[] VehiclesRegistered = null;
            // vbPorter upgrade warning: VehicleInformation As object(,)	OnWrite(string(), bool(), object, double(), string)
            public object[,] VehicleInformation;
            public int VehicleBeingRegistered;
            public bool RegisteringFleet;
            public int FleetBeingRegistered;
            public bool HeldPlate;
            public int fnx;
            public string CompareDate = "";
            public bool LostPlate;
            public bool bolFromDataInput;
            public bool bolFromLongTermDataInput;
            public bool bolFromLongTermVehicleUpdate;
            public bool bolFromVehicleUpdate;
            public string RRTextAll = String.Empty;
            // left bottom
            public string PP3Text = String.Empty;
            // right top
            public string PP2Text = String.Empty;
            // right middle
            public string PP1Text = String.Empty;
            // right bottom
            public bool FromTransfer;
            public bool NewFleet;
            public int ECorrectKey;
            public bool HeldReg;
            public bool CTAFlag;
            public bool UseTaxFlag;
            public bool OpenFlag;
            public string RememberedPlate = String.Empty;
            // vbPorter upgrade warning: TownLevel As int	OnRead(string)
            public int TownLevel;
            public string reason1 = String.Empty;
            public string reason2 = String.Empty;
            public bool LocalHalfRate;
            public bool StateHalfRate;
            public bool HVUT;
            public string AlignmentOK = String.Empty;
            public bool PrintDouble;
            public double GrandTotal;
            public string RegTypeLost = String.Empty;
            // In the Preview form this shows where they came, Data input or Plate Info
            public bool CompareRecord;
            public bool F9;
            // determines if inventory was called from the menu or the data input form (f9 is true from mvr3 form)
            public bool ListRefresh;
            // used to refresh the plate list box after inventory add/remove
            public string RegistrationType = "";
            public string RegType = String.Empty;
            // New or Renewal
            public bool NewToTheSystem;
            public bool HoldRegistrationDollar;
            public int PlateType;
            public int MonthStickers;
            public int YearStickers;
            public string MVR3PrinterName = String.Empty;
            public string MVR10PrinterName = String.Empty;
            public int MVR3;
            public int pIndex;
            // used to remember the Platetype.txt index when loading the list box - losy focus fires  with the wrong number
            // vbPorter upgrade warning: MillYear As int	OnWrite(int, double)
            public int MillYear;
            public double MillRate;
            public bool FixedMonth;
            public int gintFixedMonthMonth;
            public bool MonetaryECorrect;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsVehicles = new clsDRWrapper();
            public clsDRWrapper rsVehicles_AutoInitialized = null;
            public clsDRWrapper rsVehicles
            {
                get
                {
                    if (rsVehicles_AutoInitialized == null)
                    {
                        rsVehicles_AutoInitialized = new clsDRWrapper();
                    }
                    return rsVehicles_AutoInitialized;
                }
                set
                {
                    rsVehicles_AutoInitialized = value;
                }
            }
            // vbPorter upgrade warning: ECTotal As Decimal	OnWrite(double, Decimal, int)
            public Decimal ECTotal;
            public string RememberedDate1 = String.Empty;
            // if ETO checked and then un-done. the dates will be held
            public string RememberedDate2 = String.Empty;
            public string RememberedAgentFee = "";
            public string RememberedStateCredit = "";
            public bool RememberedTwoYear;
            public string OpID = String.Empty;
            public int OperatorLevel;
            public string UserID = "";
            public string strSql = "";
            public string ListType = String.Empty;
            // used for Inventory adjustment
            public string ListSubType = String.Empty;
            // ""
            public string Numeric1 = String.Empty;
            //
            public string Numeric2 = String.Empty;
            //
            public string Prefix1 = String.Empty;
            // used in Plate Range sub
            public string Prefix2 = String.Empty;
            //
            public string Suffix1 = String.Empty;
            //
            public string Suffix2 = String.Empty;
            //
            // vbPorter upgrade warning: LengthOfNumeric As int	OnWriteFCConvert.ToInt32(
            public int LengthOfNumeric;
            //
            public bool ETO;
            public int FinalCompareID;
            public int FinalCompareID2;
            public int FinalCompareID3;
            public bool FromInventory;
            // vbPorter upgrade warning: FullExciseTax As Decimal	OnWrite(int, Decimal)
            public Decimal FullExciseTax;
            // vbPorter upgrade warning: FullExciseCredit As Decimal	OnWrite(int, Decimal)
            public Decimal FullExciseCredit;
            public bool NoRedbook;
            // vbPorter upgrade warning: FullRegRate As Decimal	OnWrite(Decimal, int)
            public Decimal FullRegRate;
            // vbPorter upgrade warning: FullRegCredit As Decimal	OnWrite
            public Decimal FullRegCredit;
            public int lngNewID;
            // rsfinalcompare keynumber
            public bool NewPlate;
            // plate selection screen. Is the plate new or exisitng ( the one they leave with)
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsMaster = new clsDRWrapper();
            public clsDRWrapper rsMaster_AutoInitialized = null;
            public clsDRWrapper rsMaster
            {
                get
                {
                    if (rsMaster_AutoInitialized == null)
                    {
                        rsMaster_AutoInitialized = new clsDRWrapper();
                    }
                    return rsMaster_AutoInitialized;
                }
                set
                {
                    rsMaster_AutoInitialized = value;
                }
            }
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsPlateForReg = new clsDRWrapper();
            public clsDRWrapper rsPlateForReg_AutoInitialized = null;
            public clsDRWrapper rsPlateForReg
            {
                get
                {
                    if (rsPlateForReg_AutoInitialized == null)
                    {
                        rsPlateForReg_AutoInitialized = new clsDRWrapper();
                    }
                    return rsPlateForReg_AutoInitialized;
                }
                set
                {
                    rsPlateForReg_AutoInitialized = value;
                }
            }
            // these 3 rs's are for the reg selection secreen.
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsSecondPlate = new clsDRWrapper();
            public clsDRWrapper rsSecondPlate_AutoInitialized = null;
            public clsDRWrapper rsSecondPlate
            {
                get
                {
                    if (rsSecondPlate_AutoInitialized == null)
                    {
                        rsSecondPlate_AutoInitialized = new clsDRWrapper();
                    }
                    return rsSecondPlate_AutoInitialized;
                }
                set
                {
                    rsSecondPlate_AutoInitialized = value;
                }
            }
            // they contain the selected plates
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsThirdPlate = new clsDRWrapper();
            public clsDRWrapper rsThirdPlate_AutoInitialized = null;
            public clsDRWrapper rsThirdPlate
            {
                get
                {
                    if (rsThirdPlate_AutoInitialized == null)
                    {
                        rsThirdPlate_AutoInitialized = new clsDRWrapper();
                    }
                    return rsThirdPlate_AutoInitialized;
                }
                set
                {
                    rsThirdPlate_AutoInitialized = value;
                }
            }
            // to bring into the mvr3 screen
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsHoldSet = new clsDRWrapper();
            public clsDRWrapper rsHoldSet_AutoInitialized = null;
            public clsDRWrapper rsHoldSet
            {
                get
                {
                    if (rsHoldSet_AutoInitialized == null)
                    {
                        rsHoldSet_AutoInitialized = new clsDRWrapper();
                    }
                    return rsHoldSet_AutoInitialized;
                }
                set
                {
                    rsHoldSet_AutoInitialized = value;
                }
            }
            // hold rsPlate for reg
            public int TPlate;
            // Temp plate #
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsArchive = new clsDRWrapper();
            public clsDRWrapper rsArchive_AutoInitialized = null;
            public clsDRWrapper rsArchive
            {
                get
                {
                    if (rsArchive_AutoInitialized == null)
                    {
                        rsArchive_AutoInitialized = new clsDRWrapper();
                    }
                    return rsArchive_AutoInitialized;
                }
                set
                {
                    rsArchive_AutoInitialized = value;
                }
            }
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsAM = new clsDRWrapper();
            public clsDRWrapper rsAM_AutoInitialized = null;
            public clsDRWrapper rsAM
            {
                get
                {
                    if (rsAM_AutoInitialized == null)
                    {
                        rsAM_AutoInitialized = new clsDRWrapper();
                    }
                    return rsAM_AutoInitialized;
                }
                set
                {
                    rsAM_AutoInitialized = value;
                }
            }
            // vbPorter upgrade warning: Plate1Sub As int	OnWriteFCConvert.ToInt32(
            public int Plate1Sub;
            // vbPorter upgrade warning: Plate2Sub As int	OnWriteFCConvert.ToInt32(
            public int Plate2Sub;
            public int Plate3Sub;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsFinal = new clsDRWrapper();
            public clsDRWrapper rsFinal_AutoInitialized = null;
            public clsDRWrapper rsFinal
            {
                get
                {
                    if (rsFinal_AutoInitialized == null)
                    {
                        rsFinal_AutoInitialized = new clsDRWrapper();
                    }
                    return rsFinal_AutoInitialized;
                }
                set
                {
                    rsFinal_AutoInitialized = value;
                }
            }
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsFinalCompare = new clsDRWrapper();
            public clsDRWrapper rsFinalCompare_AutoInitialized = null;
            public clsDRWrapper rsFinalCompare
            {
                get
                {
                    if (rsFinalCompare_AutoInitialized == null)
                    {
                        rsFinalCompare_AutoInitialized = new clsDRWrapper();
                    }
                    return rsFinalCompare_AutoInitialized;
                }
                set
                {
                    rsFinalCompare_AutoInitialized = value;
                }
            }
            // a copy of rsFinal used for comparison. it is a snapshot of how rsFinal was at the start
            public bool SetFinalCompare;
            // used to flag if the rsfinalcompare expiredate has been set.
            public bool VehicleInfo;
            public bool DemographicInfo;
            public bool DateInfo;
            public int FinalRecordIDNumber;
            // original key number from Master file
            public int RecordIDToTerminate;
            public string ValidationLine = "";
            public string Plate1 = "";
            public string Plate2 = "";
            public string Plate3 = String.Empty;
            public int TempPlateNumber;
            public bool MVR3Warning;
            public string strCodeM = "";
            public string strCodeY = "";
            public string strCodeP = String.Empty;
            public bool ExciseAP;
            public string ForcedPlate = "";
            public string ReturnFromListBox = String.Empty;
            // contains the line item selected from the list box on reg type screen
            public bool FromETAPScreenThingy;
            // vbPorter upgrade warning: Class As string	OnWrite(string, FixedString)
            public string Class = "";
            // Class and SubClass are filled in the reg selection screen
            public string Subclass = "";
            public string Subclass2 = String.Empty;
            public string RememberedSubclass = String.Empty;
            public string TransferSubclass = "";
            public string Class1 = String.Empty;
            // Class of the first
            public string Class2 = "";
            // Class of Second Plate for reg's
            public string Class3 = String.Empty;
            // Class of Third Plate for reg's
            public bool MVR3saved;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsInventoryAdjust = new clsDRWrapper();
            public clsDRWrapper rsInventoryAdjust_AutoInitialized = null;
            public clsDRWrapper rsInventoryAdjust
            {
                get
                {
                    if (rsInventoryAdjust_AutoInitialized == null)
                    {
                        rsInventoryAdjust_AutoInitialized = new clsDRWrapper();
                    }
                    return rsInventoryAdjust_AutoInitialized;
                }
                set
                {
                    rsInventoryAdjust_AutoInitialized = value;
                }
            }
            public string ReportTown = "";
            public string ReportState = "";
            public string ReportZip = "";
            public string ReportAgent = "";
            public string ReportTelephone = "";
            public string ResidenceCode = "";
            public bool DataFormLoaded;
            public bool RebuildingLine;
            public bool FromFeesScreen;
            public bool blnLost;
            public int Wait;
            public bool ValuesFromRedbook;
            public int lngCTAAddNewID;
            public int lngUTCAddNewID;
            public bool NeedToPrintUseTax;
            public bool NeedToPrintCTA;
            public bool Networked;
            public string ChickConversion = String.Empty;
            public string TempMonth = String.Empty;
            public string TempYear = String.Empty;
            public bool TwoYear;
            public bool PreviewFlag;
            public bool PreviousETO;
            public bool blnReserve;
            public bool bolFromDosTrio;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsDoubleTitle = new clsDRWrapper();
            public clsDRWrapper rsDoubleTitle_AutoInitialized = null;
            public clsDRWrapper rsDoubleTitle
            {
                get
                {
                    if (rsDoubleTitle_AutoInitialized == null)
                    {
                        rsDoubleTitle_AutoInitialized = new clsDRWrapper();
                    }
                    return rsDoubleTitle_AutoInitialized;
                }
                set
                {
                    rsDoubleTitle_AutoInitialized = value;
                }
            }
            public FCFixedString Record1 = new FCFixedString(250);
            public FCFixedString Record2 = new FCFixedString(250);
            public FCFixedString Record3 = new FCFixedString(250);
            public string TableName = String.Empty;
            // vbPorter upgrade warning: PreEffectiveDate As DateTime	OnWrite(DateTime, string)
            public DateTime PreEffectiveDate;
            public bool PendingRegistration;
            public bool blnPrintingDupReg;
            public bool blnSuspended;
            public bool blnSR22;
            public bool blnChangeToFleet;
            public int lngFleetNumberBeingChangedTo;
            public POINTAPI pt = new POINTAPI();
            public bool blnFleetRunOnce;
            // vbPorter upgrade warning: curFleetExcise As Decimal	OnWrite(Decimal, int)
            public Decimal curFleetExcise;
            // vbPorter upgrade warning: curFleetStatePaid As Decimal	OnWrite(Decimal, int)
            public Decimal curFleetStatePaid;
            // vbPorter upgrade warning: curFleetAgentFee As Decimal	OnWrite(Decimal, int)
            public Decimal curFleetAgentFee;
            // vbPorter upgrade warning: curFleetSalesTax As Decimal	OnWrite(Decimal, int)
            public Decimal curFleetSalesTax;
            // vbPorter upgrade warning: curFleetTitleFee As Decimal	OnWrite(Decimal, int)
            public Decimal curFleetTitleFee;
            public string strFleetNameAndNumber = "";
            public string strFleetOwner = "";
            public string lngFleetOwnerPartyID = "";
            public int lngFleetResidenceCase;
            public bool blnTownPrintsUseTax;
            public bool blnTownPrintsLongTermUseTaxCTA;
            // kk12162015 tromv-1128
            public bool blnLongTermAutoPopulateUnit;
            public int intUseTaxFormVersion;
            public bool blnAllowExciseRebates;
            // vbPorter upgrade warning: blnMakeVehicleInactive As bool	OnWrite(int, bool)
            public bool blnMakeVehicleInactive;
            public string[] strPrePrintPlate = null;
            public string[] strSystemInfo = null;
            public string[] strStateInfo = null;
            public int[] lngAddedParties = null;
            public int intAddedPartyCounter;
            public int intChanges;
            public bool gboolAllowLongTermTrailers;
            public bool gboolFromLongTermDataEntry;
            public bool gboolLongRegExtension;
            public bool gboolLongRegExtensionSamePlate;
            public bool blnChargeOOTFeeOnEAP;
            public bool blnCheckedOOTFeeonEAP;
            public string[] ClassDesc = new string[100 + 1];
            public double[,] ClassMoney = new double[100 + 1, 4 + 1];
            public string[] TransactionDates = new string[100 + 1];
            public double[,] TransactionDatesMoney = new double[100 + 1, 4 + 1];
            public int ClassMax;
            public int DateMax;
            public bool gboolCorrectionExtraFee;
            public decimal SpecialtyPlateCorrExtraFee;
            public bool gboolFromBatchRegistration;
            public bool gboolFromReRegBatchRegistration;
            public bool gboolReRegBatchStarted;
            // kk05232015 tromv-999
            public bool gboolFromPrintQueue;
            public bool gblnDelayedReg;
            public bool gblnCurrentConfig;
            public bool gblnAskedCurrentConfig;
            //FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
            //public clsDRWrapper rsMVConnection = new clsDRWrapper();
            public clsDRWrapper rsMVConnection_AutoInitialized = null;
            public clsDRWrapper rsMVConnection
            {
                get
                {
                    if (rsMVConnection_AutoInitialized == null)
                    {
                        rsMVConnection_AutoInitialized = new clsDRWrapper();
                    }
                    return rsMVConnection_AutoInitialized;
                }
                set
                {
                    rsMVConnection_AutoInitialized = value;
                }
            }
            // vbPorter upgrade warning: intCorrExtendedSpecialtyPlate As int	OnWrite(int, DialogResult)
            public DialogResult intCorrExtendedSpecialtyPlate;
            public bool intCorrNewPlate;
            public string strInventoryLockedBy = "";
            // vbPorter upgrade warning: datMotorCycleExpires As DateTime	OnWrite(int, string, DateTime)
            public DateTime datMotorCycleExpires;
            // vbPorter upgrade warning: datMotorCycleEffective As DateTime	OnWrite(int, string, DateTime)
            public DateTime datMotorCycleEffective;
            public bool CorrectingNewRegMotorCycle;
            public int glngNumberOfYears;
            public OriginalPartyInfo orgOwnerInfo = new OriginalPartyInfo();
            public bool gblnLongTermTimePaymentsAllowed;
            public bool gboolFromImport;
            public int gintLongTermYearsAllowed;
            public string gstrLongTermCompanyCode = "";
            public bool blnLongTermLaserForms;
            public int[] lngIDs = null;
            public int lngRecords;
            // kk03112016 tromvs-57/tromv-1145  Not sure we have enough global variables yet - adding arrays to pass to Laser MVR-3E
            public string[] gPPriority = null;
            // kk04182016  Rework tromv-1145 Public gPPriority() As Integer
            public string[] gPPriNumber = null;
            // kk07252016  Need to save both Print Priority Number and Text to process duplicate
            public IndividualOwner BMVRecordIndividualOwner = new IndividualOwner(0);
            public OutgoingBMVDiskData BMVRecord = new OutgoingBMVDiskData();
            public RapidRenewalIndividualOwner RRInfoRecordIndividualOwner = new RapidRenewalIndividualOwner(0);
            public RapidRenewalData RRInfoRecord = new RapidRenewalData();
            public PlateCounter[] pcPlateInfo = null;
            public int intPlateCountCounter;
            public bool startingMotorVehicle = false;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }

        public static bool IsLease()
        {
            return FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "R" ||
                   FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "R" ||
                   FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "R";
        }
    }

    public enum MotorVehicleReturnValue
    {
        Failure = 0,
        Success = 1,
        NoStatus = 2
    }
}
