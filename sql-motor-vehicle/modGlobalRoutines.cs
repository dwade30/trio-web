﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public class modGlobalRoutines
	{
		//=========================================================
		// Public Sub SetFixedSize(FormName As Form, Optional UseRoutine As Boolean)
		// Dim lngTempWidth As Long
		// Dim lngTempHeight As Long
		//
		// If Not UseRoutine Then Exit Sub
		//
		// If boolMaxForms Then
		// FormName.WindowState = vbMaximized
		// Else
		// BW Change
		// Form not wide enough for date & lines
		// lngTempWidth = MDIParent.Width - MDIParent.GRID.Width - 200
		// If lngTempWidth < 1 Then Exit Sub
		// lngTempHeight = MDIParent.GRID.Height - 100
		// If lngTempHeight < 1 Then Exit Sub
		//
		// FormName.Left = MDIParent.GRID.Left + MDIParent.GRID.Width + 50 ' from 100
		// FormName.Top = MDIParent.Top + 600
		//
		// FormName.Width = MDIParent.Width - MDIParent.GRID.Width - 200  ' from 500
		// FormName.Height = MDIParent.GRID.Height - 100
		// End BW Change
		// End If
		// End Sub
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmTableMaintenance, frmExcisePaid, frmLeaseUseTax, frmCTA, frmInventory, frmInventoryAdjust, frmReport, frmPreview, frmPrePrints, frmLostPlate, frmExceptionReportItems, frmInventoryStatus, frmSpecialPermit, frmTransitPlate, frmBoosterPlate, menuRapidRenewal, frmFleetMaster, frmPrintFleet, frmPurgeHeld, frmPrintVIN, frmTellerReport, frmTellerCloseout, frmPurgeTerminated, frmVehicleUpdate, frmPurgeReports, frmSettings, frmReminder, frmFleetSelect, frmVehicleClassList, frmMultiplePlates, frmPurgeExpired, frmPurgeArchive, frmUseTax, frmEditTransferAnalysisReport, frmSpecialResCodes, frmLoadValidAccounts, frmTransitPlateInformation, frmSetupExciseTaxEstimate, frmDataInputLongTermTrailers, frmLD79Estimate, frmClearInactiveFlag, frmVehicleAddressList, frmBatchProcessing, frmComment, frmGroupAddressOverride, frmFleetGroupLabels, frmFleetExpiringList, frmVehicleHistory, frmDataInput, frmBoosterDuplicate, frmBoosterTransfer, frmFleetExpiringListMMTA, frmLongTermReRegBatch, frmPrintQueue, frmReportLongTerm, frmCentralPartySearch, frmEditCentralParties, frmExport, frmSingleLongTermCTA, frmPurgeAuditArchive, frmSetupAuditArchiveReport, frmVehicleUpdateLongTerm, frmCentralPartyMerge, frmSelectBankNumber, frmSelectPostingJournal, frmGiftCertificates)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static bool FormExist(FCForm FormName)
		{
			bool FormExist = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "FormExist";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				foreach (FCForm frm in FCGlobal.Statics.Forms)
				{
					if (frm.Name == FormName.Name)
					{
						if (FCConvert.ToString(frm.Tag) == "Open")
						{
							FormExist = true;
							return FormExist;
						}

                        break;
                    }
				}
				FormName.Tag = "Open";
				return FormExist;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				modErrorHandler.SetErrorHandler(ex);
				return FormExist;
			}
		}

		public static string PadToString_6(int intValue, int intLength)
		{
			return PadToString(intValue, intLength);
		}

		public static string PadToString_8(int intValue, int intLength)
		{
			return PadToString(intValue, intLength);
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(int, object, string)
		// vbPorter upgrade warning: intLength As int	OnWrite(int, string)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static string PadToString(int intValue, int intLength)
		{
			string PadToString;
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}
	}
}
