﻿using TWMV0000.Commands;
using SharedApplication.Messaging;

namespace TWMV0000
{
    public class TRIO_Command_Handler___Show_Modal3 : CommandHandler<$commandtype$,$returntype$>
    {
        private IModalView<$viewmodelinterface$> _view;
        public TRIO_Command_Handler___Show_Modal3(IModalView<$viewmodelinterface$> modalview)
        {
            this._view = modalview;
        }
        protected override $returntype$ Handle($commandtype$ command)
        {
            _view.ShowModal();
            return _view.ViewModel.$viewmodelproperty$;
        }
    }
}