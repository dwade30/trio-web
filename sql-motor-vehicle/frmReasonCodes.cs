//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmReasonCodes : BaseForm
	{
		public frmReasonCodes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            //FC:FINAL:DDU:#i1985 - set columns for listviews
            lstAvailable.DoubleClick += LstAvailable_DoubleClick;
            lstUsed.DoubleClick += LstUsed_DoubleClick;
			lstAvailable.Items.Clear();
			lstAvailable.Columns.Add("");
			//lstAvailable.Columns.Add("");
			int listWidth = lstAvailable.Width;
			lstAvailable.Columns[0].Width = FCConvert.ToInt32(0.2 * listWidth);
			lstAvailable.Columns[1].Width = FCConvert.ToInt32(0.76 * listWidth);

			lstSystemCodes.Items.Clear();
			lstSystemCodes.Columns.Add("");
			//lstSystemCodes.Columns.Add("");
			listWidth = lstSystemCodes.Width;
			lstSystemCodes.Columns[0].Width = FCConvert.ToInt32(0.2 * listWidth);
			lstSystemCodes.Columns[1].Width = FCConvert.ToInt32(0.76 * listWidth);

			lstUsed.Items.Clear();
			lstUsed.Columns.Add("");
			//lstUsed.Columns.Add("");
			listWidth = lstUsed.Width;
			lstUsed.Columns[0].Width = FCConvert.ToInt32(0.2 * listWidth);
			lstUsed.Columns[1].Width = FCConvert.ToInt32(0.76 * listWidth);
		}

        private void LstUsed_DoubleClick(object sender, EventArgs e)
        {
            RemoveCodeFromUsed();
        }

        public string OriginalReasons { get; set; } = "";
        private void LstAvailable_DoubleClick(object sender, EventArgs e)
        {
            AddCodeToUsed();
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmReasonCodes InstancePtr
		{
			get
			{
				return (frmReasonCodes)Sys.GetInstance(typeof(frmReasonCodes));
			}
		}

		protected frmReasonCodes _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsReasons = new clsDRWrapper();
		string[] ValCodes = new string[20 + 1];
		string[] Code = new string[20 + 1];
		int fnx;
		int fnx2;
		clsPrintCodes oPrtCodes = new clsPrintCodes();

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
		    AddCodeToUsed();	
		}

        private void AddCodeToUsed()
        {
            if (lstAvailable.SelectedIndex != -1)
            {
                if (fecherFoundation.Strings.Trim(Strings.Mid(lstAvailable.Text, 4, lstAvailable.Text.Length - 4)) == "NAME ADDED")
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "N")
                    {
                        MessageBox.Show("You must have 2 owners on the registration to use this Reason Code.", "Invalid Reason Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                ListViewItem aux = lstAvailable.Items[lstAvailable.SelectedIndex];
                lstAvailable.Items.RemoveAt(lstAvailable.SelectedIndex);
                lstUsed.Items.Add(aux);
            }
            lstUsed.Refresh();
            lstAvailable.Refresh();
        }

		public void cmdAdd_Click()
		{
			cmdAdd_Click(cmdAdd, new System.EventArgs());
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			int reasons;
			int counter;
			bool matcher = false;
			MotorVehicle.Statics.reason2 = "";

			for (fnx = 0; fnx <= lstUsed.Items.Count - 1; fnx++)
			{
				MotorVehicle.Statics.reason2 += Strings.Mid(lstUsed.Items[fnx].Text, 1, 2) + AddReasonCode(Strings.Mid(lstUsed.Items[fnx].Text, 1, 2));
			}

			reasons = FCConvert.ToInt16(MotorVehicle.Statics.reason2.Length / 2.0);
			for (fnx = 0; fnx <= lstSystemCodes.Items.Count - 1; fnx++)
			{
				matcher = false;
				for (counter = 0; counter <= reasons - 1; counter++)
				{
					if (Strings.Mid(MotorVehicle.Statics.reason2, 1 + (2 * counter), 2) == Strings.Mid(lstSystemCodes.Items[fnx].Text, 1, 2))
					{
						matcher = true;
						break;
					}
				}
				if (!matcher)
				{
					MotorVehicle.Statics.reason2 += Strings.Mid(lstSystemCodes.Items[fnx].Text, 1, 2);
					reasons += 1;
				}
			}

			MotorVehicle.Statics.RebuildingLine = true;
			Close();
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void cmdRemove_Click(object sender, System.EventArgs e)
		{
			RemoveCodeFromUsed();
		}

        private void RemoveCodeFromUsed()
        {
            if (lstUsed.SelectedIndex != -1)
            {
                ListViewItem aux = lstUsed.Items[lstUsed.SelectedIndex];
                lstUsed.Items.RemoveAt(lstUsed.SelectedIndex);
                RemoveReasonCode(Strings.Mid(lstUsed.Text, 1, 2));
                lstAvailable.Items.Add(aux);
            }
            lstUsed.Refresh();
            lstAvailable.Refresh();
            lstSystemCodes.Refresh();
        }
		public void cmdRemove_Click()
		{
			cmdRemove_Click(cmdRemove, new System.EventArgs());
		}

		private void frmReasonCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				cmdDone_Click();
			}
		}

		private void frmReasonCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				cmdDone_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmReasonCodes_Load(object sender, System.EventArgs e)
		{
			rsReasons.OpenRecordset("SELECT * FROM ReasonCodes UNION SELECT * FROM PrintPriority ORDER BY Description");
			if (rsReasons.EndOfFile() != true && rsReasons.BeginningOfFile() != true)
			{
				rsReasons.MoveLast();
				rsReasons.MoveFirst();
				FillListBox(ref rsReasons);
                GetValidationLine();
				if (ValCodes[1] != "")
					MoveCodes();
			}
			else
			{
				MessageBox.Show("There are no Reason codes in your database. Contact System Support", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

        private void GetValidationLine()
        {
            var count = OriginalReasons.Length / 2;
            for (int index = 0; index < count; index ++)
            {
                var codeChars = OriginalReasons.Skip(2 * index).Take(2);
                var code = new string(codeChars.ToArray());
                ValCodes[index + 1] = code;
            }
        }

		public bool FillListBox(ref clsDRWrapper rs1)
		{
			bool FillListBox = false;
			try
			{
				fecherFoundation.Information.Err().Clear();
				fnx2 = 0;
				for (fnx = 0; fnx <= 100; fnx++)
				{
					if (rs1.Get_Fields("include") == true)
					{
						fnx2 += 1;
						if (fecherFoundation.Strings.Trim(rs1.Get_Fields("Code")).Length == 1)
						{
							Code[fnx2] = "R" + rs1.Get_Fields("Code");
						}
						else
						{
							Code[fnx2] = rs1.Get_Fields("Code");
						}
						lstAvailable.AddItem(Code[fnx2] + "\t" + rs1.Get_Fields_String("Description"));
					}
					rs1.MoveNext();
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				fecherFoundation.Information.Err(ex).Clear();
			}
			return FillListBox;
		}

		public void MoveCodes()
		{
			int fny;
			clsDRWrapper rsCodes = new clsDRWrapper();

			rsCodes.OpenRecordset("SELECT * FROM ReasonCodes UNION SELECT * FROM PrintPriority");
			if (rsCodes.EndOfFile() != true && rsCodes.BeginningOfFile() != true)
			{
				rsCodes.MoveLast();
				rsCodes.MoveFirst();
			}
			else
			{
				return;
			}
			for (fnx = 1; fnx <= 20; fnx++)
			{
				if (ValCodes[fnx] == "")
					break;

				for (fny = 0; fny <= lstAvailable.Items.Count - 1; fny++)
				{
					if (ValCodes[fnx] == Strings.Mid(lstAvailable.Items[fny].Text, 1, 2))
					{
						ListViewItem aux = lstAvailable.Items[fny];
						lstAvailable.Items.RemoveAt(fny);
						lstUsed.Items.Add(aux);
						break;
					}
				}

				if (Strings.Mid(ValCodes[fnx], 1, 1) == "R")
				{
					rsCodes.FindFirstRecord("Code", Strings.Mid(ValCodes[fnx], 2, 1));
				}
				else
				{
					rsCodes.FindFirstRecord("Code", ValCodes[fnx]);
				}

				if (!string.IsNullOrEmpty(ValCodes[fnx]) && rsCodes.Get_Fields("include") == false)
				{
					lstSystemCodes.AddItem(ValCodes[fnx] + "\t" + rsCodes.Get_Fields_String("Description"));
				}
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason != FCCloseReason.FormControlMenu)
			{
				rsReasons.Reset();
				FCUtils.EraseSafe(ValCodes);
				FCUtils.EraseSafe(Code);
				fnx = 0;
				fnx2 = 0;
			}
			else
			{
				e.Cancel = true;
			}
		}

		private string AddReasonCode(string str1)
		{
			string AddReasonCode = "";
			return AddReasonCode;
		}

		private void RemoveReasonCode(string str1)
		{
			string SearchCode = "";
			int counter;
			string tempstr = "";
			for (counter = 0; counter <= lstSystemCodes.Items.Count - 1; counter++)
			{
				tempstr = Strings.Mid(lstSystemCodes.Items[counter].Text, 1, 2);
				if (SearchCode == tempstr)
				{
					lstSystemCodes.Items.RemoveAt(counter);
					break;
				}
			}
		}

		private void lstAvailable_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdAdd_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void lstUsed_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdRemove_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdDone_Click();
		}

        private void lstAvailable_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
