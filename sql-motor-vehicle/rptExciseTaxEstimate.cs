//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptExciseTaxEstimate.
	/// </summary>
	public partial class rptExciseTaxEstimate : BaseSectionReport
	{
		public rptExciseTaxEstimate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Excise Tax Estimate";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptExciseTaxEstimate InstancePtr
		{
			get
			{
				return (rptExciseTaxEstimate)Sys.GetInstance(typeof(rptExciseTaxEstimate));
			}
		}

		protected rptExciseTaxEstimate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptExciseTaxEstimate	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		// vbPorter upgrade warning: curMonthlyTotal As Decimal	OnWrite(int, Decimal)
		Decimal curMonthlyTotal;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(int, Decimal)
		Decimal curFinalTotal;
		int lngMonthlyVehicleCount;
		int lngMonthlyNACount;
		int lngFinalVehicleCount;
		int lngFinalNACount;
		clsDRWrapper rsInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curBasePrice As Decimal	OnWrite(int, Decimal)
		Decimal[] curBasePrice = new Decimal[6 + 1];
		// vbPorter upgrade warning: curExcise As Decimal	OnWrite(int, Decimal)
		Decimal[] curExcise = new Decimal[6 + 1];

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
			if (eArgs.EOF != true)
			{
				this.Fields["Binder"].Value = Strings.Format(rsInfo.Get_Fields_DateTime("OrderDate").Month, "00") + Strings.Format(rsInfo.Get_Fields_DateTime("OrderDate").Year, "0000");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label33 As object	OnWrite(string)
			// vbPorter upgrade warning: Label35 As object	OnWrite(string)
			// vbPorter upgrade warning: Label32 As object	OnWrite(string)
			// vbPorter upgrade warning: lblMonthRange As object	OnWrite(string)
			// vbPorter upgrade warning: lblOptions As object	OnWrite(string)
			string strStyleSQL = "";
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			Label33.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label32.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm tt");
			curMonthlyTotal = 0;
			curFinalTotal = 0;
			lngMonthlyVehicleCount = 0;
			lngMonthlyNACount = 0;
			lngFinalVehicleCount = 0;
			lngFinalNACount = 0;
			for (counter = 0; counter <= 6; counter++)
			{
				curBasePrice[counter] = 0;
				curExcise[counter] = 0;
			}
			if (frmSetupExciseTaxEstimate.InstancePtr.cmbDetail.Text == "Summary")
			{
				fldClassPlate.Text = "";
				fldOwner.Text = "";
				fldMilYear.Text = "";
				fldAmount.Text = "";
				fldClassPlate.Visible = false;
				fldOwner.Visible = false;
				fldMilYear.Visible = false;
				fldAmount.Visible = false;
				fldEstimate.Visible = false;
				Field3.Visible = false;
				Field2.Visible = false;
			}
			if (frmSetupExciseTaxEstimate.InstancePtr.cmbAllStyle.Text == "Selected")
			{
				strStyleSQL = "(";
				for (counter = 1; counter <= (frmSetupExciseTaxEstimate.InstancePtr.vsStyle.Rows - 1); counter++)
				{
					if (FCConvert.CBool(frmSetupExciseTaxEstimate.InstancePtr.vsStyle.TextMatrix(counter, 0)))
					{
						strStyleSQL += "Style = '" + frmSetupExciseTaxEstimate.InstancePtr.vsStyle.TextMatrix(counter, 1) + "' or ";
					}
				}
				strStyleSQL = " AND " + Strings.Left(strStyleSQL, strStyleSQL.Length - 4) + ")";
			}
			else
			{
				strStyleSQL = "";
			}
			lblMonthRange.Text = Strings.Format(frmSetupExciseTaxEstimate.InstancePtr.datStart, "MM/dd/yyyy") + " To " + Strings.Format(frmSetupExciseTaxEstimate.InstancePtr.datFinish, "MM/dd/yyyy");
			if (frmSetupExciseTaxEstimate.InstancePtr.cmbSpecific.Text == "All")
			{
				lblOptions.Text = "Vehicle Class: All";
			}
			else
			{
				lblOptions.Text = "Vehicle Class: " + frmSetupExciseTaxEstimate.InstancePtr.cboClass.Text;
			}
			if (frmSetupExciseTaxEstimate.InstancePtr.cmbCollected.Text == "Both")
			{
				lblOptions.Text = lblOptions.Text + "          " + "Amounts: Actual and Estimated";
			}
			else if (frmSetupExciseTaxEstimate.InstancePtr.cmbCollected.Text == "Actual Collected")
			{
				lblOptions.Text = lblOptions.Text + "          " + "Amounts: Actual";
			}
			else
			{
				lblOptions.Text = lblOptions.Text + "          " + "Amounts: Estimated";
			}
			if (frmSetupExciseTaxEstimate.InstancePtr.cmbSpecific.Text == "All")
			{
				if (frmSetupExciseTaxEstimate.InstancePtr.cmbCollected.Text == "Both")
				{
					rsInfo.OpenRecordset("SELECT c.*, p.FullNameLF FROM (SELECT PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, ExpireDate as OrderDate, 'M' as RecordType FROM Master WHERE ExpireDate >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND ExpireDate <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " UNION ALL SELECT PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, DateUpdated as OrderDate, 'A' as RecordType FROM ActivityMaster WHERE TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND Status <> 'V' AND DateUpdated >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND DateUpdated <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " ) as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID ORDER BY Year(OrderDate), Month(OrderDate), Class, Plate");
				}
				else if (frmSetupExciseTaxEstimate.InstancePtr.cmbCollected.Text == "Actual Collected")
				{
					rsInfo.OpenRecordset("SELECT FullNameLF, PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, DateUpdated as OrderDate, 'A' as RecordType FROM ActivityMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND Status <> 'V' AND DateUpdated >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND DateUpdated <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " ORDER BY Year(DateUpdated), Month(DateUpdated), Class, Plate");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT FullNameLF, PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, ExpireDate as OrderDate, 'M' as RecordType FROM Master as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE ExpireDate >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND ExpireDate <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " ORDER BY Year(ExpireDate), Month(ExpireDate), Class, Plate");
				}
			}
			else
			{
				if (frmSetupExciseTaxEstimate.InstancePtr.cmbCollected.Text == "Both")
				{
					rsInfo.OpenRecordset("SELECT c.*, p.FullNameLF FROM (SELECT PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, ExpireDate as OrderDate, 'M' as RecordType FROM Master WHERE Class = '" + frmSetupExciseTaxEstimate.InstancePtr.cboClass.Text + "' AND ExpireDate >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND ExpireDate <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " UNION ALL SELECT PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, DateUpdated as OrderDate, 'A' as RecordType FROM ActivityMaster WHERE TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND Status <> 'V' AND Class = '" + frmSetupExciseTaxEstimate.InstancePtr.cboClass.Text + "' AND DateUpdated >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND DateUpdated <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " ) as c" + " LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID ORDER BY Year(OrderDate), Month(OrderDate), Class, Plate");
				}
				else if (frmSetupExciseTaxEstimate.InstancePtr.cmbCollected.Text == "Actual Collected")
				{
					rsInfo.OpenRecordset("SELECT FullNameLF, PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, DateUpdated as OrderDate, 'A' as RecordType FROM ActivityMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND Status <> 'V' AND Class = '" + frmSetupExciseTaxEstimate.InstancePtr.cboClass.Text + "' AND DateUpdated >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND DateUpdated <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " ORDER BY Year(DateUpdated), Month(DateUpdated), Class, Plate");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT FullNameLF, PartyID1, OwnerCode1, Class, SubClass, Plate, MillYear, Year, ExpireDate, BasePrice, ExciseTaxCharged, ExciseCreditUsed, TransactionType, ExpireDate as OrderDate, 'M' as RecordType FROM Master as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Class = '" + frmSetupExciseTaxEstimate.InstancePtr.cboClass.Text + "' AND ExpireDate >= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datStart) + "' AND ExpireDate <= '" + FCConvert.ToString(frmSetupExciseTaxEstimate.InstancePtr.datFinish) + "'" + strStyleSQL + " ORDER BY Year(ExpireDate), Month(ExpireDate), Class, Plate");
				}
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldClassPlate As object	OnWrite(string)
			// vbPorter upgrade warning: fldOwner As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear As object	OnWrite(int, string, object)
			// vbPorter upgrade warning: fldAmount As object	OnWrite(string)
			// vbPorter upgrade warning: intMil As int	OnWrite(double, int)
			int intMil = 0;
			int lngBase = 0;
			// vbPorter upgrade warning: curExciseAmt As Decimal	OnWrite(int, double)
			Decimal curExciseAmt;
			fldClassPlate.Text = rsInfo.Get_Fields_String("Class") + " " + rsInfo.Get_Fields_String("Plate");
			fldOwner.Text = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("FullNameLF"))));
			if (FCConvert.ToString(rsInfo.Get_Fields("RecordType")) == "M")
			{
				if (Conversion.Val(rsInfo.Get_Fields_Int32("MillYear")) != 0)
				{
					intMil = FCConvert.ToInt16(Conversion.Val(rsInfo.Get_Fields_Int32("MillYear")) + 1);
					if (intMil > 6)
						intMil = 6;
				}
				else
				{
					if (Conversion.Val(rsInfo.Get_Fields("Year")) != 0)
					{
						if ((FCConvert.ToInt32(rsInfo.Get_Fields_DateTime("ExpireDate").Year - 1) - Conversion.Val(rsInfo.Get_Fields("Year"))) + 1 > 6)
						{
							intMil = 6;
						}
						else
						{
							if ((FCConvert.ToInt32(rsInfo.Get_Fields_DateTime("ExpireDate").Year - 1) - Conversion.Val(rsInfo.Get_Fields("Year"))) + 1 < 1)
							{
								intMil = 1;
							}
							else
							{
								intMil = FCConvert.ToInt16((FCConvert.ToInt32(rsInfo.Get_Fields_DateTime("ExpireDate").Year) - 1) - Conversion.Val(rsInfo.Get_Fields("Year")) + 1);
							}
						}
					}
					else
					{
						intMil = -1;
					}
				}
				lngBase = FCConvert.ToInt32(Math.Round(Conversion.Val(rsInfo.Get_Fields_Int32("BasePrice"))));
				if (intMil == -1 || lngBase == 0)
				{
					lngMonthlyNACount += 1;
					if (intMil != -1)
					{
						fldMilYear.Text = intMil.ToString();
					}
					else
					{
						fldMilYear.Text = "N/A";
					}
					fldAmount.Text = "N/A";
				}
				else
				{
					fldMilYear.Text = intMil.ToString();
					fldAmount.Text = Strings.Format(MotorVehicle.ExciseTax(intMil, lngBase, rsInfo.Get_Fields_String("Class"), rsInfo.Get_Fields_String("Subclass")), "#,##0.00");
					curMonthlyTotal += FCConvert.ToDecimal(fldAmount.Text);
					lngMonthlyVehicleCount += 1;
					curBasePrice[intMil] += lngBase;
					curExcise[intMil] += FCConvert.ToDecimal(fldAmount.Text);
				}
				if (frmSetupExciseTaxEstimate.InstancePtr.cmbDetail.Text != "Summary")
				{
					fldEstimate.Visible = true;
				}
			}
			else
			{
				fldMilYear.Text = rsInfo.Get_Fields_String("MillYear");
				curExciseAmt = 0;
				if (FCConvert.ToString(rsInfo.Get_Fields("TransactionType")) == "NRT" || FCConvert.ToString(rsInfo.Get_Fields("TransactionType")) == "RRT")
				{
					curExciseAmt = FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("ExciseTaxCharged") - rsInfo.Get_Fields_Decimal("ExciseCreditUsed"));
					if (curExciseAmt < 0)
						curExciseAmt = 0;
				}
				else
				{
					curExciseAmt = FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("ExciseTaxCharged"));
				}
				fldAmount.Text = Strings.Format(curExciseAmt, "#,##0.00");
				curMonthlyTotal += FCConvert.ToDecimal(fldAmount.Text);
				lngMonthlyVehicleCount += 1;
				fldEstimate.Visible = false;
				curBasePrice[rsInfo.Get_Fields_Int32("MillYear")] += rsInfo.Get_Fields_Decimal("BasePrice");
				curExcise[rsInfo.Get_Fields_Int32("MillYear")] += FCConvert.ToDecimal(fldAmount.Text);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFinalTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldFinalVehicleCount As object	OnWrite(string)
			// vbPorter upgrade warning: fldFinalNACount As object	OnWrite(string)
			// vbPorter upgrade warning: fldBasePriceYear1 As object	OnWrite(string)
			// vbPorter upgrade warning: fldBasePriceYear2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldBasePriceYear3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldBasePriceYear4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldBasePriceYear5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldBasePriceYear6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldExciseYear1 As object	OnWrite(string)
			// vbPorter upgrade warning: fldExciseYear2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldExciseYear3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldExciseYear4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldExciseYear5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldExciseYear6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldBasePriceTotalYears As object	OnWrite(string)
			// vbPorter upgrade warning: fldExciseTotalYears As object	OnWrite(string)
			fldFinalTotal.Text = Strings.Format(curFinalTotal, "#,##0.00");
			fldFinalVehicleCount.Text = Strings.Format(lngFinalVehicleCount, "#,##0");
			if (lngFinalNACount > 0)
			{
				fldFinalNACount.Visible = true;
				lblFinalNACount.Visible = true;
				fldFinalNACount.Text = Strings.Format(lngFinalNACount, "#,##0");
			}
			else
			{
				fldFinalNACount.Visible = false;
				lblFinalNACount.Visible = false;
			}
			fldBasePriceYear1.Text = Strings.Format(curBasePrice[1], "#,##0.00");
			fldBasePriceYear2.Text = Strings.Format(curBasePrice[2], "#,##0.00");
			fldBasePriceYear3.Text = Strings.Format(curBasePrice[3], "#,##0.00");
			fldBasePriceYear4.Text = Strings.Format(curBasePrice[4], "#,##0.00");
			fldBasePriceYear5.Text = Strings.Format(curBasePrice[5], "#,##0.00");
			fldBasePriceYear6.Text = Strings.Format(curBasePrice[6], "#,##0.00");
			fldExciseYear1.Text = Strings.Format(curExcise[1], "#,##0.00");
			fldExciseYear2.Text = Strings.Format(curExcise[2], "#,##0.00");
			fldExciseYear3.Text = Strings.Format(curExcise[3], "#,##0.00");
			fldExciseYear4.Text = Strings.Format(curExcise[4], "#,##0.00");
			fldExciseYear5.Text = Strings.Format(curExcise[5], "#,##0.00");
			fldExciseYear6.Text = Strings.Format(curExcise[6], "#,##0.00");
			fldBasePriceTotalYears.Text = Strings.Format(curBasePrice[1] + curBasePrice[2] + curBasePrice[3] + curBasePrice[4] + curBasePrice[5] + curBasePrice[6], "#,##0.00");
			fldExciseTotalYears.Text = Strings.Format(curExcise[1] + curExcise[2] + curExcise[3] + curExcise[4] + curExcise[5] + curExcise[6], "#,##0.00");
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldMonthlyTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldVehicleCount As object	OnWrite(string)
			// vbPorter upgrade warning: fldNACount As object	OnWrite(string)
			fldMonthlyTotal.Text = Strings.Format(curMonthlyTotal, "#,##0.00");
			fldVehicleCount.Text = Strings.Format(lngMonthlyVehicleCount, "#,##0");
			if (lngMonthlyNACount > 0)
			{
				fldNACount.Visible = true;
				lblNACount.Visible = true;
				fldNACount.Text = Strings.Format(lngMonthlyNACount, "#,##0");
			}
			else
			{
				fldNACount.Visible = false;
				lblNACount.Visible = false;
			}
			curFinalTotal += curMonthlyTotal;
			lngFinalVehicleCount += lngMonthlyVehicleCount;
			lngFinalNACount += lngMonthlyNACount;
			curMonthlyTotal = 0;
			lngMonthlyNACount = 0;
			lngMonthlyVehicleCount = 0;
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldMonthTitle As object	OnWrite(string)
			fldMonthTitle.Text = Strings.Format(Strings.Left(FCConvert.ToString(this.Fields["Binder"].Value), 2) + "/1/2000", "MMMM") + " " + Strings.Right((this.Fields["Binder"].Value).ToString(), 4);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label34 As object	OnWrite(string)
			Label34.Text = "Page " + this.PageNumber;
		}

		private void rptExciseTaxEstimate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptExciseTaxEstimate properties;
			//rptExciseTaxEstimate.Caption	= "Excise Tax Estimate";
			//rptExciseTaxEstimate.Icon	= "rptExciseTaxEstimate.dsx":0000";
			//rptExciseTaxEstimate.Left	= 0;
			//rptExciseTaxEstimate.Top	= 0;
			//rptExciseTaxEstimate.Width	= 11880;
			//rptExciseTaxEstimate.Height	= 8595;
			//rptExciseTaxEstimate.StartUpPosition	= 3;
			//rptExciseTaxEstimate.SectionData	= "rptExciseTaxEstimate.dsx":058A;
			//End Unmaped Properties
		}
	}
}
