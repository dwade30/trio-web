﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmFleetExpiringList : BaseForm
	{
		public frmFleetExpiringList()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFleetExpiringList InstancePtr
		{
			get
			{
				return (frmFleetExpiringList)Sys.GetInstance(typeof(frmFleetExpiringList));
			}
		}

		protected frmFleetExpiringList _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmFleetExpiringList_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmFleetExpiringList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFleetExpiringList properties;
			//frmFleetExpiringList.FillStyle	= 0;
			//frmFleetExpiringList.ScaleWidth	= 3885;
			//frmFleetExpiringList.ScaleHeight	= 2160;
			//frmFleetExpiringList.LinkTopic	= "Form2";
			//frmFleetExpiringList.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmFleetExpiringList_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtEndDate.Focus();
				return;
			}
			else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			frmReportViewer.InstancePtr.Init(rptFleetReminderList.InstancePtr, showProgressDialog: false);
			this.Hide();
		}
	}
}
