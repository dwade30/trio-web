//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTellerReport.
	/// </summary>
	partial class frmTellerReport
	{
		public fecherFoundation.FCComboBox cmbYes;
		public fecherFoundation.FCLabel lblYes;
		public fecherFoundation.FCComboBox cmbAll;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkTeller;
		public fecherFoundation.FCFrame fraTellerCloseout;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCComboBox cboEndDate;
		public fecherFoundation.FCComboBox cboStartDate;
		public fecherFoundation.FCLabel lblEnd;
		public fecherFoundation.FCLabel lblStart;
		public fecherFoundation.FCLabel lblTeller;
		public fecherFoundation.FCFrame fraDates;
		public fecherFoundation.FCComboBox cboReportTime;
		public fecherFoundation.FCFrame fraSetDates;
		public fecherFoundation.FCComboBox cboStart;
		public fecherFoundation.FCComboBox cboEnd;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraRange;
		public Global.T2KDateBox txtEndDate;
		public Global.T2KDateBox txtStartDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame fraSelect;
		public fecherFoundation.FCFrame fraTellers;
		public fecherFoundation.FCCheckBox chkTeller_12;
		public fecherFoundation.FCCheckBox chkTeller_13;
		public fecherFoundation.FCCheckBox chkTeller_14;
		public fecherFoundation.FCCheckBox chkTeller_15;
		public fecherFoundation.FCCheckBox chkTeller_11;
		public fecherFoundation.FCCheckBox chkTeller_10;
		public fecherFoundation.FCCheckBox chkTeller_9;
		public fecherFoundation.FCCheckBox chkTeller_8;
		public fecherFoundation.FCCheckBox chkTeller_7;
		public fecherFoundation.FCCheckBox chkTeller_6;
		public fecherFoundation.FCCheckBox chkTeller_5;
		public fecherFoundation.FCCheckBox chkTeller_4;
		public fecherFoundation.FCCheckBox chkTeller_3;
		public fecherFoundation.FCCheckBox chkTeller_2;
		public fecherFoundation.FCCheckBox chkTeller_1;
		public fecherFoundation.FCCheckBox chkTeller_0;
		public fecherFoundation.FCLabel lblInstructions;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbYes = new fecherFoundation.FCComboBox();
			this.lblYes = new fecherFoundation.FCLabel();
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.fraTellerCloseout = new fecherFoundation.FCFrame();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cboEndDate = new fecherFoundation.FCComboBox();
			this.cboStartDate = new fecherFoundation.FCComboBox();
			this.lblEnd = new fecherFoundation.FCLabel();
			this.lblStart = new fecherFoundation.FCLabel();
			this.lblTeller = new fecherFoundation.FCLabel();
			this.fraDates = new fecherFoundation.FCFrame();
			this.cboReportTime = new fecherFoundation.FCComboBox();
			this.fraSetDates = new fecherFoundation.FCFrame();
			this.cboStart = new fecherFoundation.FCComboBox();
			this.cboEnd = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraRange = new fecherFoundation.FCFrame();
			this.txtEndDate = new Global.T2KDateBox();
			this.txtStartDate = new Global.T2KDateBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.fraSelect = new fecherFoundation.FCFrame();
			this.fraTellers = new fecherFoundation.FCFrame();
			this.chkTeller_12 = new fecherFoundation.FCCheckBox();
			this.chkTeller_13 = new fecherFoundation.FCCheckBox();
			this.chkTeller_14 = new fecherFoundation.FCCheckBox();
			this.chkTeller_15 = new fecherFoundation.FCCheckBox();
			this.chkTeller_11 = new fecherFoundation.FCCheckBox();
			this.chkTeller_10 = new fecherFoundation.FCCheckBox();
			this.chkTeller_9 = new fecherFoundation.FCCheckBox();
			this.chkTeller_8 = new fecherFoundation.FCCheckBox();
			this.chkTeller_7 = new fecherFoundation.FCCheckBox();
			this.chkTeller_6 = new fecherFoundation.FCCheckBox();
			this.chkTeller_5 = new fecherFoundation.FCCheckBox();
			this.chkTeller_4 = new fecherFoundation.FCCheckBox();
			this.chkTeller_3 = new fecherFoundation.FCCheckBox();
			this.chkTeller_2 = new fecherFoundation.FCCheckBox();
			this.chkTeller_1 = new fecherFoundation.FCCheckBox();
			this.chkTeller_0 = new fecherFoundation.FCCheckBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTellerCloseout)).BeginInit();
			this.fraTellerCloseout.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDates)).BeginInit();
			this.fraDates.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSetDates)).BeginInit();
			this.fraSetDates.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSelect)).BeginInit();
			this.fraSelect.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTellers)).BeginInit();
			this.fraTellers.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(708, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraSelect);
			this.ClientArea.Controls.Add(this.fraTellerCloseout);
			this.ClientArea.Controls.Add(this.fraDates);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(708, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(708, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(167, 30);
			this.HeaderText.Text = "Teller Reports";
			// 
			// cmbYes
			// 
			this.cmbYes.AutoSize = false;
			this.cmbYes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYes.FormattingEnabled = true;
			this.cmbYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
			this.cmbYes.Location = new System.Drawing.Point(211, 30);
			this.cmbYes.Name = "cmbYes";
			this.cmbYes.Size = new System.Drawing.Size(189, 40);
			this.cmbYes.TabIndex = 2;
			this.cmbYes.Text = "No";
			// 
			// lblYes
			// 
			this.lblYes.AutoSize = true;
			this.lblYes.Location = new System.Drawing.Point(20, 44);
			this.lblYes.Name = "lblYes";
			this.lblYes.Size = new System.Drawing.Size(181, 15);
			this.lblYes.TabIndex = 3;
			this.lblYes.Text = "PRINT ON SEPARATE PAGES";
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Print Selected"});
			this.cmbAll.Location = new System.Drawing.Point(20, 80);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(189, 40);
			this.cmbAll.TabIndex = 0;
			this.cmbAll.Text = "All";
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
			// 
			// fraTellerCloseout
			// 
			this.fraTellerCloseout.BackColor = System.Drawing.Color.White;
			this.fraTellerCloseout.Controls.Add(this.cmdQuit);
			this.fraTellerCloseout.Controls.Add(this.cmdOK);
			this.fraTellerCloseout.Controls.Add(this.cboEndDate);
			this.fraTellerCloseout.Controls.Add(this.cboStartDate);
			this.fraTellerCloseout.Controls.Add(this.lblEnd);
			this.fraTellerCloseout.Controls.Add(this.lblStart);
			this.fraTellerCloseout.Controls.Add(this.lblTeller);
			this.fraTellerCloseout.Location = new System.Drawing.Point(30, 205);
			this.fraTellerCloseout.Name = "fraTellerCloseout";
			this.fraTellerCloseout.Size = new System.Drawing.Size(386, 223);
			this.fraTellerCloseout.TabIndex = 37;
			this.fraTellerCloseout.Text = "Teller Closeout Dates";
			this.fraTellerCloseout.Visible = false;
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.Location = new System.Drawing.Point(106, 163);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(94, 40);
			this.cmdQuit.TabIndex = 44;
			this.cmdQuit.Text = "Cancel";
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.Location = new System.Drawing.Point(20, 163);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(66, 40);
			this.cmdOK.TabIndex = 43;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cboEndDate
			// 
			this.cboEndDate.AutoSize = false;
			this.cboEndDate.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndDate.FormattingEnabled = true;
			this.cboEndDate.Location = new System.Drawing.Point(124, 113);
			this.cboEndDate.Name = "cboEndDate";
			this.cboEndDate.Size = new System.Drawing.Size(242, 40);
			this.cboEndDate.TabIndex = 42;
			// 
			// cboStartDate
			// 
			this.cboStartDate.AutoSize = false;
			this.cboStartDate.BackColor = System.Drawing.SystemColors.Window;
			this.cboStartDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStartDate.FormattingEnabled = true;
			this.cboStartDate.Location = new System.Drawing.Point(124, 63);
			this.cboStartDate.Name = "cboStartDate";
			this.cboStartDate.Size = new System.Drawing.Size(242, 40);
			this.cboStartDate.TabIndex = 41;
			// 
			// lblEnd
			// 
			this.lblEnd.Location = new System.Drawing.Point(20, 127);
			this.lblEnd.Name = "lblEnd";
			this.lblEnd.Size = new System.Drawing.Size(70, 15);
			this.lblEnd.TabIndex = 40;
			this.lblEnd.Text = "END DATE";
			// 
			// lblStart
			// 
			this.lblStart.Location = new System.Drawing.Point(20, 77);
			this.lblStart.Name = "lblStart";
			this.lblStart.Size = new System.Drawing.Size(75, 15);
			this.lblStart.TabIndex = 39;
			this.lblStart.Text = "START DATE";
			// 
			// lblTeller
			// 
			this.lblTeller.Location = new System.Drawing.Point(20, 30);
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Size = new System.Drawing.Size(165, 23);
			this.lblTeller.TabIndex = 38;
			this.lblTeller.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraDates
			// 
			this.fraDates.AppearanceKey = "groupBoxLeftBorder";
			this.fraDates.BackColor = System.Drawing.Color.White;
			this.fraDates.Controls.Add(this.cboReportTime);
			this.fraDates.Controls.Add(this.fraSetDates);
			this.fraDates.Controls.Add(this.fraRange);
			this.fraDates.Location = new System.Drawing.Point(30, 448);
			this.fraDates.Name = "fraDates";
			this.fraDates.Size = new System.Drawing.Size(628, 240);
			this.fraDates.TabIndex = 12;
			this.fraDates.Text = "Dates To Report";
			// 
			// cboReportTime
			// 
			this.cboReportTime.AutoSize = false;
			this.cboReportTime.BackColor = System.Drawing.SystemColors.Window;
			this.cboReportTime.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboReportTime.FormattingEnabled = true;
			this.cboReportTime.Items.AddRange(new object[] {
            "Today",
            "Period Closeout",
            "Teller Closeout",
            "Date Range",
            "All Activity Since Last Period Closeout",
            "All Closed Out Activity Since Last Period Closeout",
            "All Not Closed Out Activity Since Last Period Closeout"});
			this.cboReportTime.Location = new System.Drawing.Point(20, 30);
			this.cboReportTime.Name = "cboReportTime";
			this.cboReportTime.Size = new System.Drawing.Size(608, 40);
			this.cboReportTime.TabIndex = 36;
			this.cboReportTime.SelectedIndexChanged += new System.EventHandler(this.cboReportTime_SelectedIndexChanged);
			// 
			// fraSetDates
			// 
			this.fraSetDates.BackColor = System.Drawing.Color.White;
			this.fraSetDates.Controls.Add(this.cboStart);
			this.fraSetDates.Controls.Add(this.cboEnd);
			this.fraSetDates.Controls.Add(this.Label1);
			this.fraSetDates.Controls.Add(this.Label2);
			this.fraSetDates.Location = new System.Drawing.Point(20, 80);
			this.fraSetDates.Name = "fraSetDates";
			this.fraSetDates.Size = new System.Drawing.Size(380, 140);
			this.fraSetDates.TabIndex = 17;
			this.fraSetDates.Text = "Closeout Period Dates";
			this.fraSetDates.Visible = false;
			// 
			// cboStart
			// 
			this.cboStart.AutoSize = false;
			this.cboStart.BackColor = System.Drawing.SystemColors.Window;
			this.cboStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStart.FormattingEnabled = true;
			this.cboStart.Location = new System.Drawing.Point(118, 30);
			this.cboStart.Name = "cboStart";
			this.cboStart.Size = new System.Drawing.Size(242, 40);
			this.cboStart.TabIndex = 19;
			// 
			// cboEnd
			// 
			this.cboEnd.AutoSize = false;
			this.cboEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cboEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEnd.FormattingEnabled = true;
			this.cboEnd.Location = new System.Drawing.Point(118, 80);
			this.cboEnd.Name = "cboEnd";
			this.cboEnd.Size = new System.Drawing.Size(242, 40);
			this.cboEnd.TabIndex = 18;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(14, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(80, 17);
			this.Label1.TabIndex = 21;
			this.Label1.Text = "START DATE";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(14, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(70, 17);
			this.Label2.TabIndex = 20;
			this.Label2.Text = "END DATE";
			// 
			// fraRange
			// 
			this.fraRange.BackColor = System.Drawing.Color.White;
			this.fraRange.Controls.Add(this.txtEndDate);
			this.fraRange.Controls.Add(this.txtStartDate);
			this.fraRange.Controls.Add(this.lblTo);
			this.fraRange.Location = new System.Drawing.Point(20, 80);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(321, 90);
			this.fraRange.TabIndex = 15;
			this.fraRange.Text = "Please Select The Range To Report On";
			this.fraRange.Visible = false;
			// 
			// txtEndDate
			// 
			this.txtEndDate.Location = new System.Drawing.Point(188, 30);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(115, 40);
			this.txtEndDate.TabIndex = 24;
			this.txtEndDate.Text = "  /  /";
			// 
			// txtStartDate
			// 
			this.txtStartDate.Location = new System.Drawing.Point(20, 30);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(115, 40);
			this.txtStartDate.TabIndex = 23;
			this.txtStartDate.Text = "  /  /";
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(153, 44);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(19, 23);
			this.lblTo.TabIndex = 16;
			this.lblTo.Text = "TO";
			// 
			// fraSelect
			// 
			this.fraSelect.BackColor = System.Drawing.Color.White;
			this.fraSelect.Controls.Add(this.cmbAll);
			this.fraSelect.Controls.Add(this.cmbYes);
			this.fraSelect.Controls.Add(this.lblYes);
			this.fraSelect.Controls.Add(this.fraTellers);
			this.fraSelect.Location = new System.Drawing.Point(30, 99);
			this.fraSelect.Name = "fraSelect";
			this.fraSelect.Size = new System.Drawing.Size(628, 294);
			this.fraSelect.TabIndex = 0;
			this.fraSelect.Text = "Tellers To Report";
			// 
			// fraTellers
			// 
			this.fraTellers.AppearanceKey = "groupBoxNoBorders";
			this.fraTellers.BackColor = System.Drawing.Color.White;
			this.fraTellers.Controls.Add(this.chkTeller_12);
			this.fraTellers.Controls.Add(this.chkTeller_13);
			this.fraTellers.Controls.Add(this.chkTeller_14);
			this.fraTellers.Controls.Add(this.chkTeller_15);
			this.fraTellers.Controls.Add(this.chkTeller_11);
			this.fraTellers.Controls.Add(this.chkTeller_10);
			this.fraTellers.Controls.Add(this.chkTeller_9);
			this.fraTellers.Controls.Add(this.chkTeller_8);
			this.fraTellers.Controls.Add(this.chkTeller_7);
			this.fraTellers.Controls.Add(this.chkTeller_6);
			this.fraTellers.Controls.Add(this.chkTeller_5);
			this.fraTellers.Controls.Add(this.chkTeller_4);
			this.fraTellers.Controls.Add(this.chkTeller_3);
			this.fraTellers.Controls.Add(this.chkTeller_2);
			this.fraTellers.Controls.Add(this.chkTeller_1);
			this.fraTellers.Controls.Add(this.chkTeller_0);
			this.fraTellers.Enabled = false;
			this.fraTellers.Location = new System.Drawing.Point(20, 140);
			this.fraTellers.Name = "fraTellers";
			this.fraTellers.Size = new System.Drawing.Size(588, 134);
			this.fraTellers.TabIndex = 1;
			// 
			// chkTeller_12
			// 
			this.chkTeller_12.Location = new System.Drawing.Point(449, 0);
			this.chkTeller_12.Name = "chkTeller_12";
			this.chkTeller_12.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_12.TabIndex = 35;
			this.chkTeller_12.Visible = false;
			// 
			// chkTeller_13
			// 
			this.chkTeller_13.Location = new System.Drawing.Point(449, 36);
			this.chkTeller_13.Name = "chkTeller_13";
			this.chkTeller_13.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_13.TabIndex = 34;
			this.chkTeller_13.Visible = false;
			// 
			// chkTeller_14
			// 
			this.chkTeller_14.Location = new System.Drawing.Point(449, 72);
			this.chkTeller_14.Name = "chkTeller_14";
			this.chkTeller_14.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_14.TabIndex = 33;
			this.chkTeller_14.Visible = false;
			// 
			// chkTeller_15
			// 
			this.chkTeller_15.Location = new System.Drawing.Point(449, 108);
			this.chkTeller_15.Name = "chkTeller_15";
			this.chkTeller_15.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_15.TabIndex = 32;
			this.chkTeller_15.Visible = false;
			// 
			// chkTeller_11
			// 
			this.chkTeller_11.Location = new System.Drawing.Point(299, 108);
			this.chkTeller_11.Name = "chkTeller_11";
			this.chkTeller_11.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_11.TabIndex = 31;
			this.chkTeller_11.Visible = false;
			// 
			// chkTeller_10
			// 
			this.chkTeller_10.Location = new System.Drawing.Point(299, 72);
			this.chkTeller_10.Name = "chkTeller_10";
			this.chkTeller_10.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_10.TabIndex = 30;
			this.chkTeller_10.Visible = false;
			// 
			// chkTeller_9
			// 
			this.chkTeller_9.Location = new System.Drawing.Point(299, 36);
			this.chkTeller_9.Name = "chkTeller_9";
			this.chkTeller_9.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_9.TabIndex = 29;
			this.chkTeller_9.Visible = false;
			// 
			// chkTeller_8
			// 
			this.chkTeller_8.Location = new System.Drawing.Point(299, 0);
			this.chkTeller_8.Name = "chkTeller_8";
			this.chkTeller_8.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_8.TabIndex = 28;
			this.chkTeller_8.Visible = false;
			// 
			// chkTeller_7
			// 
			this.chkTeller_7.Location = new System.Drawing.Point(150, 108);
			this.chkTeller_7.Name = "chkTeller_7";
			this.chkTeller_7.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_7.TabIndex = 11;
			this.chkTeller_7.Visible = false;
			// 
			// chkTeller_6
			// 
			this.chkTeller_6.Location = new System.Drawing.Point(150, 72);
			this.chkTeller_6.Name = "chkTeller_6";
			this.chkTeller_6.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_6.TabIndex = 10;
			this.chkTeller_6.Visible = false;
			// 
			// chkTeller_5
			// 
			this.chkTeller_5.Location = new System.Drawing.Point(150, 36);
			this.chkTeller_5.Name = "chkTeller_5";
			this.chkTeller_5.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_5.TabIndex = 9;
			this.chkTeller_5.Visible = false;
			// 
			// chkTeller_4
			// 
			this.chkTeller_4.Location = new System.Drawing.Point(150, 0);
			this.chkTeller_4.Name = "chkTeller_4";
			this.chkTeller_4.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_4.TabIndex = 8;
			this.chkTeller_4.Visible = false;
			// 
			// chkTeller_3
			// 
			this.chkTeller_3.Location = new System.Drawing.Point(0, 108);
			this.chkTeller_3.Name = "chkTeller_3";
			this.chkTeller_3.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_3.TabIndex = 7;
			this.chkTeller_3.Visible = false;
			// 
			// chkTeller_2
			// 
			this.chkTeller_2.Location = new System.Drawing.Point(0, 72);
			this.chkTeller_2.Name = "chkTeller_2";
			this.chkTeller_2.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_2.TabIndex = 6;
			this.chkTeller_2.Visible = false;
			// 
			// chkTeller_1
			// 
			this.chkTeller_1.Location = new System.Drawing.Point(0, 36);
			this.chkTeller_1.Name = "chkTeller_1";
			this.chkTeller_1.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_1.TabIndex = 5;
			this.chkTeller_1.Visible = false;
			// 
			// chkTeller_0
			// 
			this.chkTeller_0.Location = new System.Drawing.Point(0, 0);
			this.chkTeller_0.Name = "chkTeller_0";
			this.chkTeller_0.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_0.TabIndex = 4;
			this.chkTeller_0.Visible = false;
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(406, 30);
			this.lblInstructions.TabIndex = 22;
			this.lblInstructions.Text = "PLEASE SELECT THE TELLERS AND DATES YOU WISH TO REPORT ON AND CLICK THE \"PRINT\" B" +
    "UTTON TO CREATE THE REPORT";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Text = "Print / Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(275, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Size = new System.Drawing.Size(157, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print / Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(634, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(46, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmTellerReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(708, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmTellerReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Teller Reports";
			this.Load += new System.EventHandler(this.frmTellerReport_Load);
			this.Activated += new System.EventHandler(this.frmTellerReport_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTellerReport_KeyPress);
			this.Resize += new System.EventHandler(this.frmTellerReport_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTellerCloseout)).EndInit();
			this.fraTellerCloseout.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDates)).EndInit();
			this.fraDates.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSetDates)).EndInit();
			this.fraSetDates.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			this.fraRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSelect)).EndInit();
			this.fraSelect.ResumeLayout(false);
			this.fraSelect.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTellers)).EndInit();
			this.fraTellers.ResumeLayout(false);
			this.fraTellers.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdFilePreview;
        private FCButton cmdFilePrint;
    }
}