//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmVehicleUpdateLongTerm.
	/// </summary>
	partial class frmVehicleUpdateLongTerm
	{
		public fecherFoundation.FCComboBox cmbNo;
		public fecherFoundation.FCLabel lblNo;
		public fecherFoundation.FCComboBox cmbFleet;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCFrame fraAdditionalInfo;
		public fecherFoundation.FCCheckBox chkOverrideFleetEmail;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCComboBox cboMessageCodes;
		public fecherFoundation.FCButton cmdErase;
		public Global.T2KOverTypeBox txtMessage;
		public fecherFoundation.FCLabel lblMesageCode;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCFrame fraUserFields;
		public Global.T2KOverTypeBox txtUserReason1;
		public Global.T2KOverTypeBox txtUserField1;
		public Global.T2KOverTypeBox txtUserField2;
		public Global.T2KOverTypeBox txtUserField3;
		public Global.T2KOverTypeBox txtUserReason2;
		public Global.T2KOverTypeBox txtUserReason3;
		public fecherFoundation.FCLabel lblUserField1;
		public fecherFoundation.FCLabel lblUserField2;
		public fecherFoundation.FCLabel lblUserField3;
		public fecherFoundation.FCLabel lblInfo1;
		public fecherFoundation.FCLabel lblInfo2;
		public fecherFoundation.FCLabel lblInfo3;
		public Global.T2KOverTypeBox txtEMail;
		public fecherFoundation.FCLabel lblEMail;
		public fecherFoundation.FCFrame fraFeesInfo;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdExit;
		public Global.T2KBackFillDecimal txtFEEAgentFeeReg;
		public Global.T2KBackFillDecimal txtFEESalesTax;
		public Global.T2KBackFillDecimal txtFEETitleFee;
		public Global.T2KBackFillDecimal txtFEETotal;
		public Global.T2KBackFillDecimal txtFEECreditTransfer;
		public Global.T2KBackFillDecimal txtFEERegFee;
		public Global.T2KBackFillDecimal txtFeeAgentFeeCTA;
		public Global.T2KBackFillDecimal txtFEETransferFee;
		public Global.T2KBackFillDecimal txtFeeAgentFeeTransfer;
		public Global.T2KBackFillDecimal txtFeeAgentFeeCorrection;
		public Global.T2KBackFillDecimal txtFEERushTitleFee;
		public fecherFoundation.FCLabel Label2_3;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_5;
		public fecherFoundation.FCLabel Label2_6;
		public fecherFoundation.FCLabel Label2_8;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label2_10;
		public fecherFoundation.FCLabel Label33;
		public fecherFoundation.FCLabel Label34;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel Label35;
		public fecherFoundation.FCLabel Label2_7;
		public Global.T2KOverTypeBox txtRegistrationType;
		public Global.T2KOverTypeBox txtPlateType;
		public Global.T2KOverTypeBox txtStatus;
		public Global.T2KDateBox txtExpireDate;
		public Global.T2KDateBox txtEffectiveDate;
		public fecherFoundation.FCFrame fraDataInput;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtFleetName;
		public fecherFoundation.FCTextBox txtFleetNumber;
		public fecherFoundation.FCLabel lblFleetName;
		public fecherFoundation.FCPictureBox imgFleetComment;
		public fecherFoundation.FCLabel lblFleetNumber;
		public fecherFoundation.FCButton cmdMoreInfo;
		public fecherFoundation.FCTextBox txtOpID;
		public fecherFoundation.FCCheckBox chk25YearPlate;
		public fecherFoundation.FCButton cmdReg1Edit;
		public fecherFoundation.FCButton cmdReg1Search;
		public fecherFoundation.FCTextBox txtReg1PartyID;
		public fecherFoundation.FCButton cmdReg2Edit;
		public fecherFoundation.FCButton cmdReg2Search;
		public fecherFoundation.FCTextBox txtReg2PartyID;
		public fecherFoundation.FCTextBox txtNetWeight;
		public Global.T2KOverTypeBox txtMake;
		public Global.T2KOverTypeBox txtYear;
		public Global.T2KOverTypeBox txtUnit;
		public Global.T2KOverTypeBox txtColor1;
		public Global.T2KOverTypeBox txtColor2;
		public Global.T2KOverTypeBox txtPlate;
		public Global.T2KOverTypeBox txtStyle;
		public Global.T2KOverTypeBox txtVIN;
		public Global.T2KOverTypeBox txtTitle;
		public Global.T2KOverTypeBox txtAddress1;
		public Global.T2KOverTypeBox txtCity;
		public Global.T2KOverTypeBox txtZip;
		public Global.T2KOverTypeBox txtState;
		public Global.T2KOverTypeBox txtLegalResCity;
		public Global.T2KOverTypeBox txtLegalResState;
		public Global.T2KBackFillDecimal txtRate;
		public Global.T2KBackFillDecimal txtCredit;
		public Global.T2KBackFillDecimal txtFees;
		public Global.T2KOverTypeBox txtLegalZip;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel lblRegistrant1;
		public fecherFoundation.FCLabel lblRegistrant2;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label30;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label42;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label38;
		public fecherFoundation.FCLabel Label37;
		public fecherFoundation.FCLabel Label36;
		public fecherFoundation.FCLabel Label3;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFleetGroupComment;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVehicleUpdateLongTerm));
			this.cmbNo = new fecherFoundation.FCComboBox();
			this.lblNo = new fecherFoundation.FCLabel();
			this.cmbFleet = new fecherFoundation.FCComboBox();
			this.fraAdditionalInfo = new fecherFoundation.FCFrame();
			this.chkOverrideFleetEmail = new fecherFoundation.FCCheckBox();
			this.fraMessage = new fecherFoundation.FCFrame();
			this.cboMessageCodes = new fecherFoundation.FCComboBox();
			this.cmdErase = new fecherFoundation.FCButton();
			this.txtMessage = new Global.T2KOverTypeBox();
			this.lblMesageCode = new fecherFoundation.FCLabel();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.cmdDone = new fecherFoundation.FCButton();
			this.fraUserFields = new fecherFoundation.FCFrame();
			this.txtUserReason1 = new Global.T2KOverTypeBox();
			this.txtUserField1 = new Global.T2KOverTypeBox();
			this.txtUserField2 = new Global.T2KOverTypeBox();
			this.txtUserField3 = new Global.T2KOverTypeBox();
			this.txtUserReason2 = new Global.T2KOverTypeBox();
			this.txtUserReason3 = new Global.T2KOverTypeBox();
			this.lblUserField1 = new fecherFoundation.FCLabel();
			this.lblUserField2 = new fecherFoundation.FCLabel();
			this.lblUserField3 = new fecherFoundation.FCLabel();
			this.lblInfo1 = new fecherFoundation.FCLabel();
			this.lblInfo2 = new fecherFoundation.FCLabel();
			this.lblInfo3 = new fecherFoundation.FCLabel();
			this.txtEMail = new Global.T2KOverTypeBox();
			this.lblEMail = new fecherFoundation.FCLabel();
			this.fraFeesInfo = new fecherFoundation.FCFrame();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.cmdExit = new fecherFoundation.FCButton();
			this.txtFEEAgentFeeReg = new Global.T2KBackFillDecimal();
			this.txtFEESalesTax = new Global.T2KBackFillDecimal();
			this.txtFEETitleFee = new Global.T2KBackFillDecimal();
			this.txtFEETotal = new Global.T2KBackFillDecimal();
			this.txtFEECreditTransfer = new Global.T2KBackFillDecimal();
			this.txtFEERegFee = new Global.T2KBackFillDecimal();
			this.txtFeeAgentFeeCTA = new Global.T2KBackFillDecimal();
			this.txtFEETransferFee = new Global.T2KBackFillDecimal();
			this.txtFeeAgentFeeTransfer = new Global.T2KBackFillDecimal();
			this.txtFeeAgentFeeCorrection = new Global.T2KBackFillDecimal();
			this.txtFEERushTitleFee = new Global.T2KBackFillDecimal();
			this.Label2_3 = new fecherFoundation.FCLabel();
			this.Label2_2 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label2_5 = new fecherFoundation.FCLabel();
			this.Label2_6 = new fecherFoundation.FCLabel();
			this.Label2_8 = new fecherFoundation.FCLabel();
			this.Line1 = new fecherFoundation.FCLine();
			this.Label2_10 = new fecherFoundation.FCLabel();
			this.Label33 = new fecherFoundation.FCLabel();
			this.Label34 = new fecherFoundation.FCLabel();
			this.Label2_4 = new fecherFoundation.FCLabel();
			this.Label35 = new fecherFoundation.FCLabel();
			this.Label2_7 = new fecherFoundation.FCLabel();
			this.txtRegistrationType = new Global.T2KOverTypeBox();
			this.txtPlateType = new Global.T2KOverTypeBox();
			this.txtStatus = new Global.T2KOverTypeBox();
			this.txtExpireDate = new Global.T2KDateBox();
			this.txtEffectiveDate = new Global.T2KDateBox();
			this.fraDataInput = new fecherFoundation.FCFrame();
			this.Label29 = new fecherFoundation.FCLabel();
			this.Label23 = new fecherFoundation.FCLabel();
			this.Label19 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtFleetName = new fecherFoundation.FCTextBox();
			this.txtFleetNumber = new fecherFoundation.FCTextBox();
			this.lblFleetName = new fecherFoundation.FCLabel();
			this.imgFleetComment = new fecherFoundation.FCPictureBox();
			this.lblFleetNumber = new fecherFoundation.FCLabel();
			this.cmdMoreInfo = new fecherFoundation.FCButton();
			this.txtOpID = new fecherFoundation.FCTextBox();
			this.chk25YearPlate = new fecherFoundation.FCCheckBox();
			this.cmdReg1Edit = new fecherFoundation.FCButton();
			this.cmdReg1Search = new fecherFoundation.FCButton();
			this.txtReg1PartyID = new fecherFoundation.FCTextBox();
			this.cmdReg2Edit = new fecherFoundation.FCButton();
			this.cmdReg2Search = new fecherFoundation.FCButton();
			this.txtReg2PartyID = new fecherFoundation.FCTextBox();
			this.txtNetWeight = new fecherFoundation.FCTextBox();
			this.txtMake = new Global.T2KOverTypeBox();
			this.txtYear = new Global.T2KOverTypeBox();
			this.txtUnit = new Global.T2KOverTypeBox();
			this.txtColor1 = new Global.T2KOverTypeBox();
			this.txtColor2 = new Global.T2KOverTypeBox();
			this.txtPlate = new Global.T2KOverTypeBox();
			this.txtStyle = new Global.T2KOverTypeBox();
			this.txtVIN = new Global.T2KOverTypeBox();
			this.txtTitle = new Global.T2KOverTypeBox();
			this.txtAddress1 = new Global.T2KOverTypeBox();
			this.txtCity = new Global.T2KOverTypeBox();
			this.txtZip = new Global.T2KOverTypeBox();
			this.txtState = new Global.T2KOverTypeBox();
			this.txtLegalResCity = new Global.T2KOverTypeBox();
			this.txtLegalResState = new Global.T2KOverTypeBox();
			this.txtRate = new Global.T2KBackFillDecimal();
			this.txtCredit = new Global.T2KBackFillDecimal();
			this.txtFees = new Global.T2KBackFillDecimal();
			this.txtLegalZip = new Global.T2KOverTypeBox();
			this.lblRegistrant1 = new fecherFoundation.FCLabel();
			this.lblRegistrant2 = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label24 = new fecherFoundation.FCLabel();
			this.Label20 = new fecherFoundation.FCLabel();
			this.Label22 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Label42 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label31 = new fecherFoundation.FCLabel();
			this.Label27 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label17 = new fecherFoundation.FCLabel();
			this.Label21 = new fecherFoundation.FCLabel();
			this.Label18 = new fecherFoundation.FCLabel();
			this.Label25 = new fecherFoundation.FCLabel();
			this.Label26 = new fecherFoundation.FCLabel();
			this.Label28 = new fecherFoundation.FCLabel();
			this.Label30 = new fecherFoundation.FCLabel();
			this.Label38 = new fecherFoundation.FCLabel();
			this.Label37 = new fecherFoundation.FCLabel();
			this.Label36 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFleetGroupComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.cmdFleetGroupComment = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).BeginInit();
			this.fraAdditionalInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
			this.fraMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdErase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).BeginInit();
			this.fraUserFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFeesInfo)).BeginInit();
			this.fraFeesInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEAgentFeeReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEESalesTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETitleFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEECreditTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEERegFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCTA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETransferFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCorrection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEERushTitleFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegistrationType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlateType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDataInput)).BeginInit();
			this.fraDataInput.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk25YearPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalResCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalResState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFleetGroupComment)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(988, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraFeesInfo);
			this.ClientArea.Controls.Add(this.fraAdditionalInfo);
			this.ClientArea.Controls.Add(this.txtRegistrationType);
			this.ClientArea.Controls.Add(this.txtPlateType);
			this.ClientArea.Controls.Add(this.txtStatus);
			this.ClientArea.Controls.Add(this.txtExpireDate);
			this.ClientArea.Controls.Add(this.txtEffectiveDate);
			this.ClientArea.Controls.Add(this.Label38);
			this.ClientArea.Controls.Add(this.Label37);
			this.ClientArea.Controls.Add(this.Label36);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.fraDataInput);
			this.ClientArea.Size = new System.Drawing.Size(988, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFleetGroupComment);
			this.TopPanel.Size = new System.Drawing.Size(988, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFleetGroupComment, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(336, 30);
			this.HeaderText.Text = "LTT New To System / Update ";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbNo
			// 
			this.cmbNo.AutoSize = false;
			this.cmbNo.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNo.FormattingEnabled = true;
			this.cmbNo.Items.AddRange(new object[] {
            "No",
            "Yes"});
			this.cmbNo.Location = new System.Drawing.Point(146, 30);
			this.cmbNo.Name = "cmbNo";
			this.cmbNo.Size = new System.Drawing.Size(324, 40);
			this.cmbNo.TabIndex = 127;
			this.cmbNo.Text = "Yes";
			this.ToolTip1.SetToolTip(this.cmbNo, null);
			// 
			// lblNo
			// 
			this.lblNo.AutoSize = true;
			this.lblNo.Location = new System.Drawing.Point(20, 44);
			this.lblNo.Name = "lblNo";
			this.lblNo.Size = new System.Drawing.Size(106, 15);
			this.lblNo.TabIndex = 128;
			this.lblNo.Text = "MAIL REMINDER";
			this.ToolTip1.SetToolTip(this.lblNo, null);
			// 
			// cmbFleet
			// 
			this.cmbFleet.AutoSize = false;
			this.cmbFleet.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFleet.FormattingEnabled = true;
			this.cmbFleet.Items.AddRange(new object[] {
            "Fleet",
            "No",
            "Group"});
			this.cmbFleet.Location = new System.Drawing.Point(20, 30);
			this.cmbFleet.Name = "cmbFleet";
			this.cmbFleet.Size = new System.Drawing.Size(180, 40);
			this.cmbFleet.TabIndex = 130;
			this.ToolTip1.SetToolTip(this.cmbFleet, null);
			this.cmbFleet.SelectedIndexChanged += new System.EventHandler(this.optFleet_CheckedChanged);
			// 
			// fraAdditionalInfo
			// 
			this.fraAdditionalInfo.AppearanceKey = "groupBoxLeftBorder";
			this.fraAdditionalInfo.Controls.Add(this.chkOverrideFleetEmail);
			this.fraAdditionalInfo.Controls.Add(this.cmbNo);
			this.fraAdditionalInfo.Controls.Add(this.lblNo);
			this.fraAdditionalInfo.Controls.Add(this.fraMessage);
			this.fraAdditionalInfo.Controls.Add(this.cmdDone);
			this.fraAdditionalInfo.Controls.Add(this.fraUserFields);
			this.fraAdditionalInfo.Controls.Add(this.txtEMail);
			this.fraAdditionalInfo.Controls.Add(this.lblEMail);
			this.fraAdditionalInfo.Location = new System.Drawing.Point(30, 184);
			this.fraAdditionalInfo.Name = "fraAdditionalInfo";
			this.fraAdditionalInfo.Size = new System.Drawing.Size(592, 797);
			this.fraAdditionalInfo.TabIndex = 0;
			this.fraAdditionalInfo.Text = "Additional Information";
			this.ToolTip1.SetToolTip(this.fraAdditionalInfo, null);
			this.fraAdditionalInfo.Visible = false;
			// 
			// chkOverrideFleetEmail
			// 
			this.chkOverrideFleetEmail.Location = new System.Drawing.Point(146, 130);
			this.chkOverrideFleetEmail.Name = "chkOverrideFleetEmail";
			this.chkOverrideFleetEmail.Size = new System.Drawing.Size(177, 27);
			this.chkOverrideFleetEmail.TabIndex = 126;
			this.chkOverrideFleetEmail.TabStop = false;
			this.chkOverrideFleetEmail.Text = "Override Fleet Email";
			this.ToolTip1.SetToolTip(this.chkOverrideFleetEmail, null);
			this.chkOverrideFleetEmail.Visible = false;
			this.chkOverrideFleetEmail.CheckedChanged += new System.EventHandler(this.chkOverrideFleetEmail_CheckedChanged);
			// 
			// fraMessage
			// 
			this.fraMessage.Controls.Add(this.cboMessageCodes);
			this.fraMessage.Controls.Add(this.cmdErase);
			this.fraMessage.Controls.Add(this.txtMessage);
			this.fraMessage.Controls.Add(this.lblMesageCode);
			this.fraMessage.Controls.Add(this.lblMessage);
			this.fraMessage.Location = new System.Drawing.Point(20, 167);
			this.fraMessage.Name = "fraMessage";
			this.fraMessage.Size = new System.Drawing.Size(552, 200);
			this.fraMessage.TabIndex = 15;
			this.fraMessage.Text = "User Message";
			this.ToolTip1.SetToolTip(this.fraMessage, null);
			// 
			// cboMessageCodes
			// 
			this.cboMessageCodes.AutoSize = false;
			this.cboMessageCodes.BackColor = System.Drawing.SystemColors.Window;
			this.cboMessageCodes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboMessageCodes.FormattingEnabled = true;
			this.cboMessageCodes.Items.AddRange(new object[] {
            "1 - Message ",
            "2 - Warning",
            "3 - Stop Process"});
			this.cboMessageCodes.Location = new System.Drawing.Point(142, 30);
			this.cboMessageCodes.Name = "cboMessageCodes";
			this.cboMessageCodes.Size = new System.Drawing.Size(390, 40);
			this.cboMessageCodes.TabIndex = 17;
			this.ToolTip1.SetToolTip(this.cboMessageCodes, null);
			// 
			// cmdErase
			// 
			this.cmdErase.AppearanceKey = "actionButton";
			this.cmdErase.Location = new System.Drawing.Point(20, 140);
			this.cmdErase.Name = "cmdErase";
			this.cmdErase.Size = new System.Drawing.Size(157, 40);
			this.cmdErase.TabIndex = 16;
			this.cmdErase.Text = "Erase Message";
			this.ToolTip1.SetToolTip(this.cmdErase, null);
			this.cmdErase.Click += new System.EventHandler(this.cmdErase_Click);
			// 
			// txtMessage
			// 
			this.txtMessage.AutoSize = false;
            this.txtMessage.CharacterCasing = CharacterCasing.Upper;
            this.txtMessage.LinkItem = null;
			this.txtMessage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage.LinkTopic = null;
			this.txtMessage.Location = new System.Drawing.Point(142, 80);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(390, 40);
			this.txtMessage.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.txtMessage, null);
			this.txtMessage.Enter += new System.EventHandler(this.txtMessage_Enter);
			this.txtMessage.Leave += new System.EventHandler(this.txtMessage_Leave);
			// 
			// lblMesageCode
			// 
			this.lblMesageCode.Location = new System.Drawing.Point(15, 44);
			this.lblMesageCode.Name = "lblMesageCode";
			this.lblMesageCode.Size = new System.Drawing.Size(100, 15);
			this.lblMesageCode.TabIndex = 20;
			this.lblMesageCode.Text = "MESSAGE CODE";
			this.lblMesageCode.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblMesageCode, null);
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.Transparent;
			this.lblMessage.Location = new System.Drawing.Point(20, 94);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(66, 15);
			this.lblMessage.TabIndex = 19;
			this.lblMessage.Text = "MESSAGE";
			this.ToolTip1.SetToolTip(this.lblMessage, null);
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "actionButton";
			this.cmdDone.Location = new System.Drawing.Point(242, 737);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(67, 40);
			this.cmdDone.TabIndex = 14;
			this.cmdDone.Text = "OK";
			this.ToolTip1.SetToolTip(this.cmdDone, null);
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// fraUserFields
			// 
			this.fraUserFields.Controls.Add(this.txtUserReason1);
			this.fraUserFields.Controls.Add(this.txtUserField1);
			this.fraUserFields.Controls.Add(this.txtUserField2);
			this.fraUserFields.Controls.Add(this.txtUserField3);
			this.fraUserFields.Controls.Add(this.txtUserReason2);
			this.fraUserFields.Controls.Add(this.txtUserReason3);
			this.fraUserFields.Controls.Add(this.lblUserField1);
			this.fraUserFields.Controls.Add(this.lblUserField2);
			this.fraUserFields.Controls.Add(this.lblUserField3);
			this.fraUserFields.Controls.Add(this.lblInfo1);
			this.fraUserFields.Controls.Add(this.lblInfo2);
			this.fraUserFields.Controls.Add(this.lblInfo3);
			this.fraUserFields.Location = new System.Drawing.Point(20, 377);
			this.fraUserFields.Name = "fraUserFields";
			this.fraUserFields.Size = new System.Drawing.Size(552, 340);
			this.fraUserFields.TabIndex = 1;
			this.fraUserFields.Text = "User Defined Fields";
			this.ToolTip1.SetToolTip(this.fraUserFields, null);
			this.fraUserFields.Visible = false;
			// 
			// txtUserReason1
			// 
			this.txtUserReason1.AutoSize = false;
            this.txtUserReason1.CharacterCasing = CharacterCasing.Upper;
            this.txtUserReason1.LinkItem = null;
			this.txtUserReason1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUserReason1.LinkTopic = null;
			this.txtUserReason1.Location = new System.Drawing.Point(142, 80);
			this.txtUserReason1.Name = "txtUserReason1";
			this.txtUserReason1.Size = new System.Drawing.Size(390, 40);
			this.txtUserReason1.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.txtUserReason1, null);
			this.txtUserReason1.Visible = false;
			this.txtUserReason1.Enter += new System.EventHandler(this.txtUserReason1_Enter);
			this.txtUserReason1.Leave += new System.EventHandler(this.txtUserReason1_Leave);
			// 
			// txtUserField1
			// 
			this.txtUserField1.AutoSize = false;
            this.txtUserField1.CharacterCasing = CharacterCasing.Upper;
            this.txtUserField1.LinkItem = null;
			this.txtUserField1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUserField1.LinkTopic = null;
			this.txtUserField1.Location = new System.Drawing.Point(142, 30);
			this.txtUserField1.Name = "txtUserField1";
			this.txtUserField1.Size = new System.Drawing.Size(390, 40);
			this.txtUserField1.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtUserField1, null);
			this.txtUserField1.Visible = false;
			this.txtUserField1.Enter += new System.EventHandler(this.txtUserField1_Enter);
			this.txtUserField1.Leave += new System.EventHandler(this.txtUserField1_Leave);
			// 
			// txtUserField2
			// 
			this.txtUserField2.AutoSize = false;
            this.txtUserField2.CharacterCasing = CharacterCasing.Upper;
            this.txtUserField2.LinkItem = null;
			this.txtUserField2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUserField2.LinkTopic = null;
			this.txtUserField2.Location = new System.Drawing.Point(142, 130);
			this.txtUserField2.Name = "txtUserField2";
			this.txtUserField2.Size = new System.Drawing.Size(390, 40);
			this.txtUserField2.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtUserField2, null);
			this.txtUserField2.Visible = false;
			this.txtUserField2.Enter += new System.EventHandler(this.txtUserField2_Enter);
			this.txtUserField2.Leave += new System.EventHandler(this.txtUserField2_Leave);
			// 
			// txtUserField3
			// 
			this.txtUserField3.AutoSize = false;
            this.txtUserField3.CharacterCasing = CharacterCasing.Upper;
            this.txtUserField3.LinkItem = null;
			this.txtUserField3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUserField3.LinkTopic = null;
			this.txtUserField3.Location = new System.Drawing.Point(142, 230);
			this.txtUserField3.Name = "txtUserField3";
			this.txtUserField3.Size = new System.Drawing.Size(390, 40);
			this.txtUserField3.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtUserField3, null);
			this.txtUserField3.Visible = false;
			this.txtUserField3.Enter += new System.EventHandler(this.txtUserField3_Enter);
			this.txtUserField3.Leave += new System.EventHandler(this.txtUserField3_Leave);
			// 
			// txtUserReason2
			// 
			this.txtUserReason2.AutoSize = false;
            this.txtUserReason2.CharacterCasing = CharacterCasing.Upper;
            this.txtUserReason2.LinkItem = null;
			this.txtUserReason2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUserReason2.LinkTopic = null;
			this.txtUserReason2.Location = new System.Drawing.Point(142, 180);
			this.txtUserReason2.Name = "txtUserReason2";
			this.txtUserReason2.Size = new System.Drawing.Size(390, 40);
			this.txtUserReason2.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtUserReason2, null);
			this.txtUserReason2.Visible = false;
			this.txtUserReason2.Enter += new System.EventHandler(this.txtUserReason2_Enter);
			this.txtUserReason2.Leave += new System.EventHandler(this.txtUserReason2_Leave);
			// 
			// txtUserReason3
			// 
			this.txtUserReason3.AutoSize = false;
            this.txtUserReason3.CharacterCasing = CharacterCasing.Upper;
            this.txtUserReason3.LinkItem = null;
			this.txtUserReason3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUserReason3.LinkTopic = null;
			this.txtUserReason3.Location = new System.Drawing.Point(142, 280);
			this.txtUserReason3.Name = "txtUserReason3";
			this.txtUserReason3.Size = new System.Drawing.Size(390, 40);
			this.txtUserReason3.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtUserReason3, null);
			this.txtUserReason3.Visible = false;
			this.txtUserReason3.Enter += new System.EventHandler(this.txtUserReason3_Enter);
			this.txtUserReason3.Leave += new System.EventHandler(this.txtUserReason3_Leave);
			// 
			// lblUserField1
			// 
			this.lblUserField1.BackColor = System.Drawing.Color.Transparent;
			this.lblUserField1.Location = new System.Drawing.Point(20, 44);
			this.lblUserField1.Name = "lblUserField1";
			this.lblUserField1.Size = new System.Drawing.Size(122, 15);
			this.lblUserField1.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.lblUserField1, null);
			this.lblUserField1.Visible = false;
			// 
			// lblUserField2
			// 
			this.lblUserField2.BackColor = System.Drawing.Color.Transparent;
			this.lblUserField2.Location = new System.Drawing.Point(20, 144);
			this.lblUserField2.Name = "lblUserField2";
			this.lblUserField2.Size = new System.Drawing.Size(122, 15);
			this.lblUserField2.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.lblUserField2, null);
			this.lblUserField2.Visible = false;
			// 
			// lblUserField3
			// 
			this.lblUserField3.BackColor = System.Drawing.Color.Transparent;
			this.lblUserField3.Location = new System.Drawing.Point(20, 244);
			this.lblUserField3.Name = "lblUserField3";
			this.lblUserField3.Size = new System.Drawing.Size(122, 15);
			this.lblUserField3.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.lblUserField3, null);
			this.lblUserField3.Visible = false;
			// 
			// lblInfo1
			// 
			this.lblInfo1.BackColor = System.Drawing.Color.Transparent;
			this.lblInfo1.Location = new System.Drawing.Point(20, 94);
			this.lblInfo1.Name = "lblInfo1";
			this.lblInfo1.Size = new System.Drawing.Size(92, 15);
			this.lblInfo1.TabIndex = 10;
			this.lblInfo1.Text = "INFORMATION";
			this.ToolTip1.SetToolTip(this.lblInfo1, null);
			this.lblInfo1.Visible = false;
			// 
			// lblInfo2
			// 
			this.lblInfo2.BackColor = System.Drawing.Color.Transparent;
			this.lblInfo2.Location = new System.Drawing.Point(20, 194);
			this.lblInfo2.Name = "lblInfo2";
			this.lblInfo2.Size = new System.Drawing.Size(92, 15);
			this.lblInfo2.TabIndex = 9;
			this.lblInfo2.Text = "INFORMATION";
			this.ToolTip1.SetToolTip(this.lblInfo2, null);
			this.lblInfo2.Visible = false;
			// 
			// lblInfo3
			// 
			this.lblInfo3.BackColor = System.Drawing.Color.Transparent;
			this.lblInfo3.Location = new System.Drawing.Point(20, 294);
			this.lblInfo3.Name = "lblInfo3";
			this.lblInfo3.Size = new System.Drawing.Size(92, 15);
			this.lblInfo3.TabIndex = 8;
			this.lblInfo3.Text = "INFORMATION";
			this.ToolTip1.SetToolTip(this.lblInfo3, null);
			this.lblInfo3.Visible = false;
			// 
			// txtEMail
			// 
			this.txtEMail.AutoSize = false;
            this.txtEMail.CharacterCasing = CharacterCasing.Upper;
            this.txtEMail.LinkItem = null;
			this.txtEMail.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEMail.LinkTopic = null;
			this.txtEMail.Location = new System.Drawing.Point(146, 80);
			this.txtEMail.Name = "txtEMail";
			this.txtEMail.Size = new System.Drawing.Size(324, 40);
			this.txtEMail.TabIndex = 24;
			this.ToolTip1.SetToolTip(this.txtEMail, null);
			this.txtEMail.Enter += new System.EventHandler(this.txtEMail_Enter);
			this.txtEMail.Leave += new System.EventHandler(this.txtEMail_Leave);
			// 
			// lblEMail
			// 
			this.lblEMail.BackColor = System.Drawing.Color.Transparent;
			this.lblEMail.Location = new System.Drawing.Point(20, 94);
			this.lblEMail.Name = "lblEMail";
			this.lblEMail.Size = new System.Drawing.Size(98, 15);
			this.lblEMail.TabIndex = 25;
			this.lblEMail.Text = "E-MAIL ADDRESS";
			this.ToolTip1.SetToolTip(this.lblEMail, null);
			// 
			// fraFeesInfo
			// 
			this.fraFeesInfo.Controls.Add(this.cmdReturn);
			this.fraFeesInfo.Controls.Add(this.cmdExit);
			this.fraFeesInfo.Controls.Add(this.txtFEEAgentFeeReg);
			this.fraFeesInfo.Controls.Add(this.txtFEESalesTax);
			this.fraFeesInfo.Controls.Add(this.txtFEETitleFee);
			this.fraFeesInfo.Controls.Add(this.txtFEETotal);
			this.fraFeesInfo.Controls.Add(this.txtFEECreditTransfer);
			this.fraFeesInfo.Controls.Add(this.txtFEERegFee);
			this.fraFeesInfo.Controls.Add(this.txtFeeAgentFeeCTA);
			this.fraFeesInfo.Controls.Add(this.txtFEETransferFee);
			this.fraFeesInfo.Controls.Add(this.txtFeeAgentFeeTransfer);
			this.fraFeesInfo.Controls.Add(this.txtFeeAgentFeeCorrection);
			this.fraFeesInfo.Controls.Add(this.txtFEERushTitleFee);
			this.fraFeesInfo.Controls.Add(this.Label2_3);
			this.fraFeesInfo.Controls.Add(this.Label2_2);
			this.fraFeesInfo.Controls.Add(this.Label2_1);
			this.fraFeesInfo.Controls.Add(this.Label2_5);
			this.fraFeesInfo.Controls.Add(this.Label2_6);
			this.fraFeesInfo.Controls.Add(this.Label2_8);
			this.fraFeesInfo.Controls.Add(this.Line1);
			this.fraFeesInfo.Controls.Add(this.Label2_10);
			this.fraFeesInfo.Controls.Add(this.Label33);
			this.fraFeesInfo.Controls.Add(this.Label34);
			this.fraFeesInfo.Controls.Add(this.Label2_4);
			this.fraFeesInfo.Controls.Add(this.Label35);
			this.fraFeesInfo.Controls.Add(this.Label2_7);
			this.fraFeesInfo.Location = new System.Drawing.Point(30, 184);
			this.fraFeesInfo.Name = "fraFeesInfo";
			this.fraFeesInfo.Size = new System.Drawing.Size(490, 650);
			this.fraFeesInfo.TabIndex = 26;
			this.fraFeesInfo.Text = "Fees Information";
			this.ToolTip1.SetToolTip(this.fraFeesInfo, null);
			this.fraFeesInfo.Visible = false;
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(20, 590);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(80, 40);
			this.cmdReturn.TabIndex = 28;
			this.cmdReturn.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdReturn, null);
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// cmdExit
			// 
			this.cmdExit.AppearanceKey = "actionButton";
			this.cmdExit.Location = new System.Drawing.Point(120, 590);
			this.cmdExit.Name = "cmdExit";
			this.cmdExit.Size = new System.Drawing.Size(71, 40);
			this.cmdExit.TabIndex = 27;
			this.cmdExit.Text = "Exit";
			this.ToolTip1.SetToolTip(this.cmdExit, null);
			this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
			// 
			// txtFEEAgentFeeReg
			// 
			this.txtFEEAgentFeeReg.Location = new System.Drawing.Point(280, 30);
			this.txtFEEAgentFeeReg.MaxLength = 8;
			this.txtFEEAgentFeeReg.Name = "txtFEEAgentFeeReg";
			this.txtFEEAgentFeeReg.Size = new System.Drawing.Size(190, 40);
			this.txtFEEAgentFeeReg.TabIndex = 29;
			this.txtFEEAgentFeeReg.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEEAgentFeeReg, null);
			this.txtFEEAgentFeeReg.Enter += new System.EventHandler(this.txtFEEAgentFeeReg_Enter);
			this.txtFEEAgentFeeReg.Leave += new System.EventHandler(this.txtFEEAgentFeeReg_Leave);
			this.txtFEEAgentFeeReg.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEEAgentFeeReg_Validate);
			// 
			// txtFEESalesTax
			// 
			this.txtFEESalesTax.Location = new System.Drawing.Point(280, 380);
			this.txtFEESalesTax.MaxLength = 8;
			this.txtFEESalesTax.Name = "txtFEESalesTax";
			this.txtFEESalesTax.Size = new System.Drawing.Size(190, 40);
			this.txtFEESalesTax.TabIndex = 30;
			this.txtFEESalesTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEESalesTax, null);
			this.txtFEESalesTax.Enter += new System.EventHandler(this.txtFEESalesTax_Enter);
			this.txtFEESalesTax.Leave += new System.EventHandler(this.txtFEESalesTax_Leave);
			this.txtFEESalesTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEESalesTax_Validate);
			// 
			// txtFEETitleFee
			// 
			this.txtFEETitleFee.Location = new System.Drawing.Point(280, 430);
			this.txtFEETitleFee.MaxLength = 8;
			this.txtFEETitleFee.Name = "txtFEETitleFee";
			this.txtFEETitleFee.Size = new System.Drawing.Size(190, 40);
			this.txtFEETitleFee.TabIndex = 31;
			this.txtFEETitleFee.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEETitleFee, null);
			this.txtFEETitleFee.Enter += new System.EventHandler(this.txtFEETitleFee_Enter);
			this.txtFEETitleFee.Leave += new System.EventHandler(this.txtFEETitleFee_Leave);
			this.txtFEETitleFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEETitleFee_Validate);
			// 
			// txtFEETotal
			// 
			this.txtFEETotal.Location = new System.Drawing.Point(280, 530);
			this.txtFEETotal.MaxLength = 8;
			this.txtFEETotal.Name = "txtFEETotal";
			this.txtFEETotal.Size = new System.Drawing.Size(190, 40);
			this.txtFEETotal.TabIndex = 32;
			this.txtFEETotal.TabStop = false;
			this.txtFEETotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEETotal, null);
			// 
			// txtFEECreditTransfer
			// 
			this.txtFEECreditTransfer.Location = new System.Drawing.Point(280, 280);
			this.txtFEECreditTransfer.MaxLength = 8;
			this.txtFEECreditTransfer.Name = "txtFEECreditTransfer";
			this.txtFEECreditTransfer.Size = new System.Drawing.Size(190, 40);
			this.txtFEECreditTransfer.TabIndex = 33;
			this.txtFEECreditTransfer.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEECreditTransfer, null);
			this.txtFEECreditTransfer.Enter += new System.EventHandler(this.txtFEECreditTransfer_Enter);
			this.txtFEECreditTransfer.Leave += new System.EventHandler(this.txtFEECreditTransfer_Leave);
			this.txtFEECreditTransfer.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEECreditTransfer_Validate);
			// 
			// txtFEERegFee
			// 
			this.txtFEERegFee.Location = new System.Drawing.Point(280, 230);
			this.txtFEERegFee.MaxLength = 8;
			this.txtFEERegFee.Name = "txtFEERegFee";
			this.txtFEERegFee.Size = new System.Drawing.Size(190, 40);
			this.txtFEERegFee.TabIndex = 34;
			this.txtFEERegFee.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEERegFee, null);
			this.txtFEERegFee.Enter += new System.EventHandler(this.txtFEERegFee_Enter);
			this.txtFEERegFee.Leave += new System.EventHandler(this.txtFEERegFee_Leave);
			this.txtFEERegFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEERegFee_Validate);
			// 
			// txtFeeAgentFeeCTA
			// 
			this.txtFeeAgentFeeCTA.Location = new System.Drawing.Point(280, 80);
			this.txtFeeAgentFeeCTA.MaxLength = 8;
			this.txtFeeAgentFeeCTA.Name = "txtFeeAgentFeeCTA";
			this.txtFeeAgentFeeCTA.Size = new System.Drawing.Size(190, 40);
			this.txtFeeAgentFeeCTA.TabIndex = 35;
			this.txtFeeAgentFeeCTA.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFeeAgentFeeCTA, null);
			this.txtFeeAgentFeeCTA.Enter += new System.EventHandler(this.txtFeeAgentFeeCTA_Enter);
			this.txtFeeAgentFeeCTA.Leave += new System.EventHandler(this.txtFeeAgentFeeCTA_Leave);
			this.txtFeeAgentFeeCTA.Validating += new System.ComponentModel.CancelEventHandler(this.txtFeeAgentFeeCTA_Validate);
			// 
			// txtFEETransferFee
			// 
			this.txtFEETransferFee.Location = new System.Drawing.Point(280, 330);
			this.txtFEETransferFee.MaxLength = 8;
			this.txtFEETransferFee.Name = "txtFEETransferFee";
			this.txtFEETransferFee.Size = new System.Drawing.Size(190, 40);
			this.txtFEETransferFee.TabIndex = 36;
			this.txtFEETransferFee.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEETransferFee, null);
			this.txtFEETransferFee.Enter += new System.EventHandler(this.txtFEETransferFee_Enter);
			this.txtFEETransferFee.Leave += new System.EventHandler(this.txtFEETransferFee_Leave);
			this.txtFEETransferFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEETransferFee_Validate);
			// 
			// txtFeeAgentFeeTransfer
			// 
			this.txtFeeAgentFeeTransfer.Location = new System.Drawing.Point(280, 130);
			this.txtFeeAgentFeeTransfer.MaxLength = 8;
			this.txtFeeAgentFeeTransfer.Name = "txtFeeAgentFeeTransfer";
			this.txtFeeAgentFeeTransfer.Size = new System.Drawing.Size(190, 40);
			this.txtFeeAgentFeeTransfer.TabIndex = 37;
			this.txtFeeAgentFeeTransfer.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFeeAgentFeeTransfer, null);
			this.txtFeeAgentFeeTransfer.Enter += new System.EventHandler(this.txtFeeAgentFeeTransfer_Enter);
			this.txtFeeAgentFeeTransfer.Leave += new System.EventHandler(this.txtFeeAgentFeeTransfer_Leave);
			this.txtFeeAgentFeeTransfer.Validating += new System.ComponentModel.CancelEventHandler(this.txtFeeAgentFeeTransfer_Validate);
			// 
			// txtFeeAgentFeeCorrection
			// 
			this.txtFeeAgentFeeCorrection.Location = new System.Drawing.Point(280, 180);
			this.txtFeeAgentFeeCorrection.MaxLength = 8;
			this.txtFeeAgentFeeCorrection.Name = "txtFeeAgentFeeCorrection";
			this.txtFeeAgentFeeCorrection.Size = new System.Drawing.Size(190, 40);
			this.txtFeeAgentFeeCorrection.TabIndex = 38;
			this.txtFeeAgentFeeCorrection.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFeeAgentFeeCorrection, null);
			this.txtFeeAgentFeeCorrection.Enter += new System.EventHandler(this.txtFeeAgentFeeCorrection_Enter);
			this.txtFeeAgentFeeCorrection.Leave += new System.EventHandler(this.txtFeeAgentFeeCorrection_Leave);
			this.txtFeeAgentFeeCorrection.Validating += new System.ComponentModel.CancelEventHandler(this.txtFeeAgentFeeCorrection_Validate);
			// 
			// txtFEERushTitleFee
			// 
			this.txtFEERushTitleFee.Location = new System.Drawing.Point(280, 480);
			this.txtFEERushTitleFee.MaxLength = 8;
			this.txtFEERushTitleFee.Name = "txtFEERushTitleFee";
			this.txtFEERushTitleFee.Size = new System.Drawing.Size(190, 40);
			this.txtFEERushTitleFee.TabIndex = 39;
			this.txtFEERushTitleFee.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFEERushTitleFee, null);
			this.txtFEERushTitleFee.Enter += new System.EventHandler(this.txtFEERushTitleFee_Enter);
			this.txtFEERushTitleFee.Leave += new System.EventHandler(this.txtFEERushTitleFee_Leave);
			this.txtFEERushTitleFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEERushTitleFee_Validate);
			// 
			// Label2_3
			// 
			this.Label2_3.Location = new System.Drawing.Point(20, 344);
			this.Label2_3.Name = "Label2_3";
			this.Label2_3.Size = new System.Drawing.Size(98, 15);
			this.Label2_3.TabIndex = 51;
			this.Label2_3.Text = "TRANSFER FEE";
			this.ToolTip1.SetToolTip(this.Label2_3, null);
			// 
			// Label2_2
			// 
			this.Label2_2.Location = new System.Drawing.Point(155, 94);
			this.Label2_2.Name = "Label2_2";
			this.Label2_2.Size = new System.Drawing.Size(38, 15);
			this.Label2_2.TabIndex = 50;
			this.Label2_2.Text = "CTA";
			this.Label2_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.Label2_2, null);
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(163, 44);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(30, 15);
			this.Label2_1.TabIndex = 49;
			this.Label2_1.Text = "REG";
			this.Label2_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.Label2_1, null);
			// 
			// Label2_5
			// 
			this.Label2_5.Location = new System.Drawing.Point(20, 394);
			this.Label2_5.Name = "Label2_5";
			this.Label2_5.Size = new System.Drawing.Size(98, 15);
			this.Label2_5.TabIndex = 48;
			this.Label2_5.Text = "SALES TAX";
			this.ToolTip1.SetToolTip(this.Label2_5, null);
			// 
			// Label2_6
			// 
			this.Label2_6.Location = new System.Drawing.Point(20, 444);
			this.Label2_6.Name = "Label2_6";
			this.Label2_6.Size = new System.Drawing.Size(98, 15);
			this.Label2_6.TabIndex = 47;
			this.Label2_6.Text = "TITLE FEE";
			this.ToolTip1.SetToolTip(this.Label2_6, null);
			// 
			// Label2_8
			// 
			this.Label2_8.Enabled = false;
			this.Label2_8.Location = new System.Drawing.Point(20, 544);
			this.Label2_8.Name = "Label2_8";
			this.Label2_8.Size = new System.Drawing.Size(98, 15);
			this.Label2_8.TabIndex = 46;
			this.Label2_8.Text = "TOTAL FEES";
			this.ToolTip1.SetToolTip(this.Label2_8, null);
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line1.BorderWidth = ((short)(1));
			this.Line1.LineWidth = 0;
			this.Line1.Location = new System.Drawing.Point(300, 3136);
			this.Line1.Name = "Line1";
			this.Line1.Size = new System.Drawing.Size(3450, 1);
			this.ToolTip1.SetToolTip(this.Line1, null);
			this.Line1.X1 = 300F;
			this.Line1.X2 = 3750F;
			this.Line1.Y1 = 3136F;
			this.Line1.Y2 = 3136F;
			// 
			// Label2_10
			// 
			this.Label2_10.Location = new System.Drawing.Point(20, 294);
			this.Label2_10.Name = "Label2_10";
			this.Label2_10.Size = new System.Drawing.Size(111, 15);
			this.Label2_10.TabIndex = 45;
			this.Label2_10.Text = "CREDIT: TRANSFER";
			this.ToolTip1.SetToolTip(this.Label2_10, null);
			// 
			// Label33
			// 
			this.Label33.Location = new System.Drawing.Point(20, 244);
			this.Label33.Name = "Label33";
			this.Label33.Size = new System.Drawing.Size(123, 15);
			this.Label33.TabIndex = 44;
			this.Label33.Text = "REGISTRATION RATE";
			this.ToolTip1.SetToolTip(this.Label33, null);
			// 
			// Label34
			// 
			this.Label34.Location = new System.Drawing.Point(152, 144);
			this.Label34.Name = "Label34";
			this.Label34.Size = new System.Drawing.Size(77, 15);
			this.Label34.TabIndex = 43;
			this.Label34.Text = "TRANSFER";
			this.Label34.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.Label34, null);
			// 
			// Label2_4
			// 
			this.Label2_4.Location = new System.Drawing.Point(20, 44);
			this.Label2_4.Name = "Label2_4";
			this.Label2_4.Size = new System.Drawing.Size(72, 15);
			this.Label2_4.TabIndex = 42;
			this.Label2_4.Text = "AGENT FEE";
			this.ToolTip1.SetToolTip(this.Label2_4, null);
			// 
			// Label35
			// 
			this.Label35.Location = new System.Drawing.Point(162, 194);
			this.Label35.Name = "Label35";
			this.Label35.Size = new System.Drawing.Size(84, 15);
			this.Label35.TabIndex = 41;
			this.Label35.Text = "CORRECTION";
			this.Label35.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.Label35, null);
			// 
			// Label2_7
			// 
			this.Label2_7.Location = new System.Drawing.Point(20, 494);
			this.Label2_7.Name = "Label2_7";
			this.Label2_7.Size = new System.Drawing.Size(98, 15);
			this.Label2_7.TabIndex = 40;
			this.Label2_7.Text = "RUSH TITLE FEE";
			this.ToolTip1.SetToolTip(this.Label2_7, null);
			// 
			// txtRegistrationType
			// 
			this.txtRegistrationType.AutoSize = false;
            this.txtRegistrationType.CharacterCasing = CharacterCasing.Upper;
            this.txtRegistrationType.LinkItem = null;
			this.txtRegistrationType.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRegistrationType.LinkTopic = null;
			this.txtRegistrationType.Location = new System.Drawing.Point(459, 30);
			this.txtRegistrationType.Name = "txtRegistrationType";
			this.txtRegistrationType.Size = new System.Drawing.Size(176, 40);
			this.txtRegistrationType.TabIndex = 99;
			this.ToolTip1.SetToolTip(this.txtRegistrationType, null);
			// 
			// txtPlateType
			// 
			this.txtPlateType.AutoSize = false;
            this.txtPlateType.CharacterCasing = CharacterCasing.Upper;
            this.txtPlateType.LinkItem = null;
			this.txtPlateType.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPlateType.LinkTopic = null;
			this.txtPlateType.Location = new System.Drawing.Point(459, 80);
			this.txtPlateType.Name = "txtPlateType";
			this.txtPlateType.Size = new System.Drawing.Size(176, 40);
			this.txtPlateType.TabIndex = 100;
			this.ToolTip1.SetToolTip(this.txtPlateType, null);
			// 
			// txtStatus
			// 
			this.txtStatus.AutoSize = false;
            this.txtStatus.CharacterCasing = CharacterCasing.Upper;
            this.txtStatus.LinkItem = null;
			this.txtStatus.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStatus.LinkTopic = null;
			this.txtStatus.Location = new System.Drawing.Point(166, 30);
			this.txtStatus.MaxLength = 1;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Size = new System.Drawing.Size(112, 40);
			this.txtStatus.TabIndex = 102;
			this.ToolTip1.SetToolTip(this.txtStatus, null);
			// 
			// txtExpireDate
			// 
			this.txtExpireDate.Location = new System.Drawing.Point(166, 80);
			this.txtExpireDate.Mask = "##/##/####";
			this.txtExpireDate.MaxLength = 10;
			this.txtExpireDate.Name = "txtExpireDate";
			this.txtExpireDate.Size = new System.Drawing.Size(115, 40);
			this.txtExpireDate.TabIndex = 103;
			this.txtExpireDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtExpireDate, null);
			this.txtExpireDate.Enter += new System.EventHandler(this.txtExpireDate_Enter);
			this.txtExpireDate.Leave += new System.EventHandler(this.txtExpireDate_Leave);
			// 
			// txtEffectiveDate
			// 
			this.txtEffectiveDate.Location = new System.Drawing.Point(166, 130);
			this.txtEffectiveDate.Mask = "##/##/####";
			this.txtEffectiveDate.MaxLength = 10;
			this.txtEffectiveDate.Name = "txtEffectiveDate";
			this.txtEffectiveDate.Size = new System.Drawing.Size(115, 40);
			this.txtEffectiveDate.TabIndex = 104;
			this.txtEffectiveDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtEffectiveDate, null);
			// 
			// fraDataInput
			// 
			this.fraDataInput.AppearanceKey = "groupBoxNoBorders";
			this.fraDataInput.Controls.Add(this.Label29);
			this.fraDataInput.Controls.Add(this.Label23);
			this.fraDataInput.Controls.Add(this.Label19);
			this.fraDataInput.Controls.Add(this.Frame1);
			this.fraDataInput.Controls.Add(this.cmdMoreInfo);
			this.fraDataInput.Controls.Add(this.txtOpID);
			this.fraDataInput.Controls.Add(this.chk25YearPlate);
			this.fraDataInput.Controls.Add(this.cmdReg1Edit);
			this.fraDataInput.Controls.Add(this.cmdReg1Search);
			this.fraDataInput.Controls.Add(this.txtReg1PartyID);
			this.fraDataInput.Controls.Add(this.cmdReg2Edit);
			this.fraDataInput.Controls.Add(this.cmdReg2Search);
			this.fraDataInput.Controls.Add(this.txtReg2PartyID);
			this.fraDataInput.Controls.Add(this.txtNetWeight);
			this.fraDataInput.Controls.Add(this.txtMake);
			this.fraDataInput.Controls.Add(this.txtYear);
			this.fraDataInput.Controls.Add(this.txtUnit);
			this.fraDataInput.Controls.Add(this.txtColor1);
			this.fraDataInput.Controls.Add(this.txtColor2);
			this.fraDataInput.Controls.Add(this.txtPlate);
			this.fraDataInput.Controls.Add(this.txtStyle);
			this.fraDataInput.Controls.Add(this.txtVIN);
			this.fraDataInput.Controls.Add(this.txtTitle);
			this.fraDataInput.Controls.Add(this.txtAddress1);
			this.fraDataInput.Controls.Add(this.txtCity);
			this.fraDataInput.Controls.Add(this.txtZip);
			this.fraDataInput.Controls.Add(this.txtState);
			this.fraDataInput.Controls.Add(this.txtLegalResCity);
			this.fraDataInput.Controls.Add(this.txtLegalResState);
			this.fraDataInput.Controls.Add(this.txtRate);
			this.fraDataInput.Controls.Add(this.txtCredit);
			this.fraDataInput.Controls.Add(this.txtFees);
			this.fraDataInput.Controls.Add(this.txtLegalZip);
			this.fraDataInput.Controls.Add(this.lblRegistrant1);
			this.fraDataInput.Controls.Add(this.lblRegistrant2);
			this.fraDataInput.Controls.Add(this.Label2_0);
			this.fraDataInput.Controls.Add(this.Label1);
			this.fraDataInput.Controls.Add(this.Label13);
			this.fraDataInput.Controls.Add(this.Label11);
			this.fraDataInput.Controls.Add(this.Label10);
			this.fraDataInput.Controls.Add(this.Label9);
			this.fraDataInput.Controls.Add(this.Label14);
			this.fraDataInput.Controls.Add(this.Label6);
			this.fraDataInput.Controls.Add(this.Label5);
			this.fraDataInput.Controls.Add(this.Label12);
			this.fraDataInput.Controls.Add(this.Label24);
			this.fraDataInput.Controls.Add(this.Label20);
			this.fraDataInput.Controls.Add(this.Label22);
			this.fraDataInput.Controls.Add(this.Label16);
			this.fraDataInput.Controls.Add(this.Label42);
			this.fraDataInput.Controls.Add(this.Label4);
			this.fraDataInput.Controls.Add(this.Label31);
			this.fraDataInput.Controls.Add(this.Label27);
			this.fraDataInput.Controls.Add(this.Label8);
			this.fraDataInput.Controls.Add(this.Label7);
			this.fraDataInput.Controls.Add(this.Label15);
			this.fraDataInput.Controls.Add(this.Label17);
			this.fraDataInput.Controls.Add(this.Label21);
			this.fraDataInput.Controls.Add(this.Label18);
			this.fraDataInput.Controls.Add(this.Label25);
			this.fraDataInput.Controls.Add(this.Label26);
			this.fraDataInput.Controls.Add(this.Label28);
			this.fraDataInput.Controls.Add(this.Label30);
			this.fraDataInput.Location = new System.Drawing.Point(10, 174);
			this.fraDataInput.Name = "fraDataInput";
			this.fraDataInput.Size = new System.Drawing.Size(915, 636);
			this.fraDataInput.TabIndex = 52;
			this.ToolTip1.SetToolTip(this.fraDataInput, null);
			// 
			// Label29
			// 
			this.Label29.BackColor = System.Drawing.Color.Transparent;
			this.Label29.Location = new System.Drawing.Point(240, 549);
			this.Label29.Name = "Label29";
			this.Label29.Size = new System.Drawing.Size(43, 16);
			this.Label29.TabIndex = 80;
			this.Label29.Text = "CREDIT";
			this.ToolTip1.SetToolTip(this.Label29, null);
			this.Label29.Click += new System.EventHandler(this.Label29_Click);
			// 
			// Label23
			// 
			this.Label23.BackColor = System.Drawing.Color.Transparent;
			this.Label23.Location = new System.Drawing.Point(18, 494);
			this.Label23.Name = "Label23";
			this.Label23.Size = new System.Drawing.Size(70, 25);
			this.Label23.TabIndex = 77;
			this.Label23.Text = "LEGAL RESIDENCE";
			this.ToolTip1.SetToolTip(this.Label23, null);
			// 
			// Label19
			// 
			this.Label19.BackColor = System.Drawing.Color.Transparent;
			this.Label19.Location = new System.Drawing.Point(18, 405);
			this.Label19.Name = "Label19";
			this.Label19.Size = new System.Drawing.Size(60, 31);
			this.Label19.TabIndex = 76;
			this.Label19.Text = "MAILING ADDRESS";
			this.ToolTip1.SetToolTip(this.Label19, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtFleetName);
			this.Frame1.Controls.Add(this.cmbFleet);
			this.Frame1.Controls.Add(this.txtFleetNumber);
			this.Frame1.Controls.Add(this.lblFleetName);
			this.Frame1.Controls.Add(this.imgFleetComment);
			this.Frame1.Controls.Add(this.lblFleetNumber);
			this.Frame1.Location = new System.Drawing.Point(495, 250);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(401, 200);
			this.Frame1.TabIndex = 127;
			this.Frame1.Text = "Fleet/Group";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// txtFleetName
			// 
			this.txtFleetName.AutoSize = false;
			this.txtFleetName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFleetName.Enabled = false;
			this.txtFleetName.LinkItem = null;
			this.txtFleetName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFleetName.LinkTopic = null;
			this.txtFleetName.Location = new System.Drawing.Point(214, 140);
			this.txtFleetName.Name = "txtFleetName";
			this.txtFleetName.Size = new System.Drawing.Size(167, 40);
			this.txtFleetName.TabIndex = 129;
			this.txtFleetName.TabStop = false;
			this.ToolTip1.SetToolTip(this.txtFleetName, null);
			// 
			// txtFleetNumber
			// 
			this.txtFleetNumber.AutoSize = false;
			this.txtFleetNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtFleetNumber.Enabled = false;
			this.txtFleetNumber.LinkItem = null;
			this.txtFleetNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFleetNumber.LinkTopic = null;
			this.txtFleetNumber.Location = new System.Drawing.Point(214, 90);
			this.txtFleetNumber.Name = "txtFleetNumber";
			this.txtFleetNumber.Size = new System.Drawing.Size(107, 40);
			this.txtFleetNumber.TabIndex = 128;
			this.txtFleetNumber.TabStop = false;
			this.ToolTip1.SetToolTip(this.txtFleetNumber, null);
			this.txtFleetNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtFleetNumber_Validating);
			// 
			// lblFleetName
			// 
			this.lblFleetName.Enabled = false;
			this.lblFleetName.Location = new System.Drawing.Point(20, 154);
			this.lblFleetName.Name = "lblFleetName";
			this.lblFleetName.Size = new System.Drawing.Size(90, 14);
			this.lblFleetName.TabIndex = 134;
			this.lblFleetName.Text = "FLEET NAME";
			this.ToolTip1.SetToolTip(this.lblFleetName, null);
			// 
			// imgFleetComment
			// 
			this.imgFleetComment.AllowDrop = true;
			this.imgFleetComment.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgFleetComment.DrawStyle = ((short)(0));
			this.imgFleetComment.DrawWidth = ((short)(1));
			this.imgFleetComment.FillStyle = ((short)(1));
			this.imgFleetComment.FontTransparent = true;
			this.imgFleetComment.Image = ((System.Drawing.Image)(resources.GetObject("imgFleetComment.Image")));
			this.imgFleetComment.Location = new System.Drawing.Point(341, 90);
			this.imgFleetComment.Name = "imgFleetComment";
			this.imgFleetComment.Picture = ((System.Drawing.Image)(resources.GetObject("imgFleetComment.Picture")));
			this.imgFleetComment.Size = new System.Drawing.Size(40, 40);
			this.imgFleetComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgFleetComment.TabIndex = 135;
			this.ToolTip1.SetToolTip(this.imgFleetComment, null);
			this.imgFleetComment.Visible = false;
			this.imgFleetComment.Click += new System.EventHandler(this.imgFleetComment_Click);
			// 
			// lblFleetNumber
			// 
			this.lblFleetNumber.Enabled = false;
			this.lblFleetNumber.Location = new System.Drawing.Point(20, 96);
			this.lblFleetNumber.Name = "lblFleetNumber";
			this.lblFleetNumber.Size = new System.Drawing.Size(165, 24);
			this.lblFleetNumber.TabIndex = 133;
			this.lblFleetNumber.Text = "ENTER FLEET NUMBER  (ENTER \'A\' TO ADD/SEARCH)";
			this.lblFleetNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblFleetNumber, null);
			// 
			// cmdMoreInfo
			// 
			this.cmdMoreInfo.AppearanceKey = "actionButton";
			this.cmdMoreInfo.Location = new System.Drawing.Point(495, 470);
			this.cmdMoreInfo.Name = "cmdMoreInfo";
			this.cmdMoreInfo.Size = new System.Drawing.Size(215, 40);
			this.cmdMoreInfo.TabIndex = 123;
			this.cmdMoreInfo.TabStop = false;
			this.cmdMoreInfo.Text = "Additional Information";
			this.ToolTip1.SetToolTip(this.cmdMoreInfo, "If additional information needs to be entered but not appear on the registration");
			this.cmdMoreInfo.Click += new System.EventHandler(this.cmdMoreInfo_Click);
			// 
			// txtOpID
			// 
			this.txtOpID.AutoSize = false;
			this.txtOpID.Enabled = false;
			this.txtOpID.LinkItem = null;
			this.txtOpID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOpID.LinkTopic = null;
			this.txtOpID.Location = new System.Drawing.Point(576, 530);
			this.txtOpID.LockedOriginal = true;
			this.txtOpID.Name = "txtOpID";
			this.txtOpID.ReadOnly = true;
			this.txtOpID.Size = new System.Drawing.Size(134, 40);
			this.txtOpID.TabIndex = 120;
			this.txtOpID.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtOpID, "SHows the ID of the processing agent");
			// 
			// chk25YearPlate
			// 
			this.chk25YearPlate.Location = new System.Drawing.Point(576, 590);
			this.chk25YearPlate.Name = "chk25YearPlate";
			this.chk25YearPlate.Size = new System.Drawing.Size(22, 26);
			this.chk25YearPlate.TabIndex = 119;
			this.ToolTip1.SetToolTip(this.chk25YearPlate, null);
			// 
			// cmdReg1Edit
			// 
			this.cmdReg1Edit.AppearanceKey = "toolbarButton";
			this.cmdReg1Edit.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg1Edit.Image")));
			this.cmdReg1Edit.Location = new System.Drawing.Point(260, 250);
			this.cmdReg1Edit.Name = "cmdReg1Edit";
			this.cmdReg1Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdReg1Edit.TabIndex = 113;
			this.cmdReg1Edit.TabStop = false;
			this.ToolTip1.SetToolTip(this.cmdReg1Edit, null);
			this.cmdReg1Edit.Click += new System.EventHandler(this.cmdReg1Edit_Click);
			// 
			// cmdReg1Search
			// 
			this.cmdReg1Search.AppearanceKey = "toolbarButton";
			this.cmdReg1Search.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg1Search.Image")));
			this.cmdReg1Search.Location = new System.Drawing.Point(215, 250);
			this.cmdReg1Search.Name = "cmdReg1Search";
			this.cmdReg1Search.Size = new System.Drawing.Size(40, 40);
			this.cmdReg1Search.TabIndex = 112;
			this.cmdReg1Search.TabStop = false;
			this.ToolTip1.SetToolTip(this.cmdReg1Search, null);
			this.cmdReg1Search.Click += new System.EventHandler(this.cmdReg1Search_Click);
			// 
			// txtReg1PartyID
			// 
			this.txtReg1PartyID.AutoSize = false;
			this.txtReg1PartyID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.txtReg1PartyID.LinkItem = null;
			this.txtReg1PartyID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReg1PartyID.LinkTopic = null;
			this.txtReg1PartyID.Location = new System.Drawing.Point(110, 250);
			this.txtReg1PartyID.Name = "txtReg1PartyID";
			this.txtReg1PartyID.Size = new System.Drawing.Size(100, 40);
			this.txtReg1PartyID.TabIndex = 111;
			this.ToolTip1.SetToolTip(this.txtReg1PartyID, null);
			this.txtReg1PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg1PartyID_KeyPress);
			this.txtReg1PartyID.Enter += new System.EventHandler(this.txtReg1PartyID_Enter);
			this.txtReg1PartyID.Leave += new System.EventHandler(this.txtReg1PartyID_Leave);
			this.txtReg1PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1PartyID_Validating);
			// 
			// cmdReg2Edit
			// 
			this.cmdReg2Edit.AppearanceKey = "toolbarButton";
			this.cmdReg2Edit.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg2Edit.Image")));
			this.cmdReg2Edit.Location = new System.Drawing.Point(260, 300);
			this.cmdReg2Edit.Name = "cmdReg2Edit";
			this.cmdReg2Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdReg2Edit.TabIndex = 110;
			this.cmdReg2Edit.TabStop = false;
			this.ToolTip1.SetToolTip(this.cmdReg2Edit, null);
			this.cmdReg2Edit.Click += new System.EventHandler(this.cmdReg2Edit_Click);
			// 
			// cmdReg2Search
			// 
			this.cmdReg2Search.AppearanceKey = "toolbarButton";
			this.cmdReg2Search.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg2Search.Image")));
			this.cmdReg2Search.Location = new System.Drawing.Point(215, 300);
			this.cmdReg2Search.Name = "cmdReg2Search";
			this.cmdReg2Search.Size = new System.Drawing.Size(40, 40);
			this.cmdReg2Search.TabIndex = 109;
			this.cmdReg2Search.TabStop = false;
			this.ToolTip1.SetToolTip(this.cmdReg2Search, null);
			this.cmdReg2Search.Click += new System.EventHandler(this.cmdReg2Search_Click);
			// 
			// txtReg2PartyID
			// 
			this.txtReg2PartyID.AutoSize = false;
			this.txtReg2PartyID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.txtReg2PartyID.LinkItem = null;
			this.txtReg2PartyID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReg2PartyID.LinkTopic = null;
			this.txtReg2PartyID.Location = new System.Drawing.Point(110, 300);
			this.txtReg2PartyID.Name = "txtReg2PartyID";
			this.txtReg2PartyID.Size = new System.Drawing.Size(100, 40);
			this.txtReg2PartyID.TabIndex = 108;
			this.ToolTip1.SetToolTip(this.txtReg2PartyID, null);
			this.txtReg2PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg2PartyID_KeyPress);
			this.txtReg2PartyID.Enter += new System.EventHandler(this.txtReg2PartyID_Enter);
			this.txtReg2PartyID.Leave += new System.EventHandler(this.txtReg2PartyID_Leave);
			this.txtReg2PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2PartyID_Validating);
			// 
			// txtNetWeight
			// 
			this.txtNetWeight.MaxLength = 6;
			this.txtNetWeight.AutoSize = false;
			this.txtNetWeight.BackColor = System.Drawing.SystemColors.Window;
			this.txtNetWeight.LinkItem = null;
			this.txtNetWeight.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNetWeight.LinkTopic = null;
			this.txtNetWeight.Location = new System.Drawing.Point(495, 200);
			this.txtNetWeight.Name = "txtNetWeight";
			this.txtNetWeight.Size = new System.Drawing.Size(200, 40);
			this.txtNetWeight.TabIndex = 53;
			this.ToolTip1.SetToolTip(this.txtNetWeight, null);
			this.txtNetWeight.Enter += new System.EventHandler(this.txtNetWeight_Enter);
			this.txtNetWeight.Leave += new System.EventHandler(this.txtNetWeight_Leave);
			// 
			// txtMake
			// 
			this.txtMake.AutoSize = false;
            this.txtMake.CharacterCasing = CharacterCasing.Upper;
            this.txtMake.LinkItem = null;
			this.txtMake.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMake.LinkTopic = null;
			this.txtMake.Location = new System.Drawing.Point(20, 125);
			this.txtMake.MaxLength = 4;
			this.txtMake.Name = "txtMake";
			this.txtMake.Size = new System.Drawing.Size(70, 40);
			this.txtMake.TabIndex = 54;
			this.ToolTip1.SetToolTip(this.txtMake, null);
			this.txtMake.Enter += new System.EventHandler(this.txtMake_Enter);
			this.txtMake.Leave += new System.EventHandler(this.txtMake_Leave);
			// 
			// txtYear
			// 
			this.txtYear.AutoSize = false;
            this.txtYear.CharacterCasing = CharacterCasing.Upper;
            this.txtYear.LinkItem = null;
			this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYear.LinkTopic = null;
			this.txtYear.Location = new System.Drawing.Point(110, 125);
			this.txtYear.MaxLength = 4;
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(70, 40);
			this.txtYear.TabIndex = 55;
			this.ToolTip1.SetToolTip(this.txtYear, null);
			this.txtYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYear_KeyPressEvent);
			this.txtYear.Enter += new System.EventHandler(this.txtYear_Enter);
			this.txtYear.Leave += new System.EventHandler(this.txtYear_Leave);
			// 
			// txtUnit
			// 
			this.txtUnit.AutoSize = false;
            this.txtUnit.CharacterCasing = CharacterCasing.Upper;
            this.txtUnit.LinkItem = null;
			this.txtUnit.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUnit.LinkTopic = null;
			this.txtUnit.Location = new System.Drawing.Point(200, 125);
			this.txtUnit.MaxLength = 12;
			this.txtUnit.Name = "txtUnit";
			this.txtUnit.Size = new System.Drawing.Size(150, 40);
			this.txtUnit.TabIndex = 56;
			this.ToolTip1.SetToolTip(this.txtUnit, null);
			this.txtUnit.Enter += new System.EventHandler(this.txtUnit_Enter);
			this.txtUnit.Leave += new System.EventHandler(this.txtUnit_Leave);
			// 
			// txtColor1
			// 
			this.txtColor1.AutoSize = false;
            this.txtColor1.CharacterCasing = CharacterCasing.Upper;
            this.txtColor1.LinkItem = null;
			this.txtColor1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtColor1.LinkTopic = null;
			this.txtColor1.Location = new System.Drawing.Point(370, 125);
			this.txtColor1.MaxLength = 2;
			this.txtColor1.Name = "txtColor1";
			this.txtColor1.Size = new System.Drawing.Size(50, 40);
			this.txtColor1.TabIndex = 57;
			this.ToolTip1.SetToolTip(this.txtColor1, null);
			this.txtColor1.Enter += new System.EventHandler(this.txtColor1_Enter);
			this.txtColor1.Leave += new System.EventHandler(this.txtColor1_Leave);
			// 
			// txtColor2
			// 
			this.txtColor2.AutoSize = false;
            this.txtColor2.CharacterCasing = CharacterCasing.Upper;
            this.txtColor2.LinkItem = null;
			this.txtColor2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtColor2.LinkTopic = null;
			this.txtColor2.Location = new System.Drawing.Point(425, 125);
			this.txtColor2.MaxLength = 2;
			this.txtColor2.Name = "txtColor2";
			this.txtColor2.Size = new System.Drawing.Size(50, 40);
			this.txtColor2.TabIndex = 58;
			this.ToolTip1.SetToolTip(this.txtColor2, null);
			this.txtColor2.Enter += new System.EventHandler(this.txtColor2_Enter);
			this.txtColor2.Leave += new System.EventHandler(this.txtColor2_Leave);
			// 
			// txtPlate
			// 
			this.txtPlate.AutoSize = false;
            this.txtPlate.CharacterCasing = CharacterCasing.Upper;
            this.txtPlate.LinkItem = null;
			this.txtPlate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPlate.LinkTopic = null;
			this.txtPlate.Location = new System.Drawing.Point(495, 125);
			this.txtPlate.MaxLength = 8;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Size = new System.Drawing.Size(200, 40);
			this.txtPlate.TabIndex = 59;
			this.txtPlate.TabStop = false;
			this.ToolTip1.SetToolTip(this.txtPlate, null);
			this.txtPlate.Enter += new System.EventHandler(this.txtPlate_Enter);
			this.txtPlate.Leave += new System.EventHandler(this.txtPlate_Leave);
			// 
			// txtStyle
			// 
			this.txtStyle.AutoSize = false;
            this.txtStyle.CharacterCasing = CharacterCasing.Upper;
            this.txtStyle.LinkItem = null;
			this.txtStyle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStyle.LinkTopic = null;
			this.txtStyle.Location = new System.Drawing.Point(20, 200);
			this.txtStyle.MaxLength = 2;
			this.txtStyle.Name = "txtStyle";
			this.txtStyle.Size = new System.Drawing.Size(70, 40);
			this.txtStyle.TabIndex = 60;
			this.ToolTip1.SetToolTip(this.txtStyle, null);
			this.txtStyle.Enter += new System.EventHandler(this.txtStyle_Enter);
			this.txtStyle.Leave += new System.EventHandler(this.txtStyle_Leave);
			// 
			// txtVIN
			// 
			this.txtVIN.AutoSize = false;
            this.txtVIN.CharacterCasing = CharacterCasing.Upper;
            this.txtVIN.LinkItem = null;
			this.txtVIN.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVIN.LinkTopic = null;
			this.txtVIN.Location = new System.Drawing.Point(110, 200);
			this.txtVIN.MaxLength = 17;
			this.txtVIN.Name = "txtVIN";
			this.txtVIN.Size = new System.Drawing.Size(240, 40);
			this.txtVIN.TabIndex = 61;
			this.ToolTip1.SetToolTip(this.txtVIN, null);
			this.txtVIN.Enter += new System.EventHandler(this.txtVIN_Enter);
			this.txtVIN.Leave += new System.EventHandler(this.txtVIN_Leave);
			// 
			// txtTitle
			// 
			this.txtTitle.AutoSize = false;
            this.txtTitle.CharacterCasing = CharacterCasing.Upper;
            this.txtTitle.LinkItem = null;
			this.txtTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTitle.LinkTopic = null;
			this.txtTitle.Location = new System.Drawing.Point(370, 200);
			this.txtTitle.MaxLength = 11;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Size = new System.Drawing.Size(105, 40);
			this.txtTitle.TabIndex = 62;
			this.ToolTip1.SetToolTip(this.txtTitle, null);
			this.txtTitle.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle.Leave += new System.EventHandler(this.txtTitle_Leave);
			// 
			// txtAddress1
			// 
			this.txtAddress1.AutoSize = false;
            this.txtAddress1.CharacterCasing = CharacterCasing.Upper;
            this.txtAddress1.LinkItem = null;
			this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress1.LinkTopic = null;
			this.txtAddress1.Location = new System.Drawing.Point(110, 360);
			this.txtAddress1.MaxLength = 75;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(365, 40);
			this.txtAddress1.TabIndex = 63;
			this.ToolTip1.SetToolTip(this.txtAddress1, null);
			this.txtAddress1.Enter += new System.EventHandler(this.txtAddress1_Enter);
			this.txtAddress1.Leave += new System.EventHandler(this.txtAddress1_Leave);
			// 
			// txtCity
			// 
			this.txtCity.AutoSize = false;
            this.txtCity.CharacterCasing = CharacterCasing.Upper;
            this.txtCity.LinkItem = null;
			this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCity.LinkTopic = null;
			this.txtCity.Location = new System.Drawing.Point(110, 435);
			this.txtCity.MaxLength = 30;
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(184, 40);
			this.txtCity.TabIndex = 64;
			this.ToolTip1.SetToolTip(this.txtCity, null);
			this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
			this.txtCity.Leave += new System.EventHandler(this.txtCity_Leave);
			// 
			// txtZip
			// 
			this.txtZip.AutoSize = false;
            this.txtZip.CharacterCasing = CharacterCasing.Upper;
            this.txtZip.LinkItem = null;
			this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip.LinkTopic = null;
			this.txtZip.Location = new System.Drawing.Point(374, 435);
			this.txtZip.MaxLength = 5;
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(101, 40);
			this.txtZip.TabIndex = 65;
			this.ToolTip1.SetToolTip(this.txtZip, null);
			this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
			this.txtZip.Leave += new System.EventHandler(this.txtZip_Leave);
			// 
			// txtState
			// 
			this.txtState.AutoSize = false;
            this.txtState.CharacterCasing = CharacterCasing.Upper;
            this.txtState.LinkItem = null;
			this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtState.LinkTopic = null;
			this.txtState.Location = new System.Drawing.Point(304, 435);
			this.txtState.MaxLength = 2;
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(50, 40);
			this.txtState.TabIndex = 66;
			this.ToolTip1.SetToolTip(this.txtState, null);
			this.txtState.Enter += new System.EventHandler(this.txtState_Enter);
			this.txtState.Leave += new System.EventHandler(this.txtState_Leave);
			// 
			// txtLegalResCity
			// 
			this.txtLegalResCity.AutoSize = false;
            this.txtLegalResCity.CharacterCasing = CharacterCasing.Upper;
            this.txtLegalResCity.LinkItem = null;
			this.txtLegalResCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLegalResCity.LinkTopic = null;
			this.txtLegalResCity.Location = new System.Drawing.Point(110, 485);
			this.txtLegalResCity.MaxLength = 30;
			this.txtLegalResCity.Name = "txtLegalResCity";
			this.txtLegalResCity.Size = new System.Drawing.Size(245, 40);
			this.txtLegalResCity.TabIndex = 67;
			this.ToolTip1.SetToolTip(this.txtLegalResCity, null);
			this.txtLegalResCity.Enter += new System.EventHandler(this.txtLegalResCity_Enter);
			this.txtLegalResCity.Leave += new System.EventHandler(this.txtLegalResCity_Leave);
			// 
			// txtLegalResState
			// 
			this.txtLegalResState.AutoSize = false;
            this.txtLegalResState.CharacterCasing = CharacterCasing.Upper;
            this.txtLegalResState.LinkItem = null;
			this.txtLegalResState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLegalResState.LinkTopic = null;
			this.txtLegalResState.Location = new System.Drawing.Point(365, 485);
			this.txtLegalResState.MaxLength = 2;
			this.txtLegalResState.Name = "txtLegalResState";
			this.txtLegalResState.Size = new System.Drawing.Size(50, 40);
			this.txtLegalResState.TabIndex = 68;
			this.ToolTip1.SetToolTip(this.txtLegalResState, null);
			this.txtLegalResState.Enter += new System.EventHandler(this.txtLegalResState_Enter);
			this.txtLegalResState.Leave += new System.EventHandler(this.txtLegalResState_Leave);
			// 
			// txtRate
			// 
			this.txtRate.Enabled = false;
			this.txtRate.Location = new System.Drawing.Point(150, 535);
			this.txtRate.MaxLength = 9;
			this.txtRate.Name = "txtRate";
			this.txtRate.Size = new System.Drawing.Size(70, 40);
			this.txtRate.TabIndex = 69;
			this.txtRate.TabStop = false;
			this.txtRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtRate, null);
			this.txtRate.Enter += new System.EventHandler(this.txtRate_Enter);
			this.txtRate.Leave += new System.EventHandler(this.txtRate_Leave);
			this.txtRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtRate_Validate);
			// 
			// txtCredit
			// 
			this.txtCredit.Enabled = false;
			this.txtCredit.Location = new System.Drawing.Point(289, 535);
			this.txtCredit.MaxLength = 9;
			this.txtCredit.Name = "txtCredit";
			this.txtCredit.Size = new System.Drawing.Size(66, 40);
			this.txtCredit.TabIndex = 70;
			this.txtCredit.TabStop = false;
			this.txtCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtCredit, null);
			this.txtCredit.Enter += new System.EventHandler(this.txtCredit_Enter);
			this.txtCredit.Leave += new System.EventHandler(this.txtCredit_Leave);
			this.txtCredit.Validating += new System.ComponentModel.CancelEventHandler(this.txtCredit_Validate);
			// 
			// txtFees
			// 
			this.txtFees.Enabled = false;
			this.txtFees.Location = new System.Drawing.Point(410, 535);
			this.txtFees.MaxLength = 9;
			this.txtFees.Name = "txtFees";
			this.txtFees.Size = new System.Drawing.Size(65, 40);
			this.txtFees.TabIndex = 71;
			this.txtFees.TabStop = false;
			this.txtFees.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtFees, null);
			this.txtFees.Enter += new System.EventHandler(this.txtFees_Enter);
			this.txtFees.Leave += new System.EventHandler(this.txtFees_Leave);
			// 
			// txtLegalZip
			// 
			this.txtLegalZip.AutoSize = false;
            this.txtLegalZip.CharacterCasing = CharacterCasing.Upper;
            this.txtLegalZip.LinkItem = null;
			this.txtLegalZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLegalZip.LinkTopic = null;
			this.txtLegalZip.Location = new System.Drawing.Point(425, 485);
			this.txtLegalZip.MaxLength = 5;
			this.txtLegalZip.Name = "txtLegalZip";
			this.txtLegalZip.Size = new System.Drawing.Size(50, 40);
			this.txtLegalZip.TabIndex = 72;
			this.ToolTip1.SetToolTip(this.txtLegalZip, null);
			this.txtLegalZip.Enter += new System.EventHandler(this.txtLegalZip_Enter);
			this.txtLegalZip.Leave += new System.EventHandler(this.txtLegalZip_Leave);
			// 
			// lblRegistrant1
			// 
			this.lblRegistrant1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.lblRegistrant1.Location = new System.Drawing.Point(320, 264);
			this.lblRegistrant1.Name = "lblRegistrant1";
			this.lblRegistrant1.Size = new System.Drawing.Size(155, 15);
			this.lblRegistrant1.TabIndex = 118;
			this.ToolTip1.SetToolTip(this.lblRegistrant1, null);
			// 
			// lblRegistrant2
			// 
			this.lblRegistrant2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.lblRegistrant2.Location = new System.Drawing.Point(320, 314);
			this.lblRegistrant2.Name = "lblRegistrant2";
			this.lblRegistrant2.Size = new System.Drawing.Size(155, 16);
			this.lblRegistrant2.TabIndex = 117;
			this.ToolTip1.SetToolTip(this.lblRegistrant2, null);
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(20, 65);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(248, 15);
			this.Label2_0.TabIndex = 74;
			this.Label2_0.Text = "LONG TERM SEMI-TRAILER REGISTRATION";
			this.ToolTip1.SetToolTip(this.Label2_0, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(104, 15);
			this.Label1.TabIndex = 73;
			this.Label1.Text = "STATE OF MAINE";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// Label13
			// 
			this.Label13.AutoSize = true;
			this.Label13.BackColor = System.Drawing.SystemColors.Window;
			this.Label13.BorderStyle = 1;
			this.Label13.Location = new System.Drawing.Point(552, 175);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(85, 15);
			this.Label13.TabIndex = 91;
			this.Label13.Text = "NET WEIGHT";
			this.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label13, null);
			// 
			// Label11
			// 
			this.Label11.AutoSize = true;
			this.Label11.BackColor = System.Drawing.SystemColors.Window;
			this.Label11.BorderStyle = 1;
			this.Label11.Location = new System.Drawing.Point(389, 175);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(63, 15);
			this.Label11.TabIndex = 90;
			this.Label11.Text = "TITLE NO";
			this.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label11, null);
			// 
			// Label10
			// 
			this.Label10.BackColor = System.Drawing.SystemColors.Window;
			this.Label10.BorderStyle = 1;
			this.Label10.Location = new System.Drawing.Point(108, 175);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(242, 15);
			this.Label10.TabIndex = 89;
			this.Label10.Text = "VEHICLE IDENTIFICATION NO. (SERIAL NO)";
			this.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label10, null);
			// 
			// Label9
			// 
			this.Label9.AutoSize = true;
			this.Label9.BackColor = System.Drawing.SystemColors.Window;
			this.Label9.BorderStyle = 1;
			this.Label9.Location = new System.Drawing.Point(31, 175);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(47, 15);
			this.Label9.TabIndex = 88;
			this.Label9.Text = "STYLE";
			this.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label9, null);
			// 
			// Label14
			// 
			this.Label14.AutoSize = true;
			this.Label14.BackColor = System.Drawing.SystemColors.Window;
			this.Label14.BorderStyle = 1;
			this.Label14.Location = new System.Drawing.Point(395, 100);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(55, 15);
			this.Label14.TabIndex = 87;
			this.Label14.Text = " COLOR";
			this.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label14, null);
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.BackColor = System.Drawing.SystemColors.Window;
			this.Label6.BorderStyle = 1;
			this.Label6.Location = new System.Drawing.Point(248, 100);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(46, 15);
			this.Label6.TabIndex = 86;
			this.Label6.Text = "UNIT #";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.BackColor = System.Drawing.SystemColors.Window;
			this.Label5.BorderStyle = 1;
			this.Label5.Location = new System.Drawing.Point(124, 100);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(41, 15);
			this.Label5.TabIndex = 85;
			this.Label5.Text = "YEAR";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label12
			// 
			this.Label12.BackColor = System.Drawing.SystemColors.Window;
			this.Label12.BorderStyle = 1;
			this.Label12.Location = new System.Drawing.Point(32, 100);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(43, 15);
			this.Label12.TabIndex = 84;
			this.Label12.Text = "MAKE";
			this.Label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label12, null);
			// 
			// Label24
			// 
			this.Label24.BackColor = System.Drawing.SystemColors.Window;
			this.Label24.BorderStyle = 1;
			this.Label24.Location = new System.Drawing.Point(112, 489);
			this.Label24.Name = "Label24";
			this.Label24.Size = new System.Drawing.Size(325, 33);
			this.Label24.TabIndex = 78;
			this.Label24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label24, null);
			// 
			// Label20
			// 
			this.Label20.BackColor = System.Drawing.SystemColors.Window;
			this.Label20.BorderStyle = 1;
			this.Label20.Location = new System.Drawing.Point(113, 363);
			this.Label20.Name = "Label20";
			this.Label20.Size = new System.Drawing.Size(325, 33);
			this.Label20.TabIndex = 92;
			this.Label20.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label20, null);
			// 
			// Label22
			// 
			this.Label22.BackColor = System.Drawing.SystemColors.Window;
			this.Label22.BorderStyle = 1;
			this.Label22.Location = new System.Drawing.Point(391, 410);
			this.Label22.Name = "Label22";
			this.Label22.Size = new System.Drawing.Size(62, 15);
			this.Label22.TabIndex = 94;
			this.Label22.Text = "ZIP CODE";
			this.Label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label22, null);
			// 
			// Label16
			// 
			this.Label16.AutoSize = true;
			this.Label16.BackColor = System.Drawing.Color.Transparent;
			this.Label16.Location = new System.Drawing.Point(20, 286);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(60, 15);
			this.Label16.TabIndex = 114;
			this.Label16.Text = "NAME(S)";
			this.ToolTip1.SetToolTip(this.Label16, null);
			// 
			// Label42
			// 
			this.Label42.BackColor = System.Drawing.Color.Transparent;
			this.Label42.Location = new System.Drawing.Point(495, 544);
			this.Label42.Name = "Label42";
			this.Label42.Size = new System.Drawing.Size(53, 14);
			this.Label42.TabIndex = 125;
			this.Label42.Text = "USER ID";
			this.ToolTip1.SetToolTip(this.Label42, null);
			// 
			// Label4
			// 
			this.Label4.BackColor = System.Drawing.Color.Transparent;
			this.Label4.Location = new System.Drawing.Point(495, 595);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(56, 14);
			this.Label4.TabIndex = 124;
			this.Label4.Text = "25 YEAR";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// Label31
			// 
			this.Label31.BackColor = System.Drawing.Color.Transparent;
			this.Label31.Location = new System.Drawing.Point(375, 549);
			this.Label31.Name = "Label31";
			this.Label31.Size = new System.Drawing.Size(36, 16);
			this.Label31.TabIndex = 81;
			this.Label31.Text = "FEES";
			this.ToolTip1.SetToolTip(this.Label31, null);
			this.Label31.Click += new System.EventHandler(this.Label31_Click);
			// 
			// Label27
			// 
			this.Label27.BackColor = System.Drawing.Color.Transparent;
			this.Label27.Location = new System.Drawing.Point(110, 549);
			this.Label27.Name = "Label27";
			this.Label27.Size = new System.Drawing.Size(44, 15);
			this.Label27.TabIndex = 79;
			this.Label27.Text = "RATE";
			this.ToolTip1.SetToolTip(this.Label27, null);
			this.Label27.Click += new System.EventHandler(this.Label27_Click);
			// 
			// Label8
			// 
			this.Label8.BackColor = System.Drawing.Color.Transparent;
			this.Label8.Location = new System.Drawing.Point(579, 100);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(35, 16);
			this.Label8.TabIndex = 75;
			this.Label8.Text = "TLR";
			this.ToolTip1.SetToolTip(this.Label8, null);
			// 
			// Label7
			// 
			this.Label7.BackColor = System.Drawing.SystemColors.Window;
			this.Label7.BorderStyle = 1;
			this.Label7.Location = new System.Drawing.Point(481, 129);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(213, 33);
			this.Label7.TabIndex = 98;
			this.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label7, null);
			// 
			// Label15
			// 
			this.Label15.BackColor = System.Drawing.SystemColors.Window;
			this.Label15.BorderStyle = 1;
			this.Label15.Location = new System.Drawing.Point(20, 270);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(78, 54);
			this.Label15.TabIndex = 115;
			this.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label15, null);
			// 
			// Label17
			// 
			this.Label17.BackColor = System.Drawing.SystemColors.Window;
			this.Label17.BorderStyle = 1;
			this.Label17.Location = new System.Drawing.Point(104, 247);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(385, 97);
			this.Label17.TabIndex = 116;
			this.Label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label17, null);
			// 
			// Label21
			// 
			this.Label21.BackColor = System.Drawing.SystemColors.Window;
			this.Label21.BorderStyle = 1;
			this.Label21.Location = new System.Drawing.Point(113, 437);
			this.Label21.Name = "Label21";
			this.Label21.Size = new System.Drawing.Size(242, 33);
			this.Label21.TabIndex = 93;
			this.Label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label21, null);
			// 
			// Label18
			// 
			this.Label18.BackColor = System.Drawing.SystemColors.Window;
			this.Label18.BorderStyle = 1;
			this.Label18.Location = new System.Drawing.Point(10, 387);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(78, 64);
			this.Label18.TabIndex = 82;
			this.Label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label18, null);
			// 
			// Label25
			// 
			this.Label25.BackColor = System.Drawing.SystemColors.Window;
			this.Label25.BorderStyle = 1;
			this.Label25.Location = new System.Drawing.Point(18, 492);
			this.Label25.Name = "Label25";
			this.Label25.Size = new System.Drawing.Size(78, 33);
			this.Label25.TabIndex = 83;
			this.Label25.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label25, null);
			// 
			// Label26
			// 
			this.Label26.BackColor = System.Drawing.SystemColors.Window;
			this.Label26.BorderStyle = 1;
			this.Label26.Location = new System.Drawing.Point(106, 538);
			this.Label26.Name = "Label26";
			this.Label26.Size = new System.Drawing.Size(134, 33);
			this.Label26.TabIndex = 95;
			this.Label26.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label26, null);
			// 
			// Label28
			// 
			this.Label28.BackColor = System.Drawing.SystemColors.Window;
			this.Label28.BorderStyle = 1;
			this.Label28.Location = new System.Drawing.Point(226, 538);
			this.Label28.Name = "Label28";
			this.Label28.Size = new System.Drawing.Size(134, 33);
			this.Label28.TabIndex = 96;
			this.Label28.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label28, null);
			// 
			// Label30
			// 
			this.Label30.BackColor = System.Drawing.SystemColors.Window;
			this.Label30.BorderStyle = 1;
			this.Label30.Location = new System.Drawing.Point(356, 539);
			this.Label30.Name = "Label30";
			this.Label30.Size = new System.Drawing.Size(134, 33);
			this.Label30.TabIndex = 97;
			this.Label30.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label30, null);
			// 
			// Label38
			// 
			this.Label38.Location = new System.Drawing.Point(30, 144);
			this.Label38.Name = "Label38";
			this.Label38.Size = new System.Drawing.Size(114, 14);
			this.Label38.TabIndex = 107;
			this.Label38.Text = "EFFECTIVE DATE";
			this.ToolTip1.SetToolTip(this.Label38, null);
			// 
			// Label37
			// 
			this.Label37.Location = new System.Drawing.Point(30, 44);
			this.Label37.Name = "Label37";
			this.Label37.Size = new System.Drawing.Size(53, 15);
			this.Label37.TabIndex = 106;
			this.Label37.Text = "STATUS";
			this.ToolTip1.SetToolTip(this.Label37, null);
			// 
			// Label36
			// 
			this.Label36.Location = new System.Drawing.Point(30, 94);
			this.Label36.Name = "Label36";
			this.Label36.Size = new System.Drawing.Size(114, 14);
			this.Label36.TabIndex = 105;
			this.Label36.Text = "EXPIRATION DATE";
			this.ToolTip1.SetToolTip(this.Label36, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(308, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(122, 16);
			this.Label3.TabIndex = 101;
			this.Label3.Text = "LAST REGISTRATION";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFleetGroupComment,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFleetGroupComment
			// 
			this.mnuFleetGroupComment.Enabled = false;
			this.mnuFleetGroupComment.Index = 0;
			this.mnuFleetGroupComment.Name = "mnuFleetGroupComment";
			this.mnuFleetGroupComment.Text = "Fleet / Group Comment";
			this.mnuFleetGroupComment.Click += new System.EventHandler(this.mnuFleetGroupComment_Click);
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 1;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(408, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(130, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save & Exit";
			this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// cmdFleetGroupComment
			// 
			this.cmdFleetGroupComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFleetGroupComment.AppearanceKey = "toolbarButton";
			this.cmdFleetGroupComment.Enabled = false;
			this.cmdFleetGroupComment.Location = new System.Drawing.Point(798, 29);
			this.cmdFleetGroupComment.Name = "cmdFleetGroupComment";
			this.cmdFleetGroupComment.Size = new System.Drawing.Size(162, 24);
			this.cmdFleetGroupComment.TabIndex = 1;
			this.cmdFleetGroupComment.Text = "Fleet / Group Comment";
			this.ToolTip1.SetToolTip(this.cmdFleetGroupComment, null);
			this.cmdFleetGroupComment.Click += new System.EventHandler(this.mnuFleetGroupComment_Click);
			// 
			// frmVehicleUpdateLongTerm
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(988, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVehicleUpdateLongTerm";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "LTT New To System / Update ";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmVehicleUpdateLongTerm_Load);
			this.Activated += new System.EventHandler(this.frmVehicleUpdateLongTerm_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVehicleUpdateLongTerm_KeyPress);
			this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmVehicleUpdateLongTerm_KeyUp);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).EndInit();
			this.fraAdditionalInfo.ResumeLayout(false);
			this.fraAdditionalInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
			this.fraMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdErase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).EndInit();
			this.fraUserFields.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEMail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFeesInfo)).EndInit();
			this.fraFeesInfo.ResumeLayout(false);
			this.fraFeesInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEAgentFeeReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEESalesTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETitleFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEECreditTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEERegFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCTA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETransferFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCorrection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEERushTitleFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRegistrationType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlateType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDataInput)).EndInit();
			this.fraDataInput.ResumeLayout(false);
			this.fraDataInput.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk25YearPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtColor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalResCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalResState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFleetGroupComment)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdProcessSave;
        private FCButton cmdFleetGroupComment;
    }
}
