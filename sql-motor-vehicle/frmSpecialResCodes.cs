//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmSpecialResCodes : BaseForm
	{
		public frmSpecialResCodes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSpecialResCodes InstancePtr
		{
			get
			{
				return (frmSpecialResCodes)Sys.GetInstance(typeof(frmSpecialResCodes));
			}
		}

		protected frmSpecialResCodes _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		int CodeCol;
		int ResCodeCol;

		private void frmSpecialResCodes_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSpecialResCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSpecialResCodes properties;
			//frmSpecialResCodes.FillStyle	= 0;
			//frmSpecialResCodes.ScaleWidth	= 5880;
			//frmSpecialResCodes.ScaleHeight	= 4005;
			//frmSpecialResCodes.LinkTopic	= "Form2";
			//frmSpecialResCodes.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			CodeCol = 0;
			ResCodeCol = 1;
			vsCodes.TextMatrix(0, 0, "Code");
			vsCodes.TextMatrix(0, 1, "Res Code");
			vsCodes.EditMaxLength = 5;
			vsCodes.ColWidth(0, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.3263403));
			//vsCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsCodes.Rows - 1, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, vsCodes.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			counter = 1;
			rsInfo.OpenRecordset("SELECT * FROM SpecialResCode ORDER BY ID");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsCodes.TextMatrix(counter, CodeCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					vsCodes.TextMatrix(counter, ResCodeCol, FCConvert.ToString(rsInfo.Get_Fields("ResCode")));
					rsInfo.MoveNext();
					counter += 1;
				}
				while (rsInfo.EndOfFile() != true);
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSpecialResCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSpecialResCodes_Resize(object sender, System.EventArgs e)
		{
			vsCodes.ColWidth(0, FCConvert.ToInt32(vsCodes.WidthOriginal * 0.2263403));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsCheck = new clsDRWrapper();
			int counter;
			vsCodes.Select(0, 0);
			for (counter = 1; counter <= 8; counter++)
			{
				if (fecherFoundation.Strings.Trim(vsCodes.TextMatrix(counter, ResCodeCol)) != "")
				{
					rsCheck.OpenRecordset("SELECT * FROM Residence WHERE Code = '" + vsCodes.TextMatrix(counter, ResCodeCol) + "'");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						// do nothing
					}
					else
					{
						MessageBox.Show("Invalid Residence Code", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsCodes.Select(counter, ResCodeCol);
						return;
					}
				}
			}
			for (counter = 1; counter <= 8; counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM SpecialResCode WHERE ID = " + FCConvert.ToString(counter));
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					rsInfo.Edit();
					rsInfo.Set_Fields("ResCode", vsCodes.TextMatrix(counter, ResCodeCol));
					rsInfo.Update();
				}
			}
			Close();
		}

		private void vsCodes_Enter(object sender, System.EventArgs e)
		{
			if (vsCodes.Row > 0 && vsCodes.Col > 0)
			{
				vsCodes.EditCell();
			}
		}

		private void vsCodes_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsCodes_RowColChange(object sender, System.EventArgs e)
		{
			vsCodes.EditCell();
		}
	}
}
