//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Manufacturer;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Enums;
using SharedApplication.MotorVehicle.Interfaces;
using SharedApplication.MotorVehicle.Models;
using TWSharedLibrary;
using Wisej.Web;
using Color = System.Drawing.Color;

namespace TWMV0000
{
    public partial class frmDataInput : BaseForm
    {
        public frmDataInput()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
                _InstancePtr = this;
            //FC:FINAL:DDU:#2181 - allow only numeric to textboxes
            txtNetWeight.AllowOnlyNumericInput();
            txtRegWeight.AllowOnlyNumericInput();
            txtMileage.AllowOnlyNumericInput();
            //FC:FINAl:SBE - #2199 - allow only numeric input
            txtMStickerNumber.AllowOnlyNumericInput();
            txtYStickerNumber.AllowOnlyNumericInput();
            //FC:FINAL:DDU:#2183 - allow only 0, 1 and 2 numerics in textboxes
            txtMonthNoCharge.AllowKeysOnClientKeyPress(
                "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',48,49,50");
            txtYearNoCharge.AllowKeysOnClientKeyPress(
                "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',48,49,50");
            txtMonthCharge.AllowKeysOnClientKeyPress(
                "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',48,49,50");
            txtYearCharge.AllowKeysOnClientKeyPress(
                "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',48,49,50");
            
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmDataInput InstancePtr
        {
            get { return (frmDataInput)Sys.GetInstance(typeof(frmDataInput)); }
        }

        protected frmDataInput _InstancePtr = null;

        //=========================================================
        // API's
        //[DllImport("kernel32")]
        //private static extern int WaitForSingleObject(int hHandle, int dwMilliseconds);

        // Constants
        const int INFINITE = -1;

        int NewBackColor = ColorTranslator.ToOle(Color.Transparent);  // was Color.Yellow; removed for MV-27

        // &HD2FFFF

        public FleetGroupType fleetGroupType = FleetGroupType.None;

        Decimal AgentFee;

        bool MileageOK;

        // vbPorter upgrade warning: OldColor As int	OnWrite(Color, int)
        int OldColor;
        string WhichObject;
        int Wait;
        string ValidCode = "";
        bool DOTNumberRequired;
        bool TitleRequired;
        public bool FromAP;
        bool formClosing = false;
        bool bol2yCh;
        private IBlueBookRepository blueBookRepository;
        bool blnFinalCheck;

        // Dim BackwardsFlag               As Boolean   'kk01062016 tromv-1129  Fixing Tab and Shift-Tab behavior
        bool blnCompany;
        bool blnVerifyFlag;
        public bool blnFeesFlag;

        bool blnExpirationDateChanged;

        // vbPorter upgrade warning: datOriginalExpirationDate As DateTime	OnWrite(string)
        DateTime datOriginalExpirationDate;
        bool blnDontCheckDate;
        bool blnCalculateRequired;

        DateTime datNonFleetExpiration;

        // vbPorter upgrade warning: intCorrInitialPlate As int	OnWrite(int, DialogResult)
        DialogResult intCorrInitialPlate;

        // vbPorter upgrade warning: intCorrCorrectOriginal As int	OnWrite(int, DialogResult)
        DialogResult intCorrCorrectOriginal;

        // vbPorter upgrade warning: intCorrSpecialtyPlate As int	OnWrite(int, DialogResult)
        DialogResult intCorrSpecialtyPlate;

        int intIrrevocableTrust;

        // vbPorter upgrade warning: intReturningPlate As int	OnWrite(int, DialogResult)
        DialogResult intReturningPlate;
        bool blnLoadingForm;
        bool blnChargeForPlate;
        bool boolNeedToUnload;
        private bool boolIsPreReg;
        private int intPartyType1 = 0;
        private int intPartyType2 = 0;
        private int intPartyType3 = 0;
        public string strReg1FirstName = "";
        public string strReg1LastName = "";
        public string strReg1MI = "";
        public string strReg1Designation = "";
        public string strOwner1 = "";
		private string strReg2FirstName = "";
		private string strReg2LastName = "";
		private string strReg2MI = "";
		private string strReg2Designation = "";
		private string strOwner2 = "";
		private string strReg3FirstName = "";
		private string strReg3LastName = "";
		private string strReg3MI = "";
		private string strReg3Designation = "";
		private string strOwner3 = "";
		private bool alreadySaving = false;

        private IStyleCodeService styleCodeService;

        public bool IsPreReg
        {
            set { boolIsPreReg = value; }
            get
            {
                bool IsPreReg = false;
                IsPreReg = boolIsPreReg;
                return IsPreReg;
            }
        }

        public void Init(bool boolPreReg)
        {
            IsPreReg = boolPreReg;
            boolNeedToUnload = false;
            Show(App.MainForm);
        }

        private void cboBattle_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(cboBattle.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 250, 0);
        }

        private void chk2Year_CheckedChanged(object sender, System.EventArgs e)
        {
            DateTime tempdate;
            if (bol2yCh == true)
            {
                blnCalculateRequired = true;
                if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                {
                    MotorVehicle.Statics.TwoYear = true;
                    tempdate = fecherFoundation.DateAndTime.DateAdd("yyyy", 1, FCConvert.ToDateTime(txtExpires.Text));
                    txtExpires.Text =
                        Strings.Format(
                            FindLastDay(FCConvert.ToString(tempdate.Month) + "/01/" +
                                        FCConvert.ToString(tempdate.Year)), "MM/dd/yyyy");
                    StickerAmount("Higher");
                    // increases the expire year and sticker year
                }
                else
                {
                    MotorVehicle.Statics.TwoYear = false;
                    tempdate = fecherFoundation.DateAndTime.DateAdd("yyyy", -1, FCConvert.ToDateTime(txtExpires.Text));
                    txtExpires.Text =
                        Strings.Format(
                            FindLastDay(FCConvert.ToString(tempdate.Month) + "/01/" +
                                        FCConvert.ToString(tempdate.Year)), "MM/dd/yyyy");
                    StickerAmount("Lower");
                    // decreases the expire year and sticker year
                }

                lblCalculate_Click();
                // Recalculate
            }
        }

        private void chk2Year_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
        {
            MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
            int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
            float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
            float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
            bol2yCh = true;
        }

        private void chk2Year_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
        {
            MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
            int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
            float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
            float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
            bol2yCh = false;
        }

        private void chkAgentFeeExempt_CheckedChanged(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (chkAgentFeeExempt.CheckState == Wisej.Web.CheckState.Checked)
                {
                    txtAgentFee.Text = "0.00";
                }
                else
                {
                    txtAgentFee.Text =
                        Strings.Format(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"), "##0.00");
                }
            }
        }

        private void chkAgentFeeExempt_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkAmputeeVet_CheckedChanged(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
        }

        private void chkAmputeeVet_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        // Private Sub chkBaseIsSalePrice_Click()
        // blnCalculateRequired = True
        // End Sub
        private void chkBaseIsSalePrice_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkCC_CheckedChanged(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: TitleAnswer As int	OnWrite(DialogResult)
            DialogResult TitleAnswer = 0;
            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
            {
                if (chkCC.CheckState == Wisej.Web.CheckState.Checked)
                {
                    TitleAnswer = MessageBox.Show("Is this an off road motorcycle?", "Off Road Motorcycle",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (TitleAnswer == DialogResult.Yes)
                    {
                        TitleRequired = false;
                    }
                    else
                    {
                        TitleRequired = true;
                    }
                }
            }
        }

        private void chkEAP_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkEAP.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
                {
                    chkEAP.CheckState = Wisej.Web.CheckState.Unchecked;
                    ShowWarningBox("You must uncheck Excise Tax Only before you may choose this option.",
                        "Invalid Selection");
                    return;
                }
            }

            if (MotorVehicle.Statics.PreviousETO && MotorVehicle.Statics.ExciseAP)
            {
                chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                return;
            }

            CheckEAP();
            if (MotorVehicle.Statics.ExciseAP == false)
            {
                chkEAP.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkETO_CheckedChanged(object sender, System.EventArgs e)
        {
            clsDRWrapper rsDelete = new clsDRWrapper();
            clsDRWrapper rsDeleteDouble = new clsDRWrapper();
            var chkETO_Checked = chkETO.CheckState == Wisej.Web.CheckState.Checked;
            var chkETO_Unchecked = chkETO.CheckState == Wisej.Web.CheckState.Unchecked;

            try
            {
                MotorVehicle.Statics.ETO = chkETO_Checked;

                // kk11082016 tromv-1231  Need to prevent a race condition when loading and setting this checkbox
                if (blnLoadingForm)
                {
                    return;
                }

                var isNRR = MotorVehicle.Statics.RegistrationType == "NRR";
                var isNRT = MotorVehicle.Statics.RegistrationType == "NRT";
                var isRRT = MotorVehicle.Statics.RegistrationType == "RRT";
                var isRRR = MotorVehicle.Statics.RegistrationType == "RRR";


                if (chkETO_Unchecked)
                {
                    // kk11202015 changed from P5
                    if (MotorVehicle.Statics.Class == "AP" || MotorVehicle.Statics.Class == "TX" ||
                        MotorVehicle.Statics.Class == "CS" || MotorVehicle.Statics.Class == "ST" ||
                        (MotorVehicle.Statics.Class == "AM" && MotorVehicle.Statics.Subclass == "A3") ||
                        (MotorVehicle.Statics.Class == "PC" && MotorVehicle.Statics.Subclass == "P6") ||
                        (MotorVehicle.Statics.Class == "BU" && MotorVehicle.Statics.Subclass == "B2"))
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                        ShowWarningBox("You may only do an Excise Tax Only Registration on this class of vehicle.",
                            "Invalid Option");
                        return;
                    }

                    var strPlate = MotorVehicle.Statics.rsFinal.Get_Fields_String("plate");

                    if (strPlate == "NEW" && MotorVehicle.Statics.TownLevel != 9)
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                        ShowWarningBox(
                            "You may only do an Excise Tax Only Registration on a vehicle with a plate of NEW.",
                            "Invalid Option");
                        return;
                    }

                    if (fecherFoundation.Strings.UCase(
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))) ==
                        "REGISTRATION SUSPENDED")
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                        ShowWarningBox(
                            "You may only do an Excise Tax Only Registration on this vehicle until the registrant has contacted BMV.",
                            "Invalid Option");
                        return;
                    }

                    if (MotorVehicle.Statics.blnSuspended)
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                        ShowWarningBox(
                            "You may only do an Excise Tax Only Registration because their priviledge to Register has been suspended.",
                            "Registration Priviledge Suspended");
                        return;
                    }

                    if (MotorVehicle.Statics.blnSR22)
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                        ShowWarningBox(
                            "You may only do an Excise Tax Only Registration because they must file an SR-22 certificate of insurance with BMV.",
                            "SR-22 Must Be Filed");
                        return;
                    }

                    if (MotorVehicle.Statics.Class == "PC" &&
                        Information.IsNumeric(strPlate) &&
                        Conversion.Val(strPlate) < 100)
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                        ShowWarningBox(
                            "You may only do an Excise Tax Only Registration on PC Vehicles with a plate number from 1 to 99.",
                            "Class AP");
                        return;
                    }

                    if (MotorVehicle.Statics.TownLevel == 6)
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                        ShowWarningBox(
                            "Due to your BMV Authorization Level you may only process this type of Registration as Excise Tax Only.",
                            "Invalid Authorization Level");
                        return;
                    }
                    else if (MotorVehicle.Statics.TownLevel == 1)
                    {
                        if (isNRR)
                        {
                            chkETO.CheckState = Wisej.Web.CheckState.Checked;
                            ShowWarningBox(
                                "Due to your BMV Authorization Level you may only process this type of Registration as Excise Tax Only.",
                                "Invalid Authorization Level");
                            return;
                        }
                    }
                    else
                    {
                        var isInClassSet1 =
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CO" ||
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TT" ||
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CC" ||
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" ||
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AC" ||
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF" ||
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "LC";

                        var isHeavy =
                            Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("registeredWeightNew")) >
                            26000;

                        if (MotorVehicle.Statics.TownLevel == 5 && isNRR &&
                            (MotorVehicle.Statics.PlateType != 2 || (isHeavy && isInClassSet1)))
                        {
                            chkETO.CheckState = Wisej.Web.CheckState.Checked;
                            ShowWarningBox(
                                "Due to your BMV Authorization Level you may only process this type of Registration as Excise Tax Only.",
                                "Invalid Authorization Level");
                            return;
                        }
                        else if (MotorVehicle.Statics.TownLevel == 2 && (isNRR || isNRT && isInClassSet1) && isHeavy)
                        {
                            chkETO.CheckState = Wisej.Web.CheckState.Checked;
                            ShowWarningBox(
                                "Due to your BMV Authorization Level you may only process this type of Registration as Excise Tax Only.",
                                "Invalid Authorization Level");
                            return;
                        }
                    }
                }

                if (chkETO_Checked)
                {
                    var chkEAP_Checked = chkEAP.CheckState == Wisej.Web.CheckState.Checked;

                    if (chkEAP_Checked)
                    {
                        chkETO.CheckState = Wisej.Web.CheckState.Unchecked;
                        ShowWarningBox("You must uncheck Excise Already Paid before you may choose this option.",
                            "Invalid Selection");
                        return;
                    }

                    if (isNRR && MotorVehicle.Statics.PlateType != 2)
                    {
                        MotorVehicle.Statics.RememberedPlate = txtRegistrationNumber.Text;
                        txtRegistrationNumber.Text = "NEW";
                    }

                    MotorVehicle.Statics.RememberedAgentFee = txtAgentFee.Text;
                    MotorVehicle.Statics.RememberedStateCredit = txtStateCredit.Text;
                    MotorVehicle.Statics.RememberedTwoYear = chk2Year.CheckState == Wisej.Web.CheckState.Checked;

                    if (isNRR && !MotorVehicle.Statics.NewFleet)
                        ClearDates();
                    DisableBottomOfScreen(true);
                    txtMonthCharge.Text = "";
                    txtMonthNoCharge.Text = "";
                    txtYearCharge.Text = "";
                    txtYearNoCharge.Text = "";
                    txtMStickerNumber.Text = "";
                    txtYStickerNumber.Text = "";
                    lblMSD.Text = "";
                    lblYSD.Text = "";
                    txtMStickerNumber.Enabled = false;
                    txtYStickerNumber.Enabled = false;
                    cmdCTA.Enabled = false;
                    cmdUseTax.Enabled = false;
                    txtTitle.Enabled = false;
                    txtSalesTax.Enabled = false;
                    chkSalesTaxExempt.Enabled = false;
                    chkSalesTaxExempt.CheckState = Wisej.Web.CheckState.Unchecked;
                    chkDealerSalesTax.Enabled = false;
                    chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
                    MotorVehicle.Statics.rsFinal.Set_Fields("DealerSalesTax", false);
                    MotorVehicle.Statics.rsFinal.Set_Fields("SalesTaxExempt", false);
                    chkNoFeeCTA.Enabled = false;
                    chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Unchecked;
                    chkCTAPaid.Enabled = false;
                    chkCTAPaid.CheckState = Wisej.Web.CheckState.Unchecked;
                    MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeCTA", false);
                    MotorVehicle.Statics.rsFinal.Set_Fields("TitleDone", false);
                    MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);

                    if (MotorVehicle.Statics.lngCTAAddNewID != 0)
                    {
                        rsDelete.OpenRecordset("SELECT * FROM Title WHERE ID = " +
                                               FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
                        if (rsDelete.EndOfFile() != true && rsDelete.BeginningOfFile() != true)
                        {
                            if (Conversion.Val(rsDelete.Get_Fields_String("DoubleNumber")) != 0)
                            {
                                rsDeleteDouble.Execute(
                                    "DELETE FROM DoubleCTA WHERE ID = " +
                                    FCConvert.ToString(Conversion.Val(rsDelete.Get_Fields_String("DoubleNumber"))),
                                    "TWMV0000.vb1");
                            }

                            rsDelete.Delete();
                            rsDelete.Update();
                        }

                        MotorVehicle.Statics.lngCTAAddNewID = 0;
                    }

                    MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", 0);
                    txtPreviousTitle.Text = "";
                    txtPriorTitleState.Text = "";
                    txtCTANumber.Text = "";
                    txtDoubleCTANumber.Text = "";
                    txtDoubleCTANumber.Enabled = false;
                    txtTitle.Text = "0.00";
                    if (MotorVehicle.Statics.lngUTCAddNewID != 0)
                    {
                        rsDelete.Execute(
                            "DELETE FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID),
                            "TWMV0000.vb1");
                        MotorVehicle.Statics.lngUTCAddNewID = 0;
                    }

                    MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
                    txtSalesTax.Text = "0.00";
                }
                else
                {
                    cmdCTA.Enabled = true;
                    cmdUseTax.Enabled = true;

                    if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "MAINE TRAILER"
                        && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) !=
                        "ACE REGISTRATION SERVICES LLC"
                        && fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "COUNTRYWIDE TRAILER"
                        && MotorVehicle.Statics.TownLevel != 2
                        && MotorVehicle.Statics.TownLevel != 3
                        && MotorVehicle.Statics.TownLevel != 4)
                    {
                        cmdCTA.Enabled = false;
                        cmdUseTax.Enabled = false;
                    }

                    chkSalesTaxExempt.Enabled = true;
                    chkDealerSalesTax.Enabled = true;
                    chkNoFeeCTA.Enabled = true;
                    chkCTAPaid.Enabled = true;

                    txtTitle.Enabled = MotorVehicle.Statics.TownLevel != 5;
                    txtSalesTax.Enabled = MotorVehicle.Statics.TownLevel != 5;

                    if (txtRegistrationNumber.Text == "NEW" && MotorVehicle.Statics.RememberedPlate != "")
                    {
                        txtRegistrationNumber.Text = MotorVehicle.Statics.RememberedPlate;
                    }

                    if (MotorVehicle.Statics.RememberedAgentFee != "")
                    {
                        // TROMV-1152 6.12.17  Fix type mis-match when RememberedAgentFee = ""
                        txtAgentFee.Text = MotorVehicle.Statics.RememberedAgentFee;
                    }
                    else
                    {
                        txtAgentFee.Text = "0.00";
                    }

                    txtStateCredit.Text = MotorVehicle.Statics.RememberedStateCredit;
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = MotorVehicle.Statics.RememberedStateCredit;

                    if (fecherFoundation.Strings.Trim(txtExpires.Text) == "" || txtExpires.IsEmpty)
                    {
                        txtExpires.Text = MotorVehicle.Statics.RememberedDate1;
                    }

                    if (fecherFoundation.Strings.Trim(txtEffectiveDate.Text) == "" || txtEffectiveDate.IsEmpty)
                    {
                        txtEffectiveDate.Text = MotorVehicle.Statics.RememberedDate2;
                    }


                    txtMStickerNumber.Enabled = false;
                    txtYStickerNumber.Enabled = false;
                    EnableBottomOfScreen();
                    BottomOfScreen();
                    chk2Year.CheckState = MotorVehicle.Statics.RememberedTwoYear
                        ? Wisej.Web.CheckState.Checked
                        : Wisej.Web.CheckState.Unchecked;

                    if (MotorVehicle.Statics.Class == "TC")
                    {
                        /*? On Error GoTo 0 */
                        return;
                    }

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "CI")
                    {
                        var isClassSet2 = MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                                          MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                                          MotorVehicle.Statics.Class == "MP" || MotorVehicle.Statics.Class == "MQ" ||
                                          MotorVehicle.Statics.Class == "MX" || MotorVehicle.Statics.Class == "SE" ||
                                          MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "XV" ||
                                          MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "TT";

                        var isIU = MotorVehicle.Statics.Class == "IU";

                        if (isRRR)
                        {
                            txtMStickerNumber.Enabled = true;
                            txtYStickerNumber.Enabled = true;
                            var isNotExpirationMonth = FCConvert.ToDateTime(txtExpires.Text).Month !=
                                                       MotorVehicle.Statics.rsFinalCompare
                                                           .Get_Fields_DateTime("ExpireDate").Month;

                            if (isClassSet2)
                            {
                                // ((Class = "CO" Or Class = "TT" Or Class = "CC") And (Val(rsFinal.Fields("plate")) >= 800000 And Val(rsFinal.Fields("plate")) <= 899999))
                                var isNotMotorcyleOrNotFixedMonth =
                                    !MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) ||
                                    !MotorVehicle.Statics.FixedMonth;

                                if (isNotExpirationMonth)
                                {
                                    txtMonthNoCharge.Text = isNotMotorcyleOrNotFixedMonth ? "1" : "";
                                }

                                txtYearNoCharge.Text = "1";
                                if (isNotMotorcyleOrNotFixedMonth && MotorVehicle.Statics.PlateType != 1)
                                {
                                    txtMonthNoCharge.Text = "1";
                                }
                            }
                            else if (isIU)
                            {
                                txtMonthNoCharge.Text = isNotExpirationMonth ? "2" : "0";

                                txtYearNoCharge.Text = "0";
                            }
                            else
                            {
                                if (!Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("ExpireDate")))
                                {
                                    txtMonthNoCharge.Text = "2";
                                }
                                else if (isNotExpirationMonth)
                                {
                                    txtMonthNoCharge.Text = "2";
                                }

                                txtYearNoCharge.Text = "2";

                                if (MotorVehicle.Statics.PlateType != 1)
                                {
                                    txtMonthNoCharge.Text = "2";
                                }
                            }

                            if (MotorVehicle.Statics.PlateType == 1 && Conversion.Val(txtMonthNoCharge.Text) == 0)
                                txtMStickerNumber.Enabled = false;
                            lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                            lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                            // 
                        }
                        else
                        {
                            var isClassSet1 = MotorVehicle.Statics.Class == "AU" ||
                                              MotorVehicle.Statics.Class == "PM" ||
                                              MotorVehicle.Statics.Class == "CL" ||
                                              MotorVehicle.Statics.Class == "MC" ||
                                              MotorVehicle.Statics.Class == "VM" ||
                                              MotorVehicle.Statics.Class == "MP" ||
                                              MotorVehicle.Statics.Class == "MQ" ||
                                              MotorVehicle.Statics.Class == "MX" ||
                                              MotorVehicle.Statics.Class == "SE" ||
                                              MotorVehicle.Statics.Class == "TL" ||
                                              MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT";


                            if (isRRT)
                            {
                                if (MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4)
                                {
                                    if (isClassSet2)
                                    {
                                        txtYearNoCharge.Text = "1";
                                        txtMonthNoCharge.Text = "1";
                                    }
                                    else if (isIU)
                                    {
                                        txtYearNoCharge.Text = "0";
                                        txtMonthNoCharge.Text = "0";
                                    }
                                    else
                                    {
                                        txtYearNoCharge.Text = "2";
                                        txtMonthNoCharge.Text = "2";
                                    }
                                }
                                else if (MotorVehicle.Statics.PlateType == 2 &&
                                         MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() !=
                                         MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate")
                                             .ToOADate())
                                {
                                    var expirationMonth_Final = MotorVehicle.Statics.rsFinal
                                        .Get_Fields_DateTime("ExpireDate").Month;
                                    var expirationMonth_PlateForReg = MotorVehicle.Statics.rsPlateForReg
                                        .Get_Fields_DateTime("ExpireDate").Month;
                                    var expirationYear_Final = MotorVehicle.Statics.rsFinal
                                        .Get_Fields_DateTime("ExpireDate").Year;
                                    var expirationYear_PlateForReg = MotorVehicle.Statics.rsPlateForReg
                                        .Get_Fields_DateTime("ExpireDate").Year;

                                    if (expirationMonth_Final != expirationMonth_PlateForReg &&
                                        expirationYear_Final != expirationYear_PlateForReg)
                                    {
                                        if (isClassSet1)
                                        {
                                            txtYearNoCharge.Text = "1";
                                            txtMonthNoCharge.Text = "1";
                                        }
                                        else if (isIU)
                                        {
                                            txtYearNoCharge.Text = "0";
                                            txtMonthNoCharge.Text = "0";
                                        }
                                        else
                                        {
                                            txtYearNoCharge.Text = "2";
                                            txtMonthNoCharge.Text = "2";
                                        }
                                    }
                                    else if (expirationMonth_Final != expirationMonth_PlateForReg)
                                    {
                                        txtYearNoCharge.Text = "0";

                                        if (isClassSet1)
                                        {
                                            txtMonthNoCharge.Text = "1";
                                        }
                                        else if (isIU)
                                        {
                                            txtMonthNoCharge.Text = "0";
                                        }
                                        else
                                        {
                                            txtMonthNoCharge.Text = "2";
                                        }
                                    }
                                    else
                                    {
                                        txtMonthNoCharge.Text = "0";

                                        if (isClassSet1)
                                        {
                                            txtYearNoCharge.Text = "1";
                                        }
                                        else if (isIU)
                                        {
                                            txtYearNoCharge.Text = "0";
                                        }
                                        else
                                        {
                                            txtYearNoCharge.Text = "2";
                                        }
                                    }
                                }
                                else
                                {
                                    txtYearNoCharge.Text = "0";
                                    txtMonthNoCharge.Text = "0";
                                }

                                if (blnChargeForPlate)
                                {
                                    txtYearCharge.Text = txtYearNoCharge.Text;
                                    txtMonthCharge.Text = txtMonthNoCharge.Text;
                                    txtMonthNoCharge.Text = "";
                                    txtYearNoCharge.Text = "";
                                }

                                lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                                lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                                txtYStickerNumber.Enabled = true;
                                txtMStickerNumber.Enabled = true;
                                if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) &&
                                    MotorVehicle.Statics.FixedMonth)
                                {
                                    txtMonthCharge.Text = "";
                                    txtMonthNoCharge.Text = "";
                                }
                            }
                            else if (isNRT)
                            {
                                if ((MotorVehicle.Statics.PlateType == 2 || MotorVehicle.Statics.PlateType == 3 ||
                                     MotorVehicle.Statics.PlateType == 4) && !MotorVehicle.Statics.LostPlate)
                                {
                                    if (isClassSet1)
                                    {
                                        txtYearNoCharge.Text = "1";
                                        txtMonthNoCharge.Text = "1";
                                    }
                                    else if (isIU)
                                    {
                                        txtYearNoCharge.Text = "0";
                                        txtMonthNoCharge.Text = "2";
                                    }
                                    else
                                    {
                                        txtYearNoCharge.Text = "2";
                                        txtMonthNoCharge.Text = "2";
                                    }
                                }
                                else
                                {
                                    txtYearNoCharge.Text = "0";
                                    txtMonthNoCharge.Text = "0";
                                }

                                if (blnChargeForPlate)
                                {
                                    txtYearCharge.Text = txtYearNoCharge.Text;
                                    txtMonthCharge.Text = txtMonthNoCharge.Text;
                                    txtMonthNoCharge.Text = "";
                                    txtYearNoCharge.Text = "";
                                }

                                lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                                lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                                txtYStickerNumber.Enabled = true;
                                txtMStickerNumber.Enabled = true;
                                if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) &&
                                    MotorVehicle.Statics.FixedMonth)
                                {
                                    txtMonthCharge.Text = "";
                                    txtMonthNoCharge.Text = "";
                                }
                            }
                            else if (isNRR)
                            {
                                if (MotorVehicle.Statics.TownLevel == 9)
                                {
                                    txtYearNoCharge.Text = "0";
                                    txtMonthNoCharge.Text = "0";
                                }
                                else
                                {
                                    if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                                        MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                                        MotorVehicle.Statics.Class == "MP" || MotorVehicle.Statics.Class == "VM" ||
                                        MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                                        MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                                        MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                                    {
                                        txtYearNoCharge.Text = "1";
                                        txtMonthNoCharge.Text = "1";
                                    }
                                    else if (isIU)
                                    {
                                        txtYearNoCharge.Text = "0";
                                        txtMonthNoCharge.Text = "2";
                                    }
                                    else
                                    {
                                        txtYearNoCharge.Text = "2";
                                        txtMonthNoCharge.Text = "2";
                                    }
                                }

                                lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                                lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                                txtYStickerNumber.Enabled = true;
                                txtMStickerNumber.Enabled = true;
                                if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) &&
                                    MotorVehicle.Statics.FixedMonth)
                                {
                                    txtMonthCharge.Text = "";
                                    txtMonthNoCharge.Text = "";
                                }
                            }
                            else if (MotorVehicle.Statics.RegistrationType == "GVW")
                            {
                                txtYearNoCharge.ReadOnly = true;
                                txtMonthNoCharge.ReadOnly = true;
                                txtYearCharge.ReadOnly = true;
                                txtMonthCharge.ReadOnly = true;
                                lblNumberofYear.Text = "";
                                lblNumberofMonth.Text = "";
                                txtYStickerNumber.ReadOnly = true;
                                txtMStickerNumber.ReadOnly = true;
                            }
                        }
                    }
                    else
                    {
                        txtYearNoCharge.Enabled = false;
                        txtMonthNoCharge.Enabled = false;
                        txtYearCharge.Enabled = false;
                        txtMonthCharge.Enabled = false;
                        lblNumberofYear.Text = "";
                        lblNumberofMonth.Text = "";
                        txtYStickerNumber.ReadOnly = true;
                        txtMStickerNumber.ReadOnly = true;

                        var oldColor = ColorTranslator.FromOle(OldColor);
                        txtYearNoCharge.BackColor = oldColor;
                        txtMonthNoCharge.BackColor = oldColor;
                        txtYearCharge.BackColor = oldColor;
                        txtMonthCharge.BackColor = oldColor;
                    }
                }

                if (MotorVehicle.Statics.RegistrationType != "CORR")
                {
                    lblCalculate_Click();
                }
            }
            finally
            {
                rsDelete.Dispose();
                rsDeleteDouble.Dispose();
            }
        }

        private static void ShowWarningBox(string msg, string title)
        {
            MessageBox.Show(msg, title,MessageBoxButtons.OK,MessageBoxIcon.Warning);
        }

        private void chkExciseExempt_CheckedChanged(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
            if (chkExciseExempt.CheckState == Wisej.Web.CheckState.Checked)
            {
                // txtReg1LR.Text = "R" Or
                if ((txtReg2LR.Text == "R" && txtReg2ICM.Text != "N") ||
                    (txtReg3LR.Text == "R" && txtReg3ICM.Text != "N"))
                {
                    MessageBox.Show("Please be sure to verify that the Lessor is Excise Exempt.",
                        "Lessor Excise Exempt?", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    txtExciseTax.Text = "0.00";
                    CalculateLocalECorrectFees();
                }
                else
                {
                    Recalculate();
                }
            }
            else
            {
                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    txtExciseTax.Text =
                        Strings.Format(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged"),
                            "#,##0.00");
                    CalculateLocalECorrectFees();
                }
                else
                {
                    Recalculate();
                }
            }
        }

        private void chkExciseExempt_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        // kk07222016  Added Sales Tax Exempt (NO FEE)
        private void chkSalesTaxExempt_CheckedChanged(object sender, System.EventArgs e)
        {
            // Dealer Sales Tax and Exempt are mutually exclusive, but both can be unchecked
            if (chkSalesTaxExempt.CheckState == Wisej.Web.CheckState.Checked)
            {
                chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
                txtSalesTax.Text = "0.00";
                blnCalculateRequired = true;
            }
        }

        private void chkSalesTaxExempt_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        // kk05122016  Add Dealer Sales Tax
        private void chkDealerSalesTax_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkDealerSalesTax.Checked)
            {
	            if (!FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone")))
	            {
		            chkSalesTaxExempt.Checked = false;
		            txtSalesTax.Text = "0.00";
		            blnCalculateRequired = true;
		            cmdUseTax.Enabled = false;
		            txtSalesTax.Enabled = false;
	            }
	            else
                {
	                DialogResult blnUserCancel =
		                MessageBox.Show(
			                "Do you wish to delete the Use Tax information associated with this registration?",
			                "Delete Use Tax Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
	                if (blnUserCancel == DialogResult.Yes)
	                {
		                MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
		                MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);

		                if (MotorVehicle.Statics.lngUTCAddNewID != 0)
		                {
                            using (clsDRWrapper rsDelete = new clsDRWrapper())
                            {
                                rsDelete.Execute(
                                    "DELETE FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID),
                                    "TWMV0000.vb1");
                                MotorVehicle.Statics.lngUTCAddNewID = 0;
                            }
		                }
		                chkSalesTaxExempt.Checked = false;
		                txtSalesTax.Text = "0.00";
		                blnCalculateRequired = true;
		                cmdUseTax.Enabled = false;
		                txtSalesTax.Enabled = false;
	                }
	                else
	                {
		                chkDealerSalesTax.Checked = false;
		                cmdUseTax.Enabled = true;
		                txtSalesTax.Enabled = true;
	                }
				}
            }
			else
            {
	            cmdUseTax.Enabled = true;
	            txtSalesTax.Enabled = true;
			}
        }

        private void chkDealerSalesTax_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkHalfRateLocal_CheckedChanged(object sender, System.EventArgs e)
        {
            if ((MotorVehicle.Statics.NewFleet && fleetGroupType != FleetGroupType.Group) ||
                MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                    MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                    MotorVehicle.Statics.RegistrationType))
            {
                chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Unchecked;
                return;
            }

            lblCalculate_Click();
        }

        private void chkHalfRateState_CheckedChanged(object sender, System.EventArgs e)
        {
            if ((MotorVehicle.Statics.NewFleet && fleetGroupType != FleetGroupType.Group) ||
					(!HalfRateAllowed(MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass)) ||
					MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                    MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                    MotorVehicle.Statics.RegistrationType))
            {
                chkHalfRateState.CheckState = Wisej.Web.CheckState.Unchecked;
                return;
            }

            lblCalculate_Click();
        }

        // kk05122016  Add No Fee CTA checkbox
        private void chkNoFeeCTA_CheckedChanged(object sender, System.EventArgs e)
        {
            // kk09282016  Add Title Fee already paid checkbox
            // Title Fee paid and Exempt are mutually exclusive, but both can be unchecked
            if (chkNoFeeCTA.CheckState == Wisej.Web.CheckState.Checked)
            {
                chkCTAPaid.CheckState = Wisej.Web.CheckState.Unchecked;
                txtTitle.Text = "0.00";
                blnCalculateRequired = true;
            }
        }

        private void chkNoFeeCTA_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        // kk09282016  Add Title Fee already paid checkbox
        private void chkCTAPaid_CheckedChanged(object sender, System.EventArgs e)
        {
            // Title Fee paid and Exempt are mutually exclusive, but both can be unchecked
            if (chkCTAPaid.CheckState == Wisej.Web.CheckState.Checked)
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Unchecked;
                txtTitle.Text = "0.00";
                blnCalculateRequired = true;
            }
        }

        private void chkCTAPaid_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkOverrideFleetEmail_CheckedChanged(object sender, System.EventArgs e)
        {
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                if (chkOverrideFleetEmail.CheckState == Wisej.Web.CheckState.Checked)
                {
                    txtEMail.Enabled = true;
                }
                else
                {
                    if (fleetGroupType != FleetGroupType.Group)
                    {
                        rsTemp.OpenRecordset(
                            "SELECT * FROM FleetMaster WHERE ISNull(Deleted, 0) = 0 AND Type <> 'L' AND ExpiryMonth = 99 ORDER BY Owner1Name");
                    }
                    else
                    {
                        rsTemp.OpenRecordset(
                            "SELECT * FROM FleetMaster WHERE ISNULL(Deleted, 0) = 0 AND Type <> 'L' AND FleetNumber = " +
                            FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " AND ExpiryMonth <> 99");
                    }

                    if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                    {
                        txtEMail.Text = FCConvert.ToString(rsTemp.Get_Fields_String("EmailAddress"));
                    }

                    txtEMail.Enabled = false;
                }
            }
        }

        private void chkRental_CheckedChanged(object sender, System.EventArgs e)
        {
            // 11202015 changed from P4, P5
            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PC" &&
                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P5" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P6"))
            {
                chkRental.CheckState = Wisej.Web.CheckState.Checked;
                return;
            }

            blnCalculateRequired = true;
        }

        private void chkRental_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkStateExempt_CheckedChanged(object sender, System.EventArgs e)
        {
            if (!blnLoadingForm)
            {
                blnCalculateRequired = true;
                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    if (chkStateExempt.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text = "0.00";
                        frmFeesInfo.InstancePtr.total();
                        txtRate.Text = Strings.Format(frmFeesInfo.InstancePtr.txtFEERegFee.Text, "##0.00");
                        txtFees.Text = Strings.Format(frmFeesInfo.InstancePtr.txtFEETotal.Text, "##0.00");
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(
                            MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"), "#,##0.00");
                        frmFeesInfo.InstancePtr.total();
                        txtRate.Text = Strings.Format(frmFeesInfo.InstancePtr.txtFEERegFee.Text, "##0.00");
                        txtFees.Text = Strings.Format(frmFeesInfo.InstancePtr.txtFEETotal.Text, "##0.00");
                    }
                }
            }
        }

        private void chkStateExempt_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkTrailerVanity_CheckedChanged(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
        }

        private void chkTrailerVanity_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkTransferExempt_CheckedChanged(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (chkTransferExempt.CheckState == Wisej.Web.CheckState.Checked)
                {
                    txtTransferCharge.Text = "0.00";
                }
                else
                {
                    txtTransferCharge.Text =
                        Strings.Format(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTransferCharge"),
                            "##0.00");
                }
            }
        }

        private void chkTransferExempt_Leave(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void chkVIN_CheckedChanged(object sender, System.EventArgs e)
        {
            VerifyVIN();
        }

       private void cmdDone_Click(object sender, System.EventArgs e)
        {
            if (fecherFoundation.Strings.Trim(txtMessage.Text) != "" && cboMessageCodes.SelectedIndex == -1)
            {
                ShowWarningBox(
                    "You must select a message code to go with your message before you continue to process this registration.",
                    "Invalid Message Code");
                return;
            }
            else if (cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == "")
            {
                ShowWarningBox(
                    "You must input a message to go with your code before you continue processing this registration.",
                    "No Message");
                return;
            }

            fraAdditionalInfo.Visible = false;
        }

        private void cmdErase_Click(object sender, System.EventArgs e)
        {
            cboMessageCodes.SelectedIndex = -1;
            txtMessage.Text = "";
        }

        private void cmdMoreInfo_Click(object sender, System.EventArgs e)
        {
            fraAdditionalInfo.Visible = true;
            cmbYes.Focus();
        }

        private void frmDataInput_Activated(object sender, System.EventArgs e)
        {
            if (boolNeedToUnload)
            {
                Close();
                return;
            }

            string strSql = "";
            bool IsComm;
            string tempClass = "";
            // vbPorter upgrade warning: counter As int	OnWrite(int, DialogResult)
            int counter;
            int LetterCount = 0;
            bool blnTrailerVanity;

                if (FCConvert.ToString(modGNBas.Statics.Response) == "BAD")
                {
                    frmDataInput.InstancePtr.Close();
                    modGNBas.Statics.Response = "";
                    return;
                }

                if (MotorVehicle.Statics.TownLevel == 5)
                {
                    cmdUseTax.Enabled = false;
                    cmdCTA.Enabled = false;
                    txtTitle.Enabled = false;
                    txtSalesTax.Enabled = false;
                }

                blnVerifyFlag = false;
                if (MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "GVW")
                {
                    lblExciseDiff.Visible = true;
                }
                else
                {
                    lblExciseDiff.Visible = false;
                }

                if (frmDataInput.InstancePtr.Text == "Data Input")
                {
                    frmDataInput.InstancePtr.Text = "Data Input  -  " + MotorVehicle.Statics.RegistrationType;
                }

            if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)) && !MotorVehicle.Statics.FromFeesScreen)
                {
                    lblCalculate_Click();
                    return;
                }

                frmWait.InstancePtr.Unload();
                SetUserFields();
                if (MotorVehicle.Statics.RegistrationType != "NRR")
                {
                    txtExpires.Locked = true;
                }
                else
                {
                    txtExpires.Locked = false;
                }

                if (!MotorVehicle.Statics.PreviewFlag && !MotorVehicle.Statics.CTAFlag && !MotorVehicle.Statics.UseTaxFlag)
                {
                    if (fecherFoundation.Strings.Trim(txtFleetNumber.Text) == "")
                    {
                        MotorVehicle.Statics.NewFleet = false;
                    }

                    MotorVehicle.Statics.LocalHalfRate = false;
                    MotorVehicle.Statics.StateHalfRate = false;
                    if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "MAINE TRAILER" &&
                        fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "ACE REGISTRATION SERVICES LLC" &&
                        fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "COUNTRYWIDE TRAILER")
                    {
                        if (MotorVehicle.Statics.TownLevel != 2 && MotorVehicle.Statics.TownLevel != 3 &&
                            MotorVehicle.Statics.TownLevel != 4)
                        {
                            cmdCTA.Enabled = false;
                            cmdUseTax.Enabled = false;
                        }
                    }
                }
                else
                {
                    lblCalculate_Click();
                }

                MotorVehicle.Statics.PreviewFlag = false;
                MotorVehicle.Statics.CTAFlag = false;
                MotorVehicle.Statics.UseTaxFlag = false;
                if (!MotorVehicle.Statics.FromTransfer && FCConvert.ToString(modGNBas.Statics.Response) != "BAD")
                {
                    if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")) ||
                        fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))) ==
                        "REGISTRATION SUSPENDED")
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag") + "") == 1)
                        {
                            Image1.Visible = true;
                        }
                        else if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag") + "") == 2)
                        {
                            MessageBox.Show(
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")),
                                "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag") + "") == 3)
                        {
                            MessageBox.Show(
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")),
                                "Message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            frmDataInput.InstancePtr.Close();
                            return;
                        }
                    }

                    if (!FromAP)
                    {
                        DOTNumberRequired = false;
                        TitleRequired = false;
                        Set_Tab_Index();

                        if (MotorVehicle.Statics.FromFeesScreen == true)
                        {
                            txtStateCredit.Text =
                                Strings.Format(
                                    FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) +
                                    FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditComm.Text), "#,##0.00");
                            txtRate.Text = frmFeesInfo.InstancePtr.txtFEERegFee.Text;
                            txtFees.Text = frmFeesInfo.InstancePtr.txtFEETotal.Text;
                            MotorVehicle.Statics.FromFeesScreen = false;
                            return;
                        }

                        FCGlobal.Screen.MousePointer = 0;
                        MotorVehicle.Statics.DataFormLoaded = true;
                        if (MotorVehicle.Statics.ETO)
                        {
                            Support.SendKeys("{F6}", false, this);
                            return;
                        }

                        if (MotorVehicle.Statics.RegistrationType == "LOST")
                        {
                            return;
                        }

                        if (MotorVehicle.Statics.RegistrationType != "GVW" &&
                            MotorVehicle.Statics.RegistrationType != "CORR")
                        {
                            if (CalculateEffectiveDate() == false)
                            {
                                if (txtExpires.Text == "X")
                                {
                                    Close();
                                    return;
                                }
                            }

                            if (VerifyExpireDate(txtExpires.Text) == "T")
                            {
                                // do nothing
                            }

                            if (chkHalfRateLocal.CheckState != Wisej.Web.CheckState.Checked &&
                                chkHalfRateState.CheckState != Wisej.Web.CheckState.Checked)
                            {
                                blnCalculateRequired = false;
                            }

                            SetYearNumbers();
                            SetMonthNumbers();
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "GVW")
                        {
                            UnEnableAllButWeight();
                        }
                        else
                        {
                            if (MotorVehicle.Statics.PlateType == 2)
                            {
                                var original2YearChecked =  chk2Year.Checked;
                                
                                if (CalculateEffectiveDate() == false)
                                {
                                    if (txtExpires.Text == "X")
                                    {
                                        Close();
                                        return;
                                    }
                                }
                                if (MotorVehicle.Statics.RegistrationType == "CORR")
                                {
                                    if (!chk2Year.Checked && original2YearChecked)
                                    {
                                        chk2Year.Checked = true;
                                    }
                                }
                            }
                        }

                        // end of not gvw
                        if (txtMileage.BackColor == Color.Black)
                            txtMileage.BackColor = SystemColors.Control;
                        if (txtExpires.BackColor == Color.Black)
                            txtExpires.BackColor = SystemColors.Control;
                        txtReg1DOB.BorderStyle = (BorderStyle)1;
                        txtReg2DOB.BorderStyle = (BorderStyle)1;
                        //App.DoEvents();
                        if (MotorVehicle.Statics.RegistrationType == "GVW")
                        {
                            txtBase.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"));
                            MotorVehicle.Statics.MillYear =
                                FCConvert.ToInt16(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear"));
                            txtMilYear.Text = FCConvert.ToString(MotorVehicle.Statics.MillYear);
                            txtMilYear_LostFocus();
                            txtStateCredit.Text =
                                Strings.Format(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"),
                                    "#,##0.00");
                            frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                                MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"), "#,##0.00");
                        }

                        Support.SendKeys("{F6}", false, this);
                        if (MotorVehicle.Statics.Class != "TC")
                        {
                            if (MotorVehicle.Statics.Class == "IU" && MotorVehicle.Statics.NewPlate)
                            {
                                if (!CheckIslandUse())
                                {
                                    ShowWarningBox(
                                        "There are no " +
                                        FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtExpires.Text).Year) +
                                        " Year Stickers in Inventory.", "Year Sticker Not in Inventory");
                                    frmDataInput.InstancePtr.Close();
                                    return;
                                }
                            }

                            if (txtMileage.Enabled == true && txtMileage.Text == "")
                            {
                                if (MotorVehicle.Statics.RegistrationType == "GVW")
                                {
                                    if (!fecherFoundation.FCUtils.IsNull(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer")))
                                    {
                                        txtMileage.Text =
                                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer"));
                                    }
                                    else
                                    {
                                        txtMileage.Text = "0";
                                    }

                                    txtMileage.Enabled = false;
                                    txtRegWeight.Focus();
                                }
                                else
                                {
                                    txtMileage.Focus();
                                }
                            }

                        }
                    }
                    else
                    {
                        FromAP = false;
                        if (txtresidenceCode.Text == "")
                        {
                            using (clsDRWrapper defRS = new clsDRWrapper())
                            {
                                defRS.OpenRecordset("SELECT * FROM DefaultInfo");
                                // Set DefaultInfo
                                if (defRS.EndOfFile() != true && defRS.BeginningOfFile() != true)
                                {
                                    defRS.MoveLast();
                                    defRS.MoveFirst();
                                    txtresidenceCode.Text = FCConvert.ToString(defRS.Get_Fields_String("ResidenceCode"));
                                }
                            }
                        }

                        if (MotorVehicle.Statics.ExciseAP)
                        {
                            if (VerifyExpireDate(txtExpires.Text) == "T")
                            {
                                txtMileage.Focus();
                            }
                        }
                    }

                    if (MotorVehicle.Statics.RegistrationType == "RRR")
                    {
                        if (chkEAP.TabStop == true)
                        {
                            chkEAP.TabStop = false;
                        }
                    }

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI")
                    {
                        txtYearNoCharge.ReadOnly = true;
                        txtMonthNoCharge.ReadOnly = true;
                        txtYearCharge.ReadOnly = true;
                        txtMonthCharge.ReadOnly = true;
                    }

                    if (MotorVehicle.Statics.RegistrationType == "CORR")
                    {
                        txtStateCredit.Text =
                            Strings.Format(
                                MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed") +
                                    MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"),
                                "#,##0.00");
                        lblCalculate_Click();
                    }

                    if (MotorVehicle.Statics.HeldPlate)
                    {
                    if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")))
                        {
                            chkETO.CheckState = Wisej.Web.CheckState.Checked;
                            txtMileage.Focus();
                        }
                    }
                }

                if (MotorVehicle.Statics.FromTransfer || FCConvert.ToString(modGNBas.Statics.Response) == "BAD")
                {
                    Unload();
                    return;
                }

                if (!MotorVehicle.Statics.SetFinalCompare)
                {
                    MotorVehicle.Statics.CompareDate = fecherFoundation.Strings.Trim(txtExpires.Text);
                    MotorVehicle.Statics.SetFinalCompare = true;
                }

                if (MotorVehicle.Statics.HeldPlate)
                {
                    if (!MotorVehicle.Statics.ETO)
                    {
                        if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) > 0)
                        {
                            txtMStickerNumber.Focus();
                        }
                        else
                        {
                            txtYStickerNumber.Focus();
                        }
                    }
                }
                else if (MotorVehicle.Statics.RegistrationType == "RRR" &&
                         (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" ||
                          FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CL"))
                {
                    if (!MotorVehicle.Statics.NewToTheSystem)
                    {
                        if (!MotorVehicle.Statics.ETO)
                        {
                            if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) > 0)
                            {
                                txtMStickerNumber.Focus();
                            }
                            else
                            {
                                txtYStickerNumber.Focus();
                            }
                        }
                    }
                    else
                    {
                        if (txtMileage.Enabled == false)
                        {
                            txtVin.Focus();
                            OldColor = ColorTranslator.ToOle(Color.White);
                        }
                        else
                        {
                            txtMileage.Focus();
                        }
                    }
                }
                else if (MotorVehicle.Statics.RegistrationType == "GVW")
                {
                    txtRegWeight.Focus();
                }

                if (txtExpires.Text == "00/00/0000")
                {
                    txtExpires.Text = "";
                }

                if (blnCalculateRequired == true)
                {
                    lblCalculate_Click();
                }

                if ((FCConvert.ToDecimal(txtRate.Text) == 35 ||
                     (FCConvert.ToDecimal(txtRate.Text) == FCConvert.ToDecimal(17.5) &&
                      MotorVehicle.Statics.LocalHalfRate == true)) &&
                    MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") == 25)
                {
	                txtVin.CausesValidation = false;
	                MessageBox.Show("Please be sure to verify the credit amount before proceeding with this registration.",
                        "Check Credit Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
	                txtVin.CausesValidation = true;
                }

                blnDontCheckDate = false;
                //FC:FINAL:SBE - #3956 - force focus on active control
                if (ActiveControl != null && ActiveControl.CanFocus)
                {
                    ActiveControl.Focus();
                }
                FCUtils.ApplicationUpdate(this);

        }

        private void frmDataInput_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F10)
            {
                KeyCode = (Keys)0;
            }
        }

        private void frmDataInput_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            if (KeyAscii == Keys.Return)
            {
                if (MotorVehicle.Statics.RegistrationType != "GVW")
                {
                    KeyAscii = (Keys)0;
                    //App.DoEvents();
                    Support.SendKeys("{TAB}", false);
                    return;
                }
            }
            else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
            {
                if (FCGlobal.Screen.ActiveControl is Global.T2KOverTypeBox ||
                    FCGlobal.Screen.ActiveControl is FCTextBox)
                {
                    KeyAscii = KeyAscii - 32;
                }
            }
            else if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                if (fraAdditionalInfo.Visible == true)
                {
                    fraAdditionalInfo.Visible = false;
                }
                else
                {
                    ans = MessageBox.Show("Are you sure you want to end this registration?", "Quit Registration",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.Yes)
                    {
                        StaticSettings.GlobalCommandDispatcher.Send(new ReleaseGlobalResources());
                        
                        Close();
                        return;
                    }
                }
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmDataInput_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            try
            {
                // On Error GoTo ErrorTag
                fecherFoundation.Information.Err().Clear();
                if (!(FCGlobal.Screen.ActiveControl is FCComboBox) && !(FCGlobal.Screen.ActiveControl is FCCheckBox) &&
                    !(FCGlobal.Screen.ActiveControl is FCRadioButton))
                { if (FCGlobal.Screen.ActiveControl is TextBox)
                    {
                        if (((FCGlobal.Screen.ActiveControl as TextBox).MaxLength ==
                             (FCGlobal.Screen.ActiveControl as TextBox).SelectionStart &&
                             (FCGlobal.Screen.ActiveControl as TextBox).MaxLength != 0) && KeyCode != Keys.Return &&
                            KeyCode != Keys.Down && KeyCode != Keys.Up && KeyCode != Keys.Left
                            //FC:FINAL:SBE - #2162 - do not move to another textbox when TabKey was pressed, because it will skip the current input
                            && KeyCode != Keys.Tab)
                        {
                            KeyCode = (Keys) 0;
                            //App.DoEvents();
                            Support.SendKeys("{TAB}", false);
                        }
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void frmDataInput_Load(object sender, System.EventArgs e)
        {
            if (blueBookRepository == null)
            {
                blueBookRepository = TWSharedLibrary.StaticSettings.GlobalCommandDispatcher.Send(new GetBlueBookRepository()).Result;
            }

            if (styleCodeService == null)
            {
                styleCodeService = TWSharedLibrary.StaticSettings.GlobalCommandDispatcher.Send(new GetStyleCodeService()).Result;
            }

            DialogResult ans2 = 0;
            object answer;
            clsDRWrapper defRS = new clsDRWrapper();
            clsDRWrapper rsResCode = new clsDRWrapper();
            clsDRWrapper rsClassInfo = new clsDRWrapper();
            clsDRWrapper rsTemp = new clsDRWrapper();
            boolNeedToUnload = false;
            blnLoadingForm = true;
            blnChargeForPlate = false;
            alreadySaving = false;
            try
            {

                modGlobalFunctions.SetTRIOColors(this, false);
                if (!MotorVehicle.Statics.AllowRentals)
                {
                    chkRental.Enabled = false;
                }
                else
                {
                    chkRental.Enabled = true;
                }

                ConfigBattleList();
                // kk01042018 tromvs-96  Change from hardcoded list
                if (MotorVehicle.Statics.FromTransfer || FCConvert.ToString(modGNBas.Statics.Response) == "BAD" ||
                    MotorVehicle.Statics.rsFinal.IsntAnything())
                {
                    return;
                }

                modGNBas.GetWindowSize(this);
                MotorVehicle.Statics.StateHalfRate = false;
                MotorVehicle.Statics.LocalHalfRate = false;
                MotorVehicle.Statics.blnCheckedOOTFeeonEAP = false;
                MotorVehicle.Statics.blnChargeOOTFeeOnEAP = false;
                intCorrInitialPlate = 0;
                intCorrSpecialtyPlate = 0;
                intReturningPlate = 0;
                MotorVehicle.Statics.intCorrExtendedSpecialtyPlate = 0;
                MotorVehicle.Statics.intCorrNewPlate = false;
                intIrrevocableTrust = -1;
                intCorrCorrectOriginal = 0;
                MotorVehicle.Statics.datMotorCycleExpires = DateTime.FromOADate(0);
                MotorVehicle.Statics.datMotorCycleEffective = DateTime.FromOADate(0);
                MotorVehicle.Statics.OpenFlag = false;
                blnExpirationDateChanged = false;
                blnDontCheckDate = true;
            MotorVehicle.Statics.blnMakeVehicleInactive = FCConvert.ToBoolean(0);
                OldColor = ColorTranslator.ToOle(txtExpires.BackColor);
                defRS.OpenRecordset("SELECT * FROM DefaultInfo");
                // Set DefaultInfo
                if (defRS.EndOfFile() != true && defRS.BeginningOfFile() != true)
                {
                    defRS.MoveLast();
                    defRS.MoveFirst();
                    txtCity.Text = FCConvert.ToString(defRS.Get_Fields_String("Town"));
                    txtState.Text = FCConvert.ToString(defRS.Get_Fields_String("State"));
                    rsResCode.OpenRecordset("SELECT * FROM Residence WHERE Code = '" +
                                            defRS.Get_Fields_String("ResidenceCode") + "'");
                    if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
                    {
                        txtLegalres.Text =
                            fecherFoundation.Strings.Trim(FCConvert.ToString(rsResCode.Get_Fields_String("Town")));
                        txtResState.Text = FCConvert.ToString(defRS.Get_Fields_String("State"));
                    }
                    else
                    {
                        txtLegalres.Text = "";
                    }

                    txtresidenceCode.Text = FCConvert.ToString(defRS.Get_Fields_String("ResidenceCode"));
                    txtZip.Text = Strings.Mid(FCConvert.ToString(defRS.Get_Fields_String("Zip")), 1, 5);
                }

                txtLegalres.Locked = true;

				txtCountry.Text = "US";
                txtResCountry.Text = "US";
                // 
                MotorVehicle.Statics.SetFinalCompare = false;
                MotorVehicle.Statics.GrandTotal = 0;
                txtYStickerNumber.TextAlign = HorizontalAlignment.Right;
                txtMStickerNumber.TextAlign = HorizontalAlignment.Right;
                txtMStickerNumber.MaxLength = 8;
                txtYStickerNumber.MaxLength = 8;
                if (MotorVehicle.Statics.RegisteringFleet &&
                    MotorVehicle.Statics.PreEffectiveDate.ToOADate() > DateTime.Today.ToOADate() &&
                    Information.IsDate(MotorVehicle.Statics.PreEffectiveDate))
                {
                    txtExciseTaxDate.Text = Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yy");
                }
                else
                {
                    txtExciseTaxDate.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
                }

                FillExtraInfo();
                // kk11202015 changed from P4, P5
                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PC" &&
                    (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P5" ||
                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P6") &&
                    MotorVehicle.Statics.AllowRentals)
                {
                    chkRental.CheckState = Wisej.Web.CheckState.Checked;
                }

                // 
                if (MotorVehicle.Statics.RegistrationType != "CORR" && MotorVehicle.Statics.RegistrationType != "DUPREG")
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
                }

                txtExpires.Text = "";

                switch (MotorVehicle.Statics.RegistrationType)
                {
                    case "RRR":
                        {
                            if (CalculateDates() == false)
                            {
                                return;
                            }

                            // 10/30/02 Dave
                            if (MotorVehicle.Statics.PreviousETO)
                            {
                                // kk11202015 changed from P5
                                if (MotorVehicle.Statics.Class == "AP" ||
                                    (MotorVehicle.Statics.Class == "AM" && MotorVehicle.Statics.Subclass == "A3") ||
                                    (MotorVehicle.Statics.Class == "PC" && MotorVehicle.Statics.Subclass == "P6") ||
                                    (MotorVehicle.Statics.Class == "BU" && MotorVehicle.Statics.Subclass == "B2"))
                                {
                                    MessageBox.Show(
                                        "You must change the status of this vehicle from pending before you may continue with this process",
                                        "Unable to Process");
                                    if (MotorVehicle.Statics.RegisteringFleet)
                                    {
                                        MotorVehicle.Statics.RegisteringFleet = false;
                                    }

                                    boolNeedToUnload = true;
                                    return;
                                }
                                else
                                {
                                    txtBase.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"),
                                                                  "#,##0");
                                    txtVin.Text =
                                        fecherFoundation.Strings.Trim(
                                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")));
                                    txtMake.Text =
                                        fecherFoundation.Strings.Trim(
                                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make")));
                                    txtModel.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model").Trim().Length <= 6 ? MotorVehicle.Statics.rsFinal.Get_Fields_String("model").Trim() : MotorVehicle.Statics.rsFinal.Get_Fields_String("model").Trim().Left(6);
                                    txtYear.Text =
                                        fecherFoundation.Strings.Trim(
                                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year")));
                                    txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                                                                             fecherFoundation.Strings.Trim(
                                                                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")))));
                                    txtMilYear_LostFocus();
                                    blnCalculateRequired = true;
                                }
                            }

                            FillMiscInfo();
                            FillOwnerInfo();
                            FillVehicleInfo();
                            CalculateStateFees();
                            FillExemptFlags();
                            // kk09272016 tromvs-57 follow up  'Call ExciseTaxRequired    'REVERSED the order of this line with the next line  08/11/2000  ajb
                            BottomOfScreen();
                            if (MotorVehicle.GetLowDigit() == true)
                            {
                                if (MessageBox.Show("Does this process involve a name change?", "Name Change?",
                                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                    ConfigExciseTaxOnly();
                                }
                            }

                            CalculateLocalFees();
                            // kk11202015 changed from P5
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "MM" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TX" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "ST" ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AM" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "A3") ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PC" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P6") ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "BU" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "B2"))
                            {
                                chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                ConfigExciseTaxOnly();
                            }

                            break;
                        }

                    case "RRT":
                        {
                            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) <= 0)
                            {
                                ans2 = DialogResult.Yes;
                                MotorVehicle.Statics.blnForcedAnalysis = true;
                            }
                            else
                            {
                                ans2 = MessageBox.Show("Would you like to run the Transfer Analysis Report?", "Transfer Analysis",
                                                       MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                MotorVehicle.Statics.blnForcedAnalysis = false;
                            }

                            if (ans2 == DialogResult.Yes)
                            {
                                modGNBas.Statics.Response = "";
                                frmTransferAnalysis.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                                if (MotorVehicle.Statics.FromTransfer == true)
                                {
                                    Hide();
                                    defRS.Reset();
                                    return;
                                }

                                if (FCConvert.ToString(modGNBas.Statics.Response) == "BAD")
                                {
                                    defRS.Reset();
                                    return;
                                }
                            }
                            else
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3",
                                                                        FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"))));
                            }

                            if (MotorVehicle.GetTransferCredit(this) == false)
                            {
                                ShowWarningBox("Some of the credit information will have to be supplied manually.",
                                                "Credit Information Missing");
                            }
                            else
                            {
                                FillMiscInfo();
                                FillOwnerInfo();
                                CalculateDates();
                                FillVehicleInfo();
                                FillExemptFlags();
                                // kk09272016 tromvs-57 follow up  'Call ExciseTaxRequired
                                BottomOfScreen();
                                if (MotorVehicle.GetLowDigit() == true)
                                {
                                    if (MessageBox.Show("Does this process involve a name change?", "Name Change?",
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                        ConfigExciseTaxOnly();
                                    }
                                }

                                if (MotorVehicle.Statics.PreviousETO)
                                {
                                    chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                                }

                                CalculateLocalFees();
                                CalculateStateFees();
                            }

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TX" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "ST")
                            {
                                chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                ConfigExciseTaxOnly();
                            }

                            break;
                        }

                    case "NRR":
                        {
                            if (CalculateDates() == false)
                            {
                                defRS.Reset();
                                Close();
                                return;
                            }

                            if (MotorVehicle.Statics.Subclass != "TC" && MotorVehicle.Statics.Subclass != "SE" &&
                                MotorVehicle.Statics.Subclass != "L7" &&
                                MotorVehicle.Statics.Subclass != "L4" &&
                                (MotorVehicle.Statics.Subclass != "L2" || MotorVehicle.Statics.Class == "LB") &&
                                MotorVehicle.Statics.Subclass != "C6" && MotorVehicle.Statics.Subclass != "C4" &&
                                (MotorVehicle.Statics.Subclass != "C2" || MotorVehicle.Statics.Class == "CR" ||
                                 MotorVehicle.Statics.Class == "CD" || MotorVehicle.Statics.Class == "CM") &&
                                MotorVehicle.Statics.Subclass != "CI" &&
                                (MotorVehicle.Statics.Subclass != "D1" || MotorVehicle.Statics.Class == "DS" ||
                                 MotorVehicle.Statics.Class == "DX" || MotorVehicle.Statics.Class == "DV") &&
                                MotorVehicle.Statics.ExciseAP != true && !MotorVehicle.Statics.RegisteringFleet)
                            {
                                if (!MotorVehicle.Statics.NoRedbook)
                                {
                                    if (MotorVehicle.GetRedbookValues() == true)
                                    {
                                        txtVin.Focus();
                                        if (chkVIN.CheckState == Wisej.Web.CheckState.Checked)
                                        {
                                            chkVIN.CheckState = Wisej.Web.CheckState.Unchecked;
                                            chkVIN.Visible = false;
                                        }

                                        frmWait.InstancePtr.Show();
                                        frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Building MVR3";
                                        frmWait.InstancePtr.Refresh();
                                       
                                        cSettingsController set = new cSettingsController();
                                        txtBase.Text = Strings.Format(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK"), "#,##0");
                                        txtVin.Text =
                                            fecherFoundation.Strings.Trim(set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK"));
                                        txtMake.Text =
                                            fecherFoundation.Strings.Trim(set.GetSettingValue("MAKE", "TWGNENTY", "REDBOOK"));
										string model = fecherFoundation.Strings.Trim(set.GetSettingValue("MODEL", "TWGNENTY", "REDBOOK"));
                                        txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
										txtYear.Text =
                                            fecherFoundation.Strings.Trim(set.GetSettingValue("YEAR", "TWGNENTY", "REDBOOK"));
                                        txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                                                                                 fecherFoundation.Strings.Trim(
                                                                                     set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK", "0"))));
                                        blnCalculateRequired = true;
                                        //if (Interaction.GetSetting("TWGNENTY", "REDBOOK", "VEHICLEINSYSTEM", "N") == "Y")
                                        if (set.GetSettingValue("VEHICLEINSYSTEM", "TWGNENTY", "REDBOOK", "N") == "Y")
                                        {
                                            rsTemp.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + txtVin.Text + "'");
                                            if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                                            {
                                                MotorVehicle.Statics.rsFinal.Set_Fields("make", rsTemp.Get_Fields_String("Make"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("Year", rsTemp.Get_Fields_Int32("Year"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("model", rsTemp.Get_Fields_String("Model"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("color1",
                                                                                        rsTemp.Get_Fields_String("Color1"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("color2",
                                                                                        rsTemp.Get_Fields_String("Color2"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("Style", rsTemp.Get_Fields_String("Style"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("Tires",
                                                                                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Tires"))));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("Axles",
                                                                                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Axles"))));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("Fuel", rsTemp.Get_Fields_String("Fuel"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew",
                                                                                        FCConvert.ToString(
                                                                                            Conversion.Val(rsTemp.Get_Fields_Int32("RegisteredWeightNew"))));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight",
                                                                                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("NetWeight"))));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("DOTNumber",
                                                                                        rsTemp.Get_Fields_String("DOTNumber"));
                                                MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber",
                                                                                        rsTemp.Get_Fields_String("UnitNumber"));
                                                MotorVehicle.Statics.blnMakeVehicleInactive = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    MotorVehicle.Statics.NoRedbook = false;
                                    if (chkVIN.CheckState == Wisej.Web.CheckState.Checked)
                                    {
                                        chkVIN.CheckState = Wisej.Web.CheckState.Unchecked;
                                        chkVIN.Visible = false;
                                    }

                                    cSettingsController set = new cSettingsController();
                                    txtBase.Text = Strings.Format(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK"), "#,##0");
                                    txtVin.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK"));
                                    txtMake.Text =
                                        fecherFoundation.Strings.Trim(set.GetSettingValue("MAKE", "TWGNENTY", "REDBOOK"));
									string model = fecherFoundation.Strings.Trim(set.GetSettingValue("MODEL", "TWGNENTY", "REDBOOK"));
									txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
									txtYear.Text =
                                        fecherFoundation.Strings.Trim(set.GetSettingValue("YEAR", "TWGNENTY", "REDBOOK"));
                                    txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                                                                             fecherFoundation.Strings.Trim(set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK", "0"))));
                                    blnCalculateRequired = true;
                                    //if (Interaction.GetSetting("TWGNENTY", "REDBOOK", "VEHICLEINSYSTEM", "N") == "Y")
                                    if (set.GetSettingValue("VEHICLEINSYSTEM", "TWGNENTY", "REDBOOK", "N") == "Y")
                                    {
                                        rsTemp.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + txtVin.Text + "'");
                                        if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                                        {
                                            MotorVehicle.Statics.rsFinal.Set_Fields("make", rsTemp.Get_Fields_String("Make"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Year", rsTemp.Get_Fields_Int32("Year"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("model", rsTemp.Get_Fields_String("Model"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("color1", rsTemp.Get_Fields_String("Color1"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("color2", rsTemp.Get_Fields_String("Color2"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Style", rsTemp.Get_Fields_String("Style"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Tires",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Tires"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Axles",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Axles"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Fuel", rsTemp.Get_Fields_String("Fuel"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("RegisteredWeightNew"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("NetWeight"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("DOTNumber",
                                                                                    rsTemp.Get_Fields_String("DOTNumber"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber",
                                                                                    rsTemp.Get_Fields_String("UnitNumber"));
                                            MotorVehicle.Statics.blnMakeVehicleInactive = true;
                                        }
                                    }
                                }

                                //App.DoEvents();
                                //Support.ZOrder(frmDataInput.InstancePtr, 0);
                            }

                            if (MotorVehicle.Statics.PreviousETO)
                            {
                                txtBase.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"), "#,##0");
                                txtVin.Text =
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")));
                                txtMake.Text =
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make")));
                                txtModel.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model").Trim().Length <= 6 ? MotorVehicle.Statics.rsFinal.Get_Fields_String("model").Trim() : MotorVehicle.Statics.rsFinal.Get_Fields_String("model").Trim().Left(6);
								txtYear.Text =
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year")));
                                txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                                                                         fecherFoundation.Strings.Trim(
                                                                             FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")))));
                                txtMilYear_LostFocus();
                                blnCalculateRequired = true;
                                cSettingsController set = new cSettingsController();
                                //if (Interaction.GetSetting("TWGNENTY", "REDBOOK", "VEHICLEINSYSTEM", "N") == "Y")
                                if (set.GetSettingValue("VEHICLEINSYSTEM", "TWGNENTY", "REDBOOK", "N") == "Y")
                                {
                                    rsTemp.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + txtVin.Text + "'");
                                    if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                                    {
                                        MotorVehicle.Statics.rsFinal.Set_Fields("make", rsTemp.Get_Fields_String("Make"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("Year", rsTemp.Get_Fields_Int32("Year"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("model", rsTemp.Get_Fields_String("Model"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("color1", rsTemp.Get_Fields_String("Color1"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("color2", rsTemp.Get_Fields_String("Color2"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("Style", rsTemp.Get_Fields_String("Style"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("Tires",
                                                                                FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Tires"))));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("Axles",
                                                                                FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Axles"))));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("Fuel", rsTemp.Get_Fields_String("Fuel"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew",
                                                                                FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("RegisteredWeightNew"))));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight",
                                                                                FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("NetWeight"))));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("DOTNumber", rsTemp.Get_Fields_String("DOTNumber"));
                                        MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber",
                                                                                rsTemp.Get_Fields_String("UnitNumber"));
                                        MotorVehicle.Statics.blnMakeVehicleInactive = true;
                                    }
                                }
                            }

                            FillMiscInfo();
                            FillOwnerInfo();
                            FillVehicleInfo();
                            FillExemptFlags();
                            // kk09272016 tromvs-57 follow up  'Call ExciseTaxRequired
                            BottomOfScreen();
                            if (MotorVehicle.GetLowDigit() == true)
                            {
                                if (MessageBox.Show("Does this process involve a name change?", "Name Change?",
                                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                    ConfigExciseTaxOnly();
                                }
                            }

                            CalculateLocalFees();
                            CalculateStateFees();

                            // kk11202015 changed from P5
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "MM" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TX" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "ST" ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AM" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "A3") ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PC" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P6") ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "BU" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "B2"))
                            {
                                chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                ConfigExciseTaxOnly();
                            }

                            break;
                        }

                    case "NRT":
                        {
                            if (MotorVehicle.Statics.Subclass != "TC" && MotorVehicle.Statics.Subclass != "SE" &&
                                MotorVehicle.Statics.Subclass != "L7" &&
                                MotorVehicle.Statics.Subclass != "L4" &&
                                (MotorVehicle.Statics.Subclass != "L2" || MotorVehicle.Statics.Class == "LB") &&
                                MotorVehicle.Statics.Subclass != "C6" && MotorVehicle.Statics.Subclass != "C4" &&
                                (MotorVehicle.Statics.Subclass != "C2" || MotorVehicle.Statics.Class == "CR" ||
                                 MotorVehicle.Statics.Class == "CD" || MotorVehicle.Statics.Class == "CM") &&
                                MotorVehicle.Statics.Subclass != "CI" &&
                                (MotorVehicle.Statics.Subclass != "D1" || MotorVehicle.Statics.Class == "DS" ||
                                 MotorVehicle.Statics.Class == "DX" || MotorVehicle.Statics.Class == "DV") &&
                                MotorVehicle.Statics.ExciseAP != true && MotorVehicle.Statics.PreviousETO == false)
                            {
                                if (MotorVehicle.GetRedbookValues() == true)
                                {
                                    if (chkVIN.CheckState == Wisej.Web.CheckState.Checked)
                                    {
                                        chkVIN.CheckState = Wisej.Web.CheckState.Unchecked;
                                        chkVIN.Visible = false;
                                    }

                                    //FC:FINAL:AM:#i1808 - use cSettingController instead of the registry
                                    //txtBase.Text = Strings.Format(Interaction.GetSetting("TWGNENTY", "REDBOOK", "BASE", ""), "#,##0");
                                    //txtVin.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "VIN", ""));
                                    //txtMake.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "MAKE", ""));
                                    //txtModel.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "MODEL", ""));
                                    //txtYear.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "YEAR", ""));
                                    //txtMilYear.Text = FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "MILYEAR", "0"))));
                                    cSettingsController set = new cSettingsController();
                                    txtBase.Text = Strings.Format(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK"), "#,##0");
                                    txtVin.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK"));
                                    txtMake.Text =
                                        fecherFoundation.Strings.Trim(set.GetSettingValue("MAKE", "TWGNENTY", "REDBOOK"));
                                    string model = fecherFoundation.Strings.Trim(set.GetSettingValue("MODEL", "TWGNENTY", "REDBOOK"));
                                    txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
									txtYear.Text =
                                        fecherFoundation.Strings.Trim(set.GetSettingValue("YEAR", "TWGNENTY", "REDBOOK"));
                                    txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                                                                             fecherFoundation.Strings.Trim(set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK", "0"))));
                                    blnCalculateRequired = true;
                                    //if (Interaction.GetSetting("TWGNENTY", "REDBOOK", "VEHICLEINSYSTEM", "N") == "Y")
                                    if (set.GetSettingValue("VEHICLEINSYSTEM", "TWGNENTY", "REDBOOK", "N") == "Y")
                                    {
                                        rsTemp.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + txtVin.Text + "'");
                                        if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                                        {
                                            MotorVehicle.Statics.rsFinal.Set_Fields("make", rsTemp.Get_Fields_String("Make"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Year", rsTemp.Get_Fields_Int32("Year"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("model", rsTemp.Get_Fields_String("Model"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("color1", rsTemp.Get_Fields_String("Color1"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("color2", rsTemp.Get_Fields_String("Color2"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Style", rsTemp.Get_Fields_String("Style"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Tires",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Tires"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Axles",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Axles"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("Fuel", rsTemp.Get_Fields_String("Fuel"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("RegisteredWeightNew"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight",
                                                                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("NetWeight"))));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("DOTNumber",
                                                                                    rsTemp.Get_Fields_String("DOTNumber"));
                                            MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber",
                                                                                    rsTemp.Get_Fields_String("UnitNumber"));
                                            MotorVehicle.Statics.blnMakeVehicleInactive = true;
                                        }
                                    }
                                }

                                //Support.ZOrder(frmDataInput.InstancePtr, 0);
                                //App.DoEvents();
                            }
                            else if (MotorVehicle.Statics.PreviousETO)
                            {
                                FillVehicleInfo();
                                txtBase.Text = Strings.Format(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("BasePrice"),
                                                              "#,##0");
                                txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                                                                         fecherFoundation.Strings.Trim(
                                                                             FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MillYear")))));
                                txtMilYear_LostFocus();
                                blnCalculateRequired = true;
                            }

                            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) <= 0 &&
                                MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseExempt") != true)
                            {
                                ans2 = DialogResult.Yes;
                                MotorVehicle.Statics.blnForcedAnalysis = true;
                            }
                            else
                            {
                                ans2 = MessageBox.Show("Would you like to run the Transfer Analysis Report?", "Transfer Analysis",
                                                       MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                MotorVehicle.Statics.blnForcedAnalysis = false;
                            }

                            if (ans2 == DialogResult.Yes)
                            {
                                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate")))
                                {
                                    modGNBas.Statics.Response = "";
                                    frmTransferAnalysis.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                                    if (MotorVehicle.Statics.FromTransfer == true)
                                    {
                                        defRS.Reset();
                                        Hide();
                                        return;
                                    }

                                    if (FCConvert.ToString(modGNBas.Statics.Response) == "BAD")
                                    {
                                        defRS.Reset();
                                        Hide();
                                        return;
                                    }
                                }
                                else
                                {
                                    ShowWarningBox("Expiration Date is missing, cannot run the Transfer Analysis Report",
                                                    "Missing Expiration");
                                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3",
                                                                            FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"))));
                                }
                            }
                            else
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3",
                                                                        FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"))));
                            }

                            if (MotorVehicle.GetTransferCredit(this) == false)
                            {
                                ShowWarningBox("Some of the credit information will have to be supplied manually.",
                                                "Credit Information Missing");
                            }

                            if (MotorVehicle.Statics.LostPlate || blnChargeForPlate)
                            {
                                rsClassInfo.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class +
                                                          "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'");
                                if (rsClassInfo.EndOfFile() != true && rsClassInfo.BeginningOfFile() != true)
                                {
                                    txtYearCharge.Text = FCConvert.ToString(rsClassInfo.Get_Fields_Int32("NumberOfPlateStickers"));
                                    txtMonthCharge.Text = FCConvert.ToString(rsClassInfo.Get_Fields_Int32("NumberOfPlateStickers"));
                                }
                                else
                                {
                                    txtYearCharge.Text = FCConvert.ToString(2);
                                    txtMonthCharge.Text = FCConvert.ToString(2);
                                }

                                SetMonthNumbers();
                                SetYearNumbers();
                            }

                            txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"), "MM/dd/yyyy");
                            CalculateEffectiveDate(); 
                            FillMiscInfo();
                            FillOwnerInfo();
                            FillVehicleInfo();
                            FillExemptFlags();
                            // kk09272016 tromvs-57 follow up  'Call ExciseTaxRequired
                            BottomOfScreen();
                            if (MotorVehicle.GetLowDigit() == true)
                            {
                                if (MessageBox.Show("Does this process involve a name change?", "Name Change?",
                                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                    ConfigExciseTaxOnly();
                                }
                            }

                            CalculateLocalFees();
                            CalculateStateFees();

                            if (MotorVehicle.Statics.PreviousETO)
                            {
                                chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                            }

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TX" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "ST")
                            {
                                chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                ConfigExciseTaxOnly();
                            }

                            break;
                        }

                    case "HELD":
                        {
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N")
                            {
                                MotorVehicle.Statics.NewPlate = true;
                            }
                            else
                            {
                                MotorVehicle.Statics.NewPlate = false;
                            }

                            MotorVehicle.Statics.ForcedPlate =
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate"));
                            Get_Plate_Code(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")));
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TransactionType")) == "ECR" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TransactionType")) == "ECO")
                            {
                                MotorVehicle.Statics.RegistrationType = "CORR";
                                if (MotorVehicle.Statics.NewPlate)
                                {
                                    MotorVehicle.Statics.PlateType = 2;
                                    if (!MotorVehicle.Statics.intCorrNewPlate)
                                    {
                                        // And ForcedPlate <> "N"
                                        if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                                                                  MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")) < 4 &&
                                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "TL")
                                        {
                                            intCorrCorrectOriginal = MessageBox.Show(
                                                "Should this transaction be effective as of the original transaction date? Please note this will affect half rating and 2 year registrations if applicable.",
                                                "Correct Original?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                                            if (intCorrCorrectOriginal == DialogResult.Cancel)
                                            {
                                                modGNBas.Statics.Response = "BAD";
                                                return;
                                            }
                                        }
                                        else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "TL")
                                        {
                                            intCorrCorrectOriginal = DialogResult.Yes;
                                        }
                                        else
                                        {
                                            intCorrCorrectOriginal = DialogResult.No;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MotorVehicle.Statics.RegistrationType =
                                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TransactionType"));
                            }

                            if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate").ToOADate() >
                                DateTime.Today.ToOADate())
                            {
                                txtEffectiveDate.Text =
                                    Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"), "MM/dd/yyyy");
                            }
                            else
                            {
                                txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                            }

                            txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"), "MM/dd/yyyy");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
                            {
                                frmFeesInfo.InstancePtr.txtFEETransferFee.Text =
                                    Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferFee"), "#,##0.00");
                            }

                            txtPriorTitleState.Text =
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleState"));
                            FillAll();
                            // If RegistrationType = "NRR" Or RegistrationType = "NRT" Then
                            // txtEffectiveDate.Text = Format(Date, "MM/dd/yyyy")
                            // End If
                            FillVehicleInfo();
                            frmFeesInfo.InstancePtr.txtFEERegFee.Text =
                                Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"), "#,##0.00");
                            frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text =
                                Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditFull"), "#,##0.00");
                            frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(
                                MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                                                  MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"),
                                                                  chkTrailerVanity.CheckState == Wisej.Web.CheckState.Checked,
                                                                  chk2Year.CheckState == Wisej.Web.CheckState.Checked), "#,##0.00");
                            if ((MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CR") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "UM") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "LB") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "BB") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AG") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AC") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TS") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "BC") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AW") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "LC") ||
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "BH"))
                            {
                                if (MotorVehicle.Statics.NewPlate == true ||
                                    (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 2))
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "20.00";
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "15.00";
                                }
                            }
                            else if (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                     "SW")
                            {
                                frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "20.00";
                            }
                            else if (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                     "PH")
                            {
                                if (MotorVehicle.Statics.NewPlate == true)
                                {
                                    if (MotorVehicle.Statics.Class == "PM")
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "5.00";
                                    }
                                    else
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "10.00";
                                    }
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                }
                            }
                            else if (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                     "VT")
                            {
                                if (MotorVehicle.Statics.NewPlate == true)
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "5.00";
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                }
                            }

                            FillFeesScreen();
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TX" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "ST")
                            {
                                chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                ConfigExciseTaxOnly();
                            }

                            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("EAP") &&
                                MotorVehicle.Statics.RegistrationType != "CORR")
                            {
                                MotorVehicle.Statics.PreviousETO = true;
                                MotorVehicle.Statics.blnCheck = true;
                                chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                                MotorVehicle.Statics.blnCheck = false;
                            }

                            break;
                        }

                    case "CORR":
                        {
                            if (MotorVehicle.Statics.PlateType == 2)
                            {
	                            if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")))
	                            {
		                            if (MotorVehicle.Statics.datMotorCycleExpires.ToOADate() == 0)
		                            {
			                            if (!Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("ExpireDate")))
			                            {
				                            MotorVehicle.Statics.datMotorCycleExpires = DateTime.FromOADate(0);
			                            }
			                            else
			                            {
				                            MotorVehicle.Statics.datMotorCycleExpires =
					                            MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate");
			                            }
		                            }

		                            if (MotorVehicle.Statics.gblnDelayedReg)
		                            {
			                            MotorVehicle.Statics.datMotorCycleEffective = DateTime.Today;
		                            }
		                            else
		                            {
			                            if (!Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
			                            {
				                            MotorVehicle.Statics.datMotorCycleEffective = DateTime.FromOADate(0);
			                            }
			                            else
			                            {
				                            MotorVehicle.Statics.datMotorCycleEffective =
					                            MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate");
			                            }
		                            }
								}
	                           

								if (!MotorVehicle.Statics.intCorrNewPlate)
                                {
                                    // And ForcedPlate <> "N"
                                    if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                                                              MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")) < 4 &&
                                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "TL")
                                    {
                                        intCorrCorrectOriginal = MessageBox.Show(
                                            "Should this transaction be effective as of the original transaction date? Please note this will affect half rating and 2 year registrations if applicable.",
                                            "Correct Original?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                                        if (intCorrCorrectOriginal == DialogResult.Cancel)
                                        {
                                            modGNBas.Statics.Response = "BAD";
                                            return;
                                        }
                                    }
                                    else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "TL")
                                    {
                                        intCorrCorrectOriginal = DialogResult.Yes;
                                    }
                                    else
                                    {
                                        intCorrCorrectOriginal = DialogResult.No;
                                    }
                                }

                                using (clsDRWrapper rsTaxInfo = new clsDRWrapper())
                                {
                                    MotorVehicle.Statics.strSql = "SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class +
                                                                  "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'";
                                    rsTaxInfo.OpenRecordset(MotorVehicle.Statics.strSql);
                                    if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
                                    {
                                        if (FCConvert.ToString(rsTaxInfo.Get_Fields_String("ExciseRequired")) == "Y")
                                        {
                                            MotorVehicle.Statics.rsFinal.Set_Fields("ExciseExempt", false);
                                        }
                                        else
                                        {
                                            MotorVehicle.Statics.rsFinal.Set_Fields("ExciseExempt", true);
                                        }
                                    }
                                }
                            }

                            FillAll();
                            StartTabs();
                            if (MotorVehicle.Statics.PlateType == 1)
                            {
                                blnCalculateRequired = false;
                            }
                            else
                            {
                                CalculateDates();
                            }

                            if (intCorrCorrectOriginal == DialogResult.Yes)
                            {
                                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf") == true)
                                {
                                    chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
                                    lblStateRateHR.Visible = true;
                                    lblStateCreditHR.Visible = true;
                                    lblStateFeesHR.Visible = true;
                                }
                                else
                                {
                                    chkHalfRateState.CheckState = Wisej.Web.CheckState.Unchecked;
                                }

                                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate") == true)
                                {
                                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
                                    lblTownCreditHR.Visible = true;
                                    lblExciseTaxHR.Visible = true;
                                    lblLocalSubHR.Visible = true;
                                    lblLocalBalanceHR.Visible = true;
                                }
                                else
                                {
                                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Unchecked;
                                }
                            }

                            if (MotorVehicle.Statics.PlateType == 2 && FCConvert.ToString(modGNBas.Statics.Response) != "BAD")
                            {
                                string strSql;
                                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "TL")
								{
									strSql = "SELECT Code, FormattedInventory FROM Inventory WHERE Status = 'A' AND (Code = 'PXSTL' OR Code = 'PXSLT') AND ((Prefix + CONVERT(NVARCHAR, Number) + Suffix) = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateStripped") + "' OR FormattedInventory = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateStripped") + "')";
								}
								else
								{
									strSql = "SELECT Code, FormattedInventory FROM Inventory WHERE Status = 'A' AND (Code = 'PXS" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' OR Code = 'PXD" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "') AND ((Prefix + CONVERT(NVARCHAR, Number) + Suffix) = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateStripped") + "' OR FormattedInventory = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateStripped") + "')";
								}
								rsTemp.OpenRecordset(strSql);
								if (!rsTemp.EndOfFile())
								{
									MotorVehicle.Statics.intCorrNewPlate = true;    //New plate only if from inventory
								}
	                            else
								{
									MotorVehicle.Statics.intCorrNewPlate = false;    //New plate only if from inventory
								}
                            }

                            BottomOfScreen();
                            FillCorrectionFeesScreen();
                            CalculateLocalECorrectFees();
                            txtFees.Text = Strings.Format(frmFeesInfo.InstancePtr.txtFEETotal.Text, "#,##0.00");
                            defRS.Reset();
                            blnLoadingForm = false;
                            // kk11202015 changed from P5
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "MM" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TX" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS" ||
                                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "ST" ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AM" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "A3") ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PC" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P6") ||
                                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "BU" &&
                                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "B2"))
                            {
                                chkETO.CheckState = Wisej.Web.CheckState.Checked;
                                ConfigExciseTaxOnly();
                            }

                            return;
                        }

                    case "DUPREG":
                        MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.OpID);
                        frmDataInput.InstancePtr.Hide();
                        frmPreview.InstancePtr.DisableReturnButton = true;
                        frmPreview.InstancePtr.DisableUpdateReasonCodes = true;
                        frmPreview.InstancePtr.Show(App.MainForm);
                        defRS.Reset();
                        return;

                    case "LOST":
                        SetTabStopsToTrue();
                        blnCalculateRequired = false;
                        txtClass.Text = MotorVehicle.Statics.Class;
                        txtRegistrationNumber.Text = MotorVehicle.Statics.Plate1;
                        txtEffectiveDate.Enabled = true;
                        txtEffectiveDate.TabStop = true;
                        frmPlateInfo.InstancePtr.Unload();
                        frmWait.InstancePtr.Unload();
                        defRS.Reset();
                        return;

                    case "DUPSTK":
                        break;
                    case "GVW":
                        {
                            FillAll();
                            txtExpires.Locked = true;
                            txtEffectiveDate.Locked = true;
                            VerifyExpireDate(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"),
                                                            "MM/dd/yyyy"));
                            DisableBottomOfScreen();
                            chkETO.Enabled = false;
                            chkCC.Enabled = false;
                            if (chkHalfRateState.CheckState == Wisej.Web.CheckState.Checked)
                            {
                                lblStateRateHR.Visible = true;
                                lblStateCreditHR.Visible = true;
                                lblStateFeesHR.Visible = true;
                            }

                            if (chkHalfRateLocal.CheckState == Wisej.Web.CheckState.Checked)
                            {
                                lblTownCreditHR.Visible = true;
                                lblExciseTaxHR.Visible = true;
                                lblLocalSubHR.Visible = true;
                                lblLocalBalanceHR.Visible = true;
                            }

                            BottomOfScreen();
                            FillCorrectionFeesScreen();
                            CalculateLocalECorrectFees();
                            txtFees.Text = Strings.Format(frmFeesInfo.InstancePtr.txtFEETotal.Text, "#,##0.00");
                            defRS.Reset();
                            blnLoadingForm = false;

                            break;
                        }

                    case "TRANSIT":
                        break;
                    case "BOOST":
                        break;
                    case "SPREG":
                        break;
                    case "VOID":
                        break;
                    case "UPD":
                        break;
                }

                if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
                {
                    if (!Information.IsDate(txtExpires.Text))
                    {
                        MotorVehicle.Statics.datMotorCycleExpires = DateTime.FromOADate(0);
                    }
                    else
                    {
                        MotorVehicle.Statics.datMotorCycleExpires = FCConvert.ToDateTime(txtExpires.Text);
                    }

                    if (!Information.IsDate(txtEffectiveDate.Text))
                    {
                        MotorVehicle.Statics.datMotorCycleEffective = DateTime.FromOADate(0);
                    }
                    else
                    {
                        MotorVehicle.Statics.datMotorCycleEffective = FCConvert.ToDateTime(txtEffectiveDate.Text);
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.datMotorCycleExpires.ToOADate() == 0)
                    {
                        if (!Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("ExpireDate")))
                        {
                            MotorVehicle.Statics.datMotorCycleExpires = DateTime.FromOADate(0);
                        }
                        else
                        {
                            MotorVehicle.Statics.datMotorCycleExpires =
                                MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate");
                        }
                    }

                    if (MotorVehicle.Statics.gblnDelayedReg)
                    {
                        MotorVehicle.Statics.datMotorCycleEffective = DateTime.Today;
                    }
                    else
                    {
                        if (!Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
                        {
                            MotorVehicle.Statics.datMotorCycleEffective = DateTime.FromOADate(0);
                        }
                        else
                        {
                            MotorVehicle.Statics.datMotorCycleEffective =
                                MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate");
                        }
                    }

                    if (MotorVehicle.Statics.RegistrationType == "RRR" && MotorVehicle.IsProratedMotorcycleExpirationDate(
                            MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                            MotorVehicle.Statics.datMotorCycleEffective, MotorVehicle.Statics.datMotorCycleExpires,
                            MotorVehicle.Statics.RegistrationType))
                    {
                        CalculateEffectiveDate();
                    }
                }

                StartTabs();
                TabStops();
                FillFeesScreen();
                FillTotalDue();

                if (chkHalfRateLocal.CheckState != Wisej.Web.CheckState.Checked &&
                    chkHalfRateState.CheckState != Wisej.Web.CheckState.Checked)
                {
                    blnCalculateRequired = false;
                }

                // Dave 8/15/07 Moved from form activate
                // kk11202015 changed from P5
                if (MotorVehicle.Statics.TownLevel == 6 ||
                    (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) == "NEW" && MotorVehicle.Statics.TownLevel != 9) ||
                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP" ||
                    (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "PC" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "P6") ||
                    (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "BU" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "B2"))
                {
                    chkETO.CheckState = Wisej.Web.CheckState.Checked;
                    ConfigExciseTaxOnly();
                }

                if (MotorVehicle.Statics.ETO == true)
                {
                    CalculateEffectiveDate();
                    if (VerifyExpireDate(txtExpires.Text) == "T")
                    {
                        // do nothing
                    }

                    chkETO.CheckState = Wisej.Web.CheckState.Checked;
                    ConfigExciseTaxOnly();
                    if (MotorVehicle.Statics.RegistrationType == "NRR")
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP")
                        {
                            // kk04142016 tromv-1145
                            // Dave 11/5/2007 Added for fleet registration because it shoudl show expires date
                            if (!MotorVehicle.Statics.RegisteringFleet 
                                && !MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.datMotorCycleEffective,
	                                                                  ref MotorVehicle.Statics.datMotorCycleExpires, MotorVehicle.Statics.RegistrationType))
                            {
                                txtExpires.Text = "";
                                txtEffectiveDate.Text = "";
                            }

                            txtExpires.Enabled = false;
                            txtEffectiveDate.Enabled = false;
                        }
                    }

                    txtClass.Text = MotorVehicle.Statics.Class;
                    if (MotorVehicle.Statics.RegistrationType == "NRR" &&
                        MotorVehicle.Statics.PlateType == FCConvert.ToDouble("1"))
                    {
                        txtRegistrationNumber.Text = "NEW";
                    }
                    else
                    {
                        txtRegistrationNumber.Text = MotorVehicle.Statics.Plate1;
                    }
                }
                else
                {
                    chkETO.CheckState = Wisej.Web.CheckState.Unchecked;
                    txtExpires.Enabled = true;
                    txtEffectiveDate.Enabled = true;
                }

                if (MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "CL")
                {
                    txtMileage.Text = "";
                    txtMileage.Enabled = false;
                }

                defRS.Reset();
                //App.DoEvents();
                blnLoadingForm = false;
                if (blnCalculateRequired)
                {
                    lblCalculate_Click();
                }
            }
            finally
            {
                defRS.Dispose();
                rsResCode.Dispose();
                rsClassInfo.Dispose();
                rsTemp.Dispose();
            }

        }

        // vbPorter upgrade warning: Cancel As int	OnWrite(bool)
        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;

            if (e.CloseReason == FCCloseReason.FormControlMenu)
            {
                ans = MessageBox.Show("Are you sure you want to end this registration?", "Quit Registration",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (ans == DialogResult.Yes)
                {
                    StaticSettings.GlobalCommandDispatcher.Send(new ReleaseGlobalResources());
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }

            AgentFee = 0;
            MileageOK = false;
            OldColor = 0;
            WhichObject = "";
            Wait = 0;
            MotorVehicle.Statics.HVUT = false;
            frmFeesInfo.InstancePtr.Unload();
            //FC:FINAL:SBE - #2198, #2368 - Unload() will not clear the session instance, if form was not displayed. use Dispose() to clear the instance from session
            frmFeesInfo.InstancePtr.Dispose();
            frmFeesInfoStaticsValues.InstancePtr.Unload();
            MotorVehicle.Statics.DataFormLoaded = false;
            MileageOK = false;
            //FC:FINAL:DDU:#i1803 - add flag to don't enter in Leave event
            formClosing = true;
        }

        private void frmDataInput_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (MotorVehicle.Statics.FromTransfer || MotorVehicle.Statics.RegisteringFleet)
            {
                //frmPlateInfo.InstancePtr.Show(App.MainForm);
                frmPlateInfo.InstancePtr.ShowForm();
                frmPlateInfo.InstancePtr.Timer1.Enabled = true;
            }
        }

        private void Image1_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")),
                "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //FC:FINAL:DDU:#2793 - maintain shown image from theme
            Image1.ImageSource = "imgnote?color=#707884";
        }

        private void imgFleetComment_Click(object sender, System.EventArgs e)
        {
            mnuFleetGroupComment_Click();
        }

        private void mnuFleetGroupComment_Click(object sender, System.EventArgs e)
        {
            if (Conversion.Val(txtFleetNumber.Text) > 0)
            {
                if (!frmCommentOld.InstancePtr.Init("MV", "FleetMaster", "Comment", "FleetNumber",
                    FCConvert.ToInt32(FCConvert.ToDouble(txtFleetNumber.Text)), boolModal: true))
                {
                    imgFleetComment.Visible = true;
                }
                else
                {
                    imgFleetComment.Visible = false;
                }
            }
        }

        public void mnuFleetGroupComment_Click()
        {
            mnuFleetGroupComment_Click(mnuFleetGroupComment, new System.EventArgs());
        }

        private void Label35_DoubleClick(object sender, System.EventArgs e)
        {
            if (MotorVehicle.Statics.Subclass != "TC" && MotorVehicle.Statics.Subclass != "SE" &&
                MotorVehicle.Statics.Subclass != "L7" &&
                MotorVehicle.Statics.Subclass != "L4" &&
                (MotorVehicle.Statics.Subclass != "L2" || MotorVehicle.Statics.Class == "LB") &&
                MotorVehicle.Statics.Subclass != "C6" && MotorVehicle.Statics.Subclass != "C4" &&
                (MotorVehicle.Statics.Subclass != "C2" || MotorVehicle.Statics.Class == "CR" ||
                 MotorVehicle.Statics.Class == "CD" || MotorVehicle.Statics.Class == "CM") &&
                MotorVehicle.Statics.Subclass != "CI" && (MotorVehicle.Statics.Subclass != "D1" ||
                                                          MotorVehicle.Statics.Class == "DS" ||
                                                          MotorVehicle.Statics.Class == "DX" ||
                                                          MotorVehicle.Statics.Class == "DV"))
            {
                if (MotorVehicle.GetRedbookValues() == true)
                {
                    if (chkVIN.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        chkVIN.CheckState = Wisej.Web.CheckState.Unchecked;
                        chkVIN.Visible = false;
                    }

                    // 
                    //FC:FINAL:AM:#i1808 - use cSettingController instead of the registry
                    //txtBase.Text = Strings.Format(Interaction.GetSetting("TWGNENTY", "REDBOOK", "BASE", ""), "#,##0");
                    //txtVin.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "VIN", ""));
                    //txtMake.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "MAKE", ""));
                    //txtModel.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "MODEL", ""));
                    //txtYear.Text = fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "YEAR", ""));
                    //txtMilYear.Text = FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "MILYEAR", "0"))));
                    cSettingsController set = new cSettingsController();
                    txtBase.Text = Strings.Format(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK"), "#,##0");
                    txtVin.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK"));
                    txtMake.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("MAKE", "TWGNENTY", "REDBOOK"));
                    string model = fecherFoundation.Strings.Trim(set.GetSettingValue("MODEL", "TWGNENTY", "REDBOOK"));
                    txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
                    txtYear.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("YEAR", "TWGNENTY", "REDBOOK"));
                    txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                        fecherFoundation.Strings.Trim(set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK", "0"))));
                    //if (Interaction.GetSetting("TWGNENTY", "REDBOOK", "VEHICLEINSYSTEM", "N") == "Y")
                    if (set.GetSettingValue("VEHICLEINSYSTEM", "TWGNENTY", "REDBOOK", "N") == "Y")
                    {
                        using (clsDRWrapper rsTemp = new clsDRWrapper())
                        {
                            rsTemp.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + txtVin.Text + "'");
                            if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("make", rsTemp.Get_Fields_String("Make"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("Year", rsTemp.Get_Fields_Int32("Year"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("model", rsTemp.Get_Fields_String("Model"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("color1", rsTemp.Get_Fields_String("Color1"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("color2", rsTemp.Get_Fields_String("Color2"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("Style", rsTemp.Get_Fields_String("Style"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("Tires",
                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Tires"))));
                                MotorVehicle.Statics.rsFinal.Set_Fields("Axles",
                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Axles"))));
                                MotorVehicle.Statics.rsFinal.Set_Fields("Fuel", rsTemp.Get_Fields_String("Fuel"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew",
                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("RegisteredWeightNew"))));
                                MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight",
                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("NetWeight"))));
                                MotorVehicle.Statics.rsFinal.Set_Fields("DOTNumber", rsTemp.Get_Fields_String("DOTNumber"));
                                MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber",
                                    rsTemp.Get_Fields_String("UnitNumber"));
                                MotorVehicle.Statics.blnMakeVehicleInactive = true;
                            }
                        }
                    }

                    lblCalculate_Click();
                    blnCalculateRequired = false;
                    // 
                    FillRSFinal();
                }

                txtVin.Focus();
                this.Update();
            }
        }

        private void lblTitle_DoubleClick(object sender, System.EventArgs e)
        {
            if (cmdCTA.Enabled)
            {
                MotorVehicle.Statics.CTAFlag = true;
                frmCTA.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TitleFee") != 0)
                {
                    txtTitle.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TitleFee"),
                        "#,##0.00");
                }
                else
                {
                    txtTitle.Text = "0.00";
                }
            }
        }

        private void Label46_DoubleClick(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired == true)
                lblCalculate_Click();
            blnFeesFlag = true;
            frmFeesInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            //FC:FINAL:MSH - i.issue #1804: update values in fields after closing form (frmFeesInfo will be disposed after hiding and all data will be missed)
            frmFeesInfoStaticsValues.InstancePtr.UpdateValuesFromStorage();
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                lblCalculate_Click();
                // FillFeesScreen
            }
            else
            {
                FillFeesScreen();
                FillTotalDue();
            }

            blnCalculateRequired = true;
            frmDataInput.InstancePtr.Refresh();
        }

        public void lblCalculate_Click(object sender, System.EventArgs e)
        {
            Decimal OldGVWRate;
            Decimal NewGVWRate;
            if (MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "GVW")
            {
                FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
                Recalculate();
                if (!blnLoadingForm)
                {
                    FillRSFinal();
                }

                Recalculate();
                if (!blnLoadingForm)
                {
                    FillRSFinal();
                }

                //App.DoEvents();
                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
	                MotorVehicle.CalculateECorrectFee(ref intCorrSpecialtyPlate, ref intCorrInitialPlate);
                }
/*
                if (chkSpecialtyCorrExtraFee.Value == FCCheckBox.ValueSettings.Checked)
                {
	                MotorVehicle.Statics.ECTotal += 1;
                }
*/

				FCGlobal.Screen.MousePointer = 0;
            }
            else
            {
                NewGVWRate = FCConvert.ToDecimal(Strings.Format(
                    MotorVehicle.GetRegRate(this, MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass),
                    "#,##0.00"));
                Recalculate();
            }

            blnCalculateRequired = false;
        }

        public void lblCalculate_Click()
        {
            lblCalculate_Click(lblCalculate, new System.EventArgs());
        }

        private void lblFees_DoubleClick(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired == true)
                lblCalculate_Click();
            blnFeesFlag = true;
            frmFeesInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            //FC:FINAL:MSH - i.issue #1804: update values in fields after closing form (frmFeesInfo will be disposed after hiding and all data will be missed)
            frmFeesInfoStaticsValues.InstancePtr.UpdateValuesFromStorage();
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                lblCalculate_Click();
                FillFeesScreen();
                blnCalculateRequired = true;
            }
            else
            {
                FillFeesScreen();
                FillTotalDue();
            }
        }

        private void lblStateCredit_DoubleClick(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired == true)
                lblCalculate_Click();
            blnFeesFlag = true;
            frmFeesInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            //FC:FINAL:MSH - i.issue #1804: update values in fields after closing form (frmFeesInfo will be disposed after hiding and all data will be missed)
            frmFeesInfoStaticsValues.InstancePtr.UpdateValuesFromStorage();
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                lblCalculate_Click();
                FillFeesScreen();
                blnCalculateRequired = true;
            }
            else
            {
                FillFeesScreen();
                FillTotalDue();
            }
        }

        private void lblUseTax_DoubleClick(object sender, System.EventArgs e)
        {
            if (cmdUseTax.Enabled)
            {
                mnuUseTax_Click();
            }
        }

        private void mnuCalculate_Click(object sender, System.EventArgs e)
        {
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                lblCalculate_Click();
            }
            else
            {
                Recalculate();
            }

            blnCalculateRequired = false;
        }

        private void mnuCancel_Click(object sender, System.EventArgs e)
        {
            DialogResult ans;
            
            ans = MessageBox.Show("Are you sure you want to end this registration?", "Quit Registration",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ans == DialogResult.Yes)
            {
                StaticSettings.GlobalCommandDispatcher.Send(new ReleaseGlobalResources());
                frmPreview.InstancePtr.Unload();
                Close();
            }
        }

        private void mnuCTA_Click(object sender, System.EventArgs e)
        {
            if (fecherFoundation.Strings.Trim(txtYear.Text) == "")
            {
                MessageBox.Show("You must input a year before you may process a Title.", "Year Needed",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // If Year(Date) - 16 >= txtYear.Text Then
            if (FCConvert.ToDouble(txtYear.Text) < 1995)
            {
                ShowWarningBox("You may not process a Title on a Vehicle that was made earlier than 1995.",
                    "Vehicle Too Old");
                return;
            }

            MotorVehicle.Statics.CTAFlag = true;
            frmCTA.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TitleFee") != 0)
            {
                txtTitle.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TitleFee"), "#,##0.00");
                chkCTAPaid.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            else
            {
                txtTitle.Text = "0.00";
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeCTA")))
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Unchecked;
            }
        }

        public void mnuCTA_Click()
        {
            mnuCTA_Click(mnuCTA, new System.EventArgs());
        }

        private void mnuOpenFields_Click(object sender, System.EventArgs e)
        {
            MotorVehicle.Statics.OpenFlag = true;
            SetTabStopsToTrue();
        }

        private void mnuPreview_Click(object sender, System.EventArgs e)
        {
            if (alreadySaving)
            {
                return;
            }

            alreadySaving = true;
            // vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
            DialogResult answer = 0;
            Decimal curValue;
            string strValue = "";
            // Print #100, Time & " - Start"
            //App.DoEvents();
            if (blnCompany == true)
            {
                blnCompany = false;
            }

            if (frmDataInput.InstancePtr.ActiveControl.GetName() == "txtRegWeight")
            {
                txtRegWeight_Validate(false);
            }

            // Print #100, Time & " - Validate Weight"
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (MotorVehicle.Statics.TownLevel == 1)
                {
                    if (MotorVehicle.Statics.GrandTotal != 0)
                    {
                        ShowWarningBox(
                            "You may not process a Monetary Correction with your State Authorization Level.",
                            "Invalid Authorization Level");
                        alreadySaving = false;
                        return;
                    }
                }
            }

            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
            {
	            if (MotorVehicle.Statics.TownLevel == 5)
	            {
		            if (chkETO.Checked != true && chkDealerSalesTax.Checked != true)
		            {
			            ShowWarningBox(
				            "You may not process a new registration at your State Authorization Level unless it is a dealer sale or an excise tax only transaction.",
				            "Invalid Authorization Level");
			            alreadySaving = false;
			            return;
		            }
	            }
            }

			if (modGlobalConstants.Statics.gboolCR && !MotorVehicle.Statics.bolFromWindowsCR &&
                !MotorVehicle.Statics.PendingRegistration)
            {
                MessageBox.Show("Transactions must be completed through Cash Receipting.", "Unable to Proceed",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                alreadySaving = false;
                return;
            }

            blnFinalCheck = true;
            if (blnCalculateRequired == true)
            {
                lblCalculate_Click();
                //App.DoEvents();
            }

            // Print #100, Time & " - Calculate Done"
            if (MotorVehicle.Statics.RegistrationType != "GVW")
            {
                blnVerifyFlag = true;
                if (ValidateAll() == false)
                {
                    // Bp = Beep(1000, 250)
                    blnFinalCheck = false;
                    alreadySaving = false;
                    return;
                }

                blnVerifyFlag = false;
            }
            else
            {
                if (VerifyRVW() == false)
                {
                    blnFinalCheck = false;
                    alreadySaving = false;
                    return;
                }
            }

            blnFinalCheck = false;
            if (cboBattle.Enabled == true && cboBattle.SelectedIndex < 1)
            {
                answer = MessageBox.Show(
                    "This vehicle is permitted to have a veteran decal.  Do you wish to assign one before you continue?",
                    "Assign Decal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes)
                {
                    cboBattle.Focus();
                    alreadySaving = false;
                    return;
                }
            }

            if (MotorVehicle.Statics.ShowReminders && MotorVehicle.Statics.TownLevel != 5)
            {
                if ((MotorVehicle.Statics.RegistrationType == "NRR" ||
                     MotorVehicle.Statics.RegistrationType == "NRT") &&
                    chkETO.CheckState != Wisej.Web.CheckState.Checked)
                {
                    if (MotorVehicle.Statics.lngCTAAddNewID == 0)
                    {
                        answer = MessageBox.Show(
                            "You have not completed your CTA form yet.  Would you like to do that now?", "CTA Form",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (answer == DialogResult.Yes)
                        {
                            mnuCTA_Click();
                            alreadySaving = false;
                            return;
                        }
                    }

                    if (MotorVehicle.Statics.lngUTCAddNewID == 0)
                    {
                        answer = MessageBox.Show(
                            "You have not completed your Sales Tax form yet.  Would you like to do that now?",
                            "Sale Tax Form", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (answer == DialogResult.Yes)
                        {
                            mnuUseTax_Click();
                            alreadySaving = false;
                            return;
                        }
                    }
                }
            }

            if (!Information.IsDate(txtEffectiveDate.Text) && !MotorVehicle.Statics.ETO)
            {
                ShowWarningBox("You must have a valid effective date before you may save this information.",
                    "Invalid Effective Date");
                txtEffectiveDate.Focus();
                alreadySaving = false;
                return;
            }
            else
            {
                if (Information.IsDate(txtExpires.Text))
                {
                    if (fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text).ToOADate() >
                        fecherFoundation.DateAndTime.DateValue(txtExpires.Text).ToOADate())
                    {
                        ShowWarningBox("You must have a valid effective date before you may save this information.",
                            "Invalid Effective Date");
                        txtEffectiveDate.Focus();
                        alreadySaving = false;
                        return;
                    }

                    // vbPorter upgrade warning: NM As int	OnWrite(long)
                    int NM = 0;
                    NM = FCConvert.ToInt32((fecherFoundation.DateAndTime.DateDiff("m",
                        FCConvert.ToDateTime(txtEffectiveDate.Text), FCConvert.ToDateTime(txtExpires.Text))));
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI" ||
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS" ||
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "MM")
                    {
                        if (NM > 120)
                        {
                            ShowWarningBox(
                                "You must have a valid expiration date before you may save this information.",
                                "Invalid Expiration Date");
                            txtExpires.Focus();
                            alreadySaving = false;
                            return;
                        }
                    }
                    else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "ST")
                    {
                        if (NM > 72)
                        {
                            ShowWarningBox(
                                "You must have a valid expiration date before you may save this information.",
                                "Invalid Expiration Date");
                            txtExpires.Focus();
                            alreadySaving = false;
                            return;
                        }
                    }
                    else
                    {
                        if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                        {
                            if (NM > 24)
                            {
                                ShowWarningBox(
                                    "You must have a valid expiration date before you may save this information.",
                                    "Invalid Expiration Date");
                                txtExpires.Focus();
                                alreadySaving = false;
                                return;
                            }
                        }
                        else
                        {
                            if (NM > 12)
                            {
                                ShowWarningBox(
                                    "You must have a valid expiration date before you may save this information.",
                                    "Invalid Expiration Date");
                                txtExpires.Focus();
                                alreadySaving = false; 
                                return;
                            }
                        }
                    }
                }
            }

            if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CO" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TT") &&
                Conversion.Val(txtRegWeight.Text) > 9000)
            {
                if (txtFuel.Text == "G")
                {
                    answer = MessageBox.Show("Your fuel type is set as gas.  Is this correct?", "Fuel Type",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (answer == DialogResult.No)
                    {
                        txtFuel.Focus();
                        alreadySaving = false;
                        return;
                    }
                }
            }

            if ((MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "RRR") &&
                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CC" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CO" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TT" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AC" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF" ||
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "LC" ||
                 (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TR" &&
                  FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "T1") ||
                 (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TR" &&
                  FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "T2")) &&
                (Conversion.Val(txtYear.Text) >= 1996 && Conversion.Val(txtRegWeight.Text) > 26000))
            {
            ReDoItTag:;

                InputBoxDialog ib = new InputBoxDialog();
                ib.FormPrompt =
                    "This vehicle is eligible for the excise tax reimbursement program.   " +
					"To include this vehicle, make sure that the sale price of the vehicle has been entered as the base price for determining excise, " +
                    "enter the MSRP value of the vehicle below, and click OK. Remember to fill out an Excise Tax Reimbursement form.   " +
                    "Click Cancel if this vehicle will not be included.";
                ib.FormCaption = "Excise Tax Reimbursement";
                ib.DefaultValue = "";
                
                if (ib.ShowDialog() == DialogResult.OK)
                {
                    strValue = ib.InputResponse;
                    if (Information.IsNumeric(strValue))
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("Value", FCConvert.ToDecimal(strValue));
                    }
                    else
                    {
                        FCMessageBox.Show("Only numeric values are valid for the list price.",MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Retry");
                        goto ReDoItTag;
                    }
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("Value", 0);
                }
                
                ib.Close();
            }

            MotorVehicle.Statics.PreviewFlag = true;
            if (MotorVehicle.Statics.ETO == true && MotorVehicle.Statics.RegistrationType == "NRR" &&
                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", 0);
                // Format(Now, "MM/dd/yy") 'Trim(txtExpires.Text)
                // kk03272017 tromv-1294  Allow corrections on ETO without Expire Date (NRR is not supposed to have expire date unless AP Class)
            }
            else if (MotorVehicle.Statics.ETO == true && MotorVehicle.Statics.RegistrationType == "CORR" &&
                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP" &&
                     (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("TransactionType")) ==
                      "NRR" || FCConvert.ToString(
                          MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("TransactionType")) == "ECO"))
            {
                if (fecherFoundation.Strings.Trim(txtExpires.Text) != "")
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate",
                        fecherFoundation.Strings.Trim(txtExpires.Text));
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", 0);
                }
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", fecherFoundation.Strings.Trim(txtExpires.Text));
            }

            // Print #100, Time & " - Start Saving Entered Information"
            frmWait.InstancePtr.Show();
            frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Building MVR3 Preview";
            frmWait.InstancePtr.Refresh();
            FillRSFinal();
            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.RegistrationType == "NRR")
            {
                FillSaveInfo();
            }

            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                MotorVehicle.CalculateECorrectFee(ref intCorrSpecialtyPlate, ref intCorrInitialPlate);
/*
                if (chkSpecialtyCorrExtraFee.Value == FCCheckBox.ValueSettings.Checked)
                {
	                MotorVehicle.Statics.ECTotal += 1;
                }
*/
            }

            alreadySaving = false;
            // Print #100, Time & " - End Saving Entered Information"
            frmPreview.InstancePtr.IsPreReg = IsPreReg;
            frmDataInput.InstancePtr.Hide();
            frmWait.InstancePtr.Unload();
            frmPreview.InstancePtr.Show(App.MainForm);
        }

        private void mnuProcessRedBook_Click(object sender, System.EventArgs e)
        {
            MotorVehicle.GetRedbookValues();
            cSettingsController set = new cSettingsController();
            txtBase.Text = Strings.Format(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK"), "#,##0");
            txtVin.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("VIN", "TWGNENTY", "REDBOOK"));
            txtMake.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("MAKE", "TWGNENTY", "REDBOOK"));
            string model = fecherFoundation.Strings.Trim(set.GetSettingValue("MODEL", "TWGNENTY", "REDBOOK"));
            txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
			txtYear.Text = fecherFoundation.Strings.Trim(set.GetSettingValue("YEAR", "TWGNENTY", "REDBOOK"));
            txtMilYear.Text =
                FCConvert.ToString(Conversion.Val(
                    fecherFoundation.Strings.Trim(set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK", "0"))));
            if (set.GetSettingValue("VEHICLEINSYSTEM", "TWGNENTY", "REDBOOK", "N") == "Y")
            {
                using (clsDRWrapper rsTemp = new clsDRWrapper())
                {
                    rsTemp.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + txtVin.Text + "'");
                    if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("make", rsTemp.Get_Fields_String("Make"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Year", rsTemp.Get_Fields_Int32("Year"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("model", rsTemp.Get_Fields_String("Model"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("color1", rsTemp.Get_Fields_String("Color1"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("color2", rsTemp.Get_Fields_String("Color2"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Style", rsTemp.Get_Fields_String("Style"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Tires",
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Tires"))));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Axles",
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Axles"))));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Fuel", rsTemp.Get_Fields_String("Fuel"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew",
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("RegisteredWeightNew"))));
                        MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight",
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("NetWeight"))));
                        MotorVehicle.Statics.rsFinal.Set_Fields("DOTNumber", rsTemp.Get_Fields_String("DOTNumber"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber", rsTemp.Get_Fields_String("UnitNumber"));
                        MotorVehicle.Statics.blnMakeVehicleInactive = true;
                    }
                }
            }

            lblCalculate_Click();
            blnCalculateRequired = false;
            txtVin.Focus();
        }

        private void mnuUseTax_Click(object sender, System.EventArgs e)
        {
            string str1 = "";
            
            // vbPorter upgrade warning: blnUserCancel As bool	OnWrite(bool, DialogResult)
            DialogResult blnUserCancel = 0;
            double dblSalesTax = 0;
            MotorVehicle.Statics.UseTaxFlag = true;
            if (MotorVehicle.Statics.blnTownPrintsUseTax)
            {
                // txtReg1LR.Text = "R" Or
                if (txtReg1LR.Text == "R" || (txtReg2LR.Text == "R" && txtReg2ICM.Text != "N") ||
                    (txtReg3LR.Text == "R" && txtReg3ICM.Text != "N"))
                {
                    frmLeaseUseTax.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                }
                else
                {
                    frmUseTax.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                }
            }
            else
            {
                dblSalesTax = FCConvert.ToDouble(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax"));
                object temp = dblSalesTax;
                bool cancel = frmInput.InstancePtr.Init(ref temp, "Input Sales Tax",
                    "Please enter the sales tax for this vehicle.", 1000, false,
                    modGlobalConstants.InputDTypes.idtNumber);
                if (!cancel)
                {
                    blnUserCancel = DialogResult.Yes;
                }

                dblSalesTax = FCConvert.ToDouble(temp);
                if (blnUserCancel == 0)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", dblSalesTax);
                    MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", true);
                    MotorVehicle.Statics.blnPrintUseTax = false;
                    txtSalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax"),
                        "#,##0.00");
                }
                else
                {
                    if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone")))
                    {
                        blnUserCancel =
                            MessageBox.Show(
                                "Do you wish to delete the Use Tax information associated with this registration?",
                                "Delete Use Tax Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (blnUserCancel == DialogResult.Yes)
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
                            MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);
                            txtSalesTax.Text = Strings.Format(0, "#,##0.00");
                        }
                    }
                }

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone"))
                {
	                chkDealerSalesTax.Checked = false;
				}
                return;
            }

            using (clsDRWrapper rs1 = new clsDRWrapper())
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax") != 0)
                {
                    chkSalesTaxExempt.Checked = false;
                    str1 = "SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID);

                    rs1.OpenRecordset(str1);
                    if (rs1.BeginningOfFile() != true && rs1.EndOfFile() != true)
                    {
                        rs1.MoveLast();
                        rs1.MoveFirst();
                        if (FCConvert.ToString(rs1.Get_Fields_String("ExemptCode")) == "C")
                        {
                            txtSalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax"),
                                "#,##0.00");
                        }
                        else if (rs1.Get_Fields_String("ExemptCode") != "")
                        {
                            txtSalesTax.Text = Strings.Format(0, "#,##0.00");
                        }
                        else
                        {
                            txtSalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax"),
                                "#,##0.00");
                        }
                    }
                }
                else
                {
                    txtSalesTax.Text = "0.00";
                    str1 = "SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID);
                    rs1.OpenRecordset(str1);
                    if (rs1.BeginningOfFile() != true && rs1.EndOfFile() != true)
                    {
                        rs1.MoveLast();
                        rs1.MoveFirst();
                        if (FCConvert.ToString(rs1.Get_Fields_String("ExemptCode")) != "")
                        {
                            chkSalesTaxExempt.Checked = true;
                        }
                    }
                }
            }
            
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone"))
            {
	            chkDealerSalesTax.Checked = false;
            }
		}

        public void mnuUseTax_Click()
        {
            mnuUseTax_Click(mnuUseTax, new System.EventArgs());
        }


        private void SetFleetGroupType(FleetGroupType fgType)
        {
            fleetGroupType = fgType;

            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld") == false &&
                    MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") == false)
                {
                    ClearFleetGroup();
                    return;
                }
            }

            if (MotorVehicle.Statics.blnFleetRunOnce && MotorVehicle.Statics.blnChangeToFleet)
            {
                fleetGroupType = FleetGroupType.Fleet;
                return;
            }
            else if (MotorVehicle.Statics.blnChangeToFleet)
            {
                MotorVehicle.Statics.blnFleetRunOnce = true;
            }
            switch (fgType)
            {
                case FleetGroupType.Fleet:
                    {
                        // cg12282017 tromv-1269 state reversed decision - NOT ok to prorate and transfer AP
                        // kk04172017 tromv-1269 Per Sue at BMV Audit - OK to prorate and transfer AP
                        // kk12162016 tromv-1248  Add warning message to inform users - can't use transfer credit AND prorate
                        if (MotorVehicle.Statics.RegistrationType == "NRT")
                        {
                            MessageBox.Show(
                                "Please note - transfer credit is not allowed on a prorated registration except for a motorcycle. The fees and credits will not be prorated.");
                        }

                        if (MotorVehicle.Statics.RegistrationType != "GVW")
                        {
                            if (!FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld")) &&
                                !FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew")))
                            {
                                MotorVehicle.Statics.NewFleet = true;
                                // kk11012016  tromv-1231   Keep expiration date on AP/NRT
                                // kk09032016  tromv-1220   if AP don't use the calc'd expire date and reset txtExpires.Text
                                if (Information.IsDate(txtExpires.Text) &&
                                    (MotorVehicle.Statics.Class != "AP" || MotorVehicle.Statics.RegistrationType == "NRT"))
                                {
                                    datNonFleetExpiration = fecherFoundation.DateAndTime.DateValue(txtExpires.Text);
                                }
                                else if (MotorVehicle.Statics.Class == "AP")
                                {
                                    txtExpires.Text = "";
                                }
                            }

                            if (MotorVehicle.Statics.RegistrationType == "CORR")
                            {
								if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") && 
								    MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ProrateMonth") == MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate").Month)
								{
									MotorVehicle.Statics.NewFleet = true;
								}
                            }
                        }
                        break;
                    }
                case FleetGroupType.None:
                    {
                        ClearFleetGroup();
                        MotorVehicle.Statics.NewFleet = false;
                        txtExpires_Validate(null, new System.ComponentModel.CancelEventArgs(false));
                        break;
                    }
                case FleetGroupType.Group:
                    {
                        MotorVehicle.Statics.NewFleet = false;
                        break;
                    }
            }
        }



        private void txtAddress1_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtAddress1.BackColor);
            txtAddress1.BackColor = ColorTranslator.FromOle(NewBackColor);
            if (MotorVehicle.Statics.blnSuspended)
            {
                txtAddress1.Enabled = false;
            }
        }

        private void txtAddress1_Leave(object sender, System.EventArgs e)
        {
            txtAddress1.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtAddress2_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtAddress2.BackColor);
            txtAddress2.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtAddress2_Leave(object sender, System.EventArgs e)
        {
            txtAddress2.BackColor = ColorTranslator.FromOle(OldColor);
            if (!MotorVehicle.Statics.NewToTheSystem)
            {
                if (!blnVerifyFlag && !MotorVehicle.Statics.PreviewFlag && !blnFeesFlag &&
                    !MotorVehicle.Statics.CTAFlag && !MotorVehicle.Statics.UseTaxFlag &&
                    !MotorVehicle.Statics.OpenFlag && MotorVehicle.Statics.RegistrationType != "CORR")
                {
                    if (txtCity.Text != "" && txtState.Text != "" && txtZip.Text != "" && txtCountry.Text != "")
                    {
                        txtAddress2.BackColor = ColorTranslator.FromOle(OldColor);
                        if (txtLegalResStreet.Text == "")
                        {
                            txtLegalResStreet.Focus();
                        }
                        else
                        {
                            if (Conversion.Val(txtMonthCharge.Text) > 0 || Conversion.Val(txtMonthNoCharge.Text) > 0)
                            {
                                txtMStickerNumber.Focus();
                            }
                            else
                            {
                                if (txtYStickerNumber.Enabled == true)
                                {
                                    txtYStickerNumber.Focus();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void txtAxles_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyAxles() == false)
            {
                e.Cancel = true;
                // Bp = Beep(1000, 250)
            }
        }

        private void txtBalance_Enter(object sender, System.EventArgs e)
        {
            // ToggleNumLock True
        }

        private void txtBase_Change(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
        }

        private void txtBase_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        private void txtCity_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtCity.BackColor);
            txtCity.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtCity_Leave(object sender, System.EventArgs e)
        {
            txtCity.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtCity_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyCity() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtColor1_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtColor1.BackColor);
            txtColor1.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtColor1_Leave(object sender, System.EventArgs e)
        {
            txtColor1.BackColor = ColorTranslator.FromOle(OldColor);
            // kk01062016 tromv-1129  Fix Shift-Tab behavior so we don't end up back on txtStyle - BackwardsFlag is never set to True so drop it
            if (ActiveControl.GetName() == "txtColor2")
            {
                // If Not blnVerifyFlag And Not PreviewFlag And Not blnFeesFlag And Not CTAFlag And Not UseTaxFlag And Not OpenFlag And Not BackwardsFlag Then
                if (!blnVerifyFlag && !MotorVehicle.Statics.PreviewFlag && !blnFeesFlag &&
                    !MotorVehicle.Statics.CTAFlag && !MotorVehicle.Statics.UseTaxFlag && !MotorVehicle.Statics.OpenFlag)
                {
                    if (txtColor2.Text != "")
                    {
                        txtColor2.BackColor = ColorTranslator.FromOle(OldColor);
                        txtStyle.Focus();
                    }

                    // Else
                    // BackwardsFlag = False
                }
            }
        }

        private void txtColor2_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtColor2.BackColor);
            txtColor2.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtColor2_Leave(object sender, System.EventArgs e)
        {
            txtColor2.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtCreditNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Tab))
            {
                // backspace  & tab
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
            {
                // numeric
            }
            else if (KeyAscii == Keys.Return)
            {
                // enter
            }
            else
            {
                // Bp = Beep(1000, 250)
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtCTANumber_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if (KeyAscii == 13)
            {
                txtPreviousTitle.Focus();
            }

            e.KeyChar = Strings.Chr(KeyAscii);
        }

        private void txtCTANumber_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyCTA() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }

            if (!String.IsNullOrWhiteSpace(txtCTANumber.Text))
            {
	            txtDoubleCTANumber.Enabled = true;
            }
            else
            {
	            txtDoubleCTANumber.Text = "";
	            txtDoubleCTANumber.Enabled = false;
            }
        }

        private void txtDOTNumber_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyDOT() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtEffectiveDate_Enter(object sender, System.EventArgs e)
        {
            if (MotorVehicle.Statics.RegistrationType != "CORR")
            {
                txtEffectiveDate.Locked = true;
            }
            else
            {
                txtEffectiveDate.Locked = false;
            }
        }

        //private void txtExciseTaxDate_Enter(object sender, System.EventArgs e)
        //{
        //	OldColor = ColorTranslator.ToOle(txtExciseTaxDate.BackColor);
        //	txtExciseTaxDate.BackColor = ColorTranslator.FromOle(NewBackColor);
        //}

        //private void txtExciseTaxDate_Leave(object sender, System.EventArgs e)
        //{
        //	txtExciseTaxDate.BackColor = ColorTranslator.FromOle(OldColor);
        //}

        private void txtReg1ICM_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii != Keys.I && KeyAscii != Keys.C && KeyAscii != Keys.M)
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtReg3DOB_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg3DOB.BackColor);
            txtReg3DOB.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtReg3DOB_Leave(object sender, System.EventArgs e)
        {
            txtReg3DOB.BackColor = ColorTranslator.FromOle(OldColor);
            if (!Information.IsDate(txtReg3DOB.Text))
            {
                txtReg3DOB.Text = "";
            }
        }

        private void txtSalesTax_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int ans;
            if (!Information.IsNumeric(txtSalesTax.Text))
            {
                txtSalesTax.Text = "0.00";
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone")))
            {
                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax")) !=
                    Conversion.Val(txtSalesTax.Text))
                {
                    MessageBox.Show("You may only change the amount on the Use Tax form.", "Unable To Change",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtSalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax"),
                        "#,##0.00");
                }
            }
        }

        private void txtExpires_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            // vbPorter upgrade warning: NM As int	OnWrite(long)
            int NM = 0;
            if (!Information.IsDate(txtExpires.Text) && !MotorVehicle.Statics.ETO)
            {
                e.Cancel = true;
                return;
            }
            else if (Information.IsDate(txtExpires.Text))
            {
                if (MotorVehicle.Statics.FixedMonth && MotorVehicle.Statics.gintFixedMonthMonth !=
                    FCConvert.ToDateTime(txtExpires.Text).Month)
                {
                    MessageBox.Show("Invalid expiration date", "Invalid Date", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    txtExpires.Text = Strings.Format(datOriginalExpirationDate, "MM/dd/yyyy");
                    e.Cancel = true;
                    return;
                }
            }

            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (!blnDontCheckDate)
                {
                    if (blnExpirationDateChanged)
                    {
                        if (fecherFoundation.DateAndTime.DateValue(txtExpires.Text).ToOADate() !=
                            datOriginalExpirationDate.ToOADate())
                        {
                            modGlobalFunctions.AddCYAEntry_6("MV", "Changed expiration date from system generated date",
                                "Original Date: " + FCConvert.ToString(datOriginalExpirationDate),
                                "New Date: " + txtExpires.Text);
                        }
                    }
                    else
                    {
                        if (!fecherFoundation.FCUtils.IsNull(datOriginalExpirationDate) &&
                            datOriginalExpirationDate.ToOADate() != fecherFoundation.DateAndTime
                                .DateValue(FCConvert.ToString(0)).ToOADate())
                        {
                            if (fecherFoundation.DateAndTime.DateValue(txtExpires.Text).ToOADate() !=
                                datOriginalExpirationDate.ToOADate())
                            {
                                ans = MessageBox.Show(
                                    "There are only a few situations when you should change the expiration date from what the system generates.  Are you sure you wish to do this?",
                                    "Change Expiration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (ans == DialogResult.Yes)
                                {
                                    blnExpirationDateChanged = true;
                                    modGlobalFunctions.AddCYAEntry_6("MV",
                                        "Changed expiration date from system generated date",
                                        "Original Date: " + FCConvert.ToString(datOriginalExpirationDate),
                                        "New Date: " + txtExpires.Text);
                                }
                                else
                                {
                                    e.Cancel = true;
                                }
                            }
                        }
                    }
                }

                // DJW@4/30/2010 Added section to change sticker year and month if expiration date is changed
                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "CI")
                {
                    lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                    lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                }

                return;
            }

            if (MotorVehicle.Statics.ETO)
            {
                // kk04142016 tromv-1145  Add Class <> "AP"
                if (MotorVehicle.Statics.RegistrationType == "NRR" && !MotorVehicle.Statics.NewFleet &&
                    !MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) &&
                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP")
                {
                    txtExpires.Text = "";
                }

                return;
            }

            if (fleetGroupType == FleetGroupType.Fleet && Conversion.Val(txtFleetNumber.Text) != 0)
            {
                txtExpires.Text = MotorVehicle.Statics.FleetExpDate;
                txtExpires.BackColor = ColorTranslator.FromOle(OldColor);
            }
            else
            {
                if (CalculateEffectiveDate() == false)
                {
                    Recalculate();
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "CI")
                    {
                        MessageBox.Show("This is an Invalid Expiration Date.", "Invalid Date", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        txtExpires.Text = Strings.Format(datOriginalExpirationDate, "MM/dd/yyyy");
                        CalculateEffectiveDate();
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    Recalculate();
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "CI")
                    {
                        lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                        lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                    }

                    txtExpires.BackColor = ColorTranslator.FromOle(OldColor);
                }
            }

            if (!blnDontCheckDate)
            {
                if (blnExpirationDateChanged)
                {
                    if (fecherFoundation.DateAndTime.DateValue(txtExpires.Text).ToOADate() !=
                        datOriginalExpirationDate.ToOADate())
                    {
                        modGlobalFunctions.AddCYAEntry_6("MV", "Changed expiration date from system generated date",
                            "Original Date: " + FCConvert.ToString(datOriginalExpirationDate),
                            "New Date: " + txtExpires.Text);
                    }
                }
                else
                {
                    if (!fecherFoundation.FCUtils.IsNull(datOriginalExpirationDate))
                    {
                        if (fecherFoundation.DateAndTime.DateValue(txtExpires.Text).ToOADate() !=
                            datOriginalExpirationDate.ToOADate())
                        {
                            ans = MessageBox.Show(
                                "There are only a few situations when you should change the expiration date from what the system generates.  Are you sure you wish to do this?",
                                "Change Expiration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                blnExpirationDateChanged = true;
                                modGlobalFunctions.AddCYAEntry_6("MV",
                                    "Changed expiration date from system generated date",
                                    "Original Date: " + FCConvert.ToString(datOriginalExpirationDate),
                                    "New Date: " + txtExpires.Text);
                                // added to set correct checkboxes when expiration date is changed
                                NM = fecherFoundation.DateAndTime.DateDiff("m",
                                    fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text),
                                    fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                                // ******* rsfinalcompare.fields("EffectiveDate")  used to be xxdate
                                chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Unchecked;
                                chkHalfRateState.CheckState = Wisej.Web.CheckState.Unchecked;
                                chk2Year.CheckState = Wisej.Web.CheckState.Unchecked;
                                if (NM < 4 && MotorVehicle.Statics.RegistrationType != "CORR" &&
                                    (!MotorVehicle.Statics.NewFleet || fleetGroupType == FleetGroupType.Group) &&
                                    !MotorVehicle.IsProratedMotorcycle(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                        MotorVehicle.Statics.datMotorCycleEffective,
                                        ref MotorVehicle.Statics.datMotorCycleExpires,
                                        MotorVehicle.Statics.RegistrationType))
                                {
                                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
									if (HalfRateAllowed(MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass))
									{
	                                    chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
                                    }
                                }
                                else if (NM < 6 &&
                                         (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                          "FM" ||
                                          FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                          "AF") && (!MotorVehicle.Statics.NewFleet || fleetGroupType == FleetGroupType.Group) &&
                                         !MotorVehicle.IsProratedMotorcycle(
                                             MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                             MotorVehicle.Statics.datMotorCycleEffective,
                                             ref MotorVehicle.Statics.datMotorCycleExpires,
                                             MotorVehicle.Statics.RegistrationType))
                                {
                                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
                                    chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
                                }
                                else if (MotorVehicle.Statics.RegistrationType == "RRR" ||
                                         MotorVehicle.Statics.RegistrationType == "NRR" ||
                                         MotorVehicle.Statics.RegistrationType == "NRT" ||
                                         MotorVehicle.Statics.RegistrationType == "RRT" ||
                                         MotorVehicle.Statics.RegistrationType == "CORR")
                                {
                                    if (chk2Year.Enabled ||
                                        (MotorVehicle.Statics.RegistrationType == "NRT" ||
                                         MotorVehicle.Statics.RegistrationType == "RRT"))
                                    {
                                        if (NM > 12)
                                        {
                                            chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                            Recalculate();
                                        }

                                        if (chk2Year.CheckState == Wisej.Web.CheckState.Checked &&
                                            MotorVehicle.Statics.RegistrationType != "NRT" &&
                                            MotorVehicle.Statics.RegistrationType != "RRT")
                                        {
                                            if (NM > 12 && NM < 16)
                                            {
                                                chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
                                                chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void txtFees_DblClick(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired == true)
                lblCalculate_Click();
            blnFeesFlag = true;
            frmFeesInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            //FC:FINAL:MSH - i.issue #1804: update values in fields after closing form (frmFeesInfo will be disposed after hiding and all data will be missed)
            frmFeesInfoStaticsValues.InstancePtr.UpdateValuesFromStorage();
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                lblCalculate_Click();
            }
            else
            {
                FillFeesScreen();
                FillTotalDue();
            }
        }

        public bool VerifyFleetInfo()
        {
            // VB6 Bad Scope Dim:
            string datechecker = "";
            clsDRWrapper rsFT = new clsDRWrapper();
            // vbPorter upgrade warning: xdate As DateTime	OnWrite(string, DateTime)
            DateTime xdate;
            string tempdate = "";
            clsDRWrapper rsClassInfo = new clsDRWrapper();
            clsDRWrapper rsOverride = new clsDRWrapper();
            int intHolder1;
            int intHolder2;
            string strMI = "";
            bool isVerified = true;

            try
            {
                blnDontCheckDate = true;
                if (txtFleetNumber.Text == "A")
                {
                    MotorVehicle.Statics.bolFromDataInput = true;
                    frmFleetMaster.InstancePtr.Unload();
                    frmFleetMaster.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    MotorVehicle.Statics.bolFromDataInput = false;
                }

                if (Conversion.Val(txtFleetNumber.Text) != 0)
                {
                    rsFT.OpenRecordset(
                        "SELECT * FROM FleetMaster WHERE Deleted <> 1 AND Type <> 'L' AND ExpiryMonth = 99 and fleetnumber = " +
                        txtFleetNumber.Text.ToIntegerValue(), "MotorVehicle");
                    if (!rsFT.EndOfFile())
                    {
                        fleetGroupType = FleetGroupType.Group;
                    }
                    else
                    {
                        fleetGroupType = FleetGroupType.Fleet;
                    }

                    if (fleetGroupType != FleetGroupType.Group)
                    {
                        rsFT.OpenRecordset(
                            "SELECT * FROM FleetMaster WHERE Deleted <> 1 AND Type <> 'L' AND FleetNumber = " +
                            FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " AND ExpiryMonth <> 99");
                        rsClassInfo.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" +
                                                  MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") +
                                                  "' AND SystemCode = '" +
                                                  MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
                        if (rsClassInfo.EndOfFile() != true && rsClassInfo.BeginningOfFile() != true)
                        {
                            if (FCConvert.ToString(rsClassInfo.Get_Fields_String("RenewalDate")) == "F")
                            {
                                if (Conversion.Val(rsFT.Get_Fields_Int32("ExpiryMonth")) !=
                                    MotorVehicle.Statics.gintFixedMonthMonth)
                                {
                                    MessageBox.Show(
                                        "You may not add a vehicle with a fixed renewal date to a fleet that has an expiration month other than the required expiration month",
                                        "Invalid Expiration Month", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                    txtFleetNumber.Text = "";
                                    SetFleetGroupType(FleetGroupType.None);
                                    blnDontCheckDate = false;
                                    return false;
                                }
                            }
                        }
                    }

                    if (rsFT.EndOfFile() != true && rsFT.BeginningOfFile() != true)
                    {
                        rsFT.MoveLast();
                        rsFT.MoveFirst();
                        if (!rsFT.FindFirstRecord("FleetNumber", Conversion.Val(txtFleetNumber.Text)))
                        {
                            isVerified = false;
                            if (fleetGroupType == FleetGroupType.Group)
                            {
                                MessageBox.Show(
                                    "There was no match found for Group Master Number " +
                                    FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Invalid Group",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                MessageBox.Show(
                                    "There was no match found for Fleet Master Number " +
                                    FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Invalid Fleet",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        else
                        {
                            if (fleetGroupType != FleetGroupType.Group)
                            {
                                // 
                                if (!Information.IsDate(datNonFleetExpiration) ||
                                    DateTime.Today.ToOADate() > datNonFleetExpiration.ToOADate())
                                {
                                    if (Information.IsDate(txtExpires.Text))
                                    {
                                        datNonFleetExpiration = fecherFoundation.DateAndTime.DateValue(txtExpires.Text);
                                    }
                                }

                                if (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.ETO &&
                                    (fecherFoundation.Strings.Trim(txtExpires.Text) == "" || txtExpires.IsEmpty))
                                {
                                    xdate = FCConvert.ToDateTime(Strings.Format(
                                        fecherFoundation.DateAndTime.DateAdd("m", 1, DateTime.Now), "MM/dd/yyyy"));
                                    xdate = FCConvert.ToDateTime(
                                        Strings.Format(fecherFoundation.DateAndTime.DateAdd("yyyy", 1, xdate),
                                            "MM/dd/yyyy"));
                                    xdate = fecherFoundation.DateAndTime.DateAdd("d", -(DateTime.Now.Day), xdate);
                                    tempdate = Strings.Format(xdate, "MM/dd/yyyy");
                                    if (DateTime.Today.Month >= Conversion.Val(rsFT.Get_Fields_Int32("ExpiryMonth")))
                                    {
                                        datechecker =
                                            Strings.Format(
                                                FindLastDay(rsFT.Get_Fields_Int32("ExpiryMonth") + "/01/" +
                                                            Strings.Mid(tempdate, 7, 4)), "MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        datechecker =
                                            Strings.Format(
                                                rsFT.Get_Fields_Int32("ExpiryMonth") + "/01/" +
                                                FCConvert.ToString(DateTime.Today.Year), "MM/dd/yyyy");
                                        datechecker =
                                            Strings.Format(
                                                fecherFoundation.DateAndTime.DateAdd("m", 1,
                                                    fecherFoundation.DateAndTime.DateValue(datechecker)), "MM/dd/yyyy");
                                        datechecker =
                                            Strings.Format(
                                                fecherFoundation.DateAndTime.DateAdd("d", -1,
                                                    fecherFoundation.DateAndTime.DateValue(datechecker)), "MM/dd/yyyy");
                                    }
                                }
                                else
                                {
                                    datechecker =
                                        Strings.Format(
                                            FindLastDay(rsFT.Get_Fields_Int32("ExpiryMonth") + "/01/" +
                                                        Strings.Mid(txtExpires.Text, 7, 4)), "MM/dd/yyyy");
                                    if (MotorVehicle.Statics.blnChangeToFleet &&
                                        FCConvert.ToDateTime(datechecker).Month > DateTime.Today.Month &&
                                        FCConvert.ToDateTime(datechecker).Year > DateTime.Today.Year)
                                    {
                                        datechecker =
                                            Strings.Format(
                                                fecherFoundation.DateAndTime.DateAdd("yyyy", -1,
                                                    fecherFoundation.DateAndTime.DateValue(datechecker)), "MM/dd/yyyy");
                                    }
                                }

                                if (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.ETO)
                                {
                                    if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                            fecherFoundation.DateAndTime.DateValue(datechecker)) > 12)
                                    {
                                        ShowWarningBox(
                                            "Adding this vehicle to this fleet at this time would require a registration of more than 12 months.  Unable to Process",
                                            "Invalid Registration Period");
                                        ClearFleetGroup();
                                        blnDontCheckDate = false;
                                        return false;
                                    }
                                    else
                                    {
                                        txtExpires.Text = datechecker;
                                    }

                                    // Dave 6/30/04
                                }
                                else if (fecherFoundation.DateAndTime.DateValue(datechecker).ToOADate() >
                                         datNonFleetExpiration.ToOADate())
                                {
                                    // CDate(txtExpires.Text) Then
                                    if (Strings.Left(datechecker, 6) == "02/29/")
                                    {
                                        datechecker =
                                            "02/28/" + fecherFoundation.Strings.Trim(
                                                Conversion.Str(FCConvert.ToDateTime(datechecker).Year - 1));
                                    }
                                    else
                                    {
                                        datechecker = Strings.Left(datechecker, 6) +
                                                      fecherFoundation.Strings.Trim(
                                                          Conversion.Str(FCConvert.ToDateTime(datechecker).Year - 1));
                                    }

                                    if (fecherFoundation.DateAndTime.DateValue(datechecker).ToOADate() <
                                        DateTime.Today.ToOADate())
                                    {
                                        ShowWarningBox(
                                            "Adding this vehicle to this fleet at this time would require a registration of more than 12 months.  Unable to Process",
                                            "Invalid Registration Period");
                                        ClearFleetGroup();
                                        blnDontCheckDate = false;
                                        return false;
                                    }
                                    else
                                    {
                                        txtExpires.Text = datechecker;
                                    }
                                }
                                else if (fecherFoundation.DateAndTime.DateValue(datechecker).ToOADate() <
                                         DateTime.Today.ToOADate())
                                {
                                    ShowWarningBox(
                                        "Adding this vehicle to this fleet at this time would require a registration of more than 12 months.  Unable to Process",
                                        "Invalid Registration Period");
                                    ClearFleetGroup();
                                    blnDontCheckDate = false;
                                    return false;
                                }
                                else
                                {
                                    if (MotorVehicle.Statics.blnChangeToFleet)
                                    {
                                        txtExpires.Text = Strings.Format(datechecker, "MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        txtExpires.Text =
                                            Strings.Format(
                                                FindLastDay(rsFT.Get_Fields_Int32("ExpiryMonth") + "/01/" +
                                                            Strings.Mid(txtExpires.Text, 7, 4)), "MM/dd/yyyy");
                                    }
                                }
                            }

                            txtExpires.BackColor = txtEffectiveDate.BackColor;
                            MotorVehicle.Statics.FleetExpDate = txtExpires.Text;
                            if (fleetGroupType == FleetGroupType.Group)
                            {
                                // do nothing
                            }
                            else
                            {
                                if (FCConvert.ToDateTime(datechecker).Month < 10)
                                {
                                    lblNumberofMonth.Text =
                                        "0" + fecherFoundation.Strings.Trim(
                                            Conversion.Str(FCConvert.ToDateTime(datechecker).Month));
                                }
                                else
                                {
                                    lblNumberofMonth.Text =
                                        fecherFoundation.Strings.Trim(
                                            Conversion.Str(FCConvert.ToDateTime(datechecker).Month));
                                }

                                if (FCConvert.ToDateTime(datechecker).Year - 2000 < 10)
                                {
                                    lblNumberofYear.Text =
                                        "0" + fecherFoundation.Strings.Trim(
                                            Conversion.Str(FCConvert.ToDateTime(datechecker).Year - 2000));
                                }
                                else
                                {
                                    lblNumberofYear.Text =
                                        fecherFoundation.Strings.Trim(
                                            Conversion.Str(FCConvert.ToDateTime(datechecker).Year - 2000));
                                }

                                // 1/14/05 Dave took out because we dont need month stickers just becuase we need year stickers
                                if (Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("ExpireDate")))
                                {
                                    if (FCConvert.ToDateTime(txtExpires.Text).Month != MotorVehicle.Statics
                                            .rsFinalCompare
                                            .Get_Fields_DateTime("ExpireDate").Month)
                                    {
                                        txtMonthNoCharge.Text = txtYearNoCharge.Text;
                                        txtMonthCharge.Text = txtYearCharge.Text;
                                        txtMStickerNumber.Enabled = true;
                                    }
                                }
                                else
                                {
                                    txtMonthNoCharge.Text = txtYearNoCharge.Text;
                                    txtMonthCharge.Text = txtYearCharge.Text;
                                    txtMStickerNumber.Enabled = true;
                                }

                                txtMStickerNumber.Text = "";
                                txtYStickerNumber.Text = "";
                                // 12/1/2004 Took out because it was reverting vehicle info they changed before declaring this vehicle a fleet vehicle
                                // FillVehicleInfo rsFinal
                            }

                            lblFleet.Text = MotorVehicle.GetPartyNameMiddleInitial(rsFT.Get_Fields_Int32("PartyID1"));

                            if (!blnLoadingForm) // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                            {
                                if (FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode1")) == "")
                                {
                                    txtReg1ICM.Text = "I";
                                }
                                else
                                {
                                    txtReg1ICM.Text = FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode1"));
                                }

                                if (FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode2")) == "")
                                {
                                    txtReg2ICM.Text = "N";
                                }
                                else
                                {
                                    txtReg2ICM.Text = FCConvert.ToString(rsFT.Get_Fields_String("Ownercode2"));
                                }

                                if (FCConvert.ToString(rsFT.Get_Fields_String("OwnerCode3")) == "")
                                {
                                    txtReg3ICM.Text = "N";
                                }
                                else
                                {
                                    txtReg3ICM.Text = FCConvert.ToString(rsFT.Get_Fields_String("Ownercode3"));
                                }

                                if ((fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                                     "STAAB AGENCY") &&
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(rsFT.Get_Fields_String("State"))) !=
                                    "ME")
                                {
                                    rsOverride.OpenRecordset("SELECT * FROM GroupAddressOverride");
                                    if (rsOverride.EndOfFile() != true && rsOverride.BeginningOfFile() != true)
                                    {
                                        if (!MotorVehicle.Statics.blnSuspended)
                                        {
                                            if (!fecherFoundation.FCUtils.IsNull(
                                                rsOverride.Get_Fields_String("Address")))
                                                txtAddress1.Text =
                                                    fecherFoundation.Strings.Trim(
                                                        FCConvert.ToString(rsOverride.Get_Fields_String("Address")));
                                        }
                                        else
                                        {
                                            txtAddress1.Text = "REGISTRATION SUSPENDED";
                                        }

                                        if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("Address2")))
                                            txtAddress2.Text =
                                                fecherFoundation.Strings.Trim(
                                                    FCConvert.ToString(rsOverride.Get_Fields_String("Address2")));
                                        if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("City")))
                                            txtCity.Text =
                                                fecherFoundation.Strings.Trim(
                                                    FCConvert.ToString(rsOverride.Get_Fields_String("City")));
                                        if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("State")))
                                            txtState.Text =
                                                fecherFoundation.Strings.Trim(
                                                    FCConvert.ToString(rsOverride.Get_Fields_String("State")));
                                        if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("Zip")))
                                        {
                                            if (FCConvert.ToString(rsOverride.Get_Fields_String("Zip")).Length < 5)
                                            {
                                                txtZip.Text = "0" + rsOverride.Get_Fields_String("Zip");
                                            }
                                            else
                                            {
                                                txtZip.Text = FCConvert.ToString(rsOverride.Get_Fields_String("Zip"));
                                            }
                                        }

                                        if (!fecherFoundation.FCUtils.IsNull(rsOverride.Get_Fields_String("Country")))
                                            txtCountry.Text =
                                                fecherFoundation.Strings
                                                    .Trim(FCConvert.ToString(rsOverride.Get_Fields_String("Country")))
                                                    .Length <= 2
                                                    ? fecherFoundation.Strings.Trim(
                                                        FCConvert.ToString(rsOverride.Get_Fields_String("Country")))
                                                    : "";
                                    }
                                }
                                else
                                {
                                    if (!MotorVehicle.Statics.blnSuspended)
                                    {
                                        if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Address")))
                                            txtAddress1.Text =
                                                fecherFoundation.Strings.Trim(
                                                    FCConvert.ToString(rsFT.Get_Fields_String("Address")));
                                    }
                                    else
                                    {
                                        txtAddress1.Text = "REGISTRATION SUSPENDED";
                                    }

                                    if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Address2")))
                                        txtAddress2.Text =
                                            fecherFoundation.Strings.Trim(
                                                FCConvert.ToString(rsFT.Get_Fields_String("Address2")));
                                    if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("City")))
                                        txtCity.Text =
                                            fecherFoundation.Strings.Trim(
                                                FCConvert.ToString(rsFT.Get_Fields_String("City")));
                                    if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("State")))
                                        txtState.Text =
                                            fecherFoundation.Strings.Trim(
                                                FCConvert.ToString(rsFT.Get_Fields_String("State")));
                                    if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Zip")))
                                    {
                                        if (FCConvert.ToString(rsFT.Get_Fields_String("Zip")).Length < 5)
                                        {
                                            txtZip.Text = "0" + rsFT.Get_Fields_String("Zip");
                                        }
                                        else
                                        {
                                            txtZip.Text = FCConvert.ToString(rsFT.Get_Fields_String("Zip"));
                                        }
                                    }

                                    if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Country")))
                                        txtCountry.Text =
                                            fecherFoundation.Strings
                                                .Trim(FCConvert.ToString(rsFT.Get_Fields_String("Country"))).Length <= 2
                                                ? fecherFoundation.Strings.Trim(
                                                    FCConvert.ToString(rsFT.Get_Fields_String("Country")))
                                                : "";
                                }
                            } // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                            txtEMail.Text = FCConvert.ToString(rsFT.Get_Fields_String("EmailAddress"));
                            txtEMail.Enabled = false;
                            chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
                            chkOverrideFleetEmail.Visible = true;
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Comment"))) !=
                                "")
                            {
                                imgFleetComment.Visible = true;
                            }
                            else
                            {
                                imgFleetComment.Visible = false;
                            }

                            cmdComment.Enabled = true;
                            if (MotorVehicle.Statics.ETO)
                            {
                                if (fecherFoundation.Strings.Trim(txtEffectiveDate.Text) != "" &&
                                    !txtEffectiveDate.IsEmpty)
                                {
                                    // do nothing
                                }
                                else
                                {
                                    if (MotorVehicle.Statics.RegisteringFleet)
                                    {
                                        txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.PreEffectiveDate,
                                            "MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                                    }
                                }
                            }

                            if (!blnLoadingForm) // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                            {
                                txtReg1PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID1"));
                                GetPartyInfo(rsFT.Get_Fields_Int32("PartyID1"), 1, MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("FleetNumber") == txtFleetNumber.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
                                if (Conversion.Val(rsFT.Get_Fields_Int32("PartyID2")) != 0)
                                {
                                    txtReg2PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID2"));
                                    GetPartyInfo(rsFT.Get_Fields_Int32("PartyID2"), 2, MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("FleetNumber") == txtFleetNumber.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
                                }

                                if (Conversion.Val(rsFT.Get_Fields_Int32("PartyID3")) != 0)
                                {
                                    txtReg3PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID3"));
                                    GetPartyInfo(rsFT.Get_Fields_Int32("PartyID3"), 3, MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("FleetNumber") == txtFleetNumber.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
                                }

                                txtTaxIDNumber.Text = FCConvert.ToString(rsFT.Get_Fields_String("TaxID"));
                                string vbPorterVar = txtReg1ICM.Text;
                                if (vbPorterVar == "I")
                                {
                                    txtReg1DOB.Text = Strings.Format(rsFT.Get_Fields("DOB1"), "MM/dd/yyyy");
                                }
                                else
                                {
                                    txtReg1DOB.Text = "";
                                }

                                string vbPorterVar1 = txtReg2ICM.Text;
                                if (vbPorterVar1 == "I")
                                {
                                    txtReg2DOB.Text = Strings.Format(rsFT.Get_Fields("DOB2"), "MM/dd/yyyy");
                                }
                                else
                                {
                                    txtReg2DOB.Text = "";
                                }

                                string vbPorterVar2 = txtReg3ICM.Text;
                                if (vbPorterVar2 == "I")
                                {
                                    txtReg3DOB.Text = Strings.Format(rsFT.Get_Fields("DOB3"), "MM/dd/yyyy");
                                }
                                else
                                {
                                    txtReg3DOB.Text = "";
                                }
                            } // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


                            if ((MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("FleetNew") == true ||
                                 MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("FleetOld") == true) ||
                                (FCConvert.ToString(
                                     MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("FleetNumber")) ==
                                 txtFleetNumber.Text))
                            {
                                // do nothing
                            }
                            else
                            {
                                MotorVehicle.Statics.NewFleet = true;
                            }

                            // cg12282017 tromv-1269 state reversed decision - NOT ok to prorate and transfer AP
                            // kk04172017 tromv-1269 Per Sue at BMV Audit - Allow transfer and prorate on AP
                            // kk12162016 tromv-1248  Don't recalc and reset half-rate flags if NRT and in Fleet
                            if (!(fleetGroupType == FleetGroupType.Fleet &&
                                  MotorVehicle.Statics.RegistrationType == "NRT"))
                            {
                                if (fleetGroupType == FleetGroupType.Fleet)
                                {
                                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Unchecked;
                                    chkHalfRateState.CheckState = Wisej.Web.CheckState.Unchecked;
                                    lblTownCreditHR.Visible = false;
                                    lblExciseTaxHR.Visible = false;
                                    lblLocalSubHR.Visible = false;
                                    lblLocalBalanceHR.Visible = false;
                                    lblStateRateHR.Visible = false;
                                    lblStateCreditHR.Visible = false;
                                    lblStateFeesHR.Visible = false;
                                    lblTownCreditPR.Visible = false;
                                    lblExciseTaxPR.Visible = false;
                                    lblLocalSubPR.Visible = false;
                                    lblLocalBalancePR.Visible = false;
                                    lblStateRatePR.Visible = false;
                                    lblStateCreditPR.Visible = false;
                                    lblStateFeesPR.Visible = false;
                                    MotorVehicle.Statics.StateHalfRate = false;
                                    MotorVehicle.Statics.LocalHalfRate = false;
                                }

                                Recalculate();
                            }

                            frmDataInput.InstancePtr.Refresh();
                        }
                    }
                    else
                    {
                        chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
                        chkOverrideFleetEmail.Visible = false;
                        txtEMail.Enabled = true;
                        imgFleetComment.Visible = false;
                        cmdComment.Enabled = false;
                        MessageBox.Show("This is not a valid group or fleet.", "No Groups Or Fleets",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        isVerified = false;
                    }
                }
                else
                {
                    chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
                    chkOverrideFleetEmail.Visible = false;
                    txtEMail.Text = "";
                    txtEMail.Enabled = true;
                    imgFleetComment.Visible = false;
                    cmdComment.Enabled = false;
                    SetFleetGroupType(FleetGroupType.None);
                }

                blnDontCheckDate = false;
                return isVerified;
            }
            finally
            {
                rsFT.Dispose();
                rsClassInfo.Dispose();
                rsOverride.Dispose();
            }
        }
        private void txtFleetNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!VerifyFleetInfo())
            {
                e.Cancel = true;
            }
        }

        public void txtFleetNumber_Validate(bool Cancel)
        {
            txtFleetNumber_Validating(txtFleetNumber, new System.ComponentModel.CancelEventArgs(Cancel));
        }

        private void txtFuel_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            txtFuel.Text = fecherFoundation.Strings.UCase(txtFuel.Text);
            if (VerifyFuel() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtMileage_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtMileage.BackColor);
            txtMileage.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtMileage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Tab))
            {
                // backspace  & tab
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
            {
                // numeric
            }
            else if (KeyAscii == Keys.Return)
            {
                // enter
            }
            else
            {
                // Bp = Beep(1000, 250)
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMileage_Leave(object sender, System.EventArgs e)
        {
            //FC:FINAL:DDU:#i1803 - add flag to don't enter in Leave event
            if (!formClosing)
            {
                if (!MotorVehicle.Statics.rsFinal.IsntAnything())
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("Odometer",
                        FCConvert.ToString(Conversion.Val(txtMileage.Text)));
                }

                txtMileage.BackColor = ColorTranslator.FromOle(OldColor);
            }

            formClosing = false;
        }

        private void txtMileage_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtMileage.Text == "")
                return;
            if (txtUserID.Text == "")
            {
                txtMileage.BackColor = ColorTranslator.FromOle(OldColor);
                return;
            }

            if (VerifyMileage() == false)
            {
                txtMileage.Text = "";
                e.Cancel = true;
            }

            if (e.Cancel == false)
                txtMileage.BackColor = ColorTranslator.FromOle(OldColor);
            /*? On Error GoTo 0 */
        }

        private void txtMonthCharge_TextChanged(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
        }

        private void txtMonthCharge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Back)
            {
                // backspace
                txtMStickerNumber.Enabled = false;
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D2)
            {
                // number
                txtMStickerNumber.Enabled = true;
                txtMStickerNumber.Enabled = true;
            }
            else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.NumPad3))
            {
                txtMonthCharge.Text = "";
                txtMStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }
            else
            {
                txtMStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMonthNoCharge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Back)
            {
                // backspace
                txtMStickerNumber.Enabled = false;
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D2)
            {
                // number
                txtMStickerNumber.Enabled = true;
                txtMStickerNumber.Enabled = true;
            }
            else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.NumPad3))
            {
                txtMonthNoCharge.Text = "";
                txtMStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }
            else
            {
                txtMStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMonthNoCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (CheckNumberOfStickers(FCUtils.iDiv(Conversion.Val(txtMonthCharge.Text), 1),
                    FCUtils.iDiv(Conversion.Val(txtMonthNoCharge.Text), 1)) == false)
            {
                txtMonthNoCharge.Text = "";
                e.Cancel = true;
            }
        }

        private void txtMStickerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Back)
            {
                // backspace
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
            {
                // number
            }
            else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.NumPad3))
            {
                txtMStickerNumber.Text = "";
                KeyAscii = (Keys)0;
            }
            else
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtMStickerNumber_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (Shift == 3 && KeyCode == Keys.F9)
            {
                txtMStickerNumber.Text = "";
                MotorVehicle.Statics.F9 = true;
                frmInventory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            }
        }

        public void txtMStickerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MotorVehicle.Statics.ETO)
            {
                if (txtMStickerNumber.Text != "")
                {
                    // Bp = Beep(1000, 250)
                    e.Cancel = true;
                    ShowWarningBox("You may not issue stickers on an Excise Tax Only Transaction.",
                        "Unable to Issue Stickers");
                    txtMonthCharge.Text = "";
                    txtMonthNoCharge.Text = "";
                    txtYearCharge.Text = "";
                    txtYearNoCharge.Text = "";
                    txtMStickerNumber.Text = "";
                    txtYStickerNumber.Text = "";
                }
                else
                {
                    txtMonthCharge.Text = "";
                    txtMonthNoCharge.Text = "";
                    txtYearCharge.Text = "";
                    txtYearNoCharge.Text = "";
                    txtMStickerNumber.Text = "";
                    txtYStickerNumber.Text = "";
                    return;
                }
            }
            else
            {
                MotorVehicle.Statics.MonthStickers =
                    FCConvert.ToInt32(Math.Round(Conversion.Val(txtMStickerNumber.Text)));
                if (MotorVehicle.Statics.MonthStickers != 0)
                {
                    if (MotorVehicle.Statics.RegistrationType != "CORR")
                    {
	                    if (!VerifyMonthSticker())
	                    {
		                    e.Cancel = true;
	                    }
                    }
                    else if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.MonthStickers !=
                             FCConvert.ToInt32(
                                 MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber")))
                    {
						if (!VerifyMonthSticker())
						{
							e.Cancel = true;
						}
					}
                }
            }
        }

        private void txtNetWeight_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Tab))
            {
                // backspace  & tab
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
            {
                // numeric
            }
            else if (KeyAscii == Keys.Return)
            {
                // enter
            }
            else
            {
                // Bp = Beep(1000, 250)
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtNetWeight_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            txtNetWeight.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtPreviousTitle_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                txtPriorTitleState.Focus();
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtPriorTitleState_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtPriorTitleState.BackColor);
            txtPriorTitleState.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtPriorTitleState_Leave(object sender, System.EventArgs e)
        {
            txtPriorTitleState.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtPriorTitleState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyPriorTitleState() == false)
            {
                e.Cancel = true;
            }
        }

        private void txtRate_DblClick(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired == true)
                lblCalculate_Click();
            blnFeesFlag = true;
            frmFeesInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            //FC:FINAL:MSH - i.issue #1804: update values in fields after closing form (frmFeesInfo will be disposed after hiding and all data will be missed)
            frmFeesInfoStaticsValues.InstancePtr.UpdateValuesFromStorage();
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                lblCalculate_Click();
                FillFeesScreen();
            }
            else
            {
                FillFeesScreen();
                FillTotalDue();
            }
        }

        private void txtReg1ICM_TextChanged(object sender, System.EventArgs e)
        {
            SetRegistrantActions(1);
        }

        private void txtReg1ICM_Click(object sender, System.EventArgs e)
        {
            if (fecherFoundation.Strings.UCase(txtReg1ICM.Text) == "I")
            {
                txtReg1ICM.Text = "C";
            }
            else if (fecherFoundation.Strings.UCase(txtReg1ICM.Text) == "C")
            {
                txtReg1ICM.Text = "M";
            }
            else if (fecherFoundation.Strings.UCase(txtReg1ICM.Text) == "M")
            {
                txtReg1ICM.Text = "I";
            }
        }

        private void txtReg1ICM_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg1ICM.BackColor);
            txtReg1ICM.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtReg1ICM.SelectionStart = 0;
            txtReg1ICM.SelectionLength = 1;
        }

        private void txtReg1ICM_Leave(object sender, System.EventArgs e)
        {
            txtReg1ICM.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtReg1ICM_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyOwnerCodes() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtReg1LR_Click(object sender, System.EventArgs e)
        {
            if (fecherFoundation.Strings.UCase(txtReg1LR.Text) == "N")
            {
                txtReg1LR.Text = "E";
            }
            else if (fecherFoundation.Strings.UCase(txtReg1LR.Text) == "E")
            {
                txtReg1LR.Text = "R";
            }
            else if (fecherFoundation.Strings.UCase(txtReg1LR.Text) == "R")
            {
                txtReg1LR.Text = "T";
            }
            else if (fecherFoundation.Strings.UCase(txtReg1LR.Text) == "T")
            {
                txtReg1LR.Text = "P";
            }
            else if (fecherFoundation.Strings.UCase(txtReg1LR.Text) == "P")
            {
                txtReg1LR.Text = "N";
            }
        }

        private void txtReg1LR_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg1LR.BackColor);
            txtReg1LR.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtReg1LR.SelectionStart = 0;
            txtReg1LR.SelectionLength = 1;
        }

        private void txtReg1LR_Leave(object sender, System.EventArgs e)
        {
            txtReg1LR.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtReg2LR_Click(object sender, System.EventArgs e)
        {
            if (fecherFoundation.Strings.UCase(txtReg2LR.Text) == "N")
            {
                txtReg2LR.Text = "E";
            }
            else if (fecherFoundation.Strings.UCase(txtReg2LR.Text) == "E")
            {
                txtReg2LR.Text = "R";
            }
            else if (fecherFoundation.Strings.UCase(txtReg2LR.Text) == "R")
            {
                txtReg2LR.Text = "T";
            }
            else if (fecherFoundation.Strings.UCase(txtReg2LR.Text) == "T")
            {
                txtReg2LR.Text = "P";
            }
            else if (fecherFoundation.Strings.UCase(txtReg2LR.Text) == "P")
            {
                txtReg2LR.Text = "N";
            }
        }

        private void txtReg2LR_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg2LR.BackColor);
            txtReg2LR.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtReg2LR.SelectionStart = 0;
            txtReg2LR.SelectionLength = 1;
        }

        private void txtReg2LR_Leave(object sender, System.EventArgs e)
        {
            txtReg2LR.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtReg3LR_Click(object sender, System.EventArgs e)
        {
            if (fecherFoundation.Strings.UCase(txtReg3LR.Text) == "N")
            {
                txtReg3LR.Text = "E";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3LR.Text) == "E")
            {
                txtReg3LR.Text = "R";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3LR.Text) == "R")
            {
                txtReg3LR.Text = "T";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3LR.Text) == "T")
            {
                txtReg3LR.Text = "P";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3LR.Text) == "P")
            {
                txtReg3LR.Text = "N";
            }
        }

        private void txtReg3LR_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg3LR.BackColor);
            txtReg3LR.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtReg3LR.SelectionStart = 0;
            txtReg3LR.SelectionLength = 1;
        }

        private void txtReg3LR_Leave(object sender, System.EventArgs e)
        {
            txtReg3LR.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtReg2ICM_TextChanged(object sender, System.EventArgs e)
        {
            SetRegistrantActions(2);
        }

        private void txtReg2ICM_Click(object sender, System.EventArgs e)
        {
            switch (fecherFoundation.Strings.UCase(txtReg2ICM.Text))
            {
                case "I":
                    txtReg2ICM.Text = "C";

                    break;

                case "C":
                    {
                        txtReg2ICM.Text = "D";

                        break;
                    }

                case "D":
                    {
                        txtReg2ICM.Text = "M";

                        break;
                    }

                case "M":
                    {
                        txtReg2ICM.Text = "N";

                        break;
                    }

                case "N":
                    {
                        txtReg2ICM.Text = "I";

                        break;
                    }
            }
        }

        private void txtReg2ICM_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyOwnerCodes() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtReg3ICM_TextChanged(object sender, System.EventArgs e)
        {
            SetRegistrantActions(3);
        }

        private void SetRegistrantActions(int registrantPosition)
        {
            if (registrantPosition < 1 || registrantPosition > 3) throw new ArgumentException();

            #region Point to Correct Controls

            // point to correct controls
            FCTextBox ICM_Textbox = null;
            FCTextBox PartyID_Textbox = null;
            FCPictureBox Search_Button = null;
            FCPictureBox Edit_Button = null;
            T2KDateBox DOB_Textbox = null;
            FCTextBox PartyName_Textbox = null;

			switch (registrantPosition)
            {
                case 1:
                    ICM_Textbox = txtReg1ICM;
                    PartyID_Textbox = txtReg1PartyID;
                    Search_Button = imgReg1Search;
                    Edit_Button = imgReg1Edit;
                    DOB_Textbox = txtReg1DOB;
                    PartyName_Textbox = txtRegistrant1;
					break;
                case 2:
                    ICM_Textbox = txtReg2ICM;
                    PartyID_Textbox = txtReg2PartyID;
                    Search_Button = imgReg2Search;
                    Edit_Button = imgReg2Edit;
                    DOB_Textbox = txtReg2DOB;
                    PartyName_Textbox = txtRegistrant2;
					break;
                case 3:
                    ICM_Textbox = txtReg3ICM;
                    PartyID_Textbox = txtReg3PartyID;
                    Search_Button = imgReg3Search;
                    Edit_Button = imgReg3Edit;
                    DOB_Textbox = txtReg3DOB;
                    PartyName_Textbox = txtRegistrant3;
					break;
            }

            #endregion

            // default attributes and actions allowed based on whether ICM is N or not
            var ICM = fecherFoundation.Strings.UCase(ICM_Textbox.Text);

            var ICM_is_N = ICM == "N";
            var ICM_isnot_N = ICM != "N";

            var hasPartyId = Conversion.Val(PartyID_Textbox.Text) != 0;

            PartyID_Textbox.Enabled = ICM_isnot_N;
            Search_Button.Enabled = ICM_isnot_N;
            Edit_Button.Enabled = ICM_isnot_N && hasPartyId;
            DOB_Textbox.Visible = false;

            // specific settings based on ICM
            switch (ICM)
            {
                case "I":
                    // individual, so go to the DOB field
                    DOB_Textbox.Visible = true;

                    break;
                case "!":
                    // no real setting, so set to N
                    ICM_Textbox.Text = "N";

                    break;
                case "N":
                    // clear out the party if N
                    PartyID_Textbox.Text = "";
                    PartyName_Textbox.Text = "";
					break;
            }

            // set focus for next step:
            // tab over if N, otherwise set the focus to the party id
            if (ICM_is_N)
            {
                Support.SendKeys("{TAB}", false);
            }
            else
            {
                PartyID_Textbox.Focus();
            }
        }

        private void txtReg3ICM_Click(object sender, System.EventArgs e)
        {
            if (fecherFoundation.Strings.UCase(txtReg3ICM.Text) == "I")
            {
                txtReg3ICM.Text = "C";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3ICM.Text) == "C")
            {
                txtReg3ICM.Text = "D";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3ICM.Text) == "D")
            {
                txtReg3ICM.Text = "M";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3ICM.Text) == "M")
            {
                txtReg3ICM.Text = "N";
            }
            else if (fecherFoundation.Strings.UCase(txtReg3ICM.Text) == "N")
            {
                txtReg3ICM.Text = "I";
            }
        }

        private void txtReg3ICM_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyOwnerCodes() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtRegWeight_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Tab))
            {
                // backspace  & tab
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
            {
                // numeric
            }
            else if (KeyAscii == Keys.Return)
            {
                // enter
            }
            else
            {
                // Bp = Beep(1000, 250)
                KeyAscii = (Keys)0;
            }

            if (MotorVehicle.Statics.RegistrationType == "GVW")
            {
                if (KeyAscii == Keys.Return)
                {
                    KeyAscii = (Keys)0;
                    blnCalculateRequired = true;
                }
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtRegWeight_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtRegWeight.Text !=
                FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("registeredWeightNew")))
            {
                blnCalculateRequired = true;
            }
        }

        public void txtRegWeight_Validate(bool Cancel)
        {
            txtRegWeight_Validating(txtRegWeight, new System.ComponentModel.CancelEventArgs(Cancel));
        }

        private void txtresidenceCode_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
	        if (VerifyResCode() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtSalesTax_Change(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
        }

        private void txtState_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtState.BackColor);
            txtState.BackColor = ColorTranslator.FromOle(NewBackColor);
            //txtState.SelectedText = txtState.Text;
        }

        private void txtState_Leave(object sender, System.EventArgs e)
        {
            txtState.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyState() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        private void txtCountry_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtCountry.BackColor);
            txtCountry.BackColor = ColorTranslator.FromOle(NewBackColor);
            //txtCountry.SelectedText = txtCountry.Text;
        }

        private void txtCountry_Leave(object sender, System.EventArgs e)
        {
            txtCountry.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtStateCredit_DblClick(object sender, System.EventArgs e)
        {
            if (blnCalculateRequired == true)
                lblCalculate_Click();
            blnFeesFlag = true;
            frmFeesInfo.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            //FC:FINAL:MSH - i.issue #1804: update values in fields after closing form (frmFeesInfo will be disposed after hiding and all data will be missed)
            frmFeesInfoStaticsValues.InstancePtr.UpdateValuesFromStorage();
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                lblCalculate_Click();
            }
            else
            {
                FillFeesScreen();
                FillTotalDue();
            }
        }

        private void txtSubTotal_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtSubTotal.BackColor);
            txtSubTotal.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtSubTotal_Leave(object sender, System.EventArgs e)
        {
            txtSubTotal.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtTitle_Change(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
        }

        private void txtVin_Change(object sender, System.EventArgs e)
        {
            txtVin.Text = fecherFoundation.Strings.UCase(txtVin.Text);
        }

        private void txtVin_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtVin.BackColor);
            txtVin.BackColor = ColorTranslator.FromOle(NewBackColor);
            OpenVehicleTabs();
        }

        private void txtVin_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.Insert)
            {
                KeyCode = 0;
                if (MotorVehicle.Statics.rsSaveVehicle.IsntAnything() || !MotorVehicle.Statics.RegisteringFleet)
                {
                    // do nothing
                }
                else
                {
                    FillSavedInfo();
                    Recalculate();
                }
            }
        }

        private void txtVin_Leave(object sender, System.EventArgs e)
        {
            txtVin.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtVin_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (alreadySaving)
            {
                return;
            }
            string strYear;
            if (VerifyVIN() == false)
                e.Cancel = true;
            if ((fecherFoundation.Strings.Trim(txtMake.Text) != "" &&
                 fecherFoundation.Strings.Trim(txtYear.Text) != "") ||
                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CV")
            {
                return;
            }

            strYear = GetYearFromVin(txtVin.Text);
			if (txtVin.Text.Length == 17)
			{
				var vehicle = blueBookRepository.VehicleInformation.GetVehicleInformation(txtVin.Text).FirstOrDefault();
	            if (vehicle != null)
	            {
					strYear = vehicle.ModelYear.ToString();
					string strMake = vehicle.ManufacturerName.Left(4).ToUpper();
					string strType = GetVehicleTypeCode(vehicle.ClassificationId, vehicle.CategoryId);
					txtMake.Text = modGlobalFunctions.ReturnCorrectedMakeCode(strType, vehicle.ManufacturerName, strMake);
					txtModel.Text = vehicle.ModelName.Left(6).ToUpper();
	            }
            }
			txtYear.Text = strYear;
			txtYear_Validate(null, new System.ComponentModel.CancelEventArgs(false));
		}

		private void txtYear_Change(object sender, System.EventArgs e)
        {
            if (Information.IsNumeric(txtYear.Text) == false && txtYear.Text != "")
            {
                txtYear.SelectionStart = txtYear.Text.Length - 1;
                txtYear.SelectionLength = 1;
                // Bp = Beep(1000, 250)
            }
        }

        private void txtYear_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtYear.BackColor);
            txtYear.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtYear_Leave(object sender, System.EventArgs e)
        {
            txtYear.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtMake_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtMake.BackColor);
            txtMake.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtMake_Leave(object sender, System.EventArgs e)
        {
            txtMake.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtModel_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtModel.BackColor);
            txtModel.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtModel_Leave(object sender, System.EventArgs e)
        {
            txtModel.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtStyle_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtStyle.BackColor);
            txtStyle.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtStyle_Leave(object sender, System.EventArgs e)
        {
            txtStyle.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtTires_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtTires.BackColor);
            txtTires.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtTires_Leave(object sender, System.EventArgs e)
        {
            txtTires.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtAxles_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtAxles.BackColor);
            txtAxles.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtAxles_Leave(object sender, System.EventArgs e)
        {
            txtAxles.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtNetWeight_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtNetWeight.BackColor);
            txtNetWeight.BackColor = ColorTranslator.FromOle(NewBackColor);
            if (MotorVehicle.Statics.Class == "SE")
            {
                blnCalculateRequired = true;
            }

            txtNetWeight.SelectionStart = 0;
            txtNetWeight.SelectionLength = txtNetWeight.Text.Length;
        }

        private void txtNetWeight_Leave(object sender, System.EventArgs e)
        {
            if (ColorTranslator.FromOle(OldColor) != txtVin.BackColor)
                OldColor = ColorTranslator.ToOle(txtVin.BackColor);
            txtNetWeight.BackColor = ColorTranslator.FromOle(OldColor);
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void txtRegWeight_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtRegWeight.BackColor);
            txtRegWeight.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtRegWeight.SelectionStart = 0;
            txtRegWeight.SelectionLength = txtRegWeight.Text.Length;
        }

        private void txtRegWeight_Leave(object sender, System.EventArgs e)
        {
            if (ColorTranslator.FromOle(OldColor) != txtVin.BackColor)
                OldColor = ColorTranslator.ToOle(txtVin.BackColor);
            txtRegWeight.BackColor = ColorTranslator.FromOle(OldColor);

            if (txtRegWeight.Text !=
                FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("registeredWeightNew")))
            {
	            blnCalculateRequired = true;
	            lblCalculate_Click();
            }
        }

        private void txtFuel_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtFuel.BackColor);
            txtFuel.BackColor = ColorTranslator.FromOle(NewBackColor);
            //txtFuel.SelectionLength = txtFuel.Text.Length;
        }

        private void txtFuel_Leave(object sender, System.EventArgs e)
        {
            txtFuel.BackColor = ColorTranslator.FromOle(OldColor);
            if (MotorVehicle.Statics.RegistrationType == "NRR")
            {
                if (!blnVerifyFlag && !MotorVehicle.Statics.PreviewFlag && !blnFeesFlag &&
                    !MotorVehicle.Statics.UseTaxFlag && !MotorVehicle.Statics.CTAFlag && !MotorVehicle.Statics.OpenFlag)
                {
                    // kk01062016 tromv-1129  Fix shift+tab behavior
                    if (ActiveControl.GetName() != "txtAxles")
                    {
                        txtReg1ICM.Focus();
                    }
                }
            }
        }

        private void txtReg1DOB_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg1DOB.BackColor);
            txtReg1DOB.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtReg1DOB_Leave(object sender, System.EventArgs e)
        {
            txtReg1DOB.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtDOTNumber_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtDOTNumber.BackColor);
            txtDOTNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtDOTNumber_Leave(object sender, System.EventArgs e)
        {
            txtDOTNumber.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtUnitNumber_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtUnitNumber.BackColor);
            txtUnitNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtUnitNumber_Leave(object sender, System.EventArgs e)
        {
            txtUnitNumber.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtLegalres_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtLegalres.BackColor);
            txtLegalres.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtLegalres_Leave(object sender, System.EventArgs e)
        {
            txtLegalres.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtResCountry_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtResCountry.BackColor);
            txtResCountry.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtResCountry_Leave(object sender, System.EventArgs e)
        {
            txtResCountry.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtResCountry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {

        }

        private void txtLegalres_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "MAINE MOTOR TRANSPORT" ||
                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" ||
                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "STAAB AGENCY" ||
                fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                "HASKELL REGISTRATION")
            {
                if (KeyCode == Keys.Insert)
                {
                    KeyCode = 0;
                    txtLegalres.Text = txtCity.Text;
                    txtLegalResStreet.Text = txtAddress1.Text;
                    txtResState.Text = txtState.Text;
                    txtResCountry.Text = "US";
                }
            }
        }

        private void txtLegalResStreet_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtLegalResStreet.BackColor);
            txtLegalResStreet.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtLegalResStreet_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "MAINE MOTOR TRANSPORT" ||
                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" ||
                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "STAAB AGENCY" ||
                fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                "HASKELL REGISTRATION")
            {
                if (KeyCode == Keys.Insert)
                {
                    KeyCode = 0;
                    txtLegalres.Text = txtCity.Text;
                    txtLegalResStreet.Text = txtAddress1.Text;
                    txtResState.Text = txtState.Text;
                    txtResCountry.Text = "US";
                }
            }
        }

        private void txtLegalResStreet_Leave(object sender, System.EventArgs e)
        {
            txtLegalResStreet.BackColor = ColorTranslator.FromOle(OldColor);
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                //FC:FINAL:DDU:#2302 - if user clicks outside of current form(in MDIParent Grid for example) the activecontrol set null
                if (ActiveControl != null && ActiveControl.TabIndex > txtLegalResStreet.TabIndex)
                {
                    // kk01062016 Fix Shift-tab behavior
                    if (txtMonthCharge.Visible)
                    {
                        if (Conversion.Val(txtMonthCharge.Text) > 0 || Conversion.Val(txtMonthNoCharge.Text) > 0)
                        {
                            txtMStickerNumber.Focus();
                        }
                        else
                        {
                            if (txtYStickerNumber.Enabled == true)
                            {
                                txtYStickerNumber.Focus();
                            }
                        }
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                if (fecherFoundation.Information.Err(ex).Number == 5)
                {
                    fecherFoundation.Information.Err(ex).Clear();
                }
                else
                {
                    MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) +
                                    "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
                }

                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void txtResState_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtResState.BackColor);
            txtResState.BackColor = ColorTranslator.FromOle(NewBackColor);
           // txtResState.SelectedText = txtResState.Text;
        }

        private void txtResState_Leave(object sender, System.EventArgs e)
        {
            txtResState.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtResState_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) != "MAINE MOTOR TRANSPORT" ||
                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" ||
                fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "STAAB AGENCY" ||
                fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) ==
                "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) ==
                "HASKELL REGISTRATION")
            {
                if (KeyCode == Keys.Insert)
                {
                    KeyCode = (Keys)0;
                    txtLegalres.Text = txtCity.Text;
                    txtLegalResStreet.Text = txtAddress1.Text;
                    txtResState.Text = txtState.Text;
                    txtResCountry.Text = "US";
                }
            }
        }

        private void txtresidenceCode_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtresidenceCode.BackColor);
            txtresidenceCode.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtresidenceCode_Leave(object sender, System.EventArgs e)
        {
            txtresidenceCode.BackColor = ColorTranslator.FromOle(OldColor);
            if (MotorVehicle.Statics.RegistrationType != "CORR")
            {
                Recalculate();
            }
        }

        private void txtExciseTax_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtExciseTax.BackColor);
            txtExciseTax.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtExciseTax_Leave(object sender, System.EventArgs e)
        {
            txtExciseTax.BackColor = ColorTranslator.FromOle(OldColor);
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                CalculateLocalECorrectFees();
            }
        }

        private void txtMilYear_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtMilYear.BackColor);
            txtMilYear.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtMilYear.SelectionStart = 0;
            txtMilYear.SelectionLength = 1;
        }

        private void txtMilYear_Leave(object sender, System.EventArgs e)
        {
	        if (MotorVehicle.Statics.RegistrationType == "CORR")
	        {
		        if (Convert.ToInt16(txtMilYear.Text) == MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int16("MillYear"))
		        {
			        return;
		        }
	        }

	        txtMilYear.BackColor = ColorTranslator.FromOle(OldColor);
            Recalculate();
            if (txtMilRate.Text == "")
            {
                if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CL" &&
                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "C5") ||
                    (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" &&
                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "L6"))
                {
                    if (Conversion.Val(txtMilYear.Text) == 1)
                    {
                        txtMilRate.Text = ".0250";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 2)
                    {
                        txtMilRate.Text = ".0200";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 3)
                    {
                        txtMilRate.Text = ".0160";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 4)
                    {
                        txtMilRate.Text = ".0120";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 5)
                    {
                        txtMilRate.Text = ".0120";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 6)
                    {
                        txtMilRate.Text = ".0120";
                    }
                }
                else
                {
                    if (Conversion.Val(txtMilYear.Text) == 1)
                    {
                        txtMilRate.Text = ".0240";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 2)
                    {
                        txtMilRate.Text = ".0175";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 3)
                    {
                        txtMilRate.Text = ".0135";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 4)
                    {
                        txtMilRate.Text = ".0100";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 5)
                    {
                        txtMilRate.Text = ".0065";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 6)
                    {
                        txtMilRate.Text = ".0040";
                    }
                }
            }
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
	            MotorVehicle.CalculateECorrectFee(ref intCorrSpecialtyPlate, ref intCorrInitialPlate);
            }
        }

        public void txtMilYear_LostFocus()
        {
            txtMilYear_Leave(txtMilYear, new System.EventArgs());
        }

        private void txtTransferCharge_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtTransferCharge.BackColor);
            txtTransferCharge.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtTransferCharge_Leave(object sender, System.EventArgs e)
        {
            txtTransferCharge.BackColor = ColorTranslator.FromOle(OldColor);
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                CalculateLocalECorrectFees();
            }
        }

        private void txtStateCredit_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtStateCredit.BackColor);
            txtStateCredit.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtStateCredit_Leave(object sender, System.EventArgs e)
        {
            txtStateCredit.BackColor = ColorTranslator.FromOle(OldColor);
            // Dave 12/28/06 added commercial credit in to fix call #107306
            txtStateCredit.Text =
                Strings.Format(
                    FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) +
                    FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditComm.Text), "#,##0.00");
        }

        private void txtCreditNumber_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtCreditNumber.BackColor);
            txtCreditNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtCreditNumber_Leave(object sender, System.EventArgs e)
        {
            txtCreditNumber.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtRate_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtRate.BackColor);
            txtRate.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtRate_Leave(object sender, System.EventArgs e)
        {
            txtRate.BackColor = ColorTranslator.FromOle(OldColor);
            txtRate.Text = frmFeesInfo.InstancePtr.txtFEERegFee.Text;
        }

        private void txtTownCredit_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtTownCredit.BackColor);
            txtTownCredit.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtTownCredit_Leave(object sender, System.EventArgs e)
        {
            txtTownCredit.BackColor = ColorTranslator.FromOle(OldColor);
            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                CalculateLocalECorrectFees();
            }
            else
            {
                lblCalculate_Click();
            }
        }

        private void txtFees_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtFees.BackColor);
            txtFees.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtFees_Leave(object sender, System.EventArgs e)
        {
            txtFees.BackColor = ColorTranslator.FromOle(OldColor);
            txtFees.Text = frmFeesInfo.InstancePtr.txtFEETotal.Text;
        }

        private void txtAgentFee_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtAgentFee.BackColor);
            txtAgentFee.BackColor = ColorTranslator.FromOle(NewBackColor);
            blnCalculateRequired = true;
        }

        private void txtAgentFee_Leave(object sender, System.EventArgs e)
        {
            txtAgentFee.BackColor = ColorTranslator.FromOle(OldColor);
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void txtReg2ICM_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg2ICM.BackColor);
            txtReg2ICM.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtReg2ICM.SelectionLength = 1;
        }

        private void txtReg2ICM_Leave(object sender, System.EventArgs e)
        {
            txtReg2ICM.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtReg3ICM_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg3ICM.BackColor);
            txtReg3ICM.BackColor = ColorTranslator.FromOle(NewBackColor);
            txtReg3ICM.SelectionLength = 1;
        }

        private void txtReg3ICM_Leave(object sender, System.EventArgs e)
        {
            txtReg3ICM.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtTitle_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtTitle.BackColor);
            txtTitle.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtTitle_Leave(object sender, System.EventArgs e)
        {
            txtTitle.BackColor = ColorTranslator.FromOle(OldColor);
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void txtSalesTax_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtSalesTax.BackColor);
            txtSalesTax.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtSalesTax_Leave(object sender, System.EventArgs e)
        {
            txtSalesTax.BackColor = ColorTranslator.FromOle(OldColor);
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void txtCTANumber_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtCTANumber.BackColor);
            txtCTANumber.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtCTANumber_Leave(object sender, System.EventArgs e)
        {
            txtCTANumber.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtMStickerNumber_Enter(object sender, System.EventArgs e)
        {
            if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) == 0)
            {
                //FC:FINAL:SBE - #2199 - moved to client side (JavaScript implementation)
                //Support.SendKeys("{TAB}", false);
                /*? On Error GoTo 0 */
                return;
            }

            OldColor = ColorTranslator.ToOle(txtMStickerNumber.BackColor);
            txtMStickerNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
            if (fecherFoundation.Strings.Trim(txtMStickerNumber.Text) == "0")
            {
                txtMStickerNumber.Text = "";
            }
        }

        private void txtMStickerNumber_Leave(object sender, System.EventArgs e)
        {
            txtMStickerNumber.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtYear_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
	        if (MotorVehicle.Statics.RegistrationType == "CORR")
	        {
		        if (Convert.ToInt16(txtYear.Text) == MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int16("Year"))
		        {
			        return;
		        }
	        }
            if (MotorVehicle.Statics.NewToTheSystem || (MotorVehicle.Statics.RegistrationType == "NRR" ||
                                                        MotorVehicle.Statics.RegistrationType == "NRT" ||
                                                        MotorVehicle.Statics.RegistrationType == "CORR"))
            {
                // TROMV-1304 1.16.18 kjr added RT=CORR
                // Dave 1/18/06 Skowhegan problem - we should use expire date not todays date to calculate correct mil year
                if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
                {
                    if (DateTime.Today.Year - Conversion.Val(txtYear.Text) + 1 > 6)
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("MillYear", 6);
                    }
                    else
                    {
                        if (DateTime.Today.Year - Conversion.Val(txtYear.Text) + 1 < 1)
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MillYear", 1);
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MillYear",
                                DateTime.Today.Year - Conversion.Val(txtYear.Text) + 1);
                        }
                    }
                }
                else
                {
                    // we must use current expiration date year - 1 to calculate mil year since we dont have previous information for this vehicle
                    if ((FCConvert.ToDateTime(txtExpires.Text).Year - 1) - Conversion.Val(txtYear.Text) + 1 > 6)
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("MillYear", 6);
                    }
                    else
                    {
                        if ((FCConvert.ToDateTime(txtExpires.Text).Year - 1) - Conversion.Val(txtYear.Text) + 1 < 1)
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MillYear", 1);
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MillYear",
                                (FCConvert.ToDateTime(txtExpires.Text).Year - 1) - Conversion.Val(txtYear.Text) + 1);
                        }
                    }
                }

                txtMilYear.Text = FCConvert.ToString(Conversion.Val(
                    fecherFoundation.Strings.Trim(
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")))));
                if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CL" &&
                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "C5") ||
                    (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" &&
                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "L6"))
                {
                    if (Conversion.Val(txtMilYear.Text) == 1)
                    {
                        txtMilRate.Text = ".0250";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 2)
                    {
                        txtMilRate.Text = ".0200";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 3)
                    {
                        txtMilRate.Text = ".0160";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 4)
                    {
                        txtMilRate.Text = ".0120";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 5)
                    {
                        txtMilRate.Text = ".0120";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 6)
                    {
                        txtMilRate.Text = ".0120";
                    }
                }
                else
                {
                    if (Conversion.Val(txtMilYear.Text) == 1)
                    {
                        txtMilRate.Text = ".0240";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 2)
                    {
                        txtMilRate.Text = ".0175";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 3)
                    {
                        txtMilRate.Text = ".0135";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 4)
                    {
                        txtMilRate.Text = ".0100";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 5)
                    {
                        txtMilRate.Text = ".0065";
                    }
                    else if (Conversion.Val(txtMilYear.Text) == 6)
                    {
                        txtMilRate.Text = ".0040";
                    }
                }

                Recalculate();

                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
	                MotorVehicle.CalculateECorrectFee(ref intCorrSpecialtyPlate, ref intCorrInitialPlate);

                }
            }
        }

        private void txtYearCharge_TextChanged(object sender, System.EventArgs e)
        {
            blnCalculateRequired = true;
        }

        private void txtYearCharge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Back)
            {
                // backspace
                txtYStickerNumber.Enabled = false;
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D2)
            {
                // number
                txtYStickerNumber.Enabled = true;
                txtYStickerNumber.Enabled = true;
            }
            else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.NumPad3))
            {
                txtYearCharge.Text = "";
                txtYStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }
            else
            {
                txtYStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtYearCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (CheckNumberOfStickers(FCUtils.iDiv(Conversion.Val(txtYearCharge.Text), 1),
                    FCUtils.iDiv(Conversion.Val(txtYearNoCharge.Text), 1)) == false)
            {
                txtYearCharge.Text = "";
                e.Cancel = true;
            }

            SetYearNumbers();
        }

        private void txtYearNoCharge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Back)
            {
                // backspace
                txtYStickerNumber.Enabled = false;
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D2)
            {
                // number
                txtYStickerNumber.Enabled = true;
                txtYStickerNumber.Enabled = true;
            }
            else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.NumPad3))
            {
                txtYearNoCharge.Text = "";
                txtYStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }
            else
            {
                txtYStickerNumber.Enabled = false;
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtYearNoCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (CheckNumberOfStickers(FCUtils.iDiv(Conversion.Val(txtYearCharge.Text), 1),
                    FCUtils.iDiv(Conversion.Val(txtYearNoCharge.Text), 1)) == false)
            {
                txtYearNoCharge.Text = "";
                e.Cancel = true;
            }
        }

        private void txtYStickerNumber_Enter(object sender, System.EventArgs e)
        {
            if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) == 0)
            {
                //FC:FINAL:DDU:#i1958 - send user to next tabindex control
                //Support.SendKeys("{TAB}", false);
                txtPriorTitleState.Focus();
                return;
            }

            OldColor = ColorTranslator.ToOle(txtYStickerNumber.BackColor);
            txtYStickerNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
            if (fecherFoundation.Strings.Trim(txtYStickerNumber.Text) == "0")
            {
                txtYStickerNumber.Text = "";
            }
        }

        private void txtYStickerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Back)
            {
                // backspace
            }
            else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
            {
                // number
            }
            else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.NumPad3))
            {
                txtYStickerNumber.Text = "";
                KeyAscii = (Keys)0;
            }
            else
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtYStickerNumber_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (Shift == 3 && KeyCode == Keys.F9)
            {
                txtYStickerNumber.Text = "";
                MotorVehicle.Statics.F9 = true;
                frmInventory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            }
        }

        private void txtYStickerNumber_Leave(object sender, System.EventArgs e)
        {
            txtYStickerNumber.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtPreviousTitle_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtPreviousTitle.BackColor);
            txtPreviousTitle.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtPreviousTitle_Leave(object sender, System.EventArgs e)
        {
            txtPreviousTitle.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtReg2DOB_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtReg2DOB.BackColor);
            txtReg2DOB.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtReg2DOB_Leave(object sender, System.EventArgs e)
        {
            txtReg2DOB.BackColor = ColorTranslator.FromOle(OldColor);
            if (!Information.IsDate(txtReg2DOB.Text))
            {
                txtReg2DOB.Text = "";
            }
        }

        private void txtExpires_Enter(object sender, System.EventArgs e)
        {
            if (MotorVehicle.Statics.ETO)
            {
                if (MotorVehicle.Statics.RegistrationType == "NRR" && !MotorVehicle.Statics.NewFleet)
                {
                    txtExpires.Text = "";
                }

                Support.SendKeys("{TAB}", false);
                return;
            }

            OldColor = ColorTranslator.ToOle(txtExpires.BackColor);
            txtExpires.BackColor = ColorTranslator.FromOle(NewBackColor);
            MotorVehicle.Statics.DataFormLoaded = true;
            if (MotorVehicle.Statics.RegistrationType != "NRR" && MotorVehicle.Statics.RegistrationType != "CORR")
            {
                txtExpires.Locked = true;
            }
            else
            {
                txtExpires.Locked = false;
            }

            if (Information.IsDate(txtExpires.Text))
            {
                datOriginalExpirationDate = FCConvert.ToDateTime(txtExpires.Text);
            }
        }

        public void txtExpires_Leave(object sender, System.EventArgs e)
        {
            if (txtExpires.Text == "00/00/0000")
            {
                txtExpires.Text = "";
            }

            txtExpires.BackColor = ColorTranslator.FromOle(OldColor);
            txtExpires.Locked = false;
        }

        private void txtBase_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtBase.BackColor);
            txtBase.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtBase_Leave(object sender, System.EventArgs e)
        {
            txtBase.BackColor = ColorTranslator.FromOle(OldColor);
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void txtMonthCharge_Enter(object sender, System.EventArgs e)
        {
            if (txtMonthCharge.ReadOnly != true)
            {
                OldColor = ColorTranslator.ToOle(txtMonthCharge.BackColor);
                txtMonthCharge.BackColor = ColorTranslator.FromOle(NewBackColor);
                txtMonthCharge.SelectionStart = 0;
                txtMonthCharge.SelectionLength = 1;
            }
        }

        private void txtMonthCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (CheckNumberOfStickers(FCUtils.iDiv(Conversion.Val(txtMonthCharge.Text), 1),
                    FCUtils.iDiv(Conversion.Val(txtMonthNoCharge.Text), 1)) == false)
            {
                txtMonthCharge.Text = "";
                e.Cancel = true;
            }

            SetMonthNumbers();
        }

        private void txtMonthCharge_Leave(object sender, System.EventArgs e)
        {
            txtMonthCharge.BackColor = ColorTranslator.FromOle(OldColor);
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void txtYearCharge_Leave(object sender, System.EventArgs e)
        {
            txtYearCharge.BackColor = ColorTranslator.FromOle(OldColor);
            if (blnCalculateRequired)
            {
                lblCalculate_Click();
            }
        }

        private void txtYearCharge_Enter(object sender, System.EventArgs e)
        {
            if (txtYearCharge.ReadOnly != true)
            {
                OldColor = ColorTranslator.ToOle(txtYearCharge.BackColor);
                txtYearCharge.BackColor = ColorTranslator.FromOle(NewBackColor);
                txtYearCharge.SelectionStart = 0;
                txtYearCharge.SelectionLength = 1;
            }
        }

        private void txtMonthNoCharge_Enter(object sender, System.EventArgs e)
        {
            if (txtMonthNoCharge.ReadOnly != true)
            {
                OldColor = ColorTranslator.ToOle(txtMonthNoCharge.BackColor);
                txtMonthNoCharge.BackColor = ColorTranslator.FromOle(NewBackColor);
                txtMonthNoCharge.SelectionStart = 0;
                txtMonthNoCharge.SelectionLength = 1;
            }
        }

        private void txtMonthNoCharge_Leave(object sender, System.EventArgs e)
        {
            txtMonthNoCharge.BackColor = ColorTranslator.FromOle(OldColor);
            SetMonthNumbers();
        }

        private void txtYearNoCharge_Enter(object sender, System.EventArgs e)
        {
            if (txtYearNoCharge.ReadOnly != true)
            {
                OldColor = ColorTranslator.ToOle(txtYearNoCharge.BackColor);
                txtYearNoCharge.BackColor = ColorTranslator.FromOle(NewBackColor);
                txtYearNoCharge.SelectionStart = 0;
                txtYearNoCharge.SelectionLength = 1;
            }
        }

        private void txtYearNoCharge_Leave(object sender, System.EventArgs e)
        {
            txtYearNoCharge.BackColor = ColorTranslator.FromOle(OldColor);
            SetYearNumbers();
        }

        public void txtYStickerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MotorVehicle.Statics.ETO)
            {
                if (txtYStickerNumber.Text != "")
                {
                    // Bp = Beep(1000, 250)
                    e.Cancel = true;
                    ShowWarningBox("You may not issue stickers on an Excise Tax Only Transaction.",
                        "Unable to Issue Stickers");
                    txtMonthCharge.Text = "";
                    txtMonthNoCharge.Text = "";
                    txtYearCharge.Text = "";
                    txtYearNoCharge.Text = "";
                    txtMStickerNumber.Text = "";
                    txtYStickerNumber.Text = "";
                }
                else
                {
                    txtMonthCharge.Text = "";
                    txtMonthNoCharge.Text = "";
                    txtYearCharge.Text = "";
                    txtYearNoCharge.Text = "";
                    txtMStickerNumber.Text = "";
                    txtYStickerNumber.Text = "";
                }
            }
            else
            {
                MotorVehicle.Statics.YearStickers =
                    FCConvert.ToInt32(Math.Round(Conversion.Val(txtYStickerNumber.Text)));
                if (MotorVehicle.Statics.YearStickers != 0)
                {
                    if (MotorVehicle.Statics.RegistrationType != "CORR")
                    {
                        if (!VerifyYearSticker())
                        {
                            e.Cancel = true;
                        }
                    }
                    else if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.YearStickers !=
                             FCConvert.ToInt32(
                                 MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")))
                    {
						if (!VerifyYearSticker())
						{
							e.Cancel = true;
						}
					}
                }
            }
        }

        private void txtZip_Enter(object sender, System.EventArgs e)
        {
            OldColor = ColorTranslator.ToOle(txtZip.BackColor);
            txtZip.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void txtZip_Leave(object sender, System.EventArgs e)
        {
            txtZip.BackColor = ColorTranslator.FromOle(OldColor);
        }

        private void txtZip_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyZip() == false)
            {
                // Bp = Beep(1000, 250)
                e.Cancel = true;
            }
        }

        public void FillVehicleInfo()
        {
            txtClass.Text = MotorVehicle.Statics.Class;
            txtRegistrationNumber.Text = MotorVehicle.Statics.Plate1;
            if (Strings.Mid(MotorVehicle.Statics.RegistrationType, 1, 1) != "N" || MotorVehicle.Statics.PreviousETO)
            {
                // And ExciseAP <> True
                txtVin.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
                // 
                txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
                // these values are filled by RedBook
                txtMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make"));
                // 
                string model = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("model"));
				txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
				// 
			}

            txtColor1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color1").Trim());
            txtColor2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2").Trim());
            if (txtColor2.Text == "")
                txtColor2.Text = "NA";
            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TC")
            {
                txtStyle.Text = "SC";
            }
            else
            {
                txtStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
            }

            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles")) != 0)
            {
                txtAxles.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles"));
            }
            else
            {
                txtAxles.Text = "";
            }

            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires")) != 0)
            {
                txtTires.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires"));
            }
            else
            {
                txtTires.Text = "";
            }

            // kk06232016 tromv-1145  Use the GVW from the Master record first, leave blank and validate the field later otherwise
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")) != 0)
            {
                txtRegWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew"));
            }
            else
            {
                txtRegWeight.Text = "";
            }

            // Select Case rsFinal.Fields("Class")
            // Case "VT", "LB", "BB", "FD", "DS", "WB", "TS", "SW", "BC", "AW", "CV", "MO", "PO", "PH", "PS", "AG", "CR", "CD", "CM", "DX", "GS", "UM"
            // If Right(rsFinal.Fields("Subclass"), 1) = "1" Then
            // txtRegWeight.Text = ""
            // Else
            // txtRegWeight.Text = "10000"
            // End If
            // Case "DV", "VX"
            // If Right(rsFinal.Fields("Subclass"), 1) = "1" Or Right(rsFinal.Fields("Subclass"), 1) = "3" Then
            // txtRegWeight.Text = ""
            // Else
            // txtRegWeight.Text = "10000"
            // End If
            // Case "PC"
            // kk11202015 tromv-1111
            // If Right(rsFinal.Fields("Subclass"), 1) <> 3 And Right(rsFinal.Fields("Subclass"), 1) <> 8 Then
            // txtRegWeight.Text = ""
            // Else
            // txtRegWeight.Text = "10000"
            // End If
            // Case Else
            // If .Fields("RegisteredWeightNew") = 9000 Then
            // txtRegWeight.Text = 10000
            // Else
            // txtRegWeight.Text = .Fields("RegisteredWeightNew")
            // End If
            // End Select
            if (MotorVehicle.Statics.Class == "TC" || MotorVehicle.Statics.Class == "CO" ||
                MotorVehicle.Statics.Class == "TT" || MotorVehicle.Statics.Class == "TL" ||
                MotorVehicle.Statics.Class == "CL")
            {
                if (MotorVehicle.Statics.Subclass == "T2" || MotorVehicle.Statics.Subclass == "T4" ||
                    MotorVehicle.Statics.Subclass == "T5")
                {
                    txtRegWeight.Text = "";
                    txtRegWeight.Enabled = false;
                }
                else if (MotorVehicle.Statics.Subclass == "L3" || MotorVehicle.Statics.Subclass == "L4" ||
                         MotorVehicle.Statics.Subclass == "L6")
                {
                    txtRegWeight.Text = "";
                    txtRegWeight.Enabled = false;
                }
                else if (MotorVehicle.Statics.Subclass == "C3" || MotorVehicle.Statics.Subclass == "C4" ||
                         MotorVehicle.Statics.Subclass == "C5")
                {
                    txtRegWeight.Text = "";
                    txtRegWeight.Enabled = false;
                }
                else
                {
                    txtNetWeight.Text = "";
                    txtNetWeight.Enabled = false;
                    if (MotorVehicle.Statics.Subclass == "L1" || MotorVehicle.Statics.Subclass == "L2")
                    {
                        txtRegWeight.Text = "2000";
                    }
                }

                txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
            }
            else if (MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "AP")
            {
                // kk04142016 tromv-1145  Add Class = "AP"
                txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
            }

            // 
            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "CI")
            {
	            txtFuel.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel"));
			}
            else
            {
	            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel")))
	            {
		            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel")) != "" &&
		                fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel")) != "N")
		            {
			            txtFuel.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel"));
		            }
		            else
		            {
			            if (fecherFoundation.Strings.Trim(txtFuel.Text) == "" &&
			                (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "TL" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "CL" &&
                             MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "HC" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "TC"))
			            {
				            txtFuel.Text = "G";
			            }
			            else
			            {
				            txtFuel.Text = "";
			            }
		            }
	            }
	            else
	            {
		            if (fecherFoundation.Strings.Trim(txtFuel.Text) == "" &&
		                (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "TL" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "CL" &&
                         MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "HC" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "TC"))
		            {
			            txtFuel.Text = "G";
		            }
		            else
		            {
			            txtFuel.Text = "";
		            }
	            }
			}
            

            if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) == false)
                txtUnitNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
            if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber")) == false)
                txtDOTNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber"));
            if (txtDOTNumber.Text == "0")
                txtDOTNumber.Text = "";
            // 
            // Determine the Number of Stickers for each transaction
            if (MotorVehicle.Statics.Class == "TC" || MotorVehicle.Statics.TownLevel == 9)
            {
                return;
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "CI")
            {
                if (MotorVehicle.Statics.RegistrationType == "RRR")
                {
                    txtMStickerNumber.Enabled = true;
                    txtYStickerNumber.Enabled = true;
                    if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                        MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                        MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "MP" ||
                        MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                        MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                        MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                    {
                        // ((Class = "CO" Or Class = "TT" Or Class = "CC") And (Val(rsFinal.Fields("plate")) >= 800000 And Val(rsFinal.Fields("plate")) <= 899999))
                        if (Information.IsDate(txtExpires.Text))
                        {
                            if (FCConvert.ToDateTime(txtExpires.Text).Month != MotorVehicle.Statics.rsFinalCompare
                                    .Get_Fields_DateTime("ExpireDate").Month)
                            {
                                txtMonthNoCharge.Text = "1";
                            }
                        }

                        txtYearNoCharge.Text = "1";
                        // If PlateType = 3 Then  '(PlateType = 2 And LostPlate)
                        // txtMonthNoCharge.Text = ""
                        // txtMonthCharge.Text = "1"
                        // txtYearNoCharge.Text = ""
                        // txtYearCharge.Text = "1"
                        // Else
                        if (MotorVehicle.Statics.PlateType != 1)
                        {
                            txtMonthNoCharge.Text = "1";
                        }

                        if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) && MotorVehicle.Statics.FixedMonth)
                        {
                            txtMonthCharge.Text = "";
                            txtMonthNoCharge.Text = "";
                        }
                    }
                    else if (MotorVehicle.Statics.Class == "IU")
                    {
                        if (Information.IsDate(txtExpires.Text))
                        {
                            if (FCConvert.ToDateTime(txtExpires.Text).Month != MotorVehicle.Statics.rsFinalCompare
                                    .Get_Fields_DateTime("ExpireDate").Month)
                            {
                                txtMonthNoCharge.Text = "2";
                            }
                            else
                            {
                                txtMonthNoCharge.Text = "0";
                            }
                        }
                        else
                        {
                            txtMonthNoCharge.Text = "0";
                        }

                        txtYearNoCharge.Text = "0";
                    }
                    else
                    {
                        if (!Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("ExpireDate")))
                        {
                            txtMonthNoCharge.Text = "2";
                        }
                        else if (Information.IsDate(txtExpires.Text))
                        {
                            if (FCConvert.ToDateTime(txtExpires.Text).Month != MotorVehicle.Statics.rsFinalCompare
                                    .Get_Fields_DateTime("ExpireDate").Month)
                            {
                                txtMonthNoCharge.Text = "2";
                            }
                        }

                        txtYearNoCharge.Text = "2";
                        // If PlateType = 3 Then   '(PlateType = 2 And LostPlate)
                        // txtMonthCharge.Text = "2"
                        // txtMonthNoCharge.Text = ""
                        // txtYearCharge.Text = "2"
                        // txtYearNoCharge.Text = ""
                        // Else
                        if (MotorVehicle.Statics.PlateType != 1)
                        {
                            txtMonthNoCharge.Text = "2";
                        }
                    }

                    if (MotorVehicle.Statics.PlateType == 1 && Conversion.Val(txtMonthNoCharge.Text) == 0)
                        txtMStickerNumber.Enabled = false;
                    lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                    lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                    // 
                }
                else if (MotorVehicle.Statics.RegistrationType == "RRT")
                {
                    if (MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4)
                    {
                        if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                            MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                            MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "MP" ||
                            MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                            MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                            MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                        {
                            txtYearNoCharge.Text = "1";
                            txtMonthNoCharge.Text = "1";
                        }
                        else if (MotorVehicle.Statics.Class == "IU")
                        {
                            txtYearNoCharge.Text = "0";
                            txtMonthNoCharge.Text = "0";
                        }
                        else
                        {
                            txtYearNoCharge.Text = "2";
                            txtMonthNoCharge.Text = "2";
                        }
                    }
                    else if (MotorVehicle.Statics.PlateType == 2 &&
                             MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() != MotorVehicle
                                 .Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate").ToOADate())
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Month !=
                            MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate").Month &&
                            MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year != MotorVehicle.Statics
                                .rsPlateForReg.Get_Fields_DateTime("ExpireDate").Year)
                        {
                            if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                                MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                                MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "MP" ||
                                MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                                MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                                MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                            {
                                txtYearNoCharge.Text = "1";
                                txtMonthNoCharge.Text = "1";
                            }
                            else if (MotorVehicle.Statics.Class == "IU")
                            {
                                txtYearNoCharge.Text = "0";
                                txtMonthNoCharge.Text = "0";
                            }
                            else
                            {
                                txtYearNoCharge.Text = "2";
                                txtMonthNoCharge.Text = "2";
                            }
                        }
                        else if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Month !=
                                 MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate").Month)
                        {
                            if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                                MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                                MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "MP" ||
                                MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                                MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                                MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                            {
                                txtYearNoCharge.Text = "0";
                                txtMonthNoCharge.Text = "1";
                            }
                            else if (MotorVehicle.Statics.Class == "IU")
                            {
                                txtYearNoCharge.Text = "0";
                                txtMonthNoCharge.Text = "0";
                            }
                            else
                            {
                                txtYearNoCharge.Text = "0";
                                txtMonthNoCharge.Text = "2";
                            }
                        }
                        else
                        {
                            if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                                MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                                MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "MP" ||
                                MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                                MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                                MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                            {
                                txtYearNoCharge.Text = "1";
                                txtMonthNoCharge.Text = "0";
                            }
                            else if (MotorVehicle.Statics.Class == "IU")
                            {
                                txtYearNoCharge.Text = "0";
                                txtMonthNoCharge.Text = "0";
                            }
                            else
                            {
                                txtYearNoCharge.Text = "2";
                                txtMonthNoCharge.Text = "0";
                            }
                        }
                    }
                    else
                    {
                        txtYearNoCharge.Text = "0";
                        txtMonthNoCharge.Text = "0";
                    }

                    if (blnChargeForPlate)
                    {
                        txtYearCharge.Text = txtYearNoCharge.Text;
                        txtMonthCharge.Text = txtMonthNoCharge.Text;
                        txtMonthNoCharge.Text = "";
                        txtYearNoCharge.Text = "";
                    }

                    lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                    lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                    txtYStickerNumber.Enabled = true;
                    txtMStickerNumber.Enabled = true;
                    if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) && MotorVehicle.Statics.FixedMonth)
                    {
                        txtMonthCharge.Text = "";
                        txtMonthNoCharge.Text = "";
                    }
                }
                else if (MotorVehicle.Statics.RegistrationType == "NRT")
                {
                    if ((MotorVehicle.Statics.PlateType == 2 || MotorVehicle.Statics.PlateType == 3 ||
                         MotorVehicle.Statics.PlateType == 4) && !MotorVehicle.Statics.LostPlate)
                    {
                        if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                            MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                            MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "MP" ||
                            MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                            MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                            MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                        {
                            txtYearNoCharge.Text = "1";
                            txtMonthNoCharge.Text = "1";
                        }
                        else if (MotorVehicle.Statics.Class == "IU")
                        {
                            txtYearNoCharge.Text = "0";
                            txtMonthNoCharge.Text = "2";
                        }
                        else
                        {
                            txtYearNoCharge.Text = "2";
                            txtMonthNoCharge.Text = "2";
                        }
                    }
                    else
                    {
                        txtYearNoCharge.Text = "0";
                        txtMonthNoCharge.Text = "0";
                    }

                    if (blnChargeForPlate)
                    {
                        txtYearCharge.Text = txtYearNoCharge.Text;
                        txtMonthCharge.Text = txtMonthNoCharge.Text;
                        txtMonthNoCharge.Text = "";
                        txtYearNoCharge.Text = "";
                    }

                    lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                    lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                    txtYStickerNumber.Enabled = true;
                    txtMStickerNumber.Enabled = true;
                    if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) && MotorVehicle.Statics.FixedMonth)
                    {
                        txtMonthCharge.Text = "";
                        txtMonthNoCharge.Text = "";
                    }
                }
                else if (MotorVehicle.Statics.RegistrationType == "NRR")
                {
                    if (MotorVehicle.Statics.TownLevel == 9)
                    {
                        txtYearNoCharge.Text = "0";
                        txtMonthNoCharge.Text = "0";
                    }
                    else
                    {
                        if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "PM" ||
                            MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                            MotorVehicle.Statics.Class == "MP" || MotorVehicle.Statics.Class == "VM" ||
                            MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                            MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                            MotorVehicle.Statics.Class == "XV" || MotorVehicle.Statics.Class == "TT")
                        {
                            txtYearNoCharge.Text = "1";
                            txtMonthNoCharge.Text = "1";
                        }
                        else if (MotorVehicle.Statics.Class == "IU")
                        {
                            txtYearNoCharge.Text = "0";
                            txtMonthNoCharge.Text = "2";
                        }
                        else
                        {
                            txtYearNoCharge.Text = "2";
                            txtMonthNoCharge.Text = "2";
                        }
                    }

                    lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                    lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                    txtYStickerNumber.Enabled = true;
                    txtMStickerNumber.Enabled = true;
                    if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) && MotorVehicle.Statics.FixedMonth)
                    {
                        txtMonthCharge.Text = "";
                        txtMonthNoCharge.Text = "";
                    }
                }
                else if (MotorVehicle.Statics.RegistrationType == "GVW")
                {
                    txtYearNoCharge.ReadOnly = true;
                    txtMonthNoCharge.ReadOnly = true;
                    txtYearCharge.ReadOnly = true;
                    txtMonthCharge.ReadOnly = true;
                    lblNumberofYear.Text = "";
                    lblNumberofMonth.Text = "";
                    txtYStickerNumber.ReadOnly = true;
                    txtMStickerNumber.ReadOnly = true;
                }
            }
            else
            {
                txtYearNoCharge.Enabled = false;
                txtMonthNoCharge.Enabled = false;
                txtYearCharge.Enabled = false;
                txtMonthCharge.Enabled = false;
                lblNumberofYear.Text = "";
                lblNumberofMonth.Text = "";
                txtYStickerNumber.ReadOnly = true;
                txtMStickerNumber.ReadOnly = true;
                txtYearNoCharge.BackColor = ColorTranslator.FromOle(OldColor);
                txtMonthNoCharge.BackColor = ColorTranslator.FromOle(OldColor);
                txtYearCharge.BackColor = ColorTranslator.FromOle(OldColor);
                txtMonthCharge.BackColor = ColorTranslator.FromOle(OldColor);
            }
        }

        public void FillOwnerInfo()
        {
            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "")
            {
                txtReg1ICM.Text = "I";
            }
            else
            {
                txtReg1ICM.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "")
            {
                txtReg2ICM.Text = "N";
            }
            else
            {
                txtReg2ICM.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "")
            {
                txtReg3ICM.Text = "N";
            }
            else
            {
                txtReg3ICM.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "")
            {
                txtReg1LR.Text = "N";
            }
            else
            {
                txtReg1LR.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "")
            {
                txtReg2LR.Text = "N";
            }
            else
            {
                txtReg2LR.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "")
            {
                txtReg3LR.Text = "N";
            }
            else
            {
                txtReg3LR.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3"));
            }

            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1")) != 0)
            {
                txtReg1PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"));
                GetPartyInfo(FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"))), 1);
            }
            else
            {
                ClearPartyInfo(1);
            }

            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2")) != 0)
            {
                txtReg2PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
                GetPartyInfo(FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"))), 2);
            }
            else
            {
                ClearPartyInfo(2);
            }

            var partyId3 = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID3");

            if (Conversion.Val(partyId3) != 0)
            {
                txtReg3PartyID.Text = FCConvert.ToString(partyId3);
                GetPartyInfo(FCConvert.ToInt32(Conversion.Val(partyId3)), 3);
            }
            else
            {
                ClearPartyInfo(3);
            }

            var dob1 = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1");

            if (fecherFoundation.FCUtils.IsNull(dob1) != true && dob1 != DateTime.FromOADate(0))
            {
                txtReg1DOB.Text = Strings.Format(dob1, "MM/dd/yyyy");
            }

            var dob2 = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2");

            if (fecherFoundation.FCUtils.IsNull(dob2) != true && dob2 != DateTime.FromOADate(0))
            {
                txtReg2DOB.Text = Strings.Format(dob2, "MM/dd/yyyy");
            }

            var dob3 = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3");

            if (fecherFoundation.FCUtils.IsNull(dob3) != true && dob3 != DateTime.FromOADate(0))
            {
                txtReg3DOB.Text = Strings.Format(dob3, "MM/dd/yyyy");
            }

            txtAddress1.Text = MotorVehicle.Statics.blnSuspended ? "REGISTRATION SUSPENDED" : fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));

            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address2")) != "")
            {
                txtAddress2.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address2"));
            }

            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("City")) != "")
            {
                txtCity.Text = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("State")) != "")
            {
                txtState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("State"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip")) != "")
            {
                txtZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Country")) != "")
            {
                txtCountry.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Country")).Length <= 2 ?
	                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Country")) :
	                "";
            }

            var muniName = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName));

            switch (muniName)
            {
                case "MAINE MOTOR TRANSPORT":
                    txtLegalres.Text = "MMTA SERVICES INC. 44004";

                    break;
                case "ACE REGISTRATION SERVICES LLC":
                    txtLegalres.Text = "ACE REGISTRATION";

                    break;
                case "STAAB AGENCY":
                    txtLegalres.Text = "STAAB AGENCY";

                    break;
                case "COUNTRYWIDE TRAILER":
                    txtLegalres.Text = "COUNTRYWIDE TRAILER 44018";

                    break;
                case "AB LEDUE ENTERPRISES":
                    txtLegalres.Text = "AB LEDUE ENTERPRISES";

                    break;
                case "MAINE TRAILER":
                    txtLegalres.Text = "MAINE TRAILER ME 44005";

                    break;
                case "HASKELL REGISTRATION":
                    txtLegalres.Text = "HASKELL REGISTRATION 44002";

                    break;

                default:
                {
                    var residence = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));

                    if (residence != "")
                    {
                        txtLegalres.Text = residence;
                    }

                    break;
                }
            }

            var residenceAddress = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceAddress"));

            if (residenceAddress != "")
            {
                txtLegalResStreet.Text = residenceAddress;
            }

            var residenceCountry = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCountry"));

            if (residenceCountry != "")
            {
                txtResCountry.Text = residenceCountry.Length <= 2 ? residenceCountry : "";
            }

            if (muniName == "ACE REGISTRATION SERVICES LLC")
            {
                txtresidenceCode.Text = "44021";
            }
            else if (muniName == "AB LEDUE ENTERPRISES")
            {
                txtresidenceCode.Text = "44003";
            }
            else
            {
                var residenceCode = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));

                if (residenceCode != "")
                {
                    if (residenceCode.Length < 5)
                    {
                        txtresidenceCode.Text = "0" + residenceCode;
                    }
                    else
                    {
                        txtresidenceCode.Text = residenceCode; 
                    }
                }
            }

            if (muniName == "ACE REGISTRATION SERVICES LLC")
            {
                txtResState.Text = "ME";
            }
            else if (muniName == "AB LEDUE ENTERPRISES")
            {
                txtResState.Text = "ME";
            }
            else
            {
                var residenceState = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));

                if (residenceState != "")
                {
                    txtResState.Text = residenceState;
                }
            }

            txtTaxIDNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber"));
        }

        public void BottomOfScreen()
        {
            MotorVehicle.Statics.strSql = "SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class +
                                          "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'";
            using (clsDRWrapper rsBOS = new clsDRWrapper())
            {
                rsBOS.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsBOS.EndOfFile() != true && rsBOS.BeginningOfFile() != true)
                {
                    rsBOS.MoveLast();
                    rsBOS.MoveFirst();
                }

                if (MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "NRT")
                {
                    chk2Year.CheckState = Wisej.Web.CheckState.Unchecked;
                    chk2Year.Enabled = false;
                }
                else
                {
                    if (FCConvert.ToString(rsBOS.Get_Fields_String("TwoYear")) == "Y")
                    {
                        if (MotorVehicle.Statics.RegistrationType == "CORR")
                        {
                            // do nothing
                        }
                        else
                        {
                            chk2Year.CheckState = Wisej.Web.CheckState.Unchecked;
                        }

                        chk2Year.Enabled = true;
                    }
                    else
                    {
                        chk2Year.CheckState = Wisej.Web.CheckState.Unchecked;
                        chk2Year.Enabled = false;
                    }
                }
            }
            
            if (MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "MC" ||
                MotorVehicle.Statics.Class == "MX" || MotorVehicle.Statics.Class == "PM" ||
                MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "XV")
            {
                chkCC.Enabled = true;
            }
            else
            {
                chkCC.Enabled = false;
            }

            if (MotorVehicle.Statics.AllowRentals)
            {
                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RENTAL")))
                {
                    chkRental.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkRental.CheckState = Wisej.Web.CheckState.Unchecked;
                }
            }

            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")) != 0)
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld") == true ||
                    MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") == true)
                {
                    // do nothing
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("FleetOld", true);
                }
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld") == true ||
                MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") == true)
            {
                using (clsDRWrapper rsFleet = new clsDRWrapper())
                {
                    rsFleet.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND FleetNumber = " +
                                          MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
                    if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
                    {
                        if (FCConvert.ToInt32(rsFleet.Get_Fields_Int32("ExpiryMonth")) == 99)
                        {
                            SetFleetGroupType(FleetGroupType.Group);
                        }
                        else
                        {
                            SetFleetGroupType(FleetGroupType.Fleet);
                        }
                    }
                    else
                    {
                        SetFleetGroupType(FleetGroupType.Fleet);
                    }
                }
                
                if (!MotorVehicle.Statics.rsFinal.IsFieldNull("FleetNumber"))
                {
                    txtFleetNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
                    if (MotorVehicle.Statics.RegistrationType == "NRT" ||
                        MotorVehicle.Statics.RegistrationType == "RRT")
                    {
                        // do nothing
                    }
                    else
                    {
                        txtFleetNumber_Validate(false);
                    }
                }
            }
            else if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.RegistrationType == "NRR")
            {
                if (MotorVehicle.Statics.GroupOrFleet == "G")
                {
                    SetFleetGroupType(FleetGroupType.Group);
                }
                else
                {
                    SetFleetGroupType(FleetGroupType.Fleet);
                }

                txtFleetNumber.Text = FCConvert.ToString(MotorVehicle.Statics.FleetBeingRegistered);
                txtFleetNumber_Validate(false);
            }
            else
            {
                ClearFleetGroup();
            }

            // Dave 2/27/2004
            if (MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "CL")
            {
                chkTrailerVanity.Enabled = true;
            }
            else
            {
                chkTrailerVanity.Enabled = false;
            }

            // 
            // Fill In TRAILERCODE
            MotorVehicle.Statics.rsFinal.Set_Fields("Trailer", "N");
            if (MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "CL")
            {
                if (chkExciseExempt.CheckState != Wisej.Web.CheckState.Checked)
                {
                    if (MotorVehicle.Statics.Subclass == "L6")
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("Trailer", "H");
                    }
                    else if (MotorVehicle.Statics.Subclass == "L1" || MotorVehicle.Statics.Subclass == "L3")
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("Trailer", "C");
                    }
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("Trailer", "U");
                }
            }
            else if (MotorVehicle.Statics.Class == "SE")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Trailer", "U");
            }
            else if (MotorVehicle.Statics.Class == "IU")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Trailer", "I");
            }
        }

        public bool CalculateDates()
        {
            bool CalculateDates = false;
            // vbPorter upgrade warning: xdate As DateTime	OnWrite(string, DateTime)
            DateTime xdate = default(DateTime);
            DateTime LowDate;
            DateTime HighDate;
            // vbPorter upgrade warning: ExpDate As string	OnWrite(DateTime, string)
            string ExpDate = "";
            // vbPorter upgrade warning: testMil As int	OnWrite(double, int)
            int testMil = 0;
            // vbPorter upgrade warning: testDate As DateTime	OnWrite(string)
            DateTime testDate;
            
            CalculateDates = true;
            // kk04142016 tromv-1145    If chkETO.Value = 0 Then
            var expirationDateOnPlate = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate");

            if (MotorVehicle.Statics.RegistrationType == "NRR")
            {
                if (MotorVehicle.Statics.RegisteringFleet)
                {
                    txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yyyy");
                    xdate = FCConvert.ToDateTime(Strings.Format(
                        fecherFoundation.DateAndTime.DateAdd("m", 1, MotorVehicle.Statics.PreEffectiveDate),
                        "MM/dd/yyyy"));
                    xdate = FCConvert.ToDateTime(Strings.Format(fecherFoundation.DateAndTime.DateAdd("yyyy", 1, xdate),
                        "MM/dd/yyyy"));
                    xdate = fecherFoundation.DateAndTime.DateAdd("d", -(MotorVehicle.Statics.PreEffectiveDate.Day),
                        xdate);
                }
                else
                {
                    txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                    if (MotorVehicle.Statics.FixedMonth &&
                        DateTime.Now.Month == MotorVehicle.Statics.gintFixedMonthMonth - 1)
                    {
                        if (MotorVehicle.Statics.gintFixedMonthMonth < 12)
                        {
                            xdate = FCConvert.ToDateTime(
                                FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                FCConvert.ToString(DateTime.Now.Year));
                        }
                        else
                        {
                            xdate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(DateTime.Now.Year + 1));
                        }

                        xdate = fecherFoundation.DateAndTime.DateAdd("d", -1, xdate);
                    }
                    else
                    {
                        xdate = FCConvert.ToDateTime(
                            Strings.Format(fecherFoundation.DateAndTime.DateAdd("m", 1, DateTime.Now), "MM/dd/yyyy"));
                        xdate = FCConvert.ToDateTime(
                            Strings.Format(fecherFoundation.DateAndTime.DateAdd("yyyy", 1, xdate), "MM/dd/yyyy"));
                        xdate = fecherFoundation.DateAndTime.DateAdd("d", -(DateTime.Now.Day), xdate);
                    }
                }

                txtExpires.Text = Strings.Format(xdate, "MM/dd/yyyy");
                if (MotorVehicle.Statics.PreviousETO)
                {
                    MotorVehicle.Statics.blnCheck = true;
                    chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                    MotorVehicle.Statics.blnCheck = false;
                }
            }
            else if (MotorVehicle.Statics.RegistrationType == "NRT")
            {
                if (MotorVehicle.Statics.rsFinal.IsntAnything())
                    return CalculateDates;
                txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"), "MM/dd/yyyy");
                xdate = expirationDateOnPlate;
            }
            else if (MotorVehicle.Statics.RegistrationType == "RRT")
            {
                if (MotorVehicle.Statics.rsFinal.IsntAnything())
                    return CalculateDates;
                txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"), "MM/dd/yyyy");
                xdate = expirationDateOnPlate;
            }
            else if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (MotorVehicle.Statics.rsFinal.IsntAnything() || !Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate")))
                    return CalculateDates;
                txtExpires.Text = Strings.Format(expirationDateOnPlate, "MM/dd/yyyy");
                xdate = expirationDateOnPlate;
            }
            else if (MotorVehicle.Statics.RegistrationType == "RRR")
            {
                if (MotorVehicle.Statics.rsFinal.IsntAnything())
                    return CalculateDates;
                redotag2:;
                //FC:FINAL:AM:#i1783 - check for the date
                //if (fecherFoundation.Strings.Trim(plate.Get_Fields("ExpireDate")) == "")
                if (!Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate")))
                {
                    object temp = ExpDate;
                    frmInput.InstancePtr.Init(ref temp, "Enter Current Expiration Date",
                        "This registration has no expiration date associated with it. Please enter the current expiration date.  MM/dd/yyyy'",
                        1700, false, modGlobalConstants.InputDTypes.idtDate);
                    ExpDate = temp.ToString();
                    //FC:FINAL:DDU:#i1953 - fixed datetime comparison
                    //if (ExpDate == "" || ExpDate == "12:00:00 AM")
                    if (ExpDate == Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
                    {
                        modGNBas.Statics.Response = "BAD";
                        CalculateDates = false;
                        return CalculateDates;
                    }
                    else
                    {
                        if (Information.IsDate(ExpDate) == false)
                        {
                            ShowWarningBox("You have entered an invalid date.", "Invalid Date");
                            goto redotag2;
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", ExpDate);
                            MotorVehicle.Statics.rsFinalCompare.Edit();
                            MotorVehicle.Statics.rsFinalCompare.Set_Fields("ExpireDate", ExpDate);
                            MotorVehicle.Statics.rsFinalCompare.Update();
                        }
                    }
                }

                if (MotorVehicle.Statics.NewToTheSystem == true)
                {
                    CalculateDates = true;
                    if (MotorVehicle.Statics.RegisteringFleet)
                    {
                        txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yyyy");
                    }
                    else
                    {
                        txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                    }

                    txtExpires.Text = Strings.Format(expirationDateOnPlate, "MM/dd/yyyy");
                    return CalculateDates;
                }

                if ((expirationDateOnPlate.Month == DateTime.Today.Month) &&
                    (expirationDateOnPlate.Year == (DateTime.Today.Year - 1)))
                {
                    xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", 1, expirationDateOnPlate);
                    if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")))
                    {
                        MotorVehicle.Statics.datMotorCycleExpires = xdate;
                        MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate",
                            fecherFoundation.DateAndTime.DateAdd("yyyy", 1,
                                MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")));
                        MotorVehicle.Statics.FixedMonth = true;
                        MotorVehicle.Statics.gintFixedMonthMonth = 3;
                    }
                }
                else
                {
                    xdate = FCConvert.ToDateTime(expirationDateOnPlate);
                }

            redoalltag:;
                LowDate = fecherFoundation.DateAndTime.DateAdd("m", -6, xdate);
                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS")
                {
                    ExpDate = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("yyyy", 10, xdate));
                    HighDate = fecherFoundation.DateAndTime.DateAdd("m", 120, xdate);
                }
                else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "ST")
                {
                    ExpDate = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("yyyy", 6, xdate));
                    HighDate = fecherFoundation.DateAndTime.DateAdd("m", 72, xdate);
                }
                else
                {
                    if (!MotorVehicle.Statics.TwoYear)
                    {
                        ExpDate = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("yyyy", 1, xdate));
                    }
                    else
                    {
                        ExpDate = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("yyyy", 2, xdate));
                    }

                    HighDate = fecherFoundation.DateAndTime.DateAdd("m", 12, xdate);
                }

                if (MotorVehicle.Statics.PreviousETO)
                {
                    MotorVehicle.Statics.blnCheck = true;
                    chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                    MotorVehicle.Statics.blnCheck = false;
                    ExpDate = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
                }

                //App.DoEvents();
                if (MotorVehicle.Statics.TwoYear == false)
                {
                    if (LowDate.ToOADate() > DateTime.Today.ToOADate())
                    {
                        if (!MotorVehicle.Statics.PreviousETO)
                        {
                            if (!MotorVehicle.Statics.blnChangeToFleet)
                            {
                                ShowWarningBox(
                                    "This registration cannot be processed. The expiration date of " +
                                    FCConvert.ToString(xdate) + "\r\n" + "is greater than 6 months away from now.",
                                    "Cannot Process");
                                CalculateDates = false;
                                modGNBas.Statics.Response = "BAD";
                                return CalculateDates;
                            }
                        }
                        else
                        {
                            // 10/30/02 Dave
                            MotorVehicle.Statics.blnCheck = true;
                            chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                            MotorVehicle.Statics.blnCheck = false;
                            ExpDate = FCConvert.ToString(
                                MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
                        }
                    }
                }
                else if (MotorVehicle.Statics.TwoYear == true)
                {
                    if (fecherFoundation.DateAndTime.DateAdd("yyyy", -1, LowDate).ToOADate() >
                        DateTime.Today.ToOADate())
                    {
                        if (!MotorVehicle.Statics.PreviousETO)
                        {
                            ShowWarningBox(
                                "This Registration cannot be processed. The expiration date of " +
                                FCConvert.ToString(xdate) + "\r\n" + "is greater than 17 months away from now.",
                                "Cannot Process");
                            CalculateDates = false;
                            Close();
                            return CalculateDates;
                        }
                        else
                        {
                            // 10/30/02 Dave
                            MotorVehicle.Statics.blnCheck = true;
                            chkEAP.CheckState = Wisej.Web.CheckState.Checked;
                            MotorVehicle.Statics.blnCheck = false;
                            ExpDate = FCConvert.ToString(
                                MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
                        }
                    }
                }

                // If HighDate < Date Then
                if (xdate.ToOADate() < fecherFoundation.DateAndTime.DateAdd("m", -6, DateTime.Today).ToOADate())
                {
                    // tromv-1152 03.09.2017 kjr
                    if (MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                            MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) != 0)
                    {
                        MessageBox.Show(
                            "This vanity plate has been expired for more than 6 months and must be processed as an excise only transaction. If the customer has reapplied and can proceed with this registration, please deselect the excise only transaction box to complete the full registration.",
                            "Collect Excise Only", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        MotorVehicle.Statics.ETO = true;
                    }
                }

                if (HighDate.ToOADate() < DateTime.Today.ToOADate())
                {
                redotag:;
                    if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")))
                    {
                        MotorVehicle.Statics.FixedMonth = true;
                        MotorVehicle.Statics.gintFixedMonthMonth = 3;
                    }
                    else if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber") > 0)
                    {
                        using (clsDRWrapper rsFleet = new clsDRWrapper())
                        {
                            rsFleet.OpenRecordset("SELECT * FROM FleetMaster WHERE FleetNumber = " +
                                                  MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
                            if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsFleet.Get_Fields_Int32("ExpiryMonth")) != 99)
                                {
                                    MotorVehicle.Statics.FixedMonth = true;
                                    MotorVehicle.Statics.gintFixedMonthMonth =
                                        FCConvert.ToInt16(rsFleet.Get_Fields_Int32("ExpiryMonth"));
                                }
                            }
                        }
                    }

                    if (MotorVehicle.Statics.FixedMonth == false)
                    {
                        object temp = ExpDate;
                        frmInput.InstancePtr.Init(ref temp, "Enter New Expiration Date",
                            "This registration must be processed as a delayed registration. Please enter the new ONE year expiration date in the format.  'MM/dd/yyyy'",
                            1700, false, modGlobalConstants.InputDTypes.idtDate);
                        var tempDate = Convert.ToDateTime(Strings.Format(temp, "MM/dd/yyyy"));
                        tempDate = tempDate.LastDayOfMonth();
                        ExpDate = tempDate.FormatAndPadShortDate();
                        // ExpDate = InputBox("This registration must be processed as a delayed registration. Please enter the new ONE year expiration date in the format.  'MM/dd/yyyy'")
                    }
                    else
                    {
                        // frmInput.Init ExpDate, "Enter New Expiration Date", "This registration must be processed as a delayed registration. Please enter the new ONE year expiration date. Because of the class, the expiration month must be " & Format(gintFixedMonthMonth, "00") & ".  MM/dd/yyyy'", 1200, , idtDate
                        // ExpDate = InputBox("This registration must be processed as a delayed registration. Please enter the new ONE year expiration date. Because of the class, the expiration month must be 02.  MM/dd/yyyy'")
                        if (DateTime.Today.Month >= MotorVehicle.Statics.gintFixedMonthMonth)
                        {
                            if (MotorVehicle.Statics.gintFixedMonthMonth == 12)
                            {
                                ExpDate = "1/1/" + FCConvert.ToString(DateTime.Today.Year + 2);
                            }
                            else
                            {
                                ExpDate = FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                          FCConvert.ToString(DateTime.Today.Year + 1);
                            }
                        }
                        else
                        {
                            if (MotorVehicle.Statics.gintFixedMonthMonth == 12)
                            {
                                ExpDate = "1/1/" + FCConvert.ToString(DateTime.Today.Year + 1);
                            }
                            else
                            {
                                ExpDate = FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                          FCConvert.ToString(DateTime.Today.Year);
                            }
                        }

                        ExpDate = FCConvert.ToString(
                            fecherFoundation.DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(ExpDate)));
                    }

                    if (MotorVehicle.Statics.FixedMonth && FCConvert.ToDateTime(ExpDate).Month !=
                        MotorVehicle.Statics.gintFixedMonthMonth)
                    {
                        MessageBox.Show("You have entered an invalid date.", "Invalid Date", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        goto redotag;
                    }

                    MotorVehicle.Statics.gblnDelayedReg = true;
                    if (ExpDate == Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
                    {
                        modGNBas.Statics.Response = "BAD";
                        CalculateDates = false;
                        return CalculateDates;
                    }
                    else
                    {
                        if (Information.IsDate(ExpDate) == false)
                        {
                            MessageBox.Show("You have entered an invalid date.", "Invalid Date", MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                            goto redotag;
                        }
                        else
                        {
                            testDate = FCConvert.ToDateTime(FCConvert.ToString(FCConvert.ToDateTime(ExpDate).Month) +
                                                            "/1/" + FCConvert.ToString(
                                                                FCConvert.ToDateTime(ExpDate).Year - 1));
                            if (DateTime.Today.ToOADate() < testDate.ToOADate())
                            {
                                MessageBox.Show("You have entered an invalid date.", "Invalid Date",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                goto redotag;
                            }

                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI")
                            {
                                // do nothing
                            }
                            else
                            {
                                xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", -1, FCConvert.ToDateTime(ExpDate));
                                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Year")) != 0)
                                {
                                    testMil = FCConvert.ToInt16(
                                        (FCConvert.ToDateTime(ExpDate).Year - 1) -
                                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Year")));
                                    if (testMil > 6)
                                        testMil = 6;
                                }
                                else
                                {
                                    testMil = 1;
                                }

                                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear") > testMil)
                                {
                                    // do nothing
                                }
                                else
                                {
                                    MotorVehicle.Statics.rsFinal.Set_Fields("MillYear", testMil);
                                }

                                goto redoalltag;
                            }
                        }
                    }
                }
                else
                {
                    // DJW 3/14/12 Change to MC Legislation
                    if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) &&
                        MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() <
                        DateTime.Today.ToOADate() &&
                        MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() <
                        FCConvert.ToDateTime("3/1/2013").ToOADate() && MotorVehicle.Statics.RegistrationType == "RRR")
                    {
                        MotorVehicle.Statics.gblnDelayedReg = true;
                        // DJW 1/7/2014 added additional chekc to see if year ws the same but we were before march to keep the year as the current one
                        if (FCConvert.ToDateTime(ExpDate).Year > DateTime.Today.Year ||
                            (FCConvert.ToDateTime(ExpDate).Year == DateTime.Today.Year &&
                             DateTime.Today.Month < MotorVehicle.Statics.gintFixedMonthMonth))
                        {
                            if (FCConvert.ToDateTime(ExpDate).Month < MotorVehicle.Statics.gintFixedMonthMonth)
                            {
                                ExpDate = FCConvert.ToString(
                                    fecherFoundation.DateAndTime.DateValue(
                                        "3/31/" + FCConvert.ToString(FCConvert.ToDateTime(ExpDate).Year)));
                            }
                        }
                        else
                        {
                            ExpDate = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("yyyy", 1,
                                fecherFoundation.DateAndTime.DateValue(
                                    "3/31/" + FCConvert.ToString(FCConvert.ToDateTime(ExpDate).Year))));
                        }
                    }
                }

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" ||
                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CL")
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() >
                        DateTime.Today.ToOADate())
                    {
                        txtEffectiveDate.Text =
                            Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"),
                                "MM/dd/yyyy");
                    }
                    else
                    {
                        txtEffectiveDate.Text =
                            Strings.Format(
                                FCConvert.ToString(DateTime.Now.Month) + "01" + FCConvert.ToString(DateTime.Now.Year),
                                "00/00/0000");
                    }
                }
                else
                {
                    txtEffectiveDate.Text =
                        Strings.Format(
                            FCConvert.ToString(DateTime.Now.Month) + "01" + FCConvert.ToString(DateTime.Now.Year),
                            "00/00/0000");
                }

                if (MotorVehicle.Statics.PreviousETO && ExpDate ==
                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")))
                {
                    txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate"),
                        "MM/dd/yyyy");
                }

                xdate = FindLastDay(Strings.Format(
                    FCConvert.ToString(FCConvert.ToDateTime(ExpDate).Month) + "01" +
                    FCConvert.ToString(FCConvert.ToDateTime(ExpDate).Year), "00/00/0000"));
                txtExpires.Text = Strings.Format(xdate, "MM/dd/yyyy");
            }

            if (MotorVehicle.Statics.FixedMonth == true)
            {
                if (MotorVehicle.Statics.gintFixedMonthMonth < 12)
                {
                    // DJW@3/19/2013 TROMV-725 Changed Date to plate.Fields("ExpireDate")
                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate")))
                    {
                        // DJW@changed xdate to plate.Fields("ExpireDate") becuase mc regs expiring in 3/31/2013 were getting pushed to 2015 expiration
                        if (MotorVehicle.Statics.gintFixedMonthMonth == expirationDateOnPlate.Month &&
                            xdate.Year == DateTime.Today.Year &&
                            expirationDateOnPlate.Year == DateTime.Today.Year)
                        {
                            if (MotorVehicle.Statics.RegistrationType == "RRR" ||
                                MotorVehicle.Statics.RegistrationType == "NRR")
                            {
                                xdate = FCConvert.ToDateTime(
                                    FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                    FCConvert.ToString(xdate.Year + 1));
                            }
                            else
                            {
                                xdate = FCConvert.ToDateTime(
                                    FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                    FCConvert.ToString(xdate.Year));
                            }
                        }
                        else if (xdate.Month < MotorVehicle.Statics.gintFixedMonthMonth &&
                                 xdate.Year > DateTime.Today.Year && !MotorVehicle.Statics.gblnDelayedReg)
                        {
                            xdate = FCConvert.ToDateTime(
                                FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                FCConvert.ToString(xdate.Year - 1));
                        }
                        else
                        {
                            xdate = FCConvert.ToDateTime(
                                FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                FCConvert.ToString(xdate.Year));
                        }
                    }
                    else
                    {
                        if (xdate.Month < MotorVehicle.Statics.gintFixedMonthMonth &&
                            xdate.Year > DateTime.Today.Year && !MotorVehicle.Statics.gblnDelayedReg)
                        {
                            xdate = FCConvert.ToDateTime(
                                FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                FCConvert.ToString(xdate.Year - 1));
                        }
                        else
                        {
                            xdate = FCConvert.ToDateTime(
                                FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                FCConvert.ToString(xdate.Year));
                        }
                    }
                }
                else
                {
                    xdate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(xdate.Year + 1));
                }

                xdate = fecherFoundation.DateAndTime.DateAdd("d", -1, xdate);
                if (xdate.ToOADate() < DateTime.Today.ToOADate())
                {
                    MessageBox.Show("Invalid expiration. Please contact TRIO.", "Invalid Date", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    modGNBas.Statics.Response = "BAD";
                    CalculateDates = false;
                    return CalculateDates;
                }

                // xdate = CDate(strDate & CStr(Year(xdate)))
                txtExpires.Text = Strings.Format(xdate, "MM/dd/yyyy");
            }

            // kk04142016    End If
            return CalculateDates;
        }

        public void ClearDates()
        {
            MotorVehicle.Statics.RememberedDate2 = txtEffectiveDate.Text;
            txtEffectiveDate.Text = "";
            MotorVehicle.Statics.RememberedDate1 = txtExpires.Text;
            txtExpires.Text = "";
        }

        public bool VerifyVIN()
        {
            bool VerifyVIN = false;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            
            VerifyVIN = true;
            if (fecherFoundation.Strings.Trim(txtVin.Text) == "")
            {
                MessageBox.Show("You must enter a VIN before you may proceed.", "No VIN", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyVIN = false;
                return VerifyVIN;
            }

            if (Strings.InStr(1, fecherFoundation.Strings.Trim(txtVin.Text), "_", CompareConstants.vbBinaryCompare) !=
                0)
            {
                MessageBox.Show("You must enter a valid VIN before you may proceed.", "Invalid VIN",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                VerifyVIN = false;
                return VerifyVIN;
            }

            if (fecherFoundation.Strings.Trim(txtVin.Text) == "HMDE")
            {
                ShowWarningBox("You may not enter HMDE as a VIN.  You must enter a valid VIN before you may proceed.",
                    "Invalid VIN");
                VerifyVIN = false;
                return VerifyVIN;
            }

            if (MotorVehicle.Statics.RegistrationType == "NRT")
            {
                // kk12222015 This isn't working because the VIN isn't in the Compare record.?
                if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Vin")) ==
                    fecherFoundation.Strings.Trim(txtVin.Text))
                {
                    ans = MessageBox.Show(
                        "The VIN you entered is the same VIN the transfer vehicle had.  Would you like to continue?",
                        "Duplicate VIN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        VerifyVIN = false;
                        txtVin.Focus();
                        return VerifyVIN;
                    }
                }
            }

            if (txtVin.Text != "0" && fecherFoundation.Strings.UCase(txtVin.Text) != "NONE" &&
                fecherFoundation.Strings.UCase(txtVin.Text) != "HOMEMADE" &&
                fecherFoundation.Strings.UCase(txtVin.Text) != "HOME MADE")
            {
                using (clsDRWrapper rsCheck = new clsDRWrapper())
                {
                    if (!MotorVehicle.Statics.HeldReg)
                    {
                        rsCheck.OpenRecordset("SELECT * FROM MASTER WHERE ExpireDate > '" +
                                              DateTime.Today.ToShortDateString() + "' AND VIN = '" + txtVin.Text +
                                              "' AND ID <> " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                    }
                    else
                    {
                        rsCheck.OpenRecordset("SELECT * FROM MASTER WHERE ExpireDate > '" +
                                              DateTime.Today.ToShortDateString() + "' AND VIN = '" + txtVin.Text +
                                              "' AND ID <> " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("masterID"));
                    }

                    if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
                    {
                        ans = MessageBox.Show(
                            "The VIN you entered is the same VIN that is registered on the following vehicle." + "\r\n" +
                            "\r\n" + "Class: " + rsCheck.Get_Fields_String("Class") + "\r\n" + "Plate: " +
                            rsCheck.Get_Fields_String("Plate") + "\r\n" + "Expires: " +
                            Strings.Format(rsCheck.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy") + "\r\n" + "\r\n" +
                            "Would you like to continue?", "Duplicate VIN", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);
                        if (ans == DialogResult.No)
                        {
                            VerifyVIN = false;
                            txtVin.Focus();
                            return VerifyVIN;
                        }
                    }
                }
            }

            if (chkVIN.CheckState == Wisej.Web.CheckState.Checked)
                return VerifyVIN;
            // 
            if (blnFinalCheck)
            {
                if ((MotorVehicle.Statics.Class == "MH") || (MotorVehicle.Statics.Class == "RV") ||
                    (MotorVehicle.Statics.Class == "WX") || (MotorVehicle.Statics.Class == "TC") ||
                    (MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "CL") ||
                    (MotorVehicle.Statics.Class == "TL") || (MotorVehicle.Statics.Class == "CV"))
                {
                }
                else
                {
                    if (Conversion.Val(txtYear.Text) > 1980 || txtYear.Text == "")
                    {
                        if (CheckDigit(txtVin.Text) == false)
                        {
                            DialogResult answer;
                            answer = MessageBox.Show(
                                "The VIN,  " + txtVin.Text +
                                "  is Invalid for this Vehicle and therefore may have some incorrect data.  Please double check the vehicle information before the registration is processed.  Should this VIN be used?",
                                "Invalid VIN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (answer == DialogResult.No)
                            {
                                txtVin.TabStop = true;
                                VerifyVIN = false;
                                chkVIN.CheckState = Wisej.Web.CheckState.Unchecked;
                                chkVIN.Visible = false;
                                txtVin.Focus();
                                return false;
                            }
                            else
                            {
                                txtVin.TabStop = false;
                                chkVIN.Visible = true;
                                chkVIN.CheckState = Wisej.Web.CheckState.Checked;
                                return true;
                            }
                        }
                    }
                }
            }

            return VerifyVIN;
        }

        public bool CheckDigit(string hold)
        {
            bool CheckDigit = false;
            // vbPorter upgrade warning: cd As int	OnWriteFCConvert.ToInt32(
            int[] cd = new int[17 + 1];
            int fnx;
            string OneDigit = "";
            float cdvalue;
            float cdOperand;
            float cdOperand2;
            CheckDigit = false;
            for (fnx = 1; fnx <= 17; fnx++)
            {
                OneDigit = Strings.Mid(hold, fnx, 1);
                if (string.Compare(OneDigit, "0") < 0)
                    return CheckDigit;
                if (string.Compare(OneDigit, "9") > 0)
                {
                    if (string.Compare(OneDigit, "A") < 0)
                        return CheckDigit;
                    if (string.Compare(OneDigit, "Z") > 0)
                        return CheckDigit;
                    if (string.Compare(OneDigit, "I") < 0)
                    {
                        cd[fnx] = (Convert.ToByte(OneDigit[0]) - 64);
                    }
                    else if (string.Compare(OneDigit, "S") < 0)
                    {
                        cd[fnx] = (Convert.ToByte(OneDigit[0]) - 73);
                    }
                    else
                    {
                        cd[fnx] = (Convert.ToByte(OneDigit[0]) - 81);
                    }
                }
                else
                {
                    cd[fnx] = FCConvert.ToInt32(Math.Round(Conversion.Val(OneDigit)));
                }
            }

            // fnx
            cdvalue = cd[1] * 8;
            cdvalue += (cd[2] * 7);
            cdvalue += (cd[3] * 6);
            cdvalue += (cd[4] * 5);
            cdvalue += (cd[5] * 4);
            cdvalue += (cd[6] * 3);
            cdvalue += (cd[7] * 2);
            cdvalue += (cd[8] * 10);
            cdvalue += (cd[10] * 9);
            cdvalue += (cd[11] * 8);
            cdvalue += (cd[12] * 7);
            cdvalue += (cd[13] * 6);
            cdvalue += (cd[14] * 5);
            cdvalue += (cd[15] * 4);
            cdvalue += (cd[16] * 3);
            cdvalue += (cd[17] * 2);
            if (cd[9] == FCUtils.iMod(cdvalue, 11))
                CheckDigit = true;
            if (FCUtils.iMod(cdvalue, 11) == 10 && Strings.Mid(hold, 9, 1) == "X")
                CheckDigit = true;
            return CheckDigit;
        }

        public bool VerifyColor()
        {
            bool VerifyColor = false;
            clsDRWrapper rsCLTL = new clsDRWrapper();
            clsDRWrapper rsColor = new clsDRWrapper();
            string Color1 = "";
            string Color2 = "";

            try
            {
                DialogResult answer = 0;
                VerifyColor = true;
                if (MotorVehicle.Statics.Class == "AP" && txtColor1.Text.Trim() == "" && (txtColor2.Text.Trim() == "" || txtColor2.Text.Trim() == "NA"))
                {
                    return VerifyColor;
                }
                if (txtColor1.Text != "")
                    Color1 = Strings.Trim(txtColor1.Text);
                if (txtColor2.Text != "")
                    Color2 = Strings.Trim(txtColor2.Text);
                
                MotorVehicle.Statics.strSql = "SELECT * FROM Color";
                rsColor.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsColor.EndOfFile() != true && rsColor.BeginningOfFile() != true)
                {
                    rsColor.MoveLast();
                    rsColor.MoveFirst();
                }
                else
                {
                   ShowWarningBox("The Color table has no Entries in it.", "Empty Table");
                    return VerifyColor;
                }

                if (Color1 != "")
                {
                    if (!rsColor.FindFirstRecord("Code", Color1))
                    {
                        VerifyColor = false;
                        answer = MessageBox.Show("Invalid Color code in Field 1.  Would you ike to see a list of all the Valid Colors?", "Color Code Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (answer == DialogResult.Yes)
                        {
                            GetCode("Color");
                        }

                        txtColor1.TabStop = true;
                    }
                }

                if (Color2 != "")
                {
                    if (!rsColor.FindFirstRecord("Code", Color2))
                    {
                        ShowWarningBox("Invalid Color code in Field 2.", "Invalid Color");
                        VerifyColor = false;
                        txtColor2.TabStop = true;
                    }
                }

                if (Color1 == "")
                {
                    ShowWarningBox("Please input a valid Color One.", "No Color");
                    VerifyColor = false;
                    return VerifyColor;
                }
                else if (Color2 == "")
                {
                    txtColor2.Text = "NA";
                }

                return VerifyColor;
            }
            finally
            {
                rsCLTL.Dispose();
                rsColor.Dispose();
            }
        }

        public bool VerifyYear()
        {
            bool VerifyYear = false;
            string VinText = "";
            int YearNumber;
            string YearString = "";
            bool blnOK;
            YearNumber = 0;
            VerifyYear = true;
            if (txtYear.Text.Length < 4)
            {
                VerifyYear = false;
                ShowWarningBox("There must be a valid Year.", "Invalid Year");
                return VerifyYear;
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CV")
            {
                if (Conversion.Val(txtYear.Text) + 25 > DateTime.Now.Year || Conversion.Val(txtYear.Text) < 1949)
                {
                    // Bp = Beep(1000, 250)
                    ShowWarningBox(
                        "Custom Vehicles must be at least 25 years old and cannot be older than model year 1949.",
                        "Modified Vehicle");
                    VerifyYear = false;
                    txtYear.TabStop = true;
                    return VerifyYear;
                }
            }
            else
            {
                if (Conversion.Val(txtYear.Text) < 1981 && Conversion.Val(txtYear.Text) != 0)
                    return VerifyYear;
                VinText = fecherFoundation.Strings.Trim(txtVin.Text);

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AQ" ||
                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "MQ")
                {
                    // tromv-1240 03.16.17 kjr
                    if (Conversion.Val(txtYear.Text) + 25 > DateTime.Now.Year)
                    {
                        // Bp = Beep(1000, 250)
                        ShowWarningBox("The Vehicle must be at least 25 years old to be an Antique.",
                            "Antique Vehicle");
                        VerifyYear = false;
                        txtYear.TabStop = true;
                        return VerifyYear;
                    }
                }

                if (!VerifyYearFromVinIsValid(txtYear.Text.ToIntegerValue(), VinText))
                {
                    ShowWarningBox(
                        "The Year Digit in the VIN does not match the keyed in Vehicle Year." + "\r\n" +
                        "Please check your numbers.", "Year Error");
                }
            }

            return VerifyYear;
        }

        public bool VerifyMake()
        {
            bool VerifyMake = false;
            
            string make;
            // vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
            DialogResult answer = 0;
            // 
            VerifyMake = true;
            make = fecherFoundation.Strings.Trim(txtMake.Text);
            // 
            if ((MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "TC") ||
                (MotorVehicle.Statics.Class == "TL") || (MotorVehicle.Statics.Class == "CL") ||
                (MotorVehicle.Statics.Class == "CI") || (MotorVehicle.Statics.Class == "CV"))
            {
                // do nothing
            }
            else
            {
                if (make == "")
                {
                    // Bp = Beep(1000, 250)
                    MessageBox.Show("There must be a Make entered for this vehicle.", "Make Required",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyMake = false;
                    txtMake.TabStop = true;
                    return VerifyMake;
                }

                MotorVehicle.Statics.strSql = "SELECT RTRIM(Code) AS Code FROM Make ORDER BY Code";

                using (clsDRWrapper rsMake = new clsDRWrapper())
                {
                    rsMake.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsMake.EndOfFile() != true && rsMake.BeginningOfFile() != true)
                    {
                        rsMake.MoveLast();
                        rsMake.MoveFirst();
                        if (!rsMake.FindFirstRecord("Code", make))
                        {
                            VerifyMake = false;
                            txtMake.TabStop = true;
                            answer = MessageBox.Show(
                                "That is not a valid make.  Would you like to see a list of the Valid Makes?",
                                "Invalid Make", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (answer == DialogResult.Yes)
                            {
                                GetCode("Make");
                            }
                        }
                        else
                        {
                            if (MotorVehicle.Statics.Class == "MH" || MotorVehicle.Statics.Class == "RV" ||
                                MotorVehicle.Statics.Class == "X")
                            {
                                // msgbox for body
                            }
                        }
                    }
                    else
                    {
                        // Bp = Beep(1000, 250)
                        MessageBox.Show("There are no Entries in the Make Table.", "Table Error", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        VerifyMake = false;
                        txtMake.TabStop = true;
                        return VerifyMake;
                    }
                }
            }

            return VerifyMake;
        }

        public bool VerifyModel()
        {
            bool VerifyModel = false;
            VerifyModel = true;
            if ((MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "TC") ||
                (MotorVehicle.Statics.Class == "TL") || (MotorVehicle.Statics.Class == "CL"))
            {
                // do nothing
            }
            else
            {
                if (txtModel.Text == "")
                {
                    // Bp = Beep(1000, 250)
                    MessageBox.Show("There must be a Model Type for this vehicle.", "Model Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    VerifyModel = false;
                    txtModel.TabStop = true;
                    return VerifyModel;
                }
            }

            return VerifyModel;
        }

        public bool VerifyStyle()
        {
            string Style;
            DialogResult answer = 0;
            
            Style = fecherFoundation.Strings.Trim(txtStyle.Text);
            
            if (Style == "")
            {
                MessageBox.Show("There must be a style entered for this vehicle.", "Style Required",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtStyle.TabStop = true;
                return false;
            }
            else
            {
                if (styleCodeService.ValidateStyleCode(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                    MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"),
                    MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"), Style))
                {
                    return true;
                }
                else
                {
                    answer = MessageBox.Show(
                        "The style that has been entered is invalid for this vehicle type.  Would you like to see a list of the Valid Style Codes?",
                        "Invalid Style", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (answer == DialogResult.Yes)
                    {
                        GetCode("Style");
                    }

                    txtStyle.TabStop = true;
                    return false;
                }
            }
        }

        public bool VerifyTires()
        {
            bool VerifyTires = false;
            VerifyTires = true;
            return VerifyTires;
        }

        public bool VerifyAxles()
        {
            bool VerifyAxles = false;
            VerifyAxles = true;
            return VerifyAxles;
        }

        public bool VerifyNetWeight()
        {
            bool VerifyNetWeight = false;
            VerifyNetWeight = true;
            if (MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "AP")
            {
                // kk04142016 tromv-1145
                if (Conversion.Val(txtNetWeight.Text) == 0)
                {
                    MessageBox.Show("Net Weight is a required field for this vehicle.", "Net Weight Required",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyNetWeight = false;
                    txtNetWeight.TabStop = true;
                    txtNetWeight.Focus();
                }
            }
            else
            {
                txtNetWeight.Text = "";
            }

            if (MotorVehicle.Statics.RegistrationType == "RRR")
            {
                return VerifyNetWeight;
            }

            if (VerifyNetWeight == true)
                txtNetWeight.BackColor = ColorTranslator.FromOle(OldColor);
            return VerifyNetWeight;
        }

        public bool VerifyExciseExempt()
        {
            bool VerifyExciseExempt = false;
            using (clsDRWrapper rsClass = new clsDRWrapper())
            {
                rsClass.OpenRecordset(
                    "SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") +
                    "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'",
                    "TWMV0000.vb1");
                if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
                {
                    if (FCConvert.ToString(rsClass.Get_Fields_String("ExciseRequired")) == "Y" &&
                        chkExciseExempt.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        if (MessageBox.Show(
                                "You are attempting to process an Excise Exempt transaction for a non-excise exempt plate class. Would you like to continue?",
                                "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            VerifyExciseExempt = false;
                            return VerifyExciseExempt;
                        }
                    }
                    else if (FCConvert.ToString(rsClass.Get_Fields_String("ExciseRequired")) != "Y" &&
                             chkExciseExempt.CheckState == Wisej.Web.CheckState.Unchecked)
                    {
                        if (MessageBox.Show(
                                "You are attempting to process an excise transaction on an excise exempt plate class. Would you like to continue?",
                                "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            VerifyExciseExempt = false;
                            return VerifyExciseExempt;
                        }
                    }
                }
            }
            
            VerifyExciseExempt = true;
            return VerifyExciseExempt;
        }

        public bool VerifyRVW()
        {
            bool VerifyRVW = false;
            VerifyRVW = true;
            if ((MotorVehicle.Statics.Class == "AF") || (MotorVehicle.Statics.Class == "FM") ||
                (MotorVehicle.Statics.Class == "RV") || (MotorVehicle.Statics.Class == "WX") ||
                (MotorVehicle.Statics.Class == "MH"))
            {
                if (Conversion.Val(txtRegWeight.Text) > 69000)
                {
                    MessageBox.Show("Registered Vehicle Weight must be 69,000lbs or less.", "Invalid RVW",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyRVW = false;
                    txtRegWeight.TabStop = true;
                    return VerifyRVW;
                }
            }
            else
            {
                if (Conversion.Val(txtRegWeight.Text) > 100000)
                {
                    MessageBox.Show("Registered Vehicle Weight must be 100,000lbs or less.", "Invalid RVW",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyRVW = false;
                    txtRegWeight.TabStop = true;
                    return VerifyRVW;
                }
            }

            // kk04052018 tromv-1365/tromv-1322  RVW required for MH types
            if ((MotorVehicle.Statics.Class == "RV") || (MotorVehicle.Statics.Class == "WX") ||
                (MotorVehicle.Statics.Class == "MH"))
            {
                if (Conversion.Val(txtRegWeight.Text) == 0)
                {
                    MessageBox.Show("Registered Vehicle Weight is Required.", "Required Field", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    VerifyRVW = false;
                    txtRegWeight.TabStop = true;
                    return VerifyRVW;
                }
            }
            else if ((MotorVehicle.Statics.Class == "CC") || (MotorVehicle.Statics.Class == "CO") ||
                     (MotorVehicle.Statics.Class == "FM") || (MotorVehicle.Statics.Class == "AC") ||
                     (MotorVehicle.Statics.Class == "AF") || (MotorVehicle.Statics.Class == "TR") ||
                     (MotorVehicle.Statics.Class == "TT") || (MotorVehicle.Statics.Class == "LC"))
            {
                // kk04052018 tromv-1365  No 26000 lb limit for MH types   "RV", "WX", "MH"     'kk07242017 tromv-1322  Add MH
                if (MotorVehicle.Statics.TownLevel != 3 && MotorVehicle.Statics.TownLevel != 4 &&
                    !MotorVehicle.Statics.ETO && (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT"))
                {
                    // And (RegistrationType = "NRR" Or RegistrationType = "NRT") And Not ETO And (rsFinal.fields("Class")  = "CO" Or rsFinal.fields("Class")  = "CC" Or rsFinal.fields("Class")  = "FM") Then
                    if (Conversion.Val(txtRegWeight.Text) > 26000)
                    {
                        ShowWarningBox(
                            "Because of your town level you may not process a registration on a vehicle with a registered weight greater than 26000 lbs.",
                            "Invalid RVW");
                        VerifyRVW = false;
                        txtRegWeight.TabStop = true;
                        return VerifyRVW;
                    }
                }

                if (MotorVehicle.Statics.Class == "TT")
                {
                    if (Conversion.Val(txtRegWeight.Text) < 23001)
                    {
                        ShowWarningBox("A commercial tractor must be registered with a weight greater than 23000 lbs.",
                            "Invalid RVW");
                        VerifyRVW = false;
                        txtRegWeight.TabStop = true;
                        return VerifyRVW;
                    }
                }

                if (MotorVehicle.Statics.Subclass == "T2" || MotorVehicle.Statics.Subclass == "T5" ||
                    MotorVehicle.Statics.Subclass == "T4")
                {
                    if (Conversion.Val(txtRegWeight.Text) != 0)
                    {
                        MessageBox.Show("You may not enter a Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }

                    return VerifyRVW;
                }

                if (Conversion.Val(txtRegWeight.Text) == 0)
                {
                    MessageBox.Show("Registered Vehicle Weight is Required.", "Required Field", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    VerifyRVW = false;
                    txtRegWeight.TabStop = true;
                    return VerifyRVW;
                }
                else
                {
                    if ((MotorVehicle.Statics.Class == "CC") || (MotorVehicle.Statics.Class == "CO") ||
                        (MotorVehicle.Statics.Class == "FM") || (MotorVehicle.Statics.Class == "AC") ||
                        (MotorVehicle.Statics.Class == "AF") || (MotorVehicle.Statics.Class == "TT") ||
                        (MotorVehicle.Statics.Class == "LC"))
                    {
                        if (Conversion.Val(txtRegWeight.Text) > 54999)
                        {
                            if (MotorVehicle.Statics.HVUT == false)
                            {
                                MessageBox.Show("Be sure to see proof of HVUT.", "Proof required", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                                MotorVehicle.Statics.HVUT = true;
                            }
                        }
                    }

                    if (MotorVehicle.Statics.Class == "CC" || MotorVehicle.Statics.Class == "CO" ||
                        MotorVehicle.Statics.Class == "TT")
                    {
                        // kk12302017 tromvs-96   Should we prompt for DS,DV,VT and VX?
                        if (Conversion.Val(txtRegWeight.Text) > 10000 && Conversion.Val(txtRegWeight.Text) < 26001)
                        {
                            // vbPorter upgrade warning: DotAnswer As object	OnWrite(DialogResult)
                            DialogResult DotAnswer;
                            DotAnswer = MessageBox.Show(
                                "Does this Vehicle operate outside the State of Maine for the purposes of interstate commerce?",
                                "DOT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (DotAnswer == DialogResult.Yes)
                            {
                                DOTNumberRequired = true;
                            }
                            else
                            {
                                DOTNumberRequired = false;
                            }
                        }
                    }
                }

                // 
                if (MotorVehicle.Statics.Class != "AP")
                {
                    if (Conversion.Val(txtRegWeight.Text) < 2000 || Conversion.Val(txtRegWeight.Text) > 100000)
                    {
                        MessageBox.Show("Invalid Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                        txtRegWeight.TabStop = true;
                        return VerifyRVW;
                    }
                }
            }
            else if (MotorVehicle.Statics.Class == "PC")
            {
                // kk11202015 tromv-1111
                if (Strings.Right(MotorVehicle.Statics.Subclass, 1) != "3" &&
                    Strings.Right(MotorVehicle.Statics.Subclass, 1) != "8")
                {
                    if (Conversion.Val(txtRegWeight.Text) > 6000)
                    {
                        MessageBox.Show(
                            "You have entered a weight that is not supported by the subclass of the vehicle.  If this is the correct weight please exit this screen and be sure to select the correct subclass when prompted.",
                            "Invalid Registered Weight", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }

                    txtRegWeight.Text = "";
                    VerifyRVW = true;
                }
                else
                {
                    if (Conversion.Val(txtRegWeight.Text) > 6000 && Conversion.Val(txtRegWeight.Text) <= 10000)
                    {
                        VerifyRVW = true;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }
                }

            }
            else if (MotorVehicle.Statics.Class == "SE")
            {
                if (Conversion.Val(txtRegWeight.Text) != 0)
                {
                    MessageBox.Show("You may not enter a Registered Vehicle Weight for this Class.", "Invalid RVW",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyRVW = false;
                }
            }
            else if ((MotorVehicle.Statics.Class == "AG") || (MotorVehicle.Statics.Class == "CV") ||
                     (MotorVehicle.Statics.Class == "CR") || (MotorVehicle.Statics.Class == "CD") ||
                     (MotorVehicle.Statics.Class == "CM") || (MotorVehicle.Statics.Class == "DX") ||
                     (MotorVehicle.Statics.Class == "EM") || (MotorVehicle.Statics.Class == "FD") ||
                     (MotorVehicle.Statics.Class == "GS") || (MotorVehicle.Statics.Class == "MO") ||
                     (MotorVehicle.Statics.Class == "PH") || (MotorVehicle.Statics.Class == "PO") ||
                     (MotorVehicle.Statics.Class == "PS") || (MotorVehicle.Statics.Class == "WB"))
            {
                // kk12222017 tromvs-96  Moved DS and VT down with DV and VX tromvs-97 6.25.18 added EM
                if (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "1")
                {
                    if (Conversion.Val(txtRegWeight.Text) > 6000)
                    {
                        MessageBox.Show(
                            "You have entered a weight that is not supported by the subclass of the vehicle.  If this is the correct weight please exit this screen and be sure to select the correct subclass when prompted.",
                            "Invalid Registered Weight", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }

                    txtRegWeight.Text = "";
                    VerifyRVW = true;
                }
                else
                {
                    if (Conversion.Val(txtRegWeight.Text) > 6000 && Conversion.Val(txtRegWeight.Text) <= 10000)
                    {
                        VerifyRVW = true;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }
                }
            }
            else if ((MotorVehicle.Statics.Class == "AW") || (MotorVehicle.Statics.Class == "BB") ||
                     (MotorVehicle.Statics.Class == "BC") || (MotorVehicle.Statics.Class == "BH") ||
                     (MotorVehicle.Statics.Class == "LB") || (MotorVehicle.Statics.Class == "SW") ||
                     (MotorVehicle.Statics.Class == "TS") || (MotorVehicle.Statics.Class == "UM"))
            {
                if (MotorVehicle.Statics.Subclass.Right(1) == "1")
                {
                    if (txtRegWeight.Text.ToIntegerValue() > 6000)
                    {
                        MessageBox.Show(
                            "You have entered a weight that is not supported by the subclass of the vehicle.  If this is the correct weight please exit this screen and be sure to select the correct subclass when prompted.",
                            "Invalid Registered Weight", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                else if (MotorVehicle.Statics.Subclass.Right(1) == "2")
                {
                    if (txtRegWeight.Text.ToIntegerValue() > 6000 && txtRegWeight.Text.ToIntegerValue() <= 10000)
                    {
                        VerifyRVW = true;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }
                }
                else
                {
                    if (txtRegWeight.Text.ToIntegerValue() > 10000 && txtRegWeight.Text.ToIntegerValue() < 26000)
                    {
                        VerifyRVW = true;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }
                }
            }
            else if ((MotorVehicle.Statics.Class == "DS") || (MotorVehicle.Statics.Class == "DV") ||
                     (MotorVehicle.Statics.Class == "VT") || (MotorVehicle.Statics.Class == "VX"))
            {
                // kk12222017 tromvs-96  Moved DS and VT from previous case (DS & VT only have subclass 1 & 2)
                if (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "1" ||
                    ((MotorVehicle.Statics.Class == "DV" || MotorVehicle.Statics.Class == "VX") &&
                     Strings.Right(MotorVehicle.Statics.Subclass, 1) == "3"))
                {
                    if (Conversion.Val(txtRegWeight.Text) > 6000)
                    {
                        MessageBox.Show(
                            "You have entered a weight that is not supported by the subclass of the vehicle.  If this is the correct weight please exit this screen and be sure to select the correct subclass when prompted.",
                            "Invalid Registered Weight", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }

                    txtRegWeight.Text = "";
                    VerifyRVW = true;
                }
                else if (MotorVehicle.Statics.Subclass.Right(1) == "2" ||
                         ((MotorVehicle.Statics.Class == "DV" || MotorVehicle.Statics.Class == "VX") &&
                          MotorVehicle.Statics.Subclass.Right(1) == "4"))
                {
                    if (txtRegWeight.Text.ToIntegerValue() > 6000 && txtRegWeight.Text.ToIntegerValue() <= 10000)
                    {
                        VerifyRVW = true;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }
                }
                else
                {
                    if (Conversion.Val(txtRegWeight.Text) > 10000 && Conversion.Val(txtRegWeight.Text) <= 26000)
                    {
                        // kk12302017 tromvs-96  Change upper limit to 26000
                        VerifyRVW = true;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }
                }
            }
            else if (MotorVehicle.Statics.Class == "CL")
            {
                if ((Strings.Right(MotorVehicle.Statics.Subclass, 1) == "1") ||
                    (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "2"))
                {
                    if (Conversion.Val(txtRegWeight.Text) > 2000)
                    {
                        MessageBox.Show(
                            "You have entered a weight that is not supported by the subclass of the vehicle.  If this is the correct weight please exit this screen and be sure to select the correct subclass when prompted.",
                            "Invalid Registered Weight", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }
                    else if (Conversion.Val(txtRegWeight.Text) == 0)
                    {
                        MessageBox.Show("Registered Vehicle Weight is Required.", "Required Field",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }
                }
                else if ((Strings.Right(MotorVehicle.Statics.Subclass, 1) == "3") ||
                         (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "4") ||
                         (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "5"))
                {
                    if (Conversion.Val(txtRegWeight.Text) != 0)
                    {
                        MessageBox.Show("You may not enter a Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }
                }
                else if (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "6")
                {
                    if (Conversion.Val(txtRegWeight.Text) == 0)
                    {
                        MessageBox.Show("Registered Vehicle Weight is Required.", "Required Field",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }
                }
            }
            else if (MotorVehicle.Statics.Class == "TL")
            {
                if ((Strings.Right(MotorVehicle.Statics.Subclass, 1) == "1") ||
                    (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "2"))
                {
                    if (Conversion.Val(txtRegWeight.Text) > 2000)
                    {
                        MessageBox.Show(
                            "You have entered a weight that is not supported by the subclass of the vehicle.  If this is the correct weight please exit this screen and be sure to select the correct subclass when prompted.",
                            "Invalid Registered Weight", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }
                    else if (Conversion.Val(txtRegWeight.Text) == 0)
                    {
                        MessageBox.Show("Registered Vehicle Weight is Required.", "Required Field",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }
                }
                else if ((Strings.Right(MotorVehicle.Statics.Subclass, 1) == "3") ||
                         (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "4") ||
                         (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "6"))
                {
                    if (Conversion.Val(txtRegWeight.Text) != 0)
                    {
                        MessageBox.Show("You may not enter a Registered Vehicle Weight for this Class.", "Invalid RVW",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                    }
                }
                else if (Strings.Right(MotorVehicle.Statics.Subclass, 1) == "7")
                {
                    if (Conversion.Val(txtRegWeight.Text) == 0)
                    {
                        MessageBox.Show("Registered Vehicle Weight is Required.", "Required Field",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyRVW = false;
                        return VerifyRVW;
                    }
                }
            }
            else if (MotorVehicle.Statics.Class == "CR")
            {
                if (Conversion.Val(txtRegWeight.Text) > 6000)
                {
                    MessageBox.Show(
                        "You have entered a weight that is not supported by the class of vehicle.  If this is the correct weight please exit this screen and be sure to select the CC class when prompted.",
                        "Invalid Registered Weight", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    VerifyRVW = false;
                    return VerifyRVW;
                }
            }
            else if ((MotorVehicle.Statics.Class == "CD") || (MotorVehicle.Statics.Class == "AQ"))
            {
                if (Conversion.Val(txtRegWeight.Text) != 0)
                {
                    MessageBox.Show("You may not enter a Registered Vehicle Weight for this Class.", "Invalid RVW",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyRVW = false;
                }
            }

            // 
            if (Conversion.Val(MotorVehicle.Statics.Plate1) > 799999 &&
                Conversion.Val(MotorVehicle.Statics.Plate1) < 900000 &&
                (MotorVehicle.Statics.Class == "CC" || MotorVehicle.Statics.Class == "CO" ||
                 MotorVehicle.Statics.Class == "TT"))
            {
                if (Conversion.Val(txtRegWeight.Text) < 23001)
                {
                    MessageBox.Show("Invalid Registered Vehicle Weight for a Commercial Tractor.", "Invalid RVW",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyRVW = false;
                    txtRegWeight.TabStop = true;
                }
            }

            return VerifyRVW;
        }

        public bool VerifyFuel()
        {
            bool VerifyFuel = false;
            VerifyFuel = true;
            bool executeCheckNormal = false;
            if ((MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "TC") ||
                (MotorVehicle.Statics.Class == "TL") || (MotorVehicle.Statics.Class == "CL") ||
                (MotorVehicle.Statics.Class == "HC"))
            {
                txtFuel.Text = "N";
            }
            else if (MotorVehicle.Statics.Class == "CI")
            {
                if (txtStyle.Text == "TL")
                {
                    txtFuel.Text = "N";
                }
                else
                {
                    executeCheckNormal = true;
                    goto CheckNormal;
                }
            }
            else
            {
                executeCheckNormal = true;
                goto CheckNormal;
            }

        CheckNormal:;
            if (executeCheckNormal)
            {
	            if (MotorVehicle.Statics.Class == "CI")
	            {
		            var ans = MessageBox.Show("Your fuel type is set to " + txtFuel.Text.Trim() + ".  Is this correct?", "Fuel Type", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
		            if (ans == DialogResult.No)
		            {
			            VerifyFuel = false;
			            txtFuel.TabStop = true;
			            txtFuel.Focus();
		            }
	            }

				string vbPorterVar = txtFuel.Text;
                if ((vbPorterVar == "A") || (vbPorterVar == "B") || (vbPorterVar == "C") || (vbPorterVar == "D") ||
                    (vbPorterVar == "E") || (vbPorterVar == "F") || (vbPorterVar == "G") || (vbPorterVar == "H") ||
                    (vbPorterVar == "P") || (vbPorterVar == "O"))
                {
	                
				}
                else
                {
	                if (MotorVehicle.Statics.Class != "CI" || txtFuel.Text != "N")
	                {
		                MessageBox.Show("Please enter an appropriate Fuel type.", "Fuel type Required",
			                MessageBoxButtons.OK, MessageBoxIcon.Warning);
		                VerifyFuel = false;
		                txtFuel.TabStop = true;
					}
                }

                executeCheckNormal = false;
            }

            return VerifyFuel;
        }

        public bool VerifyDOB1()
        {
            bool VerifyDOB1 = false;
            string strCheckDate = "";
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            VerifyDOB1 = true;
            // vbPorter upgrade warning: age As int	OnWrite(long)
            int age = 0;
            if (txtReg1ICM.Text != "I")
            {
                txtReg1DOB.Text = "99/99/9999";
                return VerifyDOB1;
            }

            if (Information.IsDate(txtReg1DOB.Text) == false)
            {
                if (txtReg1LR.Text != "T")
                {
                    MessageBox.Show("D.O.B. for Registrant 1 is a required field.", "DOB required",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyDOB1 = false;
                    txtReg1DOB.TabStop = true;
                }
                else
                {
                    return VerifyDOB1;
                }
            }
            else
            {
	            DateTime dob = DateTime.Parse(txtReg1DOB.Text);
                if (!IsUnder15(dob) && IsUnder18(dob))
                {
                    if (blnFinalCheck)
                    {
                        MessageBox.Show(
                            "Due to the age of the Registrant, be sure to get the signature of a Parent or Legal Guardian.", "Signature Required", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }
                else if (IsUnder15(dob))
                {
                    MessageBox.Show("The Registrant is under Age.", "Invalid Age", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    VerifyDOB1 = false;
                    txtReg1DOB.TabStop = true;
                }
                else if (IsOver100(dob))
                {
                    ans = MessageBox.Show(
                        "The age of Registrant 1 is over 100 years.  Do you wish to check your Date of Birth before you continue?",
                        "Check DOB?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.Yes)
                    {
                        VerifyDOB1 = false;
                        txtReg1DOB.TabStop = true;
                    }
                }
            }

            return VerifyDOB1;
        }

        public bool VerifyDOB2()
        {
            bool VerifyDOB2 = false;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            VerifyDOB2 = true;
            // vbPorter upgrade warning: age As int	OnWrite(long)
            int age = 0;
            if (txtReg2ICM.Text != "I")
            {
                return VerifyDOB2;
            }

            if (Information.IsDate(txtReg2DOB.Text) == false)
            {
                if (txtReg2LR.Text != "T")
                {
                    MessageBox.Show("D.O.B. for Registrant 2 is a required field.", "DOB required",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyDOB2 = false;
                    txtReg2DOB.TabStop = true;
                }
                else
                {
                    return VerifyDOB2;
                }
            }
            else
            {
	            DateTime dob = DateTime.Parse(txtReg2DOB.Text);
				if (!IsUnder15(dob) && IsUnder18(dob))
				{
                    if ((MotorVehicle.Statics.RegistrationType == "NRR") ||
                        (MotorVehicle.Statics.RegistrationType == "NRT") ||
                        (MotorVehicle.Statics.RegistrationType == "RRT"))
                    {
                        MessageBox.Show(
                            "Due to the age of the Registrant, be sure to get the signature of a Parent or Legal Guardian.", "Signature Required", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }
                else if (IsUnder15(dob))
				{
                    MessageBox.Show("The Registrant is under Age.", "Invalid Age", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    VerifyDOB2 = false;
                    txtReg2DOB.TabStop = true;
                }
                else if (IsOver100(dob))
				{
                    ans = MessageBox.Show(
                        "The age of Registrant 2 is over 100 years.  Do you wish to check your Date of Birth before you continue?",
                        "Check DOB?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.Yes)
                    {
                        VerifyDOB2 = false;
                        txtReg2DOB.TabStop = true;
                    }
                }
            }

            return VerifyDOB2;
        }

        public bool VerifyDOB3()
        {
            bool VerifyDOB3 = false;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            VerifyDOB3 = true;
            // vbPorter upgrade warning: age As int	OnWrite(long)
            int age = 0;
            if (txtReg3ICM.Text != "I")
            {
                return VerifyDOB3;
            }

            if (Information.IsDate(txtReg3DOB.Text) == false)
            {
                if (txtReg2LR.Text != "T")
                {
                    MessageBox.Show("D.O.B. for Registrant 3 is a required field.", "DOB required",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifyDOB3 = false;
                    txtReg3DOB.TabStop = true;
                }
                else
                {
                    return VerifyDOB3;
                }
            }
            else
            {
	            DateTime dob = DateTime.Parse(txtReg3DOB.Text);
				if (!IsUnder15(dob) && IsUnder18(dob))
				{
                    if ((MotorVehicle.Statics.RegistrationType == "NRR") ||
                        (MotorVehicle.Statics.RegistrationType == "NRT") ||
                        (MotorVehicle.Statics.RegistrationType == "RRT"))
                    {
                        MessageBox.Show(
                            "Due to the age of the Registrant, be sure to get the signature of a Parent or Legal Guardian.", "Signature Required", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }
                else if (IsUnder15(dob))
				{
                    MessageBox.Show("The Registrant is under Age.", "Invalid Age", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    VerifyDOB3 = false;
                    txtReg3DOB.TabStop = true;
                }
                else if (IsOver100(dob))
				{
                    ans = MessageBox.Show(
                        "The age of Registrant 3 is over 100 years.  Do you wish to check your Date of Birth before you continue?",
                        "Check DOB?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.Yes)
                    {
                        VerifyDOB3 = false;
                        txtReg3DOB.TabStop = true;
                    }
                }
            }

            return VerifyDOB3;
        }

        private bool IsUnder18(DateTime dob)
        {
	        DateTime age18dob = DateTime.Today.AddYears(-18);
	        return dob > age18dob;
        }

        private bool IsUnder15(DateTime dob)
        {
	        DateTime age15dob = DateTime.Today.AddYears(-15);
	        return dob > age15dob;
        }

        private bool IsOver100(DateTime dob)
        {
	        DateTime age100dob = DateTime.Today.AddYears(-100);
	        return dob < age100dob;
        }

		public bool VerifyOwnerCodes()
        {
            bool VerifyOwnerCodes = false;
            VerifyOwnerCodes = true;
            string vbPorterVar = txtReg1ICM.Text;
            if ((vbPorterVar == "I") || (vbPorterVar == "C") || (vbPorterVar == "M"))
            {
                // do nothing
            }
            else
            {
                MessageBox.Show("Please select an Owner code for Owner 1.", "Owner 1 Code", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                txtReg1ICM.Focus();
                VerifyOwnerCodes = false;
            }

            // 
            string vbPorterVar1 = txtReg2ICM.Text;
            if ((vbPorterVar1 == "I") || (vbPorterVar1 == "C") || (vbPorterVar1 == "M") || (vbPorterVar1 == "D") ||
                (vbPorterVar1 == "N"))
            {
                // do nothing
            }
            else
            {
                MessageBox.Show("Please select an Owner code for Owner 2.", "Owner 2 Code", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                txtReg2ICM.Focus();
                VerifyOwnerCodes = false;
            }

            string vbPorterVar2 = txtReg3ICM.Text;
            if ((vbPorterVar2 == "I") || (vbPorterVar2 == "C") || (vbPorterVar2 == "M") || (vbPorterVar2 == "D") ||
                (vbPorterVar2 == "N"))
            {
                // do nothing
            }
            else
            {
                MessageBox.Show("Please select an Owner code for Owner 3.", "Owner 3 Code", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                txtReg3ICM.Focus();
                VerifyOwnerCodes = false;
            }

            return VerifyOwnerCodes;
        }

        public bool VerifyLeaseTrustCodes()
        {
            bool VerifyLeaseTrustCodes = false;
            int intLessorCount;
            int intLeaseeCount;
            intLessorCount = 0;
            intLeaseeCount = 0;
            VerifyLeaseTrustCodes = true;
            string vbPorterVar = txtReg1LR.Text;
            if ((vbPorterVar == "T") || (vbPorterVar == "N") || (vbPorterVar == "P"))
            {
                // do nothing
            }
            else if (vbPorterVar == "E")
            {
                intLeaseeCount += 1;
            }
            else if (vbPorterVar == "R")
            {
                intLessorCount += 1;
            }
            else
            {
                MessageBox.Show("Please select a lease / trust code for Owner 1.", "Owner 1 Lease / Trust Code",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtReg1LR.Focus();
                VerifyLeaseTrustCodes = false;
            }

            // 
            string vbPorterVar1 = txtReg2LR.Text;
            if ((vbPorterVar1 == "T") || (vbPorterVar1 == "N") || (vbPorterVar1 == "P"))
            {
                // do nothing
            }
            else if (vbPorterVar1 == "E")
            {
                intLeaseeCount += 1;
            }
            else if (vbPorterVar1 == "R")
            {
                intLessorCount += 1;
            }
            else
            {
                MessageBox.Show("Please select a lease / trust code for Owner 2.", "Owner 2 Lease / Trust Code",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtReg2LR.Focus();
                VerifyLeaseTrustCodes = false;
            }

            string vbPorterVar2 = txtReg3LR.Text;
            if (vbPorterVar2 == "T")
            {
                if (txtReg3ICM.Text != "I" && txtReg3ICM.Text != "N")
                {
                    ShowWarningBox("You may only mark the third owner as a trustee if it is an individual.",
                        "Invalid Trustee");
                    VerifyLeaseTrustCodes = false;
                    return VerifyLeaseTrustCodes;
                }
            }
            else if ((vbPorterVar2 == "N") || (vbPorterVar2 == "P"))
            {
                // do nothing
            }
            else if (vbPorterVar2 == "E")
            {
                intLeaseeCount += 1;
                if (txtReg3ICM.Text != "I" && txtReg3ICM.Text != "N")
                {
                    ShowWarningBox("You may only mark the third owner as a leesee if it is an individual.",
                        "Invalid Leesee");
                    VerifyLeaseTrustCodes = false;
                    return VerifyLeaseTrustCodes;
                }
            }
            else if (vbPorterVar2 == "R")
            {
                intLessorCount += 1;
            }
            else
            {
                MessageBox.Show("Please select a lease / trust code for Owner 3.", "Owner 3 Lease / Trust Code",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtReg3LR.Focus();
                VerifyLeaseTrustCodes = false;
            }

            if (intLessorCount > 1)
            {
                MessageBox.Show("You may only mark 1 owner as a Lessor.", "To Many Lessors", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyLeaseTrustCodes = false;
            }
            else if (intLeaseeCount > 0 && intLessorCount == 0)
            {
                ShowWarningBox(
                    "A registrant is marked as a lessee, but no lessor is currently flagged. Please mark a registrant as a lessor.",
                    "Invalid Lease Codes");
                VerifyLeaseTrustCodes = false;
            }
            else if (intLeaseeCount == 0 && intLessorCount > 0)
            {
                ShowWarningBox(
                    "A registrant is marked as a lessor, but no lessee is currently flagged. Please mark at least one registrant as a lessee.",
                    "Invalid Lease Codes");
                VerifyLeaseTrustCodes = false;
            }

            return VerifyLeaseTrustCodes;
        }

        public bool VerifyAddress()
        {
            bool VerifyAddress = false;
            VerifyAddress = true;
            if (txtAddress1.Text.Trim() == "")
            {
                MessageBox.Show("Be sure there is an Address in the Address box.", "Check Address",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                VerifyAddress = false;
                txtAddress1.TabStop = true;
                return VerifyAddress;
            }

            return VerifyAddress;
        }

        public bool VerifyResCode()
        {
            bool VerifyResCode = false;
            VerifyResCode = true;
            string rescode = "";
            
            txtLegalres.Locked = true;

			// 
			if (Conversion.Val(txtresidenceCode.Text) != 0 && Conversion.Val(txtresidenceCode.Text) != 99999)
            {
                rescode = fecherFoundation.Strings.Trim(txtresidenceCode.Text);
                MotorVehicle.Statics.strSql = "SELECT * FROM Residence";

                using (clsDRWrapper rsResCode = new clsDRWrapper())
                {
                    rsResCode.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
                    {
                        rsResCode.MoveLast();
                        rsResCode.MoveFirst();
                        if (rsResCode.FindFirstRecord("Code", rescode))
                        {
                            txtLegalres.Text =
                                fecherFoundation.Strings.Trim(FCConvert.ToString(rsResCode.Get_Fields_String("Town")));
                        }
                        else
                        {
                            ShowWarningBox(
                                " The residence code is not in the BMV table." + "\r\n" + "Please input a Town and State.",
                                "Town and State Required");
                            txtresidenceCode.Text = "00000";
                            VerifyResCode = false;
                            txtresidenceCode.TabStop = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("There are no entries in the BMV residence code table.", "Empty Table",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyResCode = false;
                    }
                }
            }
            else if (Conversion.Val(txtresidenceCode.Text) == 99999)
            {
	            txtLegalres.Locked = false;
	            if (txtLegalres.Text.Trim() == "")
	            {
		            MessageBox.Show("Be sure to fill out the legal residence field.", "Residence Code Required",
			            MessageBoxButtons.OK, MessageBoxIcon.Warning);
		            VerifyResCode = false;
				}
            }
            else if (Conversion.Val(txtresidenceCode.Text) == 0)
            {
                MessageBox.Show("Residence code is required", "Residence Code Required", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyResCode = false;
            }

            return VerifyResCode;
        }

        public bool VerifyCTA()
        {
	        bool VerifyCTA = true;
            if (MotorVehicle.Statics.ETO == true)
                return VerifyCTA;
            // cta not needed
            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "NRR")
            {
                if (MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "CL")
                {
                    if ((MotorVehicle.Statics.Subclass == "L1") || (MotorVehicle.Statics.Subclass == "C1"))
                    {
                        // title not required
                        return VerifyCTA;
                    }
                    else if ((MotorVehicle.Statics.Subclass == "L2") || (MotorVehicle.Statics.Subclass == "C2"))
                    {
                        // title not required
                        return VerifyCTA;
                    }
                    else if ((MotorVehicle.Statics.Subclass == "L3") || (MotorVehicle.Statics.Subclass == "L4") ||
                             (MotorVehicle.Statics.Subclass == "C3") ||
                             (MotorVehicle.Statics.Subclass == "C4") || (MotorVehicle.Statics.Subclass == "L6") ||
                             (MotorVehicle.Statics.Subclass == "C7"))
                    {
                        if (Conversion.Val(txtNetWeight.Text) < 3001)
                        {
                            return VerifyCTA;
                        }
                    }
                    else if ((MotorVehicle.Statics.Subclass == "C5") || (MotorVehicle.Statics.Subclass == "L6"))
                    {
                        // title not required
                        return VerifyCTA;
                    }

                    return VerifyCTA;
                }
                else if (MotorVehicle.Statics.Subclass == "T1" || MotorVehicle.Statics.Subclass == "T2" ||
                         MotorVehicle.Statics.Subclass == "T3" || MotorVehicle.Statics.Subclass == "T4" ||
                         MotorVehicle.Statics.Subclass == "T5" || MotorVehicle.Statics.Subclass == "I2")
                {
                    return VerifyCTA;
                }
                else if ((MotorVehicle.Statics.Class == "AU" || MotorVehicle.Statics.Class == "MC" ||
						  MotorVehicle.Statics.Class == "MX" || MotorVehicle.Statics.Class == "PM" ||
						  MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "XV") && chkCC.CheckState == Wisej.Web.CheckState.Unchecked)
                {
                    return VerifyCTA;
                }
                else if (MotorVehicle.Statics.Class == "SE")
                {
                    return VerifyCTA;
                }
                else if (MotorVehicle.Statics.Class == "MP")
                {
                    return VerifyCTA;
                }
                else if (chkCC.CheckState == Wisej.Web.CheckState.Checked && !TitleRequired)
                {
                    return VerifyCTA;
                }
                else if (MotorVehicle.Statics.Class == "TC")
                {
                    return VerifyCTA;
                }
                else
                {
                    // do nothing
                }

                if (txtCTANumber.Text == "" || txtCTANumber.Text.Length < 2)
                {
                    if (txtYear.Text != "")
                    {
                        // If Val(txtYear.Text) > Year(Format(Now, "MM/dd/yyyy")) - 16 Then
                        if (Conversion.Val(txtYear.Text) >= 1995)
                        {
                            MessageBox.Show("CTA Number is required for all New Registrations.", "CTA required",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            VerifyCTA = false;
                            txtCTANumber.TabStop = true;
                            txtCTANumber.Focus();
                        }
                    }
                }
                else
                {
	                if (txtDoubleCTANumber.Text != "")
	                {
                        using (clsDRWrapper rsDoubleCTA = new clsDRWrapper())
                        {
                            rsDoubleCTA.OpenRecordset("SELECT * FROM DoubleCTA WHERE CTANumber = '" + txtDoubleCTANumber.Text + "'");
                            if (rsDoubleCTA.EndOfFile())
                            {
                                object strPriorOwner = "";
                                frmInput.InstancePtr.Init(ref strPriorOwner, "Prior Owner Required",
                                    "Please enter the prior owner of this vehicle.", 6000, false,
                                    modGlobalConstants.InputDTypes.idtString);
                                if (!String.IsNullOrWhiteSpace(strPriorOwner.ToString()))
                                {
                                    rsDoubleCTA.AddNew();
                                    rsDoubleCTA.Set_Fields("CTANumber", txtDoubleCTANumber.Text.ToUpper());
                                    rsDoubleCTA.Set_Fields("Customer", strPriorOwner.ToString().ToUpper());
                                    rsDoubleCTA.Update();
                                }
                                else
                                {
                                    MessageBox.Show("Prior owner is required for Double CTA.", "Prior owner required",
                                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    VerifyCTA = false;
                                }
                            }
                        }
                    }
                }
            }

            return VerifyCTA;
        }

        public bool VerifyDOT()
        {
            bool VerifyDOT = false;
            DialogResult ans = 0;
            VerifyDOT = true;
            string vbPorterVar = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");
            if ((vbPorterVar == "AC") || (vbPorterVar == "AF") || (vbPorterVar == "AP") || (vbPorterVar == "AW") ||
                (vbPorterVar == "BB") || (vbPorterVar == "BC") || (vbPorterVar == "BH") || (vbPorterVar == "CC") ||
                (vbPorterVar == "CO") || (vbPorterVar == "DS") || (vbPorterVar == "DV") || (vbPorterVar == "FM") ||
                (vbPorterVar == "LB") || (vbPorterVar == "LC") || (vbPorterVar == "SW") || (vbPorterVar == "TR") ||
                (vbPorterVar == "TS") || (vbPorterVar == "TT") || (vbPorterVar == "UM") || (vbPorterVar == "VT") ||
                (vbPorterVar == "VX"))
            {
                if (Conversion.Val(txtRegWeight.Text) > 26000 || DOTNumberRequired == true)
                {
                    if (fecherFoundation.Strings.Trim(txtDOTNumber.Text) == "" && !MotorVehicle.Statics.ETO)
                    {
                        MessageBox.Show("DOT Number is required for this Vehicle.", "DOT Number required",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        VerifyDOT = false;
                        txtDOTNumber.TabStop = true;
                        txtDOTNumber.Focus();
                    }
                }
                else if (Conversion.Val(txtRegWeight.Text) > 10000 && Conversion.Val(txtRegWeight.Text) <= 26000 &&
                         fecherFoundation.Strings.Trim(txtDOTNumber.Text) == "")
                {
                    ans = MessageBox.Show(
                        "Please verify with registrant and provide a DOT number when possible. Would you like to provide a DOT number?",
                        "DOT Number", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.Yes)
                    {
                        VerifyDOT = false;
                        txtDOTNumber.TabStop = true;
                        txtDOTNumber.Focus();
                    }
                }
            }

            return VerifyDOT;
        }

        public bool VerifyTaxID()
        {
            bool VerifyTaxID = false;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            bool blnRequired;
            VerifyTaxID = true;
            blnRequired = false;
            if (txtReg1ICM.Text == "C" && txtReg1LR.Text != "R")
            {
                blnRequired = true;
            }
            else if (txtReg2ICM.Text == "C" && txtReg2LR.Text != "R")
            {
                blnRequired = true;
            }
            else if (txtReg3ICM.Text == "C" && txtReg3LR.Text != "R")
            {
                blnRequired = true;
            }

            if (txtReg1LR.Text == "T")
            {
                if (intIrrevocableTrust == -1)
                {
                    if (MessageBox.Show("Is this an Irrevocable Living Trust?", "Irrevocable Trust?",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        intIrrevocableTrust = 1;
                    }
                    else
                    {
                        intIrrevocableTrust = 0;
                    }
                }
            }

            if (intIrrevocableTrust == 1 || intIrrevocableTrust == -1 && blnRequired)
            {
                if (txtTaxIDNumber.Text != "")
                {
                    if (Information.IsNumeric(txtTaxIDNumber.Text))
                    {
                        if (Conversion.Val(txtTaxIDNumber.Text) == 0)
                        {
                            MessageBox.Show("You must enter a Federal Tax ID Number before proceeding", null,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            txtTaxIDNumber.Focus();
                            VerifyTaxID = false;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You must enter a Federal Tax ID Number before proceeding", null,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTaxIDNumber.Focus();
                    VerifyTaxID = false;
                }
            }

            if (blnFinalCheck)
            {
                if (Information.IsNumeric(txtTaxIDNumber.Text))
                {
                    if (Conversion.Val(txtTaxIDNumber.Text) != 0 && blnRequired)
                    {
                        if (fecherFoundation.Strings.Trim(txtTaxIDNumber.Text).Length < 9)
                        {
                            ans = MessageBox.Show(
                                "Your Tax ID Number is shorter than usual Tax ID numbers.  Would you like to check your Tax ID number before you proceed?",
                                "Short Tax ID Number", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                txtTaxIDNumber.Focus();
                                VerifyTaxID = false;
                            }
                        }
                    }
                }
            }

            return VerifyTaxID;
        }

        public void MSRPvsBase(ref int MSRP)
        {
            bool DifferentIsOK = false;
            if ((MotorVehicle.Statics.Class == "CC") || (MotorVehicle.Statics.Class == "CO") ||
                (MotorVehicle.Statics.Class == "AP") || (MotorVehicle.Statics.Class == "TT") ||
                (MotorVehicle.Statics.Class == "AC") || (MotorVehicle.Statics.Class == "AF") ||
                (MotorVehicle.Statics.Class == "FM") || (MotorVehicle.Statics.Class == "LC"))
            {
                DifferentIsOK = false;
                if (txtYear.Text != "")
                {
                    if (Conversion.Val(txtYear.Text) > 1995)
                    {
                        if (txtRegWeight.Text != "")
                        {
                            if (Conversion.Val(txtRegWeight.Text) > 26000)
                            {
                                DifferentIsOK = true;
                            }
                        }
                    }
                }
            }

            if (Conversion.Val(txtBase.Text) != 0)
            {
                if (Conversion.Val(txtBase.Text) != MSRP)
                {
                    if (DifferentIsOK == false)
                    {
                        MessageBox.Show(
                            "The Base value on the screen and the MSRP value listed in the file are not the same." +
                            "\r\n" + "Please adjust the Value on screen.", "Values Do Not match", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                    }
                    else
                    {
                        ShowWarningBox("Don't forget to submit an Application for Reimbursement.",
                            "Reimbursement Application");
                    }
                }
            }
        }

        public bool VerifyPriorTitle()
        {
            bool VerifyPriorTitle = false;
            VerifyPriorTitle = true;
            
            if (MotorVehicle.Statics.ETO == true)
                return VerifyPriorTitle;
            // If Year(Now) - Val(txtYear.Text) > 15 Then Exit Function
            if (Conversion.Val(txtYear.Text) < 1995)
                return VerifyPriorTitle;
            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
            {
                using (clsDRWrapper rsTitle = new clsDRWrapper())
                {
                    MotorVehicle.Statics.strSql = "SELECT * FROM Class where SystemCode = '" +
                                              MotorVehicle.Statics.Subclass + "' AND BMVCode = '" +
                                              MotorVehicle.Statics.Class + "'";
                    rsTitle.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                    {
                        rsTitle.MoveLast();
                        rsTitle.MoveFirst();
                        if (FCConvert.ToString(rsTitle.Get_Fields_String("TitleRequired")) == "R")
                        {
                            if (txtPreviousTitle.Text == "")
                            {
                                MessageBox.Show("Previous title is required for this Vehicle.", "Previous Title Required",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                txtPreviousTitle.TabStop = true;
                                txtPreviousTitle.Focus();
                                VerifyPriorTitle = false;
                            }
                        }
                        else if (rsTitle.Get_Fields_String("TitleRequired") == "X")
                        {
                            if (Conversion.Val(txtNetWeight.Text) > 3000)
                            {
                                if (txtPreviousTitle.Text == "")
                                {
                                    ShowWarningBox("Previous title is required for this Vehicle.",
                                        "Previous Title Required");
                                    txtPreviousTitle.TabStop = true;
                                    txtPreviousTitle.Focus();
                                    VerifyPriorTitle = false;
                                }
                            }
                        }
                        else if (rsTitle.Get_Fields_String("TitleRequired") == "V")
                        {
                            if (chkCC.CheckState == Wisej.Web.CheckState.Checked && TitleRequired)
                            {
                                if (txtPreviousTitle.Text == "")
                                {
                                    ShowWarningBox("Previous title is required for this Vehicle.",
                                        "Previous Title Required");
                                    txtPreviousTitle.TabStop = true;
                                    txtPreviousTitle.Focus();
                                    VerifyPriorTitle = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("There are no entries in the Class Table for a Vehicle of this type.", "No Entries",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

            return VerifyPriorTitle;
        }

        public void ChickadeeConversion()
        {
            // vbPorter upgrade warning: answer As string	OnWrite(DialogResult)
            DialogResult answer = DialogResult.None;
            if ((MotorVehicle.Statics.Class == "PC") || (MotorVehicle.Statics.Class == "CO") ||
                (MotorVehicle.Statics.Class == "AQ") || (MotorVehicle.Statics.Class == "CM") ||
                (MotorVehicle.Statics.Class == "BU") || (MotorVehicle.Statics.Class == "DX") ||
                (MotorVehicle.Statics.Class == "FM") || (MotorVehicle.Statics.Class == "HC") ||
                (MotorVehicle.Statics.Class == "MH") || (MotorVehicle.Statics.Class == "TR") ||
                (MotorVehicle.Statics.Class == "TX") || (MotorVehicle.Statics.Class == "SE") ||
                (MotorVehicle.Statics.Class == "SR") || (MotorVehicle.Statics.Class == "WX") ||
                (MotorVehicle.Statics.Class == "TT"))
            {
                if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
                {
                    if (MotorVehicle.Statics.ReturnFromListBox != "R")
                    {
                        // this plate was not found in inventory and they selected Reserved from th list box in Plate info
                        answer = MessageBox.Show("Is this a conversion from a Lobtser plate to a Chickadee plate?",
                            "Chickadee Conversion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (answer == DialogResult.Yes)
                        {
                        }
                        else
                        {
                        }
                    }
                }
            }
        }

        public void CalculateLocalFees()
        {
            clsDRWrapper rsFees = new clsDRWrapper();
            // vbPorter upgrade warning: ExciseTaxValue As Decimal	OnWrite(Decimal, int)
            Decimal ExciseTaxValue;
            int ans;

            try
            {
                MotorVehicle.Statics.strSql = "SELECT * FROM DefaultInfo";
                rsFees.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsFees.EndOfFile() != true && rsFees.BeginningOfFile() != true)
                {
                    rsFees.MoveLast();
                    rsFees.MoveFirst();
                    if (modGlobalConstants.Statics.MuniName == "MAINE TRAILER")
                    {
                        if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                        {
                            AgentFee = 30;
                        }
                        else
                        {
                            AgentFee = 20;
                        }
                    }
                    else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "PROCUREMENT SPR")
                    {
                        AgentFee = 7;
                    }
                    else
                    {
                        if (MotorVehicle.Statics.RegistrationType == "RRR")
                        {
                            if (fecherFoundation.Strings.Trim(txtresidenceCode.Text) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode"))) ||
                                MotorVehicle.Statics.NewToTheSystem)
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentFeeReRegLocal");
                            }
                            else
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentFeeReRegOOT");
                            }
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "RRT")
                        {
                            if (fecherFoundation.Strings.Trim(txtresidenceCode.Text) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode"))) ||
                                MotorVehicle.Statics.NewToTheSystem)
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentFeeReRegLocal");
                            }
                            else
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentFeeReRegOOT");
                            }

                            txtTransferCharge.Text =
                                Strings.Format(rsFees.Get_Fields_Decimal("LocalTransferFee"), "#,##0.00");
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "NRR")
                        {
                            if (fecherFoundation.Strings.Trim(txtresidenceCode.Text) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode")))
                            )
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentfeeNewLocal");
                            }
                            else
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentFeeNewOOT");
                            }
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "NRT")
                        {
                            if (fecherFoundation.Strings.Trim(txtresidenceCode.Text) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode")))
                            )
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentfeeNewLocal");
                            }
                            else
                            {
                                AgentFee = rsFees.Get_Fields_Decimal("AgentFeeNewOOT");
                            }

                            txtTransferCharge.Text =
                                Strings.Format(rsFees.Get_Fields_Decimal("LocalTransferFee"), "#,##0.00");
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                        {
                            AgentFee = rsFees.Get_Fields_Decimal("DuplicateDecal");
                        }

                        if (chkETO.CheckState == Wisej.Web.CheckState.Checked ||
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI")
                        {
                            AgentFee = 0;
                        }
                    }

                    if (chkAmputeeVet.CheckState == Wisej.Web.CheckState.Unchecked)
                    {
                        if (MotorVehicle.Statics.RegistrationType == "ECO" ||
                            MotorVehicle.Statics.RegistrationType == "ECR")
                        {
                            txtAgentFee.Text = Strings.Format(
                                MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFee"),
                                "#,##0.00");
                        }
                        else
                        {
                            txtAgentFee.Text = Strings.Format(AgentFee, "#,##0.00");
                        }
                    }
                    else
                    {
                        txtAgentFee.Text = "0.00";
                    }
                }
                else
                {
                    ShowWarningBox(
                        "There are no Amounts in the Fees table. The values will have to be inputted on the screen.",
                        "No Fee Information");
                }

                // Excise tax
                if (MotorVehicle.Statics.ExciseAP == true)
                    return;
                if (chkExciseExempt.CheckState == Wisej.Web.CheckState.Unchecked &&
                    chkAmputeeVet.CheckState == Wisej.Web.CheckState.Unchecked)
                {
                    if (Strings.Mid(MotorVehicle.Statics.RegistrationType, 1, 1) != "N")
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear") != 0)
                        {
                            MotorVehicle.Statics.MillYear =
                                FCConvert.ToInt16(
                                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) + 1);
                            if (MotorVehicle.Statics.MillYear > 6)
                                MotorVehicle.Statics.MillYear = 6;
                            // End If
                        }
                        else
                        {
                            if (Conversion.Val(txtYear.Text) != 0)
                            {
                                MotorVehicle.Statics.MillYear = FCConvert.ToInt16(
                                    (FCConvert.ToDateTime(txtExpires.Text).Year - 1) - Conversion.Val(txtYear.Text) +
                                    1);
                                if (MotorVehicle.Statics.MillYear > 6)
                                    MotorVehicle.Statics.MillYear = 6;
                            }
                            else
                            {
                                MotorVehicle.Statics.MillYear = 1;
                            }

                            // End If
                        }

                        if (Conversion.Val(txtBase.Text) == 0)
                        {
                            ExciseTaxValue = MotorVehicle.ExciseTax(MotorVehicle.Statics.MillYear,
                                FCConvert.ToInt32(
                                    (FCConvert.ToDecimal(
                                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"))))),
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
                        }
                        else
                        {
                            ExciseTaxValue = MotorVehicle.ExciseTax(MotorVehicle.Statics.MillYear,
                                FCConvert.ToInt32(txtBase.Text),
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
                        }

                        txtMilYear.Text = FCConvert.ToString(MotorVehicle.Statics.MillYear);
                        txtMilRate.Text = Strings.Format(MotorVehicle.Statics.MillRate, ".0000");
                    }
                    else
                    {
                        MotorVehicle.Statics.MillYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtMilYear.Text)));
                        ExciseTaxValue = MotorVehicle.ExciseTax(MotorVehicle.Statics.MillYear,
                            FCConvert.ToInt32(FCConvert.ToDouble(txtBase.Text)),
                            MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                            MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
                        txtMilYear.Text = FCConvert.ToString(MotorVehicle.Statics.MillYear);
                        txtMilRate.Text = Strings.Format(MotorVehicle.Statics.MillRate, ".0000");
                    }

                    MotorVehicle.Statics.FullExciseTax = ExciseTaxValue;
                    MotorVehicle.Statics.FullExciseCredit =
                        FCConvert.ToDecimal(
                            Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")));
                    if (chkHalfRateLocal.CheckState == Wisej.Web.CheckState.Checked && ExciseTaxValue > 5)
                    {
                        if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                        {
                            ExciseTaxValue *= 1.5m;
                        }
                        else
                        {
                            ExciseTaxValue *= 0.5m;
                        }

                        if (FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)) ==
                            MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxFull"))
                        {
                            txtTownCredit.Text = FCConvert.ToString(Conversion.Val(txtTownCredit.Text) * 0.5);
                        }
                    }
                    else if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        ExciseTaxValue *= 2;
                    }
                }
                else
                {
                    // excise exempt
                    MotorVehicle.Statics.MillYear = 0;
                    ExciseTaxValue = 0;
                }

                txtExciseTax.Text = Strings.Format(ExciseTaxValue, "#,##0.00");
                // subtotal
                txtSubTotal.Text = Strings.Format(
                    ExciseTaxValue - FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)),
                    "#,##0.00");
                if (Conversion.Val(txtSubTotal.Text) < 0)
                    txtSubTotal.Text = "0.00";
                // transfer fee
                if ((MotorVehicle.Statics.RegistrationType == "RRT") ||
                    (MotorVehicle.Statics.RegistrationType == "NRT"))
                {
                    if (chkTransferExempt.CheckState != Wisej.Web.CheckState.Checked && chkExciseExempt.Enabled &&
                        FCConvert.ToDecimal(txtTownCredit.Text) > 0)
                    {
                        txtTransferCharge.Text = Strings.Format(rsFees.Get_Fields_Decimal("LocalTransferFee"), "0.00");
                    }
                    else
                    {
                        txtTransferCharge.Text = Strings.Format(0, "0.00");
                    }

                    if (MotorVehicle.Statics.blnAllowExciseRebates)
                    {
                        if (FCConvert.ToDecimal(txtExciseTax.Text) - FCConvert.ToDecimal(txtTownCredit.Text) < 0)
                        {
                            lblCreditRebate.Visible = true;
                        }
                        else
                        {
                            lblCreditRebate.Visible = false;
                        }
                    }
                    else
                    {
                        lblCreditRebate.Visible = false;
                    }
                }

                // balance
                txtBalance.Text = Strings.Format(
                    Conversion.Val(txtSubTotal.Text) + Conversion.Val(txtTransferCharge.Text),
                    "#,##0.00");
                // base
                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice")) != 0)
                {
                    txtBase.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"), "#,##0");
                }
                else if (txtBase.Text == "")
                {
                    txtBase.Text = FCConvert.ToString(0.0);
                }

                // 
                if (chkAgentFeeExempt.CheckState == Wisej.Web.CheckState.Checked)
                    txtAgentFee.Text = "0.00";
            }
            finally
            {
                rsFees.Dispose();
            }
            
        }

        public void FillExemptFlags()
        {
            // kk09272016 tromvs-57 follow up  'Change from ExciseTaxRequired()
            // Change procedure to fill the Reg Exempt flag as well because EXEMPT is required to print, instead of leaving blank
            clsDRWrapper rsClassInfo = new clsDRWrapper();


            try
            {
                MotorVehicle.Statics.strSql = "SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class +
                                              "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'";
                rsClassInfo.OpenRecordset(MotorVehicle.Statics.strSql);
                // Excise Exempt Logic
                if (rsClassInfo.EndOfFile() != true && rsClassInfo.BeginningOfFile() != true)
                {
                    rsClassInfo.MoveLast();
                    rsClassInfo.MoveFirst();
                    if (FCConvert.ToString(rsClassInfo.Get_Fields_String("ExciseRequired")) == "Y")
                    {
                        if (MotorVehicle.Statics.RegistrationType == "RRR" ||
                            MotorVehicle.Statics.RegistrationType == "RRT" ||
                            MotorVehicle.Statics.RegistrationType == "NRT" ||
                            MotorVehicle.Statics.RegistrationType == "NRR")
                        {
                            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseExempt") == true)
                            {
                                if (MessageBox.Show(
                                        "The previous registration of this vehicle was excise exempt.  Would you like to carry over this exemption?",
                                        "Excise Exempt?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                                    DialogResult.Yes)
                                {
                                    chkExciseExempt.CheckState = Wisej.Web.CheckState.Checked;
                                    goto EndExcise;
                                }
                            }
                        }

                        chkExciseExempt.Enabled = true;
                        chkExciseExempt.CheckState = Wisej.Web.CheckState.Unchecked;
                    }
                    else
                    {
                        chkExciseExempt.CheckState = Wisej.Web.CheckState.Checked;
                        chkExciseExempt.Enabled = false;
                    }
                }
                else
                {
                    if (MessageBox.Show("Is this vehicle excise exempt?", "Excise Exempt?", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        chkExciseExempt.Enabled = true;
                        chkExciseExempt.CheckState = Wisej.Web.CheckState.Checked;
                    }
                    else
                    {
                        chkExciseExempt.Enabled = true;
                        chkExciseExempt.CheckState = Wisej.Web.CheckState.Unchecked;
                    }
                }

                EndExcise: ;
                // Reg Exempt Logic
                chkStateExempt.CheckState = Wisej.Web.CheckState.Unchecked;
                if (MotorVehicle.Statics.Class != "AP")
                {
                    if (rsClassInfo.EndOfFile() != true && rsClassInfo.BeginningOfFile() != true)
                    {
                        rsClassInfo.MoveLast();
                        rsClassInfo.MoveFirst();
                        if (rsClassInfo.Get_Fields_Decimal("RegistrationFee") > 0)
                        {
                            if (MotorVehicle.Statics.RegistrationType == "RRR" ||
                                MotorVehicle.Statics.RegistrationType == "RRT" ||
                                MotorVehicle.Statics.RegistrationType == "NRT" ||
                                MotorVehicle.Statics.RegistrationType == "NRR")
                            {
                                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegExempt") == true)
                                {
                                    if (MessageBox.Show(
                                            "The previous registration of this vehicle was registration fee exempt.  Would you like to carry over this exemption?",
                                            "Registration Fee Exempt?", MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question) ==
                                        DialogResult.Yes)
                                    {
                                        chkStateExempt.CheckState = Wisej.Web.CheckState.Checked;
                                    }
                                }
                            }
                            else
                            {
                                if (rsClassInfo.Get_Fields_Boolean("RegExempt") == true)
                                {
                                    chkStateExempt.CheckState = Wisej.Web.CheckState.Checked;
                                }
                            }
                        }
                        else
                        {
                            if ((MotorVehicle.Statics.Class == "CI") || (MotorVehicle.Statics.Class == "DV") ||
                                (MotorVehicle.Statics.Class == "GS") || (MotorVehicle.Statics.Class == "MM") ||
                                (MotorVehicle.Statics.Class == "MO") || (MotorVehicle.Statics.Class == "PH") ||
                                (MotorVehicle.Statics.Class == "PM") || (MotorVehicle.Statics.Class == "PO") ||
                                (MotorVehicle.Statics.Class == "PS") || (MotorVehicle.Statics.Class == "VX") ||
                                (MotorVehicle.Statics.Class == "XV"))
                            {
                                // We know these are Exempt
                                chkStateExempt.CheckState = Wisej.Web.CheckState.Checked;
                            }
                        }
                    }
                }
            }
            finally
            {
                rsClassInfo.Dispose();
            }
        }

        private void FillMiscInfo()
        {
            txtUserID.Text = MotorVehicle.Statics.UserID;
            if (MotorVehicle.Statics.Class != "TL" && MotorVehicle.Statics.Class != "TC" &&
                MotorVehicle.Statics.Class != "CL")
                txtEvidenceofInsurance.Text = MotorVehicle.Statics.UserID;
        }

        public void CalculateStateFees()
        {
            clsDRWrapper rsClass = new clsDRWrapper();
            // vbPorter upgrade warning: curFullRate As Decimal	OnWrite(string)
            Decimal curFullRate;
            // vbPorter upgrade warning: curProrateedfee As Decimal	OnWrite(string)
            Decimal curProrateedfee;

            try
            {
                if (frmDataInput.InstancePtr.chkRental.CheckState == Wisej.Web.CheckState.Checked &&
                    (MotorVehicle.Statics.Class == "CR" || MotorVehicle.Statics.Class == "BB" ||
                     MotorVehicle.Statics.Class == "BH" || MotorVehicle.Statics.Class == "CM" ||
                     MotorVehicle.Statics.Class == "LS" || MotorVehicle.Statics.Class == "UM" ||
                     MotorVehicle.Statics.Class == "AG" || MotorVehicle.Statics.Class == "TS" ||
                     MotorVehicle.Statics.Class == "SW" || MotorVehicle.Statics.Class == "BC" ||
                     MotorVehicle.Statics.Class == "AW"))
                {
                    frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(
                        MotorVehicle.GetRegRate(this, MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass) * 2,
                        "#,##0.00");
                    // kk11202015 change from P4, P5
                }
                else if (frmDataInput.InstancePtr.chkRental.CheckState == Wisej.Web.CheckState.Checked &&
                         (MotorVehicle.Statics.Class == "PC" &&
                          (MotorVehicle.Statics.Subclass != "P5" && MotorVehicle.Statics.Subclass != "P6")))
                {
                    frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(
                        MotorVehicle.GetRegRate(this, MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass) * 2,
                        "#,##0.00");
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(
                        MotorVehicle.GetRegRate(this, MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass),
                        "#,##0.00");
                }

                if (blnLoadingForm)
                {
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text =
                        Strings.Format(MotorVehicle.GetFullStateTransferCredit(), "#,##0.00");
                }

                // cg12282017 tromv-1269 state reversed decision - NOT ok to prorate and transfer AP
                // kk04172017 tromv-1269 Per Sue at BMV Audit - OK to prorate and transfer AP
                // kk03242016 tromv-1130  Do not prorate on a transfer for a fleet vehicle - Reversed the logic and swapped the if and else sections
                // If (Not NewFleet Or optFleet(cnGroup).Value = True) And Not IsProratedMotorcycle(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                if ((MotorVehicle.Statics.NewFleet && fleetGroupType != FleetGroupType.Group &&
                     MotorVehicle.Statics.RegistrationType != "NRT" &&
                     MotorVehicle.Statics.RegistrationType != "RRT") ||
                    MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                        MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                        MotorVehicle.Statics.RegistrationType))
                {
                    // If (NewFleet And optFleet(cnGroup).Value <> True) Or IsProratedMotorcycle(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                    if (MotorVehicle.Statics.RegistrationType == "NRT" ||
                        MotorVehicle.Statics.RegistrationType == "RRT")
                    {
                        curFullRate = FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text);
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text =
                            Strings.Format(Prorate(frmFeesInfo.InstancePtr.txtFEERegFee.Text, DateDifference()),
                                "#,##0.00");
                        // kk11282016 tromv-1211  This is MC transfer - prorate transfer from full reg fee
                        // txtStateCredit.Text = Format(Prorate(frmFeesInfo.txtFEECreditTransfer.Text, DateDifference), "#,##0.00")
                        txtStateCredit.Text =
                            Strings.Format(Prorate(curFullRate.ToString(), DateDifference()), "#,##0.00");
                        // frmFeesInfo.txtFEECreditTransfer.Text = Format(Prorate(frmFeesInfo.txtFEECreditTransfer.Text, DateDifference), "#,##0.00")
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text =
                            Strings.Format(Prorate(curFullRate.ToString(), DateDifference()), "#,##0.00");
                    }
                    else
                    {
                        curFullRate = FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text);
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text =
                            Strings.Format(Prorate(frmFeesInfo.InstancePtr.txtFEERegFee.Text, DateDifference()),
                                "#,##0.00");
                        curProrateedfee = FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text);
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(curFullRate, "#,##0.00");
                        txtStateCredit.Text = Strings.Format(curFullRate - curProrateedfee, "#,##0.00");
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text =
                            Strings.Format(curFullRate - curProrateedfee, "#,##0.00");
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.StateHalfRate &&
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
                    {
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(
                            FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEERegFee.Text)) / 2,
                            "#,##0.00");
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                            FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text)) / 2,
                            "#,##0.00");
                    }
                }

                // Dave 12/28/06 got commercial credit and subrtracted that from state credit to get transfer credit to fix call #107306
                MotorVehicle.GetCommCredit(MotorVehicle.Statics.rsPlateForReg, this);
                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                        FCConvert.ToDecimal(Conversion.Val(txtStateCredit.Text)) -
                        FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEECreditComm.Text)), "#,##0.00");
                }

                if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2)
                {
                    // @DJW 9/4/08 Put this if statement in to accoutn for new legislation that changed reg rate form 25 to 35 for a lot of vehicles
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") == 35 &&
                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull") == 25)
                    {
                        txtStateCredit.Text =
                            Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"),
                                "#,##0.00");
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text =
                            Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"),
                                "#,##0.00");
                    }
                    else
                    {
                        if (intCorrCorrectOriginal == DialogResult.Yes)
                        {
                            if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("TwoYear") &&
                                chk2Year.CheckState == Wisej.Web.CheckState.Unchecked)
                            {
                                txtStateCredit.Text =
                                    Strings.Format(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull") / 2,
                                        "#,##0.00");
                                frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull") / 2,
                                    "#,##0.00");
                            }
                            else
                            {
                                txtStateCredit.Text =
                                    Strings.Format(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"),
                                        "#,##0.00");
                                frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"),
                                    "#,##0.00");
                            }
                        }
                        else
                        {
                            if (MotorVehicle.Statics.StateHalfRate)
                            {
                                txtStateCredit.Text =
                                    Strings.Format(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull") / 2,
                                        "#,##0.00");
                                frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull") / 2,
                                    "#,##0.00");
                            }
                            else
                            {
                                txtStateCredit.Text =
                                    Strings.Format(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull"),
                                        "#,##0.00");
                                frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull"), "#,##0.00");
                            }
                        }
                    }

                    if (intCorrInitialPlate == DialogResult.Yes)
                    {
                        frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(
                            MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"),
                                chkTrailerVanity.CheckState == Wisej.Web.CheckState.Checked,
                                chk2Year.CheckState == Wisej.Web.CheckState.Checked), "#,##0.00");
                        if (Convert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text) != 0 &&
                            (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "TL" ||
                             MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "CL"))
                        {
                            chkTrailerVanity.CheckState = Wisej.Web.CheckState.Checked;
                        }
                        else
                        {
                            chkTrailerVanity.CheckState = Wisej.Web.CheckState.Unchecked;
                        }
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(0, "#,##0.00");
                    }

                    if (MotorVehicle.Statics.gboolCorrectionExtraFee)
                    {
                        if (MotorVehicle.IsSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")))
                        {
                            if (!chkSpecialtyCorrExtraFee.Enabled)
                            {
                                chkSpecialtyCorrExtraFee.Visible = true;
                                chkSpecialtyCorrExtraFee.Enabled = true;
                                chkSpecialtyCorrExtraFee.Value = FCCheckBox.ValueSettings.Checked;
                            }
                        }
                    }
                }
                else if (MotorVehicle.Statics.RegistrationType == "GVW")
                {
                    frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(0, "#,##0.00");
                }
                else
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) != "P")
                    {
                        frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(
                            MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"),
                                chkTrailerVanity.CheckState == Wisej.Web.CheckState.Checked,
                                chk2Year.CheckState == Wisej.Web.CheckState.Checked), "#,##0.00");
                        if (Convert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text) != 0 &&
                            (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "TL" ||
                             MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "CL"))
                        {
                            chkTrailerVanity.CheckState = Wisej.Web.CheckState.Checked;
                        }
                        else
                        {
                            chkTrailerVanity.CheckState = Wisej.Web.CheckState.Unchecked;
                        }
                    }
                }

                if (MotorVehicle.Statics.ForcedPlate == "T")
                {
                    rsClass.OpenRecordset("SELECT OutOfRotation FROM DefaultInfo");
                    if (!rsClass.EndOfFile())
                        frmFeesInfo.InstancePtr.txtFEEReservePlate.Text =
                            Strings.Format(rsClass.Get_Fields_Decimal("OutOfRotation"), "#,##0.00");
                    else
                        frmFeesInfo.InstancePtr.txtFEEReservePlate.Text = "25.00";
                }

                rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" +
                                      MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" +
                                      MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
                if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
                {
                    // do nothing
                }
                else
                {
                    MessageBox.Show("Error with Class Records.  Please call TRIO.", "Class Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }

                if (MotorVehicle.Statics.RegistrationType != "CORR")
                {
                    if (MotorVehicle.Statics.ForcedPlate != "P" &&
                        MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "")
                    {
                        if (MotorVehicle.Statics.RegistrationType == "RRR" && MotorVehicle.Statics.NewPlate &&
                            MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) !=
                            "" &&
                            (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                             MotorVehicle.GetSpecialtyPlate(
                                 MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class"))
                            ) && intReturningPlate == 0 && MotorVehicle.Statics.LostPlate == false)
                        {
                            intReturningPlate = MessageBox.Show("Are the old plates being returned?",
                                "Returning Plates?",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        }
                        else if (intReturningPlate != 0)
                        {
                            // do nothing
                        }
                        else
                        {
                            intReturningPlate = DialogResult.Yes;
                        }

                        if (MotorVehicle.Statics.NewPlate ||
                            (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R" &&
                             MotorVehicle.Statics.RegistrationType == "NRR"))
                        {
                            if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                            {
                                if (
                                    (MotorVehicle.GetSpecialtyPlate(
                                         MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) !=
                                     MotorVehicle.GetSpecialtyPlate(
                                         MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class")) &&
                                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) !=
                                     "S" &&
                                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) !=
                                     "O"
                                    ) || intReturningPlate != DialogResult.Yes)
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = Strings.Format(
                                        rsClass.Get_Fields_Decimal("OtherFeesNew") +
                                        rsClass.Get_Fields_Decimal("OtherFeesReNew"), "#,##0.00");
                                }
                                else
                                {
                                    if (MotorVehicle.Statics.RegistrationType == "RRT" ||
                                        MotorVehicle.Statics.RegistrationType == "NRT")
                                    {
                                        // Or (RegistrationType = "CORR" And PlateType = 2) Then
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                    }
                                    else
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                            Strings.Format(
                                                rsClass.Get_Fields_Decimal("OtherFeesNew") +
                                                rsClass.Get_Fields_Decimal("OtherFeesRenew"), "#,##0.00");
                                    }
                                }
                            }
                            else
                            {
                                if (
                                    (MotorVehicle.GetSpecialtyPlate(
                                         MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) !=
                                     MotorVehicle.GetSpecialtyPlate(
                                         MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class")) &&
                                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) !=
                                     "S" &&
                                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) !=
                                     "O"
                                    ) || intReturningPlate != DialogResult.Yes)
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                        Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesNew"), "#,##0.00");
                                }
                                else
                                {
                                    if (MotorVehicle.Statics.RegistrationType == "RRT" ||
                                        MotorVehicle.Statics.RegistrationType == "NRT")
                                    {
                                        // Or (RegistrationType = "CORR" And PlateType = 2) Then
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                    }
                                    else
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                            Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesReNew"), "#,##0.00");
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                            {
                                // If (GetSpecialtyPlate(rsFinal.Fields("Class")) <> GetSpecialtyPlate(rsFinalCompare.Fields("Class")) And rsFinal.Fields("PlateType") <> "S" And rsFinal.Fields("PlateType") <> "O") Or intReturningPlate <> vbYes Then
                                // frmFeesInfo.txtFEEPlateClass.Text = Format(rsClass.Fields("OtherFeesNew") + rsClass.Fields("OtherFeesRenew"), "#,##0.00")
                                // Else
                                if (MotorVehicle.Statics.RegistrationType == "RRT" ||
                                    MotorVehicle.Statics.RegistrationType == "NRT")
                                {
                                    // Or (RegistrationType = "CORR" And PlateType = 2) Then
                                    if (FCConvert.ToString(
                                            MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) ==
                                        "T")
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                    }
                                    else
                                    {
                                        if (intCorrSpecialtyPlate == 0)
                                        {
                                            intCorrSpecialtyPlate = MessageBox.Show("Is this a new Specialty Plate?",
                                                "Specialty Plate Fee?", MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question);
                                        }

                                        if (intCorrSpecialtyPlate == DialogResult.Yes)
                                        {
                                            frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                                Strings.Format(
                                                    rsClass.Get_Fields_Decimal("OtherFeesNew") +
                                                    rsClass.Get_Fields_Decimal("OtherFeesRenew"), "#,##0.00");
                                        }
                                        else
                                        {
                                            frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                                Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesRenew") * 2,
                                                    "#,##0.00");
                                        }
                                    }
                                }
                                else
                                {
                                    if (MotorVehicle.Statics.RegistrationType == "NRR" &&
                                        MotorVehicle.Statics.PlateType == 2)
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                            Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesNew") * 2, "#,##0.00");
                                    }
                                    else
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                            Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesRenew") * 2,
                                                "#,##0.00");
                                    }
                                }

                                // End If
                            }
                            else
                            {
                                // If (GetSpecialtyPlate(rsFinal.Fields("Class")) <> GetSpecialtyPlate(rsFinalCompare.Fields("Class")) And rsFinal.Fields("PlateType") <> "S" And rsFinal.Fields("PlateType") <> "O") Or intReturningPlate <> vbYes Then
                                // frmFeesInfo.txtFEEPlateClass.Text = Format(rsClass.Fields("OtherFeesNew"), "#,##0.00")
                                // Else
                                if (MotorVehicle.Statics.RegistrationType == "RRT" ||
                                    MotorVehicle.Statics.RegistrationType == "NRT")
                                {
                                    // Or (RegistrationType = "CORR" And PlateType = 2) Then
                                    if (FCConvert.ToString(
                                            MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) ==
                                        "T")
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                    }
                                    else
                                    {
                                        if (intCorrSpecialtyPlate == 0)
                                        {
                                            intCorrSpecialtyPlate = MessageBox.Show("Is this a new Specialty Plate?",
                                                "Specialty Plate Fee?", MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question);
                                        }

                                        if (intCorrSpecialtyPlate == DialogResult.Yes)
                                        {
                                            frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                                Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesNew"), "#,##0.00");
                                        }
                                        else
                                        {
                                            frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                                Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesRenew"),
                                                    "#,##0.00");
                                        }
                                    }
                                }
                                else
                                {
                                    if (MotorVehicle.Statics.RegistrationType == "NRR" &&
                                        MotorVehicle.Statics.PlateType == 2)
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                            Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesNew"), "#,##0.00");
                                    }
                                    else
                                    {
                                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text =
                                            Strings.Format(rsClass.Get_Fields_Decimal("OtherFeesRenew"), "#,##0.00");
                                    }
                                }

                                // End If
                            }
                        }
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                    }
                }

                if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 1)
                {
                    FillCorrectionFeesScreen();
                }
                else
                {
                    FillFeesScreen();
                }

                txtFees.Text = frmFeesInfo.InstancePtr.txtFEETotal.Text;
            }
            finally
            {
                rsClass.Dispose();
            }
            
        }

        public void FillFeesScreen()
        {
            // vbPorter upgrade warning: total As Decimal	OnWrite(int, Decimal, double)
            Decimal total;
            Decimal CreditTotal;
            // vbPorter upgrade warning: StateRate As Decimal	OnWrite(Decimal, int)
            Decimal StateRate;
            // vbPorter upgrade warning: TransferCredit As Decimal	OnWrite(Decimal, int)
            Decimal TransferCredit;
            clsDRWrapper rsLostPlateFee = new clsDRWrapper();
            // vbPorter upgrade warning: curLostPlateFee As Decimal	OnWrite(int, Decimal, double)
            Decimal curLostPlateFee;
            // vbPorter upgrade warning: curLostStickers As Decimal	OnWrite(int, double, Decimal)
            Decimal curLostStickers;
            Decimal curOutOfRotationFee;
            clsDRWrapper rsPlates = new clsDRWrapper();
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;

            try
            {
                if (frmDataInput.InstancePtr.chkETO.CheckState == Wisej.Web.CheckState.Unchecked &&
                    chkAmputeeVet.CheckState == Wisej.Web.CheckState.Unchecked &&
                    chkStateExempt.CheckState == Wisej.Web.CheckState.Unchecked)
                {
                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEERegFee.Text) != "")
                    {
                        MotorVehicle.Statics.FullRegRate =
                            FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text);
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text = "0.00";
                        MotorVehicle.Statics.FullRegRate = 0;
                    }

                    if (!MotorVehicle.Statics.rsFinal.IsFieldNull("TransferCreditFull"))
                    {
                        MotorVehicle.Statics.FullRegCredit =
                            MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditFull");
                    }
                    else
                    {
                        MotorVehicle.Statics.FullRegCredit = 0;
                    }

                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) != "")
                    {
                        TransferCredit = FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text);
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = "0.00";
                        TransferCredit = 0;
                    }

                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEERegFee.Text) != "")
                    {
                        StateRate = FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text);
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEERegFee.Text = "0.00";
                        StateRate = 0;
                    }

                    if (MotorVehicle.Statics.FromFeesScreen == false)
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
                        {
                            if (frmDataInput.InstancePtr.chkHalfRateState.CheckState == Wisej.Web.CheckState.Checked &&
                                !MotorVehicle.Statics.StateHalfRate)
                            {
                                if (frmDataInput.InstancePtr.chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                                {
                                    StateRate *= 1.5m;
                                }
                                else
                                {
                                    StateRate *= 0.5m;
                                }

                                if (TransferCredit ==
                                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateFull"))
                                {
                                    // DJW 02/13/2008 - Added options o half rated 2 year transfers woudl show correctly
                                    if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("RegHalf") &&
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("TwoYear"))
                                    {
                                        // do nothing
                                    }
                                    else
                                    {
                                        if (frmDataInput.InstancePtr.chk2Year.CheckState ==
                                            Wisej.Web.CheckState.Checked)
                                        {
                                            TransferCredit = (TransferCredit / 2) * 1.5m;
                                        }
                                        else
                                        {
                                            TransferCredit *= 0.5m;
                                        }
                                    }
                                }
                            }
                            else if (frmDataInput.InstancePtr.chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                            {
                                if (chkHalfRateState.CheckState == CheckState.Checked)
                                {
                                    StateRate *= 3;
                                }
                                else
                                {
                                    StateRate *= 2;
                                }
                            }
                        }
                    }

                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEECreditComm.Text) != "")
                    {
                        CreditTotal =
                            FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEECreditComm.Text)) +
                            TransferCredit;
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "0.00";
                        CreditTotal = TransferCredit;
                    }

                    txtStateCredit.Text = Strings.Format(CreditTotal, "#,##0.00");
                    txtRate.Text = Strings.Format(StateRate, "#,##0.00");
                    // 
                    rsLostPlateFee.OpenRecordset("SELECT * FROM DefaultInfo");
                    curLostPlateFee = 0;
                    curLostStickers = 0;
                    curOutOfRotationFee = 0;

                    if (!String.IsNullOrEmpty(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text))
                    {
	                    if (MotorVehicle.Statics.RegistrationType == "RRR" ||
	                        MotorVehicle.Statics.RegistrationType == "RRT" ||
	                        MotorVehicle.Statics.RegistrationType == "NRT" ||
	                        MotorVehicle.Statics.RegistrationType == "CORR")
	                    {
		                    if (!MotorVehicle.Statics.LostPlate &&
		                        Convert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text) > 0)
		                    {
			                    MotorVehicle.Statics.LostPlate = true;
		                    }
	                    }
                    }

                    if (MotorVehicle.Statics.LostPlate || blnChargeForPlate ||
                        (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 3))
                    {
                        // DJW@07012014 replacement plates are changed a fee TROMV-891
                        rsPlates.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" +
                                               MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") +
                                               "' AND SystemCode = '" +
                                               MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
                        if (rsPlates.EndOfFile() != true && rsPlates.BeginningOfFile() != true)
                        {
                            if (FCConvert.ToInt32(rsPlates.Get_Fields_Int32("NumberOfPlateStickers")) == 0)
                            {
                                if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text) !=
                                    "")
                                {
                                    curLostPlateFee =
                                        FCConvert.ToDecimal(
                                            Conversion.Val(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text));
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                                    curLostPlateFee = 0;
                                }
                            }
                            else
                            {
                                if (MotorVehicle.GetInitialPlateFees2(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) != 0 &&
                                    MotorVehicle.GetInitialPlateFees2(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class"),
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Plate")) != 0)
                                {
                                    frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                                    curLostPlateFee = 0;
                                }
                                else
                                {
                                    if (!MotorVehicle.Statics.FromFeesScreen)
                                    {
                                        if (rsLostPlateFee.EndOfFile() != true &&
                                            rsLostPlateFee.BeginningOfFile() != true)
                                        {
                                            rsLostPlateFee.MoveLast();
                                            rsLostPlateFee.MoveFirst();
                                            curLostPlateFee = FCConvert.ToDecimal(
                                                rsLostPlateFee.Get_Fields_Decimal("ReplacementPlate") *
                                                rsPlates.Get_Fields_Int32("NumberOfPlateStickers"));
                                        }
                                    }
                                    else
                                    {
                                        {
                                            if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr
                                                    .txtFEEReplacementFee.Text) != "")
                                            {
                                                curLostPlateFee =
                                                    FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr
                                                        .txtFEEReplacementFee.Text));
                                            }
                                            else
                                            {
                                                frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                                                curLostPlateFee = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text) != "")
                            {
                                curLostPlateFee =
                                    FCConvert.ToDecimal(
                                        Conversion.Val(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text));
                            }
                            else
                            {
                                frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                                curLostPlateFee = 0;
                            }
                        }

                        if (MotorVehicle.Statics.NoReplacementFee)
                        {
                            frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                            curLostPlateFee = 0;
                        }

                        // kk03152017 tromv-1229  Don't charge repl plate fee if expired plates are Exempt (see Design Spec 3-12 New Registration/Replacement Plate)
                        if (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 3)
                        {
                            var specialtyPlate = MotorVehicle.GetSpecialtyPlate(
                                MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class"));

                            if ((specialtyPlate == "AC") || (specialtyPlate == "AF") || (specialtyPlate == "AG") ||
                                (specialtyPlate == "AW") || (specialtyPlate == "BB") || (specialtyPlate == "BH") ||
                                (specialtyPlate == "BC") || (specialtyPlate == "CR") || (specialtyPlate == "LB") ||
                                (specialtyPlate == "LC") || (specialtyPlate == "RV") || (specialtyPlate == "SW") ||
                                (specialtyPlate == "TS") || (specialtyPlate == "UM"))
                            {
                                frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                                curLostPlateFee = 0;
                            }
                            else
                            {
                                if (MotorVehicle.GetInitialPlateFees2(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class"),
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Plate")) != 0)
                                {
                                    frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                                    curLostPlateFee = 0;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text) != "")
                        {
                            curLostPlateFee =
                                FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text));
                        }
                        else
                        {
                            frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                            curLostPlateFee = 0;
                        }
                    }

                    if (MotorVehicle.Statics.ForcedPlate == "S")
                    {
                        if (Conversion.Val(frmFeesInfo.InstancePtr.txtFEEReservePlate.Text) == 0 &&
                            !MotorVehicle.Statics.blnReserve)
                        {
                            ans = MessageBox.Show("Do you wish to charge a Special Request Fee?",
                                "Charge Special Request Fee?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                curOutOfRotationFee =
                                    FCConvert.ToDecimal(rsLostPlateFee.Get_Fields_Decimal("OutOfRotation"));
                                frmFeesInfo.InstancePtr.txtFEEOutOfRotation.Text =
                                    Strings.Format(curOutOfRotationFee, "#,##0.00");
                            }

                            MotorVehicle.Statics.blnReserve = true;
                        }
                    }

                    if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtMonthCharge.Text) > 0)
                    {
                        curLostStickers = FCConvert.ToDecimal(rsLostPlateFee.Get_Fields_Double("Stickers") *
                                                              (Conversion.Val(txtYearCharge.Text) +
                                                               Conversion.Val(txtMonthCharge.Text)));
                    }

                    if (MotorVehicle.Statics.RegistrationType == "CORR" && txtClass.Text == "IU")
                    {
                        curLostStickers += rsLostPlateFee.Get_Fields_Decimal("Stickers") * 2;
                    }

                    frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = Strings.Format(curLostPlateFee, "#,##0.00");
                    frmFeesInfo.InstancePtr.txtFEEStickerFee.Text = Strings.Format(curLostStickers, "#,##0.00");
                    // 
                    frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(StateRate, "#,##0.00");
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(TransferCredit, "#,##0.00");
                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text) == "")
                    {
                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                    }

                    // Get Total
                    total = 0;
                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEERegFee.Text) != "")
                    {
                        total += FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text);
                    }

                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEECreditComm.Text) != "")
                    {
                        total -= FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditComm.Text);
                    }

                    if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) != "" &&
                        fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEERegFee.Text) != "")
                    {
                        if (FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) >
                            FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text))
                        {
                            total -= FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text);
                        }
                        else
                        {
                            total -= FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text);
                        }
                    }
                    else if (fecherFoundation.Strings.Trim(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text) != "")
                    {
                        total -= FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text);
                    }

                    if (total < 0)
                        total = 0;
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEOutOfRotation.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEReservePlate.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEStickerFee.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEETransferFee.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtDupRegFee.Text));
                    total += FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtIncreaseRVW.Text));
                    total -= FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEGiftCertificate.Text));
                    if (total < 0)
                        total = 0;
                    // Finshed Adding Total
                    frmFeesInfo.InstancePtr.txtFEETotal.Text = Strings.Format(total, "#,##0.00");
                    txtFees.Text = frmFeesInfo.InstancePtr.txtFEETotal.Text;
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtFEERegFee.Text = "0.00";
                    if (frmDataInput.InstancePtr.chkETO.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = "0.00";
                    }

                    frmFeesInfo.InstancePtr.txtFEEOutOfRotation.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtFEEReservePlate.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtFEEStickerFee.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtFEETransferFee.Text = "0.00";
                    // .txtFEEPlateClass.Text = "0.00"
                    frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtDupRegFee.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtIncreaseRVW.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = "0.00";
                    frmFeesInfo.InstancePtr.txtFEEGiftCertificate.Text = "0.00";
                    total = FCConvert.ToDecimal(Conversion.Val(frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text) +
                                                Conversion.Val(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                    frmFeesInfo.InstancePtr.txtFEETotal.Text = Strings.Format(total, "#,##0.00");
                    txtRate.Text = "0.00";
                    txtStateCredit.Text = "0.00";
                    txtFees.Text = Strings.Format(total, "#,##0.00");
                }
            }
            finally
            {
                rsLostPlateFee.Dispose();
                rsPlates.Dispose();
            }
        }

        public void FillTotalDue()
        {
            double dblExciseTax;
            double dblTownCredit;
            double dblTransferCharge;
            double dblAgentFee;
            double dblFees;
            double dblSalesTax;
            double dblTitle;
            // 
            dblExciseTax = 0;
            dblTownCredit = 0;
            dblTransferCharge = 0;
            dblAgentFee = 0;
            dblFees = 0;
            dblSalesTax = 0;
            dblTitle = 0;
            // 
            dblExciseTax = Conversion.Val(txtExciseTax.Text);
            dblTownCredit = Conversion.Val(txtTownCredit.Text);
            dblTransferCharge = Conversion.Val(txtTransferCharge.Text);
            dblAgentFee = Conversion.Val(txtAgentFee.Text);
            dblFees = Conversion.Val(txtFees.Text);
            dblSalesTax = Conversion.Val(txtSalesTax.Text);
            dblTitle = Conversion.Val(txtTitle.Text);
            // 
            if (MotorVehicle.Statics.ETO)
            {
                MotorVehicle.Statics.GrandTotal = dblExciseTax - dblTownCredit;
                // ** This line was taken out on 7/20/00 by ajb/bab  - Val(txtStateCredit.Text)
                if (MotorVehicle.Statics.GrandTotal < 0)
                    MotorVehicle.Statics.GrandTotal = 0;
                MotorVehicle.Statics.GrandTotal += dblTransferCharge;
            }
            else if (MotorVehicle.Statics.ExciseAP)
            {
                MotorVehicle.Statics.GrandTotal = dblAgentFee + dblFees + dblSalesTax + dblTitle;
            }
            else
            {
                MotorVehicle.Statics.GrandTotal = dblExciseTax - dblTownCredit;
                // ** This line was taken out on 7/20/00 by ajb/bab  - Val(txtStateCredit.Text)
                if (MotorVehicle.Statics.GrandTotal < 0)
                    MotorVehicle.Statics.GrandTotal = 0;
                MotorVehicle.Statics.GrandTotal += dblTitle + dblAgentFee + dblTransferCharge + dblFees + dblSalesTax;
                // ** This line was taken out on 7/20/00 by ajb/bab    + Val(txtRate.Text)
            }

            // 
            lblTotalDue.Text = Strings.Format(MotorVehicle.Statics.GrandTotal, "Currency");
        }

        public int MileageNumber()
        {
            int MileageNumber = 0;
            try
            {
                // On Error GoTo endtag
                fecherFoundation.Information.Err().Clear();
                MileageNumber =
                    FCConvert.ToInt32(
                        Math.Round(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("Odometer"))));
                return MileageNumber;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MileageNumber = 0;
            }

            return MileageNumber;
        }

        public void CheckEAP()
        {
            string rescode = "";
            if (chkEAP.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (!MotorVehicle.Statics.blnCheck)
                {
                    frmExcisePaid.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    //FC:FINAL:DDU:#2175 - hold disposing of form, we need just to hide and use it's controls data
                    frmExcisePaid.InstancePtr.dontDispose = false;
                    if (MotorVehicle.Statics.ExciseAP == false)
                    {
                        return;
                    }

                    MotorVehicle.Statics.ExciseAP = true;
                    txtExciseTaxDate.Text =
                        Strings.Format(frmExcisePaid.InstancePtr.txtExcisePaidDate.Text, "MM/dd/yy");
                    // txtCreditNumber.Text = frmExcisePaid.txtExciseReceiptNumber
                    txtExciseTax.Text = frmExcisePaid.InstancePtr.txtExciseTaxPaid.Text;
                    txtTownCredit.Text = frmExcisePaid.InstancePtr.txtExciseCreditAmount.Text;
                    txtSubTotal.Text = frmExcisePaid.InstancePtr.txtExciseSubTotal.Text;
                    txtTransferCharge.Text = frmExcisePaid.InstancePtr.txtExciseTransferCharge.Text;
                    txtresidenceCode.Text = frmExcisePaid.InstancePtr.txtExciseResCode.Text;
                    txtBalance.Text = frmExcisePaid.InstancePtr.txtExciseTotal.Text;
                }
                else
                {
                    //txtExciseTaxDate.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate"), "MM/dd/yy");
                    txtExciseTax.Text =
                        Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"), "#,##0.00");
                    txtTownCredit.Text =
                        Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "#,##0.00");
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull") -
                        MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") < 0)
                    {
                        txtSubTotal.Text = "0.00";
                    }
                    else
                    {
                        txtSubTotal.Text =
                            Strings.Format(
                                MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") -
                                MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "#,##0.00");
                    }

                    txtTransferCharge.Text =
                        Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"),
                            "#,##0.00");
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode")).Length < 5)
                    {
                        txtresidenceCode.Text = "0" + MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
                    }
                    else
                    {
                        txtresidenceCode.Text =
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
                    }

                    txtBalance.Text =
                        Strings.Format(
                            FCConvert.ToDecimal(txtSubTotal.Text) + FCConvert.ToDecimal(txtTransferCharge.Text),
                            "#,##0.00");
                    rescode = fecherFoundation.Strings.Trim(txtresidenceCode.Text);
                    MotorVehicle.Statics.strSql = "SELECT * FROM Residence";
                    
                    using (clsDRWrapper rsResCode = new clsDRWrapper())
                    {
                        rsResCode.OpenRecordset(MotorVehicle.Statics.strSql);
                        if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
                        {
                            rsResCode.MoveLast();
                            rsResCode.MoveFirst();
                            if (rsResCode.FindFirstRecord("Code", rescode))
                            {
                                txtLegalres.Text =
                                    fecherFoundation.Strings.Trim(FCConvert.ToString(rsResCode.Get_Fields_String("Town")));
                            }
                        }
                    }
                    
                    lblBalanceAP.Visible = true;
                    lblCreditAP.Visible = true;
                    lblCreditNumberAP.Visible = true;
                    lblExciseAP.Visible = true;
                    lblSubTotalAP.Visible = true;
                    lblTransferAP.Visible = true;
                    blnCalculateRequired = true;
                    MotorVehicle.Statics.ExciseAP = true;
                    return;
                }

                rescode = fecherFoundation.Strings.Trim(txtresidenceCode.Text);
                MotorVehicle.Statics.strSql = "SELECT * FROM Residence";
                using (clsDRWrapper rsResCode = new clsDRWrapper())
                {
                    rsResCode.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
                    {
                        rsResCode.MoveLast();
                        rsResCode.MoveFirst();
                        if (rsResCode.FindFirstRecord("Code", rescode))
                        {
                            txtLegalres.Text =
                                fecherFoundation.Strings.Trim(FCConvert.ToString(rsResCode.Get_Fields_String("Town")));
                        }
                    }
                }
            }
            else
            {
                MotorVehicle.Statics.ExciseAP = false;
            }

            lblBalanceAP.Visible = MotorVehicle.Statics.ExciseAP;
            lblCreditAP.Visible = MotorVehicle.Statics.ExciseAP;
            lblCreditNumberAP.Visible = MotorVehicle.Statics.ExciseAP;
            lblExciseAP.Visible = MotorVehicle.Statics.ExciseAP;
            lblSubTotalAP.Visible = MotorVehicle.Statics.ExciseAP;
            lblTransferAP.Visible = MotorVehicle.Statics.ExciseAP;
            blnCalculateRequired = true;
            frmExcisePaid.InstancePtr.Close();
        }

        public void TabStops()
        {
            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT" ||
                MotorVehicle.Statics.NewToTheSystem)
            {
                MotorVehicle.Statics.VehicleInfo = false;
            }

            txtReg1PartyID.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtAddress1.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtAddress2.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtCity.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtState.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtZip.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtCountry.TabStop = !MotorVehicle.Statics.DemographicInfo;
            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) != "N")
            {
                txtReg2PartyID.TabStop = !MotorVehicle.Statics.DemographicInfo;
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) != "N")
            {
                txtReg3PartyID.TabStop = !MotorVehicle.Statics.DemographicInfo;
            }

            txtResState.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtresidenceCode.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtLegalres.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtLegalResStreet.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtResCountry.TabStop = !MotorVehicle.Statics.DemographicInfo;
            txtColor2.TabStop = !MotorVehicle.Statics.VehicleInfo;
            if (MotorVehicle.Statics.Class != "TC")
            {
                txtMileage.TabStop = true;
                // True
            }

            txtBase.TabStop = false;
            // False
            txtMilYear.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtReg2ICM.TabStop = !MotorVehicle.Statics.VehicleInfo;
            if (fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")) == true)
            {
                txtExpires.TabStop = true;
            }
            else
            {
                txtExpires.TabStop = false;
            }

            txtEffectiveDate.TabStop = false;
            txtFuel.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtRegWeight.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtNetWeight.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtAxles.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtTires.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtStyle.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtColor1.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtModel.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtMake.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtYear.TabStop = !MotorVehicle.Statics.VehicleInfo;
            txtVin.TabStop = !MotorVehicle.Statics.VehicleInfo;
            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
            {
                txtCreditNumber.TabStop = true;
            }

            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "NRR")
            {
                txtPreviousTitle.TabStop = true;
                txtPriorTitleState.TabStop = true;
                txtCTANumber.TabStop = true;
                txtSalesTax.TabStop = true;
                txtTitle.TabStop = true;
            }

            txtMileage.TabStop = true;
            txtYStickerNumber.Enabled = true;
            txtMStickerNumber.Enabled = false;
            if (Conversion.Val(txtMonthCharge.Text) > 0 || Conversion.Val(txtMonthNoCharge.Text) > 0 ||
                Strings.Mid(MotorVehicle.Statics.RegistrationType, 1, 1) == "N")
            {
                txtMStickerNumber.Enabled = true;
            }

            // temporary maybe
            if (Conversion.Val(txtBase.Text) == 0)
            {
                txtBase.TabStop = true;
            }
            else
            {
                txtBase.TabStop = false;
            }

            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
            {
                MotorVehicle.Statics.VehicleInfo = true;
            }
        }

        public void StartTabs()
        {
            txtResState.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtResCountry.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtColor2.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtFleetNumber.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtZip.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtState.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtCity.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtCountry.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtAddress1.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtAddress2.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg2PartyID.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg3PartyID.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg1PartyID.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtMileage.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtBase.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtMilYear.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtCTANumber.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            //txtExciseTaxDate.TabStop = false;
            // IIf(RegistrationType = "CORR", True, False)
            txtAgentFee.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtYearNoCharge.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtMonthNoCharge.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtYearCharge.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtMonthCharge.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg3ICM.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg1ICM.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg2ICM.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg3LR.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg1LR.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg2LR.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtExpires.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg2DOB.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg3DOB.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtEffectiveDate.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtPreviousTitle.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtYStickerNumber.Enabled = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtMStickerNumber.Enabled = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtMilRate.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtUserID.TabStop = false;
            // IIf(RegistrationType = "CORR", True, False)
            txtresidenceCode.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtLegalres.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtLegalResStreet.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtUnitNumber.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtDOTNumber.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtReg1DOB.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtFuel.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtRegWeight.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtNetWeight.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtAxles.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtTires.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtStyle.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtColor1.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtModel.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtMake.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtYear.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtVin.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtRegistrationNumber.TabStop = false;
            // IIf(RegistrationType = "CORR", True, False)
            txtClass.TabStop = false;
            // IIf(RegistrationType = "CORR", True, False)
            txtTownCredit.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtBalance.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtExciseTax.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtSubTotal.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtTransferCharge.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtTitle.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtSalesTax.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtRate.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtFees.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtStateCredit.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
            txtCreditNumber.TabStop = (MotorVehicle.Statics.RegistrationType == "CORR" ? true : false);
        }

        public void Recalculate()
        {
            clsDRWrapper rsFees = new clsDRWrapper();
            Decimal ExciseTaxValue;
            Decimal AgentFeeValue;
            DialogResult answer = 0;
            clsDRWrapper rsClass = new clsDRWrapper();

            try
            {
                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    if (MotorVehicle.Statics.PlateType != 1)
                    {
                        if (MotorVehicle.Statics.PlateType == 2)
                        {
                            var specialtyPlate =
                                (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"))
                                );

                            if ((specialtyPlate == "CR") || (specialtyPlate == "UM") || (specialtyPlate == "LB") ||
                                (specialtyPlate == "BB") || (specialtyPlate == "BH") || (specialtyPlate == "AG") ||
                                (specialtyPlate == "AC") || (specialtyPlate == "AF") || (specialtyPlate == "TS") ||
                                (specialtyPlate == "PH") || (specialtyPlate == "VT") || (specialtyPlate == "SW") ||
                                (specialtyPlate == "BC") || (specialtyPlate == "AW") || (specialtyPlate == "LC"))
                            {
                                txtAgentFee.Text =
                                    Strings.Format(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"),
                                        "#,##0.00");
                            }
                            else
                            {
                                AgentFeeValue = MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFee");
                            }
                        }

                        CalculateStateFees();
                        // cg12282017 tromv-1269 state reversed decision - NOT ok to prorate and transfer AP
                        // kk04172017 tromv-1269 Per Sue at BMV Audit - OK to prorate and transfer AP
                        // kk03242016 tromv-1130  Don't prorate on transfer of fleet veh - Reverse the logic and swap the If and Else sections
                        // If (Not NewFleet Or optFleet(cnGroup).Value = True) And Not IsProratedMotorcycle(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                        if ((MotorVehicle.Statics.NewFleet && fleetGroupType == FleetGroupType.Group &&
                             MotorVehicle.Statics.RegistrationType != "NRT" &&
                             MotorVehicle.Statics.RegistrationType != "RRT") || MotorVehicle.IsProratedMotorcycle(
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                MotorVehicle.Statics.datMotorCycleEffective,
                                ref MotorVehicle.Statics.datMotorCycleExpires,
                                MotorVehicle.Statics.RegistrationType))
                        {
                            // If (NewFleet And optFleet(cnGroup).Value <> True) Or IsProratedMotorcycle(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                            if (DateDifference() < 12)
                            {
                                lblStateRatePR.Visible = true;
                                lblStateCreditPR.Visible = true;
                                lblStateFeesPR.Visible = true;
                            }
                        }
                        else
                        {
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
                            {
                                if (chkHalfRateState.CheckState == Wisej.Web.CheckState.Checked)
                                {
                                    // And Not StateHalfRate
                                    lblStateRateHR.Visible = true;
                                    lblStateCreditHR.Visible = true;
                                    lblStateFeesHR.Visible = true;
                                }
                                else if (chkHalfRateState.CheckState == Wisej.Web.CheckState.Unchecked)
                                {
                                    // And StateHalfRate
                                    lblStateRateHR.Visible = false;
                                    lblStateCreditHR.Visible = false;
                                    lblStateFeesHR.Visible = false;
                                }
                            }
                        }

                        return;
                    }
                }
                else if (MotorVehicle.Statics.RegistrationType == "GVW")
                {
                    CalculateStateFees();
                    lblTotalDue.Text = Strings.Format(txtFees.Text, "Currency");
                    return;
                }

                if (chkExciseExempt.CheckState == Wisej.Web.CheckState.Unchecked &&
                    chkAmputeeVet.CheckState == Wisej.Web.CheckState.Unchecked)
                {
                    MotorVehicle.Statics.MillYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtMilYear.Text)));
                    if (MotorVehicle.Statics.MillYear > 6)
                    {
                        MotorVehicle.Statics.MillYear = 6;
                        txtMilYear.Text = "6";
                    }
                    else if (MotorVehicle.Statics.MillYear < 1)
                    {
                        MotorVehicle.Statics.MillYear = 1;
                        txtMilYear.Text = "1";
                    }

                    if (txtBase.Text == "")
                        txtBase.Text = FCConvert.ToString(0);
                    ExciseTaxValue = MotorVehicle.ExciseTax(MotorVehicle.Statics.MillYear,
                        FCConvert.ToInt32(FCConvert.ToDouble(txtBase.Text)),
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
                    txtMilYear.Text = FCConvert.ToString(MotorVehicle.Statics.MillYear);
                    txtMilRate.Text = Strings.Format(MotorVehicle.Statics.MillRate, ".0000");

                    if ((!MotorVehicle.Statics.NewFleet || fleetGroupType == FleetGroupType.Group) &&
                        !MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                            MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                            MotorVehicle.Statics.RegistrationType))
                    {
                        if (MotorVehicle.Statics.LocalHalfRate &&
                            chkHalfRateLocal.CheckState == Wisej.Web.CheckState.Unchecked)
                        {
                            lblTownCreditHR.Visible = false;
                            lblExciseTaxHR.Visible = false;
                            lblLocalSubHR.Visible = false;
                            lblLocalBalanceHR.Visible = false;
                            txtTownCredit.Text =
                                Strings.Format(
                                    FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(txtTownCredit.Text))) * 2,
                                    "#,##0.00");
                            MotorVehicle.Statics.LocalHalfRate = false;
                        }
                    }

                    if ((!MotorVehicle.Statics.NewFleet || fleetGroupType == FleetGroupType.Group) &&
                        !MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                            MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                            MotorVehicle.Statics.RegistrationType))
                    {
                        if (chkHalfRateLocal.CheckState == Wisej.Web.CheckState.Checked && ExciseTaxValue > 5)
                        {
                            lblTownCreditHR.Visible = true;
                            lblExciseTaxHR.Visible = true;
                            lblLocalSubHR.Visible = true;
                            lblLocalBalanceHR.Visible = true;
                            if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                            {
                                ExciseTaxValue *= 1.5m;
                            }
                            else
                            {
                                ExciseTaxValue *= 0.5m;
                            }

                            if (!MotorVehicle.Statics.LocalHalfRate)
                            {
                                if (FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)) >=
                                    FCConvert.ToDecimal(Conversion.Val(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxFull"))))
                                {
                                    txtTownCredit.Text =
                                        Strings.Format(FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)) * 0.5m,
                                            "#,##0.00");
                                }

                                MotorVehicle.Statics.LocalHalfRate = true;
                            }
                        }
                        else if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                        {
                            ExciseTaxValue *= 2;
                        }
                    }
                }
                else
                {
                    // excise exempt
                    MotorVehicle.Statics.MillYear = 0;
                    ExciseTaxValue = FCConvert.ToDecimal("0.00");
                }

                // cg12282017 tromv-1269 state reversed decision - NOT ok to prorate and transfer AP
                // kk04172017 tromv-1269 Per Sue at BMV Audit - OK to prorate and transfer AP - revert this change
                // kk03242016 tromv-1130 Per BMV - Do not prorate and transfer on the same transaction unless it is MC
                // If (NewFleet And optFleet(cnGroup).Value <> True) Or IsProratedMotorcycle(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                if ((MotorVehicle.Statics.NewFleet && fleetGroupType != FleetGroupType.Group &&
                     MotorVehicle.Statics.RegistrationType != "NRT" &&
                     MotorVehicle.Statics.RegistrationType != "RRT") ||
                    MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                        MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                        MotorVehicle.Statics.RegistrationType))
                {
                    Decimal curFullExcise;
                    if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        curFullExcise = ExciseTaxValue * 2;
                        ExciseTaxValue += FCConvert.ToDecimal(Prorate(ExciseTaxValue.ToString(), DateDifference()));
                        txtTownCredit.Text =
                            Strings.Format(
                                Prorate(MotorVehicle.GetFullExciseTransferCredit().ToString(),
                                    DateDifference()),
                                "#,##0.00");
                        // txtTownCredit.Text = Format(curFullExcise - ExciseTaxValue, "#,##0.00")
                    }
                    else
                    {
                        curFullExcise = ExciseTaxValue;
                        if (!MotorVehicle.Statics.blnChangeToFleet)
                        {
                            if (MotorVehicle.Statics.RegistrationType == "CORR")
                            {
                                int prorateMonths = fecherFoundation.DateAndTime.DateDiff("m",
                                    fecherFoundation.DateAndTime.DateAdd("m", -1,
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("EffectiveDate")),
                                    fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                                ExciseTaxValue = FCConvert.ToDecimal(Prorate(ExciseTaxValue.ToString(), prorateMonths));
                                txtTownCredit.Text = Strings.Format(Prorate(
                                    MotorVehicle.GetFullExciseTransferCredit().ToString(),
                                    prorateMonths), "#,##0.00");
                            }
                            else
                            {
                                ExciseTaxValue =
                                    FCConvert.ToDecimal(Prorate(ExciseTaxValue.ToString(), DateDifference()));
                                txtTownCredit.Text =
                                    Strings.Format(
                                        Prorate(MotorVehicle.GetFullExciseTransferCredit().ToString(),
                                            DateDifference()), "#,##0.00");
                            }
                        }
                        else
                        {
                            ExciseTaxValue =
                                MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged") +
                                FCConvert.ToDecimal(Prorate(ExciseTaxValue.ToString(), DateDifference()));
                            txtTownCredit.Text =
                                Strings.Format(
                                    Prorate(MotorVehicle.GetFullExciseTransferCredit().ToString(),
                                        DateDifference()) + MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("ExciseTaxCharged"), "#,##0.00");
                        }
                    }

                    if (DateDifference() < 12)
                    {
                        lblTownCreditPR.Visible = true;
                        lblExciseTaxPR.Visible = true;
                        lblLocalSubPR.Visible = true;
                        lblLocalBalancePR.Visible = true;
                    }
                }

                if (!MotorVehicle.Statics.ExciseAP
                ) // && (MotorVehicle.Statics.RegistrationType != "CORR" || MotorVehicle.Statics.PlateType != 1))
                {
                    txtExciseTax.Text = Strings.Format(ExciseTaxValue, "#,##0.00");
                }

                // subtotal
                if (FCConvert.ToDecimal(Conversion.Val(txtExciseTax.Text)) -
                    FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)) > 0)
                {
                    txtSubTotal.Text =
                        Strings.Format(
                            FCConvert.ToDecimal(Conversion.Val(txtExciseTax.Text)) -
                            FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)), "#,##0.00");
                }
                else
                {
                    txtSubTotal.Text = "0.00";
                }

                if (MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "NRT")
                {
                    if (MotorVehicle.Statics.blnAllowExciseRebates)
                    {
                        if (FCConvert.ToDecimal(txtExciseTax.Text) - FCConvert.ToDecimal(txtTownCredit.Text) < 0)
                        {
                            lblCreditRebate.Visible = true;
                        }
                        else
                        {
                            lblCreditRebate.Visible = false;
                        }
                    }
                    else
                    {
                        lblCreditRebate.Visible = false;
                    }
                }
                else
                {
                    lblCreditRebate.Visible = false;
                }

                // agent fee
                if (modGlobalConstants.Statics.MuniName == "MAINE TRAILER")
                {
                    if (chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        AgentFeeValue = 30;
                    }
                    else
                    {
                        AgentFeeValue = 20;
                    }

                }
                else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "PROCUREMENT SPR")
                {
                    AgentFeeValue = 7;
                }
                else
                {
                    if (chkETO.CheckState == Wisej.Web.CheckState.Checked ||
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI")
                    {
                        AgentFeeValue = 0;
                    }
                    else
                    {
                        rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
                        string tempRes = "";
                        tempRes = fecherFoundation.Strings.Trim(txtresidenceCode.Text);
                        if (MotorVehicle.Statics.RegistrationType == "RRR")
                        {
                            if (fecherFoundation.Strings.Trim(tempRes) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode")))
                                || fecherFoundation.Strings.Trim(tempRes) == "99999")
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentFeeReRegLocal");
                            }
                            else
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentFeeReRegOOT");
                            }
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "RRT")
                        {
                            if (fecherFoundation.Strings.Trim(tempRes) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode")))
                                || fecherFoundation.Strings.Trim(tempRes) == "99999")
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentFeeReRegLocal");
                            }
                            else
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentFeeReRegOOT");
                            }

                            if (chkTransferExempt.CheckState != Wisej.Web.CheckState.Checked &&
                                chkExciseExempt.Enabled &&
                                FCConvert.ToDecimal(txtTownCredit.Text) > 0)
                            {
                                txtTransferCharge.Text =
                                    Strings.Format(rsFees.Get_Fields_Decimal("LocalTransferFee"), "#,##0.00");
                            }
                            else
                            {
                                txtTransferCharge.Text = Strings.Format(0, "#,##0.00");
                            }
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "NRR")
                        {
                            if (fecherFoundation.Strings.Trim(tempRes) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode")))
                                || fecherFoundation.Strings.Trim(tempRes) == "99999")
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentfeeNewLocal");
                            }
                            else
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentFeeNewOOT");
                            }
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "NRT")
                        {
                            if (fecherFoundation.Strings.Trim(tempRes) ==
                                fecherFoundation.Strings.Trim(
                                    FCConvert.ToString(rsFees.Get_Fields_String("ResidenceCode")))
                                || fecherFoundation.Strings.Trim(tempRes) == "99999")
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentfeeNewLocal");
                            }
                            else
                            {
                                AgentFeeValue = rsFees.Get_Fields_Decimal("AgentFeeNewOOT");
                            }

                            if (chkTransferExempt.CheckState != Wisej.Web.CheckState.Checked &&
                                chkExciseExempt.Enabled &&
                                FCConvert.ToDecimal(txtTownCredit.Text) > 0)
                            {
                                txtTransferCharge.Text =
                                    Strings.Format(rsFees.Get_Fields_Decimal("LocalTransferFee"), "#,##0.00");
                            }
                            else
                            {
                                txtTransferCharge.Text = Strings.Format(0, "#,##0.00");
                            }
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                        {
                            AgentFeeValue = rsFees.Get_Fields_Decimal("DuplicateDecal");
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "CORR")
                        {
                            AgentFeeValue = MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFee");
                        }
                        else
                        {
                            AgentFeeValue = 0;
                        }
                    }
                }

                // balance
                txtBalance.Text =
                    Strings.Format(
                        FCConvert.ToDecimal(Conversion.Val(txtSubTotal.Text)) +
                        FCConvert.ToDecimal(Conversion.Val(txtTransferCharge.Text)), "#,##0.00");
                if (chkAgentFeeExempt.CheckState == Wisej.Web.CheckState.Checked)
                {
                    txtAgentFee.Text = "0.00";
                }
                else
                {
                    if (MotorVehicle.Statics.RegistrationType != "GVW")
                    {
                        if (FCConvert.ToDecimal(Conversion.Val(txtAgentFee.Text)) != 0 &&
                            (FCConvert.ToDecimal(Conversion.Val(txtAgentFee.Text)) !=
                             FCConvert.ToDecimal(Conversion.Val(AgentFeeValue))) &&
                            chkETO.CheckState != Wisej.Web.CheckState.Checked)
                        {
                            answer = MessageBox.Show(
                                "The Agent Fee is different than the one calculated.  Do you wish to keep this Agent Fee?" +
                                "\r\n" + "\r\n" + "Current Fee:      " + txtAgentFee.Text + "\r\n" +
                                "Calculated Fee: " +
                                Strings.Format(AgentFeeValue, "#0.00"), "Keep Agent Fee?", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);
                            if (answer == DialogResult.Yes)
                            {
                                // do nothing
                            }
                            else
                            {
                                txtAgentFee.Text = Strings.Format(AgentFeeValue, "#0.00");
                            }
                        }
                        else
                        {
                            txtAgentFee.Text = Strings.Format(AgentFeeValue, "#0.00");
                        }
                    }
                }


                CalculateStateFees();
                // cg12282017 tromv-1269 state reversed decision - NOT ok to prorate and transfer AP
                // kk04172017 tromv-1269 Per Sue at BMV Audit - OK to prorate and transfer AP
                // kk03242016 tromv-1130 Per BMV - Do not prorate and transfer on the same transaction unless it is MC - Use the same criteria as earlier. Switch the If and Else sections around
                // If (Not NewFleet Or optFleet(cnGroup).Value = True) And Not IsProratedMotorcycle(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                if ((MotorVehicle.Statics.NewFleet && fleetGroupType != FleetGroupType.Group &&
                     MotorVehicle.Statics.RegistrationType != "NRT" &&
                     MotorVehicle.Statics.RegistrationType != "RRT") ||
                    MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                        MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                        MotorVehicle.Statics.RegistrationType))
                {
                    // If (NewFleet And optFleet(cnGroup).Value <> True) Or IsProratedMotorcycle(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                    if (DateDifference() < 12)
                    {
                        lblStateRatePR.Visible = true;
                        lblStateCreditPR.Visible = true;
                        lblStateFeesPR.Visible = true;
                    }
                }
                else
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
                    {
                        if (chkHalfRateState.CheckState == Wisej.Web.CheckState.Checked)
                        {
                            // And Not StateHalfRate
                            lblStateRateHR.Visible = true;
                            lblStateCreditHR.Visible = true;
                            lblStateFeesHR.Visible = true;
                        }
                        else if (chkHalfRateState.CheckState == Wisej.Web.CheckState.Unchecked)
                        {
                            // And StateHalfRate
                            lblStateRateHR.Visible = false;
                            lblStateCreditHR.Visible = false;
                            lblStateFeesHR.Visible = false;
                        }
                    }
                }

                FillTotalDue();
            }
            finally
            {
                rsFees.Dispose();
                rsClass.Dispose();
            }
        }

        

        public bool ValidateAll()
        {
            bool ValidateAll = false;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            // Print #100, Time & " - Start Validate"
            ValidateAll = true;
            txtVin.Focus();
            //App.DoEvents();
            if (ColorTranslator.FromOle(OldColor) != txtVin.BackColor)
                OldColor = ColorTranslator.ToOle(txtVin.BackColor);
            if (blnCalculateRequired == true)
            {
                // dave 3/31/2004 Trying to make it so people dont have to click the calculate label any more
                lblCalculate_Click();
            }

            // Print #100, Time & " - Validate 1"
            if (fecherFoundation.Strings.Trim(txtMessage.Text) != "" && cboMessageCodes.SelectedIndex == -1)
            {
                ShowWarningBox(
                    "You must select a message code to go with your message before you continue to process this registration.",
                    "Invalid Message Code");
                ValidateAll = false;
                return ValidateAll;
            }
            else if (cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == "")
            {
                ShowWarningBox(
                    "You must input a message to go with your code before you continue processing this registration.",
                    "No Message");
                ValidateAll = false;
                return ValidateAll;
            }


            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "AU" || MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "MC" ||
	                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "MX" || MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "PM" ||
	                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "VM" || MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "XV")
                {
                    if (chkCC.CheckState == Wisej.Web.CheckState.Unchecked)
                    {
                        ans = MessageBox.Show("Is this motorcycle 300cc or higher?", "300CC or Greater?",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.Yes)
                        {
                            chkCC.CheckState = Wisej.Web.CheckState.Checked;
                        }
                    }
                }
            }

            // Print #100, Time & " - Validate 2"
            if (fleetGroupType == FleetGroupType.Fleet || fleetGroupType == FleetGroupType.Group)
            {
                if (Conversion.Val(txtFleetNumber.Text) == 0)
                {
                    ShowWarningBox("You must either enter a fleet or group number before you may proceed",
                        "Invalid Fleet or Group Number");
                    ValidateAll = false;
                    return ValidateAll;
                }
            }

            if (VerifyLegalResidence() == false)
            {
                ValidateAll = false;
                return false;
            }

            if (VerifyCountry() == false)
            {
	            ValidateAll = false;
	            return false;
            }

            if (VerifyStyle() == false)
            {
                ValidateAll = false;
                return false;
            }

            if (VerifyLeaseTrustCodes() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            if (VerifyTaxID() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            if (VerifyExciseExempt() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 3"
            if (VerifyBattle() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 4"
            if (VerifyBase() == false)
            {
                txtBase.TabStop = true;
                txtBase.Focus();
                ValidateAll = false;
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 5"
            if (MotorVehicle.Statics.ETO == false)
            {
                if (VerifyMonthSticker() == false)
                {
                    ValidateAll = false;
                    txtMStickerNumber.Focus();
                    return ValidateAll;
                }
            }
            else
            {
                if (Conversion.Val(txtMStickerNumber.Text) == 0)
                {
                    // kk03272017
                    txtMStickerNumber.Text = "";
                }
            }

            if (MotorVehicle.Statics.ETO == false)
            {
                if (VerifyYearSticker() == false)
                {
                    ValidateAll = false;
                    txtYStickerNumber.Focus();
                    return ValidateAll;
                }
            }
            else
            {
                if (Conversion.Val(txtYStickerNumber.Text) == 0)
                {
                    // kk03272017
                    txtYStickerNumber.Text = "";
                }
                else
                {
                    if (fecherFoundation.Strings.Trim(txtYStickerNumber.Text) != "")
                    {
                        ValidateAll = false;
                        txtYStickerNumber.Text = "";
                        ShowWarningBox("You may not issue stickers on an Excise Tax Only Transaction.",
                            "Unable to Issue Stickers");
                        txtYStickerNumber.Text = "";
                        return ValidateAll;
                    }
                }
            }

            // Print #100, Time & " - Validate 6"
            if (VerifyMileage() == false)
            {
                ValidateAll = false;
                MessageBox.Show("You must input a valid mileage before you may continue", "Invalid Mileage",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMileage.Focus();
                return ValidateAll;
            }


            // Print #100, Time & " - Validate 8"
            if (VerifyClass() == false)
            {
                ValidateAll = false;
                txtClass.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 9"
            if (VerifyVIN() == false)
            {
                ValidateAll = false;
                txtVin.Focus();
                return false;
            }

            // Print #100, Time & " - Validate 10"
            if (VerifyYear() == false)
            {
                ValidateAll = false;
                txtYear.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 11"
            if (VerifyMake() == false)
            {
                ValidateAll = false;
                // kk05122016 tromvs-57  Move selection to separate form - hit the limit on number of controls
                // If fraCodes.Visible = False Then
                // txtMake.SetFocus
                // End If
                txtMake.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 12"
            if (VerifyModel() == false)
            {
                ValidateAll = false;
                txtModel.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 13"
            if (VerifyColor() == false)
            {
                ValidateAll = false;
                // kk05122016 tromvs-57  Move selection to separate form - hit the limit on number of controls
                // If fraCodes.Visible = False Then
                // txtColor1.SetFocus
                // End If
                txtColor1.Focus();
                return ValidateAll;
            }

            if (VerifyAddress() == false)
            {
                ValidateAll = false;
                txtAddress1.Focus();
                return ValidateAll;
            }

            if (VerifyCity() == false)
            {
                ValidateAll = false;
                txtCity.Focus();
                return ValidateAll;
            }

            if (VerifyState() == false)
            {
                ValidateAll = false;
                txtState.Focus();
                return ValidateAll;
            }

            if (VerifyZip() == false)
            {
                ValidateAll = false;
                txtZip.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 17"
            if (VerifyNetWeight() == false)
            {
                ValidateAll = false;
                txtNetWeight.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 18"
            if (VerifyRVW() == false)
            {
                ValidateAll = false;
                txtRegWeight.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 15"
            if (VerifyTires() == false)
            {
                ValidateAll = false;
                txtTires.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 16"
            if (VerifyAxles() == false)
            {
                ValidateAll = false;
                txtAxles.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 19"
            if (VerifyFuel() == false)
            {
                ValidateAll = false;
                txtFuel.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 20"
            if (VerifyRegistrants() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 21"
            if (VerifyDOB1() == false)
            {
                ValidateAll = false;
                txtReg1DOB.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 22"
            if (VerifyDOB2() == false)
            {
                ValidateAll = false;
                txtReg2DOB.Focus();
                return ValidateAll;
            }

            if (VerifyDOB3() == false)
            {
                ValidateAll = false;
                txtReg3DOB.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 23"
            // Print #100, Time & " - Validate 24"
            if (VerifyDOT() == false)
            {
                ValidateAll = false;
                txtDOTNumber.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 25"
            if (VerifyAddress() == false)
            {
                ValidateAll = false;
                txtAddress1.Focus();
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 26"
            if (VerifyResCode() == false)
            {
                ValidateAll = false;
                if (txtresidenceCode.Text == "99999")
                {
                    txtLegalres.Focus();
                }
                else
                {
                    txtresidenceCode.Focus();
                }

                return ValidateAll;
            }

            // Print #100, Time & " - Validate 27"
            //if (VerifyBase() == false)
            //{
            //    ValidateAll = false;
            //    txtBase.Focus();
            //    return ValidateAll;
            //}

            // Print #100, Time & " - Validate 28"
            if (VerifyPriorTitle() == false)
            {
                ValidateAll = false;
                // txtPreviousTitle.SetFocus
                return ValidateAll;
            }

            if (VerifyPriorTitleState() == false)
            {
                ValidateAll = false;
                // txtPriorTitleState.SetFocus
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 30"
            if (VerifyDefaultRescode() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 31"
            if (VerifyCTA() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            if (VerifyMil() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
            {
                if (!MotorVehicle.Statics.ETO)
                {
                    if (VerifySalesTax() == false)
                    {
                        ValidateAll = false;
                        return ValidateAll;
                    }
                }
            }

            if (VerifyTitleFee() == false)
            {
                ValidateAll = false;
                return ValidateAll;
            }

            // Print #100, Time & " - Validate 34"
            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
            {
                if (Conversion.Val(txtTownCredit.Text) == 0 &&
                    chkExciseExempt.CheckState != Wisej.Web.CheckState.Checked &&
                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("ExciseExempt") != true)
                {
                    ans = MessageBox.Show(
                        "You are missing town credit information. Do you wish to proceed with this registration?",
                        "No Credit Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        txtTownCredit.Focus();
                        ValidateAll = false;
                        return ValidateAll;
                    }
                }
            }

            // Print #100, Time & " - Validate 35"
            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
            {
                if (Conversion.Val(txtStateCredit.Text) == 0 &&
                    chkStateExempt.CheckState != Wisej.Web.CheckState.Checked && !MotorVehicle.Statics.ETO)
                {
                    ans = MessageBox.Show(
                        "You are missing state credit information. Do you wish to proceed with this registration?",
                        "No Credit Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        blnFeesFlag = true;
                        frmFeesInfo.InstancePtr.Show(App.MainForm);
                        frmFeesInfo.InstancePtr.txtFEECreditTransfer.Focus();
                        ValidateAll = false;
                        return ValidateAll;
                    }
                }
            }

            // Print #100, Time & " - End Validate"
            return ValidateAll;
        }

        public bool VerifyYearSticker()
        {
            bool VerifyYearSticker = false;
            VerifyYearSticker = true;
            if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 1)
                return VerifyYearSticker;
            if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) == 0)
                return VerifyYearSticker;
            if (Conversion.Val(txtYStickerNumber.Text) == 0 || MotorVehicle.CheckNumbers(this, "YS", 0,
	                FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(txtYStickerNumber.Text)))) ==
                false)
            {
				string stickerTypeDescription;
				if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class))
				{
					stickerTypeDescription = " combination ";
				}
                else if (txtYearCharge.Text.ToIntegerValue() + txtYearNoCharge.Text.ToIntegerValue() == 1)
                {
	                stickerTypeDescription = " single year ";
                }
				else if (txtYearCharge.Text.ToIntegerValue() + txtYearNoCharge.Text.ToIntegerValue() == 2)
                {
	                stickerTypeDescription = " double year ";
				}
                else
                {
	                stickerTypeDescription = "";
				}
				MessageBox.Show(
                    "There are no " + FCConvert.ToString(FCConvert.ToDateTime(txtExpires.Text).Year) +
                    stickerTypeDescription + " stickers with this number in inventory", "Invalid Sticker", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyYearSticker = false;
            }

            return VerifyYearSticker;
        }

        public bool VerifyMileage()
        {
            bool VerifyMileage = false;
            VerifyMileage = true;
            if (MotorVehicle.Statics.ETO || MotorVehicle.Statics.Class == "IU")
                MileageOK = true;
            // 
            // already checked and user said it was ok
            if (MileageOK == true)
                return VerifyMileage;
            // 
            if ((MotorVehicle.Statics.RegistrationType == "RRR") || (MotorVehicle.Statics.RegistrationType == "NRR") ||
                (MotorVehicle.Statics.RegistrationType == "NRT") || (MotorVehicle.Statics.RegistrationType == "RRT") ||
                (MotorVehicle.Statics.RegistrationType == "CORR"))
            {
                if ((MotorVehicle.Statics.Class == "CI") || (MotorVehicle.Statics.Class == "TC") ||
                    (MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "CL") ||
                    (MotorVehicle.Statics.Class == "TL") || (MotorVehicle.Statics.Class == "TR"))
                {
                    txtMileage.Text = "";
                }
                else
                {
                    if (Conversion.Val(txtMileage.Text) == 0)
                    {
                        VerifyMileage = false;
                        return VerifyMileage;
                    }
                    else
                    {
                        // vbPorter upgrade warning: ans1 As object	OnWrite(DialogResult)
                        DialogResult ans1;
                        if (Conversion.Val(txtMileage.Text) < (MileageNumber() + 1))
                        {
                            // if there is no mileagenumber, then they are equal
                            ans1 = MessageBox.Show(
                                "Old mileage = " + FCConvert.ToString(MileageNumber()) +
                                ".  New mileage entered is lower or equal to the new mileage.  Is New mileage correct?",
                                "Mileage Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans1 == DialogResult.No)
                            {
                                VerifyMileage = false;
                                MileageOK = false;
                            }
                            else
                            {
                                MileageOK = true;
                            }
                        }
                    }
                }
            }

            return VerifyMileage;
        }

        public bool VerifyClass()
        {
            bool VerifyClass = false;
            VerifyClass = true;
            if (txtClass.Text == "")
            {
                VerifyClass = false;
                MessageBox.Show("You have entered an Invalid Class Code", "Invalid Class Code", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                txtClass.TabStop = true;
            }

            return VerifyClass;
        }

        public bool VerifyRNumber()
        {
            bool VerifyRNumber = false;
            VerifyRNumber = true;
            if (txtRegistrationNumber.Text == "")
            {
                VerifyRNumber = false;
                txtRegistrationNumber.TabStop = true;
            }

            return VerifyRNumber;
        }

        public bool VerifyRegistrants()
        {
            bool VerifyRegistrants = false;
            int intCountR;
            int intCountL;
            VerifyRegistrants = true;
            if (isAllCompanies())
            {
                VerifyRegistrants = false;
                ShowWarningBox("You may not have 3 owners marked as companies on a registration.",
                    "Invalid Registrants");
            }

            if (hasNoPartyNumber(1))
            {
                VerifyRegistrants = false;
                MessageBox.Show("You must enter a party number for your first registrant before you may continue.",
                    "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtReg1PartyID.Focus();
            }
            
			if (is_not_N(2))
            {
                if (hasNoPartyNumber(2))
                {
                    MessageBox.Show("Check Registrant 2.", "Registrant 2 Required", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    txtReg2PartyID.Focus();
                    VerifyRegistrants = false;
                }
            }

			if (is_not_N(3))
            {
                if (hasNoPartyNumber(3))
                {
                    MessageBox.Show("Check Registrant 3.", "Registrant 3 Required", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    txtReg3PartyID.Focus();
                    VerifyRegistrants = false;
                }
            }

			// kk01072016 tromvs-52  Add a check below for Lessor flag selected but no Lessor info entered
			// and fix the code below to look at txtReg#LR.Text instead of txtReg#ICM.Text
			intCountR = 0;
            if (txtReg1LR.Text == "R")
            {
                intCountR += 1;
                if (txtReg1ICM.Text == "N")
                {
                    ShowWarningBox("Registrant 1 is marked as a lessor, but no lessor information has been entered.",
                        "Check Lessor");
                    VerifyRegistrants = false;
                }
            }

            if (txtReg2LR.Text == "R")
            {
                intCountR += 1;
                if (txtReg2ICM.Text == "N")
                {
                    ShowWarningBox("Registrant 2 is marked as a lessor, but no lessor information has been entered.",
                        "Check Lessor");
                    VerifyRegistrants = false;
                }
            }

            if (txtReg3LR.Text == "R")
            {
                intCountR += 1;
                if (txtReg3ICM.Text == "N")
                {
                    ShowWarningBox("Registrant 3 is marked as a lessor, but no lessor information has been entered.",
                        "Check Lessor");
                    VerifyRegistrants = false;
                }
            }

            if (intCountR > 1)
            {
                MessageBox.Show("Only 1 registrant may be marked as a lessor.", "Check Lessor", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyRegistrants = false;
            }

            // kk01072016 tromvs-52  Add checks for Lessee without Lessor and Lessee without info entered
            intCountL = 0;
            if (txtReg1LR.Text == "E")
            {
                intCountL += 1;
                if (txtReg1ICM.Text == "N")
                {
                    ShowWarningBox("Registrant 1 is marked as a lessee, but no lessee information has been entered.",
                        "Check Lessee");
                    VerifyRegistrants = false;
                }
            }

            if (txtReg2LR.Text == "E")
            {
                intCountL += 1;
                if (txtReg2ICM.Text == "N")
                {
                    ShowWarningBox("Registrant 2 is marked as a lessee, but no lessee information has been entered.",
                        "Check Lessee");
                    VerifyRegistrants = false;
                }
            }

            if (txtReg3LR.Text == "E")
            {
                intCountL += 1;
                if (txtReg1ICM.Text == "N")
                {
                    ShowWarningBox("Registrant 3 is marked as a lessee, but no lessee information has been entered.",
                        "Check Lessee");
                    VerifyRegistrants = false;
                }
            }

            if (intCountR > 0 && intCountL == 0)
            {
                ShowWarningBox(
                    "A registrant is marked as a lessor, but no lessee is currently flagged. Please mark at least one registrant as a lessee.",
                    "Check Lessee");
                VerifyRegistrants = false;
            }

            if (intCountR == 0 && intCountL > 0)
            {
                ShowWarningBox(
                    "A registrant is marked as a lessee, but no lessor is currently flagged. Please mark a registrant as a lessor.",
                    "Check Lessor");
                VerifyRegistrants = false;
            }

            return VerifyRegistrants;
        }

        private bool is_not_N(int registrantPosition)
        {
            string regICM = "";

            ValidateRegistrationPositionParameter(registrantPosition);
            switch (registrantPosition)
            {
                case 1:
                    regICM = txtReg1ICM.Text;
                    break;
                case 2:
                    regICM = txtReg2ICM.Text;
                    break;
                case 3:
                    regICM = txtReg3ICM.Text;
                    break;
            }
            return (regICM != "N");
        }

        private bool hasNoPartyNumber(int registrantPosition)
        {
            ValidateRegistrationPositionParameter(registrantPosition);

            FCTextBox partyTextBox = null;

            switch (registrantPosition)
            {
                case 1:
                    partyTextBox = txtReg1PartyID;
                    break;
                case 2:
                    partyTextBox = txtReg2PartyID;
                    break;
                case 3:
                    partyTextBox = txtReg3PartyID;
                    break;
            }
            return partyTextBox.Text.Trim().ToIntegerValue() == 0;
        }

        private static void ValidateRegistrationPositionParameter(int registrantPosition)
        {
            if (registrantPosition < 1 || registrantPosition > 3) throw new ArgumentException();
        }

        private bool isAllCompanies()
        {
            return txtReg1ICM.Text == "C" && txtReg2ICM.Text == "C" && txtReg3ICM.Text == "C";
        }

        public void FillRSFinal()
        {
            if (MotorVehicle.Statics.RegistrationType != "CORR")
            {
                if (MotorVehicle.Statics.NewFleet)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", true);
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", false);
                }
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("Class", txtClass.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("plate", txtRegistrationNumber.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNumber",
                FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtYStickerNumber.Text))));
            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNumber",
                FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtMStickerNumber.Text))));
            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerMonth",
                FCConvert.ToString(Conversion.Val(lblNumberofMonth.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerYear",
                FCConvert.ToString(Conversion.Val(lblNumberofYear.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("AmputeeVet",
                chkAmputeeVet.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsFinal.Set_Fields("FleetNumber",
                FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("CC300", chkCC.CheckState == Wisej.Web.CheckState.Checked);
            if (chkEAP.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("EAP", true);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("EAP", false);
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("TrailerVanity",
                chkTrailerVanity.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsFinal.Set_Fields("ExciseHalfRate",
                chkHalfRateLocal.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsFinal.Set_Fields("RegHalf",
                chkHalfRateState.CheckState == Wisej.Web.CheckState.Checked);
            // cg12282017 tromv-1269 state reversed decision - NOT ok to prorate and transfer AP
            // kk04172017 tromv-1269 Per Sue at BMV Audit - OK to prorate and transfer AP
            // kk03242016 tromv-1130 Don't prorate transfer of fleet vehicle
            if ((MotorVehicle.Statics.NewFleet && fleetGroupType != FleetGroupType.Group &&
                 MotorVehicle.Statics.RegistrationType != "NRT" && MotorVehicle.Statics.RegistrationType != "RRT") ||
                MotorVehicle.IsProratedMotorcycleExpirationDate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                    MotorVehicle.Statics.datMotorCycleEffective, MotorVehicle.Statics.datMotorCycleExpires,
                    MotorVehicle.Statics.RegistrationType))
            {
                // If (NewFleet And optFleet(cnGroup).Value <> True) Or IsProratedMotorcycleExpirationDate(rsFinal.Fields("Class"), datMotorCycleEffective, datMotorCycleExpires, RegistrationType) Then
                if (DateDifference() < 12)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseProrate", true);
                    MotorVehicle.Statics.rsFinal.Set_Fields("RegProrate", true);
                    if (MotorVehicle.Statics.RegistrationType != "CORR")
                    {
	                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TransactionType")) == "NRR")
	                    {
		                    MotorVehicle.Statics.rsFinal.Set_Fields("ProrateMonth",
			                    MotorVehicle.Statics.datMotorCycleEffective.Month);
	                    }
	                    else
	                    {
		                    MotorVehicle.Statics.rsFinal.Set_Fields("ProrateMonth",
			                    MotorVehicle.Statics.datMotorCycleExpires.Month);
	                    }
                    }
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseProrate", false);
                    MotorVehicle.Statics.rsFinal.Set_Fields("RegProrate", false);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ProrateMonth", 0);
                }
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseProrate", false);
                MotorVehicle.Statics.rsFinal.Set_Fields("RegProrate", false);
                MotorVehicle.Statics.rsFinal.Set_Fields("ProrateMonth", 0);
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("ExciseExempt",
                chkExciseExempt.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsFinal.Set_Fields("TransferExempt",
                chkTransferExempt.CheckState == Wisej.Web.CheckState.Checked);
            if (cboBattle.Text != "")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Battle", Strings.Left(cboBattle.Text, 2));
                // kk01042018 tromvs-96 Change to use BMV 2 digit code
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Battle", "X");
            }

            if (cmbYes.Text == "Yes")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Mail", true);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Mail", false);
            }

            if (chkRental.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("RENTAL", true);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("RENTAL", false);
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", false);
            MotorVehicle.Statics.rsFinal.Set_Fields("EMailAddress", fecherFoundation.Strings.Trim(txtEMail.Text));
            if ((cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) != "") ||
                (cboMessageCodes.SelectedIndex == -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == ""))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("MessageFlag", cboMessageCodes.SelectedIndex + 1);
                MotorVehicle.Statics.rsFinal.Set_Fields("UserMessage", fecherFoundation.Strings.Trim(txtMessage.Text));
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("UserField1", fecherFoundation.Strings.Trim(txtUserField1.Text));
            MotorVehicle.Statics.rsFinal.Set_Fields("UserReason1", fecherFoundation.Strings.Trim(txtUserReason1.Text));
            MotorVehicle.Statics.rsFinal.Set_Fields("UserField2", fecherFoundation.Strings.Trim(txtUserField2.Text));
            MotorVehicle.Statics.rsFinal.Set_Fields("UserReason2", fecherFoundation.Strings.Trim(txtUserReason2.Text));
            MotorVehicle.Statics.rsFinal.Set_Fields("UserField3", fecherFoundation.Strings.Trim(txtUserField3.Text));
            MotorVehicle.Statics.rsFinal.Set_Fields("UserReason3", fecherFoundation.Strings.Trim(txtUserReason3.Text));
            if (!MotorVehicle.Statics.ETO)
            {
	            if (!String.IsNullOrWhiteSpace(txtDoubleCTANumber.Text))
	            {
		            MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber",
			            fecherFoundation.Strings.Trim(txtDoubleCTANumber.Text).ToUpper());
	            }
	            else
	            {
					MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber", "");
				}
            }
            else
            {
	            MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber", "");
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("AgentExempt",
                chkAgentFeeExempt.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsFinal.Set_Fields("RegExempt",
                chkStateExempt.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsFinal.Set_Fields("TwoYear", chk2Year.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsFinal.Set_Fields("ETO", chkETO.CheckState == Wisej.Web.CheckState.Checked);
            if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Status", "P");
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Status", "A");
            }

            if (Conversion.Val(fecherFoundation.Strings.Trim(txtCreditNumber.Text)) != 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3",
                    fecherFoundation.Strings.Trim(txtCreditNumber.Text));
                if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3")) == 0)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3",
                        fecherFoundation.Strings.Trim(txtCreditNumber.Text));
                    MotorVehicle.Statics.rsFinal.Set_Fields("PriorTaxReceipt",
	                    fecherFoundation.Strings.Trim(txtCreditNumber.Text));
				}
				else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3",
                        FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3"))));
                    MotorVehicle.Statics.rsFinal.Set_Fields("PriorTaxReceipt",
	                    FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3"))));
                }
			}
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3",
                    FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3"))));
				MotorVehicle.Statics.rsFinal.Set_Fields("PriorTaxReceipt", 
					FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3"))));
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", 0);
            }

            if (!MotorVehicle.Statics.blnSuspended)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Address", txtAddress1.Text);
            }
            else if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Address", "");
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("Address2", txtAddress2.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("City", txtCity.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("State", txtState.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("Zip", txtZip.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("Country", txtCountry.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("Odometer", FCConvert.ToString(Conversion.Val(txtMileage.Text)));
            if (txtBase.Text != "")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("BasePrice",
                    FCConvert.ToInt32(FCConvert.ToDouble(txtBase.Text)));
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("BasePrice", 0);
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("MillYear", FCConvert.ToString(Conversion.Val(txtMilYear.Text)));
            if (!MotorVehicle.Statics.ETO)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("CTANumber", fecherFoundation.Strings.Trim(txtCTANumber.Text).ToUpper());
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("CTANumber", "");
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("PriorTitleNumber", txtPreviousTitle.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("PriorTitleState", txtPriorTitleState.Text);
            if (MotorVehicle.Statics.RegisteringFleet &&
                MotorVehicle.Statics.PreEffectiveDate.ToOADate() > DateTime.Today.ToOADate())
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxDate",
                    Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yy"));
            }
            else
            {
                if (Information.IsDate(txtExciseTaxDate.Text) == true)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxDate", txtExciseTaxDate.Text);
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxDate", Strings.Format(DateTime.Now, "MM/dd/yy"));
                }
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("AgentFee", FCConvert.ToString(Conversion.Val(txtAgentFee.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNoCharge",
                FCConvert.ToString(Conversion.Val(txtYearNoCharge.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNoCharge",
                FCConvert.ToString(Conversion.Val(txtMonthNoCharge.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerCharge",
                FCConvert.ToString(Conversion.Val(txtYearCharge.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerCharge",
                FCConvert.ToString(Conversion.Val(txtMonthCharge.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode1", txtReg1ICM.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", txtReg2ICM.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode3", txtReg3ICM.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("LeaseCode1", txtReg1LR.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("LeaseCode2", txtReg2LR.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("LeaseCode3", txtReg3LR.Text);
            if (Information.IsDate(txtReg2DOB.Text))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DOB2", txtReg2DOB.Text);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DOB2", null);
            }

            if (Information.IsDate(txtReg3DOB.Text))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DOB3", txtReg3DOB.Text);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DOB3", null);
            }

            if (MotorVehicle.Statics.RegistrationType == "RRR")
            {
                if (fecherFoundation.Strings.Trim(txtTaxIDNumber.Text).Length == 8)
                {
                    if (fecherFoundation.Strings.Trim(txtTaxIDNumber.Text) == fecherFoundation.Strings.Trim(
                            FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("TaxIDNumber"))))
                    {
                        txtTaxIDNumber.Text = "0" + fecherFoundation.Strings.Trim(txtTaxIDNumber.Text);
                    }
                }
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("TaxIDNumber", fecherFoundation.Strings.Trim(txtTaxIDNumber.Text));
            if (Information.IsDate(txtReg1DOB.Text))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DOB1", txtReg1DOB.Text);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DOB1", null);
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("PartyID1",
                FCConvert.ToString(Conversion.Val(txtReg1PartyID.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("PartyName1", txtRegistrant1.Text);
            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1") == "I")
            {
	            MotorVehicle.Statics.rsFinal.Set_Fields("Owner1", "");
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1LastName", strReg1LastName);
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1FirstName", strReg1FirstName);
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1MI", strReg1MI);
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1Designation", strReg1Designation);
            }
            else
            {
	            MotorVehicle.Statics.rsFinal.Set_Fields("Owner1", txtRegistrant1.Text);
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1LastName", "");
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1FirstName", "");
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1MI", "");
	            MotorVehicle.Statics.rsFinal.Set_Fields("Reg1Designation", "");
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2",
                FCConvert.ToString(Conversion.Val(txtReg2PartyID.Text)));
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2") > 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("PartyName2", txtRegistrant2.Text);
                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2") == "I")
                {
	                MotorVehicle.Statics.rsFinal.Set_Fields("Owner2", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2LastName", strReg2LastName);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2FirstName", strReg2FirstName);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2MI", strReg2MI);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2Designation", strReg2Designation);
                }
                else
                {
	                MotorVehicle.Statics.rsFinal.Set_Fields("Owner2", txtRegistrant2.Text);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2LastName", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2FirstName", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2MI", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg2Designation", "");
                }
            }

			MotorVehicle.Statics.rsFinal.Set_Fields("PartyID3",
                FCConvert.ToString(Conversion.Val(txtReg3PartyID.Text)));
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID3") > 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("PartyName3", txtRegistrant3.Text);
                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3") == "I")
                {
	                MotorVehicle.Statics.rsFinal.Set_Fields("Owner3", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3LastName", strReg3LastName);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3FirstName", strReg3FirstName);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3MI", strReg3MI);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3Designation", strReg3Designation);
                }
                else
                {
	                MotorVehicle.Statics.rsFinal.Set_Fields("Owner3", txtRegistrant3.Text);
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3LastName", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3FirstName", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3MI", "");
	                MotorVehicle.Statics.rsFinal.Set_Fields("Reg3Designation", "");
                }
            }

			MotorVehicle.Statics.rsFinal.Set_Fields("InsuranceInitials", txtUserID.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("OpID", txtUserID.Text);
            // 
            MotorVehicle.Statics.rsFinal.Set_Fields("PriorTitleNumber", txtPreviousTitle.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", txtresidenceCode.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", txtLegalres.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", txtResState.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceAddress", txtLegalResStreet.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCountry", txtResCountry.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber", txtUnitNumber.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("DOTNumber", txtDOTNumber.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("Fuel", txtFuel.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("Axles", FCConvert.ToString(Conversion.Val(txtAxles.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("Tires", FCConvert.ToString(Conversion.Val(txtTires.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("Style", txtStyle.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("color1", txtColor1.Text.Trim());
            MotorVehicle.Statics.rsFinal.Set_Fields("color2", txtColor2.Text.Trim());
            MotorVehicle.Statics.rsFinal.Set_Fields("model", txtModel.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("make", txtMake.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("Year", FCConvert.ToString(Conversion.Val(txtYear.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("Vin", txtVin.Text);
            // Dave 5/4/04
            // If RegistrationType = "GVW" Then
            if (fecherFoundation.FCUtils.IsNull(
                MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("RegisteredWeightNew")))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightOld", 0);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightOld",
                    FCConvert.ToString(
                        Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("RegisteredWeightNew"))));
            }

            // End If
            MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew",
                FCConvert.ToString(Conversion.Val(txtRegWeight.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(txtNetWeight.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditUsed", FCConvert.ToDecimal(txtTownCredit.Text));
            if (MotorVehicle.Statics.LocalHalfRate)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditFull",
                    FCConvert.ToDecimal(txtTownCredit.Text) * 2);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditFull", FCConvert.ToDecimal(txtTownCredit.Text));
            }

            if (chkEAP.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", 0);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", FCConvert.ToDecimal(txtExciseTax.Text));
            }

            if (MotorVehicle.Statics.LocalHalfRate)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtExciseTax.Text) * 2);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtExciseTax.Text));
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTransferCharge",
                FCConvert.ToString(Conversion.Val(txtTransferCharge.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", FCConvert.ToString(Conversion.Val(txtTitle.Text)));
            MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", FCConvert.ToDecimal(txtSalesTax.Text));
            if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 1)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge",
                    FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEERegFee.Text));
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtRate.Text));
            }

            if (chkHalfRateState.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.StateHalfRate = true;
                MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull",
                    MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") * 2);
            }
            else
            {
                MotorVehicle.Statics.StateHalfRate = false;
                MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull",
                    FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")));
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed",
                FCConvert.ToDecimal(txtStateCredit.Text) -
                FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditComm.Text));
            if (MotorVehicle.Statics.StateHalfRate)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditFull",
                    FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) * 2);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditFull",
                    FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")));
            }

            if (Information.IsDate(txtExpires.Text))
            {
                // And (optFleet(cnGroup).Value <> True Or Not ETO)
                MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", txtExpires.Text);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", null);
            }

            if (txtEffectiveDate.Text != "")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate", txtEffectiveDate.Text);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate", null);
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("CommercialCredit", frmFeesInfo.InstancePtr.txtFEECreditComm.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("InitialFee", frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("outofrotation", frmFeesInfo.InstancePtr.txtFEEOutOfRotation.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlate", frmFeesInfo.InstancePtr.txtFEEReservePlate.Text);
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate") > 0)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlateYN", -1);
            }

            if (Conversion.Val(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text) != 0)
            {
                if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    if (MotorVehicle.Statics.PlateType == 2)
                    {
                        if (intCorrSpecialtyPlate == DialogResult.Yes)
                        {
                            // rsFinal.Fields("PlateType") = "N"  Or rsFinal.Fields("PlateType") = "R"
                            MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
                            MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew",
                                FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
                            if (MotorVehicle.Statics.intCorrExtendedSpecialtyPlate == DialogResult.Yes)
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg",
                                    FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                            }
                            else
                            {
                                MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
                            }
                        }
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.NewPlate == true ||
                        (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 2))
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew",
                            FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg",
                            FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                    }
                }
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
                MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee",
                frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", frmFeesInfo.InstancePtr.txtFEEStickerFee.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", frmFeesInfo.InstancePtr.txtFEETransferFee.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("GIFTCERTIFICATEAMOUNT",
                frmFeesInfo.InstancePtr.txtFEEGiftCertificate.Text);
            MotorVehicle.Statics.rsFinal.Set_Fields("GiftCertificateNumber", frmFeesInfo.InstancePtr.txtGCNumber.Text);
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT") != 0)
            {
                if (frmFeesInfo.InstancePtr.cmbVoucher.Text == "Gift Certificate")
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("GiftCertOrVoucher", "G");
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("GiftCertOrVoucher", "V");
                }
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("GiftCertOrVoucher", "");
            }

            if (fecherFoundation.FCUtils.IsNull(
                MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee")))
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
            }

            // DJW 6/29/2009 Took this if statement out because if we don't calculate state paid the way we originally did when the function that compares ecorrect records fires we get a negative amount for sales tax and title fee
            if (MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "GVW")
            {
                if (MotorVehicle.Statics.PlateType == 1 && MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    //FC:FINAL:MSH - i.issue #1820: operator "+" can't be applied for double and decimal types
                    //MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", FCConvert.ToDecimal(txtFees.Text) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"))) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"))) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("DuplicateRegistrationFee"))));
                    MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid",
                        Conversion.Val(txtFees.Text) +
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax")) +
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TitleFee")) -
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee")));
                }
                else if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", FCConvert.ToDecimal(txtFees.Text));
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid",
                        MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") -
                        MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"));
                }
            }
            else
            {
                //FC:FINAL:MSH - i.issue #1820: operator "+" can't be applied for double and decimal types
                //MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", FCConvert.ToDecimal(txtFees.Text) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"))) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"))) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("DuplicateRegistrationFee"))));
                MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid",
                    Conversion.Val(txtFees.Text) +
                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax")) +
                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TitleFee")) -
                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee")));
            }

            MotorVehicle.Statics.rsFinal.Set_Fields("LocalPaid",
                FCConvert.ToDecimal(txtBalance.Text) + FCConvert.ToDecimal(txtAgentFee.Text));
            MotorVehicle.Statics.rsFinal.Set_Fields("ForcedPlate", MotorVehicle.Statics.ForcedPlate);
            // kk05122016
            MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeCTA",
                FCConvert.CBool(chkNoFeeCTA.CheckState == Wisej.Web.CheckState.Checked));
            MotorVehicle.Statics.rsFinal.Set_Fields("CTAPaid",
                FCConvert.CBool(chkCTAPaid.CheckState == Wisej.Web.CheckState.Checked));
            MotorVehicle.Statics.rsFinal.Set_Fields("DealerSalesTax",
                FCConvert.CBool(chkDealerSalesTax.CheckState == Wisej.Web.CheckState.Checked));
            MotorVehicle.Statics.rsFinal.Set_Fields("BaseIsSalePrice",
                FCConvert.CBool(chkBaseIsSalePrice.CheckState == Wisej.Web.CheckState.Checked));
            MotorVehicle.Statics.rsFinal.Set_Fields("SalesTaxExempt",
                FCConvert.CBool(chkSalesTaxExempt.CheckState == Wisej.Web.CheckState.Checked));
            // kk07222016
        }

        public bool CheckNumberOfStickers(int intBox1, int intBox2)
        {
            bool CheckNumberOfStickers = false;
            using (clsDRWrapper rr = new clsDRWrapper())
            {
                rr.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class +
                                 "' And SystemCode = '" + MotorVehicle.Statics.Subclass + "'");
                CheckNumberOfStickers = true;
                if (rr.EndOfFile() != true && rr.BeginningOfFile() != true)
                {
                    rr.MoveLast();
                    rr.MoveFirst();
                    if (intBox1 + intBox2 > rr.Get_Fields_Int32("NumberOfPlateStickers"))
                    {
                        CheckNumberOfStickers = false;
                    }
                }
            }
            
            return CheckNumberOfStickers;
        }

        public bool VerifyBase()
        {
            bool VerifyBase = false;
            VerifyBase = true;
            // 
            if ((MotorVehicle.Statics.RegistrationType == "RRR") || (MotorVehicle.Statics.RegistrationType == "RRT") ||
                (MotorVehicle.Statics.RegistrationType == "NRR") || (MotorVehicle.Statics.RegistrationType == "NRT"))
            {
                MotorVehicle.Statics.strSql = "SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class +
                                              "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'";

                using (clsDRWrapper rsTaxInfo = new clsDRWrapper())
                {
                    rsTaxInfo.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
                    {
                        rsTaxInfo.MoveLast();
                        rsTaxInfo.MoveFirst();
                        if (FCConvert.ToString(rsTaxInfo.Get_Fields_String("ExciseRequired")) == "Y")
                        {
                            if (Conversion.Val(txtBase.Text) == 0)
                            {
                                MessageBox.Show("Base cannot be zero for this type of registration.", "Invalid Base Value",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                VerifyBase = false;
                                txtBase.TabStop = true;
                            }
                        }
                    }
                }
            }

            return VerifyBase;
        }

        public void OpenVehicleTabs()
        {
            txtColor2.TabStop = true;
            txtMileage.TabStop = true;
            // True
            txtBase.TabStop = true;
            txtMilYear.TabStop = true;
            txtReg2ICM.TabStop = true;
            if (fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")) == true)
            {
                txtExpires.TabStop = true;
            }
            else
            {
                txtExpires.TabStop = false;
            }

            txtEffectiveDate.TabStop = true;
            txtFuel.TabStop = true;
            if ((MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "AP"))
            {
                // kk04152016 tromv-1145   Add AP, Not needed for TL or CL    'Case "SE", "TL", "CL"
                txtNetWeight.TabStop = true;
            }
            else
            {
                txtNetWeight.TabStop = false;
            }

            if ((MotorVehicle.Statics.Class == "CO") || (MotorVehicle.Statics.Class == "CC") ||
                (MotorVehicle.Statics.Class == "FM") || (MotorVehicle.Statics.Class == "TR") ||
                (MotorVehicle.Statics.Class == "MH") || (MotorVehicle.Statics.Class == "RV") ||
                (MotorVehicle.Statics.Class == "WX") || (MotorVehicle.Statics.Class == "AP") ||
                (MotorVehicle.Statics.Class == "CL") || (MotorVehicle.Statics.Class == "TL") ||
                (MotorVehicle.Statics.Class == "AC") || (MotorVehicle.Statics.Class == "AF") ||
                (MotorVehicle.Statics.Class == "TT") || (MotorVehicle.Statics.Class == "LC"))
            {
                txtRegWeight.TabStop = true;
            }
            else if ((MotorVehicle.Statics.Class == "DS") || (MotorVehicle.Statics.Class == "VT"))
            {
                if (MotorVehicle.Statics.Subclass.Right(1) == "3")
                {
                    txtRegWeight.TabStop = true;
                }
                else
                {
                    txtRegWeight.TabStop = false;
                }
            }
            else if (MotorVehicle.Statics.Class == "DV" || MotorVehicle.Statics.Class == "VX")
            {
                if (MotorVehicle.Statics.Subclass.Right(1) == "5" || MotorVehicle.Statics.Subclass.Right(1) == "6")
                {
                    txtRegWeight.TabStop = true;
                }
                else
                {
                    txtRegWeight.TabStop = false;
                }
            }
            else if ((MotorVehicle.Statics.Class == "AW") || (MotorVehicle.Statics.Class == "BB") ||
                     (MotorVehicle.Statics.Class == "BC") || (MotorVehicle.Statics.Class == "BH") ||
                     (MotorVehicle.Statics.Class == "LB") || (MotorVehicle.Statics.Class == "SW") ||
                     (MotorVehicle.Statics.Class == "TS") || (MotorVehicle.Statics.Class == "UM"))
            {
                if (MotorVehicle.Statics.Subclass.Right(1) == "3")
                {
                    txtRegWeight.TabStop = true;
                }
                else
                {
                    txtRegWeight.TabStop = false;
                }
            }
            else
            {
                txtRegWeight.TabStop = false;
            }

            txtAxles.TabStop = true;
            txtTires.TabStop = true;
            txtStyle.TabStop = true;
            txtColor1.TabStop = true;
            txtModel.TabStop = true;
            txtMake.TabStop = true;
            txtYear.TabStop = true;
            txtVin.TabStop = true;
        }

        public void OpenDemographicTabs()
        {
            txtReg1PartyID.TabStop = true;
            txtReg1DOB.TabStop = true;
            txtAddress1.TabStop = true;
            txtAddress2.TabStop = true;
            txtCity.TabStop = true;
            txtState.TabStop = true;
            txtZip.TabStop = true;
            txtCountry.TabStop = true;
            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) != "N")
            {
                txtReg2PartyID.TabStop = true;
                txtReg2DOB.TabStop = true;
            }

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) != "N")
            {
                txtReg3PartyID.TabStop = true;
                txtReg3DOB.TabStop = true;
            }

            txtResState.TabStop = true;
            txtResCountry.TabStop = true;
            txtresidenceCode.TabStop = true;
            txtLegalres.TabStop = true;
            txtLegalResStreet.TabStop = true;
        }

        public bool VerifyCity()
			{
            bool VerifyCity = false;
            VerifyCity = true;
            if (txtCity.Text.Trim() == "")
            {
                MessageBox.Show("Please be sure there is a City/Town.", "Check Address", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyCity = false;
                txtCity.TabStop = true;
            }

            return VerifyCity;
        }

        public bool VerifyState()
        {
            bool VerifyState = false;
            VerifyState = true;
            if (txtState.Text.Trim() == "")
            {
                MessageBox.Show("Please be sure there is a State.", "Check Address", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyState = false;
                txtState.TabStop = true;
            }

            using (clsDRWrapper rsStates = new clsDRWrapper())
            {
                rsStates.OpenRecordset(
                    "SELECT * FROM State WHERE Abrev = '" + txtState.Text.Trim() + "'", "MotorVehicle");
                if (rsStates.EndOfFile() != true && rsStates.BeginningOfFile() != true)
                {
                    // do nothing
                }
                else
                {
                    MessageBox.Show("The state you have enetered is not a valid state code.", "Check Address", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    VerifyState = false;
                    txtState.TabStop = true;
                }

                return VerifyState;
            }
        }

        public bool VerifyCountry()
        {
            bool VerifyCountry = true;

            bool badCountry = (txtCountry.Text != "" && txtCountry.Text != "US");
            bool badResCountry = (txtResCountry.Text != "" && txtResCountry.Text != "US");

			if (badCountry || badResCountry)
            {
	            if (MessageBox.Show("A country code other than US has been entered. Is this correct?",
		                "Check Country", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
	            {
		            VerifyCountry = false;
		            txtResCountry.TabStop = badCountry;
		            txtCountry.TabStop = badResCountry;
		            if (badCountry)
		            {
			            txtCountry.Focus();
		            }
					else
		            {
			            txtResCountry.Focus();
		            }
	            }
            }
            return VerifyCountry;
        }

        public bool VerifyZip()
        {
            bool VerifyZip = false;
            VerifyZip = true;
            if (txtZip.Text.Trim() == "")
            {
                MessageBox.Show("Please be sure there is a Zip Code.", "Check Address", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyZip = false;
                txtZip.TabStop = true;
            }

            return VerifyZip;
        }

        public bool VerifyDefaultRescode()
        {
            bool VerifyDefaultRescode = false;
            VerifyDefaultRescode = true;
            // vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
            DialogResult answer;
            clsDRWrapper rsDefaultRC = new clsDRWrapper();
            clsDRWrapper rsRegions = new clsDRWrapper();

            try
            {
                if (!modRegionalTown.IsRegionalTown())
                {
                    MotorVehicle.Statics.strSql = "SELECT * FROM DefaultInfo";
                    rsDefaultRC.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsDefaultRC.BeginningOfFile() != true && rsDefaultRC.EndOfFile() != true)
                    {
                        rsDefaultRC.MoveLast();
                        rsDefaultRC.MoveFirst();
                        if (FCConvert.ToString(rsDefaultRC.Get_Fields_String("ResidenceCode")) !=
                            fecherFoundation.Strings.Trim(txtresidenceCode.Text))
                        {
                            answer = MessageBox.Show(
                                "The residence code does not match the default residence code from the table. Is this OK?",
                                "Residence Code OK", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (answer == DialogResult.No)
                            {
                                txtresidenceCode.Focus();
                                VerifyDefaultRescode = false;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("There is no residence code in your default table.", "No Residence Code",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    rsDefaultRC.Reset();
                }
                else
                {
                    rsRegions.OpenRecordset(
                        "SELECT * FROM tblRegions WHERE ResCode = '" +
                        fecherFoundation.Strings.Trim(txtresidenceCode.Text) + "'", "CentralData");
                    if (rsRegions.EndOfFile() != true && rsRegions.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        answer = MessageBox.Show(
                            "The residence code does not match the default residence code from the table. Is this OK?",
                            "Residence Code OK", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (answer == DialogResult.No)
                        {
                            txtresidenceCode.Focus();
                            VerifyDefaultRescode = false;
                        }
                    }
                }

                return VerifyDefaultRescode;
            }
            finally
            {
                rsDefaultRC.Dispose();
                rsRegions.Dispose();
            }
        }

        public bool VerifyLegalResidence()
        {
            bool VerifyLegalResidence = true;
            if ((fecherFoundation.Strings.Trim(txtLegalResStreet.Text) == "") && (MotorVehicle.Statics.Class != "AP"))
            {
                if (MessageBox.Show("You have not entered a Legal Residence Street Address.  Do you wish to do so now?",
                        "Check Legal Residence Address", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                    DialogResult.Yes)
                {
                    VerifyLegalResidence = false;
                    txtLegalResStreet.TabStop = true;
                    txtLegalResStreet.Focus();
                }
            }

            return VerifyLegalResidence;
        }

        public void SetMonthNumbers()
        {
            if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) < 1)
            {
                lblMSD.Text = "";
            }
            else if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) == 1)
            {
                lblMSD.Text = "S";
            }
            else if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) == 2)
            {
                lblMSD.Text = "D";
            }
        }

        public void SetYearNumbers()
        {
            if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) < 1)
            {
                lblYSD.Text = "";
            }
            else if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) == 1)
            {
                lblYSD.Text = "S";
            }
            else if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) == 2)
            {
                lblYSD.Text = "D";
            }

            if (lblYSD.Text != "")
            {
                if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) &&
                    MotorVehicle.Statics.FixedMonth)
                {
                    lblYSD.Text = "C";
                }
            }
        }

        public void SetTabStopsToTrue()
        {
            txtResCountry.TabStop = true;
            txtCountry.TabStop = true;
            txtAddress2.TabStop = true;
            txtLegalResStreet.TabStop = true;
            txtReg3ICM.TabStop = true;
            txtReg3DOB.TabStop = true;
            txtReg1LR.TabStop = true;
            txtReg2LR.TabStop = true;
            txtReg3LR.TabStop = true;
            txtResState.TabStop = true;
            txtColor2.TabStop = true;
            txtFleetNumber.TabStop = true;
            txtZip.TabStop = true;
            txtState.TabStop = true;
            txtCity.TabStop = true;
            txtAddress1.TabStop = true;
            txtReg1PartyID.TabStop = true;
            txtReg2PartyID.TabStop = true;
            txtReg3PartyID.TabStop = true;
            txtMileage.TabStop = true;
            txtBase.TabStop = true;
            txtMilYear.TabStop = true;
            txtCTANumber.TabStop = true;
            txtExciseTaxDate.TabStop = true;
            txtAgentFee.TabStop = true;
            txtYearNoCharge.TabStop = true;
            txtMonthNoCharge.TabStop = true;
            txtYearCharge.TabStop = true;
            txtMonthCharge.TabStop = true;
            txtReg1ICM.TabStop = true;
            txtReg2ICM.TabStop = true;
            txtExpires.TabStop = true;
            txtReg2DOB.TabStop = true;
            txtEffectiveDate.TabStop = true;
            txtPreviousTitle.TabStop = true;
            txtYStickerNumber.Enabled = true;
            txtMStickerNumber.Enabled = true;
            txtMilRate.TabStop = true;
            txtUserID.TabStop = true;
            txtresidenceCode.TabStop = true;
            txtLegalres.TabStop = true;
            txtUnitNumber.TabStop = true;
            txtDOTNumber.TabStop = true;
            txtReg1DOB.TabStop = true;
            txtFuel.TabStop = true;
            txtRegWeight.TabStop = true;
            txtNetWeight.TabStop = true;
            txtAxles.TabStop = true;
            txtTires.TabStop = true;
            txtStyle.TabStop = true;
            txtColor1.TabStop = true;
            txtModel.TabStop = true;
            txtMake.TabStop = true;
            txtYear.TabStop = true;
            txtVin.TabStop = true;
            txtRegistrationNumber.TabStop = true;
            txtClass.TabStop = true;
            txtTownCredit.TabStop = true;
            txtBalance.TabStop = true;
            txtExciseTax.TabStop = true;
            txtSubTotal.TabStop = true;
            txtTransferCharge.TabStop = true;
            txtTitle.TabStop = true;
            txtSalesTax.TabStop = true;
            txtRate.TabStop = true;
            txtFees.TabStop = true;
            txtStateCredit.TabStop = true;
            txtCreditNumber.TabStop = true;
            chkETO.TabStop = true;
            chk2Year.TabStop = true;
            chkHalfRateLocal.TabStop = true;
            chkHalfRateState.TabStop = true;
            chkStateExempt.TabStop = true;
            chkAgentFeeExempt.TabStop = true;
            chkExciseExempt.TabStop = true;
            chkAmputeeVet.TabStop = true;
            chkTrailerVanity.TabStop = true;
            chkCC.TabStop = true;
            // kk05182016
            chkNoFeeCTA.TabStop = true;
            chkDealerSalesTax.TabStop = true;
            chkBaseIsSalePrice.TabStop = true;
            chkSalesTaxExempt.TabStop = true;
        }

        public void DisableBottomOfScreen(bool blnLeave2Year = false)
        {
            if (blnLeave2Year != true)
            {
                chk2Year.Enabled = false;
            }

            if (MotorVehicle.Statics.ETO)
            {
                chkHalfRateLocal.Enabled = true;
            }
            else
            {
                chkHalfRateLocal.Enabled = false;
            }

            chkHalfRateState.Enabled = false;
            chkStateExempt.Enabled = false;
            chkAgentFeeExempt.Enabled = false;
            if (MotorVehicle.Statics.ETO &&
                !FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseExempt")))
            {
                chkExciseExempt.Enabled = true;
            }
            else
            {
                chkExciseExempt.Enabled = false;
            }

            chkAmputeeVet.Enabled = false;
            chkTrailerVanity.Enabled = false;
            chkSalesTaxExempt.Enabled = false;
            chkDealerSalesTax.Enabled = false;
        }

        public void EnableBottomOfScreen()
        {
            chk2Year.Enabled = true;
            chkHalfRateLocal.Enabled = true;
            chkHalfRateState.Enabled = true;
            chkStateExempt.Enabled = true;
            chkAgentFeeExempt.Enabled = true;
            if (!FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseExempt")) ||
                chkExciseExempt.CheckState != Wisej.Web.CheckState.Checked)
            {
                chkExciseExempt.Enabled = true;
            }

            chkTransferExempt.Enabled = true;
            chkAmputeeVet.Enabled = true;
            chkTrailerVanity.Enabled = true;
            chkSalesTaxExempt.Enabled = true;
            chkDealerSalesTax.Enabled = true;
            // txtTrailerCode.Enabled = True
            // lblTrailerCode.Enabled = True
        }

        private void UnEnableAllButWeight()
        {
            /* object obj; */
            foreach (Control obj in this.GetAllControls())
            {
                if (obj.Name == txtRegWeight.Name)
                {
                    obj.TabIndex = 0;
                    obj.TabStop = true;
                }
                else if (obj.Name == lblCalculate.Name)
                {
                    obj.TabIndex = 1;
                    obj.Enabled = true;
                }
                else if (obj is Global.T2KBackFillDecimal)
                {
                    (obj as Global.T2KBackFillDecimal).Locked = true;
                    obj.TabStop = false;
                }
                else if (obj is Global.T2KBackFillWhole)
                {
                    (obj as Global.T2KBackFillWhole).Locked = true;
                    obj.TabStop = false;
                }
                else if (obj is Global.T2KDateBox)
                {
                    (obj as Global.T2KDateBox).Locked = true;
                    obj.TabStop = false;
                }
                else if (obj is Global.T2KOverTypeBox)
                {
                    (obj as Global.T2KOverTypeBox).Locked = true;
                    obj.TabStop = false;
                }
                else if (obj is Global.T2KPhoneNumberBox)
                {
                    (obj as Global.T2KPhoneNumberBox).Locked = true;
                    obj.TabStop = false;
                }
                else if (obj is FCTextBox)
                {
                    (obj as FCTextBox).LockedOriginal = true;
                    obj.TabStop = false;
                }
                else if (obj is FCRadioButton)
                {
                    obj.TabStop = false;
                }
            }
        }

        public void FillAll()
        {
            txtClass.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
            txtRegistrationNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO") != true)
            {
                txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"), "MM/dd/yyyy");
                txtEffectiveDate.Text =
                    Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate"), "MM/dd/yyyy");
            }
            else
            {
                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate")))
                {
                    txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"),
                        "MM/dd/yyyy");
                }

                if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
                {
                    txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                }
                else
                {
                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
                    {
                        txtEffectiveDate.Text =
                            Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"),
                                "MM/dd/yyyy");
                    }
                }
            }

            if (MotorVehicle.Statics.PlateType == 2)
            {
                if (MotorVehicle.Statics.Class == "CL" || MotorVehicle.Statics.Class == "MC" ||
                    MotorVehicle.Statics.Class == "VM" || MotorVehicle.Statics.Class == "MP" ||
                    MotorVehicle.Statics.Class == "MQ" || MotorVehicle.Statics.Class == "MX" ||
                    MotorVehicle.Statics.Class == "SE" || MotorVehicle.Statics.Class == "TL" ||
                    ((MotorVehicle.Statics.Class == "CO" || MotorVehicle.Statics.Class == "TT" ||
                      MotorVehicle.Statics.Class == "CC") &&
                     (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) >= 800000 &&
                      Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) <= 899999)))
                {
                    txtYearNoCharge.Text = "1";
                    txtMonthNoCharge.Text = "1";
                }
                else if (MotorVehicle.Statics.Class == "IU")
                {
                    txtYearNoCharge.Text = "0";
                    txtMonthNoCharge.Text = "2";
                }
                else
                {
                    txtYearNoCharge.Text = "2";
                    txtMonthNoCharge.Text = "2";
                }

                if (blnChargeForPlate || MotorVehicle.Statics.LostPlate)
                {
                    txtYearCharge.Text = txtYearNoCharge.Text;
                    txtMonthCharge.Text = txtMonthNoCharge.Text;
                    txtMonthNoCharge.Text = "";
                    txtYearNoCharge.Text = "";
                }

                lblNumberofYear.Text = Strings.Mid(txtExpires.Text, 9, 2);
                lblNumberofMonth.Text = Strings.Mid(txtExpires.Text, 1, 2);
                txtYStickerNumber.Enabled = true;
                txtMStickerNumber.Enabled = true;
                if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) && MotorVehicle.Statics.FixedMonth)
                {
                    txtMonthCharge.Text = "";
                    txtMonthNoCharge.Text = "";
                }
            }
            else
            {
                if (MotorVehicle.Statics.RegistrationType != "GVW")
                {
                    txtYearNoCharge.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge"));
                    txtMonthNoCharge.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge"));
                    txtYearCharge.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge"));
                    txtMonthCharge.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge"));
                    txtYStickerNumber.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                    txtMStickerNumber.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
                    lblNumberofMonth.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth"));
                    lblNumberofYear.Text =
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerYear"));
                }
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("AmputeeVet")))
            {
                chkAmputeeVet.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")) != 0)
            {
                using (clsDRWrapper rsFT = new clsDRWrapper())
                {
                    rsFT.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND FleetNumber = " +
                                       FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")));
                    if (rsFT.EndOfFile() != true && rsFT.BeginningOfFile() != true)
                    {
                        if (FCConvert.ToInt32(rsFT.Get_Fields_Int32("ExpiryMonth")) == 99)
                        {
                            SetFleetGroupType(FleetGroupType.Group);
                        }
                        else
                        {
                            SetFleetGroupType(FleetGroupType.Fleet);
                        }

                        txtFleetNumber.Text =
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
                    }
                    else
                    {
                        ClearFleetGroup();
                    }
                }
            }
            else
            {
                ClearFleetGroup();
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("CC300")))
            {
                chkCC.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TrailerVanity")))
            {
                chkTrailerVanity.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")))
            {
                chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")))
            {
                chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseExempt")))
            {
                chkExciseExempt.CheckState = Wisej.Web.CheckState.Checked;
                if (MotorVehicle.Statics.RegistrationType != "CORR")
                {
                    chkExciseExempt.Enabled = false;
                }
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TransferExempt")))
            {
                chkTransferExempt.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("AgentExempt")))
            {
                chkAgentFeeExempt.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegExempt")))
            {
                chkStateExempt.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwoYear")))
            {
                chk2Year.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")))
            {
                chkETO.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeCTA")))
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Checked;
            }
            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("CTAPaid")))
            {
                chkCTAPaid.CheckState = Wisej.Web.CheckState.Checked;
            }
            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("DealerSalesTax")))
            {
                chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Checked;
            }
            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("BaseIsSalePrice")))
            {
                chkBaseIsSalePrice.CheckState = Wisej.Web.CheckState.Checked;
            }
            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("SalesTaxExempt")))
            {
                chkSalesTaxExempt.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (!MotorVehicle.Statics.blnSuspended)
            {
                txtAddress1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Address");
            }
            else
            {
                txtAddress1.Text = "REGISTRATION SUSPENDED";
            }

            txtAddress2.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Address2");
            txtCity.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("City");
            txtState.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("State");
            txtZip.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip");
            txtCountry.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Country").Length <= 2 ?
	            MotorVehicle.Statics.rsFinal.Get_Fields_String("Country") :
	            "";
            txtReg1PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"));
            GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), 1);
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2") != 0)
            {
                txtReg2PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
                GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"), 2);
            }
            else
            {
	            ClearPartyInfo(2);
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID3") != 0)
            {
                txtReg3PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID3"));
                GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID3"), 3);
            }
            else
            {
	            ClearPartyInfo(3);
			}

            txtMileage.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer"));
            txtBase.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"), "#,##0");
            txtMilYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear"));
            if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "CL" &&
                 MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") == "C5") ||
                (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "TL" &&
                 MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") == "L6"))
            {
                if (Conversion.Val(txtMilYear.Text) == 1)
                {
                    txtMilRate.Text = ".0250";
                }
                else if (Conversion.Val(txtMilYear.Text) == 2)
                {
                    txtMilRate.Text = ".0200";
                }
                else if (Conversion.Val(txtMilYear.Text) == 3)
                {
                    txtMilRate.Text = ".0160";
                }
                else if (Conversion.Val(txtMilYear.Text) == 4)
                {
                    txtMilRate.Text = ".0120";
                }
                else if (Conversion.Val(txtMilYear.Text) == 5)
                {
                    txtMilRate.Text = ".0120";
                }
                else if (Conversion.Val(txtMilYear.Text) == 6)
                {
                    txtMilRate.Text = ".0120";
                }
            }
            else
            {
                if (Conversion.Val(txtMilYear.Text) == 1)
                {
                    txtMilRate.Text = ".0240";
                }
                else if (Conversion.Val(txtMilYear.Text) == 2)
                {
                    txtMilRate.Text = ".0175";
                }
                else if (Conversion.Val(txtMilYear.Text) == 3)
                {
                    txtMilRate.Text = ".0135";
                }
                else if (Conversion.Val(txtMilYear.Text) == 4)
                {
                    txtMilRate.Text = ".0100";
                }
                else if (Conversion.Val(txtMilYear.Text) == 5)
                {
                    txtMilRate.Text = ".0065";
                }
                else if (Conversion.Val(txtMilYear.Text) == 6)
                {
                    txtMilRate.Text = ".0040";
                }
            }

            txtCTANumber.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber");
            txtDoubleCTANumber.Enabled = (!String.IsNullOrWhiteSpace(txtCTANumber.Text));
            txtDoubleCTANumber.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber");
            txtExciseTaxDate.Text =
                Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate"), "MM/dd/yy");
            txtAgentFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFee"), "#0.00");
            //if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2")) != "12:00:00 AM" && fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2")) != true)
            if (!MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2").IsEmptyDate())
            {
                txtReg2DOB.Text =
                    Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2"), "MM/dd/yyyy");
            }

            //if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3")) != "12:00:00 AM" && fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3")) != true)
            if (!MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3").IsEmptyDate())
            {
                txtReg3DOB.Text =
                    Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3"), "MM/dd/yyyy");
            }

            txtUserID.Text = MotorVehicle.Statics.UserID;
            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber")) == 0)
            {
                txtPreviousTitle.Text = "";
            }
            else
            {
                txtPreviousTitle.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber");
            }

            MotorVehicle.Statics.OpID = MotorVehicle.Statics.rsFinal.Get_Fields_String("OpID");
            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode")).Length < 5)
            {
                txtresidenceCode.Text =
                    Strings.StrDup(5 - MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode").Length, "0") +
                    MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
            }
            else
            {
                txtresidenceCode.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
            }

            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence")) != "")
            {
                txtLegalres.Text =
                    fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
            }

            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceAddress")) != "")
            {
                txtLegalResStreet.Text =
                    fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceAddress"));
            }

            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCountry")) != "")
            {
                txtResCountry.Text =
                    fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCountry")).Length <= 2 ?
	                    fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCountry")) :
						"";
            }

            txtResState.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState");
            txtUnitNumber.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber");
            txtDOTNumber.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber");
            if (txtDOTNumber.Text == "0")
                txtDOTNumber.Text = "";
            //if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1")) != "12:00:00 AM" && fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1")) != true)
            if (!MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1").IsEmptyDate())
            {
                txtReg1DOB.Text =
                    Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1"), "MM/dd/yyyy");
            }

            txtTaxIDNumber.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber");
            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel") != "N")
            {
                txtFuel.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel");
            }
            else
            {
                txtFuel.Text = "";
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles") != 0)
            {
                txtAxles.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles"));
            }
            else
            {
                txtAxles.Text = "";
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires") != 0)
            {
                txtTires.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires"));
            }
            else
            {
                txtTires.Text = "";
            }

            txtStyle.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Style").Trim();
            txtColor1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("color1").Trim();
            txtColor2.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("color2").Trim();
            string model = MotorVehicle.Statics.rsFinal.Get_Fields_String("model").Trim();
			txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
			txtMake.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make").Trim();
            txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
            txtVin.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
            txtMilYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear"));
            txtRegWeight.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew"));
            txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
            txtTownCredit.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"),
                "#,##0.00");
            txtExciseTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"),
                "#,##0.00");
            txtTransferCharge.Text =
                Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "#,##0.00");
            txtTitle.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TitleFee"), "#,##0.00");
            txtSalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("SalesTax"), "#,##0.00");
            if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2)
            {
                txtRate.Text =
                    Strings.Format(
                        MotorVehicle.GetRegRate(this, MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass),
                        "#,##0.00");
            }
            else
            {
                txtRate.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"),
                    "#,##0.00");
            }

            txtStateCredit.Text =
                Strings.Format(
                    MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed") +
                    MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"),
                    "#,##0.00");
            txtCreditNumber.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3"));
            // DJW@05012014 Dont allow change of credit number on correction unless one has not been entered
            if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 1)
            {
                if (fecherFoundation.Strings.Trim(txtCreditNumber.Text) != "")
                {
                    txtCreditNumber.Enabled = false;
                }
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1") == "")
            {
                txtReg1ICM.Text = "I";
            }
            else
            {
                txtReg1ICM.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2") == "")
            {
                txtReg2ICM.Text = "N";
            }
            else
            {
                txtReg2ICM.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3") == "")
            {
                txtReg3ICM.Text = "N";
            }
            else
            {
                txtReg3ICM.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1") == "")
            {
                txtReg1LR.Text = "N";
            }
            else
            {
                txtReg1LR.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2") == "")
            {
                txtReg2LR.Text = "N";
            }
            else
            {
                txtReg2LR.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2");
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3") == "")
            {
                txtReg3LR.Text = "N";
            }
            else
            {
                txtReg3LR.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3");
            }

            // kk05122016
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeCTA"))
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("DealerSalesTax"))
            {
                chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("BaseIsSalePrice"))
            {
                chkBaseIsSalePrice.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkBaseIsSalePrice.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("SalesTaxExempt"))
            {
                // kk07222016
                chkSalesTaxExempt.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkSalesTaxExempt.CheckState = Wisej.Web.CheckState.Unchecked;
            }
        }

        public bool VerifyPriorTitleState()
        {
            return true;
        }

        public bool CalculateEffectiveDate()
        {
            bool CalculateEffectiveDate = false;
            // vbPorter upgrade warning: datTempDate As DateTime	OnWrite(string, DateTime)
            DateTime datTempDate;
            int intDifference = 0;
            if (MotorVehicle.Statics.ETO && MotorVehicle.Statics.RegistrationType == "NRR" &&
                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "AP")
            {
                CalculateEffectiveDate = true;
                return CalculateEffectiveDate;
            }

            CalculateEffectiveDate = false;
            // vbPorter upgrade warning: Newdate As object	OnWrite(string)
            object Newdate = null;
            if ((MotorVehicle.Statics.RegistrationType == "RRR") || (MotorVehicle.Statics.RegistrationType == "CORR"))
            {
                if (txtExpires.Text == "" || txtExpires.IsEmpty)
                {
                    return CalculateEffectiveDate;
                }
                else
                {
                    Newdate = Strings.Format(txtExpires.Text, "MM/dd/yyyy");
                    if (Information.IsDate(Newdate) == false)
                    {
                        // Bp = Beep(1000, 250)
                    }
                    else
                    {
                        if (VerifyExpireDate(txtExpires.Text) == "X")
                        {
                            txtExpires.Text = "X";
                            CalculateEffectiveDate = false;
                            return CalculateEffectiveDate;
                        }
                        else if (VerifyExpireDate(txtExpires.Text) == "F")
                        {
                            txtExpires.Text = "00/00/0000";
                            return CalculateEffectiveDate;
                        }

                        if (!MotorVehicle.Statics.HeldReg )
                        {
                            txtEffectiveDate.Text =
                                Strings.Format(
                                    FindFirstDay(Strings.Format(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"),
                                        "MM/dd/yyyy")), "MM/dd/yyyy");
                            if (chk2Year.CheckState == Wisej.Web.CheckState.Checked &&
                                !MotorVehicle.Statics.gblnDelayedReg && MotorVehicle.Statics.RegistrationType != "CORR")
                            {
                                txtEffectiveDate.Text =
                                    Strings.Format(
                                        fecherFoundation.DateAndTime.DateAdd("yyyy", -1,
                                            FCConvert.ToDateTime(txtEffectiveDate.Text)), "MM/dd/yyyy");
                            }
                        }

                        CalculateEffectiveDate = true;
                    }
                }
            }
            else if (MotorVehicle.Statics.RegistrationType == "NRR")
            {
                if (!MotorVehicle.Statics.HeldReg)
                {
                    if (MotorVehicle.Statics.RegisteringFleet)
                    {
                        txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yyyy");
                    }
                    else
                    {
                        if (txtExpires.Text != "")
                        {
                            if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                    FCConvert.ToDateTime(txtExpires.Text)) > 12 &&
                                fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                    FCConvert.ToDateTime(txtExpires.Text)) <= 18)
                            {
                                if (MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "CL")
                                {
                                    if (Information.IsDate(txtExpires.Text))
                                    {
                                        txtEffectiveDate.Text = Strings.Format(
                                            FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtExpires.Text)
                                                .Month) + "/01/" + FCConvert.ToString(
                                                fecherFoundation.DateAndTime.DateValue(txtExpires.Text).Year - 1),
                                            "MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                                    }
                                }
                                else
                                {
                                    intDifference = fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                                        FCConvert.ToDateTime(txtExpires.Text)) - 12;
                                    if (DateTime.Today.Month <= 12 - intDifference)
                                    {
                                        txtEffectiveDate.Text =
                                            Strings.Format(
                                                FCConvert.ToString(DateTime.Today.Month + intDifference) + "/01/" +
                                                FCConvert.ToString(DateTime.Today.Year), "MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        txtEffectiveDate.Text =
                                            Strings.Format(
                                                FCConvert.ToString((DateTime.Today.Month + intDifference) - 12) +
                                                "/01/" + FCConvert.ToString(DateTime.Today.Year + 1), "MM/dd/yyyy");
                                    }
                                }

                                // kk01062016 tromv-1133  Allow future effective date for Two Year reg
                            }
                            else if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                         FCConvert.ToDateTime(txtExpires.Text)) > 24 &&
                                     fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                         FCConvert.ToDateTime(txtExpires.Text)) <= 30 &&
                                     chk2Year.CheckState == Wisej.Web.CheckState.Checked)
                            {
                                if (Information.IsDate(txtExpires.Text))
                                {
                                    txtEffectiveDate.Text = Strings.Format(
                                        FCConvert.ToString(
                                            fecherFoundation.DateAndTime.DateValue(txtExpires.Text).Month) + "/01/" +
                                        FCConvert.ToString(
                                            fecherFoundation.DateAndTime.DateValue(txtExpires.Text).Year - 2),
                                        "MM/dd/yyyy");
                                }
                                else
                                {
                                    txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                                }
                            }
                            else if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                         FCConvert.ToDateTime(txtExpires.Text)) > 18 &&
                                     chk2Year.CheckState != Wisej.Web.CheckState.Checked)
                            {
                                CalculateEffectiveDate = false;
                                return CalculateEffectiveDate;
                            }
                            else
                            {
                                txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                            }
                        }
                        else
                        {
                            txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                        }
                    }

                    if (!MotorVehicle.Statics.RegisteringFleet)
                    {
                        if (MotorVehicle.Statics.FixedMonth == false)
                        {
                            if (chk2Year.CheckState == Wisej.Web.CheckState.Unchecked)
                            {
                                txtExpires.Text =
                                    Strings.Format(
                                        FindLastDay(FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("yyyy", 1,
                                            FCConvert.ToDateTime(Strings.Format(txtEffectiveDate.Text, "mm/yyyy"))))),
                                        "MM/dd/yyyy");
                            }
                            else
                            {
                                txtExpires.Text =
                                    Strings.Format(
                                        FindLastDay(FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("yyyy", 2,
                                            FCConvert.ToDateTime(Strings.Format(txtEffectiveDate.Text, "mm/yyyy"))))),
                                        "MM/dd/yyyy");
                            }
                        }
                    }
                }

                CalculateEffectiveDate = true;
            }
            else if ((MotorVehicle.Statics.RegistrationType == "NRT") ||
                     (MotorVehicle.Statics.RegistrationType == "RRT"))
            {
                if (!MotorVehicle.Statics.HeldReg)
                {
                    txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                }

                if (fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")) !=
                    true)
                {
                    if (!MotorVehicle.Statics.HeldReg)
                    {
                        if (MotorVehicle.Statics.FixedMonth == false)
                        {
                            txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"),
                                "MM/dd/yyyy");
                        }
                        else
                        {

                            if (MotorVehicle.Statics.gintFixedMonthMonth < 12)
                            {
                                datTempDate = FCConvert.ToDateTime(
                                    FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" +
                                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")
                                        .Year));
                            }
                            else
                            {
                                datTempDate = FCConvert.ToDateTime(
                                    "1/1/" + FCConvert.ToString(
                                        FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")
                                            .Year) + 1));
                            }

                            datTempDate = fecherFoundation.DateAndTime.DateAdd("d", -1, datTempDate);
                            if (datTempDate.ToOADate() < DateTime.Today.ToOADate())
                            {
                                txtExpires.Text = "X";
                                CalculateEffectiveDate = false;
                                return CalculateEffectiveDate;
                            }
                            else
                            {
                                txtExpires.Text = Strings.Format(datTempDate, "MM/dd/yyyy");
                                MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", datTempDate);
                            }
                        }
                    }

                    CalculateEffectiveDate = true;
                }
                else
                {
                    if (FCConvert.CBool(VerifyExpireDate(txtExpires.Text)))
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", txtExpires.Text);
                    }
                    else
                    {
                        txtExpires.Focus();
                        // Bp = Beep(1000, 250)
                    }
                }
            }
            else if ((MotorVehicle.Statics.RegistrationType == "LOST") ||
                     (MotorVehicle.Statics.RegistrationType == "GVW"))
            {
                if (Information.IsDate(txtExpires.Text) == true)
                {
                    CalculateEffectiveDate = true;
                }
            }

            // 
            if (MotorVehicle.Statics.Class == "CI")
            {
                if (DateTime.Today.Year < 2010)
                {
                    txtExpires.Text = "02/28/2010";
                }
				else if(DateTime.Today.Year < 2020)
				{
					txtExpires.Text = "02/29/2020";
				}
				else
				{
					txtExpires.Text = "02/28/2030";
				}
			}

            return CalculateEffectiveDate;
        }

        // vbPorter upgrade warning: Newdate As string	OnWrite(DateTime, string)
        // vbPorter upgrade warning: 'Return' As DateTime	OnWrite(string)
        public DateTime FindLastDay(string Newdate)
        {
            DateTime FindLastDay = System.DateTime.Now;
            // vbPorter upgrade warning: xdate As object	OnWrite(DateTime)
            object xdate;
            xdate = (fecherFoundation.DateAndTime.DateAdd("m", 1, FCConvert.ToDateTime(Newdate)));
            xdate = fecherFoundation.DateAndTime.DateAdd("d", -1, (DateTime)xdate);
            FindLastDay = FCConvert.ToDateTime(Strings.Format(xdate, "MM/dd/yyyy"));
            return FindLastDay;
        }

        // vbPorter upgrade warning: textdate As string	OnWrite(DateTime)
        // vbPorter upgrade warning: 'Return' As DateTime	OnWrite(string)
        public DateTime FindFirstDay(string textdate)
        {
            DateTime FindFirstDay = System.DateTime.Now;
            // vbPorter upgrade warning: xdate As object	OnWrite(DateTime, string)
            DateTime xdate = default(DateTime);
            if (MotorVehicle.IsProratedMotorcycleExpirationDate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                MotorVehicle.Statics.datMotorCycleEffective, MotorVehicle.Statics.datMotorCycleExpires,
                MotorVehicle.Statics.RegistrationType))
            {
                xdate = MotorVehicle.Statics.datMotorCycleExpires;
            }
            else
            {
                xdate = Convert.ToDateTime(txtExpires.Text);
            }

            if (MotorVehicle.Statics.RegistrationType != "RRR" && MotorVehicle.Statics.RegistrationType != "CORR")
            {
                FindFirstDay = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
            }
            else if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                FindFirstDay = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate");
            }
            else
            {
                if (MotorVehicle.Statics.gblnDelayedReg)
                {
                    FindFirstDay = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                }
                else
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CI")
                    {
                        xdate = Convert.ToDateTime("02/01/" + FCConvert.ToString(DateTime.Today.Year));
                    }
                    else
                    {
                        if (MotorVehicle.IsProratedMotorcycleExpirationDate(
                            MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                            MotorVehicle.Statics.datMotorCycleEffective, MotorVehicle.Statics.datMotorCycleExpires,
                            MotorVehicle.Statics.RegistrationType))
                        {
                            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
                            {
                                if (Convert.ToDateTime(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")).Year ==
                                    DateAndTime.DateAdd("yyyy", -1, xdate).Year)
                                {
                                    // do nothing
                                }
                                else
                                {
                                    xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", -1, xdate);
                                }
                            }
                            else
                            {
                                // do nothing
                            }
                        }
                        else
                        {
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CS")
                            {
                                xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", -10, xdate);
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "ST")
                            {
                                xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", -6, xdate);
                            }
                            else
                            {
                                xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", -1, xdate);
                            }
                        }
                    }

                    FindFirstDay = FCConvert.ToDateTime(Strings.Format(
                        FCConvert.ToString(xdate.Month) + "/01/" + FCConvert.ToString(xdate.Year), "MM/dd/yyyy"));
                }
            }

            return FindFirstDay;
        }

        public string VerifyExpireDate(string tdate)
        {
            string VerifyExpireDate = "";
            VerifyExpireDate = "T";
            string xdate;
            // vbPorter upgrade warning: xxdate As string	OnWrite(DateTime)
            string xxdate = "";
            // vbPorter upgrade warning: LowDate As DateTime	OnWrite(string, DateTime)
            DateTime LowDate;
            DateTime HighDate;
            string ExpDate = "";
            xdate = Strings.Mid(tdate, 1, Strings.InStr(1, tdate, "/", CompareConstants.vbBinaryCompare)) + "01/" +
                    Strings.Mid(Strings.Format(tdate, "MM/dd/yyyy"), 9, 2);
            if (Information.IsDate(xdate))
            {
                xxdate = FCConvert.ToString(FindLastDay(xdate));
            }
            else
            {
                VerifyExpireDate = "F";
                return VerifyExpireDate;
            }

            txtExpires.Text = Strings.Format(xxdate, "MM/dd/yyyy");
            if ((MotorVehicle.Statics.RegistrationType == "RRR") || (MotorVehicle.Statics.RegistrationType == "CORR"))
            {
                if (MotorVehicle.Statics.RegistrationType == "RRR")
                {
                    if (MotorVehicle.Statics.Class == "TL" &&
                        (MotorVehicle.Statics.Subclass == "L2" || MotorVehicle.Statics.Subclass == "L4" ||
                         MotorVehicle.Statics.Subclass == "L7"))
                    {
                        if (MotorVehicle.Statics.HeldReg)
                        {
                            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwoYear")))
                            {
                                chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                            }

                            LowDate = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate");
                        }
                        else if (MotorVehicle.Statics.NewToTheSystem)
                        {
                            LowDate = FCConvert.ToDateTime(txtEffectiveDate.Text);
                        }
                        else
                        {
                            if (txtEffectiveDate.Text != "")
                            {
                                LowDate = FCConvert.ToDateTime(txtEffectiveDate.Text);
                            }
                            else
                            {
                                LowDate = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate");
                            }
                        }
                    }
                    else
                    {
                        if (MotorVehicle.Statics.HeldReg)
                        {
                            LowDate = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate");
                        }
                        else
                        {
                            LowDate = DateTime.Today;
                        }
                    }
                }
                else
                {
                    if (intCorrCorrectOriginal == DialogResult.Yes)
                    {
                        LowDate = MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("EffectiveDate");
                    }
                    else
                    {
                        LowDate = DateTime.Today;
                    }
                }

                HighDate = fecherFoundation.DateAndTime.DateAdd("m", 18, DateTime.Today);
                bool executeCheck2Year = false;
                if (chk2Year.CheckState == Wisej.Web.CheckState.Unchecked)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "CI" &&
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "CS" &&
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "ST")
                    {
                        if (LowDate.ToOADate() > fecherFoundation.DateAndTime.DateValue(xxdate).ToOADate())
                        {
                            ShowWarningBox(xxdate + " is an invalid Expiration Date for this Registration.",
                                "Invalid Date");
                            if (MotorVehicle.Statics.RegistrationType == "RRR")
                            {
                                txtExpires.Text = "00/00/0000";
                                VerifyExpireDate = "F";
                            }
                            else
                            {
                                VerifyExpireDate = "X";
                            }

                            return VerifyExpireDate;
                        }
                    }

                    if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "CI" &&
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "CS" &&
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "ST")
                    {
                        if (HighDate.ToOADate() < fecherFoundation.DateAndTime.DateValue(xxdate).ToOADate() &&
                            MotorVehicle.Statics.TwoYear == false)
                        {
                            // DJW@05012013 TROMV-730 Added to hndle vehicles entered in through new to sstem that had been 2 year regs
                            if (chk2Year.Enabled)
                            {
                                chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                executeCheck2Year = true;
                                goto Check2Year;
                            }
                            else
                            {
                                ShowWarningBox(xxdate + " is an invalid Expiration Date for this Registration.",
                                    "Invalid Date");
                                VerifyExpireDate = "X";
                                return VerifyExpireDate;
                            }
                        }
                    }
                }
                else
                {
                    if (LowDate.ToOADate() > fecherFoundation.DateAndTime.DateValue(xxdate).ToOADate())
                    {
                        ShowWarningBox(xxdate + " is an invalid Expiration Date for this Registration.",
                            "Invalid Date");
                        txtExpires.Text = "00/00/0000";
                        VerifyExpireDate = "F";
                        return VerifyExpireDate;
                    }

                    executeCheck2Year = true;
                    goto Check2Year;
                }

            Check2Year:;
                if (executeCheck2Year)
                {
                    HighDate = fecherFoundation.DateAndTime.DateAdd("yyyy", 1, HighDate);
                    if (HighDate.ToOADate() < fecherFoundation.DateAndTime.DateValue(xxdate).ToOADate())
                    {
                        ShowWarningBox(xxdate + " is an invalid Expiration Date for this Registration.",
                            "Invalid Date");
                        VerifyExpireDate = "X";
                        return VerifyExpireDate;
                    }

                    executeCheck2Year = false;
                }
            }

            // vbPorter upgrade warning: NM As int	OnWrite(long, int)
            int NM = 0;
            if (MotorVehicle.Statics.RegistrationType != "CORR")
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "TL" ||
                    MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "CL")
                {
                    if (MotorVehicle.Statics.HeldReg)
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() >
                            DateTime.Today.ToOADate())
                        {
                            // Corey 08/09/2006
                            if (Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("ExpireDate")))
                            {
                                if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate() <
                                    MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate())
                                {
                                    NM = fecherFoundation.DateAndTime.DateDiff("m",
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"),
                                        FCConvert.ToDateTime(xxdate));
                                }
                                else
                                {
                                    NM = fecherFoundation.DateAndTime.DateDiff("m",
                                        MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"),
                                        FCConvert.ToDateTime(xxdate));
                                }
                            }
                            else
                            {
                                NM = 12;
                            }
                        }
                        else
                        {
                            NM = fecherFoundation.DateAndTime.DateDiff("m",
                                MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"),
                                FCConvert.ToDateTime(xxdate));
                        }
                    }
                    else
                    {
                        if (MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate() >
                            DateTime.Today.ToOADate() && !MotorVehicle.Statics.NewToTheSystem &&
                            MotorVehicle.Statics.RegistrationType != "RRT" &&
                            MotorVehicle.Statics.RegistrationType != "NRT" &&
                            MotorVehicle.Statics.RegistrationType != "NRR" && !MotorVehicle.Statics.ExciseAP)
                        {
                            NM = fecherFoundation.DateAndTime.DateDiff("m",
                                MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"),
                                FCConvert.ToDateTime(xxdate));
                            // ******* rsfinalcompare.fields("ExpireDate") used to be xxdate
                        }
                        else
                        {
                            NM = fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                FCConvert.ToDateTime(xxdate));
                            // ******* rsfinalcompare.fields("ExpireDate") used to be xxdate
                        }
                    }
                }
                else
                {
                    if (MotorVehicle.Statics.HeldReg)
                    {
                        NM = fecherFoundation.DateAndTime.DateDiff("m",
                            MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"),
                            FCConvert.ToDateTime(xxdate));
                    }
                    else
                    {
                        NM = fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today, FCConvert.ToDateTime(xxdate));
                        // ******* rsfinalcompare.fields("ExpireDate") used to be xxdate
                    }
                }
            }
            else
            {
                // TROMV726 - Half rate was not working using rsFinalCompare.Fields("DateUpdated") so we chnaged to date for plate change corrections
                if (intCorrCorrectOriginal == DialogResult.Yes)
                {
                    NM = fecherFoundation.DateAndTime.DateDiff("m",
                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("DateUpdated"),
                        FCConvert.ToDateTime(xxdate));
                }
                else
                {
                    NM = fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today, FCConvert.ToDateTime(xxdate));
                    // ******* rsfinalcompare.fields("EffectiveDate")  used to be xxdate
                }
            }

            chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Unchecked;
            chkHalfRateState.CheckState = Wisej.Web.CheckState.Unchecked;
            chk2Year.CheckState = Wisej.Web.CheckState.Unchecked;
            if (NM < 4 && (MotorVehicle.Statics.RegistrationType != "CORR" || MotorVehicle.Statics.PlateType != 1) &&
                (!MotorVehicle.Statics.NewFleet || fleetGroupType == FleetGroupType.Group) &&
                !MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                    MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                    MotorVehicle.Statics.RegistrationType))
            {
                if (!(MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2))
                {
                    // kk05092016 Default Excise HR to false on CORR w/ Plate Change
                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
                }
				if (HalfRateAllowed(MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass))
				{
					chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
				}
            }
            else if (NM < 6 &&
                     (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "FM" ||
                      MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "AF") &&
                     (!MotorVehicle.Statics.NewFleet || fleetGroupType == FleetGroupType.Group) &&
                     !MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                         MotorVehicle.Statics.datMotorCycleEffective, ref MotorVehicle.Statics.datMotorCycleExpires,
                         MotorVehicle.Statics.RegistrationType))
            {
                if (!(MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2))
                {
                    // kk05092016 Default Excise HR to false on CORR w/ Plate Change
                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
                }

                chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
            }
            else if (MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "NRR" ||
                     MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT" ||
                     MotorVehicle.Statics.RegistrationType == "CORR")
            {
                if (chk2Year.Enabled || (MotorVehicle.Statics.RegistrationType == "NRT" ||
                                         MotorVehicle.Statics.RegistrationType == "RRT"))
                {
                    if (NM > 12)
                    {
                        // TROMV727 Added correction clause becuase expiration will always match that fo old registration
                        if (MotorVehicle.Statics.RegistrationType == "CORR")
                        {
                            if (intCorrCorrectOriginal == DialogResult.Yes)
                            {
                                if (Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("EffectiveDate")))
                                {
                                    // IsDate(rsFinalCompare.Fields("ExpireDate"))
                                    if (fecherFoundation.DateAndTime.DateDiff("m",
                                            MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("EffectiveDate"),
                                            FCConvert.ToDateTime(xxdate)) > 12)
                                    {
                                        // kk04182017 tromv-1270  why add 1 day?  DateDiff("m", DateAdd("d", 1, rsFinalCompare.Fields("EffectiveDate"))
                                        chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                        Recalculate();
                                    }
                                }
                                else
                                {
                                    chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                    Recalculate();
                                }
                            }
                            else
                            {
                                if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                        FCConvert.ToDateTime(xxdate)) > 12)
                                {
                                    // kk04182017 tromv-1270  why add 1 day?  DateAdd("d", 1, Date)
                                    chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                    Recalculate();
                                }
                            }

                            // DJW@02242014 TROMV-805 Needed to add clause for transfers to set 2 year checkbox correctly
                        }
                        else if (MotorVehicle.Statics.RegistrationType == "NRT" ||
                                 MotorVehicle.Statics.RegistrationType == "RRT")
                        {
                            if (fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                    FCConvert.ToDateTime(xxdate)) > 12)
                            {
                                // kk04182017 tromv-1270  why add 1 day?  DateAdd("d", 1, Date)
                                chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                Recalculate();
                            }
                        }
                        else
                        {
                            if (Information.IsDate(MotorVehicle.Statics.rsFinalCompare.Get_Fields("ExpireDate")))
                            {
                                if (fecherFoundation.DateAndTime.DateDiff("m",
                                        fecherFoundation.DateAndTime.DateAdd("d", 1,
                                            MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate")),
                                        FCConvert.ToDateTime(xxdate)) > 12)
                                {
                                    chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                    Recalculate();
                                }
                            }
                            else
                            {
                                chk2Year.CheckState = Wisej.Web.CheckState.Checked;
                                Recalculate();
                            }
                        }
                    }

                    // DJW 02/13/2008 - Took out if statement that did not set half rate if NRT was being processed   And RegistrationType <> "NRT"
                    if (chk2Year.CheckState == Wisej.Web.CheckState.Checked &&
                        MotorVehicle.Statics.RegistrationType != "RRT")
                    {
                        if (NM > 12 && NM < 16)
                        {
                            chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
                            chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
                        }
                    }
                }
            }

            if (MotorVehicle.Statics.HeldReg)
            {
                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")))
                {
                    chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
                }

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")))
                {
                    chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
                }
            }

            return VerifyExpireDate;
        }

        private bool HalfRateAllowed(String regClass, String regSubclass)
        {
	        bool halfRateAllowed = false;
	        try
	        {
                using (clsDRWrapper rsClass = new clsDRWrapper())
                {
                    rsClass.OpenRecordset("SELECT HalfRateAllowed FROM Class " +
                                          "WHERE BMVCode = '" + regClass + "' AND SystemCode = '" + regSubclass + "'");
                    if (!rsClass.EndOfFile())
                    {
                        if (!String.IsNullOrEmpty(rsClass.Get_Fields_String("HalfRateAllowed")))
                        {
                            halfRateAllowed = rsClass.Get_Fields_String("HalfRateAllowed") == "Y";
                        }
                    }
                }
            }
	        catch(Exception ex)
	        {
                MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
	        return halfRateAllowed;
        }

        private void Set_Tab_Index()
        {
            int counter;
            counter = 0;
            txtExpires.TabIndex = counter;
            counter += 1;
            txtMileage.TabIndex = counter;
            counter += 1;
            txtVin.TabIndex = counter;
            counter += 1;
            txtYear.TabIndex = counter;
            counter += 1;
            txtMake.TabIndex = counter;
            counter += 1;
            txtModel.TabIndex = counter;
            counter += 1;
            txtColor1.TabIndex = counter;
            counter += 1;
            txtColor2.TabIndex = counter;
            counter += 1;
            txtStyle.TabIndex = counter;
            counter += 1;
            txtTires.TabIndex = counter;
            counter += 1;
            txtAxles.TabIndex = counter;
            counter += 1;
            txtNetWeight.TabIndex = counter;
            counter += 1;
            txtRegWeight.TabIndex = counter;
            counter += 1;
            txtFuel.TabIndex = counter;
            counter += 1;
            txtReg1LR.TabIndex = counter;
            counter += 1;
            txtReg1ICM.TabIndex = counter;
            counter += 1;
            txtReg1PartyID.TabIndex = counter;
            counter += 1;
            imgReg1Search.TabIndex = counter;
            counter += 1;
            imgReg1Edit.TabIndex = counter;
            counter += 1;
            txtReg1DOB.TabIndex = counter;
            counter += 1;
            txtReg2LR.TabIndex = counter;
            counter += 1;
            txtReg2ICM.TabIndex = counter;
            counter += 1;
            txtReg2PartyID.TabIndex = counter;
            counter += 1;
            imgReg2Search.TabIndex = counter;
            counter += 1;
            imgReg2Edit.TabIndex = counter;
            counter += 1;
            txtReg2DOB.TabIndex = counter;
            counter += 1;
            txtReg3LR.TabIndex = counter;
            counter += 1;
            txtReg3ICM.TabIndex = counter;
            counter += 1;
            txtReg3PartyID.TabIndex = counter;
            counter += 1;
            imgReg3Search.TabIndex = counter;
            counter += 1;
            imgReg3Edit.TabIndex = counter;
            counter += 1;
            txtReg3DOB.TabIndex = counter;
            counter += 1;
            txtUnitNumber.TabIndex = counter;
            counter += 1;
            txtDOTNumber.TabIndex = counter;
            counter += 1;
            txtTaxIDNumber.TabIndex = counter;
            counter += 1;
            txtAddress1.TabIndex = counter;
            counter += 1;
            txtAddress2.TabIndex = counter;
            counter += 1;
            txtCity.TabIndex = counter;
            counter += 1;
            txtState.TabIndex = counter;
            counter += 1;
            txtZip.TabIndex = counter;
            counter += 1;
            txtCountry.TabIndex = counter;
            counter += 1;
            txtLegalResStreet.TabIndex = counter;
            counter += 1;
            txtLegalres.TabIndex = counter;
            counter += 1;
            txtResState.TabIndex = counter;
            counter += 1;
            txtResCountry.TabIndex = counter;
            counter += 1;
            txtresidenceCode.TabIndex = counter;
            counter += 1;
            txtMonthCharge.TabIndex = counter;
            counter += 1;
            txtMonthNoCharge.TabIndex = counter;
            counter += 1;
            txtMStickerNumber.TabIndex = counter;
            counter += 1;
            txtYearCharge.TabIndex = counter;
            counter += 1;
            txtYearNoCharge.TabIndex = counter;
            counter += 1;
            txtYStickerNumber.TabIndex = counter;
            counter += 1;
            txtPreviousTitle.TabIndex = counter;
            counter += 1;
            txtPriorTitleState.TabIndex = counter;
            counter += 1;
            txtBase.TabIndex = counter;
            counter += 1;
            txtMilYear.TabIndex = counter;
            counter += 1;
            txtMilRate.TabIndex = counter;
            counter += 1;
            txtAgentFee.TabIndex = counter;
            counter += 1;
            txtExciseTax.TabIndex = counter;
            counter += 1;
            txtTownCredit.TabIndex = counter;
            counter += 1;
            txtSubTotal.TabIndex = counter;
            counter += 1;
            txtTransferCharge.TabIndex = counter;
            counter += 1;
            txtBalance.TabIndex = counter;
            counter += 1;
            txtCreditNumber.TabIndex = counter;
            counter += 1;
            txtExciseTaxDate.TabIndex = counter;
            counter += 1;
            txtRate.TabIndex = counter;
            counter += 1;
            txtStateCredit.TabIndex = counter;
            counter += 1;
            txtFees.TabIndex = counter;
            counter += 1;
            txtSalesTax.TabIndex = counter;
            counter += 1;
            txtTitle.TabIndex = counter;
            counter += 1;
            txtCTANumber.TabIndex = counter;
            counter += 1;
            txtUserID.TabIndex = counter;
            counter += 1;
            chkETO.TabIndex = counter;
            counter += 1;
            chk2Year.TabIndex = counter;
            counter += 1;
            chkHalfRateLocal.TabIndex = counter;
            counter += 1;
            chkHalfRateState.TabIndex = counter;
            counter += 1;
            chkEAP.TabIndex = counter;
            counter += 1;
            chkStateExempt.TabIndex = counter;
            counter += 1;
            chkAgentFeeExempt.TabIndex = counter;
            counter += 1;
            chkExciseExempt.TabIndex = counter;
            counter += 1;
            chkTransferExempt.TabIndex = counter;
            counter += 1;
            chkAmputeeVet.TabIndex = counter;
            counter += 1;
            chkTrailerVanity.TabIndex = counter;
            counter += 1;
            chkRental.TabIndex = counter;
            counter += 1;
            cboBattle.TabIndex = counter;
            counter += 1;
            chkCC.TabIndex = counter;
            counter += 1;
            txtFleetNumber.TabIndex = counter;
            counter += 1;
            txtRegistrationNumber.TabIndex = counter;
            counter += 1;
            txtClass.TabIndex = counter;
            counter += 1;
            txtEffectiveDate.TabIndex = counter;

            //FC:FINAL:SBE - #3956 - activate control with the new lowest TabIndex
            txtVin.CausesValidation = false;
            txtExpires.Focus();
            txtVin.CausesValidation = true;
        }

        public void StickerAmount(string Direction)
        {
            int YearNumber;
            YearNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(lblNumberofYear.Text)));
            if (Direction == "Higher")
            {
                YearNumber += 1;
            }
            else if (Direction == "Lower")
            {
                YearNumber -= 1;
            }

            lblNumberofYear.Text = Strings.Format(YearNumber, "00");
        }

        // Private Sub FillCodes()
        private void GetCodeFromVin(IEnumerable<Mfg> manufacturers)
        {

            string lastMake = "";

            string lastYear = "";

            int count = 0;
            string strMake = "";
            string strList = "";

            var strChecker = Strings.Mid(txtVin.Text, 1, 3) + Strings.Mid(txtVin.Text, 10, 1);

           

            if (manufacturers.Any())
            {
                var strYear = GetYearFromVin(txtVin.Text);
                foreach (var manufacturer in manufacturers)
                {
                    strMake = modGlobalFunctions.ReturnCorrectedMakeCode(manufacturer.Type,
                        manufacturer.Model,manufacturer.Manufacturer);
                    if (strYear == manufacturer.Year)
                    {
                        if (strYear != lastYear || strMake != lastMake)
                        {
                            if (strList == "")
                            {
                                strList = Strings.Format(strYear, "0000") + strMake + ";" + strYear + " " + strMake;
                            }
                            else
                            {
                                strList += "|" + Strings.Format(strYear, "0000") + strMake + ";" + strYear + " " +
                                           strMake;
                            }

                            lastMake = strMake;
                            lastYear = strYear;
                            count++;
                        }
                    }
                }

                if (count == 1)
                {
                    txtYear.Text = lastYear;
                    txtYear_Validate(null, new System.ComponentModel.CancelEventArgs(false));
                    txtMake.Text = lastMake;
                    txtMake.Focus();
                    return;
                }
            }

            frmSelectCode.InstancePtr.LoadList(ref strList);
            frmSelectCode.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            var strRetCode = frmSelectCode.InstancePtr.SelectedCode;
            //FC:FINAL:DDU:#i1960 - hold disposing of form, we need just to hide and use it's controls data
            frmSelectCode.InstancePtr.dontDispose = false;
            frmSelectCode.InstancePtr.Unload();
            if (strRetCode != "")
            {
                txtYear.Text = Strings.Left(strRetCode, 4);
                txtYear_Validate(null, new System.ComponentModel.CancelEventArgs(false));
                txtMake.Text = fecherFoundation.Strings.Trim(Strings.Right(strRetCode, strRetCode.Length - 4));
                txtMake.Focus();
            }
        }
        private void GetCode(string strVCode)
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            string strChecker = "";
            string strYear = "";
            string strMake = "";
            string strRetCode;
            string strList = "";

            try
            {
                if (strVCode == "Make")
                {
                    tempRS.OpenRecordset("SELECT Code, Make FROM Make ORDER BY Code");
                    if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
                    {
                        tempRS.MoveLast();
                        tempRS.MoveFirst();
                        while (tempRS.EndOfFile() != true)
                        {
                            if (strList == "")
                            {
                                strList = tempRS.Get_Fields("Code") + ";" + tempRS.Get_Fields("Code") + " - " +
                                          tempRS.Get_Fields_String("Make");
                            }
                            else
                            {
                                strList += "|" + tempRS.Get_Fields("Code") + ";" + tempRS.Get_Fields("Code") + " - " +
                                           tempRS.Get_Fields_String("Make");
                            }

                            tempRS.MoveNext();
                        }
                    }
                }
                else if (strVCode == "Style")
                {
                    var results = styleCodeService.GetValidStyleCodeList(
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"),
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    if (results.Count() > 0)
                    {
                        foreach (var style in results)
                        {
                            if (strList == "")
                            {
                                strList = style.Code + ";" + style.Code + " - " +
                                          style.Description;
                            }
                            else
                            {
                                strList += "|" + style.Code + ";" + style.Code + " - " +
                                           style.Description;
                            }
                        }
                    }
                }
                else
                {
                    tempRS.OpenRecordset("SELECT Code, Color FROM Color ORDER BY Color");
                    if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
                    {
                        tempRS.MoveLast();
                        tempRS.MoveFirst();
                        while (tempRS.EndOfFile() != true)
                        {
                            if (strList == "")
                            {
                                strList = tempRS.Get_Fields("Code") + ";" + tempRS.Get_Fields("Code") + " - " +
                                          tempRS.Get_Fields_String("Color");
                            }
                            else
                            {
                                strList += "|" + tempRS.Get_Fields("Code") + ";" + tempRS.Get_Fields("Code") + " - " +
                                           tempRS.Get_Fields_String("Color");
                            }

                            tempRS.MoveNext();
                        }
                    }
                }

                //! Load frmSelectCode;
                frmSelectCode.InstancePtr.LoadList(ref strList);
                frmSelectCode.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                strRetCode = frmSelectCode.InstancePtr.SelectedCode;
                //FC:FINAL:DDU:#i1960 - hold disposing of form, we need just to hide and use it's controls data
                frmSelectCode.InstancePtr.dontDispose = false;
                frmSelectCode.InstancePtr.Unload();
                if (strRetCode != "")
                {
                    if (strVCode == "Make")
                    {
                        txtMake.Text = strRetCode;
                        txtMake.Focus();
                    }
                    else if (strVCode == "Style")
                    {
                        txtStyle.Text = strRetCode;
                        txtStyle.Focus();
                    }
                    else if (strVCode == "VIN")
                    {
                        txtYear.Text = Strings.Left(strRetCode, 4);
                        txtYear_Validate(null, new System.ComponentModel.CancelEventArgs(false));
                        txtMake.Text = fecherFoundation.Strings.Trim(Strings.Right(strRetCode, strRetCode.Length - 4));
                        txtMake.Focus();
                    }
                    else
                    {
                        txtColor1.Text = strRetCode.Trim();
                        txtColor1.Focus();
                    }
                }
            }
            finally
            {
                tempRS.Dispose();
            }
        }

        // vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
        private string GetMonth(int x)
        {
            string GetMonth = "";
            switch (x)
            {
                case 1:
                    {
                        GetMonth = "January";
                        break;
                    }
                case 2:
                    {
                        GetMonth = "February";
                        break;
                    }
                case 3:
                    {
                        GetMonth = "March";
                        break;
                    }
                case 4:
                    {
                        GetMonth = "April";
                        break;
                    }
                case 5:
                    {
                        GetMonth = "May";
                        break;
                    }
                case 6:
                    {
                        GetMonth = "June";
                        break;
                    }
                case 7:
                    {
                        GetMonth = "July";
                        break;
                    }
                case 8:
                    {
                        GetMonth = "August";
                        break;
                    }
                case 9:
                    {
                        GetMonth = "September";
                        break;
                    }
                case 10:
                    {
                        GetMonth = "October";
                        break;
                    }
                case 11:
                    {
                        GetMonth = "November";
                        break;
                    }
                case 12:
                    {
                        GetMonth = "December";
                        break;
                    }
            }

            //end switch
            return GetMonth;
        }

        private string Prorate(string x, int y)
        {
            string Prorate = "";
            double total;
            total = FCConvert.ToDouble(x) / 12;
            total *= y;
            Prorate = total.ToString();
            return Prorate;
        }

        private int DateDifference()
        {
            int DateDifference = 0;
            // vbPorter upgrade warning: x As int	OnWrite(long, int)
            int x = 0;

            if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
            {
                if (MotorVehicle.Statics.RegisteringFleet)
                {
                    x = fecherFoundation.DateAndTime.DateDiff("m",
                        fecherFoundation.DateAndTime.DateAdd("m", -1, MotorVehicle.Statics.PreEffectiveDate),
                        fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                }
                else
                {
                    if (MotorVehicle.Statics.ETO &&
                        MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")))
                    {
                        x = fecherFoundation.DateAndTime.DateDiff("m",
                            fecherFoundation.DateAndTime.DateAdd("m", -1, DateTime.Today),
                            MotorVehicle.Statics.datMotorCycleExpires);
                    }
                    else
                    {
                        if (Information.IsDate(txtExpires.Text))
                        {
                            x = fecherFoundation.DateAndTime.DateDiff("m",
                                fecherFoundation.DateAndTime.DateAdd("m", -1, DateTime.Today),
                                fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                        }
                        else
                        {
                            x = 12;
                        }
                    }
                }
            }
            else
            {
                if (Information.IsDate(txtExpires.Text))
                {
                    if (!MotorVehicle.Statics.blnChangeToFleet)
                    {
                        if (fecherFoundation.DateAndTime.DateDiff("m",
                                MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"),
                                DateTime.Today) >= 12 || MotorVehicle.Statics.NewToTheSystem ||
                            (MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate() <
                             DateTime.Today.ToOADate() &&
                             MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate() <
                             FCConvert.ToDateTime("3/1/2012").ToOADate()))
                        {
                            x = fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today,
                                    fecherFoundation.DateAndTime.DateValue(txtExpires.Text)) + 1;
                        }
                        else
                        {
                            if (MotorVehicle.Statics.RegistrationType == "CORR")
                            {
                                x = fecherFoundation.DateAndTime.DateDiff("m",
                                    fecherFoundation.DateAndTime.DateAdd("m", -1, MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("EffectiveDate")),
                                    fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                            }
                            else if (MotorVehicle.Statics.ExciseAP)
                            {
                                x = fecherFoundation.DateAndTime.DateDiff("m",
                                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("EffectiveDate"),
                                    fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                            }
                            else
                            {
                                x = fecherFoundation.DateAndTime.DateDiff("m",
                                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"),
                                    fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                            }
                        }
                    }
                    else
                    {
                        x = fecherFoundation.DateAndTime.DateDiff("m",
                            MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"),
                            fecherFoundation.DateAndTime.DateValue(txtExpires.Text));
                        if (x <= 0)
                        {
                            x = 0;
                        }
                        else if (x > 12)
                        {
                            x = 12;
                        }

                        DateDifference = x;
                        return DateDifference;
                    }
                }
                else
                {
                    x = 12;
                }
            }

            if (MotorVehicle.Statics.RegistrationType != "NRR" && MotorVehicle.Statics.RegistrationType != "NRT")
            {
                if (fecherFoundation.DateAndTime.DateDiff("m",
                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate"), DateTime.Today) >= 12 ||
                    MotorVehicle.Statics.NewToTheSystem ||
                    (MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate() <
                     DateTime.Today.ToOADate() &&
                     MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").ToOADate() <
                     FCConvert.ToDateTime("3/1/2012").ToOADate()))
                {
                    // do nothing
                }
                else
                {
                    if (MotorVehicle.IsProratedMotorcycleExpirationDate(
                        MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                        MotorVehicle.Statics.datMotorCycleEffective, MotorVehicle.Statics.datMotorCycleExpires,
                        MotorVehicle.Statics.RegistrationType))
                    {
                        x += 1;
                    }
                }
            }

            if (x < 0)
            {
                x *= -1;
            }
            else if (x == 0)
            {
                x = 1;
            }
            else if (x > 12)
            {
                x = 12;
            }

            DateDifference = x;
            return DateDifference;
        }

        private void FillCorrectionFeesScreen()
        {
            int ans;
            clsDRWrapper rsLostPlateFee = new clsDRWrapper();

            try
            {
                rsLostPlateFee.OpenRecordset("SELECT * FROM DefaultInfo");
                //App.DoEvents();
                if (MotorVehicle.Statics.PlateType == 1)
                {
                    frmFeesInfo.InstancePtr.txtFEERegFee.Text =
                        Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00");
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtFEERegFee.Text = Strings.Format(
                        MotorVehicle.GetRegRate(this, MotorVehicle.Statics.Class, MotorVehicle.Statics.Subclass),
                        "#,##0.00");
                }

                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")) != 0)
                {
                    frmFeesInfo.InstancePtr.txtFEECreditComm.Text = Strings.Format(
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")),
                        "#,##0.00");
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtFEECreditComm.Text = "0.00";
                }

                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) != 0)
                {
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = Strings.Format(
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")),
                        "#,##0.00");
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = "0.00";
                }

                //App.DoEvents();
                if (MotorVehicle.Statics.PlateType == 2)
                {
                    if (MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                            MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"),
                            chkTrailerVanity.CheckState == Wisej.Web.CheckState.Checked,
                            chk2Year.CheckState == Wisej.Web.CheckState.Checked) > 0)
                    {
                        if (intCorrInitialPlate == 0)
                        {
                            intCorrInitialPlate =
                                MessageBox.Show("Should a Vanity Plate fee be charged for this registration?",
                                    "Vanity Plate Fee?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        }

                        if (intCorrInitialPlate == DialogResult.Yes)
                        {
                            frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(
                                MotorVehicle.GetInitialPlateFees2(
                                    MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                                    MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"),
                                    chkTrailerVanity.CheckState == Wisej.Web.CheckState.Checked,
                                    chk2Year.CheckState == Wisej.Web.CheckState.Checked), "#,##0.00");
                        }
                        else
                        {
                            if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee")) != 0)
                            {
                                frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(
                                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee")),
                                    "#,##0.00");
                            }
                            else
                            {
                                frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = "0.00";
                            }
                        }
                    }
                    else
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee")) != 0)
                        {
                            frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(
                                Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee")),
                                "#,##0.00");
                        }
                        else
                        {
                            frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = "0.00";
                        }
                    }
                }
                else
                {
                    if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee")) != 0)
                    {
                        frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = Strings.Format(
                            Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee")), "#,##0.00");
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = "0.00";
                    }
                }

                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate")) != 0)
                {
                    frmFeesInfo.InstancePtr.txtFEEReservePlate.Text = Strings.Format(
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate")), "#,##0.00");
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtFEEReservePlate.Text = "0.00";
                }

                // And rsFinal.Fields("Class") <> rsFinalCompare.Fields("Class")
                if (MotorVehicle.Statics.PlateType == 2)
                {
                    if (MotorVehicle.Statics.ForcedPlate != "P")
                    {
                        if (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) !=
                            "")
                        {
                            if (intCorrSpecialtyPlate == 0)
                            {
                                if (!MotorVehicle.Statics.LostPlate)
                                {
                                    intCorrSpecialtyPlate = MessageBox.Show("Should a new specialty fee be charged?",
                                        "Specialty Plate Fee?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                }
                                else
                                {
                                    intCorrSpecialtyPlate = DialogResult.No;
                                }
                            }

                            if (intCorrSpecialtyPlate == DialogResult.Yes)
                            {
                                MotorVehicle.Statics.NewPlate = true;
                            }
                            else
                            {
                                MotorVehicle.Statics.NewPlate = false;
                                if (MotorVehicle.Statics.PlateType == 2)
                                {
                                    // And rsFinal.Fields("PlateFeeReReg") <> 0
                                    if (MotorVehicle.Statics.intCorrExtendedSpecialtyPlate == 0)
                                    {
                                        MotorVehicle.Statics.intCorrExtendedSpecialtyPlate = MessageBox.Show(
                                            "Will this registration extend the life of the Specialty Plate?",
                                            "Specialty Plate Fee?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                    }
                                }
                            }
                        }

                        var specialtyPlate =
                            MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));

                        if ((specialtyPlate == "CR") || (specialtyPlate == "UM") || (specialtyPlate == "LB") ||
                            (specialtyPlate == "BB") || specialtyPlate == "BH" || (specialtyPlate == "AG") ||
                            (specialtyPlate == "AC") || (specialtyPlate == "AF") || (specialtyPlate == "TS") ||
                            (specialtyPlate == "BC") || (specialtyPlate == "AW") || (specialtyPlate == "LC"))
                        {
                            if (intCorrSpecialtyPlate == DialogResult.Yes)
                            {
                                frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "20.00";
                            }
                            else
                            {
                                if (MotorVehicle.Statics.intCorrExtendedSpecialtyPlate == DialogResult.Yes)
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "15.00";
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                }
                            }
                        }
                        else if (MotorVehicle.GetSpecialtyPlate(
                                     MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                 "SW")
                        {
                            if (intCorrSpecialtyPlate == DialogResult.Yes)
                            {
                                frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "20.00";
                            }
                            else
                            {
                                if (MotorVehicle.Statics.intCorrExtendedSpecialtyPlate == DialogResult.Yes)
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "20.00";
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                                }
                            }
                        }
                        else if (MotorVehicle.GetSpecialtyPlate(
                                     MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                 "PH")
                        {
                            if (intCorrSpecialtyPlate == DialogResult.Yes)
                            {
                                if (MotorVehicle.Statics.Class == "PM")
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "5.00";
                                }
                                else
                                {
                                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "10.00";
                                }
                            }
                            else
                            {
                                frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                            }
                        }
                        else if (MotorVehicle.GetSpecialtyPlate(
                                     MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) ==
                                 "VT")
                        {
                            if (intCorrSpecialtyPlate == DialogResult.Yes)
                            {
                                frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "5.00";
                                txtAgentFee.Text =
                                    Strings.Format(
                                        MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee") + 1,
                                        "#0.00");
                            }
                            else
                            {
                                frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                            }
                        }
                        else
                        {
                            frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                        }
                    }
                    else
                    {
                        frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = "0.00";
                    }

                    frmFeesInfo.InstancePtr.txtFEETransferFee.Text = "0.00";
                    if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtMonthCharge.Text) > 0)
                    {
                        frmFeesInfo.InstancePtr.txtFEEStickerFee.Text = Strings.Format(
                            Conversion.Val(rsLostPlateFee.Get_Fields_Decimal("Stickers")) *
                            (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtMonthCharge.Text)), "#,##0.00");
                    }
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = Strings.Format(
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeReReg")) +
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew")), "#,##0.00");
                    frmFeesInfo.InstancePtr.txtFEETransferFee.Text = Strings.Format(
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferFee")), "#,##0.00");
                    frmFeesInfo.InstancePtr.txtFEEStickerFee.Text = Strings.Format(
                        Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee")), "#,##0.00");
                }

                frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = Strings.Format(
                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee")), "#,##0.00");
                frmFeesInfo.InstancePtr.txtFEEGiftCertificate.Text = Strings.Format(
                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")),
                    "#,##0.00");
                frmFeesInfo.InstancePtr.txtGCNumber.Text =
                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"));
                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee")) == 0)
                {
                    // do nothing
                }
                else
                {
                    frmFeesInfo.InstancePtr.txtDupRegFee.Text = Strings.Format(
                        MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"), "#,##0.00");
                }

                frmFeesInfo.InstancePtr.total();
            }
            finally
            {
                rsLostPlateFee.Dispose();
            }
        }

        private void CalculateLocalECorrectFees()
        {
            if (txtTownCredit.Text == "")
            {
                txtTownCredit.Text = "0.00";
            }

            if (txtExciseTax.Text == "")
            {
                txtExciseTax.Text = "0.00";
            }

            if (txtTransferCharge.Text == "")
            {
                txtTransferCharge.Text = "0.00";
            }

            if (FCConvert.ToDecimal(txtExciseTax.Text) > FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)))
            {
                txtSubTotal.Text =
                    Strings.Format(
                        FCConvert.ToDecimal(txtExciseTax.Text) -
                        FCConvert.ToDecimal(Conversion.Val(txtTownCredit.Text)), "#,##0.00");
            }
            else
            {
                txtSubTotal.Text = Strings.Format(0, "#,##0.00");
            }

            if (FCConvert.ToDecimal(txtTransferCharge.Text) > 0)
            {
                txtBalance.Text =
                    Strings.Format(FCConvert.ToDecimal(txtSubTotal.Text) + FCConvert.ToDecimal(txtTransferCharge.Text),
                        "#,##0.00");
            }
            else
            {
                txtBalance.Text = Strings.Format(txtSubTotal.Text, "#,##0.00");
            }
        }

        public bool VerifyMonthSticker()
        {
            bool VerifyMonthSticker = false;
            VerifyMonthSticker = true;
            if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 1)
                return VerifyMonthSticker;
            if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) == 0)
                return VerifyMonthSticker;
           
            if (Conversion.Val(txtMStickerNumber.Text) == 0 || MotorVehicle.CheckNumbers(this, "MS", FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(txtMStickerNumber.Text)))) == false)
            {
                string stickerTypeDescription;

                if (txtMonthCharge.Text.ToIntegerValue() + txtMonthNoCharge.Text.ToIntegerValue() == 1)
                {
	                stickerTypeDescription = " single ";
                }
                else if (txtMonthCharge.Text.ToIntegerValue() + txtMonthNoCharge.Text.ToIntegerValue() == 2)
                {
	                stickerTypeDescription = " double ";
                }
                else
                {
	                stickerTypeDescription = "";
                }
				MessageBox.Show(
                    "There are no " + GetMonth(FCConvert.ToDateTime(txtExpires.Text).Month) +
                    stickerTypeDescription + " month stickers with this number in inventory.", "Invalid Sticker", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                VerifyMonthSticker = false;
            }
           
            return VerifyMonthSticker;
        }

        private void Get_Plate_Code(string plate)
        {
            using (clsDRWrapper rsPL = new clsDRWrapper())
            {
                rsPL.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'H' AND FormattedInventory = '" + plate + "'");
                if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
                {
                    MotorVehicle.Statics.strCodeP = FCConvert.ToString(rsPL.Get_Fields("Code"));
                }
            }
        }

        private void SetUserFields()
        {
            using (clsDRWrapper rsDefaultInfo = new clsDRWrapper())
            {
                rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
                if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel1")))
                {
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))) !=
                        "")
                    {
                        fraUserFields.Visible = true;
                        lblUserField1.Visible = true;
                        txtUserField1.Visible = true;
                        lblInfo1.Visible = true;
                        txtUserReason1.Visible = true;
                        lblUserField1.Text =
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1")));
                    }
                }

                if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel2")))
                {
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))) !=
                        "")
                    {
                        fraUserFields.Visible = true;
                        lblUserField2.Visible = true;
                        txtUserField2.Visible = true;
                        lblInfo2.Visible = true;
                        txtUserReason2.Visible = true;
                        lblUserField2.Text =
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2")));
                    }
                }

                if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel3")))
                {
                    if (fecherFoundation.Strings.Trim(
                            FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))) !=
                        "")
                    {
                        fraUserFields.Visible = true;
                        lblUserField3.Visible = true;
                        txtUserField3.Visible = true;
                        lblInfo3.Visible = true;
                        txtUserReason3.Visible = true;
                        lblUserField3.Text =
                            fecherFoundation.Strings.Trim(
                                FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3")));
                    }
                }
            }
        }

        private void FillExtraInfo()
        {
            int counter;
            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("Battle")))
            {
                if (fecherFoundation.Strings.Trim(
                        FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Battle"))) == "" ||
                    (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "VT" &&
                     FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "DS"))
                {
                    cboBattle.SelectedIndex = 0;
                }
                else
                {
                    for (counter = 0; counter <= cboBattle.Items.Count - 1; counter++)
                    {
                        if (Strings.Left(cboBattle.Items[counter].ToString(), 2) ==
                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Battle")))
                        {
                            // kk01042018 tromvs-96  Change to use BMV 2 digit code
                            cboBattle.SelectedIndex = counter;
                            break;
                        }
                    }
                }
            }
            else
            {
                cboBattle.SelectedIndex = 0;
            }

            if (MotorVehicle.Statics.Class != "VT" && MotorVehicle.Statics.Class != "DS")
            {
                cboBattle.Enabled = false;
            }
            else
            {
                cboBattle.Enabled = true;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Mail")))
            {
                cmbYes.Text = "Yes";
            }
            else
            {
                cmbYes.Text = "Yes";
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("EMailAddress")))
            {
                txtEMail.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("EMailAddress"));
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")) &&
                !fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")))
            {
                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")) > 0)
                {
                    cboMessageCodes.SelectedIndex =
                        FCConvert.ToInt32(
                            Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")) - 1);
                    txtMessage.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage"));
                }
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1")))
            {
                txtUserField1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1"));
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason1")))
            {
                txtUserReason1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason1"));
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField2")))
            {
                txtUserField2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField2"));
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason2")))
            {
                txtUserReason2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason2"));
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField3")))
            {
                txtUserField3.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField3"));
            }

            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason3")))
            {
                txtUserReason3.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason3"));
            }
        }

        public void FillSaveInfo()
        {
            if (MotorVehicle.Statics.rsSaveVehicle.IsntAnything())
            {
                // do nothing
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Reset();
            }

            MotorVehicle.Statics.rsSaveVehicle.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
            MotorVehicle.Statics.rsSaveVehicle.AddNew();
            if (MotorVehicle.Statics.NewFleet)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("FleetNew", true);
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Class", txtClass.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("plate", txtRegistrationNumber.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerNumber",
                FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtYStickerNumber.Text))));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MonthStickerNumber",
                FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtMStickerNumber.Text))));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MonthStickerMonth",
                FCConvert.ToString(Conversion.Val(lblNumberofMonth.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerYear",
                FCConvert.ToString(Conversion.Val(lblNumberofYear.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("AmputeeVet",
                chkAmputeeVet.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("FleetNumber",
                FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("CC300", chkCC.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TrailerVanity",
                chkTrailerVanity.CheckState == Wisej.Web.CheckState.Checked);
            // rsSaveVehicle.fields("Trailer")  = UCase(txtTrailerCode.Text)
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseHalfRate",
                chkHalfRateLocal.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RegHalf",
                chkHalfRateState.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseExempt",
                chkExciseExempt.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TwoYear",
                chk2Year.CheckState == Wisej.Web.CheckState.Checked);
            if (chkEAP.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("EAP", true);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("EAP", false);
            }

            if (cboBattle.Text != "")
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Battle", Strings.Left(cboBattle.Text, 2));
                // kk01042018 tromvs-96  Change to use BMV 2 digit code
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Battle", "X");
            }

            if (cmbYes.Text == "Yes")
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Mail", true);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Mail", false);
            }

            if (chkRental.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RENTAL", true);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RENTAL", false);
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("NoFeeDupReg", false);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("EMailAddress", fecherFoundation.Strings.Trim(txtEMail.Text));
            if ((cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) != "") ||
                (cboMessageCodes.SelectedIndex == -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == ""))
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MessageFlag", cboMessageCodes.SelectedIndex + 1);
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserMessage",
                    fecherFoundation.Strings.Trim(txtMessage.Text));
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserField1",
                fecherFoundation.Strings.Trim(txtUserField1.Text));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserReason1",
                fecherFoundation.Strings.Trim(txtUserReason1.Text));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserField2",
                fecherFoundation.Strings.Trim(txtUserField2.Text));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserReason2",
                fecherFoundation.Strings.Trim(txtUserReason2.Text));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserField3",
                fecherFoundation.Strings.Trim(txtUserField3.Text));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserReason3",
                fecherFoundation.Strings.Trim(txtUserReason3.Text));
            if (MotorVehicle.Statics.rsDoubleTitle.IsntAnything())
            {
                // do nothing
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DoubleCTANumber",
                    MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("CTANumber").ToUpper());
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("AgentExempt",
                chkAgentFeeExempt.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RegExempt",
                chkStateExempt.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TwoYear",
                chk2Year.CheckState == Wisej.Web.CheckState.Checked);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ETO", chkETO.CheckState == Wisej.Web.CheckState.Checked);
            if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Status", "P");
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Status", "A");
            }

            if (txtCreditNumber.Text != "")
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OldMVR3",
                    fecherFoundation.Strings.Trim(txtCreditNumber.Text));
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditMVR3",
                    fecherFoundation.Strings.Trim(txtCreditNumber.Text));
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OldMVR3", 0);
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditMVR3", 0);
            }

            if (!MotorVehicle.Statics.blnSuspended)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Address", txtAddress1.Text);
            }
            else
            {
                if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Address")))
                {
                    MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Address", "");
                }
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Address2", txtAddress2.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("City", txtCity.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("State", txtState.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Zip", txtZip.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Country", txtCountry.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Odometer",
                FCConvert.ToString(Conversion.Val(txtMileage.Text)));
            if (txtBase.Text != "")
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("BasePrice",
                    FCConvert.ToInt32(FCConvert.ToDouble(txtBase.Text)));
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("BasePrice", 0);
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MillYear",
                FCConvert.ToString(Conversion.Val(txtMilYear.Text)));
            if (!MotorVehicle.Statics.ETO)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("CTANumber",
                    fecherFoundation.Strings.Trim(txtCTANumber.Text).ToUpper());
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("CTANumber", "");
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PriorTitleNumber", txtPreviousTitle.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PriorTitleState", txtPriorTitleState.Text);
            if (MotorVehicle.Statics.RegisteringFleet &&
                MotorVehicle.Statics.PreEffectiveDate.ToOADate() > DateTime.Today.ToOADate())
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxDate",
                    Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yy"));
            }
            else
            {
                if (Information.IsDate(txtExciseTaxDate.Text) == true)
                {
                    MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxDate", txtExciseTaxDate.Text);
                }
                else
                {
                    MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxDate",
                        Strings.Format(DateTime.Now, "MM/dd/yy"));
                }
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("AgentFee",
                FCConvert.ToString(Conversion.Val(txtAgentFee.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerNoCharge",
                FCConvert.ToString(Conversion.Val(txtYearNoCharge.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MonthStickerNoCharge",
                FCConvert.ToString(Conversion.Val(txtMonthNoCharge.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerCharge",
                FCConvert.ToString(Conversion.Val(txtYearCharge.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MonthStickerCharge",
                FCConvert.ToString(Conversion.Val(txtMonthCharge.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OwnerCode1", txtReg1ICM.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OwnerCode2", txtReg2ICM.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OwnerCode3", txtReg3ICM.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("LeaseCode1", txtReg1LR.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("LeaseCode2", txtReg2LR.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("LeaseCode3", txtReg3LR.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PartyID1",
                FCConvert.ToString(Conversion.Val(txtReg1PartyID.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PartyID2",
                FCConvert.ToString(Conversion.Val(txtReg2PartyID.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PartyID3",
                FCConvert.ToString(Conversion.Val(txtReg3PartyID.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TaxIDNumber",
                fecherFoundation.Strings.Trim(txtTaxIDNumber.Text));
            if (Information.IsDate(txtReg2DOB.Text))
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DOB2", txtReg2DOB.Text);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DOB2", null);
            }

            if (Information.IsDate(txtReg1DOB.Text))
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DOB1", txtReg2DOB.Text);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DOB1", null);
            }

            if (Information.IsDate(txtReg3DOB.Text))
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DOB3", txtReg3DOB.Text);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DOB3", null);
            }

            // initialize dates not used yet
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExcisePaidDate", null);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("InsuranceInitials", txtUserID.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OpID", txtUserID.Text);
            // 
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PriorTitleNumber", txtPreviousTitle.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ResidenceCode", txtresidenceCode.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Residence", txtLegalres.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ResidenceState", txtResState.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ResidenceAddress", txtLegalResStreet.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ResidenceCountry", txtResCountry.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UnitNumber", txtUnitNumber.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DOTNumber", txtDOTNumber.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Fuel", txtFuel.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Axles", FCConvert.ToString(Conversion.Val(txtAxles.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Tires", FCConvert.ToString(Conversion.Val(txtTires.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Style", txtStyle.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("color1", txtColor1.Text.Trim());
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("color2", txtColor2.Text.Trim());
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("model", txtModel.Text.Trim());
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("make", txtMake.Text.Trim());
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Year", txtYear.Text.Trim());
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Vin", txtVin.Text.Trim());
            if (MotorVehicle.Statics.RegistrationType == "GVW")
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("registeredWeightOld",
                    MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("RegisteredWeightNew"));
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("registeredWeightNew",
                FCConvert.ToString(Conversion.Val(txtRegWeight.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("NetWeight",
                FCConvert.ToString(Conversion.Val(txtNetWeight.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditUsed", FCConvert.ToDecimal(txtTownCredit.Text));
            if (MotorVehicle.Statics.LocalHalfRate)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditFull",
                    FCConvert.ToDecimal(txtTownCredit.Text) * 2);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditFull",
                    FCConvert.ToDecimal(txtTownCredit.Text));
            }

            if (chkEAP.CheckState == Wisej.Web.CheckState.Checked)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxCharged", 0);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxCharged",
                    FCConvert.ToDecimal(txtExciseTax.Text));
            }

            if (MotorVehicle.Statics.LocalHalfRate)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxFull",
                    FCConvert.ToDecimal(txtExciseTax.Text) * 2);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtExciseTax.Text));
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTransferCharge",
                FCConvert.ToString(Conversion.Val(txtTransferCharge.Text)));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TitleFee", txtTitle.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("SalesTax", FCConvert.ToDecimal(txtSalesTax.Text));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtRate.Text));
            if (MotorVehicle.Statics.StateHalfRate)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtRate.Text) * 2);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtRate.Text));
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransferCreditUsed",
                FCConvert.ToDecimal(txtStateCredit.Text) -
                FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEECreditComm.Text));
            if (MotorVehicle.Statics.StateHalfRate)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransferCreditFull",
                    MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("TransferCreditUsed") * 2);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransferCreditFull",
                    MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("TransferCreditUsed"));
            }

            if (txtExpires.Text != "")
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExpireDate", txtExpires.Text);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExpireDate", null);
            }

            if (txtEffectiveDate.Text != "")
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("EffectiveDate", txtEffectiveDate.Text);
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("EffectiveDate", null);
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("CommercialCredit",
                frmFeesInfo.InstancePtr.txtFEECreditComm.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("InitialFee",
                frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("outofrotation",
                frmFeesInfo.InstancePtr.txtFEEOutOfRotation.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ReservePlate",
                frmFeesInfo.InstancePtr.txtFEEReservePlate.Text);
            if (MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ReservePlate") > 0)
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ReservePlateYN", -1);
            }

            if (Conversion.Val(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text) != 0)
            {
                if (MotorVehicle.Statics.NewPlate == true)
                {
                    MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeReReg", 0);
                    MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeNew",
                        FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                }
                else
                {
                    MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeNew", 0);
                    MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeReReg",
                        FCConvert.ToDecimal(frmFeesInfo.InstancePtr.txtFEEPlateClass.Text));
                }
            }
            else
            {
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeNew", 0);
                MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeReReg", 0);
            }

            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ReplacementFee",
                frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("StickerFee", frmFeesInfo.InstancePtr.txtFEEStickerFee.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransferFee",
                frmFeesInfo.InstancePtr.txtFEETransferFee.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("GIFTCERTIFICATEAMOUNT",
                frmFeesInfo.InstancePtr.txtFEEGiftCertificate.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("GiftCertificateNumber",
                frmFeesInfo.InstancePtr.txtGCNumber.Text);
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("StatePaid",
                FCConvert.ToDecimal(txtFees.Text) + MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("SalesTax") +
                MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("TitleFee"));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("LocalPaid",
                FCConvert.ToDecimal(txtBalance.Text) + FCConvert.ToDecimal(txtAgentFee.Text));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ForcedPlate", MotorVehicle.Statics.ForcedPlate);
            // kk05122016
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("NoFeeCTA",
                FCConvert.CBool(chkNoFeeCTA.CheckState == Wisej.Web.CheckState.Checked));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DealerSalesTax",
                FCConvert.CBool(chkDealerSalesTax.CheckState == Wisej.Web.CheckState.Checked));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("BaseIsSalePrice",
                FCConvert.CBool(chkBaseIsSalePrice.CheckState == Wisej.Web.CheckState.Checked));
            MotorVehicle.Statics.rsSaveVehicle.Set_Fields("SalesTaxExempt",
                FCConvert.CBool(chkSalesTaxExempt.CheckState == Wisej.Web.CheckState.Checked));
        }

        public void FillSavedInfo()
        {
            string strTemp = "";
            lblNumberofMonth.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MonthStickerMonth"));
            lblNumberofYear.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("YearStickerYear"));
            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("AmputeeVet")))
            {
                chkAmputeeVet.CheckState = Wisej.Web.CheckState.Checked;
            }

            txtFleetNumber.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("FleetNumber"));
            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("CC300")))
            {
                chkCC.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("TrailerVanity")))
            {
                chkTrailerVanity.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("ExciseHalfRate")))
            {
                chkHalfRateLocal.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("EAP")))
            {
                chkEAP.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("RegHalf")))
            {
                chkHalfRateState.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("RENTAL")))
            {
                chkRental.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("TwoYear")))
            {
                chk2Year.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chk2Year.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("ExciseExempt")))
            {
                chkExciseExempt.CheckState = Wisej.Web.CheckState.Checked;
                if (MotorVehicle.Statics.RegistrationType != "CORR")
                {
                    chkExciseExempt.Enabled = false;
                }
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("AgentExempt")))
            {
                chkAgentFeeExempt.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("RegExempt")))
            {
                chkStateExempt.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("TwoYear")))
            {
                chk2Year.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("ETO")))
            {
                chkETO.CheckState = Wisej.Web.CheckState.Checked;
            }

            if (!MotorVehicle.Statics.blnSuspended)
            {
                txtAddress1.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Address"));
            }
            else
            {
                txtAddress1.Text = "REGISTRATION SUSPENDED";
            }

            txtAddress2.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Address2"));
            txtCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("City"));
            txtState.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("State"));
            txtZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Zip"));
            txtCountry.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Country")).Length <= 2 ?
	            FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Country")) :
	            "";
            txtReg1PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID1"));
            GetPartyInfo(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID1"), 1);
            if (Conversion.Val(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID2")) != 0)
            {
                txtReg2PartyID.Text =
                    FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID2"));
                GetPartyInfo(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID2"), 2);
            }
            else
            {
                ClearPartyInfo(2);
            }

            if (Conversion.Val(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID3")) != 0)
            {
                txtReg3PartyID.Text =
                    FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID3"));
                GetPartyInfo(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID3"), 3);
            }
            else
            {
                ClearPartyInfo(3);
            }

            txtMileage.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("Odometer"));
            
            var muniName = fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName);
            if (MotorVehicle.Statics.GroupOrFleet != "G" ||
                (muniName == "MAINE MOTOR TRANSPORT" ||
                 muniName == "AB LEDUE ENTERPRISES" ||
                 muniName == "STAAB AGENCY" ||
                 muniName == "COUNTRYWIDE TRAILER" || 
                 muniName == "HASKELL REGISTRATION"))
            {
                txtBase.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("BasePrice"),
                    "#,##0");
                txtCTANumber.Text =
                    FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("CTANumber"));
                if (FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Fuel")) != "N")
                {
                    txtFuel.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Fuel"));
                }
                else
                {
                    txtFuel.Text = "";
                }

                if (Conversion.Val(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("Axles")) != 0)
                {
                    txtAxles.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("Axles"));
                }
                else
                {
                    txtAxles.Text = "";
                }

                if (Conversion.Val(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("Tires")) != 0)
                {
                    txtTires.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("Tires"));
                }
                else
                {
                    txtTires.Text = "";
                }

                txtStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Style").Trim());
                txtColor1.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("color1").Trim());
                txtColor2.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("color2").Trim());
                string model = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("model").Trim());
				txtModel.Text = model.Trim().Length <= 6 ? model.Trim() : model.Trim().Left(6);
				txtMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("make").Trim());
                txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields("Year"));
                strTemp = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Vin").Trim());
                if (strTemp.Length == 17)
                {
                    strTemp = Strings.Left(strTemp, 8) + "_" + Strings.Mid(strTemp, 10, 6) + "_";
                    txtVin.Text = strTemp;
                }
            }

            txtMilYear.Text =
                FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MillYear")));
            if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CL" &&
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "C5") ||
                (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" &&
                 FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "L6"))
            {
                if (Conversion.Val(txtMilYear.Text) == 1)
                {
                    txtMilRate.Text = ".0250";
                }
                else if (Conversion.Val(txtMilYear.Text) == 2)
                {
                    txtMilRate.Text = ".0200";
                }
                else if (Conversion.Val(txtMilYear.Text) == 3)
                {
                    txtMilRate.Text = ".0160";
                }
                else if (Conversion.Val(txtMilYear.Text) == 4)
                {
                    txtMilRate.Text = ".0120";
                }
                else if (Conversion.Val(txtMilYear.Text) == 5)
                {
                    txtMilRate.Text = ".0120";
                }
                else if (Conversion.Val(txtMilYear.Text) == 6)
                {
                    txtMilRate.Text = ".0120";
                }
            }
            else
            {
                if (Conversion.Val(txtMilYear.Text) == 1)
                {
                    txtMilRate.Text = ".0240";
                }
                else if (Conversion.Val(txtMilYear.Text) == 2)
                {
                    txtMilRate.Text = ".0175";
                }
                else if (Conversion.Val(txtMilYear.Text) == 3)
                {
                    txtMilRate.Text = ".0135";
                }
                else if (Conversion.Val(txtMilYear.Text) == 4)
                {
                    txtMilRate.Text = ".0100";
                }
                else if (Conversion.Val(txtMilYear.Text) == 5)
                {
                    txtMilRate.Text = ".0065";
                }
                else if (Conversion.Val(txtMilYear.Text) == 6)
                {
                    txtMilRate.Text = ".0040";
                }
            }

            txtExciseTaxDate.Text =
                Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields("ExciseTaxDate"), "MM/dd/yy");
            txtAgentFee.Text =
                Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("AgentFee"), "#0.00");
            txtYearNoCharge.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("YearStickerNoCharge"));
            txtMonthNoCharge.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MonthStickerNoCharge"));
            txtYearCharge.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("YearStickerCharge"));
            txtMonthCharge.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MonthStickerCharge"));
            //if (FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB2")) != "12:00:00 AM" && fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB2")) != true)
            if (fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB2")) !=
                true && MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB2") != DateTime.FromOADate(0))
            {
                txtReg2DOB.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB2"),
                    "MM/dd/yyyy");
            }

            txtUserID.Text = MotorVehicle.Statics.UserID;
            if (FCConvert.ToInt32(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("PriorTitleNumber")) == 0)
            {
                txtPreviousTitle.Text = "";
            }
            else
            {
                txtPreviousTitle.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("PriorTitleNumber");
                txtPriorTitleState.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("PriorTitleState");
            }

            MotorVehicle.Statics.OpID = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("OpID");
            if (MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ResidenceCode").Length < 5)
            {
                txtresidenceCode.Text =
                    Strings.StrDup(5 - MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ResidenceCode").Length,
                        "0") + MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ResidenceCode");
            }
            else
            {
                txtresidenceCode.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ResidenceCode");
            }

            txtResState.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ResidenceState");
            txtUnitNumber.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UnitNumber");
            txtDOTNumber.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("DOTNumber");
            if (txtDOTNumber.Text == "0")
                txtDOTNumber.Text = "";
            //if (FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB1")) != "12:00:00 AM" && fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB1")) != true)
            if (fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB1")) !=
                true && MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB1") != DateTime.FromOADate(0))
            {
                txtReg1DOB.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("DOB1"),
                    "MM/dd/yyyy");
            }

            txtTaxIDNumber.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("TaxIDNumber");
            txtMilYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MillYear"));
            txtRegWeight.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("RegisteredWeightNew"));
            txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields("NetWeight"));
            txtTownCredit.Text =
                Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseCreditUsed"), "#,##0.00");
            txtExciseTax.Text =
                Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseTaxCharged"), "#,##0.00");
            txtTransferCharge.Text =
                Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseTransferCharge"),
                    "#,##0.00");
            txtTitle.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("TitleFee"),
                "#,##0.00");
            txtSalesTax.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("SalesTax"),
                "#,##0.00");
            txtRate.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("RegRateCharge"),
                "#,##0.00");
            txtStateCredit.Text =
                Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("TransferCreditUsed"), "#,##0.00");
            txtCreditNumber.Text =
                FCConvert.ToString(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("ExciseCreditMVR3"));
            if (MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("ETO") != true)
            {
                txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields("ExpireDate"),
                    "MM/dd/yyyy");
                txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields("EffectiveDate"),
                    "MM/dd/yyyy");
            }
            else
            {
                if (Information.IsDate(MotorVehicle.Statics.rsSaveVehicle.Get_Fields("ExpireDate")))
                {
                    txtExpires.Text =
                        Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("ExpireDate"),
                            "MM/dd/yyyy");
                }

                if (Information.IsDate(MotorVehicle.Statics.rsSaveVehicle.Get_Fields("EffectiveDate")))
                {
                    txtEffectiveDate.Text =
                        Strings.Format(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("EffectiveDate"),
                            "MM/dd/yyyy");
                }
            }

            txtReg1ICM.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("OwnerCode1");
            txtReg2ICM.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("OwnerCode2");
            txtReg3ICM.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("OwnerCode3");
            txtReg1LR.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("LeaseCode1");
            txtReg2LR.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("LeaseCode2");
            txtReg3LR.Text = MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("LeaseCode3");
            if (MotorVehicle.Statics.GroupOrFleet == "G")
            {
                SetFleetGroupType(FleetGroupType.Group);
            }
            else
            {
                SetFleetGroupType(FleetGroupType.Fleet);
            }

            // kk05122016
            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("NoFeeCTA")))
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("DealerSalesTax")))
            {
                chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("BaseIsSalePrice")))
            {
                chkBaseIsSalePrice.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkBaseIsSalePrice.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("SalesTaxExempt")))
            {
                // kk07222016
                chkSalesTaxExempt.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkSalesTaxExempt.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            txtVin.Focus();
            /*? On Error GoTo 0 */
        }

        private bool CheckTruckDecal()
        {
            using (clsDRWrapper rs = new clsDRWrapper())
            {
                rs.OpenRecordset("SELECT * FROM Inventory WHERE Number = " + txtRegistrationNumber.Text +
                                 " AND Code = 'DYS" +
                                 Strings.Right(
                                     FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtExpires.Text).Year), 2) +
                                 "'");
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool CheckIslandUse()
        {
            using (clsDRWrapper rs = new clsDRWrapper())
            {
                rs.OpenRecordset("SELECT * FROM Inventory WHERE Number = " +
                             FCConvert.ToString(Conversion.Val(txtRegistrationNumber.Text)) + " AND Code = 'SYD" +
                             Strings.Right(
                                 FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtExpires.Text).Year), 2) +
                             "'");
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool VerifyBattle()
        {
            bool VerifyBattle = false;
            VerifyBattle = true;
            if (cboBattle.SelectedIndex != 0 && cboBattle.SelectedIndex != -1)
            {
                if (MotorVehicle.Statics.Class != "VT" && MotorVehicle.Statics.Class != "DS")
                {
                    MessageBox.Show("You may not have a Veteran Decal on this class of Vehicle.", "Invalid Decal",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboBattle.Focus();
                    VerifyBattle = false;
                }
            }

            return VerifyBattle;
        }

        public bool VerifyMil()
        {
            bool VerifyMil = false;
            // vbPorter upgrade warning: ValidYear As int	OnWriteFCConvert.ToInt32(
            int ValidYear;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            VerifyMil = true;
            ValidYear = (DateTime.Today.Year - FCConvert.ToInt32(txtYear.Text));
            if (chkExciseExempt.CheckState != Wisej.Web.CheckState.Checked)
            {
                if ((Conversion.Val(txtMilYear.Text) < ValidYear && Conversion.Val(txtMilYear.Text) != 6) ||
                    ValidYear + 3 < Conversion.Val(txtMilYear.Text))
                {
                    ans = MessageBox.Show("Are you sure this is the Mil Rate you wish to use?", "Check Mil Rate",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.Yes)
                    {
                        return VerifyMil;
                    }
                    else
                    {
                        VerifyMil = false;
                        txtMilYear.TabStop = true;
                        txtMilYear.Focus();
                    }
                }
            }

            return VerifyMil;
        }

        public bool VerifySalesTax()
        {
            bool VerifySalesTax = false;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            VerifySalesTax = true;
            if (chkSalesTaxExempt.CheckState != Wisej.Web.CheckState.Checked &&
                chkDealerSalesTax.CheckState != Wisej.Web.CheckState.Checked)
            {
                if (Conversion.Val(txtSalesTax.Text) == 0)
                {
                    MessageBox.Show("Sales tax information is required for all new registrations. " +
                        " Please fill out a sales tax form, select a sales tax option or enter a sales tax amount.",
                        "Missing Sales Tax", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    VerifySalesTax = false;
                    txtSalesTax.Focus();
                }
            }

            return VerifySalesTax;
        }

        public bool VerifyTitleFee()
        {
            bool VerifyTitleFee = false;
            // vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
            DialogResult ans = 0;
            VerifyTitleFee = true;
            if (chkNoFeeCTA.CheckState != Wisej.Web.CheckState.Checked &&
                chkCTAPaid.CheckState != Wisej.Web.CheckState.Checked)
            {
                if (Conversion.Val(txtTitle.Text) == 0 && txtCTANumber.Text.Length > 2)
                {
                    // kk03142017 tromv-1265  Use different prompt for Correction when prev reg had CTA
                    if (MotorVehicle.Statics.RegistrationType != "CORR" &&
                        MotorVehicle.Statics.RegistrationType != "DUPREG")
                    {
                        ans = MessageBox.Show("No title fee was entered. Was the title fee paid elsewhere or exempt?",
                            "Confirm Title Fee", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.No)
                        {
                            return VerifyTitleFee;
                        }
                        else
                        {
                            VerifyTitleFee = false;
                            chkNoFeeCTA.Focus();
                        }
                    }
                    else
                    {
                        ans = MessageBox.Show(
                            "The original registration did not include the title fee. Was the title fee paid elsewhere or exempt?",
                            "Confirm Title Fee", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.No)
                        {
                            return VerifyTitleFee;
                        }
                        else
                        {
                            VerifyTitleFee = false;
                            chkNoFeeCTA.Focus();
                        }
                    }
                }
            }

            // kk05122017 tromv-1265  Add a second prompt with instructions
            if (!VerifyTitleFee)
            {
                MessageBox.Show("Please select the appropriate Title Fee checkbox before continuing.", "Title Fee",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return VerifyTitleFee;
        }

        private void SetCustomFormColors()
        {
            lblCalculate.ForeColor = Color.White;
            lblCalculate.BackColor = ColorTranslator.FromOle(0xC0);
            lblTotalDue.BackColor = ColorTranslator.FromOle(0x800000);
            lblTotalDue.ForeColor = Color.MediumSeaGreen;
            chkVIN.BackColor = ColorTranslator.FromOle(0x800000);
            chkVIN.ForeColor = Color.White;
            Label35.ForeColor = Color.Blue;
            lblTitle.ForeColor = Color.Blue;
            Label46.ForeColor = Color.Blue;
            lblExciseTax.ForeColor = Color.Blue;
            lblStateCredit.ForeColor = Color.Blue;
            lblFees.ForeColor = Color.Blue;
            lblUseTax.ForeColor = Color.Blue;
            Label10.BackColor = Color.White;
            Label11.BackColor = Color.White;
            Label12.BackColor = Color.White;
            Label13.BackColor = Color.White;
            Label14.BackColor = Color.White;
            Label15.BackColor = Color.White;
            Label16.BackColor = Color.White;
            Label17.BackColor = Color.White;
            Label18.BackColor = Color.White;
            Label19.BackColor = Color.White;
            Label20.BackColor = Color.White;
            Label25.BackColor = Color.White;
            Label26.BackColor = Color.White;
            Label27.BackColor = Color.White;
            Label28.BackColor = Color.White;
            Label29.BackColor = Color.White;
            Label22.BackColor = Color.White;
            Label24.BackColor = Color.White;
            Label38.BackColor = Color.White;
            Label47.BackColor = Color.White;
            Label48.BackColor = Color.White;
            lblTaxID.BackColor = Color.White;
            Label53.BackColor = Color.White;
            lblExciseTaxHR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblTownCreditHR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblLocalSubHR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblLocalBalanceHR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblStateRateHR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblStateCreditHR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblStateFeesHR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblExciseTaxPR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblTownCreditPR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblLocalSubPR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblLocalBalancePR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblStateRatePR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblStateCreditPR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            lblStateFeesPR.BackColor = ColorTranslator.FromOle(0xC0FFFF);
            Label52.BackColor = Color.White;
            chkCC.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            lblBattle.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkRental.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkTrailerVanity.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkAmputeeVet.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkTransferExempt.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkExciseExempt.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkAgentFeeExempt.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkStateExempt.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkEAP.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkHalfRateLocal.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkHalfRateState.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            fraHalfRate.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkETO.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chk2Year.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkBaseIsSalePrice.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            fraTitleFee.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkCTAPaid.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkNoFeeCTA.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            fraSalesTax.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkDealerSalesTax.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            chkSalesTaxExempt.BackColor = ColorTranslator.FromOle(0xE0E0E0);
            lblExciseDiff.BackColor = ColorTranslator.FromOle(0x800000);
            lblCreditAP.BackColor = ColorTranslator.FromOle(0x800000);
            lblCreditRebate.BackColor = ColorTranslator.FromOle(0x800000);
            lblSubTotalAP.BackColor = ColorTranslator.FromOle(0x800000);
            lblTransferAP.BackColor = ColorTranslator.FromOle(0x800000);
            lblBalanceAP.BackColor = ColorTranslator.FromOle(0x800000);
            lblExciseAP.BackColor = ColorTranslator.FromOle(0x800000);
            lblCreditNumberAP.BackColor = ColorTranslator.FromOle(0x800000);
            //lblDateAP.BackColor = ColorTranslator.FromOle(0x800000);
            lblExciseDiff.ForeColor = Color.White;
            lblCreditAP.ForeColor = Color.White;
            lblCreditRebate.ForeColor = Color.White;
            lblSubTotalAP.ForeColor = Color.White;
            lblTransferAP.ForeColor = Color.White;
            lblBalanceAP.ForeColor = Color.White;
            lblCreditNumberAP.ForeColor = Color.White;
            //lblDateAP.ForeColor = Color.White;
            lblExciseAP.ForeColor = Color.White;
            txtReg3ICM.BackColor = ColorTranslator.FromOle(0xC0E0FF);
            txtReg1ICM.BackColor = ColorTranslator.FromOle(0xC0E0FF);
            txtReg2ICM.BackColor = ColorTranslator.FromOle(0xC0E0FF);
            txtReg3LR.BackColor = ColorTranslator.FromOle(0xC0E0FF);
            txtReg1LR.BackColor = ColorTranslator.FromOle(0xC0E0FF);
            txtReg2LR.BackColor = ColorTranslator.FromOle(0xC0E0FF);
            txtEffectiveDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtExpires.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtMileage.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtBase.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtMilYear.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtMilRate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtAgentFee.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtExciseTax.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtTownCredit.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtTransferCharge.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtBalance.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtSubTotal.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtExciseTaxDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtCreditNumber.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtRate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtStateCredit.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtFees.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtCTANumber.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtDoubleCTANumber.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtUserID.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtSalesTax.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtTitle.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtPreviousTitle.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            txtPriorTitleState.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
            lblCalculate.Font = FCUtils.FontChangeSize(lblCalculate.Font, 10);
            lblTotalDue.Font = FCUtils.FontChangeSize(lblTotalDue.Font, 12);
        }

        private void SearchForParty(int registrantPosition)
        {
            if (registrantPosition < 1 || registrantPosition > 3) throw new ArgumentException();

            int lngID = frmCentralPartySearch.InstancePtr.Init();

            if (lngID > 0)
            {
                string idString = FCConvert.ToString(lngID);
                switch (registrantPosition)
                {
                    case 1:
                        txtReg1PartyID.Text = idString;
                        GetPartyInfo(lngID, registrantPosition, CentralPartyNameAddressCheck.UseCentralPartyInfo);
                        if (txtReg1ICM.Text == "I")
                        {
                            txtReg1DOB.Focus();
                        }
                        break;
                    case 2:
                        txtReg2PartyID.Text = idString;
                        GetPartyInfo(lngID, registrantPosition, CentralPartyNameAddressCheck.UseCentralPartyInfo);
                        if (txtReg2ICM.Text == "I")
                        {
                            txtReg2DOB.Focus();
                        }
                        break;
                    case 3:
                        txtReg3PartyID.Text = idString;
                        GetPartyInfo(lngID, registrantPosition, CentralPartyNameAddressCheck.UseCentralPartyInfo);
                        if (txtReg3ICM.Text == "I")
                        {
                            txtReg3DOB.Focus();
                        }
                        break;
                }
                
            }
            else
            {
                switch (registrantPosition)
                {
                    case 1:
                        ValidateReg1PartyID();
                        break;
                    case 2:
                        ValidateReg2PartyID();
                        break;
                    case 3:
                        ValidateReg3PartyID();
                        break;
                }
            }
        }

        private void txtReg1PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtReg1PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ValidateReg1PartyID();
        }

        private void ValidateReg1PartyID()
        {
            if (Information.IsNumeric(txtReg1PartyID.Text) && Conversion.Val(txtReg1PartyID.Text) > 0)
            {
                GetPartyInfo(txtReg1PartyID.Text.ToIntegerValue(), 1, MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID1") == txtReg1PartyID.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ClearPartyInfo(1);
            }
        }

        private void txtReg2PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtReg2PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ValidateReg2PartyID();
        }

        private void ValidateReg2PartyID()
        {
            if (Information.IsNumeric(txtReg2PartyID.Text) && Conversion.Val(txtReg2PartyID.Text) > 0)
            {
                GetPartyInfo(txtReg2PartyID.Text.ToIntegerValue(), 2, MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2") == txtReg2PartyID.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ClearPartyInfo(2);
            }
        }

        private void txtReg3PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtReg3PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ValidateReg3PartyID();
        }

        private void ValidateReg3PartyID()
        {
            if (Information.IsNumeric(txtReg3PartyID.Text) && Conversion.Val(txtReg3PartyID.Text) > 0)
            {
                GetPartyInfo(txtReg3PartyID.Text.ToIntegerValue(), 3, MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3") == txtReg3PartyID.Text.ToIntegerValue() ? CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }
            else
            {
                ClearPartyInfo(3);
            }
        }

        public void ClearPartyInfo(int intReg)
        {
            switch (intReg)
            {
                case 1:
                    {
                        txtRegistrant1.Text = "";
                        txtReg1DOB.Text = "";
                        imgReg1Edit.Enabled = false;
                        imgEditRegistrant1.Enabled = false;
                        imgReg1Search.Enabled =txtReg1ICM != "N";
                        txtReg1PartyID.Enabled = false;
                        strReg1FirstName = "";
                        strReg1LastName = "";
                        strReg1MI = "";
                        strReg1Designation = "";
                        break;
                    }
                case 2:
                    {
                        txtRegistrant2.Text = "";
                        txtReg2DOB.Text = "";
                        imgReg2Edit.Enabled = false;
                        imgEditRegistrant2.Enabled = false;
                        imgReg2Search.Enabled = txtReg2ICM != "N";
                        txtReg2PartyID.Enabled = false;
                        strReg2FirstName = "";
                        strReg2LastName = "";
                        strReg2MI = "";
                        strReg2Designation = "";
                        break;
                    }
                case 3:
                    {
                        txtRegistrant3.Text = "";
                        txtReg3DOB.Text = "";
                        imgReg3Edit.Enabled = false;
                        imgEditRegistrant3.Enabled = false;
                        imgReg3Search.Enabled = txtReg3ICM != "N";
                        txtReg3PartyID.Enabled = false;
                        strReg3FirstName = "";
                        strReg3LastName = "";
                        strReg3MI = "";
                        strReg3Designation = "";
                        break;
                    }
                default:
                    {
                        // do nothing
                        break;
                    }
            }

            //end switch
        }

        public bool GetPartyInfo(int intPartyID, int intReg, CentralPartyNameAddressCheck ignoreName = CentralPartyNameAddressCheck.DoNotUseCentralPartyInfo)
        {
            cPartyController pCont = new cPartyController();
            cParty pInfo;
            cPartyAddress pAdd;
            FCCollection pComments = new FCCollection();
            pInfo = pCont.GetParty(intPartyID);

            if (!(pInfo == null))
            {
                pAdd = pInfo.GetPrimaryAddress();

                switch (intReg)
                {
                    case 1:
                        {

                            imgReg1Edit.Enabled = true;
                            imgEditRegistrant1.Enabled = true;

                            bool updateInfo = false;
                            if (ignoreName == CentralPartyNameAddressCheck.UseCentralPartyInfo)
                            {
                                updateInfo = true;
                            }
                            else if (ignoreName == CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent)
                            {
                                if (txtRegistrant1.Text.ToUpper() != pInfo.FullNameMiddleInitial.ToUpper()
                                    || ((txtReg1LR != "R") && pAdd != null && (pAdd.Address1.ToUpper() != txtAddress1.Text.ToUpper()
                                    || pAdd.Address2.ToUpper() != txtAddress2.Text.ToUpper()
                                    || pAdd.City.ToUpper() != txtCity.Text.ToUpper()
                                    || pAdd.State.ToUpper() != txtState.Text.ToUpper()
                                    || pAdd.Zip.ToUpper() != txtZip.Text.ToUpper())))
                                {
                                    updateInfo = (MessageBox.Show(
                                                      "Name and / or address on the central party record is different than the information on the registration.  Would you like to update the registration with the information from the party?",
                                                      "Update Registration Information?", MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question) == DialogResult.Yes);
                                }
                            }
                            else
                            {
                                if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1") == "I" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1FirstName") == "") ||
                                    (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1") != "I" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Owner1") == ""))
                                {
                                    updateInfo = true;
                                }
                                else if (txtRegistrant1.Text == "")
                                {
                                    if (MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName1") == "")
                                    {
                                        updateInfo = true;
                                    }
                                    else
                                    {
                                        txtRegistrant1.Text =
                                            MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName1").ToUpper();
                                        strReg1FirstName = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1FirstName").ToUpper();
                                        strReg1LastName = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1LastName").ToUpper();
                                        strReg1MI = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1MI").ToUpper();
                                        strReg1Designation = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg1Designation").ToUpper();
                                    }
                                }
                            }

                            if (updateInfo)
                            {
                                txtRegistrant1.Text = fecherFoundation.Strings.UCase(pInfo.FullNameMiddleInitial);
                                strReg1FirstName = Strings.UCase(pInfo.FirstName);
                                strReg1LastName = Strings.UCase(pInfo.LastName);
                                strReg1MI = pInfo.MiddleName.Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper();
                                strReg1Designation = Strings.UCase(pInfo.Designation);

                                if (txtReg1LR != "R" && pAdd != null)
                                {
                                    FillAddressFields(intPartyID);
                                }
                            }

                            break;
                        }
                    case 2:
                        {

                            imgReg2Edit.Enabled = true;
                            imgEditRegistrant2.Enabled = true;

                            
                            bool updateInfo = false;
                            if (ignoreName == CentralPartyNameAddressCheck.UseCentralPartyInfo)
                            {
                                updateInfo = true;
                            }
                            else if (ignoreName == CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent)
                            {
                                if (txtRegistrant2.Text.ToUpper() != pInfo.FullNameMiddleInitial.ToUpper()
                                   || ((txtReg1LR == "R") && pAdd != null && (pAdd.Address1.ToUpper() != txtAddress1.Text.ToUpper()
                                   || pAdd.Address2.ToUpper() != txtAddress2.Text.ToUpper()
                                   || pAdd.City.ToUpper() != txtCity.Text.ToUpper()
                                   || pAdd.State.ToUpper() != txtState.Text.ToUpper()
                                   || pAdd.Zip.ToUpper() != txtZip.Text.ToUpper())))
                                {
                                    updateInfo = (MessageBox.Show(
                                                      "Name and / or address on the central party record is different than the information on the registration.  Would you like to update the registration with the information from the party?",
                                                      "Update Registration Information?", MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question) == DialogResult.Yes);
                                }
                            }
                            else
                            {
                                if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2") == "I" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2FirstName") == "") ||
                                    (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2") != "I" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Owner2") == ""))
                                {
                                    updateInfo = true;
                                }
                                else if (txtRegistrant2.Text == "")
                                {
                                    if (MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName2") == "")
                                    {
                                        updateInfo = true;
                                    }
                                    else
                                    {
                                        txtRegistrant2.Text =
                                            MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName2");
                                        strReg2FirstName = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2FirstName").ToUpper();
                                        strReg2LastName = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2LastName").ToUpper();
                                        strReg2MI = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2MI").ToUpper();
                                        strReg2Designation = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg2Designation").ToUpper();
                                    }
                                }
                            }

                            if (updateInfo)
                            {

                                txtRegistrant2.Text = fecherFoundation.Strings.UCase(pInfo.FullNameMiddleInitial);
                                strReg2FirstName = Strings.UCase(pInfo.FirstName);
                                strReg2LastName = Strings.UCase(pInfo.LastName);
                                strReg2MI = pInfo.MiddleName.Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper();
                                strReg2Designation = Strings.UCase(pInfo.Designation);

                                if (txtReg1LR == "R" && pAdd != null)
                                {
                                    FillAddressFields(intPartyID);
                                }
                            }
                            
                            break;
                        }
                    case 3:
                        {
                            imgReg3Edit.Enabled = true;
                            imgEditRegistrant3.Enabled = true;

                            bool updateInfo = false;
                            if (ignoreName == CentralPartyNameAddressCheck.UseCentralPartyInfo)
                            {
                                updateInfo = true;
                            }
                            else if (ignoreName == CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent)
                            {
                                if (txtRegistrant3.Text.ToUpper() != pInfo.FullNameMiddleInitial.ToUpper())
                                {
                                    updateInfo = (MessageBox.Show(
                                                      "Name and / or address on the central party record is different than the information on the registration.  Would you like to update the registration with the information from the party?",
                                                      "Update Registration Information?", MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question) == DialogResult.Yes);
                                }
                            }
                            else
                            {
                                if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3") == "I" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3FirstName") == "") ||
                                    (MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3") != "I" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Owner3") == ""))
                                {
                                    updateInfo = true;
                                }
                                else if (txtRegistrant3.Text == "")
                                {
                                    if (MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName3") == "")
                                    {
                                        updateInfo = true;
                                    }
                                    else
                                    {
                                        txtRegistrant3.Text =
                                            MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName3");
                                        strReg3FirstName = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3FirstName").ToUpper();
                                        strReg3LastName = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3LastName").ToUpper();
                                        strReg3MI = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3MI").ToUpper();
                                        strReg3Designation = MotorVehicle.Statics.rsFinal.Get_Fields_String("Reg3Designation").ToUpper();
                                    }
                                }
                            }

                            if (updateInfo)
                            {
                                txtRegistrant3.Text = fecherFoundation.Strings.UCase(pInfo.FullNameMiddleInitial);
                                strReg3FirstName = Strings.UCase(pInfo.FirstName);
                                strReg3LastName = Strings.UCase(pInfo.LastName);
                                strReg3MI = pInfo.MiddleName.Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper();
                                strReg3Designation = Strings.UCase(pInfo.Designation);
                            }
                            
                            break;
                        }
                }
            }

            return false;
        }

        private bool ValidPossibleYearForVIN(int lowYear, int enteredYear)
        {
	        int yearToCheck = lowYear;

	        do
	        {
		        if (yearToCheck == enteredYear)
		        {
			        return true;
		        }
		        else
		        {
			        yearToCheck += 30;
		        }

	        } while (yearToCheck <= enteredYear);

	        return false;
        }
        private bool VerifyYearFromVinIsValid(int year, string vin)
        {
			int intYear = intYear = ReturnOldestPossibleVINYear(vin);
			
			if (intYear > 0)
			{
				return ValidPossibleYearForVIN(intYear, year);
			}
			else
			{
				return false;
			}
		}

        private int ReturnOldestPossibleVINYear(string strVin)
        {
	        int intYear = 0;

			if (fecherFoundation.Strings.Trim(strVin) != "" || strVin.Length >= 17)
			{
				var yearIndicator = Strings.Mid(strVin, 10, 1);

				switch (yearIndicator)
				{
					// 1980/2010
					case "A":
						intYear = 1980;

						break;

					// 1981/2011
					case "B":
						intYear = 1981;

						break;

					// 1982/2012
					case "C":
						intYear = 1982;

						break;

					// 1983/2013
					case "D":
						intYear = 1983;

						break;

					// 1984/2014
					case "E":
						intYear = 1984;

						break;
					case "F":

						// 1985/2015
						intYear = 1985;

						break;
					case "G":

						// 1986/2016
						intYear = 1986;

						break;
					case "H":

						// 1987/2017
						intYear = 1987;

						break;
					case "J":

						// 1988/2018
						intYear = 1988;

						break;
					case "K":

						// 1989/2019
						intYear = 1989;

						break;
					case "L":

						// 1990/2020
						intYear = 1990;

						break;
					case "M":

						// 1991/2021
						intYear = 1991;

						break;
					case "N":

						// 1992/2022
						intYear = 1992;

						break;
					case "P":

						// 1993/2023
						intYear = 1993;

						break;
					case "R":

						// 1994/2024
						intYear = 1994;

						break;
					case "S":

						// 1995/2025
						intYear = 1995;

						break;
					case "T":

						// 1996/2026
						intYear = 1996;

						break;
					case "V":

						// 1997/2027
						intYear = 1997;

						break;
					case "W":

						// 1998/2028
						intYear = 1998;

						break;
					case "X":

						// 1999/2029
						intYear = 1999;

						break;
					case "Y":

						// 2000/2030
						intYear = 2000;

						break;
					case "1":

						// 2001/2031
						intYear = 2001;

						break;
					case "2":

						// 2002/2032
						intYear = 2002;

						break;
					case "3":

						// 2003/2033
						intYear = 2003;

						break;
					case "4":

						// 2004/2034
						intYear = 2004;

						break;
					case "5":

						// 2005/2035
						intYear = 2005;

						break;
					case "6":

						// 2006/2036
						intYear = 2006;

						break;
					case "7":

						// 2007/2037
						intYear = 2007;

						break;
					case "8":

						// 2008/2038
						intYear = 2008;

						break;
					case "9":

						// 2009/2039
						intYear = 2009;

						break;
				}
			}

			return intYear;
        }
        private string GetYearFromVin(string strVin)
        {
            string GetYearFromVin = "";
            // Choose correct Model Year based on the 7th and 10th char of the VIN
            // If char 7 is a number, char 10 is 1980-2009
            // If it is a letter, char to is 2010-2039
            int intYear;
            intYear = ReturnOldestPossibleVINYear(strVin);

			if (intYear > 0)
			{
				// kk05122016  Check for year way in the future
				if (intYear + 30 <= DateTime.Now.Year + 2)
				{
					intYear += 30;
				}
			}

			if (intYear > 0)
            {
                GetYearFromVin = Strings.Format(intYear, "0000");
            }
            else
            {
                GetYearFromVin = "";
            }

            return GetYearFromVin;
        }

        private void ConfigExciseTaxOnly()
        {
            // kk01022017 tromv-1252  Copied some code from CheckETO_Click since running the handler is disabled during loading now
            // Specifically we need to disable the sticker number fields when an ETO is loaded
            txtMonthCharge.Text = "";
            txtMonthCharge.Enabled = false;
            txtMonthCharge.BackColor = ColorTranslator.FromOle(OldColor);
            txtMonthNoCharge.Text = "";
            txtMonthNoCharge.Enabled = false;
            txtMonthNoCharge.BackColor = ColorTranslator.FromOle(OldColor);
            txtYearCharge.Text = "";
            txtYearCharge.Enabled = false;
            txtYearCharge.BackColor = ColorTranslator.FromOle(OldColor);
            txtYearNoCharge.Text = "";
            txtYearNoCharge.Enabled = false;
            txtYearNoCharge.BackColor = ColorTranslator.FromOle(OldColor);
            txtMStickerNumber.Text = "";
            txtMStickerNumber.Enabled = false;
            txtYStickerNumber.Text = "";
            txtYStickerNumber.Enabled = false;
            lblMSD.Text = "";
            lblYSD.Text = "";
            lblNumberofYear.Text = "";
            lblNumberofMonth.Text = "";

            cmdCTA.Enabled = false;
            cmdUseTax.Enabled = false;
            txtTitle.Enabled = false;
            txtSalesTax.Enabled = false;
            chkSalesTaxExempt.Enabled = false;
            chkDealerSalesTax.Enabled = false;
            chkDealerSalesTax.CheckState = Wisej.Web.CheckState.Unchecked;
            chkNoFeeCTA.Enabled = false;
            chkNoFeeCTA.CheckState = Wisej.Web.CheckState.Unchecked;
            chkCTAPaid.Enabled = false;
            chkCTAPaid.CheckState = Wisej.Web.CheckState.Unchecked;
		}

		private void ConfigBattleList()
        {
            // kk01092018 tromvs-96  Change from hardcoded list to fill from printcodes class
            clsPrintCodes tDecals = new clsPrintCodes();
            int i;
            string strCode = "";
            cboBattle.AddItem("X - NONE");
            for (i = 1; i <= tDecals.MaxVeteranDecalCode; i++)
            {
                strCode = Strings.Format(i, "00");
                cboBattle.AddItem(strCode + " - " + tDecals.GetVeteranDecalDesc(ref strCode));
            }
        }

        private void ClearFleetGroup()
        {
            lblFleet.Text = "";
            txtFleetNumber.Text = "";
        }

        private void chkSpecialtyCorrExtraFee_CheckedChanged(object sender, EventArgs e)
        {
	        if (chkSpecialtyCorrExtraFee.Value == FCCheckBox.ValueSettings.Checked)
	        {
		        MotorVehicle.Statics.SpecialtyPlateCorrExtraFee = 0;
			}
	        else
	        {
		        MotorVehicle.Statics.SpecialtyPlateCorrExtraFee = 1;
	        }
	        MotorVehicle.CalculateECorrectFee(ref intCorrSpecialtyPlate, ref intCorrInitialPlate);
		}

        private void cmdCopyToMailingAddress_Click(object sender, EventArgs e)
        {
            if (txtReg1LR.Text == "R")
	        {
                FillAddressFields(txtReg2PartyID.Text.ToIntegerValue());
	        }
	        else
	        {
		        FillAddressFields(txtReg1PartyID.Text.ToIntegerValue());
            }
        }

        private void cmdCopyToLegalAddress_Click(object sender, EventArgs e)
        {
	        txtLegalResStreet.Text = txtAddress1.Text;
	        txtLegalres.Text = txtCity.Text;
	        txtResState.Text = txtState.Text;
	        txtResCountry.Text = txtCountry.Text;
	        if (txtState.Text != "ME" && !txtresidenceCode.Text.StartsWith("44"))
	        {
		        txtresidenceCode.Text = "99999";
		        txtLegalres.Locked = false;
	        }
	        else if (txtState.Text == "ME")
	        {
		        txtresidenceCode.Text = GetResCode(txtCity.Text);
		        txtLegalres.Locked = !String.IsNullOrEmpty(txtresidenceCode.Text);
	        }
	        else
	        {
		        txtresidenceCode.Text = "";
		        txtLegalres.Locked = false;
	        }
		}

		private void FillAddressFields(int partyId)
        {
	        cPartyController pCont = new cPartyController();
	        cParty pInfo;
	        cPartyAddress pAdd;

	        pInfo = pCont.GetParty(partyId);
	        if (!(pInfo == null))
	        {
		        pAdd = pInfo.GetPrimaryAddress();
		        if (pAdd != null)
		        {
			        txtAddress1.Text = pAdd.Address1;
			        txtAddress2.Text = pAdd.Address2;
			        txtCity.Text = pAdd.City;
			        txtState.Text = pAdd.State;
			        txtZip.Text = pAdd.Zip;
		        }
	        }
        }
        
        private void imgReg1Search_Click(object sender, EventArgs e)
        {
            SearchForParty(1);
        }

        private void imgReg1Edit_Click(object sender, EventArgs e)
        {
            int lngID;
            int originalPartyId;

            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtReg1PartyID.Text)));
            originalPartyId = lngID;

            imgReg1Edit.Enabled = false;
            lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
            imgReg1Edit.Enabled = true;
            if (lngID > 0)
            {
                txtReg1PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 1, originalPartyId == lngID ? CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }

            if (imgReg1Edit.Enabled)
            {
                imgReg1Edit.Focus();
            }
            else
            {
                txtReg1PartyID.Focus();
            }
        }

        private void imgReg2Edit_Click(object sender, EventArgs e)
        {
            int lngID;
            int originalPartyId;

            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtReg2PartyID.Text)));
            originalPartyId = lngID;

            imgReg2Edit.Enabled = false;
            lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
            imgReg2Edit.Enabled = true;
            if (lngID > 0)
            {
                txtReg2PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 2, originalPartyId == lngID ? CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }

            if (imgReg2Edit.Enabled)
            {
                imgReg2Edit.Focus();
            }
            else
            {
                txtReg2PartyID.Focus();
            }
        }

        private void imgReg2Search_Click(object sender, EventArgs e)
        {
            SearchForParty(2);
        }

        private void imgReg3Edit_Click(object sender, EventArgs e)
        {
            int lngID;
            int originalPartyId;

            lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtReg3PartyID.Text)));
            originalPartyId = lngID;

            imgReg3Edit.Enabled = false;
            lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
            imgReg3Edit.Enabled = true;
            if (lngID > 0)
            {
                txtReg3PartyID.Text = FCConvert.ToString(lngID);
                GetPartyInfo(lngID, 3, originalPartyId == lngID ? CentralPartyNameAddressCheck.AskToUseCentralPartyInfoIfDifferent : CentralPartyNameAddressCheck.UseCentralPartyInfo);
            }

            if (imgReg3Edit.Enabled)
            {
                imgReg3Edit.Focus();
            }
            else
            {
                txtReg3PartyID.Focus();
            }
        }

        private void imgFleetGroupSearch_Click(object sender, EventArgs e)
        {
            MotorVehicle.Statics.bolFromDataInput = true;
            frmFleetMaster.InstancePtr.Unload();
            frmFleetMaster.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            MotorVehicle.Statics.bolFromDataInput = false;
            if (txtFleetNumber.Text.ToIntegerValue() > 0)
            {

            }
        }

        private void imgCopyToMailingAddress_Click(object sender, EventArgs e)
        {
            if (txtReg1LR.Text == "R")
            {
                FillAddressFields(txtReg2PartyID.Text.ToIntegerValue());
            }
            else
            {
                FillAddressFields(txtReg1PartyID.Text.ToIntegerValue());
            }
        }

        private void imgCopyToLegalAddress_Click(object sender, EventArgs e)
        {
            txtLegalResStreet.Text = txtAddress1.Text;
            txtLegalres.Text = txtCity.Text;
            txtResState.Text = txtState.Text;
            txtResCountry.Text = txtCountry.Text;
        }

        private void imgEditRegistrant1_Click(object sender, EventArgs e)
        {
            UpdateRegistrantName(txtReg1ICM.Text, ref strReg1FirstName, ref strReg1MI, ref strReg1LastName, ref strReg1Designation, ref txtRegistrant1);
        }

        private void imgEditRegistrant2_Click(object sender, EventArgs e)
        {
            UpdateRegistrantName(txtReg2ICM.Text, ref strReg2FirstName, ref strReg2MI, ref strReg2LastName, ref strReg2Designation, ref txtRegistrant2);
        }

        private void imgEditRegistrant3_Click(object sender, EventArgs e)
        {
            UpdateRegistrantName(txtReg3ICM.Text, ref strReg3FirstName, ref strReg3MI, ref strReg3LastName, ref strReg3Designation, ref txtRegistrant3);
        }

        private void UpdateRegistrantName(string resgistrantType, ref string firstName, ref string middleInitial,
            ref string lastName, ref string designation, ref FCTextBox nameTextBox)
        {
            var regNameInfo = new RegistrantNameEditInfo();

            if (resgistrantType == "I")
            {
                regNameInfo.FirstName = firstName;
                regNameInfo.LastName = lastName;
                regNameInfo.MiddleInitial = middleInitial;
                regNameInfo.Designation = designation;
                regNameInfo.CompanyName = "";
                regNameInfo.Type = RegistrantType.Individual;
            }
            else
            {
                regNameInfo.FirstName = "";
                regNameInfo.LastName = "";
                regNameInfo.MiddleInitial = "";
                regNameInfo.Designation = "";
                regNameInfo.CompanyName = nameTextBox.Text;
                regNameInfo.Type = RegistrantType.Company;
            }

            var result = StaticSettings.GlobalCommandDispatcher.Send(new EditRegistrantName
            {
                CurrentInfo = regNameInfo
            }).Result;

            if (resgistrantType == "I")
            {
                firstName = result.FirstName;
                lastName = result.LastName;
                middleInitial = result.MiddleInitial;
                designation = result.Designation;

                nameTextBox.Text = FormatIndividualName(result.FirstName, result.MiddleInitial, result.LastName, result.Designation);
            }
            else
            {
                nameTextBox.Text = result.CompanyName;
            }
        }

        private string FormatIndividualName(string firstName, string middleInitial, string lastName, string designation)
        {

            return Strings.Trim(Strings.Trim(Strings.Trim(Strings.Trim(firstName) + " " + middleInitial) + " " + lastName) + " " + designation);
        }

        private string GetResCode(string town)
        {
            string code = "";
            clsDRWrapper rs = new clsDRWrapper();
            rs.OpenRecordset("SELECT * FROM Residence WHERE Town = '" + town.ToUpper() + "'");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.MoveLast();
                rs.MoveFirst();
                code = rs.Get_Fields_String("Code");
            }
            rs.DisposeOf();
            return code;
        }
        private void txtStyle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (VerifyStyle() == false)
            {
                e.Cancel = true;
            }
        }

        private string GetVehicleTypeCode(int classificationId, int categoryId)
        {
	        switch (classificationId)
	        {
		        case (int)VehicleClassificationNumber.CommercialTrucks:
			        return "H";
		        case (int)VehicleClassificationNumber.PassengerVehicles:
			        return "C";
		        case (int)VehicleClassificationNumber.Powersport:
			        return "M";
		        case (int)VehicleClassificationNumber.CommercialTrailers:
			        return "X";
		        case (int)VehicleClassificationNumber.RecreationalVehicles:
			        return "R";
		        case (int)VehicleClassificationNumber.TruckBodies:
			        return "B";
	        }

	        if (categoryId == (int)VehicleCategoryNumber.MotorCycles)
	        {
		        return "M";
	        }
	        return "";
        }
	}
}
