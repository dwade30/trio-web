//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptInventoryAdjustments.
	/// </summary>
	public partial class rptInventoryAdjustments : BaseSectionReport
	{
		public rptInventoryAdjustments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Adjustments Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptInventoryAdjustments_ReportEnd;
		}

        private void RptInventoryAdjustments_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
        }

        public static rptInventoryAdjustments InstancePtr
		{
			get
			{
				return (rptInventoryAdjustments)Sys.GetInstance(typeof(rptInventoryAdjustments));
			}
		}

		protected rptInventoryAdjustments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptInventoryAdjustments	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngMVR3Count;
		int lngSingleStickerCount;
		int lngDoubleStickerCount;
		int lngCombinationStickerCount;
		int lngDecalCount;
		int lngBoosterCount;
		int lngSpecialPermitCount;
		int lngTransitCount;
		int lngPK1;
		clsDRWrapper rs = new clsDRWrapper();
		string strLow = "";
		string strHigh = "";
		string strIT = "";
		string strLine = "";
		bool boolFirst;
		int intPageNumber;
		bool boolData;
		string strSQL = "";
		int lng1;
		int lng2;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			txtDT.Text = "";
			txtQuantity.Text = "";
			txtRange.Text = "";
			txtOperator.Text = "";
			txtExplanation.Text = "";
			if (!boolData)
			{
				eArgs.EOF = true;
				return;
			}
			if (boolFirst)
			{
				// do nothing
			}
			else
			{
				txtDesc.Text = "";
			}
			CheckAgain:
			;
			if (strIT == "M")
			{
				bool executeCheckNext = false;
				if (boolFirst)
				{
					boolFirst = false;
					if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					else
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					rs.OpenRecordset(strSQL);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						rs.MoveFirst();
						executeCheckNext = true;
						goto CheckNext;
					}
					else
					{
						strIT = "B";
						txtDesc.Text = "--------- PERMITS ----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
				else
				{
					rs.MoveNext();
					executeCheckNext = true;
					goto CheckNext;
				}
				CheckNext:
				;
				if (executeCheckNext)
				{
					executeCheckNext = false;
					if (rs.EndOfFile() != true)
					{
						if (FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "A" || FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "R")
						{
							eArgs.EOF = false;
							return;
						}
						else
						{
							rs.MoveNext();
							executeCheckNext = true;
							goto CheckNext;
						}
					}
					else
					{
						strIT = "B";
						txtDesc.Text = "--------- PERMITS ----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
			}
			else if (strIT == "B")
			{
				bool executeCheckNext2 = false;
				if (boolFirst)
				{
					boolFirst = false;
					if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (InventoryType = 'BXSXX' OR InventoryType = 'PXSXX' OR InventoryType = 'RXSXX') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					else
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (InventoryType = 'BXSXX' OR InventoryType = 'PXSXX' OR InventoryType = 'RXSXX') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					rs.OpenRecordset(strSQL);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						rs.MoveFirst();
						executeCheckNext2 = true;
						goto CheckNext2;
					}
					else
					{
						strIT = "D";
						txtDesc.Text = "---------- DECALS -----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
				else
				{
					rs.MoveNext();
					executeCheckNext2 = true;
					goto CheckNext2;
				}
				CheckNext2:
				;
				if (executeCheckNext2)
				{
					executeCheckNext2 = false;
					if (rs.EndOfFile() != true)
					{
						if (FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "A" || FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "R")
						{
							eArgs.EOF = false;
							return;
						}
						else
						{
							rs.MoveNext();
							executeCheckNext2 = true;
							goto CheckNext2;
						}
					}
					else
					{
						strIT = "D";
						txtDesc.Text = "---------- DECALS -----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
			}
			else if (strIT == "D")
			{
				bool executeCheckNext3 = false;
				if (boolFirst)
				{
					boolFirst = false;
					if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					else
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					rs.OpenRecordset(strSQL);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						rs.MoveFirst();
						executeCheckNext3 = true;
						goto CheckNext3;
					}
					else
					{
						strIT = "P";
						txtDesc.Text = "---------- PLATES -----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
				else
				{
					rs.MoveNext();
					executeCheckNext3 = true;
					goto CheckNext3;
				}
				CheckNext3:
				;
				if (executeCheckNext3)
				{
					executeCheckNext3 = false;
					if (rs.EndOfFile() != true)
					{
						if (FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "A" || FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "R")
						{
							eArgs.EOF = false;
							return;
						}
						else
						{
							rs.MoveNext();
							executeCheckNext3 = true;
							goto CheckNext3;
						}
					}
					else
					{
						strIT = "P";
						txtDesc.Text = "---------- PLATES -----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
			}
			else if (strIT == "P")
			{
				bool executeCheckNext4 = false;
				if (boolFirst)
				{
					boolFirst = false;
					if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (Left(InventoryType, 1) = '" + strIT + "' AND InventoryType <> 'PXSXX' AND InventoryType <> 'PXSLT') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					else
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (Left(InventoryType, 1) = '" + strIT + "' AND InventoryType <> 'PXSXX' AND InventoryType <> 'PXSLT') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					rs.OpenRecordset(strSQL);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						rs.MoveFirst();
						executeCheckNext4 = true;
						goto CheckNext4;
					}
					else
					{
						strIT = "S";
						txtDesc.Text = "--------- STICKERS ----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
				else
				{
					rs.MoveNext();
					executeCheckNext4 = true;
					goto CheckNext4;
				}
				CheckNext4:
				;
				if (executeCheckNext4)
				{
					executeCheckNext4 = false;
					if (rs.EndOfFile() != true)
					{
						if (FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "A" || FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "R")
						{
							eArgs.EOF = false;
							return;
						}
						else
						{
							rs.MoveNext();
							executeCheckNext4 = true;
							goto CheckNext4;
						}
					}
					else
					{
						strIT = "S";
						txtDesc.Text = "--------- STICKERS ----------";
						boolFirst = true;
						goto CheckAgain;
					}
				}
			}
			else if (strIT == "S")
			{
				bool executeCheckNext5 = false;
				if (boolFirst)
				{
					boolFirst = false;
					if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					else
					{
						strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
					}
					rs.OpenRecordset(strSQL);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						rs.MoveFirst();
						executeCheckNext5 = true;
						goto CheckNext5;
					}
					else
					{
						eArgs.EOF = true;
						return;
					}
				}
				else
				{
					rs.MoveNext();
					executeCheckNext5 = true;
					goto CheckNext5;
				}
				CheckNext5:
				;
				if (executeCheckNext5)
				{
					executeCheckNext5 = false;
					if (rs.EndOfFile() != true)
					{
						if (FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "A" || FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "R")
						{
							eArgs.EOF = false;
							return;
						}
						else
						{
							rs.MoveNext();
							executeCheckNext5 = true;
							goto CheckNext5;
						}
					}
					else
					{
						eArgs.EOF = true;
						return;
					}
				}
			}
			else
			{
				txtDesc.Text = "No Information";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
			}
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND InventoryType <> 'PXSLT' AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
			}
			else
			{
				strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSQL);
				lng1 = 0;
				lng2 = 0;
				rs.MoveLast();
				lng1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lng2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				strSQL = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND InventoryType <> 'PXSLT' AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
			}
			rs.OpenRecordset(strSQL);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				boolData = true;
				rs.MoveLast();
				rs.MoveFirst();
				strIT = "M";
				txtDesc.Text = "--------- MVR FORMS ----------";
				boolFirst = true;
			}
			else
			{
				boolData = false;
			}
			lngMVR3Count = 0;
			lngSingleStickerCount = 0;
			lngDoubleStickerCount = 0;
			lngCombinationStickerCount = 0;
			lngDecalCount = 0;
			lngBoosterCount = 0;
			lngSpecialPermitCount = 0;
			lngTransitCount = 0;
			MotorVehicle.Statics.pcPlateInfo = new MotorVehicle.PlateCounter[0 + 1];
			MotorVehicle.Statics.intPlateCountCounter = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngAmount = 0;
			int counter;
			string strClass = "";
			bool blnFound = false;
			if (boolData)
			{
				if (txtDesc.Text != "")
				{
					txtDesc.Top += 180 / 1440F;
				}
				strLine = "";
				txtDT.Text = Strings.Format(rs.Get_Fields_DateTime("DateOfAdjustment"), "MM/dd/yyyy");
				if (FCConvert.ToString(rs.Get_Fields_String("AdjustmentCode")) == "R")
				{
					txtQuantity.Text = Strings.StrDup(6 - rs.Get_Fields_Int32("QuantityAdjusted").ToString().Length, " ") + rs.Get_Fields_Int32("QuantityAdjusted");
					lngAmount = FCConvert.ToInt32(rs.Get_Fields_Int32("QuantityAdjusted"));
				}
				else
				{
					txtQuantity.Text = Strings.StrDup(5 - rs.Get_Fields_Int32("QuantityAdjusted").ToString().Length, " ") + "-" + rs.Get_Fields_Int32("QuantityAdjusted");
					lngAmount = rs.Get_Fields_Int32("QuantityAdjusted") * -1;
				}
				strLow = rs.Get_Fields("Low").ToString();
				strHigh = rs.Get_Fields("High").ToString();
				if (FCConvert.ToString(rs.Get_Fields_String("InventoryType")) == "RXSXX")
				{
					txtRange.Text = strLine + "SRP " + strLow + " - " + strHigh;
					lngSpecialPermitCount += lngAmount;
				}
				else if (rs.Get_Fields_String("InventoryType") == "PXSXX")
				{
					txtRange.Text = strLine + "TRN " + strLow + " - " + strHigh;
					lngTransitCount += lngAmount;
				}
				else if (rs.Get_Fields_String("InventoryType") == "BXSXX")
				{
					txtRange.Text = strLine + "BST " + strLow + " - " + strHigh;
					lngBoosterCount += lngAmount;
				}
				else
				{
					if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) == "D")
					{
						strLine = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1);
						lngDecalCount += lngAmount;
					}
					else if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1) == "C")
					{
						strLine = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						lngCombinationStickerCount += lngAmount;
					}
					else
					{
						strLine = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1);
						if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1) == "M")
						{
							lngMVR3Count += lngAmount;
						}
						else if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1) == "S")
						{
							if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1) == "S")
							{
								lngSingleStickerCount += lngAmount;
							}
							else
							{
								lngDoubleStickerCount += lngAmount;
							}
						}
						else
						{
							strClass = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
							blnFound = false;
							for (counter = 0; counter <= MotorVehicle.Statics.intPlateCountCounter - 1; counter++)
							{
								if (strClass == MotorVehicle.Statics.pcPlateInfo[counter].Class)
								{
									MotorVehicle.Statics.pcPlateInfo[counter].AdjustmentCount += lngAmount;
									blnFound = true;
									break;
								}
							}
							if (!blnFound)
							{
								Array.Resize(ref MotorVehicle.Statics.pcPlateInfo, MotorVehicle.Statics.intPlateCountCounter + 1);
								MotorVehicle.Statics.pcPlateInfo[MotorVehicle.Statics.intPlateCountCounter].Class = strClass;
								MotorVehicle.Statics.pcPlateInfo[MotorVehicle.Statics.intPlateCountCounter].AdjustmentCount = lngAmount;
								MotorVehicle.Statics.intPlateCountCounter += 1;
							}
						}
					}
					txtRange.Text = strLine + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2) + " " + strLow + " - " + strHigh;
				}
				// 
				txtOperator.Text = Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " ") + rs.Get_Fields_String("OpID");
				strLine = "";
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))) == "")
				{
					if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) == "S")
					{
						strLine = "RECEIVED" + Strings.StrDup(19, " ");
						strLine += " " + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
					}
					else
					{
						strLine = "RECEIVED" + Strings.StrDup(20, " ");
					}
				}
				else
				{
					if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) == "S")
					{
						if (FCConvert.ToString(rs.Get_Fields_String("reason")).Length >= 26)
						{
							strLine = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 26);
						}
						else
						{
							strLine = FCConvert.ToString(rs.Get_Fields_String("reason"));
						}
						strLine += " " + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
					}
					else
					{
						if (FCConvert.ToString(rs.Get_Fields_String("reason")).Length >= 28)
						{
							strLine = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 28);
						}
						else
						{
							strLine = FCConvert.ToString(rs.Get_Fields_String("reason"));
						}
					}
				}
				txtExplanation.Text = strLine;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldMVR3Count.Text = Strings.Format(lngMVR3Count, "#,##0");
			fldSingleStickerCount.Text = Strings.Format(lngSingleStickerCount, "#,##0");
			fldDoubleStickerCount.Text = Strings.Format(lngDoubleStickerCount, "#,##0");
			fldCombinationStickerCount.Text = Strings.Format(lngCombinationStickerCount, "#,##0");
			fldBoosterCount.Text = Strings.Format(lngBoosterCount, "#,##0");
			fldSpecialPermitCount.Text = Strings.Format(lngSpecialPermitCount, "#,##0");
			fldTransitCount.Text = Strings.Format(lngTransitCount, "#,##0");
			if (MotorVehicle.Statics.intPlateCountCounter > 0)
			{
				srptPlateCounts.Report = new srptInventoryAdjustmentsPlateSummary();
			}
			else
			{
				srptPlateCounts.Report = null;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (boolData == false)
			{
				txtNoInfo.Text = "No Information";
			}
		}

		private void rptInventoryAdjustments_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptInventoryAdjustments properties;
			//rptInventoryAdjustments.Caption	= "Inventory Adjustments Report";
			//rptInventoryAdjustments.Icon	= "rptInventoryAdjustments.dsx":0000";
			//rptInventoryAdjustments.Left	= 0;
			//rptInventoryAdjustments.Top	= 0;
			//rptInventoryAdjustments.Width	= 11880;
			//rptInventoryAdjustments.Height	= 8595;
			//rptInventoryAdjustments.StartUpPosition	= 3;
			//rptInventoryAdjustments.SectionData	= "rptInventoryAdjustments.dsx":058A;
			//End Unmaped Properties
		}
	}
}
