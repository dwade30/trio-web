//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmPrintQueue : BaseForm
	{
		public frmPrintQueue()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintQueue InstancePtr
		{
			get
			{
				return (frmPrintQueue)Sys.GetInstance(typeof(frmPrintQueue));
			}
		}

		protected frmPrintQueue _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public int intPrintedStatusCol;
		public int intPlateCol;
		public int intRegTypeCol;
		public int intOwnerCol;
		public int intYearCol;
		public int intMakeCol;
		public int intModelCol;
		public int intExpiresCol;
		public int intSelectCol;
		public int intNumberCol;
		public int intStartRow;
		public int intNumberToPrint;
		public int intIDCol;
		public int intUniqueIdentifierCol;
		clsDRWrapper rs = new clsDRWrapper();

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsPrintQueue.Rows - 1); counter++)
			{
				vsPrintQueue.TextMatrix(counter, intSelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			PrintMVR10Batch();
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsPrintQueue.Rows - 1); counter++)
			{
				vsPrintQueue.TextMatrix(counter, intSelectCol, FCConvert.ToString(true));
			}
		}

		private void frmPrintQueue_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPrintQueue_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintQueue properties;
			//frmPrintQueue.FillStyle	= 0;
			//frmPrintQueue.ScaleWidth	= 9045;
			//frmPrintQueue.ScaleHeight	= 7245;
			//frmPrintQueue.LinkTopic	= "Form2";
			//frmPrintQueue.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			intNumberCol = 1;
			intIDCol = 2;
			intUniqueIdentifierCol = 3;
			intSelectCol = 4;
			intRegTypeCol = 5;
			intOwnerCol = 6;
			intPlateCol = 7;
			intYearCol = 8;
			intMakeCol = 9;
			intModelCol = 10;
			intExpiresCol = 11;
			intPrintedStatusCol = 12;
			FormatGrid();
			vsPrintQueue.Rows = 1;
			rs.OpenRecordset("SELECT * FROM PendingActivityMaster WHERE Class = 'TL' AND Subclass = 'L9' ORDER BY Plate", "TWMV0000.vb1");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsPrintQueue.Rows += 1;
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intNumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ArchiveID")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intIDCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intSelectCol, FCConvert.ToString(false));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intOwnerCol, FCConvert.ToString(rs.Get_Fields("Owner1")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intPlateCol, FCConvert.ToString(rs.Get_Fields_String("Plate")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intYearCol, FCConvert.ToString(rs.Get_Fields("Year")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intMakeCol, FCConvert.ToString(rs.Get_Fields_String("Make")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intModelCol, FCConvert.ToString(rs.Get_Fields_String("Model")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intExpiresCol, FCConvert.ToString(rs.Get_Fields_DateTime("ExpireDate")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intRegTypeCol, FCConvert.ToString(rs.Get_Fields("TransactionType")));
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intPrintedStatusCol, "No");
					vsPrintQueue.TextMatrix(vsPrintQueue.Rows - 1, intUniqueIdentifierCol, FCConvert.ToString(rs.Get_Fields_Int32("LongTermTrailerRegUniqueIdentifier")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			else
			{
				MessageBox.Show("No registrations in print queue.", "No Registrations", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void FormatGrid()
		{
			//vsPrintQueue.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, intSelectCol, 0, intSelectCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsPrintQueue.ColDataType(intSelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsPrintQueue.ColHidden(intNumberCol, true);
			vsPrintQueue.ColHidden(intIDCol, true);
			vsPrintQueue.ColHidden(intUniqueIdentifierCol, true);
			vsPrintQueue.ColAlignment(intYearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPrintQueue.TextMatrix(0, intSelectCol, "Select");
			vsPrintQueue.TextMatrix(0, intPlateCol, "Plate");
			vsPrintQueue.TextMatrix(0, intOwnerCol, "Owner");
			vsPrintQueue.TextMatrix(0, intYearCol, "Year");
			vsPrintQueue.TextMatrix(0, intMakeCol, "Make");
			vsPrintQueue.TextMatrix(0, intModelCol, "Model");
			vsPrintQueue.TextMatrix(0, intRegTypeCol, "Reg Type");
			vsPrintQueue.TextMatrix(0, intExpiresCol, "Expiration Date");
			vsPrintQueue.TextMatrix(0, intPrintedStatusCol, "Printed?");
			vsPrintQueue.ColWidth(0, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.015));
			vsPrintQueue.ColWidth(intSelectCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.06));
			vsPrintQueue.ColWidth(intPlateCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.09));
			vsPrintQueue.ColWidth(intOwnerCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.23));
			vsPrintQueue.ColWidth(intYearCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.07));
			vsPrintQueue.ColWidth(intMakeCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
			vsPrintQueue.ColWidth(intModelCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
			vsPrintQueue.ColWidth(intRegTypeCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
			vsPrintQueue.ColWidth(intExpiresCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.13));
			vsPrintQueue.ColWidth(intPrintedStatusCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
		}

		private void frmPrintQueue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				for (counter = 1; counter <= (vsPrintQueue.Rows - 1); counter++)
				{
					if (vsPrintQueue.TextMatrix(counter, intPrintedStatusCol) != "No")
					{
						if (MessageBox.Show("You have not saved the fact that some forms have been printed.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				}
			}
		}

		private void frmPrintQueue_Resize(object sender, System.EventArgs e)
		{
			vsPrintQueue.ColWidth(0, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.015));
			vsPrintQueue.ColWidth(intSelectCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.06));
			vsPrintQueue.ColWidth(intPlateCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.09));
			vsPrintQueue.ColWidth(intOwnerCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.23));
			vsPrintQueue.ColWidth(intYearCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.07));
			vsPrintQueue.ColWidth(intMakeCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
			vsPrintQueue.ColWidth(intModelCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
			vsPrintQueue.ColWidth(intRegTypeCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
			vsPrintQueue.ColWidth(intExpiresCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.13));
			vsPrintQueue.ColWidth(intPrintedStatusCol, FCConvert.ToInt32(vsPrintQueue.WidthOriginal * 0.1));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsPrintQueue.Rows - 1); counter++)
			{
				if (vsPrintQueue.TextMatrix(counter, intPrintedStatusCol) != "No")
				{
					if (MessageBox.Show("You have not saved the fact that some forms have been printed.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						return;
					}
				}
			}
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			clsDRWrapper rsDelete = new clsDRWrapper();
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsLongTerm = new clsDRWrapper();
			for (counter = 1; counter <= (vsPrintQueue.Rows - 1); counter++)
			{
				if (vsPrintQueue.TextMatrix(counter, intPrintedStatusCol) == "Yes")
				{
					rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = " + vsPrintQueue.TextMatrix(counter, intNumberCol), "TWMV0000.vb1");
					if (rsMaster.EndOfFile() != true && rsMaster.BeginningOfFile() != true)
					{
						rsLongTerm.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + rsMaster.Get_Fields("LongTermTrailerRegKey"));
						if (rsLongTerm.EndOfFile() != true && rsLongTerm.BeginningOfFile() != true)
						{
							rsLongTerm.Edit();
							rsLongTerm.Set_Fields("UniqueIdentifier", FCConvert.ToString(Conversion.Val(vsPrintQueue.TextMatrix(counter, intUniqueIdentifierCol))));
							rsLongTerm.Update();
						}
					}
					rsDelete.Execute("DELETE FROM PendingActivityMaster WHERE ID = " + vsPrintQueue.TextMatrix(counter, intIDCol), "TWMV0000.vb1");
				}
			}
			Close();
		}

		private void vsPrintQueue_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsPrintQueue.Row > 0)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol)) == true)
				if (FCConvert.CBool(vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol)) == true)
				{
					vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsPrintQueue_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsPrintQueue.Row > 0)
			{
				if (KeyCode == Keys.Space)
				{
					KeyCode = 0;
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol)) == true)
					if (FCConvert.CBool(vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol)) == true)
					{
						vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol, FCConvert.ToString(false));
					}
					else
					{
						vsPrintQueue.TextMatrix(vsPrintQueue.Row, intSelectCol, FCConvert.ToString(true));
					}
				}
			}
		}

		private void PrintMVR10Batch()
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			int lngFormCheck;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				mnuProcessQuit.Enabled = false;
				cmdSelectAll.Enabled = false;
				cmdPrint.Enabled = false;
				cmdClearAll.Enabled = false;
				mnuProcessSave.Enabled = false;
				vsPrintQueue.Enabled = false;
				MotorVehicle.Statics.gboolFromPrintQueue = true;
				intStartRow = 1;
				intNumberToPrint = 0;
				for (intCounter = 1; intCounter <= (vsPrintQueue.Rows - 1); intCounter++)
				{
					if (FCConvert.CBool(vsPrintQueue.TextMatrix(intCounter, intSelectCol)) == true)
					{
						intNumberToPrint += 1;
					}
				}
				if (intNumberToPrint == 0)
				{
					MessageBox.Show("You must select at least 1 registration to print before you may proceed.", "Select Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (!MotorVehicle.Statics.blnLongTermLaserForms)
				{
					if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to the top as possible) next to the line titled:" + "\r\n" + "STATE OF MAINE", 23, 1, MotorVehicle.Statics.MVR10PrinterName) == DialogResult.Cancel)
					{
						MotorVehicle.Statics.PrintMVR3 = false;
					}
					else
					{
                        rptMVRT10.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                    }
				}
				else
				{
                    rptMVRT10Laser.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                }
				if (!MotorVehicle.Statics.blnLongTermLaserForms)
				{
					rptMVRT10.InstancePtr.Unload();
				}
				else
				{
					rptMVRT10Laser.InstancePtr.Unload();
				}
				if (MotorVehicle.Statics.PrintMVR3 == true)
				{
					if (fecherFoundation.Information.Err().Number == 0)
					{
						mnuProcessQuit.Enabled = false;
					}
				}
				else
				{
					mnuProcessQuit.Enabled = true;
					cmdSelectAll.Enabled = true;
					cmdPrint.Enabled = true;
					cmdClearAll.Enabled = true;
					mnuProcessSave.Enabled = true;
					vsPrintQueue.Enabled = true;
					return;
				}
				mnuProcessQuit.Enabled = true;
				cmdSelectAll.Enabled = true;
				cmdPrint.Enabled = true;
				cmdClearAll.Enabled = true;
				mnuProcessSave.Enabled = true;
				vsPrintQueue.Enabled = true;
				MotorVehicle.Statics.gboolFromPrintQueue = false;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				mnuProcessQuit.Enabled = true;
				cmdSelectAll.Enabled = true;
				cmdPrint.Enabled = true;
				cmdClearAll.Enabled = true;
				mnuProcessSave.Enabled = true;
				vsPrintQueue.Enabled = true;
				MotorVehicle.Statics.gboolFromPrintQueue = false;
			}
		}
	}
}
