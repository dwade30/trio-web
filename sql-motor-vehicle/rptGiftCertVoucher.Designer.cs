﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptGiftCertVoucher.
	/// </summary>
	partial class rptGiftCertVoucher
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptGiftCertVoucher));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.subIssued = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subRedeemed = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.txtNoInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.subIssued,
				this.subRedeemed
			});
			this.Detail.Height = 0.28125F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNoInfo
			});
			this.ReportFooter.Height = 0.625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label1,
				this.lblPage,
				this.Label33
			});
			this.GroupHeader1.Height = 1.375F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.4375F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.6875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label1.Text = "**** GIFT CERTIFICATE REGISTER ****";
			this.Label1.Top = 0F;
			this.Label1.Width = 3F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 1.875F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// subIssued
			// 
			this.subIssued.CloseBorder = false;
			this.subIssued.Height = 0F;
			this.subIssued.Left = 0F;
			this.subIssued.Name = "subIssued";
			this.subIssued.Report = null;
			this.subIssued.Top = 0.0625F;
			this.subIssued.Width = 7.4375F;
			// 
			// subRedeemed
			// 
			this.subRedeemed.CloseBorder = false;
			this.subRedeemed.Height = 0F;
			this.subRedeemed.Left = 0F;
			this.subRedeemed.Name = "subRedeemed";
			this.subRedeemed.Report = null;
			this.subRedeemed.Top = 0.1875F;
			this.subRedeemed.Width = 7.4375F;
			// 
			// txtNoInfo
			// 
			this.txtNoInfo.CanShrink = true;
			this.txtNoInfo.Height = 0.3125F;
			this.txtNoInfo.Left = 1.96875F;
			this.txtNoInfo.Name = "txtNoInfo";
			this.txtNoInfo.Style = "font-family: \'Roman 10cpi\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.txtNoInfo.Text = null;
			this.txtNoInfo.Top = 0.15625F;
			this.txtNoInfo.Width = 2.6875F;
			// 
			// rptGiftCertVoucher
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subIssued;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subRedeemed;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoInfo;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
