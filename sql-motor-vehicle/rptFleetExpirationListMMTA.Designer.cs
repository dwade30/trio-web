﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptFleetReminderListMMTA.
	/// </summary>
	partial class rptFleetReminderListMMTA
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFleetReminderListMMTA));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExpirationMonth = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblFleetOwner = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFleetName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFleetName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldClassPlate,
				this.fldVIN,
				this.fldYear,
				this.fldMake,
				this.fldExpires,
				this.Line3,
				this.Line4,
				this.fldUnit
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.lblExpirationMonth,
				this.Line2,
				this.lblFleetOwner,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label21,
				this.Label22,
				this.Label24,
				this.Label25
			});
			this.PageHeader.Height = 0.8958333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Binder,
				this.fldFleetName
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Fleet Reminder List";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.15625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label8.Text = "Label8";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.6875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.6875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label11.Text = "Label11";
			this.Label11.Top = 0F;
			this.Label11.Width = 1.3125F;
			// 
			// lblExpirationMonth
			// 
			this.lblExpirationMonth.Height = 0.1875F;
			this.lblExpirationMonth.HyperLink = null;
			this.lblExpirationMonth.Left = 1.5F;
			this.lblExpirationMonth.Name = "lblExpirationMonth";
			this.lblExpirationMonth.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblExpirationMonth.Text = "Fleet Registration Listing";
			this.lblExpirationMonth.Top = 0.40625F;
			this.lblExpirationMonth.Width = 5.15625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.03125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.875F;
			this.Line2.Width = 7.9375F;
			this.Line2.X1 = 0.03125F;
			this.Line2.X2 = 7.96875F;
			this.Line2.Y1 = 0.875F;
			this.Line2.Y2 = 0.875F;
			// 
			// lblFleetOwner
			// 
			this.lblFleetOwner.Height = 0.1875F;
			this.lblFleetOwner.HyperLink = null;
			this.lblFleetOwner.Left = 1.5F;
			this.lblFleetOwner.Name = "lblFleetOwner";
			this.lblFleetOwner.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblFleetOwner.Text = "Fleet Registration Listing";
			this.lblFleetOwner.Top = 0.21875F;
			this.lblFleetOwner.Width = 5.15625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label15.Text = "Cl \\ Plate";
			this.Label15.Top = 0.6875F;
			this.Label15.Width = 0.75F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.90625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label16.Text = "VIN";
			this.Label16.Top = 0.6875F;
			this.Label16.Width = 1.1875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.46875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label17.Text = "Year";
			this.Label17.Top = 0.6875F;
			this.Label17.Width = 0.5F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.03125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label18.Text = "Make";
			this.Label18.Top = 0.6875F;
			this.Label18.Width = 0.46875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 5.34375F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label21.Text = "Renew - How Long?";
			this.Label21.Top = 0.6875F;
			this.Label21.Width = 1.4375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 6.90625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label22.Text = "Do not renew";
			this.Label22.Top = 0.6875F;
			this.Label22.Width = 1.09375F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 4.4375F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label24.Text = "Expires";
			this.Label24.Top = 0.6875F;
			this.Label24.Width = 0.84375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 3.5625F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label25.Text = "Unit";
			this.Label25.Top = 0.6875F;
			this.Label25.Width = 0.46875F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 5.59375F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.71875F;
			// 
			// fldFleetName
			// 
			this.fldFleetName.Height = 0.1875F;
			this.fldFleetName.Left = 0F;
			this.fldFleetName.Name = "fldFleetName";
			this.fldFleetName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
			this.fldFleetName.Text = null;
			this.fldFleetName.Top = 0.0625F;
			this.fldFleetName.Width = 4.5F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.19F;
			this.fldClassPlate.Left = 0F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldClassPlate.Text = null;
			this.fldClassPlate.Top = 0F;
			this.fldClassPlate.Width = 0.875F;
			// 
			// fldVIN
			// 
			this.fldVIN.Height = 0.19F;
			this.fldVIN.Left = 0.90625F;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldVIN.Text = null;
			this.fldVIN.Top = 0F;
			this.fldVIN.Width = 1.5F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.19F;
			this.fldYear.Left = 2.4375F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.5F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.19F;
			this.fldMake.Left = 3.03125F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 0F;
			this.fldMake.Width = 0.46875F;
			// 
			// fldExpires
			// 
			this.fldExpires.Height = 0.19F;
			this.fldExpires.Left = 4.4375F;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldExpires.Text = "Field1";
			this.fldExpires.Top = 0F;
			this.fldExpires.Width = 0.84375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 5.34375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.15625F;
			this.Line3.Width = 1.4375F;
			this.Line3.X1 = 5.34375F;
			this.Line3.X2 = 6.78125F;
			this.Line3.Y1 = 0.15625F;
			this.Line3.Y2 = 0.15625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 6.90625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.15625F;
			this.Line4.Width = 1.09375F;
			this.Line4.X1 = 6.90625F;
			this.Line4.X2 = 8F;
			this.Line4.Y1 = 0.15625F;
			this.Line4.Y2 = 0.15625F;
			// 
			// fldUnit
			// 
			this.fldUnit.Height = 0.19F;
			this.fldUnit.Left = 3.5625F;
			this.fldUnit.Name = "fldUnit";
			this.fldUnit.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldUnit.Text = "Field1";
			this.fldUnit.Top = 0F;
			this.fldUnit.Width = 0.8125F;
			// 
			// rptFleetReminderListMMTA
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFleetName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnit;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExpirationMonth;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFleetOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFleetName;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
