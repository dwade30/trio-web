//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubNoFeeDupRegException.
	/// </summary>
	public partial class rptSubNoFeeDupRegException : BaseSectionReport
	{
		public rptSubNoFeeDupRegException()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Exception Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptSubNoFeeDupRegException_ReportEnd;
		}

        private void RptSubNoFeeDupRegException_ReportEnd(object sender, EventArgs e)
        {
            rsAM.DisposeOf();
        }

        public static rptSubNoFeeDupRegException InstancePtr
		{
			get
			{
				return (rptSubNoFeeDupRegException)Sys.GetInstance(typeof(rptSubNoFeeDupRegException));
			}
		}

		protected rptSubNoFeeDupRegException _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubNoFeeDupRegException	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAM = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsAM.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			else
			{
				rsAM.MoveNext();
				if (rsAM.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Per Ramona 11/30/2012
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND Status <> 'V' AND NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0 ORDER BY DateUpdated");
			}
			else
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Status <> 'V' AND NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0 ORDER BY DateUpdated");
			}
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rptNewExceptionReport.InstancePtr.boolData = true;
			}
			else
			{
				this.Close();
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rptNewExceptionReport.InstancePtr.NoFeeDupRegTotal += 1;
			fldDate.Text = Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy");
			fldReciept.Text = Strings.Format(rsAM.Get_Fields_Int32("NoFeeMVR3Number"), "00000000");
			fldClassPlate.Text = rsAM.Get_Fields_String("Class") + " " + rsAM.Get_Fields_String("plate");
			fldRegistrant.Text = MotorVehicle.GetPartyNameMiddleInitial(rsAM.Get_Fields_Int32("PartyID1"));
			fldOpID.Text = Strings.Format(rsAM.Get_Fields_String("OpID"), "!@@@");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalCount.Text = FCConvert.ToString(rptNewExceptionReport.InstancePtr.NoFeeDupRegTotal);
		}

		private void rptSubNoFeeDupRegException_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptSubNoFeeDupRegException properties;
			//rptSubNoFeeDupRegException.Caption	= "Exception Report";
			//rptSubNoFeeDupRegException.Icon	= "rptSubNoFeeDupRegException.dsx":0000";
			//rptSubNoFeeDupRegException.Left	= 0;
			//rptSubNoFeeDupRegException.Top	= 0;
			//rptSubNoFeeDupRegException.Width	= 11880;
			//rptSubNoFeeDupRegException.Height	= 8595;
			//rptSubNoFeeDupRegException.StartUpPosition	= 3;
			//rptSubNoFeeDupRegException.SectionData	= "rptSubNoFeeDupRegException.dsx":058A;
			//End Unmaped Properties
		}
	}
}
