//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPrintFleet.
	/// </summary>
	partial class frmPrintFleet
	{
		public fecherFoundation.FCComboBox cmbLongTerm;
		public fecherFoundation.FCLabel lblLongTerm;
		public fecherFoundation.FCComboBox cmbFleet;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCComboBox cboFleets;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdDisplay;
		public FCGrid vsVehicles;
		public FCGrid vsSummary;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCLabel lblSummary;
		public fecherFoundation.FCLabel lblDetail;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintFleet));
			this.cmbLongTerm = new fecherFoundation.FCComboBox();
			this.lblLongTerm = new fecherFoundation.FCLabel();
			this.cmbFleet = new fecherFoundation.FCComboBox();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.cboFleets = new fecherFoundation.FCComboBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdDisplay = new fecherFoundation.FCButton();
			this.vsVehicles = new fecherFoundation.FCGrid();
			this.vsSummary = new fecherFoundation.FCGrid();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.lblSummary = new fecherFoundation.FCLabel();
			this.lblDetail = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDisplay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSummary)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbFleet);
			this.ClientArea.Controls.Add(this.cmbLongTerm);
			this.ClientArea.Controls.Add(this.lblLongTerm);
			this.ClientArea.Controls.Add(this.cboFleets);
			this.ClientArea.Controls.Add(this.cmdDisplay);
			this.ClientArea.Controls.Add(this.vsVehicles);
			this.ClientArea.Controls.Add(this.vsSummary);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Controls.Add(this.lblSummary);
			this.ClientArea.Controls.Add(this.lblDetail);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(251, 30);
			this.HeaderText.Text = "Fleet Registration List";
			// 
			// cmbLongTerm
			// 
			this.cmbLongTerm.Items.AddRange(new object[] {
            "Long Term",
            "Annual"});
			this.cmbLongTerm.Location = new System.Drawing.Point(139, 201);
			this.cmbLongTerm.Name = "cmbLongTerm";
			this.cmbLongTerm.Size = new System.Drawing.Size(296, 40);
			this.cmbLongTerm.TabIndex = 2;
			this.cmbLongTerm.Text = "Annual";
			this.cmbLongTerm.Visible = false;
			this.cmbLongTerm.SelectedIndexChanged += new System.EventHandler(this.cmbLongTerm_SelectedIndexChanged);
			// 
			// lblLongTerm
			// 
			this.lblLongTerm.AutoSize = true;
			this.lblLongTerm.Location = new System.Drawing.Point(30, 215);
			this.lblLongTerm.Name = "lblLongTerm";
			this.lblLongTerm.Size = new System.Drawing.Size(40, 15);
			this.lblLongTerm.TabIndex = 3;
			this.lblLongTerm.Text = "TYPE";
			this.lblLongTerm.Visible = false;
			// 
			// cmbFleet
			// 
			this.cmbFleet.Items.AddRange(new object[] {
            "Fleet",
            "Group"});
			this.cmbFleet.Location = new System.Drawing.Point(30, 82);
			this.cmbFleet.Name = "cmbFleet";
			this.cmbFleet.Size = new System.Drawing.Size(251, 40);
			this.cmbFleet.Text = "Fleet";
			this.cmbFleet.SelectedIndexChanged += new System.EventHandler(this.cmbFleet_SelectedIndexChanged);
			// 
			// dlg1
			// 
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			// 
			// cboFleets
			// 
			this.cboFleets.BackColor = System.Drawing.SystemColors.Window;
			this.cboFleets.Location = new System.Drawing.Point(30, 142);
			this.cboFleets.Name = "cboFleets";
			this.cboFleets.Size = new System.Drawing.Size(405, 40);
			this.cboFleets.TabIndex = 2;
			this.cboFleets.SelectedIndexChanged += new System.EventHandler(this.cboFleets_SelectedIndexChanged);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(467, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(78, 48);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdDisplay
			// 
			this.cmdDisplay.AppearanceKey = "actionButton";
			this.cmdDisplay.Location = new System.Drawing.Point(30, 744);
			this.cmdDisplay.Name = "cmdDisplay";
			this.cmdDisplay.Size = new System.Drawing.Size(100, 48);
			this.cmdDisplay.TabIndex = 4;
			this.cmdDisplay.Text = "Display";
			this.cmdDisplay.Visible = false;
			this.cmdDisplay.Click += new System.EventHandler(this.cmdDisplay_Click);
			// 
			// vsVehicles
			// 
			this.vsVehicles.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsVehicles.Cols = 10;
			this.vsVehicles.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsVehicles.FixedCols = 0;
			this.vsVehicles.Location = new System.Drawing.Point(30, 290);
			this.vsVehicles.Name = "vsVehicles";
			this.vsVehicles.RowHeadersVisible = false;
			this.vsVehicles.Rows = 1;
			this.vsVehicles.ShowFocusCell = false;
			this.vsVehicles.Size = new System.Drawing.Size(997, 312);
			this.vsVehicles.TabIndex = 7;
			this.vsVehicles.Visible = false;
			this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
			this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
			// 
			// vsSummary
			// 
			this.vsSummary.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsSummary.Cols = 9;
			this.vsSummary.FixedCols = 0;
			this.vsSummary.Location = new System.Drawing.Point(30, 652);
			this.vsSummary.Name = "vsSummary";
			this.vsSummary.RowHeadersVisible = false;
			this.vsSummary.Rows = 1;
			this.vsSummary.ShowFocusCell = false;
			this.vsSummary.Size = new System.Drawing.Size(997, 72);
			this.vsSummary.TabIndex = 10;
			this.vsSummary.Visible = false;
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(694, 40);
			this.lblInstructions.TabIndex = 3;
			this.lblInstructions.Text = resources.GetString("lblInstructions.Text");
			// 
			// lblSummary
			// 
			this.lblSummary.Location = new System.Drawing.Point(30, 622);
			this.lblSummary.Name = "lblSummary";
			this.lblSummary.Size = new System.Drawing.Size(70, 16);
			this.lblSummary.TabIndex = 9;
			this.lblSummary.Text = "SUMMARY";
			this.lblSummary.Visible = false;
			// 
			// lblDetail
			// 
			this.lblDetail.Location = new System.Drawing.Point(30, 262);
			this.lblDetail.Name = "lblDetail";
			this.lblDetail.Size = new System.Drawing.Size(50, 16);
			this.lblDetail.TabIndex = 8;
			this.lblDetail.Text = "DETAIL";
			this.lblDetail.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmPrintFleet
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPrintFleet";
			this.Text = " Fleet Registration List";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Activated += new System.EventHandler(this.frmPrintFleet_Activated);
			this.Resize += new System.EventHandler(this.frmPrintFleet_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintFleet_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDisplay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSummary)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}