//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmSetupAuditArchiveReport.
	/// </summary>
	partial class frmSetupAuditArchiveReport
	{
		public fecherFoundation.FCComboBox cmbVINSelected;
		public fecherFoundation.FCComboBox cmbDateAll;
		public fecherFoundation.FCComboBox cmbScreenAll;
		public fecherFoundation.FCComboBox cmbPlateAll;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCFrame fraSelectedVIN;
		public fecherFoundation.FCComboBox cboVIN;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame fraSelectedScreen;
		public fecherFoundation.FCComboBox cboScreen;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraSelectedPlate;
		public fecherFoundation.FCComboBox cboPlate;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbVINSelected = new fecherFoundation.FCComboBox();
            this.cmbDateAll = new fecherFoundation.FCComboBox();
            this.cmbScreenAll = new fecherFoundation.FCComboBox();
            this.cmbPlateAll = new fecherFoundation.FCComboBox();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.fraSelectedVIN = new fecherFoundation.FCFrame();
            this.cboVIN = new fecherFoundation.FCComboBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.txtLowDate = new Global.T2KDateBox();
            this.txtHighDate = new Global.T2KDateBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.fraSelectedScreen = new fecherFoundation.FCFrame();
            this.cboScreen = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.fraSelectedPlate = new fecherFoundation.FCFrame();
            this.cboPlate = new fecherFoundation.FCComboBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFilePreview = new fecherFoundation.FCButton();
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedVIN)).BeginInit();
            this.fraSelectedVIN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedScreen)).BeginInit();
            this.fraSelectedScreen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedPlate)).BeginInit();
            this.fraSelectedPlate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFilePreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 418);
            this.BottomPanel.Size = new System.Drawing.Size(752, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(752, 358);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFilePrint);
            this.TopPanel.Size = new System.Drawing.Size(752, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(238, 30);
            this.HeaderText.Text = "Audit Archive Report";
            // 
            // cmbVINSelected
            // 
            this.cmbVINSelected.AutoSize = false;
            this.cmbVINSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbVINSelected.FormattingEnabled = true;
            this.cmbVINSelected.Items.AddRange(new object[] {
            "Selected",
            "All"});
            this.cmbVINSelected.Location = new System.Drawing.Point(20, 30);
            this.cmbVINSelected.Name = "cmbVINSelected";
            this.cmbVINSelected.Size = new System.Drawing.Size(292, 40);
            this.cmbVINSelected.TabIndex = 20;
            this.cmbVINSelected.Text = "All";
            this.cmbVINSelected.SelectedIndexChanged += new System.EventHandler(this.cmbVINSelected_SelectedIndexChanged);
            // 
            // cmbDateAll
            // 
            this.cmbDateAll.AutoSize = false;
            this.cmbDateAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDateAll.FormattingEnabled = true;
            this.cmbDateAll.Items.AddRange(new object[] {
            "All",
            "Date Range"});
            this.cmbDateAll.Location = new System.Drawing.Point(20, 30);
            this.cmbDateAll.Name = "cmbDateAll";
            this.cmbDateAll.Size = new System.Drawing.Size(292, 40);
            this.cmbDateAll.TabIndex = 14;
            this.cmbDateAll.Text = "All";
            this.cmbDateAll.SelectedIndexChanged += new System.EventHandler(this.cmbDateAll_SelectedIndexChanged);
            // 
            // cmbScreenAll
            // 
            this.cmbScreenAll.AutoSize = false;
            this.cmbScreenAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbScreenAll.FormattingEnabled = true;
            this.cmbScreenAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbScreenAll.Location = new System.Drawing.Point(20, 30);
            this.cmbScreenAll.Name = "cmbScreenAll";
            this.cmbScreenAll.Size = new System.Drawing.Size(292, 40);
            this.cmbScreenAll.TabIndex = 8;
            this.cmbScreenAll.Text = "All";
            this.cmbScreenAll.SelectedIndexChanged += new System.EventHandler(this.cmbScreenAll_SelectedIndexChanged);
            // 
            // cmbPlateAll
            // 
            this.cmbPlateAll.AutoSize = false;
            this.cmbPlateAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPlateAll.FormattingEnabled = true;
            this.cmbPlateAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbPlateAll.Location = new System.Drawing.Point(20, 30);
            this.cmbPlateAll.Name = "cmbPlateAll";
            this.cmbPlateAll.Size = new System.Drawing.Size(292, 40);
            this.cmbPlateAll.TabIndex = 3;
            this.cmbPlateAll.Text = "All";
            this.cmbPlateAll.SelectedIndexChanged += new System.EventHandler(this.cmbPlateAll_SelectedIndexChanged);
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.fraSelectedVIN);
            this.Frame4.Controls.Add(this.cmbVINSelected);
            this.Frame4.Location = new System.Drawing.Point(392, 30);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(332, 150);
            this.Frame4.TabIndex = 17;
            this.Frame4.Text = "Vin";
            // 
            // fraSelectedVIN
            // 
            this.fraSelectedVIN.AppearanceKey = "groupBoxNoBorders";
            this.fraSelectedVIN.Controls.Add(this.cboVIN);
            this.fraSelectedVIN.Enabled = false;
            this.fraSelectedVIN.Location = new System.Drawing.Point(20, 90);
            this.fraSelectedVIN.Name = "fraSelectedVIN";
            this.fraSelectedVIN.Size = new System.Drawing.Size(292, 40);
            this.fraSelectedVIN.TabIndex = 19;
            // 
            // cboVIN
            // 
            this.cboVIN.AutoSize = false;
            this.cboVIN.BackColor = System.Drawing.SystemColors.Window;
            this.cboVIN.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboVIN.Enabled = false;
            this.cboVIN.FormattingEnabled = true;
            this.cboVIN.Location = new System.Drawing.Point(0, 0);
            this.cboVIN.Name = "cboVIN";
            this.cboVIN.Size = new System.Drawing.Size(292, 40);
            this.cboVIN.TabIndex = 20;
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.fraDateRange);
            this.Frame3.Controls.Add(this.cmbDateAll);
            this.Frame3.Location = new System.Drawing.Point(392, 200);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(332, 150);
            this.Frame3.TabIndex = 10;
            this.Frame3.Text = "Date Range";
            // 
            // fraDateRange
            // 
            this.fraDateRange.AppearanceKey = "groupBoxNoBorders";
            this.fraDateRange.Controls.Add(this.txtLowDate);
            this.fraDateRange.Controls.Add(this.txtHighDate);
            this.fraDateRange.Controls.Add(this.Label2);
            this.fraDateRange.Enabled = false;
            this.fraDateRange.Location = new System.Drawing.Point(20, 90);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(292, 40);
            this.fraDateRange.TabIndex = 13;
            // 
            // txtLowDate
            // 
            this.txtLowDate.Enabled = false;
            this.txtLowDate.Location = new System.Drawing.Point(0, 0);
            this.txtLowDate.Mask = "##/##/####";
            this.txtLowDate.Name = "txtLowDate";
            this.txtLowDate.Size = new System.Drawing.Size(115, 40);
            this.txtLowDate.TabIndex = 14;
            this.txtLowDate.Text = "  /  /";
            // 
            // txtHighDate
            // 
            this.txtHighDate.Enabled = false;
            this.txtHighDate.Location = new System.Drawing.Point(156, 0);
            this.txtHighDate.Mask = "##/##/####";
            this.txtHighDate.Name = "txtHighDate";
            this.txtHighDate.Size = new System.Drawing.Size(115, 40);
            this.txtHighDate.TabIndex = 15;
            this.txtHighDate.Text = "  /  /";
            // 
            // Label2
            // 
            this.Label2.Enabled = false;
            this.Label2.Location = new System.Drawing.Point(123, 14);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(23, 22);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "TO";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.fraSelectedScreen);
            this.Frame2.Controls.Add(this.cmbScreenAll);
            this.Frame2.Location = new System.Drawing.Point(30, 200);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(332, 150);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Screen";
            // 
            // fraSelectedScreen
            // 
            this.fraSelectedScreen.AppearanceKey = "groupBoxNoBorders";
            this.fraSelectedScreen.Controls.Add(this.cboScreen);
            this.fraSelectedScreen.Enabled = false;
            this.fraSelectedScreen.Location = new System.Drawing.Point(20, 90);
            this.fraSelectedScreen.Name = "fraSelectedScreen";
            this.fraSelectedScreen.Size = new System.Drawing.Size(292, 40);
            this.fraSelectedScreen.TabIndex = 7;
            // 
            // cboScreen
            // 
            this.cboScreen.AutoSize = false;
            this.cboScreen.BackColor = System.Drawing.SystemColors.Window;
            this.cboScreen.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboScreen.Enabled = false;
            this.cboScreen.FormattingEnabled = true;
            this.cboScreen.Location = new System.Drawing.Point(0, 0);
            this.cboScreen.Name = "cboScreen";
            this.cboScreen.Size = new System.Drawing.Size(292, 40);
            this.cboScreen.TabIndex = 8;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.fraSelectedPlate);
            this.Frame1.Controls.Add(this.cmbPlateAll);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(332, 150);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Plate";
            // 
            // fraSelectedPlate
            // 
            this.fraSelectedPlate.AppearanceKey = "groupBoxNoBorders";
            this.fraSelectedPlate.Controls.Add(this.cboPlate);
            this.fraSelectedPlate.Enabled = false;
            this.fraSelectedPlate.Location = new System.Drawing.Point(20, 90);
            this.fraSelectedPlate.Name = "fraSelectedPlate";
            this.fraSelectedPlate.Size = new System.Drawing.Size(292, 40);
            this.fraSelectedPlate.TabIndex = 2;
            // 
            // cboPlate
            // 
            this.cboPlate.AutoSize = false;
            this.cboPlate.BackColor = System.Drawing.SystemColors.Window;
            this.cboPlate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPlate.Enabled = false;
            this.cboPlate.FormattingEnabled = true;
            this.cboPlate.Location = new System.Drawing.Point(0, 0);
            this.cboPlate.Name = "cboPlate";
            this.cboPlate.Size = new System.Drawing.Size(292, 40);
            this.cboPlate.TabIndex = 4;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 1;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdFilePreview
            // 
            this.cmdFilePreview.AppearanceKey = "acceptButton";
            this.cmdFilePreview.Location = new System.Drawing.Point(300, 30);
            this.cmdFilePreview.Name = "cmdFilePreview";
            this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFilePreview.Size = new System.Drawing.Size(144, 48);
            this.cmdFilePreview.TabIndex = 0;
            this.cmdFilePreview.Text = "Print Preview";
            this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrint.AppearanceKey = "toolbarButton";
            this.cmdFilePrint.Location = new System.Drawing.Point(678, 29);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdFilePrint.Size = new System.Drawing.Size(46, 24);
            this.cmdFilePrint.TabIndex = 1;
            this.cmdFilePrint.Text = "Print";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // frmSetupAuditArchiveReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(752, 526);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSetupAuditArchiveReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Audit Archive Report";
            this.Load += new System.EventHandler(this.frmSetupAuditArchiveReport_Load);
            this.Activated += new System.EventHandler(this.frmSetupAuditArchiveReport_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupAuditArchiveReport_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedVIN)).EndInit();
            this.fraSelectedVIN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedScreen)).EndInit();
            this.fraSelectedScreen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedPlate)).EndInit();
            this.fraSelectedPlate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdFilePreview;
        private FCButton cmdFilePrint;
    }
}