//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmVehicleHistory : BaseForm
	{
		public frmVehicleHistory()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVehicleHistory InstancePtr
		{
			get
			{
				return (frmVehicleHistory)Sys.GetInstance(typeof(frmVehicleHistory));
			}
		}

		protected frmVehicleHistory _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsResults = new clsDRWrapper();
		int intIndex;
		public int ClassCol;
		public int PlateCol;
		public int MVR3Col;
		public int OwnerCol;
		public int YearCol;
		public int MakeCol;
		public int ModelCol;
		public int ExpiresCol;
		public int TransTypeCol;
		public int VINCol;
		public int TransDateCol;
		public int VoidedCol;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraMulti.Visible = false;
			cmdSearch.Enabled = true;
			cmdClear.Enabled = true;
			cmbSearchType.Enabled = true;
			lblSearchInfo.Enabled = true;
			txtSearch.Enabled = true;
			txtSearch.Focus();
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			txtSearch.Text = "";
			vsResults.Rows = 1;
			txtSearch.Focus();
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			fraMulti.Visible = false;
			VehicleSearch(ref intIndex, vsMulti.TextMatrix(vsMulti.Row, 0), false);
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			vsResults.Rows = 1;
			VehicleSearch(ref intIndex, fecherFoundation.Strings.Trim(txtSearch.Text), true);
		}

		private void frmVehicleHistory_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmVehicleHistory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVehicleHistory properties;
			//frmVehicleHistory.FillStyle	= 0;
			//frmVehicleHistory.ScaleWidth	= 9045;
			//frmVehicleHistory.ScaleHeight	= 7290;
			//frmVehicleHistory.LinkTopic	= "Form2";
			//frmVehicleHistory.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			intIndex = 0;
			OwnerCol = 0;
			ClassCol = 1;
			PlateCol = 2;
			YearCol = 3;
			MakeCol = 4;
			ModelCol = 5;
			VINCol = 6;
			ExpiresCol = 7;
			MVR3Col = 8;
			VoidedCol = 9;
			TransDateCol = 10;
			TransTypeCol = 11;
			FormatGrid();
			vsMulti.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "STAAB AGENCY" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				cmbAnnual.Visible = true;
                lblAnnual.Visible = true;
			}
		}

		private void frmVehicleHistory_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				if (FCGlobal.Screen.ActiveControl is FCTextBox)
				{
					KeyAscii = KeyAscii - 32;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void VehicleSearch(ref int intSearchIndex, string strCriteria, bool blnLikeSearch)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cmdSearch.Enabled = false;
				cmdClear.Enabled = false;
				cmbSearchType.Enabled = false;
				lblSearchInfo.Enabled = false;
				txtSearch.Enabled = false;
				if (cmbAnnual.Text == "Annual")
				{
					if (intSearchIndex == 0)
					{
						if (blnLikeSearch)
						{
							rsResults.OpenRecordset("SELECT DISTINCT VIN FROM ActivityMaster WHERE VIN Like '%" + strCriteria + "%'");
							if (rsResults.EndOfFile() != true && rsResults.BeginningOfFile() != true)
							{
								if (rsResults.RecordCount() > 1)
								{
									ShowMultiples(ref intSearchIndex);
									return;
								}
								else
								{
									VehicleSearch(ref intSearchIndex, rsResults.Get_Fields_String("VIN"), false);
								}
							}
							else
							{
								MessageBox.Show("No matches found for this VIN.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							rsResults.OpenRecordset("SELECT * FROM ActivityMaster WHERE VIN = '" + strCriteria + "' ORDER BY VIN, ExpireDate DESC, DateUpdated DESC");
							FillGrid();
						}
					}
					else
					{
						if (blnLikeSearch)
						{
							rsResults.OpenRecordset("SELECT DISTINCT Plate FROM ActivityMaster WHERE Plate Like '%" + strCriteria + "%' OR PlateStripped Like '%" + strCriteria + "%'");
							if (rsResults.EndOfFile() != true && rsResults.BeginningOfFile() != true)
							{
								if (rsResults.RecordCount() > 1)
								{
									ShowMultiples(ref intSearchIndex);
									return;
								}
								else
								{
									VehicleSearch(ref intSearchIndex, rsResults.Get_Fields_String("Plate"), false);
								}
							}
							else
							{
								MessageBox.Show("No matches found for this Plate.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							rsResults.OpenRecordset("SELECT * FROM ActivityMaster WHERE Plate = '" + strCriteria + "' ORDER BY Plate, ExpireDate DESC, DateUpdated DESC");
							FillGrid();
						}
					}
				}
				else
				{
					if (intSearchIndex == 0)
					{
						if (blnLikeSearch)
						{
							rsResults.OpenRecordset("SELECT DISTINCT VIN FROM LongTermTrailerRegistrations WHERE VIN Like '%" + strCriteria + "%'");
							if (rsResults.EndOfFile() != true && rsResults.BeginningOfFile() != true)
							{
								if (rsResults.RecordCount() > 1)
								{
									ShowMultiples(ref intSearchIndex);
									return;
								}
								else
								{
									VehicleSearch(ref intSearchIndex, rsResults.Get_Fields_String("VIN"), false);
								}
							}
							else
							{
								MessageBox.Show("No matches found for this VIN.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							rsResults.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE VIN = '" + strCriteria + "' ORDER BY VIN, ExpireDate DESC, DateUpdated DESC");
							FillGrid();
						}
					}
					else
					{
						if (blnLikeSearch)
						{
							rsResults.OpenRecordset("SELECT DISTINCT Plate FROM LongTermTrailerRegistrations WHERE Plate Like '%" + strCriteria + "%'");
							if (rsResults.EndOfFile() != true && rsResults.BeginningOfFile() != true)
							{
								if (rsResults.RecordCount() > 1)
								{
									ShowMultiples(ref intSearchIndex);
									return;
								}
								else
								{
									VehicleSearch(ref intSearchIndex, rsResults.Get_Fields_String("Plate"), false);
								}
							}
							else
							{
								MessageBox.Show("No matches found for this Plate.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							rsResults.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE Plate = '" + strCriteria + "' ORDER BY Plate, ExpireDate DESC, DateUpdated DESC");
							FillGrid();
						}
					}
				}
				cmdSearch.Enabled = true;
				cmdClear.Enabled = true;
				cmbSearchType.Enabled = true;
				lblSearchInfo.Enabled = true;
				txtSearch.Enabled = true;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				cmdSearch.Enabled = true;
				cmdClear.Enabled = true;
				cmbSearchType.Enabled = true;
				lblSearchInfo.Enabled = true;
				txtSearch.Enabled = true;
			}
		}

		private void frmVehicleHistory_Resize(object sender, System.EventArgs e)
		{
			vsResults.ColWidth(OwnerCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.205));
			vsResults.ColWidth(ClassCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(PlateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(YearCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.065));
			vsResults.ColWidth(MakeCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.065));
			vsResults.ColWidth(ModelCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(VINCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.195));
			vsResults.ColWidth(MVR3Col, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(ExpiresCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.115));
			vsResults.ColWidth(TransTypeCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.15));
			vsResults.ColWidth(TransDateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.115));
			vsResults.ColWidth(VoidedCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.065));

            fraMulti.CenterToContainer(this.ClientArea);
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (vsResults.Rows > 1)
			{
				frmReportViewer.InstancePtr.Init(rptVehicleHistory.InstancePtr);
			}
			else
			{
				MessageBox.Show("You must have results before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (vsResults.Rows > 1)
			{
				rptVehicleHistory.InstancePtr.PrintReport(true);
			}
			else
			{
				MessageBox.Show("You must have results before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optSearchType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intIndex = Index;
			cmdClear_Click();
			txtSearch.Focus();
		}

		private void optSearchType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			optSearchType_CheckedChanged(index, sender, e);
		}

		private void ShowMultiples(ref int intSearch)
		{
			vsMulti.Rows = 1;
			if (intSearch == 0)
			{
				vsMulti.TextMatrix(0, 0, "VIN");
				do
				{
					vsMulti.Rows += 1;
					vsMulti.TextMatrix(vsMulti.Rows - 1, 0, FCConvert.ToString(rsResults.Get_Fields_String("VIN")));
					rsResults.MoveNext();
				}
				while (rsResults.EndOfFile() != true);
			}
			else
			{
				vsMulti.TextMatrix(0, 0, "Plate");
				do
				{
					vsMulti.Rows += 1;
					vsMulti.TextMatrix(vsMulti.Rows - 1, 0, FCConvert.ToString(rsResults.Get_Fields_String("Plate")));
					rsResults.MoveNext();
				}
				while (rsResults.EndOfFile() != true);
			}
			fraMulti.Visible = true;
			vsMulti.Row = 1;
		}

		private void vsMulti_DblClick(object sender, System.EventArgs e)
		{
			fraMulti.Visible = false;
			VehicleSearch(ref intIndex, vsMulti.TextMatrix(vsMulti.MouseRow, 0), false);
		}

		private void FillGrid()
		{
			clsDRWrapper rsParty = new clsDRWrapper();
			vsResults.Rows = 1;
			do
			{
				vsResults.Rows += 1;
				if (cmbAnnual.Text == "Annual")
				{
					rsParty.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsResults.Get_Fields_Int32("PartyID1"), "CentralParties");
					if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
					{
						// If rsResults.Fields("OwnerCode1") = "I" Then
						// vsResults.TextMatrix(vsResults.Rows - 1, OwnerCol) = rsResults.Fields("Reg1FirstName") & " " & Trim(rsResults.Fields("Reg1MI") & " " & rsResults.Fields("Reg1LastName"))
						// Else
						vsResults.TextMatrix(vsResults.Rows - 1, OwnerCol, fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FullName"))));
						// End If
					}
					else
					{
						vsResults.TextMatrix(vsResults.Rows - 1, OwnerCol, "UNKNOWN");
					}
					vsResults.TextMatrix(vsResults.Rows - 1, ClassCol, rsResults.Get_Fields_String("Class") + " / " + rsResults.Get_Fields_String("Subclass"));
					vsResults.TextMatrix(vsResults.Rows - 1, MVR3Col, FCConvert.ToString(rsResults.Get_Fields_Int32("MVR3")));
					vsResults.TextMatrix(vsResults.Rows - 1, ModelCol, FCConvert.ToString(rsResults.Get_Fields_String("Model")));
					string vbPorterVar = rsResults.Get_Fields("TransactionType");
					if (vbPorterVar == "NRR")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "New Reg Regular");
					}
					else if (vbPorterVar == "NRT")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "New Reg Transfer");
					}
					else if (vbPorterVar == "RRR")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Re Reg Regular");
					}
					else if (vbPorterVar == "RRT")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Re Reg Transfer");
					}
					else if (vbPorterVar == "DPR")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Dup Registration");
					}
					else if (vbPorterVar == "DPS")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Dup Sticker");
					}
					else if (vbPorterVar == "ECO")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Correction");
					}
					else if (vbPorterVar == "LPS")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Lost Pl / St");
					}
					else if (vbPorterVar == "99")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Pre Print Data");
					}
					else if (vbPorterVar == "GVW")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Reg Veh Wght Change");
					}
					else
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "UNKNOWN");
					}
				}
				else
				{
					rsParty.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsResults.Get_Fields_Int32("PartyID1"), "CentralParties");
					if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
					{
						vsResults.TextMatrix(vsResults.Rows - 1, OwnerCol, fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FullName"))));
					}
					else
					{
						vsResults.TextMatrix(vsResults.Rows - 1, OwnerCol, "UNKNOWN");
					}
					vsResults.TextMatrix(vsResults.Rows - 1, ClassCol, "TL / L9");
					vsResults.TextMatrix(vsResults.Rows - 1, MVR3Col, "");
					vsResults.TextMatrix(vsResults.Rows - 1, ModelCol, "");
					string vbPorterVar1 = rsResults.Get_Fields_String("RegistrationType");
					if (vbPorterVar1 == "NRR")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "New Reg Regular");
					}
					else if (vbPorterVar1 == "NRT")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "New Reg Transfer");
					}
					else if (vbPorterVar1 == "RRR")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Re Reg Regular");
					}
					else if (vbPorterVar1 == "RRT")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Re Reg Transfer");
					}
					else if (vbPorterVar1 == "DPR")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Dup Registration");
					}
					else if (vbPorterVar1 == "DPS")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Dup Sticker");
					}
					else if (vbPorterVar1 == "ECO")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Correction");
					}
					else if (vbPorterVar1 == "LPS")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Lost Pl / St");
					}
					else if (vbPorterVar1 == "99")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Pre Print Data");
					}
					else if (vbPorterVar1 == "GVW")
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "Reg Veh Wght Change");
					}
					else
					{
						vsResults.TextMatrix(vsResults.Rows - 1, TransTypeCol, "UNKNOWN");
					}
				}
				vsResults.TextMatrix(vsResults.Rows - 1, PlateCol, FCConvert.ToString(rsResults.Get_Fields_String("Plate")));
				vsResults.TextMatrix(vsResults.Rows - 1, YearCol, FCConvert.ToString(rsResults.Get_Fields("Year")));
				vsResults.TextMatrix(vsResults.Rows - 1, MakeCol, FCConvert.ToString(rsResults.Get_Fields_String("Make")));
				vsResults.TextMatrix(vsResults.Rows - 1, VINCol, FCConvert.ToString(rsResults.Get_Fields_String("VIN")));
				if (FCConvert.ToString(rsResults.Get_Fields_String("Status")) == "V")
				{
					vsResults.TextMatrix(vsResults.Rows - 1, VoidedCol, FCConvert.ToString(true));
				}
				else
				{
					vsResults.TextMatrix(vsResults.Rows - 1, VoidedCol, FCConvert.ToString(false));
				}
				vsResults.TextMatrix(vsResults.Rows - 1, ExpiresCol, Strings.Format(rsResults.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy"));
				vsResults.TextMatrix(vsResults.Rows - 1, TransDateCol, Strings.Format(rsResults.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy"));
				rsResults.MoveNext();
			}
			while (rsResults.EndOfFile() != true);
            vsResults.ColAlignment(VoidedCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		private void FormatGrid()
		{
			vsResults.TextMatrix(0, OwnerCol, "Owner");
			vsResults.TextMatrix(0, ClassCol, "Class");
			vsResults.TextMatrix(0, PlateCol, "Plate");
			vsResults.TextMatrix(0, YearCol, "Year");
			vsResults.TextMatrix(0, MakeCol, "Make");
			vsResults.TextMatrix(0, ModelCol, "Model");
			vsResults.TextMatrix(0, VINCol, "VIN");
			vsResults.TextMatrix(0, ExpiresCol, "Expires");
			vsResults.TextMatrix(0, TransTypeCol, "Reg Type");
			vsResults.TextMatrix(0, MVR3Col, "MVR3");
			vsResults.TextMatrix(0, TransDateCol, "Reg Date");
			vsResults.TextMatrix(0, VoidedCol, "Void");
			vsResults.ColWidth(OwnerCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.205));
			vsResults.ColWidth(ClassCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(PlateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(YearCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.065));
			vsResults.ColWidth(MakeCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.065));
			vsResults.ColWidth(ModelCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(VINCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.195));
			vsResults.ColWidth(MVR3Col, FCConvert.ToInt32(vsResults.WidthOriginal * 0.085));
			vsResults.ColWidth(ExpiresCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.115));
			vsResults.ColWidth(TransTypeCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.15));
			vsResults.ColWidth(TransDateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.115));
			vsResults.ColWidth(VoidedCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.065));
			vsResults.ColAlignment(OwnerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(ClassCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(MakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(VINCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(ExpiresCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(TransTypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(MVR3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(TransDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColAlignment(VoidedCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColDataType(VoidedCol, FCGrid.DataTypeSettings.flexDTBoolean);
		}
	}
}
