//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmDataInput.
	/// </summary>
	partial class frmDataInput
	{
		public fecherFoundation.FCComboBox cmbYes;
		public fecherFoundation.FCLabel lblYes;
		public fecherFoundation.FCFrame fraAdditionalInfo;
		public fecherFoundation.FCCheckBox chkOverrideFleetEmail;
		public fecherFoundation.FCFrame fraUserFields;
		public Global.T2KOverTypeBox txtUserReason1;
		public Global.T2KOverTypeBox txtUserField1;
		public Global.T2KOverTypeBox txtUserField2;
		public Global.T2KOverTypeBox txtUserField3;
		public Global.T2KOverTypeBox txtUserReason2;
		public Global.T2KOverTypeBox txtUserReason3;
		public fecherFoundation.FCLabel lblInfo3;
		public fecherFoundation.FCLabel lblInfo2;
		public fecherFoundation.FCLabel lblInfo1;
		public fecherFoundation.FCLabel lblUserField3;
		public fecherFoundation.FCLabel lblUserField2;
		public fecherFoundation.FCLabel lblUserField1;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCButton cmdErase;
		public Global.T2KOverTypeBox txtMessage;
		public fecherFoundation.FCComboBox cboMessageCodes;
		public fecherFoundation.FCLabel lblMesageCode;
		public fecherFoundation.FCLabel lblMessage;
		public Global.T2KOverTypeBox txtEMail;
		public fecherFoundation.FCLabel lblEMail;
		public fecherFoundation.FCPanel Frame1;
		public fecherFoundation.FCFrame fraTitleFee;
		public fecherFoundation.FCCheckBox chkNoFeeCTA;
		public fecherFoundation.FCCheckBox chkCTAPaid;
		public fecherFoundation.FCCheckBox chkCC;
		public fecherFoundation.FCFrame fraSalesTax;
		public fecherFoundation.FCCheckBox chkSalesTaxExempt;
		public fecherFoundation.FCCheckBox chkDealerSalesTax;
		public fecherFoundation.FCCheckBox chkBaseIsSalePrice;
		public fecherFoundation.FCTextBox txtReg2ICM;
		public fecherFoundation.FCTextBox txtReg1ICM;
		public fecherFoundation.FCTextBox txtReg3ICM;
		public fecherFoundation.FCTextBox txtReg3LR;
		public fecherFoundation.FCTextBox txtReg1LR;
		public fecherFoundation.FCTextBox txtReg2LR;
		public fecherFoundation.FCTextBox txtReg1PartyID;
		public fecherFoundation.FCTextBox txtReg2PartyID;
		public fecherFoundation.FCTextBox txtReg3PartyID;
		public fecherFoundation.FCTextBox txtCountry;
		public fecherFoundation.FCTextBox txtResCountry;
		public fecherFoundation.FCComboBox cboBattle;
		public fecherFoundation.FCCheckBox chkTransferExempt;
		public fecherFoundation.FCCheckBox chkRental;
		public fecherFoundation.FCButton cmdMoreInfo;
		public fecherFoundation.FCCheckBox chkEAP;
		public fecherFoundation.FCTextBox txtYStickerNumber;
		public fecherFoundation.FCTextBox txtMStickerNumber;
		public Global.T2KOverTypeBox txtCTANumber;
		public Global.T2KOverTypeBox txtZip;
		public fecherFoundation.FCTextBox txtPriorTitleState;
		public fecherFoundation.FCTextBox txtCreditNumber;
		public fecherFoundation.FCCheckBox chkVIN;
		public fecherFoundation.FCTextBox txtResState;
		public fecherFoundation.FCCheckBox chkAmputeeVet;
		public fecherFoundation.FCTextBox txtFleetNumber;
		public fecherFoundation.FCPictureBox imgFleetComment;
		public fecherFoundation.FCCheckBox chkTrailerVanity;
		public fecherFoundation.FCFrame fraHalfRate;
		public fecherFoundation.FCCheckBox chkHalfRateLocal;
		public fecherFoundation.FCCheckBox chkHalfRateState;
		public fecherFoundation.FCCheckBox chkExciseExempt;
		public fecherFoundation.FCCheckBox chkAgentFeeExempt;
		public fecherFoundation.FCCheckBox chkStateExempt;
		public fecherFoundation.FCCheckBox chk2Year;
		public fecherFoundation.FCCheckBox chkETO;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtMileage;
		public fecherFoundation.FCTextBox txtMilYear;
		public fecherFoundation.FCTextBox txtYearNoCharge;
		public fecherFoundation.FCTextBox txtMonthNoCharge;
		public fecherFoundation.FCTextBox txtYearCharge;
		public fecherFoundation.FCTextBox txtMonthCharge;
		public fecherFoundation.FCTextBox txtPreviousTitle;
		public fecherFoundation.FCTextBox txtMilRate;
		public fecherFoundation.FCTextBox txtFuel;
		public fecherFoundation.FCTextBox txtRegWeight;
		public fecherFoundation.FCTextBox txtNetWeight;
		public fecherFoundation.FCTextBox txtRegistrationNumber;
		public fecherFoundation.FCTextBox txtClass;
		public Global.T2KOverTypeBox txtVin;
		public Global.T2KBackFillWhole txtBase;
		public Global.T2KBackFillDecimal txtAgentFee;
		public Global.T2KBackFillDecimal txtTownCredit;
		public Global.T2KBackFillDecimal txtBalance;
		public Global.T2KBackFillDecimal txtSubTotal;
		public Global.T2KBackFillDecimal txtTransferCharge;
		public Global.T2KBackFillDecimal txtTitle;
		public Global.T2KBackFillDecimal txtSalesTax;
		public Global.T2KBackFillDecimal txtRate;
		public Global.T2KBackFillDecimal txtFees;
		public Global.T2KBackFillDecimal txtStateCredit;
		public Global.T2KDateBox txtExpires;
		public Global.T2KDateBox txtEffectiveDate;
		public Global.T2KOverTypeBox txtYear;
		public Global.T2KOverTypeBox txtMake;
		public Global.T2KOverTypeBox txtModel;
		public Global.T2KOverTypeBox txtColor1;
		public Global.T2KOverTypeBox txtColor2;
		public Global.T2KOverTypeBox txtStyle;
		public Global.T2KOverTypeBox txtTires;
		public Global.T2KOverTypeBox txtAxles;
		public Global.T2KOverTypeBox txtUnitNumber;
		public Global.T2KOverTypeBox txtDOTNumber;
		public Global.T2KOverTypeBox txtAddress1;
		public Global.T2KOverTypeBox txtLegalres;
		public Global.T2KOverTypeBox txtresidenceCode;
		public Global.T2KOverTypeBox txtCity;
		public Global.T2KOverTypeBox txtAddress2;
		public Global.T2KOverTypeBox txtLegalResStreet;
		public Global.T2KOverTypeBox txtDoubleCTANumber;
		public Global.T2KOverTypeBox txtTaxIDNumber;
		public Global.T2KDateBox txtReg2DOB;
		public Global.T2KDateBox txtReg1DOB;
		public Global.T2KDateBox txtReg3DOB;
		public fecherFoundation.FCLabel lblCreditNumberAP;
		public fecherFoundation.FCLabel lblStateFeesHR;
		public fecherFoundation.FCLabel lblStateFeesPR;
		public fecherFoundation.FCLabel lblRegistrant3;
		public fecherFoundation.FCLabel lblDOBID;
		public fecherFoundation.FCLabel Label54;
		public fecherFoundation.FCLabel lblTaxID;
		public fecherFoundation.FCLabel lblStateCreditPR;
		public fecherFoundation.FCLabel lblStateRatePR;
		public fecherFoundation.FCLabel lblExciseTaxPR;
		public fecherFoundation.FCLabel lblLocalSubPR;
		public fecherFoundation.FCLabel lblTownCreditPR;
		public fecherFoundation.FCLabel lblLocalBalancePR;
		public fecherFoundation.FCLabel lblCreditRebate;
		public fecherFoundation.FCLabel Label48;
		public fecherFoundation.FCLabel Label47;
		public fecherFoundation.FCLabel Label38;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCLabel lblExciseDiff;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel lblBattle;
		public fecherFoundation.FCLabel lblLocalBalanceHR;
		public fecherFoundation.FCLabel lblTownCreditHR;
		public fecherFoundation.FCLabel lblStateRateHR;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel lblYSD;
		public fecherFoundation.FCLabel lblMSD;
		public fecherFoundation.FCLabel lblLocalSubHR;
		public fecherFoundation.FCLabel lblStateCreditHR;
		public fecherFoundation.FCLabel lblExciseTaxHR;
		public fecherFoundation.FCLabel lblBalanceAP;
		public fecherFoundation.FCLabel lblTotalDue;
		public fecherFoundation.FCLabel lblCalculate;
		public fecherFoundation.FCLabel lblTransferAP;
		public fecherFoundation.FCLabel lblSubTotalAP;
		public fecherFoundation.FCLabel lblCreditAP;
		public fecherFoundation.FCLabel lblExciseAP;
		public fecherFoundation.FCLabel Label52;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblNumberofYear;
		public fecherFoundation.FCLabel lblNumberofMonth;
		public fecherFoundation.FCLabel Label51;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label50;
		public fecherFoundation.FCLabel Label45;
		public fecherFoundation.FCLabel Label44;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel Label53;
		public fecherFoundation.FCLabel lblUseTax;
		public fecherFoundation.FCLabel lblFees;
		public fecherFoundation.FCLabel lblStateCredit;
		public fecherFoundation.FCLabel Label46;
		public fecherFoundation.FCLabel lblCTA1;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label39;
		public fecherFoundation.FCLabel lblExciseTax;
		public fecherFoundation.FCLabel Label37;
		public fecherFoundation.FCLabel Label36;
		public fecherFoundation.FCLabel Label35;
		public fecherFoundation.FCLabel Label33;
		public fecherFoundation.FCLabel Label32;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCLabel Label30;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuOpenFields;
		public fecherFoundation.FCToolStripMenuItem mnuCTA;
		public fecherFoundation.FCToolStripMenuItem mnuUseTax;
		public fecherFoundation.FCToolStripMenuItem mnuProcessRedBook;
		public fecherFoundation.FCToolStripMenuItem mnuCalculate;
		public fecherFoundation.FCToolStripMenuItem mnuFleetGroupComment;
		public fecherFoundation.FCToolStripMenuItem mnuPreview;
		public fecherFoundation.FCToolStripMenuItem Separtor1;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataInput));
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            this.cmbYes = new fecherFoundation.FCComboBox();
            this.lblYes = new fecherFoundation.FCLabel();
            this.fraAdditionalInfo = new fecherFoundation.FCFrame();
            this.fraUserFields = new fecherFoundation.FCFrame();
            this.txtUserReason1 = new Global.T2KOverTypeBox();
            this.txtUserField1 = new Global.T2KOverTypeBox();
            this.txtUserField2 = new Global.T2KOverTypeBox();
            this.txtUserField3 = new Global.T2KOverTypeBox();
            this.txtUserReason2 = new Global.T2KOverTypeBox();
            this.txtUserReason3 = new Global.T2KOverTypeBox();
            this.lblInfo3 = new fecherFoundation.FCLabel();
            this.lblInfo2 = new fecherFoundation.FCLabel();
            this.lblInfo1 = new fecherFoundation.FCLabel();
            this.lblUserField3 = new fecherFoundation.FCLabel();
            this.lblUserField2 = new fecherFoundation.FCLabel();
            this.lblUserField1 = new fecherFoundation.FCLabel();
            this.chkOverrideFleetEmail = new fecherFoundation.FCCheckBox();
            this.cmdDone = new fecherFoundation.FCButton();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.cmdErase = new fecherFoundation.FCButton();
            this.txtMessage = new Global.T2KOverTypeBox();
            this.cboMessageCodes = new fecherFoundation.FCComboBox();
            this.lblMesageCode = new fecherFoundation.FCLabel();
            this.lblMessage = new fecherFoundation.FCLabel();
            this.txtEMail = new Global.T2KOverTypeBox();
            this.lblEMail = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCPanel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.imgCopyToLegalAddress = new fecherFoundation.FCPictureBox();
            this.imgCopyToMailingAddress = new fecherFoundation.FCPictureBox();
            this.imgFleetGroupSearch = new fecherFoundation.FCPictureBox();
            this.imgEditRegistrant3 = new fecherFoundation.FCPictureBox();
            this.imgEditRegistrant2 = new fecherFoundation.FCPictureBox();
            this.imgEditRegistrant1 = new fecherFoundation.FCPictureBox();
            this.imgReg3Edit = new fecherFoundation.FCPictureBox();
            this.imgReg3Search = new fecherFoundation.FCPictureBox();
            this.imgReg2Edit = new fecherFoundation.FCPictureBox();
            this.imgReg2Search = new fecherFoundation.FCPictureBox();
            this.imgReg1Edit = new fecherFoundation.FCPictureBox();
            this.imgReg1Search = new fecherFoundation.FCPictureBox();
            this.lblCTA2 = new fecherFoundation.FCLabel();
            this.chkSpecialtyCorrExtraFee = new fecherFoundation.FCCheckBox();
            this.txtRegistrant3 = new fecherFoundation.FCTextBox();
            this.txtRegistrant2 = new fecherFoundation.FCTextBox();
            this.txtRegistrant1 = new fecherFoundation.FCTextBox();
            this.fraHalfRate = new fecherFoundation.FCFrame();
            this.chkHalfRateLocal = new fecherFoundation.FCCheckBox();
            this.chkHalfRateState = new fecherFoundation.FCCheckBox();
            this.fraSalesTax = new fecherFoundation.FCFrame();
            this.chkSalesTaxExempt = new fecherFoundation.FCCheckBox();
            this.chkDealerSalesTax = new fecherFoundation.FCCheckBox();
            this.fraTitleFee = new fecherFoundation.FCFrame();
            this.chkNoFeeCTA = new fecherFoundation.FCCheckBox();
            this.chkCTAPaid = new fecherFoundation.FCCheckBox();
            this.txtRate = new Global.T2KBackFillDecimal();
            this.txtExciseTax = new Global.T2KBackFillDecimal();
            this.imgFleetComment = new fecherFoundation.FCPictureBox();
            this.lblFleet = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.txtFleetNumber = new fecherFoundation.FCTextBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.txtEvidenceofInsurance = new fecherFoundation.FCTextBox();
            this.txtUserID = new fecherFoundation.FCTextBox();
            this.txtExciseTaxDate = new Global.T2KDateBox();
            this.lblStateRateHR = new fecherFoundation.FCLabel();
            this.chkCC = new fecherFoundation.FCCheckBox();
            this.chkBaseIsSalePrice = new fecherFoundation.FCCheckBox();
            this.txtReg2ICM = new fecherFoundation.FCTextBox();
            this.txtReg1ICM = new fecherFoundation.FCTextBox();
            this.txtReg3ICM = new fecherFoundation.FCTextBox();
            this.txtReg3LR = new fecherFoundation.FCTextBox();
            this.txtReg1LR = new fecherFoundation.FCTextBox();
            this.txtReg2LR = new fecherFoundation.FCTextBox();
            this.txtReg1PartyID = new fecherFoundation.FCTextBox();
            this.txtReg2PartyID = new fecherFoundation.FCTextBox();
            this.txtReg3PartyID = new fecherFoundation.FCTextBox();
            this.txtCountry = new fecherFoundation.FCTextBox();
            this.txtResCountry = new fecherFoundation.FCTextBox();
            this.cboBattle = new fecherFoundation.FCComboBox();
            this.chkTransferExempt = new fecherFoundation.FCCheckBox();
            this.chkRental = new fecherFoundation.FCCheckBox();
            this.chkEAP = new fecherFoundation.FCCheckBox();
            this.txtYStickerNumber = new fecherFoundation.FCTextBox();
            this.txtMStickerNumber = new fecherFoundation.FCTextBox();
            this.txtCTANumber = new Global.T2KOverTypeBox();
            this.txtZip = new Global.T2KOverTypeBox();
            this.txtPriorTitleState = new fecherFoundation.FCTextBox();
            this.txtCreditNumber = new fecherFoundation.FCTextBox();
            this.chkVIN = new fecherFoundation.FCCheckBox();
            this.txtResState = new fecherFoundation.FCTextBox();
            this.chkAmputeeVet = new fecherFoundation.FCCheckBox();
            this.chkTrailerVanity = new fecherFoundation.FCCheckBox();
            this.chkExciseExempt = new fecherFoundation.FCCheckBox();
            this.chkAgentFeeExempt = new fecherFoundation.FCCheckBox();
            this.chkStateExempt = new fecherFoundation.FCCheckBox();
            this.chk2Year = new fecherFoundation.FCCheckBox();
            this.chkETO = new fecherFoundation.FCCheckBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtMileage = new fecherFoundation.FCTextBox();
            this.txtMilYear = new fecherFoundation.FCTextBox();
            this.txtYearNoCharge = new fecherFoundation.FCTextBox();
            this.txtMonthNoCharge = new fecherFoundation.FCTextBox();
            this.txtYearCharge = new fecherFoundation.FCTextBox();
            this.txtMonthCharge = new fecherFoundation.FCTextBox();
            this.txtPreviousTitle = new fecherFoundation.FCTextBox();
            this.txtMilRate = new fecherFoundation.FCTextBox();
            this.txtFuel = new fecherFoundation.FCTextBox();
            this.txtRegWeight = new fecherFoundation.FCTextBox();
            this.txtNetWeight = new fecherFoundation.FCTextBox();
            this.txtRegistrationNumber = new fecherFoundation.FCTextBox();
            this.txtClass = new fecherFoundation.FCTextBox();
            this.txtVin = new Global.T2KOverTypeBox();
            this.txtBase = new Global.T2KBackFillWhole();
            this.txtAgentFee = new Global.T2KBackFillDecimal();
            this.txtTownCredit = new Global.T2KBackFillDecimal();
            this.txtBalance = new Global.T2KBackFillDecimal();
            this.txtSubTotal = new Global.T2KBackFillDecimal();
            this.txtTransferCharge = new Global.T2KBackFillDecimal();
            this.txtTitle = new Global.T2KBackFillDecimal();
            this.txtSalesTax = new Global.T2KBackFillDecimal();
            this.txtFees = new Global.T2KBackFillDecimal();
            this.txtStateCredit = new Global.T2KBackFillDecimal();
            this.txtExpires = new Global.T2KDateBox();
            this.txtEffectiveDate = new Global.T2KDateBox();
            this.txtYear = new Global.T2KOverTypeBox();
            this.txtMake = new Global.T2KOverTypeBox();
            this.txtModel = new Global.T2KOverTypeBox();
            this.txtColor1 = new Global.T2KOverTypeBox();
            this.txtColor2 = new Global.T2KOverTypeBox();
            this.txtStyle = new Global.T2KOverTypeBox();
            this.txtTires = new Global.T2KOverTypeBox();
            this.txtAxles = new Global.T2KOverTypeBox();
            this.txtUnitNumber = new Global.T2KOverTypeBox();
            this.txtDOTNumber = new Global.T2KOverTypeBox();
            this.txtAddress1 = new Global.T2KOverTypeBox();
            this.txtLegalres = new Global.T2KOverTypeBox();
            this.txtresidenceCode = new Global.T2KOverTypeBox();
            this.txtCity = new Global.T2KOverTypeBox();
            this.txtAddress2 = new Global.T2KOverTypeBox();
            this.txtLegalResStreet = new Global.T2KOverTypeBox();
            this.txtDoubleCTANumber = new Global.T2KOverTypeBox();
            this.txtTaxIDNumber = new Global.T2KOverTypeBox();
            this.txtReg2DOB = new Global.T2KDateBox();
            this.txtReg1DOB = new Global.T2KDateBox();
            this.txtReg3DOB = new Global.T2KDateBox();
            this.lblCreditNumberAP = new fecherFoundation.FCLabel();
            this.lblStateFeesHR = new fecherFoundation.FCLabel();
            this.lblStateFeesPR = new fecherFoundation.FCLabel();
            this.lblRegistrant3 = new fecherFoundation.FCLabel();
            this.lblDOBID = new fecherFoundation.FCLabel();
            this.Label54 = new fecherFoundation.FCLabel();
            this.lblTaxID = new fecherFoundation.FCLabel();
            this.lblStateRatePR = new fecherFoundation.FCLabel();
            this.lblExciseTaxPR = new fecherFoundation.FCLabel();
            this.lblLocalSubPR = new fecherFoundation.FCLabel();
            this.lblTownCreditPR = new fecherFoundation.FCLabel();
            this.lblLocalBalancePR = new fecherFoundation.FCLabel();
            this.lblCreditRebate = new fecherFoundation.FCLabel();
            this.Label48 = new fecherFoundation.FCLabel();
            this.Label47 = new fecherFoundation.FCLabel();
            this.Label38 = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label26 = new fecherFoundation.FCLabel();
            this.lblExciseDiff = new fecherFoundation.FCLabel();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.lblBattle = new fecherFoundation.FCLabel();
            this.lblLocalBalanceHR = new fecherFoundation.FCLabel();
            this.lblTownCreditHR = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.lblYSD = new fecherFoundation.FCLabel();
            this.lblMSD = new fecherFoundation.FCLabel();
            this.lblLocalSubHR = new fecherFoundation.FCLabel();
            this.lblStateCreditHR = new fecherFoundation.FCLabel();
            this.lblExciseTaxHR = new fecherFoundation.FCLabel();
            this.lblBalanceAP = new fecherFoundation.FCLabel();
            this.lblTotalDue = new fecherFoundation.FCLabel();
            this.lblCalculate = new fecherFoundation.FCLabel();
            this.lblTransferAP = new fecherFoundation.FCLabel();
            this.lblSubTotalAP = new fecherFoundation.FCLabel();
            this.lblCreditAP = new fecherFoundation.FCLabel();
            this.lblExciseAP = new fecherFoundation.FCLabel();
            this.Label52 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblNumberofYear = new fecherFoundation.FCLabel();
            this.lblNumberofMonth = new fecherFoundation.FCLabel();
            this.Label51 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label50 = new fecherFoundation.FCLabel();
            this.Label45 = new fecherFoundation.FCLabel();
            this.Label44 = new fecherFoundation.FCLabel();
            this.Label29 = new fecherFoundation.FCLabel();
            this.Label28 = new fecherFoundation.FCLabel();
            this.Label27 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label25 = new fecherFoundation.FCLabel();
            this.Label53 = new fecherFoundation.FCLabel();
            this.lblUseTax = new fecherFoundation.FCLabel();
            this.lblFees = new fecherFoundation.FCLabel();
            this.lblStateCredit = new fecherFoundation.FCLabel();
            this.Label46 = new fecherFoundation.FCLabel();
            this.lblCTA1 = new fecherFoundation.FCLabel();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.Label39 = new fecherFoundation.FCLabel();
            this.lblExciseTax = new fecherFoundation.FCLabel();
            this.Label37 = new fecherFoundation.FCLabel();
            this.Label36 = new fecherFoundation.FCLabel();
            this.Label35 = new fecherFoundation.FCLabel();
            this.Label33 = new fecherFoundation.FCLabel();
            this.Label32 = new fecherFoundation.FCLabel();
            this.Label31 = new fecherFoundation.FCLabel();
            this.Label30 = new fecherFoundation.FCLabel();
            this.lblStateCreditPR = new fecherFoundation.FCLabel();
            this.cmdMoreInfo = new fecherFoundation.FCButton();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuOpenFields = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCTA = new fecherFoundation.FCToolStripMenuItem();
            this.mnuUseTax = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessRedBook = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCalculate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFleetGroupComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPreview = new fecherFoundation.FCToolStripMenuItem();
            this.Separtor1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdOpenAllFields = new fecherFoundation.FCButton();
            this.cmdCTA = new fecherFoundation.FCButton();
            this.cmdUseTax = new fecherFoundation.FCButton();
            this.cmdGetBB = new fecherFoundation.FCButton();
            this.cmdCalculate = new fecherFoundation.FCButton();
            this.cmdComment = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).BeginInit();
            this.fraAdditionalInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).BeginInit();
            this.fraUserFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdErase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToLegalAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToMailingAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetGroupSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSpecialtyCorrExtraFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraHalfRate)).BeginInit();
            this.fraHalfRate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHalfRateLocal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHalfRateState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSalesTax)).BeginInit();
            this.fraSalesTax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDealerSalesTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTitleFee)).BeginInit();
            this.fraTitleFee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoFeeCTA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCTAPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBaseIsSalePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRental)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTANumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAmputeeVet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTrailerVanity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExciseExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAgentFeeExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStateExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2Year)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkETO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAgentFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAxles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOTNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresidenceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCTANumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxIDNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg2DOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg1DOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg3DOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpenAllFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCTA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUseTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetBB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 575);
            this.BottomPanel.Size = new System.Drawing.Size(1004, 70);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraAdditionalInfo);
            this.ClientArea.Location = new System.Drawing.Point(0, 65);
            this.ClientArea.Size = new System.Drawing.Size(1024, 571);
            this.ClientArea.Controls.SetChildIndex(this.fraAdditionalInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdComment);
            this.TopPanel.Controls.Add(this.cmdCalculate);
            this.TopPanel.Controls.Add(this.cmdGetBB);
            this.TopPanel.Controls.Add(this.cmdUseTax);
            this.TopPanel.Controls.Add(this.cmdCTA);
            this.TopPanel.Controls.Add(this.cmdOpenAllFields);
            this.TopPanel.Controls.Add(this.cmdMoreInfo);
            this.TopPanel.Size = new System.Drawing.Size(1024, 65);
            this.TopPanel.Controls.SetChildIndex(this.cmdMoreInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdOpenAllFields, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCTA, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdUseTax, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdGetBB, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCalculate, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(115, 28);
            this.HeaderText.Text = "Data Input";
            // 
            // cmbYes
            // 
            this.cmbYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbYes.Location = new System.Drawing.Point(147, 30);
            this.cmbYes.Name = "cmbYes";
            this.cmbYes.Size = new System.Drawing.Size(221, 40);
            this.cmbYes.TabIndex = 1;
            this.cmbYes.Text = "Yes";
            // 
            // lblYes
            // 
            this.lblYes.AutoSize = true;
            this.lblYes.Location = new System.Drawing.Point(20, 44);
            this.lblYes.Name = "lblYes";
            this.lblYes.Size = new System.Drawing.Size(107, 15);
            this.lblYes.TabIndex = 5;
            this.lblYes.Text = "MAIL REMINDER";
            // 
            // fraAdditionalInfo
            // 
            this.fraAdditionalInfo.AppearanceKey = "groupBoxLeftBorder";
            this.fraAdditionalInfo.BackColor = System.Drawing.Color.White;
            this.fraAdditionalInfo.Controls.Add(this.fraUserFields);
            this.fraAdditionalInfo.Controls.Add(this.chkOverrideFleetEmail);
            this.fraAdditionalInfo.Controls.Add(this.cmbYes);
            this.fraAdditionalInfo.Controls.Add(this.lblYes);
            this.fraAdditionalInfo.Controls.Add(this.cmdDone);
            this.fraAdditionalInfo.Controls.Add(this.fraMessage);
            this.fraAdditionalInfo.Controls.Add(this.txtEMail);
            this.fraAdditionalInfo.Controls.Add(this.lblEMail);
            this.fraAdditionalInfo.Location = new System.Drawing.Point(30, 30);
            this.fraAdditionalInfo.Name = "fraAdditionalInfo";
            this.fraAdditionalInfo.Size = new System.Drawing.Size(583, 535);
            this.fraAdditionalInfo.TabIndex = 1;
            this.fraAdditionalInfo.Text = "Additional Information";
            this.fraAdditionalInfo.Visible = false;
            // 
            // fraUserFields
            // 
            this.fraUserFields.BackColor = System.Drawing.Color.White;
            this.fraUserFields.Controls.Add(this.txtUserReason1);
            this.fraUserFields.Controls.Add(this.txtUserField1);
            this.fraUserFields.Controls.Add(this.txtUserField2);
            this.fraUserFields.Controls.Add(this.txtUserField3);
            this.fraUserFields.Controls.Add(this.txtUserReason2);
            this.fraUserFields.Controls.Add(this.txtUserReason3);
            this.fraUserFields.Controls.Add(this.lblInfo3);
            this.fraUserFields.Controls.Add(this.lblInfo2);
            this.fraUserFields.Controls.Add(this.lblInfo1);
            this.fraUserFields.Controls.Add(this.lblUserField3);
            this.fraUserFields.Controls.Add(this.lblUserField2);
            this.fraUserFields.Controls.Add(this.lblUserField1);
            this.fraUserFields.Location = new System.Drawing.Point(20, 132);
            this.fraUserFields.Name = "fraUserFields";
            this.fraUserFields.Size = new System.Drawing.Size(488, 345);
            this.fraUserFields.TabIndex = 6;
            this.fraUserFields.Text = "User Defined Fields";
            this.fraUserFields.Visible = false;
            // 
            // txtUserReason1
            // 
            this.txtUserReason1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserReason1.Location = new System.Drawing.Point(149, 80);
            this.txtUserReason1.Name = "txtUserReason1";
            this.txtUserReason1.Size = new System.Drawing.Size(319, 40);
            this.txtUserReason1.TabIndex = 3;
            this.txtUserReason1.Visible = false;
            // 
            // txtUserField1
            // 
            this.txtUserField1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserField1.Location = new System.Drawing.Point(149, 30);
            this.txtUserField1.Name = "txtUserField1";
            this.txtUserField1.Size = new System.Drawing.Size(319, 40);
            this.txtUserField1.TabIndex = 1;
            this.txtUserField1.Visible = false;
            // 
            // txtUserField2
            // 
            this.txtUserField2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserField2.Location = new System.Drawing.Point(149, 130);
            this.txtUserField2.Name = "txtUserField2";
            this.txtUserField2.Size = new System.Drawing.Size(319, 40);
            this.txtUserField2.TabIndex = 5;
            this.txtUserField2.Visible = false;
            // 
            // txtUserField3
            // 
            this.txtUserField3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserField3.Location = new System.Drawing.Point(149, 230);
            this.txtUserField3.Name = "txtUserField3";
            this.txtUserField3.Size = new System.Drawing.Size(319, 40);
            this.txtUserField3.TabIndex = 9;
            this.txtUserField3.Visible = false;
            // 
            // txtUserReason2
            // 
            this.txtUserReason2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserReason2.Location = new System.Drawing.Point(149, 180);
            this.txtUserReason2.Name = "txtUserReason2";
            this.txtUserReason2.Size = new System.Drawing.Size(319, 40);
            this.txtUserReason2.TabIndex = 7;
            this.txtUserReason2.Visible = false;
            // 
            // txtUserReason3
            // 
            this.txtUserReason3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserReason3.Location = new System.Drawing.Point(149, 280);
            this.txtUserReason3.Name = "txtUserReason3";
            this.txtUserReason3.Size = new System.Drawing.Size(319, 40);
            this.txtUserReason3.TabIndex = 11;
            this.txtUserReason3.Visible = false;
            // 
            // lblInfo3
            // 
            this.lblInfo3.Location = new System.Drawing.Point(20, 294);
            this.lblInfo3.Name = "lblInfo3";
            this.lblInfo3.Size = new System.Drawing.Size(92, 15);
            this.lblInfo3.TabIndex = 10;
            this.lblInfo3.Text = "INFORMATION";
            this.lblInfo3.Visible = false;
            // 
            // lblInfo2
            // 
            this.lblInfo2.Location = new System.Drawing.Point(20, 194);
            this.lblInfo2.Name = "lblInfo2";
            this.lblInfo2.Size = new System.Drawing.Size(92, 15);
            this.lblInfo2.TabIndex = 6;
            this.lblInfo2.Text = "INFORMATION";
            this.lblInfo2.Visible = false;
            // 
            // lblInfo1
            // 
            this.lblInfo1.Location = new System.Drawing.Point(20, 94);
            this.lblInfo1.Name = "lblInfo1";
            this.lblInfo1.Size = new System.Drawing.Size(92, 15);
            this.lblInfo1.TabIndex = 2;
            this.lblInfo1.Text = "INFORMATION";
            this.lblInfo1.Visible = false;
            // 
            // lblUserField3
            // 
            this.lblUserField3.Location = new System.Drawing.Point(20, 244);
            this.lblUserField3.Name = "lblUserField3";
            this.lblUserField3.Size = new System.Drawing.Size(122, 19);
            this.lblUserField3.TabIndex = 8;
            this.lblUserField3.Visible = false;
            // 
            // lblUserField2
            // 
            this.lblUserField2.Location = new System.Drawing.Point(20, 144);
            this.lblUserField2.Name = "lblUserField2";
            this.lblUserField2.Size = new System.Drawing.Size(122, 19);
            this.lblUserField2.TabIndex = 4;
            this.lblUserField2.Visible = false;
            // 
            // lblUserField1
            // 
            this.lblUserField1.Location = new System.Drawing.Point(20, 44);
            this.lblUserField1.Name = "lblUserField1";
            this.lblUserField1.Size = new System.Drawing.Size(122, 19);
            this.lblUserField1.TabIndex = 12;
            this.lblUserField1.Visible = false;
            // 
            // chkOverrideFleetEmail
            // 
            this.chkOverrideFleetEmail.Location = new System.Drawing.Point(386, 86);
            this.chkOverrideFleetEmail.Name = "chkOverrideFleetEmail";
            this.chkOverrideFleetEmail.Size = new System.Drawing.Size(150, 22);
            this.chkOverrideFleetEmail.TabIndex = 4;
            this.chkOverrideFleetEmail.TabStop = false;
            this.chkOverrideFleetEmail.Text = "Override Fleet Email";
            this.chkOverrideFleetEmail.Visible = false;
            this.chkOverrideFleetEmail.CheckedChanged += new System.EventHandler(this.chkOverrideFleetEmail_CheckedChanged);
            // 
            // cmdDone
            // 
            this.cmdDone.AppearanceKey = "actionButton";
            this.cmdDone.Location = new System.Drawing.Point(40, 495);
            this.cmdDone.Name = "cmdDone";
            this.cmdDone.Size = new System.Drawing.Size(64, 40);
            this.cmdDone.TabIndex = 7;
            this.cmdDone.Text = "OK";
            this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
            // 
            // fraMessage
            // 
            this.fraMessage.BackColor = System.Drawing.Color.White;
            this.fraMessage.Controls.Add(this.cmdErase);
            this.fraMessage.Controls.Add(this.txtMessage);
            this.fraMessage.Controls.Add(this.cboMessageCodes);
            this.fraMessage.Controls.Add(this.lblMesageCode);
            this.fraMessage.Controls.Add(this.lblMessage);
            this.fraMessage.Location = new System.Drawing.Point(20, 130);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(488, 190);
            this.fraMessage.TabIndex = 5;
            this.fraMessage.Text = "User Message";
            // 
            // cmdErase
            // 
            this.cmdErase.AppearanceKey = "actionButton";
            this.cmdErase.Location = new System.Drawing.Point(20, 130);
            this.cmdErase.Name = "cmdErase";
            this.cmdErase.Size = new System.Drawing.Size(162, 40);
            this.cmdErase.TabIndex = 4;
            this.cmdErase.Text = "Erase Message";
            this.cmdErase.Click += new System.EventHandler(this.cmdErase_Click);
            // 
            // txtMessage
            // 
            this.txtMessage.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMessage.Location = new System.Drawing.Point(150, 80);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(318, 40);
            this.txtMessage.TabIndex = 3;
            // 
            // cboMessageCodes
            // 
            this.cboMessageCodes.Items.AddRange(new object[] {
            "1 - Message ",
            "2 - Warning",
            "3 - Stop Process"});
            this.cboMessageCodes.Location = new System.Drawing.Point(150, 30);
            this.cboMessageCodes.Name = "cboMessageCodes";
            this.cboMessageCodes.Size = new System.Drawing.Size(318, 40);
            this.cboMessageCodes.TabIndex = 1;
            // 
            // lblMesageCode
            // 
            this.lblMesageCode.Location = new System.Drawing.Point(20, 44);
            this.lblMesageCode.Name = "lblMesageCode";
            this.lblMesageCode.TabIndex = 5;
            this.lblMesageCode.Text = "MESSAGE CODE";
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(20, 94);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(60, 17);
            this.lblMessage.TabIndex = 2;
            this.lblMessage.Text = "MESSAGE";
            // 
            // txtEMail
            // 
            this.txtEMail.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtEMail.Location = new System.Drawing.Point(147, 80);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Size = new System.Drawing.Size(221, 40);
            this.txtEMail.TabIndex = 3;
            // 
            // lblEMail
            // 
            this.lblEMail.Location = new System.Drawing.Point(20, 94);
            this.lblEMail.Name = "lblEMail";
            this.lblEMail.Size = new System.Drawing.Size(100, 17);
            this.lblEMail.TabIndex = 2;
            this.lblEMail.Text = "E-MAIL ADDRESS";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.fcLabel4);
            this.Frame1.Controls.Add(this.fcLabel3);
            this.Frame1.Controls.Add(this.imgCopyToLegalAddress);
            this.Frame1.Controls.Add(this.imgCopyToMailingAddress);
            this.Frame1.Controls.Add(this.imgFleetGroupSearch);
            this.Frame1.Controls.Add(this.imgEditRegistrant3);
            this.Frame1.Controls.Add(this.imgEditRegistrant2);
            this.Frame1.Controls.Add(this.imgEditRegistrant1);
            this.Frame1.Controls.Add(this.imgReg3Edit);
            this.Frame1.Controls.Add(this.imgReg3Search);
            this.Frame1.Controls.Add(this.imgReg2Edit);
            this.Frame1.Controls.Add(this.imgReg2Search);
            this.Frame1.Controls.Add(this.imgReg1Edit);
            this.Frame1.Controls.Add(this.imgReg1Search);
            this.Frame1.Controls.Add(this.lblCTA2);
            this.Frame1.Controls.Add(this.chkSpecialtyCorrExtraFee);
            this.Frame1.Controls.Add(this.txtRegistrant3);
            this.Frame1.Controls.Add(this.txtRegistrant2);
            this.Frame1.Controls.Add(this.txtRegistrant1);
            this.Frame1.Controls.Add(this.fraHalfRate);
            this.Frame1.Controls.Add(this.fraSalesTax);
            this.Frame1.Controls.Add(this.fraTitleFee);
            this.Frame1.Controls.Add(this.txtRate);
            this.Frame1.Controls.Add(this.txtExciseTax);
            this.Frame1.Controls.Add(this.imgFleetComment);
            this.Frame1.Controls.Add(this.lblFleet);
            this.Frame1.Controls.Add(this.fcLabel2);
            this.Frame1.Controls.Add(this.txtFleetNumber);
            this.Frame1.Controls.Add(this.fcLabel1);
            this.Frame1.Controls.Add(this.txtEvidenceofInsurance);
            this.Frame1.Controls.Add(this.txtUserID);
            this.Frame1.Controls.Add(this.txtExciseTaxDate);
            this.Frame1.Controls.Add(this.lblStateRateHR);
            this.Frame1.Controls.Add(this.chkCC);
            this.Frame1.Controls.Add(this.chkBaseIsSalePrice);
            this.Frame1.Controls.Add(this.txtReg2ICM);
            this.Frame1.Controls.Add(this.txtReg1ICM);
            this.Frame1.Controls.Add(this.txtReg3ICM);
            this.Frame1.Controls.Add(this.txtReg3LR);
            this.Frame1.Controls.Add(this.txtReg1LR);
            this.Frame1.Controls.Add(this.txtReg2LR);
            this.Frame1.Controls.Add(this.txtReg1PartyID);
            this.Frame1.Controls.Add(this.txtReg2PartyID);
            this.Frame1.Controls.Add(this.txtReg3PartyID);
            this.Frame1.Controls.Add(this.txtCountry);
            this.Frame1.Controls.Add(this.txtResCountry);
            this.Frame1.Controls.Add(this.cboBattle);
            this.Frame1.Controls.Add(this.chkTransferExempt);
            this.Frame1.Controls.Add(this.chkRental);
            this.Frame1.Controls.Add(this.chkEAP);
            this.Frame1.Controls.Add(this.txtYStickerNumber);
            this.Frame1.Controls.Add(this.txtMStickerNumber);
            this.Frame1.Controls.Add(this.txtCTANumber);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtPriorTitleState);
            this.Frame1.Controls.Add(this.txtCreditNumber);
            this.Frame1.Controls.Add(this.chkVIN);
            this.Frame1.Controls.Add(this.txtResState);
            this.Frame1.Controls.Add(this.chkAmputeeVet);
            this.Frame1.Controls.Add(this.chkTrailerVanity);
            this.Frame1.Controls.Add(this.chkExciseExempt);
            this.Frame1.Controls.Add(this.chkAgentFeeExempt);
            this.Frame1.Controls.Add(this.chkStateExempt);
            this.Frame1.Controls.Add(this.chk2Year);
            this.Frame1.Controls.Add(this.chkETO);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtMileage);
            this.Frame1.Controls.Add(this.txtMilYear);
            this.Frame1.Controls.Add(this.txtYearNoCharge);
            this.Frame1.Controls.Add(this.txtMonthNoCharge);
            this.Frame1.Controls.Add(this.txtYearCharge);
            this.Frame1.Controls.Add(this.txtMonthCharge);
            this.Frame1.Controls.Add(this.txtPreviousTitle);
            this.Frame1.Controls.Add(this.txtMilRate);
            this.Frame1.Controls.Add(this.txtFuel);
            this.Frame1.Controls.Add(this.txtRegWeight);
            this.Frame1.Controls.Add(this.txtNetWeight);
            this.Frame1.Controls.Add(this.txtRegistrationNumber);
            this.Frame1.Controls.Add(this.txtClass);
            this.Frame1.Controls.Add(this.txtVin);
            this.Frame1.Controls.Add(this.txtBase);
            this.Frame1.Controls.Add(this.txtAgentFee);
            this.Frame1.Controls.Add(this.txtTownCredit);
            this.Frame1.Controls.Add(this.txtBalance);
            this.Frame1.Controls.Add(this.txtSubTotal);
            this.Frame1.Controls.Add(this.txtTransferCharge);
            this.Frame1.Controls.Add(this.txtTitle);
            this.Frame1.Controls.Add(this.txtSalesTax);
            this.Frame1.Controls.Add(this.txtFees);
            this.Frame1.Controls.Add(this.txtStateCredit);
            this.Frame1.Controls.Add(this.txtExpires);
            this.Frame1.Controls.Add(this.txtEffectiveDate);
            this.Frame1.Controls.Add(this.txtYear);
            this.Frame1.Controls.Add(this.txtMake);
            this.Frame1.Controls.Add(this.txtModel);
            this.Frame1.Controls.Add(this.txtColor1);
            this.Frame1.Controls.Add(this.txtColor2);
            this.Frame1.Controls.Add(this.txtStyle);
            this.Frame1.Controls.Add(this.txtTires);
            this.Frame1.Controls.Add(this.txtAxles);
            this.Frame1.Controls.Add(this.txtUnitNumber);
            this.Frame1.Controls.Add(this.txtDOTNumber);
            this.Frame1.Controls.Add(this.txtAddress1);
            this.Frame1.Controls.Add(this.txtLegalres);
            this.Frame1.Controls.Add(this.txtresidenceCode);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.txtAddress2);
            this.Frame1.Controls.Add(this.txtLegalResStreet);
            this.Frame1.Controls.Add(this.txtDoubleCTANumber);
            this.Frame1.Controls.Add(this.txtTaxIDNumber);
            this.Frame1.Controls.Add(this.txtReg2DOB);
            this.Frame1.Controls.Add(this.txtReg1DOB);
            this.Frame1.Controls.Add(this.txtReg3DOB);
            this.Frame1.Controls.Add(this.lblCreditNumberAP);
            this.Frame1.Controls.Add(this.lblStateFeesHR);
            this.Frame1.Controls.Add(this.lblStateFeesPR);
            this.Frame1.Controls.Add(this.lblRegistrant3);
            this.Frame1.Controls.Add(this.lblDOBID);
            this.Frame1.Controls.Add(this.Label54);
            this.Frame1.Controls.Add(this.lblTaxID);
            this.Frame1.Controls.Add(this.lblStateRatePR);
            this.Frame1.Controls.Add(this.lblExciseTaxPR);
            this.Frame1.Controls.Add(this.lblLocalSubPR);
            this.Frame1.Controls.Add(this.lblTownCreditPR);
            this.Frame1.Controls.Add(this.lblLocalBalancePR);
            this.Frame1.Controls.Add(this.lblCreditRebate);
            this.Frame1.Controls.Add(this.Label48);
            this.Frame1.Controls.Add(this.Label47);
            this.Frame1.Controls.Add(this.Label38);
            this.Frame1.Controls.Add(this.Label24);
            this.Frame1.Controls.Add(this.Label22);
            this.Frame1.Controls.Add(this.Label26);
            this.Frame1.Controls.Add(this.lblExciseDiff);
            this.Frame1.Controls.Add(this.Image1);
            this.Frame1.Controls.Add(this.lblBattle);
            this.Frame1.Controls.Add(this.lblLocalBalanceHR);
            this.Frame1.Controls.Add(this.lblTownCreditHR);
            this.Frame1.Controls.Add(this.Label21);
            this.Frame1.Controls.Add(this.lblYSD);
            this.Frame1.Controls.Add(this.lblMSD);
            this.Frame1.Controls.Add(this.lblLocalSubHR);
            this.Frame1.Controls.Add(this.lblStateCreditHR);
            this.Frame1.Controls.Add(this.lblExciseTaxHR);
            this.Frame1.Controls.Add(this.lblBalanceAP);
            this.Frame1.Controls.Add(this.lblTotalDue);
            this.Frame1.Controls.Add(this.lblCalculate);
            this.Frame1.Controls.Add(this.lblTransferAP);
            this.Frame1.Controls.Add(this.lblSubTotalAP);
            this.Frame1.Controls.Add(this.lblCreditAP);
            this.Frame1.Controls.Add(this.lblExciseAP);
            this.Frame1.Controls.Add(this.Label52);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.lblNumberofYear);
            this.Frame1.Controls.Add(this.lblNumberofMonth);
            this.Frame1.Controls.Add(this.Label51);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label14);
            this.Frame1.Controls.Add(this.Label50);
            this.Frame1.Controls.Add(this.Label45);
            this.Frame1.Controls.Add(this.Label44);
            this.Frame1.Controls.Add(this.Label29);
            this.Frame1.Controls.Add(this.Label28);
            this.Frame1.Controls.Add(this.Label27);
            this.Frame1.Controls.Add(this.Label23);
            this.Frame1.Controls.Add(this.Label20);
            this.Frame1.Controls.Add(this.Label19);
            this.Frame1.Controls.Add(this.Label18);
            this.Frame1.Controls.Add(this.Label17);
            this.Frame1.Controls.Add(this.Label16);
            this.Frame1.Controls.Add(this.Label15);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label25);
            this.Frame1.Controls.Add(this.Label53);
            this.Frame1.Controls.Add(this.lblUseTax);
            this.Frame1.Controls.Add(this.lblFees);
            this.Frame1.Controls.Add(this.lblStateCredit);
            this.Frame1.Controls.Add(this.Label46);
            this.Frame1.Controls.Add(this.lblCTA1);
            this.Frame1.Controls.Add(this.lblTitle);
            this.Frame1.Controls.Add(this.Label39);
            this.Frame1.Controls.Add(this.lblExciseTax);
            this.Frame1.Controls.Add(this.Label37);
            this.Frame1.Controls.Add(this.Label36);
            this.Frame1.Controls.Add(this.Label35);
            this.Frame1.Controls.Add(this.Label33);
            this.Frame1.Controls.Add(this.Label32);
            this.Frame1.Controls.Add(this.Label31);
            this.Frame1.Controls.Add(this.Label30);
            this.Frame1.Controls.Add(this.lblStateCreditPR);
            this.Frame1.Location = new System.Drawing.Point(4, 10);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1063, 565);
            this.Frame1.TabIndex = 1001;
            // 
            // fcLabel4
            // 
            this.fcLabel4.Location = new System.Drawing.Point(489, 141);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(120, 16);
            this.fcLabel4.TabIndex = 251;
            this.fcLabel4.Text = "DATE OF BIRTH";
            // 
            // fcLabel3
            // 
            this.fcLabel3.Location = new System.Drawing.Point(271, 141);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(166, 16);
            this.fcLabel3.TabIndex = 250;
            this.fcLabel3.Text = "REGISTRANT NAME";
            // 
            // imgCopyToLegalAddress
            // 
            this.imgCopyToLegalAddress.AppearanceKey = "actionButton";
            this.imgCopyToLegalAddress.FillColor = 16777215;
            this.imgCopyToLegalAddress.ImageSource = "icon-copy";
            this.imgCopyToLegalAddress.Location = new System.Drawing.Point(422, 332);
            this.imgCopyToLegalAddress.Name = "imgCopyToLegalAddress";
            this.imgCopyToLegalAddress.Size = new System.Drawing.Size(23, 15);
            this.imgCopyToLegalAddress.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgCopyToLegalAddress, "Copy from mailing address");
            this.imgCopyToLegalAddress.Click += new System.EventHandler(this.imgCopyToLegalAddress_Click);
            // 
            // imgCopyToMailingAddress
            // 
            this.imgCopyToMailingAddress.AppearanceKey = "actionButton";
            this.imgCopyToMailingAddress.FillColor = 16777215;
            this.imgCopyToMailingAddress.ImageSource = "icon-copy";
            this.imgCopyToMailingAddress.Location = new System.Drawing.Point(174, 332);
            this.imgCopyToMailingAddress.Name = "imgCopyToMailingAddress";
            this.imgCopyToMailingAddress.Size = new System.Drawing.Size(23, 15);
            this.imgCopyToMailingAddress.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgCopyToMailingAddress, "Copy address from primary registrant");
            this.imgCopyToMailingAddress.Click += new System.EventHandler(this.imgCopyToMailingAddress_Click);
            // 
            // imgFleetGroupSearch
            // 
            this.imgFleetGroupSearch.AppearanceKey = "actionButton";
            this.imgFleetGroupSearch.FillColor = 16777215;
            this.imgFleetGroupSearch.ImageSource = "icon-search-border";
            this.imgFleetGroupSearch.Location = new System.Drawing.Point(184, 261);
            this.imgFleetGroupSearch.Name = "imgFleetGroupSearch";
            this.imgFleetGroupSearch.Size = new System.Drawing.Size(23, 15);
            this.imgFleetGroupSearch.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgFleetGroupSearch.Click += new System.EventHandler(this.imgFleetGroupSearch_Click);
            // 
            // imgEditRegistrant3
            // 
            this.imgEditRegistrant3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.imgEditRegistrant3.AppearanceKey = "actionButton";
            this.imgEditRegistrant3.FillColor = 16777215;
            this.imgEditRegistrant3.ImageSource = "icon-edit-border";
            this.imgEditRegistrant3.Location = new System.Drawing.Point(452, 230);
            this.imgEditRegistrant3.Name = "imgEditRegistrant3";
            this.imgEditRegistrant3.Size = new System.Drawing.Size(23, 15);
            this.imgEditRegistrant3.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgEditRegistrant3.Click += new System.EventHandler(this.imgEditRegistrant3_Click);
            // 
            // imgEditRegistrant2
            // 
            this.imgEditRegistrant2.AppearanceKey = "actionButton";
            this.imgEditRegistrant2.FillColor = 16777215;
            this.imgEditRegistrant2.ImageSource = "icon-edit-border";
            this.imgEditRegistrant2.Location = new System.Drawing.Point(452, 197);
            this.imgEditRegistrant2.Name = "imgEditRegistrant2";
            this.imgEditRegistrant2.Size = new System.Drawing.Size(23, 15);
            this.imgEditRegistrant2.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgEditRegistrant2.Click += new System.EventHandler(this.imgEditRegistrant2_Click);
            // 
            // imgEditRegistrant1
            // 
            this.imgEditRegistrant1.AppearanceKey = "actionButton";
            this.imgEditRegistrant1.FillColor = 16777215;
            this.imgEditRegistrant1.ImageSource = "icon-edit-border";
            this.imgEditRegistrant1.Location = new System.Drawing.Point(452, 164);
            this.imgEditRegistrant1.Name = "imgEditRegistrant1";
            this.imgEditRegistrant1.Size = new System.Drawing.Size(23, 15);
            this.imgEditRegistrant1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgEditRegistrant1.Click += new System.EventHandler(this.imgEditRegistrant1_Click);
            // 
            // imgReg3Edit
            // 
            this.imgReg3Edit.AppearanceKey = "actionButton";
            this.imgReg3Edit.FillColor = 16777215;
            this.imgReg3Edit.ImageSource = "icon-edit-border";
            this.imgReg3Edit.Location = new System.Drawing.Point(217, 230);
            this.imgReg3Edit.Name = "imgReg3Edit";
            this.imgReg3Edit.Size = new System.Drawing.Size(23, 15);
            this.imgReg3Edit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg3Edit.Click += new System.EventHandler(this.imgReg3Edit_Click);
            // 
            // imgReg3Search
            // 
            this.imgReg3Search.AppearanceKey = "actionButton";
            this.imgReg3Search.FillColor = 16777215;
            this.imgReg3Search.ImageSource = "icon-search-border";
            this.imgReg3Search.Location = new System.Drawing.Point(184, 230);
            this.imgReg3Search.Name = "imgReg3Search";
            this.imgReg3Search.Size = new System.Drawing.Size(23, 15);
            this.imgReg3Search.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // imgReg2Edit
            // 
            this.imgReg2Edit.AppearanceKey = "actionButton";
            this.imgReg2Edit.FillColor = 16777215;
            this.imgReg2Edit.ImageSource = "icon-edit-border";
            this.imgReg2Edit.Location = new System.Drawing.Point(217, 196);
            this.imgReg2Edit.Name = "imgReg2Edit";
            this.imgReg2Edit.Size = new System.Drawing.Size(23, 15);
            this.imgReg2Edit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg2Edit.Click += new System.EventHandler(this.imgReg2Edit_Click);
            // 
            // imgReg2Search
            // 
            this.imgReg2Search.AppearanceKey = "actionButton";
            this.imgReg2Search.FillColor = 16777215;
            this.imgReg2Search.ImageSource = "icon-search-border";
            this.imgReg2Search.Location = new System.Drawing.Point(184, 196);
            this.imgReg2Search.Name = "imgReg2Search";
            this.imgReg2Search.Size = new System.Drawing.Size(23, 15);
            this.imgReg2Search.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg2Search.Click += new System.EventHandler(this.imgReg2Search_Click);
            // 
            // imgReg1Edit
            // 
            this.imgReg1Edit.AppearanceKey = "actionButton";
            this.imgReg1Edit.FillColor = 16777215;
            this.imgReg1Edit.ImageSource = "icon-edit-border";
            this.imgReg1Edit.Location = new System.Drawing.Point(217, 164);
            this.imgReg1Edit.Name = "imgReg1Edit";
            this.imgReg1Edit.Size = new System.Drawing.Size(23, 15);
            this.imgReg1Edit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg1Edit.Click += new System.EventHandler(this.imgReg1Edit_Click);
            // 
            // imgReg1Search
            // 
            this.imgReg1Search.AppearanceKey = "actionButton";
            this.imgReg1Search.FillColor = 16777215;
            this.imgReg1Search.ImageSource = "icon-search-border";
            this.imgReg1Search.Location = new System.Drawing.Point(184, 164);
            this.imgReg1Search.Name = "imgReg1Search";
            this.imgReg1Search.Size = new System.Drawing.Size(23, 15);
            this.imgReg1Search.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg1Search.Click += new System.EventHandler(this.imgReg1Search_Click);
            // 
            // lblCTA2
            // 
            this.lblCTA2.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblCTA2.Location = new System.Drawing.Point(893, 338);
            this.lblCTA2.Name = "lblCTA2";
            this.lblCTA2.Size = new System.Drawing.Size(40, 15);
            this.lblCTA2.TabIndex = 247;
            this.lblCTA2.Text = "CTA 2";
            // 
            // chkSpecialtyCorrExtraFee
            // 
            this.chkSpecialtyCorrExtraFee.AutoSize = false;
            this.chkSpecialtyCorrExtraFee.Enabled = false;
            this.chkSpecialtyCorrExtraFee.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkSpecialtyCorrExtraFee.Location = new System.Drawing.Point(574, 542);
            this.chkSpecialtyCorrExtraFee.Name = "chkSpecialtyCorrExtraFee";
            this.chkSpecialtyCorrExtraFee.Size = new System.Drawing.Size(186, 16);
            this.chkSpecialtyCorrExtraFee.TabIndex = 244;
            this.chkSpecialtyCorrExtraFee.TabStop = false;
            this.chkSpecialtyCorrExtraFee.Text = "$1 Specialty Plate Fee";
            this.chkSpecialtyCorrExtraFee.Visible = false;
            this.chkSpecialtyCorrExtraFee.CheckedChanged += new System.EventHandler(this.chkSpecialtyCorrExtraFee_CheckedChanged);
            // 
            // txtRegistrant3
            // 
            this.txtRegistrant3.Appearance = 0;
            this.txtRegistrant3.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtRegistrant3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRegistrant3.Cursor = Wisej.Web.Cursors.Default;
            this.txtRegistrant3.Focusable = false;
            this.txtRegistrant3.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegistrant3.Location = new System.Drawing.Point(259, 230);
            this.txtRegistrant3.LockedOriginal = true;
            this.txtRegistrant3.Name = "txtRegistrant3";
            this.txtRegistrant3.ReadOnly = true;
            this.txtRegistrant3.Size = new System.Drawing.Size(186, 15);
            this.txtRegistrant3.TabIndex = 241;
            this.txtRegistrant3.WordWrap = false;
            // 
            // txtRegistrant2
            // 
            this.txtRegistrant2.Appearance = 0;
            this.txtRegistrant2.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtRegistrant2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRegistrant2.Cursor = Wisej.Web.Cursors.Default;
            this.txtRegistrant2.Focusable = false;
            this.txtRegistrant2.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegistrant2.Location = new System.Drawing.Point(259, 197);
            this.txtRegistrant2.LockedOriginal = true;
            this.txtRegistrant2.Name = "txtRegistrant2";
            this.txtRegistrant2.ReadOnly = true;
            this.txtRegistrant2.Size = new System.Drawing.Size(186, 15);
            this.txtRegistrant2.TabIndex = 240;
            this.txtRegistrant2.WordWrap = false;
            // 
            // txtRegistrant1
            // 
            this.txtRegistrant1.Appearance = 0;
            this.txtRegistrant1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtRegistrant1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRegistrant1.Cursor = Wisej.Web.Cursors.Default;
            this.txtRegistrant1.Focusable = false;
            this.txtRegistrant1.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegistrant1.Location = new System.Drawing.Point(258, 164);
            this.txtRegistrant1.LockedOriginal = true;
            this.txtRegistrant1.Name = "txtRegistrant1";
            this.txtRegistrant1.ReadOnly = true;
            this.txtRegistrant1.Size = new System.Drawing.Size(186, 15);
            this.txtRegistrant1.TabIndex = 237;
            this.txtRegistrant1.WordWrap = false;
            // 
            // fraHalfRate
            // 
            this.fraHalfRate.BackColor = System.Drawing.Color.White;
            this.fraHalfRate.Controls.Add(this.chkHalfRateLocal);
            this.fraHalfRate.Controls.Add(this.chkHalfRateState);
            this.fraHalfRate.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fraHalfRate.Location = new System.Drawing.Point(625, 426);
            this.fraHalfRate.Name = "fraHalfRate";
            this.fraHalfRate.Size = new System.Drawing.Size(180, 46);
            this.fraHalfRate.TabIndex = 85;
            this.fraHalfRate.Text = "Half Rate ";
            // 
            // chkHalfRateLocal
            // 
            this.chkHalfRateLocal.AutoSize = false;
            this.chkHalfRateLocal.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkHalfRateLocal.Location = new System.Drawing.Point(16, 16);
            this.chkHalfRateLocal.Name = "chkHalfRateLocal";
            this.chkHalfRateLocal.Size = new System.Drawing.Size(79, 22);
            this.chkHalfRateLocal.TabIndex = 0;
            this.chkHalfRateLocal.TabStop = false;
            this.chkHalfRateLocal.Text = "Local";
            this.chkHalfRateLocal.CheckedChanged += new System.EventHandler(this.chkHalfRateLocal_CheckedChanged);
            // 
            // chkHalfRateState
            // 
            this.chkHalfRateState.AutoSize = false;
            this.chkHalfRateState.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkHalfRateState.Location = new System.Drawing.Point(108, 16);
            this.chkHalfRateState.Name = "chkHalfRateState";
            this.chkHalfRateState.Size = new System.Drawing.Size(71, 22);
            this.chkHalfRateState.TabIndex = 1;
            this.chkHalfRateState.TabStop = false;
            this.chkHalfRateState.Text = "State";
            this.chkHalfRateState.CheckedChanged += new System.EventHandler(this.chkHalfRateState_CheckedChanged);
            // 
            // fraSalesTax
            // 
            this.fraSalesTax.BackColor = System.Drawing.Color.White;
            this.fraSalesTax.Controls.Add(this.chkSalesTaxExempt);
            this.fraSalesTax.Controls.Add(this.chkDealerSalesTax);
            this.fraSalesTax.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fraSalesTax.Location = new System.Drawing.Point(879, 426);
            this.fraSalesTax.Name = "fraSalesTax";
            this.fraSalesTax.Size = new System.Drawing.Size(180, 46);
            this.fraSalesTax.TabIndex = 86;
            this.fraSalesTax.Text = "Sales Tax";
            this.ToolTip1.SetToolTip(this.fraSalesTax, "Sales tax not collected. Indicate wheter Exempt or paid to Dealer.");
            // 
            // chkSalesTaxExempt
            // 
            this.chkSalesTaxExempt.AutoSize = false;
            this.chkSalesTaxExempt.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkSalesTaxExempt.Location = new System.Drawing.Point(16, 16);
            this.chkSalesTaxExempt.Name = "chkSalesTaxExempt";
            this.chkSalesTaxExempt.Size = new System.Drawing.Size(80, 22);
            this.chkSalesTaxExempt.TabIndex = 0;
            this.chkSalesTaxExempt.TabStop = false;
            this.chkSalesTaxExempt.Text = "No Fee";
            this.chkSalesTaxExempt.CheckedChanged += new System.EventHandler(this.chkSalesTaxExempt_CheckedChanged);
            this.chkSalesTaxExempt.Leave += new System.EventHandler(this.chkSalesTaxExempt_Leave);
            // 
            // chkDealerSalesTax
            // 
            this.chkDealerSalesTax.AutoSize = false;
            this.chkDealerSalesTax.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkDealerSalesTax.Location = new System.Drawing.Point(96, 16);
            this.chkDealerSalesTax.Name = "chkDealerSalesTax";
            this.chkDealerSalesTax.Size = new System.Drawing.Size(78, 22);
            this.chkDealerSalesTax.TabIndex = 1;
            this.chkDealerSalesTax.TabStop = false;
            this.chkDealerSalesTax.Text = "Dealer";
            this.chkDealerSalesTax.CheckedChanged += new System.EventHandler(this.chkDealerSalesTax_CheckedChanged);
            this.chkDealerSalesTax.Leave += new System.EventHandler(this.chkDealerSalesTax_Leave);
            // 
            // fraTitleFee
            // 
            this.fraTitleFee.BackColor = System.Drawing.Color.White;
            this.fraTitleFee.Controls.Add(this.chkNoFeeCTA);
            this.fraTitleFee.Controls.Add(this.chkCTAPaid);
            this.fraTitleFee.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fraTitleFee.Location = new System.Drawing.Point(879, 370);
            this.fraTitleFee.Name = "fraTitleFee";
            this.fraTitleFee.Size = new System.Drawing.Size(181, 46);
            this.fraTitleFee.TabIndex = 84;
            this.fraTitleFee.Text = "Title Fee";
            this.ToolTip1.SetToolTip(this.fraTitleFee, "Title fee not collected. Indicate wheter Exempt or Paid elsewhere.");
            // 
            // chkNoFeeCTA
            // 
            this.chkNoFeeCTA.AutoSize = false;
            this.chkNoFeeCTA.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkNoFeeCTA.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkNoFeeCTA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chkNoFeeCTA.Location = new System.Drawing.Point(16, 16);
            this.chkNoFeeCTA.Name = "chkNoFeeCTA";
            this.chkNoFeeCTA.Size = new System.Drawing.Size(80, 22);
            this.chkNoFeeCTA.TabIndex = 0;
            this.chkNoFeeCTA.TabStop = false;
            this.chkNoFeeCTA.Text = "No Fee";
            this.chkNoFeeCTA.CheckedChanged += new System.EventHandler(this.chkNoFeeCTA_CheckedChanged);
            this.chkNoFeeCTA.Leave += new System.EventHandler(this.chkNoFeeCTA_Leave);
            // 
            // chkCTAPaid
            // 
            this.chkCTAPaid.AutoSize = false;
            this.chkCTAPaid.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkCTAPaid.Location = new System.Drawing.Point(96, 16);
            this.chkCTAPaid.Name = "chkCTAPaid";
            this.chkCTAPaid.Size = new System.Drawing.Size(73, 22);
            this.chkCTAPaid.TabIndex = 1;
            this.chkCTAPaid.TabStop = false;
            this.chkCTAPaid.Text = "Paid";
            this.chkCTAPaid.CheckedChanged += new System.EventHandler(this.chkCTAPaid_CheckedChanged);
            this.chkCTAPaid.Leave += new System.EventHandler(this.chkCTAPaid_Leave);
            // 
            // txtRate
            // 
            this.txtRate.AutoSize = false;
            this.txtRate.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRate.Location = new System.Drawing.Point(975, 162);
            this.txtRate.MaxLength = 9;
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(85, 15);
            this.txtRate.TabIndex = 77;
            this.txtRate.TabStop = false;
            this.txtRate.Text = "0.00";
            this.txtRate.Enter += new System.EventHandler(this.txtRate_Enter);
            this.txtRate.Leave += new System.EventHandler(this.txtRate_Leave);
            this.txtRate.DoubleClick += new System.EventHandler(this.txtRate_DblClick);
            // 
            // txtExciseTax
            // 
            this.txtExciseTax.AutoSize = false;
            this.txtExciseTax.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtExciseTax.Location = new System.Drawing.Point(764, 249);
            this.txtExciseTax.MaxLength = 7;
            this.txtExciseTax.Name = "txtExciseTax";
            this.txtExciseTax.Size = new System.Drawing.Size(90, 15);
            this.txtExciseTax.TabIndex = 69;
            this.txtExciseTax.TabStop = false;
            this.txtExciseTax.Text = "0.00";
            // 
            // imgFleetComment
            // 
            this.imgFleetComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgFleetComment.FillColor = 16777215;
            this.imgFleetComment.Image = ((System.Drawing.Image)(resources.GetObject("imgFleetComment.Image")));
            this.imgFleetComment.Location = new System.Drawing.Point(500, 261);
            this.imgFleetComment.Name = "imgFleetComment";
            this.imgFleetComment.Size = new System.Drawing.Size(40, 15);
            this.imgFleetComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgFleetComment.Visible = false;
            this.imgFleetComment.Click += new System.EventHandler(this.imgFleetComment_Click);
            // 
            // lblFleet
            // 
            this.lblFleet.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblFleet.Location = new System.Drawing.Point(323, 261);
            this.lblFleet.Name = "lblFleet";
            this.lblFleet.Size = new System.Drawing.Size(170, 15);
            this.lblFleet.TabIndex = 235;
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(19, 261);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(68, 15);
            this.fcLabel2.TabIndex = 234;
            this.fcLabel2.Text = "FLEET";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFleetNumber
            // 
            this.txtFleetNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtFleetNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtFleetNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtFleetNumber.Location = new System.Drawing.Point(99, 261);
            this.txtFleetNumber.Name = "txtFleetNumber";
            this.txtFleetNumber.Size = new System.Drawing.Size(70, 15);
            this.txtFleetNumber.TabIndex = 39;
            this.txtFleetNumber.TabStop = false;
            this.txtFleetNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtFleetNumber_Validating);
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(79, 437);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(74, 13);
            this.fcLabel1.TabIndex = 232;
            this.fcLabel1.Text = "ZIP";
            // 
            // txtEvidenceofInsurance
            // 
            this.txtEvidenceofInsurance.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtEvidenceofInsurance.Cursor = Wisej.Web.Cursors.Default;
            this.txtEvidenceofInsurance.Location = new System.Drawing.Point(514, -28);
            this.txtEvidenceofInsurance.MaxLength = 8;
            this.txtEvidenceofInsurance.Name = "txtEvidenceofInsurance";
            this.txtEvidenceofInsurance.Size = new System.Drawing.Size(114, 40);
            this.txtEvidenceofInsurance.TabIndex = 231;
            this.txtEvidenceofInsurance.TabStop = false;
            this.txtEvidenceofInsurance.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtEvidenceofInsurance.Visible = false;
            // 
            // txtUserID
            // 
            this.txtUserID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserID.Cursor = Wisej.Web.Cursors.Default;
            this.txtUserID.Location = new System.Drawing.Point(514, -24);
            this.txtUserID.MaxLength = 8;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(114, 40);
            this.txtUserID.TabIndex = 230;
            this.txtUserID.TabStop = false;
            this.txtUserID.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtUserID.Visible = false;
            // 
            // txtExciseTaxDate
            // 
            this.txtExciseTaxDate.Location = new System.Drawing.Point(515, 17);
            this.txtExciseTaxDate.Mask = "##/##/####";
            this.txtExciseTaxDate.MaxLength = 10;
            this.txtExciseTaxDate.Name = "txtExciseTaxDate";
            this.txtExciseTaxDate.Size = new System.Drawing.Size(115, 22);
            this.txtExciseTaxDate.TabIndex = 229;
            this.txtExciseTaxDate.Visible = false;
            // 
            // lblStateRateHR
            // 
            this.lblStateRateHR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblStateRateHR.Location = new System.Drawing.Point(950, 164);
            this.lblStateRateHR.Name = "lblStateRateHR";
            this.lblStateRateHR.Size = new System.Drawing.Size(23, 15);
            this.lblStateRateHR.TabIndex = 165;
            this.lblStateRateHR.Text = "HR";
            this.lblStateRateHR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateRateHR.Visible = false;
            // 
            // chkCC
            // 
            this.chkCC.AutoSize = false;
            this.chkCC.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkCC.Location = new System.Drawing.Point(621, 496);
            this.chkCC.Name = "chkCC";
            this.chkCC.Size = new System.Drawing.Size(184, 16);
            this.chkCC.TabIndex = 89;
            this.chkCC.TabStop = false;
            this.chkCC.Text = "Motorcycle >299cc?";
            this.chkCC.CheckedChanged += new System.EventHandler(this.chkCC_CheckedChanged);
            // 
            // chkBaseIsSalePrice
            // 
            this.chkBaseIsSalePrice.AutoSize = false;
            this.chkBaseIsSalePrice.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkBaseIsSalePrice.Location = new System.Drawing.Point(879, 476);
            this.chkBaseIsSalePrice.Name = "chkBaseIsSalePrice";
            this.chkBaseIsSalePrice.Size = new System.Drawing.Size(174, 18);
            this.chkBaseIsSalePrice.TabIndex = 88;
            this.chkBaseIsSalePrice.TabStop = false;
            this.chkBaseIsSalePrice.Text = "Base = Sale Price?";
            this.chkBaseIsSalePrice.Leave += new System.EventHandler(this.chkBaseIsSalePrice_Leave);
            // 
            // txtReg2ICM
            // 
            this.txtReg2ICM.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg2ICM.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg2ICM.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg2ICM.Location = new System.Drawing.Point(54, 195);
            this.txtReg2ICM.Margin = new Wisej.Web.Padding(0, 3, 0, 3);
            this.txtReg2ICM.MaxLength = 1;
            this.txtReg2ICM.Name = "txtReg2ICM";
            this.txtReg2ICM.Size = new System.Drawing.Size(38, 15);
            this.txtReg2ICM.TabIndex = 28;
            this.txtReg2ICM.Text = "N";
            this.txtReg2ICM.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg2ICM, "N = None I = Individual  M= Municipal  C = Commercial  D = Doing Business As");
            this.txtReg2ICM.Enter += new System.EventHandler(this.txtReg2ICM_Enter);
            this.txtReg2ICM.Leave += new System.EventHandler(this.txtReg2ICM_Leave);
            this.txtReg2ICM.Click += new System.EventHandler(this.txtReg2ICM_Click);
            this.txtReg2ICM.TextChanged += new System.EventHandler(this.txtReg2ICM_TextChanged);
            this.txtReg2ICM.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2ICM_Validating);
            // 
            // txtReg1ICM
            // 
            this.txtReg1ICM.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg1ICM.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg1ICM.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg1ICM.Location = new System.Drawing.Point(54, 162);
            this.txtReg1ICM.Margin = new Wisej.Web.Padding(0, 3, 0, 3);
            this.txtReg1ICM.MaxLength = 1;
            this.txtReg1ICM.Name = "txtReg1ICM";
            this.txtReg1ICM.Size = new System.Drawing.Size(38, 15);
            this.txtReg1ICM.TabIndex = 22;
            this.txtReg1ICM.Text = "I";
            this.txtReg1ICM.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg1ICM, "I = Individual  M= Municipal  C = Commercial");
            this.txtReg1ICM.Enter += new System.EventHandler(this.txtReg1ICM_Enter);
            this.txtReg1ICM.Leave += new System.EventHandler(this.txtReg1ICM_Leave);
            this.txtReg1ICM.Click += new System.EventHandler(this.txtReg1ICM_Click);
            this.txtReg1ICM.TextChanged += new System.EventHandler(this.txtReg1ICM_TextChanged);
            this.txtReg1ICM.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1ICM_Validating);
            this.txtReg1ICM.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg1ICM_KeyPress);
            // 
            // txtReg3ICM
            // 
            this.txtReg3ICM.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg3ICM.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg3ICM.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg3ICM.Location = new System.Drawing.Point(54, 228);
            this.txtReg3ICM.LockedOriginal = true;
            this.txtReg3ICM.Margin = new Wisej.Web.Padding(0, 3, 0, 3);
            this.txtReg3ICM.MaxLength = 1;
            this.txtReg3ICM.Name = "txtReg3ICM";
            this.txtReg3ICM.ReadOnly = true;
            this.txtReg3ICM.Size = new System.Drawing.Size(38, 15);
            this.txtReg3ICM.TabIndex = 34;
            this.txtReg3ICM.Text = "N";
            this.txtReg3ICM.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg3ICM, "N = None I = Individual  M= Municipal  C = Commercial  D = Doing Business As");
            this.txtReg3ICM.Enter += new System.EventHandler(this.txtReg3ICM_Enter);
            this.txtReg3ICM.Leave += new System.EventHandler(this.txtReg3ICM_Leave);
            this.txtReg3ICM.Click += new System.EventHandler(this.txtReg3ICM_Click);
            this.txtReg3ICM.TextChanged += new System.EventHandler(this.txtReg3ICM_TextChanged);
            this.txtReg3ICM.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg3ICM_Validating);
            // 
            // txtReg3LR
            // 
            this.txtReg3LR.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg3LR.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg3LR.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg3LR.Location = new System.Drawing.Point(19, 228);
            this.txtReg3LR.LockedOriginal = true;
            this.txtReg3LR.Margin = new Wisej.Web.Padding(0, 3, 0, 3);
            this.txtReg3LR.MaxLength = 1;
            this.txtReg3LR.Name = "txtReg3LR";
            this.txtReg3LR.ReadOnly = true;
            this.txtReg3LR.Size = new System.Drawing.Size(38, 15);
            this.txtReg3LR.TabIndex = 33;
            this.txtReg3LR.Text = "N";
            this.txtReg3LR.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg3LR, "N = None  E = Lessee   R = Lessor  T = Trustee  P = Personal Representative");
            this.txtReg3LR.Enter += new System.EventHandler(this.txtReg3LR_Enter);
            this.txtReg3LR.Leave += new System.EventHandler(this.txtReg3LR_Leave);
            this.txtReg3LR.Click += new System.EventHandler(this.txtReg3LR_Click);
            // 
            // txtReg1LR
            // 
            this.txtReg1LR.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg1LR.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg1LR.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg1LR.Location = new System.Drawing.Point(19, 162);
            this.txtReg1LR.Margin = new Wisej.Web.Padding(0, 3, 0, 3);
            this.txtReg1LR.MaxLength = 1;
            this.txtReg1LR.Name = "txtReg1LR";
            this.txtReg1LR.Size = new System.Drawing.Size(38, 15);
            this.txtReg1LR.TabIndex = 21;
            this.txtReg1LR.Text = "N";
            this.txtReg1LR.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg1LR, "N = None  E = Lessee   R = Lessor  T = Trustee  P = Personal Representative");
            this.txtReg1LR.Enter += new System.EventHandler(this.txtReg1LR_Enter);
            this.txtReg1LR.Leave += new System.EventHandler(this.txtReg1LR_Leave);
            this.txtReg1LR.Click += new System.EventHandler(this.txtReg1LR_Click);
            // 
            // txtReg2LR
            // 
            this.txtReg2LR.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg2LR.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg2LR.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg2LR.Location = new System.Drawing.Point(19, 195);
            this.txtReg2LR.Margin = new Wisej.Web.Padding(0, 3, 0, 3);
            this.txtReg2LR.MaxLength = 1;
            this.txtReg2LR.Name = "txtReg2LR";
            this.txtReg2LR.Size = new System.Drawing.Size(38, 15);
            this.txtReg2LR.TabIndex = 27;
            this.txtReg2LR.Text = "N";
            this.txtReg2LR.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg2LR, "N = None  E = Lessee   R = Lessor  T = Trustee  P = Personal Representative");
            this.txtReg2LR.Enter += new System.EventHandler(this.txtReg2LR_Enter);
            this.txtReg2LR.Leave += new System.EventHandler(this.txtReg2LR_Leave);
            this.txtReg2LR.Click += new System.EventHandler(this.txtReg2LR_Click);
            // 
            // txtReg1PartyID
            // 
            this.txtReg1PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg1PartyID.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg1PartyID.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg1PartyID.Location = new System.Drawing.Point(99, 162);
            this.txtReg1PartyID.Name = "txtReg1PartyID";
            this.txtReg1PartyID.Size = new System.Drawing.Size(70, 15);
            this.txtReg1PartyID.TabIndex = 23;
            this.txtReg1PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1PartyID_Validating);
            this.txtReg1PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg1PartyID_KeyPress);
            // 
            // txtReg2PartyID
            // 
            this.txtReg2PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg2PartyID.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg2PartyID.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg2PartyID.Location = new System.Drawing.Point(99, 195);
            this.txtReg2PartyID.Name = "txtReg2PartyID";
            this.txtReg2PartyID.Size = new System.Drawing.Size(70, 15);
            this.txtReg2PartyID.TabIndex = 29;
            this.txtReg2PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2PartyID_Validating);
            this.txtReg2PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg2PartyID_KeyPress);
            // 
            // txtReg3PartyID
            // 
            this.txtReg3PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg3PartyID.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg3PartyID.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg3PartyID.Location = new System.Drawing.Point(99, 228);
            this.txtReg3PartyID.Name = "txtReg3PartyID";
            this.txtReg3PartyID.Size = new System.Drawing.Size(70, 15);
            this.txtReg3PartyID.TabIndex = 35;
            this.txtReg3PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg3PartyID_Validating);
            this.txtReg3PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg3PartyID_KeyPress);
            // 
            // txtCountry
            // 
            this.txtCountry.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCountry.Cursor = Wisej.Web.Cursors.Default;
            this.txtCountry.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtCountry.Location = new System.Drawing.Point(168, 456);
            this.txtCountry.MaxLength = 2;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(60, 15);
            this.txtCountry.TabIndex = 49;
            this.txtCountry.TabStop = false;
            this.txtCountry.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // txtResCountry
            // 
            this.txtResCountry.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtResCountry.Cursor = Wisej.Web.Cursors.Default;
            this.txtResCountry.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtResCountry.Location = new System.Drawing.Point(330, 456);
            this.txtResCountry.MaxLength = 2;
            this.txtResCountry.Name = "txtResCountry";
            this.txtResCountry.Size = new System.Drawing.Size(72, 15);
            this.txtResCountry.TabIndex = 53;
            this.txtResCountry.TabStop = false;
            this.txtResCountry.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // cboBattle
            // 
            this.cboBattle.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cboBattle.Location = new System.Drawing.Point(635, 55);
            this.cboBattle.Name = "cboBattle";
            this.cboBattle.Size = new System.Drawing.Size(254, 15);
            this.cboBattle.TabIndex = 8;
            this.cboBattle.DropDown += new System.EventHandler(this.cboBattle_DropDown);
            // 
            // chkTransferExempt
            // 
            this.chkTransferExempt.AutoSize = false;
            this.chkTransferExempt.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkTransferExempt.Location = new System.Drawing.Point(597, 333);
            this.chkTransferExempt.Name = "chkTransferExempt";
            this.chkTransferExempt.Size = new System.Drawing.Size(15, 15);
            this.chkTransferExempt.TabIndex = 72;
            this.chkTransferExempt.TabStop = false;
            this.chkTransferExempt.CheckedChanged += new System.EventHandler(this.chkTransferExempt_CheckedChanged);
            this.chkTransferExempt.Leave += new System.EventHandler(this.chkTransferExempt_Leave);
            // 
            // chkRental
            // 
            this.chkRental.AutoSize = false;
            this.chkRental.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkRental.Location = new System.Drawing.Point(879, 519);
            this.chkRental.Name = "chkRental";
            this.chkRental.Size = new System.Drawing.Size(180, 15);
            this.chkRental.TabIndex = 93;
            this.chkRental.Text = "Rental Vehicle?";
            this.chkRental.CheckedChanged += new System.EventHandler(this.chkRental_CheckedChanged);
            this.chkRental.Leave += new System.EventHandler(this.chkRental_Leave);
            // 
            // chkEAP
            // 
            this.chkEAP.Anchor = Wisej.Web.AnchorStyles.Left;
            this.chkEAP.AutoSize = false;
            this.chkEAP.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkEAP.Location = new System.Drawing.Point(621, 517);
            this.chkEAP.Name = "chkEAP";
            this.chkEAP.Size = new System.Drawing.Size(183, 16);
            this.chkEAP.TabIndex = 87;
            this.chkEAP.Text = "Excise Already Paid";
            this.chkEAP.CheckedChanged += new System.EventHandler(this.chkEAP_CheckedChanged);
            // 
            // txtYStickerNumber
            // 
            this.txtYStickerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYStickerNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtYStickerNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtYStickerNumber.Location = new System.Drawing.Point(186, 536);
            this.txtYStickerNumber.Name = "txtYStickerNumber";
            this.txtYStickerNumber.Size = new System.Drawing.Size(112, 15);
            this.txtYStickerNumber.TabIndex = 61;
            this.txtYStickerNumber.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtYStickerNumber.Enter += new System.EventHandler(this.txtYStickerNumber_Enter);
            this.txtYStickerNumber.Leave += new System.EventHandler(this.txtYStickerNumber_Leave);
            this.txtYStickerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtYStickerNumber_Validating);
            this.txtYStickerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYStickerNumber_KeyPress);
            this.txtYStickerNumber.KeyUp += new Wisej.Web.KeyEventHandler(this.txtYStickerNumber_KeyUp);
            // 
            // txtMStickerNumber
            // 
            this.txtMStickerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMStickerNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtMStickerNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            clientEvent1.Event = "focusin";
            this.javaScript1.GetJavaScriptEvents(this.txtMStickerNumber).Add(clientEvent1);
            this.txtMStickerNumber.Location = new System.Drawing.Point(186, 508);
            this.txtMStickerNumber.Name = "txtMStickerNumber";
            this.txtMStickerNumber.Size = new System.Drawing.Size(112, 15);
            this.txtMStickerNumber.TabIndex = 57;
            this.txtMStickerNumber.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMStickerNumber.Enter += new System.EventHandler(this.txtMStickerNumber_Enter);
            this.txtMStickerNumber.Leave += new System.EventHandler(this.txtMStickerNumber_Leave);
            this.txtMStickerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMStickerNumber_Validating);
            this.txtMStickerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMStickerNumber_KeyPress);
            this.txtMStickerNumber.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMStickerNumber_KeyUp);
            // 
            // txtCTANumber
            // 
            this.txtCTANumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCTANumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtCTANumber.Location = new System.Drawing.Point(975, 307);
            this.txtCTANumber.MaxLength = 9;
            this.txtCTANumber.Name = "txtCTANumber";
            this.txtCTANumber.Size = new System.Drawing.Size(85, 15);
            this.txtCTANumber.TabIndex = 82;
            this.txtCTANumber.Enter += new System.EventHandler(this.txtCTANumber_Enter);
            this.txtCTANumber.Leave += new System.EventHandler(this.txtCTANumber_Leave);
            this.txtCTANumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtCTANumber_Validate);
            this.txtCTANumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCTANumber_KeyPressEvent);
            // 
            // txtZip
            // 
            this.txtZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtZip.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtZip.Location = new System.Drawing.Point(79, 456);
            this.txtZip.MaxLength = 15;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(74, 15);
            this.txtZip.TabIndex = 48;
            this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
            this.txtZip.Leave += new System.EventHandler(this.txtZip_Leave);
            this.txtZip.Validating += new System.ComponentModel.CancelEventHandler(this.txtZip_Validate);
            // 
            // txtPriorTitleState
            // 
            this.txtPriorTitleState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPriorTitleState.Cursor = Wisej.Web.Cursors.Default;
            this.txtPriorTitleState.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPriorTitleState.Location = new System.Drawing.Point(408, 536);
            this.txtPriorTitleState.MaxLength = 2;
            this.txtPriorTitleState.Name = "txtPriorTitleState";
            this.txtPriorTitleState.Size = new System.Drawing.Size(91, 15);
            this.txtPriorTitleState.TabIndex = 62;
            this.txtPriorTitleState.TabStop = false;
            this.txtPriorTitleState.Enter += new System.EventHandler(this.txtPriorTitleState_Enter);
            this.txtPriorTitleState.Leave += new System.EventHandler(this.txtPriorTitleState_Leave);
            this.txtPriorTitleState.Validating += new System.ComponentModel.CancelEventHandler(this.txtPriorTitleState_Validating);
            // 
            // txtCreditNumber
            // 
            this.txtCreditNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCreditNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtCreditNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtCreditNumber.Location = new System.Drawing.Point(764, 394);
            this.txtCreditNumber.MaxLength = 8;
            this.txtCreditNumber.Name = "txtCreditNumber";
            this.txtCreditNumber.Size = new System.Drawing.Size(90, 15);
            this.txtCreditNumber.TabIndex = 75;
            this.txtCreditNumber.TabStop = false;
            this.txtCreditNumber.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCreditNumber.Enter += new System.EventHandler(this.txtCreditNumber_Enter);
            this.txtCreditNumber.Leave += new System.EventHandler(this.txtCreditNumber_Leave);
            this.txtCreditNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCreditNumber_KeyPress);
            // 
            // chkVIN
            // 
            this.chkVIN.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkVIN.Location = new System.Drawing.Point(900, 53);
            this.chkVIN.Name = "chkVIN";
            this.chkVIN.Size = new System.Drawing.Size(109, 22);
            this.chkVIN.TabIndex = 16;
            this.chkVIN.Text = "Use This VIN";
            this.chkVIN.Visible = false;
            this.chkVIN.CheckedChanged += new System.EventHandler(this.chkVIN_CheckedChanged);
            // 
            // txtResState
            // 
            this.txtResState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtResState.Cursor = Wisej.Web.Cursors.Default;
            this.txtResState.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtResState.Location = new System.Drawing.Point(270, 456);
            this.txtResState.MaxLength = 2;
            this.txtResState.Name = "txtResState";
            this.txtResState.Size = new System.Drawing.Size(50, 15);
            this.txtResState.TabIndex = 52;
            this.txtResState.TabStop = false;
            this.txtResState.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtResState.Enter += new System.EventHandler(this.txtResState_Enter);
            this.txtResState.Leave += new System.EventHandler(this.txtResState_Leave);
            this.txtResState.KeyDown += new Wisej.Web.KeyEventHandler(this.txtResState_KeyDown);
            // 
            // chkAmputeeVet
            // 
            this.chkAmputeeVet.AutoSize = false;
            this.chkAmputeeVet.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkAmputeeVet.Location = new System.Drawing.Point(621, 474);
            this.chkAmputeeVet.Name = "chkAmputeeVet";
            this.chkAmputeeVet.Size = new System.Drawing.Size(183, 16);
            this.chkAmputeeVet.TabIndex = 86;
            this.chkAmputeeVet.TabStop = false;
            this.chkAmputeeVet.Text = "Amputee Vet.?";
            this.chkAmputeeVet.CheckedChanged += new System.EventHandler(this.chkAmputeeVet_CheckedChanged);
            this.chkAmputeeVet.Leave += new System.EventHandler(this.chkAmputeeVet_Leave);
            // 
            // chkTrailerVanity
            // 
            this.chkTrailerVanity.AutoSize = false;
            this.chkTrailerVanity.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkTrailerVanity.Location = new System.Drawing.Point(879, 498);
            this.chkTrailerVanity.Name = "chkTrailerVanity";
            this.chkTrailerVanity.Size = new System.Drawing.Size(180, 15);
            this.chkTrailerVanity.TabIndex = 91;
            this.chkTrailerVanity.TabStop = false;
            this.chkTrailerVanity.Text = "Trailer Vanity Plate?";
            this.chkTrailerVanity.CheckedChanged += new System.EventHandler(this.chkTrailerVanity_CheckedChanged);
            this.chkTrailerVanity.Leave += new System.EventHandler(this.chkTrailerVanity_Leave);
            // 
            // chkExciseExempt
            // 
            this.chkExciseExempt.AutoSize = false;
            this.chkExciseExempt.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExciseExempt.Location = new System.Drawing.Point(597, 246);
            this.chkExciseExempt.Name = "chkExciseExempt";
            this.chkExciseExempt.Size = new System.Drawing.Size(15, 15);
            this.chkExciseExempt.TabIndex = 68;
            this.chkExciseExempt.TabStop = false;
            this.chkExciseExempt.CheckedChanged += new System.EventHandler(this.chkExciseExempt_CheckedChanged);
            this.chkExciseExempt.Leave += new System.EventHandler(this.chkExciseExempt_Leave);
            // 
            // chkAgentFeeExempt
            // 
            this.chkAgentFeeExempt.AutoSize = false;
            this.chkAgentFeeExempt.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkAgentFeeExempt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkAgentFeeExempt.Location = new System.Drawing.Point(597, 217);
            this.chkAgentFeeExempt.Name = "chkAgentFeeExempt";
            this.chkAgentFeeExempt.Size = new System.Drawing.Size(15, 15);
            this.chkAgentFeeExempt.TabIndex = 66;
            this.chkAgentFeeExempt.TabStop = false;
            this.chkAgentFeeExempt.CheckedChanged += new System.EventHandler(this.chkAgentFeeExempt_CheckedChanged);
            this.chkAgentFeeExempt.Leave += new System.EventHandler(this.chkAgentFeeExempt_Leave);
            // 
            // chkStateExempt
            // 
            this.chkStateExempt.AutoSize = false;
            this.chkStateExempt.Location = new System.Drawing.Point(864, 159);
            this.chkStateExempt.Name = "chkStateExempt";
            this.chkStateExempt.Size = new System.Drawing.Size(15, 15);
            this.chkStateExempt.TabIndex = 76;
            this.chkStateExempt.TabStop = false;
            this.chkStateExempt.CheckedChanged += new System.EventHandler(this.chkStateExempt_CheckedChanged);
            this.chkStateExempt.Leave += new System.EventHandler(this.chkStateExempt_Leave);
            // 
            // chk2Year
            // 
            this.chk2Year.Anchor = Wisej.Web.AnchorStyles.Left;
            this.chk2Year.AutoSize = false;
            this.chk2Year.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chk2Year.Location = new System.Drawing.Point(879, 540);
            this.chk2Year.Name = "chk2Year";
            this.chk2Year.Size = new System.Drawing.Size(180, 15);
            this.chk2Year.TabIndex = 92;
            this.chk2Year.TabStop = false;
            this.chk2Year.Text = "Two Year?";
            this.chk2Year.CheckedChanged += new System.EventHandler(this.chk2Year_CheckedChanged);
            this.chk2Year.MouseDown += new Wisej.Web.MouseEventHandler(this.chk2Year_MouseDown);
            this.chk2Year.MouseUp += new Wisej.Web.MouseEventHandler(this.chk2Year_MouseUp);
            // 
            // chkETO
            // 
            this.chkETO.Anchor = Wisej.Web.AnchorStyles.Left;
            this.chkETO.AutoSize = false;
            this.chkETO.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkETO.Location = new System.Drawing.Point(621, 539);
            this.chkETO.Name = "chkETO";
            this.chkETO.Size = new System.Drawing.Size(183, 15);
            this.chkETO.TabIndex = 90;
            this.chkETO.TabStop = false;
            this.chkETO.Text = "Excise Tax Only?";
            this.chkETO.CheckedChanged += new System.EventHandler(this.chkETO_CheckedChanged);
            // 
            // txtState
            // 
            this.txtState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtState.Cursor = Wisej.Web.Cursors.Default;
            this.txtState.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtState.Location = new System.Drawing.Point(19, 456);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(50, 15);
            this.txtState.TabIndex = 47;
            this.txtState.Enter += new System.EventHandler(this.txtState_Enter);
            this.txtState.Leave += new System.EventHandler(this.txtState_Leave);
            this.txtState.Validating += new System.ComponentModel.CancelEventHandler(this.txtState_Validating);
            // 
            // txtMileage
            // 
            this.txtMileage.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMileage.Cursor = Wisej.Web.Cursors.Default;
            this.txtMileage.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtMileage.Location = new System.Drawing.Point(267, 55);
            this.txtMileage.MaxLength = 8;
            this.txtMileage.Name = "txtMileage";
            this.txtMileage.Size = new System.Drawing.Size(114, 15);
            this.txtMileage.TabIndex = 5;
            this.txtMileage.TabStop = false;
            this.txtMileage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMileage.Enter += new System.EventHandler(this.txtMileage_Enter);
            this.txtMileage.Leave += new System.EventHandler(this.txtMileage_Leave);
            this.txtMileage.Validating += new System.ComponentModel.CancelEventHandler(this.txtMileage_Validating);
            this.txtMileage.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMileage_KeyPress);
            // 
            // txtMilYear
            // 
            this.txtMilYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMilYear.Cursor = Wisej.Web.Cursors.Default;
            this.txtMilYear.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtMilYear.Location = new System.Drawing.Point(729, 191);
            this.txtMilYear.MaxLength = 1;
            this.txtMilYear.Name = "txtMilYear";
            this.txtMilYear.Size = new System.Drawing.Size(55, 15);
            this.txtMilYear.TabIndex = 64;
            this.txtMilYear.TabStop = false;
            this.txtMilYear.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMilYear.Enter += new System.EventHandler(this.txtMilYear_Enter);
            this.txtMilYear.Leave += new System.EventHandler(this.txtMilYear_Leave);
            // 
            // txtYearNoCharge
            // 
            this.txtYearNoCharge.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYearNoCharge.Cursor = Wisej.Web.Cursors.Default;
            this.txtYearNoCharge.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtYearNoCharge.Location = new System.Drawing.Point(116, 536);
            this.txtYearNoCharge.MaxLength = 1;
            this.txtYearNoCharge.Name = "txtYearNoCharge";
            this.txtYearNoCharge.Size = new System.Drawing.Size(40, 15);
            this.txtYearNoCharge.TabIndex = 60;
            this.txtYearNoCharge.TabStop = false;
            this.txtYearNoCharge.Enter += new System.EventHandler(this.txtYearNoCharge_Enter);
            this.txtYearNoCharge.Leave += new System.EventHandler(this.txtYearNoCharge_Leave);
            this.txtYearNoCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearNoCharge_Validating);
            this.txtYearNoCharge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYearNoCharge_KeyPress);
            // 
            // txtMonthNoCharge
            // 
            this.txtMonthNoCharge.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMonthNoCharge.Cursor = Wisej.Web.Cursors.Default;
            this.txtMonthNoCharge.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtMonthNoCharge.Location = new System.Drawing.Point(116, 508);
            this.txtMonthNoCharge.MaxLength = 1;
            this.txtMonthNoCharge.Name = "txtMonthNoCharge";
            this.txtMonthNoCharge.Size = new System.Drawing.Size(40, 15);
            this.txtMonthNoCharge.TabIndex = 56;
            this.txtMonthNoCharge.TabStop = false;
            this.txtMonthNoCharge.Enter += new System.EventHandler(this.txtMonthNoCharge_Enter);
            this.txtMonthNoCharge.Leave += new System.EventHandler(this.txtMonthNoCharge_Leave);
            this.txtMonthNoCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNoCharge_Validating);
            this.txtMonthNoCharge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMonthNoCharge_KeyPress);
            // 
            // txtYearCharge
            // 
            this.txtYearCharge.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYearCharge.Cursor = Wisej.Web.Cursors.Default;
            this.txtYearCharge.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtYearCharge.Location = new System.Drawing.Point(66, 536);
            this.txtYearCharge.MaxLength = 1;
            this.txtYearCharge.Name = "txtYearCharge";
            this.txtYearCharge.Size = new System.Drawing.Size(40, 15);
            this.txtYearCharge.TabIndex = 59;
            this.txtYearCharge.TabStop = false;
            this.txtYearCharge.Enter += new System.EventHandler(this.txtYearCharge_Enter);
            this.txtYearCharge.Leave += new System.EventHandler(this.txtYearCharge_Leave);
            this.txtYearCharge.TextChanged += new System.EventHandler(this.txtYearCharge_TextChanged);
            this.txtYearCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearCharge_Validating);
            this.txtYearCharge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYearCharge_KeyPress);
            // 
            // txtMonthCharge
            // 
            this.txtMonthCharge.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMonthCharge.Cursor = Wisej.Web.Cursors.Default;
            this.txtMonthCharge.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtMonthCharge.Location = new System.Drawing.Point(66, 508);
            this.txtMonthCharge.MaxLength = 1;
            this.txtMonthCharge.Name = "txtMonthCharge";
            this.txtMonthCharge.Size = new System.Drawing.Size(40, 15);
            this.txtMonthCharge.TabIndex = 55;
            this.txtMonthCharge.TabStop = false;
            this.txtMonthCharge.Enter += new System.EventHandler(this.txtMonthCharge_Enter);
            this.txtMonthCharge.Leave += new System.EventHandler(this.txtMonthCharge_Leave);
            this.txtMonthCharge.TextChanged += new System.EventHandler(this.txtMonthCharge_TextChanged);
            this.txtMonthCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthCharge_Validating);
            this.txtMonthCharge.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMonthCharge_KeyPress);
            // 
            // txtPreviousTitle
            // 
            this.txtPreviousTitle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPreviousTitle.Cursor = Wisej.Web.Cursors.Default;
            this.txtPreviousTitle.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPreviousTitle.Location = new System.Drawing.Point(363, 508);
            this.txtPreviousTitle.MaxLength = 20;
            this.txtPreviousTitle.Name = "txtPreviousTitle";
            this.txtPreviousTitle.Size = new System.Drawing.Size(136, 15);
            this.txtPreviousTitle.TabIndex = 58;
            this.txtPreviousTitle.TabStop = false;
            this.txtPreviousTitle.Enter += new System.EventHandler(this.txtPreviousTitle_Enter);
            this.txtPreviousTitle.Leave += new System.EventHandler(this.txtPreviousTitle_Leave);
            this.txtPreviousTitle.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPreviousTitle_KeyPress);
            // 
            // txtMilRate
            // 
            this.txtMilRate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMilRate.Cursor = Wisej.Web.Cursors.Default;
            this.txtMilRate.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtMilRate.Location = new System.Drawing.Point(794, 191);
            this.txtMilRate.LockedOriginal = true;
            this.txtMilRate.MaxLength = 6;
            this.txtMilRate.Name = "txtMilRate";
            this.txtMilRate.ReadOnly = true;
            this.txtMilRate.Size = new System.Drawing.Size(60, 15);
            this.txtMilRate.TabIndex = 65;
            this.txtMilRate.TabStop = false;
            this.txtMilRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtFuel
            // 
            this.txtFuel.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtFuel.Cursor = Wisej.Web.Cursors.Default;
            this.txtFuel.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtFuel.Location = new System.Drawing.Point(976, 110);
            this.txtFuel.MaxLength = 1;
            this.txtFuel.Name = "txtFuel";
            this.txtFuel.Size = new System.Drawing.Size(50, 15);
            this.txtFuel.TabIndex = 20;
            this.txtFuel.TabStop = false;
            this.txtFuel.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtFuel.Enter += new System.EventHandler(this.txtFuel_Enter);
            this.txtFuel.Leave += new System.EventHandler(this.txtFuel_Leave);
            this.txtFuel.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuel_Validating);
            // 
            // txtRegWeight
            // 
            this.txtRegWeight.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRegWeight.Cursor = Wisej.Web.Cursors.Default;
            this.txtRegWeight.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegWeight.Location = new System.Drawing.Point(886, 110);
            this.txtRegWeight.MaxLength = 6;
            this.txtRegWeight.Name = "txtRegWeight";
            this.txtRegWeight.Size = new System.Drawing.Size(80, 15);
            this.txtRegWeight.TabIndex = 19;
            this.txtRegWeight.TabStop = false;
            this.txtRegWeight.Enter += new System.EventHandler(this.txtRegWeight_Enter);
            this.txtRegWeight.Leave += new System.EventHandler(this.txtRegWeight_Leave);
            this.txtRegWeight.Validating += new System.ComponentModel.CancelEventHandler(this.txtRegWeight_Validating);
            this.txtRegWeight.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRegWeight_KeyPress);
            // 
            // txtNetWeight
            // 
            this.txtNetWeight.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtNetWeight.Cursor = Wisej.Web.Cursors.Default;
            this.txtNetWeight.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtNetWeight.Location = new System.Drawing.Point(802, 110);
            this.txtNetWeight.MaxLength = 6;
            this.txtNetWeight.Name = "txtNetWeight";
            this.txtNetWeight.Size = new System.Drawing.Size(74, 15);
            this.txtNetWeight.TabIndex = 18;
            this.txtNetWeight.TabStop = false;
            this.txtNetWeight.Enter += new System.EventHandler(this.txtNetWeight_Enter);
            this.txtNetWeight.Leave += new System.EventHandler(this.txtNetWeight_Leave);
            this.txtNetWeight.Validating += new System.ComponentModel.CancelEventHandler(this.txtNetWeight_Validating);
            this.txtNetWeight.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNetWeight_KeyPress);
            // 
            // txtRegistrationNumber
            // 
            this.txtRegistrationNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRegistrationNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtRegistrationNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegistrationNumber.Location = new System.Drawing.Point(479, 55);
            this.txtRegistrationNumber.LockedOriginal = true;
            this.txtRegistrationNumber.Name = "txtRegistrationNumber";
            this.txtRegistrationNumber.ReadOnly = true;
            this.txtRegistrationNumber.Size = new System.Drawing.Size(150, 15);
            this.txtRegistrationNumber.TabIndex = 7;
            this.txtRegistrationNumber.TabStop = false;
            this.txtRegistrationNumber.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // txtClass
            // 
            this.txtClass.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtClass.Cursor = Wisej.Web.Cursors.Default;
            this.txtClass.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtClass.Location = new System.Drawing.Point(397, 55);
            this.txtClass.LockedOriginal = true;
            this.txtClass.Name = "txtClass";
            this.txtClass.ReadOnly = true;
            this.txtClass.Size = new System.Drawing.Size(70, 15);
            this.txtClass.TabIndex = 6;
            this.txtClass.TabStop = false;
            this.txtClass.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // txtVin
            // 
            this.txtVin.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtVin.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtVin.Location = new System.Drawing.Point(19, 110);
            this.txtVin.MaxLength = 17;
            this.txtVin.Name = "txtVin";
            this.txtVin.Size = new System.Drawing.Size(203, 15);
            this.txtVin.TabIndex = 9;
            this.txtVin.Enter += new System.EventHandler(this.txtVin_Enter);
            this.txtVin.Leave += new System.EventHandler(this.txtVin_Leave);
            this.txtVin.TextChanged += new System.EventHandler(this.txtVin_Change);
            this.txtVin.Validating += new System.ComponentModel.CancelEventHandler(this.txtVin_Validate);
            this.txtVin.KeyDown += new Wisej.Web.KeyEventHandler(this.txtVin_KeyDownEvent);
            // 
            // txtBase
            // 
            this.txtBase.AutoSize = false;
            this.txtBase.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtBase.Location = new System.Drawing.Point(729, 162);
            this.txtBase.MaxLength = 9;
            this.txtBase.Name = "txtBase";
            this.txtBase.Size = new System.Drawing.Size(125, 15);
            this.txtBase.TabIndex = 63;
            this.txtBase.TabStop = false;
            this.txtBase.Text = "0";
            this.txtBase.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtBase.Enter += new System.EventHandler(this.txtBase_Enter);
            this.txtBase.Leave += new System.EventHandler(this.txtBase_Leave);
            this.txtBase.TextChanged += new System.EventHandler(this.txtBase_Change);
            this.txtBase.Validating += new System.ComponentModel.CancelEventHandler(this.txtBase_Validate);
            // 
            // txtAgentFee
            // 
            this.txtAgentFee.AutoSize = false;
            this.txtAgentFee.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtAgentFee.Location = new System.Drawing.Point(729, 220);
            this.txtAgentFee.MaxLength = 7;
            this.txtAgentFee.Name = "txtAgentFee";
            this.txtAgentFee.Size = new System.Drawing.Size(125, 15);
            this.txtAgentFee.TabIndex = 67;
            this.txtAgentFee.TabStop = false;
            this.txtAgentFee.Text = "0.00";
            this.txtAgentFee.Enter += new System.EventHandler(this.txtAgentFee_Enter);
            this.txtAgentFee.Leave += new System.EventHandler(this.txtAgentFee_Leave);
            // 
            // txtTownCredit
            // 
            this.txtTownCredit.AutoSize = false;
            this.txtTownCredit.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtTownCredit.Location = new System.Drawing.Point(764, 278);
            this.txtTownCredit.MaxLength = 9;
            this.txtTownCredit.Name = "txtTownCredit";
            this.txtTownCredit.Size = new System.Drawing.Size(90, 15);
            this.txtTownCredit.TabIndex = 70;
            this.txtTownCredit.TabStop = false;
            this.txtTownCredit.Text = "0.00";
            this.txtTownCredit.Enter += new System.EventHandler(this.txtTownCredit_Enter);
            this.txtTownCredit.Leave += new System.EventHandler(this.txtTownCredit_Leave);
            // 
            // txtBalance
            // 
            this.txtBalance.AutoSize = false;
            this.txtBalance.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtBalance.Location = new System.Drawing.Point(764, 365);
            this.txtBalance.MaxLength = 9;
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(90, 15);
            this.txtBalance.TabIndex = 74;
            this.txtBalance.TabStop = false;
            this.txtBalance.Text = "0.00";
            this.txtBalance.Enter += new System.EventHandler(this.txtBalance_Enter);
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.AutoSize = false;
            this.txtSubTotal.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtSubTotal.Location = new System.Drawing.Point(764, 307);
            this.txtSubTotal.MaxLength = 9;
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.Size = new System.Drawing.Size(90, 15);
            this.txtSubTotal.TabIndex = 71;
            this.txtSubTotal.TabStop = false;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.Enter += new System.EventHandler(this.txtSubTotal_Enter);
            this.txtSubTotal.Leave += new System.EventHandler(this.txtSubTotal_Leave);
            // 
            // txtTransferCharge
            // 
            this.txtTransferCharge.AutoSize = false;
            this.txtTransferCharge.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtTransferCharge.Location = new System.Drawing.Point(764, 336);
            this.txtTransferCharge.MaxLength = 8;
            this.txtTransferCharge.Name = "txtTransferCharge";
            this.txtTransferCharge.Size = new System.Drawing.Size(90, 15);
            this.txtTransferCharge.TabIndex = 73;
            this.txtTransferCharge.TabStop = false;
            this.txtTransferCharge.Text = "0.00";
            this.txtTransferCharge.Enter += new System.EventHandler(this.txtTransferCharge_Enter);
            this.txtTransferCharge.Leave += new System.EventHandler(this.txtTransferCharge_Leave);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtTitle.Location = new System.Drawing.Point(975, 278);
            this.txtTitle.MaxLength = 8;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(85, 15);
            this.txtTitle.TabIndex = 81;
            this.txtTitle.TabStop = false;
            this.txtTitle.Text = "0.00";
            this.txtTitle.Enter += new System.EventHandler(this.txtTitle_Enter);
            this.txtTitle.Leave += new System.EventHandler(this.txtTitle_Leave);
            this.txtTitle.TextChanged += new System.EventHandler(this.txtTitle_Change);
            // 
            // txtSalesTax
            // 
            this.txtSalesTax.AutoSize = false;
            this.txtSalesTax.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtSalesTax.Location = new System.Drawing.Point(975, 249);
            this.txtSalesTax.MaxLength = 9;
            this.txtSalesTax.Name = "txtSalesTax";
            this.txtSalesTax.Size = new System.Drawing.Size(85, 15);
            this.txtSalesTax.TabIndex = 80;
            this.txtSalesTax.TabStop = false;
            this.txtSalesTax.Text = "0.00";
            this.txtSalesTax.Enter += new System.EventHandler(this.txtSalesTax_Enter);
            this.txtSalesTax.Leave += new System.EventHandler(this.txtSalesTax_Leave);
            this.txtSalesTax.TextChanged += new System.EventHandler(this.txtSalesTax_Change);
            this.txtSalesTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtSalesTax_Validate);
            // 
            // txtFees
            // 
            this.txtFees.AutoSize = false;
            this.txtFees.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtFees.Location = new System.Drawing.Point(975, 220);
            this.txtFees.MaxLength = 9;
            this.txtFees.Name = "txtFees";
            this.txtFees.Size = new System.Drawing.Size(85, 15);
            this.txtFees.TabIndex = 79;
            this.txtFees.TabStop = false;
            this.txtFees.Text = "0.00";
            this.txtFees.Enter += new System.EventHandler(this.txtFees_Enter);
            this.txtFees.Leave += new System.EventHandler(this.txtFees_Leave);
            this.txtFees.DoubleClick += new System.EventHandler(this.txtFees_DblClick);
            // 
            // txtStateCredit
            // 
            this.txtStateCredit.Anchor = Wisej.Web.AnchorStyles.Left;
            this.txtStateCredit.AutoSize = false;
            this.txtStateCredit.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtStateCredit.Location = new System.Drawing.Point(975, 191);
            this.txtStateCredit.MaxLength = 9;
            this.txtStateCredit.Name = "txtStateCredit";
            this.txtStateCredit.Size = new System.Drawing.Size(85, 15);
            this.txtStateCredit.TabIndex = 78;
            this.txtStateCredit.TabStop = false;
            this.txtStateCredit.Text = "0.00";
            this.txtStateCredit.Enter += new System.EventHandler(this.txtStateCredit_Enter);
            this.txtStateCredit.Leave += new System.EventHandler(this.txtStateCredit_Leave);
            this.txtStateCredit.DoubleClick += new System.EventHandler(this.txtStateCredit_DblClick);
            // 
            // txtExpires
            // 
            this.txtExpires.AutoSize = false;
            this.txtExpires.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtExpires.Location = new System.Drawing.Point(146, 55);
            this.txtExpires.Mask = "##/##/####";
            this.txtExpires.MaxLength = 10;
            this.txtExpires.Name = "txtExpires";
            this.txtExpires.Size = new System.Drawing.Size(115, 15);
            this.txtExpires.TabIndex = 4;
            this.txtExpires.TabStop = false;
            this.txtExpires.Enter += new System.EventHandler(this.txtExpires_Enter);
            this.txtExpires.Leave += new System.EventHandler(this.txtExpires_Leave);
            this.txtExpires.Validating += new System.ComponentModel.CancelEventHandler(this.txtExpires_Validate);
            // 
            // txtEffectiveDate
            // 
            this.txtEffectiveDate.AutoSize = false;
            this.txtEffectiveDate.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtEffectiveDate.Location = new System.Drawing.Point(19, 55);
            this.txtEffectiveDate.Mask = "##/##/####";
            this.txtEffectiveDate.MaxLength = 10;
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(115, 15);
            this.txtEffectiveDate.TabIndex = 3;
            this.txtEffectiveDate.TabStop = false;
            this.txtEffectiveDate.Enter += new System.EventHandler(this.txtEffectiveDate_Enter);
            // 
            // txtYear
            // 
            this.txtYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYear.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtYear.Location = new System.Drawing.Point(231, 110);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(70, 15);
            this.txtYear.TabIndex = 10;
            this.txtYear.Enter += new System.EventHandler(this.txtYear_Enter);
            this.txtYear.Leave += new System.EventHandler(this.txtYear_Leave);
            this.txtYear.TextChanged += new System.EventHandler(this.txtYear_Change);
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validate);
            // 
            // txtMake
            // 
            this.txtMake.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMake.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtMake.Location = new System.Drawing.Point(310, 110);
            this.txtMake.MaxLength = 4;
            this.txtMake.Name = "txtMake";
            this.txtMake.Size = new System.Drawing.Size(70, 15);
            this.txtMake.TabIndex = 11;
            this.txtMake.Enter += new System.EventHandler(this.txtMake_Enter);
            this.txtMake.Leave += new System.EventHandler(this.txtMake_Leave);
            // 
            // txtModel
            // 
            this.txtModel.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtModel.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtModel.Location = new System.Drawing.Point(389, 110);
            this.txtModel.MaxLength = 6;
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(90, 15);
            this.txtModel.TabIndex = 12;
            this.txtModel.Enter += new System.EventHandler(this.txtModel_Enter);
            this.txtModel.Leave += new System.EventHandler(this.txtModel_Leave);
            // 
            // txtColor1
            // 
            this.txtColor1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor1.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtColor1.Location = new System.Drawing.Point(488, 110);
            this.txtColor1.MaxLength = 2;
            this.txtColor1.Name = "txtColor1";
            this.txtColor1.Size = new System.Drawing.Size(50, 15);
            this.txtColor1.TabIndex = 13;
            this.txtColor1.Enter += new System.EventHandler(this.txtColor1_Enter);
            this.txtColor1.Leave += new System.EventHandler(this.txtColor1_Leave);
            // 
            // txtColor2
            // 
            this.txtColor2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor2.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtColor2.Location = new System.Drawing.Point(548, 110);
            this.txtColor2.MaxLength = 2;
            this.txtColor2.Name = "txtColor2";
            this.txtColor2.Size = new System.Drawing.Size(50, 15);
            this.txtColor2.TabIndex = 14;
            this.txtColor2.Enter += new System.EventHandler(this.txtColor2_Enter);
            this.txtColor2.Leave += new System.EventHandler(this.txtColor2_Leave);
            // 
            // txtStyle
            // 
            this.txtStyle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtStyle.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtStyle.Location = new System.Drawing.Point(608, 110);
            this.txtStyle.MaxLength = 2;
            this.txtStyle.Name = "txtStyle";
            this.txtStyle.Size = new System.Drawing.Size(50, 15);
            this.txtStyle.TabIndex = 15;
            this.txtStyle.Enter += new System.EventHandler(this.txtStyle_Enter);
            this.txtStyle.Leave += new System.EventHandler(this.txtStyle_Leave);
            this.txtStyle.Validating += new System.ComponentModel.CancelEventHandler(this.txtStyle_Validating);
            // 
            // txtTires
            // 
            this.txtTires.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTires.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtTires.Location = new System.Drawing.Point(667, 110);
            this.txtTires.MaxLength = 3;
            this.txtTires.Name = "txtTires";
            this.txtTires.Size = new System.Drawing.Size(60, 15);
            this.txtTires.TabIndex = 16;
            this.txtTires.Enter += new System.EventHandler(this.txtTires_Enter);
            this.txtTires.Leave += new System.EventHandler(this.txtTires_Leave);
            // 
            // txtAxles
            // 
            this.txtAxles.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAxles.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtAxles.Location = new System.Drawing.Point(737, 110);
            this.txtAxles.MaxLength = 2;
            this.txtAxles.Name = "txtAxles";
            this.txtAxles.Size = new System.Drawing.Size(55, 15);
            this.txtAxles.TabIndex = 17;
            this.txtAxles.Enter += new System.EventHandler(this.txtAxles_Enter);
            this.txtAxles.Leave += new System.EventHandler(this.txtAxles_Leave);
            this.txtAxles.Validating += new System.ComponentModel.CancelEventHandler(this.txtAxles_Validate);
            // 
            // txtUnitNumber
            // 
            this.txtUnitNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUnitNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtUnitNumber.Location = new System.Drawing.Point(91, 304);
            this.txtUnitNumber.MaxLength = 10;
            this.txtUnitNumber.Name = "txtUnitNumber";
            this.txtUnitNumber.Size = new System.Drawing.Size(120, 15);
            this.txtUnitNumber.TabIndex = 41;
            this.txtUnitNumber.Text = "1234567890";
            this.txtUnitNumber.Enter += new System.EventHandler(this.txtUnitNumber_Enter);
            this.txtUnitNumber.Leave += new System.EventHandler(this.txtUnitNumber_Leave);
            // 
            // txtDOTNumber
            // 
            this.txtDOTNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDOTNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtDOTNumber.Location = new System.Drawing.Point(299, 304);
            this.txtDOTNumber.MaxLength = 9;
            this.txtDOTNumber.Name = "txtDOTNumber";
            this.txtDOTNumber.Size = new System.Drawing.Size(110, 15);
            this.txtDOTNumber.TabIndex = 42;
            this.txtDOTNumber.Enter += new System.EventHandler(this.txtDOTNumber_Enter);
            this.txtDOTNumber.Leave += new System.EventHandler(this.txtDOTNumber_Leave);
            this.txtDOTNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtDOTNumber_Validate);
            // 
            // txtAddress1
            // 
            this.txtAddress1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAddress1.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtAddress1.Location = new System.Drawing.Point(19, 354);
            this.txtAddress1.MaxLength = 50;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(207, 15);
            this.txtAddress1.TabIndex = 44;
            this.txtAddress1.Enter += new System.EventHandler(this.txtAddress1_Enter);
            this.txtAddress1.Leave += new System.EventHandler(this.txtAddress1_Leave);
            // 
            // txtLegalres
            // 
            this.txtLegalres.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalres.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtLegalres.Location = new System.Drawing.Point(267, 383);
            this.txtLegalres.MaxLength = 50;
            this.txtLegalres.Name = "txtLegalres";
            this.txtLegalres.Size = new System.Drawing.Size(203, 15);
            this.txtLegalres.TabIndex = 51;
            this.txtLegalres.Enter += new System.EventHandler(this.txtLegalres_Enter);
            this.txtLegalres.Leave += new System.EventHandler(this.txtLegalres_Leave);
            this.txtLegalres.KeyDown += new Wisej.Web.KeyEventHandler(this.txtLegalres_KeyDownEvent);
            // 
            // txtresidenceCode
            // 
            this.txtresidenceCode.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtresidenceCode.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtresidenceCode.Location = new System.Drawing.Point(413, 456);
            this.txtresidenceCode.MaxLength = 5;
            this.txtresidenceCode.Name = "txtresidenceCode";
            this.txtresidenceCode.Size = new System.Drawing.Size(155, 15);
            this.txtresidenceCode.TabIndex = 54;
            this.txtresidenceCode.Enter += new System.EventHandler(this.txtresidenceCode_Enter);
            this.txtresidenceCode.Leave += new System.EventHandler(this.txtresidenceCode_Leave);
            this.txtresidenceCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtresidenceCode_Validate);
            // 
            // txtCity
            // 
            this.txtCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCity.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtCity.Location = new System.Drawing.Point(19, 413);
            this.txtCity.MaxLength = 50;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(207, 15);
            this.txtCity.TabIndex = 46;
            this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
            this.txtCity.Leave += new System.EventHandler(this.txtCity_Leave);
            this.txtCity.Validating += new System.ComponentModel.CancelEventHandler(this.txtCity_Validate);
            // 
            // txtAddress2
            // 
            this.txtAddress2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAddress2.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtAddress2.Location = new System.Drawing.Point(19, 383);
            this.txtAddress2.MaxLength = 50;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(207, 15);
            this.txtAddress2.TabIndex = 45;
            this.txtAddress2.Enter += new System.EventHandler(this.txtAddress2_Enter);
            this.txtAddress2.Leave += new System.EventHandler(this.txtAddress2_Leave);
            // 
            // txtLegalResStreet
            // 
            this.txtLegalResStreet.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalResStreet.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtLegalResStreet.Location = new System.Drawing.Point(267, 354);
            this.txtLegalResStreet.MaxLength = 50;
            this.txtLegalResStreet.Name = "txtLegalResStreet";
            this.txtLegalResStreet.Size = new System.Drawing.Size(203, 15);
            this.txtLegalResStreet.TabIndex = 50;
            this.txtLegalResStreet.Enter += new System.EventHandler(this.txtLegalResStreet_Enter);
            this.txtLegalResStreet.Leave += new System.EventHandler(this.txtLegalResStreet_Leave);
            this.txtLegalResStreet.KeyDown += new Wisej.Web.KeyEventHandler(this.txtLegalResStreet_KeyDownEvent);
            // 
            // txtDoubleCTANumber
            // 
            this.txtDoubleCTANumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDoubleCTANumber.Enabled = false;
            this.txtDoubleCTANumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtDoubleCTANumber.Location = new System.Drawing.Point(975, 336);
            this.txtDoubleCTANumber.MaxLength = 11;
            this.txtDoubleCTANumber.Name = "txtDoubleCTANumber";
            this.txtDoubleCTANumber.Size = new System.Drawing.Size(85, 15);
            this.txtDoubleCTANumber.TabIndex = 83;
            // 
            // txtTaxIDNumber
            // 
            this.txtTaxIDNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTaxIDNumber.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtTaxIDNumber.Location = new System.Drawing.Point(500, 304);
            this.txtTaxIDNumber.MaxLength = 9;
            this.txtTaxIDNumber.Name = "txtTaxIDNumber";
            this.txtTaxIDNumber.Size = new System.Drawing.Size(98, 15);
            this.txtTaxIDNumber.TabIndex = 43;
            // 
            // txtReg2DOB
            // 
            this.txtReg2DOB.AutoSize = false;
            this.txtReg2DOB.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg2DOB.Location = new System.Drawing.Point(488, 195);
            this.txtReg2DOB.Mask = "##/##/####";
            this.txtReg2DOB.MaxLength = 10;
            this.txtReg2DOB.Name = "txtReg2DOB";
            this.txtReg2DOB.Size = new System.Drawing.Size(110, 15);
            this.txtReg2DOB.TabIndex = 32;
            this.txtReg2DOB.Visible = false;
            this.txtReg2DOB.Enter += new System.EventHandler(this.txtReg2DOB_Enter);
            this.txtReg2DOB.Leave += new System.EventHandler(this.txtReg2DOB_Leave);
            // 
            // txtReg1DOB
            // 
            this.txtReg1DOB.AutoSize = false;
            this.txtReg1DOB.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg1DOB.Location = new System.Drawing.Point(488, 162);
            this.txtReg1DOB.Mask = "##/##/####";
            this.txtReg1DOB.MaxLength = 10;
            this.txtReg1DOB.Name = "txtReg1DOB";
            this.txtReg1DOB.Size = new System.Drawing.Size(110, 15);
            this.txtReg1DOB.TabIndex = 26;
            this.txtReg1DOB.Enter += new System.EventHandler(this.txtReg1DOB_Enter);
            this.txtReg1DOB.Leave += new System.EventHandler(this.txtReg1DOB_Leave);
            // 
            // txtReg3DOB
            // 
            this.txtReg3DOB.AutoSize = false;
            this.txtReg3DOB.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtReg3DOB.Location = new System.Drawing.Point(489, 228);
            this.txtReg3DOB.Mask = "##/##/####";
            this.txtReg3DOB.MaxLength = 10;
            this.txtReg3DOB.Name = "txtReg3DOB";
            this.txtReg3DOB.Size = new System.Drawing.Size(109, 15);
            this.txtReg3DOB.TabIndex = 38;
            this.txtReg3DOB.Visible = false;
            this.txtReg3DOB.Enter += new System.EventHandler(this.txtReg3DOB_Enter);
            this.txtReg3DOB.Leave += new System.EventHandler(this.txtReg3DOB_Leave);
            // 
            // lblCreditNumberAP
            // 
            this.lblCreditNumberAP.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblCreditNumberAP.Location = new System.Drawing.Point(743, 396);
            this.lblCreditNumberAP.Name = "lblCreditNumberAP";
            this.lblCreditNumberAP.Size = new System.Drawing.Size(34, 15);
            this.lblCreditNumberAP.TabIndex = 159;
            this.lblCreditNumberAP.Text = "AP";
            this.lblCreditNumberAP.Visible = false;
            // 
            // lblStateFeesHR
            // 
            this.lblStateFeesHR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblStateFeesHR.Location = new System.Drawing.Point(950, 222);
            this.lblStateFeesHR.Name = "lblStateFeesHR";
            this.lblStateFeesHR.Size = new System.Drawing.Size(23, 15);
            this.lblStateFeesHR.TabIndex = 171;
            this.lblStateFeesHR.Text = "HR";
            this.lblStateFeesHR.Visible = false;
            // 
            // lblStateFeesPR
            // 
            this.lblStateFeesPR.Location = new System.Drawing.Point(950, 220);
            this.lblStateFeesPR.Name = "lblStateFeesPR";
            this.lblStateFeesPR.Size = new System.Drawing.Size(19, 13);
            this.lblStateFeesPR.TabIndex = 217;
            this.lblStateFeesPR.Text = "P/R";
            this.lblStateFeesPR.Visible = false;
            // 
            // lblRegistrant3
            // 
            this.lblRegistrant3.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblRegistrant3.Location = new System.Drawing.Point(323, 228);
            this.lblRegistrant3.Name = "lblRegistrant3";
            this.lblRegistrant3.Size = new System.Drawing.Size(170, 15);
            this.lblRegistrant3.TabIndex = 61;
            // 
            // lblDOBID
            // 
            this.lblDOBID.Anchor = Wisej.Web.AnchorStyles.Left;
            this.lblDOBID.Location = new System.Drawing.Point(514, -203);
            this.lblDOBID.Name = "lblDOBID";
            this.lblDOBID.Size = new System.Drawing.Size(41, 15);
            this.lblDOBID.TabIndex = 47;
            this.lblDOBID.Text = "DOB(S)";
            // 
            // Label54
            // 
            this.Label54.Location = new System.Drawing.Point(99, 141);
            this.Label54.Name = "Label54";
            this.Label54.Size = new System.Drawing.Size(141, 16);
            this.Label54.TabIndex = 40;
            this.Label54.Text = "CENTRAL PARTY";
            // 
            // lblTaxID
            // 
            this.lblTaxID.Anchor = Wisej.Web.AnchorStyles.Left;
            this.lblTaxID.Location = new System.Drawing.Point(430, 307);
            this.lblTaxID.Name = "lblTaxID";
            this.lblTaxID.Size = new System.Drawing.Size(63, 15);
            this.lblTaxID.TabIndex = 69;
            this.lblTaxID.Text = "TAX ID #";
            this.lblTaxID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateRatePR
            // 
            this.lblStateRatePR.Location = new System.Drawing.Point(950, 162);
            this.lblStateRatePR.Name = "lblStateRatePR";
            this.lblStateRatePR.Size = new System.Drawing.Size(19, 13);
            this.lblStateRatePR.TabIndex = 207;
            this.lblStateRatePR.Text = "P/R";
            this.lblStateRatePR.Visible = false;
            // 
            // lblExciseTaxPR
            // 
            this.lblExciseTaxPR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblExciseTaxPR.Location = new System.Drawing.Point(699, 251);
            this.lblExciseTaxPR.Name = "lblExciseTaxPR";
            this.lblExciseTaxPR.Size = new System.Drawing.Size(19, 15);
            this.lblExciseTaxPR.TabIndex = 135;
            this.lblExciseTaxPR.Text = "P/R";
            this.lblExciseTaxPR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblExciseTaxPR.Visible = false;
            // 
            // lblLocalSubPR
            // 
            this.lblLocalSubPR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblLocalSubPR.Location = new System.Drawing.Point(722, 309);
            this.lblLocalSubPR.Name = "lblLocalSubPR";
            this.lblLocalSubPR.Size = new System.Drawing.Size(19, 13);
            this.lblLocalSubPR.TabIndex = 147;
            this.lblLocalSubPR.Text = "P/R";
            this.lblLocalSubPR.Visible = false;
            // 
            // lblTownCreditPR
            // 
            this.lblTownCreditPR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTownCreditPR.Location = new System.Drawing.Point(709, 280);
            this.lblTownCreditPR.Name = "lblTownCreditPR";
            this.lblTownCreditPR.Size = new System.Drawing.Size(19, 15);
            this.lblTownCreditPR.TabIndex = 141;
            this.lblTownCreditPR.Text = "P/R";
            this.lblTownCreditPR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTownCreditPR.Visible = false;
            // 
            // lblLocalBalancePR
            // 
            this.lblLocalBalancePR.Location = new System.Drawing.Point(722, 367);
            this.lblLocalBalancePR.Name = "lblLocalBalancePR";
            this.lblLocalBalancePR.Size = new System.Drawing.Size(19, 15);
            this.lblLocalBalancePR.TabIndex = 155;
            this.lblLocalBalancePR.Text = "P/R";
            this.lblLocalBalancePR.Visible = false;
            // 
            // lblCreditRebate
            // 
            this.lblCreditRebate.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblCreditRebate.Location = new System.Drawing.Point(732, 280);
            this.lblCreditRebate.Name = "lblCreditRebate";
            this.lblCreditRebate.Size = new System.Drawing.Size(9, 15);
            this.lblCreditRebate.TabIndex = 142;
            this.lblCreditRebate.Text = "#";
            this.lblCreditRebate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCreditRebate.Visible = false;
            // 
            // Label48
            // 
            this.Label48.Location = new System.Drawing.Point(168, 437);
            this.Label48.Name = "Label48";
            this.Label48.Size = new System.Drawing.Size(72, 13);
            this.Label48.TabIndex = 79;
            this.Label48.Text = "COUNTRY";
            // 
            // Label47
            // 
            this.Label47.Location = new System.Drawing.Point(332, 437);
            this.Label47.Name = "Label47";
            this.Label47.Size = new System.Drawing.Size(70, 13);
            this.Label47.TabIndex = 85;
            this.Label47.Text = "COUNTRY";
            // 
            // Label38
            // 
            this.Label38.Location = new System.Drawing.Point(270, 437);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(50, 13);
            this.Label38.TabIndex = 83;
            this.Label38.Text = "STATE";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(229, 307);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(45, 15);
            this.Label24.TabIndex = 66;
            this.Label24.Text = "DOT #";
            this.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(19, 307);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(45, 15);
            this.Label22.TabIndex = 63;
            this.Label22.Text = "UNIT #";
            this.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label26
            // 
            this.Label26.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Label26.Location = new System.Drawing.Point(91, 303);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(97, 15);
            this.Label26.TabIndex = 65;
            // 
            // lblExciseDiff
            // 
            this.lblExciseDiff.Location = new System.Drawing.Point(764, 258);
            this.lblExciseDiff.Name = "lblExciseDiff";
            this.lblExciseDiff.Size = new System.Drawing.Size(13, 13);
            this.lblExciseDiff.TabIndex = 137;
            this.lblExciseDiff.Text = "*";
            this.lblExciseDiff.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblExciseDiff, "This is not the actual amount of excise tax that was collected. ");
            this.lblExciseDiff.Visible = false;
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.FillColor = 16777215;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(514, 499);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(50, 50);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.Image1.Visible = false;
            this.Image1.Click += new System.EventHandler(this.Image1_Click);
            // 
            // lblBattle
            // 
            this.lblBattle.Location = new System.Drawing.Point(636, 36);
            this.lblBattle.Name = "lblBattle";
            this.lblBattle.Size = new System.Drawing.Size(54, 13);
            this.lblBattle.TabIndex = 118;
            this.lblBattle.Text = "BATTLE";
            // 
            // lblLocalBalanceHR
            // 
            this.lblLocalBalanceHR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblLocalBalanceHR.Location = new System.Drawing.Point(703, 367);
            this.lblLocalBalanceHR.Name = "lblLocalBalanceHR";
            this.lblLocalBalanceHR.Size = new System.Drawing.Size(25, 15);
            this.lblLocalBalanceHR.TabIndex = 154;
            this.lblLocalBalanceHR.Text = "HR";
            this.lblLocalBalanceHR.Visible = false;
            // 
            // lblTownCreditHR
            // 
            this.lblTownCreditHR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTownCreditHR.Location = new System.Drawing.Point(687, 280);
            this.lblTownCreditHR.Name = "lblTownCreditHR";
            this.lblTownCreditHR.Size = new System.Drawing.Size(19, 15);
            this.lblTownCreditHR.TabIndex = 140;
            this.lblTownCreditHR.Text = "HR";
            this.lblTownCreditHR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTownCreditHR.Visible = false;
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(317, 539);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(85, 13);
            this.Label21.TabIndex = 107;
            this.Label21.Text = "PT STATE";
            this.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYSD
            // 
            this.lblYSD.Location = new System.Drawing.Point(168, 539);
            this.lblYSD.Name = "lblYSD";
            this.lblYSD.Size = new System.Drawing.Size(9, 13);
            this.lblYSD.TabIndex = 105;
            // 
            // lblMSD
            // 
            this.lblMSD.Location = new System.Drawing.Point(168, 511);
            this.lblMSD.Name = "lblMSD";
            this.lblMSD.Size = new System.Drawing.Size(9, 13);
            this.lblMSD.TabIndex = 97;
            // 
            // lblLocalSubHR
            // 
            this.lblLocalSubHR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblLocalSubHR.Location = new System.Drawing.Point(702, 309);
            this.lblLocalSubHR.Name = "lblLocalSubHR";
            this.lblLocalSubHR.Size = new System.Drawing.Size(19, 13);
            this.lblLocalSubHR.TabIndex = 146;
            this.lblLocalSubHR.Text = "HR";
            this.lblLocalSubHR.Visible = false;
            // 
            // lblStateCreditHR
            // 
            this.lblStateCreditHR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblStateCreditHR.Location = new System.Drawing.Point(950, 193);
            this.lblStateCreditHR.Name = "lblStateCreditHR";
            this.lblStateCreditHR.Size = new System.Drawing.Size(23, 15);
            this.lblStateCreditHR.TabIndex = 168;
            this.lblStateCreditHR.Text = "HR";
            this.lblStateCreditHR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateCreditHR.Visible = false;
            // 
            // lblExciseTaxHR
            // 
            this.lblExciseTaxHR.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblExciseTaxHR.Location = new System.Drawing.Point(724, 251);
            this.lblExciseTaxHR.Name = "lblExciseTaxHR";
            this.lblExciseTaxHR.Size = new System.Drawing.Size(19, 15);
            this.lblExciseTaxHR.TabIndex = 136;
            this.lblExciseTaxHR.Text = "HR";
            this.lblExciseTaxHR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblExciseTaxHR.Visible = false;
            // 
            // lblBalanceAP
            // 
            this.lblBalanceAP.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblBalanceAP.Location = new System.Drawing.Point(743, 367);
            this.lblBalanceAP.Name = "lblBalanceAP";
            this.lblBalanceAP.Size = new System.Drawing.Size(17, 15);
            this.lblBalanceAP.TabIndex = 156;
            this.lblBalanceAP.Text = "AP";
            this.lblBalanceAP.Visible = false;
            // 
            // lblTotalDue
            // 
            this.lblTotalDue.AutoSize = true;
            this.lblTotalDue.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblTotalDue.Location = new System.Drawing.Point(882, 142);
            this.lblTotalDue.Name = "lblTotalDue";
            this.lblTotalDue.Size = new System.Drawing.Size(137, 15);
            this.lblTotalDue.TabIndex = 1;
            this.lblTotalDue.Text = "TOTAL AMOUNT DUE";
            // 
            // lblCalculate
            // 
            this.lblCalculate.AutoSize = true;
            this.lblCalculate.Location = new System.Drawing.Point(19, 3);
            this.lblCalculate.Name = "lblCalculate";
            this.lblCalculate.Size = new System.Drawing.Size(156, 15);
            this.lblCalculate.TabIndex = 228;
            this.lblCalculate.Text = " CALCULATE REQUIRED ";
            this.lblCalculate.Visible = false;
            this.lblCalculate.Click += new System.EventHandler(this.lblCalculate_Click);
            // 
            // lblTransferAP
            // 
            this.lblTransferAP.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTransferAP.Location = new System.Drawing.Point(744, 338);
            this.lblTransferAP.Name = "lblTransferAP";
            this.lblTransferAP.Size = new System.Drawing.Size(17, 13);
            this.lblTransferAP.TabIndex = 151;
            this.lblTransferAP.Text = "AP";
            this.lblTransferAP.Visible = false;
            // 
            // lblSubTotalAP
            // 
            this.lblSubTotalAP.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblSubTotalAP.Location = new System.Drawing.Point(744, 309);
            this.lblSubTotalAP.Name = "lblSubTotalAP";
            this.lblSubTotalAP.Size = new System.Drawing.Size(17, 13);
            this.lblSubTotalAP.TabIndex = 148;
            this.lblSubTotalAP.Text = "AP";
            this.lblSubTotalAP.Visible = false;
            // 
            // lblCreditAP
            // 
            this.lblCreditAP.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblCreditAP.Location = new System.Drawing.Point(744, 280);
            this.lblCreditAP.Name = "lblCreditAP";
            this.lblCreditAP.Size = new System.Drawing.Size(17, 15);
            this.lblCreditAP.TabIndex = 143;
            this.lblCreditAP.Text = "AP";
            this.lblCreditAP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCreditAP.Visible = false;
            // 
            // lblExciseAP
            // 
            this.lblExciseAP.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblExciseAP.Location = new System.Drawing.Point(746, 251);
            this.lblExciseAP.Name = "lblExciseAP";
            this.lblExciseAP.Size = new System.Drawing.Size(17, 15);
            this.lblExciseAP.TabIndex = 141;
            this.lblExciseAP.Text = "AP";
            this.lblExciseAP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblExciseAP.Visible = false;
            // 
            // Label52
            // 
            this.Label52.Location = new System.Drawing.Point(19, 437);
            this.Label52.Name = "Label52";
            this.Label52.Size = new System.Drawing.Size(55, 13);
            this.Label52.TabIndex = 76;
            this.Label52.Text = "STATE";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(269, 36);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(65, 16);
            this.Label3.TabIndex = 6;
            this.Label3.Text = "MILEAGE";
            // 
            // lblNumberofYear
            // 
            this.lblNumberofYear.Location = new System.Drawing.Point(45, 539);
            this.lblNumberofYear.Name = "lblNumberofYear";
            this.lblNumberofYear.Size = new System.Drawing.Size(15, 13);
            this.lblNumberofYear.TabIndex = 102;
            // 
            // lblNumberofMonth
            // 
            this.lblNumberofMonth.Location = new System.Drawing.Point(45, 511);
            this.lblNumberofMonth.Name = "lblNumberofMonth";
            this.lblNumberofMonth.Size = new System.Drawing.Size(15, 13);
            this.lblNumberofMonth.TabIndex = 94;
            // 
            // Label51
            // 
            this.Label51.Location = new System.Drawing.Point(116, 485);
            this.Label51.Name = "Label51";
            this.Label51.Size = new System.Drawing.Size(25, 13);
            this.Label51.TabIndex = 91;
            this.Label51.Text = "N/C";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(66, 485);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(30, 13);
            this.Label6.TabIndex = 90;
            this.Label6.Text = "CHG";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(488, 89);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(110, 15);
            this.Label14.TabIndex = 25;
            this.Label14.Text = " COLOR";
            // 
            // Label50
            // 
            this.Label50.Location = new System.Drawing.Point(317, 511);
            this.Label50.Name = "Label50";
            this.Label50.Size = new System.Drawing.Size(23, 13);
            this.Label50.TabIndex = 99;
            this.Label50.Text = "PT";
            this.Label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label45
            // 
            this.Label45.Location = new System.Drawing.Point(22, 539);
            this.Label45.Name = "Label45";
            this.Label45.Size = new System.Drawing.Size(25, 13);
            this.Label45.TabIndex = 101;
            this.Label45.Text = "(Y)";
            // 
            // Label44
            // 
            this.Label44.Location = new System.Drawing.Point(22, 511);
            this.Label44.Name = "Label44";
            this.Label44.Size = new System.Drawing.Size(25, 13);
            this.Label44.TabIndex = 93;
            this.Label44.Text = "(M)";
            // 
            // Label29
            // 
            this.Label29.Location = new System.Drawing.Point(413, 437);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(191, 16);
            this.Label29.TabIndex = 87;
            this.Label29.Text = " LEGAL RESIDENCE CODE";
            // 
            // Label28
            // 
            this.Label28.Location = new System.Drawing.Point(267, 332);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(169, 15);
            this.Label28.TabIndex = 81;
            this.Label28.Text = " LEGAL RESIDENCE";
            // 
            // Label27
            // 
            this.Label27.Location = new System.Drawing.Point(19, 332);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(206, 20);
            this.Label27.TabIndex = 72;
            this.Label27.Text = " MAILING ADDRESS";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(734, 142);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(150, 17);
            this.Label23.TabIndex = 126;
            this.Label23.Text = "REGISTRATION FEES";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(976, 89);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(50, 15);
            this.Label20.TabIndex = 38;
            this.Label20.Text = "FUEL";
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(886, 89);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(80, 15);
            this.Label19.TabIndex = 36;
            this.Label19.Text = "REG WGT";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(802, 89);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(64, 15);
            this.Label18.TabIndex = 34;
            this.Label18.Text = "NET WGT";
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(737, 89);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(55, 15);
            this.Label17.TabIndex = 32;
            this.Label17.Text = "AXLES";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(667, 89);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(60, 15);
            this.Label16.TabIndex = 30;
            this.Label16.Text = "TIRES";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(608, 89);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(50, 15);
            this.Label15.TabIndex = 28;
            this.Label15.Text = "STYLE";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(389, 89);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(90, 15);
            this.Label13.TabIndex = 23;
            this.Label13.Text = " MODEL";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(310, 89);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(70, 15);
            this.Label12.TabIndex = 21;
            this.Label12.Text = "MAKE";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(231, 89);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(70, 15);
            this.Label11.TabIndex = 19;
            this.Label11.Text = "YEAR";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(19, 89);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(203, 15);
            this.Label10.TabIndex = 17;
            this.Label10.Text = "VIN";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(146, 36);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(61, 16);
            this.Label7.TabIndex = 4;
            this.Label7.Text = "EXPIRES";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(19, 36);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(78, 13);
            this.Label5.TabIndex = 2;
            this.Label5.Text = "EFFECTIVE";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(479, 36);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(150, 17);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "REGISTRATION NUMBER";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(397, 36);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(70, 17);
            this.Label2.TabIndex = 14;
            this.Label2.Text = "CLASS";
            // 
            // Label25
            // 
            this.Label25.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Label25.Location = new System.Drawing.Point(298, 304);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(110, 15);
            this.Label25.TabIndex = 68;
            this.Label25.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label53
            // 
            this.Label53.BorderStyle = 1;
            this.Label53.Font = new System.Drawing.Font("@small", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Label53.Location = new System.Drawing.Point(500, 304);
            this.Label53.Name = "Label53";
            this.Label53.Size = new System.Drawing.Size(90, 15);
            this.Label53.TabIndex = 71;
            this.Label53.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblUseTax
            // 
            this.lblUseTax.Anchor = Wisej.Web.AnchorStyles.Top;
            this.lblUseTax.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblUseTax.ForeColor = System.Drawing.Color.FromArgb(48, 150, 208);
            this.lblUseTax.Location = new System.Drawing.Point(893, 251);
            this.lblUseTax.Name = "lblUseTax";
            this.lblUseTax.Size = new System.Drawing.Size(30, 15);
            this.lblUseTax.TabIndex = 173;
            this.lblUseTax.Text = "S. T.";
            this.lblUseTax.DoubleClick += new System.EventHandler(this.lblUseTax_DoubleClick);
            // 
            // lblFees
            // 
            this.lblFees.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblFees.ForeColor = System.Drawing.Color.FromArgb(48, 150, 208);
            this.lblFees.Location = new System.Drawing.Point(893, 222);
            this.lblFees.Name = "lblFees";
            this.lblFees.Size = new System.Drawing.Size(35, 15);
            this.lblFees.TabIndex = 170;
            this.lblFees.Text = "FEES";
            this.lblFees.DoubleClick += new System.EventHandler(this.lblFees_DoubleClick);
            // 
            // lblStateCredit
            // 
            this.lblStateCredit.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblStateCredit.ForeColor = System.Drawing.Color.FromArgb(48, 150, 208);
            this.lblStateCredit.Location = new System.Drawing.Point(893, 193);
            this.lblStateCredit.Name = "lblStateCredit";
            this.lblStateCredit.Size = new System.Drawing.Size(50, 15);
            this.lblStateCredit.TabIndex = 167;
            this.lblStateCredit.Text = "CREDIT";
            this.lblStateCredit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStateCredit.DoubleClick += new System.EventHandler(this.lblStateCredit_DoubleClick);
            // 
            // Label46
            // 
            this.Label46.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label46.ForeColor = System.Drawing.Color.FromArgb(48, 150, 208);
            this.Label46.Location = new System.Drawing.Point(893, 164);
            this.Label46.Name = "Label46";
            this.Label46.Size = new System.Drawing.Size(46, 15);
            this.Label46.TabIndex = 164;
            this.Label46.Text = "RATE";
            this.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label46.DoubleClick += new System.EventHandler(this.Label46_DoubleClick);
            // 
            // lblCTA1
            // 
            this.lblCTA1.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblCTA1.Location = new System.Drawing.Point(893, 309);
            this.lblCTA1.Name = "lblCTA1";
            this.lblCTA1.Size = new System.Drawing.Size(40, 15);
            this.lblCTA1.TabIndex = 177;
            this.lblCTA1.Text = "CTA 1";
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(48, 150, 208);
            this.lblTitle.Location = new System.Drawing.Point(893, 280);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(40, 15);
            this.lblTitle.TabIndex = 175;
            this.lblTitle.Text = "TITLE";
            this.lblTitle.DoubleClick += new System.EventHandler(this.lblTitle_DoubleClick);
            // 
            // Label39
            // 
            this.Label39.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label39.Location = new System.Drawing.Point(625, 280);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(50, 15);
            this.Label39.TabIndex = 139;
            this.Label39.Text = "CREDIT";
            this.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExciseTax
            // 
            this.lblExciseTax.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblExciseTax.Location = new System.Drawing.Point(625, 251);
            this.lblExciseTax.Name = "lblExciseTax";
            this.lblExciseTax.Size = new System.Drawing.Size(60, 15);
            this.lblExciseTax.TabIndex = 134;
            this.lblExciseTax.Text = "EXC TAX";
            this.lblExciseTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label37
            // 
            this.Label37.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label37.Location = new System.Drawing.Point(625, 222);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(96, 15);
            this.Label37.TabIndex = 132;
            this.Label37.Text = "AGENT FEE";
            this.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label36
            // 
            this.Label36.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label36.Location = new System.Drawing.Point(625, 193);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(81, 15);
            this.Label36.TabIndex = 129;
            this.Label36.Text = "MIL. RATE";
            this.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label35
            // 
            this.Label35.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label35.ForeColor = System.Drawing.Color.FromArgb(48, 150, 208);
            this.Label35.Location = new System.Drawing.Point(625, 164);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(50, 15);
            this.Label35.TabIndex = 127;
            this.Label35.Text = "BASE";
            this.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label35.DoubleClick += new System.EventHandler(this.Label35_DoubleClick);
            // 
            // Label33
            // 
            this.Label33.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label33.Location = new System.Drawing.Point(625, 396);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(87, 15);
            this.Label33.TabIndex = 158;
            this.Label33.Text = "CREDIT NO";
            this.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label32
            // 
            this.Label32.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label32.Location = new System.Drawing.Point(625, 367);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(73, 15);
            this.Label32.TabIndex = 153;
            this.Label32.Text = "BALANCE";
            this.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label31
            // 
            this.Label31.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label31.Location = new System.Drawing.Point(625, 338);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(103, 13);
            this.Label31.TabIndex = 150;
            this.Label31.Text = "TRANS CHG";
            this.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label30
            // 
            this.Label30.Font = new System.Drawing.Font("defaultBold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label30.Location = new System.Drawing.Point(625, 309);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(71, 13);
            this.Label30.TabIndex = 145;
            this.Label30.Text = "SUBTOTAL";
            this.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStateCreditPR
            // 
            this.lblStateCreditPR.Location = new System.Drawing.Point(953, 191);
            this.lblStateCreditPR.Name = "lblStateCreditPR";
            this.lblStateCreditPR.Size = new System.Drawing.Size(20, 13);
            this.lblStateCreditPR.TabIndex = 208;
            this.lblStateCreditPR.Text = "P/R";
            this.lblStateCreditPR.Visible = false;
            // 
            // cmdMoreInfo
            // 
            this.cmdMoreInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdMoreInfo.Location = new System.Drawing.Point(158, 29);
            this.cmdMoreInfo.Name = "cmdMoreInfo";
            this.cmdMoreInfo.Size = new System.Drawing.Size(183, 24);
            this.cmdMoreInfo.TabIndex = 183;
            this.cmdMoreInfo.TabStop = false;
            this.cmdMoreInfo.Text = "Additional Information";
            this.ToolTip1.SetToolTip(this.cmdMoreInfo, "If additional information needs to be entered but not appear on the registration");
            this.cmdMoreInfo.Click += new System.EventHandler(this.cmdMoreInfo_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuOpenFields,
            this.mnuCTA,
            this.mnuUseTax,
            this.mnuProcessRedBook,
            this.mnuCalculate,
            this.mnuFleetGroupComment,
            this.mnuPreview,
            this.Separtor1,
            this.mnuCancel});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuOpenFields
            // 
            this.mnuOpenFields.Index = 0;
            this.mnuOpenFields.Name = "mnuOpenFields";
            this.mnuOpenFields.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuOpenFields.Text = "Open All Fields";
            this.mnuOpenFields.Click += new System.EventHandler(this.mnuOpenFields_Click);
            // 
            // mnuCTA
            // 
            this.mnuCTA.Index = 1;
            this.mnuCTA.Name = "mnuCTA";
            this.mnuCTA.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuCTA.Text = "CTA";
            this.mnuCTA.Click += new System.EventHandler(this.mnuCTA_Click);
            // 
            // mnuUseTax
            // 
            this.mnuUseTax.Index = 2;
            this.mnuUseTax.Name = "mnuUseTax";
            this.mnuUseTax.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuUseTax.Text = "Use Tax (Sales Tax)";
            this.mnuUseTax.Click += new System.EventHandler(this.mnuUseTax_Click);
            // 
            // mnuProcessRedBook
            // 
            this.mnuProcessRedBook.Index = 3;
            this.mnuProcessRedBook.Name = "mnuProcessRedBook";
            this.mnuProcessRedBook.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuProcessRedBook.Text = "Get Blue Book Values";
            this.mnuProcessRedBook.Click += new System.EventHandler(this.mnuProcessRedBook_Click);
            // 
            // mnuCalculate
            // 
            this.mnuCalculate.Index = 4;
            this.mnuCalculate.Name = "mnuCalculate";
            this.mnuCalculate.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuCalculate.Text = "Calculate";
            this.mnuCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
            // 
            // mnuFleetGroupComment
            // 
            this.mnuFleetGroupComment.Enabled = false;
            this.mnuFleetGroupComment.Index = 5;
            this.mnuFleetGroupComment.Name = "mnuFleetGroupComment";
            this.mnuFleetGroupComment.Text = "Fleet / Group Comment";
            this.mnuFleetGroupComment.Click += new System.EventHandler(this.mnuFleetGroupComment_Click);
            // 
            // mnuPreview
            // 
            this.mnuPreview.Index = 6;
            this.mnuPreview.Name = "mnuPreview";
            this.mnuPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPreview.Text = "Save and Preview";
            this.mnuPreview.Click += new System.EventHandler(this.mnuPreview_Click);
            // 
            // Separtor1
            // 
            this.Separtor1.Index = 7;
            this.Separtor1.Name = "Separtor1";
            this.Separtor1.Text = "-";
            // 
            // mnuCancel
            // 
            this.mnuCancel.Index = 8;
            this.mnuCancel.Name = "mnuCancel";
            this.mnuCancel.Text = "Exit";
            this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
            // 
            // cmdOpenAllFields
            // 
            this.cmdOpenAllFields.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdOpenAllFields.Location = new System.Drawing.Point(339, 29);
            this.cmdOpenAllFields.Name = "cmdOpenAllFields";
            this.cmdOpenAllFields.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdOpenAllFields.Size = new System.Drawing.Size(110, 24);
            this.cmdOpenAllFields.TabIndex = 1;
            this.cmdOpenAllFields.Text = "Open All Fields";
            this.cmdOpenAllFields.Click += new System.EventHandler(this.mnuOpenFields_Click);
            // 
            // cmdCTA
            // 
            this.cmdCTA.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCTA.Location = new System.Drawing.Point(455, 29);
            this.cmdCTA.Name = "cmdCTA";
            this.cmdCTA.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdCTA.Size = new System.Drawing.Size(44, 24);
            this.cmdCTA.TabIndex = 2;
            this.cmdCTA.Text = "CTA";
            this.cmdCTA.Click += new System.EventHandler(this.mnuCTA_Click);
            // 
            // cmdUseTax
            // 
            this.cmdUseTax.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdUseTax.Location = new System.Drawing.Point(505, 29);
            this.cmdUseTax.Name = "cmdUseTax";
            this.cmdUseTax.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdUseTax.Size = new System.Drawing.Size(136, 24);
            this.cmdUseTax.TabIndex = 3;
            this.cmdUseTax.Text = "Use Tax (Sales Tax)";
            this.cmdUseTax.Click += new System.EventHandler(this.mnuUseTax_Click);
            // 
            // cmdGetBB
            // 
            this.cmdGetBB.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdGetBB.Location = new System.Drawing.Point(647, 29);
            this.cmdGetBB.Name = "cmdGetBB";
            this.cmdGetBB.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdGetBB.Size = new System.Drawing.Size(152, 24);
            this.cmdGetBB.TabIndex = 4;
            this.cmdGetBB.Text = "Get Blue Book Values";
            this.cmdGetBB.Click += new System.EventHandler(this.mnuProcessRedBook_Click);
            // 
            // cmdCalculate
            // 
            this.cmdCalculate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCalculate.Location = new System.Drawing.Point(805, 29);
            this.cmdCalculate.Name = "cmdCalculate";
            this.cmdCalculate.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdCalculate.Size = new System.Drawing.Size(75, 24);
            this.cmdCalculate.TabIndex = 5;
            this.cmdCalculate.Text = "Calculate";
            this.cmdCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
            // 
            // cmdComment
            // 
            this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComment.Enabled = false;
            this.cmdComment.Location = new System.Drawing.Point(886, 29);
            this.cmdComment.Name = "cmdComment";
            this.cmdComment.Size = new System.Drawing.Size(154, 24);
            this.cmdComment.TabIndex = 6;
            this.cmdComment.Text = "Fleet/Group Comment";
            this.cmdComment.Click += new System.EventHandler(this.mnuFleetGroupComment_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(429, 15);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(184, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save and Preview";
            this.cmdSave.Click += new System.EventHandler(this.mnuPreview_Click);
            // 
            // frmDataInput
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1024, 636);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmDataInput";
            this.Text = " Data Input";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmDataInput_Load);
            this.Activated += new System.EventHandler(this.frmDataInput_Activated);
            this.FormClosed += new Wisej.Web.FormClosedEventHandler(this.frmDataInput_FormClosed);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDataInput_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDataInput_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmDataInput_KeyUp);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).EndInit();
            this.fraAdditionalInfo.ResumeLayout(false);
            this.fraAdditionalInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).EndInit();
            this.fraUserFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdErase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToLegalAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToMailingAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetGroupSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSpecialtyCorrExtraFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraHalfRate)).EndInit();
            this.fraHalfRate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkHalfRateLocal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHalfRateState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSalesTax)).EndInit();
            this.fraSalesTax.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDealerSalesTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTitleFee)).EndInit();
            this.fraTitleFee.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkNoFeeCTA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCTAPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBaseIsSalePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTANumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAmputeeVet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTrailerVanity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExciseExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAgentFeeExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStateExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2Year)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkETO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAgentFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStateCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAxles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOTNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresidenceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCTANumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxIDNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg2DOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg1DOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg3DOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpenAllFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCTA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUseTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetBB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdOpenAllFields;
		private FCButton cmdCTA;
		private FCButton cmdGetBB;
		private FCButton cmdUseTax;
		private FCButton cmdComment;
		private FCButton cmdCalculate;
		private FCButton cmdSave;
        private JavaScript javaScript1;
        public T2KDateBox txtExciseTaxDate;
        public FCTextBox txtUserID;
        public FCTextBox txtEvidenceofInsurance;
        public FCLabel fcLabel1;
        public FCLabel fcLabel2;
        public FCLabel lblFleet;
        public T2KBackFillDecimal txtExciseTax;
		public FCTextBox txtRegistrant1;
		public FCTextBox txtRegistrant3;
		public FCTextBox txtRegistrant2;
		public FCCheckBox chkSpecialtyCorrExtraFee;
		public FCLabel lblCTA2;
        public FCPictureBox imgReg1Search;
        public FCPictureBox imgReg1Edit;
        public FCPictureBox imgReg3Edit;
        public FCPictureBox imgReg3Search;
        public FCPictureBox imgReg2Edit;
        public FCPictureBox imgReg2Search;
        public FCPictureBox imgFleetGroupSearch;
        public FCPictureBox imgEditRegistrant3;
        public FCPictureBox imgEditRegistrant2;
        public FCPictureBox imgEditRegistrant1;
        public FCPictureBox imgCopyToMailingAddress;
        public FCPictureBox imgCopyToLegalAddress;
        public FCLabel fcLabel4;
        public FCLabel fcLabel3;
    }
}
