//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptDuplicateCTA.
	/// </summary>
	public partial class rptDuplicateCTA : BaseSectionReport
	{
		public rptDuplicateCTA()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Certificate of Title";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptDuplicateCTA InstancePtr
		{
			get
			{
				return (rptDuplicateCTA)Sys.GetInstance(typeof(rptDuplicateCTA));
			}
		}

		protected rptDuplicateCTA _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDuplicateCTA	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string CTAName = "";
		clsDRWrapper clsTitle = new clsDRWrapper();

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			MotorVehicle.Statics.boolDoubleCTA = false;
			clsTitle.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsTitle.OpenRecordset("select * from title where ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "New CTA ReportStart");
				if (MotorVehicle.Statics.boolDoubleCTA)
				{
					MotorVehicle.Statics.blnPrintDoubleCTA = false;
				}
				else
				{
					MotorVehicle.Statics.blnPrintCTA = false;
				}
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (FCConvert.ToInt32(clsTitle.Get_Fields_String("DoubleNumber")) != 0)
			{
				fldDoublCTATitle.Visible = true;
			}
			else
			{
				fldDoublCTATitle.Visible = false;
			}
			if (MotorVehicle.Statics.boolDoubleCTA)
			{
				DoubleCTA();
			}
			else
			{
				RegCTA();
			}
		}

		private void RegCTA()
		{
			string strLessee = "";
			string strSeller = "";
			int intPlaceHolder;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				txtName1.Text = clsTitle.Get_Fields_String("name1");
				if (Information.IsDate(clsTitle.Get_Fields("dob1")))
				{
					txtDOB1.Text = Strings.Format(clsTitle.Get_Fields_DateTime("dob1"), "MM/dd/yyyy");
				}
				if (Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("TelephoneNumber"))), 5) != "(000)")
				{
					txtTelephone.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("telephonenumber")));
				}
				else
				{
					txtTelephone.Text = Strings.Right(FCConvert.ToString(clsTitle.Get_Fields_String("telephonenumber")), 8);
				}
				string vbPorterVar = clsTitle.Get_Fields_String("newusedrebuilt");
				if (vbPorterVar == "N")
				{
					txtNewUsedRebuilt.Text = "N";
					txtNewUsedRebuilt.Top = 4470 / 1440F;
				}
				else if (vbPorterVar == "U")
				{
					txtNewUsedRebuilt.Text = "U";
					txtNewUsedRebuilt.Top = 4470 / 1440F + 180 / 1440F;
				}
				else if (vbPorterVar == "R")
				{
					txtNewUsedRebuilt.Text = "R";
					txtNewUsedRebuilt.Top = 4470 / 1440F + 360 / 1440F;
				}
				string vbPorterVar1 = clsTitle.Get_Fields_String("DealerType");
				if (vbPorterVar1 == "D")
				{
					fldD.Text = "D";
				}
				else if (vbPorterVar1 == "UC")
				{
					fldUC.Text = "UC";
				}
				else if (vbPorterVar1 == "MC")
				{
					fldMC.Text = "MC";
				}
				fldDealerPlate.Text = clsTitle.Get_Fields_String("DealerPlate");
				txtName2.Text = clsTitle.Get_Fields_String("name2");
				if (Information.IsDate(clsTitle.Get_Fields("dob2")))
				{
					txtDOB2.Text = Strings.Format(clsTitle.Get_Fields_DateTime("dob2"), "MM/dd/yyyy");
				}
				if (FCConvert.ToBoolean(clsTitle.Get_Fields_Boolean("RushTitle")))
				{
					txtRushTitle.Text = "X";
				}
				if (MotorVehicle.Statics.PlateType == 2)
				{
					if (frmSingleLongTermCTA.InstancePtr.cmbIllegible.Text == "Lost")
					{
						txtLost.Text = "X";
					}
					else if (frmSingleLongTermCTA.InstancePtr.cmbIllegible.Text == "Stolen")
					{
						txtStolen.Text = "X";
					}
					else if (frmSingleLongTermCTA.InstancePtr.cmbIllegible.Text == "Destroyed")
					{
						txtDestroyed.Text = "X";
					}
					else
					{
						txtIllegible.Text = "X";
					}
				}
				else
				{
					txtLost.Text = "";
					txtStolen.Text = "";
					txtDestroyed.Text = "";
					txtIllegible.Text = "";
				}
				txtAddress.Text = clsTitle.Get_Fields_String("address");
				txtCity.Text = clsTitle.Get_Fields_String("city");
				txtState.Text = clsTitle.Get_Fields("state");
				txtZip.Text = clsTitle.Get_Fields_String("zipandzip4");
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("year"));
				txtMake.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
				txtModel.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
				if (MotorVehicle.Statics.FleetCTA)
				{
					txtVIN.Text = "S.A.L.";
				}
				else
				{
					txtVIN.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("vin");
				}
				txtBodyType.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("style");
				txtPurchaseDate.Text = Strings.Format(clsTitle.Get_Fields_DateTime("purchasedate"), "MM/dd/yyyy");
				txtOdometer.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("odometer"));
				if (FCConvert.ToString(clsTitle.Get_Fields_String("odometermk")) == "K")
				{
					txtKilometer.Text = "K";
				}
				else
				{
					txtMile.Text = "M";
				}
				string vbPorterVar2 = clsTitle.Get_Fields_String("odometercondition");
				if (vbPorterVar2 == "A")
				{
					txtActual.Text = "AC";
				}
				else if (vbPorterVar2 == "E")
				{
					txtInExcess.Text = "IE";
				}
				else if (vbPorterVar2 == "C")
				{
					txtNotActual.Text = "NA";
					txtOdometerChanged.Text = "C";
				}
				else if (vbPorterVar2 == "B")
				{
					txtNotActual.Text = "NA";
					txtOdometerBroken.Text = "B";
				}
				txtFirstLienHolder.Text = clsTitle.Get_Fields_String("lh1name");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("lh1name"))).Length > 1)
				{
					txtDateofLien.Text = Strings.Format(clsTitle.Get_Fields_DateTime("lh1date"), "MM/dd/yyyy");
				}
				txtLien1Address.Text = clsTitle.Get_Fields_String("lh1address1");
				if (txtLien1Address.Text.Length > 22)
				{
					txtLien1Address.Text = Strings.Left(txtLien1Address.Text, 22);
				}
				txtLien1City.Text = clsTitle.Get_Fields_String("lh1city");
				txtLien1State.Text = clsTitle.Get_Fields_String("lh1state");
				txtLien1Zip.Text = clsTitle.Get_Fields_String("lh1zipandzip4");
				txtSecondLienHolder.Text = clsTitle.Get_Fields_String("lh2name");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("lh2name"))).Length > 1)
				{
					txtLien2Date.Text = Strings.Format(clsTitle.Get_Fields_DateTime("lh2date"), "MM/dd/yyyy");
				}
				txtLien2Address.Text = clsTitle.Get_Fields_String("lh2address1");
				txtLien2City.Text = clsTitle.Get_Fields_String("lh2city");
				txtLien2State.Text = clsTitle.Get_Fields_String("lh2state");
				txtLien2Zip.Text = clsTitle.Get_Fields_String("lh2zipandzip4");
				MotorVehicle.Statics.blnPrintCTA = true;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MotorVehicle.PrinterError("CTA / Use Tax Printer");
				MotorVehicle.Statics.blnPrintCTA = false;
			}
		}

		private void DoubleCTA()
		{
			string strSeller = "";
			int intPlaceHolder;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				txtName1.Text = clsTitle.Get_Fields_String("sellername");
				txtDOB1.Text = Strings.Format(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_DateTime("dob"), "MM/dd/yyyy");
				txtAddress.Text = clsTitle.Get_Fields_String("selleraddress1");
				txtCity.Text = clsTitle.Get_Fields_String("sellercity");
				txtState.Text = clsTitle.Get_Fields_String("sellerstate");
				txtZip.Text = clsTitle.Get_Fields_String("sellerzipandzip4");
				string vbPorterVar = clsTitle.Get_Fields_String("newusedrebuilt");
				if (vbPorterVar == "N")
				{
					txtNewUsedRebuilt.Text = "N";
					txtNewUsedRebuilt.Top = 4470 / 1440F;
				}
				else if (vbPorterVar == "U")
				{
					txtNewUsedRebuilt.Text = "U";
					txtNewUsedRebuilt.Top = 4470 / 1440F + 180 / 1440F;
				}
				else if (vbPorterVar == "R")
				{
					txtNewUsedRebuilt.Text = "R";
					txtNewUsedRebuilt.Top = 4470 / 1440F + 360 / 1440F;
				}
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("year"));
				txtMake.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
				txtModel.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
				txtVIN.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("vin");
				txtBodyType.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("style");
				txtPurchaseDate.Text = Strings.Format(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_DateTime("purchasedate"), "MM/dd/yyyy");
				if (FCConvert.ToString(clsTitle.Get_Fields_String("odometermk")) == "K")
				{
					txtKilometer.Text = "K";
				}
				else
				{
					txtMile.Text = "M";
				}
				string vbPorterVar1 = clsTitle.Get_Fields_String("odometercondition");
				if (vbPorterVar1 == "A")
				{
					txtActual.Text = "A";
				}
				else if (vbPorterVar1 == "E")
				{
					txtInExcess.Text = "IE";
				}
				else if (vbPorterVar1 == "C")
				{
					txtNotActual.Text = "NA";
					txtOdometerChanged.Text = "C";
				}
				else if (vbPorterVar1 == "B")
				{
					txtNotActual.Text = "NA";
					txtOdometerBroken.Text = "B";
				}
				txtOdometer.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("odometer"));
				MotorVehicle.Statics.blnPrintDoubleCTA = true;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MotorVehicle.PrinterError("CTA / Use Tax Printer");
				MotorVehicle.Statics.blnPrintDoubleCTA = false;
			}
		}

		private void rptDuplicateCTA_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptDuplicateCTA properties;
			//rptDuplicateCTA.Caption	= "Certificate of Title";
			//rptDuplicateCTA.Icon	= "rptDuplicateCTA.dsx":0000";
			//rptDuplicateCTA.Left	= 0;
			//rptDuplicateCTA.Top	= 0;
			//rptDuplicateCTA.Width	= 21480;
			//rptDuplicateCTA.Height	= 12990;
			//rptDuplicateCTA.StartUpPosition	= 3;
			//rptDuplicateCTA.SectionData	= "rptDuplicateCTA.dsx":058A;
			//End Unmaped Properties
		}
	}
}
