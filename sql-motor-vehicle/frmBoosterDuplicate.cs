//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmBoosterDuplicate : BaseForm
	{
		public frmBoosterDuplicate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBoosterDuplicate InstancePtr
		{
			get
			{
				return (frmBoosterDuplicate)Sys.GetInstance(typeof(frmBoosterDuplicate));
			}
		}

		protected frmBoosterDuplicate _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsBoosters = new clsDRWrapper();
		clsDRWrapper rsDefaultInfo = new clsDRWrapper();

		private void frmBoosterDuplicate_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBoosterDuplicate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBoosterDuplicate properties;
			//frmBoosterDuplicate.FillStyle	= 0;
			//frmBoosterDuplicate.ScaleWidth	= 5880;
			//frmBoosterDuplicate.ScaleHeight	= 3810;
			//frmBoosterDuplicate.LinkTopic	= "Form2";
			//frmBoosterDuplicate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo", "TWMV0000.vb1");
			rsBoosters.OpenRecordset("SELECT * FROM Booster WHERE Plate = '" + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate") + "' AND Class = '" + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class") + "' And Inactive = 0 and isnull(Voided, 0) = 0 and ExpireDate >= '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
			if (rsBoosters.EndOfFile() != true && rsBoosters.BeginningOfFile() != true)
			{
				do
				{
					cboOriginalBooster.AddItem("Permit# " + rsBoosters.Get_Fields_Int32("Permit") + "  Boosted To " + Strings.Format(rsBoosters.Get_Fields_Int32("BoostedWeight"), "#,##0") + "  Expires " + Strings.Format(rsBoosters.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy"));
					cboOriginalBooster.ItemData(cboOriginalBooster.NewIndex, FCConvert.ToInt32(rsBoosters.Get_Fields_Int32("Permit")));
					rsBoosters.MoveNext();
				}
				while (rsBoosters.EndOfFile() != true);
			}
			cboOriginalBooster.SelectedIndex = 0;
			lblFeeAmount.Text = Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DuplicateBoosterFee"), "#,##0.00");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmBoosterDuplicate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			if (txtPermit.Text == "")
			{
				MessageBox.Show("You must enter a Permit Number before you can Save.", "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				MotorVehicle.Write_PDS_Work_Record_Stuff();
			}
			rsBoosters.FindFirstRecord("Permit", cboOriginalBooster.ItemData(cboOriginalBooster.SelectedIndex));
			rs.OpenRecordset("SELECT * FROM Booster WHERE ID = 0");
			rs.AddNew();
			rs.Set_Fields("Permit", txtPermit.Text);
			rs.Set_Fields("EffectiveDate", rsBoosters.Get_Fields_DateTime("EffectiveDate"));
			rs.Set_Fields("ExpireDate", rsBoosters.Get_Fields_DateTime("ExpireDate"));
			rs.Set_Fields("BoostedWeight", rsBoosters.Get_Fields_Int32("BoostedWeight"));
			rs.Set_Fields("BoostedFrom", rsBoosters.Get_Fields_Int32("BoostedFrom"));
			rs.Set_Fields("StatePaid", FCConvert.ToDecimal(lblFeeAmount.Text));
			rs.Set_Fields("LocalPaid", 1);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("plate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
			rs.Set_Fields("Class", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Set_Fields("DateUpdated", DateTime.Now);
			rs.Set_Fields("VehicleID", rsBoosters.Get_Fields_Int32("VehicleID"));
			rs.Update();
			rsBoosters.Edit();
			rsBoosters.Set_Fields("Inactive", true);
			rsBoosters.Update();
			rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE ID = 0");
			rs.AddNew();
			rs.Set_Fields("Type", "6PM");
			rs.Set_Fields("ExceptionReportDate", DateTime.Today);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("NewClass", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
			rs.Set_Fields("NewPlate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
			rs.Set_Fields("Fee", FCConvert.ToDecimal(lblFeeAmount.Text));
			rs.Set_Fields("PermitNumber", fecherFoundation.Strings.Trim(txtPermit.Text));
			rs.Set_Fields("PermitType", "BOOSTER");
			rs.Set_Fields("ApplicantName", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1"))));
			rs.Set_Fields("MVR3Printed", 0);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			rs.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				rs.Edit();
				rs.Set_Fields("Status", "I");
				rs.Set_Fields("PeriodCloseoutID", 0);
				rs.Set_Fields("TellerCloseoutID", 0);
				rs.Update();
			}
			rs.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
			rs.AddNew();
			rs.Set_Fields("AdjustmentCode", "I");
			rs.Set_Fields("DateOfAdjustment", DateTime.Today);
			rs.Set_Fields("QuantityAdjusted", 1);
			rs.Set_Fields("InventoryType", "BXSXX");
			rs.Set_Fields("Low", txtPermit.Text);
			rs.Set_Fields("High", txtPermit.Text);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("reason", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate") + " " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MVR3"));
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			MessageBox.Show("Be sure to complete the Booster Form.", "Fill out Booster Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				//MDIParent.InstancePtr.GetMainMenu();
				MDIParent.InstancePtr.Menu18();
			}
		}

		private void txtPermit_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper tempRS = new clsDRWrapper();
			if (txtPermit.Text != "")
			{
				if (MotorVehicle.Statics.PlateGroup == 0)
				{
					tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)) + " AND Status = 'A'");
				}
				else
				{
					tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.PlateGroup) + " OR [Group] = 0)");
				}
				if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
				{
					return;
				}
				else
				{
					MessageBox.Show("This permit number was not found in Inventory.", "Invalid Permit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtPermit.Text = "";
				}
			}
		}
	}
}
