//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubNonMonetary.
	/// </summary>
	public partial class rptSubNonMonetary : BaseSectionReport
	{
		public rptSubNonMonetary()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Exception Report";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptSubNonMonetary_ReportEnd;
		}

        private void RptSubNonMonetary_ReportEnd(object sender, EventArgs e)
        {
            rsAM.DisposeOf();
			rsEX.DisposeOf();
        }

        public static rptSubNonMonetary InstancePtr
		{
			get
			{
				return (rptSubNonMonetary)Sys.GetInstance(typeof(rptSubNonMonetary));
			}
		}

		protected rptSubNonMonetary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubNonMonetary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rsEX = new clsDRWrapper();
		bool boolAM;
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsAM.EndOfFile())
				{
					boolAM = false;
					eArgs.EOF = rsEX.EndOfFile();
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			else
			{
				CheckAgain:
				;
				if (boolAM)
				{
					rsAM.MoveNext();
					if (rsAM.EndOfFile())
					{
						boolAM = false;
						blnFirstRecord = true;
						goto CheckAgain;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
				else
				{
					if (blnFirstRecord)
					{
						blnFirstRecord = false;
						eArgs.EOF = rsEX.EndOfFile();
					}
					else
					{
						rsEX.MoveNext();
						eArgs.EOF = rsEX.EndOfFile();
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND Status <> 'V' AND ETO <> 1 AND (((TransactionType = 'ECO' OR TransactionType = 'GVW') AND StatePaid <= 0) or (TransactionType = 'LPS' and ReplacementFee = 0 and StickerFee = 0)) ORDER BY DateUpdated");
				rsEX.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND Type = '4CN' ORDER BY ExceptionReportDate");
			}
			else
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Status <> 'V' AND ETO <> 1 AND (((TransactionType = 'ECO' OR TransactionType = 'GVW') AND StatePaid <= 0) or (TransactionType = 'LPS' and ReplacementFee = 0 and StickerFee = 0)) ORDER BY DateUpdated");
				rsEX.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Type = '4CN' ORDER BY ExceptionReportDate");
			}
			if ((rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true) || (rsEX.EndOfFile() != true && rsEX.BeginningOfFile() != true))
            {
                rptNewExceptionReport.InstancePtr.boolData = true;
			}
			else
			{
				this.Close();
			}
			blnFirstRecord = true;
			boolAM = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolAM == true)
			{
                rptNewExceptionReport.InstancePtr.NonMonCorrTotal += 1;
				fldDate.Text = Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy");
				fldReciept.Text = Strings.Format(rsAM.Get_Fields_Int32("MVR3"), "00000000");
				fldClassPlate.Text = rsAM.Get_Fields_String("Class") + " " + rsAM.Get_Fields_String("plate");
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS")
				{
					fldExplanation.Text = "Lost Plate/Stickers " + Strings.StrDup(20, " ");
				}
				else
				{
					if (fecherFoundation.FCUtils.IsNull(rsAM.Get_Fields_String("InfoMessage")) == false)
					{
						if (FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length <= 40)
						{
							fldExplanation.Text = rsAM.Get_Fields_String("InfoMessage") + Strings.StrDup(40 - FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length, " ");
						}
						else
						{
							fldExplanation.Text = Strings.Mid(FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")), 1, 40);
						}
					}
				}
				fldOpID.Text = Strings.Format(rsAM.Get_Fields_String("OpID"), "!@@@");
			}
			else
			{
                rptNewExceptionReport.InstancePtr.NonMonCorrTotal += 1;
				fldDate.Text = Strings.Format(rsEX.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yy");
				fldReciept.Text = Strings.Format(rsEX.Get_Fields_Int32("MVR3Number"), "00000000");
				fldClassPlate.Text = rsEX.Get_Fields_String("newclass") + " " + rsEX.Get_Fields_String("NewPlate");
				if (fecherFoundation.FCUtils.IsNull(rsEX.Get_Fields_String("reason")) == false)
				{
					if (FCConvert.ToString(rsEX.Get_Fields_String("reason")).Length <= 40)
					{
						fldExplanation.Text = rsEX.Get_Fields_String("reason") + Strings.StrDup(40 - FCConvert.ToString(rsEX.Get_Fields_String("reason")).Length, " ");
					}
					else
					{
						fldExplanation.Text = Strings.Mid(FCConvert.ToString(rsEX.Get_Fields_String("reason")), 1, 40);
					}
				}
				fldOpID.Text = Strings.Format(rsEX.Get_Fields_String("OpID"), "!@@@");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalCount.Text = FCConvert.ToString(rptNewExceptionReport.InstancePtr.NonMonCorrTotal);
		}
	}
}
