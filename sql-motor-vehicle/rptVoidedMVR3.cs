//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptVoidedMVR3.
	/// </summary>
	public partial class rptVoidedMVR3 : BaseSectionReport
	{
		public rptVoidedMVR3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Voided MVR-3 Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptVoidedMVR3_ReportEnd;
		}

        private void RptVoidedMVR3_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
        }

        public static rptVoidedMVR3 InstancePtr
		{
			get
			{
				return (rptVoidedMVR3)Sys.GetInstance(typeof(rptVoidedMVR3));
			}
		}

		protected rptVoidedMVR3 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVoidedMVR3	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngPK1;
		int lngPK2;
		int intHeadingNumber;
		string strSQL = "";
		clsDRWrapper rs = new clsDRWrapper();
		int intPageNumber;
		bool boolData;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!rs.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				//Application.DoEvents();
				// rs.Close
				rs.Reset();
				return;
			}
			Field1.Text = Strings.Format(rs.Get_Fields_DateTime("DateOfAdjustment"), "MM/dd/yyyy");
			if (FCConvert.ToString(rs.Get_Fields_String("InventoryType")) == "MXS00")
			{
				fldInventoryType.Text = "MVR3 ";
			}
			else if (rs.Get_Fields_String("InventoryType") == "BXSXX")
			{
				fldInventoryType.Text = "BOOST";
			}
			else if (rs.Get_Fields_String("InventoryType") == "PXSXX")
			{
				fldInventoryType.Text = "TRANS";
			}
			else if (rs.Get_Fields_String("InventoryType") == "RXSXX")
			{
				fldInventoryType.Text = "MVR10";
			}
			Field2.Text = Strings.Format(Strings.Format(rs.Get_Fields_Int32("QuantityAdjusted") * -1, "#########"), "@@@@@@@@@");
			Field3.Text = Strings.Format(rs.Get_Fields("Low"), "0000000") + "-" + Strings.Format(rs.Get_Fields("High"), "0000000");
			Field4.Text = rs.Get_Fields_String("OpID") + Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " ");
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("reason")) == false && FCConvert.ToString(rs.Get_Fields_String("reason")).Length <= 29)
			{
				Field5.Text = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason")))) + Strings.StrDup(29 - fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))).Length, " ");
			}
			else if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("reason")) == false)
			{
				Field5.Text = fecherFoundation.Strings.UCase(Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 29));
			}
			boolData = true;
			rs.MoveNext();
			// Loop
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
			}
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			boolData = false;
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE (InventoryType = 'MXS00' OR InventoryType = 'BXSXX' OR InventoryType = 'RXSXX' OR InventoryType = 'PXSXX') AND AdjustmentCode = 'V' AND PeriodCloseoutID < 1 ORDER BY InventoryType, Low");
			}
			else
			{
				strSQL = "SELECT * FROM InventoryAdjustments WHERE (InventoryType = 'MXS00' OR InventoryType = 'BXSXX' OR InventoryType = 'RXSXX' OR InventoryType = 'PXSXX') AND AdjustmentCode = 'V' AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY InventoryType, Low";
				rs.OpenRecordset(strSQL);
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (boolData == false)
			{
				txtNoInfo.Text = "No Information";
			}
		}

		private void rptVoidedMVR3_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptVoidedMVR3 properties;
			//rptVoidedMVR3.Caption	= "Voided MVR-3 Report";
			//rptVoidedMVR3.Icon	= "rptVoidedMVR3.dsx":0000";
			//rptVoidedMVR3.Left	= 0;
			//rptVoidedMVR3.Top	= 0;
			//rptVoidedMVR3.Width	= 11880;
			//rptVoidedMVR3.Height	= 8595;
			//rptVoidedMVR3.StartUpPosition	= 3;
			//rptVoidedMVR3.SectionData	= "rptVoidedMVR3.dsx":058A;
			//End Unmaped Properties
		}
	}
}
