﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptProcessFinancialSubtotals.
	/// </summary>
	partial class srptProcessFinancialSubtotals
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptProcessFinancialSubtotals));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblSummaryTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExcise = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAgent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReg = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblVanity = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUncoded = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExcise1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAgent1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReg1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVanity1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUncoded1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldExciseTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAgentFeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegFeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVanityTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUncodedTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAgent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVanity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUncoded)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcise1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAgent1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReg1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVanity1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUncoded1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgentFeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVanityTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUncodedTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDescription,
				this.txtExcise1,
				this.txtAgent1,
				this.txtReg1,
				this.txtVanity1,
				this.txtUncoded1
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblSummaryTitle,
				this.Binder,
				this.lblDescription,
				this.lblExcise,
				this.lblAgent,
				this.lblReg,
				this.lblVanity,
				this.lblUncoded
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.625F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line1,
				this.fldExciseTotal,
				this.fldAgentFeeTotal,
				this.fldRegFeeTotal,
				this.fldVanityTotal,
				this.fldUncodedTotal,
				this.Field1
			});
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblSummaryTitle
			// 
			this.lblSummaryTitle.Height = 0.1875F;
			this.lblSummaryTitle.HyperLink = null;
			this.lblSummaryTitle.Left = 1.5625F;
			this.lblSummaryTitle.Name = "lblSummaryTitle";
			this.lblSummaryTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblSummaryTitle.Text = "Label1";
			this.lblSummaryTitle.Top = 0.0625F;
			this.lblSummaryTitle.Width = 3.5F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 5.4375F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Binder";
			this.Binder.Top = 0.1875F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.625F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 0.1875F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblDescription.Text = "Date";
			this.lblDescription.Top = 0.375F;
			this.lblDescription.Width = 0.375F;
			// 
			// lblExcise
			// 
			this.lblExcise.Height = 0.1875F;
			this.lblExcise.HyperLink = null;
			this.lblExcise.Left = 1.21875F;
			this.lblExcise.Name = "lblExcise";
			this.lblExcise.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblExcise.Text = "Excise";
			this.lblExcise.Top = 0.375F;
			this.lblExcise.Width = 0.5F;
			// 
			// lblAgent
			// 
			this.lblAgent.Height = 0.1875F;
			this.lblAgent.HyperLink = null;
			this.lblAgent.Left = 1.96875F;
			this.lblAgent.Name = "lblAgent";
			this.lblAgent.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblAgent.Text = "Agent Fee";
			this.lblAgent.Top = 0.375F;
			this.lblAgent.Width = 0.75F;
			// 
			// lblReg
			// 
			this.lblReg.Height = 0.1875F;
			this.lblReg.HyperLink = null;
			this.lblReg.Left = 3.03125F;
			this.lblReg.Name = "lblReg";
			this.lblReg.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblReg.Text = "Reg Fee";
			this.lblReg.Top = 0.375F;
			this.lblReg.Width = 0.625F;
			// 
			// lblVanity
			// 
			this.lblVanity.Height = 0.1875F;
			this.lblVanity.HyperLink = null;
			this.lblVanity.Left = 4.03125F;
			this.lblVanity.Name = "lblVanity";
			this.lblVanity.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblVanity.Text = "Vanity";
			this.lblVanity.Top = 0.375F;
			this.lblVanity.Width = 0.625F;
			// 
			// lblUncoded
			// 
			this.lblUncoded.Height = 0.1875F;
			this.lblUncoded.HyperLink = null;
			this.lblUncoded.Left = 5.15625F;
			this.lblUncoded.Name = "lblUncoded";
			this.lblUncoded.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblUncoded.Text = "Uncoded";
			this.lblUncoded.Top = 0.375F;
			this.lblUncoded.Width = 0.5625F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 0.0625F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldDescription.Text = "Field1";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 0.8125F;
			// 
			// txtExcise1
			// 
			this.txtExcise1.Height = 0.1875F;
			this.txtExcise1.Left = 0.90625F;
			this.txtExcise1.Name = "txtExcise1";
			this.txtExcise1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.txtExcise1.Text = "Field1";
			this.txtExcise1.Top = 0F;
			this.txtExcise1.Width = 0.8125F;
			// 
			// txtAgent1
			// 
			this.txtAgent1.Height = 0.1875F;
			this.txtAgent1.Left = 1.84375F;
			this.txtAgent1.Name = "txtAgent1";
			this.txtAgent1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.txtAgent1.Text = "Field1";
			this.txtAgent1.Top = 0F;
			this.txtAgent1.Width = 0.875F;
			// 
			// txtReg1
			// 
			this.txtReg1.Height = 0.1875F;
			this.txtReg1.Left = 2.96875F;
			this.txtReg1.Name = "txtReg1";
			this.txtReg1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.txtReg1.Text = "Field1";
			this.txtReg1.Top = 0F;
			this.txtReg1.Width = 0.6875F;
			// 
			// txtVanity1
			// 
			this.txtVanity1.Height = 0.1875F;
			this.txtVanity1.Left = 3.96875F;
			this.txtVanity1.Name = "txtVanity1";
			this.txtVanity1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.txtVanity1.Text = "Field1";
			this.txtVanity1.Top = 0F;
			this.txtVanity1.Width = 0.6875F;
			// 
			// txtUncoded1
			// 
			this.txtUncoded1.Height = 0.1875F;
			this.txtUncoded1.Left = 4.96875F;
			this.txtUncoded1.Name = "txtUncoded1";
			this.txtUncoded1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.txtUncoded1.Text = "Field1";
			this.txtUncoded1.Top = 0F;
			this.txtUncoded1.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 5.75F;
			this.Line1.X1 = 5.75F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// fldExciseTotal
			// 
			this.fldExciseTotal.Height = 0.1875F;
			this.fldExciseTotal.Left = 0.90625F;
			this.fldExciseTotal.Name = "fldExciseTotal";
			this.fldExciseTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldExciseTotal.Text = "Field1";
			this.fldExciseTotal.Top = 0.03125F;
			this.fldExciseTotal.Width = 0.8125F;
			// 
			// fldAgentFeeTotal
			// 
			this.fldAgentFeeTotal.Height = 0.1875F;
			this.fldAgentFeeTotal.Left = 1.84375F;
			this.fldAgentFeeTotal.Name = "fldAgentFeeTotal";
			this.fldAgentFeeTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldAgentFeeTotal.Text = "Field1";
			this.fldAgentFeeTotal.Top = 0.03125F;
			this.fldAgentFeeTotal.Width = 0.875F;
			// 
			// fldRegFeeTotal
			// 
			this.fldRegFeeTotal.Height = 0.1875F;
			this.fldRegFeeTotal.Left = 2.96875F;
			this.fldRegFeeTotal.Name = "fldRegFeeTotal";
			this.fldRegFeeTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldRegFeeTotal.Text = "Field1";
			this.fldRegFeeTotal.Top = 0.03125F;
			this.fldRegFeeTotal.Width = 0.6875F;
			// 
			// fldVanityTotal
			// 
			this.fldVanityTotal.Height = 0.1875F;
			this.fldVanityTotal.Left = 3.96875F;
			this.fldVanityTotal.Name = "fldVanityTotal";
			this.fldVanityTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldVanityTotal.Text = "Field1";
			this.fldVanityTotal.Top = 0.03125F;
			this.fldVanityTotal.Width = 0.6875F;
			// 
			// fldUncodedTotal
			// 
			this.fldUncodedTotal.Height = 0.1875F;
			this.fldUncodedTotal.Left = 4.96875F;
			this.fldUncodedTotal.Name = "fldUncodedTotal";
			this.fldUncodedTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldUncodedTotal.Text = "Field1";
			this.fldUncodedTotal.Top = 0.03125F;
			this.fldUncodedTotal.Width = 0.75F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.0625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.Field1.Text = "Total";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 0.8125F;
			// 
			// srptProcessFinancialSubtotals
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAgent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVanity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUncoded)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExcise1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAgent1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReg1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVanity1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUncoded1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgentFeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVanityTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUncodedTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExcise1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAgent1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReg1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVanity1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUncoded1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExcise;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAgent;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReg;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVanity;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUncoded;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAgentFeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegFeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVanityTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUncodedTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
	}
}
