//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmFleetGroupLabels : BaseForm
	{
		public frmFleetGroupLabels()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFleetGroupLabels InstancePtr
		{
			get
			{
				return (frmFleetGroupLabels)Sys.GetInstance(typeof(frmFleetGroupLabels));
			}
		}

		protected frmFleetGroupLabels _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int SelectCol;
		int FleetNumberCol;
		int FleetNameCol;
		clsPrintLabel labLabelTypes = new clsPrintLabel();

		private void cboLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intIndex;
			intIndex = labLabelTypes.Get_IndexFromID(cboLabelType.ItemData(cboLabelType.SelectedIndex));
			lblLabelDescription.Text = labLabelTypes.Get_Description(intIndex);
		}

		private void cboEndName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void cboStartName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsFleets.Rows - 1); counter++)
			{
				vsFleets.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsFleets.Rows - 1); counter++)
			{
				vsFleets.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void frmFleetGroupLabels_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmFleetGroupLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFleetGroupLabels properties;
			//frmFleetGroupLabels.FillStyle	= 0;
			//frmFleetGroupLabels.ScaleWidth	= 9045;
			//frmFleetGroupLabels.ScaleHeight	= 7365;
			//frmFleetGroupLabels.LinkTopic	= "Form2";
			//frmFleetGroupLabels.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SelectCol = 0;
			FleetNumberCol = 1;
			FleetNameCol = 2;
			FillLabelTypeCombo();
			cboLabelType.SelectedIndex = 0;
			FormatGrid();
			if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
			{
				// do nothing
			}
			else
			{
				cmbFleetTypeAll.Visible = false;
				//FC:FINAL:MSH - i.issue #1865: hide label and combobox (parts of fraFleetType frame in original app)
				lblFleetTypeAll.Visible = false;
				cmbLongTerm.Visible = false;
				//fraLabelOptions.LeftOriginal = 1515;
				//fraType.LeftOriginal = 3675;
				//fraOrderBy.LeftOriginal = 5625;
			}
		}

		private void frmFleetGroupLabels_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFleetGroupLabels_Resize(object sender, System.EventArgs e)
		{
			vsFleets.ColWidth(SelectCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.15));
			vsFleets.ColWidth(FleetNumberCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.2));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnClassCodeSelected;
			string strSQL = "";
			clsDRWrapper rsInfo = new clsDRWrapper();
			string strPrinter = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			int intCPI = 0;
			int x;
			string strFont = "";
			string strFleetSQL;
			bool blnSelected = false;
			string strOldPrinter = "";
			strFleetSQL = "";
			if (cmbDateRangeAll.Text == "Selected")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
			}
			if (cmbSelected.Text == "Selected")
			{
				blnSelected = false;
				strFleetSQL = "(";
				for (counter = 1; counter <= vsFleets.Rows - 1; counter++)
				{
					if (FCConvert.ToBoolean(vsFleets.TextMatrix(counter, SelectCol)) == true)
					{
						blnSelected = true;
						strFleetSQL += vsFleets.TextMatrix(counter, FleetNumberCol) + ", ";
					}
				}
				if (!blnSelected)
				{
					MessageBox.Show("You must select at least 1 fleet / group before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else
				{
					strFleetSQL = Strings.Left(strFleetSQL, strFleetSQL.Length - 2) + ")";
				}
			}
			//FC:FINAL:MSH - i.issue #1866: wrong comparison condition
			//if (cmbFleetNameSelected.Text == "All")
			if (cmbStatusSelect.Text == "All")
			{
				if (cmbDateRangeAll.Text == "Selected")
				{
					if (strFleetSQL != "")
					{
						strSQL = "SELECT DISTINCT c.*, p.FullName FROM FleetMaster as c LEFT JOIN Master ON c.FleetNumber = Master.FleetNumber LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE c.Deleted <> 1 AND c.FleetNumber IN " + strFleetSQL + " AND IsNull(ExpireDate, '1/1/1950') <> '1950' AND ExpireDate >= '" + txtStartDate.Text + "' AND ExpireDate <= '" + txtEndDate.Text + "'";
					}
					else
					{
						strSQL = "SELECT DISTINCT c.*, p.FullName FROM FleetMaster as c LEFT JOIN Master ON c.FleetNumber = Master.FleetNumber LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE c.Deleted <> 1 AND IsNull(ExpireDate, '1/1/1950') <> '1950' AND ExpireDate >= '" + txtStartDate.Text + "' AND ExpireDate <= '" + txtEndDate.Text + "'";
					}
				}
				else
				{
					if (strFleetSQL != "")
					{
						strSQL = "SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND FleetNumber IN " + strFleetSQL;
					}
					else
					{
						strSQL = "SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1";
					}
				}
			}
			else
			{
				if (cmbDateRangeAll.Text == "Selected")
				{
					//FC:FINAL:MSH - i.issue #1866: wrong comparison condition
					//if (cmbFleet.Text == "Fleet")
					if (cmbFleet.Text == "Fleets")
					{
						if (strFleetSQL != "")
						{
							strSQL = "SELECT DISTINCT c.*, p.FullName FROM FleetMaster as c LEFT JOIN Master ON c.FleetNumber = Master.FleetNumber LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth <> 99 AND FleetNumber IN " + strFleetSQL + " AND IsNull(ExpireDate, '1/1/1950') <> '1950' AND ExpireDate >= '" + txtStartDate.Text + "' AND ExpireDate <= '" + txtEndDate.Text + "'";
						}
						else
						{
							strSQL = "SELECT DISTINCT c.*, p.FullName FROM FleetMaster as c LEFT JOIN Master ON c.FleetNumber = Master.FleetNumber LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth <> 99" + " AND IsNull(ExpireDate, '1/1/1950') <> '1950' AND ExpireDate >= '" + txtStartDate.Text + "' AND ExpireDate <= '" + txtEndDate.Text + "'";
						}
					}
					else
					{
						if (strFleetSQL != "")
						{
							strSQL = "SELECT DISTINCT c.*, p.FullName FROM FleetMaster as c LEFT JOIN Master ON c.FleetNumber = Master.FleetNumber LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth = 99 AND FleetNumber IN " + strFleetSQL + " AND IsNull(ExpireDate, '1/1/1950') <> '1950' AND ExpireDate >= '" + txtStartDate.Text + "' AND ExpireDate <= '" + txtEndDate.Text + "'";
						}
						else
						{
							strSQL = "SELECT DISTINCT c.*, p.FullName FROM FleetMaster as c LEFT JOIN Master ON c.FleetNumber = Master.FleetNumber LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth = 99" + " AND IsNull(ExpireDate, '1/1/1950') <> '1950' AND ExpireDate >= '" + txtStartDate.Text + "' AND ExpireDate <= '" + txtEndDate.Text + "'";
						}
					}
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1866: wrong comparison condition
					//if (cmbFleet.Text == "Fleet")
					if (cmbFleet.Text == "Fleets")
					{
						if (strFleetSQL != "")
						{
							strSQL = "SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth <> 99 AND FleetNumber IN " + strFleetSQL;
						}
						else
						{
							strSQL = "SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth <> 99";
						}
					}
					else
					{
						if (strFleetSQL != "")
						{
							strSQL = "SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth = 99 AND FleetNumber IN " + strFleetSQL;
						}
						else
						{
							strSQL = "SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND ExpiryMonth = 99";
						}
					}
				}
			}
			if (cmbName.Text == "Name")
			{
				strSQL += " ORDER BY FullName";
			}
			else
			{
				strSQL += " ORDER BY FleetNumber";
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				
				NumFonts = FCGlobal.Printer.Fonts.Count;
				boolUseFont = false;
				intCPI = 10;
				for (x = 0; x <= NumFonts - 1; x++)
				{
					strFont = FCGlobal.Printer.Fonts[x];
					if (fecherFoundation.Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
					{
						strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
						if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
						{
							boolUseFont = true;
							strFont = FCGlobal.Printer.Fonts[x];
							break;
						}
					}
				}
				// x
				// SHOW THE REPORT
				if (!boolUseFont)
					strFont = "";
				this.Hide();
				rptCustomLabels.InstancePtr.Init(ref strSQL, "FLEETS", cboLabelType.ItemData(cboLabelType.SelectedIndex), ref strPrinter, ref strFont);
				Close();
			}
			else
			{
				MessageBox.Show("No fleets / groups were found that matched the criteria you selected.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbFleet.Enabled = false;
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void optDateRangeAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRangeSelected.Enabled = false;
			txtStartDate.Enabled = false;
			txtEndDate.Enabled = false;
			lblTo.Enabled = false;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
		}

		private void optDateRangeSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRangeSelected.Enabled = true;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			lblTo.Enabled = true;
		}

		private void optFleetNameAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraFleetNameSelected.Enabled = false;
			cboStartName.Enabled = false;
			cboEndName.Enabled = false;
			Label2.Enabled = false;
			if (cmbFleetNameSelected.Text == "All" && cmbFleetTypeAll.Text == "All")
			{
				vsFleets.Visible = false;
				cmdSelectAll.Visible = false;
				cmdClearAll.Visible = false;
			}
			FillGrid();
		}

		private void optFleetNameSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraFleetNameSelected.Enabled = true;
			cboStartName.Enabled = true;
			cboStartName.SelectedIndex = 0;
			cboEndName.SelectedIndex = cboEndName.Items.Count - 1;
			cboEndName.Enabled = true;
			Label2.Enabled = true;
			FillGrid();
			vsFleets.Visible = true;
			cmdSelectAll.Visible = true;
			cmdClearAll.Visible = true;
		}

		private void optFleetTypeAll_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbSelected.Enabled = false;
			cmbFleet.Enabled = false;
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void optComplete_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			cmbStatusSelect.Text = "All";
			cmbFleetTypeAll.Text = "All";
			vsFleets.Visible = false;
			cmdSelectAll.Visible = false;
			cmdClearAll.Visible = false;
			for (counter = 1; counter <= (vsFleets.Rows - 1); counter++)
			{
				vsFleets.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void optGroups_CheckedChanged(object sender, System.EventArgs e)
		{
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void optAnnual_CheckedChanged(object sender, System.EventArgs e)
		{
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void optLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			FillGrid();
			vsFleets.Visible = true;
			cmdSelectAll.Visible = true;
			cmdClearAll.Visible = true;
			vsFleets.Focus();
		}

		private void FormatGrid()
		{
			vsFleets.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsFleets.TextMatrix(0, FleetNumberCol, "Number");
			vsFleets.TextMatrix(0, FleetNameCol, "Owner");
			vsFleets.ColAlignment(FleetNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFleets.ColWidth(SelectCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.15));
			vsFleets.ColWidth(FleetNumberCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.2));
		}

		private void FillGrid()
		{
			clsDRWrapper rs = new clsDRWrapper();
			string strWhere = "";
			//FC:FINAL:MSH - i.issue #1866: wrong comparison condition
			//if (cmbFleetNameSelected.Text == "All")
			if (cmbStatusSelect.Text == "All")
			{
				strWhere = "Deleted <> 1";
			}
			//FC:FINAL:MSH - i.issue #1866: wrong comparison condition
			//else if (cmbFleet.Text == "Fleet")
			else if (cmbFleet.Text == "Fleets")
			{
				strWhere = "Deleted <> 1 AND ExpiryMonth <> 99";
			}
			else
			{
				strWhere = "Deleted <> 1 AND ExpiryMonth = 99";
			}
			if (cmbFleetTypeAll.Text == "All")
			{
				// do nothing
			}
			else if (cmbLongTerm.Text == "Long Term")
			{
				if (strWhere == "")
				{
					strWhere = "Type = 'L'";
				}
				else
				{
					strWhere += " AND Type = 'L'";
				}
			}
			else
			{
				if (strWhere == "")
				{
					strWhere = "Type = 'A'";
				}
				else
				{
					strWhere += " AND Type = 'A'";
				}
			}
			if (cmbFleetNameSelected.Text == "All")
			{
				// do nothing
			}
			else
			{
				if (strWhere == "")
				{
					strWhere = "p.FullName >= '" + cboStartName.Text + "   ' AND p.FullName <= '" + cboEndName.Text + "zzzz'";
				}
				else
				{
					strWhere += " AND p.FullName >= '" + cboStartName.Text + "   ' AND p.FullName <= '" + cboEndName.Text + "zzzz'";
				}
			}
			if (strWhere != "")
			{
				strWhere = "WHERE " + strWhere;
			}
			rs.OpenRecordset("SELECT c.*, p.FullName as Party1FullName FROM FleetMaster as c LEFT JOIN " + rs.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID " + strWhere + " ORDER BY p.FullName");
			vsFleets.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsFleets.Rows += 1;
					vsFleets.TextMatrix(vsFleets.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsFleets.TextMatrix(vsFleets.Rows - 1, FleetNumberCol, FCConvert.ToString(rs.Get_Fields_Int32("FleetNumber")));
					vsFleets.TextMatrix(vsFleets.Rows - 1, FleetNameCol, fecherFoundation.Strings.UCase(FCConvert.ToString(rs.Get_Fields("Party1FullName"))));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void FillLabelTypeCombo()
		{
			int counter;
			// hide labels that are not supported by BD because of the 5 lines I need to print for an address
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE5066)
				{
					labLabelTypes.Set_Visible(counter, false);
					break;
				}
			}
			// fill combo box with all available types of labels
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_Visible(counter))
				{
					cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
					cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE4065)
					{
						cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
						cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
					}
				}
			}
		}

		private void optStatusSelect_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbFleet.Enabled = true;
			cmbSelected.Text = "Selected";
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void optFleetTypeSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbFleetTypeAll.Enabled = true;
			cmbLongTerm.Enabled = true;
			cmbSelected.Text = "Selected";
			if (vsFleets.Visible == true)
			{
				FillGrid();
			}
		}

		private void vsFleets_ClickEvent(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(vsFleets.TextMatrix(vsFleets.Row, SelectCol)) == true)
			{
				vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(false));
			}
			else
			{
				vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(true));
			}
		}

		private void vsFleets_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (FCConvert.ToBoolean(vsFleets.TextMatrix(vsFleets.Row, SelectCol)) == true)
				{
					vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void cmbSelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbSelected.Text == "Selected")
			{
				optSelected_CheckedChanged(sender, e);
			}
			else if (cmbSelected.Text == "Complete List")
			{
				optComplete_CheckedChanged(sender, e);
			}
		}

		private void cmbFleet_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1866: wrong comparison condition
			//if (cmbFleet.Text == "Fleet")
			if (cmbFleet.Text == "Fleets")
			{
				optFleet_CheckedChanged(sender, e);
			}
			//FC:FINAL:MSH - i.issue #1866: wrong comparison condition
			//else if (cmbFleet.Text == "Group")
			else if (cmbFleet.Text == "Groups")
			{
				optGroups_CheckedChanged(sender, e);
			}
		}

		private void cmbStatusSelect_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbStatusSelect.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbStatusSelect.Text == "Selected")
			{
				optStatusSelect_CheckedChanged(sender, e);
			}
		}

		private void cmbLongTerm_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbLongTerm.Text == "Annual")
			{
				optAnnual_CheckedChanged(sender, e);
			}
			else if (cmbLongTerm.Text == "Long Term")
			{
				optLongTerm_CheckedChanged(sender, e);
			}
		}

		private void cmbFleetTypeAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFleetTypeAll.Text == "All")
			{
				optFleetTypeAll_CheckedChanged(sender, e);
			}
			else if (cmbFleetTypeAll.Text == "Selected")
			{
				optFleetTypeSelected_CheckedChanged(sender, e);
			}
		}

		private void cmbDateRangeAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbDateRangeAll.Text == "All")
			{
				optDateRangeAll_CheckedChanged(sender, e);
			}
			else if (cmbDateRangeAll.Text == "Selected")
			{
				optDateRangeSelected_CheckedChanged(sender, e);
			}
		}

		private void cmbFleetNameSelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFleetNameSelected.Text == "All")
			{
				optFleetNameAll_CheckedChanged(sender, e);
			}
			else if (cmbFleetNameSelected.Text == "Selected")
			{
				optFleetNameSelected_CheckedChanged(sender, e);
			}
		}
	}
}
