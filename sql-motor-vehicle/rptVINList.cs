//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptVINList.
	/// </summary>
	public partial class rptVINList : BaseSectionReport
	{
		public rptVINList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "VIN List";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptVINList_ReportEnd;
		}

        private void RptVINList_ReportEnd(object sender, EventArgs e)
        {
            rsFleetInfo.DisposeOf();
        }

        public static rptVINList InstancePtr
		{
			get
			{
				return (rptVINList)Sys.GetInstance(typeof(rptVINList));
			}
		}

		protected rptVINList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVINList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int counter;
		int PageCounter;
		clsDRWrapper rsFleetInfo = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (counter >= frmPrintVIN.InstancePtr.vsVehicles.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			if (eArgs.EOF)
				return;
			//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
			//if (FCConvert.ToBoolean(frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 0)) == true)
			if (FCConvert.CBool(frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 0)) == true)
			{
				Field1.Text = frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 2);
				Field2.Text = frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 3);
				Field3.Text = frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 4);
			}
			else
			{
				counter += 1;
				while (counter < frmPrintVIN.InstancePtr.vsVehicles.Rows)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 0)) == true)
					if (FCConvert.CBool(frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 0)) == true)
					{
						Field1.Text = frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 2);
						Field2.Text = frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 3);
						Field3.Text = frmPrintVIN.InstancePtr.vsVehicles.TextMatrix(counter, 4);
						break;
					}
					else
					{
						counter += 1;
					}
				}
				if (counter >= frmPrintVIN.InstancePtr.vsVehicles.Rows)
				{
					eArgs.EOF = true;
					return;
				}
			}
			counter += 1;
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			MotorVehicle.Statics.rsReport.MoveFirst();
			counter = 1;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (Conversion.Val(MotorVehicle.Statics.rsReport.Get_Fields_Int32("FleetNumber")) != 0)
			{
				rsFleetInfo.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleetInfo.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE FleetNumber = " + MotorVehicle.Statics.rsReport.Get_Fields_Int32("FleetNumber"));
				Label5.Text = "FLEET  " + rsFleetInfo.Get_Fields_Int32("FleetNumber");
				Label6.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleetInfo.Get_Fields_String("FullName")));
			}
			else
			{
				Label5.Text = "Owner";
				Label6.Text = MotorVehicle.GetPartyName(MotorVehicle.Statics.rsReport.Get_Fields_Int32("PartyID1"));
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptVINList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptVINList properties;
			//rptVINList.Caption	= "VIN List";
			//rptVINList.Icon	= "rptVINList.dsx":0000";
			//rptVINList.Left	= 0;
			//rptVINList.Top	= 0;
			//rptVINList.Width	= 11880;
			//rptVINList.Height	= 8595;
			//rptVINList.StartUpPosition	= 3;
			//rptVINList.SectionData	= "rptVINList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
