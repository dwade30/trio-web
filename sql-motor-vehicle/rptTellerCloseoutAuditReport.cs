//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTellerCloseoutAuditReport.
	/// </summary>
	public partial class rptTellerCouseOutAuditReport : BaseSectionReport
	{
		public rptTellerCouseOutAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptTellerCouseOutAuditReport_ReportEnd;
		}

        private void RptTellerCouseOutAuditReport_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
        }

        public static rptTellerCouseOutAuditReport InstancePtr
		{
			get
			{
				return (rptTellerCouseOutAuditReport)Sys.GetInstance(typeof(rptTellerCouseOutAuditReport));
			}
		}

		protected rptTellerCouseOutAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTellerCloseoutAuditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnNoInfo;
		bool blnFirstRecord;
		clsDRWrapper rs = new clsDRWrapper();
		int intCode;
		string strTellers;
		string strDescription = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckAgain:
			;
			if (!blnNoInfo)
			{
				if (rs == null)
				{
					eArgs.EOF = true;
				}
				else
				{
					if (blnFirstRecord)
					{
						if (rs.EndOfFile() != true)
						{
							blnFirstRecord = false;
							eArgs.EOF = false;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
					else
					{
						rs.MoveNext();
						if (rs.EndOfFile() != true)
						{
							eArgs.EOF = false;
						}
						else
						{
							intCode += 1;
							SetRS();
							goto CheckAgain;
						}
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label35 As object	OnWrite(string)
			// vbPorter upgrade warning: Label34 As object	OnWrite(string)
			clsDRWrapper rsTellers = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			intCode = 0;
			blnNoInfo = true;
			blnFirstRecord = true;
			strTellers = "(";
			rsTellers.OpenRecordset("SELECT * FROM Operators", "SystemSettings");
			if (rsTellers.EndOfFile() != true && rsTellers.BeginningOfFile() != true)
			{
				do
				{
					strTellers += "'" + fecherFoundation.Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields("Code"))) + "', ";
					rsTellers.MoveNext();
				}
				while (rsTellers.EndOfFile() != true);
			}
			if (strTellers != "(")
			{
				strTellers = Strings.Left(strTellers, strTellers.Length - 2) + ")";
			}
			else
			{
				strTellers = "()";
			}
			SetRS();
			rsTellers.DisposeOf();
		}

		private void SetRS()
		{
			string strSQL = "";
			CheckAgain:
			;
			switch (intCode)
			{
				case 0:
					{
						// Set Teller CloseoutID for ActivityMaster
						strSQL = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Registration";
						break;
					}
				case 1:
					{
						// Set Teller CloseoutID for Special Registrations
						strSQL = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Special Registration";
						break;
					}
				case 2:
					{
						// Set Teller CloseoutID for Transit Plates
						strSQL = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Transit Plate";
						break;
					}
				case 3:
					{
						// Set Teller CloseoutID for Booster Permits
						strSQL = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Booster Permit";
						break;
					}
				case 4:
					{
						// Set Teller CloseoutID for Gift Certificates
						strSQL = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Gift Certificate";
						break;
					}
				case 5:
					{
						// Set Teller CloseoutID for InventoryAdjustments
						strSQL = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Inventory Adjustment";
						break;
					}
				case 6:
					{
						// Set Teller CloseoutID for ExciseTax
						strSQL = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Excise Tax";
						break;
					}
				case 7:
					{
						// Set Teller CloseoutID for ExceptionReport
						strSQL = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Exception Report Item";
						break;
					}
				case 8:
					{
						// Set Teller CloseoutID for TitleApplications
						strSQL = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Title Application";
						break;
					}
				case 9:
					{
						// Set Teller CloseoutID for Inventory
						strSQL = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND Upper(OpID) NOT IN " + strTellers;
						strDescription = "Inventory";
						break;
					}
				default:
					{
						strSQL = "";
						strDescription = "";
						break;
					}
			}
			//end switch
			if (strSQL != "")
			{
				rs.OpenRecordset(strSQL);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					blnNoInfo = false;
				}
				else
				{
					intCode += 1;
					goto CheckAgain;
				}
			}
			else
			{
				rs = null;
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldDescription As object	OnWrite(string)
			if (blnNoInfo)
			{
				fldNoInfo.Visible = true;
				fldTellerID.Visible = false;
				fldDescription.Visible = false;
			}
			else
			{
				fldTellerID.Text = rs.Get_Fields_String("OpID");
				fldDescription.Text = strDescription;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			Label10.Text = "Page " + this.PageNumber;
		}

		private void rptTellerCloseoutAuditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTellerCloseoutAuditReport properties;
			//rptTellerCloseoutAuditReport.Caption	= "ActiveReport1";
			//rptTellerCloseoutAuditReport.Left	= 0;
			//rptTellerCloseoutAuditReport.Top	= 0;
			//rptTellerCloseoutAuditReport.Width	= 11880;
			//rptTellerCloseoutAuditReport.Height	= 8595;
			//rptTellerCloseoutAuditReport.StartUpPosition	= 3;
			//rptTellerCloseoutAuditReport.SectionData	= "rptTellerCloseoutAuditReport.dsx":0000;
			//End Unmaped Properties
		}
	}
}
