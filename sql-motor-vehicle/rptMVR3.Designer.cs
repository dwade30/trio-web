﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptMVR3.
	/// </summary>
	partial class rptMVR3
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMVR3));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldReason1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidation1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidation2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidation3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckDigits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCumberlandCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReason2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReason3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMileage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMileage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEffective = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAxles = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetWeight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegisteredWeight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFuel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMil = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAgentFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLessor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDOTNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSubtotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitleFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCTANumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCreditNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseTaxDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistration = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistrationCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonthSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYearSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPriorTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistrationCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegistrationDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegFeeHR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegCreditHR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFeesHR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseHR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseCreditHR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSubtotalHR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseTotalHR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDoubleCTA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldReason1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidation1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidation2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidation3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckDigits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCumberlandCounty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReason2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReason3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMileage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEffective)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAxles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegisteredWeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFuel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMil)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgentFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLessor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOTNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSubtotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTaxDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthSticker)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearSticker)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPriorTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFeeHR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegCreditHR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFeesHR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseHR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseCreditHR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSubtotalHR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotalHR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOB3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoubleCTA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldReason1,
            this.fldValidation1,
            this.fldValidation2,
            this.fldValidation3,
            this.fldCheckDigits,
            this.fldCumberlandCounty,
            this.fldReason2,
            this.fldReason3,
            this.fldClass,
            this.lblMileage,
            this.fldMileage,
            this.fldPlate,
            this.fldEffective,
            this.fldExpires,
            this.fldVIN,
            this.fldYear,
            this.fldMake,
            this.fldModel,
            this.fldColor1,
            this.fldColor2,
            this.fldStyle,
            this.fldTires,
            this.fldAxles,
            this.fldNetWeight,
            this.fldRegisteredWeight,
            this.fldFuel,
            this.fldOwner1,
            this.fldDOB1,
            this.fldBase,
            this.fldOwner2,
            this.fldDOB2,
            this.fldMil,
            this.fldRegFee,
            this.fldAgentFee,
            this.fldRegCredit,
            this.fldLessor,
            this.fldUnit,
            this.fldDOTNumber,
            this.fldExcise,
            this.fldFees,
            this.fldExciseCredit,
            this.fldSalesTax,
            this.fldSubtotal,
            this.fldTitleFee,
            this.fldTransferFee,
            this.fldCTANumber,
            this.fldBalance,
            this.fldOpID,
            this.fldCreditNumber,
            this.fldExciseTaxDate,
            this.fldAddress,
            this.fldCity,
            this.fldState,
            this.fldZip,
            this.fldResidenceCity,
            this.fldResidenceState,
            this.fldResidenceCode,
            this.fldValidated,
            this.fldRegistration,
            this.fldRegistrationCity,
            this.fldMonthSticker,
            this.fldYearSticker,
            this.fldPriorTitle,
            this.fldRegistrationCode,
            this.fldRegistrationDate,
            this.fldRegFeeHR,
            this.fldRegCreditHR,
            this.fldFeesHR,
            this.fldExciseHR,
            this.fldExciseCreditHR,
            this.fldSubtotalHR,
            this.fldExciseTotalHR,
            this.fldOwner3,
            this.fldDOB3,
            this.fldAddress2,
            this.fldResidenceAddress,
            this.fldDoubleCTA});
			this.Detail.Height = 5.3125F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldReason1
			// 
			this.fldReason1.CanGrow = false;
			this.fldReason1.Height = 0.166F;
			this.fldReason1.Left = 4.282F;
			this.fldReason1.MultiLine = false;
			this.fldReason1.Name = "fldReason1";
			this.fldReason1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldReason1.Text = null;
			this.fldReason1.Top = 0.156F;
			this.fldReason1.Width = 3.5F;
			// 
			// fldValidation1
			// 
			this.fldValidation1.CanGrow = false;
			this.fldValidation1.Height = 0.19F;
			this.fldValidation1.Left = 0.344F;
			this.fldValidation1.MultiLine = false;
			this.fldValidation1.Name = "fldValidation1";
			this.fldValidation1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldValidation1.Text = null;
			this.fldValidation1.Top = 0.306F;
			this.fldValidation1.Width = 2.9375F;
			// 
			// fldValidation2
			// 
			this.fldValidation2.CanGrow = false;
			this.fldValidation2.Height = 0.19F;
			this.fldValidation2.Left = 0.344F;
			this.fldValidation2.MultiLine = false;
			this.fldValidation2.Name = "fldValidation2";
			this.fldValidation2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldValidation2.Text = null;
			this.fldValidation2.Top = 0.481F;
			this.fldValidation2.Width = 2.9375F;
			// 
			// fldValidation3
			// 
			this.fldValidation3.CanGrow = false;
			this.fldValidation3.Height = 0.19F;
			this.fldValidation3.Left = 0.34375F;
			this.fldValidation3.MultiLine = false;
			this.fldValidation3.Name = "fldValidation3";
			this.fldValidation3.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldValidation3.Text = null;
			this.fldValidation3.Top = 0.65625F;
			this.fldValidation3.Width = 2.9375F;
			// 
			// fldCheckDigits
			// 
			this.fldCheckDigits.CanGrow = false;
			this.fldCheckDigits.Height = 0.1875F;
			this.fldCheckDigits.Left = 1.1875F;
			this.fldCheckDigits.MultiLine = false;
			this.fldCheckDigits.Name = "fldCheckDigits";
			this.fldCheckDigits.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldCheckDigits.Text = null;
			this.fldCheckDigits.Top = 0.03125F;
			this.fldCheckDigits.Width = 0.3125F;
			// 
			// fldCumberlandCounty
			// 
			this.fldCumberlandCounty.CanShrink = true;
			this.fldCumberlandCounty.Height = 0.19F;
			this.fldCumberlandCounty.Left = 2.844F;
			this.fldCumberlandCounty.MultiLine = false;
			this.fldCumberlandCounty.Name = "fldCumberlandCounty";
			this.fldCumberlandCounty.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldCumberlandCounty.Text = "CUMBERLAND COUNTY";
			this.fldCumberlandCounty.Top = 0F;
			this.fldCumberlandCounty.Visible = false;
			this.fldCumberlandCounty.Width = 1.6875F;
			// 
			// fldReason2
			// 
			this.fldReason2.CanGrow = false;
			this.fldReason2.Height = 0.166F;
			this.fldReason2.Left = 4.281F;
			this.fldReason2.MultiLine = false;
			this.fldReason2.Name = "fldReason2";
			this.fldReason2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldReason2.Text = null;
			this.fldReason2.Top = 0.322F;
			this.fldReason2.Width = 3.5F;
			// 
			// fldReason3
			// 
			this.fldReason3.CanGrow = false;
			this.fldReason3.Height = 0.166F;
			this.fldReason3.Left = 4.281F;
			this.fldReason3.MultiLine = false;
			this.fldReason3.Name = "fldReason3";
			this.fldReason3.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldReason3.Text = null;
			this.fldReason3.Top = 0.488F;
			this.fldReason3.Width = 3.5F;
			// 
			// fldClass
			// 
			this.fldClass.CanGrow = false;
			this.fldClass.Height = 0.19F;
			this.fldClass.Left = 5.65625F;
			this.fldClass.MultiLine = false;
			this.fldClass.Name = "fldClass";
			this.fldClass.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldClass.Text = null;
			this.fldClass.Top = 0.6875F;
			this.fldClass.Width = 0.28125F;
			// 
			// lblMileage
			// 
			this.lblMileage.Height = 0.19F;
			this.lblMileage.HyperLink = null;
			this.lblMileage.Left = 3.28125F;
			this.lblMileage.MultiLine = false;
			this.lblMileage.Name = "lblMileage";
			this.lblMileage.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblMileage.Text = "MILEAGE";
			this.lblMileage.Top = 0.6875F;
			this.lblMileage.Width = 0.71875F;
			// 
			// fldMileage
			// 
			this.fldMileage.CanGrow = false;
			this.fldMileage.Height = 0.18F;
			this.fldMileage.Left = 4.09375F;
			this.fldMileage.MultiLine = false;
			this.fldMileage.Name = "fldMileage";
			this.fldMileage.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldMileage.Text = null;
			this.fldMileage.Top = 0.6875F;
			this.fldMileage.Width = 0.969F;
			// 
			// fldPlate
			// 
			this.fldPlate.CanGrow = false;
			this.fldPlate.Height = 0.19F;
			this.fldPlate.Left = 6.25F;
			this.fldPlate.MultiLine = false;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldPlate.Text = null;
			this.fldPlate.Top = 0.6875F;
			this.fldPlate.Width = 0.875F;
			// 
			// fldEffective
			// 
			this.fldEffective.CanGrow = false;
			this.fldEffective.Height = 0.19F;
			this.fldEffective.Left = 2.375F;
			this.fldEffective.MultiLine = false;
			this.fldEffective.Name = "fldEffective";
			this.fldEffective.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldEffective.Text = null;
			this.fldEffective.Top = 0.8437499F;
			this.fldEffective.Width = 0.90625F;
			// 
			// fldExpires
			// 
			this.fldExpires.CanGrow = false;
			this.fldExpires.Height = 0.19F;
			this.fldExpires.Left = 4.062F;
			this.fldExpires.MultiLine = false;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldExpires.Text = null;
			this.fldExpires.Top = 0.844F;
			this.fldExpires.Width = 0.875F;
			// 
			// fldVIN
			// 
			this.fldVIN.CanGrow = false;
			this.fldVIN.Height = 0.19F;
			this.fldVIN.Left = 0.375F;
			this.fldVIN.MultiLine = false;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldVIN.Text = null;
			this.fldVIN.Top = 1.46875F;
			this.fldVIN.Width = 1.65625F;
			// 
			// fldYear
			// 
			this.fldYear.CanGrow = false;
			this.fldYear.Height = 0.19F;
			this.fldYear.Left = 2.09375F;
			this.fldYear.MultiLine = false;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldYear.Text = null;
			this.fldYear.Top = 1.46875F;
			this.fldYear.Width = 0.34375F;
			// 
			// fldMake
			// 
			this.fldMake.CanGrow = false;
			this.fldMake.Height = 0.19F;
			this.fldMake.Left = 2.59375F;
			this.fldMake.MultiLine = false;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldMake.Text = null;
			this.fldMake.Top = 1.46875F;
			this.fldMake.Width = 0.46875F;
			// 
			// fldModel
			// 
			this.fldModel.CanGrow = false;
			this.fldModel.Height = 0.19F;
			this.fldModel.Left = 3.125F;
			this.fldModel.MultiLine = false;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldModel.Text = null;
			this.fldModel.Top = 1.46875F;
			this.fldModel.Width = 0.6875F;
			// 
			// fldColor1
			// 
			this.fldColor1.CanGrow = false;
			this.fldColor1.Height = 0.19F;
			this.fldColor1.Left = 3.90625F;
			this.fldColor1.MultiLine = false;
			this.fldColor1.Name = "fldColor1";
			this.fldColor1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldColor1.Text = null;
			this.fldColor1.Top = 1.46875F;
			this.fldColor1.Width = 0.3125F;
			// 
			// fldColor2
			// 
			this.fldColor2.CanGrow = false;
			this.fldColor2.Height = 0.19F;
			this.fldColor2.Left = 4.21875F;
			this.fldColor2.MultiLine = false;
			this.fldColor2.Name = "fldColor2";
			this.fldColor2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldColor2.Text = null;
			this.fldColor2.Top = 1.46875F;
			this.fldColor2.Width = 0.3125F;
			// 
			// fldStyle
			// 
			this.fldStyle.CanGrow = false;
			this.fldStyle.Height = 0.19F;
			this.fldStyle.Left = 4.65625F;
			this.fldStyle.MultiLine = false;
			this.fldStyle.Name = "fldStyle";
			this.fldStyle.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldStyle.Text = null;
			this.fldStyle.Top = 1.46875F;
			this.fldStyle.Width = 0.34375F;
			// 
			// fldTires
			// 
			this.fldTires.CanGrow = false;
			this.fldTires.Height = 0.19F;
			this.fldTires.Left = 5.03125F;
			this.fldTires.MultiLine = false;
			this.fldTires.Name = "fldTires";
			this.fldTires.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldTires.Text = null;
			this.fldTires.Top = 1.46875F;
			this.fldTires.Width = 0.25F;
			// 
			// fldAxles
			// 
			this.fldAxles.CanGrow = false;
			this.fldAxles.Height = 0.19F;
			this.fldAxles.Left = 5.4375F;
			this.fldAxles.MultiLine = false;
			this.fldAxles.Name = "fldAxles";
			this.fldAxles.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldAxles.Text = null;
			this.fldAxles.Top = 1.46875F;
			this.fldAxles.Width = 0.25F;
			// 
			// fldNetWeight
			// 
			this.fldNetWeight.CanGrow = false;
			this.fldNetWeight.Height = 0.19F;
			this.fldNetWeight.Left = 5.71875F;
			this.fldNetWeight.MultiLine = false;
			this.fldNetWeight.Name = "fldNetWeight";
			this.fldNetWeight.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldNetWeight.Text = null;
			this.fldNetWeight.Top = 1.46875F;
			this.fldNetWeight.Width = 0.53125F;
			// 
			// fldRegisteredWeight
			// 
			this.fldRegisteredWeight.CanGrow = false;
			this.fldRegisteredWeight.Height = 0.19F;
			this.fldRegisteredWeight.Left = 6.40625F;
			this.fldRegisteredWeight.MultiLine = false;
			this.fldRegisteredWeight.Name = "fldRegisteredWeight";
			this.fldRegisteredWeight.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldRegisteredWeight.Text = null;
			this.fldRegisteredWeight.Top = 1.46875F;
			this.fldRegisteredWeight.Width = 0.8125F;
			// 
			// fldFuel
			// 
			this.fldFuel.CanGrow = false;
			this.fldFuel.Height = 0.19F;
			this.fldFuel.Left = 7.40625F;
			this.fldFuel.MultiLine = false;
			this.fldFuel.Name = "fldFuel";
			this.fldFuel.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldFuel.Text = null;
			this.fldFuel.Top = 1.46875F;
			this.fldFuel.Width = 0.34375F;
			// 
			// fldOwner1
			// 
			this.fldOwner1.CanGrow = false;
			this.fldOwner1.Height = 0.19F;
			this.fldOwner1.Left = 0.34375F;
			this.fldOwner1.MultiLine = false;
			this.fldOwner1.Name = "fldOwner1";
			this.fldOwner1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldOwner1.Text = null;
			this.fldOwner1.Top = 1.84375F;
			this.fldOwner1.Width = 3.6875F;
			// 
			// fldDOB1
			// 
			this.fldDOB1.CanGrow = false;
			this.fldDOB1.Height = 0.19F;
			this.fldDOB1.Left = 4.125F;
			this.fldDOB1.MultiLine = false;
			this.fldDOB1.Name = "fldDOB1";
			this.fldDOB1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldDOB1.Text = null;
			this.fldDOB1.Top = 1.84375F;
			this.fldDOB1.Width = 0.96875F;
			// 
			// fldBase
			// 
			this.fldBase.CanGrow = false;
			this.fldBase.Height = 0.18F;
			this.fldBase.Left = 5.46875F;
			this.fldBase.MultiLine = false;
			this.fldBase.Name = "fldBase";
			this.fldBase.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldBase.Text = null;
			this.fldBase.Top = 1.875F;
			this.fldBase.Width = 0.969F;
			// 
			// fldOwner2
			// 
			this.fldOwner2.CanGrow = false;
			this.fldOwner2.Height = 0.19F;
			this.fldOwner2.Left = 0.344F;
			this.fldOwner2.MultiLine = false;
			this.fldOwner2.Name = "fldOwner2";
			this.fldOwner2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldOwner2.Text = null;
			this.fldOwner2.Top = 2.031F;
			this.fldOwner2.Width = 3.6875F;
			// 
			// fldDOB2
			// 
			this.fldDOB2.CanGrow = false;
			this.fldDOB2.Height = 0.19F;
			this.fldDOB2.Left = 4.12525F;
			this.fldDOB2.MultiLine = false;
			this.fldDOB2.Name = "fldDOB2";
			this.fldDOB2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldDOB2.Text = null;
			this.fldDOB2.Top = 2.031F;
			this.fldDOB2.Width = 0.96875F;
			// 
			// fldMil
			// 
			this.fldMil.CanGrow = false;
			this.fldMil.Height = 0.19F;
			this.fldMil.Left = 5.46925F;
			this.fldMil.MultiLine = false;
			this.fldMil.Name = "fldMil";
			this.fldMil.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldMil.Text = null;
			this.fldMil.Top = 2.187F;
			this.fldMil.Width = 0.96875F;
			// 
			// fldRegFee
			// 
			this.fldRegFee.CanGrow = false;
			this.fldRegFee.Height = 0.19F;
			this.fldRegFee.Left = 6.8755F;
			this.fldRegFee.MultiLine = false;
			this.fldRegFee.Name = "fldRegFee";
			this.fldRegFee.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldRegFee.Text = null;
			this.fldRegFee.Top = 2.187F;
			this.fldRegFee.Width = 0.78125F;
			// 
			// fldAgentFee
			// 
			this.fldAgentFee.CanGrow = false;
			this.fldAgentFee.Height = 0.19F;
			this.fldAgentFee.Left = 5.6255F;
			this.fldAgentFee.MultiLine = false;
			this.fldAgentFee.Name = "fldAgentFee";
			this.fldAgentFee.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldAgentFee.Text = null;
			this.fldAgentFee.Top = 2.437F;
			this.fldAgentFee.Width = 0.8125F;
			// 
			// fldRegCredit
			// 
			this.fldRegCredit.CanGrow = false;
			this.fldRegCredit.Height = 0.19F;
			this.fldRegCredit.Left = 6.8755F;
			this.fldRegCredit.MultiLine = false;
			this.fldRegCredit.Name = "fldRegCredit";
			this.fldRegCredit.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldRegCredit.Text = null;
			this.fldRegCredit.Top = 2.437F;
			this.fldRegCredit.Width = 0.78125F;
			// 
			// fldLessor
			// 
			this.fldLessor.CanGrow = false;
			this.fldLessor.Height = 0.19F;
			this.fldLessor.Left = 0.3442504F;
			this.fldLessor.MultiLine = false;
			this.fldLessor.Name = "fldLessor";
			this.fldLessor.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldLessor.Text = null;
			this.fldLessor.Top = 2.4995F;
			this.fldLessor.Width = 3.03125F;
			// 
			// fldUnit
			// 
			this.fldUnit.CanGrow = false;
			this.fldUnit.Height = 0.19F;
			this.fldUnit.Left = 3.3755F;
			this.fldUnit.MultiLine = false;
			this.fldUnit.Name = "fldUnit";
			this.fldUnit.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldUnit.Text = null;
			this.fldUnit.Top = 2.4995F;
			this.fldUnit.Width = 0.8125F;
			// 
			// fldDOTNumber
			// 
			this.fldDOTNumber.CanGrow = false;
			this.fldDOTNumber.Height = 0.19F;
			this.fldDOTNumber.Left = 4.28175F;
			this.fldDOTNumber.MultiLine = false;
			this.fldDOTNumber.Name = "fldDOTNumber";
			this.fldDOTNumber.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldDOTNumber.Text = null;
			this.fldDOTNumber.Top = 2.4995F;
			this.fldDOTNumber.Width = 0.8125F;
			// 
			// fldExcise
			// 
			this.fldExcise.CanGrow = false;
			this.fldExcise.Height = 0.19F;
			this.fldExcise.Left = 5.6255F;
			this.fldExcise.MultiLine = false;
			this.fldExcise.Name = "fldExcise";
			this.fldExcise.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldExcise.Text = null;
			this.fldExcise.Top = 2.687F;
			this.fldExcise.Width = 0.8125F;
			// 
			// fldFees
			// 
			this.fldFees.CanGrow = false;
			this.fldFees.Height = 0.19F;
			this.fldFees.Left = 6.8755F;
			this.fldFees.MultiLine = false;
			this.fldFees.Name = "fldFees";
			this.fldFees.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldFees.Text = null;
			this.fldFees.Top = 2.687F;
			this.fldFees.Width = 0.78125F;
			// 
			// fldExciseCredit
			// 
			this.fldExciseCredit.CanGrow = false;
			this.fldExciseCredit.Height = 0.19F;
			this.fldExciseCredit.Left = 5.6255F;
			this.fldExciseCredit.MultiLine = false;
			this.fldExciseCredit.Name = "fldExciseCredit";
			this.fldExciseCredit.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldExciseCredit.Text = null;
			this.fldExciseCredit.Top = 2.937F;
			this.fldExciseCredit.Width = 0.8125F;
			// 
			// fldSalesTax
			// 
			this.fldSalesTax.CanGrow = false;
			this.fldSalesTax.Height = 0.19F;
			this.fldSalesTax.Left = 6.8755F;
			this.fldSalesTax.MultiLine = false;
			this.fldSalesTax.Name = "fldSalesTax";
			this.fldSalesTax.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldSalesTax.Text = null;
			this.fldSalesTax.Top = 2.937F;
			this.fldSalesTax.Width = 0.78125F;
			// 
			// fldSubtotal
			// 
			this.fldSubtotal.CanGrow = false;
			this.fldSubtotal.Height = 0.19F;
			this.fldSubtotal.Left = 5.6255F;
			this.fldSubtotal.MultiLine = false;
			this.fldSubtotal.Name = "fldSubtotal";
			this.fldSubtotal.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldSubtotal.Text = null;
			this.fldSubtotal.Top = 3.187F;
			this.fldSubtotal.Width = 0.8125F;
			// 
			// fldTitleFee
			// 
			this.fldTitleFee.CanGrow = false;
			this.fldTitleFee.Height = 0.19F;
			this.fldTitleFee.Left = 6.8755F;
			this.fldTitleFee.MultiLine = false;
			this.fldTitleFee.Name = "fldTitleFee";
			this.fldTitleFee.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldTitleFee.Text = null;
			this.fldTitleFee.Top = 3.187F;
			this.fldTitleFee.Width = 0.78125F;
			// 
			// fldTransferFee
			// 
			this.fldTransferFee.CanGrow = false;
			this.fldTransferFee.Height = 0.19F;
			this.fldTransferFee.Left = 5.6255F;
			this.fldTransferFee.MultiLine = false;
			this.fldTransferFee.Name = "fldTransferFee";
			this.fldTransferFee.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldTransferFee.Text = null;
			this.fldTransferFee.Top = 3.437F;
			this.fldTransferFee.Width = 0.8125F;
			// 
			// fldCTANumber
			// 
			this.fldCTANumber.CanGrow = false;
			this.fldCTANumber.Height = 0.19F;
			this.fldCTANumber.Left = 6.8755F;
			this.fldCTANumber.MultiLine = false;
			this.fldCTANumber.Name = "fldCTANumber";
			this.fldCTANumber.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldCTANumber.Text = null;
			this.fldCTANumber.Top = 3.437F;
			this.fldCTANumber.Width = 0.78125F;
			// 
			// fldBalance
			// 
			this.fldBalance.CanGrow = false;
			this.fldBalance.Height = 0.19F;
			this.fldBalance.Left = 5.6255F;
			this.fldBalance.MultiLine = false;
			this.fldBalance.Name = "fldBalance";
			this.fldBalance.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldBalance.Text = null;
			this.fldBalance.Top = 3.687F;
			this.fldBalance.Width = 0.8125F;
			// 
			// fldOpID
			// 
			this.fldOpID.CanGrow = false;
			this.fldOpID.Height = 0.19F;
			this.fldOpID.Left = 6.8755F;
			this.fldOpID.MultiLine = false;
			this.fldOpID.Name = "fldOpID";
			this.fldOpID.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldOpID.Text = null;
			this.fldOpID.Top = 3.687F;
			this.fldOpID.Width = 0.78125F;
			// 
			// fldCreditNumber
			// 
			this.fldCreditNumber.CanGrow = false;
			this.fldCreditNumber.Height = 0.19F;
			this.fldCreditNumber.Left = 5.6255F;
			this.fldCreditNumber.MultiLine = false;
			this.fldCreditNumber.Name = "fldCreditNumber";
			this.fldCreditNumber.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldCreditNumber.Text = null;
			this.fldCreditNumber.Top = 3.937F;
			this.fldCreditNumber.Width = 0.8125F;
			// 
			// fldExciseTaxDate
			// 
			this.fldExciseTaxDate.CanGrow = false;
			this.fldExciseTaxDate.Height = 0.19F;
			this.fldExciseTaxDate.Left = 5.6255F;
			this.fldExciseTaxDate.MultiLine = false;
			this.fldExciseTaxDate.Name = "fldExciseTaxDate";
			this.fldExciseTaxDate.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldExciseTaxDate.Text = null;
			this.fldExciseTaxDate.Top = 4.187F;
			this.fldExciseTaxDate.Width = 0.9375F;
			// 
			// fldAddress
			// 
			this.fldAddress.CanGrow = false;
			this.fldAddress.Height = 0.19F;
			this.fldAddress.Left = 0.3442504F;
			this.fldAddress.MultiLine = false;
			this.fldAddress.Name = "fldAddress";
			this.fldAddress.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1" +
    "";
			this.fldAddress.Text = null;
			this.fldAddress.Top = 2.8745F;
			this.fldAddress.Width = 3.09375F;
			// 
			// fldCity
			// 
			this.fldCity.CanGrow = false;
			this.fldCity.Height = 0.19F;
			this.fldCity.Left = 0.3442504F;
			this.fldCity.MultiLine = false;
			this.fldCity.Name = "fldCity";
			this.fldCity.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldCity.Text = null;
			this.fldCity.Top = 3.187F;
			this.fldCity.Width = 1.84375F;
			// 
			// fldState
			// 
			this.fldState.CanGrow = false;
			this.fldState.Height = 0.19F;
			this.fldState.Left = 2.188F;
			this.fldState.MultiLine = false;
			this.fldState.Name = "fldState";
			this.fldState.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldState.Text = null;
			this.fldState.Top = 3.187F;
			this.fldState.Width = 0.28125F;
			// 
			// fldZip
			// 
			this.fldZip.CanGrow = false;
			this.fldZip.Height = 0.19F;
			this.fldZip.Left = 2.5005F;
			this.fldZip.MultiLine = false;
			this.fldZip.Name = "fldZip";
			this.fldZip.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldZip.Text = null;
			this.fldZip.Top = 3.187F;
			this.fldZip.Width = 0.84375F;
			// 
			// fldResidenceCity
			// 
			this.fldResidenceCity.CanGrow = false;
			this.fldResidenceCity.Height = 0.19F;
			this.fldResidenceCity.Left = 0.3442504F;
			this.fldResidenceCity.MultiLine = false;
			this.fldResidenceCity.Name = "fldResidenceCity";
			this.fldResidenceCity.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldResidenceCity.Text = null;
			this.fldResidenceCity.Top = 3.71825F;
			this.fldResidenceCity.Width = 1.78125F;
			// 
			// fldResidenceState
			// 
			this.fldResidenceState.CanGrow = false;
			this.fldResidenceState.Height = 0.19F;
			this.fldResidenceState.Left = 2.2505F;
			this.fldResidenceState.MultiLine = false;
			this.fldResidenceState.Name = "fldResidenceState";
			this.fldResidenceState.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldResidenceState.Text = null;
			this.fldResidenceState.Top = 3.71825F;
			this.fldResidenceState.Width = 0.375F;
			// 
			// fldResidenceCode
			// 
			this.fldResidenceCode.CanGrow = false;
			this.fldResidenceCode.Height = 0.19F;
			this.fldResidenceCode.Left = 2.688F;
			this.fldResidenceCode.MultiLine = false;
			this.fldResidenceCode.Name = "fldResidenceCode";
			this.fldResidenceCode.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldResidenceCode.Text = null;
			this.fldResidenceCode.Top = 3.4995F;
			this.fldResidenceCode.Width = 0.59375F;
			// 
			// fldValidated
			// 
			this.fldValidated.CanShrink = true;
			this.fldValidated.Height = 0.19F;
			this.fldValidated.Left = 3.938F;
			this.fldValidated.MultiLine = false;
			this.fldValidated.Name = "fldValidated";
			this.fldValidated.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldValidated.Text = "VALIDATED";
			this.fldValidated.Top = 3.187F;
			this.fldValidated.Visible = false;
			this.fldValidated.Width = 0.875F;
			// 
			// fldRegistration
			// 
			this.fldRegistration.CanShrink = true;
			this.fldRegistration.Height = 0.19F;
			this.fldRegistration.Left = 3.813F;
			this.fldRegistration.MultiLine = false;
			this.fldRegistration.Name = "fldRegistration";
			this.fldRegistration.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldRegistration.Text = "REGISTRATION";
			this.fldRegistration.Top = 3.3745F;
			this.fldRegistration.Visible = false;
			this.fldRegistration.Width = 1.125F;
			// 
			// fldRegistrationCity
			// 
			this.fldRegistrationCity.CanGrow = false;
			this.fldRegistrationCity.Height = 0.19F;
			this.fldRegistrationCity.Left = 3.5005F;
			this.fldRegistrationCity.MultiLine = false;
			this.fldRegistrationCity.Name = "fldRegistrationCity";
			this.fldRegistrationCity.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldRegistrationCity.Text = null;
			this.fldRegistrationCity.Top = 3.562F;
			this.fldRegistrationCity.Width = 1.75F;
			// 
			// fldMonthSticker
			// 
			this.fldMonthSticker.CanGrow = false;
			this.fldMonthSticker.Height = 0.19F;
			this.fldMonthSticker.Left = 1.3755F;
			this.fldMonthSticker.MultiLine = false;
			this.fldMonthSticker.Name = "fldMonthSticker";
			this.fldMonthSticker.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldMonthSticker.Text = null;
			this.fldMonthSticker.Top = 3.937F;
			this.fldMonthSticker.Width = 1.15625F;
			// 
			// fldYearSticker
			// 
			this.fldYearSticker.CanGrow = false;
			this.fldYearSticker.Height = 0.19F;
			this.fldYearSticker.Left = 1.3755F;
			this.fldYearSticker.MultiLine = false;
			this.fldYearSticker.Name = "fldYearSticker";
			this.fldYearSticker.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldYearSticker.Text = null;
			this.fldYearSticker.Top = 4.187F;
			this.fldYearSticker.Width = 1.15625F;
			// 
			// fldPriorTitle
			// 
			this.fldPriorTitle.CanGrow = false;
			this.fldPriorTitle.Height = 0.19F;
			this.fldPriorTitle.Left = 3.3755F;
			this.fldPriorTitle.MultiLine = false;
			this.fldPriorTitle.Name = "fldPriorTitle";
			this.fldPriorTitle.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldPriorTitle.Text = null;
			this.fldPriorTitle.Top = 4.187F;
			this.fldPriorTitle.Width = 1.875F;
			// 
			// fldRegistrationCode
			// 
			this.fldRegistrationCode.CanGrow = false;
			this.fldRegistrationCode.Height = 0.19F;
			this.fldRegistrationCode.Left = 3.063F;
			this.fldRegistrationCode.MultiLine = false;
			this.fldRegistrationCode.Name = "fldRegistrationCode";
			this.fldRegistrationCode.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldRegistrationCode.Text = null;
			this.fldRegistrationCode.Top = 3.7495F;
			this.fldRegistrationCode.Width = 0.75F;
			// 
			// fldRegistrationDate
			// 
			this.fldRegistrationDate.CanGrow = false;
			this.fldRegistrationDate.Height = 0.19F;
			this.fldRegistrationDate.Left = 3.938F;
			this.fldRegistrationDate.MultiLine = false;
			this.fldRegistrationDate.Name = "fldRegistrationDate";
			this.fldRegistrationDate.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldRegistrationDate.Text = null;
			this.fldRegistrationDate.Top = 3.7495F;
			this.fldRegistrationDate.Width = 1F;
			// 
			// fldRegFeeHR
			// 
			this.fldRegFeeHR.CanGrow = false;
			this.fldRegFeeHR.Height = 0.19F;
			this.fldRegFeeHR.Left = 6.563F;
			this.fldRegFeeHR.MultiLine = false;
			this.fldRegFeeHR.Name = "fldRegFeeHR";
			this.fldRegFeeHR.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldRegFeeHR.Text = null;
			this.fldRegFeeHR.Top = 2.187F;
			this.fldRegFeeHR.Width = 0.28125F;
			// 
			// fldRegCreditHR
			// 
			this.fldRegCreditHR.CanGrow = false;
			this.fldRegCreditHR.Height = 0.19F;
			this.fldRegCreditHR.Left = 6.563F;
			this.fldRegCreditHR.MultiLine = false;
			this.fldRegCreditHR.Name = "fldRegCreditHR";
			this.fldRegCreditHR.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldRegCreditHR.Text = null;
			this.fldRegCreditHR.Top = 2.437F;
			this.fldRegCreditHR.Width = 0.28125F;
			// 
			// fldFeesHR
			// 
			this.fldFeesHR.CanGrow = false;
			this.fldFeesHR.Height = 0.19F;
			this.fldFeesHR.Left = 6.563F;
			this.fldFeesHR.MultiLine = false;
			this.fldFeesHR.Name = "fldFeesHR";
			this.fldFeesHR.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldFeesHR.Text = null;
			this.fldFeesHR.Top = 2.687F;
			this.fldFeesHR.Width = 0.28125F;
			// 
			// fldExciseHR
			// 
			this.fldExciseHR.CanGrow = false;
			this.fldExciseHR.Height = 0.19F;
			this.fldExciseHR.Left = 5.34425F;
			this.fldExciseHR.MultiLine = false;
			this.fldExciseHR.Name = "fldExciseHR";
			this.fldExciseHR.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldExciseHR.Text = null;
			this.fldExciseHR.Top = 2.687F;
			this.fldExciseHR.Width = 0.28125F;
			// 
			// fldExciseCreditHR
			// 
			this.fldExciseCreditHR.CanGrow = false;
			this.fldExciseCreditHR.Height = 0.19F;
			this.fldExciseCreditHR.Left = 5.313F;
			this.fldExciseCreditHR.MultiLine = false;
			this.fldExciseCreditHR.Name = "fldExciseCreditHR";
			this.fldExciseCreditHR.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldExciseCreditHR.Text = null;
			this.fldExciseCreditHR.Top = 2.937F;
			this.fldExciseCreditHR.Width = 0.3125F;
			// 
			// fldSubtotalHR
			// 
			this.fldSubtotalHR.CanGrow = false;
			this.fldSubtotalHR.Height = 0.19F;
			this.fldSubtotalHR.Left = 5.34425F;
			this.fldSubtotalHR.MultiLine = false;
			this.fldSubtotalHR.Name = "fldSubtotalHR";
			this.fldSubtotalHR.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldSubtotalHR.Text = null;
			this.fldSubtotalHR.Top = 3.187F;
			this.fldSubtotalHR.Width = 0.28125F;
			// 
			// fldExciseTotalHR
			// 
			this.fldExciseTotalHR.CanGrow = false;
			this.fldExciseTotalHR.Height = 0.19F;
			this.fldExciseTotalHR.Left = 5.34425F;
			this.fldExciseTotalHR.MultiLine = false;
			this.fldExciseTotalHR.Name = "fldExciseTotalHR";
			this.fldExciseTotalHR.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldExciseTotalHR.Text = null;
			this.fldExciseTotalHR.Top = 3.687F;
			this.fldExciseTotalHR.Width = 0.28125F;
			// 
			// fldOwner3
			// 
			this.fldOwner3.CanGrow = false;
			this.fldOwner3.Height = 0.19F;
			this.fldOwner3.Left = 0.3442504F;
			this.fldOwner3.MultiLine = false;
			this.fldOwner3.Name = "fldOwner3";
			this.fldOwner3.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldOwner3.Text = null;
			this.fldOwner3.Top = 2.21825F;
			this.fldOwner3.Width = 3.6875F;
			// 
			// fldDOB3
			// 
			this.fldDOB3.CanGrow = false;
			this.fldDOB3.Height = 0.19F;
			this.fldDOB3.Left = 4.1255F;
			this.fldDOB3.MultiLine = false;
			this.fldDOB3.Name = "fldDOB3";
			this.fldDOB3.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldDOB3.Text = null;
			this.fldDOB3.Top = 2.21825F;
			this.fldDOB3.Width = 0.96875F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.CanGrow = false;
			this.fldAddress2.Height = 0.19F;
			this.fldAddress2.Left = 0.3442504F;
			this.fldAddress2.MultiLine = false;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1" +
    "";
			this.fldAddress2.Text = null;
			this.fldAddress2.Top = 3.03075F;
			this.fldAddress2.Width = 3.0625F;
			// 
			// fldResidenceAddress
			// 
			this.fldResidenceAddress.CanGrow = false;
			this.fldResidenceAddress.Height = 0.19F;
			this.fldResidenceAddress.Left = 0.3442504F;
			this.fldResidenceAddress.MultiLine = false;
			this.fldResidenceAddress.Name = "fldResidenceAddress";
			this.fldResidenceAddress.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldResidenceAddress.Text = null;
			this.fldResidenceAddress.Top = 3.4995F;
			this.fldResidenceAddress.Width = 2.3125F;
			// 
			// fldDoubleCTA
			// 
			this.fldDoubleCTA.CanGrow = false;
			this.fldDoubleCTA.Height = 0.19F;
			this.fldDoubleCTA.Left = 6.8755F;
			this.fldDoubleCTA.MultiLine = false;
			this.fldDoubleCTA.Name = "fldDoubleCTA";
			this.fldDoubleCTA.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldDoubleCTA.Text = null;
			this.fldDoubleCTA.Top = 4.03075F;
			this.fldDoubleCTA.Visible = false;
			this.fldDoubleCTA.Width = 0.78125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptMVR3
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.1875F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.499306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Courier New\'; font-style: inherit; font-variant: inherit; font-weig" +
            "ht: inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldReason1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidation1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidation2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidation3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckDigits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCumberlandCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReason2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReason3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMileage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEffective)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAxles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegisteredWeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFuel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMil)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgentFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLessor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOTNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSubtotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTaxDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthSticker)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearSticker)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPriorTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegistrationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFeeHR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegCreditHR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFeesHR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseHR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseCreditHR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSubtotalHR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotalHR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDOB3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoubleCTA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReason1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidation1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidation2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidation3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckDigits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCumberlandCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReason2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReason3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMileage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMileage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEffective;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAxles;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetWeight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegisteredWeight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFuel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBase;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMil;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAgentFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLessor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOTNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExcise;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSubtotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitleFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCTANumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseTaxDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistration;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYearSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPriorTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegFeeHR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegCreditHR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFeesHR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseHR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseCreditHR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSubtotalHR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseTotalHR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOB3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDoubleCTA;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
