﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubReportHeadingLongTerm.
	/// </summary>
	partial class rptSubReportHeadingLongTerm
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSubReportHeadingLongTerm));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTownCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAuthType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAgent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtProcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtVersion = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAuthType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAgent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVersion)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label3,
				this.Label4,
				this.txtMuni,
				this.txtTownCounty,
				this.Label5,
				this.Label6,
				this.txtAuthType,
				this.txtAgent,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.txtDate,
				this.txtProcess,
				this.txtDateReceived,
				this.txtPhone,
				this.Label12,
				this.Field1,
				this.Label13,
				this.txtVersion
			});
			this.Detail.Height = 0.96875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.09375F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label3.Text = "MUNICIPALITY:";
			this.Label3.Top = 0.21875F;
			this.Label3.Width = 1.34375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.09375F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label4.Text = "TOWN/COUNTY CODE:";
			this.Label4.Top = 0.40625F;
			this.Label4.Width = 1.75F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 1.46875F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtMuni.Text = "Field1";
			this.txtMuni.Top = 0.21875F;
			this.txtMuni.Width = 1.78125F;
			// 
			// txtTownCounty
			// 
			this.txtTownCounty.Height = 0.1875F;
			this.txtTownCounty.Left = 1.9375F;
			this.txtTownCounty.Name = "txtTownCounty";
			this.txtTownCounty.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtTownCounty.Text = "Field1";
			this.txtTownCounty.Top = 0.40625F;
			this.txtTownCounty.Width = 0.71875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.09375F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label5.Text = "AUTHORIZATION TYPE:";
			this.Label5.Top = 0.59375F;
			this.Label5.Width = 1.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.09375F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label6.Text = "AGENT";
			this.Label6.Top = 0.78125F;
			this.Label6.Width = 0.71875F;
			// 
			// txtAuthType
			// 
			this.txtAuthType.Height = 0.1875F;
			this.txtAuthType.Left = 1.9375F;
			this.txtAuthType.Name = "txtAuthType";
			this.txtAuthType.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtAuthType.Text = "Field1";
			this.txtAuthType.Top = 0.59375F;
			this.txtAuthType.Width = 1.25F;
			// 
			// txtAgent
			// 
			this.txtAgent.Height = 0.1875F;
			this.txtAgent.Left = 0.90625F;
			this.txtAgent.Name = "txtAgent";
			this.txtAgent.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtAgent.Text = "Field1";
			this.txtAgent.Top = 0.78125F;
			this.txtAgent.Width = 2.03125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.4375F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label8.Text = "DATE OF REPORT:";
			this.Label8.Top = 0.21875F;
			this.Label8.Width = 1.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.4375F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label9.Text = "PROCESS DATE RANGE:";
			this.Label9.Top = 0.40625F;
			this.Label9.Width = 1.84375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.4375F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label10.Text = "DATE RECEIVED";
			this.Label10.Top = 0.59375F;
			this.Label10.Width = 1.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.4375F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label11.Text = "TOWN PHONE NUMBER:";
			this.Label11.Top = 0.78125F;
			this.Label11.Width = 1.875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0.21875F;
			this.txtDate.Width = 1.96875F;
			// 
			// txtProcess
			// 
			this.txtProcess.Height = 0.1875F;
			this.txtProcess.Left = 5.375F;
			this.txtProcess.Name = "txtProcess";
			this.txtProcess.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtProcess.Text = "Field1";
			this.txtProcess.Top = 0.40625F;
			this.txtProcess.Width = 2F;
			// 
			// txtDateReceived
			// 
			this.txtDateReceived.Height = 0.1875F;
			this.txtDateReceived.Left = 5.375F;
			this.txtDateReceived.Name = "txtDateReceived";
			this.txtDateReceived.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtDateReceived.Text = "Field1";
			this.txtDateReceived.Top = 0.59375F;
			this.txtDateReceived.Width = 1.9375F;
			// 
			// txtPhone
			// 
			this.txtPhone.Height = 0.1875F;
			this.txtPhone.Left = 5.375F;
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtPhone.Text = "Field1";
			this.txtPhone.Top = 0.78125F;
			this.txtPhone.Width = 1.9375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.09375F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label12.Text = "VENDOR ID #:";
			this.Label12.Top = 0.03125F;
			this.Label12.Width = 1.34375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 1.46875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Field1.Text = "TRIO";
			this.Field1.Top = 0.03125F;
			this.Field1.Width = 1.46875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.4375F;
			this.Label13.MultiLine = false;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 1";
			this.Label13.Text = "VERSION UPDATE#:";
			this.Label13.Top = 0.03125F;
			this.Label13.Width = 1.875F;
			// 
			// txtVersion
			// 
			this.txtVersion.Height = 0.1875F;
			this.txtVersion.Left = 5.375F;
			this.txtVersion.Name = "txtVersion";
			this.txtVersion.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.txtVersion.Text = "Field1";
			this.txtVersion.Top = 0.03125F;
			this.txtVersion.Width = 1.96875F;
			// 
			// rptSubReportHeadingLongTerm
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAuthType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAgent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVersion)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTownCounty;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAuthType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAgent;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtProcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhone;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVersion;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
