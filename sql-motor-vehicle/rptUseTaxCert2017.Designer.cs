﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptUseTaxCert2017.
	/// </summary>
	partial class rptUseTaxCert2017
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUseTaxCert2017));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.imgPDFpage1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVin1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLength = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVin2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSellerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSellerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDateofTransfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFullPurchasePrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAllowance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUseTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserFName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserSocialSec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDatePaid1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserLName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLienHolderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLienHolderAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDifferentOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.imgPDFpage2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldExemptTypeA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWhereRegistered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDateOfOriginalReg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRentalTaxReg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeG = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.imgPDFpage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLength)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateofTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFullPurchasePrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAllowance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserFName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserSocialSec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDatePaid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserLName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifferentOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPDFpage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWhereRegistered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateOfOriginalReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRentalTaxReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeG)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.imgPDFpage1,
            this.fldType1,
            this.fldType2,
            this.fldMake1,
            this.fldModel1,
            this.fldYear1,
            this.fldMake2,
            this.fldModel2,
            this.fldYear2,
            this.fldVin1,
            this.fldLength,
            this.fldHP,
            this.fldVin2,
            this.fldSellerName,
            this.fldSellerAddress,
            this.fldDateofTransfer,
            this.fldFullPurchasePrice,
            this.fldAllowance,
            this.fldNetAmount,
            this.fldUseTax,
            this.fldExemptType,
            this.fldPurchaserFName,
            this.fldPurchaserSocialSec,
            this.fldPurchaserZip,
            this.fldRegDate,
            this.fldTaxAmount,
            this.fldDatePaid1,
            this.fldPurchaserAddress,
            this.fldPurchaserCity,
            this.fldClassPlate,
            this.fldPurchaserLName,
            this.fldPurchaserState,
            this.fldLienHolderName,
            this.fldLienHolderAddress,
            this.fldDifferentOwner});
			this.Detail.Height = 10.29167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// imgPDFpage1
			// 
			this.imgPDFpage1.Height = 10.1875F;
			this.imgPDFpage1.HyperLink = null;
			this.imgPDFpage1.ImageData = ((System.IO.Stream)(resources.GetObject("imgPDFpage1.ImageData")));
			this.imgPDFpage1.Left = 0.1875F;
			this.imgPDFpage1.LineWeight = 1F;
			this.imgPDFpage1.Name = "imgPDFpage1";
			this.imgPDFpage1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgPDFpage1.Top = 0F;
			this.imgPDFpage1.Width = 7.5625F;
			// 
			// fldType1
			// 
			this.fldType1.CanGrow = false;
			this.fldType1.Height = 0.1875F;
			this.fldType1.Left = 0.3125F;
			this.fldType1.Name = "fldType1";
			this.fldType1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldType1.Text = null;
			this.fldType1.Top = 1.930556F;
			this.fldType1.Width = 2.375F;
			// 
			// fldType2
			// 
			this.fldType2.CanGrow = false;
			this.fldType2.Height = 0.1875F;
			this.fldType2.Left = 4.25F;
			this.fldType2.Name = "fldType2";
			this.fldType2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldType2.Text = null;
			this.fldType2.Top = 1.927083F;
			this.fldType2.Width = 3.3125F;
			// 
			// fldMake1
			// 
			this.fldMake1.CanGrow = false;
			this.fldMake1.Height = 0.1875F;
			this.fldMake1.Left = 0.3125F;
			this.fldMake1.Name = "fldMake1";
			this.fldMake1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldMake1.Text = null;
			this.fldMake1.Top = 2.375F;
			this.fldMake1.Width = 1.5625F;
			// 
			// fldModel1
			// 
			this.fldModel1.CanGrow = false;
			this.fldModel1.Height = 0.1875F;
			this.fldModel1.Left = 2F;
			this.fldModel1.Name = "fldModel1";
			this.fldModel1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldModel1.Text = null;
			this.fldModel1.Top = 2.375F;
			this.fldModel1.Width = 1.125F;
			// 
			// fldYear1
			// 
			this.fldYear1.CanGrow = false;
			this.fldYear1.Height = 0.1875F;
			this.fldYear1.Left = 3.25F;
			this.fldYear1.Name = "fldYear1";
			this.fldYear1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldYear1.Text = null;
			this.fldYear1.Top = 2.375F;
			this.fldYear1.Width = 0.6875F;
			// 
			// fldMake2
			// 
			this.fldMake2.CanGrow = false;
			this.fldMake2.Height = 0.1875F;
			this.fldMake2.Left = 4.25F;
			this.fldMake2.Name = "fldMake2";
			this.fldMake2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldMake2.Text = null;
			this.fldMake2.Top = 2.375F;
			this.fldMake2.Width = 1.3125F;
			// 
			// fldModel2
			// 
			this.fldModel2.CanGrow = false;
			this.fldModel2.Height = 0.1875F;
			this.fldModel2.Left = 5.625F;
			this.fldModel2.Name = "fldModel2";
			this.fldModel2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldModel2.Text = null;
			this.fldModel2.Top = 2.375F;
			this.fldModel2.Width = 1F;
			// 
			// fldYear2
			// 
			this.fldYear2.CanGrow = false;
			this.fldYear2.Height = 0.1875F;
			this.fldYear2.Left = 6.75F;
			this.fldYear2.Name = "fldYear2";
			this.fldYear2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldYear2.Text = null;
			this.fldYear2.Top = 2.375F;
			this.fldYear2.Width = 0.625F;
			// 
			// fldVin1
			// 
			this.fldVin1.CanGrow = false;
			this.fldVin1.Height = 0.1875F;
			this.fldVin1.Left = 0.3125F;
			this.fldVin1.Name = "fldVin1";
			this.fldVin1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldVin1.Text = null;
			this.fldVin1.Top = 2.84375F;
			this.fldVin1.Width = 3.75F;
			// 
			// fldLength
			// 
			this.fldLength.CanGrow = false;
			this.fldLength.Height = 0.1875F;
			this.fldLength.Left = 3.125F;
			this.fldLength.Name = "fldLength";
			this.fldLength.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldLength.Text = null;
			this.fldLength.Top = 1.927083F;
			this.fldLength.Width = 0.5F;
			// 
			// fldHP
			// 
			this.fldHP.CanGrow = false;
			this.fldHP.Height = 0.1875F;
			this.fldHP.Left = 3.90625F;
			this.fldHP.Name = "fldHP";
			this.fldHP.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldHP.Text = null;
			this.fldHP.Top = 1.927083F;
			this.fldHP.Width = 0.25F;
			// 
			// fldVin2
			// 
			this.fldVin2.CanGrow = false;
			this.fldVin2.Height = 0.1875F;
			this.fldVin2.Left = 4.25F;
			this.fldVin2.Name = "fldVin2";
			this.fldVin2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldVin2.Text = null;
			this.fldVin2.Top = 2.84375F;
			this.fldVin2.Width = 3.4375F;
			// 
			// fldSellerName
			// 
			this.fldSellerName.CanGrow = false;
			this.fldSellerName.Height = 0.1875F;
			this.fldSellerName.Left = 1.0625F;
			this.fldSellerName.Name = "fldSellerName";
			this.fldSellerName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldSellerName.Text = null;
			this.fldSellerName.Top = 3.263889F;
			this.fldSellerName.Width = 3.5F;
			// 
			// fldSellerAddress
			// 
			this.fldSellerAddress.CanGrow = false;
			this.fldSellerAddress.Height = 0.1666667F;
			this.fldSellerAddress.Left = 1.0625F;
			this.fldSellerAddress.Name = "fldSellerAddress";
			this.fldSellerAddress.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldSellerAddress.Text = null;
			this.fldSellerAddress.Top = 3.645833F;
			this.fldSellerAddress.Width = 6.625F;
			// 
			// fldDateofTransfer
			// 
			this.fldDateofTransfer.CanGrow = false;
			this.fldDateofTransfer.Height = 0.1875F;
			this.fldDateofTransfer.Left = 5.75F;
			this.fldDateofTransfer.Name = "fldDateofTransfer";
			this.fldDateofTransfer.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDateofTransfer.Text = null;
			this.fldDateofTransfer.Top = 3.263889F;
			this.fldDateofTransfer.Width = 1.875F;
			// 
			// fldFullPurchasePrice
			// 
			this.fldFullPurchasePrice.CanGrow = false;
			this.fldFullPurchasePrice.Height = 0.1875F;
			this.fldFullPurchasePrice.Left = 5.75F;
			this.fldFullPurchasePrice.Name = "fldFullPurchasePrice";
			this.fldFullPurchasePrice.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.fldFullPurchasePrice.Text = null;
			this.fldFullPurchasePrice.Top = 4F;
			this.fldFullPurchasePrice.Width = 1.5625F;
			// 
			// fldAllowance
			// 
			this.fldAllowance.CanGrow = false;
			this.fldAllowance.Height = 0.1875F;
			this.fldAllowance.Left = 5.75F;
			this.fldAllowance.Name = "fldAllowance";
			this.fldAllowance.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.fldAllowance.Text = null;
			this.fldAllowance.Top = 4.375F;
			this.fldAllowance.Width = 1.5625F;
			// 
			// fldNetAmount
			// 
			this.fldNetAmount.CanGrow = false;
			this.fldNetAmount.Height = 0.1875F;
			this.fldNetAmount.Left = 5.75F;
			this.fldNetAmount.Name = "fldNetAmount";
			this.fldNetAmount.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.fldNetAmount.Text = null;
			this.fldNetAmount.Top = 4.75F;
			this.fldNetAmount.Width = 1.5625F;
			// 
			// fldUseTax
			// 
			this.fldUseTax.CanGrow = false;
			this.fldUseTax.Height = 0.1875F;
			this.fldUseTax.Left = 5.75F;
			this.fldUseTax.Name = "fldUseTax";
			this.fldUseTax.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.fldUseTax.Text = null;
			this.fldUseTax.Top = 5.125F;
			this.fldUseTax.Width = 1.5625F;
			// 
			// fldExemptType
			// 
			this.fldExemptType.CanGrow = false;
			this.fldExemptType.Height = 0.1875F;
			this.fldExemptType.Left = 3.875F;
			this.fldExemptType.Name = "fldExemptType";
			this.fldExemptType.Style = "font-family: \'Roman 10cpi\'; vertical-align: top; ddo-char-set: 1";
			this.fldExemptType.Text = null;
			this.fldExemptType.Top = 5.5F;
			this.fldExemptType.Width = 0.25F;
			// 
			// fldPurchaserFName
			// 
			this.fldPurchaserFName.CanGrow = false;
			this.fldPurchaserFName.Height = 0.1875F;
			this.fldPurchaserFName.Left = 0.9375F;
			this.fldPurchaserFName.Name = "fldPurchaserFName";
			this.fldPurchaserFName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserFName.Text = null;
			this.fldPurchaserFName.Top = 7.097222F;
			this.fldPurchaserFName.Width = 1.75F;
			// 
			// fldPurchaserSocialSec
			// 
			this.fldPurchaserSocialSec.CanGrow = false;
			this.fldPurchaserSocialSec.Height = 0.1875F;
			this.fldPurchaserSocialSec.Left = 4.5F;
			this.fldPurchaserSocialSec.Name = "fldPurchaserSocialSec";
			this.fldPurchaserSocialSec.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserSocialSec.Text = null;
			this.fldPurchaserSocialSec.Top = 7.097222F;
			this.fldPurchaserSocialSec.Width = 0.9375F;
			// 
			// fldPurchaserZip
			// 
			this.fldPurchaserZip.CanGrow = false;
			this.fldPurchaserZip.Height = 0.1875F;
			this.fldPurchaserZip.Left = 6.8125F;
			this.fldPurchaserZip.Name = "fldPurchaserZip";
			this.fldPurchaserZip.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserZip.Text = null;
			this.fldPurchaserZip.Top = 7.5625F;
			this.fldPurchaserZip.Width = 0.875F;
			// 
			// fldRegDate
			// 
			this.fldRegDate.CanGrow = false;
			this.fldRegDate.Height = 0.1875F;
			this.fldRegDate.Left = 3.125F;
			this.fldRegDate.Name = "fldRegDate";
			this.fldRegDate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldRegDate.Text = null;
			this.fldRegDate.Top = 8.5625F;
			this.fldRegDate.Width = 0.9375F;
			// 
			// fldTaxAmount
			// 
			this.fldTaxAmount.CanGrow = false;
			this.fldTaxAmount.Height = 0.1875F;
			this.fldTaxAmount.Left = 5.6875F;
			this.fldTaxAmount.Name = "fldTaxAmount";
			this.fldTaxAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldTaxAmount.Text = null;
			this.fldTaxAmount.Top = 8.5625F;
			this.fldTaxAmount.Width = 1.625F;
			// 
			// fldDatePaid1
			// 
			this.fldDatePaid1.CanGrow = false;
			this.fldDatePaid1.Height = 0.1875F;
			this.fldDatePaid1.Left = 4.25F;
			this.fldDatePaid1.Name = "fldDatePaid1";
			this.fldDatePaid1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDatePaid1.Text = null;
			this.fldDatePaid1.Top = 8.5625F;
			this.fldDatePaid1.Width = 1.1875F;
			// 
			// fldPurchaserAddress
			// 
			this.fldPurchaserAddress.CanGrow = false;
			this.fldPurchaserAddress.Height = 0.1875F;
			this.fldPurchaserAddress.Left = 1.5F;
			this.fldPurchaserAddress.MultiLine = false;
			this.fldPurchaserAddress.Name = "fldPurchaserAddress";
			this.fldPurchaserAddress.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserAddress.Text = null;
			this.fldPurchaserAddress.Top = 7.5625F;
			this.fldPurchaserAddress.Width = 2.5625F;
			// 
			// fldPurchaserCity
			// 
			this.fldPurchaserCity.CanGrow = false;
			this.fldPurchaserCity.Height = 0.1875F;
			this.fldPurchaserCity.Left = 4.375F;
			this.fldPurchaserCity.Name = "fldPurchaserCity";
			this.fldPurchaserCity.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserCity.Text = null;
			this.fldPurchaserCity.Top = 7.5625F;
			this.fldPurchaserCity.Width = 1.5625F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.CanGrow = false;
			this.fldClassPlate.Height = 0.1875F;
			this.fldClassPlate.Left = 0.625F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldClassPlate.Text = null;
			this.fldClassPlate.Top = 8.5625F;
			this.fldClassPlate.Width = 1.5625F;
			// 
			// fldPurchaserLName
			// 
			this.fldPurchaserLName.CanGrow = false;
			this.fldPurchaserLName.Height = 0.1875F;
			this.fldPurchaserLName.Left = 2.75F;
			this.fldPurchaserLName.Name = "fldPurchaserLName";
			this.fldPurchaserLName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserLName.Text = null;
			this.fldPurchaserLName.Top = 7.097222F;
			this.fldPurchaserLName.Width = 1.625F;
			// 
			// fldPurchaserState
			// 
			this.fldPurchaserState.CanGrow = false;
			this.fldPurchaserState.Height = 0.1875F;
			this.fldPurchaserState.Left = 6F;
			this.fldPurchaserState.Name = "fldPurchaserState";
			this.fldPurchaserState.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserState.Text = null;
			this.fldPurchaserState.Top = 7.5625F;
			this.fldPurchaserState.Width = 0.625F;
			// 
			// fldLienHolderName
			// 
			this.fldLienHolderName.CanGrow = false;
			this.fldLienHolderName.Height = 0.1875F;
			this.fldLienHolderName.Left = 1.75F;
			this.fldLienHolderName.Name = "fldLienHolderName";
			this.fldLienHolderName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldLienHolderName.Text = null;
			this.fldLienHolderName.Top = 6.104167F;
			this.fldLienHolderName.Width = 2.1875F;
			// 
			// fldLienHolderAddress
			// 
			this.fldLienHolderAddress.CanGrow = false;
			this.fldLienHolderAddress.Height = 0.1875F;
			this.fldLienHolderAddress.Left = 4.125F;
			this.fldLienHolderAddress.Name = "fldLienHolderAddress";
			this.fldLienHolderAddress.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldLienHolderAddress.Text = null;
			this.fldLienHolderAddress.Top = 6.104167F;
			this.fldLienHolderAddress.Width = 3.5625F;
			// 
			// fldDifferentOwner
			// 
			this.fldDifferentOwner.CanGrow = false;
			this.fldDifferentOwner.Height = 0.2152778F;
			this.fldDifferentOwner.Left = 3.9375F;
			this.fldDifferentOwner.MultiLine = false;
			this.fldDifferentOwner.Name = "fldDifferentOwner";
			this.fldDifferentOwner.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldDifferentOwner.Text = null;
			this.fldDifferentOwner.Top = 5.847222F;
			this.fldDifferentOwner.Width = 3.75F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.imgPDFpage2,
            this.fldExemptTypeA,
            this.fldOtherStateTax,
            this.fldOtherAmount,
            this.fldExemptNumber,
            this.fldExemptTypeC,
            this.fldExemptTypeD,
            this.fldExemptTypeB,
            this.fldExemptTypeE,
            this.fldWhereRegistered,
            this.fldRegNumber,
            this.fldDateOfOriginalReg,
            this.fldRentalTaxReg,
            this.fldExemptTypeG,
            this.fldExemptTypeF});
			this.GroupFooter1.Height = 9.677083F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// imgPDFpage2
			// 
			this.imgPDFpage2.Height = 9.677083F;
			this.imgPDFpage2.HyperLink = null;
			this.imgPDFpage2.ImageData = ((System.IO.Stream)(resources.GetObject("imgPDFpage2.ImageData")));
			this.imgPDFpage2.Left = 0F;
			this.imgPDFpage2.LineWeight = 1F;
			this.imgPDFpage2.Name = "imgPDFpage2";
			this.imgPDFpage2.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.TopLeft;
			this.imgPDFpage2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgPDFpage2.Top = 0F;
			this.imgPDFpage2.Width = 7.847222F;
			// 
			// fldExemptTypeA
			// 
			this.fldExemptTypeA.CanGrow = false;
			this.fldExemptTypeA.Height = 0.1875F;
			this.fldExemptTypeA.Left = 0.2083333F;
			this.fldExemptTypeA.Name = "fldExemptTypeA";
			this.fldExemptTypeA.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeA.Text = null;
			this.fldExemptTypeA.Top = 3.444444F;
			this.fldExemptTypeA.Width = 0.25F;
			// 
			// fldOtherStateTax
			// 
			this.fldOtherStateTax.CanGrow = false;
			this.fldOtherStateTax.Height = 0.1875F;
			this.fldOtherStateTax.Left = 0.625F;
			this.fldOtherStateTax.Name = "fldOtherStateTax";
			this.fldOtherStateTax.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldOtherStateTax.Text = null;
			this.fldOtherStateTax.Top = 6.479167F;
			this.fldOtherStateTax.Width = 2.0625F;
			// 
			// fldOtherAmount
			// 
			this.fldOtherAmount.CanGrow = false;
			this.fldOtherAmount.Height = 0.1875F;
			this.fldOtherAmount.Left = 4.125F;
			this.fldOtherAmount.Name = "fldOtherAmount";
			this.fldOtherAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldOtherAmount.Text = null;
			this.fldOtherAmount.Top = 6.479167F;
			this.fldOtherAmount.Width = 1.4375F;
			// 
			// fldExemptNumber
			// 
			this.fldExemptNumber.CanGrow = false;
			this.fldExemptNumber.Height = 0.1875F;
			this.fldExemptNumber.Left = 1.25F;
			this.fldExemptNumber.Name = "fldExemptNumber";
			this.fldExemptNumber.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptNumber.Text = null;
			this.fldExemptNumber.Top = 3.944444F;
			this.fldExemptNumber.Width = 1.9375F;
			// 
			// fldExemptTypeC
			// 
			this.fldExemptTypeC.CanGrow = false;
			this.fldExemptTypeC.Height = 0.1875F;
			this.fldExemptTypeC.Left = 0.2083333F;
			this.fldExemptTypeC.Name = "fldExemptTypeC";
			this.fldExemptTypeC.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeC.Text = null;
			this.fldExemptTypeC.Top = 5.708333F;
			this.fldExemptTypeC.Width = 0.25F;
			// 
			// fldExemptTypeD
			// 
			this.fldExemptTypeD.CanGrow = false;
			this.fldExemptTypeD.Height = 0.1875F;
			this.fldExemptTypeD.Left = 0.2083333F;
			this.fldExemptTypeD.Name = "fldExemptTypeD";
			this.fldExemptTypeD.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeD.Text = null;
			this.fldExemptTypeD.Top = 6.701389F;
			this.fldExemptTypeD.Width = 0.25F;
			// 
			// fldExemptTypeB
			// 
			this.fldExemptTypeB.CanGrow = false;
			this.fldExemptTypeB.Height = 0.1875F;
			this.fldExemptTypeB.Left = 0.2083333F;
			this.fldExemptTypeB.Name = "fldExemptTypeB";
			this.fldExemptTypeB.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeB.Text = null;
			this.fldExemptTypeB.Top = 4.159722F;
			this.fldExemptTypeB.Width = 0.25F;
			// 
			// fldExemptTypeE
			// 
			this.fldExemptTypeE.CanGrow = false;
			this.fldExemptTypeE.Height = 0.1875F;
			this.fldExemptTypeE.Left = 0.2083333F;
			this.fldExemptTypeE.Name = "fldExemptTypeE";
			this.fldExemptTypeE.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeE.Text = null;
			this.fldExemptTypeE.Top = 7.291667F;
			this.fldExemptTypeE.Width = 0.25F;
			// 
			// fldWhereRegistered
			// 
			this.fldWhereRegistered.CanGrow = false;
			this.fldWhereRegistered.Height = 0.1875F;
			this.fldWhereRegistered.Left = 1F;
			this.fldWhereRegistered.Name = "fldWhereRegistered";
			this.fldWhereRegistered.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldWhereRegistered.Text = null;
			this.fldWhereRegistered.Top = 5.489583F;
			this.fldWhereRegistered.Width = 1.8125F;
			// 
			// fldRegNumber
			// 
			this.fldRegNumber.CanGrow = false;
			this.fldRegNumber.Height = 0.1875F;
			this.fldRegNumber.Left = 3.46875F;
			this.fldRegNumber.Name = "fldRegNumber";
			this.fldRegNumber.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldRegNumber.Text = null;
			this.fldRegNumber.Top = 5.489583F;
			this.fldRegNumber.Width = 1.3125F;
			// 
			// fldDateOfOriginalReg
			// 
			this.fldDateOfOriginalReg.CanGrow = false;
			this.fldDateOfOriginalReg.Height = 0.1875F;
			this.fldDateOfOriginalReg.Left = 6.125F;
			this.fldDateOfOriginalReg.Name = "fldDateOfOriginalReg";
			this.fldDateOfOriginalReg.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDateOfOriginalReg.Text = null;
			this.fldDateOfOriginalReg.Top = 5.489583F;
			this.fldDateOfOriginalReg.Width = 1.3125F;
			// 
			// fldRentalTaxReg
			// 
			this.fldRentalTaxReg.CanGrow = false;
			this.fldRentalTaxReg.Height = 0.1875F;
			this.fldRentalTaxReg.Left = 3.125F;
			this.fldRentalTaxReg.Name = "fldRentalTaxReg";
			this.fldRentalTaxReg.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldRentalTaxReg.Text = null;
			this.fldRentalTaxReg.Top = 7.756945F;
			this.fldRentalTaxReg.Width = 1.875F;
			// 
			// fldExemptTypeG
			// 
			this.fldExemptTypeG.CanGrow = false;
			this.fldExemptTypeG.Height = 0.1875F;
			this.fldExemptTypeG.Left = 0.2083333F;
			this.fldExemptTypeG.Name = "fldExemptTypeG";
			this.fldExemptTypeG.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeG.Text = null;
			this.fldExemptTypeG.Top = 8.6875F;
			this.fldExemptTypeG.Width = 0.25F;
			// 
			// fldExemptTypeF
			// 
			this.fldExemptTypeF.CanGrow = false;
			this.fldExemptTypeF.Height = 0.1875F;
			this.fldExemptTypeF.Left = 0.2083333F;
			this.fldExemptTypeF.Name = "fldExemptTypeF";
			this.fldExemptTypeF.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeF.Text = null;
			this.fldExemptTypeF.Top = 7.979167F;
			this.fldExemptTypeF.Width = 0.25F;
			// 
			// rptUseTaxCert2017
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.3125F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.375F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.847222F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.imgPDFpage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLength)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateofTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFullPurchasePrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAllowance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserFName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserSocialSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDatePaid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserLName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifferentOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPDFpage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWhereRegistered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateOfOriginalReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRentalTaxReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeG)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgPDFpage1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVin1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLength;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVin2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSellerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSellerAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDateofTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFullPurchasePrice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAllowance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUseTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserFName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserSocialSec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDatePaid1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserLName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienHolderName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienHolderAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDifferentOwner;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgPDFpage2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWhereRegistered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDateOfOriginalReg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRentalTaxReg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeG;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeF;
	}
}
