//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTAgentDetail.
	/// </summary>
	public partial class rptLTTAgentDetail : BaseSectionReport
	{
		public rptLTTAgentDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Agent Detail";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptLTTAgentDetail_ReportEnd;
		}

        private void RptLTTAgentDetail_ReportEnd(object sender, EventArgs e)
        {
			rsTitles.DisposeOf();
            rsAM.DisposeOf();
            rs.DisposeOf();
            rsUseTax.DisposeOf();

		}

        public static rptLTTAgentDetail InstancePtr
		{
			get
			{
				return (rptLTTAgentDetail)Sys.GetInstance(typeof(rptLTTAgentDetail));
			}
		}

		protected rptLTTAgentDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLTTAgentDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int[] lngLTTLargeUnits = new int[25 + 1];
		int[] lngLTTLargeUnitsTime = new int[25 + 1];
		int[] lngLTTSmallUnits = new int[25 + 1];
		int lngLTTLargeUnitsTotal;
		int lngLTTSmallUnitsTotal;
		int lngExtensionUnits;
		int lngTransfer25YearUnits;
		int lngTransferLargeUnits;
		int lngTransferSmallUnits;
		int lngLostPlateUnits;
		int lngDuplicateUnits;
		int lngCorrectionUnits;
		int lngTitle25YearUnits;
		int lngTitleUnits;
		int lngRushTitleUnits;
		int lngSalesTaxPaidUnits;
		int lngSalesTaxNoFeeUnits;
		int lngTotalUnits;
		// vbPorter upgrade warning: curLTTLargeAmount As Decimal	OnWrite(int, Decimal)
		Decimal[] curLTTLargeAmount = new Decimal[25 + 1];
		// vbPorter upgrade warning: curLTTLargeAmountTime As Decimal	OnWrite(int, Decimal)
		Decimal[] curLTTLargeAmountTime = new Decimal[25 + 1];
		// vbPorter upgrade warning: curLTTSmallAmount As Decimal	OnWrite(int, Decimal)
		Decimal[] curLTTSmallAmount = new Decimal[25 + 1];
		// vbPorter upgrade warning: curLTTLargeAmountTotal As Decimal	OnWrite(int, Decimal)
		Decimal curLTTLargeAmountTotal;
		// vbPorter upgrade warning: curLTTSmallAmountTotal As Decimal	OnWrite(int, Decimal)
		Decimal curLTTSmallAmountTotal;
		// vbPorter upgrade warning: curExtensionAmount As Decimal	OnWrite(int, Decimal)
		Decimal curExtensionAmount;
		// vbPorter upgrade warning: curTransfer25YearAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTransfer25YearAmount;
		// vbPorter upgrade warning: curTransferLargeAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTransferLargeAmount;
		// vbPorter upgrade warning: curTransferSmallAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTransferSmallAmount;
		// vbPorter upgrade warning: curLostPlateAmount As Decimal	OnWrite(int, Decimal)
		Decimal curLostPlateAmount;
		// vbPorter upgrade warning: curDuplicateAmount As Decimal	OnWrite(int, Decimal)
		Decimal curDuplicateAmount;
		// vbPorter upgrade warning: curCorrectionAmount As Decimal	OnWrite
		Decimal curCorrectionAmount;
		// vbPorter upgrade warning: curTitle25YearAmount As Decimal	OnWrite
		Decimal curTitle25YearAmount;
		// vbPorter upgrade warning: curTitleAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTitleAmount;
		// vbPorter upgrade warning: curRushTitleAmount As Decimal	OnWrite(int, Decimal)
		Decimal curRushTitleAmount;
		// vbPorter upgrade warning: curSalesTaxPaidAmount As Decimal	OnWrite(int, Decimal)
		Decimal curSalesTaxPaidAmount;
		// vbPorter upgrade warning: curSalesTaxNoFeeAmount As Decimal	OnWrite
		Decimal curSalesTaxNoFeeAmount;
		// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTotalAmount;
        Decimal curDuplicateFee;
		clsDRWrapper rsTitles = new clsDRWrapper();
		//clsDRWrapper rsSummary = new clsDRWrapper();
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;
		int lngPK1;
		int lngPK2;
		clsDRWrapper rsUseTax = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldDuplicateFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldLostPlateFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitleFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldRushTitleFee As object	OnWrite(string)
			clsDRWrapper rsDefaults = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			if (frmReportLongTerm.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND PeriodCloseoutID < 1";
				rsTitles.OpenRecordset("SELECT * from LongTermTitleApplications WHERE PeriodCloseoutID < 1");
				rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID IN (SELECT DISTINCT UseTaxID FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND PeriodCloseoutID < 1 AND UseTaxDone = 1 AND SalesTax > 0)");
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate BETWEEN '" + frmReportLongTerm.InstancePtr.cboStart.Text + "' AND '" + frmReportLongTerm.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(MotorVehicle.Statics.strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
				MotorVehicle.Statics.strSql = "SELECT * FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2);
				rsTitles.OpenRecordset("SELECT * from LongTermTitleApplications WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2));
				rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID IN (SELECT DISTINCT UseTaxID FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND UseTaxDone = 1 AND SalesTax > 0 AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + ")");
			}
			rsAM.OpenRecordset(MotorVehicle.Statics.strSql);
			rsDefaults.OpenRecordset("SELECT * FROM DefaultInfo", "TWMV0000.vb1");
			if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
			{
                curDuplicateFee = rsDefaults.Get_Fields("AgentFeeLongTermTrailerDupReg");
				fldDuplicateFee.Text = Strings.Format(rsDefaults.Get_Fields_Decimal("AgentFeeLongTermTrailerDupReg"), "#,##0.00");
				fldLostPlateFee.Text = Strings.Format(rsDefaults.Get_Fields_Decimal("LongTermReplacementPlateFee"), "#,##0.00");
				fldTitleFee.Text = Strings.Format(rsDefaults.Get_Fields("TitleFee"), "#,##0.00");
				fldRushTitleFee.Text = Strings.Format(rsDefaults.Get_Fields("RushTitleFee"), "#,##0.00");
			}
			CalculateTotals();
			SubReport2.Report = new rptSubReportHeadingLongTerm();
		}

		private void CalculateTotals()
		{
			int counter;
			for (counter = 0; counter <= 25; counter++)
			{
				lngLTTLargeUnits[counter] = 0;
				lngLTTLargeUnitsTime[counter] = 0;
				lngLTTSmallUnits[counter] = 0;
				curLTTLargeAmount[counter] = 0;
				curLTTLargeAmountTime[counter] = 0;
				curLTTSmallAmount[counter] = 0;
			}
			lngExtensionUnits = 0;
			lngTransfer25YearUnits = 0;
			lngTransferLargeUnits = 0;
			lngTransferSmallUnits = 0;
			lngLostPlateUnits = 0;
			lngDuplicateUnits = 0;
			lngCorrectionUnits = 0;
			lngTitle25YearUnits = 0;
			lngTitleUnits = 0;
			lngRushTitleUnits = 0;
			lngSalesTaxPaidUnits = 0;
			lngSalesTaxNoFeeUnits = 0;
			lngTotalUnits = 0;
			curExtensionAmount = 0;
			curTransfer25YearAmount = 0;
			curTransferLargeAmount = 0;
			curTransferSmallAmount = 0;
			curLostPlateAmount = 0;
			curDuplicateAmount = 0;
			curCorrectionAmount = 0;
			curTitle25YearAmount = 0;
			curTitleAmount = 0;
			curRushTitleAmount = 0;
			curSalesTaxPaidAmount = 0;
			curSalesTaxNoFeeAmount = 0;
			curTotalAmount = 0;
			curLTTLargeAmountTotal = 0;
			curLTTSmallAmountTotal = 0;
			lngLTTLargeUnitsTotal = 0;
			lngLTTSmallUnitsTotal = 0;
			while (rsAM.EndOfFile() != true)
			{
				string vbPorterVar = rsAM.Get_Fields_String("RegistrationType");
				if ((vbPorterVar == "NRR") || (vbPorterVar == "RRR"))
				{
					if (rsAM.Get_Fields("Extension") == true && FCConvert.ToString(rsAM.Get_Fields_String("RegistrationType")) == "RRR")
					{
						lngExtensionUnits += 1;
						curExtensionAmount += rsAM.Get_Fields("RegistrationFee");
					}
					else
					{
						if (FCConvert.ToInt32(rsAM.Get_Fields("NetWeight")) == 2000)
						{
							lngLTTSmallUnits[rsAM.Get_Fields_Int32("NumberOfYears")] += 1;
							curLTTSmallAmount[rsAM.Get_Fields_Int32("NumberOfYears")] += rsAM.Get_Fields("RegistrationFee");
						}
						else
						{
							if (rsAM.Get_Fields_Boolean("TwentyFiveYearPlate") == true)
							{
								lngLTTLargeUnits[25] += 1;
								curLTTLargeAmount[25] += rsAM.Get_Fields("RegistrationFee");
							}
							else
							{
								if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("TimePayment")))
								{
									lngLTTLargeUnitsTime[rsAM.Get_Fields_Int32("NumberOfYears")] += 1;
									curLTTLargeAmountTime[rsAM.Get_Fields_Int32("NumberOfYears")] += rsAM.Get_Fields("RegistrationFee");
								}
								else
								{
									lngLTTLargeUnits[rsAM.Get_Fields_Int32("NumberOfYears")] += 1;
									curLTTLargeAmount[rsAM.Get_Fields_Int32("NumberOfYears")] += rsAM.Get_Fields("RegistrationFee");
								}
							}
						}
					}
				}
				else if ((vbPorterVar == "NRT") || (vbPorterVar == "RRT"))
				{
					if (rsAM.Get_Fields_Boolean("TwentyFiveYearPlate") == true)
					{
						lngTransfer25YearUnits += 1;
						curTransfer25YearAmount += rsAM.Get_Fields("TransferFee");
					}
					else
					{
						if (FCConvert.ToInt32(rsAM.Get_Fields("NetWeight")) == 2000)
						{
							lngTransferSmallUnits += 1;
							curTransferSmallAmount += rsAM.Get_Fields("TransferFee");
						}
						else
						{
							lngTransferLargeUnits += 1;
							curTransferLargeAmount += rsAM.Get_Fields("TransferFee");
						}
					}
				}
				else if (vbPorterVar == "DPR")
				{
					lngDuplicateUnits += 1;
					curDuplicateAmount += curDuplicateFee;
				}
				else if ((vbPorterVar == "ECO") || (vbPorterVar == "ECR"))
				{
					lngCorrectionUnits += 1;
					// curCorrectionAmount = curCorrectionAmount + .Fields("StateFee")
				}
				else if (vbPorterVar == "LPS")
				{
					lngLostPlateUnits += 1;
					curLostPlateAmount += rsAM.Get_Fields("AgentFee");
				}
				// If .Fields("TitleDone") Then
				// If .Fields("TwentyFiveYearPlate") = True Then
				// lngTitle25YearUnits = lngTitle25YearUnits + 1
				// curTitle25YearAmount = curTitle25YearAmount + .Fields("TitleFee")
				// Else
				// lngTitleUnits = lngTitleUnits + 1
				// curTitleAmount = curTitleAmount + .Fields("TitleFee")
				// End If
				// If .Fields("RushTitleFee") <> 0 Then
				// lngRushTitleUnits = lngRushTitleUnits + 1
				// curRushTitleAmount = curRushTitleAmount + .Fields("RushTitleFee")
				// End If
				// End If
				if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("UseTaxDone")))
				{
					if (FCConvert.ToInt32(rsAM.Get_Fields("SalesTax")) == 0)
					{
						lngSalesTaxNoFeeUnits += 1;
					}
				}
				rsAM.MoveNext();
			}
			while (rsTitles.EndOfFile() != true)
			{
				lngTitleUnits += 1;
				// .Fields("Units")
				curTitleAmount += rsTitles.Get_Fields("Fee");
				if (rsTitles.Get_Fields_Decimal("RushFee") != 0)
				{
					lngRushTitleUnits += 1;
					curRushTitleAmount += rsTitles.Get_Fields_Decimal("RushFee");
				}
				rsTitles.MoveNext();
			}
			while (rsUseTax.EndOfFile() != true)
			{
				lngSalesTaxPaidUnits += 1;
				// .Fields("Units")
				curSalesTaxPaidAmount += rsUseTax.Get_Fields_Decimal("TaxAmount");
				rsUseTax.MoveNext();
			}
			for (counter = 2; counter <= 25; counter++)
			{
				curLTTLargeAmountTotal += curLTTLargeAmount[counter] + curLTTLargeAmountTime[counter];
				curLTTSmallAmountTotal += curLTTSmallAmount[counter];
				lngLTTLargeUnitsTotal += lngLTTLargeUnits[counter] + lngLTTLargeUnitsTime[counter];
				lngLTTSmallUnitsTotal += lngLTTSmallUnits[counter];
			}
			curTotalAmount = curLTTLargeAmountTotal + curLTTSmallAmountTotal + curExtensionAmount + curTransfer25YearAmount + curTransferLargeAmount + curTransferSmallAmount + curLostPlateAmount + curDuplicateAmount + curCorrectionAmount + curTitle25YearAmount + curTitleAmount + curSalesTaxPaidAmount + curSalesTaxNoFeeAmount + curRushTitleAmount;
			lngTotalUnits = lngLTTLargeUnitsTotal + lngLTTSmallUnitsTotal + lngExtensionUnits + lngTransfer25YearUnits + lngTransferLargeUnits + lngTransferSmallUnits + lngLostPlateUnits + lngDuplicateUnits + lngCorrectionUnits + lngTitle25YearUnits + lngTitleUnits + lngSalesTaxPaidUnits + lngSalesTaxNoFeeUnits + lngRushTitleUnits;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldLTTLargeAmount25Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount24Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount24Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount23Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount23Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount22Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount22Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount21Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount21Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount20Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount20Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount19Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount19Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount18Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount18Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount17Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount17Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount16Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount16Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount15Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount15Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount14Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount14Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount13Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount13Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount12Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount12Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount12 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount11Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount11Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount11 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount10Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount10Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount10 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount9Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount9Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount9 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount8Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount8Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount8 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount7 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount7 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmount2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmountSubTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeAmountSubTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTAmountTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldExtensionAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfer25YearAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransferLargeAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransferSmallAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldLostPlateAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldDuplicateAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldCorrectionAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitle25YearsAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitleAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldRushTitleAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxPaidAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxNoFeeAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits25Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits24Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits24Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits23Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits23Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits22Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits22Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits21Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits21Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits20Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits20Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits19Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits19Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits18Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits18Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits17Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits17Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits16Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits16Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits15Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits15Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits14Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits14Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits13Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits13Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits12Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits12Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits12 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits11Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits11Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits11 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits10Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits10Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits10 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits9Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits9Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits9 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits8Full As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits8Time As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits8 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits7 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits7 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnitsSubTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnitsSubTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTUnitsTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldExtensionsUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfers25YearUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfersLargeUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfersSmallUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldLostPlateUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldDuplicateUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldCorrectionUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitle25YearUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitleUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldRushTitleUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxPaidUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxNoFeeUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalUnits As object	OnWrite(string)
			fldLTTLargeAmount25Full.Text = Strings.Format(curLTTLargeAmount[25], "#,##0.00");
			fldLTTLargeAmount24Full.Text = Strings.Format(curLTTLargeAmount[24], "#,##0.00");
			fldLTTLargeAmount24Time.Text = Strings.Format(curLTTLargeAmountTime[24], "#,##0.00");
			fldLTTLargeAmount23Full.Text = Strings.Format(curLTTLargeAmount[23], "#,##0.00");
			fldLTTLargeAmount23Time.Text = Strings.Format(curLTTLargeAmountTime[23], "#,##0.00");
			fldLTTLargeAmount22Full.Text = Strings.Format(curLTTLargeAmount[22], "#,##0.00");
			fldLTTLargeAmount22Time.Text = Strings.Format(curLTTLargeAmountTime[22], "#,##0.00");
			fldLTTLargeAmount21Full.Text = Strings.Format(curLTTLargeAmount[21], "#,##0.00");
			fldLTTLargeAmount21Time.Text = Strings.Format(curLTTLargeAmountTime[21], "#,##0.00");
			fldLTTLargeAmount20Full.Text = Strings.Format(curLTTLargeAmount[20], "#,##0.00");
			fldLTTLargeAmount20Time.Text = Strings.Format(curLTTLargeAmountTime[20], "#,##0.00");
			fldLTTLargeAmount19Full.Text = Strings.Format(curLTTLargeAmount[19], "#,##0.00");
			fldLTTLargeAmount19Time.Text = Strings.Format(curLTTLargeAmountTime[19], "#,##0.00");
			fldLTTLargeAmount18Full.Text = Strings.Format(curLTTLargeAmount[18], "#,##0.00");
			fldLTTLargeAmount18Time.Text = Strings.Format(curLTTLargeAmountTime[18], "#,##0.00");
			fldLTTLargeAmount17Full.Text = Strings.Format(curLTTLargeAmount[17], "#,##0.00");
			fldLTTLargeAmount17Time.Text = Strings.Format(curLTTLargeAmountTime[17], "#,##0.00");
			fldLTTLargeAmount16Full.Text = Strings.Format(curLTTLargeAmount[16], "#,##0.00");
			fldLTTLargeAmount16Time.Text = Strings.Format(curLTTLargeAmountTime[16], "#,##0.00");
			fldLTTLargeAmount15Full.Text = Strings.Format(curLTTLargeAmount[15], "#,##0.00");
			fldLTTLargeAmount15Time.Text = Strings.Format(curLTTLargeAmountTime[15], "#,##0.00");
			fldLTTLargeAmount14Full.Text = Strings.Format(curLTTLargeAmount[14], "#,##0.00");
			fldLTTLargeAmount14Time.Text = Strings.Format(curLTTLargeAmountTime[14], "#,##0.00");
			fldLTTLargeAmount13Full.Text = Strings.Format(curLTTLargeAmount[13], "#,##0.00");
			fldLTTLargeAmount13Time.Text = Strings.Format(curLTTLargeAmountTime[13], "#,##0.00");
			fldLTTLargeAmount12Full.Text = Strings.Format(curLTTLargeAmount[12], "#,##0.00");
			fldLTTLargeAmount12Time.Text = Strings.Format(curLTTLargeAmountTime[12], "#,##0.00");
			fldLTTSmallAmount12.Text = Strings.Format(curLTTSmallAmount[12], "#,##0.00");
			fldLTTLargeAmount11Full.Text = Strings.Format(curLTTLargeAmount[11], "#,##0.00");
			fldLTTLargeAmount11Time.Text = Strings.Format(curLTTLargeAmountTime[11], "#,##0.00");
			fldLTTSmallAmount11.Text = Strings.Format(curLTTSmallAmount[11], "#,##0.00");
			fldLTTLargeAmount10Full.Text = Strings.Format(curLTTLargeAmount[10], "#,##0.00");
			fldLTTLargeAmount10Time.Text = Strings.Format(curLTTLargeAmountTime[10], "#,##0.00");
			fldLTTSmallAmount10.Text = Strings.Format(curLTTSmallAmount[10], "#,##0.00");
			fldLTTLargeAmount9Full.Text = Strings.Format(curLTTLargeAmount[9], "#,##0.00");
			fldLTTLargeAmount9Time.Text = Strings.Format(curLTTLargeAmountTime[9], "#,##0.00");
			fldLTTSmallAmount9.Text = Strings.Format(curLTTSmallAmount[9], "#,##0.00");
			fldLTTLargeAmount8Full.Text = Strings.Format(curLTTLargeAmount[8], "#,##0.00");
			fldLTTLargeAmount8Time.Text = Strings.Format(curLTTLargeAmountTime[8], "#,##0.00");
			fldLTTSmallAmount8.Text = Strings.Format(curLTTSmallAmount[8], "#,##0.00");
			fldLTTLargeAmount7.Text = Strings.Format(curLTTLargeAmount[7], "#,##0.00");
			fldLTTSmallAmount7.Text = Strings.Format(curLTTSmallAmount[7], "#,##0.00");
			fldLTTLargeAmount6.Text = Strings.Format(curLTTLargeAmount[6], "#,##0.00");
			fldLTTSmallAmount6.Text = Strings.Format(curLTTSmallAmount[6], "#,##0.00");
			fldLTTLargeAmount5.Text = Strings.Format(curLTTLargeAmount[5], "#,##0.00");
			fldLTTSmallAmount5.Text = Strings.Format(curLTTSmallAmount[5], "#,##0.00");
			fldLTTLargeAmount4.Text = Strings.Format(curLTTLargeAmount[4], "#,##0.00");
			fldLTTSmallAmount4.Text = Strings.Format(curLTTSmallAmount[4], "#,##0.00");
			fldLTTLargeAmount3.Text = Strings.Format(curLTTLargeAmount[3], "#,##0.00");
			fldLTTSmallAmount3.Text = Strings.Format(curLTTSmallAmount[3], "#,##0.00");
			fldLTTLargeAmount2.Text = Strings.Format(curLTTLargeAmount[2], "#,##0.00");
			fldLTTSmallAmount2.Text = Strings.Format(curLTTSmallAmount[2], "#,##0.00");
			fldLTTSmallAmountSubTotal.Text = Strings.Format(curLTTSmallAmountTotal, "#,##0.00");
			fldLTTLargeAmountSubTotal.Text = Strings.Format(curLTTLargeAmountTotal, "#,##0.00");
			fldLTTAmountTotal.Text = Strings.Format(curLTTSmallAmountTotal + curLTTLargeAmountTotal, "#,##0.00");
			fldExtensionAmount.Text = Strings.Format(curExtensionAmount, "#,##0.00");
			fldTransfer25YearAmount.Text = Strings.Format(curTransfer25YearAmount, "#,##0.00");
			fldTransferLargeAmount.Text = Strings.Format(curTransferLargeAmount, "#,##0.00");
			fldTransferSmallAmount.Text = Strings.Format(curTransferSmallAmount, "#,##0.00");
			fldLostPlateAmount.Text = Strings.Format(curLostPlateAmount, "#,##0.00");
			fldDuplicateAmount.Text = Strings.Format(curDuplicateAmount, "#,##0.00");
			fldCorrectionAmount.Text = Strings.Format(curCorrectionAmount, "#,##0.00");
			fldTitle25YearsAmount.Text = Strings.Format(curTitle25YearAmount, "#,##0.00");
			fldTitleAmount.Text = Strings.Format(curTitleAmount, "#,##0.00");
			fldRushTitleAmount.Text = Strings.Format(curRushTitleAmount, "#,##0.00");
			fldSalesTaxPaidAmount.Text = Strings.Format(curSalesTaxPaidAmount, "#,##0.00");
			fldSalesTaxNoFeeAmount.Text = Strings.Format(curSalesTaxNoFeeAmount, "#,##0.00");
			fldTotalAmount.Text = Strings.Format(curTotalAmount, "#,##0.00");
			fldLTTLargeUnits25Full.Text = Strings.Format(lngLTTLargeUnits[25], "#,##0");
			fldLTTLargeUnits24Full.Text = Strings.Format(lngLTTLargeUnits[24], "#,##0");
			fldLTTLargeUnits24Time.Text = Strings.Format(lngLTTLargeUnitsTime[24], "#,##0");
			fldLTTLargeUnits23Full.Text = Strings.Format(lngLTTLargeUnits[23], "#,##0");
			fldLTTLargeUnits23Time.Text = Strings.Format(lngLTTLargeUnitsTime[23], "#,##0");
			fldLTTLargeUnits22Full.Text = Strings.Format(lngLTTLargeUnits[22], "#,##0");
			fldLTTLargeUnits22Time.Text = Strings.Format(lngLTTLargeUnitsTime[22], "#,##0");
			fldLTTLargeUnits21Full.Text = Strings.Format(lngLTTLargeUnits[21], "#,##0");
			fldLTTLargeUnits21Time.Text = Strings.Format(lngLTTLargeUnitsTime[21], "#,##0");
			fldLTTLargeUnits20Full.Text = Strings.Format(lngLTTLargeUnits[20], "#,##0");
			fldLTTLargeUnits20Time.Text = Strings.Format(lngLTTLargeUnitsTime[20], "#,##0");
			fldLTTLargeUnits19Full.Text = Strings.Format(lngLTTLargeUnits[19], "#,##0");
			fldLTTLargeUnits19Time.Text = Strings.Format(lngLTTLargeUnitsTime[19], "#,##0");
			fldLTTLargeUnits18Full.Text = Strings.Format(lngLTTLargeUnits[18], "#,##0");
			fldLTTLargeUnits18Time.Text = Strings.Format(lngLTTLargeUnitsTime[18], "#,##0");
			fldLTTLargeUnits17Full.Text = Strings.Format(lngLTTLargeUnits[17], "#,##0");
			fldLTTLargeUnits17Time.Text = Strings.Format(lngLTTLargeUnitsTime[17], "#,##0");
			fldLTTLargeUnits16Full.Text = Strings.Format(lngLTTLargeUnits[16], "#,##0");
			fldLTTLargeUnits16Time.Text = Strings.Format(lngLTTLargeUnitsTime[16], "#,##0");
			fldLTTLargeUnits15Full.Text = Strings.Format(lngLTTLargeUnits[15], "#,##0");
			fldLTTLargeUnits15Time.Text = Strings.Format(lngLTTLargeUnitsTime[15], "#,##0");
			fldLTTLargeUnits14Full.Text = Strings.Format(lngLTTLargeUnits[14], "#,##0");
			fldLTTLargeUnits14Time.Text = Strings.Format(lngLTTLargeUnitsTime[14], "#,##0");
			fldLTTLargeUnits13Full.Text = Strings.Format(lngLTTLargeUnits[13], "#,##0");
			fldLTTLargeUnits13Time.Text = Strings.Format(lngLTTLargeUnitsTime[13], "#,##0");
			fldLTTLargeUnits12Full.Text = Strings.Format(lngLTTLargeUnits[12], "#,##0");
			fldLTTLargeUnits12Time.Text = Strings.Format(lngLTTLargeUnitsTime[12], "#,##0");
			fldLTTSmallUnits12.Text = Strings.Format(lngLTTSmallUnits[12], "#,##0");
			fldLTTLargeUnits11Full.Text = Strings.Format(lngLTTLargeUnits[11], "#,##0");
			fldLTTLargeUnits11Time.Text = Strings.Format(lngLTTLargeUnitsTime[11], "#,##0");
			fldLTTSmallUnits11.Text = Strings.Format(lngLTTSmallUnits[11], "#,##0");
			fldLTTLargeUnits10Full.Text = Strings.Format(lngLTTLargeUnits[10], "#,##0");
			fldLTTLargeUnits10Time.Text = Strings.Format(lngLTTLargeUnitsTime[10], "#,##0");
			fldLTTSmallUnits10.Text = Strings.Format(lngLTTSmallUnits[10], "#,##0");
			fldLTTLargeUnits9Full.Text = Strings.Format(lngLTTLargeUnits[9], "#,##0");
			fldLTTLargeUnits9Time.Text = Strings.Format(lngLTTLargeUnitsTime[9], "#,##0");
			fldLTTSmallUnits9.Text = Strings.Format(lngLTTSmallUnits[9], "#,##0");
			fldLTTLargeUnits8Full.Text = Strings.Format(lngLTTLargeUnits[8], "#,##0");
			fldLTTLargeUnits8Time.Text = Strings.Format(lngLTTLargeUnitsTime[8], "#,##0");
			fldLTTSmallUnits8.Text = Strings.Format(lngLTTSmallUnits[8], "#,##0");
			fldLTTLargeUnits7.Text = Strings.Format(lngLTTLargeUnits[7], "#,##0");
			fldLTTSmallUnits7.Text = Strings.Format(lngLTTSmallUnits[7], "#,##0");
			fldLTTLargeUnits6.Text = Strings.Format(lngLTTLargeUnits[6], "#,##0");
			fldLTTSmallUnits6.Text = Strings.Format(lngLTTSmallUnits[6], "#,##0");
			fldLTTLargeUnits5.Text = Strings.Format(lngLTTLargeUnits[5], "#,##0");
			fldLTTSmallUnits5.Text = Strings.Format(lngLTTSmallUnits[5], "#,##0");
			fldLTTLargeUnits4.Text = Strings.Format(lngLTTLargeUnits[4], "#,##0");
			fldLTTSmallUnits4.Text = Strings.Format(lngLTTSmallUnits[4], "#,##0");
			fldLTTLargeUnits3.Text = Strings.Format(lngLTTLargeUnits[3], "#,##0");
			fldLTTSmallUnits3.Text = Strings.Format(lngLTTSmallUnits[3], "#,##0");
			fldLTTLargeUnits2.Text = Strings.Format(lngLTTLargeUnits[2], "#,##0");
			fldLTTSmallUnits2.Text = Strings.Format(lngLTTSmallUnits[2], "#,##0");
			fldLTTSmallUnitsSubTotal.Text = Strings.Format(lngLTTSmallUnitsTotal, "#,##0");
			fldLTTLargeUnitsSubTotal.Text = Strings.Format(lngLTTLargeUnitsTotal, "#,##0");
			fldLTTUnitsTotal.Text = Strings.Format(lngLTTSmallUnitsTotal + lngLTTLargeUnitsTotal, "#,##0");
			fldExtensionsUnits.Text = Strings.Format(lngExtensionUnits, "#,##0");
			fldTransfers25YearUnits.Text = Strings.Format(lngTransfer25YearUnits, "#,##0");
			fldTransfersLargeUnits.Text = Strings.Format(lngTransferLargeUnits, "#,##0");
			fldTransfersSmallUnits.Text = Strings.Format(lngTransferSmallUnits, "#,##0");
			fldLostPlateUnits.Text = Strings.Format(lngLostPlateUnits, "#,##0");
			fldDuplicateUnits.Text = Strings.Format(lngDuplicateUnits, "#,##0");
			fldCorrectionUnits.Text = Strings.Format(lngCorrectionUnits, "#,##0");
			fldTitle25YearUnits.Text = Strings.Format(lngTitle25YearUnits, "#,##0");
			fldTitleUnits.Text = Strings.Format(lngTitleUnits, "#,##0");
			fldRushTitleUnits.Text = Strings.Format(lngRushTitleUnits, "#,##0");
			fldSalesTaxPaidUnits.Text = Strings.Format(lngSalesTaxPaidUnits, "#,##0");
			fldSalesTaxNoFeeUnits.Text = Strings.Format(lngSalesTaxNoFeeUnits, "#,##0");
			fldTotalUnits.Text = Strings.Format(lngTotalUnits, "#,##0");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void rptLTTAgentDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLTTAgentDetail properties;
			//rptLTTAgentDetail.Caption	= "Agent Detail";
			//rptLTTAgentDetail.Left	= 0;
			//rptLTTAgentDetail.Top	= 0;
			//rptLTTAgentDetail.Width	= 17160;
			//rptLTTAgentDetail.Height	= 11115;
			//rptLTTAgentDetail.StartUpPosition	= 3;
			//rptLTTAgentDetail.SectionData	= "rptLTTAgentDetail.dsx":0000;
			//End Unmaped Properties
		}
	}
}
