//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmSpecialPermit : BaseForm
	{
		public frmSpecialPermit()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSpecialPermit InstancePtr
		{
			get
			{
				return (frmSpecialPermit)Sys.GetInstance(typeof(frmSpecialPermit));
			}
		}

		protected frmSpecialPermit _InstancePtr = null;
		//=========================================================
		bool BadVIN;
        private bool isBlankAddress = true;
		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private bool VerifyResCode()
		{
			bool VerifyResCode = false;
			VerifyResCode = true;
			string rescode = "";
			clsDRWrapper rsResCode = new clsDRWrapper();
			// 
			if (Conversion.Val(txtResidenceCode.Text) != 0 && Conversion.Val(txtResidenceCode.Text) != 99999)
			{
				rescode = fecherFoundation.Strings.Trim(txtResidenceCode.Text);
				MotorVehicle.Statics.strSql = "SELECT * FROM Residence";
				rsResCode.OpenRecordset(MotorVehicle.Statics.strSql);
				if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
				{
					rsResCode.MoveLast();
					rsResCode.MoveFirst();
					if (rsResCode.FindFirstRecord("Code", rescode))
					{
						// do nothing
					}
					else
					{
						MessageBox.Show(" The residence code is not in the BMV table." + "\r\n" + "Please input a valid Residence Code.", "Residence Code Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtResidenceCode.Text = "00000";
						VerifyResCode = false;
						txtResidenceCode.TabStop = true;
					}
				}
				else
				{
					MessageBox.Show("There are no entries in the BMV residence code table.", "Empty Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					VerifyResCode = false;
				}
			}
			else
			{
				MessageBox.Show("Residence code is required", "Residence Code Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				VerifyResCode = false;
			}
			return VerifyResCode;
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string strApplicant;
			if (Conversion.Val(fecherFoundation.Strings.Trim(txtCustomerNumber.Text)) == 0)
			{
				MessageBox.Show("You must fill in the Applicant Information before you can save.", "Invalid Applicant", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

            if (isBlankAddress)
            {
                MessageBox.Show("You must fill in the address before you can save.", "Invalid Address",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
			if (txtYear.Text == "")
			{
				MessageBox.Show("You must fill in the Year of the Vehicle before you can save.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtMake.Text == "")
			{
				MessageBox.Show("You must fill in the Make of the Vehicle before you can save.", "Invalid Make", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtColor.Text == "")
			{
				MessageBox.Show("You must fill in the Color of the Vehicle before you can save.", "Invalid Color", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtVIN.Text == "")
			{
				MessageBox.Show("You must fill in the VIN # of the Vehicle before you can save.", "Invalid VIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtPermit.Text == "")
			{
				MessageBox.Show("You must fill in the Permit Number before you can save.", "Invalid Permit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!VerifyResCode())
			{
				return;
			}
			strApplicant = MotorVehicle.GetPartyNameMiddleInitial(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtCustomerNumber.Text)))));
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				MotorVehicle.Write_PDS_Work_Record_Stuff();
			}
			rs.OpenRecordset("SELECT * FROM SpecialRegistration");
			rs.AddNew();
			rs.Set_Fields("PermitNumber", txtPermit.Text);
			rs.Set_Fields("EffectiveDate", txtEffectiveDate.Text);
			rs.Set_Fields("ExpireDate", txtExpireDate.Text);
			rs.Set_Fields("PartyID", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtCustomerNumber.Text))));
			rs.Set_Fields("ResidenceCode", txtResidenceCode.Text);
			rs.Set_Fields("make", txtMake.Text);
			rs.Set_Fields("Year", txtYear.Text);
			rs.Set_Fields("Color", txtColor.Text);
			rs.Set_Fields("Color2", txtColor2.Text);
			rs.Set_Fields("Vin", txtVIN.Text);
			if (BadVIN)
			{
				rs.Set_Fields("BadVIN", true);
			}
			else
			{
				rs.Set_Fields("BadVIN", false);
			}
			rs.Set_Fields("DateUpdated", DateTime.Now);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (MotorVehicle.Statics.PlateType == 1)
			{
				// Registration
				rs.Set_Fields("StatePaid", rsDefaultInfo.Get_Fields_Decimal("SpecialRegistrationFee"));
				rs.Set_Fields("LocalPaid", rsDefaultInfo.Get_Fields_Decimal("AgentFeeSpecialRegPermit"));
				rs.Set_Fields("Transfer", false);
				rs.Set_Fields("Duplicate", false);
			}
			else if (MotorVehicle.Statics.PlateType == 2)
			{
				// Transfer
				rs.Set_Fields("StatePaid", 8);
				rs.Set_Fields("LocalPaid", rsDefaultInfo.Get_Fields_Decimal("AgentFeeSpecialRegPermit"));
				rs.Set_Fields("Transfer", true);
				rs.Set_Fields("Duplicate", false);
			}
			else
			{
				// Duplicate
				rs.Set_Fields("StatePaid", 2);
				rs.Set_Fields("LocalPaid", 0);
				rs.Set_Fields("Transfer", false);
				rs.Set_Fields("Duplicate", true);
			}
			rs.Update();
			rs.OpenRecordset("SELECT * FROM ExceptionReport");
			rs.AddNew();
			rs.Set_Fields("Type", "6PM");
			rs.Set_Fields("ExceptionReportDate", DateTime.Today);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			if (MotorVehicle.Statics.PlateType == 1)
			{
				// Registration
				rs.Set_Fields("Fee", rsDefaultInfo.Get_Fields_Decimal("SpecialRegistrationFee"));
			}
			else if (MotorVehicle.Statics.PlateType == 2)
			{
				// Transfer
				rs.Set_Fields("Fee", 8);
			}
			else
			{
				// Duplicate
				rs.Set_Fields("Fee", 2);
			}
			rs.Set_Fields("PermitNumber", fecherFoundation.Strings.Trim(txtPermit.Text));
			rs.Set_Fields("PermitType", "MVR10");
			rs.Set_Fields("ApplicantName", strApplicant);
			rs.Set_Fields("MVR3Printed", 0);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			rs.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'RXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				rs.Edit();
				rs.Set_Fields("Status", "I");
				rs.Set_Fields("PeriodCloseoutID", 0);
				rs.Set_Fields("TellerCloseoutID", 0);
				rs.Update();
			}
			rs.OpenRecordset("SELECT * FROM InventoryAdjustments");
			rs.AddNew();
			rs.Set_Fields("AdjustmentCode", "I");
			rs.Set_Fields("DateOfAdjustment", DateTime.Today);
			rs.Set_Fields("QuantityAdjusted", 1);
			rs.Set_Fields("InventoryType", "RXSXX");
			rs.Set_Fields("Low", txtPermit.Text);
			rs.Set_Fields("High", txtPermit.Text);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("reason", "Issued to " + strApplicant);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			Close();
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				//MDIParent.InstancePtr.GetMainMenu();
				MDIParent.InstancePtr.Menu18();
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdProcessSave, new System.EventArgs());
		}

		private void frmSpecialPermit_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			if (DateTime.Now.Month < 3)
			{
				txtExpireDate.Text = "03/01/" + FCConvert.ToString(DateTime.Now.Year);
			}
			else
			{
				txtExpireDate.Text = "03/01/" + FCConvert.ToString(DateTime.Now.Year + 1);
			}
			txtExpireDate.Enabled = false;
			txtEffectiveDate.Enabled = true;
			txtPermit.Focus();
			this.Refresh();
		}

		private void frmSpecialPermit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmSpecialPermit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSpecialPermit properties;
			//frmSpecialPermit.FillStyle	= 0;
			//frmSpecialPermit.ScaleWidth	= 9045;
			//frmSpecialPermit.ScaleHeight	= 7095;
			//frmSpecialPermit.LinkTopic	= "Form2";
			//frmSpecialPermit.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsDefaults = new clsDRWrapper();
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			if (!modRegionalTown.IsRegionalTown())
			{
				rsDefaults.OpenRecordset("SELECT * FROM DefaultInfo");
				if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
				{
					txtResidenceCode.Text = FCConvert.ToString(rsDefaults.Get_Fields_String("ResidenceCode"));
				}
			}
		}

		private void frmSpecialPermit_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
			//frmPlateInfo.InstancePtr.Show(App.MainForm);
			frmPlateInfo.InstancePtr.ShowForm();
		}

		private void frmSpecialPermit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void txtColor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rsColor = new clsDRWrapper();
			if (txtColor.Text != "")
			{
				rsColor.OpenRecordset("SELECT * FROM Color WHERE Code = '" + txtColor.Text + "'");
				if (rsColor.EndOfFile() != true && rsColor.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					e.Cancel = true;
					txtColor.Text = "";
					MessageBox.Show("This is not a valid Color code.", "Invalid Color", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void txtColor2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rsColor = new clsDRWrapper();
			if (txtColor2.Text != "")
			{
				rsColor.OpenRecordset("SELECT * FROM Color WHERE Code = '" + txtColor2.Text + "'");
				if (rsColor.EndOfFile() != true && rsColor.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					e.Cancel = true;
					txtColor2.Text = "";
					MessageBox.Show("This is not a valid Color code.", "Invalid Color", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void txtEffectiveDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsDate(txtEffectiveDate.Text))
			{
				MessageBox.Show("You must input a valid date before you may proceed.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				return;
			}
			if (fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text).Year != DateTime.Now.Year)
			{
				MessageBox.Show("You may only set the date to a date this year.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				txtEffectiveDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				return;
			}
			if (fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text).Month < 3)
			{
				txtExpireDate.Text = "03/01/" + FCConvert.ToString(DateTime.Now.Year);
			}
			else
			{
				txtExpireDate.Text = "03/01/" + FCConvert.ToString(DateTime.Now.Year + 1);
			}
		}

		private void txtMake_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rsMake = new clsDRWrapper();
			if (txtMake.Text != "")
			{
				rsMake.OpenRecordset("SELECT * FROM Make WHERE Code = '" + txtMake.Text + "'");
				if (rsMake.EndOfFile() != true && rsMake.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					e.Cancel = true;
					txtMake.Text = "";
					MessageBox.Show("This is not a valid Make code.", "Invalid Make", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void txtPermit_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper tempRS = new clsDRWrapper();
			if (txtPermit.Text != "")
			{
				if (MotorVehicle.Statics.PlateGroup == 0)
				{
					tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'RXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)) + " AND Status = 'A'");
				}
				else
				{
					tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'RXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.PlateGroup) + " OR [Group] = 0)");
				}
				if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
				{
					return;
				}
				else
				{
					MessageBox.Show("This permit number was not found in Inventory.", "Invalid Permit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtPermit.Text = "";
				}
			}
		}

		private void txtVIN_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			if (txtVIN.Text != "")
			{
				if (CheckDigit(txtVIN.Text) == false)
				{
					answer = MessageBox.Show("This is an invalid VIN.  Do you wish to use it anyways?", "Use this VIN?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer == DialogResult.Yes)
					{
						return;
					}
					else
					{
						e.Cancel = true;
						txtVIN.Text = "";
					}
				}
			}
		}

		private void txtYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// kk08032016 tromv-1171  Don't validate the year, not sure where 80 years old came from
			// If txtYear <> "" Then
			// If txtYear.Text < Year(Now) - 80 Or txtYear.Text > Year(Now) + 1 Then
			// Cancel = True
			// txtYear.Text = ""
			// MsgBox "The year you have entered is incorrect.", vbExclamation, "Invalid Year"
			// End If
			// End If
		}

		public bool CheckDigit(string hold)
		{
			bool CheckDigit = false;
			// vbPorter upgrade warning: cd As int	OnWriteFCConvert.ToInt32(
			int[] cd = new int[17 + 1];
			int fnx;
			string OneDigit = "";
			float cdvalue;
			float cdOperand;
			float cdOperand2;
			CheckDigit = false;
			for (fnx = 1; fnx <= 17; fnx++)
			{
				OneDigit = Strings.Mid(hold, fnx, 1);
				if (string.Compare(OneDigit, "0") < 0)
					return CheckDigit;
				if (string.Compare(OneDigit, "9") > 0)
				{
					if (string.Compare(OneDigit, "A") < 0)
						return CheckDigit;
					if (string.Compare(OneDigit, "Z") > 0)
						return CheckDigit;
					if (string.Compare(OneDigit, "I") < 0)
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 64);
					}
					else if (string.Compare(OneDigit, "S") < 0)
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 73);
					}
					else
					{
						cd[fnx] = (Convert.ToByte(OneDigit[0]) - 81);
					}
				}
				else
				{
					cd[fnx] = FCConvert.ToInt32(Math.Round(Conversion.Val(OneDigit)));
				}
			}
			// fnx
			cdvalue = cd[1] * 8;
			cdvalue += (cd[2] * 7);
			cdvalue += (cd[3] * 6);
			cdvalue += (cd[4] * 5);
			cdvalue += (cd[5] * 4);
			cdvalue += (cd[6] * 3);
			cdvalue += (cd[7] * 2);
			cdvalue += (cd[8] * 10);
			cdvalue += (cd[10] * 9);
			cdvalue += (cd[11] * 8);
			cdvalue += (cd[12] * 7);
			cdvalue += (cd[13] * 6);
			cdvalue += (cd[14] * 5);
			cdvalue += (cd[15] * 4);
			cdvalue += (cd[16] * 3);
			cdvalue += (cd[17] * 2);
			if (cd[9] == FCUtils.iMod(cdvalue, 11))
				CheckDigit = true;
			if (FCUtils.iMod(cdvalue, 11) == 10 && Strings.Mid(hold, 9, 1) == "X")
				CheckDigit = true;
			return CheckDigit;
		}

		private void SetCustomFormColors()
		{
			//fraApplicant.BackColor = ColorTranslator.FromOle(0x808080);
			//fraVehicle.BackColor = ColorTranslator.FromOle(0x808080);
			//lblCustomerNumber.BackColor = ColorTranslator.FromOle(0x808080);
			//Label3.BackColor = ColorTranslator.FromOle(0x808080);
			//lblCustomerInfo.BackColor = ColorTranslator.FromOle(0x808080);
			//lblYear.BackColor = ColorTranslator.FromOle(0x808080);
			//lblMake.BackColor = ColorTranslator.FromOle(0x808080);
			//lblColor.BackColor = ColorTranslator.FromOle(0x808080);
			//lblVin.BackColor = ColorTranslator.FromOle(0x808080);
		}

		private void txtCustomerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCustomerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtCustomerNumber.Text) && Conversion.Val(txtCustomerNumber.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt16(FCConvert.ToDouble(txtCustomerNumber.Text)));
			}
			else
			{
				ClearPartyInfo();
			}
		}

		public void ClearPartyInfo()
		{
			lblCustomerInfo.Text = "";
		}

		public void GetPartyInfo(int intPartyID)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			FCCollection pComments = new FCCollection();
            string strAddress = "";

			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				lblCustomerInfo.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
				pAdd = pInfo.GetAddress("AR", intPartyID);
                if (pAdd == null)
                {
                    MessageBox.Show(
                        @"This customer's address is missing. Please update their address before continuing.",
                        "Missing Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    strAddress = pAdd.GetFormattedAddress();
                    if (strAddress == "")
                    {
                        MessageBox.Show(
                            @"This customer's address is missing. Please update their address before continuing.",
                            "Missing Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        lblCustomerInfo.Text = lblCustomerInfo.Text + "\r\n" + pAdd.GetFormattedAddress();
                        isBlankAddress = false;
                        return;
                    }
                }
				
			}

            isBlankAddress = true;
        }

		private void cmdEdit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtCustomerNumber.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtCustomerNumber.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID);
			}
		}
	}
}
