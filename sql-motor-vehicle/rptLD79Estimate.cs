﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLD79Estimate.
	/// </summary>
	public partial class rptLD79Estimate : BaseSectionReport
	{
		public rptLD79Estimate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Excise Rate Comparison";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLD79Estimate InstancePtr
		{
			get
			{
				return (rptLD79Estimate)Sys.GetInstance(typeof(rptLD79Estimate));
			}
		}

		protected rptLD79Estimate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLD79Estimate	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		// vbPorter upgrade warning: datLowDate As DateTime	OnWrite(string)
		DateTime datLowDate;
		// vbPorter upgrade warning: datHighDate As DateTime	OnWrite(string)
		DateTime datHighDate;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label11 As object	OnWrite(string)
			// vbPorter upgrade warning: Label8 As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label11.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label8.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			datLowDate = FCConvert.ToDateTime(frmLD79Estimate.InstancePtr.txtStartDate.Text);
			datHighDate = FCConvert.ToDateTime(frmLD79Estimate.InstancePtr.txtEndDate.Text);
			lblHeading.Text = "The excise tax estimates are based on New Registrations, Re Registrations, New Reg Transfers, and Re Reg Transfers completed between the dates of " + Strings.Format(datLowDate, "MM/dd/yyyy") + " and " + Strings.Format(datHighDate, "MM/dd/yyyy") + ".";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldRate1 As object	OnWrite(string)
			// vbPorter upgrade warning: fldRate2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldRate3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldRate4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldRate5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldRate6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalExcise As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalCount As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear1 As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear1Count As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear1Proposed As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear2 As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear2Count As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear2Proposed As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear3Count As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear3Proposed As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear4 As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear4Count As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear4Proposed As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear5 As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear5Count As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear5Proposed As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear6 As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear6Count As object	OnWrite(string)
			// vbPorter upgrade warning: fldMilYear6Proposed As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalExciseProposed As object	OnWrite(string)
			clsDRWrapper rsInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(int, Decimal)
			Decimal curTotal;
			// vbPorter upgrade warning: curTotalProposed As Decimal	OnWrite(int, Decimal)
			Decimal curTotalProposed;
			// vbPorter upgrade warning: lngCount As int	OnWrite(int, double)
			int lngCount;
			curTotalProposed = 0;
			curTotal = 0;
			lngCount = 0;
			fldRate1.Text = frmLD79Estimate.InstancePtr.txtProposedYear[0];
			fldRate2.Text = frmLD79Estimate.InstancePtr.txtProposedYear[1];
			fldRate3.Text = frmLD79Estimate.InstancePtr.txtProposedYear[2];
			fldRate4.Text = frmLD79Estimate.InstancePtr.txtProposedYear[3];
			fldRate5.Text = frmLD79Estimate.InstancePtr.txtProposedYear[4];
			fldRate6.Text = frmLD79Estimate.InstancePtr.txtProposedYear[5];
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged) as TotalExcise FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged - ExciseCreditUsed) as TotalExcise FROM ActivityMaster WHERE (ExciseTaxCharged - ExciseCreditUsed > 0) AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			fldTotalExcise.Text = Strings.Format(curTotal, "#,##0.00");
			fldTotalCount.Text = Strings.Format(lngCount, "#,##0");
			curTotal = 0;
			lngCount = 0;
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged) as TotalExcise FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 1 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged - ExciseCreditUsed) as TotalExcise FROM ActivityMaster WHERE (ExciseTaxCharged - ExciseCreditUsed > 0) AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 1 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			fldMilYear1.Text = Strings.Format(curTotal, "#,##0.00");
			fldMilYear1Count.Text = Strings.Format(lngCount, "#,##0");
			curTotal = 0;
			rsInfo.OpenRecordset("SELECT SUM(BasePrice) as TotalBasePrice FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 1 AND ExciseTaxCharged > 0 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalBasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[0].Text));
			}
			rsInfo.OpenRecordset("SELECT * FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 1 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					if (FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[0].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull")) > 0)
					{
						curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[0].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull"));
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			fldMilYear1Proposed.Text = Strings.Format(curTotal, "#,##0.00");
			curTotalProposed += curTotal;
			curTotal = 0;
			lngCount = 0;
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged) as TotalExcise FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 2 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged - ExciseCreditUsed) as TotalExcise FROM ActivityMaster WHERE (ExciseTaxCharged - ExciseCreditUsed > 0) AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 2 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			fldMilYear2.Text = Strings.Format(curTotal, "#,##0.00");
			fldMilYear2Count.Text = Strings.Format(lngCount, "#,##0");
			curTotal = 0;
			rsInfo.OpenRecordset("SELECT SUM(BasePrice) as TotalBasePrice FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 2 AND ExciseTaxCharged > 0 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalBasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[1].Text));
			}
			rsInfo.OpenRecordset("SELECT * FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 2 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					if (FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[1].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull")) > 0)
					{
						curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[1].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull"));
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			fldMilYear2Proposed.Text = Strings.Format(curTotal, "#,##0.00");
			curTotalProposed += curTotal;
			curTotal = 0;
			lngCount = 0;
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged) as TotalExcise FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 3 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged - ExciseCreditUsed) as TotalExcise FROM ActivityMaster WHERE (ExciseTaxCharged - ExciseCreditUsed > 0) AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 3 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			fldMilYear3.Text = Strings.Format(curTotal, "#,##0.00");
			fldMilYear3Count.Text = Strings.Format(lngCount, "#,##0");
			curTotal = 0;
			rsInfo.OpenRecordset("SELECT SUM(BasePrice) as TotalBasePrice FROM ActivityMaster WHERE Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 3 AND ExciseTaxCharged > 0 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalBasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[2].Text));
			}
			rsInfo.OpenRecordset("SELECT * FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 3 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					if (FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[2].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull")) > 0)
					{
						curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[2].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull"));
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			fldMilYear3Proposed.Text = Strings.Format(curTotal, "#,##0.00");
			curTotalProposed += curTotal;
			curTotal = 0;
			lngCount = 0;
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged) as TotalExcise FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 4 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged - ExciseCreditUsed) as TotalExcise FROM ActivityMaster WHERE (ExciseTaxCharged - ExciseCreditUsed > 0) AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 4 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			fldMilYear4.Text = Strings.Format(curTotal, "#,##0.00");
			fldMilYear4Count.Text = Strings.Format(lngCount, "#,##0");
			curTotal = 0;
			rsInfo.OpenRecordset("SELECT SUM(BasePrice) as TotalBasePrice FROM ActivityMaster WHERE Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 4 AND ExciseTaxCharged > 0 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalBasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[3].Text));
			}
			rsInfo.OpenRecordset("SELECT * FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 4 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					if (FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[3].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull")) > 0)
					{
						curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[3].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull"));
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			fldMilYear4Proposed.Text = Strings.Format(curTotal, "#,##0.00");
			curTotalProposed += curTotal;
			curTotal = 0;
			lngCount = 0;
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged) as TotalExcise FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 5 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged - ExciseCreditUsed) as TotalExcise FROM ActivityMaster WHERE (ExciseTaxCharged - ExciseCreditUsed > 0) AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 5 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			fldMilYear5.Text = Strings.Format(curTotal, "#,##0.00");
			fldMilYear5Count.Text = Strings.Format(lngCount, "#,##0");
			curTotal = 0;
			rsInfo.OpenRecordset("SELECT SUM(BasePrice) as TotalBasePrice FROM ActivityMaster WHERE Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 5 AND ExciseTaxCharged > 0 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalBasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[4].Text));
			}
			rsInfo.OpenRecordset("SELECT * FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 5 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					if (FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[4].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull")) > 0)
					{
						curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[4].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull"));
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			fldMilYear5Proposed.Text = Strings.Format(curTotal, "#,##0.00");
			curTotalProposed += curTotal;
			curTotal = 0;
			lngCount = 0;
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged) as TotalExcise FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 6 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			rsInfo.OpenRecordset("SELECT COUNT(ExciseTaxCharged) as VehicleCount, SUM(ExciseTaxCharged - ExciseCreditUsed) as TotalExcise FROM ActivityMaster WHERE (ExciseTaxCharged - ExciseCreditUsed > 0) AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 6 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += rsInfo.Get_Fields_Decimal("TotalExcise");
				lngCount += rsInfo.Get_Fields_Int32("VehicleCount");
			}
			fldMilYear6.Text = Strings.Format(curTotal, "#,##0.00");
			fldMilYear6Count.Text = Strings.Format(lngCount, "#,##0");
			curTotal = 0;
			rsInfo.OpenRecordset("SELECT SUM(BasePrice) as TotalBasePrice FROM ActivityMaster WHERE Status <> 'V' AND (TransactionType = 'NRR' OR TransactionType = 'RRR') AND MillYear = 6 AND ExciseTaxCharged > 0 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("TotalBasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[5].Text));
			}
			rsInfo.OpenRecordset("SELECT * FROM ActivityMaster WHERE ExciseTaxCharged > 0 AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND MillYear = 6 AND (DateUpdated >= '" + FCConvert.ToString(datLowDate) + "' AND DateUpdated <= '" + FCConvert.ToString(datHighDate) + "')");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					if (FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[5].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull")) > 0)
					{
						curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("BasePrice") * FCConvert.ToDecimal(frmLD79Estimate.InstancePtr.txtProposedYear[5].Text) - rsInfo.Get_Fields_Decimal("ExciseCreditFull"));
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			fldMilYear6Proposed.Text = Strings.Format(curTotal, "#,##0.00");
			curTotalProposed += curTotal;
			fldTotalExciseProposed.Text = Strings.Format(curTotalProposed, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			Label10.Text = "Page " + this.PageNumber;
		}

		private void rptLD79Estimate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLD79Estimate properties;
			//rptLD79Estimate.Caption	= "Excise Rate Comparison";
			//rptLD79Estimate.Icon	= "rptLD79Estimate.dsx":0000";
			//rptLD79Estimate.Left	= 0;
			//rptLD79Estimate.Top	= 0;
			//rptLD79Estimate.Width	= 11880;
			//rptLD79Estimate.Height	= 8595;
			//rptLD79Estimate.StartUpPosition	= 3;
			//rptLD79Estimate.SectionData	= "rptLD79Estimate.dsx":058A;
			//End Unmaped Properties
		}
	}
}
