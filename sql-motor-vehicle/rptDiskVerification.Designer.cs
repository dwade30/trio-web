﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptDiskVerification.
	/// </summary>
	partial class rptDiskVerification
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDiskVerification));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtCL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTXRCPT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStkr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtNoInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTXRCPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStkr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCL,
            this.txtPlate,
            this.txtTXRCPT,
            this.txtFee,
            this.txtOwner,
            this.txtYear,
            this.txtStkr,
            this.SubReport1});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            // 
            // txtCL
            // 
            this.txtCL.Height = 0.1875F;
            this.txtCL.Left = 0.125F;
            this.txtCL.Name = "txtCL";
            this.txtCL.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.txtCL.Text = "Field1";
            this.txtCL.Top = 0F;
            this.txtCL.Width = 0.3125F;
            // 
            // txtPlate
            // 
            this.txtPlate.Height = 0.1875F;
            this.txtPlate.Left = 0.375F;
            this.txtPlate.Name = "txtPlate";
            this.txtPlate.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.txtPlate.Text = "Field1";
            this.txtPlate.Top = 0F;
            this.txtPlate.Width = 0.819F;
            // 
            // txtTXRCPT
            // 
            this.txtTXRCPT.Height = 0.1875F;
            this.txtTXRCPT.Left = 1.194F;
            this.txtTXRCPT.Name = "txtTXRCPT";
            this.txtTXRCPT.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.txtTXRCPT.Text = "Field1";
            this.txtTXRCPT.Top = 0F;
            this.txtTXRCPT.Width = 0.71875F;
            // 
            // txtFee
            // 
            this.txtFee.Height = 0.1875F;
            this.txtFee.Left = 1.913F;
            this.txtFee.Name = "txtFee";
            this.txtFee.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.txtFee.Text = "Field1";
            this.txtFee.Top = 0F;
            this.txtFee.Width = 0.75F;
            // 
            // txtOwner
            // 
            this.txtOwner.Height = 0.1875F;
            this.txtOwner.Left = 2.663F;
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.txtOwner.Text = "Field1";
            this.txtOwner.Top = 0F;
            this.txtOwner.Width = 2.9F;
            // 
            // txtYear
            // 
            this.txtYear.Height = 0.1875F;
            this.txtYear.Left = 5.563F;
            this.txtYear.Name = "txtYear";
            this.txtYear.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.txtYear.Text = "Field1";
            this.txtYear.Top = 0F;
            this.txtYear.Width = 0.375F;
            // 
            // txtStkr
            // 
            this.txtStkr.Height = 0.1875F;
            this.txtStkr.Left = 5.938F;
            this.txtStkr.Name = "txtStkr";
            this.txtStkr.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.txtStkr.Text = "Field1";
            this.txtStkr.Top = 0F;
            this.txtStkr.Width = 1F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1875F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0F;
            this.SubReport1.Width = 7.4375F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtNoInfo});
            this.ReportFooter.Height = 0.375F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // txtNoInfo
            // 
            this.txtNoInfo.CanShrink = true;
            this.txtNoInfo.Height = 0.3125F;
            this.txtNoInfo.Left = 2.1875F;
            this.txtNoInfo.Name = "txtNoInfo";
            this.txtNoInfo.Style = "font-family: \'Roman 10cpi\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
            this.txtNoInfo.Text = null;
            this.txtNoInfo.Top = 0.0625F;
            this.txtNoInfo.Width = 2.6875F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.CanShrink = true;
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport2,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Line1,
            this.Label1,
            this.lblPage,
            this.Label33});
            this.GroupHeader1.Height = 1.78125F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.9375F;
            this.SubReport2.Left = 0F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 0.5F;
            this.SubReport2.Width = 7.5F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1875F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.125F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.Label12.Text = "CL";
            this.Label12.Top = 1.5625F;
            this.Label12.Width = 0.25F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1875F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0.375F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.Label13.Text = "PLATE";
            this.Label13.Top = 1.5625F;
            this.Label13.Width = 0.6875F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 1.163F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.Label14.Text = "TX RCPT";
            this.Label14.Top = 1.5625F;
            this.Label14.Width = 0.6875F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 2.163F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.Label15.Text = "FEE";
            this.Label15.Top = 1.5625F;
            this.Label15.Width = 0.375F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 2.663F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.Label16.Text = "OWNER";
            this.Label16.Top = 1.5625F;
            this.Label16.Width = 0.6875F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 5.5625F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.Label17.Text = "YR";
            this.Label17.Top = 1.5625F;
            this.Label17.Width = 0.3125F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 5.9375F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.Label18.Text = "STKR";
            this.Label18.Top = 1.5625F;
            this.Label18.Width = 0.6875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.1875F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.75F;
            this.Line1.Width = 6.6875F;
            this.Line1.X1 = 0.1875F;
            this.Line1.X2 = 6.875F;
            this.Line1.Y1 = 1.75F;
            this.Line1.Y2 = 1.75F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.4375F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
            this.Label1.Text = "**** FILE VERIFICATION REPORT ****";
            this.Label1.Top = 0F;
            this.Label1.Width = 4.375F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.0625F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
            this.lblPage.Text = "VENDOR ID#";
            this.lblPage.Top = 0.0625F;
            this.lblPage.Width = 0.9375F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.1875F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 2.5625F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
            this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
            this.Label33.Top = 0.1875F;
            this.Label33.Width = 2.3125F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // rptDiskVerification
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.75F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.75F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTXRCPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStkr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCL;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTXRCPT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStkr;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoInfo;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
