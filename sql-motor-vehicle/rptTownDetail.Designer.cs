﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTownDetail.
	/// </summary>
	partial class rptTownDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTownDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMain = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDollars = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEmpty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMain)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDollars)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMain,
				this.txtUnits,
				this.txtDollars,
				this.txtEmpty,
				this.txtStar
			});
			this.Detail.Height = 0.375F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label12,
				this.Label13,
				this.Label1,
				this.lblPage,
				this.Label33
			});
			this.GroupHeader1.Height = 1.6875F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.5F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.175F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label12.Text = "Units";
			this.Label12.Top = 1.515F;
			this.Label12.Width = 0.5F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.175F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.5625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label13.Text = "Dollars";
			this.Label13.Top = 1.515F;
			this.Label13.Width = 0.625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.8125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label1.Text = "**** TOWN DETAIL REPORT ****";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.5625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 1.8125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// txtMain
			// 
			this.txtMain.CanShrink = true;
			this.txtMain.Height = 0.1875F;
			this.txtMain.Left = 0.0625F;
			this.txtMain.Name = "txtMain";
			this.txtMain.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtMain.Text = null;
			this.txtMain.Top = 0F;
			this.txtMain.Width = 4.5625F;
			// 
			// txtUnits
			// 
			this.txtUnits.CanShrink = true;
			this.txtUnits.Height = 0.1875F;
			this.txtUnits.Left = 4.625F;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtUnits.Text = null;
			this.txtUnits.Top = 0F;
			this.txtUnits.Width = 0.5F;
			// 
			// txtDollars
			// 
			this.txtDollars.CanShrink = true;
			this.txtDollars.Height = 0.1875F;
			this.txtDollars.Left = 5.125F;
			this.txtDollars.Name = "txtDollars";
			this.txtDollars.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.txtDollars.Text = null;
			this.txtDollars.Top = 0F;
			this.txtDollars.Width = 1.0625F;
			// 
			// txtEmpty
			// 
			this.txtEmpty.Height = 0.1875F;
			this.txtEmpty.Left = 0.0625F;
			this.txtEmpty.Name = "txtEmpty";
			this.txtEmpty.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtEmpty.Text = null;
			this.txtEmpty.Top = 0.1875F;
			this.txtEmpty.Visible = false;
			this.txtEmpty.Width = 1.6875F;
			// 
			// txtStar
			// 
			this.txtStar.CanShrink = true;
			this.txtStar.Height = 0.1875F;
			this.txtStar.Left = 6.1875F;
			this.txtStar.Name = "txtStar";
			this.txtStar.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtStar.Text = null;
			this.txtStar.Top = 0F;
			this.txtStar.Width = 0.375F;
			// 
			// rptTownDetail
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.75F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMain)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDollars)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmpty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMain;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDollars;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmpty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStar;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
