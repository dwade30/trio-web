//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmLD79Estimate.
	/// </summary>
	partial class frmLD79Estimate
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtProposedYear;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtProposedYear_5;
		public fecherFoundation.FCTextBox txtProposedYear_4;
		public fecherFoundation.FCTextBox txtProposedYear_3;
		public fecherFoundation.FCTextBox txtProposedYear_2;
		public fecherFoundation.FCTextBox txtProposedYear_1;
		public fecherFoundation.FCTextBox txtProposedYear_0;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtProposedYear_5 = new fecherFoundation.FCTextBox();
			this.txtProposedYear_4 = new fecherFoundation.FCTextBox();
			this.txtProposedYear_3 = new fecherFoundation.FCTextBox();
			this.txtProposedYear_2 = new fecherFoundation.FCTextBox();
			this.txtProposedYear_1 = new fecherFoundation.FCTextBox();
			this.txtProposedYear_0 = new fecherFoundation.FCTextBox();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 561);
			this.BottomPanel.Size = new System.Drawing.Size(387, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.fraDateRange);
			this.ClientArea.Size = new System.Drawing.Size(387, 501);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(387, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(283, 30);
			this.HeaderText.Text = "Excise Rate Comparison";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.txtProposedYear_5);
			this.Frame2.Controls.Add(this.txtProposedYear_4);
			this.Frame2.Controls.Add(this.txtProposedYear_3);
			this.Frame2.Controls.Add(this.txtProposedYear_2);
			this.Frame2.Controls.Add(this.txtProposedYear_1);
			this.Frame2.Controls.Add(this.txtProposedYear_0);
			this.Frame2.Controls.Add(this.Label12);
			this.Frame2.Controls.Add(this.Label11);
			this.Frame2.Controls.Add(this.Label10);
			this.Frame2.Controls.Add(this.Label9);
			this.Frame2.Controls.Add(this.Label8);
			this.Frame2.Controls.Add(this.Label7);
			this.Frame2.Location = new System.Drawing.Point(30, 140);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(192, 339);
			this.Frame2.TabIndex = 4;
			this.Frame2.Text = "Proposed Mil Rates";
			// 
			// txtProposedYear_5
			// 
			this.txtProposedYear_5.AutoSize = false;
			this.txtProposedYear_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtProposedYear_5.LinkItem = null;
			this.txtProposedYear_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtProposedYear_5.LinkTopic = null;
			this.txtProposedYear_5.Location = new System.Drawing.Point(81, 280);
			this.txtProposedYear_5.Name = "txtProposedYear_5";
			this.txtProposedYear_5.Size = new System.Drawing.Size(90, 40);
			this.txtProposedYear_5.TabIndex = 16;
			this.txtProposedYear_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtProposedYear_KeyPress);
			// 
			// txtProposedYear_4
			// 
			this.txtProposedYear_4.AutoSize = false;
			this.txtProposedYear_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtProposedYear_4.LinkItem = null;
			this.txtProposedYear_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtProposedYear_4.LinkTopic = null;
			this.txtProposedYear_4.Location = new System.Drawing.Point(81, 230);
			this.txtProposedYear_4.Name = "txtProposedYear_4";
			this.txtProposedYear_4.Size = new System.Drawing.Size(90, 40);
			this.txtProposedYear_4.TabIndex = 15;
			this.txtProposedYear_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtProposedYear_KeyPress);
			// 
			// txtProposedYear_3
			// 
			this.txtProposedYear_3.AutoSize = false;
			this.txtProposedYear_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtProposedYear_3.LinkItem = null;
			this.txtProposedYear_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtProposedYear_3.LinkTopic = null;
			this.txtProposedYear_3.Location = new System.Drawing.Point(81, 180);
			this.txtProposedYear_3.Name = "txtProposedYear_3";
			this.txtProposedYear_3.Size = new System.Drawing.Size(90, 40);
			this.txtProposedYear_3.TabIndex = 14;
			this.txtProposedYear_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtProposedYear_KeyPress);
			// 
			// txtProposedYear_2
			// 
			this.txtProposedYear_2.AutoSize = false;
			this.txtProposedYear_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtProposedYear_2.LinkItem = null;
			this.txtProposedYear_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtProposedYear_2.LinkTopic = null;
			this.txtProposedYear_2.Location = new System.Drawing.Point(81, 130);
			this.txtProposedYear_2.Name = "txtProposedYear_2";
			this.txtProposedYear_2.Size = new System.Drawing.Size(90, 40);
			this.txtProposedYear_2.TabIndex = 13;
			this.txtProposedYear_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtProposedYear_KeyPress);
			// 
			// txtProposedYear_1
			// 
			this.txtProposedYear_1.AutoSize = false;
			this.txtProposedYear_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtProposedYear_1.LinkItem = null;
			this.txtProposedYear_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtProposedYear_1.LinkTopic = null;
			this.txtProposedYear_1.Location = new System.Drawing.Point(81, 80);
			this.txtProposedYear_1.Name = "txtProposedYear_1";
			this.txtProposedYear_1.Size = new System.Drawing.Size(90, 40);
			this.txtProposedYear_1.TabIndex = 12;
			this.txtProposedYear_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtProposedYear_KeyPress);
			// 
			// txtProposedYear_0
			// 
			this.txtProposedYear_0.AutoSize = false;
			this.txtProposedYear_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtProposedYear_0.LinkItem = null;
			this.txtProposedYear_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtProposedYear_0.LinkTopic = null;
			this.txtProposedYear_0.Location = new System.Drawing.Point(81, 30);
			this.txtProposedYear_0.Name = "txtProposedYear_0";
			this.txtProposedYear_0.Size = new System.Drawing.Size(90, 40);
			this.txtProposedYear_0.TabIndex = 11;
			this.txtProposedYear_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtProposedYear_KeyPress);
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(20, 44);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(50, 16);
			this.Label12.TabIndex = 10;
			this.Label12.Text = "YEAR 1";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 94);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(50, 16);
			this.Label11.TabIndex = 9;
			this.Label11.Text = "YEAR 2";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 144);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(50, 16);
			this.Label10.TabIndex = 8;
			this.Label10.Text = "YEAR 3";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 194);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(50, 16);
			this.Label9.TabIndex = 7;
			this.Label9.Text = "YEAR 4";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(20, 244);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(50, 16);
			this.Label8.TabIndex = 6;
			this.Label8.Text = "YEAR 5";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(20, 294);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(50, 16);
			this.Label7.TabIndex = 5;
			this.Label7.Text = "YEAR 6";
			// 
			// fraDateRange
			// 
			this.fraDateRange.Controls.Add(this.txtStartDate);
			this.fraDateRange.Controls.Add(this.txtEndDate);
			this.fraDateRange.Controls.Add(this.lblTo);
			this.fraDateRange.Location = new System.Drawing.Point(30, 30);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(333, 90);
			this.fraDateRange.TabIndex = 0;
			this.fraDateRange.Text = "Select Date Range";
			// 
			// txtStartDate
			// 
			this.txtStartDate.Location = new System.Drawing.Point(20, 30);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.SelLength = 0;
			this.txtStartDate.SelStart = 0;
			this.txtStartDate.Size = new System.Drawing.Size(115, 40);
			this.txtStartDate.TabIndex = 1;
			this.txtStartDate.Text = "  /  /";
			// 
			// txtEndDate
			// 
			this.txtEndDate.Location = new System.Drawing.Point(198, 30);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.SelLength = 0;
			this.txtEndDate.SelStart = 0;
			this.txtEndDate.Size = new System.Drawing.Size(115, 40);
			this.txtEndDate.TabIndex = 2;
			this.txtEndDate.Text = "  /  /";
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(158, 44);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(21, 16);
			this.lblTo.TabIndex = 3;
			this.lblTo.Text = "TO";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 0;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Location = new System.Drawing.Point(122, 30);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(140, 48);
			this.cmdPreview.TabIndex = 0;
			this.cmdPreview.Text = "Print Preview";
			this.cmdPreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmLD79Estimate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(387, 669);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLD79Estimate";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Excise Rate Comparison";
			this.Load += new System.EventHandler(this.frmLD79Estimate_Load);
			this.Activated += new System.EventHandler(this.frmLD79Estimate_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLD79Estimate_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPreview;
	}
}