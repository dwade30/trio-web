﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptThreeColumnTable.
	/// </summary>
	public partial class rptThreeColumnTable : BaseSectionReport
	{
		public rptThreeColumnTable()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Table Printout";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptThreeColumnTable InstancePtr
		{
			get
			{
				return (rptThreeColumnTable)Sys.GetInstance(typeof(rptThreeColumnTable));
			}
		}

		protected rptThreeColumnTable _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptThreeColumnTable	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int Row;
		int Col;
		int PageCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (MotorVehicle.Statics.TableName == "Style Codes")
			{
				if (frmTableMaintenance.InstancePtr.vsStyle.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsStyle.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsStyle.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsStyle.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Reason Codes")
			{
				if (frmTableMaintenance.InstancePtr.vsReasonCodes.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsReasonCodes.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsReasonCodes.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsReasonCodes.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Make Codes")
			{
				if (frmTableMaintenance.InstancePtr.vsMake.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsMake.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsMake.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsMake.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Operator ID Codes")
			{
				if (frmTableMaintenance.InstancePtr.vsOperators.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsOperators.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsOperators.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsOperators.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Class Codes")
			{
				if (frmTableMaintenance.InstancePtr.vsClass.Rows <= Row)
				{
					eArgs.EOF = true;
					return;
				}
				if (frmTableMaintenance.InstancePtr.vsClass.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsClass.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsClass.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsClass.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Commercial Truck / Truck Tractor GVW")
			{
				if (frmTableMaintenance.InstancePtr.vsGrossTable1.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsGrossTable1.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsGrossTable1.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsGrossTable1.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Farm Truck / Motor Home GVW")
			{
				if (frmTableMaintenance.InstancePtr.vsGrossTable2.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsGrossTable2.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsGrossTable2.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsGrossTable2.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Class A Special Mobile Equiptment GVW")
			{
				if (frmTableMaintenance.InstancePtr.vsGrossTable3.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsGrossTable3.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsGrossTable3.TextMatrix(Row, Col + 1);
				Field3.Text = frmTableMaintenance.InstancePtr.vsGrossTable3.TextMatrix(Row, Col + 2);
				Row += 1;
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblTitle.Text = MotorVehicle.Statics.TableName;
			if (MotorVehicle.Statics.TableName == "Style Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Reason Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Make Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Operator ID Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Class Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Commercial Truck / Truck Tractor GVW")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Farm Truck / Motor Home GVW")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Class A Special Mobile Equiptment GVW")
			{
				Row = 1;
				Col = 2;
			}
			//Application.DoEvents();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (MotorVehicle.Statics.TableName == "Style Codes")
			{
				Label1.Text = "Type";
				Label2.Text = "Code";
				Label3.Text = "Style";
			}
			else if (MotorVehicle.Statics.TableName == "Reason Codes")
			{
				Label1.Text = "Code";
				Label2.Text = "Priority";
				Label3.Text = "Description";
			}
			else if (MotorVehicle.Statics.TableName == "Make Codes")
			{
				Label1.Text = "Type";
				Label2.Text = "Code";
				Label3.Text = "Make";
			}
			else if (MotorVehicle.Statics.TableName == "Operator ID Codes")
			{
				Label1.Text = "Code";
				Label2.Text = "Operator";
				Label3.Text = "Agent Level";
			}
			else if (MotorVehicle.Statics.TableName == "Commercial Truck / Truck Tractor GVW")
			{
				Label1.Text = "From";
				Label2.Text = "To";
				Label3.Text = "Fee";
			}
			else if (MotorVehicle.Statics.TableName == "Farm Truck / Motor Home GVW")
			{
				Label1.Text = "From";
				Label2.Text = "To";
				Label3.Text = "Fee";
			}
			else if (MotorVehicle.Statics.TableName == "Class A Special Mobile Equiptment GVW")
			{
				Label1.Text = "From";
				Label2.Text = "To";
				Label3.Text = "Fee";
			}
			else if (MotorVehicle.Statics.TableName == "Class Codes")
			{
				Label1.Text = "Subclass";
				Label2.Text = "Class";
				Label3.Text = "Description";
			}
			//Application.DoEvents();
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptThreeColumnTable_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptThreeColumnTable properties;
			//rptThreeColumnTable.Caption	= "Table Printout";
			//rptThreeColumnTable.Icon	= "rptClassTable.dsx":0000";
			//rptThreeColumnTable.Left	= 0;
			//rptThreeColumnTable.Top	= 0;
			//rptThreeColumnTable.Width	= 11880;
			//rptThreeColumnTable.Height	= 8595;
			//rptThreeColumnTable.StartUpPosition	= 3;
			//rptThreeColumnTable.SectionData	= "rptClassTable.dsx":058A;
			//End Unmaped Properties
		}
	}
}
