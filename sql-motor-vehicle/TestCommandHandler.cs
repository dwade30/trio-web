﻿using TWMV0000.Commands;
using SharedApplication.Messaging;

namespace TWMV0000
{
    public class TestCommandHandler : CommandHandler<ChooseCentralParty, int>
    {
        private IModalView<ICentralPartySearchViewModel> searchView;
        public TestCommandHandler(IModalView<ICentralPartySearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override int Handle(ChooseCentralParty command)
        {
            searchView.ShowModal();
            return searchView.ViewModel.PartyId;
        }
    }
}