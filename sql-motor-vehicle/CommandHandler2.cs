﻿using TWMV0000.Commands;
using SharedApplication.Messaging;

namespace TWMV0000
{
    public class CommandHandler2 : CommandHandler<,$returntype$>
    {
        private IModalView<$viewmodelinterface$> _view;
        public CommandHandler2(IModalView<$viewmodelinterface$> modalview)
        {
            this._view = modalview;
        }
        protected override $returntype$ Handle(command)
        {
            _view.ShowModal();
            return _view.ViewModel.$viewmodelproperty$;
        }
    }
}