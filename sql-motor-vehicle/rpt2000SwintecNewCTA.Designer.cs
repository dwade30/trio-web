﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rpt2000SwintecNewCTA.
	/// </summary>
	partial class rpt2000SwintecNewCTA
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt2000SwintecNewCTA));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJointOwnership = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLegalRes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLeased = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBodyType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPurchaseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFirstLienHolder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateofLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1Address = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1City = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1State = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecondLienHolder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Address = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2City = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2State = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlateNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDealer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMile = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKilometer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtActual = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotActual = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometerChanged = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometerBroken = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Date = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPreviousTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStateofOrigin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtU = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRebuilt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmsrpamount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewMsrp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUsedMsrp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNRMSRP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSellerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSellerName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDoublCTATitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJointOwnership)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalRes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeased)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBodyType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLienHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Address)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1City)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1State)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Zip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondLienHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Address)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2City)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2State)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Zip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlateNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDealer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKilometer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActual)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotActual)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerChanged)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerBroken)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Date)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreviousTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateofOrigin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRebuilt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmsrpamount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewMsrp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedMsrp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNRMSRP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSellerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSellerName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoublCTATitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName1,
				this.txtName2,
				this.txtJointOwnership,
				this.txtDOB1,
				this.txtDOB2,
				this.txtTelephone,
				this.txtAddress,
				this.txtCity,
				this.txtState,
				this.txtZip,
				this.txtLegalRes,
				this.txtLeased,
				this.txtYear,
				this.txtMake,
				this.txtModel,
				this.txtVIN,
				this.txtBodyType,
				this.txtPurchaseDate,
				this.txtFirstLienHolder,
				this.txtDateofLien,
				this.txtLien1Address,
				this.txtLien1City,
				this.txtLien1State,
				this.txtLien1Zip,
				this.txtSecondLienHolder,
				this.txtLien2Address,
				this.txtLien2City,
				this.txtLien2State,
				this.txtLien2Zip,
				this.txtPlateNo,
				this.txtDealer,
				this.txtUsed,
				this.txtM,
				this.txtOdometer,
				this.txtMile,
				this.txtKilometer,
				this.txtActual,
				this.txtInExcess,
				this.txtNotActual,
				this.txtOdometerChanged,
				this.txtOdometerBroken,
				this.txtLien2Date,
				this.txtPreviousTitle,
				this.txtStateofOrigin,
				this.txtOther,
				this.txtNew,
				this.txtU,
				this.txtRebuilt,
				this.txtmsrpamount,
				this.txtNewMsrp,
				this.txtUsedMsrp,
				this.txtNRMSRP,
				this.txtSellerName,
				this.txtSellerName2,
				this.fldDoublCTATitle
			});
			this.Detail.Height = 8.125F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtName1
			// 
			this.txtName1.CanGrow = false;
			this.txtName1.Height = 0.21875F;
			this.txtName1.Left = 0.5625F;
			this.txtName1.MultiLine = false;
			this.txtName1.Name = "txtName1";
			this.txtName1.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtName1.Text = "  ";
			this.txtName1.Top = 0.34375F;
			this.txtName1.Width = 3F;
			// 
			// txtName2
			// 
			this.txtName2.CanGrow = false;
			this.txtName2.Height = 0.21875F;
			this.txtName2.Left = 0.5625F;
			this.txtName2.MultiLine = false;
			this.txtName2.Name = "txtName2";
			this.txtName2.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtName2.Text = "  ";
			this.txtName2.Top = 0.75F;
			this.txtName2.Width = 3F;
			// 
			// txtJointOwnership
			// 
			this.txtJointOwnership.CanGrow = false;
			this.txtJointOwnership.Height = 0.19F;
			this.txtJointOwnership.Left = 4.59375F;
			this.txtJointOwnership.MultiLine = false;
			this.txtJointOwnership.Name = "txtJointOwnership";
			this.txtJointOwnership.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtJointOwnership.Text = "  ";
			this.txtJointOwnership.Top = 0.6875F;
			this.txtJointOwnership.Width = 0.1875F;
			// 
			// txtDOB1
			// 
			this.txtDOB1.CanGrow = false;
			this.txtDOB1.Height = 0.21875F;
			this.txtDOB1.Left = 5F;
			this.txtDOB1.MultiLine = false;
			this.txtDOB1.Name = "txtDOB1";
			this.txtDOB1.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDOB1.Text = "  ";
			this.txtDOB1.Top = 0.34375F;
			this.txtDOB1.Width = 1F;
			// 
			// txtDOB2
			// 
			this.txtDOB2.CanGrow = false;
			this.txtDOB2.Height = 0.1875F;
			this.txtDOB2.Left = 5F;
			this.txtDOB2.MultiLine = false;
			this.txtDOB2.Name = "txtDOB2";
			this.txtDOB2.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDOB2.Text = "  ";
			this.txtDOB2.Top = 0.75F;
			this.txtDOB2.Width = 1F;
			// 
			// txtTelephone
			// 
			this.txtTelephone.CanGrow = false;
			this.txtTelephone.Height = 0.21875F;
			this.txtTelephone.Left = 6.09375F;
			this.txtTelephone.MultiLine = false;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtTelephone.Text = "  ";
			this.txtTelephone.Top = 0.375F;
			this.txtTelephone.Width = 1.28125F;
			// 
			// txtAddress
			// 
			this.txtAddress.CanGrow = false;
			this.txtAddress.Height = 0.21875F;
			this.txtAddress.Left = 0.34375F;
			this.txtAddress.MultiLine = false;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtAddress.Text = "  ";
			this.txtAddress.Top = 1.09375F;
			this.txtAddress.Width = 3.25F;
			// 
			// txtCity
			// 
			this.txtCity.CanGrow = false;
			this.txtCity.Height = 0.21875F;
			this.txtCity.Left = 0.3125F;
			this.txtCity.MultiLine = false;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtCity.Text = "  ";
			this.txtCity.Top = 1.5625F;
			this.txtCity.Width = 2F;
			// 
			// txtState
			// 
			this.txtState.CanGrow = false;
			this.txtState.Height = 0.21875F;
			this.txtState.Left = 2.375F;
			this.txtState.MultiLine = false;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtState.Text = "  ";
			this.txtState.Top = 1.5625F;
			this.txtState.Width = 0.4375F;
			// 
			// txtZip
			// 
			this.txtZip.CanGrow = false;
			this.txtZip.Height = 0.21875F;
			this.txtZip.Left = 3F;
			this.txtZip.MultiLine = false;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtZip.Text = "  ";
			this.txtZip.Top = 1.5625F;
			this.txtZip.Width = 1.125F;
			// 
			// txtLegalRes
			// 
			this.txtLegalRes.CanGrow = false;
			this.txtLegalRes.Height = 0.21875F;
			this.txtLegalRes.Left = 0.3125F;
			this.txtLegalRes.MultiLine = false;
			this.txtLegalRes.Name = "txtLegalRes";
			this.txtLegalRes.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLegalRes.Text = "  ";
			this.txtLegalRes.Top = 1.96875F;
			this.txtLegalRes.Width = 3.8125F;
			// 
			// txtLeased
			// 
			this.txtLeased.CanGrow = false;
			this.txtLeased.Height = 0.21875F;
			this.txtLeased.Left = 0.3125F;
			this.txtLeased.MultiLine = false;
			this.txtLeased.Name = "txtLeased";
			this.txtLeased.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLeased.Text = "  ";
			this.txtLeased.Top = 2.3125F;
			this.txtLeased.Width = 3.25F;
			// 
			// txtYear
			// 
			this.txtYear.CanGrow = false;
			this.txtYear.Height = 0.21875F;
			this.txtYear.Left = 0.3125F;
			this.txtYear.MultiLine = false;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtYear.Text = "  ";
			this.txtYear.Top = 2.875F;
			this.txtYear.Width = 0.4375F;
			// 
			// txtMake
			// 
			this.txtMake.CanGrow = false;
			this.txtMake.Height = 0.21875F;
			this.txtMake.Left = 1.03125F;
			this.txtMake.MultiLine = false;
			this.txtMake.Name = "txtMake";
			this.txtMake.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtMake.Text = "  ";
			this.txtMake.Top = 2.875F;
			this.txtMake.Width = 0.75F;
			// 
			// txtModel
			// 
			this.txtModel.CanGrow = false;
			this.txtModel.Height = 0.21875F;
			this.txtModel.Left = 1.90625F;
			this.txtModel.MultiLine = false;
			this.txtModel.Name = "txtModel";
			this.txtModel.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtModel.Text = "  ";
			this.txtModel.Top = 2.875F;
			this.txtModel.Width = 0.8125F;
			// 
			// txtVIN
			// 
			this.txtVIN.CanGrow = false;
			this.txtVIN.Height = 0.21875F;
			this.txtVIN.Left = 2.75F;
			this.txtVIN.MultiLine = false;
			this.txtVIN.Name = "txtVIN";
			this.txtVIN.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtVIN.Text = "  ";
			this.txtVIN.Top = 2.875F;
			this.txtVIN.Width = 2.375F;
			// 
			// txtBodyType
			// 
			this.txtBodyType.CanGrow = false;
			this.txtBodyType.Height = 0.21875F;
			this.txtBodyType.Left = 5.8125F;
			this.txtBodyType.MultiLine = false;
			this.txtBodyType.Name = "txtBodyType";
			this.txtBodyType.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtBodyType.Text = "  ";
			this.txtBodyType.Top = 2.875F;
			this.txtBodyType.Width = 0.4375F;
			// 
			// txtPurchaseDate
			// 
			this.txtPurchaseDate.CanGrow = false;
			this.txtPurchaseDate.Height = 0.21875F;
			this.txtPurchaseDate.Left = 1.03125F;
			this.txtPurchaseDate.MultiLine = false;
			this.txtPurchaseDate.Name = "txtPurchaseDate";
			this.txtPurchaseDate.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtPurchaseDate.Text = "  ";
			this.txtPurchaseDate.Top = 3.46875F;
			this.txtPurchaseDate.Width = 1.03125F;
			// 
			// txtFirstLienHolder
			// 
			this.txtFirstLienHolder.CanGrow = false;
			this.txtFirstLienHolder.Height = 0.21875F;
			this.txtFirstLienHolder.Left = 0.40625F;
			this.txtFirstLienHolder.MultiLine = false;
			this.txtFirstLienHolder.Name = "txtFirstLienHolder";
			this.txtFirstLienHolder.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtFirstLienHolder.Text = "  ";
			this.txtFirstLienHolder.Top = 4.90625F;
			this.txtFirstLienHolder.Width = 3.25F;
			// 
			// txtDateofLien
			// 
			this.txtDateofLien.CanGrow = false;
			this.txtDateofLien.Height = 0.21875F;
			this.txtDateofLien.Left = 4.84375F;
			this.txtDateofLien.MultiLine = false;
			this.txtDateofLien.Name = "txtDateofLien";
			this.txtDateofLien.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDateofLien.Text = "  ";
			this.txtDateofLien.Top = 4.90625F;
			this.txtDateofLien.Width = 1.0625F;
			// 
			// txtLien1Address
			// 
			this.txtLien1Address.CanGrow = false;
			this.txtLien1Address.Height = 0.21875F;
			this.txtLien1Address.Left = 0.40625F;
			this.txtLien1Address.MultiLine = false;
			this.txtLien1Address.Name = "txtLien1Address";
			this.txtLien1Address.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1Address.Text = "  ";
			this.txtLien1Address.Top = 5.40625F;
			this.txtLien1Address.Width = 1.9375F;
			// 
			// txtLien1City
			// 
			this.txtLien1City.CanGrow = false;
			this.txtLien1City.Height = 0.21875F;
			this.txtLien1City.Left = 2.40625F;
			this.txtLien1City.MultiLine = false;
			this.txtLien1City.Name = "txtLien1City";
			this.txtLien1City.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1City.Text = "  ";
			this.txtLien1City.Top = 5.40625F;
			this.txtLien1City.Width = 1.6875F;
			// 
			// txtLien1State
			// 
			this.txtLien1State.CanGrow = false;
			this.txtLien1State.Height = 0.21875F;
			this.txtLien1State.Left = 4.21875F;
			this.txtLien1State.MultiLine = false;
			this.txtLien1State.Name = "txtLien1State";
			this.txtLien1State.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1State.Text = "  ";
			this.txtLien1State.Top = 5.40625F;
			this.txtLien1State.Width = 0.75F;
			// 
			// txtLien1Zip
			// 
			this.txtLien1Zip.CanGrow = false;
			this.txtLien1Zip.Height = 0.21875F;
			this.txtLien1Zip.Left = 5.03125F;
			this.txtLien1Zip.MultiLine = false;
			this.txtLien1Zip.Name = "txtLien1Zip";
			this.txtLien1Zip.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1Zip.Text = "  ";
			this.txtLien1Zip.Top = 5.40625F;
			this.txtLien1Zip.Width = 1F;
			// 
			// txtSecondLienHolder
			// 
			this.txtSecondLienHolder.CanGrow = false;
			this.txtSecondLienHolder.Height = 0.21875F;
			this.txtSecondLienHolder.Left = 0.40625F;
			this.txtSecondLienHolder.MultiLine = false;
			this.txtSecondLienHolder.Name = "txtSecondLienHolder";
			this.txtSecondLienHolder.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtSecondLienHolder.Text = "  ";
			this.txtSecondLienHolder.Top = 5.90625F;
			this.txtSecondLienHolder.Width = 3.25F;
			// 
			// txtLien2Address
			// 
			this.txtLien2Address.CanGrow = false;
			this.txtLien2Address.Height = 0.21875F;
			this.txtLien2Address.Left = 0.40625F;
			this.txtLien2Address.MultiLine = false;
			this.txtLien2Address.Name = "txtLien2Address";
			this.txtLien2Address.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Address.Text = "  ";
			this.txtLien2Address.Top = 6.3125F;
			this.txtLien2Address.Width = 1.9375F;
			// 
			// txtLien2City
			// 
			this.txtLien2City.CanGrow = false;
			this.txtLien2City.Height = 0.21875F;
			this.txtLien2City.Left = 2.40625F;
			this.txtLien2City.MultiLine = false;
			this.txtLien2City.Name = "txtLien2City";
			this.txtLien2City.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2City.Text = "  ";
			this.txtLien2City.Top = 6.3125F;
			this.txtLien2City.Width = 1.6875F;
			// 
			// txtLien2State
			// 
			this.txtLien2State.CanGrow = false;
			this.txtLien2State.Height = 0.21875F;
			this.txtLien2State.Left = 4.21875F;
			this.txtLien2State.MultiLine = false;
			this.txtLien2State.Name = "txtLien2State";
			this.txtLien2State.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2State.Text = "  ";
			this.txtLien2State.Top = 6.3125F;
			this.txtLien2State.Width = 0.75F;
			// 
			// txtLien2Zip
			// 
			this.txtLien2Zip.CanGrow = false;
			this.txtLien2Zip.Height = 0.21875F;
			this.txtLien2Zip.Left = 5.03125F;
			this.txtLien2Zip.MultiLine = false;
			this.txtLien2Zip.Name = "txtLien2Zip";
			this.txtLien2Zip.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Zip.Text = "  ";
			this.txtLien2Zip.Top = 6.3125F;
			this.txtLien2Zip.Width = 1F;
			// 
			// txtPlateNo
			// 
			this.txtPlateNo.CanGrow = false;
			this.txtPlateNo.Height = 0.21875F;
			this.txtPlateNo.Left = 4.65625F;
			this.txtPlateNo.MultiLine = false;
			this.txtPlateNo.Name = "txtPlateNo";
			this.txtPlateNo.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtPlateNo.Text = "  ";
			this.txtPlateNo.Top = 6.875F;
			this.txtPlateNo.Width = 0.75F;
			// 
			// txtDealer
			// 
			this.txtDealer.CanGrow = false;
			this.txtDealer.Height = 0.19F;
			this.txtDealer.Left = 6.0625F;
			this.txtDealer.MultiLine = false;
			this.txtDealer.Name = "txtDealer";
			this.txtDealer.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtDealer.Text = "  ";
			this.txtDealer.Top = 6.65625F;
			this.txtDealer.Width = 0.1875F;
			// 
			// txtUsed
			// 
			this.txtUsed.CanGrow = false;
			this.txtUsed.Height = 0.19F;
			this.txtUsed.Left = 6.0625F;
			this.txtUsed.MultiLine = false;
			this.txtUsed.Name = "txtUsed";
			this.txtUsed.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtUsed.Text = "  ";
			this.txtUsed.Top = 6.96875F;
			this.txtUsed.Width = 0.1875F;
			// 
			// txtM
			// 
			this.txtM.CanGrow = false;
			this.txtM.Height = 0.19F;
			this.txtM.Left = 6.0625F;
			this.txtM.MultiLine = false;
			this.txtM.Name = "txtM";
			this.txtM.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtM.Text = "  ";
			this.txtM.Top = 7.25F;
			this.txtM.Width = 0.1875F;
			// 
			// txtOdometer
			// 
			this.txtOdometer.CanGrow = false;
			this.txtOdometer.Height = 0.21875F;
			this.txtOdometer.Left = 0.40625F;
			this.txtOdometer.MultiLine = false;
			this.txtOdometer.Name = "txtOdometer";
			this.txtOdometer.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtOdometer.Text = "  ";
			this.txtOdometer.Top = 4.15625F;
			this.txtOdometer.Width = 1.375F;
			// 
			// txtMile
			// 
			this.txtMile.CanGrow = false;
			this.txtMile.Height = 0.19F;
			this.txtMile.Left = 2.25F;
			this.txtMile.MultiLine = false;
			this.txtMile.Name = "txtMile";
			this.txtMile.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtMile.Text = "  ";
			this.txtMile.Top = 3.9375F;
			this.txtMile.Width = 0.1875F;
			// 
			// txtKilometer
			// 
			this.txtKilometer.CanGrow = false;
			this.txtKilometer.Height = 0.19F;
			this.txtKilometer.Left = 2.25F;
			this.txtKilometer.MultiLine = false;
			this.txtKilometer.Name = "txtKilometer";
			this.txtKilometer.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtKilometer.Text = "  ";
			this.txtKilometer.Top = 4.46875F;
			this.txtKilometer.Width = 0.1875F;
			// 
			// txtActual
			// 
			this.txtActual.CanGrow = false;
			this.txtActual.Height = 0.125F;
			this.txtActual.Left = 2.75F;
			this.txtActual.MultiLine = false;
			this.txtActual.Name = "txtActual";
			this.txtActual.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtActual.Text = "  ";
			this.txtActual.Top = 3.9375F;
			this.txtActual.Width = 0.1875F;
			// 
			// txtInExcess
			// 
			this.txtInExcess.CanGrow = false;
			this.txtInExcess.Height = 0.125F;
			this.txtInExcess.Left = 2.75F;
			this.txtInExcess.MultiLine = false;
			this.txtInExcess.Name = "txtInExcess";
			this.txtInExcess.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtInExcess.Text = "  ";
			this.txtInExcess.Top = 4.21875F;
			this.txtInExcess.Width = 0.1875F;
			// 
			// txtNotActual
			// 
			this.txtNotActual.CanGrow = false;
			this.txtNotActual.Height = 0.125F;
			this.txtNotActual.Left = 2.75F;
			this.txtNotActual.MultiLine = false;
			this.txtNotActual.Name = "txtNotActual";
			this.txtNotActual.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtNotActual.Text = "  ";
			this.txtNotActual.Top = 4.5F;
			this.txtNotActual.Width = 0.1875F;
			// 
			// txtOdometerChanged
			// 
			this.txtOdometerChanged.CanGrow = false;
			this.txtOdometerChanged.Height = 0.19F;
			this.txtOdometerChanged.Left = 5.03125F;
			this.txtOdometerChanged.MultiLine = false;
			this.txtOdometerChanged.Name = "txtOdometerChanged";
			this.txtOdometerChanged.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOdometerChanged.Text = "  ";
			this.txtOdometerChanged.Top = 3.9375F;
			this.txtOdometerChanged.Width = 0.1875F;
			// 
			// txtOdometerBroken
			// 
			this.txtOdometerBroken.CanGrow = false;
			this.txtOdometerBroken.Height = 0.19F;
			this.txtOdometerBroken.Left = 5.03125F;
			this.txtOdometerBroken.MultiLine = false;
			this.txtOdometerBroken.Name = "txtOdometerBroken";
			this.txtOdometerBroken.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOdometerBroken.Text = "  ";
			this.txtOdometerBroken.Top = 4.25F;
			this.txtOdometerBroken.Width = 0.1875F;
			// 
			// txtLien2Date
			// 
			this.txtLien2Date.CanGrow = false;
			this.txtLien2Date.Height = 0.21875F;
			this.txtLien2Date.Left = 4.84375F;
			this.txtLien2Date.MultiLine = false;
			this.txtLien2Date.Name = "txtLien2Date";
			this.txtLien2Date.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Date.Text = "  ";
			this.txtLien2Date.Top = 5.90625F;
			this.txtLien2Date.Width = 1.0625F;
			// 
			// txtPreviousTitle
			// 
			this.txtPreviousTitle.CanGrow = false;
			this.txtPreviousTitle.Height = 0.21875F;
			this.txtPreviousTitle.Left = 2.15625F;
			this.txtPreviousTitle.MultiLine = false;
			this.txtPreviousTitle.Name = "txtPreviousTitle";
			this.txtPreviousTitle.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtPreviousTitle.Text = "  ";
			this.txtPreviousTitle.Top = 3.46875F;
			this.txtPreviousTitle.Width = 1.125F;
			// 
			// txtStateofOrigin
			// 
			this.txtStateofOrigin.CanGrow = false;
			this.txtStateofOrigin.Height = 0.21875F;
			this.txtStateofOrigin.Left = 3.46875F;
			this.txtStateofOrigin.MultiLine = false;
			this.txtStateofOrigin.Name = "txtStateofOrigin";
			this.txtStateofOrigin.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtStateofOrigin.Text = "  ";
			this.txtStateofOrigin.Top = 3.46875F;
			this.txtStateofOrigin.Width = 0.8125F;
			// 
			// txtOther
			// 
			this.txtOther.CanGrow = false;
			this.txtOther.Height = 0.21875F;
			this.txtOther.Left = 4.46875F;
			this.txtOther.MultiLine = false;
			this.txtOther.Name = "txtOther";
			this.txtOther.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtOther.Text = "  ";
			this.txtOther.Top = 3.46875F;
			this.txtOther.Width = 1.4375F;
			// 
			// txtNew
			// 
			this.txtNew.CanGrow = false;
			this.txtNew.Height = 0.125F;
			this.txtNew.Left = 0.78125F;
			this.txtNew.MultiLine = false;
			this.txtNew.Name = "txtNew";
			this.txtNew.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtNew.Text = "  ";
			this.txtNew.Top = 3.15625F;
			this.txtNew.Width = 0.1875F;
			// 
			// txtU
			// 
			this.txtU.CanGrow = false;
			this.txtU.Height = 0.125F;
			this.txtU.Left = 0.78125F;
			this.txtU.MultiLine = false;
			this.txtU.Name = "txtU";
			this.txtU.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtU.Text = "  ";
			this.txtU.Top = 3.40625F;
			this.txtU.Width = 0.1875F;
			// 
			// txtRebuilt
			// 
			this.txtRebuilt.CanGrow = false;
			this.txtRebuilt.Height = 0.125F;
			this.txtRebuilt.Left = 0.78125F;
			this.txtRebuilt.MultiLine = false;
			this.txtRebuilt.Name = "txtRebuilt";
			this.txtRebuilt.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtRebuilt.Text = "  ";
			this.txtRebuilt.Top = 3.75F;
			this.txtRebuilt.Width = 0.1875F;
			// 
			// txtmsrpamount
			// 
			this.txtmsrpamount.CanGrow = false;
			this.txtmsrpamount.Height = 0.21875F;
			this.txtmsrpamount.Left = 4.5F;
			this.txtmsrpamount.MultiLine = false;
			this.txtmsrpamount.Name = "txtmsrpamount";
			this.txtmsrpamount.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtmsrpamount.Text = "  ";
			this.txtmsrpamount.Top = 2.3125F;
			this.txtmsrpamount.Width = 1.375F;
			// 
			// txtNewMsrp
			// 
			this.txtNewMsrp.CanGrow = false;
			this.txtNewMsrp.Height = 0.125F;
			this.txtNewMsrp.Left = 4.375F;
			this.txtNewMsrp.MultiLine = false;
			this.txtNewMsrp.Name = "txtNewMsrp";
			this.txtNewMsrp.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtNewMsrp.Text = "  ";
			this.txtNewMsrp.Top = 1.15625F;
			this.txtNewMsrp.Width = 0.1875F;
			// 
			// txtUsedMsrp
			// 
			this.txtUsedMsrp.CanGrow = false;
			this.txtUsedMsrp.Height = 0.125F;
			this.txtUsedMsrp.Left = 4.375F;
			this.txtUsedMsrp.MultiLine = false;
			this.txtUsedMsrp.Name = "txtUsedMsrp";
			this.txtUsedMsrp.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtUsedMsrp.Text = "  ";
			this.txtUsedMsrp.Top = 1.375F;
			this.txtUsedMsrp.Width = 0.1875F;
			// 
			// txtNRMSRP
			// 
			this.txtNRMSRP.CanGrow = false;
			this.txtNRMSRP.Height = 0.125F;
			this.txtNRMSRP.Left = 4.375F;
			this.txtNRMSRP.MultiLine = false;
			this.txtNRMSRP.Name = "txtNRMSRP";
			this.txtNRMSRP.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtNRMSRP.Text = "  ";
			this.txtNRMSRP.Top = 1.59375F;
			this.txtNRMSRP.Width = 0.1875F;
			// 
			// txtSellerName
			// 
			this.txtSellerName.Height = 0.19F;
			this.txtSellerName.Left = 0.4375F;
			this.txtSellerName.MultiLine = false;
			this.txtSellerName.Name = "txtSellerName";
			this.txtSellerName.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtSellerName.Text = null;
			this.txtSellerName.Top = 6.78125F;
			this.txtSellerName.Width = 3.6875F;
			// 
			// txtSellerName2
			// 
			this.txtSellerName2.Height = 0.19F;
			this.txtSellerName2.Left = 0.4375F;
			this.txtSellerName2.MultiLine = false;
			this.txtSellerName2.Name = "txtSellerName2";
			this.txtSellerName2.Style = "font-family: \'Roman 10cpi\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtSellerName2.Text = null;
			this.txtSellerName2.Top = 6.9375F;
			this.txtSellerName2.Width = 3.6875F;
			// 
			// fldDoublCTATitle
			// 
			this.fldDoublCTATitle.CanGrow = false;
			this.fldDoublCTATitle.Height = 0.21875F;
			this.fldDoublCTATitle.Left = 2.21875F;
			this.fldDoublCTATitle.MultiLine = false;
			this.fldDoublCTATitle.Name = "fldDoublCTATitle";
			this.fldDoublCTATitle.Style = "font-family: \'Roman 10cpi\'; text-align: center; vertical-align: bottom; ddo-char-" + "set: 1";
			this.fldDoublCTATitle.Text = "DOUBLE TITLE APPLICATION";
			this.fldDoublCTATitle.Top = 0F;
			this.fldDoublCTATitle.Visible = false;
			this.fldDoublCTATitle.Width = 3F;
			// 
			// rpt2000SwintecNewCTA
			//
			// 
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.625F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJointOwnership)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLegalRes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLeased)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBodyType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLienHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Address)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1City)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1State)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Zip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondLienHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Address)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2City)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2State)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Zip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlateNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDealer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKilometer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActual)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotActual)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerChanged)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerBroken)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Date)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPreviousTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateofOrigin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtU)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRebuilt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmsrpamount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewMsrp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUsedMsrp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNRMSRP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSellerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSellerName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoublCTATitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJointOwnership;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLegalRes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLeased;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBodyType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPurchaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirstLienHolder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateofLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1Address;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1City;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1State;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1Zip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondLienHolder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Address;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2City;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2State;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Zip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlateNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDealer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtM;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMile;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKilometer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtActual;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInExcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotActual;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometerChanged;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometerBroken;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Date;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPreviousTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateofOrigin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtU;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRebuilt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmsrpamount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewMsrp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUsedMsrp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNRMSRP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSellerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSellerName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDoublCTATitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
