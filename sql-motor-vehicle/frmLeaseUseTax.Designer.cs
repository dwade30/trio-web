//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmLeaseUseTax.
	/// </summary>
	partial class frmLeaseUseTax
	{
		public fecherFoundation.FCComboBox cmbUseTaxPrint;
		public fecherFoundation.FCTextBox txtRate;
		public Global.T2KBackFillWhole txtPayments;
		public Global.T2KOverTypeBox txtA1;
		public Global.T2KBackFillDecimal txtUTCPrice;
		public Global.T2KOverTypeBox txtB2;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCTextBox txtUTCExempt;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkRegistrant1;
		public fecherFoundation.FCTextBox txtUTCSSN;
		public fecherFoundation.FCTextBox txtUTCPurchaserName;
		public fecherFoundation.FCTextBox txtUTCPurchaserCity;
		public fecherFoundation.FCTextBox txtUTCPurcahserAddress;
		public fecherFoundation.FCTextBox txtUTCPurchaserZip;
		public fecherFoundation.FCTextBox txtUTCPurchaserState;
		public fecherFoundation.FCTextBox txtUTCPurchaserZip4;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCFrame fraLessor;
		public fecherFoundation.FCTextBox txtUTCSellerZip4;
		public fecherFoundation.FCTextBox txtUTCSellerAddress;
		public fecherFoundation.FCTextBox txtUTCSellerCity;
		public fecherFoundation.FCTextBox txtUTCSellerState;
		public fecherFoundation.FCTextBox txtUTCSellerZip;
		public fecherFoundation.FCTextBox txtUTCSellerName;
		public Global.T2KDateBox txtUTCDate;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCTextBox txtB1;
		public fecherFoundation.FCTextBox txtC1;
		public fecherFoundation.FCTextBox txtD1;
		public Global.T2KBackFillDecimal txtC2;
		public Global.T2KBackFillDecimal txtUTCUseTax;
		public Global.T2KBackFillWhole txtUTCNetAmount;
		public Global.T2KDateBox txtB3;
		public Global.T2KBackFillDecimal txtUTCAllowance;
		public Global.T2KBackFillDecimal txtPayment;
		public Global.T2KBackFillDecimal txtDownPayment;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel lblA1;
		public fecherFoundation.FCLabel lblB2;
		public fecherFoundation.FCLabel lblB3;
		public fecherFoundation.FCLabel lblB1;
		public fecherFoundation.FCLabel lblC1;
		public fecherFoundation.FCLabel lblC2;
		public fecherFoundation.FCLabel lblD1;
		public fecherFoundation.FCLabel lblExemptCode;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbUseTaxPrint = new fecherFoundation.FCComboBox();
            this.txtRate = new fecherFoundation.FCTextBox();
            this.txtPayments = new Global.T2KBackFillWhole();
            this.txtA1 = new Global.T2KOverTypeBox();
            this.txtUTCPrice = new Global.T2KBackFillDecimal();
            this.txtB2 = new Global.T2KOverTypeBox();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.txtUTCExempt = new fecherFoundation.FCTextBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkRegistrant1 = new fecherFoundation.FCCheckBox();
            this.txtUTCSSN = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserName = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserCity = new fecherFoundation.FCTextBox();
            this.txtUTCPurcahserAddress = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserZip = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserState = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserZip4 = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.fraLessor = new fecherFoundation.FCFrame();
            this.txtUTCSellerZip4 = new fecherFoundation.FCTextBox();
            this.txtUTCSellerAddress = new fecherFoundation.FCTextBox();
            this.txtUTCSellerCity = new fecherFoundation.FCTextBox();
            this.txtUTCSellerState = new fecherFoundation.FCTextBox();
            this.txtUTCSellerZip = new fecherFoundation.FCTextBox();
            this.txtUTCSellerName = new fecherFoundation.FCTextBox();
            this.txtUTCDate = new Global.T2KDateBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.txtB1 = new fecherFoundation.FCTextBox();
            this.txtC1 = new fecherFoundation.FCTextBox();
            this.txtD1 = new fecherFoundation.FCTextBox();
            this.txtC2 = new Global.T2KBackFillDecimal();
            this.txtUTCUseTax = new Global.T2KBackFillDecimal();
            this.txtUTCNetAmount = new Global.T2KBackFillWhole();
            this.txtB3 = new Global.T2KDateBox();
            this.txtUTCAllowance = new Global.T2KBackFillDecimal();
            this.txtPayment = new Global.T2KBackFillDecimal();
            this.txtDownPayment = new Global.T2KBackFillDecimal();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.lblA1 = new fecherFoundation.FCLabel();
            this.lblB2 = new fecherFoundation.FCLabel();
            this.lblB3 = new fecherFoundation.FCLabel();
            this.lblB1 = new fecherFoundation.FCLabel();
            this.lblC1 = new fecherFoundation.FCLabel();
            this.lblC2 = new fecherFoundation.FCLabel();
            this.lblD1 = new fecherFoundation.FCLabel();
            this.lblExemptCode = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdFileSaveExit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegistrant1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraLessor)).BeginInit();
            this.fraLessor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCUseTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCNetAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCAllowance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDownPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 533);
            this.BottomPanel.Size = new System.Drawing.Size(985, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbUseTaxPrint);
            this.ClientArea.Controls.Add(this.txtRate);
            this.ClientArea.Controls.Add(this.txtPayments);
            this.ClientArea.Controls.Add(this.txtA1);
            this.ClientArea.Controls.Add(this.txtUTCPrice);
            this.ClientArea.Controls.Add(this.txtB2);
            this.ClientArea.Controls.Add(this.cmdReturn);
            this.ClientArea.Controls.Add(this.txtUTCExempt);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraLessor);
            this.ClientArea.Controls.Add(this.txtB1);
            this.ClientArea.Controls.Add(this.txtC1);
            this.ClientArea.Controls.Add(this.txtD1);
            this.ClientArea.Controls.Add(this.txtC2);
            this.ClientArea.Controls.Add(this.txtUTCUseTax);
            this.ClientArea.Controls.Add(this.txtUTCNetAmount);
            this.ClientArea.Controls.Add(this.txtB3);
            this.ClientArea.Controls.Add(this.txtUTCAllowance);
            this.ClientArea.Controls.Add(this.txtPayment);
            this.ClientArea.Controls.Add(this.txtDownPayment);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label18);
            this.ClientArea.Controls.Add(this.Label19);
            this.ClientArea.Controls.Add(this.Label20);
            this.ClientArea.Controls.Add(this.Label21);
            this.ClientArea.Controls.Add(this.Label22);
            this.ClientArea.Controls.Add(this.Label23);
            this.ClientArea.Controls.Add(this.lblA1);
            this.ClientArea.Controls.Add(this.lblB2);
            this.ClientArea.Controls.Add(this.lblB3);
            this.ClientArea.Controls.Add(this.lblB1);
            this.ClientArea.Controls.Add(this.lblC1);
            this.ClientArea.Controls.Add(this.lblC2);
            this.ClientArea.Controls.Add(this.lblD1);
            this.ClientArea.Controls.Add(this.lblExemptCode);
            this.ClientArea.Size = new System.Drawing.Size(985, 473);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(985, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(103, 30);
            this.HeaderText.Text = "Use Tax";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbUseTaxPrint
            // 
            this.cmbUseTaxPrint.AutoSize = false;
            this.cmbUseTaxPrint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbUseTaxPrint.FormattingEnabled = true;
            this.cmbUseTaxPrint.Items.AddRange(new object[] {
            "This Form NEEDS to be printed",
            "This Form does NOT need to be printed"});
            this.cmbUseTaxPrint.Location = new System.Drawing.Point(30, 600);
            this.cmbUseTaxPrint.Name = "cmbUseTaxPrint";
            this.cmbUseTaxPrint.Size = new System.Drawing.Size(449, 40);
            this.cmbUseTaxPrint.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.cmbUseTaxPrint, null);
            // 
            // txtRate
            // 
			this.txtRate.MaxLength = 6;
            this.txtRate.AutoSize = false;
            this.txtRate.CharacterCasing = CharacterCasing.Upper;
            this.txtRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtRate.LinkItem = null;
            this.txtRate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtRate.LinkTopic = null;
            this.txtRate.Location = new System.Drawing.Point(757, 330);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(108, 40);
            this.txtRate.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtRate, null);
            this.txtRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRate_KeyPress);
            this.txtRate.Enter += new System.EventHandler(this.txtRate_Enter);
            this.txtRate.Leave += new System.EventHandler(this.txtRate_Leave);
            this.txtRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtRate_Validating);
            // 
            // txtPayments
            // 
            this.txtPayments.Location = new System.Drawing.Point(757, 80);
            this.txtPayments.MaxLength = 3;
            this.txtPayments.Name = "txtPayments";
            this.txtPayments.Size = new System.Drawing.Size(108, 40);
            this.txtPayments.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtPayments, null);
            this.txtPayments.Enter += new System.EventHandler(this.txtPayments_Enter);
            this.txtPayments.Leave += new System.EventHandler(this.txtPayments_Leave);
            // 
            // txtA1
            // 
            this.txtA1.AutoSize = false;
            this.txtA1.CharacterCasing = CharacterCasing.Upper;
            this.txtA1.LinkItem = null;
            this.txtA1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtA1.LinkTopic = null;
            this.txtA1.Location = new System.Drawing.Point(756, 480);
            this.txtA1.MaxLength = 5;
            this.txtA1.Name = "txtA1";
            this.txtA1.Size = new System.Drawing.Size(44, 40);
            this.txtA1.TabIndex = 55;
            this.ToolTip1.SetToolTip(this.txtA1, null);
            this.txtA1.Visible = false;
            this.txtA1.Enter += new System.EventHandler(this.txtA1_Enter);
            this.txtA1.Leave += new System.EventHandler(this.txtA1_Leave);
            // 
            // txtUTCPrice
            // 
            this.txtUTCPrice.Location = new System.Drawing.Point(757, 180);
            this.txtUTCPrice.MaxLength = 10;
            this.txtUTCPrice.Name = "txtUTCPrice";
            this.txtUTCPrice.Size = new System.Drawing.Size(108, 40);
            this.txtUTCPrice.TabIndex = 5;
            this.txtUTCPrice.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtUTCPrice, null);
            this.txtUTCPrice.Enter += new System.EventHandler(this.txtUTCPrice_Enter);
            this.txtUTCPrice.Leave += new System.EventHandler(this.txtUTCPrice_Leave);
            this.txtUTCPrice.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCPrice_Validate);
            // 
            // txtB2
            // 
            this.txtB2.AutoSize = false;
            this.txtB2.CharacterCasing = CharacterCasing.Upper;
            this.txtB2.LinkItem = null;
            this.txtB2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtB2.LinkTopic = null;
            this.txtB2.Location = new System.Drawing.Point(737, 530);
            this.txtB2.MaxLength = 8;
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(66, 40);
            this.txtB2.TabIndex = 24;
            this.ToolTip1.SetToolTip(this.txtB2, null);
            this.txtB2.Visible = false;
            this.txtB2.Enter += new System.EventHandler(this.txtB2_Enter);
            this.txtB2.Leave += new System.EventHandler(this.txtB2_Leave);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(30, 660);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(164, 40);
            this.cmdReturn.TabIndex = 11;
            this.cmdReturn.Text = "Return To Input";
            this.ToolTip1.SetToolTip(this.cmdReturn, null);
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // txtUTCExempt
            // 
            this.txtUTCExempt.AutoSize = false;
            this.txtUTCExempt.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCExempt.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCExempt.LinkItem = null;
            this.txtUTCExempt.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCExempt.LinkTopic = null;
            this.txtUTCExempt.Location = new System.Drawing.Point(757, 430);
            this.txtUTCExempt.Name = "txtUTCExempt";
            this.txtUTCExempt.Size = new System.Drawing.Size(40, 40);
            this.txtUTCExempt.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtUTCExempt, "A-Exempt Organization  B-Previously Used Elsewhere  C-Sales Tax Paid Elsewhere  D" +
        "-Other");
            this.txtUTCExempt.Enter += new System.EventHandler(this.txtUTCExempt_Enter);
            this.txtUTCExempt.Leave += new System.EventHandler(this.txtUTCExempt_Leave);
            this.txtUTCExempt.TextChanged += new System.EventHandler(this.txtUTCExempt_TextChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkRegistrant1);
            this.Frame1.Controls.Add(this.txtUTCSSN);
            this.Frame1.Controls.Add(this.txtUTCPurchaserName);
            this.Frame1.Controls.Add(this.txtUTCPurchaserCity);
            this.Frame1.Controls.Add(this.txtUTCPurcahserAddress);
            this.Frame1.Controls.Add(this.txtUTCPurchaserZip);
            this.Frame1.Controls.Add(this.txtUTCPurchaserState);
            this.Frame1.Controls.Add(this.txtUTCPurchaserZip4);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(554, 280);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Lessee Information";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // chkRegistrant1
            // 
            this.chkRegistrant1.Location = new System.Drawing.Point(20, 30);
            this.chkRegistrant1.Name = "chkRegistrant1";
            this.chkRegistrant1.Size = new System.Drawing.Size(401, 27);
            this.chkRegistrant1.TabIndex = 0;
            this.chkRegistrant1.Text = "Lessee is the same as Registrant #1 on registration";
            this.ToolTip1.SetToolTip(this.chkRegistrant1, null);
            this.chkRegistrant1.CheckedChanged += new System.EventHandler(this.chkRegistrant1_CheckedChanged);
            // 
            // txtUTCSSN
            // 
            this.txtUTCSSN.AutoSize = false;
            this.txtUTCSSN.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSSN.LinkItem = null;
            this.txtUTCSSN.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCSSN.LinkTopic = null;
            this.txtUTCSSN.Location = new System.Drawing.Point(141, 70);
            this.txtUTCSSN.Name = "txtUTCSSN";
            this.txtUTCSSN.Size = new System.Drawing.Size(288, 40);
            this.txtUTCSSN.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtUTCSSN, null);
            this.txtUTCSSN.Enter += new System.EventHandler(this.txtUTCSSN_Enter);
            this.txtUTCSSN.Leave += new System.EventHandler(this.txtUTCSSN_Leave);
            // 
            // txtUTCPurchaserName
            // 
            this.txtUTCPurchaserName.AutoSize = false;
            this.txtUTCPurchaserName.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCPurchaserName.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserName.LinkItem = null;
            this.txtUTCPurchaserName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCPurchaserName.LinkTopic = null;
            this.txtUTCPurchaserName.Location = new System.Drawing.Point(141, 120);
            this.txtUTCPurchaserName.Name = "txtUTCPurchaserName";
            this.txtUTCPurchaserName.Size = new System.Drawing.Size(288, 40);
            this.txtUTCPurchaserName.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtUTCPurchaserName, null);
            this.txtUTCPurchaserName.Enter += new System.EventHandler(this.txtUTCPurchaserName_Enter);
            this.txtUTCPurchaserName.Leave += new System.EventHandler(this.txtUTCPurchaserName_Leave);
            // 
            // txtUTCPurchaserCity
            // 
            this.txtUTCPurchaserCity.AutoSize = false;
            this.txtUTCPurchaserCity.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCPurchaserCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserCity.LinkItem = null;
            this.txtUTCPurchaserCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCPurchaserCity.LinkTopic = null;
            this.txtUTCPurchaserCity.Location = new System.Drawing.Point(141, 220);
            this.txtUTCPurchaserCity.Name = "txtUTCPurchaserCity";
            this.txtUTCPurchaserCity.Size = new System.Drawing.Size(155, 40);
            this.txtUTCPurchaserCity.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtUTCPurchaserCity, null);
            this.txtUTCPurchaserCity.Enter += new System.EventHandler(this.txtUTCPurchaserCity_Enter);
            this.txtUTCPurchaserCity.Leave += new System.EventHandler(this.txtUTCPurchaserCity_Leave);
            // 
            // txtUTCPurcahserAddress
            // 
            this.txtUTCPurcahserAddress.AutoSize = false;
            this.txtUTCPurcahserAddress.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCPurcahserAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurcahserAddress.LinkItem = null;
            this.txtUTCPurcahserAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCPurcahserAddress.LinkTopic = null;
            this.txtUTCPurcahserAddress.Location = new System.Drawing.Point(141, 170);
            this.txtUTCPurcahserAddress.Name = "txtUTCPurcahserAddress";
            this.txtUTCPurcahserAddress.Size = new System.Drawing.Size(288, 40);
            this.txtUTCPurcahserAddress.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtUTCPurcahserAddress, null);
            this.txtUTCPurcahserAddress.Enter += new System.EventHandler(this.txtUTCPurcahserAddress_Enter);
            this.txtUTCPurcahserAddress.Leave += new System.EventHandler(this.txtUTCPurcahserAddress_Leave);
            // 
            // txtUTCPurchaserZip
            // 
            this.txtUTCPurchaserZip.AutoSize = false;
            this.txtUTCPurchaserZip.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCPurchaserZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserZip.LinkItem = null;
            this.txtUTCPurchaserZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCPurchaserZip.LinkTopic = null;
            this.txtUTCPurchaserZip.Location = new System.Drawing.Point(375, 220);
            this.txtUTCPurchaserZip.Name = "txtUTCPurchaserZip";
            this.txtUTCPurchaserZip.Size = new System.Drawing.Size(80, 40);
            this.txtUTCPurchaserZip.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtUTCPurchaserZip, null);
            this.txtUTCPurchaserZip.Enter += new System.EventHandler(this.txtUTCPurchaserZip_Enter);
            this.txtUTCPurchaserZip.Leave += new System.EventHandler(this.txtUTCPurchaserZip_Leave);
            // 
            // txtUTCPurchaserState
            // 
            this.txtUTCPurchaserState.AutoSize = false;
            this.txtUTCPurchaserState.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCPurchaserState.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserState.LinkItem = null;
            this.txtUTCPurchaserState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCPurchaserState.LinkTopic = null;
            this.txtUTCPurchaserState.Location = new System.Drawing.Point(305, 220);
            this.txtUTCPurchaserState.Name = "txtUTCPurchaserState";
            this.txtUTCPurchaserState.Size = new System.Drawing.Size(60, 40);
            this.txtUTCPurchaserState.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtUTCPurchaserState, null);
            this.txtUTCPurchaserState.KeyUp += new Wisej.Web.KeyEventHandler(this.txtUTCPurchaserState_KeyUp);
            this.txtUTCPurchaserState.Enter += new System.EventHandler(this.txtUTCPurchaserState_Enter);
            this.txtUTCPurchaserState.Leave += new System.EventHandler(this.txtUTCPurchaserState_Leave);
            // 
            // txtUTCPurchaserZip4
            // 
            this.txtUTCPurchaserZip4.AutoSize = false;
            this.txtUTCPurchaserZip4.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCPurchaserZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserZip4.LinkItem = null;
            this.txtUTCPurchaserZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCPurchaserZip4.LinkTopic = null;
            this.txtUTCPurchaserZip4.Location = new System.Drawing.Point(464, 220);
            this.txtUTCPurchaserZip4.Name = "txtUTCPurchaserZip4";
            this.txtUTCPurchaserZip4.Size = new System.Drawing.Size(80, 40);
            this.txtUTCPurchaserZip4.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtUTCPurchaserZip4, null);
            this.txtUTCPurchaserZip4.Enter += new System.EventHandler(this.txtUTCPurchaserZip4_Enter);
            this.txtUTCPurchaserZip4.Leave += new System.EventHandler(this.txtUTCPurchaserZip4_Leave);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 134);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(50, 16);
            this.Label10.TabIndex = 40;
            this.Label10.Text = "NAME";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 184);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(65, 16);
            this.Label11.TabIndex = 39;
            this.Label11.Text = "ADDRESS";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 234);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(95, 16);
            this.Label12.TabIndex = 38;
            this.Label12.Text = "CITY/STATE/ZIP";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 84);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(23, 18);
            this.Label13.TabIndex = 37;
            this.Label13.Text = "SSN";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // fraLessor
            // 
            this.fraLessor.Controls.Add(this.txtUTCSellerZip4);
            this.fraLessor.Controls.Add(this.txtUTCSellerAddress);
            this.fraLessor.Controls.Add(this.txtUTCSellerCity);
            this.fraLessor.Controls.Add(this.txtUTCSellerState);
            this.fraLessor.Controls.Add(this.txtUTCSellerZip);
            this.fraLessor.Controls.Add(this.txtUTCSellerName);
            this.fraLessor.Controls.Add(this.txtUTCDate);
            this.fraLessor.Controls.Add(this.Label7);
            this.fraLessor.Controls.Add(this.Label14);
            this.fraLessor.Controls.Add(this.Label15);
            this.fraLessor.Controls.Add(this.Label16);
            this.fraLessor.Location = new System.Drawing.Point(31, 338);
            this.fraLessor.Name = "fraLessor";
            this.fraLessor.Size = new System.Drawing.Size(554, 243);
            this.fraLessor.TabIndex = 1;
            this.fraLessor.Text = "Lessor Information";
            this.ToolTip1.SetToolTip(this.fraLessor, null);
            // 
            // txtUTCSellerZip4
            // 
            this.txtUTCSellerZip4.AutoSize = false;
            this.txtUTCSellerZip4.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCSellerZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerZip4.LinkItem = null;
            this.txtUTCSellerZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCSellerZip4.LinkTopic = null;
            this.txtUTCSellerZip4.Location = new System.Drawing.Point(464, 180);
            this.txtUTCSellerZip4.Name = "txtUTCSellerZip4";
            this.txtUTCSellerZip4.Size = new System.Drawing.Size(80, 40);
            this.txtUTCSellerZip4.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtUTCSellerZip4, null);
            this.txtUTCSellerZip4.Enter += new System.EventHandler(this.txtUTCSellerZip4_Enter);
            this.txtUTCSellerZip4.Leave += new System.EventHandler(this.txtUTCSellerZip4_Leave);
            // 
            // txtUTCSellerAddress
            // 
            this.txtUTCSellerAddress.AutoSize = false;
            this.txtUTCSellerAddress.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCSellerAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerAddress.LinkItem = null;
            this.txtUTCSellerAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCSellerAddress.LinkTopic = null;
            this.txtUTCSellerAddress.Location = new System.Drawing.Point(141, 130);
            this.txtUTCSellerAddress.Name = "txtUTCSellerAddress";
            this.txtUTCSellerAddress.Size = new System.Drawing.Size(279, 40);
            this.txtUTCSellerAddress.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtUTCSellerAddress, null);
            this.txtUTCSellerAddress.Enter += new System.EventHandler(this.txtUTCSellerAddress_Enter);
            this.txtUTCSellerAddress.Leave += new System.EventHandler(this.txtUTCSellerAddress_Leave);
            // 
            // txtUTCSellerCity
            // 
            this.txtUTCSellerCity.AutoSize = false;
            this.txtUTCSellerCity.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCSellerCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerCity.LinkItem = null;
            this.txtUTCSellerCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCSellerCity.LinkTopic = null;
            this.txtUTCSellerCity.Location = new System.Drawing.Point(141, 180);
            this.txtUTCSellerCity.Name = "txtUTCSellerCity";
            this.txtUTCSellerCity.Size = new System.Drawing.Size(155, 40);
            this.txtUTCSellerCity.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtUTCSellerCity, null);
            this.txtUTCSellerCity.Enter += new System.EventHandler(this.txtUTCSellerCity_Enter);
            this.txtUTCSellerCity.Leave += new System.EventHandler(this.txtUTCSellerCity_Leave);
            // 
            // txtUTCSellerState
            // 
            this.txtUTCSellerState.AutoSize = false;
            this.txtUTCSellerState.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCSellerState.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerState.LinkItem = null;
            this.txtUTCSellerState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCSellerState.LinkTopic = null;
            this.txtUTCSellerState.Location = new System.Drawing.Point(305, 180);
            this.txtUTCSellerState.Name = "txtUTCSellerState";
            this.txtUTCSellerState.Size = new System.Drawing.Size(60, 40);
            this.txtUTCSellerState.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtUTCSellerState, null);
            this.txtUTCSellerState.Enter += new System.EventHandler(this.txtUTCSellerState_Enter);
            this.txtUTCSellerState.Leave += new System.EventHandler(this.txtUTCSellerState_Leave);
            // 
            // txtUTCSellerZip
            // 
            this.txtUTCSellerZip.AutoSize = false;
            this.txtUTCSellerZip.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCSellerZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerZip.LinkItem = null;
            this.txtUTCSellerZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCSellerZip.LinkTopic = null;
            this.txtUTCSellerZip.Location = new System.Drawing.Point(375, 180);
            this.txtUTCSellerZip.Name = "txtUTCSellerZip";
            this.txtUTCSellerZip.Size = new System.Drawing.Size(80, 40);
            this.txtUTCSellerZip.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtUTCSellerZip, null);
            this.txtUTCSellerZip.Enter += new System.EventHandler(this.txtUTCSellerZip_Enter);
            this.txtUTCSellerZip.Leave += new System.EventHandler(this.txtUTCSellerZip_Leave);
            // 
            // txtUTCSellerName
            // 
            this.txtUTCSellerName.AutoSize = false;
            this.txtUTCSellerName.CharacterCasing = CharacterCasing.Upper;
            this.txtUTCSellerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerName.LinkItem = null;
            this.txtUTCSellerName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtUTCSellerName.LinkTopic = null;
            this.txtUTCSellerName.Location = new System.Drawing.Point(141, 80);
            this.txtUTCSellerName.Name = "txtUTCSellerName";
            this.txtUTCSellerName.Size = new System.Drawing.Size(279, 40);
            this.txtUTCSellerName.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtUTCSellerName, null);
            this.txtUTCSellerName.Enter += new System.EventHandler(this.txtUTCSellerName_Enter);
            this.txtUTCSellerName.Leave += new System.EventHandler(this.txtUTCSellerName_Leave);
            // 
            // txtUTCDate
            // 
            this.txtUTCDate.Location = new System.Drawing.Point(141, 30);
            this.txtUTCDate.Mask = "##/##/####";
            this.txtUTCDate.Name = "txtUTCDate";
            this.txtUTCDate.Size = new System.Drawing.Size(115, 40);
            this.txtUTCDate.TabIndex = 0;
            this.txtUTCDate.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.txtUTCDate, null);
            this.txtUTCDate.Enter += new System.EventHandler(this.txtUTCDate_Enter);
            this.txtUTCDate.Leave += new System.EventHandler(this.txtUTCDate_Leave);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(100, 16);
            this.Label7.TabIndex = 56;
            this.Label7.Text = "PURCHASE DATE";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 94);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(45, 16);
            this.Label14.TabIndex = 35;
            this.Label14.Text = "NAME";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(20, 144);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(85, 18);
            this.Label15.TabIndex = 34;
            this.Label15.Text = "ADDRESS";
            this.ToolTip1.SetToolTip(this.Label15, null);
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(20, 194);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(37, 18);
            this.Label16.TabIndex = 33;
            this.Label16.Text = "C/S/Z";
            this.ToolTip1.SetToolTip(this.Label16, null);
            // 
            // txtB1
            // 
			this.txtB1.MaxLength = 2;
            this.txtB1.AutoSize = false;
            this.txtB1.CharacterCasing = CharacterCasing.Upper;
            this.txtB1.BackColor = System.Drawing.SystemColors.Window;
            this.txtB1.LinkItem = null;
            this.txtB1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtB1.LinkTopic = null;
            this.txtB1.Location = new System.Drawing.Point(707, 480);
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(40, 40);
            this.txtB1.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.txtB1, null);
            this.txtB1.Visible = false;
            this.txtB1.Enter += new System.EventHandler(this.txtB1_Enter);
            this.txtB1.Leave += new System.EventHandler(this.txtB1_Leave);
            // 
            // txtC1
            // 
			this.txtC1.MaxLength = 2;
            this.txtC1.AutoSize = false;
            this.txtC1.CharacterCasing = CharacterCasing.Upper;
            this.txtC1.BackColor = System.Drawing.SystemColors.Window;
            this.txtC1.LinkItem = null;
            this.txtC1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtC1.LinkTopic = null;
            this.txtC1.Location = new System.Drawing.Point(757, 480);
            this.txtC1.Name = "txtC1";
            this.txtC1.Size = new System.Drawing.Size(40, 40);
            this.txtC1.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.txtC1, null);
            this.txtC1.Visible = false;
            this.txtC1.Enter += new System.EventHandler(this.txtC1_Enter);
            this.txtC1.Leave += new System.EventHandler(this.txtC1_Leave);
            // 
            // txtD1
            // 
			this.txtD1.MaxLength = 30;
            this.txtD1.AutoSize = false;
            this.txtD1.CharacterCasing = CharacterCasing.Upper;
            this.txtD1.BackColor = System.Drawing.SystemColors.Window;
            this.txtD1.LinkItem = null;
            this.txtD1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtD1.LinkTopic = null;
            this.txtD1.Location = new System.Drawing.Point(757, 480);
            this.txtD1.Name = "txtD1";
            this.txtD1.Size = new System.Drawing.Size(238, 40);
            this.txtD1.TabIndex = 28;
            this.ToolTip1.SetToolTip(this.txtD1, null);
            this.txtD1.Visible = false;
            this.txtD1.Enter += new System.EventHandler(this.txtD1_Enter);
            this.txtD1.Leave += new System.EventHandler(this.txtD1_Leave);
            // 
            // txtC2
            // 
            this.txtC2.Location = new System.Drawing.Point(757, 530);
            this.txtC2.MaxLength = 8;
            this.txtC2.Name = "txtC2";
            this.txtC2.Size = new System.Drawing.Size(70, 40);
            this.txtC2.TabIndex = 27;
            this.txtC2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtC2, null);
            this.txtC2.Visible = false;
            this.txtC2.Enter += new System.EventHandler(this.txtC2_Enter);
            this.txtC2.Leave += new System.EventHandler(this.txtC2_Leave);
            this.txtC2.Validating += new System.ComponentModel.CancelEventHandler(this.txtC2_Validate);
            // 
            // txtUTCUseTax
            // 
            this.txtUTCUseTax.Location = new System.Drawing.Point(757, 380);
            this.txtUTCUseTax.MaxLength = 10;
            this.txtUTCUseTax.Name = "txtUTCUseTax";
            this.txtUTCUseTax.Size = new System.Drawing.Size(108, 40);
            this.txtUTCUseTax.TabIndex = 9;
            this.txtUTCUseTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtUTCUseTax, null);
            this.txtUTCUseTax.Enter += new System.EventHandler(this.txtUTCUseTax_Enter);
            this.txtUTCUseTax.Leave += new System.EventHandler(this.txtUTCUseTax_Leave);
            // 
            // txtUTCNetAmount
            // 
            this.txtUTCNetAmount.Location = new System.Drawing.Point(757, 280);
            this.txtUTCNetAmount.MaxLength = 11;
            this.txtUTCNetAmount.Name = "txtUTCNetAmount";
            this.txtUTCNetAmount.Size = new System.Drawing.Size(108, 40);
            this.txtUTCNetAmount.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtUTCNetAmount, null);
            this.txtUTCNetAmount.Enter += new System.EventHandler(this.txtUTCNetAmount_Enter);
            this.txtUTCNetAmount.Leave += new System.EventHandler(this.txtUTCNetAmount_Leave);
            this.txtUTCNetAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCNetAmount_Validate);
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(757, 580);
            this.txtB3.Mask = "##/##/####";
            this.txtB3.MaxLength = 10;
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(115, 40);
            this.txtB3.TabIndex = 25;
            this.txtB3.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.txtB3, null);
            this.txtB3.Visible = false;
            this.txtB3.Enter += new System.EventHandler(this.txtB3_Enter);
            this.txtB3.Leave += new System.EventHandler(this.txtB3_Leave);
            // 
            // txtUTCAllowance
            // 
            this.txtUTCAllowance.Location = new System.Drawing.Point(757, 230);
            this.txtUTCAllowance.MaxLength = 10;
            this.txtUTCAllowance.Name = "txtUTCAllowance";
            this.txtUTCAllowance.Size = new System.Drawing.Size(108, 40);
            this.txtUTCAllowance.TabIndex = 6;
            this.txtUTCAllowance.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtUTCAllowance, null);
            this.txtUTCAllowance.Enter += new System.EventHandler(this.txtUTCAllowance_Enter);
            this.txtUTCAllowance.Leave += new System.EventHandler(this.txtUTCAllowance_Leave);
            this.txtUTCAllowance.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCAllowance_Validate);
            // 
            // txtPayment
            // 
            this.txtPayment.Location = new System.Drawing.Point(757, 30);
            this.txtPayment.MaxLength = 10;
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(108, 40);
            this.txtPayment.TabIndex = 3;
            this.txtPayment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtPayment, null);
            this.txtPayment.Enter += new System.EventHandler(this.txtPayment_Enter);
            this.txtPayment.Leave += new System.EventHandler(this.txtPayment_Leave);
            // 
            // txtDownPayment
            // 
            this.txtDownPayment.Location = new System.Drawing.Point(757, 130);
            this.txtDownPayment.MaxLength = 10;
            this.txtDownPayment.Name = "txtDownPayment";
            this.txtDownPayment.Size = new System.Drawing.Size(108, 40);
            this.txtDownPayment.TabIndex = 12;
            this.txtDownPayment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtDownPayment, null);
            this.txtDownPayment.Enter += new System.EventHandler(this.txtDownPayment_Enter);
            this.txtDownPayment.Leave += new System.EventHandler(this.txtDownPayment_Leave);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(599, 243);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(20, 16);
            this.Label9.TabIndex = 64;
            this.Label9.Text = "+";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(599, 344);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(20, 16);
            this.Label8.TabIndex = 63;
            this.Label8.Text = "X";
            this.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(599, 144);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(20, 16);
            this.Label5.TabIndex = 62;
            this.Label5.Text = "+";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(599, 95);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(20, 16);
            this.Label4.TabIndex = 61;
            this.Label4.Text = "X";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(623, 144);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 16);
            this.Label3.TabIndex = 60;
            this.Label3.Text = "DOWN PAYMENT";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(623, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(100, 16);
            this.Label2.TabIndex = 58;
            this.Label2.Text = "# OF PAYMENTS";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(623, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 16);
            this.Label1.TabIndex = 57;
            this.Label1.Text = "LEASE PAYMENT";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(623, 344);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(85, 16);
            this.Label18.TabIndex = 54;
            this.Label18.Text = "    TAX RATE";
            this.ToolTip1.SetToolTip(this.Label18, null);
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(622, 194);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(85, 16);
            this.Label19.TabIndex = 53;
            this.Label19.Text = "PRICE";
            this.ToolTip1.SetToolTip(this.Label19, null);
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(622, 244);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(85, 16);
            this.Label20.TabIndex = 52;
            this.Label20.Text = "ALLOWANCE";
            this.ToolTip1.SetToolTip(this.Label20, null);
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(622, 394);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(85, 16);
            this.Label21.TabIndex = 51;
            this.Label21.Text = "USE TAX";
            this.ToolTip1.SetToolTip(this.Label21, null);
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(599, 444);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(120, 16);
            this.Label22.TabIndex = 50;
            this.Label22.Text = "EXEMPT (ABCD)";
            this.ToolTip1.SetToolTip(this.Label22, null);
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(622, 294);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(100, 16);
            this.Label23.TabIndex = 49;
            this.Label23.Text = "TOTAL AMOUNT";
            this.ToolTip1.SetToolTip(this.Label23, null);
            // 
            // lblA1
            // 
            this.lblA1.Location = new System.Drawing.Point(502, 494);
            this.lblA1.Name = "lblA1";
            this.lblA1.Size = new System.Drawing.Size(125, 16);
            this.lblA1.TabIndex = 48;
            this.lblA1.Text = "EXEMPTION NUMBER";
            this.ToolTip1.SetToolTip(this.lblA1, null);
            this.lblA1.Visible = false;
            // 
            // lblB2
            // 
            this.lblB2.Location = new System.Drawing.Point(499, 544);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(140, 16);
            this.lblB2.TabIndex = 47;
            this.lblB2.Text = "REGISTRATION NUMBER";
            this.ToolTip1.SetToolTip(this.lblB2, null);
            this.lblB2.Visible = false;
            // 
            // lblB3
            // 
            this.lblB3.Location = new System.Drawing.Point(499, 594);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(120, 16);
            this.lblB3.TabIndex = 46;
            this.lblB3.Text = "REGISTRATION DATE";
            this.ToolTip1.SetToolTip(this.lblB3, null);
            this.lblB3.Visible = false;
            // 
            // lblB1
            // 
            this.lblB1.Location = new System.Drawing.Point(502, 495);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(120, 16);
            this.lblB1.TabIndex = 45;
            this.lblB1.Text = "PREVIOUS STATE";
            this.ToolTip1.SetToolTip(this.lblB1, null);
            this.lblB1.Visible = false;
            // 
            // lblC1
            // 
            this.lblC1.Location = new System.Drawing.Point(502, 495);
            this.lblC1.Name = "lblC1";
            this.lblC1.Size = new System.Drawing.Size(120, 16);
            this.lblC1.TabIndex = 44;
            this.lblC1.Text = "STATE";
            this.ToolTip1.SetToolTip(this.lblC1, null);
            this.lblC1.Visible = false;
            // 
            // lblC2
            // 
            this.lblC2.Location = new System.Drawing.Point(499, 544);
            this.lblC2.Name = "lblC2";
            this.lblC2.Size = new System.Drawing.Size(120, 16);
            this.lblC2.TabIndex = 43;
            this.lblC2.Text = "AMOUNT";
            this.ToolTip1.SetToolTip(this.lblC2, null);
            this.lblC2.Visible = false;
            // 
            // lblD1
            // 
            this.lblD1.Location = new System.Drawing.Point(502, 495);
            this.lblD1.Name = "lblD1";
            this.lblD1.Size = new System.Drawing.Size(120, 16);
            this.lblD1.TabIndex = 42;
            this.lblD1.Text = "OTHER  (DETAIL)";
            this.ToolTip1.SetToolTip(this.lblD1, null);
            this.lblD1.Visible = false;
            // 
            // lblExemptCode
            // 
            this.lblExemptCode.Location = new System.Drawing.Point(716, 444);
            this.lblExemptCode.Name = "lblExemptCode";
            this.lblExemptCode.Size = new System.Drawing.Size(158, 16);
            this.lblExemptCode.TabIndex = 41;
            this.ToolTip1.SetToolTip(this.lblExemptCode, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSaveExit,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 0;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdFileSaveExit
            // 
            this.cmdFileSaveExit.AppearanceKey = "acceptButton";
            this.cmdFileSaveExit.Location = new System.Drawing.Point(414, 30);
            this.cmdFileSaveExit.Name = "cmdFileSaveExit";
            this.cmdFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileSaveExit.Size = new System.Drawing.Size(129, 48);
            this.cmdFileSaveExit.TabIndex = 0;
            this.cmdFileSaveExit.Text = "Save & Exit";
            this.ToolTip1.SetToolTip(this.cmdFileSaveExit, null);
            this.cmdFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // frmLeaseUseTax
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(985, 641);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmLeaseUseTax";
            this.Text = "Use Tax";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmLeaseUseTax_Load);
            this.Activated += new System.EventHandler(this.frmLeaseUseTax_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLeaseUseTax_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLeaseUseTax_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegistrant1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraLessor)).EndInit();
            this.fraLessor.ResumeLayout(false);
            this.fraLessor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCUseTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCNetAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCAllowance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDownPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSaveExit;
	}
}
