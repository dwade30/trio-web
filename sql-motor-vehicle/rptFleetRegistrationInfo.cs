﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptFleetRegistrationInfo.
	/// </summary>
	public partial class rptFleetRegistrationInfo : BaseSectionReport
	{
		public rptFleetRegistrationInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Fleet Registration Information";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptFleetRegistrationInfo InstancePtr
		{
			get
			{
				return (rptFleetRegistrationInfo)Sys.GetInstance(typeof(rptFleetRegistrationInfo));
			}
		}

		protected rptFleetRegistrationInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptFleetRegistrationInfo	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: LocalFee As Decimal	OnWrite
		Decimal LocalFee;
		// vbPorter upgrade warning: StateFee As Decimal	OnWrite
		Decimal StateFee;
		// vbPorter upgrade warning: TotalAgent As Decimal	OnWrite
		Decimal TotalAgent;
		// vbPorter upgrade warning: TotalExcise As Decimal	OnWrite
		Decimal TotalExcise;
		// vbPorter upgrade warning: TotalReg As Decimal	OnWrite
		Decimal TotalReg;
		// vbPorter upgrade warning: TotalInitial As Decimal	OnWrite
		Decimal TotalInitial;
		// vbPorter upgrade warning: TotalSpecialty As Decimal	OnWrite
		Decimal TotalSpecialty;
		// vbPorter upgrade warning: TotalCommCredit As Decimal	OnWrite
		Decimal TotalCommCredit;
		// vbPorter upgrade warning: TotalLocal As Decimal	OnWrite
		Decimal TotalLocal;
		// vbPorter upgrade warning: TotalState As Decimal	OnWrite
		Decimal TotalState;
		int PageCounter;
		bool blnFirstRecord;
		int RegisterCol;
		int ClassCol;
		int VINCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int UnitCol;
		int LocalCol;
		int StateCol;
		int TotalCol;
		int CountCol;
		int AgentCol;
		int ExciseCol;
		int TotalLocalCol;
		int RegCol;
		int InitialCol;
		int SpecialtyCol;
		int TotalStateCol;
		int GrandTotalCol;
		int lngRowCounter;
		bool blnSummaryPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (FCConvert.ToBoolean(frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, RegisterCol)) == true)
				{
					eArgs.EOF = false;
				}
				else
				{
					lngRowCounter += 1;
					CheckRecord:
					;
					if (lngRowCounter <= frmPrintFleet.InstancePtr.vsVehicles.Rows - 1)
					{
						if (FCConvert.ToBoolean(frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, RegisterCol)) == true)
						{
							eArgs.EOF = false;
						}
						else
						{
							lngRowCounter += 1;
							goto CheckRecord;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				lngRowCounter += 1;
				CheckRecord:
				;
				if (lngRowCounter <= frmPrintFleet.InstancePtr.vsVehicles.Rows - 1)
				{
					if (FCConvert.ToBoolean(frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, RegisterCol)) == true)
					{
						eArgs.EOF = false;
					}
					else
					{
						lngRowCounter += 1;
						goto CheckRecord;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			if (eArgs.EOF)
			{
				blnSummaryPage = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label11 As object	OnWrite(string)
			// vbPorter upgrade warning: Label8 As object	OnWrite(string)
			// vbPorter upgrade warning: RegisterCol As object	OnWrite
			// vbPorter upgrade warning: lblFleetNumber As object	OnWrite(string)
			// vbPorter upgrade warning: lblFleetOwner As object	OnWrite(string)
			// vbPorter upgrade warning: lblExpirationMonth As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label11.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label8.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			RegisterCol = 0;
			ClassCol = 1;
			VINCol = 2;
			YearCol = 3;
			MakeCol = 4;
			ModelCol = 5;
			UnitCol = 6;
			LocalCol = 7;
			StateCol = 8;
			TotalCol = 9;
			CountCol = 0;
			AgentCol = 1;
			ExciseCol = 2;
			TotalLocalCol = 3;
			RegCol = 4;
			InitialCol = 5;
			SpecialtyCol = 6;
			TotalStateCol = 7;
			GrandTotalCol = 8;
			LocalFee = 0;
			StateFee = 0;
			TotalLocal = 0;
			TotalState = 0;
			TotalAgent = 0;
			TotalExcise = 0;
			TotalReg = 0;
			TotalInitial = 0;
			TotalSpecialty = 0;
			TotalCommCredit = 0;
			PageCounter = 0;
			lngRowCounter = 1;
			blnFirstRecord = true;
			blnSummaryPage = false;
			if (frmPrintFleet.InstancePtr.cmbFleet.Text == "Fleet")
			{
				lblFleetNumber.Text = "FLEET " + fecherFoundation.Strings.Trim(Strings.Left(frmPrintFleet.InstancePtr.cboFleets.Text, 5));
				lblUnit.Text = "Unit";
			}
			else
			{
				lblFleetNumber.Text = "GROUP " + fecherFoundation.Strings.Trim(Strings.Left(frmPrintFleet.InstancePtr.cboFleets.Text, 5));
				lblUnit.Text = "Expires";
			}
			lblFleetOwner.Text = Strings.Right(frmPrintFleet.InstancePtr.cboFleets.Text, frmPrintFleet.InstancePtr.cboFleets.Text.Length - 5);
			if (frmPrintFleet.InstancePtr.cmbFleet.Text == "Fleet")
			{
				if (frmPrintFleet.InstancePtr.ExpMonth < 10)
				{
					lblExpirationMonth.Text = "Expiration Month: 0" + FCConvert.ToString(frmPrintFleet.InstancePtr.ExpMonth);
				}
				else
				{
					lblExpirationMonth.Text = "Expiration Month: " + FCConvert.ToString(frmPrintFleet.InstancePtr.ExpMonth);
				}
			}
			else
			{
				lblExpirationMonth.Text = "";
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldClassPlate As object	OnWrite(string)
			// vbPorter upgrade warning: fldVIN As object	OnWrite(string)
			// vbPorter upgrade warning: fldYear As object	OnWrite(string)
			// vbPorter upgrade warning: fldMake As object	OnWrite(string)
			// vbPorter upgrade warning: fldModel As object	OnWrite(string)
			// vbPorter upgrade warning: fldUnit As object	OnWrite(string)
			// vbPorter upgrade warning: fldLocal As object	OnWrite(string)
			// vbPorter upgrade warning: fldState As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotal As object	OnWrite(string)
			fldClassPlate.Text = frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, ClassCol);
			fldVIN.Text = frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, VINCol);
			fldYear.Text = frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, YearCol);
			fldMake.Text = frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, MakeCol);
			fldModel.Text = frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, ModelCol).Left(6);
			fldUnit.Text = frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, UnitCol);
			fldLocal.Text = Strings.Format(frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, LocalCol), "#,##0.00");
			fldState.Text = Strings.Format(frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, StateCol), "#,##0.00");
			fldTotal.Text = Strings.Format(frmPrintFleet.InstancePtr.vsVehicles.TextMatrix(lngRowCounter, TotalCol), "#,##0.00");
			if (frmPrintFleet.InstancePtr.vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, TotalCol) == ColorTranslator.ToOle(Color.Red))
			{
				lblBadFees.Visible = true;
			}
			else
			{
				lblBadFees.Visible = false;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalAgentFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalExcise As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalLocalSummary As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalRegistrationFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalInitialFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalSpecialtyFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalStateSummary As object	OnWrite(string)
			// vbPorter upgrade warning: fldGrandTotal As object	OnWrite(string)
			fldTotalAgentFee.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, AgentCol), "#,##0.00");
			fldTotalExcise.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, ExciseCol), "#,##0.00");
			fldTotalLocalSummary.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, TotalLocalCol), "#,##0.00");
			fldTotalRegistrationFee.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, RegCol), "#,##0.00");
			fldTotalInitialFee.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, InitialCol), "#,##0.00");
			fldTotalSpecialtyFee.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, SpecialtyCol), "#,##0.00");
			fldTotalStateSummary.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, TotalStateCol), "#,##0.00");
			fldGrandTotal.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, GrandTotalCol), "#,##0.00");
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalLocal As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalState As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalTotal As object	OnWrite(string)
			fldTotalLocal.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, TotalLocalCol), "#,##0.00");
			fldTotalState.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, TotalStateCol), "#,##0.00");
			fldTotalTotal.Text = Strings.Format(frmPrintFleet.InstancePtr.vsSummary.TextMatrix(1, GrandTotalCol), "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
			if (blnSummaryPage)
			{
				Label1.Text = "Fleet Registration Summary";
				Label15.Visible = false;
				Label16.Visible = false;
				Label17.Visible = false;
				Label18.Visible = false;
				Label19.Visible = false;
				lblUnit.Visible = false;
				Label21.Visible = false;
				Label22.Visible = false;
				Label23.Visible = false;
				Line2.Visible = false;
			}
		}

		private void rptFleetRegistrationInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptFleetRegistrationInfo properties;
			//rptFleetRegistrationInfo.Caption	= "Fleet Registration Information";
			//rptFleetRegistrationInfo.Icon	= "rptFleetRegistrationInfo.dsx":0000";
			//rptFleetRegistrationInfo.Left	= 0;
			//rptFleetRegistrationInfo.Top	= 0;
			//rptFleetRegistrationInfo.Width	= 11880;
			//rptFleetRegistrationInfo.Height	= 8595;
			//rptFleetRegistrationInfo.StartUpPosition	= 3;
			//rptFleetRegistrationInfo.SectionData	= "rptFleetRegistrationInfo.dsx":058A;
			//End Unmaped Properties
		}
	}
}
