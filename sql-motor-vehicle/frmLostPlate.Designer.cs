//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmLostPlate.
	/// </summary>
	partial class frmLostPlate
	{
		public fecherFoundation.FCComboBox cmb1;
		public fecherFoundation.FCLabel lbl1;
		public fecherFoundation.FCFrame fraInstructions;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblOwed;
		public fecherFoundation.FCTextBox txtExpires;
		public fecherFoundation.FCTextBox txtYStickerNumber;
		public fecherFoundation.FCTextBox txtMStickerNumber;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCTextBox txtMonthCharge;
		public fecherFoundation.FCTextBox txtYearCharge;
		public fecherFoundation.FCTextBox txtMonthNoCharge;
		public fecherFoundation.FCTextBox txtYearNoCharge;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblInfo;
		public fecherFoundation.FCLabel Label43;
		public fecherFoundation.FCLabel lblM;
		public fecherFoundation.FCLabel lblY;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label51;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmb1 = new fecherFoundation.FCComboBox();
			this.lbl1 = new fecherFoundation.FCLabel();
			this.fraInstructions = new fecherFoundation.FCFrame();
			this.cmdOK = new fecherFoundation.FCButton();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.lblAmount = new fecherFoundation.FCLabel();
			this.lblOwed = new fecherFoundation.FCLabel();
			this.txtExpires = new fecherFoundation.FCTextBox();
			this.txtYStickerNumber = new fecherFoundation.FCTextBox();
			this.txtMStickerNumber = new fecherFoundation.FCTextBox();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.txtMonthCharge = new fecherFoundation.FCTextBox();
			this.txtYearCharge = new fecherFoundation.FCTextBox();
			this.txtMonthNoCharge = new fecherFoundation.FCTextBox();
			this.txtYearNoCharge = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblInfo = new fecherFoundation.FCLabel();
			this.Label43 = new fecherFoundation.FCLabel();
			this.lblM = new fecherFoundation.FCLabel();
			this.lblY = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label51 = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInstructions)).BeginInit();
			this.fraInstructions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 338);
			this.BottomPanel.Size = new System.Drawing.Size(460, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.BackColor = System.Drawing.Color.White;
			this.ClientArea.Controls.Add(this.txtExpires);
			this.ClientArea.Controls.Add(this.txtYStickerNumber);
			this.ClientArea.Controls.Add(this.txtMStickerNumber);
			this.ClientArea.Controls.Add(this.cmb1);
			this.ClientArea.Controls.Add(this.lbl1);
			this.ClientArea.Controls.Add(this.txtMonthCharge);
			this.ClientArea.Controls.Add(this.txtYearCharge);
			this.ClientArea.Controls.Add(this.txtMonthNoCharge);
			this.ClientArea.Controls.Add(this.txtYearNoCharge);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblInfo);
			this.ClientArea.Controls.Add(this.Label43);
			this.ClientArea.Controls.Add(this.lblM);
			this.ClientArea.Controls.Add(this.lblY);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label51);
			this.ClientArea.Controls.Add(this.fraInstructions);
			this.ClientArea.Size = new System.Drawing.Size(460, 278);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(460, 60);
			// 
			// cmb1
			// 
			this.cmb1.AutoSize = false;
			this.cmb1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmb1.FormattingEnabled = true;
			this.cmb1.Items.AddRange(new object[] {
            "1",
            "2"});
			this.cmb1.Location = new System.Drawing.Point(268, 90);
			this.cmb1.Name = "cmb1";
			this.cmb1.Size = new System.Drawing.Size(160, 40);
			this.cmb1.TabIndex = 19;
			this.cmb1.Text = "2";
			this.cmb1.ToolTipText = null;
			this.cmb1.SelectedIndexChanged += new System.EventHandler(this.cmb1_SelectedIndexChanged);
			// 
			// lbl1
			// 
			this.lbl1.AutoSize = true;
			this.lbl1.Location = new System.Drawing.Point(30, 104);
			this.lbl1.Name = "lbl1";
			this.lbl1.Size = new System.Drawing.Size(193, 15);
			this.lbl1.TabIndex = 20;
			this.lbl1.Text = "NUMBER OF PLATES TO ISSUE";
			this.lbl1.ToolTipText = null;
			// 
			// fraInstructions
			// 
			this.fraInstructions.AppearanceKey = "groupBoxNoBorders";
			this.fraInstructions.BackColor = System.Drawing.Color.White;
			this.fraInstructions.Controls.Add(this.cmdOK);
			this.fraInstructions.Controls.Add(this.lblInstructions);
			this.fraInstructions.Controls.Add(this.lblAmount);
			this.fraInstructions.Controls.Add(this.lblOwed);
			this.fraInstructions.Location = new System.Drawing.Point(0, 0);
			this.fraInstructions.Name = "fraInstructions";
			this.fraInstructions.Size = new System.Drawing.Size(460, 278);
			this.fraInstructions.TabIndex = 18;
			this.fraInstructions.Visible = false;
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.Location = new System.Drawing.Point(30, 101);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(64, 40);
			this.cmdOK.TabIndex = 22;
			this.cmdOK.Text = "OK";
			this.cmdOK.ToolTipText = null;
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 65);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(270, 16);
			this.lblInstructions.TabIndex = 21;
			this.lblInstructions.Text = "PLEASE BE SURE TO FILL OUT AN MV-9 FORM";
			this.lblInstructions.ToolTipText = null;
			// 
			// lblAmount
			// 
			this.lblAmount.Location = new System.Drawing.Point(201, 30);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Size = new System.Drawing.Size(84, 15);
			this.lblAmount.TabIndex = 20;
			this.lblAmount.ToolTipText = null;
			// 
			// lblOwed
			// 
			this.lblOwed.Location = new System.Drawing.Point(30, 30);
			this.lblOwed.Name = "lblOwed";
			this.lblOwed.Size = new System.Drawing.Size(152, 20);
			this.lblOwed.TabIndex = 19;
			this.lblOwed.Text = "AMOUNT OWED: ";
			this.lblOwed.ToolTipText = null;
			// 
			// txtExpires
			// 
			this.txtExpires.AutoSize = false;
			this.txtExpires.BackColor = System.Drawing.SystemColors.Window;
			this.txtExpires.LinkItem = null;
			this.txtExpires.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtExpires.LinkTopic = null;
			this.txtExpires.Location = new System.Drawing.Point(30, 30);
			this.txtExpires.Name = "txtExpires";
			this.txtExpires.Size = new System.Drawing.Size(398, 40);
			this.txtExpires.TabIndex = 16;
			this.txtExpires.ToolTipText = null;
			this.txtExpires.Visible = false;
			// 
			// txtYStickerNumber
			// 
			this.txtYStickerNumber.AutoSize = false;
			this.txtYStickerNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtYStickerNumber.LinkItem = null;
			this.txtYStickerNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYStickerNumber.LinkTopic = null;
			this.txtYStickerNumber.Location = new System.Drawing.Point(315, 225);
			this.txtYStickerNumber.Name = "txtYStickerNumber";
			this.txtYStickerNumber.Size = new System.Drawing.Size(113, 40);
			this.txtYStickerNumber.TabIndex = 5;
			this.txtYStickerNumber.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtYStickerNumber.ToolTipText = null;
			this.txtYStickerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYStickerNumber_KeyPress);
			this.txtYStickerNumber.KeyUp += new Wisej.Web.KeyEventHandler(this.txtYStickerNumber_KeyUp);
			this.txtYStickerNumber.Enter += new System.EventHandler(this.txtYStickerNumber_Enter);
			this.txtYStickerNumber.Leave += new System.EventHandler(this.txtYStickerNumber_Leave);
			this.txtYStickerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtYStickerNumber_Validating);
			// 
			// txtMStickerNumber
			// 
			this.txtMStickerNumber.AutoSize = false;
			this.txtMStickerNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtMStickerNumber.LinkItem = null;
			this.txtMStickerNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMStickerNumber.LinkTopic = null;
			this.txtMStickerNumber.Location = new System.Drawing.Point(315, 175);
			this.txtMStickerNumber.Name = "txtMStickerNumber";
			this.txtMStickerNumber.Size = new System.Drawing.Size(113, 40);
			this.txtMStickerNumber.TabIndex = 2;
			this.txtMStickerNumber.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMStickerNumber.ToolTipText = null;
			this.txtMStickerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMStickerNumber_KeyPress);
			this.txtMStickerNumber.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMStickerNumber_KeyUp);
			this.txtMStickerNumber.Enter += new System.EventHandler(this.txtMStickerNumber_Enter);
			this.txtMStickerNumber.Leave += new System.EventHandler(this.txtMStickerNumber_Leave);
			this.txtMStickerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMStickerNumber_Validating);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(162, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(100, 48);
			this.cmdProcess.TabIndex = 6;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.ToolTipText = null;
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// txtMonthCharge
			// 
			this.txtMonthCharge.AutoSize = false;
			this.txtMonthCharge.BackColor = System.Drawing.SystemColors.Window;
			this.txtMonthCharge.LinkItem = null;
			this.txtMonthCharge.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMonthCharge.LinkTopic = null;
			this.txtMonthCharge.Location = new System.Drawing.Point(175, 175);
			this.txtMonthCharge.MaxLength = 1;
			this.txtMonthCharge.Name = "txtMonthCharge";
			this.txtMonthCharge.Size = new System.Drawing.Size(50, 40);
			this.txtMonthCharge.TabIndex = 0;
			this.txtMonthCharge.Text = "2";
			this.txtMonthCharge.ToolTipText = null;
			this.txtMonthCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthCharge_Validating);
			// 
			// txtYearCharge
			// 
			this.txtYearCharge.AutoSize = false;
			this.txtYearCharge.BackColor = System.Drawing.SystemColors.Window;
			this.txtYearCharge.LinkItem = null;
			this.txtYearCharge.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYearCharge.LinkTopic = null;
			this.txtYearCharge.Location = new System.Drawing.Point(175, 225);
			this.txtYearCharge.MaxLength = 1;
			this.txtYearCharge.Name = "txtYearCharge";
			this.txtYearCharge.Size = new System.Drawing.Size(50, 40);
			this.txtYearCharge.TabIndex = 3;
			this.txtYearCharge.Text = "2";
			this.txtYearCharge.ToolTipText = null;
			this.txtYearCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearCharge_Validating);
			// 
			// txtMonthNoCharge
			// 
			this.txtMonthNoCharge.AutoSize = false;
			this.txtMonthNoCharge.BackColor = System.Drawing.SystemColors.Window;
			this.txtMonthNoCharge.LinkItem = null;
			this.txtMonthNoCharge.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMonthNoCharge.LinkTopic = null;
			this.txtMonthNoCharge.Location = new System.Drawing.Point(247, 175);
			this.txtMonthNoCharge.MaxLength = 1;
			this.txtMonthNoCharge.Name = "txtMonthNoCharge";
			this.txtMonthNoCharge.Size = new System.Drawing.Size(50, 40);
			this.txtMonthNoCharge.TabIndex = 1;
			this.txtMonthNoCharge.ToolTipText = null;
			this.txtMonthNoCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNoCharge_Validating);
			// 
			// txtYearNoCharge
			// 
			this.txtYearNoCharge.AutoSize = false;
			this.txtYearNoCharge.BackColor = System.Drawing.SystemColors.Window;
			this.txtYearNoCharge.LinkItem = null;
			this.txtYearNoCharge.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYearNoCharge.LinkTopic = null;
			this.txtYearNoCharge.Location = new System.Drawing.Point(247, 225);
			this.txtYearNoCharge.MaxLength = 1;
			this.txtYearNoCharge.Name = "txtYearNoCharge";
			this.txtYearNoCharge.Size = new System.Drawing.Size(50, 40);
			this.txtYearNoCharge.TabIndex = 4;
			this.txtYearNoCharge.ToolTipText = null;
			this.txtYearNoCharge.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearNoCharge_Validating);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(315, 150);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(65, 15);
			this.Label1.TabIndex = 23;
			this.Label1.Text = "STICKER #";
			this.Label1.ToolTipText = null;
			// 
			// lblInfo
			// 
			this.lblInfo.Location = new System.Drawing.Point(30, 90);
			this.lblInfo.Name = "lblInfo";
			this.lblInfo.Size = new System.Drawing.Size(398, 30);
			this.lblInfo.TabIndex = 17;
			this.lblInfo.ToolTipText = null;
			// 
			// Label43
			// 
			this.Label43.Location = new System.Drawing.Point(30, 189);
			this.Label43.Name = "Label43";
			this.Label43.Size = new System.Drawing.Size(60, 15);
			this.Label43.TabIndex = 14;
			this.Label43.Text = "STICKERS";
			this.Label43.ToolTipText = null;
			// 
			// lblM
			// 
			this.lblM.Location = new System.Drawing.Point(125, 189);
			this.lblM.Name = "lblM";
			this.lblM.Size = new System.Drawing.Size(39, 15);
			this.lblM.TabIndex = 13;
			this.lblM.Text = "(M)";
			this.lblM.ToolTipText = null;
			// 
			// lblY
			// 
			this.lblY.Location = new System.Drawing.Point(125, 239);
			this.lblY.Name = "lblY";
			this.lblY.Size = new System.Drawing.Size(39, 15);
			this.lblY.TabIndex = 12;
			this.lblY.Text = "(Y)";
			this.lblY.ToolTipText = null;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(175, 150);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(40, 15);
			this.Label6.TabIndex = 11;
			this.Label6.Text = "CHG";
			this.Label6.ToolTipText = null;
			// 
			// Label51
			// 
			this.Label51.Location = new System.Drawing.Point(247, 150);
			this.Label51.Name = "Label51";
			this.Label51.Size = new System.Drawing.Size(30, 15);
			this.Label51.TabIndex = 10;
			this.Label51.Text = "N/C";
			this.Label51.ToolTipText = null;
			// 
			// frmLostPlate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(460, 446);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmLostPlate";
			this.Text = "Lost Plate";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmLostPlate_Load);
			this.Activated += new System.EventHandler(this.frmLostPlate_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLostPlate_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInstructions)).EndInit();
			this.fraInstructions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
