//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;

namespace TWMV0000
{
	public partial class frmTransferAnalysis : BaseForm
	{
		public frmTransferAnalysis()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            lblStateCreditPR = new List<fecherFoundation.FCLabel>();
            lblStateCreditPR.AddControlArrayElement(lblStateCreditPR_0, 0);
            lblStateCreditPR.AddControlArrayElement(lblStateCreditPR_1, 1);
            lblStateCreditPR.AddControlArrayElement(lblStateCreditPR_2, 2);
            lblStateCreditPR.AddControlArrayElement(lblStateCreditPR_3, 3);

            lblStateCreditHR = new List<fecherFoundation.FCLabel>();
            lblStateCreditHR.AddControlArrayElement(lblStateCreditHR_0, 0);
            lblStateCreditHR.AddControlArrayElement(lblStateCreditHR_1, 1);
            lblStateCreditHR.AddControlArrayElement(lblStateCreditHR_2, 2);
            lblStateCreditHR.AddControlArrayElement(lblStateCreditHR_3, 3);

            Label1 = new List<fecherFoundation.FCLabel>();
            Label1.AddControlArrayElement(Label1_0, 0);
            Label1.AddControlArrayElement(Label1_1, 1);
            Label1.AddControlArrayElement(Label1_2, 2);
            Label1.AddControlArrayElement(Label1_3, 3);
            Label1.AddControlArrayElement(Label1_4, 4);
            Label1.AddControlArrayElement(Label1_5, 5);
            Label1.AddControlArrayElement(Label1_6, 6);
            Label1.AddControlArrayElement(Label1_7, 7);
            Label1.AddControlArrayElement(Label1_8, 8);
            Label1.AddControlArrayElement(Label1_9, 9);
            Label1.AddControlArrayElement(Label1_14, 10);
            Label1.AddControlArrayElement(Label1_15, 11);
            Label1.AddControlArrayElement(Label1_16, 12);
            Label1.AddControlArrayElement(Label1_17, 13);
            Label1.AddControlArrayElement(Label1_22, 14);
            Label1.AddControlArrayElement(Label1_23, 15);
            Label1.AddControlArrayElement(Label1_24, 16);
            Label1.AddControlArrayElement(Label1_25, 17);
            Label1.AddControlArrayElement(Label1_26, 18);
            Label1.AddControlArrayElement(Label1_27, 19);
            Label1.AddControlArrayElement(Label1_28, 20);
            Label1.AddControlArrayElement(Label1_32, 21);
            Label1.AddControlArrayElement(Label1_33, 22);
            Label1.AddControlArrayElement(Label1_34, 23);
            Label1.AddControlArrayElement(Label1_35, 24);
            Label1.AddControlArrayElement(Label1_36, 25);

            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTransferAnalysis InstancePtr
		{
			get
			{
				return (frmTransferAnalysis)Sys.GetInstance(typeof(frmTransferAnalysis));
			}
		}

		protected frmTransferAnalysis _InstancePtr = null;
		//=========================================================
		// vbPorter upgrade warning: curClassFee As Decimal	OnWrite(int, Decimal)
		Decimal curClassFee;
		// vbPorter upgrade warning: curClassCredit As Decimal	OnWrite(int, double, Decimal)
		Decimal curClassCredit;
		// vbPorter upgrade warning: curClassFeeNew As Decimal	OnWrite
		Decimal curClassFeeNew;
		// vbPorter upgrade warning: curOtherFeesReNew As Decimal	OnWrite(int, Decimal)
		Decimal curOtherFeesReNew;
		clsDRWrapper recClassFee = new clsDRWrapper();
		clsDRWrapper rsDef = new clsDRWrapper();
		string sSQL;
		// vbPorter upgrade warning: curExciseTaxFull As Decimal	OnWrite(Decimal, int, string, double)
		Decimal curExciseTaxFull;
		// vbPorter upgrade warning: curRegRateFull As Decimal	OnWrite(Decimal, int, string, double)
		Decimal curRegRateFull;
		// vbPorter upgrade warning: curRegistrationFee As Decimal	OnWrite(int, Decimal)
		Decimal curRegistrationFee;
		// vbPorter upgrade warning: curTransferFee As Decimal	OnWrite
		Decimal curTransferFee;
		// vbPorter upgrade warning: curAgentFeeNew As Decimal	OnWrite
		Decimal curAgentFeeNew;
		// vbPorter upgrade warning: curAgentFeeOld As Decimal	OnWrite
		Decimal curAgentFeeOld;
		// vbPorter upgrade warning: curLocalTransferFee As Decimal	OnWrite
		Decimal curLocalTransferFee;
		// vbPorter upgrade warning: curStateTransferFee As Decimal	OnWrite
		Decimal curStateTransferFee;
		// vbPorter upgrade warning: curTransferCreditFull As Decimal	OnWrite(int, Decimal)
		Decimal curTransferCreditFull;
		// vbPorter upgrade warning: curTransferCreditUsed As Decimal	OnWrite(int, Decimal)
		Decimal curTransferCreditUsed;
		// vbPorter upgrade warning: curExciseCreditFull As Decimal	OnWrite(int, string, double, Decimal)
		Decimal curExciseCreditFull;
		// vbPorter upgrade warning: curExciseCreditUsed As Decimal	OnWrite(int, string, double, Decimal)
		Decimal curExciseCreditUsed;
		// vbPorter upgrade warning: TimeLeft As int	OnWrite(long, int)
		int TimeLeft;
		int curExciseCreditMVR3;
		// vbPorter upgrade warning: datExpireDate As DateTime	OnWrite(string, int)
		DateTime datExpireDate;
		// vbPorter upgrade warning: ans2 As int	OnWrite(DialogResult)
		DialogResult ans2;
		object lngRegWeight;
		// vbPorter upgrade warning: xdate As DateTime	OnWrite(string, DateTime)
		DateTime xdate;

		private void chkETO_CheckedChanged(object sender, System.EventArgs e)
		{
			CalculateFees();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			rptTransferAnalysisReport.InstancePtr.PrintReport(true);
			rptTransferAnalysisReport.InstancePtr.Unload();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtCreditMVR3Number.Text) == 0)
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"))));
			}
			else
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", FCConvert.ToString(Conversion.Val(txtCreditMVR3Number.Text)));
			}
			if (FCConvert.ToInt32(lngRegWeight) != 0)
			{
				if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("registeredWeightNew")) == 0)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("registeredWeightNew", lngRegWeight);
				}
			}
			fraFinal.Visible = true;
			Frame1.Visible = false;
			modGNBas.Statics.Response = "GOOD";
			if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
			{
				MotorVehicle.Statics.ETO = true;
			}
			Close();
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdProcessCredit_Click(object sender, System.EventArgs e)
		{
			if (Information.IsDate(txtExpireDate.Text) == false)
			{
				MessageBox.Show("You must enter a valid expiration date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (fecherFoundation.Strings.Trim(txtStateCredit.Text) == "")
			{
				txtStateCredit.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtLocalCredit.Text) == "")
			{
				txtLocalCredit.Text = "0.00";
			}
			curRegRateFull = FCConvert.ToDecimal(txtStateCredit.Text);
			MotorVehicle.Statics.rsFinalCompare.Edit();
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtStateCredit.Text));
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtStateCredit.Text));
			curExciseTaxFull = FCConvert.ToDecimal(txtLocalCredit.Text);
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtLocalCredit.Text));
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("ExciseTaxCharged", FCConvert.ToDecimal(txtLocalCredit.Text));
			MotorVehicle.Statics.rsFinalCompare.Update();
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtStateCredit.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtStateCredit.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtLocalCredit.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", FCConvert.ToDecimal(txtLocalCredit.Text));
			curExciseCreditMVR3 = FCConvert.ToInt32(Math.Round(Conversion.Val(txtCreditMVR3Number.Text)));
			datExpireDate = FCConvert.ToDateTime(txtExpireDate.Text);
			if (MotorVehicle.Statics.blnForcedAnalysis)
			{
				ans2 = MessageBox.Show("Would you like to run the Transfer Analysis Report?", "Transfer Analysis", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans2 == DialogResult.Yes)
				{
					// do nothing
				}
				else
				{
					cmdProcess_Click();
					return;
				}
			}
			Form_Load();
			fraFinal.Visible = true;
			Frame1.Visible = false;
		}

		private void cmdProcessNew_Click(object sender, System.EventArgs e)
		{
            modGNBas.Statics.Response = "GOOD";
            MotorVehicle.Statics.FromTransfer = true;
			MotorVehicle.Statics.TransferSubclass = MotorVehicle.Statics.Subclass;
			Close();
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.Response = "BAD";
			Close();
		}

		public void cmdQuit_Click()
		{
			cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void cmdQuitCredit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			if (Information.IsDate(txtExpireDate.Text) == false)
			{
				MessageBox.Show("You must enter a valid expiration date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (fecherFoundation.Strings.Trim(txtStateCredit.Text) == "")
				txtStateCredit.Text = "0.00";
			if (fecherFoundation.Strings.Trim(txtLocalCredit.Text) == "")
				txtLocalCredit.Text = "0.00";
			MotorVehicle.Statics.rsFinalCompare.Edit();
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtStateCredit.Text));
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtStateCredit.Text));
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtLocalCredit.Text));
			MotorVehicle.Statics.rsFinalCompare.Set_Fields("ExciseTaxCharged", FCConvert.ToDecimal(txtLocalCredit.Text));
			MotorVehicle.Statics.rsFinalCompare.Update();
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtStateCredit.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtStateCredit.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", FCConvert.ToDecimal(txtLocalCredit.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", FCConvert.ToDecimal(txtLocalCredit.Text));
			cmdProcess_Click();
		}

		private void frmTransferAnalysis_Activated(object sender, System.EventArgs e)
		{
			// If FormExist(Me) Then
			// Exit Sub
			// End If
			if (MotorVehicle.Statics.RegistrationType == "RRT")
			{
				cmdProcessNew.Text = "Process As Re Reg";
				//FC:FINAL:DDU:#i1784 - changed from array element to explicit control
				//Label1[28].Text = "RE REGISTRATION";
				Label1_28.Text = "RE REGISTRATION";
			}
			else
			{
				cmdProcessNew.Text = "Process as New Reg";
				//FC:FINAL:DDU:#i1784 - changed from array element to explicit control
				//Label1[28].Text = "NEW REGISTRATION";
				Label1_28.Text = "NEW REGISTRATION";
			}
		}

		private void frmTransferAnalysis_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTransferAnalysis_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransferAnalysis properties;
			//frmTransferAnalysis.ScaleWidth	= 9300;
			//frmTransferAnalysis.ScaleHeight	= 7170;
			//frmTransferAnalysis.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGNBas.GetWindowSize(this);
			lngRegWeight = 0;
			if (MotorVehicle.Statics.gintFixedMonthMonth > 0)
			{
				if (MotorVehicle.Statics.gintFixedMonthMonth < DateTime.Today.Month)
				{
					xdate = FCConvert.ToDateTime(FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" + FCConvert.ToString(DateTime.Now.Year + 1));
				}
				else
				{
					if (MotorVehicle.Statics.gintFixedMonthMonth < 12)
					{
						xdate = FCConvert.ToDateTime(FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth + 1) + "/1/" + FCConvert.ToString(DateTime.Now.Year));
					}
					else
					{
						xdate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(DateTime.Now.Year + 1));
					}
				}
				xdate = fecherFoundation.DateAndTime.DateAdd("d", -1, xdate);
				if (xdate.ToOADate() < DateTime.Today.ToOADate())
				{
					xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", 1, xdate);
				}
			}
			else
			{
				xdate = FCConvert.ToDateTime(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
			}
			CalculateFees();
			if (MotorVehicle.Statics.blnForcedAnalysis)
			{
				Frame1.Visible = true;
				fraFinal.Visible = false;
			}
			else
			{
				Frame1.Visible = false;
				fraFinal.Visible = true;
			}
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		public void Form_Load()
		{
			frmTransferAnalysis_Load(this, new System.EventArgs());
		}

		private void Fill_Final_Labels()
		{
			// vbPorter upgrade warning: curTotalT As Decimal	OnWrite(int, Decimal)
			Decimal curTotalT = 0;
			// vbPorter upgrade warning: curTotalNR As Decimal	OnWrite(int, Decimal)
			Decimal curTotalNR = 0;
			// vbPorter upgrade warning: curExciseTax As Decimal	OnWrite(string)
			Decimal curExciseTax = 0;
			Decimal curExciseOld = 0;
			Decimal curExciseNew = 0;
			Decimal curExciseNextYear;
			int intMilYear = 0;
			int lngBasePrice = 0;
			// vbPorter upgrade warning: curPayTodayT As Decimal	OnWrite(int, Decimal)
			Decimal curPayTodayT;
			Decimal curExciseTaxTotal;
			Decimal curRegTotal;
			// vbPorter upgrade warning: curFirstYearRegTotal As Decimal	OnWrite(int, Decimal)
			Decimal curFirstYearRegTotal;
			clsDRWrapper rsClass = new clsDRWrapper();
			Fill_Constant_Labels();
			if (Conversion.Val(frmDataInput.InstancePtr.txtMilYear.Text) != 0)
			{
				intMilYear = FCConvert.ToInt32(Math.Round(Conversion.Val(frmDataInput.InstancePtr.txtMilYear.Text)));
			}
			else
			{
				intMilYear = FCConvert.ToInt16(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")));
				if (intMilYear < 6)
				{
					intMilYear += 1;
				}
			}
			// 
			if (Conversion.Val(frmDataInput.InstancePtr.txtBase.Text) != 0)
			{
				lngBasePrice = FCConvert.ToInt32(Conversion.Val(frmDataInput.InstancePtr.txtBase.Text));
			}
			else
			{
				lngBasePrice = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice")));
			}
			if (!MotorVehicle.IsProratedMotorcycle(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), DateTime.Today, ref xdate, MotorVehicle.Statics.RegistrationType))
			{
				if (TimeLeft <= 6 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF"))
				{
					curExciseTax = FCConvert.ToDecimal(Strings.Format(MotorVehicle.ExciseTax(intMilYear, lngBasePrice, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"))) / 2, "#,##0.00"));
				}
				else if (TimeLeft <= 4)
				{
					curExciseTax = FCConvert.ToDecimal(Strings.Format(MotorVehicle.ExciseTax(intMilYear, lngBasePrice, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"))) / 2, "#,##0.00"));
				}
				else
				{
					curExciseTax = FCConvert.ToDecimal(Strings.Format(MotorVehicle.ExciseTax(intMilYear, lngBasePrice, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"))), "#,##0.00"));
				}
				lblExciseTax1Year.Text = Strings.Format(curExciseTax, "#,##0.00");
			}
			else
			{
				curExciseTax = FCConvert.ToDecimal(Prorate(fecherFoundation.Strings.Trim(MotorVehicle.ExciseTax(intMilYear, lngBasePrice, MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")).ToString()), DateDifference(DateTime.Today, ref xdate)));
				lblExciseTax1Year.Text = Strings.Format(Prorate(fecherFoundation.Strings.Trim(MotorVehicle.ExciseTax(intMilYear, lngBasePrice, MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")).ToString()), DateDifference(DateTime.Today, ref xdate)), "#,##0.00");
			}
			if (curExciseTaxFull > FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull"))))
			{
				lblExciseTaxCredit.Text = Strings.Format(curExciseTaxFull * -1, "#,##0.00");
				// rsfinal.fields("ExciseTaxCredit
			}
			else
			{
				lblExciseTaxCredit.Text = Strings.Format(curExciseCreditFull * -1, "#,##0.00");
				// rsfinal.fields("ExciseTaxCredit
			}
			// 
			if (intMilYear < 6)
			{
				curExciseNextYear = MotorVehicle.ExciseTax(intMilYear + 1, lngBasePrice, MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
			}
			else
			{
				curExciseNextYear = MotorVehicle.ExciseTax(intMilYear, lngBasePrice, MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
			}
			lblExciseTax2Year.Text = Strings.Format(curExciseNextYear, "#,##0.00");
			// 
			if (Conversion.Val(lblExciseTax1Year.Text) != 0)
				curExciseNew = FCConvert.ToDecimal(lblExciseTax1Year.Text);
			if (Conversion.Val(lblExciseTaxCredit.Text) != 0)
				curExciseOld = FCConvert.ToDecimal(lblExciseTaxCredit.Text);
			// 
			if ((curExciseNew - (curExciseOld * -1)) <= 0)
			{
				curExciseTaxTotal = curExciseNextYear;
			}
			else
			{
				curExciseTaxTotal = ((curExciseNew - (curExciseOld * -1)) + curExciseNextYear);
			}
			lblExciseTaxTotal.Text = Strings.Format(curExciseTaxTotal, "#,##0.00");
			// 
			if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblRegistrationCredit.Text = Strings.Format(0, "#,##0.00");
			}
			else
			{
				lblRegistrationCredit.Text = Strings.Format((curRegRateFull * -1) - curClassCredit, "#,##0.00");
			}
			// 
			// curRegTotal = 0
			// curRegTotal = curRegTotal + Format(curRegistrationFee, "#,##0.00")  '1styear
			// curRegTotal = curRegTotal + curRegRateFull * -1                                  'credit
			// If curRegTotal < 0 Then
			// curRegTotal = 0
			// End If
			// 
			curFirstYearRegTotal = 0;
			curFirstYearRegTotal += FCConvert.ToDecimal(Strings.Format(FCConvert.ToDecimal(lblRegistrationTax1Year.Text) - curClassFee, "#,##0.00"));
			// 1styear
			curFirstYearRegTotal += (curRegRateFull * -1);
			// credit
			if (curFirstYearRegTotal < 0)
			{
				curFirstYearRegTotal = 0;
			}
			if (curClassFee - curClassCredit < 0)
			{
				curRegTotal = curFirstYearRegTotal + FCConvert.ToDecimal(lblRegistrationTax2Year.Text) + curStateTransferFee;
			}
			else
			{
				curRegTotal = curFirstYearRegTotal + curClassFee - curClassCredit + FCConvert.ToDecimal(lblRegistrationTax2Year.Text) + curStateTransferFee;
			}
			// If Not IsProratedMotorcycle(rsFinal.Fields("Class"), Date, xdate, RegistrationType) Then
			// If TimeLeft <= 6 And rsFinal.Fields("Class") = "FM" Then
			// curRegTotal = curRegTotal + Format(curStateTransferFee, "#,##0.00")                  'transfer
			// curRegTotal = curRegTotal + Format((curRegistrationFee * 2) + curOtherFeesReNew, "#,##0.00") '2ndyear
			// lblRegistrationTotal.Text = Format(curRegTotal * 1.5, "#,##0.00")
			// ElseIf TimeLeft <= 4 Then
			// curRegTotal = curRegTotal + Format(curStateTransferFee, "#,##0.00")                  'transfer
			// curRegTotal = curRegTotal + Format((curRegistrationFee * 2) + curOtherFeesReNew, "#,##0.00") '2ndyear
			// lblRegistrationTotal.Text = Format(curRegTotal * 1.5, "#,##0.00")
			// Else
			// curRegTotal = curRegTotal + Format(curStateTransferFee, "#,##0.00")                  'transfer
			// curRegTotal = curRegTotal + Format(curRegistrationFee + curOtherFeesReNew, "#,##0.00") '2ndyear
			// End If
			// Else
			// curRegTotal = curRegTotal + Format(curStateTransferFee, "#,##0.00")                  'transfer
			// curRegTotal = curRegTotal + Format(Prorate(Trim(CStr(curRegistrationFee)), DateDifference(Date, xdate)) + curOtherFeesReNew, "#,##0.00") '2ndyear
			// End If
			if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblRegistrationTotal.Text = Strings.Format(FCConvert.ToDecimal(lblRegistrationTax2Year.Text), "#,##0.00");
			}
			else
			{
				lblRegistrationTotal.Text = Strings.Format(curRegTotal, "#,##0.00");
			}
			// 
			lblExciseT.Text = lblExciseTaxTotal.Text;
            //FC:FINAL:AM:#i1808 - use cSettingController instead of the registry
            cSettingsController set = new cSettingsController();
			if (MotorVehicle.Statics.RegistrationType == "NRT")
			{
				rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) + "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'", "TWMV0000.vb1");
				if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsClass.Get_Fields_String("ExciseRequired")) != "N")
					{
						if (MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), DateTime.Today, ref xdate, "NRR"))
						{
                            //lblExciseNR.Text = Strings.Format(Prorate(fecherFoundation.Strings.Trim(MotorVehicle.ExciseTax(FCConvert.ToInt32(fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "MILYEAR", "0"))), FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(Interaction.GetSetting("TWGNENTY", "REDBOOK", "BASE", "0")))), MotorVehicle.Statics.rsFinal.Get_Fields("Class"), MotorVehicle.Statics.rsFinal.Get_Fields("Subclass")).ToString()), DateDifference(DateTime.Today, ref xdate)), "#,##0.00");
                            lblExciseNR.Text = Strings.Format(Prorate(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.ExciseTax(FCConvert.ToInt32(fecherFoundation.Strings.Trim(set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK", "0"))), FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK", "0")))), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"))))), DateDifference(DateTime.Today, ref xdate)), "#,##0.00");
                        }
						else
						{
							lblExciseNR.Text = Strings.Format(MotorVehicle.ExciseTax(FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK",  "0")))), FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK", "0")))), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"))), "#,##0.00");
						}
					}
					else
					{
						lblExciseNR.Text = Strings.Format(0, "#,##0.00");
					}
				}
				else
				{
					if (MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), DateTime.Today, ref xdate, "NRR"))
					{
						lblExciseNR.Text = Strings.Format(Prorate(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.ExciseTax(FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(set.GetSettingValue("MILYEAR", "TWGNENTY", "REDBOOK", "0")))), FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK", "0")))), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"))))), DateDifference(DateTime.Today, ref xdate)), "#,##0.00");
					}
					else
					{
						lblExciseNR.Text = Strings.Format(MotorVehicle.ExciseTax(FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(set.GetSettingValue("TWGNENTY", "REDBOOK", "MILYEAR", "0")))), FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(set.GetSettingValue("BASE", "TWGNENTY", "REDBOOK", "0")))), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"))), "#,##0.00");
					}
				}
			}
			else
			{
				if (TimeLeft <= 6 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF"))
				{
					lblExciseNR.Text = Strings.Format(FCConvert.ToDecimal(lblExciseTax1Year.Text) * 2, "#,##0.00");
				}
				else if (TimeLeft <= 4)
				{
					lblExciseNR.Text = Strings.Format(FCConvert.ToDecimal(lblExciseTax1Year.Text) * 2, "#,##0.00");
				}
				else
				{
					lblExciseNR.Text = lblExciseTax1Year.Text;
				}
			}
			lblRegT.Text = lblRegistrationTotal.Text;
			if (chkETO.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				if (MotorVehicle.Statics.RegistrationType == "NRT")
				{
					if (MotorVehicle.IsProratedMotorcycle(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), DateTime.Today, ref xdate, "NRR") && MotorVehicle.Statics.RegistrationType == "NRT")
					{
						lblRegNR.Text = Strings.Format(Prorate(fecherFoundation.Strings.Trim(curRegistrationFee.ToString()), DateDifference(DateTime.Today, ref xdate)) + curClassFeeNew, "#,##0.00");
					}
					else
					{
						if (TimeLeft <= 6 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF"))
						{
							lblRegNR.Text = Strings.Format((curRegistrationFee * 2) + curClassFeeNew, "#,##0.00");
						}
						else if (TimeLeft <= 4)
						{
							lblRegNR.Text = Strings.Format((curRegistrationFee * 2) + curClassFeeNew, "#,##0.00");
						}
						else
						{
							lblRegNR.Text = Strings.Format((curRegistrationFee + curClassFeeNew), "#,##0.00");
						}
					}
				}
				else
				{
					if (MotorVehicle.IsProratedMotorcycle(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), DateTime.Today, ref xdate, "NRR") && MotorVehicle.Statics.RegistrationType == "NRT")
					{
						lblRegNR.Text = Strings.Format(Prorate(fecherFoundation.Strings.Trim(curRegistrationFee.ToString()), DateDifference(DateTime.Today, ref xdate)) + curOtherFeesReNew, "#,##0.00");
					}
					else
					{
						if (TimeLeft <= 6 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF"))
						{
							lblRegNR.Text = Strings.Format((curRegistrationFee * 2) + curOtherFeesReNew, "#,##0.00");
						}
						else if (TimeLeft <= 4)
						{
							lblRegNR.Text = Strings.Format((curRegistrationFee * 2) + curOtherFeesReNew, "#,##0.00");
						}
						else
						{
							lblRegNR.Text = Strings.Format((curRegistrationFee + curOtherFeesReNew), "#,##0.00");
						}
					}
				}
			}
			else
			{
				lblRegNR.Text = Strings.Format(0, "#,##0.00");
			}
			// 
			lblLocalT.Text = lblLocalTotal.Text;
			if (chkETO.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				lblLocalNR.Text = Strings.Format(curAgentFeeNew, "#,##0.00");
			}
			else
			{
				lblLocalNR.Text = Strings.Format(0, "#,##0.00");
			}
			// 
			curTotalT = 0;
			if (Conversion.Val(lblExciseT.Text) != 0)
				curTotalT += FCConvert.ToDecimal(lblExciseT.Text);
			if (Conversion.Val(lblRegT.Text) != 0)
				curTotalT += FCConvert.ToDecimal(lblRegT.Text);
			if (Conversion.Val(lblLocalT.Text) != 0)
				curTotalT += FCConvert.ToDecimal(lblLocalT.Text);
			lblTotalT.Text = Strings.Format(curTotalT, "#,##0.00");
			// 
			curTotalNR = 0;
			if (Conversion.Val(lblExciseNR.Text) != 0)
				curTotalNR += FCConvert.ToDecimal(lblExciseNR.Text);
			if (Conversion.Val(lblRegNR.Text) != 0)
				curTotalNR += FCConvert.ToDecimal(lblRegNR.Text);
			if (Conversion.Val(lblLocalNR.Text) != 0)
				curTotalNR += FCConvert.ToDecimal(lblLocalNR.Text);
			lblTotalNR.Text = Strings.Format(curTotalNR, "#,##0.00");
			// 
			lblMonthsT.Text = FCConvert.ToString(fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today, xdate) + 12);
			if (MotorVehicle.IsProratedMotorcycle(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")), DateTime.Today, ref xdate, "NRR") && MotorVehicle.Statics.RegistrationType == "NRT")
			{
				lblMonthsNR.Text = FCConvert.ToString(fecherFoundation.DateAndTime.DateDiff("m", DateTime.Today, xdate));
			}
			else
			{
				lblMonthsNR.Text = "12";
			}
			lblPerMonthT.Text = Strings.Format(FCConvert.ToDouble(curTotalT) / Conversion.Val(lblMonthsT.Text), "#,##0.00");
			lblPerMonthNR.Text = Strings.Format(FCConvert.ToDouble(curTotalNR) / Conversion.Val(lblMonthsNR.Text), "#,##0.00");
			// 
			if (lblStateCreditHR_0.Visible == true)
			{
				lblPayLaterT.Text = Strings.Format(curOtherFeesReNew + curExciseNextYear + (curRegistrationFee * 2) + curAgentFeeOld, "#,##0.00");
				// ElseIf IsProratedMotorcycle(rsFinal.Fields("Class"), Date, xdate, "NRR") And RegistrationType = "NRT" Then
				// lblPayLaterT.Text = Format(curOtherFeesReNew + curExciseNextYear + Prorate(Trim(CStr(curRegistrationFee)), DateDifference(Date, xdate)) + curAgentFeeOld, "#,##0.00")
			}
			else
			{
				lblPayLaterT.Text = Strings.Format(curOtherFeesReNew + curExciseNextYear + curRegistrationFee + curAgentFeeOld, "#,##0.00");
			}
			// 
			curPayTodayT = 0;
			if (chkETO.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				if (curExciseTax - (curExciseOld * -1) <= 0)
				{
					if (MotorVehicle.Statics.RegistrationType == "RRT")
					{
						curPayTodayT = (curStateTransferFee + curFirstYearRegTotal + curAgentFeeOld);
					}
					else
					{
						curPayTodayT = (curStateTransferFee + curFirstYearRegTotal + curAgentFeeNew);
					}
				}
				else
				{
					if (MotorVehicle.Statics.RegistrationType == "RRT")
					{
						curPayTodayT = (curExciseTax - (curExciseOld * -1)) + curFirstYearRegTotal + curLocalTransferFee + (curStateTransferFee + curAgentFeeOld);
					}
					else
					{
						curPayTodayT = (curExciseTax - (curExciseOld * -1)) + curFirstYearRegTotal + curLocalTransferFee + (curStateTransferFee + curAgentFeeNew);
					}
				}
			}
			else
			{
				if (curExciseTax - curExciseOld <= 0)
				{
					curPayTodayT = (curLocalTransferFee);
				}
				else
				{
					curPayTodayT = (curExciseTax - (curExciseOld * -1)) + curLocalTransferFee;
				}
			}
			lblPayTodayT.Text = Strings.Format(curPayTodayT, "#,##0.00");
			// 
			lblPayTodayNR.Text = lblTotalNR.Text;
		}

		private string Prorate(string x, int y)
		{
			string Prorate = "";
			double total;
			total = FCConvert.ToDouble(x) / 12;
			total *= y;
			Prorate = total.ToString();
			return Prorate;
		}

		private int DateDifference(DateTime datEffective, ref DateTime datExpire)
		{
			int DateDifference = 0;
			// vbPorter upgrade warning: x As int	OnWrite(long, int)
			int x;
			x = fecherFoundation.DateAndTime.DateDiff("m", fecherFoundation.DateAndTime.DateAdd("m", -1, datEffective), datExpire);
			if (x < 0)
			{
				x *= -1;
			}
			else if (x == 0)
			{
				x = 1;
			}
			else if (x > 12)
			{
				x = 12;
			}
			DateDifference = x;
			return DateDifference;
		}

		private void Fill_Constant_Labels()
		{
			if (MotorVehicle.Statics.RegistrationType == "NRT")
			{
				if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
				{
					lblLocalTax1Year.Text = Strings.Format(curLocalTransferFee, "#,##0.00");
					lblLocalTax2Year.Text = Strings.Format(curAgentFeeOld, "#,##0.00");
					lblLocalTotal.Text = Strings.Format((curAgentFeeOld + curLocalTransferFee), "#,##0.00");
				}
				else
				{
					lblLocalTax1Year.Text = Strings.Format(curAgentFeeNew + curLocalTransferFee, "#,##0.00");
					lblLocalTax2Year.Text = Strings.Format(curAgentFeeOld, "#,##0.00");
					lblLocalTotal.Text = Strings.Format((curAgentFeeNew + curAgentFeeOld + curLocalTransferFee), "#,##0.00");
				}
			}
			else
			{
				if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
				{
					lblLocalTax1Year.Text = Strings.Format(curLocalTransferFee, "#,##0.00");
					lblLocalTax2Year.Text = Strings.Format(curAgentFeeOld, "#,##0.00");
					lblLocalTotal.Text = Strings.Format((curAgentFeeOld + curLocalTransferFee), "#,##0.00");
				}
				else
				{
					lblLocalTax1Year.Text = Strings.Format(curAgentFeeOld + curLocalTransferFee, "#,##0.00");
					lblLocalTax2Year.Text = Strings.Format(curAgentFeeOld, "#,##0.00");
					lblLocalTotal.Text = Strings.Format((curAgentFeeOld + curAgentFeeOld + curLocalTransferFee), "#,##0.00");
				}
			}
			// 
			if (chkETO.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblRegistrationTransfer.Text = Strings.Format(0, "#,##0.00");
				lblRegistrationTax1Year.Text = Strings.Format(0, "#,##0.00");
			}
			else if (!MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), DateTime.Today, ref xdate, MotorVehicle.Statics.RegistrationType))
			{
                string regClass = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                string subClass = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
				if ((regClass == "PC" && (subClass == "P1" || subClass == "P4")) || (regClass == "MP"))
				{
					lblRegistrationTransfer.Text = Strings.Format(5, "#,##0.00");
				}
				else
				{
					lblRegistrationTransfer.Text = Strings.Format(curStateTransferFee, "#,##0.00");
				}
				lblRegistrationTax1Year.Text = Strings.Format(curRegistrationFee + curClassFee, "#,##0.00");
			}
			else
			{
				lblRegistrationTransfer.Text = Strings.Format(curStateTransferFee, "#,##0.00");
				lblRegistrationTax1Year.Text = Strings.Format(Prorate(fecherFoundation.Strings.Trim(curRegistrationFee.ToString()), DateDifference(DateTime.Today, ref xdate)) + curClassFee, "#,##0.00");
			}
			if (!MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), DateTime.Today, ref xdate, MotorVehicle.Statics.RegistrationType))
			{
				if (TimeLeft <= 6 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF"))
				{
					lblRegistrationTax2Year.Text = Strings.Format((curRegistrationFee * 2) + curOtherFeesReNew, "#,##0.00");
				}
				else if (TimeLeft <= 4)
				{
					lblRegistrationTax2Year.Text = Strings.Format((curRegistrationFee * 2) + curOtherFeesReNew, "#,##0.00");
				}
				else
				{
					lblRegistrationTax2Year.Text = Strings.Format(curRegistrationFee + curOtherFeesReNew, "#,##0.00");
				}
			}
			else
			{
				lblRegistrationTax2Year.Text = Strings.Format(curRegistrationFee + curOtherFeesReNew, "#,##0.00");
			}
			lblPayLaterNR.Text = "0.00";
		}

		private void Print_Report()
		{
			frmReportViewer.InstancePtr.Init(rptTransferAnalysisReport.InstancePtr);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				if (modGNBas.Statics.Response.ToString() != "GOOD")
				{
					modGNBas.SaveWindowSize(this);
					modGNBas.Statics.Response = "BAD";
				}
			}
		}

		private void frmTransferAnalysis_Resize(object sender, System.EventArgs e)
		{
			modGNBas.SaveWindowSize(this);

            Frame1.CenterToContainer(this.ClientArea);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			curClassFee = 0;
			curClassCredit = 0;
			curClassFeeNew = 0;
			curOtherFeesReNew = 0;
			sSQL = "";
			curExciseTaxFull = 0;
			curRegRateFull = 0;
			curRegistrationFee = 0;
			curTransferFee = 0;
			curAgentFeeNew = 0;
			curAgentFeeOld = 0;
			curLocalTransferFee = 0;
			curStateTransferFee = 0;
			curTransferCreditFull = 0;
			curTransferCreditUsed = 0;
			curExciseCreditFull = 0;
			curExciseCreditUsed = 0;
			curExciseCreditMVR3 = 0;
			datExpireDate = DateTime.FromOADate(0);
			//App.DoEvents();
			recClassFee.Reset();
			rsDef.Reset();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void txtCreditMVR3Number_Enter(object sender, System.EventArgs e)
		{
			txtCreditMVR3Number.SelectionStart = 0;
			txtCreditMVR3Number.SelectionLength = txtCreditMVR3Number.Text.Length;
		}

		private void txtCreditMVR3Number_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdProcessCredit.Focus();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLocalCredit_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				txtStateCredit.Focus();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtStateCredit_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				txtCreditMVR3Number.Focus();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void CalculateFees()
		{
			string tempClass = "";
			clsDRWrapper rsGVWFee = new clsDRWrapper();
			string tempcode = "";
			int tempweight;
			bool blnUserCancel;
			Decimal curInit;
			// MsgBox "1"
			sSQL = "SELECT * FROM DefaultInfo";
			rsDef.OpenRecordset(sSQL);
			TimeLeft = fecherFoundation.DateAndTime.DateDiff("m", FCConvert.ToDateTime(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")), DateTime.Today);
			if (TimeLeft < 0)
				TimeLeft *= -1;
			TimeLeft += 1;
			curAgentFeeNew = 0;
			curAgentFeeOld = 0;
			curLocalTransferFee = 0;
			curStateTransferFee = 0;
			// MsgBox "2"
			if (rsDef.EndOfFile() != true && rsDef.BeginningOfFile() != true)
			{
				rsDef.MoveLast();
				rsDef.MoveFirst();
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode")).Length < 5)
				{
					tempcode = "0" + MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
				}
				else
				{
					tempcode = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
				}
				curAgentFeeNew = FCConvert.ToDecimal((tempcode == FCConvert.ToString(rsDef.Get_Fields_String("ResidenceCode")) ? rsDef.Get_Fields_Decimal("AgentfeeNewLocal") : rsDef.Get_Fields_Decimal("AgentFeeNewOOT")));
				curAgentFeeOld = FCConvert.ToDecimal((tempcode == FCConvert.ToString(rsDef.Get_Fields_String("ResidenceCode")) ? rsDef.Get_Fields_Decimal("AgentFeeReRegLocal") : rsDef.Get_Fields_Decimal("AgentFeeReRegOOT")));
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")) == 0 && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) == 0)
				{
					curLocalTransferFee = 0;
				}
				else
				{
					curLocalTransferFee = FCConvert.ToDecimal(Conversion.Val(rsDef.Get_Fields_Decimal("LocalTransferFee")));
				}
                // kk10222015 Just rearranged terms
                string regClass = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");
                string subClass = MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass");
                if ((regClass == "PC" && (subClass == "P1" || subClass == "P4")) || (regClass == "MP"))
				{
					curStateTransferFee = 5;
				}
				else
				{
					curStateTransferFee = FCConvert.ToDecimal(Conversion.Val(rsDef.Get_Fields_Decimal("StateTransferFee")));
				}
			}
			// MsgBox "3"
			// Reg Fees from Class Table
			sSQL = "SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'";
			rsDef.OpenRecordset(sSQL);
			if (rsDef.EndOfFile() != true && rsDef.BeginningOfFile() != true)
			{
				rsDef.MoveLast();
				rsDef.MoveFirst();
				curRegistrationFee = FCConvert.ToDecimal(Conversion.Val(rsDef.Get_Fields("RegistrationFee")));
				if (curRegistrationFee != 0 && FCConvert.ToDouble(curRegistrationFee) <= 0.03)
				{
					if (MotorVehicle.Statics.Class == "SE")
					{
						tempClass = "SE";
					}
					else if (MotorVehicle.Statics.Class == "FM" || MotorVehicle.Statics.Class == "MH")
					{
						tempClass = "FM";
					}
					else
					{
						tempClass = "TK";
					}
					if (FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("registeredWeightNew"))) == 0)
					{
						lngRegWeight = 0;
						blnUserCancel = frmInput.InstancePtr.Init(ref lngRegWeight, "Enter Vehicle Weight", "Please enter the weight of the vehicle you are registering.", 1500, false, modGlobalConstants.InputDTypes.idtWholeNumber);
					}
					else
					{
						lngRegWeight = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")));
					}
					sSQL = "SELECT * FROM GrossVehicleWeight WHERE Type = '" + tempClass + "' AND Low <= " + FCConvert.ToString(lngRegWeight) + " AND High >= " + FCConvert.ToString(lngRegWeight);
					rsGVWFee.OpenRecordset(sSQL);
					if (rsGVWFee.EndOfFile() != true && rsGVWFee.BeginningOfFile() != true)
					{
						rsGVWFee.MoveLast();
						rsGVWFee.MoveFirst();
						curRegistrationFee = FCConvert.ToDecimal(Conversion.Val(rsGVWFee.Get_Fields("Fee")));
						if (curRegistrationFee == 0)
							curRegistrationFee = 35;
					}
				}
			}
			if (MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), DateTime.Today, ref xdate, MotorVehicle.Statics.RegistrationType))
			{
				// curRegistrationFee = ProRate(Trim(CStr(curRegistrationFee)), DateDifference(Date, xdate))
				//FC:FINAL:DDU:#i1784 - changed from array element to explicit control
				lblStateCreditPR_0.Visible = true;
				lblStateCreditPR_1.Visible = true;
				lblStateCreditPR_2.Visible = true;
				lblStateCreditPR_3.Visible = true;
			}
			else
			{
				if (TimeLeft <= 6 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF"))
				{
					curRegistrationFee /= 2;
					//FC:FINAL:DDU:#i1784 - changed from array element to explicit control
					lblStateCreditHR_0.Visible = true;
					lblStateCreditHR_1.Visible = true;
					lblStateCreditHR_2.Visible = true;
					lblStateCreditHR_3.Visible = true;
				}
				else if (TimeLeft <= 4)
				{
					curRegistrationFee /= 2;
					//FC:FINAL:DDU:#i1784 - changed from array element to explicit control
					lblStateCreditHR_0.Visible = true;
					lblStateCreditHR_1.Visible = true;
					lblStateCreditHR_2.Visible = true;
					lblStateCreditHR_3.Visible = true;
				}
				//FC:FINAL:DDU:#i1784 - changed from array element to explicit control
				lblStateCreditPR_0.Visible = false;
				lblStateCreditPR_1.Visible = false;
				lblStateCreditPR_2.Visible = false;
				lblStateCreditPR_3.Visible = false;
			}
			// MsgBox "4"
			curOtherFeesReNew = 0;
			recClassFee.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class + "' And SystemCode = '" + MotorVehicle.Statics.Subclass + "'");
			if (recClassFee.EndOfFile() != true && recClassFee.BeginningOfFile() != true)
			{
				recClassFee.MoveLast();
				recClassFee.MoveFirst();
				curOtherFeesReNew = FCConvert.ToDecimal(Conversion.Val(recClassFee.Get_Fields_Decimal("Otherfeesrenew")));
				curClassFeeNew = FCConvert.ToDecimal(Conversion.Val(recClassFee.Get_Fields_Decimal("OtherFeesNew")));
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "T")
				{
					curClassFee = 0;
				}
				else
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) != "O")
					{
						curClassFee = FCConvert.ToDecimal(Conversion.Val(recClassFee.Get_Fields_Decimal("OtherFeesNew")));
					}
					else
					{
						curClassFee = FCConvert.ToDecimal(Conversion.Val(recClassFee.Get_Fields_Decimal("Otherfeesrenew")));
					}
				}
				// If NewPlate = True Then
				// curClassFee = .Fields("OtherFeesNew")
				// Else
				// curClassFee = .Fields("Otherfeesrenew")
				// End If
			}
			if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("PlateFeeNew")) == 0)
			{
				MotorVehicle.Statics.rsFinalCompare.Edit();
				MotorVehicle.Statics.rsFinalCompare.Set_Fields("PlateFeeNew", 0);
				MotorVehicle.Statics.rsFinalCompare.Update();
			}
			if (MotorVehicle.Statics.Class == FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class")))
			{
				curClassCredit = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("PlateFeeNew")) + Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("PlateFeeRereg")));
			}
			else
			{
				curClassCredit = 0;
			}
			// MsgBox "5"
			if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "SE")
			{
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")) == 0)
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")));
				}
			}
			else
			{
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) == 0)
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")));
				}
			}
			// MsgBox "6"
			curInit = MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"), false, false, true);
			if (curInit != 0)
			{
				curClassFee += curInit;
				curClassCredit += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("InitialFee")));
			}
			curClassFee += MotorVehicle.GetOutOfRotation(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
			curOtherFeesReNew += MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"), false, false, true);
			// MsgBox "7"
			if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) < 0)
			{
				if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate")))
				{
					txtExpireDate.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
				}
				fraFinal.Visible = false;
			}
			else
			{
				// MsgBox "8"
				curTransferCreditFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditFull")));
				curTransferCreditUsed = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")));
				curExciseCreditMVR3 = FCConvert.ToInt32(Math.Round(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"))));
				// MsgBox "9"
				if (MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), DateTime.Today, ref xdate, MotorVehicle.Statics.RegistrationType))
				{
					// kk11282016 tromv-1211  RegRateFull is already prorated if it was a NRT
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "NRT")
					{
						curRegRateFull = FCConvert.ToDecimal(Prorate(curRegistrationFee.ToString(), DateDifference(DateTime.Today, ref xdate)));
					}
					else
					{
						curRegRateFull = FCConvert.ToDecimal(Prorate(fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull").ToString()), DateDifference(DateTime.Today, ref xdate)));
					}
					curExciseTaxFull = FCConvert.ToDecimal(Prorate(FCConvert.ToString(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")))), DateDifference(DateTime.Today, ref xdate)));
					curExciseCreditFull = FCConvert.ToDecimal(Prorate(FCConvert.ToString(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")))), DateDifference(DateTime.Today, ref xdate)));
					curExciseCreditUsed = FCConvert.ToDecimal(Prorate(FCConvert.ToString(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")))), DateDifference(DateTime.Today, ref xdate)));
				}
				else
				{
					if (TimeLeft <= 6 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF"))
					{
						curRegRateFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull")) / 2);
						curExciseTaxFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) / 2);
						curExciseCreditFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")) / 2);
						curExciseCreditUsed = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) / 2);
					}
					else if (TimeLeft <= 4)
					{
						curRegRateFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull")) / 2);
						curExciseTaxFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")) / 2);
						curExciseCreditFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")) / 2);
						curExciseCreditUsed = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")) / 2);
					}
					else
					{
						curRegRateFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull")));
						curExciseTaxFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull")));
						curExciseCreditFull = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull")));
						curExciseCreditUsed = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed")));
					}
				}
				// MsgBox "10"
				datExpireDate = FCConvert.ToDateTime(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"));				Fill_Final_Labels();
				// MsgBox "11"
			}
		}

		private void SetCustomFormColors()
		{
			//FC:FINAL:DDU:#i1784 - changed from array element to explicit control
			Label1_0.BackColor = ColorTranslator.FromOle(0xC0FFFF);
			Label1_9.BackColor = ColorTranslator.FromOle(0xC0FFFF);
			lblStateCreditHR_0.BackColor = ColorTranslator.FromOle(0xC0FFFF);
			lblStateCreditHR_1.BackColor = ColorTranslator.FromOle(0xC0FFFF);
			lblStateCreditHR_2.BackColor = ColorTranslator.FromOle(0xC0FFFF);
			lblStateCreditHR_3.BackColor = ColorTranslator.FromOle(0xC0FFFF);
		}
	}
}
