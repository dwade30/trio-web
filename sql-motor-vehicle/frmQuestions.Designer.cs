using fecherFoundation;
using System;

namespace TWMV0000
{
	public partial class frmQuestions
	{
		public fecherFoundation.FCComboBox cmbPrivilegeNo;
		public fecherFoundation.FCComboBox cmbSR22No;
		public fecherFoundation.FCComboBox cmbJBFiling;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		public fecherFoundation.FCFrame fraTrailerClass;
		public fecherFoundation.FCFrame fraJBFiling;
		public fecherFoundation.FCFrame fraSR22;
		public fecherFoundation.FCFrame fraRegPrivilege;
		public fecherFoundation.FCLabel lblTrailerClass;
		public fecherFoundation.FCLabel lblTrailerSubclass;
		public fecherFoundation.FCLabel lblJBFiling;
		public fecherFoundation.FCLabel lblSR22;
		public fecherFoundation.FCLabel lblPrivilege;
		public fecherFoundation.FCLabel lblInitials;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCTextBox txtUserID;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		private void InitializeComponent()
		{
			this.cmbPrivilegeNo = new fecherFoundation.FCComboBox();
			this.cmbSR22No = new fecherFoundation.FCComboBox();
			this.cmbJBFiling = new fecherFoundation.FCComboBox();
			this.fraTrailerClass = new fecherFoundation.FCFrame();
			this.lblTrailerClass = new fecherFoundation.FCLabel();
			this.lblTrailerSubclass = new fecherFoundation.FCLabel();
			this.fraJBFiling = new fecherFoundation.FCFrame();
			this.lblJBFiling = new fecherFoundation.FCLabel();
			this.fraSR22 = new fecherFoundation.FCFrame();
			this.lblSR22 = new fecherFoundation.FCLabel();
			this.fraRegPrivilege = new fecherFoundation.FCFrame();
			this.lblPrivilege = new fecherFoundation.FCLabel();
			this.lblInitials = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.txtUserID = new fecherFoundation.FCTextBox();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTrailerClass)).BeginInit();
			this.fraTrailerClass.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJBFiling)).BeginInit();
			this.fraJBFiling.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSR22)).BeginInit();
			this.fraSR22.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRegPrivilege)).BeginInit();
			this.fraRegPrivilege.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 535);
			this.BottomPanel.Size = new System.Drawing.Size(761, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraTrailerClass);
			this.ClientArea.Controls.Add(this.txtUserID);
			this.ClientArea.Controls.Add(this.fraJBFiling);
			this.ClientArea.Controls.Add(this.fraSR22);
			this.ClientArea.Controls.Add(this.fraRegPrivilege);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblInitials);
			this.ClientArea.Size = new System.Drawing.Size(761, 475);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(761, 60);
			this.TopPanel.TabIndex = 0;
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(262, 30);
			this.HeaderText.Text = "Registration Questions";
			// 
			// cmbPrivilegeNo
			// 
			this.cmbPrivilegeNo.AutoSize = false;
			this.cmbPrivilegeNo.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPrivilegeNo.FormattingEnabled = true;
			this.cmbPrivilegeNo.Items.AddRange(new object[] {
            "No",
            "Yes"});
			this.cmbPrivilegeNo.Location = new System.Drawing.Point(20, 65);
			this.cmbPrivilegeNo.Name = "cmbPrivilegeNo";
			this.cmbPrivilegeNo.Size = new System.Drawing.Size(145, 40);
			this.cmbPrivilegeNo.TabIndex = 1;
			this.cmbPrivilegeNo.Text = "No";
			this.cmbPrivilegeNo.ToolTipText = null;
			// 
			// cmbSR22No
			// 
			this.cmbSR22No.AutoSize = false;
			this.cmbSR22No.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSR22No.FormattingEnabled = true;
			this.cmbSR22No.Items.AddRange(new object[] {
            "No",
            "Yes"});
			this.cmbSR22No.Location = new System.Drawing.Point(20, 65);
			this.cmbSR22No.Name = "cmbSR22No";
			this.cmbSR22No.Size = new System.Drawing.Size(145, 40);
			this.cmbSR22No.TabIndex = 1;
			this.cmbSR22No.Text = "No";
			this.cmbSR22No.ToolTipText = null;
			// 
			// cmbJBFiling
			// 
			this.cmbJBFiling.AutoSize = false;
			this.cmbJBFiling.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbJBFiling.FormattingEnabled = true;
			this.cmbJBFiling.Items.AddRange(new object[] {
            "OK"});
			this.cmbJBFiling.Location = new System.Drawing.Point(20, 65);
			this.cmbJBFiling.Name = "cmbJBFiling";
			this.cmbJBFiling.Size = new System.Drawing.Size(145, 40);
			this.cmbJBFiling.TabIndex = 1;
			this.cmbJBFiling.Text = "OK";
			this.cmbJBFiling.ToolTipText = null;
			// 
			// fraTrailerClass
			// 
			this.fraTrailerClass.Controls.Add(this.lblTrailerClass);
			this.fraTrailerClass.Controls.Add(this.lblTrailerSubclass);
			this.fraTrailerClass.Location = new System.Drawing.Point(552, 191);
			this.fraTrailerClass.Name = "fraTrailerClass";
			this.fraTrailerClass.Size = new System.Drawing.Size(181, 109);
			this.fraTrailerClass.TabIndex = 6;
			this.fraTrailerClass.Text = "Trailer Class";
			// 
			// lblTrailerClass
			// 
			this.lblTrailerClass.Location = new System.Drawing.Point(20, 30);
			this.lblTrailerClass.Name = "lblTrailerClass";
			this.lblTrailerClass.Size = new System.Drawing.Size(27, 15);
			this.lblTrailerClass.TabIndex = 0;
			this.lblTrailerClass.Text = "TL";
			this.lblTrailerClass.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTrailerClass.ToolTipText = null;
			// 
			// lblTrailerSubclass
			// 
			this.lblTrailerSubclass.Location = new System.Drawing.Point(77, 30);
			this.lblTrailerSubclass.Name = "lblTrailerSubclass";
			this.lblTrailerSubclass.Size = new System.Drawing.Size(27, 15);
			this.lblTrailerSubclass.TabIndex = 1;
			this.lblTrailerSubclass.Text = "TL";
			this.lblTrailerSubclass.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTrailerSubclass.ToolTipText = null;
			// 
			// fraJBFiling
			// 
			this.fraJBFiling.Controls.Add(this.lblJBFiling);
			this.fraJBFiling.Controls.Add(this.cmbJBFiling);
			this.fraJBFiling.FormatCaption = false;
			this.fraJBFiling.Location = new System.Drawing.Point(30, 320);
			this.fraJBFiling.Name = "fraJBFiling";
			this.fraJBFiling.Size = new System.Drawing.Size(492, 125);
			this.fraJBFiling.TabIndex = 2;
			this.fraJBFiling.Text = "JB Filing";
			// 
			// lblJBFiling
			// 
			this.lblJBFiling.Location = new System.Drawing.Point(20, 30);
			this.lblJBFiling.Name = "lblJBFiling";
			this.lblJBFiling.Size = new System.Drawing.Size(452, 15);
			this.lblJBFiling.TabIndex = 0;
			this.lblJBFiling.Text = "PLEASE VERIFY JB FILING LIST FOR BLANKET INSURANCE COVERAGE";
			this.lblJBFiling.ToolTipText = null;
			// 
			// fraSR22
			// 
			this.fraSR22.Controls.Add(this.lblSR22);
			this.fraSR22.Controls.Add(this.cmbSR22No);
			this.fraSR22.FormatCaption = false;
			this.fraSR22.Location = new System.Drawing.Point(30, 175);
			this.fraSR22.Name = "fraSR22";
			this.fraSR22.Size = new System.Drawing.Size(492, 125);
			this.fraSR22.TabIndex = 1;
			this.fraSR22.Text = "SR-22";
			// 
			// lblSR22
			// 
			this.lblSR22.Location = new System.Drawing.Point(20, 30);
			this.lblSR22.Name = "lblSR22";
			this.lblSR22.Size = new System.Drawing.Size(452, 15);
			this.lblSR22.TabIndex = 0;
			this.lblSR22.Text = "ARE YOU REQUIRED TO FILE AN SR-22 CERTIFICATE OF INSURANCE WITH BMV?";
			this.lblSR22.ToolTipText = null;
			// 
			// fraRegPrivilege
			// 
			this.fraRegPrivilege.Controls.Add(this.lblPrivilege);
			this.fraRegPrivilege.Controls.Add(this.cmbPrivilegeNo);
			this.fraRegPrivilege.Location = new System.Drawing.Point(30, 30);
			this.fraRegPrivilege.Name = "fraRegPrivilege";
			this.fraRegPrivilege.Size = new System.Drawing.Size(492, 125);
			this.fraRegPrivilege.TabIndex = 0;
			this.fraRegPrivilege.Text = "Registration Privilege";
			// 
			// lblPrivilege
			// 
			this.lblPrivilege.Location = new System.Drawing.Point(20, 30);
			this.lblPrivilege.Name = "lblPrivilege";
			this.lblPrivilege.Size = new System.Drawing.Size(452, 15);
			this.lblPrivilege.TabIndex = 0;
			this.lblPrivilege.Text = "IS YOUR REGISTRATION OR PRIVILEGE TO REGISTER NOW UNDER SUSPENSION?";
			this.lblPrivilege.ToolTipText = null;
			// 
			// lblInitials
			// 
			this.lblInitials.Location = new System.Drawing.Point(552, 30);
			this.lblInitials.Name = "lblInitials";
			this.lblInitials.Size = new System.Drawing.Size(181, 46);
			this.lblInitials.TabIndex = 3;
			this.lblInitials.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblInitials.ToolTipText = null;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(552, 96);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(181, 15);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "PRESS ENTER TO CONTINUE";
			this.Label2.ToolTipText = null;
			// 
			// txtUserID
			// 
			this.txtUserID.AutoSize = false;
			this.txtUserID.BackColor = System.Drawing.SystemColors.Window;
			this.txtUserID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtUserID.LinkItem = null;
			this.txtUserID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtUserID.LinkTopic = null;
			this.txtUserID.Location = new System.Drawing.Point(552, 131);
			this.txtUserID.MaxLength = 3;
			this.txtUserID.Name = "txtUserID";
			this.txtUserID.Size = new System.Drawing.Size(181, 40);
			this.txtUserID.TabIndex = 5;
			this.txtUserID.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtUserID.ToolTipText = null;
			this.txtUserID.TextChanged += new System.EventHandler(this.txtUserID_TextChanged);
			this.txtUserID.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserID_Validating);
			// 
			// frmQuestions
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(761, 535);
			this.ControlBox = false;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmQuestions";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Registration Questions";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmQuestions_Load);
			this.Activated += new System.EventHandler(this.frmQuestions_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmQuestions_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTrailerClass)).EndInit();
			this.fraTrailerClass.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraJBFiling)).EndInit();
			this.fraJBFiling.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSR22)).EndInit();
			this.fraSR22.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRegPrivilege)).EndInit();
			this.fraRegPrivilege.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
