//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmSetupAuditArchiveReport : BaseForm
	{
		public frmSetupAuditArchiveReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSetupAuditArchiveReport InstancePtr
		{
			get
			{
				return (frmSetupAuditArchiveReport)Sys.GetInstance(typeof(frmSetupAuditArchiveReport));
			}
		}

		protected frmSetupAuditArchiveReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmSetupAuditArchiveReport_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupAuditArchiveReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupAuditArchiveReport properties;
			//frmSetupAuditArchiveReport.FillStyle	= 0;
			//frmSetupAuditArchiveReport.ScaleWidth	= 9045;
			//frmSetupAuditArchiveReport.ScaleHeight	= 7110;
			//frmSetupAuditArchiveReport.LinkTopic	= "Form2";
			//frmSetupAuditArchiveReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			LoadPlateCombo();
			LoadScreenCombo();
			LoadVINCombo();
		}

		private void frmSetupAuditArchiveReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.Text == "Date Range")
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtLowDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			frmReportViewer.InstancePtr.Init(rptAuditArchiveReport.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbDateAll.Text == "Date Range")
			{
				if (!Information.IsDate(txtLowDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("You have entered an invalid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtHighDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtLowDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("You have entered an invalid date range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtLowDate.Focus();
					return;
				}
			}
			rptAuditArchiveReport.InstancePtr.PrintReport(true);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadPlateCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboPlate.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT UserField1 FROM AuditChangesArchive ORDER BY UserField1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPlate.AddItem(rsInfo.Get_Fields_String("UserField1"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadScreenCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboScreen.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT Location FROM AuditChangesArchive ORDER BY Location");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboScreen.AddItem(rsInfo.Get_Fields_String("Location"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadVINCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboVIN.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT UserField2 FROM AuditChangesArchive ORDER BY UserField2");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboVIN.AddItem(rsInfo.Get_Fields_String("UserField2"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void optDateAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = false;
			Label2.Enabled = false;
			txtLowDate.Enabled = false;
			txtHighDate.Enabled = false;
			txtLowDate.Text = "";
			txtHighDate.Text = "";
		}

		private void optDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			Label2.Enabled = true;
			txtLowDate.Enabled = true;
			txtHighDate.Enabled = true;
		}

		private void optPlateAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedPlate.Enabled = false;
			cboPlate.Enabled = false;
			cboPlate.SelectedIndex = -1;
		}

		private void optPlateSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedPlate.Enabled = true;
			cboPlate.Enabled = true;
			if (cboPlate.Items.Count > 0)
			{
				cboPlate.SelectedIndex = 0;
			}
		}

		private void optScreenAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedScreen.Enabled = false;
			cboScreen.Enabled = false;
			cboScreen.SelectedIndex = -1;
		}

		private void optScreenSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedScreen.Enabled = true;
			cboScreen.Enabled = true;
			if (cboScreen.Items.Count > 0)
			{
				cboScreen.SelectedIndex = 0;
			}
		}

		private void optVINAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedVIN.Enabled = false;
			cboVIN.Enabled = false;
			cboVIN.SelectedIndex = -1;
		}

		private void optVINSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedVIN.Enabled = true;
			cboVIN.Enabled = true;
			if (cboVIN.Items.Count > 0)
			{
				cboVIN.SelectedIndex = 0;
			}
		}

		public void cmbVINSelected_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbVINSelected.Text == "All")
			{
				optVINAll_CheckedChanged(sender, e);
			}
			else if (cmbVINSelected.Text == "Selected")
			{
				optVINSelected_CheckedChanged(sender, e);
			}
		}

		public void cmbDateAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbDateAll.Text == "All")
			{
				optDateAll_CheckedChanged(sender, e);
			}
			else if (cmbDateAll.Text == "Date Range")
			{
				optDateRange_CheckedChanged(sender, e);
			}
		}

		public void cmbScreenAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbScreenAll.Text == "All")
			{
				optScreenAll_CheckedChanged(sender, e);
			}
			else if (cmbScreenAll.Text == "Selected")
			{
				optScreenSelected_CheckedChanged(sender, e);
			}
		}

		public void cmbPlateAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbPlateAll.Text == "All")
			{
				optPlateAll_CheckedChanged(sender, e);
			}
			else if (cmbPlateAll.Text == "Selected")
			{
				optPlateSelected_CheckedChanged(sender, e);
			}
		}
	}
}
