﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReminderLabels.
	/// </summary>
	partial class rptReminderLabels
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderLabels));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYearMakeModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearMakeModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.fldPlate,
				this.fldYearMakeModel,
				this.fldExpires,
				this.fldAmount,
				this.fldPlate2,
				this.fldName,
				this.fldAddress,
				this.fldCityStateZip
			});
			this.Detail.Height = 2.010417F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.9375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label1.Text = "Plate:";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 0.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label2.Text = "Vehicle:";
			this.Label2.Top = 0.25F;
			this.Label2.Width = 0.8125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label3.Text = "Expires:";
			this.Label3.Top = 0.4375F;
			this.Label3.Width = 0.8125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label4.Text = "Amount:";
			this.Label4.Top = 0.625F;
			this.Label4.Width = 0.8125F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 1.5625F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPlate.Text = "Field1";
			this.fldPlate.Top = 0.0625F;
			this.fldPlate.Width = 0.875F;
			// 
			// fldYearMakeModel
			// 
			this.fldYearMakeModel.Height = 0.1875F;
			this.fldYearMakeModel.Left = 0.9375F;
			this.fldYearMakeModel.Name = "fldYearMakeModel";
			this.fldYearMakeModel.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldYearMakeModel.Text = "Field1";
			this.fldYearMakeModel.Top = 0.25F;
			this.fldYearMakeModel.Width = 2.5625F;
			// 
			// fldExpires
			// 
			this.fldExpires.Height = 0.1875F;
			this.fldExpires.Left = 0.9375F;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExpires.Text = "Field1";
			this.fldExpires.Top = 0.4375F;
			this.fldExpires.Width = 2.5625F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 0.9375F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0.625F;
			this.fldAmount.Width = 2.5625F;
			// 
			// fldPlate2
			// 
			this.fldPlate2.Height = 0.1875F;
			this.fldPlate2.Left = 1.5625F;
			this.fldPlate2.Name = "fldPlate2";
			this.fldPlate2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPlate2.Text = "Field1";
			this.fldPlate2.Top = 1.0625F;
			this.fldPlate2.Width = 0.875F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.0625F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldName.Text = "Field1";
			this.fldName.Top = 1.25F;
			this.fldName.Width = 3.4375F;
			// 
			// fldAddress
			// 
			this.fldAddress.Height = 0.1875F;
			this.fldAddress.Left = 0.0625F;
			this.fldAddress.Name = "fldAddress";
			this.fldAddress.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldAddress.Text = "Field1";
			this.fldAddress.Top = 1.4375F;
			this.fldAddress.Width = 3.4375F;
			// 
			// fldCityStateZip
			// 
			this.fldCityStateZip.Height = 0.1875F;
			this.fldCityStateZip.Left = 0.0625F;
			this.fldCityStateZip.Name = "fldCityStateZip";
			this.fldCityStateZip.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldCityStateZip.Text = "Field1";
			this.fldCityStateZip.Top = 1.625F;
			this.fldCityStateZip.Width = 3.4375F;
			// 
			// rptReminderLabels
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 3.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearMakeModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYearMakeModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
