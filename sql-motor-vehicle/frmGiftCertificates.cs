//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmGiftCertificates : BaseForm
	{
		public frmGiftCertificates()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGiftCertificates InstancePtr
		{
			get
			{
				return (frmGiftCertificates)Sys.GetInstance(typeof(frmGiftCertificates));
			}
		}

		protected frmGiftCertificates _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int IDCol;
		int NameCol;
		int GiftCertificateNumberCol;
		int DateCol;
		int AmountCol;

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			if (vsCertificates.Row > 0)
			{
				if (MessageBox.Show("Are you sure you wish to delete this gift certificate?", "Delete Gift Certificate?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsUpdate.Execute("DELETE FROM GiftCertificateRegister WHERE ID = " + vsCertificates.TextMatrix(vsCertificates.Row, IDCol), "TWMV0000.vb1");
					vsCertificates.RemoveItem(vsCertificates.Row);
				}
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			if (fecherFoundation.Strings.Trim(txtGiftCertificateNumber.Text) == "")
			{
				MessageBox.Show("You must enter a gift certificate number before you may proceed.", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (fecherFoundation.Strings.Trim(txtPurchaser.Text) == "")
			{
				MessageBox.Show("You must enter a purchaser name before you may proceed.", "Invalid Purchaser", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (FCConvert.ToDecimal(txtAmount.Text) <= 0)
			{
				MessageBox.Show("You must enter a gift certificate amount before you may proceed.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			rsUpdate.OpenRecordset("SELECT * FROM GiftCertificateRegister");
			rsUpdate.AddNew();
			rsUpdate.Set_Fields("DateIssued", DateTime.Today);
			rsUpdate.Set_Fields("CertificateNumber", fecherFoundation.Strings.Trim(txtGiftCertificateNumber.Text));
			rsUpdate.Set_Fields("Name", fecherFoundation.Strings.Trim(txtPurchaser.Text));
			rsUpdate.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text));
			rsUpdate.Set_Fields("Issued", true);
			rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rsUpdate.Set_Fields("PeriodCloseoutID", 0);
			rsUpdate.Set_Fields("GiftCertOrVoucher", "G");
			rsUpdate.Set_Fields("RegistrationID", 0);
			rsUpdate.Update();
			txtPurchaser.Text = "";
			txtAmount.Text = "0.00";
			txtGiftCertificateNumber.Text = "";
			MessageBox.Show("Certificate saved.", "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmGiftCertificates_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmGiftCertificates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGiftCertificates properties;
			//frmGiftCertificates.FillStyle	= 0;
			//frmGiftCertificates.ScaleWidth	= 9045;
			//frmGiftCertificates.ScaleHeight	= 7200;
			//frmGiftCertificates.LinkTopic	= "Form2";
			//frmGiftCertificates.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FormatGrid();
			//FC:FINAL:MSH - i.issue #1876: don't need to change BackColors
			//SetCustomFormColors();
			tabProcess.SelectedIndex = 0;
		}

		private void SetCustomFormColors()
		{
			lblGiftCertificateNumber.BackColor = SystemColors.Control;
			lblAmount.BackColor = SystemColors.Control;
			lblApplicant.BackColor = SystemColors.Control;
		}

		private void FormatGrid()
		{
			IDCol = 0;
			DateCol = 1;
			GiftCertificateNumberCol = 2;
			NameCol = 3;
			AmountCol = 4;
			vsCertificates.ColHidden(IDCol, true);
			vsCertificates.TextMatrix(0, DateCol, "Date");
			vsCertificates.TextMatrix(0, GiftCertificateNumberCol, "Gift Cert #");
			vsCertificates.TextMatrix(0, NameCol, "Purchaser");
			vsCertificates.TextMatrix(0, AmountCol, "Amount");
			vsCertificates.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsCertificates.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCertificates.ColWidth(DateCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.15));
			vsCertificates.ColWidth(GiftCertificateNumberCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.2));
			vsCertificates.ColWidth(NameCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.45));
			vsCertificates.ColWidth(AmountCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.15));
		}

		private void frmGiftCertificates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmGiftCertificates_Resize(object sender, System.EventArgs e)
		{
			vsCertificates.ColWidth(DateCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.15));
			vsCertificates.ColWidth(GiftCertificateNumberCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.2));
			vsCertificates.ColWidth(NameCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.45));
			vsCertificates.ColWidth(AmountCol, FCConvert.ToInt32(vsCertificates.WidthOriginal * 0.15));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (tabProcess.SelectedIndex == 0)
			{
				cmdSave_Click();
			}
		}

		private void tabProcess_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			if (tabProcess.SelectedIndex == 1)
			{
				vsCertificates.Rows = 1;
				rs.OpenRecordset("SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID = 0", "TWMV0000.vb1");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					do
					{
						vsCertificates.Rows += 1;
						vsCertificates.TextMatrix(vsCertificates.Rows - 1, IDCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						vsCertificates.TextMatrix(vsCertificates.Rows - 1, GiftCertificateNumberCol, FCConvert.ToString(rs.Get_Fields_String("CertificateNumber")));
						vsCertificates.TextMatrix(vsCertificates.Rows - 1, DateCol, Strings.Format(rs.Get_Fields_DateTime("DateIssued"), "MM/dd/yy"));
						vsCertificates.TextMatrix(vsCertificates.Rows - 1, NameCol, FCConvert.ToString(rs.Get_Fields_String("Name")));
						vsCertificates.TextMatrix(vsCertificates.Rows - 1, AmountCol, Strings.Format(rs.Get_Fields("Amount"), "#,##0.00"));
						rs.MoveNext();
					}
					while (rs.EndOfFile() != true);
				}
			}
		}

		private void vsCertificates_DblClick(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}
	}
}
