﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmListBox : BaseForm
	{
		public frmListBox()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmListBox InstancePtr
		{
			get
			{
				return (frmListBox)Sys.GetInstance(typeof(frmListBox));
			}
		}

		protected frmListBox _InstancePtr = null;
		//=========================================================
		bool FormLoaded;

		private void frmListBox_Activated(object sender, System.EventArgs e)
		{
			FormLoaded = true;
		}

		private void frmListBox_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				lstSelectPlate_DblClick();
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				MotorVehicle.Statics.ReturnFromListBox = "";
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmListBox_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmListBox properties;
			//frmListBox.ScaleWidth	= 5880;
			//frmListBox.ScaleHeight	= 4380;
			//frmListBox.LinkTopic	= "Form1";
			//End Unmaped Properties
			//frmListBox.InstancePtr.Top = FCConvert.ToInt32((FCGlobal.Screen.Height - frmListBox.InstancePtr.Height) / 2.0);
			//frmListBox.InstancePtr.Left = FCConvert.ToInt32((FCGlobal.Screen.Width - frmListBox.InstancePtr.Width) / 2.0);
			frmListBox.InstancePtr.CenterToScreen();
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FormLoaded = false;
		}

		private void lstSelectPlate_DoubleClick(object sender, System.EventArgs e)
		{
			if (FormLoaded == false)
				return;
            if (lstSelectPlate.SelectedItems.Any() )
            {
                MotorVehicle.Statics.ReturnFromListBox = lstSelectPlate.Items[lstSelectPlate.SelectedIndex].Text;
                Close();
            }
            else
            {
                MessageBox.Show("Please select an item from the list box.", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
		}

		public void lstSelectPlate_DblClick()
		{
			lstSelectPlate_DoubleClick(lstSelectPlate, new System.EventArgs());
		}
	}
}
