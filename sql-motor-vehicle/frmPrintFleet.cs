//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWMV0000
{
	public partial class frmPrintFleet : BaseForm
	{
		public frmPrintFleet()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintFleet InstancePtr
		{
			get
			{
				return (frmPrintFleet)Sys.GetInstance(typeof(frmPrintFleet));
			}
		}

		protected frmPrintFleet _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsFleet = new clsDRWrapper();
		// vbPorter upgrade warning: LocalFee As Decimal	OnWrite(int, Decimal)
		Decimal LocalFee;
		// vbPorter upgrade warning: StateFee As Decimal	OnWrite(int, Decimal)
		Decimal StateFee;
		// vbPorter upgrade warning: TotalAgent As Decimal	OnWrite(int, Decimal)
		Decimal TotalAgent;
		// vbPorter upgrade warning: TotalExcise As Decimal	OnWrite(int, Decimal)
		Decimal TotalExcise;
		// vbPorter upgrade warning: TotalReg As Decimal	OnWrite(int, Decimal)
		Decimal TotalReg;
		// vbPorter upgrade warning: TotalInitial As Decimal	OnWrite(int, Decimal)
		Decimal TotalInitial;
		// vbPorter upgrade warning: TotalSpecialty As Decimal	OnWrite(int, Decimal)
		Decimal TotalSpecialty;
		// vbPorter upgrade warning: TotalCommCredit As Decimal	OnWrite
		Decimal TotalCommCredit;
		// vbPorter upgrade warning: TotalLocal As Decimal	OnWrite(int, Decimal)
		Decimal TotalLocal;
		// vbPorter upgrade warning: TotalState As Decimal	OnWrite(int, Decimal)
		Decimal TotalState;
		string strLine = "";
		public int FleetNumber;
		public int ExpMonth;
		bool NoSubclass;
		bool FeeWrong;
		int RegisterCol;
		int ClassCol;
		int VINCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int UnitCol;
		int LocalCol;
		int StateCol;
		int TotalCol;
		int CountCol;
		int AgentCol;
		int ExciseCol;
		int TotalLocalCol;
		int RegCol;
		int InitialCol;
		int SpecialtyCol;
		int TotalStateCol;
		int GrandTotalCol;
		bool SecondResize;
		int totalcount;

		private void cboFleets_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cmdDisplay_Click();
		}

		private void cmdDisplay_Click(object sender, System.EventArgs e)
		{
			int TotalVehicles = 0;

			if (cboFleets.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a fleet before you may view the Registration List.", "Invalid Fleet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			NoSubclass = false;
			GetFleetInfo();
			if (FleetNumber != 0)
			{
				MotorVehicle.Statics.rsVehicles.OpenRecordset("SELECT * FROM Master WHERE Status <> 'T' AND FleetNumber = " + FCConvert.ToString(FleetNumber));
				if (MotorVehicle.Statics.rsVehicles.EndOfFile() != true && MotorVehicle.Statics.rsVehicles.BeginningOfFile() != true)
				{
					MotorVehicle.Statics.rsVehicles.MoveLast();
					MotorVehicle.Statics.rsVehicles.MoveFirst();
					vsVehicles.Rows = 1;
					vsVehicles.Rows = MotorVehicle.Statics.rsVehicles.RecordCount() + 1;
					vsSummary.Rows = 1;
					vsSummary.Rows = 2;
					LocalFee = 0;
					StateFee = 0;
					TotalLocal = 0;
					TotalState = 0;
					TotalAgent = 0;
					TotalExcise = 0;
					TotalReg = 0;
					TotalInitial = 0;
					TotalSpecialty = 0;
					TotalCommCredit = 0;
					totalcount = 0;
					TotalVehicles = 1;
					do
					{
						totalcount += 1;
						FeeWrong = false;
						clsDRWrapper temp = MotorVehicle.Statics.rsVehicles;
						LocalFee = CalcLocalFee(ref temp);
						StateFee = CalcStateFee(ref temp);
						MotorVehicle.Statics.rsVehicles = temp;
						TotalLocal += LocalFee;
						TotalState += StateFee;
						vsVehicles.TextMatrix(TotalVehicles, RegisterCol, FCConvert.ToString(true));
						vsVehicles.TextMatrix(TotalVehicles, ClassCol, MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate"));
						vsVehicles.TextMatrix(TotalVehicles, VINCol, fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Vin"))));
						vsVehicles.TextMatrix(TotalVehicles, YearCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields("Year")));
						vsVehicles.TextMatrix(TotalVehicles, MakeCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("make")));
						vsVehicles.TextMatrix(TotalVehicles, ModelCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("model")));
						if (cmbFleet.Text == "Group")
						{
							vsVehicles.TextMatrix(TotalVehicles, UnitCol, Strings.Format(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy"));
						}
						else
						{
							vsVehicles.TextMatrix(TotalVehicles, UnitCol, FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_String("UnitNumber") + "")));
						}
						vsVehicles.TextMatrix(TotalVehicles, LocalCol, FCConvert.ToString(LocalFee));
						vsVehicles.TextMatrix(TotalVehicles, StateCol, FCConvert.ToString(StateFee));
						vsVehicles.TextMatrix(TotalVehicles, TotalCol, FCConvert.ToString(LocalFee + StateFee));
						vsVehicles.RowData(TotalVehicles, MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("ID"));
						if (FeeWrong)
						{
							vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, TotalVehicles, TotalCol, Color.Red);
						}
						TotalVehicles += 1;
						MotorVehicle.Statics.rsVehicles.MoveNext();
					}
					while (!MotorVehicle.Statics.rsVehicles.EndOfFile());
					vsSummary.TextMatrix(1, CountCol, FCConvert.ToString(totalcount));
					vsSummary.TextMatrix(1, AgentCol, FCConvert.ToString(TotalAgent));
					vsSummary.TextMatrix(1, ExciseCol, FCConvert.ToString(TotalExcise));
					vsSummary.TextMatrix(1, TotalLocalCol, FCConvert.ToString(TotalLocal));
					vsSummary.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, TotalLocalCol, 0xC000C0);
					vsSummary.TextMatrix(1, RegCol, FCConvert.ToString(TotalReg));
					vsSummary.TextMatrix(1, InitialCol, FCConvert.ToString(TotalInitial));
					vsSummary.TextMatrix(1, SpecialtyCol, FCConvert.ToString(TotalSpecialty));
					vsSummary.TextMatrix(1, TotalStateCol, FCConvert.ToString(TotalState));
					vsSummary.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, TotalStateCol, 0xC000C0);
					vsSummary.TextMatrix(1, GrandTotalCol, FCConvert.ToString(TotalState + TotalLocal));
					vsSummary.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, GrandTotalCol, Color.Blue);
					lblDetail.Visible = true;
					vsVehicles.Visible = true;
					lblSummary.Visible = true;
					vsSummary.Visible = true;
					ResizeGrids();
					frmWait.InstancePtr.Unload();
				}
				else
				{
					frmWait.InstancePtr.Unload();
					if (cmbFleet.Text == "Group")
					{
						MessageBox.Show("No vehicles found for this Group.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("No vehicles found for this Fleet.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				if (cmbFleet.Text == "Group")
				{
					MessageBox.Show("Invalid Group Number", "Invalid Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("Invalid Fleet Number", "Invalid Fleet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		public void cmdDisplay_Click()
		{
			cmdDisplay_Click(cmdDisplay, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			string FleetExpMonth = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strLine = "";

			if (cboFleets.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a fleet before you may continue", "Invalid Fleet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (vsSummary.TextMatrix(1, CountCol) == "0")
			{
				MessageBox.Show("You must select at least one vehicle to get information for before you may print this report.", "No Vehicles Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			GetFleetInfo();
			if (FleetNumber != 0)
			{
				MotorVehicle.Statics.rsVehicles.OpenRecordset("SELECT * FROM Master WHERE Status <> 'T' AND FleetNumber = " + FCConvert.ToString(FleetNumber));
				if (MotorVehicle.Statics.rsVehicles.EndOfFile() != true && MotorVehicle.Statics.rsVehicles.BeginningOfFile() != true)
				{
					MotorVehicle.Statics.rsVehicles.MoveLast();
					MotorVehicle.Statics.rsVehicles.MoveFirst();
					frmReportViewer.InstancePtr.Init(rptFleetRegistrationInfo.InstancePtr);
				}
				else
				{
					frmWait.InstancePtr.Unload();
					if (cmbFleet.Text == "Group")
					{
						MessageBox.Show("No vehicles found for this Group.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("No vehicles found for this Fleet.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				if (cmbFleet.Text == "Group")
				{
					MessageBox.Show("Invalid Group Number", "Invalid Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("Invalid Fleet Number", "Invalid Fleet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			rsTemp.Reset();
		}

		private void frmPrintFleet_Activated(object sender, System.EventArgs e)
		{
			int fnx;
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.FLEETLIST))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			RegisterCol = 0;
			ClassCol = 1;
			VINCol = 2;
			YearCol = 3;
			MakeCol = 4;
			ModelCol = 5;
			UnitCol = 6;
			LocalCol = 7;
			StateCol = 8;
			TotalCol = 9;
			CountCol = 0;
			AgentCol = 1;
			ExciseCol = 2;
			TotalLocalCol = 3;
			RegCol = 4;
			InitialCol = 5;
			SpecialtyCol = 6;
			TotalStateCol = 7;
			GrandTotalCol = 8;
			vsVehicles.ColDataType(RegisterCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVehicles.ColFormat(LocalCol, "#,##0.00");
			vsVehicles.ColFormat(StateCol, "#,##0.00");
			vsVehicles.ColFormat(TotalCol, "#,##0.00");
			ResizeGrids();
			vsVehicles.TextMatrix(0, ClassCol, "CL / Plate");
			vsVehicles.TextMatrix(0, VINCol, "VIN");
			vsVehicles.TextMatrix(0, YearCol, "Year");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			vsVehicles.TextMatrix(0, UnitCol, "Unit");
			vsVehicles.TextMatrix(0, LocalCol, "Local");
			vsVehicles.TextMatrix(0, StateCol, "State");
			vsVehicles.TextMatrix(0, TotalCol, "Total");
			//vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsVehicles.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			for (fnx = 1; fnx <= 8; fnx++)
			{
				vsSummary.ColFormat(fnx, "#,##0.00");
			}
			vsSummary.TextMatrix(0, CountCol, "Count");
			vsSummary.TextMatrix(0, AgentCol, "Agent Fee");
			vsSummary.TextMatrix(0, ExciseCol, "Excise Tax");
			vsSummary.TextMatrix(0, TotalLocalCol, "Total Local");
			vsSummary.TextMatrix(0, RegCol, "Reg Fee");
			vsSummary.TextMatrix(0, InitialCol, "Init Pl Fee");
			vsSummary.TextMatrix(0, SpecialtyCol, "Spec Pl Fee");
			vsSummary.TextMatrix(0, TotalStateCol, "Total State");
			vsSummary.TextMatrix(0, GrandTotalCol, "Grand Total");
			//vsSummary.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsSummary.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			if (cboFleets.SelectedIndex == -1)
			{
				FillFleetCombo();
			}
			this.Refresh();
		}

		private void FillFleetCombo()
		{
			lblDetail.Visible = false;
			vsVehicles.Visible = false;
			lblSummary.Visible = false;
			vsSummary.Visible = false;
			cboFleets.Clear();
			if (cmbFleet.Text == "Fleet")
			{
				if (cmbLongTerm.Text == "Long Term")
				{
					rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth <> 99 ORDER BY p.FullName");
				}
				else
				{
					rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type <> 'L' AND ExpiryMonth <> 99 ORDER BY p.FullName");
				}
			}
			else
			{
				if (cmbLongTerm.Text == "Long Term")
				{
					rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth = 99 ORDER BY p.FullName");
				}
				else
				{
					rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type <> 'L' AND ExpiryMonth = 99 ORDER BY p.FullName");
				}
			}
			if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
			{
				rsFleet.MoveLast();
				rsFleet.MoveFirst();
				do
				{
					cboFleets.AddItem(rsFleet.Get_Fields_Int32("FleetNumber") + Strings.Space(6 - FCConvert.ToString(rsFleet.Get_Fields_Int32("FleetNumber")).Length) + fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields_String("FullName"))));
					rsFleet.MoveNext();
				}
				while (rsFleet.EndOfFile() != true);
			}
		}

		private void frmPrintFleet_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void ResizeGrids()
		{
			int colWidth = vsVehicles.WidthOriginal;
			vsVehicles.ColWidth(0, (colWidth * 0.05).ToInteger());
			vsVehicles.ColWidth(ClassCol, (colWidth * 0.15).ToInteger());
			vsVehicles.ColWidth(VINCol, (colWidth * 0.19).ToInteger());
			vsVehicles.ColWidth(YearCol, (colWidth * 0.07).ToInteger());
			vsVehicles.ColWidth(MakeCol, (colWidth * 0.07).ToInteger());
			vsVehicles.ColWidth(ModelCol, (colWidth * 0.10).ToInteger());
			vsVehicles.ColWidth(UnitCol, (colWidth * 0.05).ToInteger());
			vsVehicles.ColWidth(LocalCol, (colWidth * 0.10).ToInteger());
			vsVehicles.ColWidth(StateCol, (colWidth * 0.10).ToInteger());
			vsVehicles.ColWidth(TotalCol, (colWidth * 0.10).ToInteger());

			colWidth = vsSummary.WidthOriginal;
			vsSummary.ColWidth(CountCol, (colWidth * 0.1).ToInteger());
			vsSummary.ColWidth(AgentCol, (colWidth * 0.1).ToInteger());
			vsSummary.ColWidth(ExciseCol, (colWidth * 0.1).ToInteger());
			vsSummary.ColWidth(TotalLocalCol, (colWidth * 0.1).ToInteger());
			vsSummary.ColWidth(RegCol, (colWidth * 0.1).ToInteger());
			vsSummary.ColWidth(InitialCol, (colWidth * 0.1).ToInteger());
			vsSummary.ColWidth(SpecialtyCol, (colWidth * 0.1).ToInteger());
			vsSummary.ColWidth(TotalStateCol, (colWidth * 0.13).ToInteger());
			vsSummary.ColWidth(GrandTotalCol, (colWidth * 0.13).ToInteger());
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
			rsFleet.Reset();
			MotorVehicle.Statics.rsVehicles.Reset();
		}

		private void frmPrintFleet_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(Decimal, int)
		private Decimal CalcStateFee(ref clsDRWrapper rs, bool blnAdded = true)
		{
			Decimal CalcStateFee = 0;
			// vbPorter upgrade warning: RegFee As Decimal	OnWrite(Decimal, string)
			Decimal RegFee;
			Decimal InitFee;
			Decimal CommCred;
			// vbPorter upgrade warning: SpecialFee As Decimal	OnWrite
			Decimal SpecialFee;
			int tempweight = 0;
            var specialtyClass =
                MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class"));

            if ( specialtyClass == "BH" || (specialtyClass == "BB") || (specialtyClass == "LB") || (specialtyClass == "CR") || (specialtyClass == "UM") || (specialtyClass == "AG") || (specialtyClass == "AC") || (specialtyClass == "AF") || (specialtyClass == "TS") || (specialtyClass == "BC") || (specialtyClass == "AW") || (specialtyClass == "LC"))
			{
				SpecialFee = 15;
			}
			else if (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "SW")
			{
				SpecialFee = 20;
			}
			else
			{
				SpecialFee = 0;
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) != "SE")
			{
				if (MotorVehicle.Statics.rsVehicles.IsFieldNull("RegisteredWeightNew"))
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(Math.Round(Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("RegisteredWeightNew"))));
				}
			}
			else
			{
				if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsVehicles.Get_Fields("NetWeight")))
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields("NetWeight"));
				}
			}
			RegFee = GetReg(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class"), MotorVehicle.Statics.rsVehicles.Get_Fields_String("Subclass"), tempweight);
			// kk11202015 changed from P4
			if (MotorVehicle.Statics.rsVehicles.Get_Fields_Boolean("RENTAL") && (FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "CR" || FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "UM" || (FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "PC" && FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Subclass")) != "P5")))
			{
				RegFee *= 2;
			}
			if (cmbFleet.Text == "Fleet")
			{
				if (Information.IsDate(MotorVehicle.Statics.rsVehicles.Get_Fields("ExpireDate")))
				{
					if (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) != ExpMonth)
					{
						RegFee = FCConvert.ToDecimal(Strings.Format(Pro(ref RegFee, DateDiff()), "##0.00"));
					}
				}
				else
				{
					FeeWrong = true;
				}
			}
			InitFee = MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class"), MotorVehicle.Statics.rsVehicles.Get_Fields_String("Plate"));
			CommCred = GetComm();
			CalcStateFee = RegFee - CommCred;
			if (blnAdded)
			{
				TotalReg += CalcStateFee;
				TotalInitial += InitFee;
				TotalSpecialty += SpecialFee;
			}
			else
			{
				TotalReg -= CalcStateFee;
				TotalInitial -= InitFee;
				TotalSpecialty -= SpecialFee;
			}
			if (CalcStateFee < 0)
				CalcStateFee = 0;
			CalcStateFee += InitFee + SpecialFee;
			return CalcStateFee;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(string)
		private Decimal CalcLocalFee(ref clsDRWrapper rs, bool blnAdded = true)
		{
			Decimal CalcLocalFee = 0;
			// vbPorter upgrade warning: MilYear As int	OnWriteFCConvert.ToInt32(
			int MilYear = 0;
			float MilRate = 0;
			double Base = 0;
			// vbPorter upgrade warning: Excise As Decimal	OnWrite(int, string)
			Decimal Excise;
			decimal AgentFee = 0;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string tempRes = "";
			clsDRWrapper rsExciseExempt = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs.Get_Fields_String("ResidenceCode").Length < 5)
			{
				tempRes = "0" + rs.Get_Fields_String("ResidenceCode");
			}
			else
			{
				tempRes = rs.Get_Fields_String("ResidenceCode");
			}
			if (rs.Get_Fields_String("Class") != "AP")
			{
				if (tempRes == FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode")))
				{
					AgentFee = FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegLocal"));
				}
				else
				{
					AgentFee = FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegOOT"));
				}
			}
			else
			{
				AgentFee = 0;
			}
			rsExciseExempt.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + rs.Get_Fields_String("Class") + "' AND SystemCode = '" + rs.Get_Fields_String("Subclass") + "'", "TWMV0000.vb1");
			if (rsExciseExempt.EndOfFile() != true && rsExciseExempt.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(rsExciseExempt.Get_Fields_String("ExciseRequired")) == "N")
				{
					Excise = 0;
				}
				else
				{
					if (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear")) == 0)
					{
						MilYear = ((DateTime.Today.Year - rs.Get_Fields("Year")) + 1);
						FeeWrong = true;
					}
					else
					{
						MilYear = MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear") + 1;
					}
					if (MilYear > 6)
					{
						MilYear = 6;
					}
					switch (MilYear)
					{
						case 1:
							{
								MilRate = 0.024f;
								break;
							}
						case 2:
							{
								MilRate = 0.0175f;
								break;
							}
						case 3:
							{
								MilRate = 0.0135f;
								break;
							}
						case 4:
							{
								MilRate = 0.01f;
								break;
							}
						case 5:
							{
								MilRate = 0.0065f;
								break;
							}
						case 6:
							{
								MilRate = 0.004f;
								break;
							}
					}
					//end switch
					Base = rs.Get_Fields_Int32("BasePrice");
					if (Base == 0)
					{
						FeeWrong = true;
					}
					Excise = FCConvert.ToDecimal(Strings.Format(MilRate * Base, "#0.00"));
					if (cmbFleet.Text == "Fleet")
					{
						if (Information.IsDate(MotorVehicle.Statics.rsVehicles.Get_Fields("ExpireDate")))
						{
							if (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) != ExpMonth)
							{
								Excise = FCConvert.ToDecimal(Strings.Format(Pro(ref Excise, DateDiff()), "#,##0.00"));
							}
						}
						else
						{
							FeeWrong = true;
						}
					}
					if (Excise < 5)
						Excise = 5;
				}
			}
			else
			{
				if (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear")) == 0)
				{
					MilYear = ((DateTime.Today.Year - rs.Get_Fields("Year")) + 1);
					FeeWrong = true;
				}
				else
				{
					MilYear = MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear") + 1;
				}
				if (MilYear > 6)
				{
					MilYear = 6;
				}
				switch (MilYear)
				{
					case 1:
						{
							MilRate = 0.024f;
							break;
						}
					case 2:
						{
							MilRate = 0.0175f;
							break;
						}
					case 3:
						{
							MilRate = 0.0135f;
							break;
						}
					case 4:
						{
							MilRate = 0.01f;
							break;
						}
					case 5:
						{
							MilRate = 0.0065f;
							break;
						}
					case 6:
						{
							MilRate = 0.004f;
							break;
						}
				}
				//end switch
				Base = rs.Get_Fields_Int32("BasePrice");
				if (Base == 0)
				{
					FeeWrong = true;
				}
				Excise = FCConvert.ToDecimal(Strings.Format(MilRate * Base, "#0.00"));
				if (cmbFleet.Text == "Fleet")
				{
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate")))
					{
						if (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) != ExpMonth)
						{
							Excise = FCConvert.ToDecimal(Strings.Format(Pro(ref Excise, DateDiff()), "#,##0.00"));
						}
					}
					else
					{
						FeeWrong = true;
					}
				}
				if (Excise < 5)
					Excise = 5;
			}
			if (blnAdded)
			{
				TotalExcise += Excise;
				TotalAgent += AgentFee;
			}
			else
			{
				TotalExcise -= Excise;
				TotalAgent -= AgentFee;
			}
			CalcLocalFee = FCConvert.ToDecimal(Strings.Format(Excise + AgentFee, "#0.00"));
			rsDefaultInfo.Reset();
			return CalcLocalFee;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite
		private Decimal GetReg(string Class, string Subclass, int weight)
		{
			Decimal GetReg = 0;
			int WT;
			clsDRWrapper rsClassFee = new clsDRWrapper();
			clsDRWrapper rsGVWFee = new clsDRWrapper();
			GetReg = 0;
			WT = weight;
			if (FCConvert.ToString(Class) != "AM" && Class != "BH" && FCConvert.ToString(Class) != "BU" && FCConvert.ToString(Class) != "CL" && FCConvert.ToString(Class) != "DV" && FCConvert.ToString(Class) != "FD" && FCConvert.ToString(Class) != "IU" && FCConvert.ToString(Class) != "TL" && FCConvert.ToString(Class) != "MC" && FCConvert.ToString(Class) != "PC" && FCConvert.ToString(Class) != "TR" && FCConvert.ToString(Class) != "VT" && FCConvert.ToString(Class) != "XV" && FCConvert.ToString(Class) != "VX" && FCConvert.ToString(Class) != "EM")
			{
				// tromvs-97 6.25.18 add EM
				Subclass = Class;
			}
			MotorVehicle.Statics.strSql = "SELECT * FROM CLASS WHERE BMVCode = '" + Class + "' And SystemCode =  '" + Subclass + "'";
			rsClassFee.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsClassFee.EndOfFile() != true && rsClassFee.BeginningOfFile() != true)
			{
				rsClassFee.MoveLast();
				rsClassFee.MoveFirst();
				if (fecherFoundation.FCUtils.IsNull(rsClassFee.Get_Fields("RegistrationFee")) == false)
				{
					if (rsClassFee.Get_Fields_Double("RegistrationFee") == 0.01 || rsClassFee.Get_Fields_Double("RegistrationFee") == 0.02 || rsClassFee.Get_Fields_Double("RegistrationFee") == 0.03)
					{
						string tempClass = "";
						tempClass = "FM";
						if (rsClassFee.Get_Fields_Double("RegistrationFee") == 0.01)
						{
							tempClass = "TK";
						}
						else if (rsClassFee.Get_Fields_Double("RegistrationFee") == 0.03 && WT > 54000 && FCConvert.ToString(Class) != "SE")
						{
							tempClass = "F2";
						}
						else if (Class == "SE")
						{
							tempClass = "SE";
						}
						MotorVehicle.Statics.strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = '" + tempClass + "' AND Low <= " + FCConvert.ToString(WT) + " AND High >= " + FCConvert.ToString(WT);
						rsGVWFee.OpenRecordset(MotorVehicle.Statics.strSql);
						if (rsGVWFee.EndOfFile() != true && rsGVWFee.BeginningOfFile() != true)
						{
							rsGVWFee.MoveLast();
							rsGVWFee.MoveFirst();
							GetReg = FCConvert.ToDecimal(rsGVWFee.Get_Fields("Fee"));
							if (GetReg == 0)
								GetReg = 35;
							rsClassFee.Reset();
							rsGVWFee.Reset();
							return GetReg;
						}
					}
					GetReg = FCConvert.ToDecimal(rsClassFee.Get_Fields("RegistrationFee"));
				}
				else
				{
					GetReg = 0;
				}
			}
			else
			{
				FeeWrong = true;
			}
			rsClassFee.Reset();
			rsGVWFee.Reset();
			return GetReg;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite
		private Decimal GetComm()
		{
			Decimal GetComm = 0;
			if (Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate")) > 799999 && Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate")) < 900000)
			{
				if (Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("RegisteredWeightNew")) > 23000)
				{
					if (Strings.Mid(FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Style")), 2, 1) == "5")
					{
						GetComm = 40;
					}
					else
					{
						GetComm = 0;
					}
				}
			}
			return GetComm;
		}

		private void optAnnual_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
			vsVehicles.TextMatrix(0, UnitCol, "Unit");
			vsVehicles.ColDataType(UnitCol, FCGrid.DataTypeSettings.flexDTString);
		}

		private void optGroup_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
			vsVehicles.TextMatrix(0, UnitCol, "Expires");
			vsVehicles.ColDataType(UnitCol, FCGrid.DataTypeSettings.flexDTDate);
		}

		private Decimal Pro(ref Decimal x, int y)
		{
			Decimal Pro = 0;
			// vbPorter upgrade warning: total As double	OnWrite(Decimal, double)
			double total;
			total = FCConvert.ToDouble(x / 12);
			total *= y;
			Pro = FCConvert.ToDecimal(total);
			return Pro;
		}

		private int DateDiff()
		{
			int DateDiff = 0;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x = 0;
			if (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) < ExpMonth)
			{
				x = ExpMonth - FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) + 1;
			}
			else
			{
				x = 12 - (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) - ExpMonth) - 1;
			}
			if (x == 0)
			{
				x = 12;
			}
			DateDiff = x;
			return DateDiff;
		}

		private void GetFleetInfo()
		{
			clsDRWrapper rsFleetInfo = new clsDRWrapper();
			FleetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(Strings.Mid(cboFleets.Text, 1, 5)))));
			rsFleetInfo.OpenRecordset("SELECT * FROM FleetMaster WHERE FleetNumber = " + FCConvert.ToString(FleetNumber));
			ExpMonth = FCConvert.ToInt16(rsFleetInfo.Get_Fields_Int32("ExpiryMonth"));
		}

		private void optLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
		}

		private void vsVehicles_ClickEvent(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - wrong input value for FCConvert.ToBoolean (same with i.issue #1883)
			//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) != true)
			if (FCUtils.CBool(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) != true)
			{
				vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
				Recalculate(vsVehicles.Row, true);
			}
			else
			{
				vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
				Recalculate(vsVehicles.Row, false);
			}
		}

		private void vsVehicles_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				//FC:FINAL:MSH - wrong input value for FCConvert.ToBoolean (same with i.issue #1883)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) != true)
				if (FCUtils.CBool(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) != true)
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
					Recalculate(vsVehicles.Row, true);
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
					Recalculate(vsVehicles.Row, false);
				}
			}
		}

		private void Recalculate(int lngRow = -1, bool blnAdded = true)
		{
			int counter;
			MotorVehicle.Statics.rsVehicles.MoveFirst();
			counter = 1;
			if (lngRow == -1)
			{
				LocalFee = 0;
				StateFee = 0;
				TotalLocal = 0;
				TotalState = 0;
				TotalAgent = 0;
				TotalExcise = 0;
				TotalReg = 0;
				TotalInitial = 0;
				TotalSpecialty = 0;
				TotalCommCredit = 0;
				totalcount = 0;
				do
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, RegisterCol)) == true)
					if (FCConvert.CBool(vsVehicles.TextMatrix(counter, RegisterCol)) == true)
					{
						clsDRWrapper temp = MotorVehicle.Statics.rsVehicles;
						LocalFee = CalcLocalFee(ref temp);
						StateFee = CalcStateFee(ref temp);
						MotorVehicle.Statics.rsVehicles = temp;
						TotalLocal += LocalFee;
						TotalState += StateFee;
						totalcount += 1;
					}
					counter += 1;
					MotorVehicle.Statics.rsVehicles.MoveNext();
				}
				while (MotorVehicle.Statics.rsVehicles.EndOfFile() != true);
			}
			else
			{
				if (MotorVehicle.Statics.rsVehicles.FindFirstRecord("ID", vsVehicles.RowData(lngRow)))
				{
					clsDRWrapper temp = MotorVehicle.Statics.rsVehicles;
					if (blnAdded)
					{
						LocalFee = CalcLocalFee(ref temp);
						StateFee = CalcStateFee(ref temp);
						TotalLocal += LocalFee;
						TotalState += StateFee;
						totalcount += 1;
					}
					else
					{
						LocalFee = CalcLocalFee(ref temp, false);
						StateFee = CalcStateFee(ref temp, false);
						TotalLocal -= LocalFee;
						TotalState -= StateFee;
						totalcount -= 1;
					}
					MotorVehicle.Statics.rsVehicles = temp;
				}
			}
			vsSummary.TextMatrix(1, CountCol, FCConvert.ToString(totalcount));
			vsSummary.TextMatrix(1, AgentCol, FCConvert.ToString(TotalAgent));
			vsSummary.TextMatrix(1, ExciseCol, FCConvert.ToString(TotalExcise));
			vsSummary.TextMatrix(1, TotalLocalCol, FCConvert.ToString(TotalLocal));
			vsSummary.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, TotalLocalCol, 0xC000C0);
			vsSummary.TextMatrix(1, RegCol, FCConvert.ToString(TotalReg));
			vsSummary.TextMatrix(1, InitialCol, FCConvert.ToString(TotalInitial));
			vsSummary.TextMatrix(1, SpecialtyCol, FCConvert.ToString(TotalSpecialty));
			vsSummary.TextMatrix(1, TotalStateCol, FCConvert.ToString(TotalState));
			vsSummary.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, TotalStateCol, 0xC000C0);
			vsSummary.TextMatrix(1, GrandTotalCol, FCConvert.ToString(TotalState + TotalLocal));
			vsSummary.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, GrandTotalCol, Color.Blue);
		}

		public void cmbFleet_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbFleet.Text == "Group")
			{
				optGroup_CheckedChanged(sender, e);
			}
			else if (cmbFleet.Text == "Fleet")
			{
				optFleet_CheckedChanged(sender, e);
			}
		}

		public void cmbLongTerm_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbLongTerm.Text == "Annual")
			{
				optAnnual_CheckedChanged(sender, e);
			}
			else if (cmbLongTerm.Text == "Long Term")
			{
				optLongTerm_CheckedChanged(sender, e);
			}
		}
	}
}
