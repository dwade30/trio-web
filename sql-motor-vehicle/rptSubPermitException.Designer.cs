﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubPermitException.
	/// </summary>
	partial class rptSubPermitException
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSubPermitException));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldPermitNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPermitType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldApplicantName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalMoney = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldPermitNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPermitType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldApplicantName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldPermitNumber,
            this.fldPermitType,
            this.fldApplicantName,
            this.fldOpID,
            this.fldClassPlate,
            this.fldFee,
            this.fldDate});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldPermitNumber
			// 
			this.fldPermitNumber.Height = 0.19F;
			this.fldPermitNumber.Left = 0.825F;
			this.fldPermitNumber.Name = "fldPermitNumber";
			this.fldPermitNumber.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldPermitNumber.Text = "Field1";
			this.fldPermitNumber.Top = 0F;
			this.fldPermitNumber.Width = 0.726F;
			// 
			// fldPermitType
			// 
			this.fldPermitType.Height = 0.19F;
			this.fldPermitType.Left = 1.6375F;
			this.fldPermitType.Name = "fldPermitType";
			this.fldPermitType.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldPermitType.Text = "Field1";
			this.fldPermitType.Top = 0F;
			this.fldPermitType.Width = 1F;
			// 
			// fldApplicantName
			// 
			this.fldApplicantName.Height = 0.19F;
			this.fldApplicantName.Left = 2.732F;
			this.fldApplicantName.Name = "fldApplicantName";
			this.fldApplicantName.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldApplicantName.Text = "Field1";
			this.fldApplicantName.Top = 0F;
			this.fldApplicantName.Width = 1.9995F;
			// 
			// fldOpID
			// 
			this.fldOpID.Height = 0.19F;
			this.fldOpID.Left = 7.0415F;
			this.fldOpID.Name = "fldOpID";
			this.fldOpID.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldOpID.Text = "Field1";
			this.fldOpID.Top = 0F;
			this.fldOpID.Width = 0.5F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.19F;
			this.fldClassPlate.Left = 4.827001F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldClassPlate.Text = "Field1";
			this.fldClassPlate.Top = 0F;
			this.fldClassPlate.Width = 1.093F;
			// 
			// fldFee
			// 
			this.fldFee.Height = 0.19F;
			this.fldFee.Left = 6.028F;
			this.fldFee.Name = "fldFee";
			this.fldFee.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.fldFee.Text = "Field1";
			this.fldFee.Top = 0F;
			this.fldFee.Width = 0.84375F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label2,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label10,
            this.Label11,
            this.Label12,
            this.label3,
            this.label4});
			this.GroupHeader1.Height = 1.021F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.8245001F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.Label1.Text = "PERMIT";
			this.Label1.Top = 0.67475F;
			this.Label1.Width = 0.7264999F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.637F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label2.Text = "PERMIT TYPE";
			this.Label2.Top = 0.831F;
			this.Label2.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.7315F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label5.Text = "APPLICANT NAME--------------";
			this.Label5.Top = 0.831F;
			this.Label5.Width = 2F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 7.041F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.Label6.Text = "OPID";
			this.Label6.Top = 0.831F;
			this.Label6.Width = 0.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.8245001F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.Label7.Text = "NUMBER";
			this.Label7.Top = 0.831F;
			this.Label7.Width = 0.7264999F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.5625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label10.Text = "---- PERMITS ----";
			this.Label10.Top = 0.15625F;
			this.Label10.Width = 1.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.857751F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label11.Text = "CLASS / PLATE";
			this.Label11.Top = 0.831F;
			this.Label11.Width = 1.06225F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6.027501F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "FEE";
			this.Label12.Top = 0.831F;
			this.Label12.Width = 0.84375F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label9,
            this.fldTotalCount,
            this.fldTotalMoney});
			this.GroupFooter1.Height = 0.3229167F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.03125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label9.Text = "TOTAL PERMITS";
			this.Label9.Top = 0.0625F;
			this.Label9.Width = 1.21875F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 1.562F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldTotalCount.Text = "Field1";
			this.fldTotalCount.Top = 0.062F;
			this.fldTotalCount.Width = 0.75F;
			// 
			// label3
			// 
			this.label3.Height = 0.1875F;
			this.label3.HyperLink = null;
			this.label3.Left = 1.249875F;
			this.label3.Name = "label3";
			this.label3.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.label3.Text = "(ALL FORMS MUST BE SENT TO BUREAU OF MOTOR VEHICLES) ";
			this.label3.Top = 0.341F;
			this.label3.Width = 4.50025F;
			// 
			// fldTotalMoney
			// 
			this.fldTotalMoney.Height = 0.1875F;
			this.fldTotalMoney.Left = 3.192708F;
			this.fldTotalMoney.Name = "fldTotalMoney";
			this.fldTotalMoney.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldTotalMoney.Text = "Field1";
			this.fldTotalMoney.Top = 0.06770834F;
			this.fldTotalMoney.Width = 1.15625F;
			// 
			// label4
			// 
			this.label4.Height = 0.19F;
			this.label4.HyperLink = null;
			this.label4.Left = 0F;
			this.label4.Name = "label4";
			this.label4.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.label4.Text = "DATE";
			this.label4.Top = 0.831F;
			this.label4.Width = 0.6210001F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 0F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.726F;
			// 
			// rptSubPermitException
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.541667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldPermitNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPermitType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldApplicantName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPermitNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPermitType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldApplicantName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMoney;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label label4;
	}
}
