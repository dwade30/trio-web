﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptInventoryAdjustments.
	/// </summary>
	partial class rptInventoryAdjustments
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptInventoryAdjustments));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtDT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtQuantity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOperator = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExplanation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtNoInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMVR3Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSingleStickerCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDoubleStickerCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCombinationStickerCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.srptPlateCounts = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBoosterCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSpecialPermitCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTransitCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOperator)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExplanation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSingleStickerCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoubleStickerCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCombinationStickerCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBoosterCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpecialPermitCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransitCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDT,
            this.txtQuantity,
            this.txtRange,
            this.txtOperator,
            this.txtExplanation,
            this.txtDesc});
			this.Detail.Height = 0.4375F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtDT
			// 
			this.txtDT.CanShrink = true;
			this.txtDT.Height = 0.1875F;
			this.txtDT.Left = 0.0625F;
			this.txtDT.Name = "txtDT";
			this.txtDT.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
			this.txtDT.Text = null;
			this.txtDT.Top = 0.25F;
			this.txtDT.Width = 0.75F;
			// 
			// txtQuantity
			// 
			this.txtQuantity.CanShrink = true;
			this.txtQuantity.Height = 0.1875F;
			this.txtQuantity.Left = 1.03125F;
			this.txtQuantity.Name = "txtQuantity";
			this.txtQuantity.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.txtQuantity.Text = null;
			this.txtQuantity.Top = 0.25F;
			this.txtQuantity.Width = 0.4375F;
			// 
			// txtRange
			// 
			this.txtRange.CanShrink = true;
			this.txtRange.Height = 0.1875F;
			this.txtRange.Left = 1.6875F;
			this.txtRange.Name = "txtRange";
			this.txtRange.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
			this.txtRange.Text = null;
			this.txtRange.Top = 0.25F;
			this.txtRange.Width = 1.8125F;
			// 
			// txtOperator
			// 
			this.txtOperator.CanShrink = true;
			this.txtOperator.Height = 0.1875F;
			this.txtOperator.Left = 3.625F;
			this.txtOperator.Name = "txtOperator";
			this.txtOperator.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.txtOperator.Text = null;
			this.txtOperator.Top = 0.25F;
			this.txtOperator.Width = 0.6875F;
			// 
			// txtExplanation
			// 
			this.txtExplanation.CanShrink = true;
			this.txtExplanation.Height = 0.1875F;
			this.txtExplanation.Left = 4.5F;
			this.txtExplanation.Name = "txtExplanation";
			this.txtExplanation.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: left; " +
    "ddo-char-set: 1";
			this.txtExplanation.Text = null;
			this.txtExplanation.Top = 0.25F;
			this.txtExplanation.Width = 2.9375F;
			// 
			// txtDesc
			// 
			this.txtDesc.CanShrink = true;
			this.txtDesc.Height = 0.25F;
			this.txtDesc.Left = 1.5F;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
			this.txtDesc.Text = null;
			this.txtDesc.Top = 0F;
			this.txtDesc.Width = 2.25F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtNoInfo});
			this.ReportFooter.Height = 0.5104167F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// txtNoInfo
			// 
			this.txtNoInfo.CanShrink = true;
			this.txtNoInfo.Height = 0.3125F;
			this.txtNoInfo.Left = 2.5625F;
			this.txtNoInfo.Name = "txtNoInfo";
			this.txtNoInfo.Style = "font-family: \\000027Roman\\00002010cpi\\000027; font-size: 12pt; font-weight: bold;" +
    " ddo-char-set: 1";
			this.txtNoInfo.Text = null;
			this.txtNoInfo.Top = 0.0625F;
			this.txtNoInfo.Width = 2.6875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanShrink = true;
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport2,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label1,
            this.lblPage,
            this.Label33});
			this.GroupHeader1.Height = 1.875F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.4375F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.375F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.0625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: center; ddo-char-set: 1" +
    "";
			this.Label12.Text = "Date Adjusted";
			this.Label12.Top = 1.5F;
			this.Label12.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.375F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \\000027Roman\\00002010cpi\\000027; ddo-char-set: 1";
			this.Label13.Text = "Quantity Adjusted";
			this.Label13.Top = 1.5F;
			this.Label13.Width = 0.75F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.375F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.1875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: center; ddo-char-set: 1" +
    "";
			this.Label14.Text = "Range Adjusted";
			this.Label14.Top = 1.5F;
			this.Label14.Width = 0.75F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.375F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \\000027Roman\\00002010cpi\\000027; ddo-char-set: 1";
			this.Label15.Text = "Operator Initials";
			this.Label15.Top = 1.5F;
			this.Label15.Width = 0.75F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 4.5F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \\000027Roman\\00002010cpi\\000027; ddo-char-set: 1";
			this.Label16.Text = "Explanation";
			this.Label16.Top = 1.6875F;
			this.Label16.Width = 2.75F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.6875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \\000027Roman\\00002010cpi\\000027; ddo-char-set: 1";
			this.Label1.Text = "**** INVENTORY ADJUSTMENT REPORT ****";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \\000027Roman\\00002010cpi\\000027; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 0.9375F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \\000027Roman\\00002010cpi\\000027; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label17,
            this.Label18,
            this.Label19,
            this.fldMVR3Count,
            this.Label20,
            this.Label21,
            this.fldSingleStickerCount,
            this.Label22,
            this.fldDoubleStickerCount,
            this.Label23,
            this.fldCombinationStickerCount,
            this.srptPlateCounts,
            this.Label25,
            this.Label26,
            this.fldBoosterCount,
            this.Label27,
            this.fldSpecialPermitCount,
            this.Label28,
            this.fldTransitCount});
			this.GroupFooter1.Height = 2.008333F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \\000027Roman\\00002010cpi\\000027; font-weight: bold; text-align: left" +
    "; ddo-char-set: 1";
			this.Label17.Text = "SUMMARY OF INVENTORY ADJUSTMENTS:";
			this.Label17.Top = 0.1875F;
			this.Label17.Width = 3.125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label18.Text = "FORMS:";
			this.Label18.Top = 0.56F;
			this.Label18.Width = 0.75F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 1F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label19.Text = "MVR-3";
			this.Label19.Top = 0.56F;
			this.Label19.Width = 1.3125F;
			// 
			// fldMVR3Count
			// 
			this.fldMVR3Count.CanShrink = true;
			this.fldMVR3Count.Height = 0.1875F;
			this.fldMVR3Count.Left = 2.938F;
			this.fldMVR3Count.Name = "fldMVR3Count";
			this.fldMVR3Count.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.fldMVR3Count.Text = null;
			this.fldMVR3Count.Top = 0.56F;
			this.fldMVR3Count.Width = 0.5625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label20.Text = "STICKERS:";
			this.Label20.Top = 0.76F;
			this.Label20.Width = 0.9375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label21.Text = "SINGLE";
			this.Label21.Top = 0.76F;
			this.Label21.Width = 1.3125F;
			// 
			// fldSingleStickerCount
			// 
			this.fldSingleStickerCount.CanShrink = true;
			this.fldSingleStickerCount.Height = 0.1875F;
			this.fldSingleStickerCount.Left = 2.938F;
			this.fldSingleStickerCount.Name = "fldSingleStickerCount";
			this.fldSingleStickerCount.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.fldSingleStickerCount.Text = null;
			this.fldSingleStickerCount.Top = 0.76F;
			this.fldSingleStickerCount.Width = 0.5625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.9995F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label22.Text = "DOUBLE";
			this.Label22.Top = 0.96F;
			this.Label22.Width = 1.3125F;
			// 
			// fldDoubleStickerCount
			// 
			this.fldDoubleStickerCount.CanShrink = true;
			this.fldDoubleStickerCount.Height = 0.1875F;
			this.fldDoubleStickerCount.Left = 2.938F;
			this.fldDoubleStickerCount.Name = "fldDoubleStickerCount";
			this.fldDoubleStickerCount.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.fldDoubleStickerCount.Text = null;
			this.fldDoubleStickerCount.Top = 0.96F;
			this.fldDoubleStickerCount.Width = 0.5625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.9995F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label23.Text = "COMBINATION";
			this.Label23.Top = 1.16F;
			this.Label23.Width = 1.3125F;
			// 
			// fldCombinationStickerCount
			// 
			this.fldCombinationStickerCount.CanShrink = true;
			this.fldCombinationStickerCount.Height = 0.1875F;
			this.fldCombinationStickerCount.Left = 2.938F;
			this.fldCombinationStickerCount.Name = "fldCombinationStickerCount";
			this.fldCombinationStickerCount.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.fldCombinationStickerCount.Text = null;
			this.fldCombinationStickerCount.Top = 1.16F;
			this.fldCombinationStickerCount.Width = 0.5625F;
			// 
			// srptPlateCounts
			// 
			this.srptPlateCounts.CloseBorder = false;
			this.srptPlateCounts.Height = 0F;
			this.srptPlateCounts.Left = 0F;
			this.srptPlateCounts.Name = "srptPlateCounts";
			this.srptPlateCounts.Report = null;
			this.srptPlateCounts.Top = 1.36F;
			this.srptPlateCounts.Width = 3.5F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label25.Text = "PERMITS:";
			this.Label25.Top = 1.36F;
			this.Label25.Width = 0.9375F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 1F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label26.Text = "BOOSTERS";
			this.Label26.Top = 1.36F;
			this.Label26.Width = 1.3125F;
			// 
			// fldBoosterCount
			// 
			this.fldBoosterCount.CanShrink = true;
			this.fldBoosterCount.Height = 0.1875F;
			this.fldBoosterCount.Left = 2.938F;
			this.fldBoosterCount.Name = "fldBoosterCount";
			this.fldBoosterCount.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.fldBoosterCount.Text = null;
			this.fldBoosterCount.Top = 1.36F;
			this.fldBoosterCount.Width = 0.5625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 1F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label27.Text = "MVR-10";
			this.Label27.Top = 1.56F;
			this.Label27.Width = 1.3125F;
			// 
			// fldSpecialPermitCount
			// 
			this.fldSpecialPermitCount.CanShrink = true;
			this.fldSpecialPermitCount.Height = 0.1875F;
			this.fldSpecialPermitCount.Left = 2.938F;
			this.fldSpecialPermitCount.Name = "fldSpecialPermitCount";
			this.fldSpecialPermitCount.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.fldSpecialPermitCount.Text = null;
			this.fldSpecialPermitCount.Top = 1.56F;
			this.fldSpecialPermitCount.Width = 0.5625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 1F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \\000027Roman\\00002010cpi\\000027; text-align: left; ddo-char-set: 1";
			this.Label28.Text = "TRANSIT";
			this.Label28.Top = 1.76F;
			this.Label28.Width = 1.3125F;
			// 
			// fldTransitCount
			// 
			this.fldTransitCount.CanShrink = true;
			this.fldTransitCount.Height = 0.1875F;
			this.fldTransitCount.Left = 2.9375F;
			this.fldTransitCount.Name = "fldTransitCount";
			this.fldTransitCount.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: center" +
    "; ddo-char-set: 1";
			this.fldTransitCount.Text = null;
			this.fldTransitCount.Top = 1.76F;
			this.fldTransitCount.Width = 0.5625F;
			// 
			// rptInventoryAdjustments
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOperator)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExplanation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSingleStickerCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoubleStickerCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCombinationStickerCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBoosterCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpecialPermitCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransitCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtQuantity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRange;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOperator;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExplanation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoInfo;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMVR3Count;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSingleStickerCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDoubleStickerCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCombinationStickerCount;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptPlateCounts;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBoosterCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpecialPermitCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransitCount;
	}
}
