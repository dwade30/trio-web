﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptProcessFinancial.
	/// </summary>
	public partial class rptProcessFinancial : BaseSectionReport
	{
        private bool processAll = false;
        private bool createJournal = false;

		public rptProcessFinancial()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Rapid Renewal Financial Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptProcessFinancial InstancePtr
		{
			get
			{
				return (rptProcessFinancial)Sys.GetInstance(typeof(rptProcessFinancial));
			}
		}

		protected rptProcessFinancial _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptProcessFinancial	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double UncodedTotal;
		int TransactionCount;
		double TransactionTotal;
		string SavePlate = "";
		int PageCounter;
		// vbPorter upgrade warning: curExcise As Decimal	OnWrite(int, Decimal)
		Decimal curExcise;
		// vbPorter upgrade warning: curMerchantFee As Decimal	OnWrite(int, Decimal)
		Decimal curMerchantFee;
		string strDownloadPath;
		int intProcessRow;

		private bool checkEOF()
		{
			bool checkEOF = false;
			CheckAgain:
			;
			if (!FCFileSystem.EOF(2))
			{
				checkEOF = false;
			}
			else
			{
				if (processAll)
				{
					if (intProcessRow < menuRapidRenewal.InstancePtr.vsFinancial.Rows - 1)
					{
						FCFileSystem.FileClose(2);
						intProcessRow += 1;
						FCFileSystem.FileOpen(2, strDownloadPath + menuRapidRenewal.InstancePtr.vsFinancial.TextMatrix(intProcessRow, 1), OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
						goto CheckAgain;
					}
					else
					{
						checkEOF = true;
					}
				}
				else
				{
					checkEOF = true;
				}
			}
			return checkEOF;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!checkEOF())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				FCFileSystem.FileClose(2);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			int lngJournal = 0;
			if (createJournal)
			{
				lngJournal = 0;
				if (curExcise != 0 || curMerchantFee != 0)
				{
					MotorVehicle.CreateJournalEntry(ref lngJournal, ref curExcise, ref curMerchantFee);
					if (lngJournal == 0)
					{
						MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						menuRapidRenewal.InstancePtr.blnPostJournal = false;
					}
					else
					{
						if (modSecurity.ValidPermissions(null, MotorVehicle.AUTOPOSTRAPIDRENEWAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptMVRapidRenewal))
						{
							if (MessageBox.Show("The rapid renewal deposit has been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								menuRapidRenewal.InstancePtr.clsJournalInfo = new clsPostingJournalInfo();
								menuRapidRenewal.InstancePtr.clsJournalInfo.JournalNumber = lngJournal;
								menuRapidRenewal.InstancePtr.clsJournalInfo.JournalType = "CR";
								menuRapidRenewal.InstancePtr.clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
								menuRapidRenewal.InstancePtr.clsJournalInfo.CheckDate = "";
								menuRapidRenewal.InstancePtr.clsPostInfo.AddJournal(menuRapidRenewal.InstancePtr.clsJournalInfo);
								menuRapidRenewal.InstancePtr.blnPostJournal = true;
							}
							else
							{
								menuRapidRenewal.InstancePtr.blnPostJournal = false;
							}
						}
						else
						{
							menuRapidRenewal.InstancePtr.blnPostJournal = false;
							MessageBox.Show("The rapid renewal deposit has been saved into journal " + Strings.Format(lngJournal, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			int counter2;
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label33.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label32.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			curExcise = 0;
			curMerchantFee = 0;
			intProcessRow = 0;
			for (counter = 0; counter <= 100; counter++)
			{
				for (counter2 = 0; counter2 <= 4; counter2++)
				{
					MotorVehicle.Statics.TransactionDatesMoney[counter, counter2] = 0;
					MotorVehicle.Statics.ClassMoney[counter, counter2] = 0;
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string rec = "";
			string strLine = "";
			double ExciseTotal;
			double AgentFeeTotal;
			double RegFeeTotal;
			string plate;
			string strClass;
			string TransactionDate;
			string TransactionType;
			double Amount;
			string VIN;
			int counter;
			bool found;
			rec = FCFileSystem.LineInput(2);
			found = false;
			TransactionDate = Strings.Mid(rec, 15, 2) + "/" + Strings.Mid(rec, 17, 2) + "/" + Strings.Mid(rec, 11, 4);
			strClass = fecherFoundation.Strings.Trim(Strings.Mid(rec, 9, 2));
			plate = fecherFoundation.Strings.Trim(Strings.Mid(rec, 1, 8));
			TransactionType = fecherFoundation.Strings.Trim(Strings.Mid(rec, 19, 8));
			Amount = Conversion.Val(fecherFoundation.Strings.Trim(Strings.Mid(rec, 27, 8))) / 100;
			VIN = fecherFoundation.Strings.Trim(Strings.Mid(rec, 35, 17));
			if (SavePlate != plate)
			{
				SavePlate = plate;
				if (TransactionType != "MERCFEE")
				{
					TransactionCount += 1;
					txtDate.Text = TransactionDate;
					txtPlate.Text = strClass + " " + Strings.Format(plate, "!@@@@@@@@");
					txtVin.Text = VIN;
					txtType.Text = Strings.Format(TransactionType, "@@@@@@@@");
					txtAmount.Text = Strings.Format(Strings.Format(Amount, "#,##0.00"), "@@@@@@@@");
				}
				else
				{
					txtDate.Text = "";
					txtPlate.Text = "";
					txtVin.Text = "MERCHANT FEE";
					txtType.Text = "";
					txtAmount.Text = Strings.Format(Strings.Format(Amount * -1, "#,##0.00"), "@@@@@@@@");
				}
			}
			else
			{
				// strLine = "      "  " & Format(TransactionType, "@@@@@@@@") & "    " & Format(Format(amount, "#,##0.00"), "@@@@@@@@") & vbNewLine
				txtDate.Text = "";
				txtPlate.Text = "";
				txtVin.Text = "";
				txtType.Text = Strings.Format(TransactionType, "@@@@@@@@");
				txtAmount.Text = Strings.Format(Strings.Format(Amount, "#,##0.00"), "@@@@@@@@");
			}
			if (TransactionType != "MERCFEE")
			{
				TransactionTotal += Amount;
				for (counter = 0; counter <= MotorVehicle.Statics.ClassMax; counter++)
				{
					if (MotorVehicle.Statics.ClassDesc[counter] == strClass)
					{
						found = true;
						if (TransactionType == "REGFEE")
						{
							MotorVehicle.Statics.ClassMoney[counter, 0] += Amount;
						}
						else if (TransactionType == "VANITY")
						{
							MotorVehicle.Statics.ClassMoney[counter, 1] += Amount;
						}
						else if (TransactionType == "EXCISE")
						{
							MotorVehicle.Statics.ClassMoney[counter, 2] += Amount;
						}
						else if (TransactionType == "AGTFEE")
						{
							MotorVehicle.Statics.ClassMoney[counter, 3] += Amount;
						}
						else
						{
							MotorVehicle.Statics.ClassMoney[counter, 4] += Amount;
						}
						break;
					}
				}
				if (!found)
				{
					MotorVehicle.Statics.ClassMax += 1;
					MotorVehicle.Statics.ClassDesc[MotorVehicle.Statics.ClassMax - 1] = strClass;
					if (TransactionType == "REGFEE")
					{
						MotorVehicle.Statics.ClassMoney[MotorVehicle.Statics.ClassMax - 1, 0] += Amount;
					}
					else if (TransactionType == "VANITY")
					{
						MotorVehicle.Statics.ClassMoney[MotorVehicle.Statics.ClassMax - 1, 1] += Amount;
					}
					else if (TransactionType == "EXCISE")
					{
						MotorVehicle.Statics.ClassMoney[MotorVehicle.Statics.ClassMax - 1, 2] += Amount;
					}
					else if (TransactionType == "AGTFEE")
					{
						MotorVehicle.Statics.ClassMoney[MotorVehicle.Statics.ClassMax - 1, 3] += Amount;
					}
					else
					{
						MotorVehicle.Statics.ClassMoney[MotorVehicle.Statics.ClassMax - 1, 4] += Amount;
					}
				}
				found = false;
				for (counter = 0; counter <= MotorVehicle.Statics.DateMax; counter++)
				{
					if (MotorVehicle.Statics.TransactionDates[counter] == TransactionDate)
					{
						found = true;
						if (TransactionType == "REGFEE")
						{
							MotorVehicle.Statics.TransactionDatesMoney[counter, 0] += Amount;
						}
						else if (TransactionType == "VANITY")
						{
							MotorVehicle.Statics.TransactionDatesMoney[counter, 1] += Amount;
						}
						else if (TransactionType == "EXCISE")
						{
							MotorVehicle.Statics.TransactionDatesMoney[counter, 2] += Amount;
						}
						else if (TransactionType == "AGTFEE")
						{
							MotorVehicle.Statics.TransactionDatesMoney[counter, 3] += Amount;
						}
						else
						{
							MotorVehicle.Statics.TransactionDatesMoney[counter, 4] += Amount;
						}
						break;
					}
				}
				if (!found)
				{
					MotorVehicle.Statics.DateMax += 1;
					MotorVehicle.Statics.TransactionDates[MotorVehicle.Statics.DateMax - 1] = TransactionDate;
					if (TransactionType == "REGFEE")
					{
						MotorVehicle.Statics.TransactionDatesMoney[MotorVehicle.Statics.DateMax - 1, 0] += Amount;
					}
					else if (TransactionType == "VANITY")
					{
						MotorVehicle.Statics.TransactionDatesMoney[MotorVehicle.Statics.DateMax - 1, 1] += Amount;
					}
					else if (TransactionType == "EXCISE")
					{
						MotorVehicle.Statics.TransactionDatesMoney[MotorVehicle.Statics.DateMax - 1, 2] += Amount;
					}
					else if (TransactionType == "AGTFEE")
					{
						MotorVehicle.Statics.TransactionDatesMoney[MotorVehicle.Statics.DateMax - 1, 3] += Amount;
					}
					else
					{
						MotorVehicle.Statics.TransactionDatesMoney[MotorVehicle.Statics.DateMax - 1, 4] += Amount;
					}
				}
				if (TransactionType == "EXCISE")
				{
					curExcise += FCConvert.ToDecimal(Amount);
				}
			}
			else
			{
				TransactionTotal -= Amount;
				curMerchantFee += FCConvert.ToDecimal(Amount);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			lblTrans.Text = "Transactions Processed - " + FCConvert.ToString(TransactionCount);
			lblTotal.Text = "Total     -  " + Strings.Space(9) + Strings.Format(TransactionTotal, "@@@@@@@@");
			fldExciseTotal.Text = Strings.Format(curExcise, "#,##0.00");
			fldMerchantFee.Text = Strings.Format(curMerchantFee * -1, "#,##0.00");
			fldNet.Text = Strings.Format(curExcise - curMerchantFee, "#,##0.00");
			if (MotorVehicle.Statics.DateMax > 0 || MotorVehicle.Statics.ClassMax > 0)
			{
				SubReport1.Report = new srptProcessFinancialSubtotals();
			}
			else
			{
				SubReport1.Report = null;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void Init(string strDLPath, bool processAll, bool createJournal)
		{
			strDownloadPath = strDLPath;
            this.processAll = processAll;
            this.createJournal = createJournal;

			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: true);
		}
    }
}
