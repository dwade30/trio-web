//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReminderLabels.
	/// </summary>
	public partial class rptReminderLabels : BaseSectionReport
	{
		public rptReminderLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Reminder Labels";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptReminderLabels InstancePtr
		{
			get
			{
				return (rptReminderLabels)Sys.GetInstance(typeof(rptReminderLabels));
			}
		}

		protected rptReminderLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int counter;
		// kk05162017 tromv-1115  Change from Integer to Long
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: Amount As double	OnWriteFCConvert.ToDecimal(
			double Amount = 0;
			Decimal amount1 = 0;
			Decimal amount2 = 0;
			Decimal amount3 = 0;
			if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			if (eArgs.EOF)
				return;
			//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
			//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			{
				fldPlate.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
				fldPlate2.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
				fldYearMakeModel.Text = fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4)) + " " + fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5)) + " " + fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6));
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
				{
					amount1 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7));
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8) != "")
				{
					amount2 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8));
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9) != "")
				{
					amount3 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9));
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
				{
					Amount = FCConvert.ToDouble(amount1 + amount2 + amount3);
					fldAmount.Text = Strings.Format(Amount, "$#,##0.00");
				}
				else
				{
					fldAmount.Text = "Call Office for Amount";
				}
				fldName.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
				if (!MotorVehicle.Statics.rsReport.FindFirstRecord("ID", Conversion.Val(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 0))))
				{
					// do nothing
				}
				else
				{
					fldExpires.Text = Strings.Format(MotorVehicle.Statics.rsReport.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
					if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsReport.Get_Fields_String("Address")))
					{
						fldAddress.Text = MotorVehicle.Statics.rsReport.Get_Fields_String("Address");
					}
					else
					{
						fldAddress.Text = "";
					}
					fldCityStateZip.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("Zip")));
				}
			}
			else
			{
				counter += 1;
				while (counter < frmReminder.InstancePtr.vsVehicles.Rows)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					{
						fldPlate.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
						fldPlate2.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
						fldYearMakeModel.Text = fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4)) + " " + fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5)) + " " + fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6));
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
						{
							amount1 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7));
						}
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8) != "")
						{
							amount2 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8));
						}
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9) != "")
						{
							amount3 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9));
						}
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
						{
							Amount = FCConvert.ToDouble(amount1 + amount2 + amount3);
							fldAmount.Text = Strings.Format(Amount, "$#,##0.00");
						}
						else
						{
							fldAmount.Text = "Call Office for Amount";
						}
						fldName.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
						if (!MotorVehicle.Statics.rsReport.FindFirstRecord("ID", Conversion.Val(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 0))))
						{
							// do nothing
						}
						else
						{
							fldExpires.Text = Strings.Format(MotorVehicle.Statics.rsReport.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
							fldAddress.Text = MotorVehicle.Statics.rsReport.Get_Fields_String("Address");
							fldCityStateZip.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("Zip")));
						}
						break;
					}
					else
					{
						counter += 1;
					}
				}
				if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
				{
					eArgs.EOF = true;
					return;
				}
			}
			counter += 1;
			eArgs.EOF = false;
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			counter = 1;
			//rptReminderLabels.InstancePtr.Printer.RenderMode = 1;
			//Application.DoEvents();
		}

		private void rptReminderLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderLabels properties;
			//rptReminderLabels.Caption	= "Reminder Labels";
			//rptReminderLabels.Icon	= "rptReminderLabels.dsx":0000";
			//rptReminderLabels.Left	= 0;
			//rptReminderLabels.Top	= 0;
			//rptReminderLabels.Width	= 11880;
			//rptReminderLabels.Height	= 8595;
			//rptReminderLabels.StartUpPosition	= 3;
			//rptReminderLabels.SectionData	= "rptReminderLabels.dsx":058A;
			//End Unmaped Properties
		}
	}
}
