//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmVoidBooster.
	/// </summary>
	partial class frmVoidBooster
	{
		public fecherFoundation.FCComboBox cmbBooster;
		public fecherFoundation.FCLabel lblBooster;
		public fecherFoundation.FCTextBox txtReason;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdYes;
		public fecherFoundation.FCButton cmdNO;
		public fecherFoundation.FCLabel lblStickers;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCLabel lblVehicle;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel lblPlateClass;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCTextBox txtVoidedMVR3;
		public fecherFoundation.FCLabel lblReason;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblDescription;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbBooster = new fecherFoundation.FCComboBox();
            this.lblBooster = new fecherFoundation.FCLabel();
            this.txtReason = new fecherFoundation.FCTextBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmdYes = new fecherFoundation.FCButton();
            this.cmdNO = new fecherFoundation.FCButton();
            this.lblStickers = new fecherFoundation.FCLabel();
            this.lblDate = new fecherFoundation.FCLabel();
            this.lblVehicle = new fecherFoundation.FCLabel();
            this.lblAddress = new fecherFoundation.FCLabel();
            this.lblPlateClass = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.txtVoidedMVR3 = new fecherFoundation.FCTextBox();
            this.lblReason = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(680, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbBooster);
            this.ClientArea.Controls.Add(this.lblBooster);
            this.ClientArea.Controls.Add(this.txtReason);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.txtVoidedMVR3);
            this.ClientArea.Controls.Add(this.lblReason);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(680, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(680, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(139, 30);
            this.HeaderText.Text = "Void Permit";
            // 
            // cmbBooster
            // 
            this.cmbBooster.AutoSize = false;
            this.cmbBooster.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbBooster.FormattingEnabled = true;
            this.cmbBooster.Items.AddRange(new object[] {
            "Booster",
            "Transit Plate",
            "Special Reg Permit"});
            this.cmbBooster.Location = new System.Drawing.Point(363, 30);
            this.cmbBooster.Name = "cmbBooster";
            this.cmbBooster.Size = new System.Drawing.Size(271, 40);
            this.cmbBooster.TabIndex = 0;
            this.cmbBooster.Text = "Booster";
            this.cmbBooster.SelectedIndexChanged += new System.EventHandler(this.cmbBooster_SelectedIndexChanged);
            // 
            // lblBooster
            // 
            this.lblBooster.AutoSize = true;
            this.lblBooster.Location = new System.Drawing.Point(30, 44);
            this.lblBooster.Name = "lblBooster";
            this.lblBooster.Size = new System.Drawing.Size(90, 15);
            this.lblBooster.TabIndex = 1;
            this.lblBooster.Text = "PERMIT TYPE";
            // 
            // txtReason
            // 
			this.txtReason.MaxLength = 29;
            this.txtReason.AutoSize = false;
            this.txtReason.LinkItem = null;
            this.txtReason.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtReason.LinkTopic = null;
            this.txtReason.Location = new System.Drawing.Point(144, 435);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(264, 40);
            this.txtReason.TabIndex = 11;
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Enabled = false;
            this.cmdProcess.Location = new System.Drawing.Point(224, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(143, 48);
            this.cmdProcess.TabIndex = 15;
            this.cmdProcess.Text = "Process Void";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmdYes);
            this.Frame1.Controls.Add(this.cmdNO);
            this.Frame1.Controls.Add(this.lblStickers);
            this.Frame1.Controls.Add(this.lblDate);
            this.Frame1.Controls.Add(this.lblVehicle);
            this.Frame1.Controls.Add(this.lblAddress);
            this.Frame1.Controls.Add(this.lblPlateClass);
            this.Frame1.Controls.Add(this.lblName);
            this.Frame1.Location = new System.Drawing.Point(30, 185);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(378, 230);
            this.Frame1.TabIndex = 5;
            this.Frame1.Text = "Is This The Correct Information?";
            this.Frame1.Visible = false;
            // 
            // cmdYes
            // 
            this.cmdYes.AppearanceKey = "actionButton";
            this.cmdYes.Location = new System.Drawing.Point(194, 170);
            this.cmdYes.Name = "cmdYes";
            this.cmdYes.Size = new System.Drawing.Size(69, 40);
            this.cmdYes.TabIndex = 2;
            this.cmdYes.Text = "YES";
            this.cmdYes.Click += new System.EventHandler(this.cmdYes_Click);
            // 
            // cmdNO
            // 
            this.cmdNO.AppearanceKey = "actionButton";
            this.cmdNO.Location = new System.Drawing.Point(116, 170);
            this.cmdNO.Name = "cmdNO";
            this.cmdNO.Size = new System.Drawing.Size(64, 40);
            this.cmdNO.TabIndex = 3;
            this.cmdNO.Text = "NO";
            this.cmdNO.Click += new System.EventHandler(this.cmdNO_Click);
            // 
            // lblStickers
            // 
            this.lblStickers.Location = new System.Drawing.Point(6, 142);
            this.lblStickers.Name = "lblStickers";
            this.lblStickers.Size = new System.Drawing.Size(364, 17);
            this.lblStickers.TabIndex = 12;
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(6, 119);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(364, 17);
            this.lblDate.TabIndex = 10;
            // 
            // lblVehicle
            // 
            this.lblVehicle.Location = new System.Drawing.Point(6, 72);
            this.lblVehicle.Name = "lblVehicle";
            this.lblVehicle.Size = new System.Drawing.Size(364, 17);
            this.lblVehicle.TabIndex = 9;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(6, 49);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(364, 17);
            this.lblAddress.TabIndex = 8;
            // 
            // lblPlateClass
            // 
            this.lblPlateClass.Location = new System.Drawing.Point(6, 95);
            this.lblPlateClass.Name = "lblPlateClass";
            this.lblPlateClass.Size = new System.Drawing.Size(364, 17);
            this.lblPlateClass.TabIndex = 7;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(6, 26);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(364, 17);
            this.lblName.TabIndex = 6;
            // 
            // txtVoidedMVR3
            // 
			this.txtVoidedMVR3.MaxLength = 10;
            this.txtVoidedMVR3.AutoSize = false;
            this.txtVoidedMVR3.BackColor = System.Drawing.SystemColors.Window;
            this.txtVoidedMVR3.LinkItem = null;
            this.txtVoidedMVR3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtVoidedMVR3.LinkTopic = null;
            this.txtVoidedMVR3.Location = new System.Drawing.Point(363, 90);
            this.txtVoidedMVR3.Name = "txtVoidedMVR3";
            this.txtVoidedMVR3.Size = new System.Drawing.Size(271, 40);
            this.txtVoidedMVR3.TabIndex = 1;
            this.txtVoidedMVR3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtVoidedMVR3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtVoidedMVR3_KeyPress);
            this.txtVoidedMVR3.Leave += new System.EventHandler(this.txtVoidedMVR3_Leave);
            // 
            // lblReason
            // 
            this.lblReason.Location = new System.Drawing.Point(30, 449);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(48, 17);
            this.lblReason.TabIndex = 14;
            this.lblReason.Text = "REASON";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(363, 150);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(192, 15);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "PRESS ENTER TO  RETRIEVE DATA";
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(27, 104);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(316, 30);
            this.lblDescription.TabIndex = 0;
            this.lblDescription.Text = "ENTER THE BOOSTER NUMBER TO VOID";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileProcess,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileProcess
            // 
            this.mnuFileProcess.Index = 0;
            this.mnuFileProcess.Name = "mnuFileProcess";
            this.mnuFileProcess.Text = "Process";
            this.mnuFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // frmVoidBooster
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(680, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmVoidBooster";
            this.Text = "Void Permit";
            this.Load += new System.EventHandler(this.frmVoidBooster_Load);
            this.FormClosed += new Wisej.Web.FormClosedEventHandler(FrmVoidBooster_FormClosed);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion
    }
}
