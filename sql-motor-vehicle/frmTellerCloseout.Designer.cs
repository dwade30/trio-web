//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTellerCloseout.
	/// </summary>
	partial class frmTellerCloseout
	{
		public fecherFoundation.FCComboBox cmbAll;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkTeller;
		public fecherFoundation.FCFrame fraSelect;
		public fecherFoundation.FCFrame fraTellers;
		public fecherFoundation.FCCheckBox chkTeller_12;
		public fecherFoundation.FCCheckBox chkTeller_13;
		public fecherFoundation.FCCheckBox chkTeller_14;
		public fecherFoundation.FCCheckBox chkTeller_15;
		public fecherFoundation.FCCheckBox chkTeller_11;
		public fecherFoundation.FCCheckBox chkTeller_10;
		public fecherFoundation.FCCheckBox chkTeller_9;
		public fecherFoundation.FCCheckBox chkTeller_8;
		public fecherFoundation.FCCheckBox chkTeller_7;
		public fecherFoundation.FCCheckBox chkTeller_6;
		public fecherFoundation.FCCheckBox chkTeller_5;
		public fecherFoundation.FCCheckBox chkTeller_4;
		public fecherFoundation.FCCheckBox chkTeller_3;
		public fecherFoundation.FCCheckBox chkTeller_2;
		public fecherFoundation.FCCheckBox chkTeller_1;
		public fecherFoundation.FCCheckBox chkTeller_0;
		public fecherFoundation.FCButton cmdCloseout;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.fraSelect = new fecherFoundation.FCFrame();
			this.fraTellers = new fecherFoundation.FCFrame();
			this.chkTeller_12 = new fecherFoundation.FCCheckBox();
			this.chkTeller_13 = new fecherFoundation.FCCheckBox();
			this.chkTeller_14 = new fecherFoundation.FCCheckBox();
			this.chkTeller_15 = new fecherFoundation.FCCheckBox();
			this.chkTeller_11 = new fecherFoundation.FCCheckBox();
			this.chkTeller_10 = new fecherFoundation.FCCheckBox();
			this.chkTeller_9 = new fecherFoundation.FCCheckBox();
			this.chkTeller_8 = new fecherFoundation.FCCheckBox();
			this.chkTeller_7 = new fecherFoundation.FCCheckBox();
			this.chkTeller_6 = new fecherFoundation.FCCheckBox();
			this.chkTeller_5 = new fecherFoundation.FCCheckBox();
			this.chkTeller_4 = new fecherFoundation.FCCheckBox();
			this.chkTeller_3 = new fecherFoundation.FCCheckBox();
			this.chkTeller_2 = new fecherFoundation.FCCheckBox();
			this.chkTeller_1 = new fecherFoundation.FCCheckBox();
			this.chkTeller_0 = new fecherFoundation.FCCheckBox();
			this.cmdCloseout = new fecherFoundation.FCButton();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelect)).BeginInit();
			this.fraSelect.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTellers)).BeginInit();
			this.fraTellers.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseout)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdCloseout);
			this.BottomPanel.Location = new System.Drawing.Point(0, 465);
			this.BottomPanel.Size = new System.Drawing.Size(807, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraSelect);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(807, 405);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(807, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(178, 30);
			this.HeaderText.Text = "Teller Closeout ";
			// 
			// cmbAll
			// 
			this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Closeout Selected"});
			this.cmbAll.Location = new System.Drawing.Point(20, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(237, 40);
			this.cmbAll.TabIndex = 5;
			this.cmbAll.Text = "All";
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
			// 
			// fraSelect
			// 
			this.fraSelect.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraSelect.Controls.Add(this.fraTellers);
			this.fraSelect.Controls.Add(this.cmbAll);
			this.fraSelect.Location = new System.Drawing.Point(30, 65);
			this.fraSelect.Name = "fraSelect";
			this.fraSelect.Size = new System.Drawing.Size(750, 274);
			this.fraSelect.TabIndex = 2;
			this.fraSelect.Text = "Tellers To Closeout";
			// 
			// fraTellers
			// 
			this.fraTellers.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraTellers.AppearanceKey = "groupBoxNoBorders";
			this.fraTellers.Controls.Add(this.chkTeller_12);
			this.fraTellers.Controls.Add(this.chkTeller_13);
			this.fraTellers.Controls.Add(this.chkTeller_14);
			this.fraTellers.Controls.Add(this.chkTeller_15);
			this.fraTellers.Controls.Add(this.chkTeller_11);
			this.fraTellers.Controls.Add(this.chkTeller_10);
			this.fraTellers.Controls.Add(this.chkTeller_9);
			this.fraTellers.Controls.Add(this.chkTeller_8);
			this.fraTellers.Controls.Add(this.chkTeller_7);
			this.fraTellers.Controls.Add(this.chkTeller_6);
			this.fraTellers.Controls.Add(this.chkTeller_5);
			this.fraTellers.Controls.Add(this.chkTeller_4);
			this.fraTellers.Controls.Add(this.chkTeller_3);
			this.fraTellers.Controls.Add(this.chkTeller_2);
			this.fraTellers.Controls.Add(this.chkTeller_1);
			this.fraTellers.Controls.Add(this.chkTeller_0);
			this.fraTellers.Enabled = false;
			this.fraTellers.Location = new System.Drawing.Point(20, 90);
			this.fraTellers.Name = "fraTellers";
			this.fraTellers.Size = new System.Drawing.Size(724, 164);
			this.fraTellers.TabIndex = 4;
			// 
			// chkTeller_12
			// 
			this.chkTeller_12.Location = new System.Drawing.Point(580, 0);
			this.chkTeller_12.Name = "chkTeller_12";
			this.chkTeller_12.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_12.TabIndex = 20;
			this.chkTeller_12.Visible = false;
			// 
			// chkTeller_13
			// 
			this.chkTeller_13.Location = new System.Drawing.Point(580, 46);
			this.chkTeller_13.Name = "chkTeller_13";
			this.chkTeller_13.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_13.TabIndex = 19;
			this.chkTeller_13.Visible = false;
			// 
			// chkTeller_14
			// 
			this.chkTeller_14.Location = new System.Drawing.Point(580, 92);
			this.chkTeller_14.Name = "chkTeller_14";
			this.chkTeller_14.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_14.TabIndex = 18;
			this.chkTeller_14.Visible = false;
			// 
			// chkTeller_15
			// 
			this.chkTeller_15.Location = new System.Drawing.Point(580, 138);
			this.chkTeller_15.Name = "chkTeller_15";
			this.chkTeller_15.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_15.TabIndex = 17;
			this.chkTeller_15.Visible = false;
			// 
			// chkTeller_11
			// 
			this.chkTeller_11.Location = new System.Drawing.Point(390, 138);
			this.chkTeller_11.Name = "chkTeller_11";
			this.chkTeller_11.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_11.TabIndex = 16;
			this.chkTeller_11.Visible = false;
			// 
			// chkTeller_10
			// 
			this.chkTeller_10.Location = new System.Drawing.Point(390, 92);
			this.chkTeller_10.Name = "chkTeller_10";
			this.chkTeller_10.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_10.TabIndex = 15;
			this.chkTeller_10.Visible = false;
			// 
			// chkTeller_9
			// 
			this.chkTeller_9.Location = new System.Drawing.Point(390, 46);
			this.chkTeller_9.Name = "chkTeller_9";
			this.chkTeller_9.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_9.TabIndex = 14;
			this.chkTeller_9.Visible = false;
			// 
			// chkTeller_8
			// 
			this.chkTeller_8.Location = new System.Drawing.Point(390, 0);
			this.chkTeller_8.Name = "chkTeller_8";
			this.chkTeller_8.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_8.TabIndex = 13;
			this.chkTeller_8.Visible = false;
			// 
			// chkTeller_7
			// 
			this.chkTeller_7.Location = new System.Drawing.Point(200, 138);
			this.chkTeller_7.Name = "chkTeller_7";
			this.chkTeller_7.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_7.TabIndex = 12;
			this.chkTeller_7.Visible = false;
			// 
			// chkTeller_6
			// 
			this.chkTeller_6.Location = new System.Drawing.Point(200, 92);
			this.chkTeller_6.Name = "chkTeller_6";
			this.chkTeller_6.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_6.TabIndex = 11;
			this.chkTeller_6.Visible = false;
			// 
			// chkTeller_5
			// 
			this.chkTeller_5.Location = new System.Drawing.Point(200, 46);
			this.chkTeller_5.Name = "chkTeller_5";
			this.chkTeller_5.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_5.TabIndex = 10;
			this.chkTeller_5.Visible = false;
			// 
			// chkTeller_4
			// 
			this.chkTeller_4.Location = new System.Drawing.Point(200, 0);
			this.chkTeller_4.Name = "chkTeller_4";
			this.chkTeller_4.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_4.TabIndex = 9;
			this.chkTeller_4.Visible = false;
			// 
			// chkTeller_3
			// 
			this.chkTeller_3.Location = new System.Drawing.Point(0, 138);
			this.chkTeller_3.Name = "chkTeller_3";
			this.chkTeller_3.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_3.TabIndex = 8;
			this.chkTeller_3.Visible = false;
			// 
			// chkTeller_2
			// 
			this.chkTeller_2.Location = new System.Drawing.Point(0, 92);
			this.chkTeller_2.Name = "chkTeller_2";
			this.chkTeller_2.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_2.TabIndex = 7;
			this.chkTeller_2.Visible = false;
			// 
			// chkTeller_1
			// 
			this.chkTeller_1.Location = new System.Drawing.Point(0, 46);
			this.chkTeller_1.Name = "chkTeller_1";
			this.chkTeller_1.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_1.TabIndex = 6;
			this.chkTeller_1.Visible = false;
			// 
			// chkTeller_0
			// 
			this.chkTeller_0.Name = "chkTeller_0";
			this.chkTeller_0.Size = new System.Drawing.Size(22, 26);
			this.chkTeller_0.TabIndex = 5;
			this.chkTeller_0.Visible = false;
			// 
			// cmdCloseout
			// 
			this.cmdCloseout.AppearanceKey = "acceptButton";
			this.cmdCloseout.Location = new System.Drawing.Point(334, 30);
			this.cmdCloseout.Name = "cmdCloseout";
			this.cmdCloseout.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdCloseout.Size = new System.Drawing.Size(178, 48);
			this.cmdCloseout.Text = "Closeout Teller(s)";
			this.cmdCloseout.Click += new System.EventHandler(this.cmdCloseout_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(750, 15);
			this.lblInstructions.TabIndex = 22;
			this.lblInstructions.Text = "PLEASE SELECT THE TELLERS YOU WISH TO CLOSEOUT AND CLICK THE \"CLOSEOUT TELLER(S)\"" +
    " BUTTON TO END THE TELLER PERIOD";
			this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmTellerCloseout
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(807, 573);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmTellerCloseout";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Teller Closeout ";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmTellerCloseout_Load);
			this.Activated += new System.EventHandler(this.frmTellerCloseout_Activated);
			this.Resize += new System.EventHandler(this.frmTellerCloseout_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTellerCloseout_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelect)).EndInit();
			this.fraSelect.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTellers)).EndInit();
			this.fraTellers.ResumeLayout(false);
			this.fraTellers.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTeller_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseout)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}