//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;

namespace TWMV0000
{
	public partial class frmInventory : BaseForm
	{
		public frmInventory()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmInventory InstancePtr
		{
			get
			{
				return (frmInventory)Sys.GetInstance(typeof(frmInventory));
			}
		}

		protected frmInventory _InstancePtr = null;
		//=========================================================
		string strSQL;
		string strSQLItems;
		clsDRWrapper rsPlates = new clsDRWrapper();
		clsDRWrapper rsItems = new clsDRWrapper();
		clsDRWrapper rsPrint = new clsDRWrapper();
		FCFileSystem ff = new FCFileSystem();
		public string InventoryPrinter = "";
		string TitleType = "";
		int NumberOfRecords;
		int[] Stickers = new int[20 + 1];
		// vbPorter upgrade warning: MaxElements As int	OnWriteFCConvert.ToInt32(
		int MaxElements;
		// vbPorter upgrade warning: fnx As int	OnRead
		int fnx;
		int TypeCol;
		int OpIDCol;
		int LowCol;
		int HighCol;
		int StatusCol;
		int GroupCol;
		int DateCol;
		int CountCol;
		double[] dblColPerecnts = new double[7 + 1];
		public int intSelectedGroup;
		public string strHeading = "";
		public int intIndex;
		// vbPorter upgrade warning: intRowHeight As int	OnWriteFCConvert.ToInt32(
		int intRowHeight;
		public string strSortBy = "";

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUsers = new clsDRWrapper();
			clsDRWrapper rsCheck = new clsDRWrapper();
			rsUsers.OpenRecordset("SELECT * FROM Operators WHERE Code = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUsers.EndOfFile() != true && rsUsers.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(Conversion.Val(rsUsers.Get_Fields_String("Level"))) > 2)
				{
					MessageBox.Show("Your User Level is too low to Add or Delete Inventory.", "Invalid User Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("Your User Level is too low to Add or Delete Inventory.", "Invalid User Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (MotorVehicle.Statics.ListType == "MVR")
			{
				frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
                optInventoryType_Click(0);
			}
			else if (MotorVehicle.Statics.ListType == "P")
			{
				if (lstPlates.SelectedIndex != -1)
				{
                    MotorVehicle.Statics.ListSubType = Strings.Mid(lstPlates.Items[lstPlates.SelectedIndex].Text, 1, 2);
                }
				else
				{
					MessageBox.Show("Please select a plate type from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
                lstPlates_DblClick();
            }
			else if ((MotorVehicle.Statics.ListType == "DY") || (MotorVehicle.Statics.ListType == "SY") || (MotorVehicle.Statics.ListType == "COMBINATION"))
			{
				if (lstItems.SelectedIndex != -1)
				{
                    MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Items[lstItems.SelectedIndex].Text);
				}
				else
				{
					MessageBox.Show("Please select a sticker year from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
                lstItems_DblClick();
			}
			else if ((MotorVehicle.Statics.ListType == "DM") || (MotorVehicle.Statics.ListType == "SM"))
			{
				if (lstItems.SelectedIndex != -1)
				{
					MotorVehicle.Statics.ListSubType = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
				}
				else
				{
					MessageBox.Show("Please select a sticker month from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
                lstItems_DblClick();
			}
			else if (MotorVehicle.Statics.ListType == "DECAL")
			{
				if (lstItems.SelectedIndex != -1)
				{
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Text);
					MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Items[lstItems.SelectedIndex].Text);
				}
				else
				{
					MessageBox.Show("Please select a decal year from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// kk05182018 tromvs-96  Warn against adding Truck Camper decals
				if (DialogResult.Yes == MessageBox.Show("Effective November 1, 2017, municipalities will no longer be registering truck campers and therefore will not be issuing decals." + "\r\n" + "Do you want to continue?", "Confirm Add/Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
				{
					frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
                    lstItems_DblClick();
				}
			}
			else if ( MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
			{
				frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
                optInventoryType_Click(8);
			}
			else if (MotorVehicle.Statics.ListType == "BOOSTER")
            {
                frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
                optInventoryType_Click(7);
			}
			else
			{
				MessageBox.Show("Please select an item to add.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cmdChange_Click(object sender, System.EventArgs e)
		{
			int intVBTab;
			string strHold;
			if (vs1.Row < 1)
			{
				MessageBox.Show("You must select a range of inventory whose group you wish to change before you may continue this process.", "No Range Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmInventoryAdjust.InstancePtr.txtLow.Text = fecherFoundation.Strings.Trim(vs1.TextMatrix(vs1.Row, LowCol));
            frmInventoryAdjust.InstancePtr.txtHigh.Text = fecherFoundation.Strings.Trim(vs1.TextMatrix(vs1.Row, HighCol));
            MotorVehicle.Statics.ListSubType = Strings.Mid((lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].Text : ""), 1, 2);
			strHold = fecherFoundation.Strings.Trim(vs1.TextMatrix(vs1.Row, GroupCol));
			if (strHold == "ALL")
			{
				frmInventoryAdjust.InstancePtr.txtGroup.Text = "0";
			}
			else
			{
				frmInventoryAdjust.InstancePtr.txtGroup.Text = strHold;
			}
			// 
			frmInventoryAdjust.InstancePtr.cmbAdd.Enabled = false;
			frmInventoryAdjust.InstancePtr.txtComment.Enabled = false;
			frmInventoryAdjust.InstancePtr.Label5.Enabled = false;
			frmInventoryAdjust.InstancePtr.cmbAdd.Enabled = false;
			frmInventoryAdjust.InstancePtr.txtInventoryAdjustDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			frmInventoryAdjust.InstancePtr.Show(App.MainForm);
            //FC:FINAL:AM:#2229 - move code after Show
            if (MotorVehicle.Statics.OpID != "")
            {
                frmInventoryAdjust.InstancePtr.txtInitials.Text = MotorVehicle.Statics.OpID;
            }
            if (frmInventoryAdjust.InstancePtr.txtInitials.Text != "")
			{
				frmInventoryAdjust.InstancePtr.txtGroup.Focus();
				frmInventoryAdjust.InstancePtr.txtGroup.SelectionStart = 0;
				frmInventoryAdjust.InstancePtr.txtGroup.SelectionLength = 1;
			}
			else
			{
				frmInventoryAdjust.InstancePtr.txtInitials.Focus();
			}
		}

		private void cmdPrintAll_Click(object sender, System.EventArgs e)
		{
			int intCounter;

			for (intCounter = 0; intCounter <= 9; intCounter++)
			{
				intIndex = intCounter;
				switch (intIndex)
                {
                    case 0:
                        strHeading = "MVR3 Forms";

                        break;
                    case 1:
                        strHeading = "Plate";

                        break;
                    case 2:
                        strHeading = "Double Year Stickers";

                        break;
                    case 3:
                        strHeading = "Double Month Stickers";

                        break;
                    case 4:
                        strHeading = "Single Year Stickers";

                        break;
                    case 5:
                        strHeading = "Single Month Stickers";

                        break;
                    case 6:
                        strHeading = "Decals";

                        break;
                    case 7:
                        strHeading = "Boosters";

                        break;
                    case 8:
                        strHeading = "MVR10 Forms";

                        break;
                    case 9:
                        strHeading = "Combination Stickers";

                        break;
                    default:
                        return;
                }
				rptInventory.InstancePtr.Init(ref InventoryPrinter, true);
				rptInventory.InstancePtr.Unload();
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptInventory"))
				{
					// do nothing
				}
				else
				{
					goto CheckIt;
				}
			}
		}

		private void cmdPrintSelected_Click(object sender, System.EventArgs e)
		{
			GrapeCity.ActiveReports.Export.Word.Section.RtfExport ArTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
			string temp = "";
			string strLine = "";
			string strFilePath = "";
			string strDirectory = "";
			
			if ((dlg1.Flags & FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile)) == FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile))// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E") & vbPorterConverter.cdlPDPrintToFile) {
			{
				strDirectory = Environment.CurrentDirectory;
				fecherFoundation.Information.Err().Clear();
				dlg1.Filter = "*.doc";
				dlg1.ShowSave();
				if (fecherFoundation.Information.Err().Number == 0)
				{
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Building Report");
					temp = dlg1.FileName;
					if (Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare) == 0)
					{
						temp += ".doc";
					}
					else
					{
						temp = Strings.Left(temp, Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare)) + "doc";
					}
				}
				else
				{
					Environment.CurrentDirectory = strDirectory;
					return;
				}
				switch (cmbInventoryType.Text)
                {
                    case "MVR3s":
                        strHeading = "MVR3 Forms";
                        intIndex = 0;

                        break;
                    case "Plates":
                        strHeading = "Plate";
                        intIndex = 1;

                        break;
                    case "Double Year Stickers":
                        strHeading = "Double Year Stickers";
                        intIndex = 2;

                        break;
                    case "Double Month Stickers":
                        strHeading = "Double Month Stickers";
                        intIndex = 3;

                        break;
                    case "Single Year Stickers":
                        strHeading = "Single Year Stickers";
                        intIndex = 4;

                        break;
                    case "Single Month Stickers":
                        strHeading = "Single Month Stickers";
                        intIndex = 5;

                        break;
                    case "Decals":
                        strHeading = "Decals";
                        intIndex = 6;

                        break;
                    case "Boosters":
                        strHeading = "Boosters";
                        intIndex = 7;

                        break;
                    default:
                    {
                        switch (cmbInventoryType.Text)
                        {
                            case "Boosters":
                                strHeading = "MVR10 Forms";
                                intIndex = 8;

                                break;
                            case "Combination Stickers":
                                strHeading = "Combination Stickers";
                                intIndex = 9;

                                break;
                            default:
                                frmWait.InstancePtr.Unload();
                                return;
                        }

                        break;
                    }
                }
				rptInventory.InstancePtr.Run(false);
				rptInventory.InstancePtr.Unload();
				frmWait.InstancePtr.Unload();
				CheckIt2:

                if (!MotorVehicle.JobComplete("rptInventory"))
                {
                    goto CheckIt2;
                }

                Environment.CurrentDirectory = strDirectory;
			}
			else
			{
				switch (cmbInventoryType.Text)
                {
                    case "MVR3s":
                        strHeading = "MVR3 Forms";
                        intIndex = 0;

                        break;
                    case "Plates":
                        strHeading = "Plate";
                        intIndex = 1;

                        break;
                    case "Double Year Stickers":
                        strHeading = "Double Year Stickers";
                        intIndex = 2;

                        break;
                    case "Double Month Stickers":
                        strHeading = "Double Month Stickers";
                        intIndex = 3;

                        break;
                    case "Single Year Stickers":
                        strHeading = "Single Year Stickers";
                        intIndex = 4;

                        break;
                    case "Single Month Stickers":
                        strHeading = "Single Month Stickers";
                        intIndex = 5;

                        break;
                    case "Decals":
                        strHeading = "Decals";
                        intIndex = 6;

                        break;
                    case "Boosters":
                        strHeading = "Boosters";
                        intIndex = 7;

                        break;
                    default:
                    {
                        switch (cmbInventoryType.Text)
                        {
                            case "Boosters":
                                strHeading = "MVR10 Forms";
                                intIndex = 8;

                                break;
                            case "Combination Stickers":
                                strHeading = "Combination Stickers";
                                intIndex = 9;

                                break;
                            default:
                                return;
                        }

                        break;
                    }
                }
				rptInventory.InstancePtr.Init(ref InventoryPrinter, false);
				rptInventory.InstancePtr.Unload();
			}
		}

		private void frmInventory_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.INVENTORYMAINTENANCE))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			TypeCol = 0;
			DateCol = 1;
			OpIDCol = 2;
			LowCol = 3;
			HighCol = 4;
			StatusCol = 5;
			GroupCol = 6;
			CountCol = 7;
			vs1.ColWidth(TypeCol, 700);
			vs1.ColWidth(DateCol, 900);
			vs1.ColWidth(OpIDCol, 700);
			vs1.ColWidth(LowCol, 1200);
			vs1.ColWidth(HighCol, 1200);
			vs1.ColWidth(StatusCol, 800);
			vs1.ColWidth(GroupCol, 800);
			vs1.ColWidth(CountCol, 1000);
			dblColPerecnts[0] = 0.086892;
			dblColPerecnts[1] = 0.110926;
			dblColPerecnts[2] = 0.086892;
			dblColPerecnts[3] = 0.1479014;
			dblColPerecnts[4] = 0.1479014;
			dblColPerecnts[5] = 0.0979846;
			dblColPerecnts[6] = 0.0979846;
			dblColPerecnts[7] = 0.1238674;
			vs1.TextMatrix(0, TypeCol, "Type");
			vs1.TextMatrix(0, DateCol, "Date");
			vs1.TextMatrix(0, OpIDCol, "OpID");
			vs1.TextMatrix(0, LowCol, "Low");
			vs1.TextMatrix(0, HighCol, "High");
			vs1.TextMatrix(0, StatusCol, "Status");
			vs1.TextMatrix(0, GroupCol, "Group");
			vs1.TextMatrix(0, CountCol, "Count");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(OpIDCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(StatusCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(LowCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(HighCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(GroupCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			if (MotorVehicle.Statics.ListRefresh == true)
			{
				if (lstPlates.SelectedIndex != -1)
				{
					lstPlates_DblClick();
				}
				else if (lstItems.SelectedIndex != -1)
				{
					lstItems_DblClick();
				}
				else
				{
					if (cmbInventoryType.Text == "MVR3s")
					{
						optInventoryType_Click(0);
					}
					else if (cmbInventoryType.Text == "Plates")
					{
						optInventoryType_Click(1);
					}
					else if (cmbInventoryType.Text == "Double Year Stickers")
					{
						optInventoryType_Click(2);
					}
					else if (cmbInventoryType.Text == "Double Month Stickers")
					{
						optInventoryType_Click(3);
					}
					else if (cmbInventoryType.Text == "Single Year Stickers")
					{
						optInventoryType_Click(4);
					}
					else if (cmbInventoryType.Text == "Single Month Stickers")
					{
						optInventoryType_Click(5);
					}
					else if (cmbInventoryType.Text == "Decals")
					{
						optInventoryType_Click(6);
					}
					else if (cmbInventoryType.Text == "Boosters")
					{
						optInventoryType_Click(7);
					}
					else if (cmbInventoryType.Text == "Boosters")
					{
						optInventoryType_Click(8);
					}
					else if (cmbInventoryType.Text == "Combination Stickers")
					{
						optInventoryType_Click(9);
					}
				}
				MotorVehicle.Statics.ListRefresh = false;
			}

            cmbInventoryType.SelectedIndex = 0;
        }

		private void frmInventory_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmInventory.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmInventory_Load(object sender, System.EventArgs e)
		{
			if (MotorVehicle.Statics.F9 == true)
				cmdAdd.Enabled = false;
            cmbStart.Visible = false;
			lstItems.Visible = false;
			intSelectedGroup = 0;
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this, false);

            lstPlates.Columns[0].Width = FCConvert.ToInt32(lstPlates.Width * 0.20);
            lstPlates.Columns.Add("", FCConvert.ToInt32(lstPlates.Width * 0.75), HorizontalAlignment.Left);
            lstPlates.GridLineStyle = GridLineStyle.None;
			//FC:FINAL:DDU:#2228 - prevent user from resizing columns
			foreach (ColumnHeader item in lstPlates.Columns)
			{
				item.Resizable = false;
				item.Movable = false;
			}
		}

		private void frmInventory_Resize(object sender, System.EventArgs e)
		{
			int counter;
			//App.DoEvents();
			intRowHeight = vs1.RowHeight(0);
			for (counter = 0; counter <= 7; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * dblColPerecnts[counter]));
			}

			if (lstItems.Visible)
			{
				if (cmbInventoryType.Text == "Double Month Stickers" || cmbInventoryType.Text == "Single Month Stickers")
				{
					//lstItems.HeightOriginal = 2500;
				}
			}
		}

		public void Form_Resize()
		{
			frmInventory_Resize(this, new System.EventArgs());
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			strSQL = "";
			strSQLItems = "";
			//App.DoEvents();
			// rsPlates.Close
			// rsItems.Close
			rsPlates.Reset();
			rsItems.Reset();
			ff = null;
			NumberOfRecords = 0;
			FCUtils.EraseSafe(Stickers);
			MaxElements = 0;
			fnx = 0;
			if (MotorVehicle.Statics.F9 == true)
			{
				// if the inventory screen was called from data input
				MotorVehicle.Statics.F9 = false;
			}
		}

		public void lstItems_DoubleClick(object sender, System.EventArgs e)
		{
			string Month = "";
			string xx = "";
			if (MotorVehicle.Statics.ListType == "P")
			{
				// moved to lstPlates.click
			}
			else if (MotorVehicle.Statics.ListType == "SM")
			{
				Month = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMS' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMS' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//MessageBox.Show("There are no single " + lstItems.Text + " stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					MessageBox.Show("There are no single " + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "") + " stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "DM")
			{
				Month = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMD' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMD' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//MessageBox.Show("There are no double " + lstItems.Text + " stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					MessageBox.Show("There are no double " + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "") + " stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "DY")
			{
				//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
				//xx = "SYD" + lstItems.Text;
				xx = "SYD" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no double year stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "COMBINATION")
			{
				//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
				//xx = "SYC" + lstItems.Text;
				xx = "SYC" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no combination stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "SY")
			{
				string xy = "";
				//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
				//xy = "SYS" + lstItems.Text;
				xy = "SYS" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xy + "' AND Status = 'A'  ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xy + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no single year stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "DECAL")
			{
				string YY = "";
				//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
				//YY = "DYS" + lstItems.Text;
				YY = "DYS" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code  ='" + YY + "' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code  ='" + YY + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no decals in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "BOOSTER")
			{
			}
			else if (MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
			{
			}
		}

		public void lstItems_DblClick()
		{
			lstItems_DoubleClick(lstItems, new System.EventArgs());
		}

		private void lstItems_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			string Month = "";
			string xx = "";
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (MotorVehicle.Statics.ListType == "P")
				{
					// moved to lstPlates.click
				}
				else if (MotorVehicle.Statics.ListType == "SM")
				{
					Month = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
					if (intSelectedGroup == 0)
					{
						strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMS' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' ORDER by Number";
					}
					else
					{
						strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMS' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by Number";
					}
					rsItems.OpenRecordset(strSQLItems);
					if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
					{
						rsItems.MoveLast();
						rsItems.MoveFirst();
						FillRoutine(ref rsItems);
					}
					else
					{
						vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
						vs1.Rows = 1;
						//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
						//MessageBox.Show("There are no single " + lstItems.Text + "  stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MessageBox.Show("There are no single " + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "") + "  stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					rsItems.Reset();
				}
				else if (MotorVehicle.Statics.ListType == "DM")
				{
					Month = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
					if (intSelectedGroup == 0)
					{
						strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMD' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' ORDER by Number";
					}
					else
					{
						strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMD' AND substring(Code,4,2) = '" + Month + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by Number";
					}
					rsItems.OpenRecordset(strSQLItems);
					if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
					{
						rsItems.MoveLast();
						rsItems.MoveFirst();
						FillRoutine(ref rsItems);
					}
					else
					{
						vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
						vs1.Rows = 1;
						//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
						//MessageBox.Show("There are no double  " + lstItems.Text + "  stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MessageBox.Show("There are no double  " + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "") + "  stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					rsItems.Reset();
				}
				else if (MotorVehicle.Statics.ListType == "DY")
				{
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//xx = "SYD" + lstItems.Text;
					xx = "SYD" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
					if (intSelectedGroup == 0)
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' ORDER by Number";
					}
					else
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by Number";
					}
					rsItems.OpenRecordset(strSQLItems);
					if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
					{
						rsItems.MoveLast();
						rsItems.MoveFirst();
						FillRoutine(ref rsItems);
					}
					else
					{
						vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
						vs1.Rows = 1;
						MessageBox.Show("There are no double year stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					rsItems.Reset();
				}
				else if (MotorVehicle.Statics.ListType == "COMBINATION")
				{
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//xx = "SYC" + lstItems.Text;
					xx = "SYC" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
					if (intSelectedGroup == 0)
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' ORDER by Number";
					}
					else
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by Number";
					}
					rsItems.OpenRecordset(strSQLItems);
					if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
					{
						rsItems.MoveLast();
						rsItems.MoveFirst();
						FillRoutine(ref rsItems);
					}
					else
					{
						vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
						vs1.Rows = 1;
						MessageBox.Show("There are no combination stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					rsItems.Reset();
				}
				else if (MotorVehicle.Statics.ListType == "SY")
				{
					string xy = "";
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//xy = "SYS" + lstItems.Text;
					xy = "SYS" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
					if (intSelectedGroup == 0)
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xy + "' AND Status = 'A'  ORDER by Number";
					}
					else
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xy + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by Number";
					}
					rsItems.OpenRecordset(strSQLItems);
					if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
					{
						rsItems.MoveLast();
						rsItems.MoveFirst();
						FillRoutine(ref rsItems);
					}
					else
					{
						vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
						vs1.Rows = 1;
						MessageBox.Show("There are no single year stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					rsItems.Reset();
				}
				else if (MotorVehicle.Statics.ListType == "DECAL")
				{
					string YY = "";
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//YY = "DYS" + lstItems.Text;
					YY = "DYS" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
					if (intSelectedGroup == 0)
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code  ='" + YY + "' AND Status = 'A' ORDER by Number";
					}
					else
					{
						strSQLItems = "SELECT * FROM Inventory WHERE Code  ='" + YY + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by Number";
					}
					rsItems.OpenRecordset(strSQLItems);
					if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
					{
						rsItems.MoveLast();
						rsItems.MoveFirst();
						FillRoutine(ref rsItems);
					}
					else
					{
						vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
						vs1.Rows = 1;
						MessageBox.Show("There are no decals in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					rsItems.Reset();
				}
				else if (MotorVehicle.Statics.ListType == "BOOSTER")
				{
				}
				else if (MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
				{
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void lstPlates_DoubleClick(object sender, System.EventArgs e)
		{
			vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			vs1.Rows = 1;
            var classString = "";
            if (lstPlates.ListIndex >= 0)
            {
                classString = lstPlates.Items[lstPlates.ListIndex].Text;
            }
			if (intSelectedGroup == 0)
			{
				// strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" & Mid$(lstPlates.Text, 1, 2) & "' AND Status = 'A' ORDER by InventoryDate, " & strSortBy & "suffix, Prefix + convert(nvarchar, Number) + Suffix"
				//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
				//strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(lstPlates.Text, 1, 2) + "' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "suffix, Prefix , Number";
				strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(classString, 1, 2) + "' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "suffix, Prefix , Number";
			}
			else
			{
				// strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" & Mid$(lstPlates.Text, 1, 2) & "' AND Status = 'A' AND [Group] = " & intSelectedGroup & " ORDER by InventoryDate, " & strSortBy & "suffix, Prefix + convert(nvarchar, Number) + Suffix"
				//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
				//strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(lstPlates.Text, 1, 2) + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "suffix, Prefix, Number";
				strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(classString, 1, 2) + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "suffix, Prefix, Number";
			}
			rsPlates.OpenRecordset(strSQL);
			if (rsPlates.EndOfFile() != true && rsPlates.BeginningOfFile() != true)
			{
				rsPlates.MoveLast();
				rsPlates.MoveFirst();
				FillRoutine(ref rsPlates);
			}
			else
			{
				//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView) and use SubItem value (WiseJ doesn't display tabs and spaces, so for this is used parsing input value into columns)
				//MessageBox.Show("There are no " + Strings.Left(lstPlates.Text, 2) + " (" + fecherFoundation.Strings.Trim(Strings.Mid(lstPlates.Text, 4, lstPlates.Text.Length - 3)) + ") plates in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				MessageBox.Show("There are no " + Strings.Left((lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].Text : ""), 2) + " (" + fecherFoundation.Strings.Trim((lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].SubItems[1].Text : "")) + ") plates in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			//App.DoEvents();
			// rsPlates.Close
			rsPlates.Reset();
		}

		public void lstPlates_DblClick()
		{
			lstPlates_DoubleClick(lstPlates, new System.EventArgs());
		}

		private void lstPlates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
				vs1.Rows = 1;
				if (intSelectedGroup == 0)
				{
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(lstPlates.Text, 1, 2) + "' AND Status = 'A' ORDER by prefix, number, suffix";
					strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid((lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].Text : ""), 1, 2) + "' AND Status = 'A' ORDER by prefix, number, suffix";
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid(lstPlates.Text, 1, 2) + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by prefix, number, suffix";
					strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid((lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].Text : ""), 1, 2) + "' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by prefix, number, suffix";
				}
				rsPlates.OpenRecordset(strSQL);
				if (rsPlates.EndOfFile() != true && rsPlates.BeginningOfFile() != true)
				{
					rsPlates.MoveLast();
					rsPlates.MoveFirst();
					FillRoutine(ref rsPlates);
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
					//MessageBox.Show("There are no  " + lstPlates.Text + "  plates in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					MessageBox.Show("There are no  " + (lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].Text : "") + "  plates in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				//App.DoEvents();
				// rsPlates.Close
				rsPlates.Reset();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		
		public void optInventoryType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{//
			// this selection decides what type of object to get from inventory. Some have multiple type and will fill
			// another list box to select from. Others will fill the Display box at the bottom.
			// FillRoutine() fills the display box
			// GetListItems() fills the list box(es) on the right
			vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			// bottom of screen
			vs1.Rows = 1;
			lstItems.Clear();
			// top right - used to select Months - not Sorted
			lstPlates.Clear();
            lstPlates.Columns[0].Width = FCConvert.ToInt32(lstPlates.Width * 0.20);
            lstPlates.Columns.Add("", FCConvert.ToInt32(lstPlates.Width * 0.75), HorizontalAlignment.Left);
			//FC:FINAL:DDU:#2228 - prevent user from resizing columns
			foreach (ColumnHeader item in lstPlates.Columns)
			{
				item.Resizable = false;
				item.Movable = false;
			}

			// used to select Plate types - Sorted
			switch (Index)
			{
				case 0:
					{
						MotorVehicle.Statics.ListType = "MVR";
						if (intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						rsItems.OpenRecordset(strSQLItems);
						if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
						{
							rsItems.MoveLast();
							rsItems.MoveFirst();
							FillRoutine(ref rsItems);
						}
						else
						{
							MessageBox.Show("There are no MVR3 forms in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						rsItems.Reset();
						lblItemType.Text = "";
						break;
					}
				case 1:
					{
						MotorVehicle.Statics.ListType = "P";
						GetListItems(ref MotorVehicle.Statics.ListType);
						lblItemType.Text = "Plate Type";
						break;
					}
				case 2:
					{
						MotorVehicle.Statics.ListType = "DY";
						GetListItems(ref MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Year";
						break;
					}
				case 3:
					{
						MotorVehicle.Statics.ListType = "DM";
						GetListItems(ref MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Month";
						break;
					}
				case 4:
					{
						MotorVehicle.Statics.ListType = "SY";
						GetListItems(ref MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Year";
						break;
					}
				case 5:
					{
						MotorVehicle.Statics.ListType = "SM";
						GetListItems(ref MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Month";
						rsItems.Reset();
						break;
					}
				case 6:
					{
						MotorVehicle.Statics.ListType = "DECAL";
						GetListItems(ref MotorVehicle.Statics.ListType);
						lblItemType.Text = "Decals Year";
						break;
					}
				case 7:
					{
						MotorVehicle.Statics.ListType = "BOOSTER";
						if (intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'BX' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'BX' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						rsItems.OpenRecordset(strSQLItems);
						if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
						{
							rsItems.MoveLast();
							rsItems.MoveFirst();
							FillRoutine(ref rsItems);
						}
						else
						{
							MessageBox.Show("There are no booster plates in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						rsItems.Reset();
						lblItemType.Text = "";
						break;
					}
				case 8:
					{
						MotorVehicle.Statics.ListType = "SPECIALREGPERMIT";
						if (intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'RX' AND Status = 'A' ORDER by InventoryDate, " + strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'RX' AND Status = 'A' AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						rsItems.OpenRecordset(strSQLItems);
						if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
						{
							rsItems.MoveLast();
							rsItems.MoveFirst();
							FillRoutine(ref rsItems);
						}
						else
						{
							MessageBox.Show("There are no MVR10 forms in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						rsItems.Reset();
						lblItemType.Text = "";
						break;
					}
				case 9:
					{
						MotorVehicle.Statics.ListType = "COMBINATION";
						GetListItems(ref MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Year";
						break;
					}
			}
			//end switch
		}

		public void optInventoryType_Click(int Index)
		{
			optInventoryType_CheckedChanged(Index, cmbInventoryType, new System.EventArgs());
		}

		private void optInventoryType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbInventoryType.SelectedIndex;
			optInventoryType_CheckedChanged(index, sender, e);
		}

		public void FillRoutine(ref clsDRWrapper rs)
		{
			bool first = false;
			int lngStart;
			int lngStop;
			int lngRow;
			int lngFirst = 0;
			int lngLast = 0;
			int intGroup = 0;
			string strprefix = "";
			string strSuffix = "";
			int Difference = 0;
			int counter = 0;
			string strOriginalPrefix = "";
			string strStartPlate = "";
			DateTime datInventoryDate;
			vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			vs1.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				lngFirst = rs.Get_Fields_Int32("Number");
				lngLast = rs.Get_Fields_Int32("Number");
				intGroup = rs.Get_Fields_Int16("Group");
				datInventoryDate = rs.Get_Fields_DateTime("InventoryDate");
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("prefix")) == "")
					strprefix = "";
				else
					strprefix = rs.Get_Fields_String("prefix");
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Suffix")) == "")
					strSuffix = "";
				else
					strSuffix = rs.Get_Fields_String("Suffix");
				strOriginalPrefix = strprefix;
				strStartPlate = rs.Get_Fields_String("FormattedInventory");
				counter = 1;
				while (!rs.EndOfFile())
				{
					if (strprefix.Length > rs.Get_Fields_String("prefix").Length && Strings.Right(strprefix, 1) == "0")
					{
						strprefix = Strings.Left(strprefix, (strprefix.Length - (strprefix.Length - rs.Get_Fields_String("prefix").Length)));
					}
					if (rs.Get_Fields_Int32("Number") == lngLast && (strprefix + fecherFoundation.Strings.Trim(lngLast.ToString()) + strSuffix == rs.Get_Fields_String("prefix") + rs.Get_Fields_Int32("Number") + rs.Get_Fields_String("Suffix")) && rs.Get_Fields_Int16("Group") == intGroup && rs.Get_Fields_DateTime("InventoryDate").ToOADate() == datInventoryDate.ToOADate())
					{
						if (lngLast == lngFirst)
						{
							first = true;
						}
						else
						{
							first = false;
						}
						lngLast += 1;
					}
					else if (rs.Get_Fields_Int32("Number") == lngLast && fecherFoundation.Strings.Trim(rs.Get_Fields_String("prefix")) == "" && fecherFoundation.Strings.Trim(rs.Get_Fields_String("Suffix")) == "" && rs.Get_Fields_Int16("Group") == intGroup && rs.Get_Fields_DateTime("InventoryDate").ToOADate() == datInventoryDate.ToOADate())
					{
						if (lngLast == lngFirst)
						{
							first = true;
						}
						else
						{
							first = false;
						}
						lngLast += 1;
					}
					else
					{
						rs.MovePrevious();
						Difference = (rs.Get_Fields_Int32("Number") - lngFirst) + 1;
						vs1.Rows += 1;
						vs1.TextMatrix(counter, TypeCol, Strings.Mid(rs.Get_Fields("Code"), 4, 2));
						vs1.TextMatrix(counter, DateCol, Strings.Format(rs.Get_Fields_DateTime("InventoryDate"), "MM/dd/yy"));
						vs1.TextMatrix(counter, OpIDCol, rs.Get_Fields_String("OpID"));
						vs1.TextMatrix(counter, LowCol, strStartPlate);
						// strOriginalPrefix & lngFirst & .Fields("Suffix")
						vs1.TextMatrix(counter, HighCol, rs.Get_Fields_String("FormattedInventory"));
						// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
						vs1.TextMatrix(counter, StatusCol, rs.Get_Fields_String("Status"));
						vs1.TextMatrix(counter, CountCol, FCConvert.ToString(Difference));
						if (rs.Get_Fields_Int16("Group") == 0)
						{
							vs1.TextMatrix(counter, GroupCol, "ALL");
						}
						else
						{
							vs1.TextMatrix(counter, GroupCol, rs.Get_Fields_Int16("Group"));
						}
						counter += 1;
						rs.MoveNext();
						lngFirst = rs.Get_Fields_Int32("Number");
						lngLast = rs.Get_Fields_Int32("Number");
						intGroup = rs.Get_Fields_Int16("Group");
						datInventoryDate = rs.Get_Fields_DateTime("InventoryDate");
						if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("prefix")) == "")
							strprefix = "";
						else
							strprefix = rs.Get_Fields_String("prefix");
						if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Suffix")) == "")
							strSuffix = "";
						else
							strSuffix = rs.Get_Fields_String("Suffix");
						strOriginalPrefix = strprefix;
						strStartPlate = rs.Get_Fields_String("FormattedInventory");
						rs.MovePrevious();
					}
					rs.MoveNext();
				}
				rs.MovePrevious();
				Difference = 0;
				if (rs.Get_Fields_Int32("Number") != lngFirst || rs.RecordCount() == 1 || first)
				{
					Difference = (rs.Get_Fields_Int32("Number") - lngFirst) + 1;
					vs1.Rows += 1;
					vs1.TextMatrix(counter, TypeCol, Strings.Mid(rs.Get_Fields("Code"), 4, 2));
					vs1.TextMatrix(counter, DateCol, Strings.Format(rs.Get_Fields_DateTime("InventoryDate"), "MM/dd/yy"));
					vs1.TextMatrix(counter, OpIDCol, rs.Get_Fields_String("OpID"));
					vs1.TextMatrix(counter, LowCol, strStartPlate);
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					vs1.TextMatrix(counter, HighCol, rs.Get_Fields_String("FormattedInventory"));
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					vs1.TextMatrix(counter, StatusCol, rs.Get_Fields_String("Status"));
					vs1.TextMatrix(counter, CountCol, FCConvert.ToString(Difference));
					if (rs.Get_Fields_Int16("Group") == 0)
					{
						vs1.TextMatrix(counter, GroupCol, "ALL");
					}
					else
					{
						vs1.TextMatrix(counter, GroupCol, rs.Get_Fields_Int16("Group"));
					}
					counter += 1;
				}
			}
			vs1.Rows = counter;
			Form_Resize();
		}

		public void GetListItems(ref string ListType)
		{
			// vbPorter upgrade warning: tempcounter As int	OnWriteFCConvert.ToInt32(
			int tempcounter;
			bool Already = false;
			// vbPorter upgrade warning: xx As int	OnWrite(string, int)
			int xx = 0;
			if (ListType == "P")
			{
				lstPlates.Visible = true;
				lstItems.Visible = false;
				//FC:FINAL:MSH - i.issue #1782: initialize list for displaying all data (WiseJ doesn't display tabs and spaces)
				int listWidth = lstPlates.Width;
				lstPlates.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.20);
				lstPlates.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.75);
				lstPlates.GridLineStyle = GridLineStyle.None;
				strSQLItems = "SELECT * FROM InventoryMaster WHERE Type = 'P' ORDER BY MonthYearClass";
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{                        
						lstPlates.AddItem(rsItems.Get_Fields_String("MonthYearClass") + "\t" + rsItems.Get_Fields_String("Description"));
                       
						rsItems.MoveNext();
					}
				}
				rsItems.Reset();
			}
			else if (ListType == "MVR")
			{
			}
			else if (ListType == "DY")
			{
				for (fnx = 0; fnx <= 6; fnx++)
				{
					Stickers[fnx] = 0;
				}
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,3) = 'SYD' AND Status = 'A'";
				xx = FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2));
				fnx = 0;
				while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 4)))
				{
					Stickers[fnx] = xx + 100;
					xx += 1;
					fnx += 1;
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						for (tempcounter = 0; tempcounter <= fnx; tempcounter++)
						{
							if (Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100 == Stickers[tempcounter])
							{
								Already = true;
								break;
							}
						}
						if (!Already)
						{
							Stickers[fnx] = (FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2))) + 100));
							fnx += 1;
						}
						else
						{
							Already = false;
						}
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
					// xx = Val(lstItems.List(lstItems.ListCount - 1))
					// Do Until xx = (Right(Year(Now), 2) + 3)
					// xx = xx + 1
					// lstItems.AddItem PadToString(xx, 2)
					// Loop
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
				}
				rsItems.Reset();
			}
			else if (ListType == "COMBINATION")
			{
				for (fnx = 0; fnx <= 6; fnx++)
				{
					Stickers[fnx] = 0;
				}
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,3) = 'SYC' AND Status = 'A'";
				xx = FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2));
				fnx = 0;
				while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 4)))
				{
					Stickers[fnx] = xx + 100;
					xx += 1;
					fnx += 1;
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						for (tempcounter = 0; tempcounter <= fnx; tempcounter++)
						{
							if (Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100 == Stickers[tempcounter])
							{
								Already = true;
								break;
							}
						}
						if (!Already)
						{
							Stickers[fnx] = (FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100));
							fnx += 1;
						}
						else
						{
							Already = false;
						}
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
					// xx = Val(lstItems.List(lstItems.ListCount - 1))
					// Do Until xx = (Right(Year(Now), 2) + 3)
					// xx = xx + 1
					// lstItems.AddItem PadToString(xx, 2)
					// Loop
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
				}
				rsItems.Reset();
			}
			else if (ListType == "SY")
			{
				FCUtils.EraseSafe(Stickers);
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,3)  = 'SYS' AND Status = 'A'";
				xx = FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2));
				fnx = 0;
				while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 4)))
				{
					Stickers[fnx] = xx + 100;
					xx += 1;
					fnx += 1;
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						for (tempcounter = 0; tempcounter <= fnx; tempcounter++)
						{
							if (Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100 == Stickers[tempcounter])
							{
								Already = true;
								break;
							}
						}
						if (!Already)
						{
							Stickers[fnx] = (FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100));
							fnx += 1;
						}
						else
						{
							Already = false;
						}
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
					// xx = Val(lstItems.List(lstItems.ListCount - 1))
					// Do Until xx = (Right(Year(Now), 2) + 3)
					// xx = xx + 1
					// lstItems.AddItem PadToString(xx, 2)
					// Loop
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
				}
				//lstItems.HeightOriginal = 1271;
				rsItems.Reset();
			}
			else if ((ListType == "SM") || (ListType == "DM"))
			{
				lstPlates.Visible = false;
				lstItems.Visible = true;
				lstItems.AddItem("January");
				lstItems.AddItem("February");
				lstItems.AddItem("March");
				lstItems.AddItem("April");
				lstItems.AddItem("May");
				lstItems.AddItem("June");
				lstItems.AddItem("July");
				lstItems.AddItem("August");
				lstItems.AddItem("September");
				lstItems.AddItem("October");
				lstItems.AddItem("November");
				lstItems.AddItem("December");
				//lstItems.HeightOriginal = 2500;
			}
			else if (ListType == "DECAL")
			{
				FCUtils.EraseSafe(Stickers);
				// 
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,2)  = 'DY'";
				rsItems.OpenRecordset(strSQLItems);
				fnx = 0;
				// 
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						Stickers[fnx] = (FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100));
						// 
						fnx += 1;
						// 
						// lstItems.AddItem (Mid$(.fields("Code"), 4, 2))
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					xx = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2))));
					while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 4)))
					{
						for (fnx = 0; fnx <= MaxElements - 1; fnx++)
						{
							if (Stickers[fnx] == xx + 100)
							{
								goto CheckNext;
							}
						}
						Stickers[MaxElements] = xx + 100;
						MaxElements += 1;
						CheckNext:
						;
						xx += 1;
					}
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
					// MsgBox "There are no Decals in Inventory", vbOKOnly, "No Inventory"
				}
				//lstItems.HeightOriginal = 1271;
				rsItems.Reset();
			}
			else if (ListType == "BOOSTER")
			{
			}
			else if (ListType == "SPECIALREGPERMIT")
			{
			}
		}

		private int[] Jims_Sort_Array(ref int[] Stickers)
		{
			int[] Jims_Sort_Array = null;
			int I;
			int j;
			// vbPorter upgrade warning: Min As int	OnWriteFCConvert.ToInt32(
			int Min;
			int MinIndex;
			int temp = 0;
			// 
			Min = Stickers[0];
			MinIndex = 0;
			// 
			for (I = 1; I <= MaxElements - 1; I++)
			{
				Min = Stickers[I - 1];
				MinIndex = I - 1;
				for (j = I; j <= MaxElements - 1; j++)
				{
					if (Stickers[j] < Stickers[MinIndex])
					{
						Min = Stickers[j];
						MinIndex = j;
					}
				}
				temp = Stickers[I - 1];
				Stickers[I - 1] = Stickers[MinIndex];
				Stickers[MinIndex] = temp;
			}
			Jims_Sort_Array = Stickers;
			return Jims_Sort_Array;
		}

		private void optSelection_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intSelectedGroup = Index;
			if (cmbInventoryType.Text == "MVR3s")
			{
				optInventoryType_Click(0);
			}
			else if (cmbInventoryType.Text == "Boosters")
			{
				optInventoryType_Click(7);
			}
			else if (cmbInventoryType.Text == "MVR10s")
			{
				optInventoryType_Click(8);
			}
			else if (cmbInventoryType.Text == "Plates")
			{
				lstPlates_DblClick();
			}
			else if (cmbInventoryType.Text == "Double Year Stickers" || cmbInventoryType.Text == "Double Month Stickers" || cmbInventoryType.Text == "Single Year Stickers" || cmbInventoryType.Text == "Single Month Stickers" || cmbInventoryType.Text == "Decals" || cmbInventoryType.Text == "Combination Stickers")
			{
				lstItems_DblClick();
			}
			else
			{
				// do nothing
			}
		}

		private void optSelection_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSelection.SelectedIndex;
			optSelection_CheckedChanged(index, sender, e);
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			int intCount;
			bool blnAddProtect;
			bool blnRemoveProtect;
			intCount = 0;
			blnAddProtect = false;
			blnRemoveProtect = false;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case MotorVehicle.ADDINVENTORY:
						{
							if (strPerm == "N")
							{
								if (intCount == 0)
								{
									intCount = 1;
									blnAddProtect = true;
								}
								else
								{
									if (blnRemoveProtect)
									{
										cmdAdd.Enabled = false;
									}
								}
							}
							else
							{
								intCount = 1;
								blnAddProtect = false;
							}
							break;
						}
					case MotorVehicle.REMOVEINVENTORY:
						{
							if (strPerm == "N")
							{
								if (intCount == 0)
								{
									intCount = 1;
									blnRemoveProtect = true;
								}
								else
								{
									if (blnAddProtect)
									{
										cmdAdd.Enabled = false;
									}
								}
							}
							else
							{
								intCount = 1;
								blnRemoveProtect = false;
							}
							break;
						}
					case MotorVehicle.CHANGEINVENTORYGROUP:
						{
							if (strPerm == "N")
							{
								cmdChange.Enabled = false;
							}
							else
							{
								cmdChange.Enabled = true;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As object	OnWrite(DialogResult)
			DialogResult ans;
			int intVBTab;
			string strHold = "";
			if (vs1.MouseRow <= 0)
			{
				if (vs1.MouseCol == OpIDCol)
				{
					strSortBy = "OPID, ";
				}
				else if (vs1.MouseCol == TypeCol)
				{
					strSortBy = "substring(Code, 4, 2), ";
				}
				else if (vs1.MouseCol == DateCol)
				{
					strSortBy = "";
				}
				else if (vs1.MouseCol == LowCol || vs1.MouseCol == HighCol || vs1.MouseCol == CountCol)
				{
					strSortBy = "";
				}
				else if (vs1.MouseCol == StatusCol)
				{
					strSortBy = "Status, ";
				}
				else if (vs1.MouseCol == GroupCol)
				{
					strSortBy = "GroupID, ";
				}
				if (cmbInventoryType.Text == "MVR3s")
				{
					optInventoryType_Click(0);
				}
				else if (cmbInventoryType.Text == "Plates")
				{
					lstPlates_DblClick();
				}
				else if (cmbInventoryType.Text == "Double Year Stickers" || cmbInventoryType.Text == "Double Month Stickers" || cmbInventoryType.Text == "Single Year Stickers" || cmbInventoryType.Text == "Single Month Stickers" || cmbInventoryType.Text == "Decals" || cmbInventoryType.Text == "Combination Stickers")
				{
					lstItems_DblClick();
				}
				else if (cmbInventoryType.Text == "Boosters")
				{
					optInventoryType_Click(7);
				}
				else if (cmbInventoryType.Text == "SpecialRegPermit")
				{
					optInventoryType_Click(8);
				}
				return;
			}

            var listType = MotorVehicle.Statics.ListType;

			ans = MessageBox.Show("Would you like to delete this entire range from inventory?", "DELETE  --  WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
			if (ans == DialogResult.Yes)
			{
				if (MotorVehicle.Statics.ListType == "MVR")
				{
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "P")
				{
					if (lstPlates.SelectedIndex != -1)
					{
						//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
						//MotorVehicle.Statics.ListSubType = Strings.Mid(lstPlates.Text, 1, 2);
						MotorVehicle.Statics.ListSubType = Strings.Mid(lstPlates.Items[lstPlates.SelectedIndex].Text, 1, 2);
					}
					else
					{
						MessageBox.Show("Please select a plate type from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "DY")
				{
					if (lstItems.SelectedIndex != -1)
					{
						//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
						//MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Text);
						MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Items[lstItems.SelectedIndex].Text);
					}
					else
					{
						MessageBox.Show("Please select a sticker year from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "COMBINATION")
				{
					if (lstItems.SelectedIndex != -1)
					{
						//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
						//MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Text);
						MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Items[lstItems.SelectedIndex].Text);
					}
					else
					{
						MessageBox.Show("Please select a sticker year from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "DM")
				{
					if (lstItems.SelectedIndex != -1)
					{
						MotorVehicle.Statics.ListSubType = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
					}
					else
					{
						MessageBox.Show("Please select a sticker month from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "SY")
				{
					if (lstItems.SelectedIndex != -1)
					{
						//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
						//MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Text);
						MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Items[lstItems.SelectedIndex].Text);
					}
					else
					{
						MessageBox.Show("Please select a sticker year from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "SM")
				{
					if (lstItems.SelectedIndex != -1)
					{
						MotorVehicle.Statics.ListSubType = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
					}
					else
					{
						MessageBox.Show("Please select a sticker month from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "DECAL")
				{
					if (lstItems.SelectedIndex != -1)
					{
						//FC:FINAL:MSH - i.issue #1782: replace empty property (.Text isn't relevant for ListView)
						//MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Text);
						MotorVehicle.Statics.ListSubType = fecherFoundation.Strings.Trim(lstItems.Items[lstItems.SelectedIndex].Text);
					}
					else
					{
						MessageBox.Show("Please select a decal year from the selection box.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "BOOSTER")
				{
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else if (MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
				{
					//frmInventoryAdjust.InstancePtr.Show(App.MainForm);
				}
				else
				{
					MessageBox.Show("Please select an item to add.", "Type Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
				// Load frmInventoryAdjust
				frmInventoryAdjust.InstancePtr.cmbAdd.Text = "Remove from Inventory";
				// Load Low in Range
				frmInventoryAdjust.InstancePtr.txtLow.Text = fecherFoundation.Strings.Trim(vs1.TextMatrix(vs1.Row, LowCol));
				// Load High in Range
				frmInventoryAdjust.InstancePtr.txtHigh.Text = fecherFoundation.Strings.Trim(vs1.TextMatrix(vs1.Row, HighCol));
				// 
				frmInventoryAdjust.InstancePtr.cmbAdjust.Text = "Show as an adjustment";
				frmInventoryAdjust.InstancePtr.txtInventoryAdjustDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
                frmInventoryAdjust.InstancePtr.Show(FormShowEnum.Modal);
				//frmInventoryAdjust.InstancePtr.txtInitials.Focus();
                switch (listType)
                {
					case "MVR":
						optInventoryType_Click(0);
                        break;
					case "P":
						lstPlates_DblClick();
                        break;
					case "DY":
					case "SY":
					case "COMBINATION":
					case "DM":
					case "SM":
					case "DECAL":
						lstItems_DblClick();
                        break;
					case "BOOSTER":
						optInventoryType_Click(7);
                        break;
					case "SPECIALREGPERMIT":
						optInventoryType_Click(8);
                        break;
                }
			}
		}

  
    }
}
