﻿using System;
using System.Web.Compilation;
using Autofac;
using Autofac.Core.Activators.Reflection;
using SharedApplication;
using SharedApplication.MotorVehicle;

namespace TWMV0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmEditRegistrantName>().As<IModalView<IEditRegistrantNameViewModel>>();
        }
    }
}