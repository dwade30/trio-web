﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptMVRT10.
	/// </summary>
	partial class rptMVRT10
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMVRT10));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetWeight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransactionDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCTANumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMileage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTransaction = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidationLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidationLine2Description = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidationLine2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld2000lbsNotice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransaction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine2Description)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNotice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldVIN,
				this.fldYear,
				this.fldMake,
				this.fldColor1,
				this.fldColor2,
				this.fldStyle,
				this.fldNetWeight,
				this.fldOwner1,
				this.fldTransactionDate,
				this.fldOldPlate,
				this.fldUnit,
				this.fldCTANumber,
				this.fldAddress,
				this.fldCity,
				this.fldState,
				this.fldZip,
				this.fldResidenceCity,
				this.fldResidenceState,
				this.fldResidenceCode,
				this.lblMileage,
				this.fldExpires,
				this.fldPlate,
				this.lblTransaction,
				this.fldDescription,
				this.fldReRegYes,
				this.fldReRegNo,
				this.fldOwner2,
				this.fldValidationLine1,
				this.fldValidationLine2Description,
				this.fldValidationLine2,
				this.fld2000lbsNotice
			});
			this.Detail.Height = 3.489583F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldVIN
			// 
			this.fldVIN.CanGrow = false;
			this.fldVIN.Height = 0.19F;
			this.fldVIN.Left = 1.75F;
			this.fldVIN.MultiLine = false;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldVIN.Text = null;
			this.fldVIN.Top = 1.0625F;
			this.fldVIN.Width = 1.8125F;
			// 
			// fldYear
			// 
			this.fldYear.CanGrow = false;
			this.fldYear.Height = 0.19F;
			this.fldYear.Left = 1.5F;
			this.fldYear.MultiLine = false;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldYear.Text = null;
			this.fldYear.Top = 0.71875F;
			this.fldYear.Width = 0.34375F;
			// 
			// fldMake
			// 
			this.fldMake.CanGrow = false;
			this.fldMake.Height = 0.19F;
			this.fldMake.Left = 0.5F;
			this.fldMake.MultiLine = false;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldMake.Text = null;
			this.fldMake.Top = 0.71875F;
			this.fldMake.Width = 0.6875F;
			// 
			// fldColor1
			// 
			this.fldColor1.CanGrow = false;
			this.fldColor1.Height = 0.19F;
			this.fldColor1.Left = 3.46875F;
			this.fldColor1.MultiLine = false;
			this.fldColor1.Name = "fldColor1";
			this.fldColor1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldColor1.Text = null;
			this.fldColor1.Top = 0.71875F;
			this.fldColor1.Width = 0.3125F;
			// 
			// fldColor2
			// 
			this.fldColor2.CanGrow = false;
			this.fldColor2.Height = 0.19F;
			this.fldColor2.Left = 3.78125F;
			this.fldColor2.MultiLine = false;
			this.fldColor2.Name = "fldColor2";
			this.fldColor2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldColor2.Text = null;
			this.fldColor2.Top = 0.71875F;
			this.fldColor2.Width = 0.3125F;
			// 
			// fldStyle
			// 
			this.fldStyle.CanGrow = false;
			this.fldStyle.Height = 0.19F;
			this.fldStyle.Left = 0.625F;
			this.fldStyle.MultiLine = false;
			this.fldStyle.Name = "fldStyle";
			this.fldStyle.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldStyle.Text = null;
			this.fldStyle.Top = 1.0625F;
			this.fldStyle.Width = 0.34375F;
			// 
			// fldNetWeight
			// 
			this.fldNetWeight.CanGrow = false;
			this.fldNetWeight.Height = 0.19F;
			this.fldNetWeight.Left = 5F;
			this.fldNetWeight.MultiLine = false;
			this.fldNetWeight.Name = "fldNetWeight";
			this.fldNetWeight.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldNetWeight.Text = null;
			this.fldNetWeight.Top = 1.0625F;
			this.fldNetWeight.Width = 0.53125F;
			// 
			// fldOwner1
			// 
			this.fldOwner1.CanGrow = false;
			this.fldOwner1.Height = 0.19F;
			this.fldOwner1.Left = 1.25F;
			this.fldOwner1.MultiLine = false;
			this.fldOwner1.Name = "fldOwner1";
			this.fldOwner1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldOwner1.Text = null;
			this.fldOwner1.Top = 1.3125F;
			this.fldOwner1.Width = 3.84375F;
			// 
			// fldTransactionDate
			// 
			this.fldTransactionDate.CanGrow = false;
			this.fldTransactionDate.Height = 0.19F;
			this.fldTransactionDate.Left = 0.375F;
			this.fldTransactionDate.MultiLine = false;
			this.fldTransactionDate.Name = "fldTransactionDate";
			this.fldTransactionDate.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldTransactionDate.Text = null;
			this.fldTransactionDate.Top = 2.75F;
			this.fldTransactionDate.Width = 0.9375F;
			// 
			// fldOldPlate
			// 
			this.fldOldPlate.CanGrow = false;
			this.fldOldPlate.Height = 0.19F;
			this.fldOldPlate.Left = 1.625F;
			this.fldOldPlate.MultiLine = false;
			this.fldOldPlate.Name = "fldOldPlate";
			this.fldOldPlate.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldOldPlate.Text = null;
			this.fldOldPlate.Top = 2.75F;
			this.fldOldPlate.Width = 1.09375F;
			// 
			// fldUnit
			// 
			this.fldUnit.CanGrow = false;
			this.fldUnit.Height = 0.19F;
			this.fldUnit.Left = 2.34375F;
			this.fldUnit.MultiLine = false;
			this.fldUnit.Name = "fldUnit";
			this.fldUnit.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldUnit.Text = null;
			this.fldUnit.Top = 0.71875F;
			this.fldUnit.Width = 1F;
			// 
			// fldCTANumber
			// 
			this.fldCTANumber.CanGrow = false;
			this.fldCTANumber.Height = 0.19F;
			this.fldCTANumber.Left = 3.875F;
			this.fldCTANumber.MultiLine = false;
			this.fldCTANumber.Name = "fldCTANumber";
			this.fldCTANumber.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldCTANumber.Text = null;
			this.fldCTANumber.Top = 1.0625F;
			this.fldCTANumber.Width = 0.90625F;
			// 
			// fldAddress
			// 
			this.fldAddress.CanGrow = false;
			this.fldAddress.Height = 0.19F;
			this.fldAddress.Left = 1.25F;
			this.fldAddress.MultiLine = false;
			this.fldAddress.Name = "fldAddress";
			this.fldAddress.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldAddress.Text = null;
			this.fldAddress.Top = 1.71875F;
			this.fldAddress.Width = 3.8125F;
			// 
			// fldCity
			// 
			this.fldCity.CanGrow = false;
			this.fldCity.Height = 0.19F;
			this.fldCity.Left = 1.25F;
			this.fldCity.MultiLine = false;
			this.fldCity.Name = "fldCity";
			this.fldCity.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldCity.Text = null;
			this.fldCity.Top = 2.03125F;
			this.fldCity.Width = 2.0625F;
			// 
			// fldState
			// 
			this.fldState.CanGrow = false;
			this.fldState.Height = 0.19F;
			this.fldState.Left = 3.46875F;
			this.fldState.MultiLine = false;
			this.fldState.Name = "fldState";
			this.fldState.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldState.Text = null;
			this.fldState.Top = 2.03125F;
			this.fldState.Width = 0.375F;
			// 
			// fldZip
			// 
			this.fldZip.CanGrow = false;
			this.fldZip.Height = 0.19F;
			this.fldZip.Left = 3.875F;
			this.fldZip.MultiLine = false;
			this.fldZip.Name = "fldZip";
			this.fldZip.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldZip.Text = null;
			this.fldZip.Top = 2.03125F;
			this.fldZip.Width = 0.5625F;
			// 
			// fldResidenceCity
			// 
			this.fldResidenceCity.CanGrow = false;
			this.fldResidenceCity.Height = 0.19F;
			this.fldResidenceCity.Left = 1.28125F;
			this.fldResidenceCity.MultiLine = false;
			this.fldResidenceCity.Name = "fldResidenceCity";
			this.fldResidenceCity.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldResidenceCity.Text = null;
			this.fldResidenceCity.Top = 2.3125F;
			this.fldResidenceCity.Width = 2.09375F;
			// 
			// fldResidenceState
			// 
			this.fldResidenceState.CanGrow = false;
			this.fldResidenceState.Height = 0.19F;
			this.fldResidenceState.Left = 3.46875F;
			this.fldResidenceState.MultiLine = false;
			this.fldResidenceState.Name = "fldResidenceState";
			this.fldResidenceState.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldResidenceState.Text = null;
			this.fldResidenceState.Top = 2.3125F;
			this.fldResidenceState.Width = 0.375F;
			// 
			// fldResidenceCode
			// 
			this.fldResidenceCode.CanGrow = false;
			this.fldResidenceCode.Height = 0.19F;
			this.fldResidenceCode.Left = 3.875F;
			this.fldResidenceCode.MultiLine = false;
			this.fldResidenceCode.Name = "fldResidenceCode";
			this.fldResidenceCode.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldResidenceCode.Text = null;
			this.fldResidenceCode.Top = 2.3125F;
			this.fldResidenceCode.Width = 0.59375F;
			// 
			// lblMileage
			// 
			this.lblMileage.Height = 0.19F;
			this.lblMileage.HyperLink = null;
			this.lblMileage.Left = 1.03125F;
			this.lblMileage.MultiLine = false;
			this.lblMileage.Name = "lblMileage";
			this.lblMileage.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblMileage.Text = "Expires";
			this.lblMileage.Top = 3.125F;
			this.lblMileage.Width = 0.59375F;
			// 
			// fldExpires
			// 
			this.fldExpires.CanGrow = false;
			this.fldExpires.Height = 0.19F;
			this.fldExpires.Left = 1.6875F;
			this.fldExpires.MultiLine = false;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldExpires.Text = null;
			this.fldExpires.Top = 3.125F;
			this.fldExpires.Width = 0.8125F;
			// 
			// fldPlate
			// 
			this.fldPlate.CanGrow = false;
			this.fldPlate.Height = 0.19F;
			this.fldPlate.Left = 4.78125F;
			this.fldPlate.MultiLine = false;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldPlate.Text = null;
			this.fldPlate.Top = 0.71875F;
			this.fldPlate.Width = 0.75F;
			// 
			// lblTransaction
			// 
			this.lblTransaction.Height = 0.1875F;
			this.lblTransaction.HyperLink = null;
			this.lblTransaction.Left = 3.125F;
			this.lblTransaction.MultiLine = false;
			this.lblTransaction.Name = "lblTransaction";
			this.lblTransaction.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblTransaction.Text = null;
			this.lblTransaction.Top = 2.71875F;
			this.lblTransaction.Width = 1.34375F;
			// 
			// fldDescription
			// 
			this.fldDescription.CanGrow = false;
			this.fldDescription.Height = 0.19F;
			this.fldDescription.Left = 1.03125F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldDescription.Text = null;
			this.fldDescription.Top = 2.9375F;
			this.fldDescription.Width = 2.59375F;
			// 
			// fldReRegYes
			// 
			this.fldReRegYes.CanGrow = false;
			this.fldReRegYes.Height = 0.19F;
			this.fldReRegYes.Left = 5.3125F;
			this.fldReRegYes.MultiLine = false;
			this.fldReRegYes.Name = "fldReRegYes";
			this.fldReRegYes.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldReRegYes.Text = "X";
			this.fldReRegYes.Top = 0.375F;
			this.fldReRegYes.Width = 0.15625F;
			// 
			// fldReRegNo
			// 
			this.fldReRegNo.CanGrow = false;
			this.fldReRegNo.Height = 0.19F;
			this.fldReRegNo.Left = 5.6875F;
			this.fldReRegNo.MultiLine = false;
			this.fldReRegNo.Name = "fldReRegNo";
			this.fldReRegNo.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fldReRegNo.Text = "X";
			this.fldReRegNo.Top = 0.375F;
			this.fldReRegNo.Width = 0.15625F;
			// 
			// fldOwner2
			// 
			this.fldOwner2.CanGrow = false;
			this.fldOwner2.Height = 0.19F;
			this.fldOwner2.Left = 1.25F;
			this.fldOwner2.MultiLine = false;
			this.fldOwner2.Name = "fldOwner2";
			this.fldOwner2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldOwner2.Text = null;
			this.fldOwner2.Top = 1.46875F;
			this.fldOwner2.Width = 3.84375F;
			// 
			// fldValidationLine1
			// 
			this.fldValidationLine1.CanGrow = false;
			this.fldValidationLine1.Height = 0.1875F;
			this.fldValidationLine1.Left = 2.5F;
			this.fldValidationLine1.MultiLine = false;
			this.fldValidationLine1.Name = "fldValidationLine1";
			this.fldValidationLine1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldValidationLine1.Text = null;
			this.fldValidationLine1.Top = 0F;
			this.fldValidationLine1.Width = 3.34375F;
			// 
			// fldValidationLine2Description
			// 
			this.fldValidationLine2Description.CanGrow = false;
			this.fldValidationLine2Description.Height = 0.1875F;
			this.fldValidationLine2Description.Left = 3.59375F;
			this.fldValidationLine2Description.MultiLine = false;
			this.fldValidationLine2Description.Name = "fldValidationLine2Description";
			this.fldValidationLine2Description.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.fldValidationLine2Description.Text = null;
			this.fldValidationLine2Description.Top = 0.1875F;
			this.fldValidationLine2Description.Width = 1.1875F;
			// 
			// fldValidationLine2
			// 
			this.fldValidationLine2.CanGrow = false;
			this.fldValidationLine2.Height = 0.1875F;
			this.fldValidationLine2.Left = 4.8125F;
			this.fldValidationLine2.MultiLine = false;
			this.fldValidationLine2.Name = "fldValidationLine2";
			this.fldValidationLine2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.fldValidationLine2.Text = null;
			this.fldValidationLine2.Top = 0.1875F;
			this.fldValidationLine2.Width = 1.03125F;
			// 
			// fld2000lbsNotice
			// 
			this.fld2000lbsNotice.CanGrow = false;
			this.fld2000lbsNotice.Height = 0.19F;
			this.fld2000lbsNotice.Left = 2.78125F;
			this.fld2000lbsNotice.MultiLine = false;
			this.fld2000lbsNotice.Name = "fld2000lbsNotice";
			this.fld2000lbsNotice.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.fld2000lbsNotice.Text = "GVW 2000lbs max";
			this.fld2000lbsNotice.Top = 0.375F;
			this.fld2000lbsNotice.Visible = false;
			this.fld2000lbsNotice.Width = 1.3125F;
			// 
			// rptMVRT10
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 5.854167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Courier New\'; font-style: inherit; font-variant: inherit; font-weig" + "ht: inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransaction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine2Description)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNotice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetWeight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransactionDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCTANumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMileage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTransaction;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidationLine1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidationLine2Description;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidationLine2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld2000lbsNotice;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
