﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmQuestions : BaseForm
	{
		public frmQuestions()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmQuestions InstancePtr
		{
			get
			{
				return (frmQuestions)Sys.GetInstance(typeof(frmQuestions));
			}
		}

		protected frmQuestions _InstancePtr = null;
		//=========================================================
		private void frmQuestions_Activated(object sender, System.EventArgs e)
		{
			string strMessage = "";
			if (MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "TC" || MotorVehicle.Statics.Class == "CL")
			{
				strMessage = "Please Input Your Initials";
				if (MotorVehicle.Statics.RegistrationType == "RRR" && (MotorVehicle.Statics.Class == "TL" || MotorVehicle.Statics.Class == "CL"))
				{
					lblTrailerClass.Text = MotorVehicle.Statics.Class;
					lblTrailerSubclass.Text = MotorVehicle.Statics.Subclass;
					fraTrailerClass.Visible = true;
				}
				else
				{
					fraTrailerClass.Visible = false;
				}
			}
			else
			{
				strMessage = "Please Input Your Initials for Insurance Verification";
				fraTrailerClass.Visible = false;
			}
			lblInitials.Text = strMessage;
			if (MotorVehicle.Statics.RegistrationType == "DUPSTK")
			{
				fraRegPrivilege.Enabled = false;
				lblPrivilege.Enabled = false;
				cmbPrivilegeNo.Enabled = false;
				fraSR22.Enabled = false;
				lblSR22.Enabled = false;
				cmbSR22No.Enabled = false;
				//FC:FINAL:DDU:#2275 - focus on correct control
				txtUserID.Focus();
			}
		}

		private void frmQuestions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			clsDRWrapper rsoperator = new clsDRWrapper();
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				MotorVehicle.Statics.UserID = "";
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (FCGlobal.Screen.ActiveControl.Name == "txtUserID")
				{
					if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.UserID).Length == 3 && fecherFoundation.Strings.Trim(MotorVehicle.Statics.UserID) != "")
					{
						rsoperator.OpenRecordset("SELECT * FROM Operators WHERE Code = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.UserID) + "'", "SystemSettings");
						if (rsoperator.EndOfFile() != true && rsoperator.BeginningOfFile() != true)
						{
							Close();
						}
						else
						{
							if (MotorVehicle.Statics.UserID != "988")
							{
								MessageBox.Show("This is an Invalid User ID.", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtUserID.Text = "";
							}
							else
							{
								Close();
							}
						}
					}
					else
					{
						MessageBox.Show("This is an Invalid User ID.", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtUserID.Text = "";
					}
				}
				else
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmQuestions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmQuestions properties;
			//frmQuestions.ScaleWidth	= 5880;
			//frmQuestions.ScaleHeight	= 4095;
			//frmQuestions.LinkTopic	= "Form1";
			//End Unmaped Properties
			MotorVehicle.Statics.UserID = "";
			MotorVehicle.Statics.ChickConversion = "N";
			JBFiling(MotorVehicle.Statics.Subclass);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
		}
		// vbPorter upgrade warning: Subclass As object	OnWrite(string)
		public void JBFiling(object Subclass)
		{
			if ((MotorVehicle.Statics.Class == "PC" && FCConvert.ToString(Subclass) == "P5") || (MotorVehicle.Statics.Class == "LS" && FCConvert.ToString(Subclass) == "S2"))
			{
				// kk10222015 tromv-1111 changed from P4
				fraJBFiling.Enabled = true;
				cmbJBFiling.Enabled = true;
				lblJBFiling.Enabled = true;
				fraJBFiling.Font = FCUtils.FontChangeBold(fraJBFiling.Font, true);
			}
			else
			{
				fraJBFiling.Enabled = false;
				cmbJBFiling.Enabled = false;
				fraJBFiling.Font = FCUtils.FontChangeBold(fraJBFiling.Font, false);
				lblJBFiling.Enabled = false;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
            try
            {
                ValidateSelections();
                if (MotorVehicle.Statics.UserID == "")
                {
                    fecherFoundation.Information.Err().Raise(12345, "", "User ID is Empty", null, null);
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		private void txtUserID_TextChanged(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.UserID = fecherFoundation.Strings.Trim(txtUserID.Text);
		}

		private void txtUserID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If Len(txtUserID.Text) > 0 Then
			// UserID = Trim(txtUserID.Text)
			// Else
			// If Class <> "TL" And Class <> "CL" And Class <> "TC" Then
			// Cancel = True
			// End If
			// End If
		}

		public void ValidateSelections()
		{
			if (cmbSR22No.Text == "Yes")
			{
				if ((MotorVehicle.Statics.RegistrationType == "DUPREG") || (MotorVehicle.Statics.RegistrationType == "GVW"))
				{
					fecherFoundation.Information.Err().Raise(12346, "SR-22 Registration", "Unable to Process SR-22 Registrations.", null, null);
					MotorVehicle.Statics.blnSR22 = false;
				}
				else
				{
					MessageBox.Show("This will be Processed as an Excise Tax Only Registration.", "ETO", MessageBoxButtons.OK, MessageBoxIcon.Information);
					MotorVehicle.Statics.ETO = true;
					MotorVehicle.Statics.blnSR22 = true;
				}
			}
			else
			{
				MotorVehicle.Statics.blnSR22 = false;
			}
			// 
			if (cmbPrivilegeNo.Text == "Yes")
			{
				MessageBox.Show("This will be Processed as an Excise Tax Only Registration.", "ETO", MessageBoxButtons.OK, MessageBoxIcon.Information);
				MotorVehicle.Statics.ETO = true;
				MotorVehicle.Statics.blnSuspended = true;
				// Err.Raise 12347, "Registration Suspension", "You are Unable to Process this Registration."
			}
			else
			{
				MotorVehicle.Statics.blnSuspended = false;
			}
		}

		private void SetCustomFormColors()
		{
			txtUserID.Font = FCUtils.FontChangeSize(txtUserID.Font, 14);
		}
	}
}
