using fecherFoundation;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmVehicleClassList
	{
		public fecherFoundation.FCComboBox cmbMultiple;
		public fecherFoundation.FCComboBox cmbPlate;
		public fecherFoundation.FCLabel lblPlate;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCComboBox cmbAllStatus;
		public fecherFoundation.FCComboBox cmbAllSubclass;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		public fecherFoundation.FCFrame fraDates;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraSubclass;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraSpecificStatus;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCComboBox cboStatus;
		public fecherFoundation.FCComboBox cboClass;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame fraSpecificSubclass;
		public fecherFoundation.FCComboBox cboSubclass;
		public fecherFoundation.FCGrid vsClass;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbMultiple = new fecherFoundation.FCComboBox();
			this.cmbPlate = new fecherFoundation.FCComboBox();
			this.lblPlate = new fecherFoundation.FCLabel();
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.cmbAllStatus = new fecherFoundation.FCComboBox();
			this.cmbAllSubclass = new fecherFoundation.FCComboBox();
			this.fraDates = new fecherFoundation.FCFrame();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.fraSpecificStatus = new fecherFoundation.FCFrame();
			this.cboStatus = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cboClass = new fecherFoundation.FCComboBox();
			this.fraSubclass = new fecherFoundation.FCFrame();
			this.fraSpecificSubclass = new fecherFoundation.FCFrame();
			this.cboSubclass = new fecherFoundation.FCComboBox();
			this.vsClass = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDates)).BeginInit();
			this.fraDates.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificStatus)).BeginInit();
			this.fraSpecificStatus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSubclass)).BeginInit();
			this.fraSubclass.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificSubclass)).BeginInit();
			this.fraSpecificSubclass.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(675, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbPlate);
			this.ClientArea.Controls.Add(this.lblPlate);
			this.ClientArea.Controls.Add(this.fraDates);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(675, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(675, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(207, 30);
			this.HeaderText.Text = "Vehicle Class List";
			// 
			// cmbMultiple
			// 
			this.cmbMultiple.AutoSize = false;
			this.cmbMultiple.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbMultiple.FormattingEnabled = true;
			this.cmbMultiple.Items.AddRange(new object[] {
            "Multiple",
            "Specific"});
			this.cmbMultiple.Location = new System.Drawing.Point(20, 30);
			this.cmbMultiple.Name = "cmbMultiple";
			this.cmbMultiple.Size = new System.Drawing.Size(192, 40);
			this.cmbMultiple.TabIndex = 26;
			this.cmbMultiple.Text = "Specific";
			this.cmbMultiple.SelectedIndexChanged += new System.EventHandler(this.cmbMultiple_SelectedIndexChanged);
			// 
			// cmbPlate
			// 
			this.cmbPlate.AutoSize = false;
			this.cmbPlate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPlate.FormattingEnabled = true;
			this.cmbPlate.Items.AddRange(new object[] {
            "Plate",
            "Exp Date"});
			this.cmbPlate.Location = new System.Drawing.Point(419, 200);
			this.cmbPlate.Name = "cmbPlate";
			this.cmbPlate.Size = new System.Drawing.Size(143, 40);
			this.cmbPlate.TabIndex = 0;
			this.cmbPlate.Text = "Plate";
			// 
			// lblPlate
			// 
			this.lblPlate.AutoSize = true;
			this.lblPlate.Location = new System.Drawing.Point(292, 214);
			this.lblPlate.Name = "lblPlate";
			this.lblPlate.Size = new System.Drawing.Size(72, 15);
			this.lblPlate.TabIndex = 1;
			this.lblPlate.Text = "ORDER BY";
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Expiration Date Range",
            "Effective Date Range",
            "Excise Tax Date Range"});
			this.cmbAll.Location = new System.Drawing.Point(20, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(320, 40);
			this.cmbAll.TabIndex = 16;
			this.cmbAll.Text = "All";
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
			// 
			// cmbAllStatus
			// 
			this.cmbAllStatus.AutoSize = false;
			this.cmbAllStatus.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllStatus.FormattingEnabled = true;
			this.cmbAllStatus.Items.AddRange(new object[] {
            "All",
            "Specific"});
			this.cmbAllStatus.Location = new System.Drawing.Point(20, 30);
			this.cmbAllStatus.Name = "cmbAllStatus";
			this.cmbAllStatus.Size = new System.Drawing.Size(230, 40);
			this.cmbAllStatus.TabIndex = 5;
			this.cmbAllStatus.Text = "All";
			this.cmbAllStatus.SelectedIndexChanged += new System.EventHandler(this.cmbAllStatus_SelectedIndexChanged);
			// 
			// cmbAllSubclass
			// 
			this.cmbAllSubclass.AutoSize = false;
			this.cmbAllSubclass.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllSubclass.FormattingEnabled = true;
			this.cmbAllSubclass.Items.AddRange(new object[] {
            "All",
            "Specific"});
			this.cmbAllSubclass.Location = new System.Drawing.Point(20, 30);
			this.cmbAllSubclass.Name = "cmbAllSubclass";
			this.cmbAllSubclass.Size = new System.Drawing.Size(147, 40);
			this.cmbAllSubclass.TabIndex = 9;
			this.cmbAllSubclass.Text = "All";
			this.cmbAllSubclass.SelectedIndexChanged += new System.EventHandler(this.cmbAllSubclass_SelectedIndexChanged);
			// 
			// fraDates
			// 
			this.fraDates.AppearanceKey = "groupBoxLeftBorder";
			this.fraDates.Controls.Add(this.fraDateRange);
			this.fraDates.Controls.Add(this.cmbAll);
			this.fraDates.Location = new System.Drawing.Point(292, 260);
			this.fraDates.Name = "fraDates";
			this.fraDates.Size = new System.Drawing.Size(365, 197);
			this.fraDates.TabIndex = 10;
			this.fraDates.Text = "Dates To Report";
			// 
			// fraDateRange
			// 
			this.fraDateRange.Controls.Add(this.txtStartDate);
			this.fraDateRange.Controls.Add(this.txtEndDate);
			this.fraDateRange.Controls.Add(this.lblTo);
			this.fraDateRange.Enabled = false;
			this.fraDateRange.Location = new System.Drawing.Point(20, 90);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(334, 84);
			this.fraDateRange.TabIndex = 15;
			this.fraDateRange.Text = "Select Date Range";
			// 
			// txtStartDate
			// 
			this.txtStartDate.Enabled = false;
			this.txtStartDate.Location = new System.Drawing.Point(20, 30);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.SelLength = 0;
			this.txtStartDate.SelStart = 0;
			this.txtStartDate.Size = new System.Drawing.Size(120, 40);
			this.txtStartDate.TabIndex = 16;
			this.txtStartDate.Text = "  /  /";
			// 
			// txtEndDate
			// 
			this.txtEndDate.Enabled = false;
			this.txtEndDate.Location = new System.Drawing.Point(194, 30);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.SelLength = 0;
			this.txtEndDate.SelStart = 0;
			this.txtEndDate.Size = new System.Drawing.Size(120, 40);
			this.txtEndDate.TabIndex = 17;
			this.txtEndDate.Text = "  /  /";
			// 
			// lblTo
			// 
			this.lblTo.Enabled = false;
			this.lblTo.Location = new System.Drawing.Point(160, 44);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(21, 21);
			this.lblTo.TabIndex = 18;
			this.lblTo.Text = "TO";
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.fraSpecificStatus);
			this.Frame3.Controls.Add(this.cmbAllStatus);
			this.Frame3.Location = new System.Drawing.Point(292, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(270, 150);
			this.Frame3.TabIndex = 1;
			this.Frame3.Text = "Status";
			// 
			// fraSpecificStatus
			// 
			this.fraSpecificStatus.AppearanceKey = "groupBoxNoBorders";
			this.fraSpecificStatus.Controls.Add(this.cboStatus);
			this.fraSpecificStatus.Enabled = false;
			this.fraSpecificStatus.Location = new System.Drawing.Point(20, 90);
			this.fraSpecificStatus.Name = "fraSpecificStatus";
			this.fraSpecificStatus.Size = new System.Drawing.Size(230, 49);
			this.fraSpecificStatus.TabIndex = 4;
			// 
			// cboStatus
			// 
			this.cboStatus.AutoSize = false;
			this.cboStatus.BackColor = System.Drawing.SystemColors.Window;
			this.cboStatus.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStatus.Enabled = false;
			this.cboStatus.FormattingEnabled = true;
			this.cboStatus.Items.AddRange(new object[] {
            "A - Active",
            "P - Pending",
            "T - Terminated"});
			this.cboStatus.Location = new System.Drawing.Point(0, 0);
			this.cboStatus.Name = "cboStatus";
			this.cboStatus.Size = new System.Drawing.Size(230, 40);
			this.cboStatus.TabIndex = 19;
			this.cboStatus.DropDown += new System.EventHandler(this.cboStatus_DropDown);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxLeftBorder";
			this.Frame1.Controls.Add(this.cboClass);
			this.Frame1.Controls.Add(this.cmbMultiple);
			this.Frame1.Controls.Add(this.fraSubclass);
			this.Frame1.Controls.Add(this.vsClass);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(232, 319);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Vehicle Class";
			// 
			// cboClass
			// 
			this.cboClass.AutoSize = false;
			this.cboClass.BackColor = System.Drawing.SystemColors.Window;
			this.cboClass.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboClass.FormattingEnabled = true;
			this.cboClass.Location = new System.Drawing.Point(20, 90);
			this.cboClass.Name = "cboClass";
			this.cboClass.Size = new System.Drawing.Size(192, 40);
			this.cboClass.TabIndex = 25;
			this.cboClass.SelectedIndexChanged += new System.EventHandler(this.cboClass_SelectedIndexChanged);
			// 
			// fraSubclass
			// 
			this.fraSubclass.Controls.Add(this.fraSpecificSubclass);
			this.fraSubclass.Controls.Add(this.cmbAllSubclass);
			this.fraSubclass.Location = new System.Drawing.Point(20, 150);
			this.fraSubclass.Name = "fraSubclass";
			this.fraSubclass.Size = new System.Drawing.Size(192, 150);
			this.fraSubclass.TabIndex = 5;
			this.fraSubclass.Text = "Subclass";
			// 
			// fraSpecificSubclass
			// 
			this.fraSpecificSubclass.AppearanceKey = "groupBoxNoBorders";
			this.fraSpecificSubclass.Controls.Add(this.cboSubclass);
			this.fraSpecificSubclass.Enabled = false;
			this.fraSpecificSubclass.Location = new System.Drawing.Point(20, 90);
			this.fraSpecificSubclass.Name = "fraSpecificSubclass";
			this.fraSpecificSubclass.Size = new System.Drawing.Size(154, 40);
			this.fraSpecificSubclass.TabIndex = 8;
			// 
			// cboSubclass
			// 
			this.cboSubclass.AutoSize = false;
			this.cboSubclass.BackColor = System.Drawing.SystemColors.Window;
			this.cboSubclass.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSubclass.Enabled = false;
			this.cboSubclass.FormattingEnabled = true;
			this.cboSubclass.Location = new System.Drawing.Point(0, 0);
			this.cboSubclass.Name = "cboSubclass";
			this.cboSubclass.Size = new System.Drawing.Size(147, 40);
			this.cboSubclass.TabIndex = 9;
			this.cboSubclass.DropDown += new System.EventHandler(this.cboSubclass_DropDown);
			// 
			// vsClass
			// 
			this.vsClass.AllowSelection = false;
			this.vsClass.AllowUserToResizeColumns = false;
			this.vsClass.AllowUserToResizeRows = false;
			this.vsClass.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsClass.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsClass.BackColorBkg = System.Drawing.Color.Empty;
			this.vsClass.BackColorFixed = System.Drawing.Color.Empty;
			this.vsClass.BackColorSel = System.Drawing.Color.Empty;
			this.vsClass.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsClass.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsClass.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsClass.ColumnHeadersHeight = 30;
			this.vsClass.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsClass.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsClass.DragIcon = null;
			this.vsClass.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsClass.ExtendLastCol = true;
			this.vsClass.FixedCols = 0;
			this.vsClass.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsClass.FrozenCols = 0;
			this.vsClass.GridColor = System.Drawing.Color.Empty;
			this.vsClass.GridColorFixed = System.Drawing.Color.Empty;
			this.vsClass.Location = new System.Drawing.Point(20, 90);
			this.vsClass.Name = "vsClass";
			this.vsClass.OutlineCol = 0;
			this.vsClass.ReadOnly = true;
			this.vsClass.RowHeadersVisible = false;
			this.vsClass.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsClass.RowHeightMin = 0;
			this.vsClass.Rows = 1;
			this.vsClass.ScrollTipText = null;
			this.vsClass.ShowColumnVisibilityMenu = false;
			this.vsClass.ShowFocusCell = false;
			this.vsClass.Size = new System.Drawing.Size(192, 183);
			this.vsClass.StandardTab = true;
			this.vsClass.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsClass.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsClass.TabIndex = 26;
			this.vsClass.Visible = false;
			this.vsClass.KeyDown += new Wisej.Web.KeyEventHandler(this.vsClass_KeyDownEvent);
			this.vsClass.Click += new System.EventHandler(this.vsClass_ClickEvent);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(257, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(148, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(601, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(46, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmVehicleClassList
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(675, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVehicleClassList";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Vehicle Class List";
			this.Load += new System.EventHandler(this.frmVehicleClassList_Load);
			this.Activated += new System.EventHandler(this.frmVehicleClassList_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVehicleClassList_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDates)).EndInit();
			this.fraDates.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificStatus)).EndInit();
			this.fraSpecificStatus.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSubclass)).EndInit();
			this.fraSubclass.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificSubclass)).EndInit();
			this.fraSpecificSubclass.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private fecherFoundation.FCButton cmdFilePreview;
        private fecherFoundation.FCButton cmdFilePrint;
    }
}