//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTSubLostPlates.
	/// </summary>
	public partial class rptLTTSubLostPlates : BaseSectionReport
	{
		public rptLTTSubLostPlates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptLTTSubLostPlates_ReportEnd;
		}

        private void RptLTTSubLostPlates_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
			rsAM.DisposeOf();
        }

        public static rptLTTSubLostPlates InstancePtr
		{
			get
			{
				return (rptLTTSubLostPlates)Sys.GetInstance(typeof(rptLTTSubLostPlates));
			}
		}

		protected rptLTTSubLostPlates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLTTSubLostPlates	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;
		int lngPK1;
		int lngPK2;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsAM.EndOfFile())
				{
					lblNoInfo.Visible = true;
				}
				else
				{
					eArgs.EOF = false;
					lblNoInfo.Visible = false;
				}
			}
			else
			{
				rsAM.MoveNext();
				if (rsAM.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			if (frmReportLongTerm.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				MotorVehicle.Statics.strSql = "SELECT FleetMaster.CompanyCode, LongTermTrailerRegistrations.* FROM FleetMaster INNER JOIN LongTermTrailerRegistrations ON FleetMaster.FleetNumber = LongTermTrailerRegistrations.FleetNumber WHERE Status <> 'V' AND PeriodCloseoutID < 1 AND OldPlate & '' <> Plate AND RegistrationType = 'LPS' ORDER BY FleetMaster.CompanyCode, LongTermTrailerRegistrations.OldPlate";
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate BETWEEN '" + frmReportLongTerm.InstancePtr.cboStart.Text + "' AND '" + frmReportLongTerm.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(MotorVehicle.Statics.strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
				MotorVehicle.Statics.strSql = "SELECT FleetMaster.CompanyCode, LongTermTrailerRegistrations.* FROM FleetMaster INNER JOIN LongTermTrailerRegistrations ON FleetMaster.FleetNumber = LongTermTrailerRegistrations.FleetNumber WHERE Status <> 'V' AND OldPlate & '' <> Plate AND RegistrationType = 'LPS' AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY FleetMaster.CompanyCode, LongTermTrailerRegistrations.OldPlate";
			}
			rsAM.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldPlate As object	OnWrite(object, string)
			// vbPorter upgrade warning: fldCompanyCode As object	OnWrite(object, string)
			if (rsAM.EndOfFile() != true)
			{
				fldPlate.Text = rsAM.Get_Fields_String("OldPlate");
				fldCompanyCode.Text = rsAM.Get_Fields_String("CompanyCode");
			}
			else
			{
				fldPlate.Text = "";
				fldCompanyCode.Text = "";
			}
		}

		private void rptLTTSubLostPlates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLTTSubLostPlates properties;
			//rptLTTSubLostPlates.Caption	= "ActiveReport1";
			//rptLTTSubLostPlates.Left	= 0;
			//rptLTTSubLostPlates.Top	= 0;
			//rptLTTSubLostPlates.Width	= 12465;
			//rptLTTSubLostPlates.Height	= 5490;
			//rptLTTSubLostPlates.StartUpPosition	= 3;
			//rptLTTSubLostPlates.SectionData	= "rptLTTSubLostPlates.dsx":0000;
			//End Unmaped Properties
		}
	}
}
