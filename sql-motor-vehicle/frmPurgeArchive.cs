﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmPurgeArchive : BaseForm
	{
		public frmPurgeArchive()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPurgeArchive InstancePtr
		{
			get
			{
				return (frmPurgeArchive)Sys.GetInstance(typeof(frmPurgeArchive));
			}
		}

		protected frmPurgeArchive _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/11/2001
		// This form will be used to purge any outdated information
		// the town wants to get rid of to decrease the size of their
		// database
		// ********************************************************
		clsDRWrapper rsPurge = new clsDRWrapper();

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			int totalcount = 0;
			DateTime tempdate;
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			if (!Information.IsDate(txtDate.Text))
			{
				MessageBox.Show("This is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (DateAndTime.DateValue(txtDate.Text) > DateAndTime.DateValue(Strings.Format(DateAndTime.DateAdd("yyyy", -5, DateTime.Today), "MM/dd/yyyy")))
			{
				MessageBox.Show("You must enter a date that is at least 5 years in the past.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			tempdate = fecherFoundation.DateAndTime.DateValue(txtDate.Text);
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			rsPurge.OpenRecordset("SELECT * FROM ArchiveMaster WHERE DateUpdated <= '" + FCConvert.ToString(tempdate) + "'");
			if (rsPurge.EndOfFile() != true && rsPurge.BeginningOfFile() != true)
			{
				rsPurge.MoveLast();
				rsPurge.MoveFirst();
				totalcount = rsPurge.RecordCount();
				frmWait.InstancePtr.Unload();
				//App.DoEvents();
				ans = MessageBox.Show(FCConvert.ToString(totalcount) + " records will be deleted.  Do you wish to continue?", "Delete Records?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Deleting Records";
					frmWait.InstancePtr.Refresh();
					rsPurge.Execute("DELETE FROM ArchiveMaster WHERE DateUpdated <= '" + FCConvert.ToString(tempdate) + "'", "TWMV0000.vb1");
					frmWait.InstancePtr.Unload();
					//App.DoEvents();
					MessageBox.Show("Purge Complete!!  " + FCConvert.ToString(totalcount) + " records deleted", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					return;
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//App.DoEvents();
				MessageBox.Show("No records found that were saved before that date.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			Close();
		}

		private void frmPurgeArchive_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPurgeArchive_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeArchive properties;
			//frmPurgeArchive.FillStyle	= 0;
			//frmPurgeArchive.ScaleWidth	= 5880;
			//frmPurgeArchive.ScaleHeight	= 3870;
			//frmPurgeArchive.LinkTopic	= "Form2";
			//frmPurgeArchive.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPurgeArchive_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmPurgeArchive_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}
	}
}
