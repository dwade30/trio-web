namespace TWMV0000
{
	/// <summary>
	/// Summary description for InventoryMaster.
	/// </summary>
	partial class rptInventoryMaster
	{
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptInventoryMaster));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.srptMVR3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptPlate = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptPermit = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptDouble = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptCombination = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptSingle = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Height = 0F;
			this.pageHeader.Name = "pageHeader";
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.srptMVR3,
            this.srptPlate,
            this.srptPermit,
            this.srptDouble,
            this.srptCombination,
            this.srptSingle});
			this.detail.Height = 1.572916F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// srptMVR3
			// 
			this.srptMVR3.CloseBorder = false;
			this.srptMVR3.Height = 0.135F;
			this.srptMVR3.Left = 0F;
			this.srptMVR3.Name = "srptMVR3";
			this.srptMVR3.Report = null;
			this.srptMVR3.ReportName = "subReport1";
			this.srptMVR3.Top = 0F;
			this.srptMVR3.Width = 7.5F;
			// 
			// srptPlate
			// 
			this.srptPlate.CloseBorder = false;
			this.srptPlate.Height = 0.135F;
			this.srptPlate.Left = 0F;
			this.srptPlate.Name = "srptPlate";
			this.srptPlate.Report = null;
			this.srptPlate.ReportName = "subReport1";
			this.srptPlate.Top = 1.078F;
			this.srptPlate.Width = 7.5F;
			// 
			// srptPermit
			// 
			this.srptPermit.CloseBorder = false;
			this.srptPermit.Height = 0.135F;
			this.srptPermit.Left = 0F;
			this.srptPermit.Name = "srptPermit";
			this.srptPermit.Report = null;
			this.srptPermit.ReportName = "subReport1";
			this.srptPermit.Top = 1.338F;
			this.srptPermit.Width = 7.5F;
			// 
			// srptDouble
			// 
			this.srptDouble.CloseBorder = false;
			this.srptDouble.Height = 0.135F;
			this.srptDouble.Left = 0F;
			this.srptDouble.Name = "srptDouble";
			this.srptDouble.Report = null;
			this.srptDouble.ReportName = "subReport1";
			this.srptDouble.Top = 0.78F;
			this.srptDouble.Width = 7.5F;
			// 
			// srptCombination
			// 
			this.srptCombination.CloseBorder = false;
			this.srptCombination.Height = 0.135F;
			this.srptCombination.Left = 0F;
			this.srptCombination.Name = "srptCombination";
			this.srptCombination.Report = null;
			this.srptCombination.ReportName = "subReport1";
			this.srptCombination.Top = 0.52F;
			this.srptCombination.Width = 7.5F;
			// 
			// srptSingle
			// 
			this.srptSingle.CloseBorder = false;
			this.srptSingle.Height = 0.135F;
			this.srptSingle.Left = 0F;
			this.srptSingle.Name = "srptSingle";
			this.srptSingle.Report = null;
			this.srptSingle.ReportName = "subReport1";
			this.srptSingle.Top = 0.26F;
			this.srptSingle.Width = 7.5F;
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			// 
			// rptInventoryMaster
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.pageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptMVR3;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptPlate;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptPermit;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptDouble;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptCombination;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptSingle;
	}
}
