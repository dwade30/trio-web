//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmReasonCodes.
	/// </summary>
	partial class frmReasonCodes
	{
		public fecherFoundation.FCListBox lstSystemCodes;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCButton cmdRemove;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCListBox lstUsed;
		public fecherFoundation.FCListBox lstAvailable;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReasonCodes));
            this.lstSystemCodes = new fecherFoundation.FCListBox();
            this.cmdDone = new fecherFoundation.FCButton();
            this.cmdRemove = new fecherFoundation.FCButton();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.lstUsed = new fecherFoundation.FCListBox();
            this.lstAvailable = new fecherFoundation.FCListBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 660);
            this.BottomPanel.Size = new System.Drawing.Size(1006, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lstSystemCodes);
            this.ClientArea.Controls.Add(this.cmdDone);
            this.ClientArea.Controls.Add(this.cmdRemove);
            this.ClientArea.Controls.Add(this.cmdAdd);
            this.ClientArea.Controls.Add(this.lstUsed);
            this.ClientArea.Controls.Add(this.lstAvailable);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1006, 600);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1006, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(261, 30);
            this.HeaderText.Text = "Update Reason Codes";
            // 
            // lstSystemCodes
            // 
            this.lstSystemCodes.BackColor = System.Drawing.SystemColors.Window;
            this.lstSystemCodes.Location = new System.Drawing.Point(30, 368);
            this.lstSystemCodes.Name = "lstSystemCodes";
            this.lstSystemCodes.Size = new System.Drawing.Size(417, 200);
            this.lstSystemCodes.TabIndex = 7;
            // 
            // cmdDone
            // 
            this.cmdDone.AppearanceKey = "actionButton";
            this.cmdDone.Location = new System.Drawing.Point(30, 278);
            this.cmdDone.Name = "cmdDone";
            this.cmdDone.Size = new System.Drawing.Size(95, 40);
            this.cmdDone.TabIndex = 6;
            this.cmdDone.Text = "Return";
            this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
            // 
            // cmdRemove
            // 
            this.cmdRemove.AppearanceKey = "actionButton";
            this.cmdRemove.Image = ((System.Drawing.Image)(resources.GetObject("cmdRemove.Image")));
            this.cmdRemove.Location = new System.Drawing.Point(482, 169);
            this.cmdRemove.Name = "cmdRemove";
            this.cmdRemove.Size = new System.Drawing.Size(40, 40);
            this.cmdRemove.TabIndex = 5;
            this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.Location = new System.Drawing.Point(482, 108);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(40, 40);
            this.cmdAdd.TabIndex = 4;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // lstUsed
            // 
            this.lstUsed.BackColor = System.Drawing.SystemColors.Window;
            this.lstUsed.Location = new System.Drawing.Point(540, 60);
            this.lstUsed.Name = "lstUsed";
            this.lstUsed.Size = new System.Drawing.Size(417, 200);
            this.lstUsed.TabIndex = 3;
            this.lstUsed.KeyPress += new Wisej.Web.KeyPressEventHandler(this.lstUsed_KeyPress);
            // 
            // lstAvailable
            // 
            this.lstAvailable.BackColor = System.Drawing.SystemColors.Window;
            this.lstAvailable.Location = new System.Drawing.Point(30, 60);
            this.lstAvailable.Name = "lstAvailable";
            this.lstAvailable.Size = new System.Drawing.Size(417, 200);
            this.lstAvailable.TabIndex = 8;
            this.lstAvailable.SelectedIndexChanged += new System.EventHandler(this.lstAvailable_SelectedIndexChanged);
            this.lstAvailable.KeyPress += new Wisej.Web.KeyPressEventHandler(this.lstAvailable_KeyPress);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 338);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(360, 15);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "SYSTEM ASSIGNED CODES";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(540, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(360, 15);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "CODES TO BE USED";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(360, 15);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "CODES AVAILABLE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 0;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // frmReasonCodes
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1006, 660);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmReasonCodes";
            this.Text = "Update Reason Codes";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmReasonCodes_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReasonCodes_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReasonCodes_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}