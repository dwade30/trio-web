//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class menuRapidRenewal : BaseForm
	{
        string currentFileName = "";
        long currentFileSize = 0;

        public menuRapidRenewal()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            this.FTP1 = new Chilkat.Ftp2();
            this.FTP1.EnableEvents = true;
            this.FTP1.OnProgressInfo += new Chilkat.Ftp2.ProgressInfoEventHandler(FTP1_TransferProgress);
			this.FTP1.OnPercentDone += FTP1_OnPercentDone;
            
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static menuRapidRenewal InstancePtr
		{
			get
			{
				return (menuRapidRenewal)Sys.GetInstance(typeof(menuRapidRenewal));
			}
		}

		protected menuRapidRenewal _InstancePtr = null;
		//=========================================================
		public bool LiveFlag;
		int FileCol;
		int LocalCol;
		string[] strFolders = null;
		string[] strDetailFiles = null;
		string[] strFinancialFiles = null;
		int intFolders;
		int intDetailFiles;
		int intFinancialFiles;
		string strResCode = "";
		string strDownloadFolder = "";
		string strDownloadArchiveFolder = "";
		public clsBudgetaryPosting clsPostInfo;
		public clsPostingJournalInfo clsJournalInfo;
        public bool blnPostJournal;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			cboStartMonth.SelectedIndex = -1;
			cboStartYear.SelectedIndex = -1;
			cboEndMonth.SelectedIndex = -1;
			cboEndYear.SelectedIndex = -1;
			fraRange.Visible = false;
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorTag = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				string Start;
				string Finish = "";
				clsDRWrapper rs = new clsDRWrapper();
				if (cboStartMonth.SelectedIndex == -1 || cboStartYear.SelectedIndex == -1 || cboEndMonth.SelectedIndex == -1 || cboEndYear.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a beginning Year and Month and an ending Year and Month before you may continue.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				Start = Conversion.Str(cboStartMonth.SelectedIndex + 1) + "/1/" + cboStartYear.Text;
				if (cboEndMonth.SelectedIndex == 11)
				{
					Finish = "1/1/" + Conversion.Str(Conversion.Val(cboEndYear.Text) + 1);
				}
				else
				{
					Finish = Conversion.Str(cboEndMonth.SelectedIndex + 2) + "/1/" + cboEndYear.Text;
				}
				if (fecherFoundation.DateAndTime.DateValue(Finish).ToOADate() < fecherFoundation.DateAndTime.DateValue(Start).ToOADate())
				{
					MessageBox.Show("Your ending date must be later then your beginning date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				rs.OpenRecordset("SELECT c.*, p.FirstName as Party1FirstName, p.MiddleName as Party1MI, p.LastName as Party1LastName, p.Designation as Party1Designation, q.FirstName as Party2FirstName, q.MiddleName as Party2MI, q.LastName as Party2LastName, q.Designation as Party2Designation, r.FirstName as Party3FirstName, r.MiddleName as Party3MI, r.LastName as Party3LastName, r.Designation as Party3Designation FROM Master as c LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE convert(int, isnull(MessageFlag, 0)) <> 3 AND ExpireDate BETWEEN '" + Start + "' and '" + Finish + "' AND (Status = 'A' OR Status = 'P') ORDER BY ExpireDate");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					cboStartMonth.SelectedIndex = -1;
					cboStartYear.SelectedIndex = -1;
					cboEndMonth.SelectedIndex = -1;
					cboEndYear.SelectedIndex = -1;
					fraRange.Visible = false;
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorTag;
					/* On Error GoTo ErrorTag */
					fecherFoundation.Information.Err().Clear();
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Processing Data";
					frmWait.InstancePtr.Refresh();
					rs.MoveLast();
					rs.MoveFirst();
					LoadData(ref rs);
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				frmWait.InstancePtr.Unload();
				//App.DoEvents();
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description);
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorTag:
						//? goto ErrorTag;
						break;
				}
			}
		}

		private void cmdProcessAll_Click(object sender, System.EventArgs e)
		{
			int counter;
            bool success = true;
            FCGrid grid;

			try
			{
				fecherFoundation.Information.Err().Clear();
				clsPostInfo = new clsBudgetaryPosting();
				clsJournalInfo = new clsPostingJournalInfo();

                if (vsDetail.Visible)
                {
                    grid = vsDetail;
                }
                else
                {
                    grid = vsFinancial;
                }
				
				if (grid.Rows == 0)
				{
					return;
				}
				cmbLoadData.Enabled = false;
				grid.Enabled = false;
				cmdDownload.Enabled = false;

				for (counter = 0; counter <= (grid.Rows - 1); counter++)
				{
                    if (FCConvert.CBool(grid.TextMatrix(counter, LocalCol)) != true)
                    {
                        VerifyFTPConnection();
                        success = DLFile(grid.TextMatrix(counter, FileCol), strDownloadFolder, grid.TextMatrix(counter, FileCol));
                        if (success)
                        {
                            grid.SetCellText(LocalCol, counter, FCConvert.ToString(true));
                        }
                        else
                        {
                            break;
                        }
                    }
				}

				if (success)
                {
                    ProcessAllFiles();
                    ProgressBar1.Value = 0;
                    MessageBox.Show("Process Complete", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("One or more files did not update successfully" + "\r\n" + "Please try again", "Update not successful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                if (blnPostJournal)
                {
                    clsPostInfo.AllowPreview = true;
                    clsPostInfo.PostJournals();
                }

				clsPostInfo = null;
				clsJournalInfo = null;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In cmdDownload " + "line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				clsPostInfo = null;
				clsJournalInfo = null;
			}
		}

        private void menuRapidRenewal_Activated(object sender, System.EventArgs e)
        {
            bool executeNoFTPFolder = false;
            bool executeErrConnect = false;
            int counter;
            string strFileName;
            bool blnFound = false;
            try
            {
                int ii;
                if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
                {
                    return;
                }
                if (!modSecurity.ValidPermissions(this, MotorVehicle.RAPIDRENEWAL))
                {
                    MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Close();
                    return;
                }
                for (ii = (DateTime.Now.Year - 5); ii <= (DateTime.Now.Year + 2); ii++)
                {
                    cboStartYear.AddItem(ii.ToString());
                    cboEndYear.AddItem(ii.ToString());
                }
                SetDefaultMonthRange();
                this.Refresh();
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Connecting to FTP Site");
                
                strDownloadFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "RapidRenewal\\");
                strDownloadArchiveFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "RapidRenewalArchive\\");
                fecherFoundation.Information.Err().Clear();
                FTP1.Hostname = "www1.maine.gov";
                FTP1.Username = "xxtrio";
                FTP1.Password = "303.harris";
                FTP1.Port = 21;
                try
                {
                    if (!FTP1.Connect())
                    {
                        executeErrConnect = true;
                        goto Err_Connect;
                    }
                }
                catch (Exception ex)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
					executeErrConnect = true;
                    goto Err_Connect;
                }
                Err_Connect:
                if (executeErrConnect)
                {
                    executeNoFTPFolder = true;
                    executeErrConnect = false;
                    goto NoFTPFolder;
                }

                fecherFoundation.Information.Err().Clear();
                FTP1.ChangeRemoteDir("RapidRenewal");
                FTP1.ChangeRemoteDir(strResCode);
                RefreshSocket();
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Building File List", true, intDetailFiles + intFinancialFiles + 1);
                if (intDetailFiles > 0)
                {
                    for (counter = 0; counter <= intDetailFiles - 1; counter++)
                    {
                        frmWait.InstancePtr.prgProgress.Value = counter + 1;
                        vsDetail.Rows += 1;
                        vsDetail.TextMatrix(vsDetail.Rows - 1, FileCol, strDetailFiles[counter]);
                        vsDetail.TextMatrix(vsDetail.Rows - 1, LocalCol, FCConvert.ToString(false));
                    }
                    vsDetail.Row = 0;
                }
                if (intFinancialFiles > 0)
                {
                    for (counter = 0; counter <= intFinancialFiles - 1; counter++)
                    {
                        frmWait.InstancePtr.prgProgress.Value = counter + 1;
                        vsFinancial.Rows += 1;
                        vsFinancial.TextMatrix(vsFinancial.Rows - 1, FileCol, strFinancialFiles[counter]);
                        vsFinancial.TextMatrix(vsFinancial.Rows - 1, LocalCol, FCConvert.ToString(false));
                    }
                    vsFinancial.Row = 0;
                }
                executeNoFTPFolder = true;
                goto NoFTPFolder;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (fecherFoundation.Information.Err(ex).Number == -2147217408)
                {
                    executeNoFTPFolder = true;
                    goto NoFTPFolder;
                }
                else
                {
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In form_activate line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            #region NoFTPFolder label
            NoFTPFolder:
            if (executeNoFTPFolder)
            {
                executeNoFTPFolder = false;
                strFileName = FCFileSystem.Dir(strDownloadFolder + "*.rrf", 0);
                while (strFileName != "")
                {
                    blnFound = false;
                    for (counter = 0; counter <= (vsFinancial.Rows - 1); counter++)
                    {
                        if (fecherFoundation.Strings.UCase(vsFinancial.TextMatrix(counter, FileCol)) == fecherFoundation.Strings.UCase(strFileName))
                        {
                            vsFinancial.TextMatrix(counter, LocalCol, FCConvert.ToString(true));
                            blnFound = true;
                            break;
                        }
                    }
                    if (!blnFound)
                    {
                        vsFinancial.AddItem("");
                        vsFinancial.TextMatrix(vsFinancial.Rows - 1, FileCol, strFileName);
                        vsFinancial.TextMatrix(vsFinancial.Rows - 1, LocalCol, FCConvert.ToString(true));
                    }
                    strFileName = FCFileSystem.Dir();
                }
                strFileName = FCFileSystem.Dir(strDownloadFolder + "*.rri", 0);
                while (strFileName != "")
                {
                    blnFound = false;
                    for (counter = 0; counter <= (vsDetail.Rows - 1); counter++)
                    {
                        if (fecherFoundation.Strings.UCase(vsDetail.TextMatrix(counter, FileCol)) == fecherFoundation.Strings.UCase(strFileName))
                        {
                            vsDetail.TextMatrix(counter, LocalCol, FCConvert.ToString(true));
							blnFound = true;
							break;
                        }
                    }
                    if (!blnFound)
                    {
                        vsDetail.AddItem("");
                        vsDetail.TextMatrix(vsDetail.Rows - 1, FileCol, strFileName);
                        vsDetail.TextMatrix(vsDetail.Rows - 1, LocalCol, FCConvert.ToString(true));
                    }
                    strFileName = FCFileSystem.Dir();
                }
                if (vsDetail.Rows > 0)
                {
                    vsDetail.Select(0, FileCol);
                    vsDetail.Sort = FCGrid.SortSettings.flexSortGenericAscending;
                }
                if (vsFinancial.Rows > 0)
                {
                    vsFinancial.Select(0, FileCol);
                    vsFinancial.Sort = FCGrid.SortSettings.flexSortGenericAscending;
                }
                frmWait.InstancePtr.prgProgress.Value = frmWait.InstancePtr.prgProgress.Maximum;
                frmWait.InstancePtr.Unload();
                executeNoFTPFolder = false;
                return;
            }
            #endregion
        }

		private void SetDefaultMonthRange()
		{
			// vbPorter upgrade warning: intStartMonth As int	OnWriteFCConvert.ToInt32(
			int intStartMonth;
			// vbPorter upgrade warning: intStartYear As int	OnWriteFCConvert.ToInt32(
			int intStartYear;
			// vbPorter upgrade warning: intEndMonth As int	OnWriteFCConvert.ToInt32(
			int intEndMonth;
			// vbPorter upgrade warning: intEndYear As int	OnWriteFCConvert.ToInt32(
			int intEndYear;
			DateTime tempdate;
			intStartMonth = DateTime.Today.Month;
			intStartYear = DateTime.Today.Year;
			tempdate = fecherFoundation.DateAndTime.DateAdd("m", 2, DateTime.Today);
			intEndMonth = tempdate.Month;
			intEndYear = tempdate.Year;
			cboStartMonth.SelectedIndex = intStartMonth - 1;
			cboStartYear.Text = FCConvert.ToString(intStartYear);
			cboEndMonth.SelectedIndex = intEndMonth - 1;
			cboEndYear.Text = FCConvert.ToString(intEndYear);
		}

		private void RefreshSocket()
		{
			FCCollection loDirCollection = new FCCollection();
            
            try
            {
                if (!FTP1.IsConnected)
                    return;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                for (int i = 0; i < FTP1.GetDirCount(); i++)
                {
                    string fileName = FTP1.GetFilename(i);
                    if (FTP1.GetIsDirectory(i))
                    {
                        Array.Resize(ref strFolders, intFolders + 1);
                        intFolders += 1;
                        strFolders[intFolders - 1] = fileName;
                    }
                    else if (Strings.Right(Strings.UCase(fileName), 3) == "RRI")
                    {
                        Array.Resize(ref strDetailFiles, intDetailFiles + 1);
                        intDetailFiles += 1;
                        strDetailFiles[intDetailFiles - 1] = fileName;
                    }
                    else if (Strings.Right(Strings.UCase(fileName), 3) == "RRF")
                    {
                        Array.Resize(ref strFinancialFiles, intFinancialFiles + 1);
                        intFinancialFiles += 1;
                        strFinancialFiles[intFinancialFiles - 1] = fileName;
                    }
                }
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In RefreshSocket line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

		private void menuRapidRenewal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			clsDRWrapper rs = new clsDRWrapper();
			int ii;
			// handles initial load to RR
			if (KeyCode == Keys.I)
			{
				KeyCode = (Keys)0;
				answer = MessageBox.Show("This will create an initial file to send to Rapid Renewal.  Do you wish to continue?", "Initial Load to Rapid Renewal", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (answer == DialogResult.No)
				{
					return;
				}
				else
				{
					rs.OpenRecordset("SELECT * FROM Master");
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						frmWait.InstancePtr.Show();
						frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Processing Data";
						frmWait.InstancePtr.Refresh();
						rs.MoveLast();
						rs.MoveFirst();
						LoadData(ref rs);
						frmWait.InstancePtr.Unload();
					}
					return;
				}
			}
		}

		private void menuRapidRenewal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int temp;
			switch (KeyAscii)
			{
			// handles the escape key being pressed
				case Keys.Escape:
					{
						KeyAscii = (Keys)0;
						Close();
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void menuRapidRenewal_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
            FCFileSystem fso = new FCFileSystem();
			DirectoryInfo fd;
			if (!Directory.Exists("RapidRenewal"))
			{
				fd = Directory.CreateDirectory("RapidRenewal");
				fd.Attributes = FileAttributes.Normal;
			}
			if (!Directory.Exists("RapidRenewalArchive"))
			{
				fd = Directory.CreateDirectory("RapidRenewalArchive");
				fd.Attributes = FileAttributes.Normal;
			}
			LocalCol = 0;
			FileCol = 1;
			vsDetail.ColHidden(LocalCol, true);
			vsFinancial.ColHidden(LocalCol, true);
			vsDetail.ColDataType(LocalCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsFinancial.ColDataType(LocalCol, FCGrid.DataTypeSettings.flexDTBoolean);
			rsDefaultInfo.OpenRecordset("SELECT * FROM WMV");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				if (FCConvert.ToBoolean(rsDefaultInfo.Get_Fields_Boolean("RRCreateCRJournal")))
				{
					fraRapidRenewal.Visible = true;
					chkCreateJournal.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkCreateJournal.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				strResCode = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode"));
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
            blnPostJournal = false;
		}

		private int DateDifference(DateTime datExpires)
		{
			int DateDifference = 0;
			// vbPorter upgrade warning: datNewExp As DateTime	OnWrite(string)
			DateTime datNewExp;
			int x;
			if (datExpires.Month < 3)
			{
				datNewExp = FCConvert.ToDateTime("3/31/" + FCConvert.ToString(datExpires.Year));
			}
			else
			{
				datNewExp = FCConvert.ToDateTime("3/31/" + FCConvert.ToString(datExpires.Year + 1));
			}
			x = fecherFoundation.DateAndTime.DateDiff("m", datExpires, datNewExp) + 1;
			if (x == 0)
			{
				x = 1;
			}
			else if (x > 12)
			{
				x = 12;
			}
			DateDifference = x;
			return DateDifference;
		}

		private Decimal Prorate(Decimal x, int y)
		{
			Decimal Prorate = 0;
			Decimal total;
			total = x / 12;
			total *= y;
			Prorate = total;
			return Prorate;
		}

		private void LoadData(ref clsDRWrapper rs)
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string rec = "";
			int tempMillYear = 0;
			clsDRWrapper rsExciseExempt = new clsDRWrapper();
			int counter = 0;
			clsDRWrapper rsMast = new clsDRWrapper();
            FCFileSystem fs = new FCFileSystem();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
            if (FCFileSystem.FileExists(rsDefaultInfo.Get_Fields_String("ResidenceCode") + "RR.DAT"))
            {
                //FC:FINAL:SBE - #i2239 - close file if it was opened by another process
                FCFileSystem.FileClose(2);
                FCFileSystem.DeleteFile(rsDefaultInfo.Get_Fields_String("ResidenceCode") + "RR.DAT");//, true);
            }
			FCFileSystem.FileOpen(2, rsDefaultInfo.Get_Fields_String("ResidenceCode") + "RR.DAT", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.RRInfoRecord));
			counter = 1;
			while (rs.EndOfFile() != true)
			{
				MotorVehicle.ClearRRRecord();
				rec = Strings.StrDup(690, " ");
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("PlateStripped")) == "")
				{
					rsMast.OpenRecordset("SELECT * FROM Master WHERE ID = " + rs.Get_Fields_Int32("ID"));
					if (rsMast.EndOfFile() != true && rsMast.BeginningOfFile() != true)
					{
						rsMast.Edit();
						rsMast.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(rs.Get_Fields_String("plate")).Replace("&", "").Replace(" ", "").Replace("-", ""));
						rsMast.Update();
					}
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("PlateStripped")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.plate = rs.Get_Fields_String("PlateStripped");
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Class")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.Class = rs.Get_Fields_String("Class");
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Subclass")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.Subclass = rs.Get_Fields_String("Subclass");
				}
				if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 0)
				{
					if (rs.Get_Fields_Int32("MillYear") == 0)
					{
						if (Conversion.Val(rs.Get_Fields("Year")) != 0)
						{
							if (rs.Get_Fields_DateTime("ExpireDate").Year - Conversion.Val(rs.Get_Fields("Year")) > 6)
							{
								tempMillYear = 6;
							}
							else
							{
								tempMillYear = FCConvert.ToInt32((rs.Get_Fields_DateTime("ExpireDate").Year - Conversion.Val(rs.Get_Fields("Year"))));
							}
						}
						else
						{
							tempMillYear = 1;
						}
					}
				}
				if (rs.Get_Fields_Boolean("ExciseExempt") == true)
				{
					//FC:FINAL:JTA - exception thrown, convert string formt result to decimal
                    //MotorVehicle.Statics.RRInfoRecord.ExciseTax = FCConvert.ToString(FCConvert.ToInt32(Strings.Format("0", "##0.00")) * 100);
                    MotorVehicle.Statics.RRInfoRecord.ExciseTax = FCConvert.ToString(FCConvert.ToDecimal(Strings.Format("0", "##0.00")) * 100);
                    goto SkipExcise;
				}
				else
				{
					rsExciseExempt.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + rs.Get_Fields_String("Class") + "' AND SystemCode = '" + rs.Get_Fields_String("Subclass") + "'", "TWMV0000.vb1");
					if (rsExciseExempt.EndOfFile() != true && rsExciseExempt.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rsExciseExempt.Get_Fields_String("ExciseRequired")) == "N")
						{
                            //FC:FINAL:JTA - exception thrown, convert string formt result to decimal
                            //MotorVehicle.Statics.RRInfoRecord.ExciseTax = FCConvert.ToString(FCConvert.ToInt32(Strings.Format("0", "##0.00")) * 100);
                            MotorVehicle.Statics.RRInfoRecord.ExciseTax = FCConvert.ToString(FCConvert.ToDecimal(Strings.Format("0", "##0.00")) * 100);
                            goto SkipExcise;
						}
					}
				}
				// vbPorter upgrade warning: curExcise As Decimal	OnWriteFCConvert.ToDouble(
				Decimal curExcise = 0;
				if (Conversion.Val(rs.Get_Fields_Int32("BasePrice")) != 0)
				{
					if (rs.Get_Fields_Int32("MillYear") == 0)
					{
						switch (tempMillYear)
						{
							case 1:
								{
									// 2.4
									curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.0175);
									break;
								}
							case 2:
								{
									// 1.75
									curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.0135);
									break;
								}
							case 3:
								{
									// 1.35
									curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.01);
									break;
								}
							case 4:
								{
									// 1
									curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.0065);
									break;
								}
							case 5:
								{
									// 0.65
									curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.004);
									break;
								}
							case 6:
								{
									// 0.4
									curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.004);
									break;
								}
						}
						//end switch
					}
					else if (rs.Get_Fields_Int32("MillYear") == 1)
					{
						// 2.4
						curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.0175);
					}
					else if (rs.Get_Fields_Int32("MillYear") == 2)
					{
						// 1.75
						curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.0135);
					}
					else if (rs.Get_Fields_Int32("MillYear") == 3)
					{
						// 1.35
						curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.01);
					}
					else if (rs.Get_Fields_Int32("MillYear") == 4)
					{
						// 1
						curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.0065);
					}
					else if (rs.Get_Fields_Int32("MillYear") == 5)
					{
						// 0.65
						curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.004);
					}
					else if (rs.Get_Fields_Int32("MillYear") == 6)
					{
						// 0.4
						curExcise = FCConvert.ToDecimal(rs.Get_Fields_Int32("BasePrice") * 0.004);
					}
					if (MotorVehicle.IsMotorcycle(rs.Get_Fields_String("Class")))
					{
						if (rs.Get_Fields_DateTime("ExpireDate").Month != 3 && rs.Get_Fields_DateTime("ExpireDate").Month != 4 && rs.Get_Fields_DateTime("ExpireDate") < FCConvert.ToDateTime("3/1/2013"))
						{
							curExcise = FCConvert.ToDecimal(modGNBas.Round_8(FCConvert.ToDouble(Prorate(curExcise, DateDifference(rs.Get_Fields_DateTime("ExpireDate")))), 2));
						}
					}
                    //FC:FINAL:JTA - exception thrown, convert string formt result to decimal
                    //MotorVehicle.Statics.RRInfoRecord.ExciseTax = FCConvert.ToString(FCConvert.ToInt32(Strings.Format(curExcise, "##0.00")) * 100);
                    MotorVehicle.Statics.RRInfoRecord.ExciseTax = FCConvert.ToString(FCConvert.ToDecimal(Strings.Format(curExcise, "##0.00")) * 100);
                }
				SkipExcise:
				;
				if (Conversion.Val(rs.Get_Fields_Int32("BasePrice")) != 0)
				{
					MotorVehicle.Statics.RRInfoRecord.BaseValue = FCConvert.ToString(rs.Get_Fields_Int32("BasePrice") * 100);
				}
				else
				{
					MotorVehicle.Statics.RRInfoRecord.BaseValue = "0";
				}
				if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 0)
				{
					switch (tempMillYear)
					{
						case 1:
							{
								// 2.4
								MotorVehicle.Statics.RRInfoRecord.MilRate = "1750";
								break;
							}
						case 2:
							{
								// 1.75
								MotorVehicle.Statics.RRInfoRecord.MilRate = "1350";
								break;
							}
						case 3:
							{
								// 1.35
								MotorVehicle.Statics.RRInfoRecord.MilRate = "1000";
								break;
							}
						case 4:
							{
								// 1
								MotorVehicle.Statics.RRInfoRecord.MilRate = "0650";
								break;
							}
						case 5:
							{
								// 0.65
								MotorVehicle.Statics.RRInfoRecord.MilRate = "0400";
								break;
							}
						case 6:
							{
								// 0.4
								MotorVehicle.Statics.RRInfoRecord.MilRate = "0400";
								break;
							}
					}
					//end switch
				}
				else if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 1)
				{
					// 2.4
					MotorVehicle.Statics.RRInfoRecord.MilRate = "1750";
				}
				else if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 2)
				{
					// 1.75
					MotorVehicle.Statics.RRInfoRecord.MilRate = "1350";
				}
				else if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 3)
				{
					// 1.35
					MotorVehicle.Statics.RRInfoRecord.MilRate = "1000";
				}
				else if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 4)
				{
					// 1
					MotorVehicle.Statics.RRInfoRecord.MilRate = "0650";
				}
				else if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 5)
				{
					// 0.65
					MotorVehicle.Statics.RRInfoRecord.MilRate = "0400";
				}
				else if (Conversion.Val(rs.Get_Fields_Int32("MillYear")) == 6)
				{
					// 0.4
					MotorVehicle.Statics.RRInfoRecord.MilRate = "0400";
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Vin")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.VIN = rs.Get_Fields_String("Vin");
				}
				if (Conversion.Val(rs.Get_Fields("Year")) != 0)
				{
					MotorVehicle.Statics.RRInfoRecord.Year = FCConvert.ToString(rs.Get_Fields("Year"));
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("make")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.make = rs.Get_Fields_String("make");
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("model")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.model = rs.Get_Fields_String("model");
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Style")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.Style = rs.Get_Fields_String("Style");
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("color1")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.Color1 = rs.Get_Fields_String("color1");
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("color2")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.Color2 = rs.Get_Fields_String("color2");
				}
				if (Conversion.Val(rs.Get_Fields_Int32("Axles")) != 0)
				{
					MotorVehicle.Statics.RRInfoRecord.Axles = FCConvert.ToString(rs.Get_Fields_Int32("Axles"));
				}
				if (Conversion.Val(rs.Get_Fields("NetWeight")) != 0)
				{
					MotorVehicle.Statics.RRInfoRecord.NetWeight = FCConvert.ToString(rs.Get_Fields("NetWeight"));
				}
				if (Conversion.Val(rs.Get_Fields_Int32("RegisteredWeightNew")) != 0)
				{
					MotorVehicle.Statics.RRInfoRecord.RegisteredWeight = FCConvert.ToString(rs.Get_Fields_Int32("RegisteredWeightNew"));
				}
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Fuel")) != "")
				{
					MotorVehicle.Statics.RRInfoRecord.Fuel = rs.Get_Fields_String("Fuel");
				}
				if (rs.Get_Fields_String("OwnerCode1") == "I")
				{
					if (Information.IsDate(rs.Get_Fields("DOB1")))
					{
						MotorVehicle.Statics.RRInfoRecord.DOB1 = (FCConvert.ToString(rs.Get_Fields_DateTime("DOB1").Year) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DOB1").Month), 2) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DOB1").Day), 2));
					}
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.LastName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rs.Get_Fields("Party1LastName")));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.FirstName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rs.Get_Fields("Party1FirstName")));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.MiddleInitial.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(rs.Get_Fields("Party1MI") + ""), 1));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.Suffix.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(rs.Get_Fields("Party1Designation") + ""), 3));
                    var refVar = MotorVehicle.Statics.RRInfoRecord.Owner1;
                    Strings.MidSet(ref refVar, 1, 20, MotorVehicle.Statics.RRInfoRecordIndividualOwner.LastName.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner1 = refVar;
                    Strings.MidSet(ref refVar, 21, 15, MotorVehicle.Statics.RRInfoRecordIndividualOwner.FirstName.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner1 = refVar;
                    Strings.MidSet(ref refVar, 36, 1, MotorVehicle.Statics.RRInfoRecordIndividualOwner.MiddleInitial.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner1 = refVar;
                    Strings.MidSet(ref refVar, 37, 3, MotorVehicle.Statics.RRInfoRecordIndividualOwner.Suffix.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner1 = refVar;
                }
				else
				{
					if (!(fecherFoundation.Strings.Trim(rs.Get_Fields("Party1FirstName")) == ""))
					{
						MotorVehicle.Statics.RRInfoRecord.Owner1 = fecherFoundation.Strings.UCase(rs.Get_Fields("Party1FirstName"));
					}
				}
				if (!(fecherFoundation.Strings.Trim(rs.Get_Fields_String("Address")) == ""))
				{
					if (rs.Get_Fields_String("Address").Length > 72)
					{
						MotorVehicle.Statics.RRInfoRecord.Address1 = Strings.Mid(rs.Get_Fields_String("Address"), 1, 72);
					}
					else
					{
						MotorVehicle.Statics.RRInfoRecord.Address1 = rs.Get_Fields_String("Address");
					}
				}
				if (!(fecherFoundation.Strings.Trim(rs.Get_Fields_String("Address2")) == ""))
				{
					if (rs.Get_Fields_String("Address2").Length > 72)
					{
						MotorVehicle.Statics.RRInfoRecord.Address2 = Strings.Mid(rs.Get_Fields_String("Address2"), 1, 72);
					}
					else
					{
						MotorVehicle.Statics.RRInfoRecord.Address2 = rs.Get_Fields_String("Address2");
					}
				}
				if (!(fecherFoundation.Strings.Trim(rs.Get_Fields_String("City")) == ""))
				{
					MotorVehicle.Statics.RRInfoRecord.City = rs.Get_Fields_String("City");
				}
				if (!(fecherFoundation.Strings.Trim(rs.Get_Fields("State")) == ""))
				{
					MotorVehicle.Statics.RRInfoRecord.State = rs.Get_Fields("State");
				}
				if (!(fecherFoundation.Strings.Trim(rs.Get_Fields_String("Zip")) == ""))
				{
					MotorVehicle.Statics.RRInfoRecord.Zip = (rs.Get_Fields_String("Zip") + rs.Get_Fields_String("Zip4"));
				}
				if (rs.Get_Fields_String("OwnerCode2") == "I")
				{
					if (Information.IsDate(rs.Get_Fields("DOB2")))
					{
						MotorVehicle.Statics.RRInfoRecord.DOB2 = (FCConvert.ToString(rs.Get_Fields_DateTime("DOB2").Year) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DOB2").Month), 2) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DOB2").Day), 2));
					}
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.LastName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rs.Get_Fields("Party2LastName")));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.FirstName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rs.Get_Fields("Party2FirstName")));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.MiddleInitial.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(rs.Get_Fields("Party2MI") + ""), 1));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.Suffix.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(rs.Get_Fields("Party2Designation") + ""), 3));
                    var refVar = MotorVehicle.Statics.RRInfoRecord.Owner2;
                    Strings.MidSet(ref refVar, 1, 20, MotorVehicle.Statics.RRInfoRecordIndividualOwner.LastName.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner2 = refVar;
                    Strings.MidSet(ref refVar, 21, 15, MotorVehicle.Statics.RRInfoRecordIndividualOwner.FirstName.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner2 = refVar;
                    Strings.MidSet(ref refVar, 36, 1, MotorVehicle.Statics.RRInfoRecordIndividualOwner.MiddleInitial.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner2 = refVar;
                    Strings.MidSet(ref refVar, 37, 3, MotorVehicle.Statics.RRInfoRecordIndividualOwner.Suffix.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner2 = refVar;
                }
				else
				{
					if (!(fecherFoundation.Strings.Trim(rs.Get_Fields("Party2FirstName")) == ""))
					{
						MotorVehicle.Statics.RRInfoRecord.Owner2 = fecherFoundation.Strings.UCase(rs.Get_Fields("Party2FirstName"));
					}
				}
				if (rs.Get_Fields_String("OwnerCode3") == "I")
				{
					if (Information.IsDate(rs.Get_Fields("DOB3")))
					{
						MotorVehicle.Statics.RRInfoRecord.DOB3 = (FCConvert.ToString(rs.Get_Fields_DateTime("DOB3").Year) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DOB3").Month), 2) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DOB3").Day), 2));
					}
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.LastName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rs.Get_Fields("Party3LastName")));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.FirstName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(rs.Get_Fields("Party3FirstName")));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.MiddleInitial.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(rs.Get_Fields("Party3MI") + ""), 1));
					MotorVehicle.Statics.RRInfoRecordIndividualOwner.Suffix.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(rs.Get_Fields("Party3Designation") + ""), 3));
                    var refVar = MotorVehicle.Statics.RRInfoRecord.Owner3;
                    Strings.MidSet(ref refVar, 1, 20, MotorVehicle.Statics.RRInfoRecordIndividualOwner.LastName.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner3 = refVar;
                    Strings.MidSet(ref refVar, 21, 15, MotorVehicle.Statics.RRInfoRecordIndividualOwner.FirstName.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner3 = refVar;
                    Strings.MidSet(ref refVar, 36, 1, MotorVehicle.Statics.RRInfoRecordIndividualOwner.MiddleInitial.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner3 = refVar;
                    Strings.MidSet(ref refVar, 37, 3, MotorVehicle.Statics.RRInfoRecordIndividualOwner.Suffix.Value);
                    MotorVehicle.Statics.RRInfoRecord.Owner3 = refVar;
                }
				else
				{
					if (!(fecherFoundation.Strings.Trim(rs.Get_Fields("Party3FirstName")) == ""))
					{
						MotorVehicle.Statics.RRInfoRecord.Owner3 = fecherFoundation.Strings.UCase(rs.Get_Fields("Party3FirstName"));
					}
				}
				if (rs.Get_Fields_Boolean("ExciseExempt"))
				{
					MotorVehicle.Statics.RRInfoRecord.ExciseExempt = "Y";
				}
				if (rs.Get_Fields_Boolean("RegExempt"))
				{
					MotorVehicle.Statics.RRInfoRecord.RegExempt = "Y";
				}
				if (Information.IsDate(rs.Get_Fields("DateUpdated")))
				{
					MotorVehicle.Statics.RRInfoRecord.TransactionDate = (FCConvert.ToString(rs.Get_Fields_DateTime("DateUpdated").Year) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DateUpdated").Month), 2) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("DateUpdated").Day), 2));
				}
				if (!(rs.IsFieldNull("YearStickerNumber")))
				{
					MotorVehicle.Statics.RRInfoRecord.OldStickerNumber = FCConvert.ToString(rs.Get_Fields_Int32("YearStickerNumber"));
				}
				if (!(rs.IsFieldNull("MVR3")))
				{
					MotorVehicle.Statics.RRInfoRecord.OldMVR3 = FCConvert.ToString(rs.Get_Fields_Int32("MVR3"));
				}
				if (Information.IsDate(rs.Get_Fields("EffectiveDate")))
				{
					MotorVehicle.Statics.RRInfoRecord.EffectiveDate = (FCConvert.ToString(rs.Get_Fields_DateTime("EffectiveDate").Year) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("EffectiveDate").Month), 2) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("EffectiveDate").Day), 2));
				}
				if (Information.IsDate(rs.Get_Fields("ExpireDate")))
				{
					MotorVehicle.Statics.RRInfoRecord.ExpirationDate = (FCConvert.ToString(rs.Get_Fields_DateTime("ExpireDate").Year) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("ExpireDate").Month), 2) + AppendZeros(FCConvert.ToString(rs.Get_Fields_DateTime("ExpireDate").Day), 2));
				}
				if (!(fecherFoundation.Strings.Trim(rs.Get_Fields_String("ResidenceCode")) == ""))
				{
					MotorVehicle.Statics.RRInfoRecord.ResidenceCode = rs.Get_Fields_String("ResidenceCode");
				}
				MotorVehicle.Statics.RRInfoRecord.CRLF = "\r\n";
				FCFileSystem.FilePut(2, MotorVehicle.Statics.RRInfoRecord, -1/*? counter */);
				counter += 1;
				rs.MoveNext();
			}
			FCFileSystem.FileClose(2);
			frmWait.InstancePtr.Unload();
            
            string fileName = rsDefaultInfo.Get_Fields_String("ResidenceCode") + "RR.DAT";
            MessageBox.Show(rs.RecordCount() + " records have been successfully extracted into " + fileName, "Extract Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            FCUtils.Download(Path.Combine(FCFileSystem.CurDir(), fileName), fileName);
		}

		private string AppendZeros(string x, int y)
		{
			string AppendZeros = "";
			if (fecherFoundation.Strings.Trim(x).Length < y)
			{
				AppendZeros = Strings.StrDup(y - x.Length, "0") + fecherFoundation.Strings.Trim(x);
			}
			else
			{
				AppendZeros = fecherFoundation.Strings.Trim(x);
			}
			return AppendZeros;
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case MotorVehicle.MONTHLYLOAD:
						{
							if (strPerm == "N")
							{
								if (cmbLoadData.Items.Contains("Create Rapid Renewal Data File"))
								{
									cmbLoadData.Items.Remove("Create Rapid Renewal Data File");
								}
							}
							else
							{
								if (!cmbLoadData.Items.Contains("Create Rapid Renewal Data File"))
								{
									cmbLoadData.Items.Insert(0, "Create Rapid Renewal Data File");
								}
							}
							break;
						}
					case MotorVehicle.FINANCIALPROCESSING:
						{
							if (strPerm == "N")
							{
								if (cmbLoadData.Items.Contains("Process Financial File"))
								{
									cmbLoadData.Items.Remove("Process Financial File");
								}
								if (cmbLoadData.Items.Contains("Process Detail File"))
								{
									cmbLoadData.Items.Remove("Process Detail File");
								}
							}
							else
							{
								if (!cmbLoadData.Items.Contains("Process Financial File"))
								{
									cmbLoadData.Items.Insert(1, "Process Financial File");
								}
								if (!cmbLoadData.Items.Contains("Process Detail File"))
								{
									cmbLoadData.Items.Insert(2, "Process Detail File");
								}
							}
							break;
						}
				}
				//end switch
				if (!cmbLoadData.Items.Contains("Create Rapid Renewal Data File") && !cmbLoadData.Items.Contains("Process Financial File") && !cmbLoadData.Items.Contains("Process Detail File"))
				{
					fraRange.Visible = false;
				}
				else if (cmbLoadData.Items.Contains("Create Rapid Renewal Data File"))
				{
					cmbLoadData.Text = "Create Rapid Renewal Data File";
				}
				else if (cmbLoadData.Items.Contains("Process Financial File"))
				{
					cmbLoadData.Text = "Process Financial File";
				}
				else
				{
					cmbLoadData.Text = "Process Detail File";
				}
				clsChildList.MoveNext();
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FTP1.IsConnected)
				FTP1.Disconnect();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

        private void ProcessFile(string strFile)
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
            FCFileSystem fs = new FCFileSystem();
			if (fecherFoundation.Strings.UCase(Strings.Right(strFile, 3)) == "RRF")
			{
				FCFileSystem.FileOpen(2, strDownloadFolder + strFile, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Processing";
				frmWait.InstancePtr.Refresh();
				rptProcessFinancial.InstancePtr.Init("", false, chkCreateJournal.Checked);
				frmWait.InstancePtr.Unload();
			}
			else
			{
				answer = MessageBox.Show("Are you running this in Live Mode?", "Live Mode?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (answer == DialogResult.Yes)
				{
					LiveFlag = true;
				}
				else
				{
					LiveFlag = false;
				}
				FCFileSystem.FileOpen(2, strDownloadFolder + strFile, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.RRInfoRecord));
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Processing";
				frmWait.InstancePtr.Refresh();
				rptProcessDetail.InstancePtr.Init("", false);
				frmWait.InstancePtr.Unload();
			}
			if (fecherFoundation.Strings.UCase(Strings.Right(strFile, 3)) == "RRF" || LiveFlag)
			{
                FCFileSystem.FileCopy(strDownloadFolder + strFile, strDownloadArchiveFolder + strFile);//, true);
                FCFileSystem.DeleteFile(strDownloadFolder + strFile);//, true);
				if (vsDetail.Visible == true)
				{
					vsDetail.RemoveItem(vsDetail.Row);
				}
				else
				{
					vsFinancial.RemoveItem(vsFinancial.Row);
				}
			}
			else
			{
				vsDetail.TextMatrix(vsDetail.Row, LocalCol, FCConvert.ToString(true));
			}
			cmbLoadData.Enabled = true;
			if (vsDetail.Visible == true)
			{
				vsDetail.Enabled = true;
			}
			else
			{
				vsFinancial.Enabled = true;
			}

            if (blnPostJournal)
            {
                clsPostInfo.AllowPreview = true;
                clsPostInfo.PostJournals();
            }

			cmdDownload.Enabled = true;
			cmdProcessAll.Enabled = true;
		}

		private void ProcessAllFiles()
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
            FCFileSystem fs = new FCFileSystem();
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (vsFinancial.Visible)
			{
				FCFileSystem.FileOpen(2, strDownloadFolder + vsFinancial.TextMatrix(0, FileCol), OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Processing";
				frmWait.InstancePtr.Refresh();
				rptProcessFinancial.InstancePtr.Init(strDownloadFolder, true, chkCreateJournal.Checked);
				frmWait.InstancePtr.Unload();
			}
			else
			{
				answer = MessageBox.Show("Are you running this in Live Mode?", "Live Mode?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (answer == DialogResult.Yes)
				{
					LiveFlag = true;
				}
				else
				{
					LiveFlag = false;
				}
				FCFileSystem.FileOpen(2, strDownloadFolder + vsDetail.TextMatrix(0, FileCol), OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.RRInfoRecord));
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Processing";
				frmWait.InstancePtr.Refresh();
				rptProcessDetail.InstancePtr.Init(strDownloadFolder, true);
				frmWait.InstancePtr.Unload();
			}
			if (vsFinancial.Visible)
			{
				for (counter = 0; counter <= (vsFinancial.Rows - 1); counter++)
				{
                    FCFileSystem.FileCopy(strDownloadFolder + vsFinancial.TextMatrix(counter, FileCol), strDownloadArchiveFolder + vsFinancial.TextMatrix(counter, FileCol));//, true);
                    FCFileSystem.DeleteFile(strDownloadFolder + vsFinancial.TextMatrix(counter, FileCol));//, true);
				}
				vsFinancial.Rows = 0;
			}
			else
			{
				if (LiveFlag)
				{
					for (counter = 0; counter <= (vsDetail.Rows - 1); counter++)
					{
                        FCFileSystem.FileCopy(strDownloadFolder + vsDetail.TextMatrix(counter, FileCol), strDownloadArchiveFolder + vsDetail.TextMatrix(counter, FileCol));//, true);
                        FCFileSystem.DeleteFile(strDownloadFolder + vsDetail.TextMatrix(counter, FileCol));//, true);
					}
					vsDetail.Rows = 0;
				}
				else
				{
					for (counter = 0; counter <= (vsDetail.Rows - 1); counter++)
					{
						vsDetail.TextMatrix(counter, LocalCol, FCConvert.ToString(true));
					}
				}
			}
			cmbLoadData.Enabled = true;
			if (vsDetail.Visible == true)
			{
				vsDetail.Enabled = true;
			}
			else
			{
				vsFinancial.Enabled = true;
			}
			cmdDownload.Enabled = true;
			cmdProcessAll.Enabled = true;
		}

        private void FTP1_TransferProgress(object sender, Chilkat.ProgressInfoEventArgs e)
        {
            AddStatusMsg($"{e.Name}: {e.Value}");
        }

        private void FTP1_OnPercentDone(object sender, Chilkat.PercentDoneEventArgs args)
        {
            int percentDone = args.PercentDone;

			if (fecherFoundation.Strings.UCase(Strings.Right(currentFileName, 3)) == "DAT")
            {
                frmWait.InstancePtr.prgProgress.Value = percentDone;
            }
            else
            {
                ProgressBar1.Value = percentDone;
                FCUtils.ApplicationUpdate(ProgressBar1);
            }
		}

        private void VerifyFTPConnection()
        {
			if (!FTP1.IsConnected)
				ConnectToFTPAndSetDirectory();
        }

        private void ConnectToFTPAndSetDirectory()
        {
            FTP1.Hostname = "www1.maine.gov";
            FTP1.Username = "xxtrio";
            FTP1.Password = "303.harris";
            FTP1.Port = 21;
            FTP1.Connect();
            FTP1.ChangeRemoteDir("RapidRenewal");
            FTP1.ChangeRemoteDir(strResCode);
		}

		private bool DLFile(string FileName, string downloadpath, string downloadas)
		{
            if (FCFileSystem.Dir(downloadpath + "\\" + downloadas, 0) != "")
            {
                FCFileSystem.Kill(downloadpath + "\\" + downloadas);
            }

            ProgressBar1.Value = 0;
            ProgressBar1.Maximum = 100;

            FTP1.AutoGetSizeForProgress = true;

			// Download the file.
			VerifyFTPConnection();
			var success = FTP1.GetFile(FileName, Path.Combine(downloadpath, downloadas));
			if (success != true)
            {
                AddStatusMsg(FTP1.LastErrorText);
            }
            else
            {
				if (!StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
				{
					VerifyFTPConnection();
					FTP1.DeleteRemoteFile(FileName);
				}
            }

            return success;
        }

		private void AddStatusMsg(string Message)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lstMessages.AddItem(Message);
				lstMessages.SetSelected(lstMessages.ListCount - 1, true);
				lstMessages.Refresh();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddStatusMsg line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdDownload_Click(object sender, System.EventArgs e)
        {
            bool success = true;
            FCGrid grid;

			try
			{
				fecherFoundation.Information.Err().Clear();
				clsPostInfo = new clsBudgetaryPosting();
				clsJournalInfo = new clsPostingJournalInfo();


                if (vsDetail.Visible)
                {
                    grid = vsDetail;
                }
                else
                {
                    grid = vsFinancial;
                }

                if (grid.Rows == 0)
				{
					return;
				}

				cmbLoadData.Enabled = false;
				grid.Enabled = false;
				cmdDownload.Enabled = false;
				cmdProcessAll.Enabled = false;

                if (FCConvert.CBool(grid.TextMatrix(grid.Row, LocalCol)) != true)
                {
                    success = DLFile(grid.TextMatrix(grid.Row, FileCol), strDownloadFolder, grid.TextMatrix(grid.Row, FileCol));
                    if (success)
                    {
                        grid.SetCellText(LocalCol, grid.Row, FCConvert.ToString(true));
                    }
                }

				if (success)
				{
					ProcessFile(grid.TextMatrix(grid.Row, FileCol));
                    ProgressBar1.Value = 0;
                    MessageBox.Show("Process Complete", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
                    MessageBox.Show("One or more files did not update successfully" + "\r\n" + "Please try again", "Update not successful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				
				clsPostInfo = null;
				clsJournalInfo = null;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In cmdDownload " + "line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				clsPostInfo = null;
				clsJournalInfo = null;
				StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		private void optLoadData_CheckedChanged(object sender, System.EventArgs e)
		{
			fraFileOptions.Visible = false;
			fraRange.Visible = true;
			SetDefaultMonthRange();
		}

		private void optProcessDetail_CheckedChanged(object sender, System.EventArgs e)
		{
			fraRange.Visible = false;
			vsFinancial.Visible = false;
			vsDetail.Visible = true;
            vsDetail.BringToFront();
			fraFileOptions.Visible = true;
		}

		private void optProcessFinancial_CheckedChanged(object sender, System.EventArgs e)
		{
			fraRange.Visible = false;
			vsDetail.Visible = false;
			vsFinancial.Visible = true;
            vsFinancial.BringToFront();
			fraFileOptions.Visible = true;
		}

		public void cmbLoadData_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbLoadData.Text == "Create Rapid Renewal Data File")
			{
				optLoadData_CheckedChanged(sender, e);
			}
			else if (cmbLoadData.Text == "Process Detail File")
			{
				optProcessDetail_CheckedChanged(sender, e);
			}
			else if (cmbLoadData.Text == "Process Financial File")
			{
				optProcessFinancial_CheckedChanged(sender, e);
			}
		}
	}
}
