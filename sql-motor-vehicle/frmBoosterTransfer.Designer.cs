//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmBoosterTransfer.
	/// </summary>
	partial class frmBoosterTransfer
	{
		public FCGrid vsPermits;
		public fecherFoundation.FCLabel lblFeeAmount;
		public fecherFoundation.FCLabel lblFee;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsPermits = new fecherFoundation.FCGrid();
            this.lblFeeAmount = new fecherFoundation.FCLabel();
            this.lblFee = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPermits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 357);
            this.BottomPanel.Size = new System.Drawing.Size(602, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsPermits);
            this.ClientArea.Controls.Add(this.lblFeeAmount);
            this.ClientArea.Controls.Add(this.lblFee);
            this.ClientArea.Size = new System.Drawing.Size(602, 297);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(602, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(224, 30);
            this.HeaderText.Text = "Transfer Booster(s)";
            // 
            // vsPermits
            // 
            this.vsPermits.AllowSelection = false;
            this.vsPermits.AllowUserToResizeColumns = false;
            this.vsPermits.AllowUserToResizeRows = false;
            this.vsPermits.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsPermits.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsPermits.BackColorBkg = System.Drawing.Color.Empty;
            this.vsPermits.BackColorFixed = System.Drawing.Color.Empty;
            this.vsPermits.BackColorSel = System.Drawing.Color.Empty;
            this.vsPermits.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsPermits.Cols = 6;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsPermits.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsPermits.ColumnHeadersHeight = 30;
            this.vsPermits.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsPermits.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsPermits.DragIcon = null;
            this.vsPermits.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsPermits.ExtendLastCol = true;
            this.vsPermits.FixedCols = 0;
            this.vsPermits.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsPermits.FrozenCols = 0;
            this.vsPermits.GridColor = System.Drawing.Color.Empty;
            this.vsPermits.GridColorFixed = System.Drawing.Color.Empty;
            this.vsPermits.Location = new System.Drawing.Point(30, 65);
            this.vsPermits.Name = "vsPermits";
            this.vsPermits.OutlineCol = 0;
            this.vsPermits.ReadOnly = true;
            this.vsPermits.RowHeadersVisible = false;
            this.vsPermits.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsPermits.RowHeightMin = 0;
            this.vsPermits.Rows = 1;
            this.vsPermits.ScrollTipText = null;
            this.vsPermits.ShowColumnVisibilityMenu = false;
            this.vsPermits.ShowFocusCell = false;
            this.vsPermits.Size = new System.Drawing.Size(544, 220);
            this.vsPermits.StandardTab = true;
            this.vsPermits.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsPermits.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsPermits.TabIndex = 2;
            this.vsPermits.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.vsPermits_BeforeRowColChange);
            this.vsPermits.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsPermits_ValidateEdit);
            this.vsPermits.CurrentCellChanged += new System.EventHandler(this.vsPermits_RowColChange);
            this.vsPermits.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPermits_KeyDownEvent);
            this.vsPermits.Click += new System.EventHandler(this.vsPermits_ClickEvent);
            // 
            // lblFeeAmount
            // 
            this.lblFeeAmount.Location = new System.Drawing.Point(95, 30);
            this.lblFeeAmount.Name = "lblFeeAmount";
            this.lblFeeAmount.Size = new System.Drawing.Size(66, 15);
            this.lblFeeAmount.TabIndex = 1;
            this.lblFeeAmount.Text = "$0.00";
            // 
            // lblFee
            // 
            this.lblFee.Location = new System.Drawing.Point(30, 30);
            this.lblFee.Name = "lblFee";
            this.lblFee.Size = new System.Drawing.Size(30, 15);
            this.lblFee.TabIndex = 0;
            this.lblFee.Text = "FEE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(250, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(104, 48);
            this.cmdProcessSave.TabIndex = 0;
            this.cmdProcessSave.Text = "Process";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmBoosterTransfer
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(602, 465);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBoosterTransfer";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Transfer Booster(s)";
            this.Load += new System.EventHandler(this.frmBoosterTransfer_Load);
            this.Activated += new System.EventHandler(this.frmBoosterTransfer_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBoosterTransfer_KeyPress);
            this.Resize += new System.EventHandler(this.frmBoosterTransfer_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPermits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessSave;
	}
}