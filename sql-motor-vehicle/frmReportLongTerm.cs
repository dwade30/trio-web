//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmReportLongTerm : BaseForm
	{
		public frmReportLongTerm()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReportLongTerm InstancePtr
		{
			get
			{
				return (frmReportLongTerm)Sys.GetInstance(typeof(frmReportLongTerm));
			}
		}

		protected frmReportLongTerm _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsPC1 = new clsDRWrapper();
		DateTime DateTime;
		string strResCode = "";
		clsDRWrapper rs = new clsDRWrapper();
		public string ReportPrinter = "";
		int intCopies;

		private void cmdCloseout_Click(object sender, System.EventArgs e)
		{
			int lngID = 0;
			// vbPorter upgrade warning: GetOut As object	OnWrite(DialogResult)
			DialogResult GetOut;
			clsDRWrapper rsUser = new clsDRWrapper();
			clsDRWrapper rsTellers = new clsDRWrapper();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(Conversion.Val(rsUser.Get_Fields_String("Level"))) > 2)
				{
					MessageBox.Show("You do not have a high enough Operator Level to Close out a Period.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to Close out a Period.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			GetOut = MessageBox.Show("Please be sure that all users are out of the Motor Vehicle System before Closing Out Period.", "Everyone Out of Motor Vehicle", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
			if (GetOut == DialogResult.Cancel)
			{
				MessageBox.Show("Process was stopped by you.", "Process Not Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				rsUser.Execute("UPDATE Inventory SET Pending = 0 WHERE Pending = 1", "TWMV0000.vb1");
			}
			fecherFoundation.Information.Err().Clear();
			bool executePeriodEndError = false;
			try
			{
				// On Error GoTo PeriodEndError
				fecherFoundation.Information.Err().Clear();
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Closing Period";
				frmWait.InstancePtr.Refresh();
				//App.DoEvents();
				FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
				// Set Closeout Key by adding a new record in table
				rsPC1.OpenRecordset("SELECT * FROM PeriodCloseoutLongTerm");
				DateTime = DateTime.Now;
				rsPC1.AddNew();
				rsPC1.Set_Fields("OpID", MotorVehicle.Statics.OpID);
				rsPC1.Set_Fields("IssueDate", DateTime);
				cboStart.AddItem(DateTime.ToString());
				cboEnd.AddItem(DateTime.ToString());
				cboStart.Refresh();
				cboEnd.Refresh();
				cboStart.SelectedIndex = 0;
				cboEnd.SelectedIndex = cboEnd.Items.Count - 1;
				rsPC1.Update();
				lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
				// Set Period CloseoutID for ActivityMaster
				MotorVehicle.Statics.strSql = "SELECT * FROM LongTermTrailerRegistrations WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				MotorVehicle.Statics.strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutid < 1 AND AdjustmentCode <> 'P' AND InventoryType = 'PXSLT'";
				if (SetCloseoutID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				// Set Period CloseoutKey for TitleApplications
				MotorVehicle.Statics.strSql = "SELECT * FROM LongTermTitleApplications WHERE PeriodCloseoutid < 1";
				if (SetCloseoutID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				MotorVehicle.Statics.strSql = "SELECT * FROM CloseoutInventory";
				if (PerformInventoryCloseout(ref MotorVehicle.Statics.strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				rsPC1.Reset();
				Set_Newest_Date_In_CBO();
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = 0;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				executePeriodEndError = true;
				goto PeriodEndError;
			}
			PeriodEndError:
			if (executePeriodEndError)
			{
				string strErrMessage;
				// 05/16/2008 CJG
				// err gets cleared when you run the following code before showing the error message, so save the message
				strErrMessage = "Period Closeout was not successful.  Error number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + " was encountered.  (" + fecherFoundation.Information.Err().Description + ")";
				rsPC1.FindFirstRecord("ID", lngID);
				rsPC1.Delete();
				rsPC1.Update();
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = 0;
				MessageBox.Show(strErrMessage, "Error processing closeout.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				fecherFoundation.Information.Err().Clear();
				executePeriodEndError = false;
			}
		}

		public bool PerformInventoryCloseout(ref string str1, ref int lng1)
		{
			bool PerformInventoryCloseout = false;
			PerformInventoryCloseout = false;
			int lngHoldNumber = 0;
			int lngHoldHigh = 0;
			int lngNewNumber = 0;
			clsDRWrapper rsIOH = new clsDRWrapper();
			clsDRWrapper rs1 = new clsDRWrapper();
			clsDRWrapper rs2 = new clsDRWrapper();
			string strHoldType = "";
			int lngCount;
			string strCode = "";
			string strOriginalPrefix = "";
			string strStartPlate = "";
			rs1.OpenRecordset(str1);
			rsIOH.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout");
			// Plate's
			string strprefix = "";
			string strSuffix = "";
			rs2.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'PXSLT' And (Status = 'A' OR Status = 'H' OR Status = 'P') ORDER BY substring(Code,4,2), Prefix + convert(nvarchar, Number) + Suffix");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = FCConvert.ToString(rs2.Get_Fields("Code"));
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "")
					strprefix = FCConvert.ToString(rs2.Get_Fields_String("prefix"));
				else
					strprefix = "";
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
					strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
				else
					strSuffix = "";
				strOriginalPrefix = strprefix;
				strStartPlate = FCConvert.ToString(rs2.Get_Fields_String("FormattedInventory"));
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (strprefix.Length > FCConvert.ToString(rs2.Get_Fields_String("prefix")).Length && Strings.Right(strprefix, 1) == "0")
					{
						strprefix = Strings.Left(strprefix, (strprefix.Length - (strprefix.Length - FCConvert.ToString(rs2.Get_Fields_String("prefix")).Length)));
					}
					if ((fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "" && strprefix != FCConvert.ToString(rs2.Get_Fields_String("prefix"))) || (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "" && strSuffix != FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) || (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"))))
					{
						rs2.MovePrevious();
						rsIOH.AddNew();
						rsIOH.Set_Fields("Type", strCode);
						if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
						{
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "")
							strprefix = FCConvert.ToString(rs2.Get_Fields_String("prefix"));
						else
							strprefix = "";
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
							strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
						else
							strSuffix = "";
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", strStartPlate);
						// strOriginalPrefix & lngHoldNumber & strSuffix
						rs1.Set_Fields("High", rs2.Get_Fields_String("FormattedInventory"));
						// strPrefix & lngNewNumber - 1 & strSuffix
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						rs2.MoveNext();
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "")
							strprefix = FCConvert.ToString(rs2.Get_Fields_String("prefix"));
						else
							strprefix = "";
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
							strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
						else
							strSuffix = "";
						strOriginalPrefix = strprefix;
						strStartPlate = FCConvert.ToString(rs2.Get_Fields_String("FormattedInventory"));
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						strCode = FCConvert.ToString(rs2.Get_Fields("Code"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirst("PeriodCloseoutID = " + FCConvert.ToString(lng1) + " AND Type = '" + rs2.Get_Fields("Code") + "'");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
					{
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", strStartPlate);
					// strOriginalPrefix & lngHoldNumber & strSuffix
					rs1.Set_Fields("High", rs2.Get_Fields_String("FormattedInventory"));
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// 
			PerformInventoryCloseout = true;
			//App.DoEvents();
			// rs1.Close
			// RS2.Close
			rs1.Reset();
			rs2.Reset();
			return PerformInventoryCloseout;
		}

		private bool SetCloseoutID(ref string str1, ref int lng1)
		{
			bool SetCloseoutID = false;
			SetCloseoutID = false;
			clsDRWrapper rs1 = new clsDRWrapper();
			rs1.OpenRecordset(str1);
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				while (!rs1.EndOfFile())
				{
					rs1.Edit();
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
					rs1.MoveNext();
				}
				SetCloseoutID = true;
			}
			else
			{
				SetCloseoutID = true;
			}
			return SetCloseoutID;
		}

		private void cmdCloseoutPrint_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.TableName = "Long Term Period Closeout";
			modDuplexPrinting.DuplexPrintReport(rptTwoColumnTable.InstancePtr);
			rptTwoColumnTable.InstancePtr.Unload();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			FCFileSystem a;
			string strDirectory = "";
			string strReportDrive = "";
			
				if (cmbInterim.Text == "Live Reports")
				{
					if (cboStart.SelectedIndex < 0 || cboEnd.SelectedIndex < 0)
					{
						MessageBox.Show("You must select a valid date range before you may continue.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else if (fecherFoundation.DateAndTime.DateValue(cboStart.Text).ToOADate() >= fecherFoundation.DateAndTime.DateValue(cboEnd.Text).ToOADate())
					{
						MessageBox.Show("You must select a valid date range before you may continue.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				if (cmbReport.Text == "Create Data File")
				{
					frmReport.InstancePtr.MousePointer = MousePointerConstants.vbHourglass;
					CreateDiskFile();
					frmReport.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
				if (cmbReport.Text == "Upload Data File To BMV")
				{
					string FileName = "";
                    FCFileSystem ff = new FCFileSystem();
					FileName = strResCode + "LTIN.DAT";
					if (!FCFileSystem.FileExists(FileName))
					{
						MessageBox.Show("Unable to find the data file to send.", "File Not Found ", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						ULFile(ref FileName);
					}
					return;
				}
				if (cmbReport.Text == "Full Set Of Reports")
				{
					ReportPrinter = FCGlobal.Printer.DeviceName;
					PrintRoutines();
				}
				else
				{
					PrintRoutines();
				}
			
            frmReport.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
        }

		public void PrintRoutines()
		{
			clsDRWrapper rsDel = new clsDRWrapper();
			bool AllFlag;
			try
			{
				// On Error GoTo ErrTag
				fecherFoundation.Information.Err().Clear();
				frmReport.InstancePtr.MousePointer = MousePointerConstants.vbHourglass;
				if (cmbReport.Text == "Full Set Of Reports")
				{
					AllFlag = true;
					rptCoverSheet.InstancePtr.Init(Convert.ToDateTime(cboStart.Text), Convert.ToDateTime(cboEnd.Text), true);
					rptCoverSheet.InstancePtr.Unload();
					rptLTTAgentSummary.InstancePtr.Unload();
					// kk11142016 tromv-1233
					rptLTTAgentSummary.InstancePtr.PrintReport(false);
					rptLTTAgentDetail.InstancePtr.Unload();
					// kk11142016 tromv-1233
					rptLTTAgentDetail.InstancePtr.PrintReport(false);
					rptLTTAgentInventory.InstancePtr.Unload();
					// kk11142016 tromv-1233
					rptLTTAgentInventory.InstancePtr.PrintReport(false);
					rptLTTPlateInventory.InstancePtr.Unload();
					// kk11142016 tromv-1233
					rptLTTPlateInventory.InstancePtr.PrintReport(false);
					AllFlag = false;
					frmReport.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
				if (cmbReport.Text == "LTT Agent Summary")
				{
					rptLTTAgentSummary.InstancePtr.Unload();
					// kk11142016 tromv-1233
					frmReportViewer.InstancePtr.Init(rptLTTAgentSummary.InstancePtr);
					//rptLTTAgentSummary.InstancePtr.Show(App.MainForm);
				}
				else if (cmbReport.Text == "LTT Agent Detail")
				{
					rptLTTAgentDetail.InstancePtr.Unload();
					// kk11142016 tromv-1233
					frmReportViewer.InstancePtr.Init(rptLTTAgentDetail.InstancePtr);
					//rptLTTAgentDetail.InstancePtr.Show(App.MainForm);
				}
				else if (cmbReport.Text == "LTT Agent Inventory")
				{
					rptLTTAgentInventory.InstancePtr.Unload();
					// kk11142016 tromv-1233
					frmReportViewer.InstancePtr.Init(rptLTTAgentInventory.InstancePtr);
					//rptLTTAgentInventory.InstancePtr.Show(App.MainForm);
				}
				else if (cmbReport.Text == "LTT Plate Inventory")
				{
					rptLTTPlateInventory.InstancePtr.Unload();
					// kk11142016 tromv-1233
					frmReportViewer.InstancePtr.Init(rptLTTPlateInventory.InstancePtr);
					//rptLTTPlateInventory.InstancePtr.Show(App.MainForm);
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
            frmReport.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
        }

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdSaveDates_Click(object sender, System.EventArgs e)
		{
			int lngStart = 0;
			int lngEnd = 0;
			rs.OpenRecordset("SELECT * FROM PeriodCloseoutLongTerm ORDER BY IssueDate DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				// Start
				rs.FindFirstRecord("IssueDate", cboStart.Text);
				if (rs.NoMatch == false)
					lngStart = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				// End
				rs.FindFirstRecord("IssueDate", cboEnd.Text);
				if (rs.NoMatch == false)
					lngEnd = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				// Write to WMV
				MotorVehicle.UpdateMVVariable("LongTermReportStartIndex", modGlobalRoutines.PadToString(lngStart, 4));
				MotorVehicle.UpdateMVVariable("LongTermReportEndIndex", modGlobalRoutines.PadToString(lngEnd, 4));
			}
			//App.DoEvents();
			// rs.Close
			rs.Reset();
			MessageBox.Show("Dates saved successfully!", "Info Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void frmReportLongTerm_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		public void GetResCode()
		{
			clsDRWrapper rsResCode = new clsDRWrapper();
			rsResCode.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
			{
				rsResCode.MoveLast();
				rsResCode.MoveFirst();
				strResCode = fecherFoundation.Strings.Trim(FCConvert.ToString(rsResCode.Get_Fields_String("ResidenceCode")));
			}
			//App.DoEvents();
			// rsResCode.Close
			rsResCode.Reset();
		}

		private void frmReportLongTerm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReportLongTerm properties;
			//frmReportLongTerm.FillStyle	= 0;
			//frmReportLongTerm.ScaleWidth	= 9045;
			//frmReportLongTerm.ScaleHeight	= 6930;
			//frmReportLongTerm.LinkTopic	= "Form2";
			//frmReportLongTerm.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int intStart = 0;
			int intEnd = 0;
			bool bolStart = false;
			bool bolEnd = false;
            FCFileSystem ff = new FCFileSystem();
			GetResCode();
			rs.OpenRecordset("SELECT * FROM PeriodCloseoutLongTerm ORDER BY ID DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				if (Conversion.Val(MotorVehicle.GetMVVariable("LongTermReportStartIndex")) != 0)
					intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(MotorVehicle.GetMVVariable("LongTermReportStartIndex"))));
				if (Conversion.Val(MotorVehicle.GetMVVariable("LongTermReportEndIndex")) != 0)
					intEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(MotorVehicle.GetMVVariable("LongTermReportEndIndex"))));
				while (!rs.EndOfFile())
				{
					cboStart.AddItem(FCConvert.ToString(rs.Get_Fields_DateTime("IssueDate")));
					cboEnd.AddItem(FCConvert.ToString(rs.Get_Fields_DateTime("IssueDate")));
					if (bolStart == false)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("ID")) == intStart)
						{
							intStart = cboStart.Items.Count - 1;
							bolStart = true;
						}
					}
					if (bolEnd == false)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("ID")) == intEnd)
						{
							intEnd = cboEnd.Items.Count - 1;
							bolEnd = true;
						}
					}
					rs.MoveNext();
				}
				if (intStart > cboStart.Items.Count - 1)
				{
					// Do Nothing
				}
				else
				{
					if (bolStart == true)
					{
						cboStart.SelectedIndex = intStart;
					}
					else
					{
						if (cboStart.Items.Count > 1)
						{
							cboStart.SelectedIndex = 1;
						}
						else if (cboStart.Items.Count > 0)
						{
							cboStart.SelectedIndex = 0;
						}
					}
				}
				if (intEnd > cboEnd.Items.Count - 1)
				{
					// Do Nothing
				}
				else
				{
					if (bolEnd == true)
					{
						cboEnd.SelectedIndex = intEnd;
					}
					else
					{
						cboEnd.SelectedIndex = 0;
					}
				}
			}
			//App.DoEvents();
			rs.Reset();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmReportLongTerm_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optInterim_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				fraClose.Enabled = false;
				fraSetDates.Enabled = false;
				cboEnd.Enabled = false;
				cboStart.Enabled = false;
				cmdSaveDates.Enabled = false;
				Label1.Enabled = false;
				Label2.Enabled = false;
				cmdCloseout.Enabled = false;
				if (cmbReport.Items.Contains("Full Set Of Reports"))
				{
					cmbReport.Items.Remove("Full Set Of Reports");
				}
				if (cmbReport.Items.Contains("Create Data File"))
				{
					cmbReport.Items.Remove("Create Data File");
				}
				if (cmbReport.Items.Contains("LTT Plate Inventory"))
				{
					cmbReport.Items.Remove("LTT Plate Inventory");
				}
			}
			else
			{
				fraClose.Enabled = true;
				fraSetDates.Enabled = true;
				cboEnd.Enabled = true;
				cboStart.Enabled = true;
				cmdSaveDates.Enabled = true;
				Label1.Enabled = true;
				Label2.Enabled = true;
				cmdCloseout.Enabled = true;
				if (!cmbReport.Items.Contains("Full Set Of Reports"))
				{
					cmbReport.Items.Insert(0, "Full Set Of Reports");
				}
				if (cmbReport.Items.Contains("Create Data File"))
				{
					cmbReport.Items.Insert(4, "Create Data File");
				}
				if (cmbReport.Items.Contains("LTT Plate Inventory"))
				{
					cmbReport.Items.Insert(5, "LTT Plate Inventory");
				}
			}
		}

		private void optInterim_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbInterim.SelectedIndex;
			optInterim_CheckedChanged(index, sender, e);
		}

		private void Set_Newest_Date_In_CBO()
		{
			int jj;
			// Set Start
			for (jj = 0; jj <= cboStart.Items.Count - 1; jj++)
			{
				if (cboStart.Items[jj].ToString() == DateTime.ToOADate().ToString())
				{
					cboStart.SelectedIndex = jj - 1;
					break;
				}
			}
			// jj
			if (cboStart.SelectedIndex == -1)
			{
				cboStart.SelectedIndex = 0;
			}
			if (fecherFoundation.DateAndTime.DateValue(cboStart.Items[cboStart.SelectedIndex].ToString()).ToOADate() > fecherFoundation.DateAndTime.DateValue(cboStart.Items[0].ToString()).ToOADate())
			{
				// do nothing
			}
			else
			{
				cboStart.SelectedIndex = 0;
			}
			// Set Ending
			for (jj = 0; jj <= cboEnd.Items.Count - 1; jj++)
			{
				if (cboEnd.Items[jj].ToString() == DateTime.ToOADate().ToString())
				{
					cboEnd.SelectedIndex = jj;
					break;
				}
			}
			// jj
		}

		private object ULFile(ref string FileName)
		{
			object ULFile = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Chilkat.SFtp sftp = new Chilkat.SFtp();
				clsDRWrapper rsGeneral = new clsDRWrapper();
				rsGeneral.OpenRecordset("SELECT * FROM WMV");
				if (rsGeneral.EndOfFile() != true && rsGeneral.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsGeneral.Get_Fields_String("LongTermFTPAddress")) == "" || FCConvert.ToString(rsGeneral.Get_Fields_String("LongTermFTPPassword")) == "" || FCConvert.ToString(rsGeneral.Get_Fields_String("LongTermFTPUser")) == "")
					{
						MessageBox.Show("Missing FTP Information", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return ULFile;
					}
				}
				else
				{
					MessageBox.Show("Missing FTP Information", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return ULFile;
				}
				// Any string automatically begins a fully-functional 30-day trial.
				bool success;
				success = sftp.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
    //            if (success != true)
				//{
				//	MessageBox.Show(sftp.LastErrorText);
				//	return ULFile;
				//}
				// Set some timeouts, in milliseconds:
				sftp.ConnectTimeoutMs = 15000;
				sftp.IdleTimeoutMs = 15000;
				// Connect to the SSH server.
				// The standard SSH port = 22
				// The hostname may be a hostname or IP address.
				int port;
				string hostname;
				hostname = FCConvert.ToString(rsGeneral.Get_Fields_String("LongTermFTPAddress"));
				// "ftps.maine.gov"
				port = 22;
				success = sftp.Connect(hostname, port);
				if (success != true)
				{
					MessageBox.Show(sftp.LastErrorText);
					return ULFile;
				}
				// Authenticate with the SSH server.  Chilkat SFTP supports
				// both password-based authenication as well as public-key
				// authentication.  This example uses password authenication.
				success = sftp.AuthenticatePw(rsGeneral.Get_Fields_String("LongTermFTPUser"), rsGeneral.Get_Fields_String("LongTermFTPPassword"));
				if (success != true)
				{
					MessageBox.Show(sftp.LastErrorText);
					return ULFile;
				}
				// After authenticating, the SFTP subsystem must be initialized:
				success = sftp.InitializeSftp();
				if (success != true)
				{
					MessageBox.Show(sftp.LastErrorText);
					return ULFile;
				}
				// Upload from the local file to the SSH server.
				// Important -- the remote filepath is the 1st argument,
				// the local filepath is the 2nd argument;
				string remoteFilePath;
				remoteFilePath = "/BMVTrailerRegistrationSubmissions/" + FileName;
				string localFilePath;
				localFilePath = FileName;
				success = sftp.UploadFileByName(remoteFilePath, localFilePath);
				if (success != true)
				{
					MessageBox.Show(sftp.LastErrorText);
					return ULFile;
				}
				MessageBox.Show("File uploaded successfully.", "File Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return ULFile;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ULFile " + "line " + fecherFoundation.Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ULFile;
		}

		public bool CreateDiskFile()
		{
			bool CreateDiskFile = false;
			clsDRWrapper rsDiskReport = new clsDRWrapper();
			FCFileSystem ff = new FCFileSystem();
			clsDRWrapper rs1 = new clsDRWrapper();
			DateTime dat1;
			DateTime dat2;
			int lngPK1;
			int lngPK2;
			string LocalFile;
			string strReportDrive;
			int counter;
			StreamWriter tstData;
			string strTranType = "";
			clsDRWrapper rsFleet = new clsDRWrapper();
            var strLine = "";

			dat1 = FCConvert.ToDateTime(frmReport.InstancePtr.cboStart.Text);
			dat2 = FCConvert.ToDateTime(frmReport.InstancePtr.cboEnd.Text);
			MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(MotorVehicle.Statics.strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (!rs.EndOfFile())
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			// 
			// MsgBox "3"
			if (cmbInterim.Text == "Live Reports")
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM LongTermTrailerRegistrations WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND RegistrationType <> 'DPS' ORDER BY DateUpdated";
				// Removed Requested by BMV 4/21/05  AND TransactionType <> 'LPS'
			}
			else
			{
				return CreateDiskFile;
			}
			rsFleet.OpenRecordset("SELECT * FROM FleetMaster WHERE IsNull(CompanyCode, '')  = '' AND FleetNumber IN (SELECT DISTINCT FleetNumber FROM LongTermTrailerRegistrations WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND RegistrationType <> 'DPS')", "TWMV0000.vb1");
			if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
			{
				MotorVehicle.Statics.strSql = "";
				do
				{
					MotorVehicle.Statics.strSql += rsFleet.Get_Fields_Int32("FleetNumber") + ", ";
					rsFleet.MoveNext();
				}
				while (rsFleet.EndOfFile() != true);
				MotorVehicle.Statics.strSql = Strings.Left(MotorVehicle.Statics.strSql, MotorVehicle.Statics.strSql.Length - 2);
				MessageBox.Show("The following groups must have a company code entered for them before you may create a file for the state." + "\r\n" + "\r\n" + MotorVehicle.Statics.strSql, "Invalid Company Codes", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return CreateDiskFile;
			}


            var fileFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "BMVReports");

            if (!Directory.Exists(fileFolder))
            {
                Directory.CreateDirectory(fileFolder);
            }
			string FileName;
			//FileName = strResCode + "LTIN.DAT";
            FileName = Path.Combine("BMVReports", strResCode + "LTIN.DAT");
			rsDiskReport.OpenRecordset(MotorVehicle.Statics.strSql);

			if (!rsDiskReport.EndOfFile())
			{
				rsDiskReport.MoveLast();
				rsDiskReport.MoveFirst();
				FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
				if (FCFileSystem.FileExists(FileName))
				{
                    FCFileSystem.DeleteFile(FileName);//, true);
				}
                tstData = FCFileSystem.CreateTextFile(FileName);//, true, false);
				while (!rsDiskReport.EndOfFile())
                {
                    strLine = "";
					if (FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "DPR")
					{
						strTranType = "D";
					}
					else if (FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "RRT" || FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "NRT")
					{
						strTranType = "T";
					}
					else if (FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "ECO" || FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "ECR")
					{
						strTranType = "C";
					}
					else if (rsDiskReport.Get_Fields_String("RegistrationType") == "LPS")
					{
						strTranType = "L";
					}
					else if (rsDiskReport.Get_Fields_String("RegistrationType") == "NRR")
					{
						strTranType = "N";
					}
					else
					{
						if (rsDiskReport.Get_Fields("Extension") == true)
						{
							strTranType = "E";
						}
						else
						{
							strTranType = "R";
						}
					}

                    strLine = rsDiskReport.Get_Fields_String("Plate").Replace("-", "") + "|";
                    strLine += strTranType + "|";
					strLine += (rsDiskReport.Get_Fields_DateTime("ExpireDate").Year - rsDiskReport.Get_Fields_Int32("NumberOfYears")).ToString() + "|";
					strLine += Strings.Format(rsDiskReport.Get_Fields_DateTime("ExpireDate"), "yyyymmdd") + "|";
					strLine += rsDiskReport.Get_Fields_String("Make") + "|";
					strLine += rsDiskReport.Get_Fields_Int32("Year").ToString() + "|";
					strLine += rsDiskReport.Get_Fields_String("Style") + "|";
					if (FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "LPS")
					{
						strLine += rsDiskReport.Get_Fields_Double("AgentFee").FormatAsMoney() + "|";
					}
					else if (rsDiskReport.Get_Fields_String("RegistrationType") == "DPR")
					{
						strLine += "2.00" + "|";
					}
					else if (FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "RRT" || FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "NRT")
					{
						strLine +=rsDiskReport.Get_Fields_Double("TransferFee").FormatAsMoney() + "|";
					}
					else if (rsDiskReport.Get_Fields_String("RegistrationType") == "ECO")
					{
						strLine += "0.00" + "|";
					}
					else
					{
						if (rsDiskReport.Get_Fields("RegistrationFee") - rsDiskReport.Get_Fields_Double("RegistrationCredit") >= 0)
						{
							strLine += (rsDiskReport.Get_Fields_Double("RegistrationFee") - rsDiskReport.Get_Fields_Double("RegistrationCredit")).FormatAsMoney() + "|";
						}
						else
						{
							strLine +="0.00|";
						}
					}
					strLine += rsDiskReport.Get_Fields("Unit") + "|";
					if (FCConvert.ToString(rsDiskReport.Get_Fields_String("RegistrationType")) == "NRR")
					{
						strLine += Strings.Format(rsDiskReport.Get_Fields_DateTime("DateUpdated"), "yyyymmdd") + "|";
					}
					else
					{
						strLine += Strings.Format(rsDiskReport.Get_Fields_DateTime("EffectiveDate"), "yyyymmdd") + "|";
					}
					strLine += Strings.Format(rsDiskReport.Get_Fields_DateTime("DateUpdated"), "yyyymmdd") + "|";
					rsFleet.OpenRecordset("SELECT * FROM FleetMaster WHERE FleetNumber = " + FCConvert.ToString(Conversion.Val(rsDiskReport.Get_Fields_Int32("FleetNumber"))), "TWMV0000.vb1");
					if (!rsFleet.EndOfFile())
					{
						strLine += rsFleet.Get_Fields_String("CompanyCode") + "|";
					}
					else
					{
						// tstData.Write "|"
						FCGlobal.Screen.MousePointer = 0;
						MessageBox.Show("Plate " + rsDiskReport.Get_Fields_String("Plate") + " is registered to grouop " + rsDiskReport.Get_Fields_Int32("FleetNumber") + " which we are unable to locate.  You must set up this group and have a company code entered for them before you may create a file for the state.", "Group Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return false;
					}
					strLine += rsDiskReport.Get_Fields_String("VIN") + "|";
					if (strTranType == "R" || strTranType == "E")
					{
						strLine += "Y|";
					}
					else
					{
						strLine += "N|";
					}
					strLine += rsDiskReport.Get_Fields_String("OldPlate") + "|";
					if (rsDiskReport.Get_Fields_Int32("NetWeight") == 2000)
					{
						strLine += "Y|";
					}
					else
					{
						strLine += "N|";
					}
					strLine += MotorVehicle.Statics.gstrLongTermCompanyCode + "|";
					if (rsDiskReport.Get_Fields_Boolean("TimePayment"))
					{
						strLine += "TIME|";
					}
					else
					{
						strLine += "FULL|";
					}
					if (rsDiskReport.Get_Fields_Int32("NumberOfYears") == 24)
					{
						strLine += "25";
					}
					else if (rsDiskReport.Get_Fields_Int32("NumberOfYears") > 12)
					{
						strLine += "20";
					}
					else
					{
						strLine += "12";
					}

                    if (rsDiskReport.Get_Fields_Boolean("Extension") &&
                        (rsDiskReport.Get_Fields_String("Plate") == rsDiskReport.Get_Fields_String("OldPlate") ||
                         rsDiskReport.Get_Fields_String("OldPlate") == ""))
                    {
                        strLine += "|" + rsDiskReport.Get_Fields_Int32("NumberOfYears").ToString();
                    }
                    tstData.WriteLine(strLine);
					rsDiskReport.MoveNext();
				}
                tstData.Close();
				FCGlobal.Screen.MousePointer = 0;
			}
            FCFileSystem a;
            a = new FCFileSystem();

			strReportDrive = Path.Combine(FCFileSystem.Statics.UserDataFolder,"BMVReports");

			if (!Directory.Exists(strReportDrive))
			{
				Directory.CreateDirectory(strReportDrive);
			}

			if (FCFileSystem.FileExists(FileName))
			{
                FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, FileName), FileName);
				MessageBox.Show(strReportDrive + FileName + " has been created.", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				CreateDiskFile = true;
			}
			else
			{
				//MessageBox.Show("Could not find file " + LocalFile + ".  Please check your reporting dates and try again.", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				MessageBox.Show("Could not find file " + FileName + ".  Please check your reporting dates and try again.", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				CreateDiskFile = false;
			}
			return CreateDiskFile;
		}

		private void SetCustomFormColors()
		{
			//cmbInterim.ForeColor = Color.Blue;
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 4 || Index == 6)
			{
				cmdProcessSave.Text = "Process";
			}
			else
			{
				cmdProcessSave.Text = "Print";
			}
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}
	}
}
