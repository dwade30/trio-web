//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReminderNotice.
	/// </summary>
	public partial class rptReminderNotice : BaseSectionReport
	{
		public rptReminderNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Reminder Notices";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptReminderNotice InstancePtr
		{
			get
			{
				return (rptReminderNotice)Sys.GetInstance(typeof(rptReminderNotice));
			}
		}

		protected rptReminderNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderNotice	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int counter;
		// kk05162017 tromv-1115  Change from Integer to Long
		public bool blnShowHeadings;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: Amount As double	OnWriteFCConvert.ToDecimal(
			double Amount = 0;
			Decimal amount1 = 0;
			Decimal amount2 = 0;
			Decimal amount3 = 0;
			if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			if (eArgs.EOF)
				return;
			//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
			//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
			{
				fldPlate.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
				fldYear.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4);
				fldMake.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5);
				fldModel.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6);
				fldExpires.Text = Conversion.Str(frmReminder.InstancePtr.cboMonth.SelectedIndex + 1) + "/" + frmReminder.InstancePtr.cboYear.Text;
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
				{
					amount1 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7));
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8) != "")
				{
					amount2 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8));
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9) != "")
				{
					amount3 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9));
				}
				if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
				{
					Amount = FCConvert.ToDouble(amount1 + amount2 + amount3);
					fldAmount.Text = Strings.Format(Amount, "$#,##0.00");
				}
				else
				{
					fldAmount.Text = "Call Office for Amount";
				}
				fldName.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
				if (!MotorVehicle.Statics.rsReport.FindFirstRecord("ID", Conversion.Val(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 0))))
				{
					// do nothing
				}
				else
				{
					if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsReport.Get_Fields_String("Address")))
					{
						fldAddress.Text = MotorVehicle.Statics.rsReport.Get_Fields_String("Address");
					}
					else
					{
						fldAddress.Text = "";
					}
					fldCityStateZip.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("Zip")));
				}
			}
			else
			{
				counter += 1;
				while (counter < frmReminder.InstancePtr.vsVehicles.Rows)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
					{
						fldPlate.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 3);
						fldYear.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 4);
						fldMake.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 5);
						fldModel.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 6);
						fldExpires.Text = Conversion.Str(frmReminder.InstancePtr.cboMonth.SelectedIndex + 1) + "/" + frmReminder.InstancePtr.cboYear.Text;
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
						{
							amount1 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7));
						}
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8) != "")
						{
							amount2 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 8));
						}
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9) != "")
						{
							amount3 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 9));
						}
						if (frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 7) != "")
						{
							Amount = FCConvert.ToDouble(amount1 + amount2 + amount3);
							fldAmount.Text = Strings.Format(Amount, "$#,##0.00");
						}
						else
						{
							fldAmount.Text = "Call Office for Amount";
						}
						fldName.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
						if (!MotorVehicle.Statics.rsReport.FindFirstRecord("ID", Conversion.Val(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 0))))
						{
							// do nothing
						}
						else
						{
							fldAddress.Text = MotorVehicle.Statics.rsReport.Get_Fields_String("Address");
							fldCityStateZip.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("Zip")));
						}
						break;
					}
					else
					{
						counter += 1;
					}
				}
				if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
				{
					eArgs.EOF = true;
					return;
				}
			}
			counter += 1;
			eArgs.EOF = false;
			//Application.DoEvents();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			counter = 1;
			//rptReminderNotice.InstancePtr.Printer.RenderMode = 1;
			//Application.DoEvents();
			Label2.Visible = blnShowHeadings;
			Label3.Visible = blnShowHeadings;
			Label4.Visible = blnShowHeadings;
			Label5.Visible = blnShowHeadings;
			Line1.Visible = blnShowHeadings;
			Line2.Visible = blnShowHeadings;
			fldMessage.Text = frmReminder.InstancePtr.txtMessage.Text;
		}

		private void rptReminderNotice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderNotice properties;
			//rptReminderNotice.Caption	= "Reminder Notices";
			//rptReminderNotice.Icon	= "rptReminderNotice.dsx":0000";
			//rptReminderNotice.Left	= 0;
			//rptReminderNotice.Top	= 0;
			//rptReminderNotice.Width	= 11880;
			//rptReminderNotice.Height	= 8595;
			//rptReminderNotice.StartUpPosition	= 3;
			//rptReminderNotice.SectionData	= "rptReminderNotice.dsx":058A;
			//End Unmaped Properties
		}
	}
}
