﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptVehicleAddressList.
	/// </summary>
	public partial class rptVehicleAddressList : BaseSectionReport
	{
		public rptVehicleAddressList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Vehicle Address List";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptVehicleAddressList InstancePtr
		{
			get
			{
				return (rptVehicleAddressList)Sys.GetInstance(typeof(rptVehicleAddressList));
			}
		}

		protected rptVehicleAddressList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVehicleAddressList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int lngRowCounter;
		int lngVehicleCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckNext = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (FCConvert.ToBoolean(frmVehicleAddressList.InstancePtr.vsResults.TextMatrix(lngRowCounter, 0)) == true)
				{
					lngVehicleCounter += 1;
					eArgs.EOF = false;
				}
				else
				{
					executeCheckNext = true;
					goto CheckNext;
				}
			}
			else
			{
				executeCheckNext = true;
				goto CheckNext;
			}
			CheckNext:
			;
			if (executeCheckNext)
			{
				lngRowCounter += 1;
				if (lngRowCounter <= frmVehicleAddressList.InstancePtr.vsResults.Rows - 1)
				{
					if (FCConvert.ToBoolean(frmVehicleAddressList.InstancePtr.vsResults.TextMatrix(lngRowCounter, 0)) == true)
					{
						lngVehicleCounter += 1;
						eArgs.EOF = false;
					}
					else
					{
						goto CheckNext;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
				executeCheckNext = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label35 As object	OnWrite(string)
			// vbPorter upgrade warning: Label34 As object	OnWrite(string)
			// vbPorter upgrade warning: lblStreet As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			lngRowCounter = 1;
			lngVehicleCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (fecherFoundation.Strings.Trim(frmVehicleAddressList.InstancePtr.txtLow.Text) == "" && fecherFoundation.Strings.Trim(frmVehicleAddressList.InstancePtr.txtHigh.Text) == "")
			{
				lblStreet.Text = fecherFoundation.Strings.Trim(frmVehicleAddressList.InstancePtr.txtStreetName.Text);
			}
			else
			{
				lblStreet.Text = fecherFoundation.Strings.Trim(frmVehicleAddressList.InstancePtr.txtLow.Text) + " To " + fecherFoundation.Strings.Trim(frmVehicleAddressList.InstancePtr.txtHigh.Text) + " " + fecherFoundation.Strings.Trim(frmVehicleAddressList.InstancePtr.txtStreetName.Text);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldClass As object	OnWrite(string)
			// vbPorter upgrade warning: fldPlate As object	OnWrite(string)
			// vbPorter upgrade warning: fldOwner As object	OnWrite(string)
			// vbPorter upgrade warning: fldAddress As object	OnWrite(string)
			fldClass.Text = frmVehicleAddressList.InstancePtr.vsResults.TextMatrix(lngRowCounter, 1);
			fldPlate.Text = frmVehicleAddressList.InstancePtr.vsResults.TextMatrix(lngRowCounter, 2);
			fldOwner.Text = frmVehicleAddressList.InstancePtr.vsResults.TextMatrix(lngRowCounter, 3);
			fldAddress.Text = frmVehicleAddressList.InstancePtr.vsResults.TextMatrix(lngRowCounter, 4);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotal As object	OnWriteFCConvert.ToInt32(
			fldTotal.Text = lngVehicleCounter.ToString();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			Label10.Text = "Page " + this.PageNumber;
		}

		private void rptVehicleAddressList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptVehicleAddressList properties;
			//rptVehicleAddressList.Caption	= "Vehicle Address List";
			//rptVehicleAddressList.Icon	= "rptVehicleAddressList.dsx":0000";
			//rptVehicleAddressList.Left	= 0;
			//rptVehicleAddressList.Top	= 0;
			//rptVehicleAddressList.Width	= 11880;
			//rptVehicleAddressList.Height	= 8595;
			//rptVehicleAddressList.StartUpPosition	= 3;
			//rptVehicleAddressList.SectionData	= "rptVehicleAddressList.dsx":508A;
			//End Unmaped Properties
		}
	}
}
