//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPurgeReports.
	/// </summary>
	partial class frmPurgeReports
	{
		public fecherFoundation.FCButton cmdPurge;
		public fecherFoundation.FCComboBox cboPeriod;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdPurge = new fecherFoundation.FCButton();
            this.cboPeriod = new fecherFoundation.FCComboBox();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 174);
            this.BottomPanel.Size = new System.Drawing.Size(507, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPurge);
            this.ClientArea.Controls.Add(this.cboPeriod);
            this.ClientArea.Controls.Add(this.lblInstructions);
            this.ClientArea.Size = new System.Drawing.Size(507, 114);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(507, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(322, 30);
            this.HeaderText.Text = "Purge Reporting Information";
            // 
            // cmdPurge
            // 
            this.cmdPurge.AppearanceKey = "acceptButton";
            this.cmdPurge.Location = new System.Drawing.Point(30, 145);
            this.cmdPurge.Name = "cmdPurge";
            this.cmdPurge.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPurge.Size = new System.Drawing.Size(90, 48);
            this.cmdPurge.TabIndex = 1;
            this.cmdPurge.Text = "Purge";
            this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
            // 
            // cboPeriod
            // 
            this.cboPeriod.AutoSize = false;
            this.cboPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cboPeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.Location = new System.Drawing.Point(30, 85);
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(240, 40);
            this.cboPeriod.TabIndex = 0;
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(448, 40);
            this.lblInstructions.TabIndex = 3;
            this.lblInstructions.Text = "PLEASE SELECT A PERIOD CLOSEOUT DATE AT LEAST FIVE YEARS IN THE PAST AND CLICK TH" +
    "E \'PURGE\' BUTTON.  ALL REPORTING INFORMATION THAT THAT WAS SAVED ON OR BEFORE TH" +
    "AT DATE WILL BE DELETED";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 0;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmPurgeReports
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(507, 282);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPurgeReports";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Purge Reporting Information";
            this.Load += new System.EventHandler(this.frmPurgeReports_Load);
            this.Activated += new System.EventHandler(this.frmPurgeReports_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeReports_KeyPress);
            this.Resize += new System.EventHandler(this.frmPurgeReports_Resize);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}