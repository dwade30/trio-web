﻿using TWMV0000.Commands;
using SharedApplication.Messaging;

namespace TWMV0000
{
    public class TRIO_Command_Handler___Show_Modal2 : CommandHandler<ChooseCentralParty, int>
    {
        private IModalView<ICentralPartySearchViewModel> searchView;
        public TRIO_Command_Handler___Show_Modal2(IModalView<ICentralPartySearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override int Handle(ChooseCentralParty command)
        {
            searchView.ShowModal();
            return searchView.ViewModel.PartyId;
        }
    }
}