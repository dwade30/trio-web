﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptMVRT10Laser.
	/// </summary>
	partial class rptMVRT10Laser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMVRT10Laser));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetWeight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransactionDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCTANumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMileage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTransaction = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegYes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidationLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld2000lbsNotice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVINForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYearForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMakeForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor1Form2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor2Form2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStyleForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetWeightForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner1Form2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransactionDateForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOldPlateForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCTANumberForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddressForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCityForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStateForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldZipForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCityForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceStateForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCodeForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMileageForm2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldExpiresForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlateForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTransactionForm2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDescriptionForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegYesForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegNoForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner2Form2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidationLine1Form2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld2000lbsNoticeForm2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVINForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYearForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMakeForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor1Form3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldColor2Form3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStyleForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetWeightForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner1Form3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransactionDateForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOldPlateForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnitForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCTANumberForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddressForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCityForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStateForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldZipForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCityForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceStateForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldResidenceCodeForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMileageForm3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldExpiresForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlateForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTransactionForm3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDescriptionForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegYesForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReRegNoForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner2Form3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldValidationLine1Form3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld2000lbsNoticeForm3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.bcdPlate = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransaction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNotice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVINForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMakeForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1Form2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2Form2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyleForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeightForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1Form2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDateForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlateForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumberForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddressForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZipForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCityForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceStateForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCodeForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileageForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpiresForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlateForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransactionForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescriptionForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYesForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNoForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2Form2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1Form2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNoticeForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVINForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMakeForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1Form3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2Form3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyleForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeightForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1Form3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDateForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlateForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumberForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddressForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZipForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCityForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceStateForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCodeForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileageForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpiresForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlateForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransactionForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescriptionForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYesForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNoForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2Form3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1Form3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNoticeForm3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldVIN,
				this.fldYear,
				this.fldMake,
				this.fldColor1,
				this.fldColor2,
				this.fldStyle,
				this.fldNetWeight,
				this.fldOwner1,
				this.fldTransactionDate,
				this.fldOldPlate,
				this.fldUnit,
				this.fldCTANumber,
				this.fldAddress,
				this.fldCity,
				this.fldState,
				this.fldZip,
				this.fldResidenceCity,
				this.fldResidenceState,
				this.fldResidenceCode,
				this.lblMileage,
				this.fldExpires,
				this.fldPlate,
				this.lblTransaction,
				this.fldDescription,
				this.fldReRegYes,
				this.fldReRegNo,
				this.fldOwner2,
				this.fldValidationLine1,
				this.fld2000lbsNotice,
				this.fldVINForm2,
				this.fldYearForm2,
				this.fldMakeForm2,
				this.fldColor1Form2,
				this.fldColor2Form2,
				this.fldStyleForm2,
				this.fldNetWeightForm2,
				this.fldOwner1Form2,
				this.fldTransactionDateForm2,
				this.fldOldPlateForm2,
				this.fldUnitForm2,
				this.fldCTANumberForm2,
				this.fldAddressForm2,
				this.fldCityForm2,
				this.fldStateForm2,
				this.fldZipForm2,
				this.fldResidenceCityForm2,
				this.fldResidenceStateForm2,
				this.fldResidenceCodeForm2,
				this.lblMileageForm2,
				this.fldExpiresForm2,
				this.fldPlateForm2,
				this.lblTransactionForm2,
				this.fldDescriptionForm2,
				this.fldReRegYesForm2,
				this.fldReRegNoForm2,
				this.fldOwner2Form2,
				this.fldValidationLine1Form2,
				this.fld2000lbsNoticeForm2,
				this.fldVINForm3,
				this.fldYearForm3,
				this.fldMakeForm3,
				this.fldColor1Form3,
				this.fldColor2Form3,
				this.fldStyleForm3,
				this.fldNetWeightForm3,
				this.fldOwner1Form3,
				this.fldTransactionDateForm3,
				this.fldOldPlateForm3,
				this.fldUnitForm3,
				this.fldCTANumberForm3,
				this.fldAddressForm3,
				this.fldCityForm3,
				this.fldStateForm3,
				this.fldZipForm3,
				this.fldResidenceCityForm3,
				this.fldResidenceStateForm3,
				this.fldResidenceCodeForm3,
				this.lblMileageForm3,
				this.fldExpiresForm3,
				this.fldPlateForm3,
				this.lblTransactionForm3,
				this.fldDescriptionForm3,
				this.fldReRegYesForm3,
				this.fldReRegNoForm3,
				this.fldOwner2Form3,
				this.fldValidationLine1Form3,
				this.fld2000lbsNoticeForm3,
				this.bcdPlate
			});
			this.Detail.Height = 10.6875F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldVIN
			// 
			this.fldVIN.CanGrow = false;
			this.fldVIN.Height = 0.1875F;
			this.fldVIN.Left = 2.25F;
			this.fldVIN.MultiLine = false;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldVIN.Text = null;
			this.fldVIN.Top = 1.0625F;
			this.fldVIN.Width = 1.8125F;
			// 
			// fldYear
			// 
			this.fldYear.CanGrow = false;
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 2F;
			this.fldYear.MultiLine = false;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldYear.Text = null;
			this.fldYear.Top = 0.75F;
			this.fldYear.Width = 0.375F;
			// 
			// fldMake
			// 
			this.fldMake.CanGrow = false;
			this.fldMake.Height = 0.1875F;
			this.fldMake.Left = 1F;
			this.fldMake.MultiLine = false;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldMake.Text = null;
			this.fldMake.Top = 0.75F;
			this.fldMake.Width = 0.6875F;
			// 
			// fldColor1
			// 
			this.fldColor1.CanGrow = false;
			this.fldColor1.Height = 0.1875F;
			this.fldColor1.Left = 4F;
			this.fldColor1.MultiLine = false;
			this.fldColor1.Name = "fldColor1";
			this.fldColor1.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldColor1.Text = null;
			this.fldColor1.Top = 0.75F;
			this.fldColor1.Width = 0.3125F;
			// 
			// fldColor2
			// 
			this.fldColor2.CanGrow = false;
			this.fldColor2.Height = 0.1875F;
			this.fldColor2.Left = 4.3125F;
			this.fldColor2.MultiLine = false;
			this.fldColor2.Name = "fldColor2";
			this.fldColor2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldColor2.Text = null;
			this.fldColor2.Top = 0.75F;
			this.fldColor2.Width = 0.3125F;
			// 
			// fldStyle
			// 
			this.fldStyle.CanGrow = false;
			this.fldStyle.Height = 0.1875F;
			this.fldStyle.Left = 1.125F;
			this.fldStyle.MultiLine = false;
			this.fldStyle.Name = "fldStyle";
			this.fldStyle.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldStyle.Text = null;
			this.fldStyle.Top = 1.0625F;
			this.fldStyle.Width = 0.375F;
			// 
			// fldNetWeight
			// 
			this.fldNetWeight.CanGrow = false;
			this.fldNetWeight.Height = 0.1875F;
			this.fldNetWeight.Left = 5.5F;
			this.fldNetWeight.MultiLine = false;
			this.fldNetWeight.Name = "fldNetWeight";
			this.fldNetWeight.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldNetWeight.Text = null;
			this.fldNetWeight.Top = 1.0625F;
			this.fldNetWeight.Width = 0.5F;
			// 
			// fldOwner1
			// 
			this.fldOwner1.CanGrow = false;
			this.fldOwner1.Height = 0.1875F;
			this.fldOwner1.Left = 1.75F;
			this.fldOwner1.MultiLine = false;
			this.fldOwner1.Name = "fldOwner1";
			this.fldOwner1.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldOwner1.Text = null;
			this.fldOwner1.Top = 1.263889F;
			this.fldOwner1.Width = 3.875F;
			// 
			// fldTransactionDate
			// 
			this.fldTransactionDate.CanGrow = false;
			this.fldTransactionDate.Height = 0.1875F;
			this.fldTransactionDate.Left = 0.875F;
			this.fldTransactionDate.MultiLine = false;
			this.fldTransactionDate.Name = "fldTransactionDate";
			this.fldTransactionDate.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldTransactionDate.Text = null;
			this.fldTransactionDate.Top = 2.75F;
			this.fldTransactionDate.Width = 0.9375F;
			// 
			// fldOldPlate
			// 
			this.fldOldPlate.CanGrow = false;
			this.fldOldPlate.Height = 0.1875F;
			this.fldOldPlate.Left = 2.125F;
			this.fldOldPlate.MultiLine = false;
			this.fldOldPlate.Name = "fldOldPlate";
			this.fldOldPlate.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldOldPlate.Text = null;
			this.fldOldPlate.Top = 2.75F;
			this.fldOldPlate.Width = 1.125F;
			// 
			// fldUnit
			// 
			this.fldUnit.CanGrow = false;
			this.fldUnit.Height = 0.1875F;
			this.fldUnit.Left = 2.875F;
			this.fldUnit.MultiLine = false;
			this.fldUnit.Name = "fldUnit";
			this.fldUnit.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldUnit.Text = null;
			this.fldUnit.Top = 0.75F;
			this.fldUnit.Width = 1F;
			// 
			// fldCTANumber
			// 
			this.fldCTANumber.CanGrow = false;
			this.fldCTANumber.Height = 0.1875F;
			this.fldCTANumber.Left = 4.375F;
			this.fldCTANumber.MultiLine = false;
			this.fldCTANumber.Name = "fldCTANumber";
			this.fldCTANumber.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldCTANumber.Text = null;
			this.fldCTANumber.Top = 1.0625F;
			this.fldCTANumber.Width = 0.9375F;
			// 
			// fldAddress
			// 
			this.fldAddress.CanGrow = false;
			this.fldAddress.Height = 0.1875F;
			this.fldAddress.Left = 1.75F;
			this.fldAddress.MultiLine = false;
			this.fldAddress.Name = "fldAddress";
			this.fldAddress.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAddress.Text = null;
			this.fldAddress.Top = 1.75F;
			this.fldAddress.Width = 3.8125F;
			// 
			// fldCity
			// 
			this.fldCity.CanGrow = false;
			this.fldCity.Height = 0.1875F;
			this.fldCity.Left = 1.75F;
			this.fldCity.MultiLine = false;
			this.fldCity.Name = "fldCity";
			this.fldCity.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCity.Text = null;
			this.fldCity.Top = 2.0625F;
			this.fldCity.Width = 2.0625F;
			// 
			// fldState
			// 
			this.fldState.CanGrow = false;
			this.fldState.Height = 0.1875F;
			this.fldState.Left = 3.9375F;
			this.fldState.MultiLine = false;
			this.fldState.Name = "fldState";
			this.fldState.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldState.Text = null;
			this.fldState.Top = 2.0625F;
			this.fldState.Width = 0.375F;
			// 
			// fldZip
			// 
			this.fldZip.CanGrow = false;
			this.fldZip.Height = 0.1875F;
			this.fldZip.Left = 4.375F;
			this.fldZip.MultiLine = false;
			this.fldZip.Name = "fldZip";
			this.fldZip.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldZip.Text = null;
			this.fldZip.Top = 2.0625F;
			this.fldZip.Width = 0.5625F;
			// 
			// fldResidenceCity
			// 
			this.fldResidenceCity.CanGrow = false;
			this.fldResidenceCity.Height = 0.1875F;
			this.fldResidenceCity.Left = 1.75F;
			this.fldResidenceCity.MultiLine = false;
			this.fldResidenceCity.Name = "fldResidenceCity";
			this.fldResidenceCity.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldResidenceCity.Text = null;
			this.fldResidenceCity.Top = 2.3125F;
			this.fldResidenceCity.Width = 2.1875F;
			// 
			// fldResidenceState
			// 
			this.fldResidenceState.CanGrow = false;
			this.fldResidenceState.Height = 0.1875F;
			this.fldResidenceState.Left = 3.9375F;
			this.fldResidenceState.MultiLine = false;
			this.fldResidenceState.Name = "fldResidenceState";
			this.fldResidenceState.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldResidenceState.Text = null;
			this.fldResidenceState.Top = 2.3125F;
			this.fldResidenceState.Width = 0.375F;
			// 
			// fldResidenceCode
			// 
			this.fldResidenceCode.CanGrow = false;
			this.fldResidenceCode.Height = 0.1875F;
			this.fldResidenceCode.Left = 4.3125F;
			this.fldResidenceCode.MultiLine = false;
			this.fldResidenceCode.Name = "fldResidenceCode";
			this.fldResidenceCode.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldResidenceCode.Text = null;
			this.fldResidenceCode.Top = 2.3125F;
			this.fldResidenceCode.Width = 0.625F;
			// 
			// lblMileage
			// 
			this.lblMileage.Height = 0.1875F;
			this.lblMileage.HyperLink = null;
			this.lblMileage.Left = 1.5625F;
			this.lblMileage.MultiLine = false;
			this.lblMileage.Name = "lblMileage";
			this.lblMileage.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.lblMileage.Text = "Expires";
			this.lblMileage.Top = 3.125F;
			this.lblMileage.Width = 0.5625F;
			// 
			// fldExpires
			// 
			this.fldExpires.CanGrow = false;
			this.fldExpires.Height = 0.1875F;
			this.fldExpires.Left = 2.1875F;
			this.fldExpires.MultiLine = false;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldExpires.Text = null;
			this.fldExpires.Top = 3.125F;
			this.fldExpires.Width = 0.8125F;
			// 
			// fldPlate
			// 
			this.fldPlate.CanGrow = false;
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 5.3125F;
			this.fldPlate.MultiLine = false;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldPlate.Text = null;
			this.fldPlate.Top = 0.75F;
			this.fldPlate.Width = 0.75F;
			// 
			// lblTransaction
			// 
			this.lblTransaction.Height = 0.1875F;
			this.lblTransaction.HyperLink = null;
			this.lblTransaction.Left = 3.625F;
			this.lblTransaction.MultiLine = false;
			this.lblTransaction.Name = "lblTransaction";
			this.lblTransaction.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.lblTransaction.Text = null;
			this.lblTransaction.Top = 2.75F;
			this.lblTransaction.Width = 1.375F;
			// 
			// fldDescription
			// 
			this.fldDescription.CanGrow = false;
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 1.5625F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldDescription.Text = null;
			this.fldDescription.Top = 2.9375F;
			this.fldDescription.Width = 2.5625F;
			// 
			// fldReRegYes
			// 
			this.fldReRegYes.CanGrow = false;
			this.fldReRegYes.Height = 0.125F;
			this.fldReRegYes.Left = 5.8125F;
			this.fldReRegYes.MultiLine = false;
			this.fldReRegYes.Name = "fldReRegYes";
			this.fldReRegYes.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReRegYes.Text = "X";
			this.fldReRegYes.Top = 0.4375F;
			this.fldReRegYes.Width = 0.1875F;
			// 
			// fldReRegNo
			// 
			this.fldReRegNo.CanGrow = false;
			this.fldReRegNo.Height = 0.125F;
			this.fldReRegNo.Left = 6.1875F;
			this.fldReRegNo.MultiLine = false;
			this.fldReRegNo.Name = "fldReRegNo";
			this.fldReRegNo.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReRegNo.Text = "X";
			this.fldReRegNo.Top = 0.4375F;
			this.fldReRegNo.Width = 0.1875F;
			// 
			// fldOwner2
			// 
			this.fldOwner2.CanGrow = false;
			this.fldOwner2.Height = 0.1875F;
			this.fldOwner2.Left = 1.75F;
			this.fldOwner2.MultiLine = false;
			this.fldOwner2.Name = "fldOwner2";
			this.fldOwner2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldOwner2.Text = null;
			this.fldOwner2.Top = 1.451389F;
			this.fldOwner2.Width = 3.875F;
			// 
			// fldValidationLine1
			// 
			this.fldValidationLine1.CanGrow = false;
			this.fldValidationLine1.Height = 0.125F;
			this.fldValidationLine1.Left = 1F;
			this.fldValidationLine1.MultiLine = false;
			this.fldValidationLine1.Name = "fldValidationLine1";
			this.fldValidationLine1.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldValidationLine1.Text = null;
			this.fldValidationLine1.Top = 0.1875F;
			this.fldValidationLine1.Width = 5.375F;
			// 
			// fld2000lbsNotice
			// 
			this.fld2000lbsNotice.CanGrow = false;
			this.fld2000lbsNotice.Height = 0.125F;
			this.fld2000lbsNotice.Left = 3.3125F;
			this.fld2000lbsNotice.MultiLine = false;
			this.fld2000lbsNotice.Name = "fld2000lbsNotice";
			this.fld2000lbsNotice.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fld2000lbsNotice.Text = "GVW 2000lbs max";
			this.fld2000lbsNotice.Top = 0.4375F;
			this.fld2000lbsNotice.Visible = false;
			this.fld2000lbsNotice.Width = 1.3125F;
			// 
			// fldVINForm2
			// 
			this.fldVINForm2.CanGrow = false;
			this.fldVINForm2.Height = 0.1875F;
			this.fldVINForm2.Left = 2.25F;
			this.fldVINForm2.MultiLine = false;
			this.fldVINForm2.Name = "fldVINForm2";
			this.fldVINForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldVINForm2.Text = null;
			this.fldVINForm2.Top = 4.75F;
			this.fldVINForm2.Width = 1.8125F;
			// 
			// fldYearForm2
			// 
			this.fldYearForm2.CanGrow = false;
			this.fldYearForm2.Height = 0.1875F;
			this.fldYearForm2.Left = 2F;
			this.fldYearForm2.MultiLine = false;
			this.fldYearForm2.Name = "fldYearForm2";
			this.fldYearForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldYearForm2.Text = null;
			this.fldYearForm2.Top = 4.4375F;
			this.fldYearForm2.Width = 0.375F;
			// 
			// fldMakeForm2
			// 
			this.fldMakeForm2.CanGrow = false;
			this.fldMakeForm2.Height = 0.1875F;
			this.fldMakeForm2.Left = 1F;
			this.fldMakeForm2.MultiLine = false;
			this.fldMakeForm2.Name = "fldMakeForm2";
			this.fldMakeForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldMakeForm2.Text = null;
			this.fldMakeForm2.Top = 4.4375F;
			this.fldMakeForm2.Width = 0.6875F;
			// 
			// fldColor1Form2
			// 
			this.fldColor1Form2.CanGrow = false;
			this.fldColor1Form2.Height = 0.1875F;
			this.fldColor1Form2.Left = 4F;
			this.fldColor1Form2.MultiLine = false;
			this.fldColor1Form2.Name = "fldColor1Form2";
			this.fldColor1Form2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldColor1Form2.Text = null;
			this.fldColor1Form2.Top = 4.4375F;
			this.fldColor1Form2.Width = 0.3125F;
			// 
			// fldColor2Form2
			// 
			this.fldColor2Form2.CanGrow = false;
			this.fldColor2Form2.Height = 0.1875F;
			this.fldColor2Form2.Left = 4.3125F;
			this.fldColor2Form2.MultiLine = false;
			this.fldColor2Form2.Name = "fldColor2Form2";
			this.fldColor2Form2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldColor2Form2.Text = null;
			this.fldColor2Form2.Top = 4.4375F;
			this.fldColor2Form2.Width = 0.3125F;
			// 
			// fldStyleForm2
			// 
			this.fldStyleForm2.CanGrow = false;
			this.fldStyleForm2.Height = 0.1875F;
			this.fldStyleForm2.Left = 1.125F;
			this.fldStyleForm2.MultiLine = false;
			this.fldStyleForm2.Name = "fldStyleForm2";
			this.fldStyleForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldStyleForm2.Text = null;
			this.fldStyleForm2.Top = 4.75F;
			this.fldStyleForm2.Width = 0.375F;
			// 
			// fldNetWeightForm2
			// 
			this.fldNetWeightForm2.CanGrow = false;
			this.fldNetWeightForm2.Height = 0.1875F;
			this.fldNetWeightForm2.Left = 5.5F;
			this.fldNetWeightForm2.MultiLine = false;
			this.fldNetWeightForm2.Name = "fldNetWeightForm2";
			this.fldNetWeightForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldNetWeightForm2.Text = null;
			this.fldNetWeightForm2.Top = 4.75F;
			this.fldNetWeightForm2.Width = 0.5F;
			// 
			// fldOwner1Form2
			// 
			this.fldOwner1Form2.CanGrow = false;
			this.fldOwner1Form2.Height = 0.1875F;
			this.fldOwner1Form2.Left = 1.75F;
			this.fldOwner1Form2.MultiLine = false;
			this.fldOwner1Form2.Name = "fldOwner1Form2";
			this.fldOwner1Form2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldOwner1Form2.Text = null;
			this.fldOwner1Form2.Top = 4.951389F;
			this.fldOwner1Form2.Width = 3.875F;
			// 
			// fldTransactionDateForm2
			// 
			this.fldTransactionDateForm2.CanGrow = false;
			this.fldTransactionDateForm2.Height = 0.1875F;
			this.fldTransactionDateForm2.Left = 0.875F;
			this.fldTransactionDateForm2.MultiLine = false;
			this.fldTransactionDateForm2.Name = "fldTransactionDateForm2";
			this.fldTransactionDateForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldTransactionDateForm2.Text = null;
			this.fldTransactionDateForm2.Top = 6.4375F;
			this.fldTransactionDateForm2.Width = 0.9375F;
			// 
			// fldOldPlateForm2
			// 
			this.fldOldPlateForm2.CanGrow = false;
			this.fldOldPlateForm2.Height = 0.1875F;
			this.fldOldPlateForm2.Left = 2.125F;
			this.fldOldPlateForm2.MultiLine = false;
			this.fldOldPlateForm2.Name = "fldOldPlateForm2";
			this.fldOldPlateForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldOldPlateForm2.Text = null;
			this.fldOldPlateForm2.Top = 6.4375F;
			this.fldOldPlateForm2.Width = 1.125F;
			// 
			// fldUnitForm2
			// 
			this.fldUnitForm2.CanGrow = false;
			this.fldUnitForm2.Height = 0.1875F;
			this.fldUnitForm2.Left = 2.875F;
			this.fldUnitForm2.MultiLine = false;
			this.fldUnitForm2.Name = "fldUnitForm2";
			this.fldUnitForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldUnitForm2.Text = null;
			this.fldUnitForm2.Top = 4.4375F;
			this.fldUnitForm2.Width = 1F;
			// 
			// fldCTANumberForm2
			// 
			this.fldCTANumberForm2.CanGrow = false;
			this.fldCTANumberForm2.Height = 0.1875F;
			this.fldCTANumberForm2.Left = 4.375F;
			this.fldCTANumberForm2.MultiLine = false;
			this.fldCTANumberForm2.Name = "fldCTANumberForm2";
			this.fldCTANumberForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldCTANumberForm2.Text = null;
			this.fldCTANumberForm2.Top = 4.75F;
			this.fldCTANumberForm2.Width = 0.9375F;
			// 
			// fldAddressForm2
			// 
			this.fldAddressForm2.CanGrow = false;
			this.fldAddressForm2.Height = 0.1875F;
			this.fldAddressForm2.Left = 1.75F;
			this.fldAddressForm2.MultiLine = false;
			this.fldAddressForm2.Name = "fldAddressForm2";
			this.fldAddressForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAddressForm2.Text = null;
			this.fldAddressForm2.Top = 5.4375F;
			this.fldAddressForm2.Width = 3.8125F;
			// 
			// fldCityForm2
			// 
			this.fldCityForm2.CanGrow = false;
			this.fldCityForm2.Height = 0.1875F;
			this.fldCityForm2.Left = 1.75F;
			this.fldCityForm2.MultiLine = false;
			this.fldCityForm2.Name = "fldCityForm2";
			this.fldCityForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCityForm2.Text = null;
			this.fldCityForm2.Top = 5.75F;
			this.fldCityForm2.Width = 2.0625F;
			// 
			// fldStateForm2
			// 
			this.fldStateForm2.CanGrow = false;
			this.fldStateForm2.Height = 0.1875F;
			this.fldStateForm2.Left = 3.9375F;
			this.fldStateForm2.MultiLine = false;
			this.fldStateForm2.Name = "fldStateForm2";
			this.fldStateForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldStateForm2.Text = null;
			this.fldStateForm2.Top = 5.75F;
			this.fldStateForm2.Width = 0.375F;
			// 
			// fldZipForm2
			// 
			this.fldZipForm2.CanGrow = false;
			this.fldZipForm2.Height = 0.1875F;
			this.fldZipForm2.Left = 4.375F;
			this.fldZipForm2.MultiLine = false;
			this.fldZipForm2.Name = "fldZipForm2";
			this.fldZipForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldZipForm2.Text = null;
			this.fldZipForm2.Top = 5.75F;
			this.fldZipForm2.Width = 0.5625F;
			// 
			// fldResidenceCityForm2
			// 
			this.fldResidenceCityForm2.CanGrow = false;
			this.fldResidenceCityForm2.Height = 0.1875F;
			this.fldResidenceCityForm2.Left = 1.75F;
			this.fldResidenceCityForm2.MultiLine = false;
			this.fldResidenceCityForm2.Name = "fldResidenceCityForm2";
			this.fldResidenceCityForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldResidenceCityForm2.Text = null;
			this.fldResidenceCityForm2.Top = 6F;
			this.fldResidenceCityForm2.Width = 2.1875F;
			// 
			// fldResidenceStateForm2
			// 
			this.fldResidenceStateForm2.CanGrow = false;
			this.fldResidenceStateForm2.Height = 0.1875F;
			this.fldResidenceStateForm2.Left = 3.9375F;
			this.fldResidenceStateForm2.MultiLine = false;
			this.fldResidenceStateForm2.Name = "fldResidenceStateForm2";
			this.fldResidenceStateForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldResidenceStateForm2.Text = null;
			this.fldResidenceStateForm2.Top = 6F;
			this.fldResidenceStateForm2.Width = 0.375F;
			// 
			// fldResidenceCodeForm2
			// 
			this.fldResidenceCodeForm2.CanGrow = false;
			this.fldResidenceCodeForm2.Height = 0.1875F;
			this.fldResidenceCodeForm2.Left = 4.3125F;
			this.fldResidenceCodeForm2.MultiLine = false;
			this.fldResidenceCodeForm2.Name = "fldResidenceCodeForm2";
			this.fldResidenceCodeForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldResidenceCodeForm2.Text = null;
			this.fldResidenceCodeForm2.Top = 6F;
			this.fldResidenceCodeForm2.Width = 0.625F;
			// 
			// lblMileageForm2
			// 
			this.lblMileageForm2.Height = 0.1875F;
			this.lblMileageForm2.HyperLink = null;
			this.lblMileageForm2.Left = 1.5625F;
			this.lblMileageForm2.MultiLine = false;
			this.lblMileageForm2.Name = "lblMileageForm2";
			this.lblMileageForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.lblMileageForm2.Text = "Expires";
			this.lblMileageForm2.Top = 6.8125F;
			this.lblMileageForm2.Width = 0.5625F;
			// 
			// fldExpiresForm2
			// 
			this.fldExpiresForm2.CanGrow = false;
			this.fldExpiresForm2.Height = 0.1875F;
			this.fldExpiresForm2.Left = 2.1875F;
			this.fldExpiresForm2.MultiLine = false;
			this.fldExpiresForm2.Name = "fldExpiresForm2";
			this.fldExpiresForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldExpiresForm2.Text = null;
			this.fldExpiresForm2.Top = 6.8125F;
			this.fldExpiresForm2.Width = 0.8125F;
			// 
			// fldPlateForm2
			// 
			this.fldPlateForm2.CanGrow = false;
			this.fldPlateForm2.Height = 0.1875F;
			this.fldPlateForm2.Left = 5.3125F;
			this.fldPlateForm2.MultiLine = false;
			this.fldPlateForm2.Name = "fldPlateForm2";
			this.fldPlateForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldPlateForm2.Text = null;
			this.fldPlateForm2.Top = 4.4375F;
			this.fldPlateForm2.Width = 0.75F;
			// 
			// lblTransactionForm2
			// 
			this.lblTransactionForm2.Height = 0.1875F;
			this.lblTransactionForm2.HyperLink = null;
			this.lblTransactionForm2.Left = 3.625F;
			this.lblTransactionForm2.MultiLine = false;
			this.lblTransactionForm2.Name = "lblTransactionForm2";
			this.lblTransactionForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.lblTransactionForm2.Text = null;
			this.lblTransactionForm2.Top = 6.4375F;
			this.lblTransactionForm2.Width = 1.375F;
			// 
			// fldDescriptionForm2
			// 
			this.fldDescriptionForm2.CanGrow = false;
			this.fldDescriptionForm2.Height = 0.1875F;
			this.fldDescriptionForm2.Left = 1.5625F;
			this.fldDescriptionForm2.MultiLine = false;
			this.fldDescriptionForm2.Name = "fldDescriptionForm2";
			this.fldDescriptionForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldDescriptionForm2.Text = null;
			this.fldDescriptionForm2.Top = 6.625F;
			this.fldDescriptionForm2.Width = 2.5625F;
			// 
			// fldReRegYesForm2
			// 
			this.fldReRegYesForm2.CanGrow = false;
			this.fldReRegYesForm2.Height = 0.125F;
			this.fldReRegYesForm2.Left = 5.8125F;
			this.fldReRegYesForm2.MultiLine = false;
			this.fldReRegYesForm2.Name = "fldReRegYesForm2";
			this.fldReRegYesForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReRegYesForm2.Text = "X";
			this.fldReRegYesForm2.Top = 4.0625F;
			this.fldReRegYesForm2.Width = 0.1875F;
			// 
			// fldReRegNoForm2
			// 
			this.fldReRegNoForm2.CanGrow = false;
			this.fldReRegNoForm2.Height = 0.125F;
			this.fldReRegNoForm2.Left = 6.1875F;
			this.fldReRegNoForm2.MultiLine = false;
			this.fldReRegNoForm2.Name = "fldReRegNoForm2";
			this.fldReRegNoForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReRegNoForm2.Text = "X";
			this.fldReRegNoForm2.Top = 4.0625F;
			this.fldReRegNoForm2.Width = 0.1875F;
			// 
			// fldOwner2Form2
			// 
			this.fldOwner2Form2.CanGrow = false;
			this.fldOwner2Form2.Height = 0.1875F;
			this.fldOwner2Form2.Left = 1.75F;
			this.fldOwner2Form2.MultiLine = false;
			this.fldOwner2Form2.Name = "fldOwner2Form2";
			this.fldOwner2Form2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldOwner2Form2.Text = null;
			this.fldOwner2Form2.Top = 5.138889F;
			this.fldOwner2Form2.Width = 3.875F;
			// 
			// fldValidationLine1Form2
			// 
			this.fldValidationLine1Form2.CanGrow = false;
			this.fldValidationLine1Form2.Height = 0.125F;
			this.fldValidationLine1Form2.Left = 1F;
			this.fldValidationLine1Form2.MultiLine = false;
			this.fldValidationLine1Form2.Name = "fldValidationLine1Form2";
			this.fldValidationLine1Form2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldValidationLine1Form2.Text = null;
			this.fldValidationLine1Form2.Top = 3.8125F;
			this.fldValidationLine1Form2.Width = 5.375F;
			// 
			// fld2000lbsNoticeForm2
			// 
			this.fld2000lbsNoticeForm2.CanGrow = false;
			this.fld2000lbsNoticeForm2.Height = 0.125F;
			this.fld2000lbsNoticeForm2.Left = 3.3125F;
			this.fld2000lbsNoticeForm2.MultiLine = false;
			this.fld2000lbsNoticeForm2.Name = "fld2000lbsNoticeForm2";
			this.fld2000lbsNoticeForm2.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fld2000lbsNoticeForm2.Text = "GVW 2000lbs max";
			this.fld2000lbsNoticeForm2.Top = 4.0625F;
			this.fld2000lbsNoticeForm2.Visible = false;
			this.fld2000lbsNoticeForm2.Width = 1.3125F;
			// 
			// fldVINForm3
			// 
			this.fldVINForm3.CanGrow = false;
			this.fldVINForm3.Height = 0.1875F;
			this.fldVINForm3.Left = 2.25F;
			this.fldVINForm3.MultiLine = false;
			this.fldVINForm3.Name = "fldVINForm3";
			this.fldVINForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldVINForm3.Text = null;
			this.fldVINForm3.Top = 8.4375F;
			this.fldVINForm3.Width = 1.8125F;
			// 
			// fldYearForm3
			// 
			this.fldYearForm3.CanGrow = false;
			this.fldYearForm3.Height = 0.1875F;
			this.fldYearForm3.Left = 2F;
			this.fldYearForm3.MultiLine = false;
			this.fldYearForm3.Name = "fldYearForm3";
			this.fldYearForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldYearForm3.Text = null;
			this.fldYearForm3.Top = 8.125F;
			this.fldYearForm3.Width = 0.375F;
			// 
			// fldMakeForm3
			// 
			this.fldMakeForm3.CanGrow = false;
			this.fldMakeForm3.Height = 0.1875F;
			this.fldMakeForm3.Left = 1F;
			this.fldMakeForm3.MultiLine = false;
			this.fldMakeForm3.Name = "fldMakeForm3";
			this.fldMakeForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldMakeForm3.Text = null;
			this.fldMakeForm3.Top = 8.125F;
			this.fldMakeForm3.Width = 0.6875F;
			// 
			// fldColor1Form3
			// 
			this.fldColor1Form3.CanGrow = false;
			this.fldColor1Form3.Height = 0.1875F;
			this.fldColor1Form3.Left = 4F;
			this.fldColor1Form3.MultiLine = false;
			this.fldColor1Form3.Name = "fldColor1Form3";
			this.fldColor1Form3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldColor1Form3.Text = null;
			this.fldColor1Form3.Top = 8.125F;
			this.fldColor1Form3.Width = 0.3125F;
			// 
			// fldColor2Form3
			// 
			this.fldColor2Form3.CanGrow = false;
			this.fldColor2Form3.Height = 0.1875F;
			this.fldColor2Form3.Left = 4.3125F;
			this.fldColor2Form3.MultiLine = false;
			this.fldColor2Form3.Name = "fldColor2Form3";
			this.fldColor2Form3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldColor2Form3.Text = null;
			this.fldColor2Form3.Top = 8.125F;
			this.fldColor2Form3.Width = 0.3125F;
			// 
			// fldStyleForm3
			// 
			this.fldStyleForm3.CanGrow = false;
			this.fldStyleForm3.Height = 0.1875F;
			this.fldStyleForm3.Left = 1.125F;
			this.fldStyleForm3.MultiLine = false;
			this.fldStyleForm3.Name = "fldStyleForm3";
			this.fldStyleForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldStyleForm3.Text = null;
			this.fldStyleForm3.Top = 8.4375F;
			this.fldStyleForm3.Width = 0.375F;
			// 
			// fldNetWeightForm3
			// 
			this.fldNetWeightForm3.CanGrow = false;
			this.fldNetWeightForm3.Height = 0.1875F;
			this.fldNetWeightForm3.Left = 5.5F;
			this.fldNetWeightForm3.MultiLine = false;
			this.fldNetWeightForm3.Name = "fldNetWeightForm3";
			this.fldNetWeightForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldNetWeightForm3.Text = null;
			this.fldNetWeightForm3.Top = 8.4375F;
			this.fldNetWeightForm3.Width = 0.5F;
			// 
			// fldOwner1Form3
			// 
			this.fldOwner1Form3.CanGrow = false;
			this.fldOwner1Form3.Height = 0.1875F;
			this.fldOwner1Form3.Left = 1.75F;
			this.fldOwner1Form3.MultiLine = false;
			this.fldOwner1Form3.Name = "fldOwner1Form3";
			this.fldOwner1Form3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldOwner1Form3.Text = null;
			this.fldOwner1Form3.Top = 8.638889F;
			this.fldOwner1Form3.Width = 3.875F;
			// 
			// fldTransactionDateForm3
			// 
			this.fldTransactionDateForm3.CanGrow = false;
			this.fldTransactionDateForm3.Height = 0.1875F;
			this.fldTransactionDateForm3.Left = 0.875F;
			this.fldTransactionDateForm3.MultiLine = false;
			this.fldTransactionDateForm3.Name = "fldTransactionDateForm3";
			this.fldTransactionDateForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldTransactionDateForm3.Text = null;
			this.fldTransactionDateForm3.Top = 10.125F;
			this.fldTransactionDateForm3.Width = 0.9375F;
			// 
			// fldOldPlateForm3
			// 
			this.fldOldPlateForm3.CanGrow = false;
			this.fldOldPlateForm3.Height = 0.1875F;
			this.fldOldPlateForm3.Left = 2.125F;
			this.fldOldPlateForm3.MultiLine = false;
			this.fldOldPlateForm3.Name = "fldOldPlateForm3";
			this.fldOldPlateForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldOldPlateForm3.Text = null;
			this.fldOldPlateForm3.Top = 10.125F;
			this.fldOldPlateForm3.Width = 1.125F;
			// 
			// fldUnitForm3
			// 
			this.fldUnitForm3.CanGrow = false;
			this.fldUnitForm3.Height = 0.1875F;
			this.fldUnitForm3.Left = 2.875F;
			this.fldUnitForm3.MultiLine = false;
			this.fldUnitForm3.Name = "fldUnitForm3";
			this.fldUnitForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldUnitForm3.Text = null;
			this.fldUnitForm3.Top = 8.125F;
			this.fldUnitForm3.Width = 1F;
			// 
			// fldCTANumberForm3
			// 
			this.fldCTANumberForm3.CanGrow = false;
			this.fldCTANumberForm3.Height = 0.1875F;
			this.fldCTANumberForm3.Left = 4.375F;
			this.fldCTANumberForm3.MultiLine = false;
			this.fldCTANumberForm3.Name = "fldCTANumberForm3";
			this.fldCTANumberForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldCTANumberForm3.Text = null;
			this.fldCTANumberForm3.Top = 8.4375F;
			this.fldCTANumberForm3.Width = 0.9375F;
			// 
			// fldAddressForm3
			// 
			this.fldAddressForm3.CanGrow = false;
			this.fldAddressForm3.Height = 0.1875F;
			this.fldAddressForm3.Left = 1.75F;
			this.fldAddressForm3.MultiLine = false;
			this.fldAddressForm3.Name = "fldAddressForm3";
			this.fldAddressForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAddressForm3.Text = null;
			this.fldAddressForm3.Top = 9.125F;
			this.fldAddressForm3.Width = 3.8125F;
			// 
			// fldCityForm3
			// 
			this.fldCityForm3.CanGrow = false;
			this.fldCityForm3.Height = 0.1875F;
			this.fldCityForm3.Left = 1.75F;
			this.fldCityForm3.MultiLine = false;
			this.fldCityForm3.Name = "fldCityForm3";
			this.fldCityForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCityForm3.Text = null;
			this.fldCityForm3.Top = 9.4375F;
			this.fldCityForm3.Width = 2.0625F;
			// 
			// fldStateForm3
			// 
			this.fldStateForm3.CanGrow = false;
			this.fldStateForm3.Height = 0.1875F;
			this.fldStateForm3.Left = 3.9375F;
			this.fldStateForm3.MultiLine = false;
			this.fldStateForm3.Name = "fldStateForm3";
			this.fldStateForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldStateForm3.Text = null;
			this.fldStateForm3.Top = 9.4375F;
			this.fldStateForm3.Width = 0.375F;
			// 
			// fldZipForm3
			// 
			this.fldZipForm3.CanGrow = false;
			this.fldZipForm3.Height = 0.1875F;
			this.fldZipForm3.Left = 4.375F;
			this.fldZipForm3.MultiLine = false;
			this.fldZipForm3.Name = "fldZipForm3";
			this.fldZipForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldZipForm3.Text = null;
			this.fldZipForm3.Top = 9.4375F;
			this.fldZipForm3.Width = 0.5625F;
			// 
			// fldResidenceCityForm3
			// 
			this.fldResidenceCityForm3.CanGrow = false;
			this.fldResidenceCityForm3.Height = 0.1875F;
			this.fldResidenceCityForm3.Left = 1.75F;
			this.fldResidenceCityForm3.MultiLine = false;
			this.fldResidenceCityForm3.Name = "fldResidenceCityForm3";
			this.fldResidenceCityForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldResidenceCityForm3.Text = null;
			this.fldResidenceCityForm3.Top = 9.6875F;
			this.fldResidenceCityForm3.Width = 2.1875F;
			// 
			// fldResidenceStateForm3
			// 
			this.fldResidenceStateForm3.CanGrow = false;
			this.fldResidenceStateForm3.Height = 0.1875F;
			this.fldResidenceStateForm3.Left = 3.9375F;
			this.fldResidenceStateForm3.MultiLine = false;
			this.fldResidenceStateForm3.Name = "fldResidenceStateForm3";
			this.fldResidenceStateForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldResidenceStateForm3.Text = null;
			this.fldResidenceStateForm3.Top = 9.6875F;
			this.fldResidenceStateForm3.Width = 0.375F;
			// 
			// fldResidenceCodeForm3
			// 
			this.fldResidenceCodeForm3.CanGrow = false;
			this.fldResidenceCodeForm3.Height = 0.1875F;
			this.fldResidenceCodeForm3.Left = 4.3125F;
			this.fldResidenceCodeForm3.MultiLine = false;
			this.fldResidenceCodeForm3.Name = "fldResidenceCodeForm3";
			this.fldResidenceCodeForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldResidenceCodeForm3.Text = null;
			this.fldResidenceCodeForm3.Top = 9.6875F;
			this.fldResidenceCodeForm3.Width = 0.625F;
			// 
			// lblMileageForm3
			// 
			this.lblMileageForm3.Height = 0.1875F;
			this.lblMileageForm3.HyperLink = null;
			this.lblMileageForm3.Left = 1.5625F;
			this.lblMileageForm3.MultiLine = false;
			this.lblMileageForm3.Name = "lblMileageForm3";
			this.lblMileageForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.lblMileageForm3.Text = "Expires";
			this.lblMileageForm3.Top = 10.5F;
			this.lblMileageForm3.Width = 0.5625F;
			// 
			// fldExpiresForm3
			// 
			this.fldExpiresForm3.CanGrow = false;
			this.fldExpiresForm3.Height = 0.1875F;
			this.fldExpiresForm3.Left = 2.1875F;
			this.fldExpiresForm3.MultiLine = false;
			this.fldExpiresForm3.Name = "fldExpiresForm3";
			this.fldExpiresForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldExpiresForm3.Text = null;
			this.fldExpiresForm3.Top = 10.5F;
			this.fldExpiresForm3.Width = 0.8125F;
			// 
			// fldPlateForm3
			// 
			this.fldPlateForm3.CanGrow = false;
			this.fldPlateForm3.Height = 0.1875F;
			this.fldPlateForm3.Left = 5.3125F;
			this.fldPlateForm3.MultiLine = false;
			this.fldPlateForm3.Name = "fldPlateForm3";
			this.fldPlateForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldPlateForm3.Text = null;
			this.fldPlateForm3.Top = 8.125F;
			this.fldPlateForm3.Width = 0.75F;
			// 
			// lblTransactionForm3
			// 
			this.lblTransactionForm3.Height = 0.1875F;
			this.lblTransactionForm3.HyperLink = null;
			this.lblTransactionForm3.Left = 3.625F;
			this.lblTransactionForm3.MultiLine = false;
			this.lblTransactionForm3.Name = "lblTransactionForm3";
			this.lblTransactionForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.lblTransactionForm3.Text = null;
			this.lblTransactionForm3.Top = 10.125F;
			this.lblTransactionForm3.Width = 1.375F;
			// 
			// fldDescriptionForm3
			// 
			this.fldDescriptionForm3.CanGrow = false;
			this.fldDescriptionForm3.Height = 0.1875F;
			this.fldDescriptionForm3.Left = 1.5625F;
			this.fldDescriptionForm3.MultiLine = false;
			this.fldDescriptionForm3.Name = "fldDescriptionForm3";
			this.fldDescriptionForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldDescriptionForm3.Text = null;
			this.fldDescriptionForm3.Top = 10.3125F;
			this.fldDescriptionForm3.Width = 2.5625F;
			// 
			// fldReRegYesForm3
			// 
			this.fldReRegYesForm3.CanGrow = false;
			this.fldReRegYesForm3.Height = 0.125F;
			this.fldReRegYesForm3.Left = 5.8125F;
			this.fldReRegYesForm3.MultiLine = false;
			this.fldReRegYesForm3.Name = "fldReRegYesForm3";
			this.fldReRegYesForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReRegYesForm3.Text = "X";
			this.fldReRegYesForm3.Top = 7.75F;
			this.fldReRegYesForm3.Width = 0.1875F;
			// 
			// fldReRegNoForm3
			// 
			this.fldReRegNoForm3.CanGrow = false;
			this.fldReRegNoForm3.Height = 0.125F;
			this.fldReRegNoForm3.Left = 6.1875F;
			this.fldReRegNoForm3.MultiLine = false;
			this.fldReRegNoForm3.Name = "fldReRegNoForm3";
			this.fldReRegNoForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReRegNoForm3.Text = "X";
			this.fldReRegNoForm3.Top = 7.75F;
			this.fldReRegNoForm3.Width = 0.1875F;
			// 
			// fldOwner2Form3
			// 
			this.fldOwner2Form3.CanGrow = false;
			this.fldOwner2Form3.Height = 0.1875F;
			this.fldOwner2Form3.Left = 1.75F;
			this.fldOwner2Form3.MultiLine = false;
			this.fldOwner2Form3.Name = "fldOwner2Form3";
			this.fldOwner2Form3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; ddo-char-set: 1";
			this.fldOwner2Form3.Text = null;
			this.fldOwner2Form3.Top = 8.826389F;
			this.fldOwner2Form3.Width = 3.875F;
			// 
			// fldValidationLine1Form3
			// 
			this.fldValidationLine1Form3.CanGrow = false;
			this.fldValidationLine1Form3.Height = 0.125F;
			this.fldValidationLine1Form3.Left = 1F;
			this.fldValidationLine1Form3.MultiLine = false;
			this.fldValidationLine1Form3.Name = "fldValidationLine1Form3";
			this.fldValidationLine1Form3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldValidationLine1Form3.Text = null;
			this.fldValidationLine1Form3.Top = 7.5F;
			this.fldValidationLine1Form3.Width = 5.375F;
			// 
			// fld2000lbsNoticeForm3
			// 
			this.fld2000lbsNoticeForm3.CanGrow = false;
			this.fld2000lbsNoticeForm3.Height = 0.125F;
			this.fld2000lbsNoticeForm3.Left = 3.3125F;
			this.fld2000lbsNoticeForm3.MultiLine = false;
			this.fld2000lbsNoticeForm3.Name = "fld2000lbsNoticeForm3";
			this.fld2000lbsNoticeForm3.Style = "font-family: \'Roman 12cpi\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fld2000lbsNoticeForm3.Text = "GVW 2000lbs max";
			this.fld2000lbsNoticeForm3.Top = 7.75F;
			this.fld2000lbsNoticeForm3.Visible = false;
			this.fld2000lbsNoticeForm3.Width = 1.3125F;
			// 
			// bcdPlate
			// 
			//this.bcdPlate.Font = new System.Drawing.Font("Courier New", 8F);
			this.bcdPlate.Height = 0.3125F;
			this.bcdPlate.Left = 5.625F;
			this.bcdPlate.Name = "bcdPlate";
			this.bcdPlate.QuietZoneBottom = 0F;
			this.bcdPlate.QuietZoneLeft = 0F;
			this.bcdPlate.QuietZoneRight = 0F;
			this.bcdPlate.QuietZoneTop = 0F;
			this.bcdPlate.Top = 9.75F;
			this.bcdPlate.Width = 1.4375F;
			// 
			// rptMVRT10Laser
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Courier New\'; font-style: inherit; font-variant: inherit; font-weig" + "ht: inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "inherit; font-size: 10pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransaction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNotice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVINForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMakeForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1Form2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2Form2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyleForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeightForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1Form2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDateForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlateForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumberForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddressForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZipForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCityForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceStateForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCodeForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileageForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpiresForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlateForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransactionForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescriptionForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYesForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNoForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2Form2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1Form2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNoticeForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVINForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYearForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMakeForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor1Form3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldColor2Form3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStyleForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetWeightForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner1Form3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransactionDateForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldPlateForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnitForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCTANumberForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddressForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCityForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldZipForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCityForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceStateForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldResidenceCodeForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMileageForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpiresForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlateForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransactionForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescriptionForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegYesForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReRegNoForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner2Form3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldValidationLine1Form3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2000lbsNoticeForm3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetWeight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransactionDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCTANumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMileage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTransaction;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegYes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegNo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidationLine1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld2000lbsNotice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVINForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYearForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMakeForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor1Form2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor2Form2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStyleForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetWeightForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner1Form2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransactionDateForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOldPlateForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCTANumberForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddressForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCityForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldZipForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCityForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceStateForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCodeForm2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMileageForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpiresForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlateForm2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTransactionForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescriptionForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegYesForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegNoForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner2Form2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidationLine1Form2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld2000lbsNoticeForm2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVINForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYearForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMakeForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor1Form3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor2Form3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStyleForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetWeightForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner1Form3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransactionDateForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOldPlateForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnitForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCTANumberForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddressForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCityForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldZipForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCityForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceStateForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCodeForm3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMileageForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpiresForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlateForm3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTransactionForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescriptionForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegYesForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReRegNoForm3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner2Form3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidationLine1Form3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld2000lbsNoticeForm3;
		private GrapeCity.ActiveReports.SectionReportModel.Barcode bcdPlate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
