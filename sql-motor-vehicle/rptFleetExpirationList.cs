//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptFleetReminderList.
	/// </summary>
	public partial class rptFleetReminderList : BaseSectionReport
	{
		public rptFleetReminderList()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Fleet Reminder List";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptFleetReminderList_ReportEnd;
		}

        private void RptFleetReminderList_ReportEnd(object sender, EventArgs e)
        {
			rsFleets.DisposeOf();
            rsVehicles.DisposeOf();

		}

        public static rptFleetReminderList InstancePtr
		{
			get
			{
				return (rptFleetReminderList)Sys.GetInstance(typeof(rptFleetReminderList));
			}
		}

		protected rptFleetReminderList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptFleetReminderList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsFleets = new clsDRWrapper();
		clsDRWrapper rsVehicles = new clsDRWrapper();
		// vbPorter upgrade warning: LocalFee As Decimal	OnWrite(int, Decimal)
		Decimal LocalFee;
		// vbPorter upgrade warning: StateFee As Decimal	OnWrite(int, Decimal)
		Decimal StateFee;
		// vbPorter upgrade warning: TotalAgent As Decimal	OnWrite(int, Decimal)
		Decimal TotalAgent;
		// vbPorter upgrade warning: TotalExcise As Decimal	OnWrite(int, Decimal)
		Decimal TotalExcise;
		// vbPorter upgrade warning: TotalReg As Decimal	OnWrite(int, Decimal)
		Decimal TotalReg;
		// vbPorter upgrade warning: TotalInitial As Decimal	OnWrite(int, Decimal)
		Decimal TotalInitial;
		// vbPorter upgrade warning: TotalSpecialty As Decimal	OnWrite(int, Decimal)
		Decimal TotalSpecialty;
		// vbPorter upgrade warning: TotalCommCredit As Decimal	OnWrite
		Decimal TotalCommCredit;
		// vbPorter upgrade warning: TotalLocal As Decimal	OnWrite(int, Decimal)
		Decimal TotalLocal;
		// vbPorter upgrade warning: TotalState As Decimal	OnWrite(int, Decimal)
		Decimal TotalState;
		int PageCounter;
		bool blnFirstRecord;
		// vbPorter upgrade warning: datStartDate As DateTime	OnWrite(string)
		DateTime datStartDate;
		// vbPorter upgrade warning: datEndDate As DateTime	OnWrite(string)
		DateTime datEndDate;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckVehicle:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsVehicles.MoveNext();
				if (rsVehicles.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsFleets.MoveNext();
					if (rsFleets.EndOfFile() != true)
					{
						rsVehicles.OpenRecordset("SELECT * FROM MASTER WHERE FleetNumber = " + rsFleets.Get_Fields_Int32("FleetNumber") + " AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "' AND Status <> 'T' ORDER BY Plate");
						blnFirstRecord = true;
						goto CheckVehicle;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (eArgs.EOF == false)
			{
				this.Fields["Binder"].Value = rsFleets.Get_Fields_Int32("FleetNumber");
				// If rsFleets.Fields("ExpiryMonth") <> 99 Then
				// lblFleetNumber = "FLEET " & rsFleets.Fields("FleetNumber")
				// Else
				// lblFleetNumber = "GROUP " & rsFleets.Fields("FleetNumber")
				// End If
				// lblFleetOwner = rsFleets.Fields("Owner1Name")
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label11 As object	OnWrite(string)
			// vbPorter upgrade warning: Label8 As object	OnWrite(string)
			// vbPorter upgrade warning: lblExpirationMonth As object	OnWrite(string)
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label11.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label8.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			datStartDate = FCConvert.ToDateTime(frmFleetExpiringList.InstancePtr.txtStartDate.Text);
			datEndDate = FCConvert.ToDateTime(frmFleetExpiringList.InstancePtr.txtEndDate.Text);
			lblExpirationMonth.Text = "Vehicles Expiring " + frmFleetExpiringList.InstancePtr.txtStartDate.Text + " to " + frmFleetExpiringList.InstancePtr.txtEndDate.Text;
			LocalFee = 0;
			StateFee = 0;
			TotalLocal = 0;
			TotalState = 0;
			TotalAgent = 0;
			TotalExcise = 0;
			TotalReg = 0;
			TotalInitial = 0;
			TotalSpecialty = 0;
			TotalCommCredit = 0;
			PageCounter = 0;
			blnFirstRecord = true;
			rsFleets.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleets.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND FleetNumber IN (SELECT DISTINCT FleetNumber FROM Master WHERE FleetNumber <> 0 AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "') ORDER BY p.FullName");
			if (rsFleets.EndOfFile() != true && rsFleets.BeginningOfFile() != true)
			{
				rsVehicles.OpenRecordset("SELECT * FROM MASTER WHERE FleetNumber = " + rsFleets.Get_Fields_Int32("FleetNumber") + " AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "' AND Status <> 'T' ORDER BY Plate");
			}
			else
			{
				MessageBox.Show("No records found that match the criteria.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldClassPlate As object	OnWrite(string)
			// vbPorter upgrade warning: fldVIN As object	OnWrite(string)
			// vbPorter upgrade warning: fldUnit As object	OnWriteFCConvert.ToDouble(
			// vbPorter upgrade warning: fldExpires As object	OnWrite(string)
			// vbPorter upgrade warning: fldLocal As object	OnWrite(string)
			// vbPorter upgrade warning: fldState As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotal As object	OnWrite(string)
			LocalFee = CalcLocalFee(ref rsVehicles);
			StateFee = CalcStateFee(ref rsVehicles);
			TotalLocal += LocalFee;
			TotalState += StateFee;
			fldClassPlate.Text = rsVehicles.Get_Fields_String("Class") + " " + rsVehicles.Get_Fields_String("plate");
			fldVIN.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Vin")));
			fldYear.Text = FCConvert.ToString(rsVehicles.Get_Fields("Year"));
			fldMake.Text = FCConvert.ToString(rsVehicles.Get_Fields_String("make"));
			fldModel.Text = FCConvert.ToString(rsVehicles.Get_Fields_String("model"));
			fldUnit.Text = FCConvert.ToString(Conversion.Val(rsVehicles.Get_Fields_String("UnitNumber")) + "");
			fldExpires.Text = Strings.Format(rsVehicles.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
			fldLocal.Text = Strings.Format(LocalFee, "#,##0.00");
			fldState.Text = Strings.Format(StateFee, "#,##0.00");
			fldTotal.Text = Strings.Format(LocalFee + StateFee, "#,##0.00");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotalLocal As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalState As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalAgentFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalExcise As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalLocalSummary As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalRegistrationFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalInitialFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalSpecialtyFee As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalStateSummary As object	OnWrite(string)
			// vbPorter upgrade warning: fldGrandTotal As object	OnWrite(string)
			fldTotalLocal.Text = Strings.Format(TotalLocal, "#,##0.00");
			fldTotalState.Text = Strings.Format(TotalState, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(TotalLocal + TotalState, "#,##0.00");
			fldTotalAgentFee.Text = Strings.Format(TotalAgent, "#,##0.00");
			fldTotalExcise.Text = Strings.Format(TotalExcise, "#,##0.00");
			fldTotalLocalSummary.Text = Strings.Format(TotalLocal, "#,##0.00");
			fldTotalRegistrationFee.Text = Strings.Format(TotalReg, "#,##0.00");
			fldTotalInitialFee.Text = Strings.Format(TotalInitial, "#,##0.00");
			fldTotalSpecialtyFee.Text = Strings.Format(TotalSpecialty, "#,##0.00");
			fldTotalStateSummary.Text = Strings.Format(TotalState, "#,##0.00");
			fldGrandTotal.Text = Strings.Format(TotalLocal + TotalState, "#,##0.00");
			LocalFee = 0;
			StateFee = 0;
			TotalLocal = 0;
			TotalState = 0;
			TotalAgent = 0;
			TotalExcise = 0;
			TotalReg = 0;
			TotalInitial = 0;
			TotalSpecialty = 0;
			TotalCommCredit = 0;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(Decimal, int)
		private Decimal CalcStateFee(ref clsDRWrapper rs, bool blnAdded = true)
		{
			Decimal CalcStateFee = 0;
			// vbPorter upgrade warning: FeeWrong As object	OnWrite(bool)
			object FeeWrong;
			// - "AutoDim"
			// vbPorter upgrade warning: RegFee As Decimal	OnWrite(Decimal, string)
			Decimal RegFee;
			Decimal InitFee;
			Decimal CommCred;
			// vbPorter upgrade warning: SpecialFee As Decimal	OnWrite
			Decimal SpecialFee;
			int tempweight = 0;
            var specialtyPlate = MotorVehicle.GetSpecialtyPlate(rsVehicles.Get_Fields_String("Class"));

            if ((specialtyPlate == "BB") || specialtyPlate == "BH" || (specialtyPlate == "LB") || (specialtyPlate == "CR") || (specialtyPlate == "UM") || (specialtyPlate == "AG") || (specialtyPlate == "AC") || (specialtyPlate == "AF") || (specialtyPlate == "TS") || (specialtyPlate == "BC") || (specialtyPlate == "AW") || (specialtyPlate == "LC"))
			{
				SpecialFee = 15;
			}
			else if (MotorVehicle.GetSpecialtyPlate(rsVehicles.Get_Fields_String("Class")) == "SW")
			{
				SpecialFee = 20;
			}
			else
			{
				SpecialFee = 0;
			}
			if (FCConvert.ToString(rsVehicles.Get_Fields_String("Class")) != "SE")
			{
				if (rsVehicles.IsFieldNull("RegisteredWeightNew"))
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(Math.Round(Conversion.Val(rsVehicles.Get_Fields_Int32("RegisteredWeightNew"))));
				}
			}
			else
			{
				if (fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields("NetWeight")))
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(rsVehicles.Get_Fields("NetWeight"));
				}
			}
			RegFee = GetReg(rsVehicles.Get_Fields_String("Class"), rsVehicles.Get_Fields_String("Subclass"), tempweight);
			// kk11202015 changed from P4
			if (FCConvert.CBool(rsVehicles.Get_Fields_Boolean("RENTAL")) && (FCConvert.ToString(rsVehicles.Get_Fields_String("Class")) == "CR" || FCConvert.ToString(rsVehicles.Get_Fields_String("Class")) == "UM" || (FCConvert.ToString(rsVehicles.Get_Fields_String("Class")) == "PC" && FCConvert.ToString(rsVehicles.Get_Fields_String("Subclass")) != "P5")))
			{
				RegFee *= 2;
			}
			if (frmFleetExpiringListMMTA.InstancePtr.cmbFleetTypeSelected.Text == "Fleets")
			{
				if (Information.IsDate(rsVehicles.Get_Fields("ExpireDate")))
				{
					if (FCConvert.ToInt32(rsVehicles.Get_Fields_DateTime("ExpireDate").Month) != frmPlateInfo.InstancePtr.ExpMonth)
					{
						RegFee = FCConvert.ToDecimal(Strings.Format(Pro(ref RegFee, DateDiff()), "##0.00"));
					}
				}
				else
				{
					FeeWrong = true;
				}
			}
			InitFee = MotorVehicle.GetInitialPlateFees2(rsVehicles.Get_Fields_String("Class"), rsVehicles.Get_Fields_String("Plate"));
			CommCred = GetComm();
			CalcStateFee = RegFee - CommCred;
			if (blnAdded)
			{
				TotalReg += CalcStateFee;
				TotalInitial += InitFee;
				TotalSpecialty += SpecialFee;
			}
			else
			{
				TotalReg -= CalcStateFee;
				TotalInitial -= InitFee;
				TotalSpecialty -= SpecialFee;
			}
			if (CalcStateFee < 0)
				CalcStateFee = 0;
			CalcStateFee += InitFee + SpecialFee;
			return CalcStateFee;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(string)
		private Decimal CalcLocalFee(ref clsDRWrapper rs, bool blnAdded = true)
		{
			Decimal CalcLocalFee = 0;
			// vbPorter upgrade warning: FeeWrong As object	OnWrite(bool)
			object FeeWrong;
			// - "AutoDim"
			// vbPorter upgrade warning: MilYear As int	OnWriteFCConvert.ToInt32(
			int MilYear = 0;
			float MilRate = 0;
			double Base = 0;
			// vbPorter upgrade warning: Excise As Decimal	OnWrite(int, string)
			Decimal Excise;
			decimal AgentFee = 0;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string tempRes = "";
			clsDRWrapper rsExciseExempt = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs.Get_Fields_String("ResidenceCode").Length < 5)
			{
				tempRes = "0" + rs.Get_Fields_String("ResidenceCode");
			}
			else
			{
				tempRes = FCConvert.ToString(rs.Get_Fields_String("ResidenceCode"));
			}
			if (rs.Get_Fields_String("Class") != "AP")
			{
				if (tempRes == FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode")))
				{
					AgentFee = FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegLocal"));
				}
				else
				{
					AgentFee = FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegOOT"));
				}
			}
			else
			{
				AgentFee = 0;
			}
			rsExciseExempt.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + rs.Get_Fields_String("Class") + "' AND SystemCode = '" + rs.Get_Fields_String("Subclass") + "'", "TWMV0000.vb1");
			if (rsExciseExempt.EndOfFile() != true && rsExciseExempt.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(rsExciseExempt.Get_Fields_String("ExciseRequired")) == "N")
				{
					Excise = 0;
				}
				else
				{
					if (Conversion.Val(rsVehicles.Get_Fields_Int32("MillYear")) == 0)
					{
						MilYear = DateTime.Today.Year - Conversion.Val(rs.Get_Fields("Year")) + 1;
						FeeWrong = true;
					}
					else
					{
						MilYear = FCConvert.ToInt32(Conversion.Val(rsVehicles.Get_Fields_Int32("MillYear"))) + 1;
					}
					if (MilYear > 6)
					{
						MilYear = 6;
					}
					switch (MilYear)
					{
						case 1:
							{
								MilRate = 0.024f;
								break;
							}
						case 2:
							{
								MilRate = 0.0175f;
								break;
							}
						case 3:
							{
								MilRate = 0.0135f;
								break;
							}
						case 4:
							{
								MilRate = 0.01f;
								break;
							}
						case 5:
							{
								MilRate = 0.0065f;
								break;
							}
						case 6:
							{
								MilRate = 0.004f;
								break;
							}
					}
					//end switch
					Base = Conversion.Val(rs.Get_Fields_Int32("BasePrice"));
					if (Base == 0)
					{
						FeeWrong = true;
					}
					Excise = FCConvert.ToDecimal(Strings.Format(MilRate * Base, "#0.00"));
					if (frmFleetExpiringListMMTA.InstancePtr.cmbFleetTypeSelected.Text == "Fleets")
					{
						if (Information.IsDate(rsVehicles.Get_Fields("ExpireDate")))
						{
							if (FCConvert.ToInt32(rsVehicles.Get_Fields_DateTime("ExpireDate").Month) != frmPlateInfo.InstancePtr.ExpMonth)
							{
								Excise = FCConvert.ToDecimal(Strings.Format(Pro(ref Excise, DateDiff()), "#,##0.00"));
							}
						}
						else
						{
							FeeWrong = true;
						}
					}
					if (Excise < 5)
						Excise = 5;
				}
			}
			else
			{
				if (Conversion.Val(rsVehicles.Get_Fields_Int32("MillYear")) == 0)
				{
					MilYear = DateTime.Today.Year - FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("Year"))) + 1;
					FeeWrong = true;
				}
				else
				{
					MilYear = FCConvert.ToInt32(Conversion.Val(rsVehicles.Get_Fields_Int32("MillYear"))) + 1;
				}
				if (MilYear > 6)
				{
					MilYear = 6;
				}
				switch (MilYear)
				{
					case 1:
						{
							MilRate = 0.024f;
							break;
						}
					case 2:
						{
							MilRate = 0.0175f;
							break;
						}
					case 3:
						{
							MilRate = 0.0135f;
							break;
						}
					case 4:
						{
							MilRate = 0.01f;
							break;
						}
					case 5:
						{
							MilRate = 0.0065f;
							break;
						}
					case 6:
						{
							MilRate = 0.004f;
							break;
						}
				}
				//end switch
				Base = Conversion.Val(rs.Get_Fields_Int32("BasePrice"));
				if (Base == 0)
				{
					FeeWrong = true;
				}
				Excise = FCConvert.ToDecimal(Strings.Format(MilRate * Base, "#0.00"));
				if (frmFleetExpiringListMMTA.InstancePtr.cmbFleetTypeSelected.Text == "Fleets")
				{
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rsVehicles.Get_Fields_DateTime("ExpireDate")))
					{
						if (Convert.ToDateTime(rsVehicles.Get_Fields_DateTime("ExpireDate")).Month != frmPlateInfo.InstancePtr.ExpMonth)
						{
							Excise = FCConvert.ToDecimal(Strings.Format(Pro(ref Excise, DateDiff()), "#,##0.00"));
						}
					}
					else
					{
						FeeWrong = true;
					}
				}
				if (Excise < 5)
					Excise = 5;
			}
			if (blnAdded)
			{
				TotalExcise += Excise;
				TotalAgent += AgentFee;
			}
			else
			{
				TotalExcise -= Excise;
				TotalAgent -= AgentFee;
			}
			CalcLocalFee = FCConvert.ToDecimal(Strings.Format(Excise + AgentFee, "#0.00"));
			rsDefaultInfo.Reset();
			return CalcLocalFee;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite
		private Decimal GetReg(string Class, string Subclass, int weight)
		{
			Decimal GetReg = 0;
			// vbPorter upgrade warning: FeeWrong As object	OnWrite(bool)
			object FeeWrong;
			// - "AutoDim"
			int WT;
			clsDRWrapper rsClassFee = new clsDRWrapper();
			clsDRWrapper rsGVWFee = new clsDRWrapper();
			GetReg = 0;
			WT = weight;
			if (FCConvert.ToString(Class) != "AM" && Class != "BH" && FCConvert.ToString(Class) != "BU" && FCConvert.ToString(Class) != "CL" && FCConvert.ToString(Class) != "DV" && FCConvert.ToString(Class) != "FD" && FCConvert.ToString(Class) != "IU" && FCConvert.ToString(Class) != "TL" && FCConvert.ToString(Class) != "MC" && FCConvert.ToString(Class) != "PC" && FCConvert.ToString(Class) != "TR" && FCConvert.ToString(Class) != "VT" && FCConvert.ToString(Class) != "XV" && FCConvert.ToString(Class) != "VX" && FCConvert.ToString(Class) != "EM")
			{
				// tromvs-97 6.25.18 add EM
				Subclass = Class;
			}
			MotorVehicle.Statics.strSql = "SELECT * FROM CLASS WHERE BMVCode = '" + Class + "' And SystemCode =  '" + Subclass + "'";
			rsClassFee.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsClassFee.EndOfFile() != true && rsClassFee.BeginningOfFile() != true)
			{
				rsClassFee.MoveLast();
				rsClassFee.MoveFirst();
				if (fecherFoundation.FCUtils.IsNull(rsClassFee.Get_Fields("RegistrationFee")) == false)
				{
					if (Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.01 || Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.02 || Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.03)
					{
						string tempClass = "";
						tempClass = "FM";
						if (Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.01)
						{
							tempClass = "TK";
						}
						else if (Conversion.Val(rsClassFee.Get_Fields("RegistrationFee")) == 0.03 && WT > 54000 && FCConvert.ToString(Class) != "SE")
						{
							tempClass = "F2";
						}
						else if (Class == "SE")
						{
							tempClass = "SE";
						}
						MotorVehicle.Statics.strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = '" + tempClass + "' AND Low <= " + FCConvert.ToString(WT) + " AND High >= " + FCConvert.ToString(WT);
						rsGVWFee.OpenRecordset(MotorVehicle.Statics.strSql);
						if (rsGVWFee.EndOfFile() != true && rsGVWFee.BeginningOfFile() != true)
						{
							rsGVWFee.MoveLast();
							rsGVWFee.MoveFirst();
							GetReg = FCConvert.ToDecimal(rsGVWFee.Get_Fields("Fee"));
							if (GetReg == 0)
								GetReg = 35;
							rsClassFee.Reset();
							rsGVWFee.Reset();
							return GetReg;
						}
					}
					GetReg = FCConvert.ToDecimal(rsClassFee.Get_Fields("RegistrationFee"));
				}
				else
				{
					GetReg = 0;
				}
			}
			else
			{
				FeeWrong = true;
			}
			rsClassFee.Reset();
			rsGVWFee.Reset();
			return GetReg;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite
		private Decimal GetComm()
		{
			Decimal GetComm = 0;
			if (Conversion.Val(rsVehicles.Get_Fields_String("plate")) > 799999 && Conversion.Val(rsVehicles.Get_Fields_String("plate")) < 900000)
			{
				if (Conversion.Val(rsVehicles.Get_Fields_Int32("RegisteredWeightNew")) > 23000)
				{
					if (Strings.Mid(FCConvert.ToString(rsVehicles.Get_Fields_String("Style")), 2, 1) == "5")
					{
						GetComm = 40;
					}
					else
					{
						GetComm = 0;
					}
				}
			}
			return GetComm;
		}

		private Decimal Pro(ref Decimal x, int y)
		{
			Decimal Pro = 0;
			// vbPorter upgrade warning: total As double	OnWrite(Decimal, double)
			double total;
			total = FCConvert.ToDouble(x / 12);
			total *= y;
			Pro = FCConvert.ToDecimal(total);
			return Pro;
		}

		private int DateDiff()
		{
			int DateDiff = 0;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x = 0;
			if (FCConvert.ToInt32(rsVehicles.Get_Fields_DateTime("ExpireDate").Month) < frmPlateInfo.InstancePtr.ExpMonth)
			{
				x = ((frmPlateInfo.InstancePtr.ExpMonth - FCConvert.ToInt32(rsVehicles.Get_Fields_DateTime("ExpireDate").Month) + 1));
			}
			else
			{
				x = ((12 - (FCConvert.ToInt32(rsVehicles.Get_Fields_DateTime("ExpireDate").Month - frmPlateInfo.InstancePtr.ExpMonth))) - 1);
			}
			if (x == 0)
			{
				x = 12;
			}
			DateDiff = x;
			return DateDiff;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFleetName As object	OnWrite(string)
			PageCounter = 0;
			if (Conversion.Val(rsFleets.Get_Fields_Int32("ExpiryMonth")) != 99)
			{
				fldFleetName.Text = "FLEET " + rsFleets.Get_Fields_Int32("FleetNumber") + "  " + fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleets.Get_Fields_String("FullName")));
			}
			else
			{
				fldFleetName.Text = "GROUP " + rsFleets.Get_Fields_Int32("FleetNumber") + "  " + fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleets.Get_Fields_String("FullName")));
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptFleetReminderList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptFleetReminderList properties;
			//rptFleetReminderList.Caption	= "Fleet Reminder List";
			//rptFleetReminderList.Icon	= "rptFleetExpirationList.dsx":0000";
			//rptFleetReminderList.Left	= 0;
			//rptFleetReminderList.Top	= 0;
			//rptFleetReminderList.Width	= 11880;
			//rptFleetReminderList.Height	= 8595;
			//rptFleetReminderList.StartUpPosition	= 3;
			//rptFleetReminderList.SectionData	= "rptFleetExpirationList.dsx":508A;
			//End Unmaped Properties
		}
	}
}
