//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
    public partial class frmPrePrints : BaseForm
    {
        public frmPrePrints()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmPrePrints InstancePtr
        {
            get
            {
                return (frmPrePrints)Sys.GetInstance(typeof(frmPrePrints));
            }
        }

        protected frmPrePrints _InstancePtr = null;
        //=========================================================
        // vbPorter upgrade warning: strPrintLine As FixedString	OnWrite(string)
        FCFixedString strPrintLine = new FCFixedString(70);
        // vbPorter upgrade warning: DiskExpireDate As DateTime	OnWrite(string, int)
        DateTime DiskExpireDate;
        // vbPorter upgrade warning: TransactionDate As DateTime	OnWrite(string)
        DateTime TransactionDate;
        clsDRWrapper rsA = new clsDRWrapper();
        clsDRWrapper rsB = new clsDRWrapper();
        string strDrive;
        string strStateLevel;
        string strMVLevel;
        int lngMaxMV;
        string strResCode;
        int lngMCounter;
        string FixPlate;
        string Plate8 = "";
        string VIN = "";
        string TRecord;
        string found;
        string Rec1;
        string BadFlag;
        int BadFlagCount;
        int LineCount;
        public string strADDEdit = "";
        public bool blnBMVDataRefresh;
        // Dim WithEvents temp As clsSQLServerDataConnection.PartyUtil
        int PlateCol;
        int OwnerCol;
        int YearCol;
        int MakeCol;
        int ModelCol;
        private string updateFileName = "";
        private void cmdChangeDrive_Click(object sender, System.EventArgs e)
        {
            FCFileSystem fs = new FCFileSystem();

            MDIParent.InstancePtr.CommonDialog1.Filter = "*.*";
            MDIParent.InstancePtr.CommonDialog1.CancelError = true; 
            try
            {
                MDIParent.InstancePtr.CommonDialog1.ShowOpen();
                updateFileName = MDIParent.InstancePtr.CommonDialog1.FileName;

				lblDrive.Text = "Selected file: " + Path.GetFileName(MDIParent.InstancePtr.CommonDialog1.FileName);
                cmdProcess.Enabled = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void cmdPrint_Click(object sender, System.EventArgs e)
        {
            bool blnRecords;
            blnRecords = true;
            if (tabVehicleInfo.SelectedIndex == 0)
            {
                if (vsAdded.Rows <= 1)
                {
                    blnRecords = false;
                }
            }
            else if (tabVehicleInfo.SelectedIndex == 1)
            {
                if (vsEdited.Rows <= 1)
                {
                    blnRecords = false;
                }
            }
            else
            {
                if (vsNotUpdated.Rows <= 1)
                {
                    blnRecords = false;
                }
            }
            if (!blnRecords)
            {
                MessageBox.Show("No Information", "No Information Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                frmReportViewer.InstancePtr.Init(rptBMV.InstancePtr);
            }
        }

        private void cmdProcess_Click(object sender, System.EventArgs e)
        {
            ShowWait(true);


            FCUtils.StartTask(this, () =>
            {
                try
                {
                    ProcessRoutine();

                    if (MotorVehicle.Statics.intAddedPartyCounter <= 0) return;

                    if (MessageBox.Show("Central Parties were added to the system based on data from the BMV Update Disk.  Would you like to use the merge utility to see if the added parties could be merged into any parties already in the system?", "Merge Added Parties?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

                    int counter;
                    string strTemp = "";
                    for (counter = 0; counter <= MotorVehicle.Statics.intAddedPartyCounter - 1; counter++)
                    {
                        strTemp += FCConvert.ToString(MotorVehicle.Statics.lngAddedParties[counter]) + ", ";
                    }
                    strTemp = Strings.Left(strTemp, strTemp.Length - 2);
                    frmCentralPartyMerge.InstancePtr.Init(strTemp);

                }
                finally
                {
                    EndWait();
                    FCUtils.ApplicationUpdate(this);
                }
            });


           }

        private void ProcessRoutine()
        {
            string strEA = "";
            int counter;
            DialogResult ans = 0;

            clsDRWrapper rsSpecialRes = new clsDRWrapper();
            // 
            rsA.OpenRecordset("SELECT * FROM DefaultInfo");
            rsA.MoveLast();
            rsA.MoveFirst();
            strResCode = FCConvert.ToString(rsA.Get_Fields_String("ResidenceCode"));

            rsA.Reset();
        TryOver:
			FCFileSystem.FileOpen(2, updateFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.BMVRecord));
            if (fecherFoundation.Information.Err().Number == 71)
            {
                ans = MessageBox.Show("Put disk in drive A.  Hit enter when ready.", "Put Disk in Drive", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (ans == DialogResult.Cancel)
                {
                    frmPrePrints.InstancePtr.Close();
                    return;
                }
                else
                {
                    fecherFoundation.Information.Err().Clear();
                    goto TryOver;
                }
            }
            else if (fecherFoundation.Information.Err().Number == 52)
            {
                ans = MessageBox.Show("Bad file name/number.  Hit enter when ready.", "Bad File Name/Number", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (ans == DialogResult.Cancel)
                {
                    frmPrePrints.InstancePtr.Close();
                    return;
                }
                else
                {
                    fecherFoundation.Information.Err().Clear();
                    goto TryOver;
                }
            }
            
            MotorVehicle.Statics.intChanges = 0;
            MotorVehicle.Statics.intAddedPartyCounter = 0;
            MotorVehicle.Statics.strPrePrintPlate = new string[0 + 1];
            MotorVehicle.Statics.strSystemInfo = new string[0 + 1];
            MotorVehicle.Statics.strStateInfo = new string[0 + 1];
            // 
            ResetVehicleInfo();
            vsAdded.Redraw = false;
            vsEdited.Redraw = false;
            vsNotUpdated.Redraw = false;
            for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(2) / Marshal.SizeOf(MotorVehicle.Statics.BMVRecord)); counter++)
            {
                FCFileSystem.FileGet(2, ref MotorVehicle.Statics.BMVRecord, counter);
                if (counter % 10 == 0)
                {
                    UpdateWait("Processing File ... " + FCConvert.ToString(counter) + " of " + FCConvert.ToString(FCFileSystem.LOF(2) / Marshal.SizeOf(MotorVehicle.Statics.BMVRecord)) + " Records Completed");
                }
                FixPlate = "N";
                Plate8 = fecherFoundation.Strings.RTrim(MotorVehicle.Statics.BMVRecord.plate);
                VIN = fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.VIN);
                MotorVehicle.Statics.Class = MotorVehicle.Statics.BMVRecord.Class;
                DiskExpireDate = FCConvert.ToDateTime(Strings.Mid(MotorVehicle.Statics.BMVRecord.ExpirationDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.ExpirationDate, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.ExpirationDate, 4));
                TransactionDate = FCConvert.ToDateTime(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.TransactionDate, 4));
                MotorVehicle.Statics.strSql = "SELECT * FROM Master WHERE (Plate = '" + fecherFoundation.Strings.Trim(Plate8) + "' OR PlateStripped = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.PlateStripped) + "') And " + "Class = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.Class) + "' And Status <> 'T' And rtrim(Vin) = '" + fecherFoundation.Strings.Trim(VIN) + "' ORDER BY DateUpdated DESC";
                Debug.WriteLine("Start " + FCConvert.ToString(DateTime.Now));
                rsA.OpenRecordset(MotorVehicle.Statics.strSql);
                Debug.WriteLine("Found Record " + FCConvert.ToString(DateTime.Now));
                strEA = "";
                if (rsA.EndOfFile() != true && rsA.BeginningOfFile() != true)
                {
                    rsA.MoveLast();
                    rsA.MoveFirst();
                    if (rsA.IsFieldNull("DateUpdated"))
                    {
                        rsA.Edit();
                        strEA = "E";
                    }
                    else if (fecherFoundation.DateAndTime.DateValue(Strings.Format(rsA.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy")).ToOADate() <= TransactionDate.ToOADate())
                    {
                        rsA.Edit();
                        strEA = "E";
                    }
                    else
                    {
                        AddGridRow(ref rsA, "N");
                        goto Tag2005;
                    }
                    BuildRecord(ref rsA, ref strEA);
                    AddGridRow(ref rsA, strEA);
                }
                else
                {
                    bool executeIgnoreVIN = false;
                    if (VIN == "0")
                    {
                        executeIgnoreVIN = true;
                        goto IgnoreVIN;
                    }
                    MotorVehicle.Statics.strSql = "SELECT * FROM Master WHERE rtrim(Plate) = 'NEW' And " + "Status <> 'T' And rtrim(Vin) = '" + fecherFoundation.Strings.Trim(VIN) + "' ORDER BY DateUpdated DESC";
                    /*? On Error GoTo 0 */
                    rsB.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsB.EndOfFile() != true && rsB.BeginningOfFile() != true)
                    {
                        rsB.MoveLast();
                        rsB.MoveFirst();
                        if (rsB.IsFieldNull("DateUpdated"))
                        {
                            rsB.Edit();
                            strEA = "E";
                        }
                        else if (fecherFoundation.DateAndTime.DateValue(Strings.Format(rsB.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy")).ToOADate() <= TransactionDate.ToOADate())
                        {
                            rsB.Edit();
                            strEA = "E";
                        }
                        else
                        {
                            AddGridRow(ref rsB, "N");
                            goto Tag2005;
                        }
                        BuildRecord(ref rsB, ref strEA);
                        AddGridRow(ref rsB, strEA);
                    }
                    else
                    {
                        // kk11072016 tromv-1232  Check for Pre-print using old data. Look for the pre-print record in ActivityMaster (transferred)
                        MotorVehicle.Statics.strSql = "SELECT * FROM Master WHERE (Plate = '" + fecherFoundation.Strings.Trim(Plate8) + "' OR PlateStripped = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.PlateStripped) + "') And " + "Class = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.Class) + "' And Status <> 'T' ORDER BY DateUpdated DESC";
                        /*? On Error GoTo 0 */
                        rsA.OpenRecordset(MotorVehicle.Statics.strSql);
                        if (rsA.EndOfFile() != true && rsA.BeginningOfFile() != true)
                        {
                            rsA.MoveLast();
                            rsA.MoveFirst();
                            if (!rsA.IsFieldNull("DateUpdated") && rsA.Get_Fields_DateTime("DateUpdated").ToOADate() >= TransactionDate.ToOADate())
                            {
                                AddGridRow(ref rsA, "N");
                                goto Tag2005;
                            }
                            else
                            {
                                MotorVehicle.Statics.strSql = "SELECT * FROM ActivityMaster WHERE (Plate = '" + fecherFoundation.Strings.Trim(Plate8) + "' OR PlateStripped = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.PlateStripped) + "') And " + "Class = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.Class) + "' And Status <> 'T' And rtrim(ltrim(Vin)) = '" + fecherFoundation.Strings.Trim(VIN) + "' ORDER BY DateUpdated DESC";
                                /*? On Error GoTo 0 */
                                rsB.OpenRecordset(MotorVehicle.Statics.strSql);
                                if (rsB.EndOfFile() != true && rsB.BeginningOfFile() != true)
                                {
                                    rsB.MoveLast();
                                    rsB.MoveFirst();
                                    if (fecherFoundation.DateAndTime.DateValue(fecherFoundation.Strings.Trim(FCConvert.ToString(rsA.Get_Fields_DateTime("DateUpdated")))).ToOADate() >= fecherFoundation.DateAndTime.DateValue(fecherFoundation.Strings.Trim(FCConvert.ToString(rsB.Get_Fields_DateTime("DateUpdated")))).ToOADate())
                                    {
                                        AddGridRow(ref rsA, "N");
                                        goto Tag2005;
                                    }
                                }
                                // kk05112017 tromv-1286  Change to overwrite the Master record if the file has newer data, even if the VIN doesn't match
                                // We reach this code if a master record exists with a different VIN and we didn't find an ActivityMaster record with the same VIN as the file
                                // or the Master record with a different VIN is newer than the ActivityMaster record (i.e. a newer transfer was done at the town)
                                // kk01132016 tromv-1260  Reset rsB to Master table so Type 99 records aren't added to ArchiveMaster
                                // rsB.OpenRecordset "SELECT * FROM Master WHERE ID = -1"
                                rsA.Edit();
                                strEA = "E";
                                BuildRecord(ref rsA, ref strEA);
                                AddGridRow(ref rsA, strEA);
                                goto Tag2005;
                            }
                        }
                        executeIgnoreVIN = true;
                        goto IgnoreVIN;
                    }
                IgnoreVIN:;
                    if (executeIgnoreVIN)
                    {
                        // We reach here if the VIN in the file is "0" or there is no matching Master record for this plate and class
                        rsA.AddNew();
                        strEA = "A";
                        BuildRecord(ref rsA, ref strEA);
                        AddGridRow(ref rsA, strEA);
                        executeIgnoreVIN = false;
                    }
                }
            Tag2005:
                ;
            }
            FCFileSystem.FileClose(2);
            // Fill Grid With Adds
            vsAdded.Redraw = true;
            vsEdited.Redraw = true;
            vsNotUpdated.Redraw = true;
            //fraProcessing.Visible = false;
            UpdateTabTotals();
            MessageBox.Show("Process completed successfully!", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // 
            if (MotorVehicle.Statics.intChanges > 0)
            {
                MessageBox.Show("There are vehicles with information that did not match the state's records.  Please review the following report and make any necessary changes in New to System Update.", "Vehicle Changes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //App.DoEvents();
                frmReportViewer.InstancePtr.Init(rptChangesList.InstancePtr);
            }
        }

        private void AddGridRow(ref clsDRWrapper rs3, string strType)
        {
            string strOwner;
            strOwner = MotorVehicle.GetPartyNameMiddleInitial(rs3.Get_Fields_Int32("PartyID1"), true);
            if (strType == "A")
            {
                vsAdded.Rows += 1;
                vsAdded.TextMatrix(vsAdded.Rows - 1, PlateCol, rs3.Get_Fields_String("plate"));
                vsAdded.TextMatrix(vsAdded.Rows - 1, OwnerCol, strOwner);
                vsAdded.TextMatrix(vsAdded.Rows - 1, YearCol, rs3.Get_Fields("Year"));
                vsAdded.TextMatrix(vsAdded.Rows - 1, MakeCol, rs3.Get_Fields_String("Make"));
                vsAdded.TextMatrix(vsAdded.Rows - 1, ModelCol, rs3.Get_Fields_String("Model"));
                rs3.Update();
            }
            else if (strType == "E")
            {
                vsEdited.Rows += 1;
                vsEdited.TextMatrix(vsEdited.Rows - 1, PlateCol, rs3.Get_Fields_String("plate"));
                vsEdited.TextMatrix(vsEdited.Rows - 1, OwnerCol, strOwner);
                vsEdited.TextMatrix(vsEdited.Rows - 1, YearCol, rs3.Get_Fields("Year"));
                vsEdited.TextMatrix(vsEdited.Rows - 1, MakeCol, rs3.Get_Fields_String("Make"));
                vsEdited.TextMatrix(vsEdited.Rows - 1, ModelCol, rs3.Get_Fields_String("Model"));
                rs3.Update();
            }
            else
            {
                vsNotUpdated.Rows += 1;
                vsNotUpdated.TextMatrix(vsNotUpdated.Rows - 1, PlateCol, rs3.Get_Fields_String("plate"));
                vsNotUpdated.TextMatrix(vsNotUpdated.Rows - 1, OwnerCol, strOwner);
                vsNotUpdated.TextMatrix(vsNotUpdated.Rows - 1, YearCol, rs3.Get_Fields("Year"));
                vsNotUpdated.TextMatrix(vsNotUpdated.Rows - 1, MakeCol, rs3.Get_Fields_String("Make"));
                vsNotUpdated.TextMatrix(vsNotUpdated.Rows - 1, ModelCol, rs3.Get_Fields_String("Model"));
            }
        }

        private void ResetVehicleInfo()
        {
            vsAdded.Rows = 1;
            vsEdited.Rows = 1;
            vsNotUpdated.Rows = 1;
            tabVehicleInfo.TabPages[0].Text = "Added";
            tabVehicleInfo.TabPages[1].Text = "Edited";
            tabVehicleInfo.TabPages[2].Text = "Not Updated";
        }

        private void UpdateTabTotals()
        {
            tabVehicleInfo.TabPages[0].Text = "Added (" + FCConvert.ToString(vsAdded.Rows - 1) + ")";
            tabVehicleInfo.TabPages[1].Text = "Edited (" + FCConvert.ToString(vsEdited.Rows - 1) + ")";
            tabVehicleInfo.TabPages[2].Text = "Not Updated (" + FCConvert.ToString(vsNotUpdated.Rows - 1) + ")";
        }

        private void BuildRecord(ref clsDRWrapper rs1, ref string strEditAdd)
        {
            // vbPorter upgrade warning: strClass As string	OnWrite(FixedString)
            string strClass;
            string strPlate;
            string strEffectiveDate;
            string strExpiresDate;
            string strVin;
            string strFee = "";
            string strNetWeight;
            string strGrossWeight;
            // vbPorter upgrade warning: strOwner1 As string	OnWrite(FixedString)
            string strOwner1;
            string strDOTNumber;
            string strMilYear = "";
            string strTemp = "";
            // vbPorter upgrade warning: strYear As string	OnWrite(FixedString)
            string strYear = "";
            clsDRWrapper rsParty = new clsDRWrapper();
            int lngDeletedID;
            int lngDeletedID2;
            cCreateGUID clsGuid = new cCreateGUID();
            clsDRWrapper rsNameView = new clsDRWrapper();
            clsPrintCodes oPrtCodes;
            string[] originalOwners = new string[3] { "", "", "" };
            string tempName = "";

            strClass = MotorVehicle.Statics.BMVRecord.Class;
            strPlate = fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.plate);
            strEffectiveDate = Strings.Mid(MotorVehicle.Statics.BMVRecord.EffectiveDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.EffectiveDate, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.EffectiveDate, 4);
            strExpiresDate = Strings.Mid(MotorVehicle.Statics.BMVRecord.ExpirationDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.ExpirationDate, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.ExpirationDate, 4);
            strVin = fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.VIN);
            // strFee = Trim(BMVRecord.RegistrationFee)
            strNetWeight = fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.NetWeight);
            strGrossWeight = fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.RegisteredWeight);
            strOwner1 = MotorVehicle.Statics.BMVRecord.Owner1;
            strDOTNumber = fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DOTNumber);
            // 
            lngDeletedID = 0;
            lngDeletedID2 = 0;
            if (rs1.Get_Fields_Int32("PartyID1") > 0)
            {
                originalOwners[0] = MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID1")).Trim();
            }

            if (rs1.Get_Fields_Int32("PartyID2") > 0)
            {
                originalOwners[1] = MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2")).Trim();
            }

            originalOwners[2] = MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID3")).Trim();

            if (strEditAdd == "A")
            {
                rs1.Set_Fields("TransactionType", "99");
                rs1.Set_Fields("MillYear", 0);
                rs1.Set_Fields("RegRateFull", 0);
                rs1.Set_Fields("Trailer", "N");
                rs1.Set_Fields("InsuranceInitials", "");
                rs1.Set_Fields("Subclass", "!!");
                rs1.Set_Fields("BasePrice", 0);
                rs1.Set_Fields("TransferCreditFull", 0);
                rs1.Set_Fields("TransferCreditUsed", 0);
                rs1.Set_Fields("DuplicateRegistrationFee", 0);
                rs1.Set_Fields("CommercialCredit", 0);
                rs1.Set_Fields("LocalPaid", 0);
                rs1.Set_Fields("StatePaid", 0);
                rs1.Set_Fields("ReservePlate", 0);
                rs1.Set_Fields("ExciseCreditFull", 0);
                rs1.Set_Fields("ExciseCreditUsed", 0);
                rs1.Set_Fields("ExciseCreditMVR3", 0);
                rs1.Set_Fields("outofrotation", 0);
                rs1.Set_Fields("PlateFeeReReg", 0);
                rs1.Set_Fields("TransferFee", 0);
                rs1.Set_Fields("SalesTax", 0);
                rs1.Set_Fields("TitleFee", 0);
                rs1.Set_Fields("Value", 0);
                rs1.Set_Fields("FleetNumber", 0);
                rs1.Set_Fields("PartyID2", 0);
                rs1.Set_Fields("PartyID3", 0);
                rs1.Set_Fields("AmputeeVet", 0);
                rs1.Set_Fields("PriorTitleNumber", "");
                rs1.Set_Fields("PriorTitleState", "");
                rs1.Set_Fields("CTANumber", 0);
                rs1.Set_Fields("InfoCodes", "");
                rs1.Set_Fields("InfoMessage", "");
                rs1.Set_Fields("AgentFee", 0);
                rs1.Set_Fields("GIFTCERTIFICATEAMOUNT", 0);
                rs1.Set_Fields("GiftCertificateNumber", 0);
                rs1.Set_Fields("CTANumber", 0);
                rs1.Set_Fields("ForcedPlate", "N");
                rs1.Set_Fields("InitialFee", 0);
                rs1.Set_Fields("ReplacementFee", 0);
                rs1.Set_Fields("OldMVR3", 0);
                rs1.Set_Fields("ReservePlateYN", 0);
                rs1.Set_Fields("ExciseTaxFull", 0);
                rs1.Set_Fields("ExciseTaxCharged", 0);
                rs1.Set_Fields("StickerFee", 0);
                if (MotorVehicle.Statics.BMVRecord.DOB1 == "99999999")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner1)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 1);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner1));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode1", "C");
                    rs1.Set_Fields("PartyID1", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName1", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner1", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg1LastName", "");
                    rs1.Set_Fields("Reg1FirstName", "");
                    rs1.Set_Fields("Reg1MI", "");
                    rs1.Set_Fields("Reg1Designation", "");
                }
                else
                {
                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 1, 50);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 51, 20);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 71, 1);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 72, 3);
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 0);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                        rsParty.Set_Fields("LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                        rsParty.Set_Fields("MiddleName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                        rsParty.Set_Fields("Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode1", "I");
                    rs1.Set_Fields("PartyID1", rsParty.Get_Fields_Int32("ID"));
                    rsNameView.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsParty.Get_Fields_Int32("ID"), "CentralParties");
                    rs1.Set_Fields("PartyName1", fecherFoundation.Strings.UCase(FCConvert.ToString(rsNameView.Get_Fields_String("FullNameMiddleInitial"))));
                    rs1.Set_Fields("Owner1", "");
                    rs1.Set_Fields("Reg1LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                    rs1.Set_Fields("Reg1FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                    rs1.Set_Fields("Reg1MI", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                    rs1.Set_Fields("Reg1Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                    if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DOB1) != "")
                    {
                        if (Information.IsDate(Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB1, 4)))
                        {
                            rs1.Set_Fields("DOB1", Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB1, 4));
                        }
                    }
                }
                if (MotorVehicle.Statics.BMVRecord.DOB2 != "99999999" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DOB2) != "")
                {
                    MotorVehicle.Statics.BMVRecordIndividualOwner = new MotorVehicle.IndividualOwner(0);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 1, 50);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 51, 20);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 71, 1);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 72, 3);
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 0);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                        rsParty.Set_Fields("LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                        rsParty.Set_Fields("MiddleName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                        rsParty.Set_Fields("Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode2", "I");
                    rs1.Set_Fields("PartyID2", rsParty.Get_Fields_Int32("ID"));
                    rsNameView.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsParty.Get_Fields_Int32("ID"), "CentralParties");
                    rs1.Set_Fields("PartyName2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsNameView.Get_Fields_String("FullName"))));
                    rs1.Set_Fields("Owner2", "");
                    rs1.Set_Fields("Reg2LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                    rs1.Set_Fields("Reg2FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                    rs1.Set_Fields("Reg2MI", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                    rs1.Set_Fields("Reg2Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                    if (Information.IsDate(Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB2, 4)))
                    {
                        rs1.Set_Fields("DOB2", Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB2, 4));
                    }
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName) != "" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2) == "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 1);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode2", "D");
                    rs1.Set_Fields("PartyID2", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg2LastName", "");
                    rs1.Set_Fields("Reg2FirstName", "");
                    rs1.Set_Fields("Reg2MI", "");
                    rs1.Set_Fields("Reg2Designation", "");
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2) != "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile())
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 1);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode2", "C");
                    rs1.Set_Fields("PartyID2", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg2LastName", "");
                    rs1.Set_Fields("Reg2FirstName", "");
                    rs1.Set_Fields("Reg2MI", "");
                    rs1.Set_Fields("Reg2Designation", "");
                }
                else
                {
                    rs1.Set_Fields("OwnerCode2", "N");
                    rs1.Set_Fields("PartyID2", 0);
                    rs1.Set_Fields("PartyName2", "");
                    rs1.Set_Fields("Owner2", "");
                    rs1.Set_Fields("Reg2LastName", "");
                    rs1.Set_Fields("Reg2FirstName", "");
                    rs1.Set_Fields("Reg2MI", "");
                    rs1.Set_Fields("Reg2Designation", "");
                }
                if (MotorVehicle.Statics.BMVRecord.DOB3 != "99999999" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DOB3) != "")
                {
                    if (Information.IsDate(Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB3, 4)))
                    {
                        rs1.Set_Fields("DOB3", Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB3, 4));
                    }
                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 1, 50);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 51, 20);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 71, 1);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 72, 3);
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile())
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 0);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                        rsParty.Set_Fields("LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                        rsParty.Set_Fields("MiddleName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                        rsParty.Set_Fields("Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode3", "I");
                    rs1.Set_Fields("PartyID3", rsParty.Get_Fields_Int32("ID"));
                    rsNameView.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsParty.Get_Fields_Int32("ID"), "CentralParties");
                    rs1.Set_Fields("PartyName3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsNameView.Get_Fields_String("FullNameMiddleInitial"))));
                    rs1.Set_Fields("Owner3", "");
                    rs1.Set_Fields("Reg3LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                    rs1.Set_Fields("Reg3FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                    rs1.Set_Fields("Reg3MI", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                    rs1.Set_Fields("Reg3Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName) != "" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3) == "" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2) != "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile())
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 1);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode3", "D");
                    rs1.Set_Fields("PartyID3", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg3LastName", "");
                    rs1.Set_Fields("Reg3FirstName", "");
                    rs1.Set_Fields("Reg3MI", "");
                    rs1.Set_Fields("Reg3Designation", "");
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3) != "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile())
                    {
                        rsParty.AddNew();
                        rsParty.Set_Fields("PartyType", 1);
                        rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3));
                        rsParty.Set_Fields("PartyGuid", clsGuid.CreateGUID());
                        rsParty.Set_Fields("DateCreated", DateTime.Today);
                        rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                        rsParty.Update();
                        Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                        MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                        MotorVehicle.Statics.intAddedPartyCounter += 1;
                    }
                    rs1.Set_Fields("OwnerCode3", "C");
                    rs1.Set_Fields("PartyID3", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg3LastName", "");
                    rs1.Set_Fields("Reg3FirstName", "");
                    rs1.Set_Fields("Reg3MI", "");
                    rs1.Set_Fields("Reg3Designation", "");
                }
                else
                {
                    rs1.Set_Fields("OwnerCode3", "N");
                    rs1.Set_Fields("PartyID3", 0);
                    rs1.Set_Fields("PartyName3", "");
                    rs1.Set_Fields("Owner3", "");
                    rs1.Set_Fields("Reg3LastName", "");
                    rs1.Set_Fields("Reg3FirstName", "");
                    rs1.Set_Fields("Reg3MI", "");
                    rs1.Set_Fields("Reg3Designation", "");
                }
            }
            else
            {
                if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Year) != "" && Information.IsNumeric(MotorVehicle.Statics.BMVRecord.Year))
                {
                    strYear = MotorVehicle.Statics.BMVRecord.Year;
                }
                if (fecherFoundation.Strings.Trim(rs1.Get_Fields_String("Vin")) != fecherFoundation.Strings.Trim(strVin) || Conversion.Val(strYear) != Conversion.Val(fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields("Year")))) || fecherFoundation.Strings.Trim(rs1.Get_Fields_String("make")) != fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.make) || fecherFoundation.Strings.Trim(rs1.Get_Fields_String("model")) != fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.model))
                {
                    AddChangesToList(strPlate,
                        rs1.Get_Fields_String("Year") + " " + rs1.Get_Fields_String("make") + " " +
                        rs1.Get_Fields_String("model"),
                        strYear + " " + MotorVehicle.Statics.BMVRecord.make.Trim() + " " +
                        MotorVehicle.Statics.BMVRecord.model.Trim());
                }
                if (MotorVehicle.Statics.BMVRecord.DOB1 == "99999999")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + rs1.Get_Fields_Int32("PartyID1") + " OR ID = " + rs1.Get_Fields_Int32("PartyID2") + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner1)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID1"))
                        {
                            // do nothing
                        }
                        else if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID2"))
                        {
                            rs1.Set_Fields("PartyID2", rs1.Get_Fields_Int32("PartyID1"));
                        }
                        else if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID3"))
                        {
                            rs1.Set_Fields("PartyID3", rs1.Get_Fields_Int32("PartyID1"));
                        }
                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner1)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            if (!ContainsName(originalOwners, MotorVehicle.Statics.BMVRecord.Owner1))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID1")),
                                    MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID")));
                            }
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 1);
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner1));
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));

                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID1")),
                                    MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID")) + "  (NEW)");
                            }
                            lngDeletedID = rs1.Get_Fields_Int32("PartyID1");
                        }
                    }
                    rs1.Set_Fields("OwnerCode1", "C");
                    rs1.Set_Fields("PartyID1", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName1", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner1", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg1LastName", "");
                    rs1.Set_Fields("Reg1FirstName", "");
                    rs1.Set_Fields("Reg1MI", "");
                    rs1.Set_Fields("Reg1Designation", "");
                }
                else
                {
                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 1, 50);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 51, 20);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 71, 1);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 72, 3);
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + rs1.Get_Fields_Int32("PartyID1") + " OR ID = " + rs1.Get_Fields_Int32("PartyID2") + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID1"))
                        {
                            // do nothing
                        }
                        else if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID2"))
                        {
                            rs1.Set_Fields("PartyID2", rs1.Get_Fields_Int32("PartyID1"));
                        }
                        else if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID3"))
                        {
                            rs1.Set_Fields("PartyID3", rs1.Get_Fields_Int32("PartyID1"));
                        }
                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID1")), tempName);
                            }
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 0);
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy",
                                modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                            rsParty.Set_Fields("LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                            rsParty.Set_Fields("MiddleName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                            rsParty.Set_Fields("Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID1")), MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID")) + "  (NEW)");
                            }
                            lngDeletedID = rs1.Get_Fields_Int32("PartyID1");
                        }
                    }
                    rs1.Set_Fields("OwnerCode1", "I");
                    rs1.Set_Fields("PartyID1", rsParty.Get_Fields_Int32("ID"));
                    rsNameView.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsParty.Get_Fields_Int32("ID"), "CentralParties");
                    rs1.Set_Fields("PartyName1", fecherFoundation.Strings.UCase(FCConvert.ToString(rsNameView.Get_Fields_String("FullNameMiddleInitial"))));
                    rs1.Set_Fields("Owner1", "");
                    rs1.Set_Fields("Reg1LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                    rs1.Set_Fields("Reg1FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                    rs1.Set_Fields("Reg1MI", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                    rs1.Set_Fields("Reg1Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                    if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DOB1) != "")
                    {
                        if (Information.IsDate(Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB1, 4)))
                        {
                            rs1.Set_Fields("DOB1", Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB1, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB1, 4));
                        }
                    }
                }
                if (MotorVehicle.Statics.BMVRecord.DOB2 != "99999999" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DOB2) != "")
                {
                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 1, 50);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 51, 20);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 71, 1);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner2, 72, 3);
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + FCConvert.ToString(lngDeletedID) + " OR ID = " + rs1.Get_Fields_Int32("PartyID2") + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID2"))
                        {
                            // do nothing
                        }
                        else if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID3"))
                        {
                            rs1.Set_Fields("PartyID3", rs1.Get_Fields_Int32("PartyID2"));
                        }
                        else if (FCConvert.ToInt32(rsParty.Get_Fields_Int32("ID")) == lngDeletedID)
                        {
                            // do nothing
                        }
                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2")), tempName);
                            }
                            Array.Resize(ref MotorVehicle.Statics.strPrePrintPlate, MotorVehicle.Statics.intChanges + 1);
                            Array.Resize(ref MotorVehicle.Statics.strSystemInfo, MotorVehicle.Statics.intChanges + 1);
                            Array.Resize(ref MotorVehicle.Statics.strStateInfo, MotorVehicle.Statics.intChanges + 1);
                            MotorVehicle.Statics.strPrePrintPlate[MotorVehicle.Statics.intChanges] = strPlate;
                            MotorVehicle.Statics.strSystemInfo[MotorVehicle.Statics.intChanges] = rs1.Get_Fields_Int32("PartyID2") + " " + MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2"));
                            MotorVehicle.Statics.strStateInfo[MotorVehicle.Statics.intChanges] = rsParty.Get_Fields_Int32("ID") + " " + MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            MotorVehicle.Statics.intChanges += 1;
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 0);
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                            rsParty.Set_Fields("LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                            rsParty.Set_Fields("MiddleName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                            rsParty.Set_Fields("Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2")), MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID")) + "  (NEW)");
                            }
                            lngDeletedID2 = rs1.Get_Fields_Int32("PartyID2");
                        }
                    }
                    rs1.Set_Fields("OwnerCode2", "I");
                    rs1.Set_Fields("PartyID2", rsParty.Get_Fields_Int32("ID"));
                    rsNameView.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsParty.Get_Fields_Int32("ID"), "CentralParties");
                    rs1.Set_Fields("PartyName2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsNameView.Get_Fields_String("FullName"))));
                    rs1.Set_Fields("Owner2", "");
                    rs1.Set_Fields("Reg2LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                    rs1.Set_Fields("Reg2FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                    rs1.Set_Fields("Reg2MI", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                    rs1.Set_Fields("Reg2Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                    if (Information.IsDate(Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB2, 4)))
                    {
                        rs1.Set_Fields("DOB2", Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB2, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB2, 4));
                    }
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName) != "" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2) == "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + FCConvert.ToString(lngDeletedID) + " OR ID = " + rs1.Get_Fields_Int32("PartyID2") + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName)))) + "'", "CentralParties");
                    if (!rsParty.EndOfFile())
                    {
                        if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID2"))
                        {
                            // do nothing
                        }
                        else if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID3"))
                        {
                            rs1.Set_Fields("PartyID3", rs1.Get_Fields_Int32("PartyID2"));
                        }
                        else if (FCConvert.ToInt32(rsParty.Get_Fields_Int32("ID")) == lngDeletedID)
                        {
                            // do nothing
                        }
                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, MotorVehicle.Statics.BMVRecord.DBAName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2")), tempName);
                            }
                            Array.Resize(ref MotorVehicle.Statics.strPrePrintPlate, MotorVehicle.Statics.intChanges + 1);
                            Array.Resize(ref MotorVehicle.Statics.strSystemInfo, MotorVehicle.Statics.intChanges + 1);
                            Array.Resize(ref MotorVehicle.Statics.strStateInfo, MotorVehicle.Statics.intChanges + 1);
                            MotorVehicle.Statics.strPrePrintPlate[MotorVehicle.Statics.intChanges] = strPlate;
                            MotorVehicle.Statics.strSystemInfo[MotorVehicle.Statics.intChanges] = rs1.Get_Fields_Int32("PartyID2") + " " + MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2"));
                            MotorVehicle.Statics.strStateInfo[MotorVehicle.Statics.intChanges] = rsParty.Get_Fields_Int32("ID") + " " + MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            MotorVehicle.Statics.intChanges += 1;
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 1);
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName));
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2")), tempName);
                            }
                            lngDeletedID2 = rs1.Get_Fields_Int32("PartyID2");
                        }
                    }
                    rs1.Set_Fields("OwnerCode2", "D");
                    rs1.Set_Fields("PartyID2", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg2LastName", "");
                    rs1.Set_Fields("Reg2FirstName", "");
                    rs1.Set_Fields("Reg2MI", "");
                    rs1.Set_Fields("Reg2Designation", "");
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2) != "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + FCConvert.ToString(lngDeletedID) + " OR ID = " + rs1.Get_Fields_Int32("PartyID2") + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID2"))
                        {
                            // do nothing
                        }
                        else if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID3"))
                        {
                            rs1.Set_Fields("PartyID3", rs1.Get_Fields_Int32("PartyID2"));
                        }
                        else if (FCConvert.ToInt32(rsParty.Get_Fields_Int32("ID")) == lngDeletedID)
                        {
                            // do nothing
                        }
                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2")), MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID")));
                            }
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 1);
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2));
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID2")), tempName);
                            }
                            lngDeletedID2 = rs1.Get_Fields_Int32("PartyID2");
                        }
                    }
                    rs1.Set_Fields("OwnerCode2", "C");
                    rs1.Set_Fields("PartyID2", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner2", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg2LastName", "");
                    rs1.Set_Fields("Reg2FirstName", "");
                    rs1.Set_Fields("Reg2MI", "");
                    rs1.Set_Fields("Reg2Designation", "");
                }
                else
                {
                    rs1.Set_Fields("OwnerCode2", "N");
                    rs1.Set_Fields("PartyID2", 0);
                    rs1.Set_Fields("PartyName2", "");
                    rs1.Set_Fields("Owner2", "");
                    rs1.Set_Fields("Reg2LastName", "");
                    rs1.Set_Fields("Reg2FirstName", "");
                    rs1.Set_Fields("Reg2MI", "");
                    rs1.Set_Fields("Reg2Designation", "");
                }
                if (MotorVehicle.Statics.BMVRecord.DOB3 != "99999999" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DOB3) != "")
                {
                    if (Information.IsDate(Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB3, 4)))
                    {
                        rs1.Set_Fields("DOB3", Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.DOB3, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.DOB3, 4));
                    }
                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 1, 50);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 51, 20);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 71, 1);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner3, 72, 3);
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + FCConvert.ToString(lngDeletedID) + " OR ID = " + FCConvert.ToString(lngDeletedID2) + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID3"))
                        {
                            // do nothing
                        }
                        else if (FCConvert.ToInt32(rsParty.Get_Fields_Int32("ID")) == lngDeletedID || FCConvert.ToInt32(rsParty.Get_Fields_Int32("ID")) == lngDeletedID2)
                        {
                            // do nothing
                        }
                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 0 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName)))) + "' AND upper(isnull(LastName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName)))) + "' AND upper(isnull(MiddleName, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial)))) + "' AND upper(isnull(Designation, '')) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID3")), tempName);
                            }
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 0);
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                            rsParty.Set_Fields("LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                            rsParty.Set_Fields("MiddleName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                            rsParty.Set_Fields("Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            tempName = MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID"));
                            if (!ContainsName(originalOwners, tempName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID3")), tempName + "  (NEW)");
                            }
                        }
                    }
                    rs1.Set_Fields("OwnerCode3", "I");
                    rs1.Set_Fields("PartyID3", rsParty.Get_Fields_Int32("ID"));
                    rsNameView.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + rsParty.Get_Fields_Int32("ID"), "CentralParties");
                    rs1.Set_Fields("PartyName3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsNameView.Get_Fields_String("FullNameMiddleInitial"))));
                    rs1.Set_Fields("Owner3", "");
                    rs1.Set_Fields("Reg3LastName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName));
                    rs1.Set_Fields("Reg3FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName));
                    rs1.Set_Fields("Reg3MI", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial));
                    rs1.Set_Fields("Reg3Designation", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName) != "" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3) == "" && fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner2) != "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + FCConvert.ToString(lngDeletedID) + " OR ID = " + FCConvert.ToString(lngDeletedID2) + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {

                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            if (!ContainsName(originalOwners, MotorVehicle.Statics.BMVRecord.DBAName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID3")), MotorVehicle.Statics.BMVRecord.DBAName);
                            }
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 1);
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.DBAName));
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            if (!ContainsName(originalOwners, MotorVehicle.Statics.BMVRecord.DBAName))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID3")), MotorVehicle.Statics.BMVRecord.DBAName + "  (NEW)");
                            }
                        }
                    }
                    rs1.Set_Fields("OwnerCode3", "D");
                    rs1.Set_Fields("PartyID3", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg3LastName", "");
                    rs1.Set_Fields("Reg3FirstName", "");
                    rs1.Set_Fields("Reg3MI", "");
                    rs1.Set_Fields("Reg3Designation", "");
                }
                else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3) != "")
                {
                    rsParty.OpenRecordset("SELECT * FROM Parties WHERE (ID = " + FCConvert.ToString(lngDeletedID) + " OR ID = " + FCConvert.ToString(lngDeletedID2) + " OR ID = " + rs1.Get_Fields_Int32("PartyID3") + ") AND PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3)))) + "'", "CentralParties");
                    if (rsParty.EndOfFile() != true && rsParty.BeginningOfFile() != true)
                    {
                        if (rsParty.Get_Fields_Int32("ID") == rs1.Get_Fields_Int32("PartyID3"))
                        {
                            // do nothing
                        }
                        else if (FCConvert.ToInt32(rsParty.Get_Fields_Int32("ID")) == lngDeletedID || FCConvert.ToInt32(rsParty.Get_Fields_Int32("ID")) == lngDeletedID2)
                        {
                            // do nothing
                        }
                    }
                    else
                    {
                        rsParty.OpenRecordset("SELECT * FROM Parties WHERE PartyType = 1 AND upper(FirstName) = '" + FCConvert.ToString(MotorVehicle.FixQuotes(fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3)))) + "'", "CentralParties");
                        if (!rsParty.EndOfFile())
                        {
                            if (!ContainsName(originalOwners, MotorVehicle.Statics.BMVRecord.Owner3.Trim()))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID3")), MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID")));
                            }
                        }
                        else
                        {
                            rsParty.AddNew();
                            rsParty.Set_Fields("PartyType", 1);
                            rsParty.Set_Fields("FirstName", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Owner3));
                            rsParty.Set_Fields("PartyGuid", Guid.NewGuid());
                            rsParty.Set_Fields("DateCreated", DateTime.Now);
                            rsParty.Set_Fields("CreatedBy",
                                modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                            rsParty.Update();
                            Array.Resize(ref MotorVehicle.Statics.lngAddedParties, MotorVehicle.Statics.intAddedPartyCounter + 1);
                            MotorVehicle.Statics.lngAddedParties[MotorVehicle.Statics.intAddedPartyCounter] = rsParty.Get_Fields_Int32("ID");
                            MotorVehicle.Statics.intAddedPartyCounter += 1;
                            if (!ContainsName(originalOwners, MotorVehicle.Statics.BMVRecord.Owner3))
                            {
                                AddChangesToList(strPlate, MotorVehicle.GetPartyNameMiddleInitial(rs1.Get_Fields_Int32("PartyID3")), MotorVehicle.GetPartyNameMiddleInitial(rsParty.Get_Fields_Int32("ID")) + "  (NEW)");
                            }
                        }
                    }
                    rs1.Set_Fields("OwnerCode3", "C");
                    rs1.Set_Fields("PartyID3", rsParty.Get_Fields_Int32("ID"));
                    rs1.Set_Fields("PartyName3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Owner3", fecherFoundation.Strings.UCase(FCConvert.ToString(rsParty.Get_Fields_String("FirstName"))));
                    rs1.Set_Fields("Reg3LastName", "");
                    rs1.Set_Fields("Reg3FirstName", "");
                    rs1.Set_Fields("Reg3MI", "");
                    rs1.Set_Fields("Reg3Designation", "");
                }
                else
                {
                    rs1.Set_Fields("OwnerCode3", "N");
                    rs1.Set_Fields("PartyID3", 0);
                    rs1.Set_Fields("PartyName3", "");
                    rs1.Set_Fields("Owner3", "");
                    rs1.Set_Fields("Reg3LastName", "");
                    rs1.Set_Fields("Reg3FirstName", "");
                    rs1.Set_Fields("Reg3MI", "");
                    rs1.Set_Fields("Reg3Designation", "");
                }
            }
            if (MotorVehicle.Statics.BMVRecord.RentalFlag == "Y")
            {
                rs1.Set_Fields("Rental", true);
            }
            else
            {
                rs1.Set_Fields("Rental", false);
            }

            rs1.Set_Fields("Status", "A");
            if (MotorVehicle.Statics.BMVRecord.VanityFlag == "Y")
            {
                rs1.Set_Fields("VanityFlag", true);
            }
            else
            {
                rs1.Set_Fields("VanityFlag", false);
            }
            if (strDOTNumber == "")
            {
                rs1.Set_Fields("DOTNumber", "");
            }
            else
            {
                rs1.Set_Fields("DOTNumber", strDOTNumber);
            }
            rs1.Set_Fields("Odometer", Conversion.Val(MotorVehicle.Statics.BMVRecord.Mileage));
            if (Information.IsDate(strExpiresDate))
            {
                rs1.Set_Fields("ExpireDate", strExpiresDate);
            }
            rs1.Set_Fields("Class", strClass.Trim());
            rs1.Set_Fields("plate", strPlate.Trim());
            rs1.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.PlateStripped));
            rs1.Set_Fields("Vin", strVin.Trim());
            rs1.Set_Fields("color1", MotorVehicle.Statics.BMVRecord.Color1.Trim());
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Color2.Trim()) == "")
            {
                rs1.Set_Fields("color2", "NA");
            }
            else
            {
                rs1.Set_Fields("color2", MotorVehicle.Statics.BMVRecord.Color2.Trim());
            }
            rs1.Set_Fields("UnitNumber", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Unit.Trim()));
            if (rs1.Get_Fields_String("UnitNumber") != "")
            {
                while (Strings.Left(rs1.Get_Fields_String("UnitNumber"), 1) == "0")
                {
                    rs1.Set_Fields("UnitNumber", Strings.Right(rs1.Get_Fields_String("UnitNumber").Trim(), rs1.Get_Fields_String("UnitNumber").Trim().Length - 1));
                    if (rs1.Get_Fields_String("UnitNumber") == "")
                    {
                        break;
                    }
                }
            }
            rs1.Set_Fields("Year", Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Year.Trim())));
            rs1.Set_Fields("make", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.make.Trim()));
            rs1.Set_Fields("model", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.model.Trim()));
            rs1.Set_Fields("Style", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Style.Trim()));
            if (MotorVehicle.Statics.BMVRecord.Tires.Trim() == "")
            {
                rs1.Set_Fields("Tires", 0);
            }
            else
            {
                rs1.Set_Fields("Tires", Conversion.Val(MotorVehicle.Statics.BMVRecord.Tires.Trim()));
            }
            if (MotorVehicle.Statics.BMVRecord.Axles.Trim() == "")
            {
                rs1.Set_Fields("Axles", 0);
            }
            else
            {
                rs1.Set_Fields("Axles", Conversion.Val(MotorVehicle.Statics.BMVRecord.Axles.Trim()));
            }
            if (fecherFoundation.Strings.Trim(strNetWeight) == "")
            {
                rs1.Set_Fields("NetWeight", 0);
            }
            else
            {
                rs1.Set_Fields("NetWeight", Conversion.Val(strNetWeight));
            }
            if (fecherFoundation.Strings.Trim(strGrossWeight) == "")
            {
                rs1.Set_Fields("registeredWeightNew", 0);
            }
            else
            {
                rs1.Set_Fields("registeredWeightNew", Conversion.Val(strGrossWeight));
            }
            rs1.Set_Fields("Fuel", MotorVehicle.Statics.BMVRecord.Fuel.Trim());
            if (rs1.Get_Fields_String("Class") == "TL" || rs1.Get_Fields_String("Class") == "TC" || rs1.Get_Fields_String("Class") == "SE")
            {
                rs1.Set_Fields("Fuel", " ");
            }
            rs1.Set_Fields("Address", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Address1));
            rs1.Set_Fields("Address2", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Address2));
            rs1.Set_Fields("City", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.City));
            rs1.Set_Fields("State", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.State));
            rs1.Set_Fields("Zip", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Zip));
            rs1.Set_Fields("Country", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Country));
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.TaxIDNumber) == "")
            {
                rs1.Set_Fields("TaxIDNumber", "");
            }
            else
            {
                rs1.Set_Fields("TaxIDNumber", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.TaxIDNumber).Replace("-", ""));
            }
            rs1.Set_Fields("ReReg", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.RegFlag));
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.ExciseExemptFlag) == "N")
            {
                rs1.Set_Fields("ExciseExempt", 0);
            }
            else
            {
                rs1.Set_Fields("ExciseExempt", -1);
            }
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.FeeExemptFlag) == "N")
            {
                rs1.Set_Fields("RegExempt", 0);
            }
            else
            {
                rs1.Set_Fields("RegExempt", -1);
            }
            rs1.Set_Fields("ResidenceCode", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.ResidenceCode));
            rs1.Set_Fields("Residence", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.ResidenceCity));
            rs1.Set_Fields("ResidenceState", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.ResidenceState));
            rs1.Set_Fields("ResidenceAddress", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.ResidenceAddress));
            rs1.Set_Fields("ResidenceCountry", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.ResidenceCountry));
            if (Information.IsDate(strEffectiveDate))
            {
                rs1.Set_Fields("EffectiveDate", strEffectiveDate);
            }
            rs1.Set_Fields("MVR3", Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.MVR3)));
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.OldMVR3) == "")
            {
                rs1.Set_Fields("OldMVR3", 0);
            }
            else
            {
                rs1.Set_Fields("OldMVR3", Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.OldMVR3)));
            }
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.OldClass) == "")
            {
                rs1.Set_Fields("OldClass", "");
            }
            else
            {
                rs1.Set_Fields("OldClass", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.OldClass));
            }
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.OldPlate) == "")
            {
                rs1.Set_Fields("OldPlate", "");
            }
            else
            {
                rs1.Set_Fields("OldPlate", fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.OldPlate));
            }
            rs1.Set_Fields("ETO", 0);
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.LeaseFlag1) == "E")
            {
                rs1.Set_Fields("LeaseCode1", "E");
            }
            else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.LeaseFlag1) == "R")
            {
                rs1.Set_Fields("LeaseCode1", "R");
            }
            else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.TrustFlag1) == "E")
            {
                rs1.Set_Fields("LeaseCode1", "T");
            }
            else
            {
                rs1.Set_Fields("LeaseCode1", "N");
            }
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.LeaseFlag2) == "E")
            {
                rs1.Set_Fields("LeaseCode2", "E");
            }
            else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.LeaseFlag2) == "R")
            {
                rs1.Set_Fields("LeaseCode2", "R");
            }
            else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.TrustFlag2) == "E")
            {
                rs1.Set_Fields("LeaseCode2", "T");
            }
            else
            {
                rs1.Set_Fields("LeaseCode2", "N");
            }
            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.LeaseFlag3) == "E")
            {
                rs1.Set_Fields("LeaseCode3", "E");
            }
            else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.LeaseFlag3) == "R")
            {
                rs1.Set_Fields("LeaseCode3", "R");
            }
            else if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.TrustFlag3) == "E")
            {
                rs1.Set_Fields("LeaseCode3", "T");
            }
            else
            {
                rs1.Set_Fields("LeaseCode3", "N");
            }
            if (!blnBMVDataRefresh)
            {
                if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.TransactionDate) != "")
                {
                    if (!Information.IsDate(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.TransactionDate, 4) + " " + FCConvert.ToString(Conversion.Val(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 10, 2))) + ":" + FCConvert.ToString(Conversion.Val(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 13, 2))) + ":" + FCConvert.ToString(Conversion.Val(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 15, 2)))))
                    {
                        rs1.Set_Fields("DateUpdated", DateTime.Today);
                        // kk07142016   .Fields("TimeUpdated") = Time
                    }
                    else
                    {
                        rs1.Set_Fields("DateUpdated", Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 7, 2) + "/" + Strings.Left(MotorVehicle.Statics.BMVRecord.TransactionDate, 4) + " " + Strings.Format(FCConvert.ToString(Conversion.Val(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 10, 2))) + ":" + FCConvert.ToString(Conversion.Val(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 13, 2))) + ":" + FCConvert.ToString(Conversion.Val(Strings.Mid(MotorVehicle.Statics.BMVRecord.TransactionDate, 15, 2))), "hh:mm:ss AMPM"));
                        // kk07142016   .Fields("TimeUpdated") = Format(Val(Mid(BMVRecord.TransactionDate, 10, 2)) & ":" & Val(Mid(BMVRecord.TransactionDate, 13, 2)) & ":" & Val(Mid(BMVRecord.TransactionDate, 15, 2)), "hh:mm:ss AMPM")
                    }
                }
                else
                {
                    rs1.Set_Fields("DateUpdated", DateTime.Today);
                    // kk07142016   .Fields("TimeUpdated") = Time
                }
            }
            if (rs1.Get_Fields_String("Class") == "VT" || rs1.Get_Fields_String("Class") == "DS")
            {
                // kk06222016  Pull in decal for DS also
                if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Battle) != "")
                {
                    // kk07192016  Moved decal definitions to clsPrintCodes
                    oPrtCodes = new clsPrintCodes();
                    rs1.Set_Fields("Battle", oPrtCodes.GetVeteranDecalCode(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecord.Battle)));
                    oPrtCodes = null;
                }
            }
        }

        private void AddChangesToList(string strPlate, string strSystem, string strState)
        {
            Array.Resize(ref MotorVehicle.Statics.strPrePrintPlate, MotorVehicle.Statics.intChanges + 1);
            Array.Resize(ref MotorVehicle.Statics.strSystemInfo, MotorVehicle.Statics.intChanges + 1);
            Array.Resize(ref MotorVehicle.Statics.strStateInfo, MotorVehicle.Statics.intChanges + 1);
            MotorVehicle.Statics.strPrePrintPlate[MotorVehicle.Statics.intChanges] = strPlate;
            MotorVehicle.Statics.strSystemInfo[MotorVehicle.Statics.intChanges] = strSystem;
            MotorVehicle.Statics.strStateInfo[MotorVehicle.Statics.intChanges] = strState;
            MotorVehicle.Statics.intChanges += 1;
        }

        private bool ContainsName(string[] names, string name)
        {
            foreach (var tempName in names)
            {
                if (tempName.Trim() == name.Trim())
                {
                    return true;
                }
            }

            return false;
        }

        private void frmPrePrints_Activated(object sender, System.EventArgs e)
        {
            if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
            {
                return;
            }
            if (!modSecurity.ValidPermissions(this, MotorVehicle.BMVUPDATEDISK))
            {
                MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Close();
            }
        }

        private void frmPrePrints_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                frmPrePrints.InstancePtr.Close();
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmPrePrints_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmPrePrints properties;
            //frmPrePrints.ScaleWidth	= 9045;
            //frmPrePrints.ScaleHeight	= 7230;
            //frmPrePrints.LinkTopic	= "Form1";
            //End Unmaped Properties
            modGNBas.GetWindowSize(this);
            modGlobalFunctions.SetTRIOColors(this);
            //SetCustomFormColors();
            if (blnBMVDataRefresh)
            {
                Text = "BMV Data Refresh";
            }
            else
            {
                Text = " Process BMV Update File";
            }
            PlateCol = 0;
            OwnerCol = 1;
            YearCol = 2;
            MakeCol = 3;
            ModelCol = 4;
            vsAdded.ColWidth(PlateCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.16));
            vsAdded.ColWidth(OwnerCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.36));
            vsAdded.ColWidth(YearCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.16));
            vsAdded.ColWidth(MakeCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.16));
            vsAdded.ColWidth(ModelCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.1));
            vsAdded.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAdded.ColAlignment(OwnerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAdded.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAdded.ColAlignment(MakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAdded.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsAdded.TextMatrix(0, PlateCol, "Plate");
            vsAdded.TextMatrix(0, OwnerCol, "Owner");
            vsAdded.TextMatrix(0, YearCol, "Year");
            vsAdded.TextMatrix(0, MakeCol, "Make");
            vsAdded.TextMatrix(0, ModelCol, "Model");
            vsEdited.ColWidth(PlateCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.16));
            vsEdited.ColWidth(OwnerCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.36));
            vsEdited.ColWidth(YearCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.16));
            vsEdited.ColWidth(MakeCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.16));
            vsEdited.ColWidth(ModelCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.1));
            vsEdited.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsEdited.ColAlignment(OwnerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsEdited.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsEdited.ColAlignment(MakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsEdited.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsEdited.TextMatrix(0, PlateCol, "Plate");
            vsEdited.TextMatrix(0, OwnerCol, "Owner");
            vsEdited.TextMatrix(0, YearCol, "Year");
            vsEdited.TextMatrix(0, MakeCol, "Make");
            vsEdited.TextMatrix(0, ModelCol, "Model");
            vsNotUpdated.ColWidth(PlateCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.16));
            vsNotUpdated.ColWidth(OwnerCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.36));
            vsNotUpdated.ColWidth(YearCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.16));
            vsNotUpdated.ColWidth(MakeCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.16));
            vsNotUpdated.ColWidth(ModelCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.1));
            vsNotUpdated.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsNotUpdated.ColAlignment(OwnerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsNotUpdated.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsNotUpdated.ColAlignment(MakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsNotUpdated.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsNotUpdated.TextMatrix(0, PlateCol, "Plate");
            vsNotUpdated.TextMatrix(0, OwnerCol, "Owner");
            vsNotUpdated.TextMatrix(0, YearCol, "Year");
            vsNotUpdated.TextMatrix(0, MakeCol, "Make");
            vsNotUpdated.TextMatrix(0, ModelCol, "Model");
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                modGNBas.SaveWindowSize(this);
            }
        }

        private void frmPrePrints_Resize(object sender, System.EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                modGNBas.SaveWindowSize(this);
            }
            vsAdded.ColWidth(PlateCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.16));
            vsAdded.ColWidth(OwnerCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.36));
            vsAdded.ColWidth(YearCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.16));
            vsAdded.ColWidth(MakeCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.16));
            vsAdded.ColWidth(ModelCol, FCConvert.ToInt32(vsAdded.WidthOriginal * 0.1));
            vsEdited.ColWidth(PlateCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.16));
            vsEdited.ColWidth(OwnerCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.36));
            vsEdited.ColWidth(YearCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.16));
            vsEdited.ColWidth(MakeCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.16));
            vsEdited.ColWidth(ModelCol, FCConvert.ToInt32(vsEdited.WidthOriginal * 0.1));
            vsNotUpdated.ColWidth(PlateCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.16));
            vsNotUpdated.ColWidth(OwnerCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.36));
            vsNotUpdated.ColWidth(YearCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.16));
            vsNotUpdated.ColWidth(MakeCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.16));
            vsNotUpdated.ColWidth(ModelCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.1));
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            strPrintLine.Value = "";
            DiskExpireDate = DateTime.FromOADate(0);
            //App.DoEvents();
            rsA.Reset();
            rsB.Reset();
            strDrive = "";
            strStateLevel = "";
            strMVLevel = "";
            lngMaxMV = 0;
            strResCode = "";
            Rec1 = "";
            lngMCounter = 0;
            FixPlate = "";
            Plate8 = "";
            VIN = "";
            TRecord = "";
            found = "";
            BadFlag = "";
            BadFlagCount = 0;
            LineCount = 0;
        }

        private void mnuFileExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void SetCustomFormColors()
        {
            lblDrive.ForeColor = Color.Blue;
        }

    }
}
