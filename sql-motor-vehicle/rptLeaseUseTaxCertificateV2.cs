//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLeaseUseTaxCertificateV2.
	/// </summary>
	public partial class rptLeaseUseTaxCertificateV2 : BaseSectionReport
	{
		public rptLeaseUseTaxCertificateV2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Use Tax Certificate";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptLeaseUseTaxCertificateV2 InstancePtr
		{
			get
			{
				return (rptLeaseUseTaxCertificateV2)Sys.GetInstance(typeof(rptLeaseUseTaxCertificateV2));
			}
		}

		protected rptLeaseUseTaxCertificateV2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLeaseUseTaxCertificateV2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Last Updated:              01/28/2002
		private bool boolIsPrintTest;

		public void PrintTest()
		{
			boolIsPrintTest = true;
			//this.PrintReport(false);
            this.PrintReportOnDotMatrix("ctaprintername");
		}

		private void GetInformation()
		{
			// this sub will get all of the information needed and fill the fields on the Form
			double dblTaxAmount = 0;
			double dblPurchasePrice = 0;
			double dblAllowance = 0;
			double dblPaymentAmount = 0;
			int lngNumberOfPayments = 0;
			double dblDownPayment = 0;
			clsDRWrapper rsUseTax = new clsDRWrapper();
			int TB;
			string SellerAddress = "";
			string ExmptCode = "";
			string Owner = "";
			string LienHolder1 = "";
			int x;
			// 
            const string numberFormat = "#,###";

            const string currencyFormat = "#,##0.00";

            const string largeCurrencyFormat = "###,###.00";

            if (!boolIsPrintTest)
			{
				rsUseTax.OpenRecordset($"SELECT * FROM UseTax WHERE ID = {FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID)}", "TWMV0000.vb1");

                if (rsUseTax.EndOfFile() || rsUseTax.BeginningOfFile())
                {
                    MotorVehicle.Statics.blnPrintUseTax = false;
                    this.Cancel();

                    return;
                }

                rsUseTax.MoveLast();
                rsUseTax.MoveFirst();

                fldMake1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
				fldModel1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
				fldYear1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
				fldVin1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
				fldSellerName.Text = rsUseTax.Get_Fields_String("SellerName");
				fldDateofTransfer.Text = Information.IsDate(rsUseTax.Get_Fields("TradedDate")) 
                    ? Strings.Format(rsUseTax.Get_Fields_DateTime("TradedDate"), "MM/dd/yyyy") 
                    : "";
				SellerAddress = $"{fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerAddress1")))} {fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerCity")))} {rsUseTax.Get_Fields_String("SellerState")} {fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerZipAndZip4")))}";
				fldSellerAddress.Text = fecherFoundation.Strings.Trim(SellerAddress);
				dblPaymentAmount = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Double("LeasePaymentAmount")) == false)
					dblPaymentAmount = rsUseTax.Get_Fields_Double("LeasePaymentAmount");
				fldLeasePayment.Text = Strings.Format(dblPaymentAmount, numberFormat);
				lngNumberOfPayments = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Int32("NumberOfPayments")) == false)
					lngNumberOfPayments = FCConvert.ToInt32(rsUseTax.Get_Fields_Int32("NumberOfPayments"));
				fldNumberOfPayments.Text = Strings.Format(lngNumberOfPayments, numberFormat);
				fldPaymentTotal.Text = Strings.Format(dblPaymentAmount * lngNumberOfPayments, numberFormat);
				dblDownPayment = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Double("LeaseDownPayment")) == false)
					dblDownPayment = rsUseTax.Get_Fields_Double("LeaseDownPayment");
				fldDownPayment.Text = Strings.Format(dblDownPayment, numberFormat);
				dblPurchasePrice = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("PurchasePrice")) == false)
					dblPurchasePrice = Conversion.Val(rsUseTax.Get_Fields_Decimal("PurchasePrice"));
				dblAllowance = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("Allowance")) == false)
					dblAllowance = Conversion.Val(rsUseTax.Get_Fields_Decimal("Allowance"));
				fldAllowance.Text = Strings.Format(dblAllowance, numberFormat);
				fldTotalAmount.Text = Strings.Format(dblPurchasePrice + dblAllowance, numberFormat);
				dblTaxAmount = 0;
				if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("TaxAmount")) == false)
					dblTaxAmount = Conversion.Val(rsUseTax.Get_Fields_Decimal("TaxAmount"));
				fldUseTax.Text = Strings.Format(dblTaxAmount, currencyFormat);
				ExmptCode = FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptCode"));
				switch (ExmptCode)
                {
                    case "A":
                        fldExemptTypeA.Text = "A";
                        fldExemptNumber.Text = rsUseTax.Get_Fields_String("ExemptNumber");

                        break;
                    case "B":
                        fldExemptTypeB.Text = "B";

                        break;
                    case "C":
                        fldExemptTypeC.Text = "C";

                        break;
                    case "D":
                        fldExemptTypeD.Text = "D";
                        fldOtherReason.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptDescription")));

                        break;
                }
                fldPurchaserName.Text = $"{rsUseTax.Get_Fields_String("PurchaserFName").Trim()} {rsUseTax.Get_Fields_String("PurchaserLName").Trim()}";
				fldPurchaserSocialSec.Text = rsUseTax.Get_Fields_String("PurchaserSSN");
				fldPurchaserAddress.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserAddress")));
				fldPurchaserCity.Text = rsUseTax.Get_Fields_String("PurchaserCity");
				fldPurchaserState.Text = rsUseTax.Get_Fields_String("PurchaserState");
				fldPurchaserZip.Text = rsUseTax.Get_Fields_String("PurchaserZipAndZip4");
				if (MotorVehicle.Statics.FleetUseTax)
				{
					fldClassPlate.Text = "S.A.L.";
				}
				else
				{
					fldClassPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");
				}
				fldRegDate.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
				fldTaxAmount.Text = Strings.Format(Strings.Format(dblTaxAmount, largeCurrencyFormat), "@@@@@@@@@@");
			}
			else
			{
				fldMake1.Text = "MAKE";
				fldModel1.Text = "MODEL";
				fldYear1.Text = FCConvert.ToString(DateTime.Today.Year);
				fldVin1.Text = "123456789";
				fldSellerName.Text = "Seller Name";
				fldDateofTransfer.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				SellerAddress = "Address 1" + " " + "Seller City" + " " + "ST" + " " + "00000";
				fldSellerAddress.Text = fecherFoundation.Strings.Trim(SellerAddress);
				dblPaymentAmount = 500;
				fldLeasePayment.Text = Strings.Format(dblPaymentAmount, numberFormat);
				lngNumberOfPayments = 36;
				fldNumberOfPayments.Text = Strings.Format(lngNumberOfPayments, numberFormat);
				fldPaymentTotal.Text = Strings.Format(dblPaymentAmount * lngNumberOfPayments, numberFormat);
				dblDownPayment = 1000;
				fldDownPayment.Text = Strings.Format(dblDownPayment, numberFormat);
				dblPurchasePrice = 32000;
				dblAllowance = 350;
				fldAllowance.Text = Strings.Format(dblAllowance, numberFormat);
				fldTotalAmount.Text = Strings.Format(dblPurchasePrice + dblAllowance, numberFormat);
				dblTaxAmount = 1100;
				fldUseTax.Text = Strings.Format(dblTaxAmount, currencyFormat);
				fldExemptTypeA.Text = "A";
				fldExemptNumber.Text = "12345";
				fldExemptTypeB.Text = "B";
				fldExemptTypeC.Text = "C";
				fldExemptTypeD.Text = "D";
				fldOtherReason.Text = "Other reason text";
				fldPurchaserName.Text = "Purchaser Name";
				fldPurchaserSocialSec.Text = "000990000";
				fldPurchaserAddress.Text = "Purchaser Address";
				fldPurchaserCity.Text = "Purchaser City";
				fldPurchaserState.Text = "ST";
				fldPurchaserZip.Text = "12345";
				fldClassPlate.Text = "PC 123456";
				fldRegDate.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
				fldTaxAmount.Text = Strings.Format(Strings.Format(dblTaxAmount, largeCurrencyFormat), "@@@@@@@@@@");
			}
			rsUseTax.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			this.Document.Printer.PrinterName = MotorVehicle.Statics.gstrCTAPrinterName;
			/*? For Each */
			foreach (FCPrinter p in FCGlobal.Printers)
			{
                if (p.DeviceName != MotorVehicle.Statics.gstrCTAPrinterName) continue;

                if (tempPrinter.DriverName == "EPSON24")
                {
                    for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
                    {
                        (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 10cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
                    }
                }
                break;
            }
			/*? Next */
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			GetInformation();
			MotorVehicle.Statics.blnPrintUseTax = true;
		}

	}
}
