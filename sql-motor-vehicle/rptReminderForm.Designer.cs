﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReminderForm.
	/// </summary>
	partial class rptReminderForm
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderForm));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTownName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTownAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTownAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTownAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTownAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPlate,
				this.fldYear,
				this.fldMake,
				this.fldModel,
				this.fldAmount
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			GroupHeader1.DataField = "grouping";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.fldDate,
				this.lblTownName,
				this.lblTownAddress1,
				this.lblTownAddress2,
				this.lblTownAddress3,
				this.fldName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldStatement,
				this.Label7,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label15,
				this.Line1,
				this.lblTownAddress4
			});
			this.GroupHeader1.Height = 3.322917F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMessage
			});
			this.GroupFooter1.Height = 5.0625F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 0";
			this.Label1.Text = "Motor Vehicle Registration Reminder Notice";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 4.5F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 5.5625F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0.125F;
			this.fldDate.Width = 0.9375F;
			// 
			// lblTownName
			// 
			this.lblTownName.Height = 0.1875F;
			this.lblTownName.HyperLink = null;
			this.lblTownName.Left = 1F;
			this.lblTownName.Name = "lblTownName";
			this.lblTownName.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblTownName.Text = "Motor Vehicle Registration Reminder Notice";
			this.lblTownName.Top = 0.375F;
			this.lblTownName.Width = 4.5F;
			// 
			// lblTownAddress1
			// 
			this.lblTownAddress1.Height = 0.1875F;
			this.lblTownAddress1.HyperLink = null;
			this.lblTownAddress1.Left = 1F;
			this.lblTownAddress1.Name = "lblTownAddress1";
			this.lblTownAddress1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblTownAddress1.Text = "Motor Vehicle Registration Reminder Notice";
			this.lblTownAddress1.Top = 0.5625F;
			this.lblTownAddress1.Width = 4.5F;
			// 
			// lblTownAddress2
			// 
			this.lblTownAddress2.Height = 0.1875F;
			this.lblTownAddress2.HyperLink = null;
			this.lblTownAddress2.Left = 1F;
			this.lblTownAddress2.Name = "lblTownAddress2";
			this.lblTownAddress2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblTownAddress2.Text = "Motor Vehicle Registration Reminder Notice";
			this.lblTownAddress2.Top = 0.75F;
			this.lblTownAddress2.Width = 4.5F;
			// 
			// lblTownAddress3
			// 
			this.lblTownAddress3.Height = 0.1875F;
			this.lblTownAddress3.HyperLink = null;
			this.lblTownAddress3.Left = 1F;
			this.lblTownAddress3.Name = "lblTownAddress3";
			this.lblTownAddress3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblTownAddress3.Text = "Motor Vehicle Registration Reminder Notice";
			this.lblTownAddress3.Top = 0.9375F;
			this.lblTownAddress3.Width = 4.5F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldName.Text = "Field1";
			this.fldName.Top = 1.6875F;
			this.fldName.Width = 2.5625F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 0F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldAddress1.Text = "Field2";
			this.fldAddress1.Top = 1.875F;
			this.fldAddress1.Width = 2.5625F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 0F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldAddress2.Text = "Field3";
			this.fldAddress2.Top = 2.0625F;
			this.fldAddress2.Width = 2.5625F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 0F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldAddress3.Text = "Field4";
			this.fldAddress3.Top = 2.25F;
			this.fldAddress3.Width = 2.5625F;
			// 
			// fldStatement
			// 
			this.fldStatement.Height = 0.1875F;
			this.fldStatement.Left = 0F;
			this.fldStatement.Name = "fldStatement";
			this.fldStatement.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldStatement.Text = "Field4";
			this.fldStatement.Top = 2.6875F;
			this.fldStatement.Width = 6.25F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Plate";
			this.Label7.Top = 3.125F;
			this.Label7.Width = 0.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label9.Text = "Year";
			this.Label9.Top = 3.125F;
			this.Label9.Width = 0.4375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label10.Text = "Make";
			this.Label10.Top = 3.125F;
			this.Label10.Width = 0.4375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.5625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label11.Text = "Model";
			this.Label11.Top = 3.125F;
			this.Label11.Width = 0.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.875F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label15.Text = "Amount";
			this.Label15.Top = 3.125F;
			this.Label15.Width = 0.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.3125F;
			this.Line1.Width = 6.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 3.3125F;
			this.Line1.Y2 = 3.3125F;
			// 
			// lblTownAddress4
			// 
			this.lblTownAddress4.Height = 0.1875F;
			this.lblTownAddress4.HyperLink = null;
			this.lblTownAddress4.Left = 1F;
			this.lblTownAddress4.Name = "lblTownAddress4";
			this.lblTownAddress4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblTownAddress4.Text = "Motor Vehicle Registration Reminder Notice";
			this.lblTownAddress4.Top = 1.125F;
			this.lblTownAddress4.Width = 4.5F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 0F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPlate.Text = "Field1";
			this.fldPlate.Top = 0F;
			this.fldPlate.Width = 0.5625F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 0.625F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.375F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.1875F;
			this.fldMake.Left = 1.125F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 0F;
			this.fldMake.Width = 0.375F;
			// 
			// fldModel
			// 
			this.fldModel.Height = 0.1875F;
			this.fldModel.Left = 1.5625F;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldModel.Text = "Field1";
			this.fldModel.Top = 0F;
			this.fldModel.Width = 0.5625F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 2.3125F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 2.0625F;
			// 
			// fldMessage
			// 
			this.fldMessage.CanGrow = false;
			this.fldMessage.Height = 4.875F;
			this.fldMessage.Left = 0F;
			this.fldMessage.Name = "fldMessage";
			this.fldMessage.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldMessage.Text = null;
			this.fldMessage.Top = 0.1875F;
			this.fldMessage.Width = 6.5F;
			// 
			// rptReminderForm
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTownName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTownAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTownAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTownAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatement;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTownAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMessage;
	}
}
