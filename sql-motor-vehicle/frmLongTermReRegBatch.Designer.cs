//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmLongTermReRegBatch.
	/// </summary>
	partial class frmLongTermReRegBatch
	{
		public fecherFoundation.FCComboBox cmbGroup;
		public fecherFoundation.FCFrame fraReprint;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancelReprint;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCFrame fraRegLength;
		public fecherFoundation.FCComboBox cboLength;
		public FCGrid vsRegLength;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblTotalDue;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblFleetLength;
		public fecherFoundation.FCButton cmdBack;
		public fecherFoundation.FCButton cmdNext;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdQueue;
		public fecherFoundation.FCFrame fraFleetSelect;
		public fecherFoundation.FCComboBox cboFleets;
		public fecherFoundation.FCFrame fraSelectVehicles;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCButton cmdSelectAll;
		public FCGrid vsVehicles;
		public fecherFoundation.FCLabel lblFleetNameVehicles;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbGroup = new fecherFoundation.FCComboBox();
            this.fraReprint = new fecherFoundation.FCFrame();
            this.txtLow = new fecherFoundation.FCTextBox();
            this.txtHigh = new fecherFoundation.FCTextBox();
            this.cmdOK = new fecherFoundation.FCButton();
            this.cmdCancelReprint = new fecherFoundation.FCButton();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.fraRegLength = new fecherFoundation.FCFrame();
            this.cboLength = new fecherFoundation.FCComboBox();
            this.vsRegLength = new fecherFoundation.FCGrid();
            this.Line1 = new fecherFoundation.FCLine();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblTotalDue = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblFleetLength = new fecherFoundation.FCLabel();
            this.cmdBack = new fecherFoundation.FCButton();
            this.cmdNext = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdQueue = new fecherFoundation.FCButton();
            this.fraFleetSelect = new fecherFoundation.FCFrame();
            this.cboFleets = new fecherFoundation.FCComboBox();
            this.fraSelectVehicles = new fecherFoundation.FCFrame();
            this.cmdClearAll = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.vsVehicles = new fecherFoundation.FCGrid();
            this.lblFleetNameVehicles = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmbPlateOption = new fecherFoundation.FCComboBox();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprint)).BeginInit();
            this.fraReprint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelReprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRegLength)).BeginInit();
            this.fraRegLength.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRegLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQueue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFleetSelect)).BeginInit();
            this.fraFleetSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectVehicles)).BeginInit();
            this.fraSelectVehicles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(662, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSelectVehicles);
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.cmdBack);
            this.ClientArea.Controls.Add(this.cmdQueue);
            this.ClientArea.Controls.Add(this.fraReprint);
            this.ClientArea.Controls.Add(this.fraFleetSelect);
            this.ClientArea.Controls.Add(this.fraRegLength);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdNext);
            this.ClientArea.Size = new System.Drawing.Size(662, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(662, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(331, 30);
            this.HeaderText.Text = "Long Term Fleet Registration";
            // 
            // cmbGroup
            // 
            this.cmbGroup.Items.AddRange(new object[] {
            "Group",
            "Fleet"});
            this.cmbGroup.Location = new System.Drawing.Point(20, 30);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(298, 40);
            this.cmbGroup.TabIndex = 9;
            this.cmbGroup.Text = "Fleet";
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            // 
            // fraReprint
            // 
            this.fraReprint.Controls.Add(this.txtLow);
            this.fraReprint.Controls.Add(this.txtHigh);
            this.fraReprint.Controls.Add(this.cmdOK);
            this.fraReprint.Controls.Add(this.cmdCancelReprint);
            this.fraReprint.Controls.Add(this.Label6);
            this.fraReprint.Controls.Add(this.Label3);
            this.fraReprint.Controls.Add(this.Label4);
            this.fraReprint.Location = new System.Drawing.Point(30, 30);
            this.fraReprint.Name = "fraReprint";
            this.fraReprint.Size = new System.Drawing.Size(260, 269);
            this.fraReprint.TabIndex = 22;
            this.fraReprint.Text = "Enter Mvr3 Range";
            this.fraReprint.Visible = false;
            // 
            // txtLow
            // 
            this.txtLow.BackColor = System.Drawing.SystemColors.Window;
            this.txtLow.Location = new System.Drawing.Point(118, 90);
            this.txtLow.Name = "txtLow";
            this.txtLow.Size = new System.Drawing.Size(122, 40);
            this.txtLow.TabIndex = 26;
            this.txtLow.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtHigh
            // 
            this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
            this.txtHigh.Location = new System.Drawing.Point(118, 150);
            this.txtHigh.Name = "txtHigh";
            this.txtHigh.Size = new System.Drawing.Size(122, 40);
            this.txtHigh.TabIndex = 25;
            this.txtHigh.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(20, 208);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(66, 40);
            this.cmdOK.TabIndex = 24;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancelReprint
            // 
            this.cmdCancelReprint.AppearanceKey = "actionButton";
            this.cmdCancelReprint.Location = new System.Drawing.Point(118, 208);
            this.cmdCancelReprint.Name = "cmdCancelReprint";
            this.cmdCancelReprint.Size = new System.Drawing.Size(96, 40);
            this.cmdCancelReprint.TabIndex = 23;
            this.cmdCancelReprint.Text = "Cancel";
            this.cmdCancelReprint.Click += new System.EventHandler(this.cmdCancelReprint_Click);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 30);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(220, 40);
            this.Label6.TabIndex = 29;
            this.Label6.Text = "PLEASE ENTER THE RANGE OF MVR3 FORMS YOU WISH TO REPRINT";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 104);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(46, 16);
            this.Label3.TabIndex = 28;
            this.Label3.Text = "LOW";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 164);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(46, 16);
            this.Label4.TabIndex = 27;
            this.Label4.Text = "HIGH";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "actionButton";
            this.cmdSave.Location = new System.Drawing.Point(287, 541);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(130, 40);
            this.cmdSave.TabIndex = 21;
            this.cmdSave.Text = "Save Batch";
            this.cmdSave.Visible = false;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // fraRegLength
            // 
            this.fraRegLength.Controls.Add(this.cboLength);
            this.fraRegLength.Controls.Add(this.vsRegLength);
            this.fraRegLength.Controls.Add(this.Line1);
            this.fraRegLength.Controls.Add(this.Label1);
            this.fraRegLength.Controls.Add(this.lblTotalDue);
            this.fraRegLength.Controls.Add(this.Label2);
            this.fraRegLength.Controls.Add(this.lblFleetLength);
            this.fraRegLength.Location = new System.Drawing.Point(30, 30);
            this.fraRegLength.Name = "fraRegLength";
            this.fraRegLength.Size = new System.Drawing.Size(575, 491);
            this.fraRegLength.TabIndex = 14;
            this.fraRegLength.Text = "Please Select Registration Length";
            // 
            // cboLength
            // 
            this.cboLength.BackColor = System.Drawing.SystemColors.Window;
            this.cboLength.Items.AddRange(new object[] {
            "2 Years",
            "3 Years",
            "4 Years",
            "5 Years",
            "6 Years",
            "7 Years",
            "8 Years",
            "9 Years",
            "10 Years",
            "11 Years",
            "12 Years"});
            this.cboLength.Location = new System.Drawing.Point(225, 65);
            this.cboLength.Name = "cboLength";
            this.cboLength.Size = new System.Drawing.Size(167, 40);
            this.cboLength.TabIndex = 15;
            this.cboLength.SelectedIndexChanged += new System.EventHandler(this.cboLength_SelectedIndexChanged);
            // 
            // vsRegLength
            // 
            this.vsRegLength.Cols = 16;
            this.vsRegLength.ExtendLastCol = true;
            this.vsRegLength.Location = new System.Drawing.Point(20, 125);
            this.vsRegLength.Name = "vsRegLength";
            this.vsRegLength.Rows = 15;
            this.vsRegLength.ShowFocusCell = false;
            this.vsRegLength.Size = new System.Drawing.Size(533, 314);
            this.vsRegLength.TabIndex = 16;
            this.vsRegLength.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsRegLength_ChangeEdit);
            this.vsRegLength.CurrentCellChanged += new System.EventHandler(this.vsRegLength_RowColChange);
            // 
            // Line1
            // 
            this.Line1.Location = new System.Drawing.Point(5310, 6021);
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(2850, 1);
            this.Line1.X1 = 5310F;
            this.Line1.X2 = 8160F;
            this.Line1.Y1 = 6021F;
            this.Line1.Y2 = 6021F;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 460);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(50, 16);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "TOTAL";
            // 
            // lblTotalDue
            // 
            this.lblTotalDue.Location = new System.Drawing.Point(71, 460);
            this.lblTotalDue.Name = "lblTotalDue";
            this.lblTotalDue.Size = new System.Drawing.Size(130, 16);
            this.lblTotalDue.TabIndex = 19;
            this.lblTotalDue.Text = "LABEL2";
            this.lblTotalDue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 79);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(171, 16);
            this.Label2.TabIndex = 18;
            this.Label2.Text = "REGISTRATION LENGTH";
            // 
            // lblFleetLength
            // 
            this.lblFleetLength.Location = new System.Drawing.Point(20, 30);
            this.lblFleetLength.Name = "lblFleetLength";
            this.lblFleetLength.Size = new System.Drawing.Size(150, 15);
            this.lblFleetLength.TabIndex = 17;
            this.lblFleetLength.Text = "REGISTRATION LENGTH";
            // 
            // cmdBack
            // 
            this.cmdBack.AppearanceKey = "actionButton";
            this.cmdBack.Location = new System.Drawing.Point(30, 540);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(82, 40);
            this.cmdBack.TabIndex = 12;
            this.cmdBack.Text = "Back";
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // cmdNext
            // 
            this.cmdNext.AppearanceKey = "actionButton";
            this.cmdNext.Location = new System.Drawing.Point(142, 540);
            this.cmdNext.Name = "cmdNext";
            this.cmdNext.Size = new System.Drawing.Size(125, 40);
            this.cmdNext.TabIndex = 11;
            this.cmdNext.Text = "Next";
            this.cmdNext.Click += new System.EventHandler(this.cmdNext_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "actionButton";
            this.cmdPrint.Location = new System.Drawing.Point(142, 541);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(125, 40);
            this.cmdPrint.TabIndex = 10;
            this.cmdPrint.Text = "Print Batch";
            this.cmdPrint.Visible = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdQueue
            // 
            this.cmdQueue.AppearanceKey = "actionButton";
            this.cmdQueue.Location = new System.Drawing.Point(287, 541);
            this.cmdQueue.Name = "cmdQueue";
            this.cmdQueue.Size = new System.Drawing.Size(202, 40);
            this.cmdQueue.TabIndex = 9;
            this.cmdQueue.Text = "Send to Print Queue";
            this.cmdQueue.Visible = false;
            this.cmdQueue.Click += new System.EventHandler(this.cmdQueue_Click);
            // 
            // fraFleetSelect
            // 
            this.fraFleetSelect.Controls.Add(this.cboFleets);
            this.fraFleetSelect.Controls.Add(this.cmbGroup);
            this.fraFleetSelect.Location = new System.Drawing.Point(30, 30);
            this.fraFleetSelect.Name = "fraFleetSelect";
            this.fraFleetSelect.Size = new System.Drawing.Size(340, 152);
            this.fraFleetSelect.TabIndex = 5;
            this.fraFleetSelect.Text = "Please Select The Fleet / Group";
            // 
            // cboFleets
            // 
            this.cboFleets.BackColor = System.Drawing.SystemColors.Window;
            this.cboFleets.Location = new System.Drawing.Point(20, 90);
            this.cboFleets.Name = "cboFleets";
            this.cboFleets.Size = new System.Drawing.Size(298, 40);
            this.cboFleets.TabIndex = 8;
            // 
            // fraSelectVehicles
            // 
            this.fraSelectVehicles.Controls.Add(this.cmbPlateOption);
            this.fraSelectVehicles.Controls.Add(this.cmdClearAll);
            this.fraSelectVehicles.Controls.Add(this.cmdSelectAll);
            this.fraSelectVehicles.Controls.Add(this.vsVehicles);
            this.fraSelectVehicles.Controls.Add(this.lblFleetNameVehicles);
            this.fraSelectVehicles.Location = new System.Drawing.Point(30, 30);
            this.fraSelectVehicles.Name = "fraSelectVehicles";
            this.fraSelectVehicles.Size = new System.Drawing.Size(553, 492);
            this.fraSelectVehicles.Text = "Please Select The Vehicles To Register";
            // 
            // cmdClearAll
            // 
            this.cmdClearAll.AppearanceKey = "actionButton";
            this.cmdClearAll.Location = new System.Drawing.Point(164, 432);
            this.cmdClearAll.Name = "cmdClearAll";
            this.cmdClearAll.Size = new System.Drawing.Size(108, 40);
            this.cmdClearAll.TabIndex = 2;
            this.cmdClearAll.Text = "Clear All";
            this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.AppearanceKey = "actionButton";
            this.cmdSelectAll.Location = new System.Drawing.Point(20, 432);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(114, 40);
            this.cmdSelectAll.TabIndex = 1;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // vsVehicles
            // 
            this.vsVehicles.Cols = 11;
            this.vsVehicles.ExtendLastCol = true;
            this.vsVehicles.Location = new System.Drawing.Point(20, 84);
            this.vsVehicles.Name = "vsVehicles";
            this.vsVehicles.Rows = 23;
            this.vsVehicles.ShowFocusCell = false;
            this.vsVehicles.Size = new System.Drawing.Size(513, 332);
            this.vsVehicles.TabIndex = 3;
            this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
            this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
            // 
            // lblFleetNameVehicles
            // 
            this.lblFleetNameVehicles.Location = new System.Drawing.Point(266, 41);
            this.lblFleetNameVehicles.Name = "lblFleetNameVehicles";
            this.lblFleetNameVehicles.Size = new System.Drawing.Size(234, 15);
            this.lblFleetNameVehicles.TabIndex = 4;
            this.lblFleetNameVehicles.Text = "REGISTRATION LENGTH";
            // 
            // MainMenu1
            // 
            this.MainMenu1.Name = null;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmbPlateOption
            // 
            this.cmbPlateOption.Items.AddRange(new object[] {
            "Group",
            "Fleet"});
            this.cmbPlateOption.Location = new System.Drawing.Point(20, 30);
            this.cmbPlateOption.Name = "cmbPlateOption";
            this.cmbPlateOption.Size = new System.Drawing.Size(217, 40);
            this.cmbPlateOption.TabIndex = 1;
            this.cmbPlateOption.Text = "Fleet";
            // 
            // frmLongTermReRegBatch
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(662, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmLongTermReRegBatch";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Long Term Fleet Registration";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmLongTermReRegBatch_Load);
            this.Activated += new System.EventHandler(this.frmLongTermReRegBatch_Activated);
            this.Resize += new System.EventHandler(this.frmLongTermReRegBatch_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLongTermReRegBatch_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprint)).EndInit();
            this.fraReprint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelReprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRegLength)).EndInit();
            this.fraRegLength.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsRegLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQueue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFleetSelect)).EndInit();
            this.fraFleetSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectVehicles)).EndInit();
            this.fraSelectVehicles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        public FCComboBox cmbPlateOption;
    }
}