﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptProcessFinancialSubtotals.
	/// </summary>
	public partial class srptProcessFinancialSubtotals : FCSectionReport
	{
		public srptProcessFinancialSubtotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptProcessFinancialSubtotals InstancePtr
		{
			get
			{
				return (srptProcessFinancialSubtotals)Sys.GetInstance(typeof(srptProcessFinancialSubtotals));
			}
		}

		protected srptProcessFinancialSubtotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptProcessFinancialSubtotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strType = "";
		bool blnFirstRecord;
		int counter;
		Decimal curExciseTotal;
		Decimal curAgentFeeTotal;
		Decimal curRegFeeTotal;
		Decimal curVanityTotal;
		Decimal curUncodedTotal;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeTryAgain = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (strType == "D")
				{
					if (counter <= MotorVehicle.Statics.DateMax - 1)
					{
						eArgs.EOF = false;
					}
					else
					{
						strType = "C";
						counter = 0;
						executeTryAgain = true;
						goto TryAgain;
					}
				}
				else
				{
					if (counter <= MotorVehicle.Statics.ClassMax - 1)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				counter += 1;
				executeTryAgain = true;
				goto TryAgain;
			}
			TryAgain:
			;
			if (executeTryAgain)
			{
				if (strType == "D")
				{
					if (counter <= MotorVehicle.Statics.DateMax - 1)
					{
						eArgs.EOF = false;
					}
					else
					{
						strType = "C";
						counter = 0;
						executeTryAgain = true;
						goto TryAgain;
					}
				}
				else
				{
					if (counter <= MotorVehicle.Statics.ClassMax - 1)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
				executeTryAgain = false;
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = strType;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldExciseTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldAgentFeeTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldRegFeeTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldVanityTotal As object	OnWrite(string)
			// vbPorter upgrade warning: fldUncodedTotal As object	OnWrite(string)
			strType = "D";
			blnFirstRecord = true;
			for (counter = 0; counter <= MotorVehicle.Statics.ClassMax - 1; counter++)
			{
				curExciseTotal += FCConvert.ToDecimal(MotorVehicle.Statics.ClassMoney[counter, 2]);
				curAgentFeeTotal += FCConvert.ToDecimal(MotorVehicle.Statics.ClassMoney[counter, 3]);
				curRegFeeTotal += FCConvert.ToDecimal(MotorVehicle.Statics.ClassMoney[counter, 0]);
				curVanityTotal += FCConvert.ToDecimal(MotorVehicle.Statics.ClassMoney[counter, 1]);
				curUncodedTotal += FCConvert.ToDecimal(MotorVehicle.Statics.ClassMoney[counter, 4]);
			}
			fldExciseTotal.Text = Strings.Format(curExciseTotal, "#,##0.00");
			fldAgentFeeTotal.Text = Strings.Format(curAgentFeeTotal, "#,##0.00");
			fldRegFeeTotal.Text = Strings.Format(curRegFeeTotal, "#,##0.00");
			fldVanityTotal.Text = Strings.Format(curVanityTotal, "#,##0.00");
			fldUncodedTotal.Text = Strings.Format(curUncodedTotal, "#,##0.00");
			counter = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (strType == "D")
			{
				fldDescription.Text = MotorVehicle.Statics.TransactionDates[counter];
				txtExcise1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.TransactionDatesMoney[counter, 2], "#,##0.00"), "@@@@@@@@");
				txtAgent1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.TransactionDatesMoney[counter, 3], "#,##0.00"), "@@@@@@@@");
				txtReg1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.TransactionDatesMoney[counter, 0], "#,##0.00"), "@@@@@@@@");
				txtVanity1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.TransactionDatesMoney[counter, 1], "#,##0.00"), "@@@@@@@@");
				txtUncoded1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.TransactionDatesMoney[counter, 4], "#,##0.00"), "@@@@@@@@");
			}
			else
			{
				fldDescription.Text = MotorVehicle.Statics.ClassDesc[counter];
				txtExcise1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.ClassMoney[counter, 2], "#,##0.00"), "@@@@@@@@");
				txtAgent1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.ClassMoney[counter, 3], "#,##0.00"), "@@@@@@@@");
				txtReg1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.ClassMoney[counter, 0], "#,##0.00"), "@@@@@@@@");
				txtVanity1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.ClassMoney[counter, 1], "#,##0.00"), "@@@@@@@@");
				txtUncoded1.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.ClassMoney[counter, 4], "#,##0.00"), "@@@@@@@@");
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lblDescription As object	OnWrite(string)
			if (strType == "D")
			{
				lblSummaryTitle.Text = "* * * * * * * * Date Summary * * * * * * * *";
				lblDescription.Text = "Date";
			}
			else
			{
				lblSummaryTitle.Text = "* * * * * * * * Class Summary * * * * * * * *";
				lblDescription.Text = "Class";
			}
		}

		
	}
}
