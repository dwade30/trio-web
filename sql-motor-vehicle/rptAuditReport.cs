//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptAuditReport.
	/// </summary>
	public partial class rptAuditReport : BaseSectionReport
	{
		public rptAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Audit Archive Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptAuditReport InstancePtr
		{
			get
			{
				return (rptAuditReport)Sys.GetInstance(typeof(rptAuditReport));
			}
		}

		protected rptAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAuditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					eArgs.EOF = false;
				}
				else
				{
					rsData.MoveNext();
					eArgs.EOF = rsData.EndOfFile();
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsArchiveInfo = new clsDRWrapper();
			clsDRWrapper rsAuditInfo = new clsDRWrapper();
			rsAuditInfo.OpenRecordset("SELECT * FROM AuditChanges", MotorVehicle.DEFAULTDATABASE);
			rsArchiveInfo.OpenRecordset("SELECT * FROM AuditChangesArchive", MotorVehicle.DEFAULTDATABASE);
			if (rsAuditInfo.EndOfFile() != true && rsAuditInfo.BeginningOfFile() != true)
			{
				do
				{
					rsArchiveInfo.AddNew();
					rsArchiveInfo.Set_Fields("Location", rsAuditInfo.Get_Fields_String("Location"));
					rsArchiveInfo.Set_Fields("ChangeDescription", rsAuditInfo.Get_Fields_String("ChangeDescription"));
					rsArchiveInfo.Set_Fields("UserField1", rsAuditInfo.Get_Fields_String("UserField1"));
					rsArchiveInfo.Set_Fields("UserField2", rsAuditInfo.Get_Fields_String("UserField2"));
					rsArchiveInfo.Set_Fields("UserField3", rsAuditInfo.Get_Fields_String("UserField3"));
					rsArchiveInfo.Set_Fields("UserField4", rsAuditInfo.Get_Fields_String("UserField4"));
					rsArchiveInfo.Set_Fields("UserID", rsAuditInfo.Get_Fields("UserID"));
					rsArchiveInfo.Set_Fields("DateUpdated", rsAuditInfo.Get_Fields_DateTime("DateUpdated"));
					rsArchiveInfo.Set_Fields("TimeUpdated", rsAuditInfo.Get_Fields_DateTime("TimeUpdated"));
					rsArchiveInfo.Update();
					rsAuditInfo.Delete();
					rsAuditInfo.Update();
					rsAuditInfo.MoveNext();
				}
				while (rsAuditInfo.EndOfFile() != true);
			}
			rsData.DisposeOf();
			rsArchiveInfo.DisposeOf();
			rsAuditInfo.DisposeOf();
			MessageBox.Show("Audit information archived.", "Audit Info Archived", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			blnFirstRecord = true;
			rsData.OpenRecordset("Select * from AuditChanges ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1, UserField2", MotorVehicle.DEFAULTDATABASE);
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldPlate.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField1"));
			fldVIN.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField2"));
			fldOPID.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField3"));
			txtUser.Text = FCConvert.ToString(rsData.Get_Fields("UserID"));
			txtDateTimeField.Text = Strings.Format(rsData.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy") + " " + Strings.Format(rsData.Get_Fields_DateTime("TimeUpdated"),"h:mm tt");
			fldLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location")));
			fldDescription.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void rptAuditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAuditReport properties;
			//rptAuditReport.Caption	= "Audit Archive Report";
			//rptAuditReport.Icon	= "rptAuditReport.dsx":0000";
			//rptAuditReport.Left	= 0;
			//rptAuditReport.Top	= 0;
			//rptAuditReport.Width	= 19080;
			//rptAuditReport.Height	= 12990;
			//rptAuditReport.StartUpPosition	= 3;
			//rptAuditReport.WindowState	= 2;
			//rptAuditReport.SectionData	= "rptAuditReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
