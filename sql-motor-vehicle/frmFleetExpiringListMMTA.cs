//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmFleetExpiringListMMTA : BaseForm
	{
		public frmFleetExpiringListMMTA()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFleetExpiringListMMTA InstancePtr
		{
			get
			{
				return (frmFleetExpiringListMMTA)Sys.GetInstance(typeof(frmFleetExpiringListMMTA));
			}
		}

		protected frmFleetExpiringListMMTA _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int SelectCol;
		int FleetNumberCol;
		int FleetNameCol;
		public string strFleets = "";

		private void cboEndName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void cboStartName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsFleets.Rows - 1; counter++)
			{
				vsFleets.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsFleets.Rows - 1; counter++)
			{
				vsFleets.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void frmFleetExpiringListMMTA_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmFleetExpiringListMMTA_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFleetExpiringListMMTA properties;
			//frmFleetExpiringListMMTA.FillStyle	= 0;
			//frmFleetExpiringListMMTA.ScaleWidth	= 9045;
			//frmFleetExpiringListMMTA.ScaleHeight	= 7335;
			//frmFleetExpiringListMMTA.LinkTopic	= "Form2";
			//frmFleetExpiringListMMTA.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SelectCol = 0;
			FleetNumberCol = 1;
			FleetNameCol = 2;
			FormatGrid();
			FillCombos();
		}

		private void FillCombos()
		{
			int counter;
			cboStartName.Clear();
			cboEndName.Clear();
			cboStartName.AddItem(" ");
			cboEndName.AddItem(" ");
			for (counter = 65; counter <= 90; counter++)
			{
				cboStartName.AddItem(FCConvert.ToString(Convert.ToChar(counter)));
				cboEndName.AddItem(FCConvert.ToString(Convert.ToChar(counter)));
			}
		}

		private void frmFleetExpiringListMMTA_Resize(object sender, System.EventArgs e)
		{
			vsFleets.ColWidth(SelectCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.15));
			vsFleets.ColWidth(FleetNumberCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.2));
		}

		private void frmFleetExpiringListMMTA_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			bool blnFound;
			int counter;
			if (!Information.IsDate(txtStartDate.Text))
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			else if (!Information.IsDate(txtEndDate.Text))
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtEndDate.Focus();
				return;
			}
			else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStartDate.Focus();
				return;
			}
			strFleets = "";
			blnFound = false;
			if (cmbAll.Text == "Selected" || cmbFleetTypeSelected.Text == "Selected")
			{
				for (counter = 1; counter <= vsFleets.Rows - 1; counter++)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsFleets.TextMatrix(counter, SelectCol)) == true)
					if (FCConvert.CBool(vsFleets.TextMatrix(counter, SelectCol)) == true)
					{
						blnFound = true;
						if (strFleets == "")
						{
							strFleets = vsFleets.TextMatrix(counter, FleetNumberCol);
						}
						else
						{
							strFleets += ", " + vsFleets.TextMatrix(counter, FleetNumberCol);
						}
					}
				}
			}
			else
			{
				blnFound = true;
			}
			if (!blnFound)
			{
				MessageBox.Show("You must select at least one fleet or group before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "STAAB AGENCY")
				{
					frmReportViewer.InstancePtr.Init(rptFleetReminderListStaab.InstancePtr);
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptFleetReminderListMMTA.InstancePtr);
				}
			}
			this.Hide();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbGroups.Enabled = false;
			if (cmbFleetTypeSelected.Text == "All" && cmbFleetNameSelected.Text == "All")
			{
				vsFleets.Visible = false;
				cmdSelectAll.Visible = false;
				cmdClearAll.Visible = false;
			}
			FillGrid();
		}

		private void optFleetNameAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraFleetNameSelected.Enabled = false;
			cboStartName.Enabled = false;
			cboEndName.Enabled = false;
			Label2.Enabled = false;
			if (cmbAll.Text == "All" && cmbFleetTypeSelected.Text == "All")
			{
				vsFleets.Visible = false;
				cmdSelectAll.Visible = false;
				cmdClearAll.Visible = false;
			}
			FillGrid();
		}

		private void optFleetNameSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraFleetNameSelected.Enabled = true;
			cboStartName.Enabled = true;
			cboStartName.SelectedIndex = 0;
			cboEndName.SelectedIndex = cboEndName.Items.Count - 1;
			cboEndName.Enabled = true;
			Label2.Enabled = true;
			FillGrid();
			vsFleets.Visible = true;
			cmdSelectAll.Visible = true;
			cmdClearAll.Visible = true;
		}

		private void optFleetTypeAll_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbFleetTypeSelected.Enabled = false;
			cmbAnnual.Enabled = false;
			if (cmbAll.Text == "All" && cmbFleetNameSelected.Text == "All")
			{
				vsFleets.Visible = false;
				cmdSelectAll.Visible = false;
				cmdClearAll.Visible = false;
			}
			FillGrid();
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void optGroups_CheckedChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void optAnnual_CheckedChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void optLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			FillGrid();
		}

		private void FormatGrid()
		{
			vsFleets.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsFleets.TextMatrix(0, FleetNumberCol, "Number");
			vsFleets.TextMatrix(0, FleetNameCol, "Owner");
			vsFleets.ColAlignment(FleetNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFleets.ColWidth(SelectCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.15));
			vsFleets.ColWidth(FleetNumberCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.2));
		}

		private void FillGrid()
		{
			clsDRWrapper rs = new clsDRWrapper();
			string strWhere = "";
			if (cmbAll.Text == "All")
			{
				strWhere = "Deleted <> 1";
			}
			else if (cmbGroups.Text == "Fleets")
			{
				strWhere = "Deleted <> 1 AND ExpiryMonth <> 99";
			}
			else
			{
				strWhere = "Deleted <> 1 AND ExpiryMonth = 99";
			}
			if (cmbFleetTypeSelected.Text == "All")
			{
				// do nothing
			}
			else if (cmbAnnual.Text == "Long Term")
			{
				if (strWhere == "")
				{
					strWhere = "Type = 'L'";
				}
				else
				{
					strWhere += " AND Type = 'L'";
				}
			}
			else
			{
				if (strWhere == "")
				{
					strWhere = "Type = 'A'";
				}
				else
				{
					strWhere += " AND Type = 'A'";
				}
			}
			if (cmbFleetNameSelected.Text == "All")
			{
				// do nothing
			}
			else
			{
				if (strWhere == "")
				{
					strWhere = "p.FullName >= '" + cboStartName.Text + "   ' AND p.FullName <= '" + cboEndName.Text + "zzzz'";
				}
				else
				{
					strWhere += " AND p.FullName >= '" + cboStartName.Text + "   ' AND p.FullName <= '" + cboEndName.Text + "zzzz'";
				}
			}
			if (strWhere != "")
			{
				strWhere = "WHERE " + strWhere;
			}
			rs.OpenRecordset("SELECT c.*, p.FullName as Party1FullName FROM FleetMaster as c LEFT JOIN " + rs.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID " + strWhere + " ORDER BY p.FullName");
			vsFleets.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsFleets.Rows += 1;
					vsFleets.TextMatrix(vsFleets.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsFleets.TextMatrix(vsFleets.Rows - 1, FleetNumberCol, FCConvert.ToString(rs.Get_Fields_Int32("FleetNumber")));
					vsFleets.TextMatrix(vsFleets.Rows - 1, FleetNameCol, FCConvert.ToString(rs.Get_Fields("Party1FullName")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void optStatusSelect_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbGroups.Enabled = true;
			FillGrid();
			vsFleets.Visible = true;
			cmdSelectAll.Visible = true;
			cmdClearAll.Visible = true;
		}

		private void optFleetTypeSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbFleetTypeSelected.Enabled = true;
			cmbAnnual.Enabled = true;
			FillGrid();
			vsFleets.Visible = true;
			cmdSelectAll.Visible = true;
			cmdClearAll.Visible = true;
		}

		private void vsFleets_ClickEvent(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
			//if (FCConvert.ToBoolean(vsFleets.TextMatrix(vsFleets.Row, SelectCol)) == true)
			if (FCConvert.CBool(vsFleets.TextMatrix(vsFleets.Row, SelectCol)) == true)
			{
				vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(false));
			}
			else
			{
				vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(true));
			}
		}

		private void vsFleets_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsFleets.TextMatrix(vsFleets.Row, SelectCol)) == true)
				if (FCConvert.CBool(vsFleets.TextMatrix(vsFleets.Row, SelectCol)) == true)
				{
					vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsFleets.TextMatrix(vsFleets.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void cmbGroups_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbGroups.Text == "Groups")
			{
				optGroups_CheckedChanged(sender, e);
			}
			else if (cmbGroups.Text == "Fleets")
			{
				optFleet_CheckedChanged(sender, e);
			}
		}

		private void cmbAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Selected")
			{
				optStatusSelect_CheckedChanged(sender, e);
			}
		}

		private void cmbAnnual_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAnnual.Text == "Annual")
			{
				optAnnual_CheckedChanged(sender, e);
			}
			else if (cmbAnnual.Text == "Long Term")
			{
				optLongTerm_CheckedChanged(sender, e);
			}
		}

		private void cmbFleetTypeSelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFleetTypeSelected.Text == "All")
			{
				optFleetTypeAll_CheckedChanged(sender, e);
			}
			else if (cmbFleetNameSelected.Text == "Selected")
			{
				optFleetTypeSelected_CheckedChanged(sender, e);
			}
		}

		private void cmbFleetNameSelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFleetNameSelected.Text == "All")
			{
				optFleetNameAll_CheckedChanged(sender, e);
			}
			else if (cmbFleetNameSelected.Text == "Selected")
			{
				optFleetNameSelected_CheckedChanged(sender, e);
			}
		}
	}
}
