﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptInventory.
	/// </summary>
	partial class rptInventory
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptInventory));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeading = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHigh = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHigh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtType,
				this.txtDate,
				this.txtOpID,
				this.txtLow,
				this.txtHigh,
				this.txtStatus,
				this.txtGroup,
				this.txtCount
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeading,
				this.lblType,
				this.lblDate,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Line1,
				this.Label1,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11
			});
			this.PageHeader.Height = 0.8645833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeading
			// 
			this.lblHeading.Height = 0.1875F;
			this.lblHeading.HyperLink = null;
			this.lblHeading.Left = 0F;
			this.lblHeading.Name = "lblHeading";
			this.lblHeading.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblHeading.Text = null;
			this.lblHeading.Top = 0.375F;
			this.lblHeading.Width = 6.5F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 0.125F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.65625F;
			this.lblType.Width = 0.375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.65625F;
			this.lblDate.Width = 0.375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.8125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Op ID";
			this.Label2.Top = 0.65625F;
			this.Label2.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label3.Text = "Low";
			this.Label3.Top = 0.65625F;
			this.Label3.Width = 0.375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label4.Text = "High";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 0.375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.25F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label5.Text = "Status";
			this.Label5.Top = 0.65625F;
			this.Label5.Width = 0.5F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label6.Text = "Count";
			this.Label6.Top = 0.65625F;
			this.Label6.Width = 0.4375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Group";
			this.Label7.Top = 0.65625F;
			this.Label7.Width = 0.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.84375F;
			this.Line1.Width = 6.46875F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 0.84375F;
			this.Line1.Y2 = 0.84375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Inventory Printout";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.40625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label8.Text = "Label8";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.1875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label11.Text = "Label11";
			this.Label11.Top = 0F;
			this.Label11.Width = 1.3125F;
			// 
			// txtType
			// 
			this.txtType.CanShrink = true;
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 0.125F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtType.Text = null;
			this.txtType.Top = 0F;
			this.txtType.Width = 0.5F;
			// 
			// txtDate
			// 
			this.txtDate.CanShrink = true;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 0.8125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 0.875F;
			// 
			// txtOpID
			// 
			this.txtOpID.CanShrink = true;
			this.txtOpID.Height = 0.1875F;
			this.txtOpID.Left = 1.8125F;
			this.txtOpID.Name = "txtOpID";
			this.txtOpID.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtOpID.Text = null;
			this.txtOpID.Top = 0F;
			this.txtOpID.Width = 0.6875F;
			// 
			// txtLow
			// 
			this.txtLow.CanShrink = true;
			this.txtLow.Height = 0.1875F;
			this.txtLow.Left = 2.625F;
			this.txtLow.Name = "txtLow";
			this.txtLow.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtLow.Text = null;
			this.txtLow.Top = 0F;
			this.txtLow.Width = 0.6875F;
			// 
			// txtHigh
			// 
			this.txtHigh.CanShrink = true;
			this.txtHigh.Height = 0.1875F;
			this.txtHigh.Left = 3.4375F;
			this.txtHigh.Name = "txtHigh";
			this.txtHigh.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtHigh.Text = null;
			this.txtHigh.Top = 0F;
			this.txtHigh.Width = 0.6875F;
			// 
			// txtStatus
			// 
			this.txtStatus.CanShrink = true;
			this.txtStatus.Height = 0.1875F;
			this.txtStatus.Left = 4.25F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtStatus.Text = null;
			this.txtStatus.Top = 0F;
			this.txtStatus.Width = 0.6875F;
			// 
			// txtGroup
			// 
			this.txtGroup.CanShrink = true;
			this.txtGroup.Height = 0.1875F;
			this.txtGroup.Left = 5F;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtGroup.Text = null;
			this.txtGroup.Top = 0F;
			this.txtGroup.Width = 0.6875F;
			// 
			// txtCount
			// 
			this.txtCount.CanShrink = true;
			this.txtCount.Height = 0.1875F;
			this.txtCount.Left = 5.8125F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.txtCount.Text = null;
			this.txtCount.Top = 0F;
			this.txtCount.Width = 0.6875F;
			// 
			// rptInventory
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.520833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHigh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHigh;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
