//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTellerReport.
	/// </summary>
	public partial class rptTellerReport : BaseSectionReport
	{
		public rptTellerReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Teller Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptTellerReport_ReportEnd;
		}

        private void RptTellerReport_ReportEnd(object sender, EventArgs e)
        {
			rsTransit.DisposeOf();
            rsSpecial.DisposeOf();
            rsBooster.DisposeOf();
            rsActivity.DisposeOf();
            rsFees.DisposeOf();

		}

        public static rptTellerReport InstancePtr
		{
			get
			{
				return (rptTellerReport)Sys.GetInstance(typeof(rptTellerReport));
			}
		}

		protected rptTellerReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTellerReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		int TotalUnits;
		double TotalMoney;
		double TotalLocal;
		double TotalState;
		int SaveUnits;
		double SaveMoney;
		double SaveLocal;
		double SaveState;
		bool blnFirstRecord;
		bool blnFirst;
		Decimal TotalAgent;
		Decimal TotalLocalTransfer;
		Decimal TotalExcise;
		Decimal TotalUseTax;
		Decimal TotalTitleFee;
		Decimal TotalReg;
		Decimal TotalBooster;
		Decimal TotalDupReg;
		Decimal TotalSpecialtyPlate;
		Decimal TotalInitialPlate;
		Decimal TotalStateTransfer;
		Decimal TotalTransit;
		Decimal TotalSpecialPermit;
		clsDRWrapper rsTransit = new clsDRWrapper();
		clsDRWrapper rsSpecial = new clsDRWrapper();
		clsDRWrapper rsBooster = new clsDRWrapper();
		clsDRWrapper rsActivity = new clsDRWrapper();
		bool blnDoingTransit;
		bool blnDoingSpecial;
		bool blnDoingBooster;
		string strDates = "";
		//clsDRWrapper rsVehicleInfo = new clsDRWrapper();
		clsDRWrapper rsFees = new clsDRWrapper();

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			rptTellerReport.InstancePtr.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckNewTeller = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				blnFirst = false;
				if (rsActivity.EndOfFile() != true && rsActivity.BeginningOfFile() != true)
				{
					blnDoingTransit = false;
					blnDoingSpecial = false;
					blnDoingBooster = false;
					eArgs.EOF = false;
				}
				else if (rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true)
				{
					blnDoingTransit = true;
					blnDoingSpecial = false;
					blnDoingBooster = false;
					eArgs.EOF = false;
				}
				else if (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true)
				{
					blnDoingTransit = false;
					blnDoingSpecial = true;
					blnDoingBooster = false;
					eArgs.EOF = false;
				}
				else if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
				{
					blnDoingTransit = false;
					blnDoingSpecial = false;
					blnDoingBooster = true;
					eArgs.EOF = false;
				}
				else
				{
					executeCheckNewTeller = true;
					goto CheckNewTeller;
				}
			}
			else
			{
				executeCheckNewTeller = true;
				goto CheckNewTeller;
			}
			CheckNewTeller:
			;
			if (executeCheckNewTeller)
			{
				if (blnDoingTransit == false && blnDoingSpecial == false && blnDoingBooster == false)
				{
					if (blnFirst)
					{
						blnFirst = false;
					}
					else
					{
						rsActivity.MoveNext();
					}
					if (rsActivity.EndOfFile() != true)
					{
						eArgs.EOF = false;
						goto SetBinder;
					}
					else
					{
						blnFirst = true;
						blnDoingTransit = true;
					}
				}
				if (blnDoingTransit == true)
				{
					if (blnFirst)
					{
						blnFirst = false;
					}
					else
					{
						rsTransit.MoveNext();
					}
					if (rsTransit.EndOfFile() != true)
					{
						eArgs.EOF = false;
						goto SetBinder;
					}
					else
					{
						blnFirst = true;
						blnDoingTransit = false;
						blnDoingSpecial = true;
					}
				}
				if (blnDoingSpecial == true)
				{
					if (blnFirst)
					{
						blnFirst = false;
					}
					else
					{
						rsSpecial.MoveNext();
					}
					if (rsSpecial.EndOfFile() != true)
					{
						eArgs.EOF = false;
						goto SetBinder;
					}
					else
					{
						blnFirst = true;
						blnDoingSpecial = false;
						blnDoingBooster = true;
					}
				}
				if (blnDoingBooster == true)
				{
					if (blnFirst)
					{
						blnFirst = false;
					}
					else
					{
						rsBooster.MoveNext();
					}
					if (rsBooster.EndOfFile() != true)
					{
						eArgs.EOF = false;
						goto SetBinder;
					}
					else
					{
						blnFirst = true;
						blnDoingBooster = false;
					}
				}
				MotorVehicle.Statics.rsTellers.MoveNext();
				if (MotorVehicle.Statics.rsTellers.EndOfFile() != true)
				{
					if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 0)
					{
						rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "' ORDER BY OpID, MVR3");
					}
					else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 1)
					{
						rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK2) + " ORDER BY OpID, MVR3");
					}
					else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 3)
					{
						rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND Convert(Date, DateUpdated) BETWEEN '" + frmTellerReport.InstancePtr.txtStartDate.Text + "' and '" + frmTellerReport.InstancePtr.txtEndDate.Text + "' ORDER BY OpID, MVR3");
					}
					else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 2)
					{
						rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ") ORDER BY OpID, MVR3");
					}
					else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 4)
					{
						rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 = 0 ORDER BY OpID, MVR3");
					}
					else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 5)
					{
						rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID <> 0 ORDER BY OpID, MVR3");
					}
					else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 6)
					{
						rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID = 0 ORDER BY OpID, MVR3");
					}
					rsTransit.OpenRecordset("SELECT * FROM TransitPlates WHERE Voided = 0 AND OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND " + strDates);
					rsBooster.OpenRecordset("SELECT * FROM Booster WHERE Voided = 0 AND OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND " + strDates);
					rsSpecial.OpenRecordset("SELECT * FROM SpecialRegistration WHERE Voided = 0 AND OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND " + strDates);
					blnFirst = true;
					executeCheckNewTeller = true;
					goto CheckNewTeller;
				}
				else
				{
					eArgs.EOF = true;
				}
				executeCheckNewTeller = false;
			}
			SetBinder:
			;
			if (eArgs.EOF != true)
			{
				this.Fields["Binder"].Value = fecherFoundation.Strings.UCase(FCConvert.ToString(MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID")));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			TotalUnits = 0;
			TotalMoney = 0;
			TotalLocal = 0;
			TotalState = 0;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 0)
			{
				Label5.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 1)
			{
				Label5.Text = Strings.Format(frmTellerReport.InstancePtr.cboStart.Items[frmTellerReport.InstancePtr.cboStart.SelectedIndex].ToString(), "MM/dd/yyyy") + " TO " + Strings.Format(frmTellerReport.InstancePtr.cboEnd.Items[frmTellerReport.InstancePtr.cboEnd.SelectedIndex].ToString(), "MM/dd/yyyy");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 3)
			{
				Label5.Text = frmTellerReport.InstancePtr.txtStartDate.Text + " TO " + frmTellerReport.InstancePtr.txtEndDate.Text;
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 2)
			{
				Label5.Text = "Specific Teller Closeout Periods";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 4)
			{
				Label5.Text = "All Activity Since Last Closeout Period";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 5)
			{
				Label5.Text = "All Closed Out Activity Since Last Period Closeout";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 6)
			{
				Label5.Text = "All Non Closed Out Activity Since Last Period Closeout";
			}
			if (frmTellerReport.InstancePtr.cmbYes.Text == "Yes")
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			else
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			GroupFooter2.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			blnFirst = true;
			blnFirstRecord = true;
			MotorVehicle.Statics.rsTellers.MoveFirst();
			blnDoingBooster = false;
			blnDoingSpecial = false;
			blnDoingTransit = false;
			if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 1)
			{
				strDates = " PeriodCloseoutID > " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK2);
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 3)
			{
				strDates = " Convert(Date, DateUpdated) BETWEEN '" + frmTellerReport.InstancePtr.txtStartDate.Text + "' and '" + frmTellerReport.InstancePtr.txtEndDate.Text + "'";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 0)
			{
				strDates = " Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "'";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 2)
			{
				strDates = " TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ")";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 4)
			{
				strDates = " PeriodCloseoutID = 0";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 5)
			{
				strDates = " PeriodCloseoutID = 0 AND TellerCloseoutID <> 0";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 6)
			{
				strDates = " PeriodCloseoutID = 0 AND TellerCloseoutID = 0";
			}
			rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
			if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 0)
			{
				rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "' ORDER BY OpID, MVR3");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 1)
			{
				rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK2) + " ORDER BY OpID, MVR3");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 3)
			{
				rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND Convert(Date, DateUpdated) BETWEEN '" + frmTellerReport.InstancePtr.txtStartDate.Text + "' and '" + frmTellerReport.InstancePtr.txtEndDate.Text + "' ORDER BY OpID, MVR3");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 2)
			{
				rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ") ORDER BY OpID, MVR3");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 4)
			{
				rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 = 0 ORDER BY OpID, MVR3");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 5)
			{
				rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID <> 0 ORDER BY OpID, MVR3");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 6)
			{
				rsActivity.OpenRecordset("SELECT * FROM ActivityMaster WHERE OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID = 0 ORDER BY OpID, MVR3");
			}
			rsTransit.OpenRecordset("SELECT * FROM TransitPlates WHERE Voided = 0 AND OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND " + strDates);
			rsBooster.OpenRecordset("SELECT * FROM Booster WHERE Voided = 0 AND OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND " + strDates);
			rsSpecial.OpenRecordset("SELECT * FROM SpecialRegistration WHERE Voided = 0 AND OpID = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "' AND " + strDates);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnDoingTransit)
			{
				TotalUnits += 1;
				fldMVR3.Text = "";
				fldClass.Text = "";
				fldPlate.Text = rsTransit.Get_Fields_String("plate");
				fldType.Text = "TST";
				fldYear.Text = rsTransit.Get_Fields_String("Year");
				fldMake.Text = rsTransit.Get_Fields_String("make");
				fldModel.Text = rsTransit.Get_Fields_String("model").Left(6);
				TotalMoney += rsTransit.Get_Fields_Double("StatePaid") + rsTransit.Get_Fields_Double("LocalPaid");
				TotalLocal += rsTransit.Get_Fields_Double("LocalPaid");
				TotalState += rsTransit.Get_Fields_Double("StatePaid");
				fldLocal.Text = Strings.Format(rsTransit.Get_Fields("LocalPaid"), "#,##0.00");
				fldState.Text = Strings.Format(rsTransit.Get_Fields("StatePaid"), "#,##0.00");
				fldTotal.Text = Strings.Format(rsTransit.Get_Fields("StatePaid") + rsTransit.Get_Fields("LocalPaid"), "#,##0.00");
				TotalAgent += rsTransit.Get_Fields("LocalPaid");
				TotalTransit += rsTransit.Get_Fields("StatePaid");
			}
			else if (blnDoingSpecial)
			{
				TotalUnits += 1;
				fldMVR3.Text = "";
				fldClass.Text = "";
				fldPlate.Text = "";
				fldType.Text = "SPR";
				fldYear.Text = rsSpecial.Get_Fields_String("Year");
				fldMake.Text = rsSpecial.Get_Fields_String("make");
				fldModel.Text = "";
				TotalMoney += rsSpecial.Get_Fields_Double("StatePaid") + rsSpecial.Get_Fields_Double("LocalPaid");
				TotalLocal += rsSpecial.Get_Fields_Double("LocalPaid");
				TotalState += rsSpecial.Get_Fields_Double("StatePaid");
				fldLocal.Text = Strings.Format(rsSpecial.Get_Fields("LocalPaid"), "#,##0.00");
				fldState.Text = Strings.Format(rsSpecial.Get_Fields("StatePaid"), "#,##0.00");
				fldTotal.Text = Strings.Format(rsSpecial.Get_Fields("StatePaid") + rsSpecial.Get_Fields("LocalPaid"), "#,##0.00");
				TotalAgent += rsSpecial.Get_Fields("LocalPaid");
				TotalSpecialPermit += rsSpecial.Get_Fields("StatePaid");
			}
			else if (blnDoingBooster)
			{
				TotalUnits += 1;
				fldMVR3.Text = "";
				fldYear.Text = "";
				fldMake.Text = "";
				fldModel.Text = "";
				if (!fecherFoundation.FCUtils.IsNull(rsBooster.Get_Fields_String("plate")))
				{
					fldPlate.Text = rsBooster.Get_Fields_String("plate");
				}
				else
				{
					fldPlate.Text = "";
				}
				if (!fecherFoundation.FCUtils.IsNull(rsBooster.Get_Fields_String("Class")))
				{
					fldClass.Text = rsBooster.Get_Fields_String("Class");
				}
				else
				{
					fldClass.Text = "";
				}
				fldType.Text = "BST";
				TotalMoney += rsBooster.Get_Fields_Double("StatePaid") + rsBooster.Get_Fields_Double("LocalPaid");
				TotalLocal += rsBooster.Get_Fields_Double("LocalPaid");
				TotalState += rsBooster.Get_Fields_Double("StatePaid");
				fldLocal.Text = Strings.Format(rsBooster.Get_Fields("LocalPaid"), "#,##0.00");
				fldState.Text = Strings.Format(rsBooster.Get_Fields("StatePaid"), "#,##0.00");
				fldTotal.Text = Strings.Format(rsBooster.Get_Fields("StatePaid") + rsBooster.Get_Fields("LocalPaid"), "#,##0.00");
				TotalAgent += rsBooster.Get_Fields("LocalPaid");
				TotalBooster += rsBooster.Get_Fields("StatePaid");
			}
			else
			{
				TotalUnits += 1;
				fldClass.Text = rsActivity.Get_Fields_String("Class");
				fldPlate.Text = rsActivity.Get_Fields_String("plate");
				fldMVR3.Text = rsActivity.Get_Fields_String("MVR3");
				fldType.Text = rsActivity.Get_Fields_String("TransactionType");
				fldYear.Text = rsActivity.Get_Fields_String("Year");
				fldMake.Text = rsActivity.Get_Fields_String("make");
				fldModel.Text = rsActivity.Get_Fields_String("model").Left(6);
				if (FCConvert.ToString(rsActivity.Get_Fields("TransactionType")) == "DPR")
				{
					if (FCConvert.ToString(rsActivity.Get_Fields_String("Class")) != "CI")
					{
						TotalLocal += rsFees.Get_Fields_Double("DupRegAgentFee");
						fldLocal.Text = Strings.Format(rsFees.Get_Fields_Decimal("DupRegAgentFee"), "#,##0.00");
						TotalAgent += rsFees.Get_Fields_Decimal("DupRegAgentFee");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields_Decimal("DuplicateRegistrationFee") + rsFees.Get_Fields_Decimal("DupRegAgentFee"), "#,##0.00");
						TotalMoney += rsActivity.Get_Fields_Double("DuplicateRegistrationFee") + rsFees.Get_Fields_Double("DupRegAgentFee");
					}
					else
					{
						fldLocal.Text = Strings.Format(0, "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields_Decimal("DuplicateRegistrationFee"), "#,##0.00");
						TotalMoney += rsActivity.Get_Fields_Double("DuplicateRegistrationFee");
					}
					TotalState += rsActivity.Get_Fields_Double("DuplicateRegistrationFee");
					fldState.Text = Strings.Format(rsActivity.Get_Fields_Decimal("DuplicateRegistrationFee"), "#,##0.00");
					TotalDupReg += rsActivity.Get_Fields_Decimal("DuplicateRegistrationFee");
				}
				else if (FCConvert.ToString(rsActivity.Get_Fields("TransactionType")) == "NRR" || FCConvert.ToString(rsActivity.Get_Fields("TransactionType")) == "RRR")
				{
					if (FCConvert.ToBoolean(rsActivity.Get_Fields_Boolean("ETO")))
					{
						TotalExcise += (rsActivity.Get_Fields_Decimal("ExciseTaxCharged") - rsActivity.Get_Fields_Decimal("ExciseCreditUsed"));
						TotalMoney += rsActivity.Get_Fields_Double("LocalPaid");
						TotalLocal += rsActivity.Get_Fields_Double("LocalPaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
						fldState.Text = Strings.Format(0, "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
					}
					else if (rsActivity.Get_Fields_Boolean("EAP"))
					{
						TotalAgent += rsActivity.Get_Fields("AgentFee");
						TotalReg += rsActivity.Get_Fields_Decimal("RegRateCharge") - rsActivity.Get_Fields_Decimal("CommercialCredit") + rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee");
						TotalSpecialtyPlate += FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeReReg")) + FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeNew"));
						if (FCConvert.ToString(rsActivity.Get_Fields_String("GiftCertOrVoucher")) == "V")
						{
							TotalSpecialtyPlate -= FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("GiftCertificateAmount"));
						}
						TotalInitialPlate += rsActivity.Get_Fields_Decimal("InitialFee");
						TotalUseTax += rsActivity.Get_Fields("SalesTax");
						TotalTitleFee += rsActivity.Get_Fields("TitleFee");
						TotalMoney += rsActivity.Get_Fields_Double("StatePaid") + rsActivity.Get_Fields_Double("AgentFee");
						TotalLocal += rsActivity.Get_Fields_Double("AgentFee");
						TotalState += rsActivity.Get_Fields_Double("StatePaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("AgentFee"), "#,##0.00");
						fldState.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("StatePaid") + rsActivity.Get_Fields("AgentFee"), "#,##0.00");
					}
					else
					{
						TotalAgent += rsActivity.Get_Fields("AgentFee");
						TotalExcise += (rsActivity.Get_Fields_Decimal("ExciseTaxCharged") - rsActivity.Get_Fields_Decimal("ExciseCreditUsed"));
						TotalReg += rsActivity.Get_Fields_Decimal("RegRateCharge") - rsActivity.Get_Fields_Decimal("CommercialCredit") + rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee") - rsActivity.Get_Fields_Decimal("TransferCreditUsed");
						TotalSpecialtyPlate += FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeReReg")) + FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeNew"));
						if (FCConvert.ToString(rsActivity.Get_Fields_String("GiftCertOrVoucher")) == "V")
						{
							TotalSpecialtyPlate -= FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("GiftCertificateAmount"));
						}
						TotalInitialPlate += rsActivity.Get_Fields_Decimal("InitialFee");
						TotalUseTax += rsActivity.Get_Fields("SalesTax");
						TotalTitleFee += rsActivity.Get_Fields("TitleFee");
						TotalMoney += rsActivity.Get_Fields_Double("StatePaid") + rsActivity.Get_Fields_Double("LocalPaid");
						TotalLocal += rsActivity.Get_Fields_Double("LocalPaid");
						TotalState += rsActivity.Get_Fields_Double("StatePaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
						fldState.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("StatePaid") + rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
					}
				}
				else if (FCConvert.ToString(rsActivity.Get_Fields("TransactionType")) == "NRT" || FCConvert.ToString(rsActivity.Get_Fields("TransactionType")) == "RRT")
				{
					if (FCConvert.ToBoolean(rsActivity.Get_Fields_Boolean("ETO")))
					{
						if (rsActivity.Get_Fields_Decimal("ExciseTaxCharged") >= rsActivity.Get_Fields_Decimal("ExciseCreditUsed"))
						{
							TotalExcise += (rsActivity.Get_Fields_Decimal("ExciseTaxCharged") - rsActivity.Get_Fields_Decimal("ExciseCreditUsed"));
						}
						TotalLocalTransfer += rsActivity.Get_Fields_Decimal("ExciseTransferCharge");
						TotalMoney += rsActivity.Get_Fields_Double("LocalPaid");
						TotalLocal += rsActivity.Get_Fields_Double("LocalPaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
						fldState.Text = Strings.Format(0, "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
					}
					else if (rsActivity.Get_Fields_Boolean("EAP"))
					{
						TotalAgent += rsActivity.Get_Fields("AgentFee");
						if (rsActivity.Get_Fields_Decimal("RegRateCharge") >= (rsActivity.Get_Fields_Decimal("TransferCreditUsed") + rsActivity.Get_Fields_Decimal("CommercialCredit")))
						{
							TotalReg += (rsActivity.Get_Fields_Decimal("RegRateCharge") - rsActivity.Get_Fields_Decimal("TransferCreditUsed") - rsActivity.Get_Fields_Decimal("CommercialCredit")) + rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee");
						}
						else
						{
							TotalReg += rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee");
						}
						TotalSpecialtyPlate += FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeReReg")) + FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeNew"));
						if (FCConvert.ToString(rsActivity.Get_Fields_String("GiftCertOrVoucher")) == "V")
						{
							TotalSpecialtyPlate -= FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("GiftCertificateAmount"));
						}
						TotalStateTransfer += rsActivity.Get_Fields("TransferFee");
						TotalInitialPlate += rsActivity.Get_Fields_Decimal("InitialFee");
						TotalUseTax += rsActivity.Get_Fields("SalesTax");
						TotalTitleFee += rsActivity.Get_Fields("TitleFee");
						TotalMoney += rsActivity.Get_Fields_Double("StatePaid") + rsActivity.Get_Fields_Double("AgentFee");
						TotalLocal += rsActivity.Get_Fields_Double("AgentFee");
						TotalState += rsActivity.Get_Fields_Double("StatePaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("AgentFee"), "#,##0.00");
						fldState.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("StatePaid") + rsActivity.Get_Fields("AgentFee"), "#,##0.00");
					}
					else
					{
						TotalAgent += rsActivity.Get_Fields("AgentFee");
						if (rsActivity.Get_Fields_Decimal("ExciseTaxCharged") >= rsActivity.Get_Fields_Decimal("ExciseCreditUsed"))
						{
							TotalExcise += (rsActivity.Get_Fields_Decimal("ExciseTaxCharged") - rsActivity.Get_Fields_Decimal("ExciseCreditUsed"));
						}
						if (rsActivity.Get_Fields_Decimal("RegRateCharge") >= (rsActivity.Get_Fields_Decimal("TransferCreditUsed") + rsActivity.Get_Fields_Decimal("CommercialCredit")))
						{
							TotalReg += (rsActivity.Get_Fields_Decimal("RegRateCharge") - rsActivity.Get_Fields_Decimal("TransferCreditUsed") - rsActivity.Get_Fields_Decimal("CommercialCredit")) + rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee");
						}
						else
						{
							TotalReg += rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee");
						}
						TotalSpecialtyPlate += FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeReReg")) + FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeNew"));
						if (FCConvert.ToString(rsActivity.Get_Fields_String("GiftCertOrVoucher")) == "V")
						{
							TotalSpecialtyPlate -= FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("GiftCertificateAmount"));
						}
						TotalInitialPlate += rsActivity.Get_Fields_Decimal("InitialFee");
						TotalUseTax += rsActivity.Get_Fields("SalesTax");
						TotalTitleFee += rsActivity.Get_Fields("TitleFee");
						TotalLocalTransfer += rsActivity.Get_Fields_Decimal("ExciseTransferCharge");
						TotalStateTransfer += rsActivity.Get_Fields("TransferFee");
						TotalMoney += rsActivity.Get_Fields_Double("StatePaid") + rsActivity.Get_Fields_Double("LocalPaid");
						TotalLocal += rsActivity.Get_Fields_Double("LocalPaid");
						TotalState += rsActivity.Get_Fields_Double("StatePaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
						fldState.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("StatePaid") + rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
					}
				}
				else if (rsActivity.Get_Fields("TransactionType") == "LPS")
				{
					TotalReg += rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee");
					TotalMoney += rsActivity.Get_Fields_Double("ReplacementFee") + rsActivity.Get_Fields_Double("StickerFee");
					TotalState += rsActivity.Get_Fields_Double("ReplacementFee") + rsActivity.Get_Fields_Double("StickerFee");
					fldLocal.Text = Strings.Format(0, "#,##0.00");
					fldState.Text = Strings.Format(rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee"), "#,##0.00");
					fldTotal.Text = Strings.Format(rsActivity.Get_Fields_Decimal("ReplacementFee") + rsActivity.Get_Fields_Decimal("StickerFee"), "#,##0.00");
				}
				else if (rsActivity.Get_Fields("TransactionType") == "GVW")
				{
					TotalReg += rsActivity.Get_Fields("StatePaid");
					TotalMoney += rsActivity.Get_Fields_Double("StatePaid");
					TotalState += rsActivity.Get_Fields_Double("StatePaid");
					fldLocal.Text = Strings.Format(0, "#,##0.00");
					fldState.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
					fldTotal.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
				}
				else if (rsActivity.Get_Fields("TransactionType") == "DPS")
				{
					TotalReg += rsActivity.Get_Fields_Decimal("StickerFee");
					TotalMoney += rsActivity.Get_Fields_Double("StickerFee");
					TotalState += rsActivity.Get_Fields_Double("StickerFee");
					fldLocal.Text = Strings.Format(0, "#,##0.00");
					fldState.Text = Strings.Format(rsActivity.Get_Fields_Decimal("StickerFee"), "#,##0.00");
					fldTotal.Text = Strings.Format(rsActivity.Get_Fields_Decimal("StickerFee"), "#,##0.00");
				}
				else if (rsActivity.Get_Fields("TransactionType") == "ECO")
				{
					TotalAgent += rsActivity.Get_Fields("AgentFee");
					TotalExcise += rsActivity.Get_Fields_Decimal("ExciseTaxCharged");
					if (rsActivity.Get_Fields_Decimal("TransferCreditUsed") != 0)
					{
						if (rsActivity.Get_Fields_Decimal("TransferCreditUsed") <= rsActivity.Get_Fields_Decimal("RegRateCharge"))
						{
							TotalReg += (rsActivity.Get_Fields_Decimal("RegRateCharge") - rsActivity.Get_Fields_Decimal("TransferCreditUsed"));
						}
					}
					TotalUseTax += rsActivity.Get_Fields("SalesTax");
					TotalTitleFee += rsActivity.Get_Fields("TitleFee");
					TotalSpecialtyPlate += FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeReReg")) + FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("PlateFeeNew"));
					if (FCConvert.ToString(rsActivity.Get_Fields_String("GiftCertOrVoucher")) == "V")
					{
						TotalSpecialtyPlate -= FCConvert.ToDecimal(rsActivity.Get_Fields_Decimal("GiftCertificateAmount"));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsActivity.Get_Fields_Decimal("InitialFee")))
					{
						TotalInitialPlate += rsActivity.Get_Fields_Decimal("InitialFee");
					}
					if (!fecherFoundation.FCUtils.IsNull(rsActivity.Get_Fields("StatePaid")) && !fecherFoundation.FCUtils.IsNull(rsActivity.Get_Fields("StatePaid")))
					{
						TotalMoney += rsActivity.Get_Fields_Double("StatePaid") + rsActivity.Get_Fields_Double("LocalPaid");
						TotalLocal += rsActivity.Get_Fields_Double("LocalPaid");
						TotalState += rsActivity.Get_Fields_Double("StatePaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
						fldState.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("StatePaid") + rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
					}
					else if (fecherFoundation.FCUtils.IsNull(rsActivity.Get_Fields("StatePaid")))
					{
						TotalMoney += rsActivity.Get_Fields_Double("LocalPaid");
						TotalLocal += rsActivity.Get_Fields_Double("LocalPaid");
						fldLocal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
						fldState.Text = Strings.Format(0, "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("LocalPaid"), "#,##0.00");
					}
					else
					{
						TotalMoney += rsActivity.Get_Fields_Double("StatePaid");
						TotalState += rsActivity.Get_Fields_Double("StatePaid");
						fldLocal.Text = Strings.Format(0, "#,##0.00");
						fldState.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
						fldTotal.Text = Strings.Format(rsActivity.Get_Fields("StatePaid"), "#,##0.00");
					}
				}
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalUnits.Text = TotalUnits.ToString();
			fldTotalMoney.Text = Strings.Format(TotalMoney, "#,##0.00");
			fldTotalLocal.Text = Strings.Format(TotalLocal, "#,##0.00");
			fldTotalState.Text = Strings.Format(TotalState, "#,##0.00");
			TotalUnits = 0;
			TotalMoney = 0;
			TotalLocal = 0;
			TotalState = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldAgentFee.Text = Strings.Format(TotalAgent, "#,##0.00");
			fldExcise.Text = Strings.Format(TotalExcise, "#,##0.00");
			fldLocalTransfer.Text = Strings.Format(TotalLocalTransfer, "#,##0.00");
			fldLocalTotal.Text = Strings.Format(TotalLocalTransfer + TotalExcise + TotalAgent, "#,##0.00");
			fldRegFee.Text = Strings.Format(TotalReg, "#,##0.00");
			fldBooster.Text = Strings.Format(TotalBooster, "#,##0.00");
			fldSpecialty.Text = Strings.Format(TotalSpecialtyPlate, "#,##0.00");
			fldInitial.Text = Strings.Format(TotalInitialPlate, "#,##0.00");
			fldDupReg.Text = Strings.Format(TotalDupReg, "#,##0.00");
			fldStateTransfer.Text = Strings.Format(TotalStateTransfer, "#,##0.00");
			fldTransit.Text = Strings.Format(TotalTransit, "#,##0.00");
			fldSpecialReg.Text = Strings.Format(TotalSpecialPermit, "#,##0.00");
			fldUseTax.Text = Strings.Format(TotalUseTax, "#,##0.00");
			fldTitleFee.Text = Strings.Format(TotalTitleFee, "#,##0.00");
			fldStateTotal.Text = Strings.Format(TotalTitleFee + TotalUseTax + TotalSpecialPermit + TotalTransit + TotalDupReg + TotalInitialPlate + TotalSpecialtyPlate + TotalBooster + TotalReg + TotalStateTransfer, "#,##0.00");
			fldTellerReportTotal.Text = Strings.Format(FCConvert.ToDecimal(fldStateTotal.Text) + FCConvert.ToDecimal(fldLocalTotal.Text), "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (MotorVehicle.Statics.rsTellers.EndOfFile())
				MotorVehicle.Statics.rsTellers.MovePrevious();
			rsTemp.OpenRecordset("SELECT * FROM Operators WHERE Code = '" + MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID") + "'", "SystemSettings");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				lblTellerName.Text = rsTemp.Get_Fields_String("Name");
			}
			else
			{
				lblTellerName.Text = MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID");
			}
			rsTemp.DisposeOf();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptTellerReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTellerReport properties;
			//rptTellerReport.Caption	= "Teller Report";
			//rptTellerReport.Icon	= "rptTellerReport.dsx":0000";
			//rptTellerReport.Left	= 0;
			//rptTellerReport.Top	= 0;
			//rptTellerReport.Width	= 11880;
			//rptTellerReport.Height	= 8595;
			//rptTellerReport.StartUpPosition	= 3;
			//rptTellerReport.SectionData	= "rptTellerReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
