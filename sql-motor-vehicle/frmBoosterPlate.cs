//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmBoosterPlate : BaseForm
	{
		public frmBoosterPlate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBoosterPlate InstancePtr
		{
			get
			{
				return (frmBoosterPlate)Sys.GetInstance(typeof(frmBoosterPlate));
			}
		}

		protected frmBoosterPlate _InstancePtr = null;
		//=========================================================
		public Decimal curStatePaid;
		public Decimal curLocalPaid;
		// vbPorter upgrade warning: datExpiresDate As DateTime	OnWrite(string)	OnRead(string)
		DateTime datExpiresDate;
		// vbPorter upgrade warning: datBoostedExpDate As DateTime	OnWrite(string)
		DateTime datBoostedExpDate;
		int lngOriginalWeight;
		int IDCol;
		int PermitCol;
		int BoostedCol;
		int ExpiresCol;

		private void cboLength_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: tempdate As string	OnWrite(DateTime)
			string tempdate;
			if (cboLength.SelectedIndex == -1)
			{
				return;
			}
			tempdate = FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text));
			tempdate = FCConvert.ToString(fecherFoundation.DateAndTime.DateAdd("m", cboLength.SelectedIndex + 1, FCConvert.ToDateTime(tempdate)));
			if (fecherFoundation.DateAndTime.DateValue(tempdate).ToOADate() > datExpiresDate.ToOADate())
			{
				if (fecherFoundation.DateAndTime.DateDiff("m", datExpiresDate, FCConvert.ToDateTime(tempdate)) < 2)
				{
					if (cmbBoostOriginalWeight.Text == "Boost Active Booster")
					{
						MessageBox.Show("The length of time on the booster permit extends past the end of the original booster so the booster permit will end on the booster expiration date.", "Booster Shortened", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("The length of time on the booster permit extends past the end of the registration so the booster permit will end on the registration expiration date.", "Booster Shortened", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					tempdate = FCConvert.ToString(datExpiresDate);
				}
				else
				{
					if (cmbBoostOriginalWeight.Text == "Boost Active Booster")
					{
						MessageBox.Show("You may not issue a booster for this length of time because it is over 1 month past the end of the original booster", "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("You may not issue a booster for this length of time because it is over 1 month past the expiration date of the registration", "Invalid Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtExpireDate.Text = "";
					e.Cancel = true;
					cboLength.SelectedIndex = -1;
					return;
				}
			}
			txtExpireDate.Enabled = true;
			txtExpireDate.Text = Strings.Format(tempdate, "MM/dd/yyyy");
			txtExpireDate.Enabled = false;
		}

		private void cmdCalculate_Click(object sender, System.EventArgs e)
		{
			double originalfee = 0;
			double boostedfee = 0;
			clsDRWrapper rsWC = new clsDRWrapper();
			float multiplier = 0;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			if (txtBoostedWeight.Text == "")
			{
				MessageBox.Show("You must select a boosted weight class before you can calculate the fee.", "Invalid Weight Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (cboLength.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a permit length before you can calculate the fee.", "Invalid Permit Length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (Conversion.Val(txtCurrentWeight.Text) >= Conversion.Val(txtBoostedWeight.Text))
			{
				MessageBox.Show("The boosted weight class must be greater than the original weight class.", "Invalid Booster Weight Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "FM")
			{
				originalfee = FarmWeightRegCost(FCConvert.ToInt32(txtCurrentWeight.Text));
				boostedfee = FarmWeightRegCost(FCConvert.ToInt32(txtBoostedWeight.Text));
			}
			else
			{
				originalfee = CommRegWeightCost(FCConvert.ToInt32(txtCurrentWeight.Text));
				boostedfee = CommRegWeightCost(FCConvert.ToInt32(txtBoostedWeight.Text));
			}
			switch (cboLength.SelectedIndex)
			{
				case 0:
					{
						multiplier = 0.2f;
						break;
					}
				case 1:
					{
						multiplier = 0.3f;
						break;
					}
				case 2:
					{
						multiplier = 0.4f;
						break;
					}
				case 3:
					{
						multiplier = 0.5f;
						break;
					}
				case 4:
					{
						multiplier = 0.6f;
						break;
					}
				case 5:
					{
						multiplier = 0.7f;
						break;
					}
				case 6:
					{
						multiplier = 0.75f;
						break;
					}
				case 7:
					{
						multiplier = 0.8f;
						break;
					}
			}
			//end switch
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			lblFeeAmount.Text = Strings.Format(((boostedfee - originalfee) * multiplier) + Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster")), "$#,##0.00");
		}

		public void cmdCalculate_Click()
		{
			cmdCalculate_Click(cmdCalculate, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsBooster = new clsDRWrapper();
			if (lblFeeAmount.Text == "$0.00")
			{
				MessageBox.Show("You must calculate the Fee before you can Save.", "Invalid Fee", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtPermit.Text == "")
			{
				MessageBox.Show("You must enter a Permit Number before you can Save.", "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmdCalculate.Enabled)
			{
				cmdCalculate_Click();
			}
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				curLocalPaid = FCConvert.ToDecimal(Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster")));
				//FC:FINAL:BB:#i2213 - use Conversion.CCur as in original
				//curStatePaid = FCConvert.ToDecimal(Conversion.Val(lblFeeAmount.Text) - Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster")));
				curStatePaid = Conversion.CCur(lblFeeAmount.Text) - FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"));
				MotorVehicle.Write_PDS_Work_Record_Stuff();
			}
			rs.OpenRecordset("SELECT * FROM Booster");
			rs.AddNew();
			rs.Set_Fields("Permit", txtPermit.Text);
			rs.Set_Fields("EffectiveDate", txtEffectiveDate.Text);
			rs.Set_Fields("ExpireDate", txtExpireDate.Text);
			rs.Set_Fields("BoostedFrom", FCConvert.ToInt32(txtCurrentWeight.Text));
			rs.Set_Fields("BoostedWeight", FCConvert.ToInt32(txtBoostedWeight.Text));
			//FC:FINAL:BB:#i2213 - use Conversion.CCur as in original
			//rs.Set_Fields("StatePaid", FCConvert.ToDecimal(Conversion.Val(lblFeeAmount.Text) - (Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster")))));
			rs.Set_Fields("StatePaid", Conversion.CCur(lblFeeAmount.Text) - FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster")));
			rs.Set_Fields("LocalPaid", rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"));
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("plate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
			rs.Set_Fields("Class", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Set_Fields("DateUpdated", DateTime.Now);
			rs.Set_Fields("VehicleID", MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
			rs.Update();
			rs.OpenRecordset("SELECT * FROM ExceptionReport");
			rs.AddNew();
			rs.Set_Fields("Type", "6PM");
			rs.Set_Fields("ExceptionReportDate", DateTime.Today);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("NewClass", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
			rs.Set_Fields("NewPlate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
			//FC:FINAL:BB:#i2213 - use Conversion.CCur as in original
			//rs.Set_Fields("Fee", FCConvert.ToDecimal(Conversion.Val(lblFeeAmount.Text) - (Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster")))));
			rs.Set_Fields("Fee", Conversion.CCur(lblFeeAmount.Text) - FCConvert.ToDecimal(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster")));
			rs.Set_Fields("PermitNumber", fecherFoundation.Strings.Trim(txtPermit.Text));
			rs.Set_Fields("PermitType", "BOOSTER");
			rs.Set_Fields("ApplicantName", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1"))));
			rs.Set_Fields("MVR3Printed", 0);
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			rs.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				rs.Edit();
				rs.Set_Fields("Status", "I");
				rs.Set_Fields("PeriodCloseoutID", 0);
				rs.Set_Fields("TellerCloseoutID", 0);
				rs.Update();
			}
			rs.OpenRecordset("SELECT * FROM InventoryAdjustments");
			rs.AddNew();
			rs.Set_Fields("AdjustmentCode", "I");
			rs.Set_Fields("DateOfAdjustment", DateTime.Today);
			rs.Set_Fields("QuantityAdjusted", 1);
			rs.Set_Fields("InventoryType", "BXSXX");
			rs.Set_Fields("Low", txtPermit.Text);
			rs.Set_Fields("High", txtPermit.Text);
			rs.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			rs.Set_Fields("reason", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate") + " " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("MVR3"));
			rs.Set_Fields("PeriodCloseoutID", 0);
			rs.Set_Fields("TellerCloseoutID", 0);
			rs.Update();
			MessageBox.Show("Be sure to complete the Booster Form.", "Fill out Booster Form", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				//MDIParent.InstancePtr.GetMainMenu();
				MDIParent.InstancePtr.Menu18();
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmBoosterPlate_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int UpperWeight;
			int LowerWeight;
			clsDRWrapper rsBoosterInfo = new clsDRWrapper();
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			int lngWeightCheck;
			bool blnBoosterFound;
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			IDCol = 0;
			PermitCol = 1;
			BoostedCol = 2;
			ExpiresCol = 3;
			vsBooster.TextMatrix(0, PermitCol, "Permit #");
			vsBooster.TextMatrix(0, BoostedCol, "Boosted To");
			vsBooster.TextMatrix(0, ExpiresCol, "Expires");
			vsBooster.ColHidden(IDCol, true);
			vsBooster.ColAlignment(PermitCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBooster.ColAlignment(BoostedCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBooster.ColAlignment(ExpiresCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBooster.ColWidth(PermitCol, FCConvert.ToInt32(vsBooster.WidthOriginal * 0.3));
			vsBooster.ColWidth(BoostedCol, FCConvert.ToInt32(vsBooster.WidthOriginal * 0.3));
			vsBooster.ColWidth(ExpiresCol, FCConvert.ToInt32(vsBooster.WidthOriginal * 0.3));
			vsBooster.ExtendLastCol = true;
			txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtExpireDate.Enabled = false;
			lngWeightCheck = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("RegisteredWeightNew"));
			lngOriginalWeight = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("RegisteredWeightNew"));
			datExpiresDate = FCConvert.ToDateTime(MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate"));
			rsBoosterInfo.OpenRecordset("SELECT * FROM Booster WHERE VehicleID = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID") + " And Inactive = 0 and isnull(Voided, 0) = 0 and ExpireDate >= '" + DateTime.Today.ToShortDateString() + "' ORDER By ExpireDate, BoostedFrom", "TWMV0000.vb1");
			blnBoosterFound = false;
			if (rsBoosterInfo.EndOfFile() != true && rsBoosterInfo.BeginningOfFile() != true)
			{
				do
				{
					if (datExpiresDate.ToOADate() == rsBoosterInfo.Get_Fields_DateTime("ExpireDate").ToOADate())
					{
						// do nothing
					}
					else
					{
						datExpiresDate = FCConvert.ToDateTime(rsBoosterInfo.Get_Fields_DateTime("ExpireDate"));
						vsBooster.Rows += 1;
					}
					vsBooster.TextMatrix(vsBooster.Rows - 1, IDCol, FCConvert.ToString(rsBoosterInfo.Get_Fields_Int32("ID")));
					vsBooster.TextMatrix(vsBooster.Rows - 1, PermitCol, FCConvert.ToString(rsBoosterInfo.Get_Fields_Int32("Permit")));
					vsBooster.TextMatrix(vsBooster.Rows - 1, BoostedCol, Strings.Format(rsBoosterInfo.Get_Fields_Int32("BoostedWeight"), "#,##0"));
					vsBooster.TextMatrix(vsBooster.Rows - 1, ExpiresCol, Strings.Format(rsBoosterInfo.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy"));
					// If lngWeightCheck = rsBoosterInfo.Fields("BoostedFrom") Then
					// lngWeightCheck = rsBoosterInfo.Fields("BoostedWeight")
					// datExpiresDate = rsBoosterInfo.Fields("ExpireDate")
					// blnBoosterFound = True
					// Else
					// Exit Do
					// End If
					rsBoosterInfo.MoveNext();
				}
				while (rsBoosterInfo.EndOfFile() != true);
			}
			if (vsBooster.Rows == 1)
			{
				if (cmbBoostOriginalWeight.Items.Contains("Boost Active Booster"))
				{
					cmbBoostOriginalWeight.Items.Remove("Boost Active Booster");
				}
				cmbBoostOriginalWeight.Text = "Boost Registered Vehicle Weight";
				// If MsgBox("This vehicle already has an active booster permit.  Do you want this booster to further boost the registered weight of the vehicle?", vbQuestion + vbYesNo, "Boost Booster?") = vbYes Then
				// counter = DateDiff("m", Date, datExpiresDate)
				// If counter < 1 Then
				// counter = 1
				// End If
				// cboLength.ListIndex = counter - 1
				// cboLength.Enabled = False
				// txtExpireDate.Text = Format(datExpiresDate, "MM/dd/yyyy")
				// datBoostedExpDate = "1/1/1900"
				// Else
				// txtEffectiveDate.Text = Format(DateAdd("d", 1, datExpiresDate), "MM/dd/yyyy")
				// datBoostedExpDate = datExpiresDate
				// datExpiresDate = rsPlateForReg.Fields("ExpireDate")
				// End If
				// Else
				// datBoostedExpDate = "1/1/1900"
			}
			else
			{
				cmbBoostOriginalWeight.Text = "Boost Active Booster";
			}
			txtCurrentWeight.Text = FCConvert.ToString(lngWeightCheck);
			txtCurrentWeight.Enabled = false;
			txtPermit.Focus();
			this.Refresh();
		}

		private void frmBoosterPlate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmBoosterPlate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBoosterPlate properties;
			//frmBoosterPlate.FillStyle	= 0;
			//frmBoosterPlate.ScaleWidth	= 9045;
			//frmBoosterPlate.ScaleHeight	= 7365;
			//frmBoosterPlate.LinkTopic	= "Form2";
			//frmBoosterPlate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmBoosterPlate_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
			vsBooster.ColWidth(PermitCol, FCConvert.ToInt32(vsBooster.WidthOriginal * 0.3));
			vsBooster.ColWidth(BoostedCol, FCConvert.ToInt32(vsBooster.WidthOriginal * 0.3));
			vsBooster.ColWidth(ExpiresCol, FCConvert.ToInt32(vsBooster.WidthOriginal * 0.3));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
			//frmPlateInfo.InstancePtr.Show(App.MainForm);
			frmPlateInfo.InstancePtr.ShowForm();
		}

		private void frmBoosterPlate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void optBoostBooster_CheckedChanged(object sender, System.EventArgs e)
		{
			vsBooster.Enabled = true;
			if (vsBooster.Row < 1)
			{
				vsBooster.Row = 1;
			}
			vsBooster_RowColChange(null, null);
		}

		private void optBoostOriginalWeight_CheckedChanged(object sender, System.EventArgs e)
		{
			vsBooster.Enabled = false;
			cboLength.Enabled = true;
			if (vsBooster.Rows > 1)
			{
				datBoostedExpDate = FCConvert.ToDateTime(vsBooster.TextMatrix(vsBooster.Rows - 1, ExpiresCol));
				txtEffectiveDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", 1, datBoostedExpDate), "MM/dd/yyyy");
			}
			else
			{
				datBoostedExpDate = FCConvert.ToDateTime("1/1/1900");
				txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			datExpiresDate = FCConvert.ToDateTime(MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate"));
			cboLength.SelectedIndex = -1;
			txtExpireDate.Text = "";
			txtCurrentWeight.Text = FCConvert.ToString(lngOriginalWeight);
		}

		private void txtBoostedWeight_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtBoostedWeight.Text != "")
			{
				if (!Information.IsNumeric(txtBoostedWeight.Text) || FCConvert.ToDouble(txtBoostedWeight.Text) < 0 || FCConvert.ToDouble(txtBoostedWeight.Text) > 100000)
				{
					MessageBox.Show("You must enter a number greater than 0 but no greater than 100000 in this field.", "Invalid Weight", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtBoostedWeight.Text = "";
					e.Cancel = true;
					txtBoostedWeight.Focus();
				}
			}
		}

		private void txtEffectiveDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWrite(long, int)
			int counter = 0;
			if (Information.IsDate(txtEffectiveDate.Text))
			{
				if (fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text).ToOADate() < datBoostedExpDate.ToOADate() || fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text).ToOADate() > datExpiresDate.ToOADate())
				{
					//if (datBoostedExpDate.ToOADate() == FCConvert.ToDateTime("1/1/1900").ToOADate() || datBoostedExpDate.ToOADate() == FCConvert.ToDateTime("12:00:00 AM").ToOADate())
					if (datBoostedExpDate.ToOADate() == FCConvert.ToDateTime("1/1/1900").ToOADate() || Strings.Format(datBoostedExpDate.ToOADate(), "MM/dd/yyyy") == Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
					{
						MessageBox.Show("Your effective date must be before " + FCConvert.ToString(datExpiresDate), "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("Your effective date must be between " + FCConvert.ToString(datBoostedExpDate) + " and " + FCConvert.ToString(datExpiresDate), "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					e.Cancel = true;
				}
				else
				{
					if (cboLength.Enabled == false)
					{
						counter = fecherFoundation.DateAndTime.DateDiff("m", fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text), datExpiresDate);
						if (counter < 1)
						{
							counter = 1;
						}
						cboLength.SelectedIndex = counter - 1;
					}
				}
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void txtPermit_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper tempRS = new clsDRWrapper();
			if (txtPermit.Text != "")
			{
				if (MotorVehicle.Statics.PlateGroup == 0)
				{
					tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)) + " AND Status = 'A'");
				}
				else
				{
					tempRS.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'BXSXX' AND Number = " + FCConvert.ToString(Conversion.Val(txtPermit.Text)) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.PlateGroup) + " OR [Group] = 0)");
				}
				if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
				{
					return;
				}
				else
				{
					MessageBox.Show("This permit number was not found in Inventory.", "Invalid Permit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtPermit.Text = "";
				}
			}
		}
		// vbPorter upgrade warning: weight As int	OnWrite(VB.TextBox)
		private double FarmWeightRegCost(int weight)
		{
			double FarmWeightRegCost = 0;
			clsDRWrapper rsWC = new clsDRWrapper();
			int Low;
			int High;
			rsWC.OpenRecordset("SELECT * FROM GrossVehicleWeight WHERE Type = 'FM' AND Low <= " + FCConvert.ToString(weight) + " AND High >=  " + FCConvert.ToString(weight));
			if (rsWC.EndOfFile() != true && rsWC.BeginningOfFile() != true)
			{
				FarmWeightRegCost = rsWC.Get_Fields("Fee");
			}
			return FarmWeightRegCost;
		}
		// vbPorter upgrade warning: weight As int	OnWrite(VB.TextBox)
		private double CommRegWeightCost(int weight)
		{
			double CommRegWeightCost = 0;
			clsDRWrapper rsWC = new clsDRWrapper();
			int Low;
			int High;
			rsWC.OpenRecordset("SELECT * FROM GrossVehicleWeight WHERE Type = 'TK' AND Low <= " + FCConvert.ToString(weight) + " AND High >= " + FCConvert.ToString(weight));
			if (rsWC.EndOfFile() != true && rsWC.BeginningOfFile() != true)
			{
				CommRegWeightCost = rsWC.Get_Fields("Fee");
			}
			return CommRegWeightCost;
		}

		private void SetCustomFormColors()
		{
			lblPermit.ForeColor = Color.Blue;
		}

		private void vsBooster_RowColChange(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWrite(long, int)
			int counter = 0;
			// vbPorter upgrade warning: lngWeight As int	OnWrite(string)
			int lngWeight = 0;
			if (vsBooster.Row > 0)
			{
				datExpiresDate = FCConvert.ToDateTime(vsBooster.TextMatrix(vsBooster.Row, ExpiresCol));
				txtExpireDate.Text = Strings.Format(datExpiresDate, "MM/dd/yyyy");
				if (vsBooster.Row > 1)
				{
					datBoostedExpDate = FCConvert.ToDateTime(vsBooster.TextMatrix(vsBooster.Row - 1, ExpiresCol));
					txtEffectiveDate.Text = Strings.Format(fecherFoundation.DateAndTime.DateAdd("d", 1, datBoostedExpDate), "MM/dd/yyyy");
				}
				else
				{
					datBoostedExpDate = FCConvert.ToDateTime("1/1/1900");
					txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				}
				counter = fecherFoundation.DateAndTime.DateDiff("m", fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text), datExpiresDate);
				if (counter < 1)
				{
					counter = 1;
				}
				cboLength.SelectedIndex = counter - 1;
				// cboLength.Enabled = False
				//FC:FINAL:BB:#i2213 - fixed conversion error
				//lngWeight = FCConvert.ToInt32(vsBooster.TextMatrix(vsBooster.Row, BoostedCol));
				lngWeight = FCConvert.ToInt32(Conversion.Val(vsBooster.TextMatrix(vsBooster.Row, BoostedCol)));
				txtCurrentWeight.Text = FCConvert.ToString(lngWeight);
			}
		}

		public void cmbBoostOriginalWeight_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbBoostOriginalWeight.Text == "Boost Registered Vehicle Weight")
			{
				optBoostOriginalWeight_CheckedChanged(sender, e);
			}
			if (cmbBoostOriginalWeight.Text == "Boost Active Booster")
			{
				optBoostBooster_CheckedChanged(sender, e);
			}
		}
	}
}
