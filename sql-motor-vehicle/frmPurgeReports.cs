//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmPurgeReports : BaseForm
	{
		public frmPurgeReports()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPurgeReports InstancePtr
		{
			get
			{
				return (frmPurgeReports)Sys.GetInstance(typeof(frmPurgeReports));
			}
		}

		protected frmPurgeReports _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/11/2001
		// This form will be used to purge any outdated information
		// the town wants to get rid of to decrease the size of their
		// database
		// ********************************************************
		clsDRWrapper rsPurge = new clsDRWrapper();

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			int totalcount;
			int lngCloseoutID = 0;
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans;
			// vbPorter upgrade warning: tempdate As DateTime	OnWrite(string)
			DateTime tempdate;
			clsDRWrapper rsPeriodInfo = new clsDRWrapper();
			clsDRWrapper rsPurgeInfo = new clsDRWrapper();
			if (!Information.IsDate(cboPeriod.Text))
			{
				MessageBox.Show("This is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (DateAndTime.DateValue(cboPeriod.Text) > DateAndTime.DateValue(Strings.Format(DateAndTime.DateAdd("yyyy", -5, DateTime.Today), "MM/dd/yyyy")))
			{
				MessageBox.Show("You must select a date that is at least 5 years in the past.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			tempdate = FCConvert.ToDateTime(Strings.Format(fecherFoundation.DateAndTime.DateValue(cboPeriod.Text), "MM/dd/yyyy"));
			rsPeriodInfo.OpenRecordset("SELECT * FROM PeriodCloseout WHERE IssueDate = '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(cboPeriod.Text)) + "'");
			if (rsPeriodInfo.EndOfFile() != true && rsPeriodInfo.BeginningOfFile() != true)
			{
				lngCloseoutID = FCConvert.ToInt32(rsPeriodInfo.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Could not find period closeout information for the date you selected", "Invalid Closeout Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			ans = MessageBox.Show("All reporting information saved on or before this closeout date will be deleted.  Do you wish to continue?", "Delete Records?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Deleting Records";
				frmWait.InstancePtr.Refresh();
				rsPurgeInfo.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 <> 0 AND OldMVR3 <= " + FCConvert.ToString(lngCloseoutID));
				if (rsPurgeInfo.EndOfFile() != true && rsPurgeInfo.BeginningOfFile() != true)
				{
					do
					{
						rsPurgeInfo.Delete();
						rsPurgeInfo.Update();
						rsPurgeInfo.MoveNext();
					}
					while (rsPurgeInfo.EndOfFile() != true);
				}
				rsPurgeInfo.Execute("DELETE FROM CloseoutInventory WHERE PeriodCloseoutID <= " + FCConvert.ToString(lngCloseoutID), "TWMV0000.vb1");
				rsPurgeInfo.Execute("DELETE FROM Inventory WHERE Status <> 'A' AND Date <= '" + FCConvert.ToString(tempdate) + "'", "TWMV0000.vb1");
				rsPurgeInfo.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID <> 0 AND PeriodCloseoutID <= " + FCConvert.ToString(lngCloseoutID));
				if (rsPurgeInfo.EndOfFile() != true && rsPurgeInfo.BeginningOfFile() != true)
				{
					do
					{
						rsPurgeInfo.Delete();
						rsPurgeInfo.Update();
						rsPurgeInfo.MoveNext();
					}
					while (rsPurgeInfo.EndOfFile() != true);
				}
				rsPurgeInfo.Execute("DELETE FROM InventoryOnHandAtPeriodCloseout WHERE PeriodCloseoutID <= " + FCConvert.ToString(lngCloseoutID), "TWMV0000.vb1");
				rsPurgeInfo.Execute("DELETE FROM PeriodCloseout WHERE Key <= " + FCConvert.ToString(lngCloseoutID), "TWMV0000.vb1");
				rsPurgeInfo.Execute("DELETE FROM TitleApplications WHERE PeriodCloseoutID <> 0 AND PeriodCloseoutID <= " + FCConvert.ToString(lngCloseoutID), "TWMV0000.vb1");
				rsPurgeInfo.Execute("DELETE FROM TransitPlates WHERE PeriodCloseoutID <> 0 AND PeriodCloseoutID <= " + FCConvert.ToString(lngCloseoutID), "TWMV0000.vb1");
				rsPurgeInfo.Execute("DELETE FROM SpecialRegistration WHERE PeriodCloseoutID <> 0 AND PeriodCloseoutID <= " + FCConvert.ToString(lngCloseoutID), "TWMV0000.vb1");
				frmWait.InstancePtr.Unload();
				//App.DoEvents();
				MessageBox.Show("Purge Complete!", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				return;
			}
			Close();
		}

		private void frmPurgeReports_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPurgeReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeReports properties;
			//frmPurgeReports.FillStyle	= 0;
			//frmPurgeReports.ScaleWidth	= 5880;
			//frmPurgeReports.ScaleHeight	= 3945;
			//frmPurgeReports.LinkTopic	= "Form2";
			//frmPurgeReports.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsPeriodInfo = new clsDRWrapper();
			cboPeriod.Clear();
			rsPeriodInfo.OpenRecordset("SELECT * FROM PeriodCloseout ORDER BY ID");
			if (rsPeriodInfo.EndOfFile() != true && rsPeriodInfo.BeginningOfFile() != true)
			{
				do
				{
					cboPeriod.AddItem(FCConvert.ToString(rsPeriodInfo.Get_Fields_DateTime("IssueDate")));
					rsPeriodInfo.MoveNext();
				}
				while (rsPeriodInfo.EndOfFile() != true);
			}
			if (cboPeriod.Items.Count > 0)
			{
				cboPeriod.SelectedIndex = 0;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPurgeReports_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmPurgeReports_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}
	}
}
