//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmDataInputLongTermTrailers.
	/// </summary>
	partial class frmDataInputLongTermTrailers
	{
		public fecherFoundation.FCComboBox cmbYes;
		public fecherFoundation.FCLabel lblYes;
		public fecherFoundation.FCComboBox cmbFleet;
		public fecherFoundation.FCComboBox cmbNoMaineReReg;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCFrame fraAdditionalInfo;
		public fecherFoundation.FCCheckBox chkOverrideFleetEmail;
		public fecherFoundation.FCFrame fraUserFields;
		public Global.T2KOverTypeBox txtUserReason1;
		public Global.T2KOverTypeBox txtUserField1;
		public Global.T2KOverTypeBox txtUserField2;
		public Global.T2KOverTypeBox txtUserField3;
		public Global.T2KOverTypeBox txtUserReason2;
		public Global.T2KOverTypeBox txtUserReason3;
		public fecherFoundation.FCLabel lblInfo3;
		public fecherFoundation.FCLabel lblInfo2;
		public fecherFoundation.FCLabel lblInfo1;
		public fecherFoundation.FCLabel lblUserField3;
		public fecherFoundation.FCLabel lblUserField2;
		public fecherFoundation.FCLabel lblUserField1;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCButton cmdErase;
		public fecherFoundation.FCComboBox cboMessageCodes;
		public Global.T2KOverTypeBox txtMessage;
		public fecherFoundation.FCLabel lblMesageCode;
		public fecherFoundation.FCLabel lblMessage;
		public Global.T2KOverTypeBox txtEMail;
		public fecherFoundation.FCLabel lblEMail;
		public fecherFoundation.FCFrame fraFeesInfo;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCButton cmdReturn;
		public Global.T2KBackFillDecimal txtFEEAgentFeeReg;
		public Global.T2KBackFillDecimal txtFEESalesTax;
		public Global.T2KBackFillDecimal txtFEETitleFee;
		public Global.T2KBackFillDecimal txtFEETotal;
		public Global.T2KBackFillDecimal txtFEECreditTransfer;
		public Global.T2KBackFillDecimal txtFEERegFee;
		public Global.T2KBackFillDecimal txtFeeAgentFeeCTA;
		public Global.T2KBackFillDecimal txtFEETransferFee;
		public Global.T2KBackFillDecimal txtFeeAgentFeeTransfer;
		public Global.T2KBackFillDecimal txtFeeAgentFeeCorrection;
		public Global.T2KBackFillDecimal txtFEERushTitleFee;
		public fecherFoundation.FCLabel Label2_7;
		public fecherFoundation.FCLabel Label35;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel Label34;
		public fecherFoundation.FCLabel Label33;
		public fecherFoundation.FCLabel Label2_10;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label2_8;
		public fecherFoundation.FCLabel Label2_6;
		public fecherFoundation.FCLabel Label2_5;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label2_3;
		public fecherFoundation.FCCheckBox chkTimePayment;
		public fecherFoundation.FCCheckBox chk25YearPlate;
		public fecherFoundation.FCFrame fraDataInput;
		public fecherFoundation.FCTextBox txtReg2PartyID;
		public fecherFoundation.FCButton cmdReg2Search;
		public fecherFoundation.FCButton cmdReg2Edit;
		public fecherFoundation.FCTextBox txtReg1PartyID;
		public fecherFoundation.FCButton cmdReg1Search;
		public fecherFoundation.FCButton cmdReg1Edit;
		public fecherFoundation.FCFrame fraFleet;
		public fecherFoundation.FCTextBox txtFleetNumber;
		public fecherFoundation.FCTextBox txtFleetName;
		public fecherFoundation.FCPictureBox imgFleetComment;
		public fecherFoundation.FCLabel lblFleetNumber;
		public fecherFoundation.FCLabel lblFleetName;
		public fecherFoundation.FCTextBox txtNetWeight;
		public fecherFoundation.FCButton cmdMoreInfo;
		public Global.T2KOverTypeBox txtMake;
		public Global.T2KOverTypeBox txtYear;
		public Global.T2KOverTypeBox txtUnit;
		public Global.T2KOverTypeBox txtColor1;
		public Global.T2KOverTypeBox txtColor2;
		public Global.T2KOverTypeBox txtPlate;
		public Global.T2KOverTypeBox txtStyle;
		public Global.T2KOverTypeBox txtVIN;
		public Global.T2KOverTypeBox txtTitle;
		public Global.T2KOverTypeBox txtAddress1;
		public Global.T2KOverTypeBox txtCity;
		public Global.T2KOverTypeBox txtZip;
		public Global.T2KOverTypeBox txtState;
		public Global.T2KOverTypeBox txtLegalResCity;
		public Global.T2KOverTypeBox txtLegalResState;
		public Global.T2KBackFillDecimal txtRate;
		public Global.T2KBackFillDecimal txtCredit;
		public Global.T2KBackFillDecimal txtFees;
		public Global.T2KDateBox txtExpires;
		public Global.T2KOverTypeBox txtLegalZip;
		public Global.T2KOverTypeBox txtLegalResAddress;
		public Global.T2KOverTypeBox txtModel;
		public fecherFoundation.FCLabel Label36;
		public fecherFoundation.FCLabel lblRegistrant2;
		public fecherFoundation.FCLabel lblRegistrant1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLine Line18;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCLabel Label32;
		public fecherFoundation.FCComboBox cboLength;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblTotalDue;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuCTA;
		public fecherFoundation.FCToolStripMenuItem mnuUseTax;
		public fecherFoundation.FCToolStripMenuItem mnuFileCalculate;
		public fecherFoundation.FCToolStripMenuItem mnuFleetGroupComment;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataInputLongTermTrailers));
            this.cmbYes = new fecherFoundation.FCComboBox();
            this.lblYes = new fecherFoundation.FCLabel();
            this.cmbFleet = new fecherFoundation.FCComboBox();
            this.cmbNoMaineReReg = new fecherFoundation.FCComboBox();
            this.fraAdditionalInfo = new fecherFoundation.FCFrame();
            this.chkOverrideFleetEmail = new fecherFoundation.FCCheckBox();
            this.fraUserFields = new fecherFoundation.FCFrame();
            this.txtUserReason1 = new Global.T2KOverTypeBox();
            this.txtUserField1 = new Global.T2KOverTypeBox();
            this.txtUserField2 = new Global.T2KOverTypeBox();
            this.txtUserField3 = new Global.T2KOverTypeBox();
            this.txtUserReason2 = new Global.T2KOverTypeBox();
            this.txtUserReason3 = new Global.T2KOverTypeBox();
            this.lblInfo3 = new fecherFoundation.FCLabel();
            this.lblInfo2 = new fecherFoundation.FCLabel();
            this.lblInfo1 = new fecherFoundation.FCLabel();
            this.lblUserField3 = new fecherFoundation.FCLabel();
            this.lblUserField2 = new fecherFoundation.FCLabel();
            this.lblUserField1 = new fecherFoundation.FCLabel();
            this.cmdDone = new fecherFoundation.FCButton();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.cmdErase = new fecherFoundation.FCButton();
            this.cboMessageCodes = new fecherFoundation.FCComboBox();
            this.txtMessage = new Global.T2KOverTypeBox();
            this.lblMesageCode = new fecherFoundation.FCLabel();
            this.lblMessage = new fecherFoundation.FCLabel();
            this.txtEMail = new Global.T2KOverTypeBox();
            this.lblEMail = new fecherFoundation.FCLabel();
            this.fraFeesInfo = new fecherFoundation.FCFrame();
            this.cmdExit = new fecherFoundation.FCButton();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.txtFEEAgentFeeReg = new Global.T2KBackFillDecimal();
            this.txtFEESalesTax = new Global.T2KBackFillDecimal();
            this.txtFEETitleFee = new Global.T2KBackFillDecimal();
            this.txtFEETotal = new Global.T2KBackFillDecimal();
            this.txtFEECreditTransfer = new Global.T2KBackFillDecimal();
            this.txtFEERegFee = new Global.T2KBackFillDecimal();
            this.txtFeeAgentFeeCTA = new Global.T2KBackFillDecimal();
            this.txtFEETransferFee = new Global.T2KBackFillDecimal();
            this.txtFeeAgentFeeTransfer = new Global.T2KBackFillDecimal();
            this.txtFeeAgentFeeCorrection = new Global.T2KBackFillDecimal();
            this.txtFEERushTitleFee = new Global.T2KBackFillDecimal();
            this.Label2_7 = new fecherFoundation.FCLabel();
            this.Label35 = new fecherFoundation.FCLabel();
            this.Label2_4 = new fecherFoundation.FCLabel();
            this.Label34 = new fecherFoundation.FCLabel();
            this.Label33 = new fecherFoundation.FCLabel();
            this.Label2_10 = new fecherFoundation.FCLabel();
            this.Line1 = new fecherFoundation.FCLine();
            this.Label2_8 = new fecherFoundation.FCLabel();
            this.Label2_6 = new fecherFoundation.FCLabel();
            this.Label2_5 = new fecherFoundation.FCLabel();
            this.Label2_1 = new fecherFoundation.FCLabel();
            this.Label2_2 = new fecherFoundation.FCLabel();
            this.Label2_3 = new fecherFoundation.FCLabel();
            this.chkTimePayment = new fecherFoundation.FCCheckBox();
            this.chk25YearPlate = new fecherFoundation.FCCheckBox();
            this.fraDataInput = new fecherFoundation.FCFrame();
            this.txtReg2PartyID = new fecherFoundation.FCTextBox();
            this.cmdReg2Search = new fecherFoundation.FCButton();
            this.cmdReg2Edit = new fecherFoundation.FCButton();
            this.txtReg1PartyID = new fecherFoundation.FCTextBox();
            this.cmdReg1Search = new fecherFoundation.FCButton();
            this.cmdReg1Edit = new fecherFoundation.FCButton();
            this.fraFleet = new fecherFoundation.FCFrame();
            this.txtFleetNumber = new fecherFoundation.FCTextBox();
            this.txtFleetName = new fecherFoundation.FCTextBox();
            this.imgFleetComment = new fecherFoundation.FCPictureBox();
            this.lblFleetNumber = new fecherFoundation.FCLabel();
            this.lblFleetName = new fecherFoundation.FCLabel();
            this.txtNetWeight = new fecherFoundation.FCTextBox();
            this.cmdMoreInfo = new fecherFoundation.FCButton();
            this.txtMake = new Global.T2KOverTypeBox();
            this.txtYear = new Global.T2KOverTypeBox();
            this.txtUnit = new Global.T2KOverTypeBox();
            this.txtColor1 = new Global.T2KOverTypeBox();
            this.txtColor2 = new Global.T2KOverTypeBox();
            this.txtPlate = new Global.T2KOverTypeBox();
            this.txtStyle = new Global.T2KOverTypeBox();
            this.txtVIN = new Global.T2KOverTypeBox();
            this.txtTitle = new Global.T2KOverTypeBox();
            this.txtAddress1 = new Global.T2KOverTypeBox();
            this.txtCity = new Global.T2KOverTypeBox();
            this.txtZip = new Global.T2KOverTypeBox();
            this.txtState = new Global.T2KOverTypeBox();
            this.txtLegalResCity = new Global.T2KOverTypeBox();
            this.txtLegalResState = new Global.T2KOverTypeBox();
            this.txtRate = new Global.T2KBackFillDecimal();
            this.txtCredit = new Global.T2KBackFillDecimal();
            this.txtFees = new Global.T2KBackFillDecimal();
            this.txtExpires = new Global.T2KDateBox();
            this.txtLegalZip = new Global.T2KOverTypeBox();
            this.txtLegalResAddress = new Global.T2KOverTypeBox();
            this.txtModel = new Global.T2KOverTypeBox();
            this.Label36 = new fecherFoundation.FCLabel();
            this.lblRegistrant2 = new fecherFoundation.FCLabel();
            this.lblRegistrant1 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2_0 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Line18 = new fecherFoundation.FCLine();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label27 = new fecherFoundation.FCLabel();
            this.Label29 = new fecherFoundation.FCLabel();
            this.Label31 = new fecherFoundation.FCLabel();
            this.Label32 = new fecherFoundation.FCLabel();
            this.cboLength = new fecherFoundation.FCComboBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblTotalDue = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCTA = new fecherFoundation.FCToolStripMenuItem();
            this.mnuUseTax = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileCalculate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFleetGroupComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdCTA = new fecherFoundation.FCButton();
            this.cmdUseTax = new fecherFoundation.FCButton();
            this.cmdCalculate = new fecherFoundation.FCButton();
            this.cmdGroupComment = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).BeginInit();
            this.fraAdditionalInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).BeginInit();
            this.fraUserFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdErase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFeesInfo)).BeginInit();
            this.fraFeesInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEEAgentFeeReg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEESalesTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEETitleFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEETotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEECreditTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEERegFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCTA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEETransferFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCorrection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEERushTitleFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTimePayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk25YearPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDataInput)).BeginInit();
            this.fraDataInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg2Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg2Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg1Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg1Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFleet)).BeginInit();
            this.fraFleet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCTA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUseTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGroupComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 600);
            this.BottomPanel.Size = new System.Drawing.Size(852, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraFeesInfo);
            this.ClientArea.Controls.Add(this.fraAdditionalInfo);
            this.ClientArea.Controls.Add(this.fraDataInput);
            this.ClientArea.Controls.Add(this.chkTimePayment);
            this.ClientArea.Controls.Add(this.chk25YearPlate);
            this.ClientArea.Controls.Add(this.cboLength);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblTotalDue);
            this.ClientArea.Size = new System.Drawing.Size(852, 540);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdGroupComment);
            this.TopPanel.Controls.Add(this.cmdCalculate);
            this.TopPanel.Controls.Add(this.cmdUseTax);
            this.TopPanel.Controls.Add(this.cmdCTA);
            this.TopPanel.Size = new System.Drawing.Size(852, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCTA, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdUseTax, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCalculate, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdGroupComment, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(125, 30);
            this.HeaderText.Text = "Data Input";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbYes
            // 
            this.cmbYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbYes.Location = new System.Drawing.Point(150, 30);
            this.cmbYes.Name = "cmbYes";
            this.cmbYes.Size = new System.Drawing.Size(221, 40);
            this.cmbYes.TabIndex = 132;
            this.cmbYes.Text = "Yes";
            this.ToolTip1.SetToolTip(this.cmbYes, null);
            // 
            // lblYes
            // 
            this.lblYes.AutoSize = true;
            this.lblYes.Location = new System.Drawing.Point(20, 44);
            this.lblYes.Name = "lblYes";
            this.lblYes.Size = new System.Drawing.Size(116, 17);
            this.lblYes.TabIndex = 133;
            this.lblYes.Text = "MAIL REMINDER";
            this.ToolTip1.SetToolTip(this.lblYes, null);
            // 
            // cmbFleet
            // 
            this.cmbFleet.Items.AddRange(new object[] {
            "Fleet",
            "No",
            "Group"});
            this.cmbFleet.Location = new System.Drawing.Point(20, 30);
            this.cmbFleet.Name = "cmbFleet";
            this.cmbFleet.Size = new System.Drawing.Size(256, 40);
            this.cmbFleet.TabIndex = 124;
            this.cmbFleet.Text = "No";
            this.ToolTip1.SetToolTip(this.cmbFleet, null);
            this.cmbFleet.SelectedIndexChanged += new System.EventHandler(this.optFleet_CheckedChanged);
            // 
            // cmbNoMaineReReg
            // 
            this.cmbNoMaineReReg.Items.AddRange(new object[] {
            "NO",
            "YES - Expires this year",
            "YES - Expires next year"});
            this.cmbNoMaineReReg.Location = new System.Drawing.Point(459, 32);
            this.cmbNoMaineReReg.Name = "cmbNoMaineReReg";
            this.cmbNoMaineReReg.Size = new System.Drawing.Size(234, 40);
            this.cmbNoMaineReReg.TabIndex = 19;
            this.cmbNoMaineReReg.Text = "NO";
            this.ToolTip1.SetToolTip(this.cmbNoMaineReReg, null);
            this.cmbNoMaineReReg.SelectedIndexChanged += new System.EventHandler(this.cmbNoMaineReReg_SelectedIndexChanged);
            // 
            // fraAdditionalInfo
            // 
            this.fraAdditionalInfo.AppearanceKey = "groupBoxLeftBorder";
            this.fraAdditionalInfo.Controls.Add(this.chkOverrideFleetEmail);
            this.fraAdditionalInfo.Controls.Add(this.cmbYes);
            this.fraAdditionalInfo.Controls.Add(this.lblYes);
            this.fraAdditionalInfo.Controls.Add(this.fraUserFields);
            this.fraAdditionalInfo.Controls.Add(this.cmdDone);
            this.fraAdditionalInfo.Controls.Add(this.fraMessage);
            this.fraAdditionalInfo.Controls.Add(this.txtEMail);
            this.fraAdditionalInfo.Controls.Add(this.lblEMail);
            this.fraAdditionalInfo.Location = new System.Drawing.Point(30, 129);
            this.fraAdditionalInfo.Name = "fraAdditionalInfo";
            this.fraAdditionalInfo.Size = new System.Drawing.Size(567, 740);
            this.fraAdditionalInfo.TabIndex = 3;
            this.fraAdditionalInfo.Text = "Additional Information";
            this.ToolTip1.SetToolTip(this.fraAdditionalInfo, null);
            this.fraAdditionalInfo.Visible = false;
            // 
            // chkOverrideFleetEmail
            // 
            this.chkOverrideFleetEmail.Location = new System.Drawing.Point(387, 84);
            this.chkOverrideFleetEmail.Name = "chkOverrideFleetEmail";
            this.chkOverrideFleetEmail.Size = new System.Drawing.Size(155, 24);
            this.chkOverrideFleetEmail.TabIndex = 131;
            this.chkOverrideFleetEmail.TabStop = false;
            this.chkOverrideFleetEmail.Text = "Override Fleet Email";
            this.ToolTip1.SetToolTip(this.chkOverrideFleetEmail, null);
            this.chkOverrideFleetEmail.Visible = false;
            this.chkOverrideFleetEmail.CheckedChanged += new System.EventHandler(this.chkOverrideFleetEmail_CheckedChanged);
            // 
            // fraUserFields
            // 
            this.fraUserFields.Controls.Add(this.txtUserReason1);
            this.fraUserFields.Controls.Add(this.txtUserField1);
            this.fraUserFields.Controls.Add(this.txtUserField2);
            this.fraUserFields.Controls.Add(this.txtUserField3);
            this.fraUserFields.Controls.Add(this.txtUserReason2);
            this.fraUserFields.Controls.Add(this.txtUserReason3);
            this.fraUserFields.Controls.Add(this.lblInfo3);
            this.fraUserFields.Controls.Add(this.lblInfo2);
            this.fraUserFields.Controls.Add(this.lblInfo1);
            this.fraUserFields.Controls.Add(this.lblUserField3);
            this.fraUserFields.Controls.Add(this.lblUserField2);
            this.fraUserFields.Controls.Add(this.lblUserField1);
            this.fraUserFields.Location = new System.Drawing.Point(20, 330);
            this.fraUserFields.Name = "fraUserFields";
            this.fraUserFields.Size = new System.Drawing.Size(496, 340);
            this.fraUserFields.TabIndex = 37;
            this.fraUserFields.Text = "User Defined Fields";
            this.ToolTip1.SetToolTip(this.fraUserFields, null);
            this.fraUserFields.Visible = false;
            // 
            // txtUserReason1
            // 
            this.txtUserReason1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserReason1.Location = new System.Drawing.Point(170, 80);
            this.txtUserReason1.Name = "txtUserReason1";
            this.txtUserReason1.Size = new System.Drawing.Size(306, 40);
            this.txtUserReason1.TabIndex = 69;
            this.ToolTip1.SetToolTip(this.txtUserReason1, null);
            this.txtUserReason1.Visible = false;
            this.txtUserReason1.Enter += new System.EventHandler(this.txtUserReason1_Enter);
            this.txtUserReason1.Leave += new System.EventHandler(this.txtUserReason1_Leave);
            // 
            // txtUserField1
            // 
            this.txtUserField1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserField1.Location = new System.Drawing.Point(170, 30);
            this.txtUserField1.Name = "txtUserField1";
            this.txtUserField1.Size = new System.Drawing.Size(306, 40);
            this.txtUserField1.TabIndex = 67;
            this.ToolTip1.SetToolTip(this.txtUserField1, null);
            this.txtUserField1.Visible = false;
            this.txtUserField1.Enter += new System.EventHandler(this.txtUserField1_Enter);
            this.txtUserField1.Leave += new System.EventHandler(this.txtUserField1_Leave);
            // 
            // txtUserField2
            // 
            this.txtUserField2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserField2.Location = new System.Drawing.Point(170, 130);
            this.txtUserField2.Name = "txtUserField2";
            this.txtUserField2.Size = new System.Drawing.Size(306, 40);
            this.txtUserField2.TabIndex = 71;
            this.ToolTip1.SetToolTip(this.txtUserField2, null);
            this.txtUserField2.Visible = false;
            this.txtUserField2.Enter += new System.EventHandler(this.txtUserField2_Enter);
            this.txtUserField2.Leave += new System.EventHandler(this.txtUserField2_Leave);
            // 
            // txtUserField3
            // 
            this.txtUserField3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserField3.Location = new System.Drawing.Point(170, 230);
            this.txtUserField3.Name = "txtUserField3";
            this.txtUserField3.Size = new System.Drawing.Size(305, 40);
            this.txtUserField3.TabIndex = 75;
            this.ToolTip1.SetToolTip(this.txtUserField3, null);
            this.txtUserField3.Visible = false;
            this.txtUserField3.Enter += new System.EventHandler(this.txtUserField3_Enter);
            this.txtUserField3.Leave += new System.EventHandler(this.txtUserField3_Leave);
            // 
            // txtUserReason2
            // 
            this.txtUserReason2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserReason2.Location = new System.Drawing.Point(170, 180);
            this.txtUserReason2.Name = "txtUserReason2";
            this.txtUserReason2.Size = new System.Drawing.Size(306, 40);
            this.txtUserReason2.TabIndex = 73;
            this.ToolTip1.SetToolTip(this.txtUserReason2, null);
            this.txtUserReason2.Visible = false;
            this.txtUserReason2.Enter += new System.EventHandler(this.txtUserReason2_Enter);
            this.txtUserReason2.Leave += new System.EventHandler(this.txtUserReason2_Leave);
            // 
            // txtUserReason3
            // 
            this.txtUserReason3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUserReason3.Location = new System.Drawing.Point(170, 280);
            this.txtUserReason3.Name = "txtUserReason3";
            this.txtUserReason3.Size = new System.Drawing.Size(306, 40);
            this.txtUserReason3.TabIndex = 77;
            this.ToolTip1.SetToolTip(this.txtUserReason3, null);
            this.txtUserReason3.Visible = false;
            this.txtUserReason3.Enter += new System.EventHandler(this.txtUserReason3_Enter);
            this.txtUserReason3.Leave += new System.EventHandler(this.txtUserReason3_Leave);
            // 
            // lblInfo3
            // 
            this.lblInfo3.BackColor = System.Drawing.Color.Transparent;
            this.lblInfo3.Location = new System.Drawing.Point(20, 294);
            this.lblInfo3.Name = "lblInfo3";
            this.lblInfo3.Size = new System.Drawing.Size(92, 16);
            this.lblInfo3.TabIndex = 54;
            this.lblInfo3.Text = "INFORMATION";
            this.ToolTip1.SetToolTip(this.lblInfo3, null);
            this.lblInfo3.Visible = false;
            // 
            // lblInfo2
            // 
            this.lblInfo2.BackColor = System.Drawing.Color.Transparent;
            this.lblInfo2.Location = new System.Drawing.Point(20, 194);
            this.lblInfo2.Name = "lblInfo2";
            this.lblInfo2.Size = new System.Drawing.Size(92, 16);
            this.lblInfo2.TabIndex = 53;
            this.lblInfo2.Text = "INFORMATION";
            this.ToolTip1.SetToolTip(this.lblInfo2, null);
            this.lblInfo2.Visible = false;
            // 
            // lblInfo1
            // 
            this.lblInfo1.BackColor = System.Drawing.Color.Transparent;
            this.lblInfo1.Location = new System.Drawing.Point(20, 94);
            this.lblInfo1.Name = "lblInfo1";
            this.lblInfo1.Size = new System.Drawing.Size(92, 16);
            this.lblInfo1.TabIndex = 52;
            this.lblInfo1.Text = "INFORMATION";
            this.ToolTip1.SetToolTip(this.lblInfo1, null);
            this.lblInfo1.Visible = false;
            // 
            // lblUserField3
            // 
            this.lblUserField3.BackColor = System.Drawing.Color.Transparent;
            this.lblUserField3.Location = new System.Drawing.Point(20, 244);
            this.lblUserField3.Name = "lblUserField3";
            this.lblUserField3.Size = new System.Drawing.Size(122, 20);
            this.lblUserField3.TabIndex = 51;
            this.ToolTip1.SetToolTip(this.lblUserField3, null);
            this.lblUserField3.Visible = false;
            // 
            // lblUserField2
            // 
            this.lblUserField2.BackColor = System.Drawing.Color.Transparent;
            this.lblUserField2.Location = new System.Drawing.Point(20, 144);
            this.lblUserField2.Name = "lblUserField2";
            this.lblUserField2.Size = new System.Drawing.Size(122, 20);
            this.lblUserField2.TabIndex = 50;
            this.ToolTip1.SetToolTip(this.lblUserField2, null);
            this.lblUserField2.Visible = false;
            // 
            // lblUserField1
            // 
            this.lblUserField1.BackColor = System.Drawing.Color.Transparent;
            this.lblUserField1.Location = new System.Drawing.Point(20, 44);
            this.lblUserField1.Name = "lblUserField1";
            this.lblUserField1.Size = new System.Drawing.Size(122, 20);
            this.lblUserField1.TabIndex = 49;
            this.ToolTip1.SetToolTip(this.lblUserField1, null);
            this.lblUserField1.Visible = false;
            // 
            // cmdDone
            // 
            this.cmdDone.AppearanceKey = "actionButton";
            this.cmdDone.Location = new System.Drawing.Point(20, 680);
            this.cmdDone.Name = "cmdDone";
            this.cmdDone.Size = new System.Drawing.Size(66, 40);
            this.cmdDone.TabIndex = 79;
            this.cmdDone.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdDone, null);
            this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
            // 
            // fraMessage
            // 
            this.fraMessage.Controls.Add(this.cmdErase);
            this.fraMessage.Controls.Add(this.cboMessageCodes);
            this.fraMessage.Controls.Add(this.txtMessage);
            this.fraMessage.Controls.Add(this.lblMesageCode);
            this.fraMessage.Controls.Add(this.lblMessage);
            this.fraMessage.Location = new System.Drawing.Point(20, 130);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(496, 190);
            this.fraMessage.TabIndex = 34;
            this.fraMessage.Text = "User Message";
            this.ToolTip1.SetToolTip(this.fraMessage, null);
            // 
            // cmdErase
            // 
            this.cmdErase.AppearanceKey = "actionButton";
            this.cmdErase.Location = new System.Drawing.Point(170, 80);
            this.cmdErase.Name = "cmdErase";
            this.cmdErase.Size = new System.Drawing.Size(162, 40);
            this.cmdErase.TabIndex = 65;
            this.cmdErase.Text = "Erase Message";
            this.ToolTip1.SetToolTip(this.cmdErase, null);
            this.cmdErase.Click += new System.EventHandler(this.cmdErase_Click);
            // 
            // cboMessageCodes
            // 
            this.cboMessageCodes.BackColor = System.Drawing.SystemColors.Window;
            this.cboMessageCodes.Items.AddRange(new object[] {
            "1 - Message ",
            "2 - Warning",
            "3 - Stop Process"});
            this.cboMessageCodes.Location = new System.Drawing.Point(170, 30);
            this.cboMessageCodes.Name = "cboMessageCodes";
            this.cboMessageCodes.Size = new System.Drawing.Size(305, 40);
            this.cboMessageCodes.TabIndex = 61;
            this.ToolTip1.SetToolTip(this.cboMessageCodes, null);
            // 
            // txtMessage
            // 
            this.txtMessage.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMessage.Location = new System.Drawing.Point(170, 130);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(306, 40);
            this.txtMessage.TabIndex = 63;
            this.ToolTip1.SetToolTip(this.txtMessage, null);
            this.txtMessage.Enter += new System.EventHandler(this.txtMessage_Enter);
            this.txtMessage.Leave += new System.EventHandler(this.txtMessage_Leave);
            // 
            // lblMesageCode
            // 
            this.lblMesageCode.Location = new System.Drawing.Point(20, 44);
            this.lblMesageCode.Name = "lblMesageCode";
            this.lblMesageCode.TabIndex = 35;
            this.lblMesageCode.Text = "MESSAGE CODE";
            this.ToolTip1.SetToolTip(this.lblMesageCode, null);
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblMessage.Location = new System.Drawing.Point(20, 144);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(70, 16);
            this.lblMessage.TabIndex = 36;
            this.lblMessage.Text = "MESSAGE";
            this.ToolTip1.SetToolTip(this.lblMessage, null);
            // 
            // txtEMail
            // 
            this.txtEMail.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtEMail.Location = new System.Drawing.Point(150, 80);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Size = new System.Drawing.Size(221, 40);
            this.txtEMail.TabIndex = 59;
            this.ToolTip1.SetToolTip(this.txtEMail, null);
            this.txtEMail.Enter += new System.EventHandler(this.txtEMail_Enter);
            this.txtEMail.Leave += new System.EventHandler(this.txtEMail_Leave);
            // 
            // lblEMail
            // 
            this.lblEMail.BackColor = System.Drawing.Color.Transparent;
            this.lblEMail.Location = new System.Drawing.Point(20, 94);
            this.lblEMail.Name = "lblEMail";
            this.lblEMail.TabIndex = 56;
            this.lblEMail.Text = "E-MAIL ADDRESS";
            this.ToolTip1.SetToolTip(this.lblEMail, null);
            // 
            // fraFeesInfo
            // 
            this.fraFeesInfo.Controls.Add(this.cmdExit);
            this.fraFeesInfo.Controls.Add(this.cmdReturn);
            this.fraFeesInfo.Controls.Add(this.txtFEEAgentFeeReg);
            this.fraFeesInfo.Controls.Add(this.txtFEESalesTax);
            this.fraFeesInfo.Controls.Add(this.txtFEETitleFee);
            this.fraFeesInfo.Controls.Add(this.txtFEETotal);
            this.fraFeesInfo.Controls.Add(this.txtFEECreditTransfer);
            this.fraFeesInfo.Controls.Add(this.txtFEERegFee);
            this.fraFeesInfo.Controls.Add(this.txtFeeAgentFeeCTA);
            this.fraFeesInfo.Controls.Add(this.txtFEETransferFee);
            this.fraFeesInfo.Controls.Add(this.txtFeeAgentFeeTransfer);
            this.fraFeesInfo.Controls.Add(this.txtFeeAgentFeeCorrection);
            this.fraFeesInfo.Controls.Add(this.txtFEERushTitleFee);
            this.fraFeesInfo.Controls.Add(this.Label2_7);
            this.fraFeesInfo.Controls.Add(this.Label35);
            this.fraFeesInfo.Controls.Add(this.Label2_4);
            this.fraFeesInfo.Controls.Add(this.Label34);
            this.fraFeesInfo.Controls.Add(this.Label33);
            this.fraFeesInfo.Controls.Add(this.Label2_10);
            this.fraFeesInfo.Controls.Add(this.Line1);
            this.fraFeesInfo.Controls.Add(this.Label2_8);
            this.fraFeesInfo.Controls.Add(this.Label2_6);
            this.fraFeesInfo.Controls.Add(this.Label2_5);
            this.fraFeesInfo.Controls.Add(this.Label2_1);
            this.fraFeesInfo.Controls.Add(this.Label2_2);
            this.fraFeesInfo.Controls.Add(this.Label2_3);
            this.fraFeesInfo.Location = new System.Drawing.Point(30, 129);
            this.fraFeesInfo.Name = "fraFeesInfo";
            this.fraFeesInfo.Size = new System.Drawing.Size(303, 650);
            this.fraFeesInfo.TabIndex = 58;
            this.fraFeesInfo.Text = "Fees Information";
            this.ToolTip1.SetToolTip(this.fraFeesInfo, null);
            this.fraFeesInfo.Visible = false;
            // 
            // cmdExit
            // 
            this.cmdExit.AppearanceKey = "actionButton";
            this.cmdExit.Location = new System.Drawing.Point(122, 590);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(72, 40);
            this.cmdExit.TabIndex = 48;
            this.cmdExit.Text = "Exit";
            this.ToolTip1.SetToolTip(this.cmdExit, null);
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(20, 590);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(80, 40);
            this.cmdReturn.TabIndex = 47;
            this.cmdReturn.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdReturn, null);
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // txtFEEAgentFeeReg
            // 
            this.txtFEEAgentFeeReg.Location = new System.Drawing.Point(173, 30);
            this.txtFEEAgentFeeReg.MaxLength = 8;
            this.txtFEEAgentFeeReg.Name = "txtFEEAgentFeeReg";
            this.txtFEEAgentFeeReg.Size = new System.Drawing.Size(110, 22);
            this.txtFEEAgentFeeReg.TabIndex = 38;
            this.ToolTip1.SetToolTip(this.txtFEEAgentFeeReg, null);
            this.txtFEEAgentFeeReg.Enter += new System.EventHandler(this.txtFEEAgentFeeReg_Enter);
            this.txtFEEAgentFeeReg.Leave += new System.EventHandler(this.txtFEEAgentFeeReg_Leave);
            this.txtFEEAgentFeeReg.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEEAgentFeeReg_Validate);
            // 
            // txtFEESalesTax
            // 
            this.txtFEESalesTax.Location = new System.Drawing.Point(173, 380);
            this.txtFEESalesTax.MaxLength = 8;
            this.txtFEESalesTax.Name = "txtFEESalesTax";
            this.txtFEESalesTax.Size = new System.Drawing.Size(110, 22);
            this.txtFEESalesTax.TabIndex = 44;
            this.ToolTip1.SetToolTip(this.txtFEESalesTax, null);
            this.txtFEESalesTax.Enter += new System.EventHandler(this.txtFEESalesTax_Enter);
            this.txtFEESalesTax.Leave += new System.EventHandler(this.txtFEESalesTax_Leave);
            this.txtFEESalesTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEESalesTax_Validate);
            // 
            // txtFEETitleFee
            // 
            this.txtFEETitleFee.Location = new System.Drawing.Point(173, 430);
            this.txtFEETitleFee.MaxLength = 8;
            this.txtFEETitleFee.Name = "txtFEETitleFee";
            this.txtFEETitleFee.Size = new System.Drawing.Size(110, 22);
            this.txtFEETitleFee.TabIndex = 45;
            this.ToolTip1.SetToolTip(this.txtFEETitleFee, null);
            this.txtFEETitleFee.Enter += new System.EventHandler(this.txtFEETitleFee_Enter);
            this.txtFEETitleFee.Leave += new System.EventHandler(this.txtFEETitleFee_Leave);
            this.txtFEETitleFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEETitleFee_Validate);
            // 
            // txtFEETotal
            // 
            this.txtFEETotal.Location = new System.Drawing.Point(173, 530);
            this.txtFEETotal.MaxLength = 8;
            this.txtFEETotal.Name = "txtFEETotal";
            this.txtFEETotal.Size = new System.Drawing.Size(110, 22);
            this.txtFEETotal.TabIndex = 46;
            this.txtFEETotal.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtFEETotal, null);
            // 
            // txtFEECreditTransfer
            // 
            this.txtFEECreditTransfer.Location = new System.Drawing.Point(173, 280);
            this.txtFEECreditTransfer.MaxLength = 8;
            this.txtFEECreditTransfer.Name = "txtFEECreditTransfer";
            this.txtFEECreditTransfer.Size = new System.Drawing.Size(110, 22);
            this.txtFEECreditTransfer.TabIndex = 42;
            this.ToolTip1.SetToolTip(this.txtFEECreditTransfer, null);
            this.txtFEECreditTransfer.Enter += new System.EventHandler(this.txtFEECreditTransfer_Enter);
            this.txtFEECreditTransfer.Leave += new System.EventHandler(this.txtFEECreditTransfer_Leave);
            this.txtFEECreditTransfer.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEECreditTransfer_Validate);
            // 
            // txtFEERegFee
            // 
            this.txtFEERegFee.Location = new System.Drawing.Point(173, 230);
            this.txtFEERegFee.MaxLength = 8;
            this.txtFEERegFee.Name = "txtFEERegFee";
            this.txtFEERegFee.Size = new System.Drawing.Size(110, 22);
            this.txtFEERegFee.TabIndex = 41;
            this.ToolTip1.SetToolTip(this.txtFEERegFee, null);
            this.txtFEERegFee.Enter += new System.EventHandler(this.txtFEERegFee_Enter);
            this.txtFEERegFee.Leave += new System.EventHandler(this.txtFEERegFee_Leave);
            this.txtFEERegFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEERegFee_Validate);
            // 
            // txtFeeAgentFeeCTA
            // 
            this.txtFeeAgentFeeCTA.Location = new System.Drawing.Point(173, 80);
            this.txtFeeAgentFeeCTA.MaxLength = 8;
            this.txtFeeAgentFeeCTA.Name = "txtFeeAgentFeeCTA";
            this.txtFeeAgentFeeCTA.Size = new System.Drawing.Size(110, 22);
            this.txtFeeAgentFeeCTA.TabIndex = 39;
            this.ToolTip1.SetToolTip(this.txtFeeAgentFeeCTA, null);
            this.txtFeeAgentFeeCTA.Enter += new System.EventHandler(this.txtFeeAgentFeeCTA_Enter);
            this.txtFeeAgentFeeCTA.Leave += new System.EventHandler(this.txtFeeAgentFeeCTA_Leave);
            this.txtFeeAgentFeeCTA.Validating += new System.ComponentModel.CancelEventHandler(this.txtFeeAgentFeeCTA_Validate);
            // 
            // txtFEETransferFee
            // 
            this.txtFEETransferFee.Location = new System.Drawing.Point(173, 330);
            this.txtFEETransferFee.MaxLength = 8;
            this.txtFEETransferFee.Name = "txtFEETransferFee";
            this.txtFEETransferFee.Size = new System.Drawing.Size(110, 22);
            this.txtFEETransferFee.TabIndex = 43;
            this.ToolTip1.SetToolTip(this.txtFEETransferFee, null);
            this.txtFEETransferFee.Enter += new System.EventHandler(this.txtFEETransferFee_Enter);
            this.txtFEETransferFee.Leave += new System.EventHandler(this.txtFEETransferFee_Leave);
            this.txtFEETransferFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEETransferFee_Validate);
            // 
            // txtFeeAgentFeeTransfer
            // 
            this.txtFeeAgentFeeTransfer.Location = new System.Drawing.Point(173, 130);
            this.txtFeeAgentFeeTransfer.MaxLength = 8;
            this.txtFeeAgentFeeTransfer.Name = "txtFeeAgentFeeTransfer";
            this.txtFeeAgentFeeTransfer.Size = new System.Drawing.Size(110, 22);
            this.txtFeeAgentFeeTransfer.TabIndex = 40;
            this.ToolTip1.SetToolTip(this.txtFeeAgentFeeTransfer, null);
            this.txtFeeAgentFeeTransfer.Enter += new System.EventHandler(this.txtFeeAgentFeeTransfer_Enter);
            this.txtFeeAgentFeeTransfer.Leave += new System.EventHandler(this.txtFeeAgentFeeTransfer_Leave);
            this.txtFeeAgentFeeTransfer.Validating += new System.ComponentModel.CancelEventHandler(this.txtFeeAgentFeeTransfer_Validate);
            // 
            // txtFeeAgentFeeCorrection
            // 
            this.txtFeeAgentFeeCorrection.Location = new System.Drawing.Point(173, 180);
            this.txtFeeAgentFeeCorrection.MaxLength = 8;
            this.txtFeeAgentFeeCorrection.Name = "txtFeeAgentFeeCorrection";
            this.txtFeeAgentFeeCorrection.Size = new System.Drawing.Size(110, 22);
            this.txtFeeAgentFeeCorrection.TabIndex = 115;
            this.ToolTip1.SetToolTip(this.txtFeeAgentFeeCorrection, null);
            this.txtFeeAgentFeeCorrection.Enter += new System.EventHandler(this.txtFeeAgentFeeCorrection_Enter);
            this.txtFeeAgentFeeCorrection.Leave += new System.EventHandler(this.txtFeeAgentFeeCorrection_Leave);
            this.txtFeeAgentFeeCorrection.Validating += new System.ComponentModel.CancelEventHandler(this.txtFeeAgentFeeCorrection_Validate);
            // 
            // txtFEERushTitleFee
            // 
            this.txtFEERushTitleFee.Location = new System.Drawing.Point(172, 480);
            this.txtFEERushTitleFee.MaxLength = 8;
            this.txtFEERushTitleFee.Name = "txtFEERushTitleFee";
            this.txtFEERushTitleFee.Size = new System.Drawing.Size(110, 22);
            this.txtFEERushTitleFee.TabIndex = 129;
            this.ToolTip1.SetToolTip(this.txtFEERushTitleFee, null);
            this.txtFEERushTitleFee.Enter += new System.EventHandler(this.txtFEERushTitleFee_Enter);
            this.txtFEERushTitleFee.Leave += new System.EventHandler(this.txtFEERushTitleFee_Leave);
            this.txtFEERushTitleFee.Validating += new System.ComponentModel.CancelEventHandler(this.txtFEERushTitleFee_Validate);
            // 
            // Label2_7
            // 
            this.Label2_7.Location = new System.Drawing.Point(20, 494);
            this.Label2_7.Name = "Label2_7";
            this.Label2_7.Size = new System.Drawing.Size(98, 14);
            this.Label2_7.TabIndex = 130;
            this.Label2_7.Text = "RUSH TITLE FEE";
            this.ToolTip1.SetToolTip(this.Label2_7, null);
            // 
            // Label35
            // 
            this.Label35.Location = new System.Drawing.Point(20, 194);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(104, 14);
            this.Label35.TabIndex = 116;
            this.Label35.Text = "CORRECTION";
            this.ToolTip1.SetToolTip(this.Label35, null);
            // 
            // Label2_4
            // 
            this.Label2_4.Location = new System.Drawing.Point(20, 44);
            this.Label2_4.Name = "Label2_4";
            this.Label2_4.Size = new System.Drawing.Size(72, 14);
            this.Label2_4.TabIndex = 114;
            this.Label2_4.Text = "AGENT FEE";
            this.ToolTip1.SetToolTip(this.Label2_4, null);
            // 
            // Label34
            // 
            this.Label34.Location = new System.Drawing.Point(20, 144);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(104, 14);
            this.Label34.TabIndex = 113;
            this.Label34.Text = "TRANSFER";
            this.ToolTip1.SetToolTip(this.Label34, null);
            // 
            // Label33
            // 
            this.Label33.Location = new System.Drawing.Point(20, 244);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(130, 15);
            this.Label33.TabIndex = 74;
            this.Label33.Text = "REGISTRATION RATE";
            this.ToolTip1.SetToolTip(this.Label33, null);
            // 
            // Label2_10
            // 
            this.Label2_10.Location = new System.Drawing.Point(20, 294);
            this.Label2_10.Name = "Label2_10";
            this.Label2_10.Size = new System.Drawing.Size(115, 14);
            this.Label2_10.TabIndex = 72;
            this.Label2_10.Text = "CREDIT: TRANSFER";
            this.ToolTip1.SetToolTip(this.Label2_10, null);
            // 
            // Line1
            // 
            this.Line1.Location = new System.Drawing.Point(300, 3136);
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(3450, 1);
            this.ToolTip1.SetToolTip(this.Line1, null);
            this.Line1.X1 = 300F;
            this.Line1.X2 = 3750F;
            this.Line1.Y1 = 3136F;
            this.Line1.Y2 = 3136F;
            // 
            // Label2_8
            // 
            this.Label2_8.Enabled = false;
            this.Label2_8.Location = new System.Drawing.Point(20, 544);
            this.Label2_8.Name = "Label2_8";
            this.Label2_8.Size = new System.Drawing.Size(98, 14);
            this.Label2_8.TabIndex = 70;
            this.Label2_8.Text = "TOTAL FEES";
            this.ToolTip1.SetToolTip(this.Label2_8, null);
            // 
            // Label2_6
            // 
            this.Label2_6.Location = new System.Drawing.Point(20, 444);
            this.Label2_6.Name = "Label2_6";
            this.Label2_6.Size = new System.Drawing.Size(98, 14);
            this.Label2_6.TabIndex = 68;
            this.Label2_6.Text = "TITLE FEE";
            this.ToolTip1.SetToolTip(this.Label2_6, null);
            // 
            // Label2_5
            // 
            this.Label2_5.Location = new System.Drawing.Point(20, 394);
            this.Label2_5.Name = "Label2_5";
            this.Label2_5.Size = new System.Drawing.Size(98, 14);
            this.Label2_5.TabIndex = 66;
            this.Label2_5.Text = "SALES TAX";
            this.ToolTip1.SetToolTip(this.Label2_5, null);
            // 
            // Label2_1
            // 
            this.Label2_1.Location = new System.Drawing.Point(94, 44);
            this.Label2_1.Name = "Label2_1";
            this.Label2_1.Size = new System.Drawing.Size(35, 14);
            this.Label2_1.TabIndex = 64;
            this.Label2_1.Text = "REG";
            this.Label2_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.Label2_1, null);
            // 
            // Label2_2
            // 
            this.Label2_2.Location = new System.Drawing.Point(20, 94);
            this.Label2_2.Name = "Label2_2";
            this.Label2_2.Size = new System.Drawing.Size(108, 14);
            this.Label2_2.TabIndex = 62;
            this.Label2_2.Text = "CTA";
            this.ToolTip1.SetToolTip(this.Label2_2, null);
            // 
            // Label2_3
            // 
            this.Label2_3.Location = new System.Drawing.Point(20, 344);
            this.Label2_3.Name = "Label2_3";
            this.Label2_3.Size = new System.Drawing.Size(98, 14);
            this.Label2_3.TabIndex = 60;
            this.Label2_3.Text = "TRANSFER FEE";
            this.ToolTip1.SetToolTip(this.Label2_3, null);
            // 
            // chkTimePayment
            // 
            this.chkTimePayment.Location = new System.Drawing.Point(360, 55);
            this.chkTimePayment.Name = "chkTimePayment";
            this.chkTimePayment.Size = new System.Drawing.Size(117, 24);
            this.chkTimePayment.TabIndex = 128;
            this.chkTimePayment.Text = "Time Payment";
            this.ToolTip1.SetToolTip(this.chkTimePayment, null);
            this.chkTimePayment.CheckedChanged += new System.EventHandler(this.chkTimePayment_CheckedChanged);
            // 
            // chk25YearPlate
            // 
            this.chk25YearPlate.Location = new System.Drawing.Point(360, 92);
            this.chk25YearPlate.Name = "chk25YearPlate";
            this.chk25YearPlate.Size = new System.Drawing.Size(155, 24);
            this.chk25YearPlate.TabIndex = 127;
            this.chk25YearPlate.Text = "25 Year Registration";
            this.ToolTip1.SetToolTip(this.chk25YearPlate, null);
            this.chk25YearPlate.CheckedChanged += new System.EventHandler(this.chk25YearPlate_CheckedChanged);
            // 
            // fraDataInput
            // 
            this.fraDataInput.AppearanceKey = "groupBoxNoBorders";
            this.fraDataInput.Controls.Add(this.txtReg2PartyID);
            this.fraDataInput.Controls.Add(this.cmbNoMaineReReg);
            this.fraDataInput.Controls.Add(this.cmdReg2Search);
            this.fraDataInput.Controls.Add(this.cmdReg2Edit);
            this.fraDataInput.Controls.Add(this.txtReg1PartyID);
            this.fraDataInput.Controls.Add(this.cmdReg1Search);
            this.fraDataInput.Controls.Add(this.cmdReg1Edit);
            this.fraDataInput.Controls.Add(this.fraFleet);
            this.fraDataInput.Controls.Add(this.txtNetWeight);
            this.fraDataInput.Controls.Add(this.cmdMoreInfo);
            this.fraDataInput.Controls.Add(this.txtMake);
            this.fraDataInput.Controls.Add(this.txtYear);
            this.fraDataInput.Controls.Add(this.txtUnit);
            this.fraDataInput.Controls.Add(this.txtColor1);
            this.fraDataInput.Controls.Add(this.txtColor2);
            this.fraDataInput.Controls.Add(this.txtPlate);
            this.fraDataInput.Controls.Add(this.txtStyle);
            this.fraDataInput.Controls.Add(this.txtVIN);
            this.fraDataInput.Controls.Add(this.txtTitle);
            this.fraDataInput.Controls.Add(this.txtAddress1);
            this.fraDataInput.Controls.Add(this.txtCity);
            this.fraDataInput.Controls.Add(this.txtZip);
            this.fraDataInput.Controls.Add(this.txtState);
            this.fraDataInput.Controls.Add(this.txtLegalResCity);
            this.fraDataInput.Controls.Add(this.txtLegalResState);
            this.fraDataInput.Controls.Add(this.txtRate);
            this.fraDataInput.Controls.Add(this.txtCredit);
            this.fraDataInput.Controls.Add(this.txtFees);
            this.fraDataInput.Controls.Add(this.txtExpires);
            this.fraDataInput.Controls.Add(this.txtLegalZip);
            this.fraDataInput.Controls.Add(this.txtLegalResAddress);
            this.fraDataInput.Controls.Add(this.txtModel);
            this.fraDataInput.Controls.Add(this.Label36);
            this.fraDataInput.Controls.Add(this.lblRegistrant2);
            this.fraDataInput.Controls.Add(this.lblRegistrant1);
            this.fraDataInput.Controls.Add(this.Label1);
            this.fraDataInput.Controls.Add(this.Label2_0);
            this.fraDataInput.Controls.Add(this.Label4);
            this.fraDataInput.Controls.Add(this.Line18);
            this.fraDataInput.Controls.Add(this.Image1);
            this.fraDataInput.Controls.Add(this.Label12);
            this.fraDataInput.Controls.Add(this.Label5);
            this.fraDataInput.Controls.Add(this.Label6);
            this.fraDataInput.Controls.Add(this.Label14);
            this.fraDataInput.Controls.Add(this.Label9);
            this.fraDataInput.Controls.Add(this.Label10);
            this.fraDataInput.Controls.Add(this.Label11);
            this.fraDataInput.Controls.Add(this.Label13);
            this.fraDataInput.Controls.Add(this.Label22);
            this.fraDataInput.Controls.Add(this.Label8);
            this.fraDataInput.Controls.Add(this.Label16);
            this.fraDataInput.Controls.Add(this.Label19);
            this.fraDataInput.Controls.Add(this.Label23);
            this.fraDataInput.Controls.Add(this.Label27);
            this.fraDataInput.Controls.Add(this.Label29);
            this.fraDataInput.Controls.Add(this.Label31);
            this.fraDataInput.Controls.Add(this.Label32);
            this.fraDataInput.Location = new System.Drawing.Point(10, 129);
            this.fraDataInput.Name = "fraDataInput";
            this.fraDataInput.Size = new System.Drawing.Size(789, 628);
            this.fraDataInput.TabIndex = 76;
            this.ToolTip1.SetToolTip(this.fraDataInput, null);
            // 
            // txtReg2PartyID
            // 
            this.txtReg2PartyID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtReg2PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg2PartyID.Location = new System.Drawing.Point(103, 285);
            this.txtReg2PartyID.Name = "txtReg2PartyID";
            this.txtReg2PartyID.Size = new System.Drawing.Size(50, 40);
            this.txtReg2PartyID.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.txtReg2PartyID, null);
            this.txtReg2PartyID.Enter += new System.EventHandler(this.txtReg2PartyID_Enter);
            this.txtReg2PartyID.Leave += new System.EventHandler(this.txtReg2PartyID_Leave);
            this.txtReg2PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2PartyID_Validating);
            this.txtReg2PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg2PartyID_KeyPress);
            // 
            // cmdReg2Search
            // 
            this.cmdReg2Search.AppearanceKey = "actionButton";
            this.cmdReg2Search.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg2Search.Image")));
            this.cmdReg2Search.Location = new System.Drawing.Point(163, 285);
            this.cmdReg2Search.Name = "cmdReg2Search";
            this.cmdReg2Search.Size = new System.Drawing.Size(40, 40);
            this.cmdReg2Search.TabIndex = 19;
            this.cmdReg2Search.TabStop = false;
            this.ToolTip1.SetToolTip(this.cmdReg2Search, null);
            this.cmdReg2Search.Click += new System.EventHandler(this.cmdReg2Search_Click);
            // 
            // cmdReg2Edit
            // 
            this.cmdReg2Edit.AppearanceKey = "actionButton";
            this.cmdReg2Edit.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg2Edit.Image")));
            this.cmdReg2Edit.Location = new System.Drawing.Point(213, 285);
            this.cmdReg2Edit.Name = "cmdReg2Edit";
            this.cmdReg2Edit.Size = new System.Drawing.Size(40, 40);
            this.cmdReg2Edit.TabIndex = 20;
            this.cmdReg2Edit.TabStop = false;
            this.ToolTip1.SetToolTip(this.cmdReg2Edit, null);
            this.cmdReg2Edit.Click += new System.EventHandler(this.cmdReg2Edit_Click);
            // 
            // txtReg1PartyID
            // 
            this.txtReg1PartyID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtReg1PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg1PartyID.Location = new System.Drawing.Point(103, 235);
            this.txtReg1PartyID.Name = "txtReg1PartyID";
            this.txtReg1PartyID.Size = new System.Drawing.Size(50, 40);
            this.txtReg1PartyID.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.txtReg1PartyID, null);
            this.txtReg1PartyID.Enter += new System.EventHandler(this.txtReg1PartyID_Enter);
            this.txtReg1PartyID.Leave += new System.EventHandler(this.txtReg1PartyID_Leave);
            this.txtReg1PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1PartyID_Validating);
            this.txtReg1PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg1PartyID_KeyPress);
            // 
            // cmdReg1Search
            // 
            this.cmdReg1Search.AppearanceKey = "actionButton";
            this.cmdReg1Search.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg1Search.Image")));
            this.cmdReg1Search.Location = new System.Drawing.Point(163, 235);
            this.cmdReg1Search.Name = "cmdReg1Search";
            this.cmdReg1Search.Size = new System.Drawing.Size(40, 40);
            this.cmdReg1Search.TabIndex = 15;
            this.cmdReg1Search.TabStop = false;
            this.ToolTip1.SetToolTip(this.cmdReg1Search, null);
            this.cmdReg1Search.Click += new System.EventHandler(this.cmdReg1Search_Click);
            // 
            // cmdReg1Edit
            // 
            this.cmdReg1Edit.AppearanceKey = "actionButton";
            this.cmdReg1Edit.Image = ((System.Drawing.Image)(resources.GetObject("cmdReg1Edit.Image")));
            this.cmdReg1Edit.Location = new System.Drawing.Point(213, 235);
            this.cmdReg1Edit.Name = "cmdReg1Edit";
            this.cmdReg1Edit.Size = new System.Drawing.Size(40, 40);
            this.cmdReg1Edit.TabIndex = 16;
            this.cmdReg1Edit.TabStop = false;
            this.ToolTip1.SetToolTip(this.cmdReg1Edit, null);
            this.cmdReg1Edit.Click += new System.EventHandler(this.cmdReg1Edit_Click);
            // 
            // fraFleet
            // 
            this.fraFleet.Controls.Add(this.txtFleetNumber);
            this.fraFleet.Controls.Add(this.cmbFleet);
            this.fraFleet.Controls.Add(this.txtFleetName);
            this.fraFleet.Controls.Add(this.imgFleetComment);
            this.fraFleet.Controls.Add(this.lblFleetNumber);
            this.fraFleet.Controls.Add(this.lblFleetName);
            this.fraFleet.Location = new System.Drawing.Point(461, 235);
            this.fraFleet.Name = "fraFleet";
            this.fraFleet.Size = new System.Drawing.Size(296, 230);
            this.fraFleet.TabIndex = 118;
            this.fraFleet.Text = "Fleet/Group";
            this.ToolTip1.SetToolTip(this.fraFleet, null);
            // 
            // txtFleetNumber
            // 
            this.txtFleetNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtFleetNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtFleetNumber.Enabled = false;
            this.txtFleetNumber.Location = new System.Drawing.Point(20, 120);
            this.txtFleetNumber.Name = "txtFleetNumber";
            this.txtFleetNumber.Size = new System.Drawing.Size(40, 40);
            this.txtFleetNumber.TabIndex = 123;
            this.txtFleetNumber.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtFleetNumber, null);
            this.txtFleetNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtFleetNumber_Validating);
            // 
            // txtFleetName
            // 
            this.txtFleetName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFleetName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtFleetName.Enabled = false;
            this.txtFleetName.Location = new System.Drawing.Point(125, 170);
            this.txtFleetName.Name = "txtFleetName";
            this.txtFleetName.Size = new System.Drawing.Size(151, 40);
            this.txtFleetName.TabIndex = 122;
            this.txtFleetName.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtFleetName, null);
            // 
            // imgFleetComment
            // 
            this.imgFleetComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgFleetComment.Image = ((System.Drawing.Image)(resources.GetObject("imgFleetComment.Image")));
            this.imgFleetComment.Location = new System.Drawing.Point(70, 120);
            this.imgFleetComment.Name = "imgFleetComment";
            this.imgFleetComment.Picture = ((System.Drawing.Image)(resources.GetObject("imgFleetComment.Picture")));
            this.imgFleetComment.Size = new System.Drawing.Size(40, 40);
            this.imgFleetComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgFleetComment.TabIndex = 125;
            this.ToolTip1.SetToolTip(this.imgFleetComment, null);
            this.imgFleetComment.Visible = false;
            this.imgFleetComment.Click += new System.EventHandler(this.imgFleetComment_Click);
            // 
            // lblFleetNumber
            // 
            this.lblFleetNumber.Enabled = false;
            this.lblFleetNumber.Location = new System.Drawing.Point(20, 80);
            this.lblFleetNumber.Name = "lblFleetNumber";
            this.lblFleetNumber.Size = new System.Drawing.Size(256, 30);
            this.lblFleetNumber.TabIndex = 125;
            this.lblFleetNumber.Text = "ENTER FLEET NUMBER  (ENTER \'A\' TO ADD/SEARCH)";
            this.ToolTip1.SetToolTip(this.lblFleetNumber, null);
            // 
            // lblFleetName
            // 
            this.lblFleetName.Enabled = false;
            this.lblFleetName.Location = new System.Drawing.Point(20, 184);
            this.lblFleetName.Name = "lblFleetName";
            this.lblFleetName.Size = new System.Drawing.Size(80, 15);
            this.lblFleetName.TabIndex = 124;
            this.lblFleetName.Text = "FLEET NAME";
            this.ToolTip1.SetToolTip(this.lblFleetName, null);
            // 
            // txtNetWeight
            // 
            this.txtNetWeight.BackColor = System.Drawing.SystemColors.Window;
            this.txtNetWeight.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtNetWeight.Location = new System.Drawing.Point(460, 184);
            this.txtNetWeight.MaxLength = 6;
            this.txtNetWeight.Name = "txtNetWeight";
            this.txtNetWeight.Size = new System.Drawing.Size(81, 40);
            this.txtNetWeight.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtNetWeight, null);
            this.txtNetWeight.Enter += new System.EventHandler(this.txtNetWeight_Enter);
            this.txtNetWeight.Leave += new System.EventHandler(this.txtNetWeight_Leave);
            // 
            // cmdMoreInfo
            // 
            this.cmdMoreInfo.AppearanceKey = "actionButton";
            this.cmdMoreInfo.Location = new System.Drawing.Point(425, 511);
            this.cmdMoreInfo.Name = "cmdMoreInfo";
            this.cmdMoreInfo.Size = new System.Drawing.Size(221, 40);
            this.cmdMoreInfo.TabIndex = 32;
            this.cmdMoreInfo.TabStop = false;
            this.cmdMoreInfo.Text = "Additional Information";
            this.ToolTip1.SetToolTip(this.cmdMoreInfo, "If additional information needs to be entered but not appear on the registration");
            this.cmdMoreInfo.Click += new System.EventHandler(this.cmdMoreInfo_Click);
            // 
            // txtMake
            // 
            this.txtMake.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMake.Location = new System.Drawing.Point(20, 115);
            this.txtMake.MaxLength = 4;
            this.txtMake.Name = "txtMake";
            this.txtMake.Size = new System.Drawing.Size(50, 40);
            this.txtMake.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtMake, null);
            this.txtMake.Enter += new System.EventHandler(this.txtMake_Enter);
            this.txtMake.Leave += new System.EventHandler(this.txtMake_Leave);
            // 
            // txtYear
            // 
            this.txtYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYear.Location = new System.Drawing.Point(150, 115);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(60, 40);
            this.txtYear.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtYear, null);
            this.txtYear.Enter += new System.EventHandler(this.txtYear_Enter);
            this.txtYear.Leave += new System.EventHandler(this.txtYear_Leave);
            this.txtYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYear_KeyPressEvent);
            // 
            // txtUnit
            // 
            this.txtUnit.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUnit.Location = new System.Drawing.Point(220, 115);
            this.txtUnit.MaxLength = 12;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(90, 40);
            this.txtUnit.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtUnit, null);
            this.txtUnit.Enter += new System.EventHandler(this.txtUnit_Enter);
            this.txtUnit.Leave += new System.EventHandler(this.txtUnit_Leave);
            // 
            // txtColor1
            // 
            this.txtColor1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor1.Location = new System.Drawing.Point(320, 115);
            this.txtColor1.MaxLength = 2;
            this.txtColor1.Name = "txtColor1";
            this.txtColor1.Size = new System.Drawing.Size(40, 40);
            this.txtColor1.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtColor1, null);
            this.txtColor1.Enter += new System.EventHandler(this.txtColor1_Enter);
            this.txtColor1.Leave += new System.EventHandler(this.txtColor1_Leave);
            // 
            // txtColor2
            // 
            this.txtColor2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor2.Location = new System.Drawing.Point(370, 115);
            this.txtColor2.MaxLength = 2;
            this.txtColor2.Name = "txtColor2";
            this.txtColor2.Size = new System.Drawing.Size(40, 40);
            this.txtColor2.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtColor2, null);
            this.txtColor2.Enter += new System.EventHandler(this.txtColor2_Enter);
            this.txtColor2.Leave += new System.EventHandler(this.txtColor2_Leave);
            // 
            // txtPlate
            // 
            this.txtPlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPlate.Location = new System.Drawing.Point(481, 115);
            this.txtPlate.MaxLength = 8;
            this.txtPlate.Name = "txtPlate";
            this.txtPlate.Size = new System.Drawing.Size(110, 40);
            this.txtPlate.TabIndex = 9;
            this.txtPlate.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtPlate, null);
            this.txtPlate.Enter += new System.EventHandler(this.txtPlate_Enter);
            this.txtPlate.Leave += new System.EventHandler(this.txtPlate_Leave);
            // 
            // txtStyle
            // 
            this.txtStyle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtStyle.Location = new System.Drawing.Point(20, 185);
            this.txtStyle.MaxLength = 2;
            this.txtStyle.Name = "txtStyle";
            this.txtStyle.Size = new System.Drawing.Size(50, 40);
            this.txtStyle.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtStyle, null);
            this.txtStyle.Enter += new System.EventHandler(this.txtStyle_Enter);
            this.txtStyle.Leave += new System.EventHandler(this.txtStyle_Leave);
            // 
            // txtVIN
            // 
            this.txtVIN.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtVIN.Location = new System.Drawing.Point(80, 185);
            this.txtVIN.MaxLength = 17;
            this.txtVIN.Name = "txtVIN";
            this.txtVIN.Size = new System.Drawing.Size(280, 40);
            this.txtVIN.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtVIN, null);
            this.txtVIN.Enter += new System.EventHandler(this.txtVIN_Enter);
            this.txtVIN.Leave += new System.EventHandler(this.txtVIN_Leave);
            this.txtVIN.Validating += new System.ComponentModel.CancelEventHandler(this.txtVIN_Validate);
            // 
            // txtTitle
            // 
            this.txtTitle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTitle.Location = new System.Drawing.Point(370, 185);
            this.txtTitle.MaxLength = 11;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(80, 40);
            this.txtTitle.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtTitle, null);
            this.txtTitle.Enter += new System.EventHandler(this.txtTitle_Enter);
            this.txtTitle.Leave += new System.EventHandler(this.txtTitle_Leave);
            // 
            // txtAddress1
            // 
            this.txtAddress1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAddress1.Location = new System.Drawing.Point(103, 335);
            this.txtAddress1.MaxLength = 75;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(304, 40);
            this.txtAddress1.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.txtAddress1, null);
            this.txtAddress1.Enter += new System.EventHandler(this.txtAddress1_Enter);
            this.txtAddress1.Leave += new System.EventHandler(this.txtAddress1_Leave);
            // 
            // txtCity
            // 
            this.txtCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(103, 411);
            this.txtCity.MaxLength = 30;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(163, 40);
            this.txtCity.TabIndex = 22;
            this.ToolTip1.SetToolTip(this.txtCity, null);
            this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
            this.txtCity.Leave += new System.EventHandler(this.txtCity_Leave);
            // 
            // txtZip
            // 
            this.txtZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtZip.Location = new System.Drawing.Point(346, 411);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(60, 40);
            this.txtZip.TabIndex = 24;
            this.ToolTip1.SetToolTip(this.txtZip, null);
            this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
            this.txtZip.Leave += new System.EventHandler(this.txtZip_Leave);
            // 
            // txtState
            // 
            this.txtState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(276, 411);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(60, 40);
            this.txtState.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.txtState, null);
            this.txtState.Enter += new System.EventHandler(this.txtState_Enter);
            this.txtState.Leave += new System.EventHandler(this.txtState_Leave);
            // 
            // txtLegalResCity
            // 
            this.txtLegalResCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalResCity.Location = new System.Drawing.Point(103, 511);
            this.txtLegalResCity.MaxLength = 30;
            this.txtLegalResCity.Name = "txtLegalResCity";
            this.txtLegalResCity.Size = new System.Drawing.Size(167, 40);
            this.txtLegalResCity.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.txtLegalResCity, null);
            this.txtLegalResCity.Enter += new System.EventHandler(this.txtLegalResCity_Enter);
            this.txtLegalResCity.Leave += new System.EventHandler(this.txtLegalResCity_Leave);
            // 
            // txtLegalResState
            // 
            this.txtLegalResState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalResState.Location = new System.Drawing.Point(280, 511);
            this.txtLegalResState.MaxLength = 2;
            this.txtLegalResState.Name = "txtLegalResState";
            this.txtLegalResState.Size = new System.Drawing.Size(56, 40);
            this.txtLegalResState.TabIndex = 27;
            this.ToolTip1.SetToolTip(this.txtLegalResState, null);
            this.txtLegalResState.Enter += new System.EventHandler(this.txtLegalResState_Enter);
            this.txtLegalResState.Leave += new System.EventHandler(this.txtLegalResState_Leave);
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(103, 561);
            this.txtRate.MaxLength = 9;
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(54, 22);
            this.txtRate.TabIndex = 29;
            this.txtRate.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtRate, null);
            this.txtRate.Enter += new System.EventHandler(this.txtRate_Enter);
            this.txtRate.Leave += new System.EventHandler(this.txtRate_Leave);
            // 
            // txtCredit
            // 
            this.txtCredit.Location = new System.Drawing.Point(254, 561);
            this.txtCredit.MaxLength = 9;
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.Size = new System.Drawing.Size(54, 22);
            this.txtCredit.TabIndex = 30;
            this.txtCredit.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtCredit, null);
            this.txtCredit.Enter += new System.EventHandler(this.txtCredit_Enter);
            this.txtCredit.Leave += new System.EventHandler(this.txtCredit_Leave);
            // 
            // txtFees
            // 
            this.txtFees.Location = new System.Drawing.Point(392, 561);
            this.txtFees.MaxLength = 9;
            this.txtFees.Name = "txtFees";
            this.txtFees.Size = new System.Drawing.Size(60, 22);
            this.txtFees.TabIndex = 31;
            this.txtFees.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtFees, null);
            this.txtFees.Enter += new System.EventHandler(this.txtFees_Enter);
            this.txtFees.Leave += new System.EventHandler(this.txtFees_Leave);
            // 
            // txtExpires
            // 
            this.txtExpires.Location = new System.Drawing.Point(552, 561);
            this.txtExpires.Mask = "##/##/####";
            this.txtExpires.MaxLength = 10;
            this.txtExpires.Name = "txtExpires";
            this.txtExpires.Size = new System.Drawing.Size(115, 22);
            this.txtExpires.TabIndex = 78;
            this.txtExpires.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtExpires, null);
            this.txtExpires.Enter += new System.EventHandler(this.txtExpires_Enter);
            this.txtExpires.Leave += new System.EventHandler(this.txtExpires_Leave);
            // 
            // txtLegalZip
            // 
            this.txtLegalZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalZip.Location = new System.Drawing.Point(346, 511);
            this.txtLegalZip.MaxLength = 5;
            this.txtLegalZip.Name = "txtLegalZip";
            this.txtLegalZip.Size = new System.Drawing.Size(60, 40);
            this.txtLegalZip.TabIndex = 28;
            this.ToolTip1.SetToolTip(this.txtLegalZip, null);
            this.txtLegalZip.Enter += new System.EventHandler(this.txtLegalZip_Enter);
            this.txtLegalZip.Leave += new System.EventHandler(this.txtLegalZip_Leave);
            // 
            // txtLegalResAddress
            // 
            this.txtLegalResAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalResAddress.Location = new System.Drawing.Point(103, 461);
            this.txtLegalResAddress.MaxLength = 30;
            this.txtLegalResAddress.Name = "txtLegalResAddress";
            this.txtLegalResAddress.Size = new System.Drawing.Size(303, 40);
            this.txtLegalResAddress.TabIndex = 25;
            this.txtLegalResAddress.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtLegalResAddress, null);
            this.txtLegalResAddress.Visible = false;
            this.txtLegalResAddress.Enter += new System.EventHandler(this.txtLegalResAddress_Enter);
            this.txtLegalResAddress.Leave += new System.EventHandler(this.txtLegalResAddress_Leave);
            // 
            // txtModel
            // 
            this.txtModel.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtModel.Location = new System.Drawing.Point(80, 115);
            this.txtModel.MaxLength = 6;
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(60, 40);
            this.txtModel.TabIndex = 133;
            this.txtModel.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtModel, null);
            this.txtModel.Enter += new System.EventHandler(this.txtModel_Enter);
            this.txtModel.Leave += new System.EventHandler(this.txtModel_Leave);
            // 
            // Label36
            // 
            this.Label36.BackColor = System.Drawing.SystemColors.Window;
            this.Label36.BorderStyle = 1;
            this.Label36.Location = new System.Drawing.Point(80, 93);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(50, 16);
            this.Label36.TabIndex = 132;
            this.Label36.Text = "MODEL";
            this.ToolTip1.SetToolTip(this.Label36, null);
            // 
            // lblRegistrant2
            // 
            this.lblRegistrant2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRegistrant2.Location = new System.Drawing.Point(273, 299);
            this.lblRegistrant2.Name = "lblRegistrant2";
            this.lblRegistrant2.Size = new System.Drawing.Size(167, 16);
            this.lblRegistrant2.TabIndex = 126;
            this.ToolTip1.SetToolTip(this.lblRegistrant2, null);
            // 
            // lblRegistrant1
            // 
            this.lblRegistrant1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRegistrant1.Location = new System.Drawing.Point(273, 249);
            this.lblRegistrant1.Name = "lblRegistrant1";
            this.lblRegistrant1.Size = new System.Drawing.Size(167, 16);
            this.lblRegistrant1.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.lblRegistrant1, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 20);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(110, 16);
            this.Label1.TabIndex = 109;
            this.Label1.Text = "STATE OF MAINE";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label2_0
            // 
            this.Label2_0.Location = new System.Drawing.Point(20, 46);
            this.Label2_0.Name = "Label2_0";
            this.Label2_0.Size = new System.Drawing.Size(240, 16);
            this.Label2_0.TabIndex = 108;
            this.Label2_0.Text = "LONG TERM SEMI-TRAILER REGISTRATION";
            this.ToolTip1.SetToolTip(this.Label2_0, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(275, 46);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(160, 16);
            this.Label4.TabIndex = 107;
            this.Label4.Text = "MAINE RE-REGISTRATION?";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Line18
            // 
            this.Line18.Location = new System.Drawing.Point(7110, 5111);
            this.Line18.Name = "Line18";
            this.Line18.Size = new System.Drawing.Size(1140, 1);
            this.ToolTip1.SetToolTip(this.Line18, null);
            this.Line18.X1 = 7110F;
            this.Line18.X2 = 8250F;
            this.Line18.Y1 = 5111F;
            this.Line18.Y2 = 5111F;
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(667, 511);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(40, 40);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.Image1.TabIndex = 135;
            this.ToolTip1.SetToolTip(this.Image1, null);
            this.Image1.Visible = false;
            this.Image1.Click += new System.EventHandler(this.Image1_Click);
            // 
            // Label12
            // 
            this.Label12.BackColor = System.Drawing.SystemColors.Window;
            this.Label12.BorderStyle = 1;
            this.Label12.Location = new System.Drawing.Point(20, 93);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(40, 16);
            this.Label12.TabIndex = 106;
            this.Label12.Text = "MAKE";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label5
            // 
            this.Label5.BackColor = System.Drawing.SystemColors.Window;
            this.Label5.BorderStyle = 1;
            this.Label5.Location = new System.Drawing.Point(150, 93);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(40, 16);
            this.Label5.TabIndex = 105;
            this.Label5.Text = "YEAR";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label6
            // 
            this.Label6.BackColor = System.Drawing.SystemColors.Window;
            this.Label6.BorderStyle = 1;
            this.Label6.Location = new System.Drawing.Point(220, 93);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(50, 16);
            this.Label6.TabIndex = 104;
            this.Label6.Text = "UNIT #";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label14
            // 
            this.Label14.BackColor = System.Drawing.SystemColors.Window;
            this.Label14.BorderStyle = 1;
            this.Label14.Location = new System.Drawing.Point(320, 93);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(90, 16);
            this.Label14.TabIndex = 103;
            this.Label14.Text = " COLOR";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.SystemColors.Window;
            this.Label9.BorderStyle = 1;
            this.Label9.Location = new System.Drawing.Point(20, 166);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(45, 16);
            this.Label9.TabIndex = 100;
            this.Label9.Text = "STYLE";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // Label10
            // 
            this.Label10.BackColor = System.Drawing.SystemColors.Window;
            this.Label10.BorderStyle = 1;
            this.Label10.Location = new System.Drawing.Point(91, 166);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(250, 16);
            this.Label10.TabIndex = 99;
            this.Label10.Text = "VEHICLE IDENTIFICATION NO. (SERIAL NO.)";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label11
            // 
            this.Label11.BackColor = System.Drawing.SystemColors.Window;
            this.Label11.BorderStyle = 1;
            this.Label11.Location = new System.Drawing.Point(370, 166);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(60, 16);
            this.Label11.TabIndex = 98;
            this.Label11.Text = "TITLE NO";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label13
            // 
            this.Label13.BackColor = System.Drawing.SystemColors.Window;
            this.Label13.BorderStyle = 1;
            this.Label13.Location = new System.Drawing.Point(460, 166);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(81, 16);
            this.Label13.TabIndex = 97;
            this.Label13.Text = "NET WEIGHT";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label22
            // 
            this.Label22.BackColor = System.Drawing.SystemColors.Window;
            this.Label22.BorderStyle = 1;
            this.Label22.Location = new System.Drawing.Point(346, 385);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(61, 16);
            this.Label22.TabIndex = 89;
            this.Label22.Text = "ZIP CODE";
            this.ToolTip1.SetToolTip(this.Label22, null);
            // 
            // Label8
            // 
            this.Label8.BackColor = System.Drawing.Color.Transparent;
            this.Label8.Location = new System.Drawing.Point(427, 129);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(35, 16);
            this.Label8.TabIndex = 101;
            this.Label8.Text = "TLR";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label16
            // 
            this.Label16.BackColor = System.Drawing.Color.Transparent;
            this.Label16.Location = new System.Drawing.Point(20, 249);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(56, 16);
            this.Label16.TabIndex = 95;
            this.Label16.Text = "NAME(S)";
            this.ToolTip1.SetToolTip(this.Label16, null);
            // 
            // Label19
            // 
            this.Label19.BackColor = System.Drawing.Color.Transparent;
            this.Label19.Location = new System.Drawing.Point(20, 349);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(60, 30);
            this.Label19.TabIndex = 92;
            this.Label19.Text = "MAILING ADDRESS";
            this.ToolTip1.SetToolTip(this.Label19, null);
            // 
            // Label23
            // 
            this.Label23.BackColor = System.Drawing.Color.Transparent;
            this.Label23.Location = new System.Drawing.Point(20, 465);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(66, 31);
            this.Label23.TabIndex = 88;
            this.Label23.Text = "LEGAL RESIDENCE";
            this.ToolTip1.SetToolTip(this.Label23, null);
            // 
            // Label27
            // 
            this.Label27.BackColor = System.Drawing.Color.Transparent;
            this.Label27.Location = new System.Drawing.Point(20, 575);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(50, 16);
            this.Label27.TabIndex = 85;
            this.Label27.Text = "RATE";
            this.ToolTip1.SetToolTip(this.Label27, null);
            this.Label27.Click += new System.EventHandler(this.Label27_Click);
            // 
            // Label29
            // 
            this.Label29.BackColor = System.Drawing.Color.Transparent;
            this.Label29.Location = new System.Drawing.Point(177, 575);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(50, 16);
            this.Label29.TabIndex = 83;
            this.Label29.Text = "CREDIT";
            this.ToolTip1.SetToolTip(this.Label29, null);
            this.Label29.Click += new System.EventHandler(this.Label29_Click);
            // 
            // Label31
            // 
            this.Label31.BackColor = System.Drawing.Color.Transparent;
            this.Label31.Location = new System.Drawing.Point(328, 575);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(40, 16);
            this.Label31.TabIndex = 81;
            this.Label31.Text = "FEES";
            this.ToolTip1.SetToolTip(this.Label31, null);
            this.Label31.Click += new System.EventHandler(this.Label31_Click);
            // 
            // Label32
            // 
            this.Label32.BackColor = System.Drawing.Color.Transparent;
            this.Label32.Location = new System.Drawing.Point(471, 575);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(56, 16);
            this.Label32.TabIndex = 80;
            this.Label32.Text = "EXPIRES";
            this.ToolTip1.SetToolTip(this.Label32, null);
            // 
            // cboLength
            // 
            this.cboLength.BackColor = System.Drawing.SystemColors.Window;
            this.cboLength.Enabled = false;
            this.cboLength.Items.AddRange(new object[] {
            "2 Years",
            "3 Years",
            "4 Years",
            "5 Years",
            "6 Years",
            "7 Years",
            "8 Years",
            "9 Years",
            "10 Years",
            "11 Years",
            "12 Years"});
            this.cboLength.Location = new System.Drawing.Point(198, 55);
            this.cboLength.Name = "cboLength";
            this.cboLength.Size = new System.Drawing.Size(142, 40);
            this.ToolTip1.SetToolTip(this.cboLength, null);
            this.cboLength.SelectedIndexChanged += new System.EventHandler(this.cboLength_SelectedIndexChanged);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 69);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(150, 16);
            this.Label3.TabIndex = 112;
            this.Label3.Text = "REGISTRATION LENGTH";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // lblTotalDue
            // 
            this.lblTotalDue.AutoSize = true;
            this.lblTotalDue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.lblTotalDue.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblTotalDue.Location = new System.Drawing.Point(30, 30);
            this.lblTotalDue.Name = "lblTotalDue";
            this.lblTotalDue.Size = new System.Drawing.Size(147, 17);
            this.lblTotalDue.TabIndex = 111;
            this.lblTotalDue.Text = "TOTAL AMOUNT DUE";
            this.ToolTip1.SetToolTip(this.lblTotalDue, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuCTA,
            this.mnuUseTax,
            this.mnuFileCalculate,
            this.mnuFleetGroupComment,
            this.mnuSeperator1,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuCTA
            // 
            this.mnuCTA.Index = 0;
            this.mnuCTA.Name = "mnuCTA";
            this.mnuCTA.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuCTA.Text = "CTA";
            this.mnuCTA.Click += new System.EventHandler(this.mnuCTA_Click);
            // 
            // mnuUseTax
            // 
            this.mnuUseTax.Index = 1;
            this.mnuUseTax.Name = "mnuUseTax";
            this.mnuUseTax.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuUseTax.Text = "Use Tax (Sales Tax)";
            this.mnuUseTax.Click += new System.EventHandler(this.mnuUseTax_Click);
            // 
            // mnuFileCalculate
            // 
            this.mnuFileCalculate.Index = 2;
            this.mnuFileCalculate.Name = "mnuFileCalculate";
            this.mnuFileCalculate.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuFileCalculate.Text = "Calculate";
            this.mnuFileCalculate.Click += new System.EventHandler(this.mnuFileCalculate_Click);
            // 
            // mnuFleetGroupComment
            // 
            this.mnuFleetGroupComment.Enabled = false;
            this.mnuFleetGroupComment.Index = 3;
            this.mnuFleetGroupComment.Name = "mnuFleetGroupComment";
            this.mnuFleetGroupComment.Text = "Fleet / Group Comment";
            this.mnuFleetGroupComment.Click += new System.EventHandler(this.mnuFleetGroupComment_Click);
            // 
            // mnuSeperator1
            // 
            this.mnuSeperator1.Index = 4;
            this.mnuSeperator1.Name = "mnuSeperator1";
            this.mnuSeperator1.Text = "-";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 5;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Continue";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 6;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 7;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdCTA
            // 
            this.cmdCTA.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCTA.Location = new System.Drawing.Point(390, 29);
            this.cmdCTA.Name = "cmdCTA";
            this.cmdCTA.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdCTA.Size = new System.Drawing.Size(44, 24);
            this.cmdCTA.TabIndex = 1;
            this.cmdCTA.Text = "CTA";
            this.cmdCTA.Click += new System.EventHandler(this.mnuCTA_Click);
            // 
            // cmdUseTax
            // 
            this.cmdUseTax.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdUseTax.Location = new System.Drawing.Point(440, 29);
            this.cmdUseTax.Name = "cmdUseTax";
            this.cmdUseTax.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdUseTax.Size = new System.Drawing.Size(133, 24);
            this.cmdUseTax.TabIndex = 2;
            this.cmdUseTax.Text = "Use Tax (Sales Tax)";
            this.cmdUseTax.Click += new System.EventHandler(this.mnuUseTax_Click);
            // 
            // cmdCalculate
            // 
            this.cmdCalculate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCalculate.Location = new System.Drawing.Point(579, 29);
            this.cmdCalculate.Name = "cmdCalculate";
            this.cmdCalculate.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdCalculate.Size = new System.Drawing.Size(68, 24);
            this.cmdCalculate.TabIndex = 3;
            this.cmdCalculate.Text = "Calculate";
            this.cmdCalculate.Click += new System.EventHandler(this.mnuFileCalculate_Click);
            // 
            // cmdGroupComment
            // 
            this.cmdGroupComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdGroupComment.Enabled = false;
            this.cmdGroupComment.Location = new System.Drawing.Point(653, 29);
            this.cmdGroupComment.Name = "cmdGroupComment";
            this.cmdGroupComment.Size = new System.Drawing.Size(166, 24);
            this.cmdGroupComment.TabIndex = 4;
            this.cmdGroupComment.Text = "Fleet / Group Comment";
            this.cmdGroupComment.Click += new System.EventHandler(this.mnuFleetGroupComment_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(338, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(174, 48);
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmDataInputLongTermTrailers
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(852, 708);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmDataInputLongTermTrailers";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Data Input";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmDataInputLongTermTrailers_Load);
            this.Activated += new System.EventHandler(this.frmDataInputLongTermTrailers_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDataInputLongTermTrailers_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmDataInputLongTermTrailers_KeyUp);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).EndInit();
            this.fraAdditionalInfo.ResumeLayout(false);
            this.fraAdditionalInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).EndInit();
            this.fraUserFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdErase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFeesInfo)).EndInit();
            this.fraFeesInfo.ResumeLayout(false);
            this.fraFeesInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEEAgentFeeReg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEESalesTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEETitleFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEETotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEECreditTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEERegFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCTA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEETransferFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFeeAgentFeeCorrection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFEERushTitleFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTimePayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk25YearPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDataInput)).EndInit();
            this.fraDataInput.ResumeLayout(false);
            this.fraDataInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg2Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg2Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg1Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReg1Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFleet)).EndInit();
            this.fraFleet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCTA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUseTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGroupComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdGroupComment;
		private FCButton cmdCalculate;
		private FCButton cmdUseTax;
		private FCButton cmdCTA;
		private FCButton cmdSave;
	}
}
