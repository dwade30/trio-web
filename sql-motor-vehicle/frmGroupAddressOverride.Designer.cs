//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmGroupAddressOverride.
	/// </summary>
	partial class frmGroupAddressOverride
	{
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCountry;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCountry = new fecherFoundation.FCTextBox();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 440);
            this.BottomPanel.Size = new System.Drawing.Size(532, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtAddress2);
            this.ClientArea.Controls.Add(this.txtAddress);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCountry);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Size = new System.Drawing.Size(532, 380);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(532, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(279, 30);
            this.HeaderText.Text = "Group Address Override";
            // 
            // txtAddress2
            // 
            this.txtAddress2.AutoSize = false;
            this.txtAddress2.CharacterCasing = CharacterCasing.Upper;
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.LinkItem = null;
            this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress2.LinkTopic = null;
            this.txtAddress2.Location = new System.Drawing.Point(163, 90);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(341, 40);
            this.txtAddress2.TabIndex = 10;
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.CharacterCasing = CharacterCasing.Upper;
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.LinkItem = null;
            this.txtAddress.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress.LinkTopic = null;
            this.txtAddress.Location = new System.Drawing.Point(163, 30);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(341, 40);
            this.txtAddress.TabIndex = 0;
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.CharacterCasing = CharacterCasing.Upper;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(163, 150);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(341, 40);
            this.txtCity.TabIndex = 1;
            // 
            // txtZip
            // 
			this.txtZip.MaxLength = 15;
            this.txtZip.AutoSize = false;
            this.txtZip.CharacterCasing = CharacterCasing.Upper;
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.LinkItem = null;
            this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(163, 270);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(341, 40);
            this.txtZip.TabIndex = 3;
            // 
            // txtState
            // 
			this.txtState.MaxLength = 2;
            this.txtState.AutoSize = false;
            this.txtState.CharacterCasing = CharacterCasing.Upper;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(163, 210);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(341, 40);
            this.txtState.TabIndex = 2;
            // 
            // txtCountry
            // 
			this.txtCountry.MaxLength = 2;
            this.txtCountry.AutoSize = false;
            this.txtCountry.CharacterCasing = CharacterCasing.Upper;
            this.txtCountry.BackColor = System.Drawing.SystemColors.Window;
            this.txtCountry.LinkItem = null;
            this.txtCountry.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCountry.LinkTopic = null;
            this.txtCountry.Location = new System.Drawing.Point(163, 330);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(341, 40);
            this.txtCountry.TabIndex = 4;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 104);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(80, 15);
            this.Label6.TabIndex = 11;
            this.Label6.Text = "ADDRESS 2";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 344);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(63, 15);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "COUNTRY";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(63, 15);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "ADDRESS";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 164);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(63, 15);
            this.Label5.TabIndex = 7;
            this.Label5.Text = "CITY";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 224);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(63, 15);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "STATE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 284);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(63, 15);
            this.Label2.TabIndex = 5;
            this.Label2.Text = "ZIP";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(207, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(102, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Process";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmGroupAddressOverride
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(532, 548);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmGroupAddressOverride";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Group Address Override";
            this.Load += new System.EventHandler(this.frmGroupAddressOverride_Load);
            this.Activated += new System.EventHandler(this.frmGroupAddressOverride_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGroupAddressOverride_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
