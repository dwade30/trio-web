﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWMV0000
{
	public partial class frmTempPlate : BaseForm
	{
		public frmTempPlate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTempPlate InstancePtr
		{
			get
			{
				return (frmTempPlate)Sys.GetInstance(typeof(frmTempPlate));
			}
		}

		protected frmTempPlate _InstancePtr = null;
		//=========================================================
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.TempPlateNumber = 0;
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if (Information.IsNumeric(txtTempNumber.Text))
			{
				MotorVehicle.Statics.TempPlateNumber = FCConvert.ToInt32(FCConvert.ToDouble(txtTempNumber.Text));
				Close();
			}
			else
			{
				// Bp = Beep(1000, 250)
				txtTempNumber.Text = "";
				txtTempNumber.Focus();
			}
		}

		private void frmTempPlate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTempPlate properties;
			//frmTempPlate.ScaleWidth	= 4005;
			//frmTempPlate.ScaleHeight	= 2685;
			//frmTempPlate.LinkTopic	= "Form1";
			//End Unmaped Properties
			frmTempPlate.InstancePtr.LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal - frmTempPlate.InstancePtr.WidthOriginal) / 2.0);
			frmTempPlate.InstancePtr.TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal - frmTempPlate.InstancePtr.HeightOriginal) / 2.0);
			modGlobalFunctions.SetTRIOColors(this, false);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}
	}
}
