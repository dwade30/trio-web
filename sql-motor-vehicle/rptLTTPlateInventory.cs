//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTPlateInventory.
	/// </summary>
	public partial class rptLTTPlateInventory : BaseSectionReport
	{
		public rptLTTPlateInventory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Agent Inventory";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptLTTPlateInventory_ReportEnd;
		}

        private void RptLTTPlateInventory_ReportEnd(object sender, EventArgs e)
        {
			rs.DisposeOf();
            rsP.DisposeOf();
            rsQ.DisposeOf();
            rsR.DisposeOf();
            rsS.DisposeOf();
            rsT.DisposeOf();
            rsU.DisposeOf();
            rsPlatesOnHandAtStart.DisposeOf();
            rsPlatesReceived.DisposeOf();
            rsPlatesIssued.DisposeOf();
            rsPlatesVoided.DisposeOf();

		}

        public static rptLTTPlateInventory InstancePtr
		{
			get
			{
				return (rptLTTPlateInventory)Sys.GetInstance(typeof(rptLTTPlateInventory));
			}
		}

		protected rptLTTPlateInventory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLTTPlateInventory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngSOP;
		int lngAR;
		int lngVI;
		int lngAA;
		int lngEOP;
		int lngEndPK;
		int lng1;
		int lng2;
		int lngPK;
		int lngPK1;
		int lngPK2;
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL;
		//clsDRWrapper rs1 = new clsDRWrapper();
		clsDRWrapper rsP = new clsDRWrapper();
		clsDRWrapper rsQ = new clsDRWrapper();
		// Amount On Hand At Start
		clsDRWrapper rsR = new clsDRWrapper();
		// Amount Received
		clsDRWrapper rsS = new clsDRWrapper();
		// Amount Issued
		clsDRWrapper rsT = new clsDRWrapper();
		// Amount Adjusted
		clsDRWrapper rsU = new clsDRWrapper();
		// Amount On Hand At End
		int lngQ;
		int lngR;
		int lngS;
		int lngT;
		int lngU;
		//
		string strSSS = "";
		string strBMVCode = "";
		bool boolFirst;
		bool boolItem;
		string strTC = "";
		string strMM = "";
		int intPageNumber;
		bool blnFirstRecord;
		clsDRWrapper rsPlatesOnHandAtStart = new clsDRWrapper();
		clsDRWrapper rsPlatesReceived = new clsDRWrapper();
		clsDRWrapper rsPlatesIssued = new clsDRWrapper();
		clsDRWrapper rsPlatesVoided = new clsDRWrapper();

		private void strPlateReconciliation()
		{
			TM_XX_Tag:
			;
			lngQ = 0;
			lngR = 0;
			lngS = 0;
			lngT = 0;
			lngU = 0;
			rsPlatesOnHandAtStart.OpenRecordset("SELECT * FROM CloseoutInventory WHERE PeriodCloseoutKey = " + FCConvert.ToString(lngPK) + " AND InventoryType = 'PXSLT' ORDER BY substring(InventoryType,4,2), Low");
			rsQ.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE substring(Type,1,1) = 'P' And substring(Type,4,2) = 'LT' And PeriodCloseoutID = " + FCConvert.ToString(lngPK));
			if (rsQ.EndOfFile() != true && rsQ.BeginningOfFile() != true)
			{
				rsQ.MoveLast();
				rsQ.MoveFirst();
				while (!rsQ.EndOfFile())
				{
					lngQ += rsQ.Get_Fields_Int32("Number");
					rsQ.MoveNext();
				}
			}
			rsPlatesReceived.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = 'LT' And AdjustmentCode = 'R' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			rsR.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = 'LT' And AdjustmentCode = 'R' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsR.EndOfFile() != true && rsR.BeginningOfFile() != true)
			{
				rsR.MoveLast();
				rsR.MoveFirst();
				while (!rsR.EndOfFile())
				{
					lngR += rsR.Get_Fields_Int32("QuantityAdjusted");
					rsR.MoveNext();
				}
			}
			rsPlatesIssued.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = 'LT' And AdjustmentCode = 'I' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			rsS.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = 'LT' And AdjustmentCode = 'I' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsS.EndOfFile() != true && rsS.BeginningOfFile() != true)
			{
				rsS.MoveLast();
				rsS.MoveFirst();
				while (!rsS.EndOfFile())
				{
					lngS += rsS.Get_Fields_Int32("QuantityAdjusted");
					rsS.MoveNext();
				}
			}
			rsPlatesVoided.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = 'LT' And AdjustmentCode = 'A' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			rsT.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = 'LT' And AdjustmentCode = 'A' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsT.EndOfFile() != true && rsT.BeginningOfFile() != true)
			{
				rsT.MoveLast();
				rsT.MoveFirst();
				while (!rsT.EndOfFile())
				{
					lngT += rsT.Get_Fields_Int32("QuantityAdjusted");
					rsT.MoveNext();
				}
			}
			rsU.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE substring(Type,1,1) = 'P' And substring(Type,4,2) = 'LT' And PeriodCloseoutID = " + FCConvert.ToString(lngEndPK));
			if (rsU.EndOfFile() != true && rsU.BeginningOfFile() != true)
			{
				rsU.MoveLast();
				rsU.MoveFirst();
				while (!rsU.EndOfFile())
				{
					lngU += rsU.Get_Fields_Int32("Number");
					rsU.MoveNext();
				}
			}
			// 
			txtOnHand.Text = Strings.Format(lngQ, "@@@@@@@@");
			txtRecorded.Text = Strings.Format(lngR, "@@@@@@@@@@");
			txtIssues.Text = Strings.Format(lngS, "@@@@@@@@@@");
			txtAdjusted.Text = Strings.Format(lngT, "@@@@@@@@@@");
			txtEnd.Text = Strings.Format(lngU, "@@@@@@@@@@");
			if (lngQ + lngR - lngS - lngT != lngU)
			{
				txtEnd.Text = txtEnd + "  **";
			}
			lngSOP += lngQ;
			lngAR += lngR;
			lngVI += lngS;
			lngAA += lngT;
			lngEOP += lngU;
			rsP.MoveNext();
			txtItem.Text = "ITEMIZED LISTING----------------";
		}
		// Private Sub strItem()
		// boolItem = True
		// With rs
		// strTC = Mid$(.Fields("InventoryType"), 1, 1)
		// If strTC <> "P" Then GoTo NextTag4
		// If .EndOfFile <> True Then
		// If strTC <> "P" Then GoTo GotOne5
		// strMM = Mid$(.Fields("InventoryType"), 4, 2)
		// txtItem1 = Format(strMM & " " & .Fields("Low"), "@@@@@@@@@@@@")
		// txtItem1 = txtItem1 & "  -  "
		// txtItem1 = txtItem1 & Format(.Fields("High"), "!@@@@@@@@")
		// .MoveNext
		// End If
		//
		// If .EndOfFile <> True Then
		// strMM = Mid$(.Fields("InventoryType"), 4, 2)
		// strTC = Mid$(.Fields("InventoryType"), 1, 1)
		// If strTC <> "P" Then GoTo GotOne5
		// txtItem2 = Format(strMM & " " & .Fields("Low"), "@@@@@@@@@@@@")
		// txtItem2 = txtItem2 & "  -  "
		// txtItem2 = txtItem2 & Format(.Fields("High"), "!@@@@@@@@")
		// .MoveNext
		// End If
		//
		// If .EndOfFile <> True Then
		// strTC = Mid$(.Fields("InventoryType"), 1, 1)
		// strMM = Mid$(.Fields("InventoryType"), 4, 2)
		// If strTC <> "P" Then GoTo GotOne5
		// txtItem3 = Format(strMM & " " & .Fields("Low"), "@@@@@@@@@@@@")
		// txtItem3 = txtItem3 & "  -  "
		// txtItem3 = txtItem3 & Format(.Fields("High"), "!@@@@@@@@")
		// End If
		// GotOne5:
		// NextTag4:
		// If .EndOfFile <> True Then .MoveNext
		//
		// End With
		//
		// End Sub
		private void strItem()
		{
			if (rsPlatesOnHandAtStart.EndOfFile() != true)
			{
				txtPlatesOnHandAtStart.Text = Strings.Format(rsPlatesOnHandAtStart.Get_Fields("Low"), "@@@@@@@@@@@@");
				txtPlatesOnHandAtStart.Text = txtPlatesOnHandAtStart + "  -  ";
				txtPlatesOnHandAtStart.Text = txtPlatesOnHandAtStart + Strings.Format(rsPlatesOnHandAtStart.Get_Fields("High"), "!@@@@@@@@");
				rsPlatesOnHandAtStart.MoveNext();
			}
			else
			{
				txtPlatesOnHandAtStart.Text = "";
			}
			if (rsPlatesReceived.EndOfFile() != true)
			{
				txtPlatesReceived.Text = GetInventoryPlateRange(ref rsPlatesReceived);
			}
			else
			{
				txtPlatesReceived.Text = "";
			}
			if (rsPlatesIssued.EndOfFile() != true)
			{
				txtPlatesIssued.Text = GetInventoryPlateRange(ref rsPlatesIssued);
			}
			else
			{
				txtPlatesIssued.Text = "";
			}
			if (rsPlatesVoided.EndOfFile() != true)
			{
				txtPlatesVoided.Text = GetInventoryPlateRange(ref rsPlatesVoided);
			}
			else
			{
				txtPlatesVoided.Text = "";
			}
			if (rs.EndOfFile() != true)
			{
				txtPlatesOnHandAtEnd.Text = Strings.Format(rs.Get_Fields("Low"), "@@@@@@@@@@@@");
				txtPlatesOnHandAtEnd.Text = txtPlatesOnHandAtEnd + "  -  ";
				txtPlatesOnHandAtEnd.Text = txtPlatesOnHandAtEnd + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");
				rs.MoveNext();
			}
			else
			{
				txtPlatesOnHandAtEnd.Text = "";
			}
		}

		public string GetInventoryPlateRange(ref clsDRWrapper rs)
		{
			string GetInventoryPlateRange = "";
			string strLow;
			string strHigh;
			strLow = rs.Get_Fields("Low");
			strHigh = rs.Get_Fields("High");
			rs.MoveNext();
			GetInventoryPlateRange = strLow + " - " + strHigh;
			return GetInventoryPlateRange;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			txtOnHand.Text = "";
			txtRecorded.Text = "";
			txtIssues.Text = "";
			txtAdjusted.Text = "";
			txtEnd.Text = "";
			txtPlatesOnHandAtStart.Text = "";
			txtPlatesReceived.Text = "";
			txtPlatesIssued.Text = "";
			txtPlatesVoided.Text = "";
			txtPlatesOnHandAtEnd.Text = "";
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
				strPlateReconciliation();
			}
			else if (!rs.EndOfFile())
			{
				txtOnHand.Text = "";
				txtRecorded.Text = "";
				txtIssues.Text = "";
				txtAdjusted.Text = "";
				txtEnd.Text = "";
				txtItem.Text = "";
				eArgs.EOF = false;
				strItem();
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			string strLevel = "";
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			txtDescTitle.Text = "Plates---------------------";
			strSQL = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate BETWEEN '" + frmReportLongTerm.InstancePtr.cboStart.Text + "' AND '" + frmReportLongTerm.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngPK = lngPK1;
				lng1 = lngPK1;
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngEndPK = lngPK2;
				lng2 = lngPK2;
			}
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				strSQL = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID < 1 AND InventoryType = 'PXSLT' ORDER BY substring(InventoryType,4,2), Low";
			}
			else
			{
				strSQL = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " AND InventoryType = 'PXSLT' ORDER BY substring(InventoryType,4,2), Low";
			}
			rs.OpenRecordset(strSQL);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
			}
			blnFirstRecord = true;
			lngSOP = 0;
			lngAR = 0;
			lngVI = 0;
			lngAA = 0;
			lngEOP = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (txtItem.Text != "")
			{
				txtItem.Top += 180 / 1440F;
				txtPlatesOnHandAtEnd.Top += 180 / 1440F;
				txtPlatesOnHandAtStart.Top += 180 / 1440F;
				txtPlatesReceived.Top += 180 / 1440F;
				txtPlatesIssued.Top += 180 / 1440F;
				txtPlatesVoided.Top += 180 / 1440F;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeadingLongTerm();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void rptLTTPlateInventory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLTTPlateInventory properties;
			//rptLTTPlateInventory.Caption	= "Agent Inventory";
			//rptLTTPlateInventory.Left	= 0;
			//rptLTTPlateInventory.Top	= 0;
			//rptLTTPlateInventory.Width	= 20280;
			//rptLTTPlateInventory.Height	= 11115;
			//rptLTTPlateInventory.StartUpPosition	= 3;
			//rptLTTPlateInventory.SectionData	= "rptLTTPlateInventory.dsx":0000;
			//End Unmaped Properties
		}
	}
}
