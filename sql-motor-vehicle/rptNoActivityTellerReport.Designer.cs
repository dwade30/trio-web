﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptNoActivityTellerReport.
	/// </summary>
	partial class rptNoActivityTellerReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNoActivityTellerReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTellerName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMVR3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalMoney = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAgentFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocalTransfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRegFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBooster = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDupReg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSpecialty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInitial = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStateTransfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTransit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSpecialReg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTellerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgentFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBooster)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDupReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpecialty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInitial)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpecialReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldClass,
				this.fldPlate,
				this.fldYear,
				this.fldMake,
				this.fldModel,
				this.fldLocal,
				this.fldState,
				this.fldTotal,
				this.fldMVR3,
				this.fldType
			});
			this.Detail.Height = 0.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label5,
				this.Label1,
				this.Label32,
				this.Label9,
				this.Label10,
				this.Label33
			});
			this.PageHeader.Height = 0.6979167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.field2
			});
			this.GroupHeader2.DataField = "Binder2";
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.KeepTogether = true;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			//
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label18,
				this.Label19,
				this.Line3,
				this.Label20,
				this.Label21,
				this.Label22,
				this.fldAgentFee,
				this.fldExcise,
				this.fldLocalTransfer,
				this.Label23,
				this.Line4,
				this.Label24,
				this.Label25,
				this.Label26,
				this.fldRegFee,
				this.fldBooster,
				this.fldDupReg,
				this.Label27,
				this.Label28,
				this.Label29,
				this.fldSpecialty,
				this.fldInitial,
				this.fldStateTransfer,
				this.Label30,
				this.Label31,
				this.fldTransit,
				this.fldSpecialReg
			});
			this.GroupFooter2.Height = 2.75F;
			this.GroupFooter2.KeepTogether = true;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label15,
				this.lblTellerName,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Line1,
				this.Label16,
				this.Field1
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.53125F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label17,
				this.fldTotalUnits,
				this.fldTotalMoney,
				this.Line2,
				this.fldTotalLocal,
				this.fldTotalState
			});
			this.GroupFooter1.Height = 0.5625F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label5.Text = "Label5";
			this.Label5.Top = 0.375F;
			this.Label5.Width = 6.5F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Teller Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 6.5F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label32.Text = "Label32";
			this.Label32.Top = 0.1875F;
			this.Label32.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 5.1875F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label33.Text = "Label33";
			this.Label33.Top = 0F;
			this.Label33.Width = 1.3125F;
			// 
			// field2
			// 
			this.field2.DataField = "Binder2";
			this.field2.Height = 0.125F;
			this.field2.Left = 1.125F;
			this.field2.Name = "field2";
			this.field2.Style = "font-size: 8pt; ddo-char-set: 1";
			this.field2.Text = "Binder2";
			this.field2.Top = 0.0625F;
			this.field2.Visible = false;
			this.field2.Width = 0.6875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.0625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label15.Text = "TELLER";
			this.Label15.Top = 0F;
			this.Label15.Width = 0.75F;
			// 
			// lblTellerName
			// 
			this.lblTellerName.Height = 0.1875F;
			this.lblTellerName.HyperLink = null;
			this.lblTellerName.Left = 0.8125F;
			this.lblTellerName.Name = "lblTellerName";
			this.lblTellerName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.lblTellerName.Text = null;
			this.lblTellerName.Top = 0F;
			this.lblTellerName.Width = 2.25F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label6.Text = "CLASS";
			this.Label6.Top = 0.3125F;
			this.Label6.Width = 0.5625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.6875F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "PLATE";
			this.Label7.Top = 0.3125F;
			this.Label7.Width = 0.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.4375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "VEHICLE-----------";
			this.Label8.Top = 0.3125F;
			this.Label8.Width = 1.5625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label11.Text = "LOCAL";
			this.Label11.Top = 0.3125F;
			this.Label11.Width = 0.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.0625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label12.Text = "STATE";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 0.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.9375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label13.Text = "TOTAL";
			this.Label13.Top = 0.3125F;
			this.Label13.Width = 0.5F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 1.375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label14.Text = "MVR3";
			this.Label14.Top = 0.3125F;
			this.Label14.Width = 0.4375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 6.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.875F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label16.Text = "TYPE";
			this.Label16.Top = 0.3125F;
			this.Label16.Width = 0.5F;
			// 
			// Field1
			// 
			this.Field1.DataField = "Binder";
			this.Field1.Height = 0.125F;
			this.Field1.Left = 3.6875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 8pt; ddo-char-set: 1";
			this.Field1.Text = "Binder";
			this.Field1.Top = 0.0625F;
			this.Field1.Visible = false;
			this.Field1.Width = 0.5625F;
			// 
			// fldClass
			// 
			this.fldClass.Height = 0.1875F;
			this.fldClass.Left = 0.0625F;
			this.fldClass.Name = "fldClass";
			this.fldClass.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldClass.Text = "Field1";
			this.fldClass.Top = 0F;
			this.fldClass.Width = 0.4375F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 0.5625F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPlate.Text = "Field1";
			this.fldPlate.Top = 0F;
			this.fldPlate.Width = 0.6875F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 2.4375F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.375F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.1875F;
			this.fldMake.Left = 2.9375F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 0F;
			this.fldMake.Width = 0.375F;
			// 
			// fldModel
			// 
			this.fldModel.Height = 0.1875F;
			this.fldModel.Left = 3.375F;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldModel.Text = "Field1";
			this.fldModel.Top = 0F;
			this.fldModel.Width = 0.5625F;
			// 
			// fldLocal
			// 
			this.fldLocal.Height = 0.1875F;
			this.fldLocal.Left = 4.0625F;
			this.fldLocal.Name = "fldLocal";
			this.fldLocal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldLocal.Text = "Field1";
			this.fldLocal.Top = 0F;
			this.fldLocal.Width = 0.75F;
			// 
			// fldState
			// 
			this.fldState.Height = 0.1875F;
			this.fldState.Left = 4.875F;
			this.fldState.Name = "fldState";
			this.fldState.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldState.Text = "Field1";
			this.fldState.Top = 0F;
			this.fldState.Width = 0.75F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 5.625F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.8125F;
			// 
			// fldMVR3
			// 
			this.fldMVR3.Height = 0.1875F;
			this.fldMVR3.Left = 1.25F;
			this.fldMVR3.Name = "fldMVR3";
			this.fldMVR3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldMVR3.Text = "Field1";
			this.fldMVR3.Top = 0F;
			this.fldMVR3.Width = 0.625F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 1.9375F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldType.Text = "Field1";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.5F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.25F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 11pt; ddo-char-set: 1";
			this.Label17.Text = "Registrations Completed";
			this.Label17.Top = 0.0625F;
			this.Label17.Width = 2.5F;
			// 
			// fldTotalUnits
			// 
			this.fldTotalUnits.Height = 0.1875F;
			this.fldTotalUnits.Left = 2.75F;
			this.fldTotalUnits.Name = "fldTotalUnits";
			this.fldTotalUnits.Style = "font-family: \'Tahoma\'; font-size: 11pt; ddo-char-set: 1";
			this.fldTotalUnits.Text = "Field1";
			this.fldTotalUnits.Top = 0.0625F;
			this.fldTotalUnits.Width = 0.625F;
			// 
			// fldTotalMoney
			// 
			this.fldTotalMoney.Height = 0.1875F;
			this.fldTotalMoney.Left = 5.625F;
			this.fldTotalMoney.Name = "fldTotalMoney";
			this.fldTotalMoney.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldTotalMoney.Text = "Field1";
			this.fldTotalMoney.Top = 0.125F;
			this.fldTotalMoney.Width = 0.8125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.9375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.0625F;
			this.Line2.Width = 2.5F;
			this.Line2.X1 = 3.9375F;
			this.Line2.X2 = 6.4375F;
			this.Line2.Y1 = 0.0625F;
			this.Line2.Y2 = 0.0625F;
			// 
			// fldTotalLocal
			// 
			this.fldTotalLocal.Height = 0.1875F;
			this.fldTotalLocal.Left = 4F;
			this.fldTotalLocal.Name = "fldTotalLocal";
			this.fldTotalLocal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldTotalLocal.Text = "Field2";
			this.fldTotalLocal.Top = 0.125F;
			this.fldTotalLocal.Width = 0.8125F;
			// 
			// fldTotalState
			// 
			this.fldTotalState.Height = 0.1875F;
			this.fldTotalState.Left = 4.8125F;
			this.fldTotalState.Name = "fldTotalState";
			this.fldTotalState.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldTotalState.Text = "Field3";
			this.fldTotalState.Top = 0.125F;
			this.fldTotalState.Width = 0.8125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.25F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 1";
			this.Label18.Text = "Summary";
			this.Label18.Top = 0.125F;
			this.Label18.Width = 6.5F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 11pt; text-align: center; ddo-char-set: 1";
			this.Label19.Text = "Local Fees";
			this.Label19.Top = 0.5625F;
			this.Label19.Width = 6.5F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.0625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.75F;
			this.Line3.Width = 6.375F;
			this.Line3.X1 = 0.0625F;
			this.Line3.X2 = 6.4375F;
			this.Line3.Y1 = 0.75F;
			this.Line3.Y2 = 0.75F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.75F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label20.Text = "Excise Tax";
			this.Label20.Top = 0.8125F;
			this.Label20.Width = 1F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1.5625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label21.Text = "Agent Fee";
			this.Label21.Top = 0.8125F;
			this.Label21.Width = 1F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.9375F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label22.Text = "Transfer Fee";
			this.Label22.Top = 0.8125F;
			this.Label22.Width = 1F;
			// 
			// fldAgentFee
			// 
			this.fldAgentFee.Height = 0.1875F;
			this.fldAgentFee.Left = 1.5625F;
			this.fldAgentFee.Name = "fldAgentFee";
			this.fldAgentFee.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldAgentFee.Text = "Field2";
			this.fldAgentFee.Top = 1F;
			this.fldAgentFee.Width = 1F;
			// 
			// fldExcise
			// 
			this.fldExcise.Height = 0.1875F;
			this.fldExcise.Left = 2.75F;
			this.fldExcise.Name = "fldExcise";
			this.fldExcise.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldExcise.Text = "Field2";
			this.fldExcise.Top = 1F;
			this.fldExcise.Width = 1F;
			// 
			// fldLocalTransfer
			// 
			this.fldLocalTransfer.Height = 0.1875F;
			this.fldLocalTransfer.Left = 3.9375F;
			this.fldLocalTransfer.Name = "fldLocalTransfer";
			this.fldLocalTransfer.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldLocalTransfer.Text = "Field2";
			this.fldLocalTransfer.Top = 1F;
			this.fldLocalTransfer.Width = 1F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 11pt; text-align: center; ddo-char-set: 1";
			this.Label23.Text = "State Fees";
			this.Label23.Top = 1.375F;
			this.Label23.Width = 6.5F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.5625F;
			this.Line4.Width = 6.375F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 6.375F;
			this.Line4.Y1 = 1.5625F;
			this.Line4.Y2 = 1.5625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.125F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label24.Text = "Booster";
			this.Label24.Top = 1.625F;
			this.Label24.Width = 1F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.0625F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label25.Text = "Reg Fee";
			this.Label25.Top = 1.625F;
			this.Label25.Width = 1F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 2.1875F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label26.Text = "Dup Reg";
			this.Label26.Top = 1.625F;
			this.Label26.Width = 1F;
			// 
			// fldRegFee
			// 
			this.fldRegFee.Height = 0.1875F;
			this.fldRegFee.Left = 0.0625F;
			this.fldRegFee.Name = "fldRegFee";
			this.fldRegFee.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldRegFee.Text = "Field2";
			this.fldRegFee.Top = 1.8125F;
			this.fldRegFee.Width = 1F;
			// 
			// fldBooster
			// 
			this.fldBooster.Height = 0.1875F;
			this.fldBooster.Left = 1.125F;
			this.fldBooster.Name = "fldBooster";
			this.fldBooster.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldBooster.Text = "Field2";
			this.fldBooster.Top = 1.8125F;
			this.fldBooster.Width = 1F;
			// 
			// fldDupReg
			// 
			this.fldDupReg.Height = 0.1875F;
			this.fldDupReg.Left = 2.1875F;
			this.fldDupReg.Name = "fldDupReg";
			this.fldDupReg.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldDupReg.Text = "Field2";
			this.fldDupReg.Top = 1.8125F;
			this.fldDupReg.Width = 1F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 4.3125F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label27.Text = "Vanity Plate";
			this.Label27.Top = 1.625F;
			this.Label27.Width = 1F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 3.25F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label28.Text = "Spec Plate";
			this.Label28.Top = 1.625F;
			this.Label28.Width = 1F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label29.Text = "Transfer Fee";
			this.Label29.Top = 1.625F;
			this.Label29.Width = 1F;
			// 
			// fldSpecialty
			// 
			this.fldSpecialty.Height = 0.1875F;
			this.fldSpecialty.Left = 3.25F;
			this.fldSpecialty.Name = "fldSpecialty";
			this.fldSpecialty.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldSpecialty.Text = "Field2";
			this.fldSpecialty.Top = 1.8125F;
			this.fldSpecialty.Width = 1F;
			// 
			// fldInitial
			// 
			this.fldInitial.Height = 0.1875F;
			this.fldInitial.Left = 4.3125F;
			this.fldInitial.Name = "fldInitial";
			this.fldInitial.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldInitial.Text = "Field2";
			this.fldInitial.Top = 1.8125F;
			this.fldInitial.Width = 1F;
			// 
			// fldStateTransfer
			// 
			this.fldStateTransfer.Height = 0.1875F;
			this.fldStateTransfer.Left = 5.375F;
			this.fldStateTransfer.Name = "fldStateTransfer";
			this.fldStateTransfer.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldStateTransfer.Text = "Field2";
			this.fldStateTransfer.Top = 1.8125F;
			this.fldStateTransfer.Width = 1F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 3.25F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label30.Text = "Spec Reg Permit";
			this.Label30.Top = 2.125F;
			this.Label30.Width = 1.375F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 1.875F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label31.Text = "Transit Plates";
			this.Label31.Top = 2.125F;
			this.Label31.Width = 1.3125F;
			// 
			// fldTransit
			// 
			this.fldTransit.Height = 0.1875F;
			this.fldTransit.Left = 2.0625F;
			this.fldTransit.Name = "fldTransit";
			this.fldTransit.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldTransit.Text = "Field2";
			this.fldTransit.Top = 2.3125F;
			this.fldTransit.Width = 1F;
			// 
			// fldSpecialReg
			// 
			this.fldSpecialReg.Height = 0.1875F;
			this.fldSpecialReg.Left = 3.4375F;
			this.fldSpecialReg.Name = "fldSpecialReg";
			this.fldSpecialReg.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.fldSpecialReg.Text = "Field2";
			this.fldSpecialReg.Top = 2.3125F;
			this.fldSpecialReg.Width = 1F;
			// 
			// rptNoActivityTellerReport
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.9166667F;
			this.PageSettings.Margins.Right = 0.9166667F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTellerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAgentFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocalTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBooster)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDupReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpecialty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInitial)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStateTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpecialReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMVR3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox field2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAgentFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExcise;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocalTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBooster;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDupReg;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpecialty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInitial;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpecialReg;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTellerName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMoney;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalState;
	}
}
