﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rpt2000SwintecLeaseUseTaxCertificate.
	/// </summary>
	partial class rpt2000SwintecLeaseUseTaxCertificate
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt2000SwintecLeaseUseTaxCertificate));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldMake1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVin1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSellerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSellerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDateofTransfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAllowance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUseTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserSocialSec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherReason = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLeasePayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNumberOfPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDownPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldMake1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateofTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAllowance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserSocialSec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherReason)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLeasePayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDownPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMake1,
				this.fldModel1,
				this.fldYear1,
				this.fldVin1,
				this.fldSellerName,
				this.fldSellerAddress,
				this.fldDateofTransfer,
				this.fldTotalAmount,
				this.fldAllowance,
				this.fldExemptTypeA,
				this.fldExemptNumber,
				this.fldUseTax,
				this.fldPurchaserName,
				this.fldExemptTypeC,
				this.fldExemptTypeB,
				this.fldExemptTypeD,
				this.fldPurchaserSocialSec,
				this.fldPurchaserZip,
				this.fldRegDate,
				this.fldTaxAmount,
				this.fldPurchaserAddress,
				this.fldPurchaserCity,
				this.fldPurchaserState,
				this.fldOtherReason,
				this.fldClassPlate,
				this.fldLeasePayment,
				this.fldNumberOfPayments,
				this.fldPaymentTotal,
				this.fldDownPayment
			});
			this.Detail.Height = 10.5F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldMake1
			// 
			this.fldMake1.CanGrow = false;
			this.fldMake1.Height = 0.1875F;
			this.fldMake1.Left = 0.96875F;
			this.fldMake1.MultiLine = false;
			this.fldMake1.Name = "fldMake1";
			this.fldMake1.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldMake1.Text = "MAKE";
			this.fldMake1.Top = 2.03125F;
			this.fldMake1.Width = 1.03125F;
			// 
			// fldModel1
			// 
			this.fldModel1.CanGrow = false;
			this.fldModel1.Height = 0.1875F;
			this.fldModel1.Left = 2.09375F;
			this.fldModel1.MultiLine = false;
			this.fldModel1.Name = "fldModel1";
			this.fldModel1.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldModel1.Text = "MODEL";
			this.fldModel1.Top = 2.03125F;
			this.fldModel1.Width = 0.8125F;
			// 
			// fldYear1
			// 
			this.fldYear1.CanGrow = false;
			this.fldYear1.Height = 0.1875F;
			this.fldYear1.Left = 3.25F;
			this.fldYear1.MultiLine = false;
			this.fldYear1.Name = "fldYear1";
			this.fldYear1.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldYear1.Text = "YEAR";
			this.fldYear1.Top = 2.03125F;
			this.fldYear1.Width = 0.6875F;
			// 
			// fldVin1
			// 
			this.fldVin1.CanGrow = false;
			this.fldVin1.Height = 0.1875F;
			this.fldVin1.Left = 4.15625F;
			this.fldVin1.MultiLine = false;
			this.fldVin1.Name = "fldVin1";
			this.fldVin1.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldVin1.Text = "VEHICLE VIN";
			this.fldVin1.Top = 2.03125F;
			this.fldVin1.Width = 2.125F;
			// 
			// fldSellerName
			// 
			this.fldSellerName.CanGrow = false;
			this.fldSellerName.Height = 0.1875F;
			this.fldSellerName.Left = 2.03125F;
			this.fldSellerName.MultiLine = false;
			this.fldSellerName.Name = "fldSellerName";
			this.fldSellerName.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldSellerName.Text = "LESSOR NAME";
			this.fldSellerName.Top = 1.03125F;
			this.fldSellerName.Width = 2.5F;
			// 
			// fldSellerAddress
			// 
			this.fldSellerAddress.CanGrow = false;
			this.fldSellerAddress.Height = 0.1875F;
			this.fldSellerAddress.Left = 2.375F;
			this.fldSellerAddress.MultiLine = false;
			this.fldSellerAddress.Name = "fldSellerAddress";
			this.fldSellerAddress.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldSellerAddress.Text = "LESSOR ADDRESS";
			this.fldSellerAddress.Top = 1.34375F;
			this.fldSellerAddress.Width = 5.1875F;
			// 
			// fldDateofTransfer
			// 
			this.fldDateofTransfer.CanGrow = false;
			this.fldDateofTransfer.Height = 0.1875F;
			this.fldDateofTransfer.Left = 6.75F;
			this.fldDateofTransfer.MultiLine = false;
			this.fldDateofTransfer.Name = "fldDateofTransfer";
			this.fldDateofTransfer.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldDateofTransfer.Text = "LESS DATE";
			this.fldDateofTransfer.Top = 1.03125F;
			this.fldDateofTransfer.Width = 1F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.CanGrow = false;
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 7.03125F;
			this.fldTotalAmount.MultiLine = false;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldTotalAmount.Text = "TOTAL";
			this.fldTotalAmount.Top = 4.9375F;
			this.fldTotalAmount.Width = 1.09375F;
			// 
			// fldAllowance
			// 
			this.fldAllowance.CanGrow = false;
			this.fldAllowance.Height = 0.1875F;
			this.fldAllowance.Left = 7.03125F;
			this.fldAllowance.MultiLine = false;
			this.fldAllowance.Name = "fldAllowance";
			this.fldAllowance.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldAllowance.Text = "ALLOWANCE";
			this.fldAllowance.Top = 4.625F;
			this.fldAllowance.Width = 1.09375F;
			// 
			// fldExemptTypeA
			// 
			this.fldExemptTypeA.CanGrow = false;
			this.fldExemptTypeA.Height = 0.1875F;
			this.fldExemptTypeA.Left = 0.6875F;
			this.fldExemptTypeA.MultiLine = false;
			this.fldExemptTypeA.Name = "fldExemptTypeA";
			this.fldExemptTypeA.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldExemptTypeA.Text = "X";
			this.fldExemptTypeA.Top = 6.59375F;
			this.fldExemptTypeA.Width = 0.25F;
			// 
			// fldExemptNumber
			// 
			this.fldExemptNumber.CanGrow = false;
			this.fldExemptNumber.Height = 0.1875F;
			this.fldExemptNumber.Left = 2.9375F;
			this.fldExemptNumber.MultiLine = false;
			this.fldExemptNumber.Name = "fldExemptNumber";
			this.fldExemptNumber.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldExemptNumber.Text = "87364";
			this.fldExemptNumber.Top = 6.75F;
			this.fldExemptNumber.Width = 0.875F;
			// 
			// fldUseTax
			// 
			this.fldUseTax.CanGrow = false;
			this.fldUseTax.Height = 0.1875F;
			this.fldUseTax.Left = 7.03125F;
			this.fldUseTax.MultiLine = false;
			this.fldUseTax.Name = "fldUseTax";
			this.fldUseTax.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldUseTax.Text = "USE TAX";
			this.fldUseTax.Top = 5.25F;
			this.fldUseTax.Width = 1.09375F;
			// 
			// fldPurchaserName
			// 
			this.fldPurchaserName.CanGrow = false;
			this.fldPurchaserName.Height = 0.1875F;
			this.fldPurchaserName.Left = 0.90625F;
			this.fldPurchaserName.MultiLine = false;
			this.fldPurchaserName.Name = "fldPurchaserName";
			this.fldPurchaserName.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserName.Text = "LESSEE NAME";
			this.fldPurchaserName.Top = 8.78125F;
			this.fldPurchaserName.Width = 2.25F;
			// 
			// fldExemptTypeC
			// 
			this.fldExemptTypeC.CanGrow = false;
			this.fldExemptTypeC.Height = 0.1875F;
			this.fldExemptTypeC.Left = 0.6875F;
			this.fldExemptTypeC.MultiLine = false;
			this.fldExemptTypeC.Name = "fldExemptTypeC";
			this.fldExemptTypeC.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldExemptTypeC.Text = "X";
			this.fldExemptTypeC.Top = 7.25F;
			this.fldExemptTypeC.Width = 0.25F;
			// 
			// fldExemptTypeB
			// 
			this.fldExemptTypeB.CanGrow = false;
			this.fldExemptTypeB.Height = 0.1875F;
			this.fldExemptTypeB.Left = 0.6875F;
			this.fldExemptTypeB.MultiLine = false;
			this.fldExemptTypeB.Name = "fldExemptTypeB";
			this.fldExemptTypeB.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldExemptTypeB.Text = "X";
			this.fldExemptTypeB.Top = 6.9375F;
			this.fldExemptTypeB.Width = 0.25F;
			// 
			// fldExemptTypeD
			// 
			this.fldExemptTypeD.CanGrow = false;
			this.fldExemptTypeD.Height = 0.1875F;
			this.fldExemptTypeD.Left = 0.6875F;
			this.fldExemptTypeD.MultiLine = false;
			this.fldExemptTypeD.Name = "fldExemptTypeD";
			this.fldExemptTypeD.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldExemptTypeD.Text = "X";
			this.fldExemptTypeD.Top = 7.5625F;
			this.fldExemptTypeD.Width = 0.25F;
			// 
			// fldPurchaserSocialSec
			// 
			this.fldPurchaserSocialSec.CanGrow = false;
			this.fldPurchaserSocialSec.Height = 0.1875F;
			this.fldPurchaserSocialSec.Left = 3.6875F;
			this.fldPurchaserSocialSec.MultiLine = false;
			this.fldPurchaserSocialSec.Name = "fldPurchaserSocialSec";
			this.fldPurchaserSocialSec.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserSocialSec.Text = "SOC SEC NUMBER";
			this.fldPurchaserSocialSec.Top = 8.78125F;
			this.fldPurchaserSocialSec.Width = 1.75F;
			// 
			// fldPurchaserZip
			// 
			this.fldPurchaserZip.CanGrow = false;
			this.fldPurchaserZip.Height = 0.1875F;
			this.fldPurchaserZip.Left = 6.53125F;
			this.fldPurchaserZip.MultiLine = false;
			this.fldPurchaserZip.Name = "fldPurchaserZip";
			this.fldPurchaserZip.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserZip.Text = "09827-2098";
			this.fldPurchaserZip.Top = 9.3125F;
			this.fldPurchaserZip.Width = 0.875F;
			// 
			// fldRegDate
			// 
			this.fldRegDate.CanGrow = false;
			this.fldRegDate.Height = 0.1875F;
			this.fldRegDate.Left = 3.875F;
			this.fldRegDate.MultiLine = false;
			this.fldRegDate.Name = "fldRegDate";
			this.fldRegDate.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldRegDate.Text = "08/31/2004";
			this.fldRegDate.Top = 10.3125F;
			this.fldRegDate.Width = 1.5625F;
			// 
			// fldTaxAmount
			// 
			this.fldTaxAmount.CanGrow = false;
			this.fldTaxAmount.Height = 0.1875F;
			this.fldTaxAmount.Left = 6.375F;
			this.fldTaxAmount.MultiLine = false;
			this.fldTaxAmount.Name = "fldTaxAmount";
			this.fldTaxAmount.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldTaxAmount.Text = "TAX PAID";
			this.fldTaxAmount.Top = 10.3125F;
			this.fldTaxAmount.Width = 1.5F;
			// 
			// fldPurchaserAddress
			// 
			this.fldPurchaserAddress.CanGrow = false;
			this.fldPurchaserAddress.Height = 0.1875F;
			this.fldPurchaserAddress.Left = 0.90625F;
			this.fldPurchaserAddress.MultiLine = false;
			this.fldPurchaserAddress.Name = "fldPurchaserAddress";
			this.fldPurchaserAddress.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserAddress.Text = "LESEE ADDRESS";
			this.fldPurchaserAddress.Top = 9.3125F;
			this.fldPurchaserAddress.Width = 2F;
			// 
			// fldPurchaserCity
			// 
			this.fldPurchaserCity.CanGrow = false;
			this.fldPurchaserCity.Height = 0.1875F;
			this.fldPurchaserCity.Left = 3.78125F;
			this.fldPurchaserCity.MultiLine = false;
			this.fldPurchaserCity.Name = "fldPurchaserCity";
			this.fldPurchaserCity.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserCity.Text = "CITY";
			this.fldPurchaserCity.Top = 9.3125F;
			this.fldPurchaserCity.Width = 1.4375F;
			// 
			// fldPurchaserState
			// 
			this.fldPurchaserState.CanGrow = false;
			this.fldPurchaserState.Height = 0.1875F;
			this.fldPurchaserState.Left = 5.78125F;
			this.fldPurchaserState.MultiLine = false;
			this.fldPurchaserState.Name = "fldPurchaserState";
			this.fldPurchaserState.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserState.Text = "ME";
			this.fldPurchaserState.Top = 9.3125F;
			this.fldPurchaserState.Width = 0.625F;
			// 
			// fldOtherReason
			// 
			this.fldOtherReason.CanGrow = false;
			this.fldOtherReason.Height = 0.1875F;
			this.fldOtherReason.Left = 3.1875F;
			this.fldOtherReason.MultiLine = false;
			this.fldOtherReason.Name = "fldOtherReason";
			this.fldOtherReason.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldOtherReason.Text = null;
			this.fldOtherReason.Top = 7.5625F;
			this.fldOtherReason.Width = 3.0625F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.CanGrow = false;
			this.fldClassPlate.Height = 0.1875F;
			this.fldClassPlate.Left = 1.53125F;
			this.fldClassPlate.MultiLine = false;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldClassPlate.Text = "1234RR PC";
			this.fldClassPlate.Top = 10.3125F;
			this.fldClassPlate.Width = 1.5625F;
			// 
			// fldLeasePayment
			// 
			this.fldLeasePayment.CanGrow = false;
			this.fldLeasePayment.Height = 0.1875F;
			this.fldLeasePayment.Left = 3.03125F;
			this.fldLeasePayment.MultiLine = false;
			this.fldLeasePayment.Name = "fldLeasePayment";
			this.fldLeasePayment.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldLeasePayment.Text = "PAYMENT";
			this.fldLeasePayment.Top = 3F;
			this.fldLeasePayment.Width = 1.09375F;
			// 
			// fldNumberOfPayments
			// 
			this.fldNumberOfPayments.CanGrow = false;
			this.fldNumberOfPayments.Height = 0.1875F;
			this.fldNumberOfPayments.Left = 5.78125F;
			this.fldNumberOfPayments.MultiLine = false;
			this.fldNumberOfPayments.Name = "fldNumberOfPayments";
			this.fldNumberOfPayments.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldNumberOfPayments.Text = "#OF";
			this.fldNumberOfPayments.Top = 3F;
			this.fldNumberOfPayments.Width = 0.53125F;
			// 
			// fldPaymentTotal
			// 
			this.fldPaymentTotal.CanGrow = false;
			this.fldPaymentTotal.Height = 0.1875F;
			this.fldPaymentTotal.Left = 7.03125F;
			this.fldPaymentTotal.MultiLine = false;
			this.fldPaymentTotal.Name = "fldPaymentTotal";
			this.fldPaymentTotal.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPaymentTotal.Text = "PAY TOT";
			this.fldPaymentTotal.Top = 3F;
			this.fldPaymentTotal.Width = 1.09375F;
			// 
			// fldDownPayment
			// 
			this.fldDownPayment.CanGrow = false;
			this.fldDownPayment.Height = 0.1875F;
			this.fldDownPayment.Left = 7.03125F;
			this.fldDownPayment.MultiLine = false;
			this.fldDownPayment.Name = "fldDownPayment";
			this.fldDownPayment.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldDownPayment.Text = "D PAYMNT";
			this.fldDownPayment.Top = 4.3125F;
			this.fldDownPayment.Width = 1.09375F;
			// 
			// rpt2000SwintecLeaseUseTaxCertificate
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldMake1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateofTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAllowance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserSocialSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherReason)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLeasePayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNumberOfPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDownPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVin1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSellerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSellerAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDateofTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAllowance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUseTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserSocialSec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherReason;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLeasePayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNumberOfPayments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDownPayment;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
