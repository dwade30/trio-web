//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubStickerException.
	/// </summary>
	public partial class rptSubStickerException : BaseSectionReport
	{
		public rptSubStickerException()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Exception Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptSubStickerException_ReportEnd;
		}

        private void RptSubStickerException_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
        }

        public static rptSubStickerException InstancePtr
		{
			get
			{
				return (rptSubStickerException)Sys.GetInstance(typeof(rptSubStickerException));
			}
		}

		protected rptSubStickerException _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubStickerException	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rs.EndOfFile();
			}
			else
			{
				rs.MoveNext();
				eArgs.EOF = rs.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND Type = '1S' ORDER BY ExceptionReportDate");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Type = '1S' ORDER BY ExceptionReportDate");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rptNewExceptionReport.InstancePtr.boolData = true;
			}
			else
			{
				this.Close();
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{

			string tempSticker = "";
			string strDS = "";
			
			fldClassPlate.Text = rs.Get_Fields_String("OriginalClass") + " " + rs.Get_Fields_String("OriginalPlate");
			fldIssued.Text = Strings.Format(rs.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yyyy");
			if (FCConvert.ToInt32(rs.Get_Fields_Int32("ReplacementNumberOfStickers")) == 2)
			{
				strDS = "D";
				rptNewExceptionReport.InstancePtr.StickerTotal += 2;
			}
			else
			{
				strDS = "S";
				rptNewExceptionReport.InstancePtr.StickerTotal += 1;
			}
			if (rs.Get_Fields_Int32("ReplacementMonthYear") > 99)
			{
				tempSticker = rs.Get_Fields_String("ReplacementMMYY") + Strings.Format(Strings.Right(fecherFoundation.Strings.Trim(rs.Get_Fields_Int32("ReplacementMonthYear").ToString()), 2), "00") + " " + rs.Get_Fields_Int32("ReplacementStickerNumber") + " " + strDS;
			}
			else
			{
				tempSticker = rs.Get_Fields_String("ReplacementMMYY") + Strings.Format(rs.Get_Fields_Int32("ReplacementMonthYear"), "00") + " " + rs.Get_Fields_Int32("ReplacementStickerNumber") + " " + strDS;
			}
			fldReplacement.Text = tempSticker;
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("reason")) == false)
			{
				if (FCConvert.ToString(rs.Get_Fields_String("reason")).Length <= 20)
				{
					fldExplanation.Text = rs.Get_Fields_String("reason") + Strings.StrDup(20 - FCConvert.ToString(rs.Get_Fields_String("reason")).Length, " ");
				}
				else
				{
					fldExplanation.Text = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 20);
				}
			}

			rptNewExceptionReport.InstancePtr.StickerMoneyTotal = rptNewExceptionReport.InstancePtr.StickerMoneyTotal + rs.Get_Fields("Fee");
			fldFee.Text = Strings.Format(rs.Get_Fields("Fee"), "#0.00");
			fldOpID.Text = rs.Get_Fields_String("OpID");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotal As object	OnWrite
			fldTotalCount.Text = FCConvert.ToString(rptNewExceptionReport.InstancePtr.StickerTotal);
			fldTotalMoney.Text = rptNewExceptionReport.InstancePtr.StickerMoneyTotal.ToString("#,##0.00");
		}
	}
}
