//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;

namespace TWMV0000
{
	public partial class frmTableMaintenance : BaseForm
	{
		public frmTableMaintenance()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            lblInstruct = new List<fecherFoundation.FCLabel>();
            lblInstruct.AddControlArrayElement(lblInstruct_0, 0);
            lblInstruct.AddControlArrayElement(lblInstruct_1, 1);
            lblInstruct.AddControlArrayElement(lblInstruct_2, 2);

            lblLevel = new List<fecherFoundation.FCLabel>();
            lblLevel.AddControlArrayElement(lblLevel_0, 0);
            lblLevel.AddControlArrayElement(lblLevel_1, 1);
            lblLevel.AddControlArrayElement(lblLevel_2, 2);
            lblLevel.AddControlArrayElement(lblLevel_3, 3);

			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTableMaintenance InstancePtr
		{
			get
			{
				return (frmTableMaintenance)Sys.GetInstance(typeof(frmTableMaintenance));
			}
		}

		protected frmTableMaintenance _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsResidence = new clsDRWrapper();
		clsDRWrapper rsColors = new clsDRWrapper();
		clsDRWrapper rsMake = new clsDRWrapper();
		clsDRWrapper rsClass = new clsDRWrapper();
		clsDRWrapper rsStyle = new clsDRWrapper();
		clsDRWrapper rsReasonCodes = new clsDRWrapper();
		clsDRWrapper rsPrintPriority = new clsDRWrapper();
		clsDRWrapper rsState = new clsDRWrapper();
		clsDRWrapper rsOperators = new clsDRWrapper();
		clsDRWrapper rsCounty = new clsDRWrapper();
		clsDRWrapper rsFleet = new clsDRWrapper();
		clsDRWrapper rsDefaultInfo = new clsDRWrapper();
		clsDRWrapper rsGrossVehicleWeight1 = new clsDRWrapper();
		clsDRWrapper rsGrossVehicleWeight2 = new clsDRWrapper();
		clsDRWrapper rsGrossVehicleWeight3 = new clsDRWrapper();
		// These Variables are used for deleting lines from tables
		int[] lngResidence = new int[99 + 1];
		int[] lngColors = new int[99 + 1];
		int[] lngMake = new int[99 + 1];
		int[] lngClass = new int[99 + 1];
		int[] lngStyle = new int[99 + 1];
		int[] lngReasonCodes = new int[99 + 1];
		int[] lngPrintPriority = new int[99 + 1];
		int[] lngState = new int[99 + 1];
		int[] lngOperators = new int[99 + 1];
		int[] lngCounty = new int[99 + 1];
		int[] lngFleet = new int[99 + 1];
		int[] lngGrossWeight1 = new int[99 + 1];
		int[] lngGrossWeight2 = new int[99 + 1];
		int[] lngGrossWeight3 = new int[99 + 1];
		// *********************************************
		bool ResidenceChanged;
		bool ColorChanged;
		bool MakeChanged;
		bool ClassChanged;
		bool StyleChanged;
		bool ReasonChanged;
		bool PrintChanged;
		bool StateChanged;
		bool OperatorsChanged;
		bool CountyChanged;
		bool DefaultInfoChanged;
		bool GrossWeightChanged1;
		bool GrossWeightChanged2;
		bool GrossWeightChanged3;
		bool FleetChanged;
		string HoldString;
		bool ResSort;
		bool StateSort;
		bool CountySort;
		// vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
		int fnx;
		string strSql;
		// vbPorter upgrade warning: ResColumn As int	OnWriteFCConvert.ToInt32(
		int ResColumn;
		// vbPorter upgrade warning: StateColumn As int	OnWriteFCConvert.ToInt32(
		int StateColumn;
		// vbPorter upgrade warning: CountyColumn As int	OnWriteFCConvert.ToInt32(
		int CountyColumn;
		bool CorrectSave;

		public void SaveData()
		{
			CorrectSave = true;
			for (fnx = 1; fnx <= (vsOperators.Rows - 1); fnx++)
			{
				if (vsOperators.TextMatrix(fnx, 2) != "" && vsOperators.TextMatrix(fnx, 2).Length != 3)
				{
					MessageBox.Show("All Operator ID's must have 3 characters.", "Invalid Operator ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CorrectSave = false;
					return;
				}
			}
			Delete_Items();
			if (ResidenceChanged == true)
			{
				for (fnx = 1; fnx <= (vsResCodes.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsResCodes.TextMatrix(fnx, 1)) != "")
					{
						rsResidence.FindFirstRecord("ID", Conversion.Val(vsResCodes.TextMatrix(fnx, 1)));
						if (rsResidence.NoMatch == false)
							rsResidence.Edit();
						else
							rsResidence.AddNew();
						rsResidence.Set_Fields("Code", vsResCodes.TextMatrix(fnx, 2));
						rsResidence.Set_Fields("Town", vsResCodes.TextMatrix(fnx, 3));
						rsResidence.Update();
					}
					else
					{
						if (fecherFoundation.Strings.Trim(vsResCodes.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsResCodes.TextMatrix(fnx, 3)) != "")
						{
							rsResidence.FindFirstRecord("Code", vsResCodes.TextMatrix(fnx, 2));
							if (rsResidence.NoMatch == false)
								rsResidence.Edit();
							else
								rsResidence.AddNew();
							rsResidence.Set_Fields("Code", vsResCodes.TextMatrix(fnx, 2));
							rsResidence.Set_Fields("Town", vsResCodes.TextMatrix(fnx, 3));
							rsResidence.Update();
						}
					}
				}
				// fnx
				ResidenceChanged = false;
			}
			// 
			if (StateChanged == true)
			{
				for (fnx = 1; fnx <= (vsState.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsState.TextMatrix(fnx, 1)) != "")
					{
						if (fecherFoundation.Strings.Trim(vsState.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsState.TextMatrix(fnx, 3)) != "")
						{
							rsState.FindFirstRecord("ID", Conversion.Val(vsState.TextMatrix(fnx, 1)));
							if (rsState.NoMatch == false)
								rsState.Edit();
							else
								rsState.AddNew();
							rsState.Set_Fields("Abrev", vsState.TextMatrix(fnx, 2));
							rsState.Set_Fields("State", vsState.TextMatrix(fnx, 3));
							rsState.Update();
						}
						else
						{
							rsState.FindFirstRecord("ID", Conversion.Val(vsState.TextMatrix(fnx, 1)));
							rsState.Delete();
							rsState.Update();
						}
					}
					else
					{
						if (fecherFoundation.Strings.Trim(vsState.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsState.TextMatrix(fnx, 3)) != "")
						{
							rsState.AddNew();
							rsState.Set_Fields("Abrev", vsState.TextMatrix(fnx, 2));
							rsState.Set_Fields("State", vsState.TextMatrix(fnx, 3));
							rsState.Update();
						}
					}
				}
				// fnx
				StateChanged = false;
			}
			// 
			if (ColorChanged == true)
			{
				for (fnx = 1; fnx <= (vsColor.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsColor.TextMatrix(fnx, 1)) != "")
					{
						rsColors.FindFirstRecord("ID", Conversion.Val(vsColor.TextMatrix(fnx, 1)));
						if (rsColors.NoMatch == false)
							rsColors.Edit();
						else
							rsColors.AddNew();
						rsColors.Set_Fields("Code", vsColor.TextMatrix(fnx, 2));
						rsColors.Set_Fields("Color", vsColor.TextMatrix(fnx, 3));
						rsColors.Update();
					}
				}
				// fnx
				ColorChanged = false;
			}
			// 
			if (MakeChanged == true)
			{
				for (fnx = 1; fnx <= (vsMake.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsMake.TextMatrix(fnx, 1)) != "")
					{
						rsMake.FindFirstRecord("ID", Conversion.Val(vsMake.TextMatrix(fnx, 1)));
						if (rsMake.NoMatch == false)
						{
							rsMake.Edit();
						}
						else
						{
							rsMake.AddNew();
						}
						rsMake.Set_Fields("Type", vsMake.TextMatrix(fnx, 2));
						rsMake.Set_Fields("Code", vsMake.TextMatrix(fnx, 3));
						rsMake.Set_Fields("make", vsMake.TextMatrix(fnx, 4));
						rsMake.Update();
					}
					else if (fecherFoundation.Strings.Trim(vsMake.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsMake.TextMatrix(fnx, 3)) != "" && fecherFoundation.Strings.Trim(vsMake.TextMatrix(fnx, 4)) != "")
					{
						rsMake.AddNew();
						rsMake.Set_Fields("Type", vsMake.TextMatrix(fnx, 2));
						rsMake.Set_Fields("Code", vsMake.TextMatrix(fnx, 3));
						rsMake.Set_Fields("make", vsMake.TextMatrix(fnx, 4));
						rsMake.Update();
					}
				}
				// fnx
				MakeChanged = false;
			}
			// 
			if (ClassChanged == true)
			{
				for (fnx = 1; fnx <= (vsClass.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsClass.TextMatrix(fnx, 2)) != "")
					{
						rsClass.FindFirstRecord2("SystemCode, BMVCode", vsClass.TextMatrix(fnx, 3) + "," + vsClass.TextMatrix(fnx, 2), ",");
						if (rsClass.NoMatch == false)
							rsClass.Edit();
						else
							rsClass.AddNew();
						rsClass.Set_Fields("SystemCode", vsClass.TextMatrix(fnx, 3));
						rsClass.Set_Fields("BMVCode", vsClass.TextMatrix(fnx, 2));
						rsClass.Set_Fields("Description", fecherFoundation.Strings.Trim(vsClass.TextMatrix(fnx, 4)));
						rsClass.Set_Fields("ExciseRequired", vsClass.TextMatrix(fnx, 5));
						rsClass.Set_Fields("RegistrationFee", FCConvert.ToString(Conversion.Val(vsClass.TextMatrix(fnx, 6))));
						rsClass.Set_Fields("OtherFeesNew", FCConvert.ToString(Conversion.Val(vsClass.TextMatrix(fnx, 7))));
						rsClass.Set_Fields("Otherfeesrenew", FCConvert.ToString(Conversion.Val(vsClass.TextMatrix(fnx, 8))));
						rsClass.Set_Fields("HalfRateAllowed", vsClass.TextMatrix(fnx, 9));
						rsClass.Set_Fields("TwoYear", vsClass.TextMatrix(fnx, 10));
						rsClass.Set_Fields("LocationNew", vsClass.TextMatrix(fnx, 11));
						rsClass.Set_Fields("LocationTransfer", vsClass.TextMatrix(fnx, 12));
						rsClass.Set_Fields("LocationRenewal", vsClass.TextMatrix(fnx, 13));
						rsClass.Set_Fields("NumberOfPlateStickers", FCConvert.ToString(Conversion.Val(vsClass.TextMatrix(fnx, 14))));
						rsClass.Set_Fields("renewaldate", vsClass.TextMatrix(fnx, 15));
						rsClass.Set_Fields("TitleRequired", vsClass.TextMatrix(fnx, 16));
						rsClass.Set_Fields("Emission", vsClass.TextMatrix(fnx, 17));
						rsClass.Set_Fields("InsuranceRequired", vsClass.TextMatrix(fnx, 18));
						rsClass.Update();
					}
				}
				// fnx
				ClassChanged = false;
			}
			// 
			if (StyleChanged == true)
			{
				for (fnx = 1; fnx <= (vsStyle.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsStyle.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsStyle.TextMatrix(fnx, 3)) != "" && fecherFoundation.Strings.Trim(vsStyle.TextMatrix(fnx, 4)) != "")
					{
						rsStyle.FindFirstRecord("ID", Conversion.Val(vsStyle.TextMatrix(fnx, 1)));
						if (rsStyle.NoMatch == false)
						{
							rsStyle.Edit();
						}
						else
						{
							rsStyle.AddNew();
						}
						rsStyle.Set_Fields("Type", vsStyle.TextMatrix(fnx, 2));
						rsStyle.Set_Fields("Code", vsStyle.TextMatrix(fnx, 3));
						rsStyle.Set_Fields("Style", vsStyle.TextMatrix(fnx, 4));
						rsStyle.Update();
					}
				}
				// fnx
				StyleChanged = false;
			}
			// 
			if (ReasonChanged == true)
			{
				for (fnx = 1; fnx <= (vsReasonCodes.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsReasonCodes.TextMatrix(fnx, 1)) != "")
					{
						rsReasonCodes.FindFirstRecord("ID", Conversion.Val(vsReasonCodes.TextMatrix(fnx, 1)));
						if (rsReasonCodes.NoMatch == false)
							rsReasonCodes.Edit();
						else
							rsReasonCodes.AddNew();
						rsReasonCodes.Set_Fields("Code", vsReasonCodes.TextMatrix(fnx, 2));
						rsReasonCodes.Set_Fields("Priority", vsReasonCodes.TextMatrix(fnx, 3));
						rsReasonCodes.Set_Fields("Description", vsReasonCodes.TextMatrix(fnx, 4));
						rsReasonCodes.Update();
					}
				}
				// fnx
				ReasonChanged = false;
			}
			// 
			if (PrintChanged == true)
			{
				for (fnx = 1; fnx <= (vsPrint.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsPrint.TextMatrix(fnx, 1)) != "")
					{
						rsPrintPriority.FindFirstRecord("ID", Conversion.Val(vsPrint.TextMatrix(fnx, 1)));
						if (rsPrintPriority.NoMatch == false)
							rsPrintPriority.Edit();
						else
							rsPrintPriority.AddNew();
						rsPrintPriority.Set_Fields("Priority", vsPrint.TextMatrix(fnx, 2));
						rsPrintPriority.Set_Fields("Description", vsPrint.TextMatrix(fnx, 3));
						rsPrintPriority.Update();
					}
				}
				// fnx
				PrintChanged = false;
			}
			// 
			if (OperatorsChanged == true)
			{
				if (rsOperators.RecordCount() > 0)
				{
					rsOperators.MoveLast();
					rsOperators.MoveFirst();
					do
					{
						rsOperators.Delete();
						rsOperators.Update();
						rsOperators.MoveNext();
					}
					while (rsOperators.EndOfFile() != true);
				}
				for (fnx = 1; fnx <= (vsOperators.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsOperators.TextMatrix(fnx, 2)) != "")
					{
						rsOperators.AddNew();
						rsOperators.Set_Fields("Code", vsOperators.TextMatrix(fnx, 2));
						rsOperators.Set_Fields("Name", vsOperators.TextMatrix(fnx, 3));
						rsOperators.Set_Fields("Level", vsOperators.TextMatrix(fnx, 4));
						rsOperators.Set_Fields("PlateGroup", FCConvert.ToString(Conversion.Val(vsOperators.TextMatrix(fnx, 5))));
						rsOperators.Set_Fields("StickerGroup", FCConvert.ToString(Conversion.Val(vsOperators.TextMatrix(fnx, 6))));
						rsOperators.Set_Fields("MVR3Group", FCConvert.ToString(Conversion.Val(vsOperators.TextMatrix(fnx, 7))));
						rsOperators.Update();
					}
				}
				// fnx
				OperatorsChanged = false;
			}
			// 
			if (CountyChanged == true)
			{
				for (fnx = 1; fnx <= (vsCounty.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsCounty.TextMatrix(fnx, 1)) != "")
					{
						rsCounty.FindFirstRecord("ID", Conversion.Val(vsCounty.TextMatrix(fnx, 1)));
						if (rsCounty.NoMatch == false)
							rsCounty.Edit();
						else
							rsCounty.AddNew();
						rsCounty.Set_Fields("Code", vsCounty.TextMatrix(fnx, 2));
						rsCounty.Set_Fields("Name", vsCounty.TextMatrix(fnx, 3));
						rsCounty.Update();
					}
				}
				// fnx
				CountyChanged = false;
			}
			// 
			if (GrossWeightChanged1 == true)
			{
				for (fnx = 1; fnx <= (vsGrossTable1.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsGrossTable1.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsGrossTable1.TextMatrix(fnx, 3)) != "" && fecherFoundation.Strings.Trim(vsGrossTable1.TextMatrix(fnx, 4)) != "")
					{
						rsGrossVehicleWeight1.FindFirstRecord("ID", Conversion.Val(vsGrossTable1.TextMatrix(fnx, 0)));
						if (rsGrossVehicleWeight1.NoMatch == false)
							rsGrossVehicleWeight1.Edit();
						else
							rsGrossVehicleWeight1.AddNew();
						rsGrossVehicleWeight1.Set_Fields("Type", "TK");
						rsGrossVehicleWeight1.Set_Fields("Low", vsGrossTable1.TextMatrix(fnx, 2));
						rsGrossVehicleWeight1.Set_Fields("High", vsGrossTable1.TextMatrix(fnx, 3));
						rsGrossVehicleWeight1.Set_Fields("Fee", vsGrossTable1.TextMatrix(fnx, 4));
						rsGrossVehicleWeight1.Update();
					}
				}
				// fnx
				GrossWeightChanged1 = false;
			}
			if (GrossWeightChanged2 == true)
			{
				for (fnx = 1; fnx <= (vsGrossTable2.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsGrossTable2.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsGrossTable2.TextMatrix(fnx, 3)) != "" && fecherFoundation.Strings.Trim(vsGrossTable2.TextMatrix(fnx, 4)) != "")
					{
						rsGrossVehicleWeight2.FindFirstRecord("ID", Conversion.Val(vsGrossTable2.TextMatrix(fnx, 0)));
						if (rsGrossVehicleWeight2.NoMatch == false)
							rsGrossVehicleWeight2.Edit();
						else
							rsGrossVehicleWeight2.AddNew();
						rsGrossVehicleWeight2.Set_Fields("Type", "FM");
						rsGrossVehicleWeight2.Set_Fields("Low", vsGrossTable2.TextMatrix(fnx, 2));
						rsGrossVehicleWeight2.Set_Fields("High", vsGrossTable2.TextMatrix(fnx, 3));
						rsGrossVehicleWeight2.Set_Fields("Fee", vsGrossTable2.TextMatrix(fnx, 4));
						rsGrossVehicleWeight2.Update();
					}
				}
				// fnx
				GrossWeightChanged2 = false;
			}
			if (GrossWeightChanged3 == true)
			{
				for (fnx = 1; fnx <= (vsGrossTable3.Rows - 1); fnx++)
				{
					if (fecherFoundation.Strings.Trim(vsGrossTable3.TextMatrix(fnx, 2)) != "" && fecherFoundation.Strings.Trim(vsGrossTable3.TextMatrix(fnx, 3)) != "" && fecherFoundation.Strings.Trim(vsGrossTable3.TextMatrix(fnx, 4)) != "")
					{
						rsGrossVehicleWeight3.FindFirstRecord("ID", Conversion.Val(vsGrossTable3.TextMatrix(fnx, 0)));
						if (rsGrossVehicleWeight3.NoMatch == false)
							rsGrossVehicleWeight3.Edit();
						else
							rsGrossVehicleWeight3.AddNew();
						rsGrossVehicleWeight3.Set_Fields("Type", "F2");
						rsGrossVehicleWeight3.Set_Fields("Low", vsGrossTable3.TextMatrix(fnx, 2));
						rsGrossVehicleWeight3.Set_Fields("High", vsGrossTable3.TextMatrix(fnx, 3));
						rsGrossVehicleWeight3.Set_Fields("Fee", vsGrossTable3.TextMatrix(fnx, 4));
						rsGrossVehicleWeight3.Update();
					}
				}
				// fnx
				GrossWeightChanged3 = false;
			}
			fnx = 3;
			if (DefaultInfoChanged == true)
			{
				if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
				{
					rsDefaultInfo.MoveFirst();
					rsDefaultInfo.Edit();
				}
				else
				{
					rsDefaultInfo.AddNew();
				}
				// DefaultInfo Table
				rsDefaultInfo.Set_Fields("ResidenceCode", vsDefaults.TextMatrix(1, fnx));
				rsDefaultInfo.Set_Fields("Town", vsDefaults.TextMatrix(2, fnx));
				rsDefaultInfo.Set_Fields("State", vsDefaults.TextMatrix(3, fnx));
				rsDefaultInfo.Set_Fields("Zip", vsDefaults.TextMatrix(4, fnx) + " ");
				rsDefaultInfo.Set_Fields("ReportTown", vsDefaults.TextMatrix(6, fnx));
				rsDefaultInfo.Set_Fields("ReportState", vsDefaults.TextMatrix(7, fnx));
				rsDefaultInfo.Set_Fields("ReportZip", vsDefaults.TextMatrix(8, fnx));
				rsDefaultInfo.Set_Fields("ReportAgent", vsDefaults.TextMatrix(9, fnx));
				rsDefaultInfo.Set_Fields("ReportTelephone", vsDefaults.TextMatrix(10, fnx));
				rsDefaultInfo.Set_Fields("ExciseVSAgent", fecherFoundation.Strings.UCase(vsDefaults.TextMatrix(5, fnx)));
				rsDefaultInfo.Set_Fields("UserLabel1", vsDefaults.TextMatrix(11, fnx) + " ");
				rsDefaultInfo.Set_Fields("UserLabel2", vsDefaults.TextMatrix(12, fnx) + " ");
				rsDefaultInfo.Set_Fields("UserLabel3", vsDefaults.TextMatrix(13, fnx) + " ");
				rsDefaultInfo.Set_Fields("Address", vsDefaults.TextMatrix(14, fnx));
				if (vsDefaults.TextMatrix(15, 3) == "Y")
				{
					rsDefaultInfo.Set_Fields("ExtraFeeForCorrection", true);
				}
				else
				{
					rsDefaultInfo.Set_Fields("ExtraFeeForCorrection", false);
				}
				MotorVehicle.Statics.ReportAgent = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportAgent"));
				MotorVehicle.Statics.ReportState = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportState"));
				MotorVehicle.Statics.ReportTelephone = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportTelephone"));
				MotorVehicle.Statics.ReportTown = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportTown"));
				MotorVehicle.Statics.ReportZip = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportZip"));
				MotorVehicle.Statics.ResidenceCode = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode"));
				MotorVehicle.Statics.gboolCorrectionExtraFee = FCConvert.ToBoolean(rsDefaultInfo.Get_Fields_Boolean("ExtraFeeForCorrection"));
				// Agent Table
				if (vsAgentFee.TextMatrix(1, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("TitleFee", vsAgentFee.TextMatrix(1, fnx));
					rsDefaultInfo.Set_Fields("TitleFee", decimal.Parse(vsAgentFee.TextMatrix(1, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(2, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeReRegLocal", vsAgentFee.TextMatrix(2, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeReRegLocal", decimal.Parse(vsAgentFee.TextMatrix(2, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(3, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeReRegOOT", vsAgentFee.TextMatrix(3, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeReRegOOT", decimal.Parse(vsAgentFee.TextMatrix(3, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(4, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentfeeNewLocal", vsAgentFee.TextMatrix(4, fnx));
					rsDefaultInfo.Set_Fields("AgentfeeNewLocal", decimal.Parse(vsAgentFee.TextMatrix(4, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(5, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeNewOOT", vsAgentFee.TextMatrix(5, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeNewOOT", decimal.Parse(vsAgentFee.TextMatrix(5, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(6, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeBooster", vsAgentFee.TextMatrix(6, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeBooster", decimal.Parse(vsAgentFee.TextMatrix(6, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(7, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeTransit", vsAgentFee.TextMatrix(7, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeTransit", decimal.Parse(vsAgentFee.TextMatrix(7, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(8, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeSpecialRegPermit", vsAgentFee.TextMatrix(8, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeSpecialRegPermit", decimal.Parse(vsAgentFee.TextMatrix(8, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(9, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("DupRegAgentFee", vsAgentFee.TextMatrix(9, fnx));
					rsDefaultInfo.Set_Fields("DupRegAgentFee", decimal.Parse(vsAgentFee.TextMatrix(9, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(10, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("InitialPlateFee", vsAgentFee.TextMatrix(10, fnx));
					rsDefaultInfo.Set_Fields("InitialPlateFee", decimal.Parse(vsAgentFee.TextMatrix(10, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(11, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("DuplicateRegistration", vsAgentFee.TextMatrix(11, fnx));
					rsDefaultInfo.Set_Fields("DuplicateRegistration", decimal.Parse(vsAgentFee.TextMatrix(11, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(12, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("DuplicateDecal", vsAgentFee.TextMatrix(12, fnx));
					rsDefaultInfo.Set_Fields("DuplicateDecal", decimal.Parse(vsAgentFee.TextMatrix(12, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(13, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("ReplacementPlate", vsAgentFee.TextMatrix(13, fnx));
					rsDefaultInfo.Set_Fields("ReplacementPlate", decimal.Parse(vsAgentFee.TextMatrix(13, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(14, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("Stickers", vsAgentFee.TextMatrix(14, fnx));
					rsDefaultInfo.Set_Fields("Stickers", decimal.Parse(vsAgentFee.TextMatrix(14, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(15, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("SpecialRegistrationFee", vsAgentFee.TextMatrix(15, fnx));
					rsDefaultInfo.Set_Fields("SpecialRegistrationFee", decimal.Parse(vsAgentFee.TextMatrix(15, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(16, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("TransitPlateFee", vsAgentFee.TextMatrix(16, fnx));
					rsDefaultInfo.Set_Fields("TransitPlateFee", decimal.Parse(vsAgentFee.TextMatrix(16, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(17, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("TransitPlateRoundTrip", vsAgentFee.TextMatrix(17, fnx));
					rsDefaultInfo.Set_Fields("TransitPlateRoundTrip", decimal.Parse(vsAgentFee.TextMatrix(17, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(18, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("LocalTransferFee", vsAgentFee.TextMatrix(18, fnx));
					rsDefaultInfo.Set_Fields("LocalTransferFee", decimal.Parse(vsAgentFee.TextMatrix(18, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(19, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("StateTransferFee", vsAgentFee.TextMatrix(19, fnx));
					rsDefaultInfo.Set_Fields("StateTransferFee", decimal.Parse(vsAgentFee.TextMatrix(19, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(20, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("DuplicateBoosterFee", vsAgentFee.TextMatrix(20, fnx));
					rsDefaultInfo.Set_Fields("DuplicateBoosterFee", decimal.Parse(vsAgentFee.TextMatrix(20, fnx), System.Globalization.NumberStyles.Currency));
				}
				if (vsAgentFee.TextMatrix(21, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("MinimumExcise", vsAgentFee.TextMatrix(21, fnx));
					rsDefaultInfo.Set_Fields("MinimumExcise", decimal.Parse(vsAgentFee.TextMatrix(21, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("MinimumExcise", 5);
				}
				if (vsAgentFee.TextMatrix(22, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailer", vsAgentFee.TextMatrix(22, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailer", decimal.Parse(vsAgentFee.TextMatrix(22, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailer", 0);
				}
				if (vsAgentFee.TextMatrix(23, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerCTA", vsAgentFee.TextMatrix(23, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerCTA", decimal.Parse(vsAgentFee.TextMatrix(23, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerCTA", 0);
				}
				if (vsAgentFee.TextMatrix(24, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerTransfer", vsAgentFee.TextMatrix(24, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerTransfer", decimal.Parse(vsAgentFee.TextMatrix(24, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerTransfer", 0);
				}
				if (vsAgentFee.TextMatrix(25, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerCorrection", vsAgentFee.TextMatrix(25, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerCorrection", decimal.Parse(vsAgentFee.TextMatrix(25, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerCorrection", 0);
				}
				if (vsAgentFee.TextMatrix(26, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerDupReg", vsAgentFee.TextMatrix(26, fnx));
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerDupReg", decimal.Parse(vsAgentFee.TextMatrix(26, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("AgentFeeLongTermTrailerDupReg", 0);
				}
				if (vsAgentFee.TextMatrix(27, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("LongTermReplacementPlateFee", vsAgentFee.TextMatrix(27, fnx));
					rsDefaultInfo.Set_Fields("LongTermReplacementPlateFee", decimal.Parse(vsAgentFee.TextMatrix(27, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("LongTermReplacementPlateFee", 0);
				}
				if (vsAgentFee.TextMatrix(28, fnx) != "")
				{
					//FC:FINAL:MSH - i.issue #1884: value can't be saved in currency format and will be missed
					//rsDefaultInfo.Set_Fields("RushTitleFee", vsAgentFee.TextMatrix(28, fnx));
					rsDefaultInfo.Set_Fields("RushTitleFee", decimal.Parse(vsAgentFee.TextMatrix(28, fnx), System.Globalization.NumberStyles.Currency));
				}
				else
				{
					rsDefaultInfo.Set_Fields("RushTitleFee", 0);
				}
				rsDefaultInfo.Update();
				DefaultInfoChanged = false;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraTables.Visible = false;
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			int fnx;
			bool PrintIt;
			PrintIt = false;
			for (fnx = 0; fnx <= 14; fnx++)
			{
				if (cmbTable.SelectedIndex == fnx)
				{
					PrintIt = true;
					break;
				}
			}
			if (!PrintIt)
			{
				MessageBox.Show("You must select a table to print before you may proceed.", "Select Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				if (cmbTable.Text == "Residence Codes")
				{
					MotorVehicle.Statics.TableName = "Residence Codes";
					frmReportViewer.InstancePtr.Init(rptTwoColumnTable.InstancePtr);
					// Unload rptTwoColumnTable
				}
				else if (cmbTable.Text == "State Codes")
				{
					MotorVehicle.Statics.TableName = "State Codes";
					frmReportViewer.InstancePtr.Init(rptTwoColumnTable.InstancePtr);
					// Unload rptTwoColumnTable
				}
				else if (cmbTable.Text == "Style Codes")
				{
					MotorVehicle.Statics.TableName = "Style Codes";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
				else if (cmbTable.Text == "County Codes")
				{
					MotorVehicle.Statics.TableName = "County Codes";
					frmReportViewer.InstancePtr.Init(rptTwoColumnTable.InstancePtr);
					// Unload rptTwoColumnTable
				}
				else if (cmbTable.Text == "Make Codes")
				{
					MotorVehicle.Statics.TableName = "Make Codes";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
				else if (cmbTable.Text == "Fees")
				{
					MotorVehicle.Statics.TableName = "Fees";
					frmReportViewer.InstancePtr.Init(rptTwoColumnTable.InstancePtr);
					// Unload rptTwoColumnTable
				}
				else if (cmbTable.Text == "Operator ID Codes")
				{
					MotorVehicle.Statics.TableName = "Operator ID Codes";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
				else if (cmbTable.Text == "Color Codes")
				{
					MotorVehicle.Statics.TableName = "Color Codes";
					frmReportViewer.InstancePtr.Init(rptTwoColumnTable.InstancePtr);
					// Unload rptTwoColumnTable
				}
				else if (cmbTable.Text == "Default Values")
				{
					MotorVehicle.Statics.TableName = "Default Values";
					frmReportViewer.InstancePtr.Init(rptTwoColumnTable.InstancePtr);
					// Unload rptTwoColumnTable
				}
				else if (cmbTable.Text == "Reason Codes")
				{
					MotorVehicle.Statics.TableName = "Reason Codes";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
				else if (cmbTable.Text == "Print Priorities")
				{
					MotorVehicle.Statics.TableName = "Print Priorities";
					frmReportViewer.InstancePtr.Init(rptTwoColumnTable.InstancePtr);
					// Unload rptTwoColumnTable
				}
				else if (cmbTable.Text == "Class Codes")
				{
					MotorVehicle.Statics.TableName = "Class Codes";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
				else if (cmbTable.Text == "Commercial Truck / Truck Tractor GVW")
				{
					MotorVehicle.Statics.TableName = "Commercial Truck / Truck Tractor GVW";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
				else if (cmbTable.Text == "Farm Truck / Motor Home GVW")
				{
					MotorVehicle.Statics.TableName = "Farm Truck / Motor Home GVW";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
				else if (cmbTable.Text == "Class A Special Mobile Equiptment GVW")
				{
					MotorVehicle.Statics.TableName = "Class A Special Mobile Equiptment GVW";
					frmReportViewer.InstancePtr.Init(rptThreeColumnTable.InstancePtr);
					// Unload rptThreeColumnTable
				}
			}
			// DoEvents
			// Me.ZOrder 0
		}

		private void frmTableMaintenance_Activated(object sender, System.EventArgs e)
		{
            if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.TABLEPROCESSING))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			Clear_Arrays();
			SetGridProperties();
			Fill_Grid();
            //Form_Resize();
            //tabTables.Visible = true;
            //frmWait.InstancePtr.Unload();
            //FC:FINAL:SBE - #4276 - use BaseForm implementation for long running processes instead of frmWait
            this.EndWait();
		}

		private void frmTableMaintenance_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmTableMaintenance_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmTableMaintenance.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTableMaintenance_Load(object sender, System.EventArgs e)
		{
            //Begin Unmaped Properties
            //frmTableMaintenance properties;
            //frmTableMaintenance.ScaleWidth	= 9045;
            //frmTableMaintenance.ScaleHeight	= 7170;
            //frmTableMaintenance.LinkTopic	= "Form1";
            //End Unmaped Properties
            //FC:FINAL:SBE - #4276 - use BaseForm implementation for long running processes instead of frmWait
            //frmWait.InstancePtr.Show();
            //frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Retrieving Table Information";
            this.ShowWait();
            this.UpdateWait("Please Wait..." + "\r\n" + "Retrieving Table Information");
			//Support.ZOrder(frmWait.InstancePtr, 0);
			//frmWait.InstancePtr.Refresh();
			//App.DoEvents();
			//tabTables.SelectedIndex = 5;
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
        }

		private void frmTableMaintenance_Resize(object sender, System.EventArgs e)
		{
            // vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
            int fnx;
			int total;
			vsResCodes.AutoSize(1, vsResCodes.Cols - 1);
			vsState.AutoSize(1, vsState.Cols - 1);
			vsResCodes.AutoSize(1, vsResCodes.Cols - 1);
			vsState.AutoSize(1, vsState.Cols - 1);
			total = 0;
			for (fnx = 0; fnx <= (vsResCodes.Cols - 1); fnx++)
			{
				total += vsResCodes.ColWidth(fnx);
			}
			if (vsResCodes.BottomRow < vsResCodes.Rows - 1)
			{
				//vsResCodes.Width = total + 300;
			}
			else
			{
				//vsResCodes.Width = total + 100;
			}
			total = 0;
			for (fnx = 0; fnx <= (vsState.Cols - 1); fnx++)
			{
				total += vsState.ColWidth(fnx);
			}
			if (vsState.BottomRow < vsState.Rows - 1)
			{
				//vsState.Width = total + 300;
			}
			else
			{
				//vsState.Width = total + 100;
			}
			vsOperators.AutoSize(2);
			vsOperators.AutoSize(2);
			vsOperators.AutoSize(4, 7);
			vsOperators.AutoSize(4, 7);
			vsOperators.ColWidth(3, vsOperators.ColWidth(4) + 300);
			total = 0;
			for (fnx = 0; fnx <= (vsOperators.Cols - 1); fnx++)
			{
				total += vsOperators.ColWidth(fnx);
			}
			if (vsOperators.BottomRow < vsOperators.Rows - 1)
			{
				//vsOperators.Width = total + 300;
			}
			else
			{
				//vsOperators.Width = total + 100;
			}
			// vsReasonCodes.AutoSize 1, vsReasonCodes.Cols - 1
			// vsReasonCodes.AutoSize 1, vsReasonCodes.Cols - 1
			vsPrint.AutoSize(1, vsPrint.Cols - 1);
			vsPrint.AutoSize(1, vsPrint.Cols - 1);
			total = 0;
			for (fnx = 0; fnx <= (vsPrint.Cols - 1); fnx++)
			{
				total += vsPrint.ColWidth(fnx);
			}
			if (vsPrint.BottomRow < vsPrint.Rows - 1)
			{
				//vsPrint.Width = total;
			}
			else
			{
				//vsPrint.Width = total;
			}
			total = 0;
			for (fnx = 0; fnx <= (vsReasonCodes.Cols - 1); fnx++)
			{
				total += vsReasonCodes.ColWidth(fnx);
			}
			if (vsReasonCodes.BottomRow < vsReasonCodes.Rows - 1)
			{
				//vsReasonCodes.Width = total;
			}
			else
			{
				//vsReasonCodes.Width = total;
			}
            //FC:FINAL:BSE:#4360 remove autosize to use column fixed width
			//vsClass.AutoSize(2, vsClass.Cols - 1);
			//vsClass.AutoSize(2, vsClass.Cols - 1);
			total = 0;
			for (fnx = 0; fnx <= 5; fnx++)
			{
				total += vsClass.ColWidth(fnx);
			}
			if (vsClass.BottomRow < vsClass.Rows - 1)
			{
				//vsClass.Width = total + 300;
			}
			else
			{
				//vsClass.Width = total + 100;
			}
		    vsDefaults.AutoSize(2, vsDefaults.Cols - 2);
			vsDefaults.AutoSize(2, vsDefaults.Cols - 2);
			//vsDefaults.ColWidth(vsDefaults.Cols - 1, vsDefaults.ColWidth(vsDefaults.Cols - 2) - 300);
			total = 0;
			for (fnx = 0; fnx <= (vsDefaults.Cols - 1); fnx++)
			{
				total += vsDefaults.ColWidth(fnx);
			}
			if (vsDefaults.BottomRow < vsDefaults.Rows - 1)
			{
				//vsDefaults.Width = total + 300;
			}
			else
			{
				//vsDefaults.Width = total + 100;
			}
			vsStyle.AutoSize(1, vsStyle.Cols - 1);
			vsMake.AutoSize(1, vsMake.Cols - 1);
			vsColor.AutoSize(1, vsColor.Cols - 1);
			vsAgentFee.AutoSize(1, vsAgentFee.Cols - 1);
            fraTables.CenterToContainer(this.ClientArea);
        }

		public void Form_Resize()
		{
			frmTableMaintenance_Resize(this, new System.EventArgs());
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//App.DoEvents();
			rsResidence.Reset();
			rsColors.Reset();
			rsMake.Reset();
			rsClass.Reset();
			rsStyle.Reset();
			rsReasonCodes.Reset();
			rsPrintPriority.Reset();
			rsState.Reset();
			rsCounty.Reset();
			rsFleet.Reset();
			rsDefaultInfo.Reset();
			FCUtils.EraseSafe(lngResidence);
			FCUtils.EraseSafe(lngColors);
			FCUtils.EraseSafe(lngMake);
			FCUtils.EraseSafe(lngClass);
			FCUtils.EraseSafe(lngStyle);
			FCUtils.EraseSafe(lngReasonCodes);
			FCUtils.EraseSafe(lngPrintPriority);
			FCUtils.EraseSafe(lngState);
			FCUtils.EraseSafe(lngOperators);
			FCUtils.EraseSafe(lngCounty);
			FCUtils.EraseSafe(lngFleet);
			ResidenceChanged = false;
			ColorChanged = false;
			MakeChanged = false;
			ClassChanged = false;
			StyleChanged = false;
			ReasonChanged = false;
			PrintChanged = false;
			StateChanged = false;
			OperatorsChanged = false;
			CountyChanged = false;
			DefaultInfoChanged = false;
			FleetChanged = false;
			HoldString = "";
			ResSort = false;
			StateSort = false;
			CountySort = false;
			fnx = 0;
			strSql = "";
			ResColumn = 0;
			StateColumn = 0;
			CountyColumn = 0;
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuProcessPrint_Click(object sender, System.EventArgs e)
		{
			fraTables.Visible = true;
			cmbTable.Focus();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			frmTableMaintenance.InstancePtr.Close();
		}

		private void mnuSaveQuit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3 || FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 4)
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			vsClass.Select(0, 0);
			vsClass.Focus();
			vsAgentFee.Focus();
			SaveData();
			if (CorrectSave)
			{
                //FC:FINAL:BSE #2331 save should not close form and message box should appear 
                //frmTableMaintenance.InstancePtr.Close();
               FCMessageBox.Show("Save successful", MsgBoxStyle.Information, "Saved");
			}
		}

		private void txtFleetNumber_KeyPress(ref int KeyAscii)
		{
			if (KeyAscii == 8)
			{
				// backspace
			}
			else if (KeyAscii >= 48 && KeyAscii <= 57)
			{
				// number
			}
			else
			{
				KeyAscii = 0;
			}
		}

		private void tabTables_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
			int fnx;
			int total = 0;
			if (tabTables.SelectedIndex == 0)
			{
				vsResCodes.AutoSize(1, vsResCodes.Cols - 1);
				vsState.AutoSize(1, vsState.Cols - 1);
				vsResCodes.AutoSize(1, vsResCodes.Cols - 1);
				vsState.AutoSize(1, vsState.Cols - 1);
				total = 0;
				for (fnx = 0; fnx <= (vsResCodes.Cols - 1); fnx++)
				{
					total += vsResCodes.ColWidth(fnx);
				}
				if (vsResCodes.BottomRow < vsResCodes.Rows - 1)
				{
					//vsResCodes.Width = total + 300;
				}
				else
				{
					//vsResCodes.Width = total + 100;
				}
				total = 0;
				for (fnx = 0; fnx <= (vsState.Cols - 1); fnx++)
				{
					total += vsState.ColWidth(fnx);
				}
				if (vsState.BottomRow < vsState.Rows - 1)
				{
					//vsState.Width = total + 300;
				}
				else
				{
					//vsState.Width = total + 100;
				}
			}
			else if (tabTables.SelectedIndex == 3)
			{
				vsOperators.AutoSize(2);
				vsOperators.AutoSize(2);
				vsOperators.AutoSize(4, 7);
				vsOperators.AutoSize(4, 7);
				vsOperators.ColWidth(3, vsOperators.ColWidth(4) + 300);
				total = 0;
				for (fnx = 0; fnx <= (vsOperators.Cols - 1); fnx++)
				{
					total += vsOperators.ColWidth(fnx);
				}
				if (vsOperators.BottomRow < vsOperators.Rows - 1)
				{
					//vsOperators.Width = total + 300;
				}
				else
				{
					//vsOperators.Width = total + 100;
				}
			}
			else if (tabTables.SelectedIndex == 6)
			{
				vsReasonCodes.AutoSize(1, vsReasonCodes.Cols - 1);
				vsPrint.AutoSize(1, vsPrint.Cols - 1);
				vsReasonCodes.AutoSize(1, vsReasonCodes.Cols - 1);
				vsPrint.AutoSize(1, vsPrint.Cols - 1);
				//vsPrint.ColWidth(vsPrint.Cols - 1, vsPrint.ColWidth(vsPrint.Cols - 1));
				vsReasonCodes.ColWidth(vsReasonCodes.Cols - 1, vsReasonCodes.ColWidth(vsReasonCodes.Cols - 1));
				total = 0;
				for (fnx = 0; fnx <= (vsPrint.Cols - 1); fnx++)
				{
					total += vsPrint.ColWidth(fnx);
				}
				if (vsPrint.BottomRow < vsPrint.Rows - 1)
				{
					//vsPrint.Width = total - 400;
				}
				else
				{
					//vsPrint.Width = total - 600;
				}
				total = 0;
				for (fnx = 0; fnx <= (vsReasonCodes.Cols - 1); fnx++)
				{
					total += vsReasonCodes.ColWidth(fnx);
				}
				if (vsReasonCodes.BottomRow < vsReasonCodes.Rows - 1)
				{
					//vsReasonCodes.Width = total;
				}
				else
				{
					//vsReasonCodes.Width = total;
				}
			}
			else if (tabTables.SelectedIndex == 8)
			{
                //FC:FINAL:BSE:#4360 remove autosize to use column fixed width
                //vsClass.AutoSize(2, vsClass.Cols - 1);
                //vsClass.AutoSize(2, vsClass.Cols - 1);
                total = 0;
				for (fnx = 0; fnx <= 5; fnx++)
				{
					total += vsClass.ColWidth(fnx);
				}
				if (vsClass.BottomRow < vsClass.Rows - 1)
				{
					//vsClass.Width = total + 300;
				}
				else
				{
					//vsClass.Width = total + 100;
				}
			}
			else if (tabTables.SelectedIndex == 9)
			{
				vsDefaults.AutoSize(2, vsDefaults.Cols - 2);
				vsDefaults.AutoSize(2, vsDefaults.Cols - 2);
				//vsDefaults.ColWidth(vsDefaults.Cols - 1, vsDefaults.ColWidth(vsDefaults.Cols - 2) - 300);
				total = 0;
				for (fnx = 0; fnx <= (vsDefaults.Cols - 1); fnx++)
				{
					total += vsDefaults.ColWidth(fnx);
				}
				if (vsDefaults.BottomRow < vsDefaults.Rows - 1)
				{
					//vsDefaults.Width = total + 300;
				}
				else
				{
					//vsDefaults.Width = total + 100;
				}
			}
		}

		private void vsAgentFee_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsAgentFee_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (DefaultInfoChanged != true)
			{
				if (vsAgentFee.EditText != vsAgentFee.TextMatrix(vsAgentFee.Row, vsAgentFee.Col))
					DefaultInfoChanged = true;
			}
		}

		private void vsClass_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsClass.ColSel == vsClass.Cols - 1)
					DeleteFlag(ref vsClass, ref lngClass);
			}
			else
			{
				if (vsClass.Editable == FCGrid.EditableSettings.flexEDKbdMouse)
				{
					vsClass.EditCell();
				}
			}
		}

		private void vsClass_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			if (vsClass.Col == 5 || vsClass.Col == 9 || vsClass.Col == 10)
			{
				if ((FCConvert.ToString(Convert.ToChar(KeyAscii)) != "Y" && FCConvert.ToString(Convert.ToChar(KeyAscii)) != "N") && KeyAscii != 8)
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsClass_RowColChange(object sender, System.EventArgs e)
		{
			if (vsClass.Col < 5)
			{
				vsClass.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				vsClass.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			if (vsClass.Col == 5 || vsClass.Col == 9 || vsClass.Col == 10)
			{
				vsClass.EditMaxLength = 1;
			}
			else
			{
				vsClass.EditMaxLength = 0;
			}
		}

		private void vsClass_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (ClassChanged != true)
			{
				if (vsClass.EditText != vsClass.TextMatrix(vsClass.Row, vsClass.Col))
					ClassChanged = true;
			}
		}

		private void vsCounty_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (CountyChanged != true)
			{
				if (vsCounty.EditText != vsCounty.TextMatrix(vsCounty.Row, vsCounty.Col))
					CountyChanged = true;
			}
		}

		private void vsDefaults_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsDefaults_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDefaults[e.ColumnIndex, e.RowIndex];
            if (vsDefaults.GetFlexRowIndex(e.RowIndex) == 15)
			{
				//ToolTip1.SetToolTip(vsDefaults, "The state allows an extra $1 fee for any mid-year specialty plate changes. Entering 'Y' in this option will add an extra $1 to the Agent Fee for all eligible transactions.");
				cell.ToolTipText = "The state allows an extra $1 fee for any mid-year specialty plate changes. Entering 'Y' in this option will add an extra $1 to the Agent Fee for all eligible transactions.";
			}
			else
			{
                //ToolTip1.SetToolTip(vsDefaults, "");
                cell.ToolTipText = "";
			}
		}

		private void vsDefaults_RowColChange(object sender, System.EventArgs e)
		{
			if (vsDefaults.Col != 3)
			{
				vsDefaults.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				vsDefaults.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			if ((vsDefaults.Row == 3 || vsDefaults.Row == 7) && vsDefaults.Col == 3)
			{
				vsDefaults.EditMaxLength = 2;
			}
			else
			{
				vsDefaults.EditMaxLength = 0;
			}
		}

		private void vsDefaults_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsDefaults.Row == 15)
			{
				if (vsDefaults.EditText != "Y" && vsDefaults.EditText != "N")
				{
					MessageBox.Show("You may only enter a Y or an N in this field.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
			}
			if (DefaultInfoChanged != true)
			{
				if (vsDefaults.EditText != vsDefaults.TextMatrix(vsDefaults.Row, vsDefaults.Col))
					DefaultInfoChanged = true;
			}
		}

		private void vsGrossTable1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsGrossTable2_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsGrossTable3_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

        private void VsGrossTable_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.AllowOnlyNumericInput();
            }
        }

		private void vsGrossTable1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsGrossTable1.ColSel == vsGrossTable1.Cols - 1)
					DeleteGrossFlag(ref vsGrossTable1, ref lngGrossWeight1);
			}
			else
			{
				vsGrossTable1.EditCell();
			}
		}

		private void vsGrossTable2_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsGrossTable2.ColSel == vsGrossTable2.Cols - 1)
					DeleteGrossFlag(ref vsGrossTable2, ref lngGrossWeight2);
			}
			else
			{
				vsGrossTable2.EditCell();
			}
		}

		private void vsGrossTable3_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsGrossTable3.ColSel == vsGrossTable3.Cols - 1)
					DeleteGrossFlag(ref vsGrossTable3, ref lngGrossWeight3);
			}
			else
			{
				vsGrossTable3.EditCell();
			}
		}

		private void vsMake_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsMake.ColSel == vsMake.Cols - 1)
					DeleteFlag(ref vsMake, ref lngMake);
			}
			else
			{
				vsMake.EditCell();
			}
		}

		private void vsMake_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsMake_RowColChange(object sender, System.EventArgs e)
		{
			if (vsMake.Col == 3)
			{
				vsMake.EditMaxLength = 4;
			}
			else
			{
				vsMake.EditMaxLength = 0;
			}
		}

		private void vsMake_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (MakeChanged != true)
			{
				if (vsMake.EditText != vsMake.TextMatrix(vsMake.Row, vsMake.Col))
					MakeChanged = true;
			}
		}

		private void vsOperators_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsOperators.ColSel == vsOperators.Cols - 1)
				{
					// OperatorsChanged = True
					DeleteFlag(ref vsOperators, ref lngOperators);
				}
			}
			else
			{
				vsOperators.EditCell();
			}
		}

		private void vsOperators_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsOperators_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
			int fnx;
			if (vsOperators.Col == 4)
			{
				if (Conversion.Val(vsOperators.EditText) == 1)
				{
					for (fnx = 1; fnx <= (vsOperators.Rows - 1); fnx++)
					{
						if (fnx != vsOperators.Row)
						{
							if (Conversion.Val(vsOperators.TextMatrix(fnx, vsOperators.Col)) == 1)
							{
								MessageBox.Show("You may only have one Primary Agent.", "Too Many Agents", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
								return;
							}
						}
					}
				}
			}
			if (vsOperators.Col == 5 || vsOperators.Col == 6 || vsOperators.Col == 7)
			{
				if (fecherFoundation.Strings.Trim(vsOperators.EditText) != "")
				{
					if (!Information.IsNumeric(vsOperators.EditText) || (Conversion.Val(vsOperators.EditText) < 0 || Conversion.Val(vsOperators.EditText) > 12))
					{
						MessageBox.Show("You may only enter a number between 0 and 12 for an Inventory Group.", "Invalid Group Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						return;
					}
				}
			}
			if (OperatorsChanged != true)
			{
				if (vsOperators.EditText != vsOperators.TextMatrix(vsOperators.Row, vsOperators.Col))
					OperatorsChanged = true;
			}
		}

		private void vsPrint_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsPrint.ColSel == vsPrint.Cols - 1)
					DeleteFlag(ref vsPrint, ref lngPrintPriority);
			}
			else
			{
				vsPrint.EditCell();
			}
		}

		private void vsPrint_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (PrintChanged != true)
			{
				if (vsPrint.EditText != vsPrint.TextMatrix(vsPrint.Row, vsPrint.Col))
					PrintChanged = true;
			}
		}

		private void vsReasonCodes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsReasonCodes.ColSel == vsReasonCodes.Cols - 1)
					DeleteFlag(ref vsReasonCodes, ref lngReasonCodes);
			}
			else
			{
				vsReasonCodes.EditCell();
			}
		}

		private void vsReasonCodes_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (ReasonChanged != true)
			{
				if (vsReasonCodes.EditText != vsReasonCodes.TextMatrix(vsReasonCodes.Row, vsReasonCodes.Col))
					ReasonChanged = true;
			}
		}

		private void vsResCodes_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			HoldString = vsResCodes.TextMatrix(vsResCodes.Row, vsResCodes.Col);
		}

		private void vsResCodes_DblClick(object sender, System.EventArgs e)
		{
			if (vsResCodes.MouseRow == 1)
			{
				if (ResColumn != vsResCodes.MouseCol)
				{
					if (ResSort == true)
					{
						vsResCodes.Select(1, vsResCodes.MouseCol);
						vsResCodes.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						ResColumn = vsResCodes.MouseCol;
						ResSort = false;
					}
					else
					{
						vsResCodes.Select(1, vsResCodes.MouseCol);
						vsResCodes.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						ResColumn = vsResCodes.MouseCol;
						ResSort = true;
					}
				}
				else if (ResColumn == vsResCodes.MouseCol)
				{
					if (ResSort == true)
					{
						vsResCodes.Select(1, vsResCodes.MouseCol);
						vsResCodes.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						ResColumn = vsResCodes.MouseCol;
						ResSort = false;
					}
					else
					{
						vsResCodes.Select(1, vsResCodes.MouseCol);
						vsResCodes.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						ResColumn = vsResCodes.MouseCol;
						ResSort = true;
					}
				}
			}
		}

		private void vsResCodes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsResCodes.ColSel == vsResCodes.Cols - 1)
					DeleteFlag(ref vsResCodes, ref lngResidence);
			}
			else
			{
				vsResCodes.EditCell();
			}
		}

		private void vsResCodes_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (ResidenceChanged != true)
			{
				if (vsResCodes.EditText != vsResCodes.TextMatrix(vsResCodes.Row, vsResCodes.Col))
					ResidenceChanged = true;
			}
		}

		private void vsState_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			HoldString = vsState.TextMatrix(vsState.Row, vsState.Col);
		}

		private void vsState_DblClick(object sender, System.EventArgs e)
		{
			if (vsState.MouseRow == 1)
			{
				if (StateColumn != vsState.MouseCol)
				{
					if (StateSort == true)
					{
						vsState.Select(1, vsState.MouseCol);
						vsState.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						StateColumn = vsState.MouseCol;
						StateSort = false;
					}
					else
					{
						vsState.Select(1, vsState.MouseCol);
						vsState.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						StateColumn = vsState.MouseCol;
						StateSort = true;
					}
				}
				else if (StateColumn == vsState.MouseCol)
				{
					if (StateSort == true)
					{
						vsState.Select(1, vsState.MouseCol);
						vsState.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						StateColumn = vsState.MouseCol;
						StateSort = false;
					}
					else
					{
						vsState.Select(1, vsState.MouseCol);
						vsState.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						StateColumn = vsState.MouseCol;
						StateSort = true;
					}
				}
			}
		}

		private void vsState_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsState.ColSel == vsState.Cols - 1)
					DeleteFlag(ref vsState, ref lngState);
			}
			else
			{
				vsState.EditCell();
			}
		}

		private void vsCounty_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			HoldString = vsCounty.TextMatrix(vsCounty.Row, vsCounty.Col);
		}

		private void vsCounty_DblClick(object sender, System.EventArgs e)
		{
			if (vsCounty.MouseRow == 1)
			{
				if (CountyColumn != vsCounty.MouseCol)
				{
					if (CountySort == true)
					{
						vsCounty.Select(1, vsCounty.MouseCol);
						vsCounty.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						CountyColumn = vsResCodes.MouseCol;
						CountySort = false;
					}
					else
					{
						vsCounty.Select(1, vsCounty.MouseCol);
						vsCounty.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						CountyColumn = vsCounty.MouseCol;
						CountySort = true;
					}
				}
				else if (CountyColumn == vsCounty.MouseCol)
				{
					if (CountySort == true)
					{
						vsCounty.Select(1, vsCounty.MouseCol);
						vsCounty.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						CountyColumn = vsCounty.MouseCol;
						CountySort = false;
					}
					else
					{
						vsCounty.Select(1, vsCounty.MouseCol);
						vsCounty.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						CountyColumn = vsCounty.MouseCol;
						CountySort = true;
					}
				}
			}
		}

		private void vsCounty_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsCounty.ColSel == vsCounty.Cols - 1)
					DeleteFlag(ref vsCounty, ref lngCounty);
			}
			else
			{
				vsCounty.EditCell();
			}
		}

		private void vsState_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (vsState.Col == 2 && fecherFoundation.Strings.Trim(vsState.EditText) != "")
			{
				for (counter = 1; counter <= (vsState.Rows - 1); counter++)
				{
					if (counter != vsState.Row)
					{
						if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(vsState.EditText)) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(vsState.TextMatrix(counter, 2))))
						{
							MessageBox.Show("This code already exists in this table.", "Code Already Exists", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
					}
				}
			}
			if (StateChanged != true)
			{
				if (vsState.EditText != vsState.TextMatrix(vsState.Row, vsState.Col))
					StateChanged = true;
			}
		}

		private void vsStyle_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (vsStyle.ColSel == vsStyle.Cols - 1)
					DeleteFlag(ref vsStyle, ref lngStyle);
			}
			else
			{
				vsStyle.EditCell();
			}
		}

		private void vsStyle_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsStyle_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (StyleChanged != true)
			{
				if (vsStyle.EditText != vsStyle.TextMatrix(vsStyle.Row, vsStyle.Col))
					StyleChanged = true;
			}
		}

		private void vsGrossTable1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GrossWeightChanged1 != true)
			{
				if (vsGrossTable1.EditText != vsGrossTable1.TextMatrix(vsGrossTable1.Row, vsGrossTable1.Col))
					GrossWeightChanged1 = true;
			}
		}

		private void vsGrossTable2_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GrossWeightChanged2 != true)
			{
				if (vsGrossTable2.EditText != vsGrossTable2.TextMatrix(vsGrossTable2.Row, vsGrossTable2.Col))
					GrossWeightChanged2 = true;
			}
		}

		private void vsGrossTable3_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GrossWeightChanged3 != true)
			{
				if (vsGrossTable3.EditText != vsGrossTable3.TextMatrix(vsGrossTable3.Row, vsGrossTable3.Col))
					GrossWeightChanged3 = true;
			}
		}

		private void Fill_Grid()
		{
			SetGridProperties();
			// Residence
			strSql = "SELECT * FROM Residence ORDER BY Town";
			rsResidence.OpenRecordset(strSql);
			if (rsResidence.EndOfFile() != true && rsResidence.BeginningOfFile() != true)
			{
				rsResidence.MoveLast();
				rsResidence.MoveFirst();
				vsResCodes.Rows = rsResidence.RecordCount() + 10;
				fnx = 0;
				while (!rsResidence.EndOfFile())
				{
					fnx += 1;
					vsResCodes.TextMatrix(fnx, 1, FCConvert.ToString(rsResidence.Get_Fields_Int32("ID")));
					vsResCodes.TextMatrix(fnx, 2, FCConvert.ToString(rsResidence.Get_Fields("Code")));
					vsResCodes.TextMatrix(fnx, 3, FCConvert.ToString(rsResidence.Get_Fields_String("Town")));
					rsResidence.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Residence Table.", "No Residence Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			fnx = 0;
			// State
			strSql = "SELECT * FROM State ORDER BY State";
			rsState.OpenRecordset(strSql);
			if (rsState.EndOfFile() != true && rsState.BeginningOfFile() != true)
			{
				rsState.MoveLast();
				rsState.MoveFirst();
				vsState.Rows = rsState.RecordCount() + 10;
				fnx = 0;
				while (!rsState.EndOfFile())
				{
					fnx += 1;
					vsState.TextMatrix(fnx, 1, FCConvert.ToString(rsState.Get_Fields_Int32("ID")));
					vsState.TextMatrix(fnx, 2, FCConvert.ToString(rsState.Get_Fields_String("Abrev")));
					vsState.TextMatrix(fnx, 3, FCConvert.ToString(rsState.Get_Fields("State")));
					rsState.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the State Table.", "No State Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// County
			strSql = "SELECT * FROM County ORDER BY Name";
			rsCounty.OpenRecordset(strSql);
			if (rsCounty.EndOfFile() != true && rsCounty.BeginningOfFile() != true)
			{
				rsCounty.MoveLast();
				rsCounty.MoveFirst();
				vsCounty.Rows = rsCounty.RecordCount() + 10;
				fnx = 0;
				while (!rsCounty.EndOfFile())
				{
					fnx += 1;
					vsCounty.TextMatrix(fnx, 1, FCConvert.ToString(rsCounty.Get_Fields_Int32("ID")));
					vsCounty.TextMatrix(fnx, 2, FCConvert.ToString(rsCounty.Get_Fields("Code")));
					vsCounty.TextMatrix(fnx, 3, FCConvert.ToString(rsCounty.Get_Fields_String("Name")));
					rsCounty.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the County Table", "No County Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Make
			strSql = "SELECT * FROM Make ORDER BY Type, Make";
			rsMake.OpenRecordset(strSql);
			if (rsMake.EndOfFile() != true && rsMake.BeginningOfFile() != true)
			{
				rsMake.MoveLast();
				rsMake.MoveFirst();
				vsMake.Rows = rsMake.RecordCount() + 10;
				fnx = 0;
				while (!rsMake.EndOfFile())
				{
					fnx += 1;
					vsMake.TextMatrix(fnx, 1, FCConvert.ToString(rsMake.Get_Fields_Int32("ID")));
					vsMake.TextMatrix(fnx, 2, FCConvert.ToString(rsMake.Get_Fields("Type")));
					vsMake.TextMatrix(fnx, 3, FCConvert.ToString(rsMake.Get_Fields("Code")));
					vsMake.TextMatrix(fnx, 4, FCConvert.ToString(rsMake.Get_Fields_String("make")));
					rsMake.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Make Table", "No Make Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// Operators
			strSql = "SELECT * FROM Operators WHERE Name <> '' And Code <> '___'";
			rsOperators.OpenRecordset(strSql, "SystemSettings");
			if (rsOperators.EndOfFile() != true && rsOperators.BeginningOfFile() != true)
			{
				rsOperators.MoveLast();
				rsOperators.MoveFirst();
				vsOperators.Rows = rsOperators.RecordCount() + 10;
				fnx = 0;
				while (!rsOperators.EndOfFile())
				{
					fnx += 1;
					vsOperators.TextMatrix(fnx, 1, FCConvert.ToString(rsOperators.Get_Fields_Int32("ID")));
					vsOperators.TextMatrix(fnx, 2, FCConvert.ToString(rsOperators.Get_Fields("Code")));
					vsOperators.TextMatrix(fnx, 3, FCConvert.ToString(rsOperators.Get_Fields_String("Name")));
					vsOperators.TextMatrix(fnx, 4, FCConvert.ToString(rsOperators.Get_Fields_String("Level")));
					vsOperators.TextMatrix(fnx, 5, FCConvert.ToString(rsOperators.Get_Fields_Int16("PlateGroup")));
					vsOperators.TextMatrix(fnx, 6, FCConvert.ToString(rsOperators.Get_Fields_Int16("StickerGroup")));
					vsOperators.TextMatrix(fnx, 7, FCConvert.ToString(rsOperators.Get_Fields_Int16("MVR3Group")));
					rsOperators.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Operators Table", "No Operator Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Color
			strSql = "SELECT * FROM Color ORDER BY Color";
			rsColors.OpenRecordset(strSql);
			if (rsColors.EndOfFile() != true && rsColors.BeginningOfFile() != true)
			{
				rsColors.MoveLast();
				rsColors.MoveFirst();
				vsColor.Rows = rsColors.RecordCount() + 10;
				fnx = 0;
				while (!rsColors.EndOfFile())
				{
					fnx += 1;
					vsColor.TextMatrix(fnx, 1, FCConvert.ToString(rsColors.Get_Fields_Int32("ID")));
					vsColor.TextMatrix(fnx, 2, FCConvert.ToString(rsColors.Get_Fields("Code")));
					vsColor.TextMatrix(fnx, 3, FCConvert.ToString(rsColors.Get_Fields_String("Color")));
					rsColors.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Color Table", "No Color Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Style
			strSql = "SELECT * FROM Style ORDER BY Type, Style";
			rsStyle.OpenRecordset(strSql);
			if (rsStyle.EndOfFile() != true && rsStyle.BeginningOfFile() != true)
			{
				rsStyle.MoveLast();
				rsStyle.MoveFirst();
				vsStyle.Rows = rsStyle.RecordCount() + 10;
				fnx = 0;
				while (!rsStyle.EndOfFile())
				{
					fnx += 1;
					vsStyle.TextMatrix(fnx, 1, FCConvert.ToString(rsStyle.Get_Fields_Int32("ID")));
					vsStyle.TextMatrix(fnx, 2, FCConvert.ToString(rsStyle.Get_Fields("Type")));
					vsStyle.TextMatrix(fnx, 3, FCConvert.ToString(rsStyle.Get_Fields("Code")));
					vsStyle.TextMatrix(fnx, 4, FCConvert.ToString(rsStyle.Get_Fields_String("Style")));
					rsStyle.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Style Table", "No Style Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Reason Codes
			strSql = "SELECT * FROM ReasonCodes";
			rsReasonCodes.OpenRecordset(strSql);
			if (rsReasonCodes.EndOfFile() != true && rsReasonCodes.BeginningOfFile() != true)
			{
				rsReasonCodes.MoveLast();
				rsReasonCodes.MoveFirst();
				vsReasonCodes.Rows = rsReasonCodes.RecordCount() + 10;
				fnx = 0;
				while (!rsReasonCodes.EndOfFile())
				{
					fnx += 1;
					vsReasonCodes.TextMatrix(fnx, 1, FCConvert.ToString(rsReasonCodes.Get_Fields_Int32("ID")));
					vsReasonCodes.TextMatrix(fnx, 2, FCConvert.ToString(rsReasonCodes.Get_Fields("Code")));
					vsReasonCodes.TextMatrix(fnx, 3, FCConvert.ToString(rsReasonCodes.Get_Fields("Priority")));
					vsReasonCodes.TextMatrix(fnx, 4, FCConvert.ToString(rsReasonCodes.Get_Fields_String("Description")));
					rsReasonCodes.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Information Reason Codes Table.", "No Reason Codes Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Print Priorities
			strSql = "SELECT * FROM PrintPriority";
			rsPrintPriority.OpenRecordset(strSql);
			if (rsPrintPriority.EndOfFile() != true && rsPrintPriority.BeginningOfFile() != true)
			{
				rsPrintPriority.MoveLast();
				rsPrintPriority.MoveFirst();
				vsPrint.Rows = rsPrintPriority.RecordCount() + 10;
				fnx = 0;
				while (!rsPrintPriority.EndOfFile())
				{
					fnx += 1;
					vsPrint.TextMatrix(fnx, 1, FCConvert.ToString(rsPrintPriority.Get_Fields_Int32("ID")));
					vsPrint.TextMatrix(fnx, 2, FCConvert.ToString(rsPrintPriority.Get_Fields("Priority")));
					vsPrint.TextMatrix(fnx, 3, FCConvert.ToString(rsPrintPriority.Get_Fields_String("Description")));
					rsPrintPriority.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Print Priority Table.", "No Print Priority Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Class
			strSql = "SELECT * FROM Class ORDER BY BMVCode, SystemCode";
			rsClass.OpenRecordset(strSql);
			if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
			{
				rsClass.MoveLast();
				rsClass.MoveFirst();
				vsClass.Rows = rsClass.RecordCount() + 10;
				fnx = 0;
				while (!rsClass.EndOfFile())
				{
					fnx += 1;
					vsClass.TextMatrix(fnx, 2, FCConvert.ToString(rsClass.Get_Fields_String("BMVCode")));
					vsClass.TextMatrix(fnx, 3, FCConvert.ToString(rsClass.Get_Fields_String("SystemCode")));
					vsClass.TextMatrix(fnx, 4, FCConvert.ToString(rsClass.Get_Fields_String("Description")));
					vsClass.TextMatrix(fnx, 5, FCConvert.ToString(rsClass.Get_Fields_String("ExciseRequired")));
					vsClass.TextMatrix(fnx, 6, FCConvert.ToString(rsClass.Get_Fields("RegistrationFee")));
					vsClass.TextMatrix(fnx, 7, FCConvert.ToString(rsClass.Get_Fields_Decimal("OtherFeesNew")));
					vsClass.TextMatrix(fnx, 8, FCConvert.ToString(rsClass.Get_Fields_Decimal("Otherfeesrenew")));
					vsClass.TextMatrix(fnx, 9, FCConvert.ToString(rsClass.Get_Fields_String("HalfRateAllowed")));
					vsClass.TextMatrix(fnx, 10, FCConvert.ToString(rsClass.Get_Fields("TwoYear")));
					vsClass.TextMatrix(fnx, 11, FCConvert.ToString(rsClass.Get_Fields_String("LocationNew")));
					vsClass.TextMatrix(fnx, 12, FCConvert.ToString(rsClass.Get_Fields_String("LocationTransfer")));
					vsClass.TextMatrix(fnx, 13, FCConvert.ToString(rsClass.Get_Fields_String("LocationRenewal")));
					vsClass.TextMatrix(fnx, 14, FCConvert.ToString(rsClass.Get_Fields_Int32("NumberOfPlateStickers")));
					vsClass.TextMatrix(fnx, 15, FCConvert.ToString(rsClass.Get_Fields_String("renewaldate")));
					vsClass.TextMatrix(fnx, 16, FCConvert.ToString(rsClass.Get_Fields_String("TitleRequired")));
					vsClass.TextMatrix(fnx, 17, FCConvert.ToString(rsClass.Get_Fields_String("Emission")));
					vsClass.TextMatrix(fnx, 18, FCConvert.ToString(rsClass.Get_Fields("InsuranceRequired")));
					rsClass.MoveNext();
				}
				vsClass.Rows = fnx + 1;
			}
			else
			{
				MessageBox.Show("There are no current records in the Information Reason Codes Table.", "No Reason Codes Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Gross Vehicle Weight
			strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = 'TK' ORDER BY Low";
			rsGrossVehicleWeight1.OpenRecordset(strSql);
			if (rsGrossVehicleWeight1.EndOfFile() != true && rsGrossVehicleWeight1.BeginningOfFile() != true)
			{
				rsGrossVehicleWeight1.MoveLast();
				rsGrossVehicleWeight1.MoveFirst();
				vsGrossTable1.Rows = rsGrossVehicleWeight1.RecordCount() + 10;
				fnx = 0;
				while (!rsGrossVehicleWeight1.EndOfFile())
				{
					fnx += 1;
					vsGrossTable1.TextMatrix(fnx, 0, FCConvert.ToString(rsGrossVehicleWeight1.Get_Fields_Int32("ID")));
					vsGrossTable1.TextMatrix(fnx, 2, FCConvert.ToString(rsGrossVehicleWeight1.Get_Fields("Low")));
					vsGrossTable1.TextMatrix(fnx, 3, FCConvert.ToString(rsGrossVehicleWeight1.Get_Fields("High")));
					vsGrossTable1.TextMatrix(fnx, 4, FCConvert.ToString(rsGrossVehicleWeight1.Get_Fields("Fee")));
					rsGrossVehicleWeight1.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Gross Vehicle Weight Table", "No GVW Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			fnx = 0;
			strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = 'FM' ORDER BY Low";
			rsGrossVehicleWeight2.OpenRecordset(strSql);
			if (rsGrossVehicleWeight2.EndOfFile() != true && rsGrossVehicleWeight2.BeginningOfFile() != true)
			{
				rsGrossVehicleWeight2.MoveLast();
				rsGrossVehicleWeight2.MoveFirst();
				vsGrossTable2.Rows = rsGrossVehicleWeight2.RecordCount() + 10;
				fnx = 0;
				while (!rsGrossVehicleWeight2.EndOfFile())
				{
					fnx += 1;
					vsGrossTable2.TextMatrix(fnx, 0, FCConvert.ToString(rsGrossVehicleWeight2.Get_Fields_Int32("ID")));
					vsGrossTable2.TextMatrix(fnx, 2, FCConvert.ToString(rsGrossVehicleWeight2.Get_Fields("Low")));
					vsGrossTable2.TextMatrix(fnx, 3, FCConvert.ToString(rsGrossVehicleWeight2.Get_Fields("High")));
					vsGrossTable2.TextMatrix(fnx, 4, FCConvert.ToString(rsGrossVehicleWeight2.Get_Fields("Fee")));
					rsGrossVehicleWeight2.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Gross Vehicle Weight Table", "No GVW Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			fnx = 0;
			strSql = "SELECT * FROM GrossVehicleWeight WHERE Type = 'F2' ORDER BY Low";
			rsGrossVehicleWeight3.OpenRecordset(strSql);
			if (rsGrossVehicleWeight3.EndOfFile() != true && rsGrossVehicleWeight3.BeginningOfFile() != true)
			{
				rsGrossVehicleWeight3.MoveLast();
				rsGrossVehicleWeight3.MoveFirst();
				vsGrossTable3.Rows = rsGrossVehicleWeight3.RecordCount() + 10;
				fnx = 0;
				while (!rsGrossVehicleWeight3.EndOfFile())
				{
					fnx += 1;
					vsGrossTable3.TextMatrix(fnx, 0, FCConvert.ToString(rsGrossVehicleWeight3.Get_Fields_Int32("ID")));
					vsGrossTable3.TextMatrix(fnx, 2, FCConvert.ToString(rsGrossVehicleWeight3.Get_Fields("Low")));
					vsGrossTable3.TextMatrix(fnx, 3, FCConvert.ToString(rsGrossVehicleWeight3.Get_Fields("High")));
					vsGrossTable3.TextMatrix(fnx, 4, FCConvert.ToString(rsGrossVehicleWeight3.Get_Fields("Fee")));
					rsGrossVehicleWeight3.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no current records in the Gross Vehicle Weight Table", "No GVW Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			fnx = 0;
			// Agent Fees & DefaultInfo
			vsAgentFee.TextMatrix(0, 2, "Category");
			vsAgentFee.TextMatrix(0, 3, "Value");
			vsAgentFee.TextMatrix(1, 2, "Title Fee");
			vsAgentFee.TextMatrix(2, 2, "Agent Fee ReReg. Local");
			vsAgentFee.TextMatrix(3, 2, "Agent Fee ReReg O.O.T.");
			vsAgentFee.TextMatrix(4, 2, "Agent Fee New Local");
			vsAgentFee.TextMatrix(5, 2, "Agent Fee New O.O.T.");
			vsAgentFee.TextMatrix(6, 2, "Agent Fee Booster Permit");
			vsAgentFee.TextMatrix(7, 2, "Agent Fee Transit Plate");
			vsAgentFee.TextMatrix(8, 2, "Agent Fee Special Registration Permit");
			vsAgentFee.TextMatrix(9, 2, "Agent Fee Duplicate Registration");
			vsAgentFee.TextMatrix(10, 2, "Vanity Plate Fee");
			vsAgentFee.TextMatrix(11, 2, "Duplicate Registration");
			vsAgentFee.TextMatrix(12, 2, "Duplicate Decal");
			vsAgentFee.TextMatrix(13, 2, "Replacement Plate");
			vsAgentFee.TextMatrix(14, 2, "Stickers");
			vsAgentFee.TextMatrix(15, 2, "Special Registration Fee");
			vsAgentFee.TextMatrix(16, 2, "Transit Plate Fee (One Way)");
			vsAgentFee.TextMatrix(17, 2, "Transit Plate Fee (Round Trip)");
			vsAgentFee.TextMatrix(18, 2, "Excise Tax Transfer Charge");
			vsAgentFee.TextMatrix(19, 2, "State Transfer Fee");
			vsAgentFee.TextMatrix(20, 2, "Duplicate Booster Fee");
			vsAgentFee.TextMatrix(21, 2, "Minimum Excise");
			if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
			{
				vsAgentFee.TextMatrix(22, 2, "Agent Fee Long Term Trailer");
				vsAgentFee.TextMatrix(23, 2, "Agent Fee Long Term Trailer CTA");
				vsAgentFee.TextMatrix(24, 2, "Agent Fee Long Term Trailer Transfer");
				vsAgentFee.TextMatrix(25, 2, "Agent Fee Long Term Trailer Correction");
				vsAgentFee.TextMatrix(26, 2, "Agent Fee Long Term Trailer Dup Reg");
				vsAgentFee.TextMatrix(27, 2, "Long Term Replacement Plate");
				vsAgentFee.TextMatrix(28, 2, "Rush Title Fee");
			}
			// DefaultInfo
			vsDefaults.TextMatrix(0, 2, "Category");
			vsDefaults.TextMatrix(0, 3, "Value");
			vsDefaults.TextMatrix(1, 2, "Residence Code");
			vsDefaults.TextMatrix(2, 2, "Default Town");
			vsDefaults.TextMatrix(3, 2, "Default State");
			vsDefaults.TextMatrix(4, 2, "Default Zip");
			vsDefaults.TextMatrix(5, 2, "Transfer -  Excise or Agent?");
			vsDefaults.TextMatrix(6, 2, "Report Town");
			vsDefaults.TextMatrix(7, 2, "Report State");
			vsDefaults.TextMatrix(8, 2, "Report Zip");
			vsDefaults.TextMatrix(9, 2, "Report Agent Name");
			vsDefaults.TextMatrix(10, 2, "Report Telephone Number");
			vsDefaults.TextMatrix(11, 2, "User Defined Label 1");
			vsDefaults.TextMatrix(12, 2, "User Defined Label 2");
			vsDefaults.TextMatrix(13, 2, "User Defined Label 3");
			vsDefaults.TextMatrix(14, 2, "Default Address");
			vsDefaults.TextMatrix(15, 2, "Extra $1 on Specialty Plate Change");
			strSql = "SELECT * FROM DefaultInfo";
			rsDefaultInfo.OpenRecordset(strSql);
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				rsDefaultInfo.MoveLast();
				rsDefaultInfo.MoveFirst();
				// Agent
				vsAgentFee.TextMatrix(1, 3, Strings.Format(rsDefaultInfo.Get_Fields("TitleFee"), "$###.00"));
				vsAgentFee.TextMatrix(2, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegLocal"), "$###.00"));
				vsAgentFee.TextMatrix(3, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegOOT"), "$###.00"));
				vsAgentFee.TextMatrix(4, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentfeeNewLocal"), "$###.00"));
				vsAgentFee.TextMatrix(5, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeNewOOT"), "$###.00"));
				vsAgentFee.TextMatrix(6, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeBooster"), "$###.00"));
				vsAgentFee.TextMatrix(7, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeTransit"), "$###.00"));
				vsAgentFee.TextMatrix(8, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeSpecialRegPermit"), "$###.00"));
				vsAgentFee.TextMatrix(9, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DupRegAgentFee"), "$###.00"));
				vsAgentFee.TextMatrix(10, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("InitialPlateFee"), "$###.00"));
				vsAgentFee.TextMatrix(11, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DuplicateRegistration"), "$###.00"));
				vsAgentFee.TextMatrix(12, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DuplicateDecal"), "$###.00"));
				vsAgentFee.TextMatrix(13, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("ReplacementPlate"), "$###.00"));
				vsAgentFee.TextMatrix(14, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("Stickers"), "$###.00"));
				vsAgentFee.TextMatrix(15, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("SpecialRegistrationFee"), "$###.00"));
				vsAgentFee.TextMatrix(16, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("TransitPlateFee"), "$###.00"));
				vsAgentFee.TextMatrix(17, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("TransitPlateRoundTrip"), "$###.00"));
				vsAgentFee.TextMatrix(18, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("LocalTransferFee"), "$###.00"));
				vsAgentFee.TextMatrix(19, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("StateTransferFee"), "$###.00"));
				vsAgentFee.TextMatrix(20, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("DuplicateBoosterFee"), "$###.00"));
				vsAgentFee.TextMatrix(21, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("MinimumExcise"), "$###.00"));
				if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
				{
					vsAgentFee.TextMatrix(22, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeLongTermTrailer"), "$###.00"));
					vsAgentFee.TextMatrix(23, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeLongTermTrailerCTA"), "$###.00"));
					vsAgentFee.TextMatrix(24, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeLongTermTrailerTransfer"), "$###.00"));
					vsAgentFee.TextMatrix(25, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeLongTermTrailerCorrection"), "$###.00"));
					vsAgentFee.TextMatrix(26, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("AgentFeeLongTermTrailerDupReg"), "$###.00"));
					vsAgentFee.TextMatrix(27, 3, Strings.Format(rsDefaultInfo.Get_Fields_Decimal("LongTermReplacementPlateFee"), "$###.00"));
					vsAgentFee.TextMatrix(28, 3, Strings.Format(rsDefaultInfo.Get_Fields("RushTitleFee"), "$###.00"));
				}
				// DefaultInfo
				vsDefaults.TextMatrix(1, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode")));
				vsDefaults.TextMatrix(2, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Town")));
				vsDefaults.TextMatrix(3, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields("State")));
				vsDefaults.TextMatrix(4, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Zip")));
				vsDefaults.TextMatrix(5, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ExciseVSAgent")));
				vsDefaults.TextMatrix(6, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportTown")));
				vsDefaults.TextMatrix(7, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportState")));
				vsDefaults.TextMatrix(8, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportZip")));
				vsDefaults.TextMatrix(9, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportAgent")));
				vsDefaults.TextMatrix(10, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ReportTelephone")));
				vsDefaults.TextMatrix(11, 3, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))));
				vsDefaults.TextMatrix(12, 3, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))));
				vsDefaults.TextMatrix(13, 3, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))));
				vsDefaults.TextMatrix(14, 3, FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Address")));
				if (rsDefaultInfo.Get_Fields_Boolean("ExtraFeeForCorrection") == true)
				{
					vsDefaults.TextMatrix(15, 3, "Y");
				}
				else
				{
					vsDefaults.TextMatrix(15, 3, "N");
				}
			}
		}
		// vbPorter upgrade warning: vs1 As vsFlexGrid	OnWrite(FCGridCtl.vsFlexGrid)
		private object DeleteFlag(ref FCGrid vs1, ref int[] lng1)
		{
			object DeleteFlag = null;
			// vbPorter upgrade warning: xx As object	OnWriteFCConvert.ToInt32(
			int xx;
			if (Conversion.Val(vs1.TextMatrix(vs1.Row, 1)) != 0)
			{
				for (xx = Information.LBound(lng1); xx <= Information.UBound(lng1, 1); xx++)
				{
					if (lng1[xx] == 0)
					{
						lng1[xx] = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 1))));
						vs1.RemoveItem(vs1.Row);
						Support.SendKeys("{UP}", false);
						break;
					}
				}
				// xx
			}
			return DeleteFlag;
		}
		// vbPorter upgrade warning: vs1 As vsFlexGrid	OnWrite(FCGridCtl.vsFlexGrid)
		private object DeleteGrossFlag(ref FCGrid vs1, ref int[] lng1)
		{
			object DeleteGrossFlag = null;
			// vbPorter upgrade warning: xx As object	OnWriteFCConvert.ToInt32(
			int xx;
			if (Conversion.Val(vs1.TextMatrix(vs1.Row, 0)) != 0)
			{
				for (xx = Information.LBound(lng1); xx <= Information.UBound(lng1, 1); xx++)
				{
					if (lng1[xx] == 0)
					{
						lng1[xx] = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
						vs1.RemoveItem(vs1.Row);
						Support.SendKeys("{UP}", false);
						break;
					}
				}
				// xx
			}
			return DeleteGrossFlag;
		}

		private void SetGridProperties()
		{
			// Residence
			vsResCodes.Cols = 4;
			vsResCodes.FixedCols = 2;
			vsResCodes.ColWidth(0, 0);
            //vsResCodes.ColWidth(1, 400);
            //vsResCodes.ColWidth(2, 900);
            vsResCodes.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsResCodes.Width * 0.10)));
            vsResCodes.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsResCodes.Width* 0.25)));
            vsResCodes.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsResCodes.Width * 0.60)));
            // vsResCodes.ExtendLastCol = True
            //vsResCodes.Width = 4000;
			vsResCodes.Rows = 600;
			vsResCodes.TextMatrix(0, 2, "Code");
			vsResCodes.TextMatrix(0, 3, "Town Name");
			vsResCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsResCodes.Rows - 1, vsResCodes.Cols - 1, 1);
			// State
			vsState.Cols = 4;
			vsState.FixedCols = 2;
			vsState.ColWidth(0, 0);
			//vsState.ColWidth(1, 400);
			//vsState.ColWidth(2, 900);
            vsState.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsState.Width * 0.10)));
            vsState.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsState.Width * 0.15)));
            vsState.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsState.Width * 0.70)));
            // vsState.ExtendLastCol = True
            //vsState.Width = 4000;
            vsState.Rows = 75;
			vsState.TextMatrix(0, 2, "Abrev.");
			vsState.TextMatrix(0, 3, "State");
			vsState.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsState.Rows - 1, 2, 1);
			vsState.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsState.Rows - 1, 3, 1);
			// County
			vsCounty.Cols = 4;
			vsCounty.FixedCols = 2;
			vsCounty.ColWidth(0, 0);
            //vsCounty.ColWidth(1, 400);
            //vsCounty.ColWidth(2, 900);
            vsCounty.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsCounty.Width * 0.10)));
            vsCounty.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsCounty.Width * 0.15)));
            vsCounty.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsCounty.Width * 0.70)));
            vsCounty.ExtendLastCol = true;
			//vsCounty.Width = 4000;
			vsCounty.Rows = 100;
			vsCounty.TextMatrix(0, 2, "Code");
			vsCounty.TextMatrix(0, 3, "Name");
			vsCounty.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsCounty.Rows - 1, 2, 1);
			vsCounty.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsCounty.Rows - 1, 3, 1);
			// Make
			vsMake.Cols = 5;
			vsMake.FixedCols = 2;
			vsMake.ColWidth(0, 0);
            vsMake.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsMake.Width * 0.10)));
            vsMake.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsMake.Width * 0.15)));
            vsMake.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsMake.Width * 0.15)));
            vsMake.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsMake.Width * 0.60)));
            // vsMake.ColWidth(1) = 400
            // vsMake.ColWidth(2) = 650
            // vsMake.ColWidth(3) = 800
            vsMake.ExtendLastCol = true;
			// vsMake.Width = 5000
			vsMake.Rows = 100;
			vsMake.TextMatrix(0, 2, "Type");
			vsMake.TextMatrix(0, 3, "Code");
			vsMake.TextMatrix(0, 4, "Make");
			vsMake.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsMake.Rows - 1, 2, 1);
			vsMake.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsMake.Rows - 1, 3, 1);
			vsMake.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, vsMake.Rows - 1, 4, 1);
			// Operators
			vsOperators.Cols = 8;
			vsOperators.FixedCols = 2;
			vsOperators.ColWidth(0, 0);
            //vsOperators.ColWidth(1, 400);
            //vsOperators.ColWidth(2, 650);
            //vsOperators.ColWidth(3, 1800);
            //vsOperators.ColWidth(4, 1000);
            //vsOperators.ColWidth(5, 650);
            //vsOperators.ColWidth(6, 650);
            //vsOperators.ColWidth(7, 650);
            vsOperators.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsOperators.Width * 0.05)));
            vsOperators.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsOperators.Width * 0.10)));
            vsOperators.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsOperators.Width * 0.25)));
            vsOperators.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsOperators.Width * 0.15)));
            vsOperators.ColWidth(5, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsOperators.Width * 0.15)));
            vsOperators.ColWidth(6, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsOperators.Width * 0.15)));
            vsOperators.ColWidth(7, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsOperators.Width * 0.15)));
            // vsOperators.ExtendLastCol = True
            //vsOperators.Width = 7000;
            vsOperators.Rows = 100;
			vsOperators.TextMatrix(0, 2, "Code");
			vsOperators.TextMatrix(0, 3, "Operator");
			vsOperators.TextMatrix(0, 4, "Agent Level");
			vsOperators.TextMatrix(0, 5, "Plate Group");
			vsOperators.TextMatrix(0, 6, "Sticker Group");
			vsOperators.TextMatrix(0, 7, "MVR3 Group");
			vsOperators.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsOperators.Rows - 1, 2, 1);
			vsOperators.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsOperators.Rows - 1, 3, 1);
			vsOperators.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, vsOperators.Rows - 1, 4, 1);
			vsOperators.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 5, vsOperators.Rows - 1, 5, 1);
			vsOperators.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 6, vsOperators.Rows - 1, 6, 1);
			vsOperators.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 7, vsOperators.Rows - 1, 7, 1);
			// Color
			vsColor.Cols = 4;
			vsColor.FixedCols = 2;
			vsColor.ColWidth(0, 0);
            // vsColor.ColWidth(1) = 400
            // vsColor.ColWidth(2) = 650
            // vsColor.ColWidth(3) = 2000
            vsColor.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsColor.Width * 0.10)));
            vsColor.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsColor.Width * 0.15)));
            vsColor.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsColor.Width * 0.75)));
            vsColor.ExtendLastCol = true;
			// vsColor.Width = 4000
			vsColor.Rows = 100;
			vsColor.TextMatrix(0, 2, "Code");
			vsColor.TextMatrix(0, 3, "Color");
			vsColor.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsColor.Rows - 1, 2, 1);
			vsColor.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsColor.Rows - 1, 3, 1);
			// Style
			vsStyle.Cols = 5;
			vsStyle.FixedCols = 2;
			vsStyle.ColWidth(0, 0);
            // vsStyle.ColWidth(1) = 400
            // vsStyle.ColWidth(2) = 650
            // vsStyle.ColWidth(3) = 700
            vsStyle.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsStyle.Width * 0.10)));
            vsStyle.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsStyle.Width * 0.15)));
            vsStyle.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsStyle.Width * 0.15)));
            vsStyle.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsStyle.Width * 0.60)));
            vsStyle.ExtendLastCol = true;
			// vsStyle.Width = 5000
			vsStyle.Rows = 100;
			vsStyle.TextMatrix(0, 2, "Type");
			vsStyle.TextMatrix(0, 3, "Code");
			vsStyle.TextMatrix(0, 4, "Style");
			vsStyle.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsStyle.Rows - 1, 2, 1);
			vsStyle.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsStyle.Rows - 1, 3, 1);
			vsStyle.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, vsStyle.Rows - 1, 4, 1);
			// Reason Codes
			vsReasonCodes.Cols = 5;
			vsReasonCodes.FixedCols = 2;
			vsReasonCodes.ColWidth(0, 0);
            //vsReasonCodes.ColWidth(1, 400);
            //vsReasonCodes.ColWidth(2, 650);
            //vsReasonCodes.ColWidth(3, 1000);
            vsReasonCodes.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsReasonCodes.Width * 0.10)));
            vsReasonCodes.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsReasonCodes.Width * 0.12)));
            vsReasonCodes.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsReasonCodes.Width * 0.12)));
            vsReasonCodes.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsReasonCodes.Width * 0.64)));
            vsReasonCodes.ExtendLastCol = true;
			//vsReasonCodes.Width = 5800;
			vsReasonCodes.Rows = 100;
			vsReasonCodes.TextMatrix(0, 2, "Code");
			vsReasonCodes.TextMatrix(0, 3, "Priority");
			vsReasonCodes.TextMatrix(0, 4, "Description");
			vsReasonCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsReasonCodes.Rows - 1, 2, 1);
			vsReasonCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsReasonCodes.Rows - 1, 3, 1);
			vsReasonCodes.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, vsReasonCodes.Rows - 1, 4, 1);
			// Print Priorities
			vsPrint.Cols = 4;
			vsPrint.FixedCols = 2;
            vsPrint.ColWidth(0, 0);
            //vsPrint.ColWidth(1, 350);
            //vsPrint.ColWidth(2, 900);
            vsPrint.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsPrint.Width * 0.10)));
            vsPrint.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsPrint.Width * 0.15)));
            vsPrint.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsPrint.Width * 0.70)));
            // vsPrint.ExtendLastCol = True
            //vsPrint.Width = 4500;
            vsPrint.Rows = 100;
			vsPrint.TextMatrix(0, 2, "Priority");
			vsPrint.TextMatrix(0, 3, "Description");
			vsPrint.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, vsPrint.Rows - 1, 2, 1);
			vsPrint.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, vsPrint.Rows - 1, 3, 1);
			// Class
			vsClass.Cols = 19;
			vsClass.FixedCols = 3;
			//vsClass.Width = 8000;
			vsClass.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 0, 18, 9);
			vsClass.ColWidth(0, 0);
			vsClass.ColWidth(1, 0);
            //vsClass.ColWidth(2, 1000);
            //vsClass.ColWidth(3, 1040);
            //vsClass.ColWidth(4, 3500);
            vsClass.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(80)));
            vsClass.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(100)));
            vsClass.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(420)));
            // description
            //vsClass.ColWidth(5, 900);
            //vsClass.ColWidth(6, 900);
            //vsClass.ColWidth(7, 1500);
            //vsClass.ColWidth(8, 1500);
            //vsClass.ColWidth(9, 1000);
            //vsClass.ColWidth(10, 1000);
            //vsClass.ColWidth(11, 1300);
            //vsClass.ColWidth(12, 1500);
            //vsClass.ColWidth(13, 1500);
            //vsClass.ColWidth(14, 1600);
            //vsClass.ColWidth(15, 1200);
            //vsClass.ColWidth(16, 1200);
            //vsClass.ColWidth(17, 1000);
            //vsClass.ColWidth(18, 900);
            vsClass.ColWidth(5, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(70)));
            vsClass.ColWidth(6, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(70)));
            vsClass.ColWidth(7, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(120)));
            vsClass.ColWidth(8, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(180)));
            vsClass.ColWidth(9, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(90)));
            vsClass.ColWidth(10, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(90)));
            vsClass.ColWidth(11, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(100)));
            vsClass.ColWidth(12, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(135)));
            vsClass.ColWidth(13, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(145)));
            vsClass.ColWidth(14, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(140)));
            vsClass.ColWidth(15, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(120)));
            vsClass.ColWidth(16, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(110)));
            vsClass.ColWidth(17, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(90)));
            vsClass.ColWidth(18, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(100)));            
            // vsClass.ExtendLastCol = True
            vsClass.Rows = 25;
			vsClass.TextMatrix(0, 3, "System Code");
			vsClass.TextMatrix(0, 2, "BMV Code");
			vsClass.TextMatrix(0, 4, "Description");
			vsClass.TextMatrix(0, 5, "Exc. Req.");
			vsClass.TextMatrix(0, 6, "Reg. Fee");
			vsClass.ColFormat(6, "$###.00");
			vsClass.TextMatrix(0, 7, "Other Fees New");
			vsClass.ColFormat(7, "$###.00");
			vsClass.TextMatrix(0, 8, "Other Fees Renew");
			vsClass.ColFormat(8, "$###.00");
			vsClass.TextMatrix(0, 9, "Half Rate");
			vsClass.TextMatrix(0, 10, "Two Year");
			vsClass.TextMatrix(0, 11, "Location New");
			vsClass.TextMatrix(0, 12, "Location Transfer");
			vsClass.TextMatrix(0, 13, "Location Renewal");
			vsClass.TextMatrix(0, 14, "# of Plates/Stickers");
			vsClass.TextMatrix(0, 15, "Renewal Date");
			vsClass.TextMatrix(0, 16, "Title Required");
			vsClass.TextMatrix(0, 17, "Emission");
			vsClass.TextMatrix(0, 18, "Ins. Required");
			vsClass.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsClass.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsClass.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// Agent Fees
			vsAgentFee.Cols = 4;
			vsAgentFee.FixedCols = 2;
			vsAgentFee.ColWidth(0, 0);
            // vsAgentFee.ColWidth(1) = 300
            // vsAgentFee.ColWidth(2) = 2900
            // vsAgentFee.ColWidth(3) = 1100
            vsAgentFee.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsAgentFee.Width * 0.02)));
            vsAgentFee.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsAgentFee.Width * 0.68)));
            vsAgentFee.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsAgentFee.Width * 0.30)));
            vsAgentFee.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAgentFee.ExtendLastCol = true;
			// (row,col)
			// vsAgentFee.Width = 4500
			// DefaultInfo Table
			vsDefaults.Cols = 4;
			vsDefaults.FixedCols = 2;
			vsDefaults.ColWidth(0, 0);
            //vsDefaults.ColWidth(1, 400);
            //vsDefaults.ColWidth(2, 2700);
            //vsDefaults.ColWidth(3, 1200);
            vsDefaults.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsDefaults.Width * 0.02)));
            vsDefaults.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsDefaults.Width * 0.46)));
            vsDefaults.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsDefaults.Width * 0.46)));
            vsDefaults.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// vsDefaults.ExtendLastCol = True '(row,col)
			//vsDefaults.Width = 5500;
			// Gross Vehicle Weight
			vsGrossTable1.ColWidth(0, 0);
            //vsGrossTable1.ColWidth(1, 200);
            //vsGrossTable1.ColWidth(2, 1000);
            //vsGrossTable1.ColWidth(3, 1000);
            vsGrossTable1.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable1.Width * 0.02)));
            vsGrossTable1.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable1.Width * 0.24)));
            vsGrossTable1.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable1.Width * 0.24)));
            vsGrossTable1.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable1.Width * 0.44)));
            vsGrossTable1.Rows = 100;
			vsGrossTable1.TextMatrix(0, 2, "From");
			vsGrossTable1.TextMatrix(0, 3, "To");
			vsGrossTable1.TextMatrix(0, 4, "Fee");
			vsGrossTable1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, 3, 1);
			vsGrossTable1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrossTable1.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrossTable1.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrossTable1.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrossTable1.ColFormat(4, "#,##0.00");
			vsGrossTable2.ColWidth(0, 0);
			//vsGrossTable2.ColWidth(1, 200);
			//vsGrossTable2.ColWidth(2, 1000);
			//vsGrossTable2.ColWidth(3, 1000);
            vsGrossTable2.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable2.Width * 0.02)));
            vsGrossTable2.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable2.Width * 0.24)));
            vsGrossTable2.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable2.Width * 0.24)));
            vsGrossTable2.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable2.Width * 0.44)));
            vsGrossTable2.Rows = 100;
			vsGrossTable2.TextMatrix(0, 2, "From");
			vsGrossTable2.TextMatrix(0, 3, "To");
			vsGrossTable2.TextMatrix(0, 4, "Fee");
			vsGrossTable2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, 3, 1);
			vsGrossTable2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrossTable2.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrossTable2.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrossTable2.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrossTable2.ColFormat(4, "#,##0.00");
			vsGrossTable3.ColWidth(0, 0);
            //vsGrossTable3.ColWidth(1, 200);
            //vsGrossTable3.ColWidth(2, 1000);
            //vsGrossTable3.ColWidth(3, 1000);
            vsGrossTable3.ColWidth(1, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable3.Width * 0.02)));
            vsGrossTable3.ColWidth(2, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable3.Width * 0.24)));
            vsGrossTable3.ColWidth(3, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable3.Width * 0.24)));
            vsGrossTable3.ColWidth(4, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsGrossTable3.Width * 0.44)));
            vsGrossTable3.Rows = 100;
			vsGrossTable3.TextMatrix(0, 2, "From");
			vsGrossTable3.TextMatrix(0, 3, "To");
			vsGrossTable3.TextMatrix(0, 4, "Fee");
			vsGrossTable3.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, 3, 1);
			vsGrossTable3.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrossTable3.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrossTable3.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrossTable3.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrossTable3.ColFormat(4, "#,##0.00");
			// Set All Grids To Editable TRUE
			vsAgentFee.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsClass.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			// vsColor.Editable = True
			vsCounty.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsDefaults.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsMake.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsOperators.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsPrint.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsReasonCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsResCodes.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsState.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsStyle.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsGrossTable1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsGrossTable2.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsGrossTable3.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void Delete_Items()
		{
			int ii;
			for (ii = 0; ii <= 99; ii++)
			{
				if (lngResidence[ii] != 0)
					Delete_Record(ref rsResidence, ref lngResidence[ii]);
				if (lngColors[ii] != 0)
					Delete_Record(ref rsColors, ref lngColors[ii]);
				if (lngMake[ii] != 0)
					Delete_Record(ref rsMake, ref lngMake[ii]);
				if (lngClass[ii] != 0)
					Delete_Record(ref rsClass, ref lngClass[ii]);
				if (lngStyle[ii] != 0)
					Delete_Record(ref rsStyle, ref lngStyle[ii]);
				if (lngReasonCodes[ii] != 0)
					Delete_Record(ref rsReasonCodes, ref lngReasonCodes[ii]);
				if (lngPrintPriority[ii] != 0)
					Delete_Record(ref rsPrintPriority, ref lngPrintPriority[ii]);
				if (lngState[ii] != 0)
					Delete_Record(ref rsState, ref lngState[ii]);
				if (lngOperators[ii] != 0)
					Delete_Operator_Record(ref rsOperators, ref lngOperators[ii]);
				if (lngCounty[ii] != 0)
					Delete_Record(ref rsCounty, ref lngCounty[ii]);
				if (lngFleet[ii] != 0)
					Delete_Record(ref rsFleet, ref lngFleet[ii]);
				if (lngGrossWeight1[ii] != 0)
				{
					Delete_Record(ref rsGrossVehicleWeight1, ref lngGrossWeight1[ii]);
				}
				if (lngGrossWeight2[ii] != 0)
				{
					Delete_Record(ref rsGrossVehicleWeight2, ref lngGrossWeight2[ii]);
				}
				if (lngGrossWeight3[ii] != 0)
				{
					Delete_Record(ref rsGrossVehicleWeight3, ref lngGrossWeight3[ii]);
				}
			}
		}

		private void Delete_Record(ref clsDRWrapper rsRecordSet, ref int lngIDNumber)
		{
			rsRecordSet.FindFirstRecord("ID", lngIDNumber);
			rsRecordSet.Delete();
			rsRecordSet.Update();
		}

		private void Delete_Operator_Record(ref clsDRWrapper rsRecordSet, ref int lngIDNumber)
		{
			if (rsRecordSet.FindFirstRecord("ID", lngIDNumber))
			{
				rsRecordSet.Delete();
				rsRecordSet.Update();
			}
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case MotorVehicle.STYLECODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[5].Enabled = false;
							}
							else
							{
								tabTables.TabPages[5].Enabled = true;
							}
							break;
						}
					case MotorVehicle.STATECODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[0].Enabled = false;
							}
							else
							{
								tabTables.TabPages[0].Enabled = true;
							}
							break;
						}
					case MotorVehicle.COUNTYCODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[1].Enabled = false;
							}
							else
							{
								tabTables.TabPages[1].Enabled = true;
							}
							break;
						}
					case MotorVehicle.REASONCODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[6].Enabled = false;
							}
							else
							{
								tabTables.TabPages[6].Enabled = true;
							}
							break;
						}
					case MotorVehicle.MAKECODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[2].Enabled = false;
							}
							else
							{
								tabTables.TabPages[2].Enabled = true;
							}
							break;
						}
					case MotorVehicle.FEES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[7].Enabled = false;
							}
							else
							{
								tabTables.TabPages[7].Enabled = true;
							}
							break;
						}
					case MotorVehicle.OPERATORCODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[3].Enabled = false;
							}
							else
							{
								tabTables.TabPages[3].Enabled = true;
							}
							break;
						}
					case MotorVehicle.CLASSCODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[8].Enabled = false;
							}
							else
							{
								tabTables.TabPages[8].Enabled = true;
							}
							break;
						}
					case MotorVehicle.COLORCODES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[4].Enabled = false;
							}
							else
							{
								tabTables.TabPages[4].Enabled = true;
							}
							break;
						}
					case MotorVehicle.DEFAULTVALUES:
						{
							if (strPerm == "N")
							{
								tabTables.TabPages[9].Enabled = false;
							}
							else
							{
								tabTables.TabPages[9].Enabled = true;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void Clear_Arrays()
		{
			for (fnx = 0; fnx <= 99; fnx++)
			{
				lngResidence[fnx] = 0;
				lngColors[fnx] = 0;
				lngMake[fnx] = 0;
				lngClass[fnx] = 0;
				lngStyle[fnx] = 0;
				lngReasonCodes[fnx] = 0;
				lngPrintPriority[fnx] = 0;
				lngState[fnx] = 0;
				lngOperators[fnx] = 0;
				lngCounty[fnx] = 0;
				lngFleet[fnx] = 0;
				lngGrossWeight1[fnx] = 0;
				lngGrossWeight2[fnx] = 0;
				lngGrossWeight3[fnx] = 0;
			}
		}

		private void SetCustomFormColors()
		{
            //lblInstruct[0].BackColor = SystemColors.Control;
            //lblInstruct[1].BackColor = SystemColors.Control;
            //lblInstruct[2].BackColor = SystemColors.Control;
            //Label2.BackColor = SystemColors.Control;
			//Label8.BackColor = SystemColors.Control;
			//Label11.BackColor = SystemColors.Control;
            //lblLevel[0].BackColor = SystemColors.Control;
            //lblLevel[1].BackColor = SystemColors.Control;
            //lblLevel[2].BackColor = SystemColors.Control;
            //lblLevel[3].BackColor = SystemColors.Control;
        }
    }
}
