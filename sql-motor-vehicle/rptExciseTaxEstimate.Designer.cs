﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptExciseTaxEstimate.
	/// </summary>
	partial class rptExciseTaxEstimate
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExciseTaxEstimate));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMonthRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOptions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMonthTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEstimate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblVehicleCount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNACount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMonthlyTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVehicleCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNACount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFinalVehicleCount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFinalNACount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalVehicleCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalNACount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBasePriceYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBasePriceYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBasePriceYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBasePriceYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBasePriceYear5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBasePriceYear6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseYear5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseYear6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBasePriceTotalYears = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExciseTotalYears = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMonthRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEstimate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVehicleCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNACount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthlyTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVehicleCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNACount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalVehicleCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalNACount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalVehicleCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalNACount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceTotalYears)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotalYears)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldClassPlate,
				this.fldOwner,
				this.fldMilYear,
				this.fldAmount,
				this.fldEstimate
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label7,
				this.Label8,
				this.Line1,
				this.Label1,
				this.Label32,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Label36,
				this.lblMonthRange,
				this.Label37,
				this.lblOptions
			});
			this.PageHeader.Height = 0.96875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field2,
				this.Field3
			});
			this.PageFooter.Height = 0.1875F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFinalVehicleCount,
				this.lblFinalNACount,
				this.Line3,
				this.Label42,
				this.fldFinalTotal,
				this.fldFinalVehicleCount,
				this.fldFinalNACount,
				this.Line4,
				this.Label43,
				this.Label44,
				this.Label45,
				this.Label46,
				this.Label47,
				this.Label48,
				this.Label49,
				this.Label50,
				this.Label51,
				this.Label52,
				this.fldBasePriceYear1,
				this.fldBasePriceYear2,
				this.fldBasePriceYear3,
				this.fldBasePriceYear4,
				this.fldBasePriceYear5,
				this.fldBasePriceYear6,
				this.fldExciseYear1,
				this.fldExciseYear2,
				this.fldExciseYear3,
				this.fldExciseYear4,
				this.fldExciseYear5,
				this.fldExciseYear6,
				this.Line5,
				this.Label53,
				this.fldBasePriceTotalYears,
				this.fldExciseTotalYears
			});
			this.GroupFooter1.Height = 2.625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			//
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMonthTitle,
				this.Binder
			});
			this.GroupHeader2.DataField = "Binder";
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			//
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.CanShrink = true;
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblVehicleCount,
				this.lblNACount,
				this.Line2,
				this.Label39,
				this.fldMonthlyTotal,
				this.fldVehicleCount,
				this.fldNACount
			});
			this.GroupFooter2.Height = 0.5625F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.15625F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "CL / Plate";
			this.Label7.Top = 0.75F;
			this.Label7.Width = 1F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 1.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Owner";
			this.Label8.Top = 0.75F;
			this.Label8.Width = 2.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9375F;
			this.Line1.Width = 6.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 0.9375F;
			this.Line1.Y2 = 0.9375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Excise Tax Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.6875F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label32.Text = "Label32";
			this.Label32.Top = 0.1875F;
			this.Label32.Width = 1.5F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label33.Text = "Label33";
			this.Label33.Top = 0F;
			this.Label33.Width = 1.5F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 5.1875F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label34.Text = "Label34";
			this.Label34.Top = 0.1875F;
			this.Label34.Width = 1.3125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 5.1875F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label35.Text = "Label35";
			this.Label35.Top = 0F;
			this.Label35.Width = 1.3125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 4.25F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label36.Text = "Mi Year";
			this.Label36.Top = 0.75F;
			this.Label36.Width = 0.53125F;
			// 
			// lblMonthRange
			// 
			this.lblMonthRange.Height = 0.21875F;
			this.lblMonthRange.HyperLink = null;
			this.lblMonthRange.Left = 1.5F;
			this.lblMonthRange.Name = "lblMonthRange";
			this.lblMonthRange.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblMonthRange.Text = "Excise Tax Estimate";
			this.lblMonthRange.Top = 0.21875F;
			this.lblMonthRange.Width = 3.6875F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 4.78125F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label37.Text = "Amount";
			this.Label37.Top = 0.75F;
			this.Label37.Width = 1F;
			// 
			// lblOptions
			// 
			this.lblOptions.Height = 0.21875F;
			this.lblOptions.HyperLink = null;
			this.lblOptions.Left = 1.5F;
			this.lblOptions.Name = "lblOptions";
			this.lblOptions.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblOptions.Text = "Excise Tax Estimate";
			this.lblOptions.Top = 0.4375F;
			this.lblOptions.Width = 3.6875F;
			// 
			// fldMonthTitle
			// 
			this.fldMonthTitle.Height = 0.1875F;
			this.fldMonthTitle.Left = 0F;
			this.fldMonthTitle.Name = "fldMonthTitle";
			this.fldMonthTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldMonthTitle.Text = "Field1";
			this.fldMonthTitle.Top = 0.0625F;
			this.fldMonthTitle.Width = 3.375F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 3.96875F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.03125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.8125F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.1875F;
			this.fldClassPlate.Left = 0.15625F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Text = "Field1";
			this.fldClassPlate.Top = 0F;
			this.fldClassPlate.Width = 1F;
			// 
			// fldOwner
			// 
			this.fldOwner.Height = 0.1875F;
			this.fldOwner.Left = 1.1875F;
			this.fldOwner.Name = "fldOwner";
			this.fldOwner.Text = "Field1";
			this.fldOwner.Top = 0F;
			this.fldOwner.Width = 2.9375F;
			// 
			// fldMilYear
			// 
			this.fldMilYear.Height = 0.1875F;
			this.fldMilYear.Left = 4.1875F;
			this.fldMilYear.Name = "fldMilYear";
			this.fldMilYear.Text = "Field1";
			this.fldMilYear.Top = 0F;
			this.fldMilYear.Width = 0.53125F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 4.78125F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "text-align: right";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1F;
			// 
			// fldEstimate
			// 
			this.fldEstimate.Height = 0.125F;
			this.fldEstimate.Left = 5.78125F;
			this.fldEstimate.Name = "fldEstimate";
			this.fldEstimate.Style = "font-size: 10pt; font-weight: bold; text-align: right";
			this.fldEstimate.Text = "*";
			this.fldEstimate.Top = 0F;
			this.fldEstimate.Visible = false;
			this.fldEstimate.Width = 0.125F;
			// 
			// lblVehicleCount
			// 
			this.lblVehicleCount.Height = 0.1875F;
			this.lblVehicleCount.HyperLink = null;
			this.lblVehicleCount.Left = 0.96875F;
			this.lblVehicleCount.Name = "lblVehicleCount";
			this.lblVehicleCount.Style = "font-weight: bold; text-align: right";
			this.lblVehicleCount.Text = "Vehicle Count:";
			this.lblVehicleCount.Top = 0.15625F;
			this.lblVehicleCount.Width = 1.15625F;
			// 
			// lblNACount
			// 
			this.lblNACount.Height = 0.1875F;
			this.lblNACount.HyperLink = null;
			this.lblNACount.Left = 0.96875F;
			this.lblNACount.Name = "lblNACount";
			this.lblNACount.Style = "font-weight: bold; text-align: right";
			this.lblNACount.Text = "N/A Count:";
			this.lblNACount.Top = 0.34375F;
			this.lblNACount.Width = 1.15625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 2.28125F;
			this.Line2.X1 = 5.90625F;
			this.Line2.X2 = 3.625F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 3.625F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-weight: bold; text-align: right";
			this.Label39.Text = "Monthly Total:";
			this.Label39.Top = 0.03125F;
			this.Label39.Width = 1.15625F;
			// 
			// fldMonthlyTotal
			// 
			this.fldMonthlyTotal.Height = 0.1875F;
			this.fldMonthlyTotal.Left = 4.84375F;
			this.fldMonthlyTotal.Name = "fldMonthlyTotal";
			this.fldMonthlyTotal.Style = "font-weight: bold; text-align: right";
			this.fldMonthlyTotal.Text = "Field1";
			this.fldMonthlyTotal.Top = 0.03125F;
			this.fldMonthlyTotal.Width = 1F;
			// 
			// fldVehicleCount
			// 
			this.fldVehicleCount.Height = 0.1875F;
			this.fldVehicleCount.Left = 2.15625F;
			this.fldVehicleCount.Name = "fldVehicleCount";
			this.fldVehicleCount.Style = "font-weight: bold; text-align: right";
			this.fldVehicleCount.Text = "Field1";
			this.fldVehicleCount.Top = 0.15625F;
			this.fldVehicleCount.Width = 0.625F;
			// 
			// fldNACount
			// 
			this.fldNACount.Height = 0.1875F;
			this.fldNACount.Left = 2.15625F;
			this.fldNACount.Name = "fldNACount";
			this.fldNACount.Style = "font-weight: bold; text-align: right";
			this.fldNACount.Text = "Field1";
			this.fldNACount.Top = 0.34375F;
			this.fldNACount.Width = 0.625F;
			// 
			// lblFinalVehicleCount
			// 
			this.lblFinalVehicleCount.Height = 0.1875F;
			this.lblFinalVehicleCount.HyperLink = null;
			this.lblFinalVehicleCount.Left = 0.96875F;
			this.lblFinalVehicleCount.Name = "lblFinalVehicleCount";
			this.lblFinalVehicleCount.Style = "font-weight: bold; text-align: right";
			this.lblFinalVehicleCount.Text = "Vehicle Count:";
			this.lblFinalVehicleCount.Top = 0.15625F;
			this.lblFinalVehicleCount.Width = 1.15625F;
			// 
			// lblFinalNACount
			// 
			this.lblFinalNACount.Height = 0.1875F;
			this.lblFinalNACount.HyperLink = null;
			this.lblFinalNACount.Left = 0.96875F;
			this.lblFinalNACount.Name = "lblFinalNACount";
			this.lblFinalNACount.Style = "font-weight: bold; text-align: right";
			this.lblFinalNACount.Text = "N/A Count:";
			this.lblFinalNACount.Top = 0.34375F;
			this.lblFinalNACount.Width = 1.15625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 3.625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.28125F;
			this.Line3.X1 = 5.90625F;
			this.Line3.X2 = 3.625F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 3.625F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-weight: bold; text-align: right";
			this.Label42.Text = "Final Total:";
			this.Label42.Top = 0.03125F;
			this.Label42.Width = 1.15625F;
			// 
			// fldFinalTotal
			// 
			this.fldFinalTotal.Height = 0.1875F;
			this.fldFinalTotal.Left = 4.84375F;
			this.fldFinalTotal.Name = "fldFinalTotal";
			this.fldFinalTotal.Style = "font-weight: bold; text-align: right";
			this.fldFinalTotal.Text = "Field2";
			this.fldFinalTotal.Top = 0.03125F;
			this.fldFinalTotal.Width = 1F;
			// 
			// fldFinalVehicleCount
			// 
			this.fldFinalVehicleCount.Height = 0.1875F;
			this.fldFinalVehicleCount.Left = 2.15625F;
			this.fldFinalVehicleCount.Name = "fldFinalVehicleCount";
			this.fldFinalVehicleCount.Style = "font-weight: bold; text-align: right";
			this.fldFinalVehicleCount.Text = "Field1";
			this.fldFinalVehicleCount.Top = 0.15625F;
			this.fldFinalVehicleCount.Width = 0.625F;
			// 
			// fldFinalNACount
			// 
			this.fldFinalNACount.Height = 0.1875F;
			this.fldFinalNACount.Left = 2.15625F;
			this.fldFinalNACount.Name = "fldFinalNACount";
			this.fldFinalNACount.Style = "font-weight: bold; text-align: right";
			this.fldFinalNACount.Text = "Field1";
			this.fldFinalNACount.Top = 0.34375F;
			this.fldFinalNACount.Width = 0.625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 1.5F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.1875F;
			this.Line4.Width = 3.3125F;
			this.Line4.X1 = 1.5F;
			this.Line4.X2 = 4.8125F;
			this.Line4.Y1 = 1.1875F;
			this.Line4.Y2 = 1.1875F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 2.375F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-size: 11pt; font-weight: bold; text-align: center";
			this.Label43.Text = "Mil Year Summary";
			this.Label43.Top = 0.75F;
			this.Label43.Width = 1.4375F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 1.5F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-weight: bold; text-align: left";
			this.Label44.Text = "Mil Year";
			this.Label44.Top = 1F;
			this.Label44.Width = 0.75F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 3.6875F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-weight: bold; text-align: right";
			this.Label45.Text = "Excise";
			this.Label45.Top = 1F;
			this.Label45.Width = 1.125F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 2.3125F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-weight: bold; text-align: right";
			this.Label46.Text = "Base Price";
			this.Label46.Top = 1F;
			this.Label46.Width = 1.25F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 1.5F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-weight: bold; text-align: left";
			this.Label47.Text = "1";
			this.Label47.Top = 1.25F;
			this.Label47.Width = 0.75F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 1.5F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-weight: bold; text-align: left";
			this.Label48.Text = "2";
			this.Label48.Top = 1.4375F;
			this.Label48.Width = 0.75F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1875F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 1.5F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-weight: bold; text-align: left";
			this.Label49.Text = "3";
			this.Label49.Top = 1.625F;
			this.Label49.Width = 0.75F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1875F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 1.5F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-weight: bold; text-align: left";
			this.Label50.Text = "4";
			this.Label50.Top = 1.8125F;
			this.Label50.Width = 0.75F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 1.5F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-weight: bold; text-align: left";
			this.Label51.Text = "5";
			this.Label51.Top = 2F;
			this.Label51.Width = 0.75F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 1.5F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-weight: bold; text-align: left";
			this.Label52.Text = "6";
			this.Label52.Top = 2.1875F;
			this.Label52.Width = 0.75F;
			// 
			// fldBasePriceYear1
			// 
			this.fldBasePriceYear1.Height = 0.1875F;
			this.fldBasePriceYear1.Left = 2.3125F;
			this.fldBasePriceYear1.Name = "fldBasePriceYear1";
			this.fldBasePriceYear1.Style = "font-weight: bold; text-align: right";
			this.fldBasePriceYear1.Text = "Field2";
			this.fldBasePriceYear1.Top = 1.25F;
			this.fldBasePriceYear1.Width = 1.25F;
			// 
			// fldBasePriceYear2
			// 
			this.fldBasePriceYear2.Height = 0.1875F;
			this.fldBasePriceYear2.Left = 2.3125F;
			this.fldBasePriceYear2.Name = "fldBasePriceYear2";
			this.fldBasePriceYear2.Style = "font-weight: bold; text-align: right";
			this.fldBasePriceYear2.Text = "Field2";
			this.fldBasePriceYear2.Top = 1.4375F;
			this.fldBasePriceYear2.Width = 1.25F;
			// 
			// fldBasePriceYear3
			// 
			this.fldBasePriceYear3.Height = 0.1875F;
			this.fldBasePriceYear3.Left = 2.3125F;
			this.fldBasePriceYear3.Name = "fldBasePriceYear3";
			this.fldBasePriceYear3.Style = "font-weight: bold; text-align: right";
			this.fldBasePriceYear3.Text = "Field2";
			this.fldBasePriceYear3.Top = 1.625F;
			this.fldBasePriceYear3.Width = 1.25F;
			// 
			// fldBasePriceYear4
			// 
			this.fldBasePriceYear4.Height = 0.1875F;
			this.fldBasePriceYear4.Left = 2.3125F;
			this.fldBasePriceYear4.Name = "fldBasePriceYear4";
			this.fldBasePriceYear4.Style = "font-weight: bold; text-align: right";
			this.fldBasePriceYear4.Text = "Field2";
			this.fldBasePriceYear4.Top = 1.8125F;
			this.fldBasePriceYear4.Width = 1.25F;
			// 
			// fldBasePriceYear5
			// 
			this.fldBasePriceYear5.Height = 0.1875F;
			this.fldBasePriceYear5.Left = 2.3125F;
			this.fldBasePriceYear5.Name = "fldBasePriceYear5";
			this.fldBasePriceYear5.Style = "font-weight: bold; text-align: right";
			this.fldBasePriceYear5.Text = "Field2";
			this.fldBasePriceYear5.Top = 2F;
			this.fldBasePriceYear5.Width = 1.25F;
			// 
			// fldBasePriceYear6
			// 
			this.fldBasePriceYear6.Height = 0.1875F;
			this.fldBasePriceYear6.Left = 2.3125F;
			this.fldBasePriceYear6.Name = "fldBasePriceYear6";
			this.fldBasePriceYear6.Style = "font-weight: bold; text-align: right";
			this.fldBasePriceYear6.Text = "Field2";
			this.fldBasePriceYear6.Top = 2.1875F;
			this.fldBasePriceYear6.Width = 1.25F;
			// 
			// fldExciseYear1
			// 
			this.fldExciseYear1.Height = 0.1875F;
			this.fldExciseYear1.Left = 3.6875F;
			this.fldExciseYear1.Name = "fldExciseYear1";
			this.fldExciseYear1.Style = "font-weight: bold; text-align: right";
			this.fldExciseYear1.Text = "Field2";
			this.fldExciseYear1.Top = 1.25F;
			this.fldExciseYear1.Width = 1.125F;
			// 
			// fldExciseYear2
			// 
			this.fldExciseYear2.Height = 0.1875F;
			this.fldExciseYear2.Left = 3.6875F;
			this.fldExciseYear2.Name = "fldExciseYear2";
			this.fldExciseYear2.Style = "font-weight: bold; text-align: right";
			this.fldExciseYear2.Text = "Field2";
			this.fldExciseYear2.Top = 1.4375F;
			this.fldExciseYear2.Width = 1.125F;
			// 
			// fldExciseYear3
			// 
			this.fldExciseYear3.Height = 0.1875F;
			this.fldExciseYear3.Left = 3.6875F;
			this.fldExciseYear3.Name = "fldExciseYear3";
			this.fldExciseYear3.Style = "font-weight: bold; text-align: right";
			this.fldExciseYear3.Text = "Field2";
			this.fldExciseYear3.Top = 1.625F;
			this.fldExciseYear3.Width = 1.125F;
			// 
			// fldExciseYear4
			// 
			this.fldExciseYear4.Height = 0.1875F;
			this.fldExciseYear4.Left = 3.6875F;
			this.fldExciseYear4.Name = "fldExciseYear4";
			this.fldExciseYear4.Style = "font-weight: bold; text-align: right";
			this.fldExciseYear4.Text = "Field2";
			this.fldExciseYear4.Top = 1.8125F;
			this.fldExciseYear4.Width = 1.125F;
			// 
			// fldExciseYear5
			// 
			this.fldExciseYear5.Height = 0.1875F;
			this.fldExciseYear5.Left = 3.6875F;
			this.fldExciseYear5.Name = "fldExciseYear5";
			this.fldExciseYear5.Style = "font-weight: bold; text-align: right";
			this.fldExciseYear5.Text = "Field2";
			this.fldExciseYear5.Top = 2F;
			this.fldExciseYear5.Width = 1.125F;
			// 
			// fldExciseYear6
			// 
			this.fldExciseYear6.Height = 0.1875F;
			this.fldExciseYear6.Left = 3.6875F;
			this.fldExciseYear6.Name = "fldExciseYear6";
			this.fldExciseYear6.Style = "font-weight: bold; text-align: right";
			this.fldExciseYear6.Text = "Field2";
			this.fldExciseYear6.Top = 2.1875F;
			this.fldExciseYear6.Width = 1.125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 1.5F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.375F;
			this.Line5.Width = 3.3125F;
			this.Line5.X1 = 1.5F;
			this.Line5.X2 = 4.8125F;
			this.Line5.Y1 = 2.375F;
			this.Line5.Y2 = 2.375F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 1.5F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-weight: bold; text-align: left";
			this.Label53.Text = "Total:";
			this.Label53.Top = 2.4375F;
			this.Label53.Width = 0.75F;
			// 
			// fldBasePriceTotalYears
			// 
			this.fldBasePriceTotalYears.Height = 0.1875F;
			this.fldBasePriceTotalYears.Left = 2.3125F;
			this.fldBasePriceTotalYears.Name = "fldBasePriceTotalYears";
			this.fldBasePriceTotalYears.Style = "font-weight: bold; text-align: right";
			this.fldBasePriceTotalYears.Text = "Field2";
			this.fldBasePriceTotalYears.Top = 2.4375F;
			this.fldBasePriceTotalYears.Width = 1.25F;
			// 
			// fldExciseTotalYears
			// 
			this.fldExciseTotalYears.Height = 0.1875F;
			this.fldExciseTotalYears.Left = 3.6875F;
			this.fldExciseTotalYears.Name = "fldExciseTotalYears";
			this.fldExciseTotalYears.Style = "font-weight: bold; text-align: right";
			this.fldExciseTotalYears.Text = "Field2";
			this.fldExciseTotalYears.Top = 2.4375F;
			this.fldExciseTotalYears.Width = 1.125F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.19F;
			this.Field2.Left = 0F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-size: 10pt; text-align: right";
			this.Field2.Text = "*";
			this.Field2.Top = 0.03125F;
			this.Field2.Width = 0.15625F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.15625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-size: 9pt; text-align: left";
			this.Field3.Text = "= Estimated";
			this.Field3.Top = 0F;
			this.Field3.Width = 1F;
			// 
			// rptExciseTaxEstimate
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMonthRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEstimate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVehicleCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNACount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthlyTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVehicleCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNACount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalVehicleCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalNACount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalVehicleCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalNACount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBasePriceTotalYears)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotalYears)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEstimate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMonthRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOptions;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFinalVehicleCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFinalNACount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalVehicleCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalNACount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBasePriceYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBasePriceYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBasePriceYear3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBasePriceYear4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBasePriceYear5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBasePriceYear6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseYear3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseYear4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseYear5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseYear6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBasePriceTotalYears;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseTotalYears;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVehicleCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNACount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthlyTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVehicleCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNACount;
	}
}
