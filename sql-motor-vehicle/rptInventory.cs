//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptInventory.
	/// </summary>
	public partial class rptInventory : BaseSectionReport
	{
		public rptInventory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptInventory_ReportEnd;
		}

        private void RptInventory_ReportEnd(object sender, EventArgs e)
        {
            rsPrint.DisposeOf();
        }

        public static rptInventory InstancePtr
		{
			get
			{
				return (rptInventory)Sys.GetInstance(typeof(rptInventory));
			}
		}

		protected rptInventory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptInventory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strSQL = "";
		string strSQLItems = "";
		//clsDRWrapper rsPlates = new clsDRWrapper();
		//clsDRWrapper rsItems = new clsDRWrapper();
		clsDRWrapper rsPrint = new clsDRWrapper();
        FCFileSystem ff = new FCFileSystem();
		string TitleType = "";
		int NumberOfRecords;
		int[] Stickers = new int[6 + 1];
		int MaxElements;
		int fnx;
		int lngStart;
		int lngStop;
		int lngRow;
		int lngFirst;
		int lngLast;
		string strPrefix = "";
		string strSuffix = "";
		int Difference;
		int TB;
		int lines;
		int intGroup;
		bool boolDidSection;
		int PageCounter;
		public bool blnAll;
		string strOriginalPrefix = "";
		string strStartPlate = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDidSection == true)
			{
				eArgs.EOF = true;
				return;
			}
			if (!rsPrint.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				rsPrint.MovePrevious();
				Difference = 0;
				if (FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number")) != lngFirst || rsPrint.RecordCount() == 1)
				{
					boolDidSection = true;
					lines += 1;
					Difference = (rsPrint.Get_Fields_Int32("Number") - lngFirst) + 1;
					txtType.Text = Strings.Mid(FCConvert.ToString(rsPrint.Get_Fields("Code")), 4, 2);
					txtDate.Text = Strings.Format(rsPrint.Get_Fields_DateTime("InventoryDate"), "MM/dd/yyyy");
					txtOpID.Text = rsPrint.Get_Fields_String("OpID");
					txtLow.Text = strStartPlate;
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					txtHigh.Text = rsPrint.Get_Fields_String("FormattedInventory");
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					txtStatus.Text = rsPrint.Get_Fields_String("Status");
					if (FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) == 0)
					{
						txtGroup.Text = "All";
					}
					else
					{
						txtGroup.Text = rsPrint.Get_Fields_String("Group");
					}
					txtCount.Text = Difference.ToString();
					lines = lines % 60;
					eArgs.EOF = false;
					return;
				}
				else
				{
					Difference = (rsPrint.Get_Fields_Int32("Number") - lngFirst) + 1;
					txtType.Text = Strings.Mid(FCConvert.ToString(rsPrint.Get_Fields("Code")), 4, 2);
					txtDate.Text = Strings.Format(rsPrint.Get_Fields_DateTime("InventoryDate"), "MM/dd/yyyy");
					txtOpID.Text = rsPrint.Get_Fields_String("OpID");
					txtLow.Text = strStartPlate;
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					txtHigh.Text = rsPrint.Get_Fields_String("FormattedInventory");
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					txtStatus.Text = rsPrint.Get_Fields_String("Status");
					if (FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) == 0)
					{
						txtGroup.Text = "All";
					}
					else
					{
						txtGroup.Text = rsPrint.Get_Fields_String("Group");
					}
					txtCount.Text = Difference.ToString();
					boolDidSection = true;
					eArgs.EOF = false;
					return;
				}
			}
			txtType.Text = "";
			txtDate.Text = "";
			txtOpID.Text = "";
			txtLow.Text = "";
			txtHigh.Text = "";
			txtStatus.Text = "";
			txtGroup.Text = "";
			txtCount.Text = "";
			while (!rsPrint.EndOfFile())
			{
				if (strPrefix.Length > FCConvert.ToString(rsPrint.Get_Fields_String("prefix")).Length && Strings.Right(strPrefix, 1) == "0")
				{
					strPrefix = Strings.Left(strPrefix, (strPrefix.Length - (strPrefix.Length - FCConvert.ToString(rsPrint.Get_Fields_String("prefix")).Length)));
				}
				if (FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number")) == lngLast && (strPrefix == FCConvert.ToString(rsPrint.Get_Fields_String("prefix")) && strSuffix == FCConvert.ToString(rsPrint.Get_Fields_String("Suffix"))) && intGroup == FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")))
				{
					lngLast += 1;
				}
				else if (FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number")) == lngLast && fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("prefix")) == true && fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("Suffix")) == true && intGroup == FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")))
				{
					lngLast += 1;
				}
				else
				{
					lines += 1;
					rsPrint.MovePrevious();
					Difference = (rsPrint.Get_Fields_Int32("Number") - lngFirst) + 1;
					txtType.Text = Strings.Mid(FCConvert.ToString(rsPrint.Get_Fields("Code")), 4, 2);
					txtDate.Text = Strings.Format(rsPrint.Get_Fields_DateTime("InventoryDate"), "MM/dd/yyyy");
					txtOpID.Text = rsPrint.Get_Fields_String("OpID");
					txtLow.Text = strStartPlate;
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					txtHigh.Text = rsPrint.Get_Fields_String("FormattedInventory");
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					txtStatus.Text = rsPrint.Get_Fields_String("Status");
					if (FCConvert.ToInt32(rsPrint.Get_Fields_Int16("Group")) == 0)
					{
						txtGroup.Text = "All";
					}
					else
					{
						txtGroup.Text = rsPrint.Get_Fields_String("Group");
					}
					txtCount.Text = FCConvert.ToString(Difference);
					rsPrint.MoveNext();
					lngFirst = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
					lngLast = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
					intGroup = FCConvert.ToInt16(rsPrint.Get_Fields_Int16("Group"));
					if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("prefix")) == true)
						strPrefix = "";
					else
						strPrefix = FCConvert.ToString(rsPrint.Get_Fields_String("prefix"));
					if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("Suffix")) == true)
						strSuffix = "";
					else
						strSuffix = FCConvert.ToString(rsPrint.Get_Fields_String("Suffix"));
					strOriginalPrefix = strPrefix;
					strStartPlate = FCConvert.ToString(rsPrint.Get_Fields_String("FormattedInventory"));
					// .MovePrevious
					eArgs.EOF = false;
					return;
				}
				rsPrint.MoveNext();
				lines = lines % 60;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int Index;
			// vbPorter upgrade warning: intAnswer As int	OnWrite(DialogResult)
			DialogResult intAnswer = DialogResult.None;
			// vbPorter upgrade warning: strMonth As string	OnWrite(int, string)
			string strMonth = "";
			string Month = "";
			this.Document.Printer.PrinterName = frmInventory.InstancePtr.InventoryPrinter;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label11.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label8.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm tt");
			Index = frmInventory.InstancePtr.intIndex;
			switch (Index)
			{
				case 0:
					{
						// TitleType = "MVR3 Forms"
						if (frmInventory.InstancePtr.intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Number";
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no MVR3 forms in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 1:
					{
						if (frmInventory.InstancePtr.lstPlates.SelectedIndex != -1)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected plates?", "Print Selected Plates?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = Trim(Mid(lstPlates.List(lstPlates.ListIndex), 3, Len(lstPlates.List(lstPlates.ListIndex)) - 2)) & " Plates"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE (Code = 'PXD" + Strings.Mid(frmInventory.InstancePtr.lstPlates.Items[frmInventory.InstancePtr.lstPlates.SelectedIndex].Text, 1, 2) + "' OR Code = 'PXS" + Strings.Mid(frmInventory.InstancePtr.lstPlates.Items[frmInventory.InstancePtr.lstPlates.SelectedIndex].Text, 1, 2) + "') AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE (Code = 'PXD" + Strings.Mid(frmInventory.InstancePtr.lstPlates.Items[frmInventory.InstancePtr.lstPlates.SelectedIndex].Text, 1, 2) + "' OR Code = 'PXS" + Strings.Mid(frmInventory.InstancePtr.lstPlates.Items[frmInventory.InstancePtr.lstPlates.SelectedIndex].Text, 1, 2) + "') AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
							}
							else
							{
								// TitleType = "Plates"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
								}
							}
						}
						else
						{
							// TitleType = "Plates"
							if (frmInventory.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'PX%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "substring(Code, 4, 2), suffix, Prefix + convert(nvarchar, Number) + Suffix";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Plates in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 2:
					{
						if (frmInventory.InstancePtr.lstItems.SelectedIndex != -1)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Double Year Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYD" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYD" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Double Year Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Double Year Stickers"
							if (frmInventory.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYD%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Double Year Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 3:
					{
						if (frmInventory.InstancePtr.lstItems.SelectedIndex != -1)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								strMonth = FCConvert.ToString(frmInventory.InstancePtr.lstItems.SelectedIndex + 1);
								if (Conversion.Val(strMonth) < 10)
								{
									strMonth = "0" + strMonth;
								}
								// TitleType = lstItems.List(lstItems.ListIndex) & " Double Month Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMD" + strMonth + "'AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMD" + strMonth + "'AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Double Month Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Double Month Stickers"
							if (frmInventory.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMD%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Double Month Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 4:
					{
						if (frmInventory.InstancePtr.lstItems.SelectedIndex != -1)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Single Year Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYS" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYS" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Single Year Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Single Year Stickers"
							if (frmInventory.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYS%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Single Year Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 5:
					{
						if (frmInventory.InstancePtr.lstItems.SelectedIndex != -1)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								strMonth = FCConvert.ToString(frmInventory.InstancePtr.lstItems.SelectedIndex + 1);
								if (Conversion.Val(strMonth) < 10)
								{
									strMonth = "0" + strMonth;
								}
								// TitleType = lstItems.List(lstItems.ListIndex) & " Single Month Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMS" + strMonth + "'AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SMS" + strMonth + "'AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Single Month Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Single Month Stickers"
							if (frmInventory.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SMS%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Single Month Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 6:
					{
						if (frmInventory.InstancePtr.lstItems.SelectedIndex != -1)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Decals"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'DYS" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'DYS" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Decals"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Decals"
							if (frmInventory.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'D%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Decals in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 7:
					{
						// TitleType = "Boosters"
						if (frmInventory.InstancePtr.intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'BX' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'BX' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Number";
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Booster Plates in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 8:
					{
						// TitleType = "Boosters"
						if (frmInventory.InstancePtr.intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Number";
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Booster Plates in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
				case 9:
					{
						if (frmInventory.InstancePtr.lstItems.SelectedIndex != -1)
						{
							intAnswer = MessageBox.Show("Would you like a printout of just the selected stickers?", "Print Selected Stickers?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (intAnswer == DialogResult.Yes)
							{
								// TitleType = "20" & Mid(lstItems.List(lstItems.ListIndex), 1, 2) & " Single Year Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYC" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code = 'SYC" + Strings.Mid(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text, 1, 2) + "'AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
							else
							{
								// TitleType = "Single Year Stickers"
								if (frmInventory.InstancePtr.intSelectedGroup == 0)
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
								else
								{
									strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
								}
							}
						}
						else
						{
							// TitleType = "Single Year Stickers"
							if (frmInventory.InstancePtr.intSelectedGroup == 0)
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' AND Status = 'A' ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
							else
							{
								strSQLItems = "SELECT * FROM Inventory WHERE Code like 'SYC%' AND Status = 'A' AND [Group] = " + FCConvert.ToString(frmInventory.InstancePtr.intSelectedGroup) + " ORDER by " + frmInventory.InstancePtr.strSortBy + "Code, Number";
							}
						}
						rsPrint.OpenRecordset(strSQLItems);
						if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
						{
							rsPrint.MoveLast();
							rsPrint.MoveFirst();
							PrintRoutine();
						}
						else
						{
							MessageBox.Show("There are no Combination Stickers in Inventory", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							this.Cancel();
						}
						break;
					}
			}
			//end switch
		}

		private void PrintRoutine()
		{
			if (rsPrint.EndOfFile() != true && rsPrint.BeginningOfFile() != true)
			{
				rsPrint.MoveLast();
				rsPrint.MoveFirst();
				lngFirst = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
				lngLast = FCConvert.ToInt32(rsPrint.Get_Fields_Int32("Number"));
				intGroup = FCConvert.ToInt16(rsPrint.Get_Fields_Int16("Group"));
				if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("prefix")) == true)
					strPrefix = "";
				else
					strPrefix = FCConvert.ToString(rsPrint.Get_Fields_String("prefix"));
				if (fecherFoundation.FCUtils.IsNull(rsPrint.Get_Fields_String("Suffix")) == true)
					strSuffix = "";
				else
					strSuffix = FCConvert.ToString(rsPrint.Get_Fields_String("Suffix"));
				strOriginalPrefix = strPrefix;
				strStartPlate = FCConvert.ToString(rsPrint.Get_Fields_String("FormattedInventory"));
				TB = 3;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblHeading.Text = frmInventory.InstancePtr.strHeading;
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void Init(ref string InvPrinter, bool blnAllInventory = false)
		{
			blnAll = blnAllInventory;
			modDuplexPrinting.DuplexPrintReport(this, InvPrinter);
		}

		private void rptInventory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptInventory properties;
			//rptInventory.Caption	= "Inventory Report";
			//rptInventory.Icon	= "rptInventory.dsx":0000";
			//rptInventory.Left	= 0;
			//rptInventory.Top	= 0;
			//rptInventory.Width	= 11880;
			//rptInventory.Height	= 8595;
			//rptInventory.StartUpPosition	= 3;
			//rptInventory.SectionData	= "rptInventory.dsx":058A;
			//End Unmaped Properties
		}
	}
}
