//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmMultiplePlates.
	/// </summary>
	partial class frmMultiplePlates
	{
		public fecherFoundation.FCGrid vsVehicles;
		public fecherFoundation.FCLabel lblHeader;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.vsVehicles = new fecherFoundation.FCGrid();
            this.lblHeader = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 368);
            this.BottomPanel.Size = new System.Drawing.Size(667, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsVehicles);
            this.ClientArea.Controls.Add(this.lblHeader);
            this.ClientArea.Size = new System.Drawing.Size(667, 308);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(667, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(168, 30);
            this.HeaderText.Text = "Select Vehicle";
            // 
            // vsVehicles
            // 
            this.vsVehicles.AllowSelection = false;
            this.vsVehicles.AllowUserToResizeColumns = false;
            this.vsVehicles.AllowUserToResizeRows = false;
            this.vsVehicles.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsVehicles.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorBkg = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorSel = System.Drawing.Color.Empty;
            this.vsVehicles.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsVehicles.Cols = 6;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsVehicles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsVehicles.ColumnHeadersHeight = 30;
            this.vsVehicles.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsVehicles.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsVehicles.DragIcon = null;
            this.vsVehicles.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsVehicles.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsVehicles.ExtendLastCol = true;
            this.vsVehicles.FixedCols = 0;
            this.vsVehicles.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.FrozenCols = 0;
            this.vsVehicles.GridColor = System.Drawing.Color.Empty;
            this.vsVehicles.GridColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.Location = new System.Drawing.Point(30, 65);
            this.vsVehicles.Name = "vsVehicles";
            this.vsVehicles.OutlineCol = 0;
            this.vsVehicles.ReadOnly = true;
            this.vsVehicles.RowHeadersVisible = false;
            this.vsVehicles.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsVehicles.RowHeightMin = 0;
            this.vsVehicles.Rows = 1;
            this.vsVehicles.ScrollTipText = null;
            this.vsVehicles.ShowColumnVisibilityMenu = false;
            this.vsVehicles.ShowFocusCell = false;
            this.vsVehicles.Size = new System.Drawing.Size(608, 221);
            this.vsVehicles.StandardTab = true;
            this.vsVehicles.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsVehicles.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsVehicles.TabIndex = 1;
            this.vsVehicles.DoubleClick += new System.EventHandler(this.vsVehicles_DblClick);
            // 
            // lblHeader
            // 
            this.lblHeader.Location = new System.Drawing.Point(30, 30);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(608, 15);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MainMenu1
            // 
            this.MainMenu1.Name = null;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(272, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(102, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmMultiplePlates
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(667, 476);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmMultiplePlates";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Select Vehicle";
            this.Load += new System.EventHandler(this.frmMultiplePlates_Load);
            this.Activated += new System.EventHandler(this.frmMultiplePlates_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMultiplePlates_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}