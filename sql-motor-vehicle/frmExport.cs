﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using System.IO;

namespace TWMV0000
{
	public partial class frmExport : BaseForm
	{
		public frmExport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmExport InstancePtr
		{
			get
			{
				return (frmExport)Sys.GetInstance(typeof(frmExport));
			}
		}

		protected frmExport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();

		private void frmExport_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmExport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExport properties;
			//frmExport.FillStyle	= 0;
			//frmExport.ScaleWidth	= 5880;
			//frmExport.ScaleHeight	= 3810;
			//frmExport.LinkTopic	= "Form2";
			//frmExport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmExport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			string strSQL = "";
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			string strDirectory = "";
			string temp = "";
			if (cmbExciseTaxDateRange.Text != "All")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
			}
			if (cmbVehicles.Text == "Transactions")
			{
				strSQL = "SELECT c.*, p.FirstName as Party1FirstName, Left(p.MiddleName + ' ',1) as Party1MI, p.LastName as Party1LastName, p.Designation as Party1Designation, q.FirstName as Party2FirstName, Left(q.MiddleName + ' ',1) as Party2MI, q.LastName as Party2LastName, q.Designation as Party2Designation, r.FirstName as Party3FirstName, Left(r.MiddleName + ' ',1) as Party3MI, r.LastName as Party3LastName, r.Designation as Party3Designation FROM ActivityMaster as c LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE ";
			}
			else
			{
				strSQL = "SELECT c.*, p.FirstName as Party1FirstName, Left(p.MiddleName + ' ',1) as Party1MI, p.LastName as Party1LastName, p.Designation as Party1Designation, q.FirstName as Party2FirstName, Left(q.MiddleName + ' ',1) as Party2MI, q.LastName as Party2LastName, q.Designation as Party2Designation, r.FirstName as Party3FirstName, Left(r.MiddleName + ' ',1) as Party3MI, r.LastName as Party3LastName, r.Designation as Party3Designation FROM Master as c LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " + MotorVehicle.Statics.rsVehicles.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE ";
			}
			if (cmbExciseTaxDateRange.Text == "Effective Date Range")
			{
				strSQL += "(EffectiveDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtStartDate.Text)) + "' AND EffectiveDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtEndDate.Text)) + "') AND ";
			}
			else if (cmbExciseTaxDateRange.Text == "Excise Tax Date Range")
			{
				strSQL += "(ExciseTaxDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtStartDate.Text)) + "' AND ExciseTaxDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtEndDate.Text)) + "') AND ";
			}
			//FC:FINAL:MSH - i.issue #1886: comparing with wrong value
			//else if (cmbExciseTaxDateRange.Text == "Excise Tax Date Range")
			else if (cmbExciseTaxDateRange.Text == "Expiration Date Range")
			{
				strSQL += "(ExpireDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtStartDate.Text)) + "' AND ExpireDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(txtEndDate.Text)) + "') AND ";
			}
			if (cmbExciseTaxDateRange.Text == "All")
			{
				if (cmbExpDate.Text == "Plate")
				{
					strSQL = Strings.Left(strSQL, strSQL.Length - 7) + " ORDER BY Plate";
				}
				else
				{
					strSQL = Strings.Left(strSQL, strSQL.Length - 7) + " ORDER BY ExpireDate, Plate";
				}
			}
			else
			{
				if (cmbExpDate.Text == "Plate")
				{
					strSQL = Strings.Left(strSQL, strSQL.Length - 5) + " ORDER BY Plate";
				}
				else
				{
					strSQL = Strings.Left(strSQL, strSQL.Length - 5) + " ORDER BY ExpireDate, Plate";
				}
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				strDirectory = Environment.CurrentDirectory;
				fecherFoundation.Information.Err().Clear();
				
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Exporting Data", true, rsInfo.RecordCount(), true);
				//FC:FINAL:MSH - i.issue #1859: use value from combobox as name for temp file
				temp = cmbVehicles.Text.Replace(' ', '_') + "_Export.csv";
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, temp, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				for (counter = 1; counter <= (rsInfo.FieldsCount - 1); counter++)
				{
					if (counter < rsInfo.FieldsCount - 1)
					{
						FCFileSystem.Write(1, rsInfo.Get_FieldsIndexName(counter));
					}
					else
					{
						FCFileSystem.WriteLine(1, rsInfo.Get_FieldsIndexName(counter));
					}
				}
				do
				{
					for (counter = 1; counter <= (rsInfo.FieldsCount - 1); counter++)
					{
						if (counter < rsInfo.FieldsCount - 1)
						{
							//FC:FINAL:MSH - i.issue #1885: check type of value and get short datetime format if value is DateTime
							//FCFileSystem.Write(1, fecherFoundation.Strings.Trim(rsInfo.Get_Fields(rsInfo.Get_FieldsIndexName(counter)).ToString()));
							object value = rsInfo.Get_Fields(rsInfo.Get_FieldsIndexName(counter));
							if (value is DateTime)
							{
								//FC:FINAL:DDU:#2301 - show time too when needed
								FCFileSystem.Write(1, ((DateTime)value).ToString("M/d/yyyy H:mm"));
							}
							else
							{
								FCFileSystem.Write(1, fecherFoundation.Strings.Trim(FCConvert.ToString(value)));
							}
						}
						else
						{
							//FC:FINAL:MSH - i.issue #1885: check type of value and get short datetime format if value is DateTime
							//FCFileSystem.WriteLine(1, fecherFoundation.Strings.Trim(rsInfo.Get_Fields(rsInfo.Get_FieldsIndexName(counter)).ToString()));
							object value = rsInfo.Get_Fields(rsInfo.Get_FieldsIndexName(counter));
							if (value is DateTime)
							{
								//FC:FINAL:DDU:#2301 - show time too when needed
								FCFileSystem.Write(1, ((DateTime)value).ToString("M/d/yyyy H:mm"));
							}
							else
							{
								FCFileSystem.WriteLine(1, fecherFoundation.Strings.Trim(FCConvert.ToString(value)));
							}
						}
					}
					frmWait.InstancePtr.IncrementProgress();
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				frmWait.InstancePtr.Unload();
				//FC:FINAL:MSH - i.issue #1859: close file when filling completed
				FCFileSystem.FileClose(1);
				//FC:FINAL:MSH - i.issue #1859: send file to client
				using (var stream = new FileStream(Path.Combine(FCFileSystem.Statics.UserDataFolder, temp), FileMode.Open))
				{
					FCUtils.Download(stream, temp);
				}
				MessageBox.Show("Export complete.", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = false;
			lblTo.Enabled = false;
			txtStartDate.Enabled = false;
			txtEndDate.Enabled = false;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
		}

		private void optEffectiveDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			lblTo.Enabled = true;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			txtStartDate.Focus();
		}

		private void optExciseTaxDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			lblTo.Enabled = true;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			txtStartDate.Focus();
		}

		private void optExpirationDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			lblTo.Enabled = true;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			txtStartDate.Focus();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmbExciseTaxDateRange_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbExciseTaxDateRange.Text == "Excise Tax Date Range")
			{
				optExciseTaxDateRange_CheckedChanged(sender, e);
			}
			else if (cmbExciseTaxDateRange.Text == "Effective Date Range")
			{
				optEffectiveDateRange_CheckedChanged(sender, e);
			}
			else if (cmbExciseTaxDateRange.Text == "Expiration Date Range")
			{
				optExpirationDateRange_CheckedChanged(sender, e);
			}
			else if (cmbExciseTaxDateRange.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
		}
	}
}
