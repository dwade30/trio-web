//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.Drawing;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptProcessDetail.
	/// </summary>
	public partial class rptProcessDetail : BaseSectionReport
	{
        private bool processAll = false;

		public rptProcessDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Rapid Renewal Detail Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptProcessDetail InstancePtr
		{
			get
			{
				return (rptProcessDetail)Sys.GetInstance(typeof(rptProcessDetail));
			}
		}

		protected rptProcessDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptProcessDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int FoundCount;
		int NotFoundCount;
		int BadMileageCount;
		int BadMVR3Count;
		int BadExpireCount;
		int BadMilCount;
		double total;
		int PageCounter;
		int counter;
		string strDownloadPath;
		int intProcessRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!checkEOF())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				FCFileSystem.FileClose(2);
			}
		}

		private bool checkEOF()
		{
			bool checkEOF = false;
			CheckAgain:
			;
			if (counter <= FCFileSystem.LOF(2) / Marshal.SizeOf(MotorVehicle.Statics.RRInfoRecord))
			{
				checkEOF = false;
			}
			else
			{
				if (processAll)
				{
					if (intProcessRow < menuRapidRenewal.InstancePtr.vsDetail.Rows - 1)
					{
						FCFileSystem.FileClose(2);
						intProcessRow += 1;
						counter = 1;
						FCFileSystem.FileOpen(2, strDownloadPath + menuRapidRenewal.InstancePtr.vsDetail.TextMatrix(intProcessRow, 1), OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.RRInfoRecord));
						goto CheckAgain;
					}
					else
					{
						checkEOF = true;
					}
				}
				else
				{
					checkEOF = true;
				}
			}
			return checkEOF;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			counter = 1;
			intProcessRow = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string rec = "";
			clsDRWrapper rs = new clsDRWrapper();
			string Class;
			string plate;
			bool BadMileage = false;
			bool BadMVR3 = false;
			bool BadExpire = false;
			bool BadMil = false;
			string strLine;
			string OldExpirationDate = "";
			string OldMVR3 = "";
			string OldMileage = "";
			string OldMil = "";
			string NewExpirationDate = "";
			string NewMVR3 = "";
			string NewMileage = "";
			// vbPorter upgrade warning: NewMil As string	OnWrite
			string NewMil = "";
			string tempLive = "";
			// vbPorter upgrade warning: intMil As int	OnRead(string)
			int intMil = 0;
			string strippedPlate;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label33.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label32.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			rtfWarning.Visible = false;
			txtNotUpdated.Visible = false;
			if (menuRapidRenewal.InstancePtr.LiveFlag)
			{
				tempLive = "Live";
			}
			else
			{
				tempLive = "Test";
			}
			lblMode.Text = "Mode: " + tempLive;
			FCFileSystem.FileGet(2, ref MotorVehicle.Statics.RRInfoRecord, counter);
			counter += 1;
			strLine = Strings.StrDup(80, " ");
			Class = fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Class);
			plate = fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.plate);
			strippedPlate = fecherFoundation.Strings.Trim(plate).Replace("&", "").Replace(" ", "").Replace("-", "");
			rs.OpenRecordset("SELECT * FROM Master WHERE Class = '" + Class + "' AND PlateStripped = '" + strippedPlate + "' AND Status <> 'T' AND Inactive = 0");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				rs.Edit();

                var intialFee = MotorVehicle.GetInitialPlateFees(Class, plate,
                    rs.Get_Fields_Boolean("TrailerVanity"),
                    rs.Get_Fields_Boolean("TwoYear"));

                decimal platefee = 0;
                if (MotorVehicle.IsSpecialtyPlate(Class))
                {
                    platefee = MotorVehicle.GetSpecialtyPlateRenewalFee(Class);
				}

				FoundCount += 1;
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.ExciseTax) != "")
				{
					rs.Set_Fields("ExciseTaxFull", Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.ExciseTax)) / 100);
					rs.Set_Fields("ExciseTaxCharged", Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.ExciseTax)) / 100);
				}
				else
				{
					rs.Set_Fields("ExciseTaxFull", 0);
					rs.Set_Fields("ExciseTaxCharged", 0);
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.BaseValue) != "")
				{
					rs.Set_Fields("BasePrice", Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.BaseValue)) / 100);
				}
				else
				{
					rs.Set_Fields("BasePrice", 0);
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MilRate) != "")
				{
					if (FCConvert.ToDouble(FCConvert.ToInt32(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MilRate)) / 1000) == 2.4)
					{
						intMil = 1;
					}
					else if (FCConvert.ToDouble(FCConvert.ToInt32(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MilRate)) / 1000) == 1.75)
					{
						intMil = 2;
					}
					else if (FCConvert.ToDouble(FCConvert.ToInt32(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MilRate)) / 1000) == 1.35)
					{
						intMil = 3;
					}
					else if (FCConvert.ToDouble(FCConvert.ToInt32(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MilRate)) / 1000) == 1)
					{
						intMil = 4;
					}
					else if (FCConvert.ToDouble(FCConvert.ToInt32(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MilRate)) / 1000) == 0.65)
					{
						intMil = 5;
					}
					else if (FCConvert.ToDouble(FCConvert.ToInt32(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MilRate)) / 1000) == 0.4)
					{
						intMil = 6;
					}
					if (intMil != Conversion.Val(rs.Get_Fields_Int32("MillYear")) + 1 && (intMil != 6 || Conversion.Val(rs.Get_Fields_Int32("MillYear")) != 6))
					{
						OldMil = FCConvert.ToString(rs.Get_Fields_Int32("MillYear"));
						NewMil = FCConvert.ToString(intMil);
						BadMil = true;
					}
					else
					{
						BadMil = false;
					}
					rs.Set_Fields("MillYear", intMil);
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.VIN) != "")
				{
					rs.Set_Fields("Vin", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.VIN));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Year) != "" && Information.IsNumeric(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Year)))
				{
					rs.Set_Fields("Year", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Year));
					if (FCConvert.ToString(rs.Get_Fields("Year")).Length == 2)
					{
						if (Conversion.Val(Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2)) < Conversion.Val(rs.Get_Fields("Year")))
						{
							rs.Set_Fields("Year", "19" + rs.Get_Fields("Year"));
						}
						else
						{
							rs.Set_Fields("Year", "20" + rs.Get_Fields("Year"));
						}
					}
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.make) != "")
				{
					rs.Set_Fields("make", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.make));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.model) != "")
				{
					rs.Set_Fields("model", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.model));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Style) != "")
				{
					rs.Set_Fields("Style", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Style));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Color1) != "")
				{
					rs.Set_Fields("color1", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Color1));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Color2) != "")
				{
					rs.Set_Fields("color2", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Color2));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Axles) != "")
				{
					rs.Set_Fields("Axles", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Axles))));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.NetWeight) != "")
				{
					rs.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.NetWeight))));
				}
				else
				{
					rs.Set_Fields("NetWeight", 0);
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.RegisteredWeight) != "")
				{
					rs.Set_Fields("registeredWeightNew", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.RegisteredWeight))));
				}
				else
				{
					rs.Set_Fields("registeredWeightNew", 0);
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Fuel) != "")
				{
					rs.Set_Fields("Fuel", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Fuel));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Address1) != "")
				{
					rs.Set_Fields("Address", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Address1));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Address2) != "")
				{
					rs.Set_Fields("Address2", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Address2));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.City) != "")
				{
					rs.Set_Fields("City", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.City));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.State) != "")
				{
					rs.Set_Fields("State", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.State));
				}
				if (fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.Zip, 1, 5)) != "")
				{
					rs.Set_Fields("Zip", fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.Zip, 1, 5)));
				}
				if (fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.Zip, 6, 4)) != "")
				{
					rs.Set_Fields("Zip4", fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.Zip, 6, 4)));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.TransactionDate) != "")
				{
					rs.Set_Fields("DateUpdated", fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 7, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 1, 4)));
					rs.Set_Fields("ExcisePaidDate", fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 7, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 1, 4)));
					rs.Set_Fields("ExciseTaxDate", fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 7, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.TransactionDate, 1, 4)));
				}
				if (Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.OldMVR3)) != rs.Get_Fields_Int32("MVR3"))
				{
					OldMVR3 = FCConvert.ToString(rs.Get_Fields_Int32("MVR3"));
					NewMVR3 = fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MVR3);
					BadMVR3 = true;
				}
				else
				{
					BadMVR3 = false;
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.OldMVR3) != "")
				{
					rs.Set_Fields("OldMVR3", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.OldMVR3));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MVR3) != "")
				{
					rs.Set_Fields("MVR3", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.MVR3));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.EffectiveDate) != "")
				{
					rs.Set_Fields("EffectiveDate", fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.EffectiveDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.EffectiveDate, 7, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.EffectiveDate, 1, 4)));
				}
				// kk06012016 tromvs-60   Comparing string to date is not working - add Cdate()
				if (fecherFoundation.DateAndTime.DateValue(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 7, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 1, 4)).ToOADate() < rs.Get_Fields_DateTime("ExpireDate").ToOADate())
				{
					OldExpirationDate = FCConvert.ToString(rs.Get_Fields_DateTime("ExpireDate"));
					NewExpirationDate = fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 7, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 1, 4));
					BadExpire = true;
				}
				else
				{
					BadExpire = false;
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.ExpirationDate) != "")
				{
					rs.Set_Fields("ExpireDate", fecherFoundation.Strings.Trim(Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 5, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 7, 2) + "/" + Strings.Mid(MotorVehicle.Statics.RRInfoRecord.ExpirationDate, 1, 4)));
				}
				if (Conversion.Val(fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Mileage)) < rs.Get_Fields_Int32("Odometer"))
				{
					OldMileage = FCConvert.ToString(rs.Get_Fields_Int32("Odometer"));
					NewMileage = fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Mileage);
					BadMileage = true;
				}
				else
				{
					BadMileage = false;
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Mileage) != "")
				{
					rs.Set_Fields("Odometer", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.Mileage));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.ResidenceCode) != "")
				{
					rs.Set_Fields("ResidenceCode", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.ResidenceCode));
				}
				if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.stickernumber) != "")
				{
					rs.Set_Fields("YearStickerNumber", fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRInfoRecord.stickernumber));
				}
				rs.Set_Fields("MonthStickerNumber", 0);
				rs.Set_Fields("OpID", "R/R");
				rs.Set_Fields("Status", "A");
				rs.Set_Fields("StickerFee", 0);
				rs.Set_Fields("TransferFee", 0);
				rs.Set_Fields("AgentFee", 0);
				rs.Set_Fields("SalesTax", 0);
				rs.Set_Fields("TitleFee", 0);
				rs.Set_Fields("ExciseCreditFull", 0);
				rs.Set_Fields("ExciseCreditUsed", 0);
				rs.Set_Fields("ExciseTransferCharge", 0);
				rs.Set_Fields("ExciseCreditMVR3", 0);
				rs.Set_Fields("TitleDone", 0);
				rs.Set_Fields("UseTaxDone", 0);
				// .Fields("BoosterKey") = 0
				rs.Set_Fields("ETO", 0);
				rs.Set_Fields("InfoCodes", "");
				rs.Set_Fields("InfoMessage", "");
				rs.Set_Fields("ECorrectInfoCodes", "");
				rs.Set_Fields("ECorrectInfoMessage", "");
				rs.Set_Fields("DoubleCTANumber", "");
				rs.Set_Fields("GiftCertificateNumber", 0);
                rs.Set_Fields("GiftCertOrVoucher", "");
				rs.Set_Fields("GIFTCERTIFICATEAMOUNT", 0);
				rs.Set_Fields("StatePaid", 0);
				rs.Set_Fields("LocalPaid", 0);
				rs.Set_Fields("RegRateCharge", 0);
				rs.Set_Fields("TransferCreditFull", 0);
				rs.Set_Fields("TransferCreditUsed", 0);
				rs.Set_Fields("DuplicateRegistrationFee", 0);
				rs.Set_Fields("ReservePlate", 0);
				rs.Set_Fields("ReplacementFee", 0);
				// kk01282016 tromv-979  Leaving the fees in these fields is causing extra fees in StatePaid on corrections
				// Either clear them or account for them in StatePaid
				rs.Set_Fields("RegHalf", false);
				rs.Set_Fields("ExciseHalfRate", false);
				rs.Set_Fields("RegRateFull", 0);
				rs.Set_Fields("InitialFee", intialFee);
				rs.Set_Fields("OutOfRotation", 0);
				rs.Set_Fields("PlateFeeNew", 0);
				rs.Set_Fields("PlateFeeReReg", platefee);
				rs.Set_Fields("TransactionType", "RRR");
				// put these in just so the Master record looks correct
				rs.Set_Fields("ReReg", "Y");
				rs.Set_Fields("PlateType", "S");
				rs.Set_Fields("PrintPriorities", "");
                rs.Set_Fields("ReasonCodes", "");
				total += Conversion.Val(rs.Get_Fields_Decimal("ExciseTaxCharged"));
				txtPlate.Text = Class + " " + Strings.Format(plate, "!@@@@@@@@");
				txtOwner.Text = fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rs.Get_Fields_Int32("PartyID1")));
				txtExp.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("ExpireDate")));
				txtExcise.Text = Strings.Format(fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_Decimal("ExciseTaxCharged"))), "#,##0.00");
				txtYMM.Text = rs.Get_Fields("Year") + " " + rs.Get_Fields_String("make") + " " + rs.Get_Fields_String("model");
				rtfWarning.Text = "";
				// kk06012016 tromvs-60  Move Exp Date warning up and add Not Updated text. Decrease Found Count
				if (BadExpire)
				{
					rtfWarning.Visible = true;
					//FC:FINAL:DSE WordWrapping not working in RichTextBox
					//rtfWarning.Text = "Expiration Date:  " + OldExpirationDate + " TO " + NewExpirationDate + "\r\n";
					rtfWarning.SetHtmlText("Expiration Date:  " + OldExpirationDate + " TO " + NewExpirationDate + "\r\n");
					txtNotUpdated.Visible = true;
					txtNotUpdated.Text = "Not Updated";
					BadExpireCount += 1;
					FoundCount -= 1;
				}
				if (BadMVR3)
				{
					rtfWarning.Visible = true;
					//FC:FINAL:DSE WordWrapping not working in RichTextBox
					//rtfWarning.Text = rtfWarning.Text + "MVR3: " + OldMVR3 + " TO " + NewMVR3 + "\r\n";
					rtfWarning.SetHtmlText(rtfWarning.Text + "MVR3: " + OldMVR3 + " TO " + NewMVR3 + "\r\n");
					BadMVR3Count += 1;
				}
				if (BadMileage)
				{
					rtfWarning.Visible = true;
					//FC:FINAL:DSE WordWrapping not working in RichTextBox
					//rtfWarning.Text = rtfWarning.Text + "Mileage: " + OldMileage + " TO " + NewMileage + "\r\n";
					rtfWarning.SetHtmlText(rtfWarning.Text + "Mileage: " + OldMileage + " TO " + NewMileage + "\r\n");
					BadMileageCount += 1;
				}
				// If BadExpire Then
				// rtfWarning.Visible = True
				// rtfWarning.Text = rtfWarning.Text & "Expiration Date:  " & OldExpirationDate & " TO " & NewExpirationDate & vbNewLine
				// BadExpireCount = BadExpireCount + 1
				// End If
				if (BadMil)
				{
					rtfWarning.Visible = true;
					//FC:FINAL:DSE WordWrapping not working in RichTextBox
					//rtfWarning.Text = rtfWarning.Text + "Mil Year:  " + OldMil + " TO " + NewMil + "\r\n";
					rtfWarning.SetHtmlText(rtfWarning.Text + "Mil Year:  " + OldMil + " TO " + NewMil + "\r\n");
					BadMilCount += 1;
				}
				if (menuRapidRenewal.InstancePtr.LiveFlag && !BadExpire)
				{
					// kk06012016 tromvs-60  Add BadExpire to not update if bad expire date
					rs.Update();
				}
				txtNotFound.Text = "";
			}
			else
			{
				txtPlate.Text = "";
				txtOwner.Text = "";
				txtExp.Text = "";
				txtExcise.Text = "";
				rtfWarning.Text = "";
				txtYMM.Text = "";
				NotFoundCount += 1;
				txtNotFound.Text = Class + " " + plate + " Not Found To Update";
			}
			rs.DisposeOf();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// kk06012016 tromvs-60  Move Exp Date warning up into Not Updated section
			rtf1.Font = new Font("Courier New", rtf1.Font.Size);
			// kk06012016  Make the font fixed width so we can line things up
			rtf1.Text = "Status:" + Strings.Space(11) + "Updated " + Strings.Space(10) + "- " + FCConvert.ToString(FoundCount) + "<br>";
			rtf1.Text = rtf1.Text + Strings.Space(18) + "Not Found " + Strings.Space(8) + "- " + FCConvert.ToString(NotFoundCount) + "<br>";
			rtf1.Text = rtf1.Text + Strings.Space(18) + "Bad Exp Date " + Strings.Space(5) + "- " + FCConvert.ToString(BadExpireCount) + "<br>" + "<br>";
			rtf1.Text = rtf1.Text + "Warnings:" + Strings.Space(9) + "Mileage " + Strings.Space(10) + "- " + FCConvert.ToString(BadMileageCount) + "<br>";
			// rtf1.Text = rtf1.Text & Space(18) & "Exp Date " & Space(9) & "- " & BadExpireCount & vbNewLine
			rtf1.Text = rtf1.Text + Strings.Space(18) + "MVR3 " + Strings.Space(13) + "- " + FCConvert.ToString(BadMVR3Count) + "<br>";
			rtf1.Text = rtf1.Text + Strings.Space(18) + "Mil Year " + Strings.Space(9) + "- " + FCConvert.ToString(BadMilCount) + "\r\n";
			//FC:FINAL:DSE WordWrapping not working in RichTextBox
			rtf1.SetHtmlText(rtf1.Text);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void Init(string strDLPath, bool processAll)
		{
			strDownloadPath = strDLPath;
            this.processAll = processAll;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: true);
		}
    }
}
