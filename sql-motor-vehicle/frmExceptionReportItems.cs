//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmExceptionReportItems : BaseForm
	{
		public frmExceptionReportItems()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			fraAdjustments = new System.Collections.Generic.List<FCFrame>();
            fraAdjustments.AddControlArrayElement(fraAdjustments_0, 0);
            fraAdjustments.AddControlArrayElement(fraAdjustments_1, 1);
            fraAdjustments.AddControlArrayElement(fraAdjustments_2, 2);
			//FC:FINAL:DDU:#2238 - allow only + and - on textbox
			txtMonetaryPlusMinus.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',43,45");
			//FC:FINAL:DDU:#2239 - allow specific characters as in original
			txtNewStickerSD.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',68,100,83,115");
			txtOriginalStickerSD.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',68,100,83,115");
			txtMonthOrYear.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',77,109,89,121");
			txtOriginalStickerNumber.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',48..57");
			txtNewStickerNumber.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',48..57");
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmExceptionReportItems InstancePtr
		{
			get
			{
				return (frmExceptionReportItems)Sys.GetInstance(typeof(frmExceptionReportItems));
			}
		}

		protected frmExceptionReportItems _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsExceptionItems = new clsDRWrapper();
		string EType = "";
		bool QuitButton;
		clsDRWrapper rsInventoryStickerAdjust = new clsDRWrapper();
		clsDRWrapper rsStickerNumber = new clsDRWrapper();
		clsDRWrapper rsStickerClass = new clsDRWrapper();
		clsDRWrapper rsClass = new clsDRWrapper();
		clsDRWrapper rsSubClass = new clsDRWrapper();

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			QuitButton = true;
			Close();
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (cmbType.Text == "Monetary Adjustment")
			{
				if (txtMonetaryReason.Text == "")
				{
					MessageBox.Show("You must enter a reason before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (lstMonetaryCategories.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter a category before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if ((txtMonetaryAmount.Text == ".00" || txtMonetaryAmount.Text == "0.00" || txtMonetaryAmount.Text == "") && lstMonetaryCategories.Items[lstMonetaryCategories.SelectedIndex].ToString() != "SALES TAX - NO FEE")
				{
					MessageBox.Show("You must enter an amount before you can save this exception.", "Enter Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbType.Text == "Sticker Adjustment")
			{
				if (txtStickerReason.Text == "")
				{
					MessageBox.Show("You must enter a reason before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtMonthOrYear.Text == "")
				{
					MessageBox.Show("You must enter a Y or M in the M/Y field before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (lstOriginalStickerClass.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter a Vehicle Class before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtOriginalStickerPlate.Text == "")
				{
					MessageBox.Show("You must enter a Vehicle Plate before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (lstOriginalMonthYear.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter a Month/Year for the original sticker before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (lstNewMonthYear.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter a Month/Year for the new sticker before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtOriginalStickerSD.Text == "")
				{
					MessageBox.Show("You must indicate whether the original sticker was a Single or Double before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtNewStickerSD.Text == "")
				{
					MessageBox.Show("You must indicate whether the new sticker was a Single or Double before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtOriginalStickerNumber.Text == "")
				{
					MessageBox.Show("You must enter the original sticker number before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtNewStickerNumber.Text == "")
				{
					MessageBox.Show("You must enter the new sticker number before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbType.Text == "Plate Adjustment")
			{
				if (txtPlateReason.Text == "")
				{
					MessageBox.Show("You must enter a reason before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (lstOriginalClass.SelectedIndex == -1 || lstNewClass.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter a class for the original and new plates before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtOriginalPlate.Text == "")
				{
					MessageBox.Show("You must enter the original plate number before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (txtNewPlate.Text == "")
				{
					MessageBox.Show("You must enter the new plate number before you can save this exception.", "Enter Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (fraAdjustments[1].Visible == true)
			{
				if (StickerAdjustment() == false)
				{
					// Exit Sub
				}
			}
			PutValuesInDB();
			QuitButton = false;
			//FC:FINAL:DDU - prevent form to close
			//Close();
			//FC:FINAL:DDU:#2243 - added message box when Save ends
			FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
            //FC:FINAL:BSE #2523 clear form after saving
            this.cmbType.SelectedIndex = -1;
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdFileSaveExit, new System.EventArgs());
		}

		private void frmExceptionReportItems_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.ExceptionReport))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			Support.SendKeys("{TAB}", false);
		}

		private void frmExceptionReportItems_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			else if (KeyAscii == Keys.Escape)
			{
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmExceptionReportItems_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExceptionReportItems properties;
			//frmExceptionReportItems.ScaleWidth	= 9045;
			//frmExceptionReportItems.ScaleHeight	= 6930;
			//frmExceptionReportItems.LinkTopic	= "Form1";
			//End Unmaped Properties
			LoadMonetaryCategories();
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (QuitButton == true)
			{
				QuitButton = false;
				// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
				DialogResult answer;
				answer = MessageBox.Show("Quit without Saving?", "No Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (answer == DialogResult.Yes)
				{
				}
				else
				{
					e.Cancel = true;
				}
			}
			else
			{
			}
		}

		private void lstNewClass_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lstNewSubClass.Clear();
			MotorVehicle.Statics.strSql = "SELECT SystemCode FROM Class WHERE BMVCode = '" + lstNewClass.Text + "'";
			rsSubClass.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsSubClass.EndOfFile() != true && rsSubClass.BeginningOfFile() != true)
			{
				rsSubClass.MoveLast();
				rsSubClass.MoveFirst();
				while (!rsSubClass.EndOfFile())
				{
					lstNewSubClass.AddItem(rsSubClass.Get_Fields_String("SystemCode"));
					rsSubClass.MoveNext();
				}
				lstNewSubClass.SelectedIndex = 0;
			}
			else
			{
				lstNewSubClass.Clear();
			}
		}

		private void lstOriginalClass_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lstOriginalSubClass.Clear();
			MotorVehicle.Statics.strSql = "SELECT SystemCode FROM Class WHERE BMVCode = '" + lstOriginalClass.Text + "'";
			rsSubClass.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsSubClass.EndOfFile() != true && rsSubClass.BeginningOfFile() != true)
			{
				rsSubClass.MoveLast();
				rsSubClass.MoveFirst();
				while (!rsSubClass.EndOfFile())
				{
					lstOriginalSubClass.AddItem(rsSubClass.Get_Fields_String("SystemCode"));
					rsSubClass.MoveNext();
				}
				lstOriginalSubClass.SelectedIndex = 0;
			}
			else
			{
				lstOriginalSubClass.Clear();
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void optType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{ 
            int ii;
			// 
			for (ii = 0; ii <= 2; ii++)
			{
				fraAdjustments[ii].Visible = false;
			}
            //FC:FINAL:BSE #2523 clear form after saving
            if (Index == -1)
            {
                return;
            }
            // ii
            fraAdjustments[Index].Visible = true;
			//Support.ZOrder(fraAdjustments[Index], 0);
			switch (Index)
			{
				case 0:
					{
						EType = "Monetary";
						break;
					}
				case 1:
					{
						EType = "Sticker";
						LoadStickerClass();
						txtOriginalStickerDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
						break;
					}
				case 2:
					{
						EType = "Plate";
						LoadClasses();
						break;
					}
			}
			//end switch
		}

		private void optType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbType.SelectedIndex;
			optType_CheckedChanged(index, sender, e);
		}

		private void txtMonetaryPlusMinus_Enter(object sender, System.EventArgs e)
		{
			txtMonetaryPlusMinus.SelectionLength = 1;
		}

		private void txtMonetaryPlusMinus_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			txtMonetaryPlusMinus.ForeColor = Color.Black;
			switch (KeyAscii)
			{
				case Keys.Execute:
					{
						// plus
						break;
					}
				case Keys.Insert:
					{
						txtMonetaryPlusMinus.ForeColor = Color.Red;
						// minus
						break;
					}
				case Keys.Back:
					{
						// backspace
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMonthOrYear_TextChanged(object sender, System.EventArgs e)
		{
			txtMonthOrYear.Text = fecherFoundation.Strings.UCase(txtMonthOrYear.Text);
			LoadMonthYear(txtMonthOrYear.Text);
		}

		private void txtMonthOrYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.Subtract:
				case Keys.M:
					{
						// M
						break;
					}
				case Keys.F10:
				case Keys.Y:
					{
						// Y
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNewPlate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNewStickerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Tab))
			{
				// backspace  & tab
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
				// numeric
			}
			else if (KeyAscii == Keys.Return)
			{
				// enter
			}
			else
			{
				// Bp = Beep(1000, 250)
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNewStickerSD_TextChanged(object sender, System.EventArgs e)
		{
			txtNewStickerSD.Text = fecherFoundation.Strings.UCase(txtNewStickerSD.Text);
		}

		private void txtNewStickerSD_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.F4:
				case Keys.S:
					{
						// S
						break;
					}
				case Keys.NumPad4:
				case Keys.D:
					{
						// D
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void LoadMonetaryCategories()
		{
			clsDRWrapper rsTSH = new clsDRWrapper();
			rsTSH.OpenRecordset("SELECT * FROM TownSummaryHeadings ORDER BY Heading");
			if (rsTSH.EndOfFile() != true && rsTSH.BeginningOfFile() != true)
			{
				rsTSH.MoveLast();
				rsTSH.MoveFirst();
				lstMonetaryCategories.Clear();
				while (!rsTSH.EndOfFile())
				{
					if (rsTSH.Get_Fields_Int32("ID") != 39)
					{
						lstMonetaryCategories.AddItem(rsTSH.Get_Fields_String("Heading"));
					}
					rsTSH.MoveNext();
				}
			}
		}

		private bool PutValuesInDB()
		{
			bool PutValuesInDB = false;
			clsDRWrapper tempRS = new clsDRWrapper();
			MotorVehicle.Statics.strSql = "SELECT * FROM ExceptionReport";
			rsExceptionItems.OpenRecordset(MotorVehicle.Statics.strSql);
			rsExceptionItems.AddNew();
			if (cmbType.Text == "Monetary Adjustment")
			{
				EType = "5MA";
			}
			else if (cmbType.Text == "Sticker Adjustment")
			{
				EType = "1S";
			}
			else if (cmbType.Text == "Plate Adjustment")
			{
				EType = "2P";
			}
			rsExceptionItems.Set_Fields("Type", EType);
			if (cmbType.Text == "Sticker Adjustment")
			{
				rsExceptionItems.Set_Fields("OriginalStickerNumber", FCConvert.ToString(Conversion.Val(txtOriginalStickerNumber.Text)));
			}
			rsExceptionItems.Set_Fields("OriginalMMYY", txtMonthOrYear.Text);
			if (cmbType.Text == "Sticker Adjustment")
			{
				if (txtMonthOrYear.Text == "M")
				{
					rsExceptionItems.Set_Fields("OriginalMonthYear", (lstOriginalMonthYear.SelectedIndex + 1));
                    rsExceptionItems.Set_Fields("ReplacementMonthYear", (lstNewMonthYear.SelectedIndex + 1));
				}
				else
				{
					rsExceptionItems.Set_Fields("OriginalMonthYear", FCConvert.ToString(Conversion.Val(Strings.Mid(lstOriginalMonthYear.Text, 3, 2))));
                    rsExceptionItems.Set_Fields("ReplacementMonthYear", FCConvert.ToString(Conversion.Val(Strings.Mid(lstNewMonthYear.Text, 3, 2))));
				}
				
			}
			if (cmbType.Text == "Sticker Adjustment")
			{
				rsExceptionItems.Set_Fields("OriginalNumberOfStickers", ((txtOriginalStickerSD.Text == "S") ? 1 : 2));
			}
			else
			{
				rsExceptionItems.Set_Fields("OriginalNumberOfStickers", 2);
			}
			rsExceptionItems.Set_Fields("OriginalDoubleSingle", FCConvert.ToString(Conversion.Val(txtOriginalStickerSD.Text)));
			if (cmbType.Text == "Sticker Adjustment")
			{
				rsExceptionItems.Set_Fields("ReplacementStickerNumber", FCConvert.ToString(Conversion.Val(txtNewStickerNumber.Text)));
			}
			rsExceptionItems.Set_Fields("ReplacementMMYY", txtMonthOrYear.Text);
			if (cmbType.Text == "Sticker Adjustment")
			{
				rsExceptionItems.Set_Fields("ReplacementNumberOfStickers", ((txtNewStickerSD.Text == "S") ? 1 : 2));
			}
			else
			{
				rsExceptionItems.Set_Fields("ReplacementNumberOfStickers", 2);
			}
			rsExceptionItems.Set_Fields("ReplacementDoubleSingle", FCConvert.ToString(Conversion.Val(txtNewStickerSD.Text)));
			if (cmbType.Text == "Sticker Adjustment")
			{
				rsExceptionItems.Set_Fields("OriginalClass", fecherFoundation.Strings.Trim(lstOriginalStickerClass.Text));
			}
			else
			{
				rsExceptionItems.Set_Fields("OriginalClass", fecherFoundation.Strings.Trim(lstOriginalClass.Text));
			}
			rsExceptionItems.Set_Fields("newclass", fecherFoundation.Strings.Trim(lstNewClass.Text));
			if (cmbType.Text == "Sticker Adjustment")
			{
				rsExceptionItems.Set_Fields("OriginalPlate", fecherFoundation.Strings.Trim(txtOriginalStickerPlate.Text));
			}
			else
			{
				rsExceptionItems.Set_Fields("OriginalPlate", fecherFoundation.Strings.Trim(txtOriginalPlate.Text));
				rsExceptionItems.Set_Fields("NewPlate", fecherFoundation.Strings.Trim(txtNewPlate.Text));
			}
			if (cmbType.Text == "Plate Adjustment")
			{
				if (!Information.IsDate(txtPlateAdjustmentDate.Text))
				{
					rsExceptionItems.Set_Fields("ExceptionReportDate", DateTime.Today);
				}
				else
				{
					rsExceptionItems.Set_Fields("ExceptionReportDate", txtPlateAdjustmentDate.Text);
				}
			}
			else
			{
				if (!Information.IsDate(txtOriginalStickerDate.Text))
				{
					rsExceptionItems.Set_Fields("ExceptionReportDate", DateTime.Today);
				}
				else
				{
					rsExceptionItems.Set_Fields("ExceptionReportDate", txtOriginalStickerDate.Text);
				}
			}
			rsExceptionItems.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			if (lstMonetaryCategories.Text != "OVER/SHORT" && lstMonetaryCategories.Text != "")
			{
				tempRS.OpenRecordset("SELECT * FROM TownSummaryHeadings WHERE Heading = '" + lstMonetaryCategories.Text + "'");
				rsExceptionItems.Set_Fields("CategoryAdjusted", strGetCategory(Conversion.Str(tempRS.Get_Fields_Int32("ID"))));
			}
			else
			{
				rsExceptionItems.Set_Fields("CategoryAdjusted", strGetCategory("39"));
			}
			if (EType == "5MA")
			{
				rsExceptionItems.Set_Fields("reason", fecherFoundation.Strings.Trim(txtMonetaryReason.Text));
			}
			else if (EType == "1S")
			{
				rsExceptionItems.Set_Fields("reason", fecherFoundation.Strings.Trim(txtStickerReason.Text));
			}
			else
			{
				rsExceptionItems.Set_Fields("reason", fecherFoundation.Strings.Trim(txtPlateReason.Text));
			}
			rsExceptionItems.Set_Fields("MVR3Number", 0);
			if (txtMonetaryPlusMinus.Text == "+")
			{
				rsExceptionItems.Set_Fields("Fee", txtMonetaryAmount.Text);
			}
			else
			{
				rsExceptionItems.Set_Fields("Fee", FCConvert.ToDecimal(txtMonetaryAmount.Text) * -1);
			}
			//FC:FINAL:MSH - i.issue #1862: in original will be returned an empty string if we try to get a value by a negative index (use .Text property for getting current text)
			//if (FCConvert.ToInt32(rsExceptionItems.Get_Fields("Fee")) == 0 && lstMonetaryCategories.Items[lstMonetaryCategories.SelectedIndex].ToString() == "SALES TAX - NO FEE")
			if (FCConvert.ToInt32(rsExceptionItems.Get_Fields("Fee")) == 0 && lstMonetaryCategories.Text == "SALES TAX - NO FEE")
			{
				rsExceptionItems.Set_Fields("AddOrSubtract", txtMonetaryPlusMinus.Text);
			}
			rsExceptionItems.Set_Fields("MVR3Printed", false);
			rsExceptionItems.Set_Fields("PeriodCloseoutID", 0);
			rsExceptionItems.Set_Fields("TellerCloseoutID", 0);
			rsExceptionItems.Update();
			rsExceptionItems.Reset();
			return PutValuesInDB;
		}

		private void LoadMonthYear(string MY)
		{
			lstNewMonthYear.Clear();
			lstOriginalMonthYear.Clear();
			if (MY == "Y")
			{
				lstNewMonthYear.AddItem((DateTime.Now.Year - 1).ToString());
				lstNewMonthYear.AddItem((DateTime.Now.Year).ToString());
				lstNewMonthYear.AddItem((DateTime.Now.Year + 1).ToString());
				lstNewMonthYear.AddItem((DateTime.Now.Year + 2).ToString());
				lstOriginalMonthYear.AddItem((DateTime.Now.Year - 1).ToString());
				lstOriginalMonthYear.AddItem((DateTime.Now.Year).ToString());
				lstOriginalMonthYear.AddItem((DateTime.Now.Year + 1).ToString());
				lstOriginalMonthYear.AddItem((DateTime.Now.Year + 2).ToString());
			}
			else if (MY == "M")
			{
				lstNewMonthYear.AddItem("January");
				lstNewMonthYear.AddItem("February");
				lstNewMonthYear.AddItem("March");
				lstNewMonthYear.AddItem("April");
				lstNewMonthYear.AddItem("May");
				lstNewMonthYear.AddItem("June");
				lstNewMonthYear.AddItem("July");
				lstNewMonthYear.AddItem("August");
				lstNewMonthYear.AddItem("September");
				lstNewMonthYear.AddItem("October");
				lstNewMonthYear.AddItem("November");
				lstNewMonthYear.AddItem("December");
				lstOriginalMonthYear.AddItem("January");
				lstOriginalMonthYear.AddItem("February");
				lstOriginalMonthYear.AddItem("March");
				lstOriginalMonthYear.AddItem("April");
				lstOriginalMonthYear.AddItem("May");
				lstOriginalMonthYear.AddItem("June");
				lstOriginalMonthYear.AddItem("July");
				lstOriginalMonthYear.AddItem("August");
				lstOriginalMonthYear.AddItem("September");
				lstOriginalMonthYear.AddItem("October");
				lstOriginalMonthYear.AddItem("November");
				lstOriginalMonthYear.AddItem("December");
				lstNewMonthYear.SelectedIndex = 0;
				lstOriginalMonthYear.SelectedIndex = 0;
			}
			else
			{
				lstNewMonthYear.Clear();
				lstOriginalMonthYear.Clear();
			}
		}

		private void txtOriginalStickerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Tab))
			{
				// backspace  & tab
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
				// numeric
			}
			else if (KeyAscii == Keys.Return)
			{
				// enter
			}
			else
			{
				// Bp = Beep(1000, 250)
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtOriginalStickerSD_TextChanged(object sender, System.EventArgs e)
		{
			txtOriginalStickerSD.Text = fecherFoundation.Strings.UCase(txtOriginalStickerSD.Text);
		}

		private void txtOriginalStickerSD_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.F4:
				case Keys.S:
					{
						// S
						break;
					}
				case Keys.NumPad4:
				case Keys.D:
					{
						// D
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void LoadClasses()
		{
			MotorVehicle.Statics.strSql = "SELECT DISTINCT  BMVCode FROM Class ORDER BY BMVCode";
			rsClass.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
			{
				rsClass.MoveLast();
				rsClass.MoveFirst();
				while (!rsClass.EndOfFile())
				{
					lstOriginalClass.AddItem(rsClass.Get_Fields_String("BMVCode"));
					lstNewClass.AddItem(rsClass.Get_Fields_String("BMVCode"));
					rsClass.MoveNext();
				}
				lstOriginalClass.SelectedIndex = 0;
				lstNewClass.SelectedIndex = 0;
			}
			else
			{
				MessageBox.Show("There are no Entries in the Class Table. Call TRIO Support.", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		private void LoadStickerClass()
		{
			lstOriginalStickerClass.Clear();
			MotorVehicle.Statics.strSql = "SELECT DISTINCT BMVCode FROM Class";
			rsStickerClass.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsStickerClass.EndOfFile() != true && rsStickerClass.BeginningOfFile() != true)
			{
				rsStickerClass.MoveLast();
				rsStickerClass.MoveFirst();
				while (!rsStickerClass.EndOfFile())
				{
					lstOriginalStickerClass.AddItem(rsStickerClass.Get_Fields_String("BMVCode"));
					rsStickerClass.MoveNext();
				}
			}
		}

		private bool StickerAdjustment()
		{
			bool StickerAdjustment = false;
			string singledouble = "";
			string stickernumber = "";
			string MY = "";
			string Code;
			// 
			StickerAdjustment = true;
			if (ValidateStickerControls() == false)
			{
				MessageBox.Show(" There is a problem with some of the information. Please check for accurate information on the screen.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				StickerAdjustment = false;
				return StickerAdjustment;
			}
			Code = "S" + fecherFoundation.Strings.Trim(txtMonthOrYear.Text) + fecherFoundation.Strings.Trim(txtOriginalStickerSD.Text);
			if (txtMonthOrYear.Text == "Y")
			{
				Code += Strings.Mid(lstOriginalMonthYear.Text, 3, 2);
			}
			else
			{
				Code += FCConvert.ToString(modGlobalRoutines.PadToString((lstOriginalMonthYear.SelectedIndex + 1), 2));
			}
			// 
			if (cmbReturnYes.Text == "Yes")
			{
				rsStickerNumber.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + Code + "' AND Number = " + txtOriginalStickerNumber.Text);
				if (rsStickerNumber.EndOfFile() != true && rsStickerNumber.BeginningOfFile() != true)
				{
					rsStickerNumber.Edit();
					rsStickerNumber.Set_Fields("Status", "A");
					rsStickerNumber.Update();
				}
				else
				{
					MessageBox.Show("This sticker could not be found in inventory.  It will have to be added back into inventory manually.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				// received record here
				MotorVehicle.Statics.strSql = "SELECT  * FROM InventoryAdjustment";
				rsInventoryStickerAdjust.OpenRecordset(MotorVehicle.Statics.strSql);
				rsInventoryStickerAdjust.AddNew();
				rsInventoryStickerAdjust.Set_Fields("AdjustmentCode", "R");
				rsInventoryStickerAdjust.Set_Fields("DateOfAdjustment", txtOriginalStickerDate.Text);
				rsInventoryStickerAdjust.Set_Fields("QuantityAdjusted", 1);
				rsInventoryStickerAdjust.Set_Fields("InventoryType", Code);
				rsInventoryStickerAdjust.Set_Fields("Low", txtOriginalStickerNumber.Text);
				rsInventoryStickerAdjust.Set_Fields("High", txtOriginalStickerNumber.Text);
				rsInventoryStickerAdjust.Set_Fields("OpID", MotorVehicle.Statics.UserID);
				rsInventoryStickerAdjust.Set_Fields("reason", "");
				rsInventoryStickerAdjust.Set_Fields("PeriodCloseoutID", 0);
				rsInventoryStickerAdjust.Set_Fields("TellerCloseoutID", 0);
				rsInventoryStickerAdjust.Update();
			}
			// end of return to inventory
			// issued record here
			if (Conversion.Val(txtNewStickerNumber.Text) != 0)
			{

                Code = "S" + fecherFoundation.Strings.Trim(txtMonthOrYear.Text) + fecherFoundation.Strings.Trim(txtNewStickerSD.Text);
                if (txtMonthOrYear.Text == "Y")
                {
                    Code += Strings.Mid(lstNewMonthYear.Text, 3, 2);
                }
                else
                {
                    Code += FCConvert.ToString(modGlobalRoutines.PadToString((lstNewMonthYear.SelectedIndex + 1), 2));
                }
				rsStickerNumber.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + Code + "' AND Number = " + txtNewStickerNumber.Text);
                if (rsStickerNumber.EndOfFile() != true && rsStickerNumber.BeginningOfFile() != true)
                {
                    rsStickerNumber.Edit();
                    rsStickerNumber.Set_Fields("Status", "I");
                    rsStickerNumber.Update();
                }
                else
                {
                    MessageBox.Show("This sticker could not be found in inventory.  It will have to be added back into inventory manually.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

				MotorVehicle.Statics.strSql = "SELECT  * FROM InventoryAdjustments";
				rsInventoryStickerAdjust.OpenRecordset(MotorVehicle.Statics.strSql);
				rsInventoryStickerAdjust.AddNew();
				rsInventoryStickerAdjust.Set_Fields("AdjustmentCode", "I");
				rsInventoryStickerAdjust.Set_Fields("DateOfAdjustment", txtOriginalStickerDate.Text);
				rsInventoryStickerAdjust.Set_Fields("QuantityAdjusted", 1);
				rsInventoryStickerAdjust.Set_Fields("InventoryType", Code);
				rsInventoryStickerAdjust.Set_Fields("Low", txtNewStickerNumber.Text);
				rsInventoryStickerAdjust.Set_Fields("High", txtNewStickerNumber.Text);
				rsInventoryStickerAdjust.Set_Fields("OpID", MotorVehicle.Statics.UserID);
				rsInventoryStickerAdjust.Set_Fields("reason", "");
				rsInventoryStickerAdjust.Set_Fields("PeriodCloseoutID", 0);
				rsInventoryStickerAdjust.Set_Fields("TellerCloseoutID", 0);
				rsInventoryStickerAdjust.Update();
			}
			rsStickerNumber.Reset();
			rsInventoryStickerAdjust.Reset();
			return StickerAdjustment;
		}

		private bool ValidateStickerControls()
		{
			bool ValidateStickerControls = false;
			ValidateStickerControls = true;
			if (txtMonthOrYear.Text == "")
			{
				ValidateStickerControls = false;
				return ValidateStickerControls;
			}
			if (lstOriginalMonthYear.SelectedIndex == -1)
			{
				ValidateStickerControls = false;
				return ValidateStickerControls;
			}
			if (txtOriginalStickerSD.Text == "")
			{
				ValidateStickerControls = false;
				return ValidateStickerControls;
			}
			if (Conversion.Val(txtOriginalStickerNumber.Text) == 0)
			{
				ValidateStickerControls = false;
				return ValidateStickerControls;
			}
			if (Conversion.Val(txtNewStickerNumber.Text) == 0)
			{
				ValidateStickerControls = false;
				return ValidateStickerControls;
			}
			if (txtOriginalStickerPlate.Text == "")
			{
				ValidateStickerControls = false;
				return ValidateStickerControls;
			}
			return ValidateStickerControls;
		}

		private string strGetCategory(string str1)
		{
			string strGetCategory = "";
			clsDRWrapper rr = new clsDRWrapper();
			strGetCategory = "";
			rr.OpenRecordset("SELECT * FROM TownSummaryHeadings");
			rr.MoveLast();
			rr.MoveFirst();
			while (!rr.EndOfFile())
			{
				if (Conversion.Val(str1) == rr.Get_Fields_Int32("ID"))
				{
					if (lstMonetaryCategories.SelectedIndex != -1)
					{
						if (lstMonetaryCategories.Items[lstMonetaryCategories.SelectedIndex].ToString() == FCConvert.ToString(rr.Get_Fields_String("Heading")))
						{
							strGetCategory = rr.Get_Fields_Int32("CategoryCode").ToString();
							break;
						}
					}
				}
				rr.MoveNext();
			}
			if (Conversion.Val(str1) == 39)
			{
				strGetCategory = 195.ToString();
			}
			if (strGetCategory == "")
				strGetCategory = "299";
			return strGetCategory;
		}
	}
}
