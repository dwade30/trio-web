//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptNoActivityTellerReport.
	/// </summary>
	public partial class rptNoActivityTellerReport : BaseSectionReport
	{
		public rptNoActivityTellerReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Teller Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptNoActivityTellerReport_ReportEnd;
		}

        private void RptNoActivityTellerReport_ReportEnd(object sender, EventArgs e)
        {
			rsTransit.DisposeOf();
            rsSpecial.DisposeOf();
            rsBooster.DisposeOf();
            rsFees.DisposeOf();

		}

        public static rptNoActivityTellerReport InstancePtr
		{
			get
			{
				return (rptNoActivityTellerReport)Sys.GetInstance(typeof(rptNoActivityTellerReport));
			}
		}

		protected rptNoActivityTellerReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNoActivityTellerReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		int TotalUnits;
		double TotalMoney;
		double TotalLocal;
		double TotalState;
		int SaveUnits;
		double SaveMoney;
		double SaveLocal;
		double SaveState;
		bool blnFirst;
		string strCode = "";
		bool blnLast;
		Decimal TotalAgent;
		Decimal TotalLocalTransfer;
		Decimal TotalExcise;
		Decimal TotalReg;
		Decimal TotalBooster;
		Decimal TotalDupReg;
		Decimal TotalSpecialtyPlate;
		Decimal TotalInitialPlate;
		Decimal TotalStateTransfer;
		Decimal TotalTransit;
		Decimal TotalSpecialPermit;
		clsDRWrapper rsTransit = new clsDRWrapper();
		clsDRWrapper rsSpecial = new clsDRWrapper();
		clsDRWrapper rsBooster = new clsDRWrapper();
		bool blnDoingTransit;
		bool blnDoingSpecial;
		bool blnDoingBooster;
		string strDates = "";
		//clsDRWrapper rsVehicleInfo = new clsDRWrapper();
		clsDRWrapper rsFees = new clsDRWrapper();

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			rptNoActivityTellerReport.InstancePtr.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (MotorVehicle.Statics.rsTellers.EndOfFile() == true)
			{
				blnLast = true;
				eArgs.EOF = true;
				return;
			}
			else
			{
				blnLast = false;
			}
			if (fecherFoundation.Strings.UCase(strCode) != fecherFoundation.Strings.UCase(FCConvert.ToString(MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID"))))
			{
				if (blnFirst)
				{
					blnFirst = false;
					strCode = fecherFoundation.Strings.UCase(FCConvert.ToString(MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID")));
					blnDoingTransit = false;
					blnDoingSpecial = false;
					blnDoingBooster = false;
					rsTransit.OpenRecordset("SELECT * FROM TransitPlates WHERE OpID = '" + strCode + "' AND " + strDates);
					if (rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true)
					{
						rsTransit.MoveLast();
						rsTransit.MoveFirst();
						blnDoingTransit = true;
					}
					rsBooster.OpenRecordset("SELECT * FROM Booster WHERE OpID = '" + strCode + "' AND " + strDates);
					if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
					{
						rsBooster.MoveLast();
						rsBooster.MoveFirst();
						if (!blnDoingTransit)
						{
							blnDoingBooster = true;
						}
					}
					rsSpecial.OpenRecordset("SELECT * FROM SpecialRegistration WHERE OpID = '" + strCode + "' AND " + strDates);
					if (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true)
					{
						rsSpecial.MoveLast();
						rsSpecial.MoveFirst();
						if (!blnDoingTransit && !blnDoingBooster)
						{
							blnDoingSpecial = true;
						}
					}
				}
				else
				{
					if (rsTransit.EndOfFile() != true)
					{
						blnDoingTransit = true;
						blnDoingSpecial = false;
						blnDoingBooster = false;
						goto DoneCheckingFlag;
					}
					else
					{
						blnDoingTransit = false;
					}
					if (rsSpecial.EndOfFile() != true)
					{
						blnDoingTransit = false;
						blnDoingSpecial = true;
						blnDoingBooster = false;
						goto DoneCheckingFlag;
					}
					else
					{
						blnDoingSpecial = false;
					}
					if (rsBooster.EndOfFile() != true)
					{
						blnDoingTransit = false;
						blnDoingSpecial = false;
						blnDoingBooster = true;
						goto DoneCheckingFlag;
					}
					else
					{
						blnDoingBooster = false;
					}
					rptNoActivityTellerReport.InstancePtr.Fields["Binder"].Value = fecherFoundation.Strings.UCase(FCConvert.ToString(MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID")));
					strCode = fecherFoundation.Strings.UCase(FCConvert.ToString(MotorVehicle.Statics.rsTellers.Get_Fields_String("OpID")));
					SaveUnits = TotalUnits;
					SaveMoney = TotalMoney;
					SaveLocal = TotalLocal;
					SaveState = TotalState;
					TotalUnits = 0;
					TotalMoney = 0;
					TotalLocal = 0;
					TotalState = 0;
					blnDoingTransit = false;
					blnDoingSpecial = false;
					blnDoingBooster = false;
					rsTransit.OpenRecordset("SELECT * FROM TransitPlates WHERE OpID = '" + strCode + "' AND " + strDates);
					if (rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true)
					{
						rsTransit.MoveLast();
						rsTransit.MoveFirst();
						blnDoingTransit = true;
					}
					rsBooster.OpenRecordset("SELECT * FROM Booster WHERE OpID = '" + strCode + "' AND " + strDates);
					if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
					{
						rsBooster.MoveLast();
						rsBooster.MoveFirst();
						if (!blnDoingTransit)
						{
							blnDoingBooster = true;
						}
					}
					rsSpecial.OpenRecordset("SELECT * FROM SpecialRegistration WHERE OpID = '" + strCode + "' AND " + strDates);
					if (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true)
					{
						rsSpecial.MoveLast();
						rsSpecial.MoveFirst();
						if (!blnDoingTransit && !blnDoingBooster)
						{
							blnDoingSpecial = true;
						}
					}
				}
			}
			DoneCheckingFlag:
			;
			if (blnDoingTransit)
			{
				TotalUnits += 1;
				fldMVR3.Text = "";
				fldClass.Text = "";
				fldPlate.Text = rsTransit.Get_Fields_String("plate");
				fldType.Text = "TST";
				fldYear.Text = rsTransit.Get_Fields_String("Year");
				fldMake.Text = rsTransit.Get_Fields_String("make");
				fldModel.Text = rsTransit.Get_Fields_String("model");
				TotalMoney += rsTransit.Get_Fields("StatePaid") + rsTransit.Get_Fields("LocalPaid");
				TotalLocal += rsTransit.Get_Fields("LocalPaid");
				TotalState += rsTransit.Get_Fields("StatePaid");
				fldLocal.Text = Strings.Format(rsTransit.Get_Fields("LocalPaid"), "#,##0.00");
				fldState.Text = Strings.Format(rsTransit.Get_Fields("StatePaid"), "#,##0.00");
				fldTotal.Text = Strings.Format(rsTransit.Get_Fields("StatePaid") + rsTransit.Get_Fields("LocalPaid"), "#,##0.00");
				TotalAgent += rsTransit.Get_Fields("LocalPaid");
				TotalTransit += rsTransit.Get_Fields("StatePaid");
				rsTransit.MoveNext();
				if (rsTransit.EndOfFile() == true)
				{
					blnDoingTransit = false;
					if ((rsSpecial.EndOfFile() && rsSpecial.BeginningOfFile()) && (rsBooster.EndOfFile() && rsBooster.BeginningOfFile()))
					{
						MotorVehicle.Statics.rsTellers.MoveNext();
					}
					else if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
					{
						blnDoingBooster = true;
					}
					else
					{
						blnDoingSpecial = true;
					}
				}
			}
			else if (blnDoingSpecial)
			{
				TotalUnits += 1;
				fldMVR3.Text = "";
				fldClass.Text = "";
				fldPlate.Text = "";
				fldType.Text = "SPR";
				fldYear.Text = rsSpecial.Get_Fields_String("Year");
				fldMake.Text = rsSpecial.Get_Fields_String("make");
				fldModel.Text = "";
				TotalMoney += rsSpecial.Get_Fields("StatePaid") + rsSpecial.Get_Fields("LocalPaid");
				TotalLocal += rsSpecial.Get_Fields("LocalPaid");
				TotalState += rsSpecial.Get_Fields("StatePaid");
				fldLocal.Text = Strings.Format(rsSpecial.Get_Fields("LocalPaid"), "#,##0.00");
				fldState.Text = Strings.Format(rsSpecial.Get_Fields("StatePaid"), "#,##0.00");
				fldTotal.Text = Strings.Format(rsSpecial.Get_Fields("StatePaid") + rsSpecial.Get_Fields("LocalPaid"), "#,##0.00");
				TotalAgent += rsSpecial.Get_Fields("LocalPaid");
				TotalSpecialPermit += rsSpecial.Get_Fields("StatePaid");
				rsSpecial.MoveNext();
				if (rsSpecial.EndOfFile() == true)
				{
					blnDoingSpecial = false;
					MotorVehicle.Statics.rsTellers.MoveNext();
				}
			}
			else if (blnDoingBooster)
			{
				TotalUnits += 1;
				fldMVR3.Text = "";
				fldYear.Text = "";
				fldMake.Text = "";
				fldModel.Text = "";
				if (!fecherFoundation.FCUtils.IsNull(rsBooster.Get_Fields_String("plate")))
				{
					fldPlate.Text = rsBooster.Get_Fields_String("plate");
				}
				else
				{
					fldPlate.Text = "";
				}
				if (!fecherFoundation.FCUtils.IsNull(rsBooster.Get_Fields_String("Class")))
				{
					fldClass.Text = rsBooster.Get_Fields_String("Class");
				}
				else
				{
					fldClass.Text = "";
				}
				fldType.Text = "BST";
				TotalMoney += rsBooster.Get_Fields("StatePaid") + rsBooster.Get_Fields("LocalPaid");
				TotalLocal += rsBooster.Get_Fields("LocalPaid");
				TotalState += rsBooster.Get_Fields("StatePaid");
				fldLocal.Text = Strings.Format(rsBooster.Get_Fields("LocalPaid"), "#,##0.00");
				fldState.Text = Strings.Format(rsBooster.Get_Fields("StatePaid"), "#,##0.00");
				fldTotal.Text = Strings.Format(rsBooster.Get_Fields("StatePaid") + rsBooster.Get_Fields("LocalPaid"), "#,##0.00");
				TotalAgent += rsBooster.Get_Fields("LocalPaid");
				TotalBooster += rsBooster.Get_Fields("StatePaid");
				rsBooster.MoveNext();
				if (rsBooster.EndOfFile() == true)
				{
					blnDoingBooster = false;
					if (rsSpecial.EndOfFile() && rsSpecial.BeginningOfFile())
					{
						MotorVehicle.Statics.rsTellers.MoveNext();
					}
					else
					{
						blnDoingSpecial = true;
					}
				}
			}
			else
			{
				MotorVehicle.Statics.rsTellers.MoveNext();
				if (MotorVehicle.Statics.rsTellers.EndOfFile() == true)
				{
					blnLast = true;
					eArgs.EOF = true;
					return;
				}
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			TotalUnits = 0;
			TotalMoney = 0;
			TotalLocal = 0;
			TotalState = 0;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label33.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label32.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 0)
			{
				Label5.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 1)
			{
				Label5.Text = Strings.Format(frmTellerReport.InstancePtr.cboStart.Items[frmTellerReport.InstancePtr.cboStart.SelectedIndex].ToString(), "MM/dd/yyyy") + " TO " + Strings.Format(frmTellerReport.InstancePtr.cboEnd.Items[frmTellerReport.InstancePtr.cboEnd.SelectedIndex].ToString(), "MM/dd/yyyy");
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 3)
			{
				Label5.Text = frmTellerReport.InstancePtr.txtStartDate.Text + " TO " + frmTellerReport.InstancePtr.txtEndDate.Text;
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 2)
			{
				Label5.Text = "Specific Teller Closeout Periods";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 4)
			{
				Label5.Text = "All Activity Since Last Closeout Period";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 5)
			{
				Label5.Text = "All Closed Out Activity Since Last Period Closeout";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 6)
			{
				Label5.Text = "All Non Closed Out Activity Since Last Period Closeout";
			}
			if (frmTellerReport.InstancePtr.cmbYes.Text == "Yes")
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			else
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			GroupFooter2.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			blnFirst = true;
			MotorVehicle.Statics.rsTellers.MoveLast();
			MotorVehicle.Statics.rsTellers.MoveFirst();
			blnDoingBooster = false;
			blnDoingSpecial = false;
			blnDoingTransit = false;
			if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 1)
			{
				strDates = " PeriodCloseoutID > " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK2);
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 3)
			{
				strDates = " Convert(Date, DateUpdated) BETWEEN '" + frmTellerReport.InstancePtr.txtStartDate.Text + "' and '" + frmTellerReport.InstancePtr.txtEndDate.Text + "'";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 0)
			{
				strDates = " Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "'";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 2)
			{
				strDates = " TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ")";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 4)
			{
				strDates = " PeriodCloseoutID = 0";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 5)
			{
				strDates = " PeriodCloseoutID = 0 AND TellerCloseoutID <> 0";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 6)
			{
				strDates = " PeriodCloseoutID = 0 AND TellerCloseoutID = 0";
			}
			rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (blnLast != true)
			{
				fldTotalUnits.Text = SaveUnits.ToString();
				fldTotalMoney.Text = Strings.Format(SaveMoney, "#,##0.00");
				fldTotalLocal.Text = Strings.Format(SaveLocal, "#,##0.00");
				fldTotalState.Text = Strings.Format(SaveState, "#,##0.00");
			}
			else
			{
				fldTotalUnits.Text = TotalUnits.ToString();
				fldTotalMoney.Text = Strings.Format(TotalMoney, "#,##0.00");
				fldTotalLocal.Text = Strings.Format(TotalLocal, "#,##0.00");
				fldTotalState.Text = Strings.Format(TotalState, "#,##0.00");
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldAgentFee.Text = Strings.Format(TotalAgent, "#,##0.00");
			fldExcise.Text = Strings.Format(TotalExcise, "#,##0.00");
			fldLocalTransfer.Text = Strings.Format(TotalLocalTransfer, "#,##0.00");
			fldRegFee.Text = Strings.Format(TotalReg, "#,##0.00");
			fldBooster.Text = Strings.Format(TotalBooster, "#,##0.00");
			fldSpecialty.Text = Strings.Format(TotalSpecialtyPlate, "#,##0.00");
			fldInitial.Text = Strings.Format(TotalInitialPlate, "#,##0.00");
			fldDupReg.Text = Strings.Format(TotalDupReg, "#,##0.00");
			fldStateTransfer.Text = Strings.Format(TotalStateTransfer, "#,##0.00");
			fldTransit.Text = Strings.Format(TotalTransit, "#,##0.00");
			fldSpecialReg.Text = Strings.Format(TotalSpecialPermit, "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM Operators WHERE Code = '" + strCode + "'", "SystemSettings");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				lblTellerName.Text = rsTemp.Get_Fields_String("Name");
			}
			else
			{
				lblTellerName.Text = strCode;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptNoActivityTellerReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptNoActivityTellerReport properties;
			//rptNoActivityTellerReport.Caption	= "Teller Report";
			//rptNoActivityTellerReport.Icon	= "rptNoActivityTellerReport.dsx":0000";
			//rptNoActivityTellerReport.Left	= 0;
			//rptNoActivityTellerReport.Top	= 0;
			//rptNoActivityTellerReport.Width	= 11880;
			//rptNoActivityTellerReport.Height	= 8595;
			//rptNoActivityTellerReport.StartUpPosition	= 3;
			//rptNoActivityTellerReport.SectionData	= "rptNoActivityTellerReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
