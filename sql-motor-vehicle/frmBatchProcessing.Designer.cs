//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmBatchProcessing.
	/// </summary>
	partial class frmBatchProcessing
	{
		public fecherFoundation.FCFrame fraReprint;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraReprintMVR3Entry;
		public fecherFoundation.FCButton cmdCancelReprint;
		public fecherFoundation.FCButton cmdOKReprint;
		public FCGrid vsReprint;
		public fecherFoundation.FCLabel Label5;
		public FCGrid vsVehicles;
		public fecherFoundation.FCTextBox txtNumberOfVehicles;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFileQueue;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.fraReprint = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOK = new fecherFoundation.FCButton();
            this.txtHigh = new fecherFoundation.FCTextBox();
            this.txtLow = new fecherFoundation.FCTextBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.fraReprintMVR3Entry = new fecherFoundation.FCFrame();
            this.cmdCancelReprint = new fecherFoundation.FCButton();
            this.cmdOKReprint = new fecherFoundation.FCButton();
            this.vsReprint = new fecherFoundation.FCGrid();
            this.Label5 = new fecherFoundation.FCLabel();
            this.vsVehicles = new fecherFoundation.FCGrid();
            this.txtNumberOfVehicles = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileQueue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.cmdFileQueue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprint)).BeginInit();
            this.fraReprint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprintMVR3Entry)).BeginInit();
            this.fraReprintMVR3Entry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelReprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKReprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsReprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileQueue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraReprint);
            this.ClientArea.Controls.Add(this.fraReprintMVR3Entry);
            this.ClientArea.Controls.Add(this.vsVehicles);
            this.ClientArea.Controls.Add(this.txtNumberOfVehicles);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileQueue);
            this.TopPanel.Controls.Add(this.cmdFilePrint);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileQueue, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(213, 30);
            this.HeaderText.Text = "Batch Registration";
            // 
            // fraReprint
            // 
            this.fraReprint.Controls.Add(this.cmdCancel);
            this.fraReprint.Controls.Add(this.cmdOK);
            this.fraReprint.Controls.Add(this.txtHigh);
            this.fraReprint.Controls.Add(this.txtLow);
            this.fraReprint.Controls.Add(this.Label4);
            this.fraReprint.Controls.Add(this.Label3);
            this.fraReprint.Controls.Add(this.Label2);
            this.fraReprint.Location = new System.Drawing.Point(187, 184);
            this.fraReprint.Name = "fraReprint";
            this.fraReprint.Size = new System.Drawing.Size(426, 245);
            this.fraReprint.TabIndex = 3;
            this.fraReprint.Text = "Enter Mvr3 Range";
            this.fraReprint.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(104, 185);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 40);
            this.cmdCancel.TabIndex = 10;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(20, 185);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(64, 40);
            this.cmdOK.TabIndex = 9;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // txtHigh
            // 
            this.txtHigh.AutoSize = false;
            this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
            this.txtHigh.LinkItem = null;
            this.txtHigh.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtHigh.LinkTopic = null;
            this.txtHigh.Location = new System.Drawing.Point(95, 125);
            this.txtHigh.Name = "txtHigh";
            this.txtHigh.Size = new System.Drawing.Size(201, 40);
            this.txtHigh.TabIndex = 7;
            this.txtHigh.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtHigh.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtHigh_KeyPress);
            // 
            // txtLow
            // 
            this.txtLow.AutoSize = false;
            this.txtLow.BackColor = System.Drawing.SystemColors.Window;
            this.txtLow.LinkItem = null;
            this.txtLow.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLow.LinkTopic = null;
            this.txtLow.Location = new System.Drawing.Point(95, 65);
            this.txtLow.Name = "txtLow";
            this.txtLow.Size = new System.Drawing.Size(201, 40);
            this.txtLow.TabIndex = 5;
            this.txtLow.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtLow.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLow_KeyPress);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 139);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(46, 16);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "HIGH";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 79);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(46, 16);
            this.Label3.TabIndex = 6;
            this.Label3.Text = "LOW";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(386, 15);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "PLEASE ENTER THE RANGE OF MVR3 FORMS YOU WISH TO REPRINT";
            // 
            // fraReprintMVR3Entry
            // 
            this.fraReprintMVR3Entry.Controls.Add(this.cmdCancelReprint);
            this.fraReprintMVR3Entry.Controls.Add(this.cmdOKReprint);
            this.fraReprintMVR3Entry.Controls.Add(this.vsReprint);
            this.fraReprintMVR3Entry.Controls.Add(this.Label5);
            this.fraReprintMVR3Entry.Location = new System.Drawing.Point(50, 30);
            this.fraReprintMVR3Entry.Name = "fraReprintMVR3Entry";
            this.fraReprintMVR3Entry.Size = new System.Drawing.Size(604, 505);
            this.fraReprintMVR3Entry.TabIndex = 11;
            this.fraReprintMVR3Entry.Text = "Reprint Mvr3 Forms";
            this.fraReprintMVR3Entry.Visible = false;
            // 
            // cmdCancelReprint
            // 
            this.cmdCancelReprint.AppearanceKey = "actionButton";
            this.cmdCancelReprint.Location = new System.Drawing.Point(106, 445);
            this.cmdCancelReprint.Name = "cmdCancelReprint";
            this.cmdCancelReprint.Size = new System.Drawing.Size(94, 40);
            this.cmdCancelReprint.TabIndex = 14;
            this.cmdCancelReprint.Text = "Cancel";
            this.cmdCancelReprint.Click += new System.EventHandler(this.cmdCancelReprint_Click);
            // 
            // cmdOKReprint
            // 
            this.cmdOKReprint.AppearanceKey = "actionButton";
            this.cmdOKReprint.Location = new System.Drawing.Point(20, 445);
            this.cmdOKReprint.Name = "cmdOKReprint";
            this.cmdOKReprint.Size = new System.Drawing.Size(66, 40);
            this.cmdOKReprint.TabIndex = 13;
            this.cmdOKReprint.Text = "OK";
            this.cmdOKReprint.Click += new System.EventHandler(this.cmdOKReprint_Click);
            // 
            // vsReprint
            // 
            this.vsReprint.AllowSelection = false;
            this.vsReprint.AllowUserToResizeColumns = false;
            this.vsReprint.AllowUserToResizeRows = false;
            this.vsReprint.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsReprint.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsReprint.BackColorBkg = System.Drawing.Color.Empty;
            this.vsReprint.BackColorFixed = System.Drawing.Color.Empty;
            this.vsReprint.BackColorSel = System.Drawing.Color.Empty;
            this.vsReprint.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsReprint.Cols = 3;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsReprint.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsReprint.ColumnHeadersHeight = 30;
            this.vsReprint.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsReprint.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsReprint.DragIcon = null;
            this.vsReprint.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsReprint.ExtendLastCol = true;
            this.vsReprint.FixedCols = 2;
            this.vsReprint.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsReprint.FrozenCols = 0;
            this.vsReprint.GridColor = System.Drawing.Color.Empty;
            this.vsReprint.GridColorFixed = System.Drawing.Color.Empty;
            this.vsReprint.Location = new System.Drawing.Point(20, 65);
            this.vsReprint.Name = "vsReprint";
            this.vsReprint.OutlineCol = 0;
            this.vsReprint.ReadOnly = true;
            this.vsReprint.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsReprint.RowHeightMin = 0;
            this.vsReprint.Rows = 1;
            this.vsReprint.ScrollTipText = null;
            this.vsReprint.ShowColumnVisibilityMenu = false;
            this.vsReprint.ShowFocusCell = false;
            this.vsReprint.Size = new System.Drawing.Size(564, 360);
            this.vsReprint.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsReprint.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsReprint.TabIndex = 12;
            this.vsReprint.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsReprint_KeyPressEdit);
            this.vsReprint.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsReprint_BeforeEdit);
            this.vsReprint.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsReprint_AfterEdit);
            this.vsReprint.CurrentCellChanged += new System.EventHandler(this.vsReprint_RowColChange);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(564, 15);
            this.Label5.TabIndex = 15;
            this.Label5.Text = "PLEASE ENTER THE REPLACEMENT MVR3 FOMRS TO BE USED FOR THE REPRINT";
            // 
            // vsVehicles
            // 
            this.vsVehicles.AllowSelection = false;
            this.vsVehicles.AllowUserToResizeColumns = false;
            this.vsVehicles.AllowUserToResizeRows = false;
            this.vsVehicles.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsVehicles.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsVehicles.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorBkg = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.BackColorSel = System.Drawing.Color.Empty;
            this.vsVehicles.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsVehicles.Cols = 18;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsVehicles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsVehicles.ColumnHeadersHeight = 30;
            this.vsVehicles.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsVehicles.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsVehicles.DragIcon = null;
            this.vsVehicles.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsVehicles.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsVehicles.ExtendLastCol = true;
            this.vsVehicles.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.FrozenCols = 0;
            this.vsVehicles.GridColor = System.Drawing.Color.Empty;
            this.vsVehicles.GridColorFixed = System.Drawing.Color.Empty;
            this.vsVehicles.Location = new System.Drawing.Point(30, 90);
            this.vsVehicles.Name = "vsVehicles";
            this.vsVehicles.OutlineCol = 0;
            this.vsVehicles.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsVehicles.RowHeightMin = 0;
            this.vsVehicles.Rows = 1;
            this.vsVehicles.ScrollTipText = null;
            this.vsVehicles.ShowColumnVisibilityMenu = false;
            this.vsVehicles.ShowFocusCell = false;
            this.vsVehicles.Size = new System.Drawing.Size(984, 409);
            this.vsVehicles.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsVehicles.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsVehicles.TabIndex = 2;
            this.vsVehicles.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsVehicles_KeyPressEdit);
            this.vsVehicles.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsVehicles_BeforeEdit);
            this.vsVehicles.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsVehicles_AfterEdit);
            this.vsVehicles.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsVehicles_ValidateEdit);
            this.vsVehicles.CurrentCellChanged += new System.EventHandler(this.vsVehicles_RowColChange);
            // 
            // txtNumberOfVehicles
            // 
            this.txtNumberOfVehicles.AutoSize = false;
            this.txtNumberOfVehicles.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumberOfVehicles.LinkItem = null;
            this.txtNumberOfVehicles.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtNumberOfVehicles.LinkTopic = null;
            this.txtNumberOfVehicles.Location = new System.Drawing.Point(412, 30);
            this.txtNumberOfVehicles.Name = "txtNumberOfVehicles";
            this.txtNumberOfVehicles.Size = new System.Drawing.Size(48, 40);
            this.txtNumberOfVehicles.TabIndex = 1;
            this.txtNumberOfVehicles.Text = "1";
            this.txtNumberOfVehicles.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtNumberOfVehicles.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNumberOfVehicles_KeyPress);
            this.txtNumberOfVehicles.Validating += new System.ComponentModel.CancelEventHandler(this.txtNumberOfVehicles_Validating);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(330, 16);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "PLEASE ENTER THE NUMBER OF VEHICLES TO REGISTER";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFileQueue,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePrint.Text = "Print Batch";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFileQueue
            // 
            this.mnuFileQueue.Index = 1;
            this.mnuFileQueue.Name = "mnuFileQueue";
            this.mnuFileQueue.Text = "Queue Batch";
            this.mnuFileQueue.Visible = false;
            this.mnuFileQueue.Click += new System.EventHandler(this.mnuFileQueue_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Enabled = false;
            this.mnuProcessSave.Index = 2;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save Batch";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 3;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 4;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Enabled = false;
            this.cmdSave.Location = new System.Drawing.Point(470, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(130, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save Batch";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrint.AppearanceKey = "toolbarButton";
            this.cmdFilePrint.Location = new System.Drawing.Point(966, 29);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdFilePrint.Size = new System.Drawing.Size(84, 24);
            this.cmdFilePrint.TabIndex = 1;
            this.cmdFilePrint.Text = "Print Batch";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // cmdFileQueue
            // 
            this.cmdFileQueue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileQueue.AppearanceKey = "toolbarButton";
            this.cmdFileQueue.Location = new System.Drawing.Point(860, 29);
            this.cmdFileQueue.Name = "cmdFileQueue";
            this.cmdFileQueue.Size = new System.Drawing.Size(100, 24);
            this.cmdFileQueue.TabIndex = 2;
            this.cmdFileQueue.Text = "Queue Batch";
            this.cmdFileQueue.Visible = false;
            this.cmdFileQueue.Click += new System.EventHandler(this.mnuFileQueue_Click);
            // 
            // frmBatchProcessing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBatchProcessing";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Batch Registration";
            this.Load += new System.EventHandler(this.frmBatchProcessing_Load);
            this.Activated += new System.EventHandler(this.frmBatchProcessing_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBatchProcessing_KeyPress);
            this.Resize += new System.EventHandler(this.frmBatchProcessing_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprint)).EndInit();
            this.fraReprint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprintMVR3Entry)).EndInit();
            this.fraReprintMVR3Entry.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelReprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKReprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsReprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileQueue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdFilePrint;
		private FCButton cmdFileQueue;
	}
}