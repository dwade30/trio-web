﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTAgentDetail.
	/// </summary>
	partial class rptLTTAgentDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLTTAgentDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits12Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExtensionsUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount12Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExtensionAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits11Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount11Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits10Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount10Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits9Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount9Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits8Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount8Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnitsSubTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmountSubTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnitsSubTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmountSubTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTUnitsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTSmallUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTSmallAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits20Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount20Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits20Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount20Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field61 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits19Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount19Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field64 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits19Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount19Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits18Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount18Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field70 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits18Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount18Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field73 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits17Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount17Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field76 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits17Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount17Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field79 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits16Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount16Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field82 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits16Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount16Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field85 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits15Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount15Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field88 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits15Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount15Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field91 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits14Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount14Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field94 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits14Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount14Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field97 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits13Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount13Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits13Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount13Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits12Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount12Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits11Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount11Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field109 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits10Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount10Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits9Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount9Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits8Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount8Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits25Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount25Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits24Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount24Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits24Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount24Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field130 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits23Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount23Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits23Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount23Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field136 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits22Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount22Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits22Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount22Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field142 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits21Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount21Full = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field145 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLTTLargeUnits21Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLTTLargeAmount21Time = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label135 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTransfersLargeUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransfersSmallUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLostPlateUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDuplicateUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitleUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxPaidUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxNoFeeUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferLargeAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransferSmallAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLostPlateAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDuplicateAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitleAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxPaidAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSalesTaxNoFeeAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLostPlateFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDuplicateFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitleFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label136 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTransfers25YearUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransfer25YearAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field151 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label137 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTitle25YearUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle25YearsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field154 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label138 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRushTitleUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRushTitleAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRushTitleFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits12Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionsUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount12Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits11Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount11Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits10Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount10Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits9Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount9Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits8Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount8Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnitsSubTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmountSubTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnitsSubTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmountSubTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTUnitsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits20Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount20Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits20Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount20Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits19Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount19Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits19Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount19Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits18Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount18Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits18Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount18Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits17Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount17Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits17Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount17Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits16Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount16Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits16Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount16Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits15Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount15Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits15Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount15Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits14Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount14Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits14Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount14Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits13Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount13Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits13Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount13Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits12Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount12Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits11Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount11Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits10Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount10Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits9Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount9Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits8Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount8Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits25Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount25Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits24Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount24Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits24Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount24Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits23Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount23Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits23Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount23Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits22Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount22Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field139)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits22Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount22Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field142)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits21Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount21Full)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field145)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits21Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount21Time)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field148)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersLargeUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersSmallUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferLargeAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferSmallAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfers25YearUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfer25YearAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field151)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field154)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label17,
				this.Label19,
				this.fldLTTLargeUnits12Full,
				this.fldLTTSmallUnits12,
				this.fldExtensionsUnits,
				this.fldLTTLargeAmount12Full,
				this.fldLTTSmallAmount12,
				this.fldExtensionAmount,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33,
				this.Label34,
				this.Label35,
				this.Label36,
				this.Label40,
				this.Label41,
				this.Label42,
				this.Label43,
				this.Label44,
				this.Label45,
				this.Label46,
				this.Label47,
				this.Label48,
				this.Label52,
				this.Label53,
				this.fldLTTLargeUnits11Full,
				this.fldLTTLargeAmount11Full,
				this.fldLTTLargeUnits10Full,
				this.fldLTTLargeAmount10Full,
				this.fldLTTLargeUnits9Full,
				this.fldLTTLargeAmount9Full,
				this.fldLTTLargeUnits8Full,
				this.fldLTTLargeAmount8Full,
				this.fldLTTLargeUnits7,
				this.fldLTTLargeAmount7,
				this.fldLTTLargeUnits6,
				this.fldLTTLargeAmount6,
				this.fldLTTLargeUnits5,
				this.fldLTTLargeAmount5,
				this.fldLTTLargeUnitsSubTotal,
				this.fldLTTLargeAmountSubTotal,
				this.fldLTTSmallUnits11,
				this.fldLTTSmallAmount11,
				this.fldLTTSmallUnits10,
				this.fldLTTSmallAmount10,
				this.fldLTTSmallUnits9,
				this.fldLTTSmallAmount9,
				this.fldLTTSmallUnits8,
				this.fldLTTSmallAmount8,
				this.fldLTTSmallUnits7,
				this.fldLTTSmallAmount7,
				this.fldLTTSmallUnits6,
				this.fldLTTSmallAmount6,
				this.fldLTTSmallUnits5,
				this.fldLTTSmallAmount5,
				this.fldLTTSmallUnitsSubTotal,
				this.fldLTTSmallAmountSubTotal,
				this.fldLTTUnitsTotal,
				this.fldLTTAmountTotal,
				this.Label54,
				this.Label55,
				this.Label56,
				this.fldLTTLargeUnits4,
				this.fldLTTLargeAmount4,
				this.fldLTTLargeUnits3,
				this.fldLTTLargeAmount3,
				this.fldLTTLargeUnits2,
				this.fldLTTLargeAmount2,
				this.Label57,
				this.Label58,
				this.Label59,
				this.fldLTTSmallUnits4,
				this.fldLTTSmallAmount4,
				this.fldLTTSmallUnits3,
				this.fldLTTSmallAmount3,
				this.fldLTTSmallUnits2,
				this.fldLTTSmallAmount2,
				this.Field1,
				this.Field2,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field17,
				this.Field18,
				this.Field20,
				this.Field21,
				this.Field22,
				this.Field23,
				this.Field24,
				this.Field25,
				this.Field26,
				this.Field29,
				this.Field30,
				this.Field31,
				this.Field32,
				this.Field33,
				this.Field34,
				this.Label61,
				this.fldLTTLargeUnits20Full,
				this.fldLTTLargeAmount20Full,
				this.Field51,
				this.Label70,
				this.Label71,
				this.fldLTTLargeUnits20Time,
				this.fldLTTLargeAmount20Time,
				this.Field61,
				this.Label72,
				this.Label73,
				this.fldLTTLargeUnits19Full,
				this.fldLTTLargeAmount19Full,
				this.Field64,
				this.Label74,
				this.Label75,
				this.fldLTTLargeUnits19Time,
				this.fldLTTLargeAmount19Time,
				this.Field67,
				this.Label76,
				this.Label77,
				this.fldLTTLargeUnits18Full,
				this.fldLTTLargeAmount18Full,
				this.Field70,
				this.Label78,
				this.Label79,
				this.fldLTTLargeUnits18Time,
				this.fldLTTLargeAmount18Time,
				this.Field73,
				this.Label80,
				this.Label81,
				this.fldLTTLargeUnits17Full,
				this.fldLTTLargeAmount17Full,
				this.Field76,
				this.Label82,
				this.Label83,
				this.fldLTTLargeUnits17Time,
				this.fldLTTLargeAmount17Time,
				this.Field79,
				this.Label84,
				this.Label85,
				this.fldLTTLargeUnits16Full,
				this.fldLTTLargeAmount16Full,
				this.Field82,
				this.Label86,
				this.Label87,
				this.fldLTTLargeUnits16Time,
				this.fldLTTLargeAmount16Time,
				this.Field85,
				this.Label88,
				this.Label89,
				this.fldLTTLargeUnits15Full,
				this.fldLTTLargeAmount15Full,
				this.Field88,
				this.Label90,
				this.Label91,
				this.fldLTTLargeUnits15Time,
				this.fldLTTLargeAmount15Time,
				this.Field91,
				this.Label92,
				this.Label93,
				this.fldLTTLargeUnits14Full,
				this.fldLTTLargeAmount14Full,
				this.Field94,
				this.Label94,
				this.Label95,
				this.fldLTTLargeUnits14Time,
				this.fldLTTLargeAmount14Time,
				this.Field97,
				this.Label96,
				this.Label97,
				this.fldLTTLargeUnits13Full,
				this.fldLTTLargeAmount13Full,
				this.Field100,
				this.Label98,
				this.Label99,
				this.fldLTTLargeUnits13Time,
				this.fldLTTLargeAmount13Time,
				this.Field103,
				this.Label100,
				this.Label101,
				this.fldLTTLargeUnits12Time,
				this.fldLTTLargeAmount12Time,
				this.Field106,
				this.Label102,
				this.Label103,
				this.Label104,
				this.fldLTTLargeUnits11Time,
				this.fldLTTLargeAmount11Time,
				this.Field109,
				this.Label105,
				this.Label106,
				this.Label107,
				this.fldLTTLargeUnits10Time,
				this.fldLTTLargeAmount10Time,
				this.Field112,
				this.Label108,
				this.Label109,
				this.Label110,
				this.fldLTTLargeUnits9Time,
				this.fldLTTLargeAmount9Time,
				this.Field115,
				this.Label111,
				this.Label112,
				this.Label113,
				this.fldLTTLargeUnits8Time,
				this.fldLTTLargeAmount8Time,
				this.Field118,
				this.Label114,
				this.Label115,
				this.Label116,
				this.fldLTTLargeUnits25Full,
				this.fldLTTLargeAmount25Full,
				this.Field121,
				this.Label117,
				this.Label120,
				this.fldLTTLargeUnits24Full,
				this.fldLTTLargeAmount24Full,
				this.Field127,
				this.Label121,
				this.Label122,
				this.fldLTTLargeUnits24Time,
				this.fldLTTLargeAmount24Time,
				this.Field130,
				this.Label123,
				this.Label124,
				this.fldLTTLargeUnits23Full,
				this.fldLTTLargeAmount23Full,
				this.Field133,
				this.Label125,
				this.Label126,
				this.fldLTTLargeUnits23Time,
				this.fldLTTLargeAmount23Time,
				this.Field136,
				this.Label127,
				this.Label128,
				this.fldLTTLargeUnits22Full,
				this.fldLTTLargeAmount22Full,
				this.Field139,
				this.Label129,
				this.Label130,
				this.fldLTTLargeUnits22Time,
				this.fldLTTLargeAmount22Time,
				this.Field142,
				this.Label131,
				this.Label132,
				this.fldLTTLargeUnits21Full,
				this.fldLTTLargeAmount21Full,
				this.Field145,
				this.Label133,
				this.Label134,
				this.fldLTTLargeUnits21Time,
				this.fldLTTLargeAmount21Time,
				this.Field148,
				this.Label135,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Line2,
				this.fldTransfersLargeUnits,
				this.fldTransfersSmallUnits,
				this.fldLostPlateUnits,
				this.fldDuplicateUnits,
				this.fldCorrectionUnits,
				this.fldTitleUnits,
				this.fldSalesTaxPaidUnits,
				this.fldSalesTaxNoFeeUnits,
				this.fldTotalUnits,
				this.fldTransferLargeAmount,
				this.fldTransferSmallAmount,
				this.fldLostPlateAmount,
				this.fldDuplicateAmount,
				this.fldCorrectionAmount,
				this.fldTitleAmount,
				this.fldSalesTaxPaidAmount,
				this.fldSalesTaxNoFeeAmount,
				this.fldTotalAmount,
				this.Field4,
				this.Field5,
				this.fldLostPlateFee,
				this.fldDuplicateFee,
				this.fldTitleFee,
				this.Label136,
				this.fldTransfers25YearUnits,
				this.fldTransfer25YearAmount,
				this.Field151,
				this.Label137,
				this.fldTitle25YearUnits,
				this.fldTitle25YearsAmount,
				this.Field154,
				this.Label138,
				this.fldRushTitleUnits,
				this.fldRushTitleAmount,
				this.fldRushTitleFee
			});
			this.Detail.Height = 13.1875F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label12,
				this.Label13,
				this.Label16,
				this.Label1,
				this.lblPage,
				this.Line1,
				this.Label60,
				this.Label69
			});
			this.PageHeader.Height = 1.59375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.3125F;
			this.SubReport2.Width = 7.46875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.15625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label12.Text = "Category";
			this.Label12.Top = 1.375F;
			this.Label12.Width = 2.09375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.8125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "Units";
			this.Label13.Top = 1.375F;
			this.Label13.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 5.9375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label16.Text = "Dollars";
			this.Label16.Top = 1.375F;
			this.Label16.Width = 1.125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.78125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label1.Text = "**** LTT AGENT DETAIL REPORT****";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 3.625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.5625F;
			this.Line1.Width = 7.40625F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.5625F;
			this.Line1.Y2 = 1.5625F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1875F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 3.625F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label60.Text = "Fee";
			this.Label60.Top = 1.375F;
			this.Label60.Width = 0.875F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 2.375F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label69.Text = "Type";
			this.Label69.Top = 1.375F;
			this.Label69.Width = 0.875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.1875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label17.Text = "LTT - Large  12 Years";
			this.Label17.Top = 4.6875F;
			this.Label17.Width = 2.0625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.1875F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label19.Text = "Extensions";
			this.Label19.Top = 10.4375F;
			this.Label19.Width = 2.0625F;
			// 
			// fldLTTLargeUnits12Full
			// 
			this.fldLTTLargeUnits12Full.Height = 0.1875F;
			this.fldLTTLargeUnits12Full.Left = 4.8125F;
			this.fldLTTLargeUnits12Full.Name = "fldLTTLargeUnits12Full";
			this.fldLTTLargeUnits12Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits12Full.Text = "Field1";
			this.fldLTTLargeUnits12Full.Top = 4.6875F;
			this.fldLTTLargeUnits12Full.Width = 0.8125F;
			// 
			// fldLTTSmallUnits12
			// 
			this.fldLTTSmallUnits12.Height = 0.1875F;
			this.fldLTTSmallUnits12.Left = 4.8125F;
			this.fldLTTSmallUnits12.Name = "fldLTTSmallUnits12";
			this.fldLTTSmallUnits12.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits12.Text = "Field2";
			this.fldLTTSmallUnits12.Top = 7.9375F;
			this.fldLTTSmallUnits12.Width = 0.8125F;
			// 
			// fldExtensionsUnits
			// 
			this.fldExtensionsUnits.Height = 0.1875F;
			this.fldExtensionsUnits.Left = 4.8125F;
			this.fldExtensionsUnits.Name = "fldExtensionsUnits";
			this.fldExtensionsUnits.Style = "font-size: 9pt; text-align: right";
			this.fldExtensionsUnits.Text = "Field3";
			this.fldExtensionsUnits.Top = 10.4375F;
			this.fldExtensionsUnits.Width = 0.8125F;
			// 
			// fldLTTLargeAmount12Full
			// 
			this.fldLTTLargeAmount12Full.Height = 0.1875F;
			this.fldLTTLargeAmount12Full.Left = 5.9375F;
			this.fldLTTLargeAmount12Full.Name = "fldLTTLargeAmount12Full";
			this.fldLTTLargeAmount12Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount12Full.Text = "Field13";
			this.fldLTTLargeAmount12Full.Top = 4.6875F;
			this.fldLTTLargeAmount12Full.Width = 1.125F;
			// 
			// fldLTTSmallAmount12
			// 
			this.fldLTTSmallAmount12.Height = 0.1875F;
			this.fldLTTSmallAmount12.Left = 5.9375F;
			this.fldLTTSmallAmount12.Name = "fldLTTSmallAmount12";
			this.fldLTTSmallAmount12.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount12.Text = "Field14";
			this.fldLTTSmallAmount12.Top = 7.9375F;
			this.fldLTTSmallAmount12.Width = 1.125F;
			// 
			// fldExtensionAmount
			// 
			this.fldExtensionAmount.Height = 0.1875F;
			this.fldExtensionAmount.Left = 5.9375F;
			this.fldExtensionAmount.Name = "fldExtensionAmount";
			this.fldExtensionAmount.Style = "font-size: 9pt; text-align: right";
			this.fldExtensionAmount.Text = "Field15";
			this.fldExtensionAmount.Top = 10.4375F;
			this.fldExtensionAmount.Width = 1.125F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.1875F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label30.Text = "LTT - Large  11 Years";
			this.Label30.Top = 5.0625F;
			this.Label30.Width = 2.0625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.1875F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label31.Text = "LTT - Large  10 Years";
			this.Label31.Top = 5.4375F;
			this.Label31.Width = 2.0625F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0.1875F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label32.Text = "LTT - Large  9 Years";
			this.Label32.Top = 5.8125F;
			this.Label32.Width = 2.0625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.1875F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label33.Text = "LTT - Large  8 Years";
			this.Label33.Top = 6.1875F;
			this.Label33.Width = 2.0625F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.1875F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label34.Text = "LTT - Large  7 Years";
			this.Label34.Top = 6.5625F;
			this.Label34.Width = 2.0625F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.1875F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label35.Text = "LTT - Large  6 Years";
			this.Label35.Top = 6.75F;
			this.Label35.Width = 2.0625F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.1875F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label36.Text = "LTT - Large  5 Years";
			this.Label36.Top = 6.9375F;
			this.Label36.Width = 2.0625F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 0.75F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label40.Text = "Sub Total";
			this.Label40.Top = 7.6875F;
			this.Label40.Width = 1.5F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1875F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.1875F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label41.Text = "LTT - Small  12 Years";
			this.Label41.Top = 7.9375F;
			this.Label41.Width = 2.0625F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0.1875F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label42.Text = "LTT - Small  11 Years";
			this.Label42.Top = 8.125F;
			this.Label42.Width = 2.0625F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 0.1875F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label43.Text = "LTT - Small  10 Years";
			this.Label43.Top = 8.3125F;
			this.Label43.Width = 2.0625F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 0.1875F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label44.Text = "LTT - Small  9 Years";
			this.Label44.Top = 8.5F;
			this.Label44.Width = 2.0625F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0.1875F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label45.Text = "LTT - Small  8 Years";
			this.Label45.Top = 8.6875F;
			this.Label45.Width = 2.0625F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 0.1875F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label46.Text = "LTT - Small  7 Years";
			this.Label46.Top = 8.875F;
			this.Label46.Width = 2.0625F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 0.1875F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label47.Text = "LTT - Small  6 Years";
			this.Label47.Top = 9.0625F;
			this.Label47.Width = 2.0625F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 0.1875F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label48.Text = "LTT - Small  5 Years";
			this.Label48.Top = 9.25F;
			this.Label48.Width = 2.0625F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 0.75F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label52.Text = "Sub Total";
			this.Label52.Top = 10F;
			this.Label52.Width = 1.5F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 1.1875F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label53.Text = "LTT Total";
			this.Label53.Top = 10.1875F;
			this.Label53.Width = 1.0625F;
			// 
			// fldLTTLargeUnits11Full
			// 
			this.fldLTTLargeUnits11Full.Height = 0.1875F;
			this.fldLTTLargeUnits11Full.Left = 4.8125F;
			this.fldLTTLargeUnits11Full.Name = "fldLTTLargeUnits11Full";
			this.fldLTTLargeUnits11Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits11Full.Text = "Field1";
			this.fldLTTLargeUnits11Full.Top = 5.0625F;
			this.fldLTTLargeUnits11Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount11Full
			// 
			this.fldLTTLargeAmount11Full.Height = 0.1875F;
			this.fldLTTLargeAmount11Full.Left = 5.9375F;
			this.fldLTTLargeAmount11Full.Name = "fldLTTLargeAmount11Full";
			this.fldLTTLargeAmount11Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount11Full.Text = "Field13";
			this.fldLTTLargeAmount11Full.Top = 5.0625F;
			this.fldLTTLargeAmount11Full.Width = 1.125F;
			// 
			// fldLTTLargeUnits10Full
			// 
			this.fldLTTLargeUnits10Full.Height = 0.1875F;
			this.fldLTTLargeUnits10Full.Left = 4.8125F;
			this.fldLTTLargeUnits10Full.Name = "fldLTTLargeUnits10Full";
			this.fldLTTLargeUnits10Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits10Full.Text = "Field1";
			this.fldLTTLargeUnits10Full.Top = 5.4375F;
			this.fldLTTLargeUnits10Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount10Full
			// 
			this.fldLTTLargeAmount10Full.Height = 0.1875F;
			this.fldLTTLargeAmount10Full.Left = 5.9375F;
			this.fldLTTLargeAmount10Full.Name = "fldLTTLargeAmount10Full";
			this.fldLTTLargeAmount10Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount10Full.Text = "Field13";
			this.fldLTTLargeAmount10Full.Top = 5.4375F;
			this.fldLTTLargeAmount10Full.Width = 1.125F;
			// 
			// fldLTTLargeUnits9Full
			// 
			this.fldLTTLargeUnits9Full.Height = 0.1875F;
			this.fldLTTLargeUnits9Full.Left = 4.8125F;
			this.fldLTTLargeUnits9Full.Name = "fldLTTLargeUnits9Full";
			this.fldLTTLargeUnits9Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits9Full.Text = "Field1";
			this.fldLTTLargeUnits9Full.Top = 5.8125F;
			this.fldLTTLargeUnits9Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount9Full
			// 
			this.fldLTTLargeAmount9Full.Height = 0.1875F;
			this.fldLTTLargeAmount9Full.Left = 5.9375F;
			this.fldLTTLargeAmount9Full.Name = "fldLTTLargeAmount9Full";
			this.fldLTTLargeAmount9Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount9Full.Text = "Field13";
			this.fldLTTLargeAmount9Full.Top = 5.8125F;
			this.fldLTTLargeAmount9Full.Width = 1.125F;
			// 
			// fldLTTLargeUnits8Full
			// 
			this.fldLTTLargeUnits8Full.Height = 0.1875F;
			this.fldLTTLargeUnits8Full.Left = 4.8125F;
			this.fldLTTLargeUnits8Full.Name = "fldLTTLargeUnits8Full";
			this.fldLTTLargeUnits8Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits8Full.Text = "Field1";
			this.fldLTTLargeUnits8Full.Top = 6.1875F;
			this.fldLTTLargeUnits8Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount8Full
			// 
			this.fldLTTLargeAmount8Full.Height = 0.1875F;
			this.fldLTTLargeAmount8Full.Left = 5.9375F;
			this.fldLTTLargeAmount8Full.Name = "fldLTTLargeAmount8Full";
			this.fldLTTLargeAmount8Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount8Full.Text = "Field13";
			this.fldLTTLargeAmount8Full.Top = 6.1875F;
			this.fldLTTLargeAmount8Full.Width = 1.125F;
			// 
			// fldLTTLargeUnits7
			// 
			this.fldLTTLargeUnits7.Height = 0.1875F;
			this.fldLTTLargeUnits7.Left = 4.8125F;
			this.fldLTTLargeUnits7.Name = "fldLTTLargeUnits7";
			this.fldLTTLargeUnits7.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits7.Text = "Field1";
			this.fldLTTLargeUnits7.Top = 6.5625F;
			this.fldLTTLargeUnits7.Width = 0.8125F;
			// 
			// fldLTTLargeAmount7
			// 
			this.fldLTTLargeAmount7.Height = 0.1875F;
			this.fldLTTLargeAmount7.Left = 5.9375F;
			this.fldLTTLargeAmount7.Name = "fldLTTLargeAmount7";
			this.fldLTTLargeAmount7.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount7.Text = "Field13";
			this.fldLTTLargeAmount7.Top = 6.5625F;
			this.fldLTTLargeAmount7.Width = 1.125F;
			// 
			// fldLTTLargeUnits6
			// 
			this.fldLTTLargeUnits6.Height = 0.1875F;
			this.fldLTTLargeUnits6.Left = 4.8125F;
			this.fldLTTLargeUnits6.Name = "fldLTTLargeUnits6";
			this.fldLTTLargeUnits6.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits6.Text = "Field1";
			this.fldLTTLargeUnits6.Top = 6.75F;
			this.fldLTTLargeUnits6.Width = 0.8125F;
			// 
			// fldLTTLargeAmount6
			// 
			this.fldLTTLargeAmount6.Height = 0.1875F;
			this.fldLTTLargeAmount6.Left = 5.9375F;
			this.fldLTTLargeAmount6.Name = "fldLTTLargeAmount6";
			this.fldLTTLargeAmount6.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount6.Text = "Field13";
			this.fldLTTLargeAmount6.Top = 6.75F;
			this.fldLTTLargeAmount6.Width = 1.125F;
			// 
			// fldLTTLargeUnits5
			// 
			this.fldLTTLargeUnits5.Height = 0.1875F;
			this.fldLTTLargeUnits5.Left = 4.8125F;
			this.fldLTTLargeUnits5.Name = "fldLTTLargeUnits5";
			this.fldLTTLargeUnits5.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits5.Text = "Field13";
			this.fldLTTLargeUnits5.Top = 6.9375F;
			this.fldLTTLargeUnits5.Width = 0.8125F;
			// 
			// fldLTTLargeAmount5
			// 
			this.fldLTTLargeAmount5.Height = 0.1875F;
			this.fldLTTLargeAmount5.Left = 5.9375F;
			this.fldLTTLargeAmount5.Name = "fldLTTLargeAmount5";
			this.fldLTTLargeAmount5.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount5.Text = "Field13";
			this.fldLTTLargeAmount5.Top = 6.9375F;
			this.fldLTTLargeAmount5.Width = 1.125F;
			// 
			// fldLTTLargeUnitsSubTotal
			// 
			this.fldLTTLargeUnitsSubTotal.Height = 0.1875F;
			this.fldLTTLargeUnitsSubTotal.Left = 4.8125F;
			this.fldLTTLargeUnitsSubTotal.Name = "fldLTTLargeUnitsSubTotal";
			this.fldLTTLargeUnitsSubTotal.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnitsSubTotal.Text = "Field1";
			this.fldLTTLargeUnitsSubTotal.Top = 7.6875F;
			this.fldLTTLargeUnitsSubTotal.Width = 0.8125F;
			// 
			// fldLTTLargeAmountSubTotal
			// 
			this.fldLTTLargeAmountSubTotal.Height = 0.1875F;
			this.fldLTTLargeAmountSubTotal.Left = 5.9375F;
			this.fldLTTLargeAmountSubTotal.Name = "fldLTTLargeAmountSubTotal";
			this.fldLTTLargeAmountSubTotal.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmountSubTotal.Text = "Field13";
			this.fldLTTLargeAmountSubTotal.Top = 7.6875F;
			this.fldLTTLargeAmountSubTotal.Width = 1.125F;
			// 
			// fldLTTSmallUnits11
			// 
			this.fldLTTSmallUnits11.Height = 0.1875F;
			this.fldLTTSmallUnits11.Left = 4.8125F;
			this.fldLTTSmallUnits11.Name = "fldLTTSmallUnits11";
			this.fldLTTSmallUnits11.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits11.Text = "Field1";
			this.fldLTTSmallUnits11.Top = 8.125F;
			this.fldLTTSmallUnits11.Width = 0.8125F;
			// 
			// fldLTTSmallAmount11
			// 
			this.fldLTTSmallAmount11.Height = 0.1875F;
			this.fldLTTSmallAmount11.Left = 5.9375F;
			this.fldLTTSmallAmount11.Name = "fldLTTSmallAmount11";
			this.fldLTTSmallAmount11.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount11.Text = "Field13";
			this.fldLTTSmallAmount11.Top = 8.125F;
			this.fldLTTSmallAmount11.Width = 1.125F;
			// 
			// fldLTTSmallUnits10
			// 
			this.fldLTTSmallUnits10.Height = 0.1875F;
			this.fldLTTSmallUnits10.Left = 4.8125F;
			this.fldLTTSmallUnits10.Name = "fldLTTSmallUnits10";
			this.fldLTTSmallUnits10.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits10.Text = "Field25";
			this.fldLTTSmallUnits10.Top = 8.3125F;
			this.fldLTTSmallUnits10.Width = 0.8125F;
			// 
			// fldLTTSmallAmount10
			// 
			this.fldLTTSmallAmount10.Height = 0.1875F;
			this.fldLTTSmallAmount10.Left = 5.9375F;
			this.fldLTTSmallAmount10.Name = "fldLTTSmallAmount10";
			this.fldLTTSmallAmount10.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount10.Text = "Field13";
			this.fldLTTSmallAmount10.Top = 8.3125F;
			this.fldLTTSmallAmount10.Width = 1.125F;
			// 
			// fldLTTSmallUnits9
			// 
			this.fldLTTSmallUnits9.Height = 0.1875F;
			this.fldLTTSmallUnits9.Left = 4.8125F;
			this.fldLTTSmallUnits9.Name = "fldLTTSmallUnits9";
			this.fldLTTSmallUnits9.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits9.Text = "Field1";
			this.fldLTTSmallUnits9.Top = 8.5F;
			this.fldLTTSmallUnits9.Width = 0.8125F;
			// 
			// fldLTTSmallAmount9
			// 
			this.fldLTTSmallAmount9.Height = 0.1875F;
			this.fldLTTSmallAmount9.Left = 5.9375F;
			this.fldLTTSmallAmount9.Name = "fldLTTSmallAmount9";
			this.fldLTTSmallAmount9.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount9.Text = "Field13";
			this.fldLTTSmallAmount9.Top = 8.5F;
			this.fldLTTSmallAmount9.Width = 1.125F;
			// 
			// fldLTTSmallUnits8
			// 
			this.fldLTTSmallUnits8.Height = 0.1875F;
			this.fldLTTSmallUnits8.Left = 4.8125F;
			this.fldLTTSmallUnits8.Name = "fldLTTSmallUnits8";
			this.fldLTTSmallUnits8.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits8.Text = "Field1";
			this.fldLTTSmallUnits8.Top = 8.6875F;
			this.fldLTTSmallUnits8.Width = 0.8125F;
			// 
			// fldLTTSmallAmount8
			// 
			this.fldLTTSmallAmount8.Height = 0.1875F;
			this.fldLTTSmallAmount8.Left = 5.9375F;
			this.fldLTTSmallAmount8.Name = "fldLTTSmallAmount8";
			this.fldLTTSmallAmount8.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount8.Text = "Field13";
			this.fldLTTSmallAmount8.Top = 8.6875F;
			this.fldLTTSmallAmount8.Width = 1.125F;
			// 
			// fldLTTSmallUnits7
			// 
			this.fldLTTSmallUnits7.Height = 0.1875F;
			this.fldLTTSmallUnits7.Left = 4.8125F;
			this.fldLTTSmallUnits7.Name = "fldLTTSmallUnits7";
			this.fldLTTSmallUnits7.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits7.Text = "Field1";
			this.fldLTTSmallUnits7.Top = 8.875F;
			this.fldLTTSmallUnits7.Width = 0.8125F;
			// 
			// fldLTTSmallAmount7
			// 
			this.fldLTTSmallAmount7.Height = 0.1875F;
			this.fldLTTSmallAmount7.Left = 5.9375F;
			this.fldLTTSmallAmount7.Name = "fldLTTSmallAmount7";
			this.fldLTTSmallAmount7.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount7.Text = "Field13";
			this.fldLTTSmallAmount7.Top = 8.875F;
			this.fldLTTSmallAmount7.Width = 1.125F;
			// 
			// fldLTTSmallUnits6
			// 
			this.fldLTTSmallUnits6.Height = 0.1875F;
			this.fldLTTSmallUnits6.Left = 4.8125F;
			this.fldLTTSmallUnits6.Name = "fldLTTSmallUnits6";
			this.fldLTTSmallUnits6.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits6.Text = "Field1";
			this.fldLTTSmallUnits6.Top = 9.0625F;
			this.fldLTTSmallUnits6.Width = 0.8125F;
			// 
			// fldLTTSmallAmount6
			// 
			this.fldLTTSmallAmount6.Height = 0.1875F;
			this.fldLTTSmallAmount6.Left = 5.9375F;
			this.fldLTTSmallAmount6.Name = "fldLTTSmallAmount6";
			this.fldLTTSmallAmount6.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount6.Text = "Field13";
			this.fldLTTSmallAmount6.Top = 9.0625F;
			this.fldLTTSmallAmount6.Width = 1.125F;
			// 
			// fldLTTSmallUnits5
			// 
			this.fldLTTSmallUnits5.Height = 0.1875F;
			this.fldLTTSmallUnits5.Left = 4.8125F;
			this.fldLTTSmallUnits5.Name = "fldLTTSmallUnits5";
			this.fldLTTSmallUnits5.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits5.Text = "Field1";
			this.fldLTTSmallUnits5.Top = 9.25F;
			this.fldLTTSmallUnits5.Width = 0.8125F;
			// 
			// fldLTTSmallAmount5
			// 
			this.fldLTTSmallAmount5.Height = 0.1875F;
			this.fldLTTSmallAmount5.Left = 5.9375F;
			this.fldLTTSmallAmount5.Name = "fldLTTSmallAmount5";
			this.fldLTTSmallAmount5.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount5.Text = "Field13";
			this.fldLTTSmallAmount5.Top = 9.25F;
			this.fldLTTSmallAmount5.Width = 1.125F;
			// 
			// fldLTTSmallUnitsSubTotal
			// 
			this.fldLTTSmallUnitsSubTotal.Height = 0.1875F;
			this.fldLTTSmallUnitsSubTotal.Left = 4.8125F;
			this.fldLTTSmallUnitsSubTotal.Name = "fldLTTSmallUnitsSubTotal";
			this.fldLTTSmallUnitsSubTotal.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnitsSubTotal.Text = "Field1";
			this.fldLTTSmallUnitsSubTotal.Top = 10F;
			this.fldLTTSmallUnitsSubTotal.Width = 0.8125F;
			// 
			// fldLTTSmallAmountSubTotal
			// 
			this.fldLTTSmallAmountSubTotal.Height = 0.1875F;
			this.fldLTTSmallAmountSubTotal.Left = 5.9375F;
			this.fldLTTSmallAmountSubTotal.Name = "fldLTTSmallAmountSubTotal";
			this.fldLTTSmallAmountSubTotal.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmountSubTotal.Text = "Field13";
			this.fldLTTSmallAmountSubTotal.Top = 10F;
			this.fldLTTSmallAmountSubTotal.Width = 1.125F;
			// 
			// fldLTTUnitsTotal
			// 
			this.fldLTTUnitsTotal.Height = 0.1875F;
			this.fldLTTUnitsTotal.Left = 4.8125F;
			this.fldLTTUnitsTotal.Name = "fldLTTUnitsTotal";
			this.fldLTTUnitsTotal.Style = "font-size: 9pt; text-align: right";
			this.fldLTTUnitsTotal.Text = "Field1";
			this.fldLTTUnitsTotal.Top = 10.1875F;
			this.fldLTTUnitsTotal.Width = 0.8125F;
			// 
			// fldLTTAmountTotal
			// 
			this.fldLTTAmountTotal.Height = 0.1875F;
			this.fldLTTAmountTotal.Left = 5.9375F;
			this.fldLTTAmountTotal.Name = "fldLTTAmountTotal";
			this.fldLTTAmountTotal.Style = "font-size: 9pt; text-align: right";
			this.fldLTTAmountTotal.Text = "Field13";
			this.fldLTTAmountTotal.Top = 10.1875F;
			this.fldLTTAmountTotal.Width = 1.125F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1875F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 0.1875F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label54.Text = "LTT - Large  4 Years";
			this.Label54.Top = 7.125F;
			this.Label54.Width = 2.0625F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1875F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 0.1875F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label55.Text = "LTT - Large  3 Years";
			this.Label55.Top = 7.3125F;
			this.Label55.Width = 2.0625F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1875F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 0.1875F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label56.Text = "LTT - Large  2 Years";
			this.Label56.Top = 7.5F;
			this.Label56.Width = 2.0625F;
			// 
			// fldLTTLargeUnits4
			// 
			this.fldLTTLargeUnits4.Height = 0.1875F;
			this.fldLTTLargeUnits4.Left = 4.8125F;
			this.fldLTTLargeUnits4.Name = "fldLTTLargeUnits4";
			this.fldLTTLargeUnits4.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits4.Text = "Field1";
			this.fldLTTLargeUnits4.Top = 7.125F;
			this.fldLTTLargeUnits4.Width = 0.8125F;
			// 
			// fldLTTLargeAmount4
			// 
			this.fldLTTLargeAmount4.Height = 0.1875F;
			this.fldLTTLargeAmount4.Left = 5.9375F;
			this.fldLTTLargeAmount4.Name = "fldLTTLargeAmount4";
			this.fldLTTLargeAmount4.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount4.Text = "Field13";
			this.fldLTTLargeAmount4.Top = 7.125F;
			this.fldLTTLargeAmount4.Width = 1.125F;
			// 
			// fldLTTLargeUnits3
			// 
			this.fldLTTLargeUnits3.Height = 0.1875F;
			this.fldLTTLargeUnits3.Left = 4.8125F;
			this.fldLTTLargeUnits3.Name = "fldLTTLargeUnits3";
			this.fldLTTLargeUnits3.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits3.Text = "Field1";
			this.fldLTTLargeUnits3.Top = 7.3125F;
			this.fldLTTLargeUnits3.Width = 0.8125F;
			// 
			// fldLTTLargeAmount3
			// 
			this.fldLTTLargeAmount3.Height = 0.1875F;
			this.fldLTTLargeAmount3.Left = 5.9375F;
			this.fldLTTLargeAmount3.Name = "fldLTTLargeAmount3";
			this.fldLTTLargeAmount3.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount3.Text = "Field13";
			this.fldLTTLargeAmount3.Top = 7.3125F;
			this.fldLTTLargeAmount3.Width = 1.125F;
			// 
			// fldLTTLargeUnits2
			// 
			this.fldLTTLargeUnits2.Height = 0.1875F;
			this.fldLTTLargeUnits2.Left = 4.8125F;
			this.fldLTTLargeUnits2.Name = "fldLTTLargeUnits2";
			this.fldLTTLargeUnits2.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits2.Text = "Field13";
			this.fldLTTLargeUnits2.Top = 7.5F;
			this.fldLTTLargeUnits2.Width = 0.8125F;
			// 
			// fldLTTLargeAmount2
			// 
			this.fldLTTLargeAmount2.Height = 0.1875F;
			this.fldLTTLargeAmount2.Left = 5.9375F;
			this.fldLTTLargeAmount2.Name = "fldLTTLargeAmount2";
			this.fldLTTLargeAmount2.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount2.Text = "Field13";
			this.fldLTTLargeAmount2.Top = 7.5F;
			this.fldLTTLargeAmount2.Width = 1.125F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1875F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 0.1875F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label57.Text = "LTT - Small  4 Years";
			this.Label57.Top = 9.4375F;
			this.Label57.Width = 2.0625F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 0.1875F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label58.Text = "LTT - Small  3 Years";
			this.Label58.Top = 9.625F;
			this.Label58.Width = 2.0625F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1875F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 0.1875F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label59.Text = "LTT - Small  2 Years";
			this.Label59.Top = 9.8125F;
			this.Label59.Width = 2.0625F;
			// 
			// fldLTTSmallUnits4
			// 
			this.fldLTTSmallUnits4.Height = 0.1875F;
			this.fldLTTSmallUnits4.Left = 4.8125F;
			this.fldLTTSmallUnits4.Name = "fldLTTSmallUnits4";
			this.fldLTTSmallUnits4.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits4.Text = "Field1";
			this.fldLTTSmallUnits4.Top = 9.4375F;
			this.fldLTTSmallUnits4.Width = 0.8125F;
			// 
			// fldLTTSmallAmount4
			// 
			this.fldLTTSmallAmount4.Height = 0.1875F;
			this.fldLTTSmallAmount4.Left = 5.9375F;
			this.fldLTTSmallAmount4.Name = "fldLTTSmallAmount4";
			this.fldLTTSmallAmount4.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount4.Text = "Field13";
			this.fldLTTSmallAmount4.Top = 9.4375F;
			this.fldLTTSmallAmount4.Width = 1.125F;
			// 
			// fldLTTSmallUnits3
			// 
			this.fldLTTSmallUnits3.Height = 0.1875F;
			this.fldLTTSmallUnits3.Left = 4.8125F;
			this.fldLTTSmallUnits3.Name = "fldLTTSmallUnits3";
			this.fldLTTSmallUnits3.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits3.Text = "Field1";
			this.fldLTTSmallUnits3.Top = 9.625F;
			this.fldLTTSmallUnits3.Width = 0.8125F;
			// 
			// fldLTTSmallAmount3
			// 
			this.fldLTTSmallAmount3.Height = 0.1875F;
			this.fldLTTSmallAmount3.Left = 5.9375F;
			this.fldLTTSmallAmount3.Name = "fldLTTSmallAmount3";
			this.fldLTTSmallAmount3.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount3.Text = "Field13";
			this.fldLTTSmallAmount3.Top = 9.625F;
			this.fldLTTSmallAmount3.Width = 1.125F;
			// 
			// fldLTTSmallUnits2
			// 
			this.fldLTTSmallUnits2.Height = 0.1875F;
			this.fldLTTSmallUnits2.Left = 4.8125F;
			this.fldLTTSmallUnits2.Name = "fldLTTSmallUnits2";
			this.fldLTTSmallUnits2.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallUnits2.Text = "Field1";
			this.fldLTTSmallUnits2.Top = 9.8125F;
			this.fldLTTSmallUnits2.Width = 0.8125F;
			// 
			// fldLTTSmallAmount2
			// 
			this.fldLTTSmallAmount2.Height = 0.1875F;
			this.fldLTTSmallAmount2.Left = 5.9375F;
			this.fldLTTSmallAmount2.Name = "fldLTTSmallAmount2";
			this.fldLTTSmallAmount2.Style = "font-size: 9pt; text-align: right";
			this.fldLTTSmallAmount2.Text = "Field13";
			this.fldLTTSmallAmount2.Top = 9.8125F;
			this.fldLTTSmallAmount2.Width = 1.125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 3.625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 9pt; text-align: right";
			this.Field1.Text = "144.00";
			this.Field1.Top = 4.6875F;
			this.Field1.Width = 0.875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 3.625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-size: 9pt; text-align: right";
			this.Field2.Text = "60.00";
			this.Field2.Top = 7.9375F;
			this.Field2.Width = 0.875F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 3.625F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-size: 9pt; text-align: right";
			this.Field12.Text = "132.00";
			this.Field12.Top = 5.0625F;
			this.Field12.Width = 0.875F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 3.625F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-size: 9pt; text-align: right";
			this.Field13.Text = "120.00";
			this.Field13.Top = 5.4375F;
			this.Field13.Width = 0.875F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 3.625F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-size: 9pt; text-align: right";
			this.Field14.Text = "108.00";
			this.Field14.Top = 5.8125F;
			this.Field14.Width = 0.875F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 3.625F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-size: 9pt; text-align: right";
			this.Field15.Text = "96.00";
			this.Field15.Top = 6.1875F;
			this.Field15.Width = 0.875F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 3.625F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-size: 9pt; text-align: right";
			this.Field16.Text = "84.00";
			this.Field16.Top = 6.5625F;
			this.Field16.Width = 0.875F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 3.625F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-size: 9pt; text-align: right";
			this.Field17.Text = "72.00";
			this.Field17.Top = 6.75F;
			this.Field17.Width = 0.875F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1875F;
			this.Field18.Left = 3.625F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-size: 9pt; text-align: right";
			this.Field18.Text = "60.00";
			this.Field18.Top = 6.9375F;
			this.Field18.Width = 0.875F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 3.625F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-size: 9pt; text-align: right";
			this.Field20.Text = "55.00";
			this.Field20.Top = 8.125F;
			this.Field20.Width = 0.875F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 3.625F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-size: 9pt; text-align: right";
			this.Field21.Text = "50.00";
			this.Field21.Top = 8.3125F;
			this.Field21.Width = 0.875F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 3.625F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-size: 9pt; text-align: right";
			this.Field22.Text = "45.00";
			this.Field22.Top = 8.5F;
			this.Field22.Width = 0.875F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 3.625F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-size: 9pt; text-align: right";
			this.Field23.Text = "40.00";
			this.Field23.Top = 8.6875F;
			this.Field23.Width = 0.875F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1875F;
			this.Field24.Left = 3.625F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-size: 9pt; text-align: right";
			this.Field24.Text = "35.00";
			this.Field24.Top = 8.875F;
			this.Field24.Width = 0.875F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1875F;
			this.Field25.Left = 3.625F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-size: 9pt; text-align: right";
			this.Field25.Text = "30.00";
			this.Field25.Top = 9.0625F;
			this.Field25.Width = 0.875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 3.625F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-size: 9pt; text-align: right";
			this.Field26.Text = "25.00";
			this.Field26.Top = 9.25F;
			this.Field26.Width = 0.875F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 3.625F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-size: 9pt; text-align: right";
			this.Field29.Text = "48.00";
			this.Field29.Top = 7.125F;
			this.Field29.Width = 0.875F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 3.625F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-size: 9pt; text-align: right";
			this.Field30.Text = "36.00";
			this.Field30.Top = 7.3125F;
			this.Field30.Width = 0.875F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.1875F;
			this.Field31.Left = 3.625F;
			this.Field31.Name = "Field31";
			this.Field31.Style = "font-size: 9pt; text-align: right";
			this.Field31.Text = "24.00";
			this.Field31.Top = 7.5F;
			this.Field31.Width = 0.875F;
			// 
			// Field32
			// 
			this.Field32.Height = 0.1875F;
			this.Field32.Left = 3.625F;
			this.Field32.Name = "Field32";
			this.Field32.Style = "font-size: 9pt; text-align: right";
			this.Field32.Text = "20.00";
			this.Field32.Top = 9.4375F;
			this.Field32.Width = 0.875F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1875F;
			this.Field33.Left = 3.625F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-size: 9pt; text-align: right";
			this.Field33.Text = "15.00";
			this.Field33.Top = 9.625F;
			this.Field33.Width = 0.875F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.1875F;
			this.Field34.Left = 3.625F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-size: 9pt; text-align: right";
			this.Field34.Text = "10.00";
			this.Field34.Top = 9.8125F;
			this.Field34.Width = 0.875F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1875F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 0.1875F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label61.Text = "LTT - Large  20 Years";
			this.Label61.Top = 1.6875F;
			this.Label61.Width = 2.0625F;
			// 
			// fldLTTLargeUnits20Full
			// 
			this.fldLTTLargeUnits20Full.Height = 0.1875F;
			this.fldLTTLargeUnits20Full.Left = 4.8125F;
			this.fldLTTLargeUnits20Full.Name = "fldLTTLargeUnits20Full";
			this.fldLTTLargeUnits20Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits20Full.Text = "Field1";
			this.fldLTTLargeUnits20Full.Top = 1.6875F;
			this.fldLTTLargeUnits20Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount20Full
			// 
			this.fldLTTLargeAmount20Full.Height = 0.1875F;
			this.fldLTTLargeAmount20Full.Left = 5.9375F;
			this.fldLTTLargeAmount20Full.Name = "fldLTTLargeAmount20Full";
			this.fldLTTLargeAmount20Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount20Full.Text = "Field13";
			this.fldLTTLargeAmount20Full.Top = 1.6875F;
			this.fldLTTLargeAmount20Full.Width = 1.125F;
			// 
			// Field51
			// 
			this.Field51.Height = 0.1875F;
			this.Field51.Left = 3.625F;
			this.Field51.Name = "Field51";
			this.Field51.Style = "font-size: 9pt; text-align: right";
			this.Field51.Text = "240.00";
			this.Field51.Top = 1.6875F;
			this.Field51.Width = 0.875F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 2.375F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label70.Text = "Full";
			this.Label70.Top = 1.6875F;
			this.Label70.Width = 0.875F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 0.1875F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label71.Text = "LTT - Large  20 Years";
			this.Label71.Top = 1.875F;
			this.Label71.Width = 2.0625F;
			// 
			// fldLTTLargeUnits20Time
			// 
			this.fldLTTLargeUnits20Time.Height = 0.1875F;
			this.fldLTTLargeUnits20Time.Left = 4.8125F;
			this.fldLTTLargeUnits20Time.Name = "fldLTTLargeUnits20Time";
			this.fldLTTLargeUnits20Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits20Time.Text = "Field1";
			this.fldLTTLargeUnits20Time.Top = 1.875F;
			this.fldLTTLargeUnits20Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount20Time
			// 
			this.fldLTTLargeAmount20Time.Height = 0.1875F;
			this.fldLTTLargeAmount20Time.Left = 5.9375F;
			this.fldLTTLargeAmount20Time.Name = "fldLTTLargeAmount20Time";
			this.fldLTTLargeAmount20Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount20Time.Text = "Field13";
			this.fldLTTLargeAmount20Time.Top = 1.875F;
			this.fldLTTLargeAmount20Time.Width = 1.125F;
			// 
			// Field61
			// 
			this.Field61.Height = 0.1875F;
			this.Field61.Left = 3.625F;
			this.Field61.Name = "Field61";
			this.Field61.Style = "font-size: 9pt; text-align: right";
			this.Field61.Text = "36.00";
			this.Field61.Top = 1.875F;
			this.Field61.Width = 0.875F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 2.375F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label72.Text = "Time";
			this.Label72.Top = 1.875F;
			this.Label72.Width = 0.875F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1875F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 0.1875F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label73.Text = "LTT - Large  19 Years";
			this.Label73.Top = 2.0625F;
			this.Label73.Width = 2.0625F;
			// 
			// fldLTTLargeUnits19Full
			// 
			this.fldLTTLargeUnits19Full.Height = 0.1875F;
			this.fldLTTLargeUnits19Full.Left = 4.8125F;
			this.fldLTTLargeUnits19Full.Name = "fldLTTLargeUnits19Full";
			this.fldLTTLargeUnits19Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits19Full.Text = "Field1";
			this.fldLTTLargeUnits19Full.Top = 2.0625F;
			this.fldLTTLargeUnits19Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount19Full
			// 
			this.fldLTTLargeAmount19Full.Height = 0.1875F;
			this.fldLTTLargeAmount19Full.Left = 5.9375F;
			this.fldLTTLargeAmount19Full.Name = "fldLTTLargeAmount19Full";
			this.fldLTTLargeAmount19Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount19Full.Text = "Field13";
			this.fldLTTLargeAmount19Full.Top = 2.0625F;
			this.fldLTTLargeAmount19Full.Width = 1.125F;
			// 
			// Field64
			// 
			this.Field64.Height = 0.1875F;
			this.Field64.Left = 3.625F;
			this.Field64.Name = "Field64";
			this.Field64.Style = "font-size: 9pt; text-align: right";
			this.Field64.Text = "228.00";
			this.Field64.Top = 2.0625F;
			this.Field64.Width = 0.875F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 2.375F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label74.Text = "Full";
			this.Label74.Top = 2.0625F;
			this.Label74.Width = 0.875F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1875F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 0.1875F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label75.Text = "LTT - Large  19 Years";
			this.Label75.Top = 2.25F;
			this.Label75.Width = 2.0625F;
			// 
			// fldLTTLargeUnits19Time
			// 
			this.fldLTTLargeUnits19Time.Height = 0.1875F;
			this.fldLTTLargeUnits19Time.Left = 4.8125F;
			this.fldLTTLargeUnits19Time.Name = "fldLTTLargeUnits19Time";
			this.fldLTTLargeUnits19Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits19Time.Text = "Field1";
			this.fldLTTLargeUnits19Time.Top = 2.25F;
			this.fldLTTLargeUnits19Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount19Time
			// 
			this.fldLTTLargeAmount19Time.Height = 0.1875F;
			this.fldLTTLargeAmount19Time.Left = 5.9375F;
			this.fldLTTLargeAmount19Time.Name = "fldLTTLargeAmount19Time";
			this.fldLTTLargeAmount19Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount19Time.Text = "Field13";
			this.fldLTTLargeAmount19Time.Top = 2.25F;
			this.fldLTTLargeAmount19Time.Width = 1.125F;
			// 
			// Field67
			// 
			this.Field67.Height = 0.1875F;
			this.Field67.Left = 3.625F;
			this.Field67.Name = "Field67";
			this.Field67.Style = "font-size: 9pt; text-align: right";
			this.Field67.Text = "36.00";
			this.Field67.Top = 2.25F;
			this.Field67.Width = 0.875F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.1875F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 2.375F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label76.Text = "Time";
			this.Label76.Top = 2.25F;
			this.Label76.Width = 0.875F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1875F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 0.1875F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label77.Text = "LTT - Large  18 Years";
			this.Label77.Top = 2.4375F;
			this.Label77.Width = 2.0625F;
			// 
			// fldLTTLargeUnits18Full
			// 
			this.fldLTTLargeUnits18Full.Height = 0.1875F;
			this.fldLTTLargeUnits18Full.Left = 4.8125F;
			this.fldLTTLargeUnits18Full.Name = "fldLTTLargeUnits18Full";
			this.fldLTTLargeUnits18Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits18Full.Text = "Field1";
			this.fldLTTLargeUnits18Full.Top = 2.4375F;
			this.fldLTTLargeUnits18Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount18Full
			// 
			this.fldLTTLargeAmount18Full.Height = 0.1875F;
			this.fldLTTLargeAmount18Full.Left = 5.9375F;
			this.fldLTTLargeAmount18Full.Name = "fldLTTLargeAmount18Full";
			this.fldLTTLargeAmount18Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount18Full.Text = "Field13";
			this.fldLTTLargeAmount18Full.Top = 2.4375F;
			this.fldLTTLargeAmount18Full.Width = 1.125F;
			// 
			// Field70
			// 
			this.Field70.Height = 0.1875F;
			this.Field70.Left = 3.625F;
			this.Field70.Name = "Field70";
			this.Field70.Style = "font-size: 9pt; text-align: right";
			this.Field70.Text = "216.00";
			this.Field70.Top = 2.4375F;
			this.Field70.Width = 0.875F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1875F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 2.375F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label78.Text = "Full";
			this.Label78.Top = 2.4375F;
			this.Label78.Width = 0.875F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1875F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 0.1875F;
			this.Label79.Name = "Label79";
			this.Label79.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label79.Text = "LTT - Large  18 Years";
			this.Label79.Top = 2.625F;
			this.Label79.Width = 2.0625F;
			// 
			// fldLTTLargeUnits18Time
			// 
			this.fldLTTLargeUnits18Time.Height = 0.1875F;
			this.fldLTTLargeUnits18Time.Left = 4.8125F;
			this.fldLTTLargeUnits18Time.Name = "fldLTTLargeUnits18Time";
			this.fldLTTLargeUnits18Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits18Time.Text = "Field1";
			this.fldLTTLargeUnits18Time.Top = 2.625F;
			this.fldLTTLargeUnits18Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount18Time
			// 
			this.fldLTTLargeAmount18Time.Height = 0.1875F;
			this.fldLTTLargeAmount18Time.Left = 5.9375F;
			this.fldLTTLargeAmount18Time.Name = "fldLTTLargeAmount18Time";
			this.fldLTTLargeAmount18Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount18Time.Text = "Field13";
			this.fldLTTLargeAmount18Time.Top = 2.625F;
			this.fldLTTLargeAmount18Time.Width = 1.125F;
			// 
			// Field73
			// 
			this.Field73.Height = 0.1875F;
			this.Field73.Left = 3.625F;
			this.Field73.Name = "Field73";
			this.Field73.Style = "font-size: 9pt; text-align: right";
			this.Field73.Text = "36.00";
			this.Field73.Top = 2.625F;
			this.Field73.Width = 0.875F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1875F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 2.375F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label80.Text = "Time";
			this.Label80.Top = 2.625F;
			this.Label80.Width = 0.875F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1875F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 0.1875F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label81.Text = "LTT - Large  17 Years";
			this.Label81.Top = 2.8125F;
			this.Label81.Width = 2.0625F;
			// 
			// fldLTTLargeUnits17Full
			// 
			this.fldLTTLargeUnits17Full.Height = 0.1875F;
			this.fldLTTLargeUnits17Full.Left = 4.8125F;
			this.fldLTTLargeUnits17Full.Name = "fldLTTLargeUnits17Full";
			this.fldLTTLargeUnits17Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits17Full.Text = "Field1";
			this.fldLTTLargeUnits17Full.Top = 2.8125F;
			this.fldLTTLargeUnits17Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount17Full
			// 
			this.fldLTTLargeAmount17Full.Height = 0.1875F;
			this.fldLTTLargeAmount17Full.Left = 5.9375F;
			this.fldLTTLargeAmount17Full.Name = "fldLTTLargeAmount17Full";
			this.fldLTTLargeAmount17Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount17Full.Text = "Field13";
			this.fldLTTLargeAmount17Full.Top = 2.8125F;
			this.fldLTTLargeAmount17Full.Width = 1.125F;
			// 
			// Field76
			// 
			this.Field76.Height = 0.1875F;
			this.Field76.Left = 3.625F;
			this.Field76.Name = "Field76";
			this.Field76.Style = "font-size: 9pt; text-align: right";
			this.Field76.Text = "204.00";
			this.Field76.Top = 2.8125F;
			this.Field76.Width = 0.875F;
			// 
			// Label82
			// 
			this.Label82.Height = 0.1875F;
			this.Label82.HyperLink = null;
			this.Label82.Left = 2.375F;
			this.Label82.Name = "Label82";
			this.Label82.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label82.Text = "Full";
			this.Label82.Top = 2.8125F;
			this.Label82.Width = 0.875F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1875F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 0.1875F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label83.Text = "LTT - Large  17 Years";
			this.Label83.Top = 3F;
			this.Label83.Width = 2.0625F;
			// 
			// fldLTTLargeUnits17Time
			// 
			this.fldLTTLargeUnits17Time.Height = 0.1875F;
			this.fldLTTLargeUnits17Time.Left = 4.8125F;
			this.fldLTTLargeUnits17Time.Name = "fldLTTLargeUnits17Time";
			this.fldLTTLargeUnits17Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits17Time.Text = "Field1";
			this.fldLTTLargeUnits17Time.Top = 3F;
			this.fldLTTLargeUnits17Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount17Time
			// 
			this.fldLTTLargeAmount17Time.Height = 0.1875F;
			this.fldLTTLargeAmount17Time.Left = 5.9375F;
			this.fldLTTLargeAmount17Time.Name = "fldLTTLargeAmount17Time";
			this.fldLTTLargeAmount17Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount17Time.Text = "Field13";
			this.fldLTTLargeAmount17Time.Top = 3F;
			this.fldLTTLargeAmount17Time.Width = 1.125F;
			// 
			// Field79
			// 
			this.Field79.Height = 0.1875F;
			this.Field79.Left = 3.625F;
			this.Field79.Name = "Field79";
			this.Field79.Style = "font-size: 9pt; text-align: right";
			this.Field79.Text = "36.00";
			this.Field79.Top = 3F;
			this.Field79.Width = 0.875F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1875F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 2.375F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label84.Text = "Time";
			this.Label84.Top = 3F;
			this.Label84.Width = 0.875F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1875F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0.1875F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label85.Text = "LTT - Large  16 Years";
			this.Label85.Top = 3.1875F;
			this.Label85.Width = 2.0625F;
			// 
			// fldLTTLargeUnits16Full
			// 
			this.fldLTTLargeUnits16Full.Height = 0.1875F;
			this.fldLTTLargeUnits16Full.Left = 4.8125F;
			this.fldLTTLargeUnits16Full.Name = "fldLTTLargeUnits16Full";
			this.fldLTTLargeUnits16Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits16Full.Text = "Field1";
			this.fldLTTLargeUnits16Full.Top = 3.1875F;
			this.fldLTTLargeUnits16Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount16Full
			// 
			this.fldLTTLargeAmount16Full.Height = 0.1875F;
			this.fldLTTLargeAmount16Full.Left = 5.9375F;
			this.fldLTTLargeAmount16Full.Name = "fldLTTLargeAmount16Full";
			this.fldLTTLargeAmount16Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount16Full.Text = "Field13";
			this.fldLTTLargeAmount16Full.Top = 3.1875F;
			this.fldLTTLargeAmount16Full.Width = 1.125F;
			// 
			// Field82
			// 
			this.Field82.Height = 0.1875F;
			this.Field82.Left = 3.625F;
			this.Field82.Name = "Field82";
			this.Field82.Style = "font-size: 9pt; text-align: right";
			this.Field82.Text = "192.00";
			this.Field82.Top = 3.1875F;
			this.Field82.Width = 0.875F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1875F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 2.375F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label86.Text = "Full";
			this.Label86.Top = 3.1875F;
			this.Label86.Width = 0.875F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1875F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 0.1875F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label87.Text = "LTT - Large  16 Years";
			this.Label87.Top = 3.375F;
			this.Label87.Width = 2.0625F;
			// 
			// fldLTTLargeUnits16Time
			// 
			this.fldLTTLargeUnits16Time.Height = 0.1875F;
			this.fldLTTLargeUnits16Time.Left = 4.8125F;
			this.fldLTTLargeUnits16Time.Name = "fldLTTLargeUnits16Time";
			this.fldLTTLargeUnits16Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits16Time.Text = "Field1";
			this.fldLTTLargeUnits16Time.Top = 3.375F;
			this.fldLTTLargeUnits16Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount16Time
			// 
			this.fldLTTLargeAmount16Time.Height = 0.1875F;
			this.fldLTTLargeAmount16Time.Left = 5.9375F;
			this.fldLTTLargeAmount16Time.Name = "fldLTTLargeAmount16Time";
			this.fldLTTLargeAmount16Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount16Time.Text = "Field13";
			this.fldLTTLargeAmount16Time.Top = 3.375F;
			this.fldLTTLargeAmount16Time.Width = 1.125F;
			// 
			// Field85
			// 
			this.Field85.Height = 0.1875F;
			this.Field85.Left = 3.625F;
			this.Field85.Name = "Field85";
			this.Field85.Style = "font-size: 9pt; text-align: right";
			this.Field85.Text = "36.00";
			this.Field85.Top = 3.375F;
			this.Field85.Width = 0.875F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1875F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 2.375F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label88.Text = "Time";
			this.Label88.Top = 3.375F;
			this.Label88.Width = 0.875F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1875F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 0.1875F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label89.Text = "LTT - Large  15 Years";
			this.Label89.Top = 3.5625F;
			this.Label89.Width = 2.0625F;
			// 
			// fldLTTLargeUnits15Full
			// 
			this.fldLTTLargeUnits15Full.Height = 0.1875F;
			this.fldLTTLargeUnits15Full.Left = 4.8125F;
			this.fldLTTLargeUnits15Full.Name = "fldLTTLargeUnits15Full";
			this.fldLTTLargeUnits15Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits15Full.Text = "Field1";
			this.fldLTTLargeUnits15Full.Top = 3.5625F;
			this.fldLTTLargeUnits15Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount15Full
			// 
			this.fldLTTLargeAmount15Full.Height = 0.1875F;
			this.fldLTTLargeAmount15Full.Left = 5.9375F;
			this.fldLTTLargeAmount15Full.Name = "fldLTTLargeAmount15Full";
			this.fldLTTLargeAmount15Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount15Full.Text = "Field13";
			this.fldLTTLargeAmount15Full.Top = 3.5625F;
			this.fldLTTLargeAmount15Full.Width = 1.125F;
			// 
			// Field88
			// 
			this.Field88.Height = 0.1875F;
			this.Field88.Left = 3.625F;
			this.Field88.Name = "Field88";
			this.Field88.Style = "font-size: 9pt; text-align: right";
			this.Field88.Text = "180.00";
			this.Field88.Top = 3.5625F;
			this.Field88.Width = 0.875F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1875F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 2.375F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label90.Text = "Full";
			this.Label90.Top = 3.5625F;
			this.Label90.Width = 0.875F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1875F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 0.1875F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label91.Text = "LTT - Large  15 Years";
			this.Label91.Top = 3.75F;
			this.Label91.Width = 2.0625F;
			// 
			// fldLTTLargeUnits15Time
			// 
			this.fldLTTLargeUnits15Time.Height = 0.1875F;
			this.fldLTTLargeUnits15Time.Left = 4.8125F;
			this.fldLTTLargeUnits15Time.Name = "fldLTTLargeUnits15Time";
			this.fldLTTLargeUnits15Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits15Time.Text = "Field1";
			this.fldLTTLargeUnits15Time.Top = 3.75F;
			this.fldLTTLargeUnits15Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount15Time
			// 
			this.fldLTTLargeAmount15Time.Height = 0.1875F;
			this.fldLTTLargeAmount15Time.Left = 5.9375F;
			this.fldLTTLargeAmount15Time.Name = "fldLTTLargeAmount15Time";
			this.fldLTTLargeAmount15Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount15Time.Text = "Field13";
			this.fldLTTLargeAmount15Time.Top = 3.75F;
			this.fldLTTLargeAmount15Time.Width = 1.125F;
			// 
			// Field91
			// 
			this.Field91.Height = 0.1875F;
			this.Field91.Left = 3.625F;
			this.Field91.Name = "Field91";
			this.Field91.Style = "font-size: 9pt; text-align: right";
			this.Field91.Text = "36.00";
			this.Field91.Top = 3.75F;
			this.Field91.Width = 0.875F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.1875F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 2.375F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label92.Text = "Time";
			this.Label92.Top = 3.75F;
			this.Label92.Width = 0.875F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1875F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 0.1875F;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label93.Text = "LTT - Large  14 Years";
			this.Label93.Top = 3.9375F;
			this.Label93.Width = 2.0625F;
			// 
			// fldLTTLargeUnits14Full
			// 
			this.fldLTTLargeUnits14Full.Height = 0.1875F;
			this.fldLTTLargeUnits14Full.Left = 4.8125F;
			this.fldLTTLargeUnits14Full.Name = "fldLTTLargeUnits14Full";
			this.fldLTTLargeUnits14Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits14Full.Text = "Field1";
			this.fldLTTLargeUnits14Full.Top = 3.9375F;
			this.fldLTTLargeUnits14Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount14Full
			// 
			this.fldLTTLargeAmount14Full.Height = 0.1875F;
			this.fldLTTLargeAmount14Full.Left = 5.9375F;
			this.fldLTTLargeAmount14Full.Name = "fldLTTLargeAmount14Full";
			this.fldLTTLargeAmount14Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount14Full.Text = "Field13";
			this.fldLTTLargeAmount14Full.Top = 3.9375F;
			this.fldLTTLargeAmount14Full.Width = 1.125F;
			// 
			// Field94
			// 
			this.Field94.Height = 0.1875F;
			this.Field94.Left = 3.625F;
			this.Field94.Name = "Field94";
			this.Field94.Style = "font-size: 9pt; text-align: right";
			this.Field94.Text = "168.00";
			this.Field94.Top = 3.9375F;
			this.Field94.Width = 0.875F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1875F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 2.375F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label94.Text = "Full";
			this.Label94.Top = 3.9375F;
			this.Label94.Width = 0.875F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1875F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 0.1875F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label95.Text = "LTT - Large  14 Years";
			this.Label95.Top = 4.125F;
			this.Label95.Width = 2.0625F;
			// 
			// fldLTTLargeUnits14Time
			// 
			this.fldLTTLargeUnits14Time.Height = 0.1875F;
			this.fldLTTLargeUnits14Time.Left = 4.8125F;
			this.fldLTTLargeUnits14Time.Name = "fldLTTLargeUnits14Time";
			this.fldLTTLargeUnits14Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits14Time.Text = "Field1";
			this.fldLTTLargeUnits14Time.Top = 4.125F;
			this.fldLTTLargeUnits14Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount14Time
			// 
			this.fldLTTLargeAmount14Time.Height = 0.1875F;
			this.fldLTTLargeAmount14Time.Left = 5.9375F;
			this.fldLTTLargeAmount14Time.Name = "fldLTTLargeAmount14Time";
			this.fldLTTLargeAmount14Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount14Time.Text = "Field13";
			this.fldLTTLargeAmount14Time.Top = 4.125F;
			this.fldLTTLargeAmount14Time.Width = 1.125F;
			// 
			// Field97
			// 
			this.Field97.Height = 0.1875F;
			this.Field97.Left = 3.625F;
			this.Field97.Name = "Field97";
			this.Field97.Style = "font-size: 9pt; text-align: right";
			this.Field97.Text = "36.00";
			this.Field97.Top = 4.125F;
			this.Field97.Width = 0.875F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1875F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 2.375F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label96.Text = "Time";
			this.Label96.Top = 4.125F;
			this.Label96.Width = 0.875F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1875F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 0.1875F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label97.Text = "LTT - Large  13 Years";
			this.Label97.Top = 4.3125F;
			this.Label97.Width = 2.0625F;
			// 
			// fldLTTLargeUnits13Full
			// 
			this.fldLTTLargeUnits13Full.Height = 0.1875F;
			this.fldLTTLargeUnits13Full.Left = 4.8125F;
			this.fldLTTLargeUnits13Full.Name = "fldLTTLargeUnits13Full";
			this.fldLTTLargeUnits13Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits13Full.Text = "Field1";
			this.fldLTTLargeUnits13Full.Top = 4.3125F;
			this.fldLTTLargeUnits13Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount13Full
			// 
			this.fldLTTLargeAmount13Full.Height = 0.1875F;
			this.fldLTTLargeAmount13Full.Left = 5.9375F;
			this.fldLTTLargeAmount13Full.Name = "fldLTTLargeAmount13Full";
			this.fldLTTLargeAmount13Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount13Full.Text = "Field13";
			this.fldLTTLargeAmount13Full.Top = 4.3125F;
			this.fldLTTLargeAmount13Full.Width = 1.125F;
			// 
			// Field100
			// 
			this.Field100.Height = 0.1875F;
			this.Field100.Left = 3.625F;
			this.Field100.Name = "Field100";
			this.Field100.Style = "font-size: 9pt; text-align: right";
			this.Field100.Text = "156.00";
			this.Field100.Top = 4.3125F;
			this.Field100.Width = 0.875F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1875F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 2.375F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label98.Text = "Full";
			this.Label98.Top = 4.3125F;
			this.Label98.Width = 0.875F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1875F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0.1875F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label99.Text = "LTT - Large  13 Years";
			this.Label99.Top = 4.5F;
			this.Label99.Width = 2.0625F;
			// 
			// fldLTTLargeUnits13Time
			// 
			this.fldLTTLargeUnits13Time.Height = 0.1875F;
			this.fldLTTLargeUnits13Time.Left = 4.8125F;
			this.fldLTTLargeUnits13Time.Name = "fldLTTLargeUnits13Time";
			this.fldLTTLargeUnits13Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits13Time.Text = "Field1";
			this.fldLTTLargeUnits13Time.Top = 4.5F;
			this.fldLTTLargeUnits13Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount13Time
			// 
			this.fldLTTLargeAmount13Time.Height = 0.1875F;
			this.fldLTTLargeAmount13Time.Left = 5.9375F;
			this.fldLTTLargeAmount13Time.Name = "fldLTTLargeAmount13Time";
			this.fldLTTLargeAmount13Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount13Time.Text = "Field13";
			this.fldLTTLargeAmount13Time.Top = 4.5F;
			this.fldLTTLargeAmount13Time.Width = 1.125F;
			// 
			// Field103
			// 
			this.Field103.Height = 0.1875F;
			this.Field103.Left = 3.625F;
			this.Field103.Name = "Field103";
			this.Field103.Style = "font-size: 9pt; text-align: right";
			this.Field103.Text = "36.00";
			this.Field103.Top = 4.5F;
			this.Field103.Width = 0.875F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1875F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 2.375F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label100.Text = "Time";
			this.Label100.Top = 4.5F;
			this.Label100.Width = 0.875F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.1875F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 0.1875F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label101.Text = "LTT - Large  12 Years";
			this.Label101.Top = 4.875F;
			this.Label101.Width = 2.0625F;
			// 
			// fldLTTLargeUnits12Time
			// 
			this.fldLTTLargeUnits12Time.Height = 0.1875F;
			this.fldLTTLargeUnits12Time.Left = 4.8125F;
			this.fldLTTLargeUnits12Time.Name = "fldLTTLargeUnits12Time";
			this.fldLTTLargeUnits12Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits12Time.Text = "Field1";
			this.fldLTTLargeUnits12Time.Top = 4.875F;
			this.fldLTTLargeUnits12Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount12Time
			// 
			this.fldLTTLargeAmount12Time.Height = 0.1875F;
			this.fldLTTLargeAmount12Time.Left = 5.9375F;
			this.fldLTTLargeAmount12Time.Name = "fldLTTLargeAmount12Time";
			this.fldLTTLargeAmount12Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount12Time.Text = "Field13";
			this.fldLTTLargeAmount12Time.Top = 4.875F;
			this.fldLTTLargeAmount12Time.Width = 1.125F;
			// 
			// Field106
			// 
			this.Field106.Height = 0.1875F;
			this.Field106.Left = 3.625F;
			this.Field106.Name = "Field106";
			this.Field106.Style = "font-size: 9pt; text-align: right";
			this.Field106.Text = "48.00";
			this.Field106.Top = 4.875F;
			this.Field106.Width = 0.875F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1875F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 2.375F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label102.Text = "Full";
			this.Label102.Top = 4.6875F;
			this.Label102.Width = 0.875F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.1875F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 2.375F;
			this.Label103.Name = "Label103";
			this.Label103.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label103.Text = "Time";
			this.Label103.Top = 4.875F;
			this.Label103.Width = 0.875F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.1875F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 0.1875F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label104.Text = "LTT - Large  11 Years";
			this.Label104.Top = 5.25F;
			this.Label104.Width = 2.0625F;
			// 
			// fldLTTLargeUnits11Time
			// 
			this.fldLTTLargeUnits11Time.Height = 0.1875F;
			this.fldLTTLargeUnits11Time.Left = 4.8125F;
			this.fldLTTLargeUnits11Time.Name = "fldLTTLargeUnits11Time";
			this.fldLTTLargeUnits11Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits11Time.Text = "Field1";
			this.fldLTTLargeUnits11Time.Top = 5.25F;
			this.fldLTTLargeUnits11Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount11Time
			// 
			this.fldLTTLargeAmount11Time.Height = 0.1875F;
			this.fldLTTLargeAmount11Time.Left = 5.9375F;
			this.fldLTTLargeAmount11Time.Name = "fldLTTLargeAmount11Time";
			this.fldLTTLargeAmount11Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount11Time.Text = "Field13";
			this.fldLTTLargeAmount11Time.Top = 5.25F;
			this.fldLTTLargeAmount11Time.Width = 1.125F;
			// 
			// Field109
			// 
			this.Field109.Height = 0.1875F;
			this.Field109.Left = 3.625F;
			this.Field109.Name = "Field109";
			this.Field109.Style = "font-size: 9pt; text-align: right";
			this.Field109.Text = "48.00";
			this.Field109.Top = 5.25F;
			this.Field109.Width = 0.875F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.1875F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 2.375F;
			this.Label105.Name = "Label105";
			this.Label105.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label105.Text = "Full";
			this.Label105.Top = 5.0625F;
			this.Label105.Width = 0.875F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.1875F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 2.375F;
			this.Label106.Name = "Label106";
			this.Label106.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label106.Text = "Time";
			this.Label106.Top = 5.25F;
			this.Label106.Width = 0.875F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.1875F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 0.1875F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label107.Text = "LTT - Large  10 Years";
			this.Label107.Top = 5.625F;
			this.Label107.Width = 2.0625F;
			// 
			// fldLTTLargeUnits10Time
			// 
			this.fldLTTLargeUnits10Time.Height = 0.1875F;
			this.fldLTTLargeUnits10Time.Left = 4.8125F;
			this.fldLTTLargeUnits10Time.Name = "fldLTTLargeUnits10Time";
			this.fldLTTLargeUnits10Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits10Time.Text = "Field1";
			this.fldLTTLargeUnits10Time.Top = 5.625F;
			this.fldLTTLargeUnits10Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount10Time
			// 
			this.fldLTTLargeAmount10Time.Height = 0.1875F;
			this.fldLTTLargeAmount10Time.Left = 5.9375F;
			this.fldLTTLargeAmount10Time.Name = "fldLTTLargeAmount10Time";
			this.fldLTTLargeAmount10Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount10Time.Text = "Field13";
			this.fldLTTLargeAmount10Time.Top = 5.625F;
			this.fldLTTLargeAmount10Time.Width = 1.125F;
			// 
			// Field112
			// 
			this.Field112.Height = 0.1875F;
			this.Field112.Left = 3.625F;
			this.Field112.Name = "Field112";
			this.Field112.Style = "font-size: 9pt; text-align: right";
			this.Field112.Text = "48.00";
			this.Field112.Top = 5.625F;
			this.Field112.Width = 0.875F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.1875F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 2.375F;
			this.Label108.Name = "Label108";
			this.Label108.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label108.Text = "Full";
			this.Label108.Top = 5.4375F;
			this.Label108.Width = 0.875F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.1875F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 2.375F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label109.Text = "Time";
			this.Label109.Top = 5.625F;
			this.Label109.Width = 0.875F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1875F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 0.1875F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label110.Text = "LTT - Large  9 Years";
			this.Label110.Top = 6F;
			this.Label110.Width = 2.0625F;
			// 
			// fldLTTLargeUnits9Time
			// 
			this.fldLTTLargeUnits9Time.Height = 0.1875F;
			this.fldLTTLargeUnits9Time.Left = 4.8125F;
			this.fldLTTLargeUnits9Time.Name = "fldLTTLargeUnits9Time";
			this.fldLTTLargeUnits9Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits9Time.Text = "Field1";
			this.fldLTTLargeUnits9Time.Top = 6F;
			this.fldLTTLargeUnits9Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount9Time
			// 
			this.fldLTTLargeAmount9Time.Height = 0.1875F;
			this.fldLTTLargeAmount9Time.Left = 5.9375F;
			this.fldLTTLargeAmount9Time.Name = "fldLTTLargeAmount9Time";
			this.fldLTTLargeAmount9Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount9Time.Text = "Field13";
			this.fldLTTLargeAmount9Time.Top = 6F;
			this.fldLTTLargeAmount9Time.Width = 1.125F;
			// 
			// Field115
			// 
			this.Field115.Height = 0.1875F;
			this.Field115.Left = 3.625F;
			this.Field115.Name = "Field115";
			this.Field115.Style = "font-size: 9pt; text-align: right";
			this.Field115.Text = "48.00";
			this.Field115.Top = 6F;
			this.Field115.Width = 0.875F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1875F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 2.375F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label111.Text = "Full";
			this.Label111.Top = 5.8125F;
			this.Label111.Width = 0.875F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 2.375F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label112.Text = "Time";
			this.Label112.Top = 6F;
			this.Label112.Width = 0.875F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1875F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 0.1875F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label113.Text = "LTT - Large  8 Years";
			this.Label113.Top = 6.375F;
			this.Label113.Width = 2.0625F;
			// 
			// fldLTTLargeUnits8Time
			// 
			this.fldLTTLargeUnits8Time.Height = 0.1875F;
			this.fldLTTLargeUnits8Time.Left = 4.8125F;
			this.fldLTTLargeUnits8Time.Name = "fldLTTLargeUnits8Time";
			this.fldLTTLargeUnits8Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits8Time.Text = "Field1";
			this.fldLTTLargeUnits8Time.Top = 6.375F;
			this.fldLTTLargeUnits8Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount8Time
			// 
			this.fldLTTLargeAmount8Time.Height = 0.1875F;
			this.fldLTTLargeAmount8Time.Left = 5.9375F;
			this.fldLTTLargeAmount8Time.Name = "fldLTTLargeAmount8Time";
			this.fldLTTLargeAmount8Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount8Time.Text = "Field13";
			this.fldLTTLargeAmount8Time.Top = 6.375F;
			this.fldLTTLargeAmount8Time.Width = 1.125F;
			// 
			// Field118
			// 
			this.Field118.Height = 0.1875F;
			this.Field118.Left = 3.625F;
			this.Field118.Name = "Field118";
			this.Field118.Style = "font-size: 9pt; text-align: right";
			this.Field118.Text = "48.00";
			this.Field118.Top = 6.375F;
			this.Field118.Width = 0.875F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1875F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 2.375F;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label114.Text = "Full";
			this.Label114.Top = 6.1875F;
			this.Label114.Width = 0.875F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 2.375F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label115.Text = "Time";
			this.Label115.Top = 6.375F;
			this.Label115.Width = 0.875F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1875F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 0.1875F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label116.Text = "LTT - Large  25 Years";
			this.Label116.Top = 0F;
			this.Label116.Width = 2.0625F;
			// 
			// fldLTTLargeUnits25Full
			// 
			this.fldLTTLargeUnits25Full.Height = 0.1875F;
			this.fldLTTLargeUnits25Full.Left = 4.8125F;
			this.fldLTTLargeUnits25Full.Name = "fldLTTLargeUnits25Full";
			this.fldLTTLargeUnits25Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits25Full.Text = "Field1";
			this.fldLTTLargeUnits25Full.Top = 0F;
			this.fldLTTLargeUnits25Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount25Full
			// 
			this.fldLTTLargeAmount25Full.Height = 0.1875F;
			this.fldLTTLargeAmount25Full.Left = 5.9375F;
			this.fldLTTLargeAmount25Full.Name = "fldLTTLargeAmount25Full";
			this.fldLTTLargeAmount25Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount25Full.Text = "Field13";
			this.fldLTTLargeAmount25Full.Top = 0F;
			this.fldLTTLargeAmount25Full.Width = 1.125F;
			// 
			// Field121
			// 
			this.Field121.Height = 0.1875F;
			this.Field121.Left = 3.625F;
			this.Field121.Name = "Field121";
			this.Field121.Style = "font-size: 9pt; text-align: right";
			this.Field121.Text = "80.00";
			this.Field121.Top = 0F;
			this.Field121.Width = 0.875F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 2.375F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label117.Text = "Full";
			this.Label117.Top = 0F;
			this.Label117.Width = 0.875F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1875F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 0.1875F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label120.Text = "LTT - Large  24 Years";
			this.Label120.Top = 0.1875F;
			this.Label120.Width = 2.0625F;
			// 
			// fldLTTLargeUnits24Full
			// 
			this.fldLTTLargeUnits24Full.Height = 0.1875F;
			this.fldLTTLargeUnits24Full.Left = 4.8125F;
			this.fldLTTLargeUnits24Full.Name = "fldLTTLargeUnits24Full";
			this.fldLTTLargeUnits24Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits24Full.Text = "Field1";
			this.fldLTTLargeUnits24Full.Top = 0.1875F;
			this.fldLTTLargeUnits24Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount24Full
			// 
			this.fldLTTLargeAmount24Full.Height = 0.1875F;
			this.fldLTTLargeAmount24Full.Left = 5.9375F;
			this.fldLTTLargeAmount24Full.Name = "fldLTTLargeAmount24Full";
			this.fldLTTLargeAmount24Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount24Full.Text = "Field13";
			this.fldLTTLargeAmount24Full.Top = 0.1875F;
			this.fldLTTLargeAmount24Full.Width = 1.125F;
			// 
			// Field127
			// 
			this.Field127.Height = 0.1875F;
			this.Field127.Left = 3.625F;
			this.Field127.Name = "Field127";
			this.Field127.Style = "font-size: 9pt; text-align: right";
			this.Field127.Text = "288.00";
			this.Field127.Top = 0.1875F;
			this.Field127.Width = 0.875F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1875F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 2.375F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label121.Text = "Full";
			this.Label121.Top = 0.1875F;
			this.Label121.Width = 0.875F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1875F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 0.1875F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label122.Text = "LTT - Large  24 Years";
			this.Label122.Top = 0.375F;
			this.Label122.Width = 2.0625F;
			// 
			// fldLTTLargeUnits24Time
			// 
			this.fldLTTLargeUnits24Time.Height = 0.1875F;
			this.fldLTTLargeUnits24Time.Left = 4.8125F;
			this.fldLTTLargeUnits24Time.Name = "fldLTTLargeUnits24Time";
			this.fldLTTLargeUnits24Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits24Time.Text = "Field1";
			this.fldLTTLargeUnits24Time.Top = 0.375F;
			this.fldLTTLargeUnits24Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount24Time
			// 
			this.fldLTTLargeAmount24Time.Height = 0.1875F;
			this.fldLTTLargeAmount24Time.Left = 5.9375F;
			this.fldLTTLargeAmount24Time.Name = "fldLTTLargeAmount24Time";
			this.fldLTTLargeAmount24Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount24Time.Text = "Field13";
			this.fldLTTLargeAmount24Time.Top = 0.375F;
			this.fldLTTLargeAmount24Time.Width = 1.125F;
			// 
			// Field130
			// 
			this.Field130.Height = 0.1875F;
			this.Field130.Left = 3.625F;
			this.Field130.Name = "Field130";
			this.Field130.Style = "font-size: 9pt; text-align: right";
			this.Field130.Text = "36.00";
			this.Field130.Top = 0.375F;
			this.Field130.Width = 0.875F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1875F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 2.375F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label123.Text = "Time";
			this.Label123.Top = 0.375F;
			this.Label123.Width = 0.875F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1875F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0.1875F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label124.Text = "LTT - Large  23 Years";
			this.Label124.Top = 0.5625F;
			this.Label124.Width = 2.0625F;
			// 
			// fldLTTLargeUnits23Full
			// 
			this.fldLTTLargeUnits23Full.Height = 0.1875F;
			this.fldLTTLargeUnits23Full.Left = 4.8125F;
			this.fldLTTLargeUnits23Full.Name = "fldLTTLargeUnits23Full";
			this.fldLTTLargeUnits23Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits23Full.Text = "Field1";
			this.fldLTTLargeUnits23Full.Top = 0.5625F;
			this.fldLTTLargeUnits23Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount23Full
			// 
			this.fldLTTLargeAmount23Full.Height = 0.1875F;
			this.fldLTTLargeAmount23Full.Left = 5.9375F;
			this.fldLTTLargeAmount23Full.Name = "fldLTTLargeAmount23Full";
			this.fldLTTLargeAmount23Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount23Full.Text = "Field13";
			this.fldLTTLargeAmount23Full.Top = 0.5625F;
			this.fldLTTLargeAmount23Full.Width = 1.125F;
			// 
			// Field133
			// 
			this.Field133.Height = 0.1875F;
			this.Field133.Left = 3.625F;
			this.Field133.Name = "Field133";
			this.Field133.Style = "font-size: 9pt; text-align: right";
			this.Field133.Text = "276.00";
			this.Field133.Top = 0.5625F;
			this.Field133.Width = 0.875F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1875F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 2.375F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label125.Text = "Full";
			this.Label125.Top = 0.5625F;
			this.Label125.Width = 0.875F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1875F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 0.1875F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label126.Text = "LTT - Large  23 Years";
			this.Label126.Top = 0.75F;
			this.Label126.Width = 2.0625F;
			// 
			// fldLTTLargeUnits23Time
			// 
			this.fldLTTLargeUnits23Time.Height = 0.1875F;
			this.fldLTTLargeUnits23Time.Left = 4.8125F;
			this.fldLTTLargeUnits23Time.Name = "fldLTTLargeUnits23Time";
			this.fldLTTLargeUnits23Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits23Time.Text = "Field1";
			this.fldLTTLargeUnits23Time.Top = 0.75F;
			this.fldLTTLargeUnits23Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount23Time
			// 
			this.fldLTTLargeAmount23Time.Height = 0.1875F;
			this.fldLTTLargeAmount23Time.Left = 5.9375F;
			this.fldLTTLargeAmount23Time.Name = "fldLTTLargeAmount23Time";
			this.fldLTTLargeAmount23Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount23Time.Text = "Field13";
			this.fldLTTLargeAmount23Time.Top = 0.75F;
			this.fldLTTLargeAmount23Time.Width = 1.125F;
			// 
			// Field136
			// 
			this.Field136.Height = 0.1875F;
			this.Field136.Left = 3.625F;
			this.Field136.Name = "Field136";
			this.Field136.Style = "font-size: 9pt; text-align: right";
			this.Field136.Text = "36.00";
			this.Field136.Top = 0.75F;
			this.Field136.Width = 0.875F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1875F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 2.375F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label127.Text = "Time";
			this.Label127.Top = 0.75F;
			this.Label127.Width = 0.875F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1875F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 0.1875F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label128.Text = "LTT - Large  22 Years";
			this.Label128.Top = 0.9375F;
			this.Label128.Width = 2.0625F;
			// 
			// fldLTTLargeUnits22Full
			// 
			this.fldLTTLargeUnits22Full.Height = 0.1875F;
			this.fldLTTLargeUnits22Full.Left = 4.8125F;
			this.fldLTTLargeUnits22Full.Name = "fldLTTLargeUnits22Full";
			this.fldLTTLargeUnits22Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits22Full.Text = "Field1";
			this.fldLTTLargeUnits22Full.Top = 0.9375F;
			this.fldLTTLargeUnits22Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount22Full
			// 
			this.fldLTTLargeAmount22Full.Height = 0.1875F;
			this.fldLTTLargeAmount22Full.Left = 5.9375F;
			this.fldLTTLargeAmount22Full.Name = "fldLTTLargeAmount22Full";
			this.fldLTTLargeAmount22Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount22Full.Text = "Field13";
			this.fldLTTLargeAmount22Full.Top = 0.9375F;
			this.fldLTTLargeAmount22Full.Width = 1.125F;
			// 
			// Field139
			// 
			this.Field139.Height = 0.1875F;
			this.Field139.Left = 3.625F;
			this.Field139.Name = "Field139";
			this.Field139.Style = "font-size: 9pt; text-align: right";
			this.Field139.Text = "264.00";
			this.Field139.Top = 0.9375F;
			this.Field139.Width = 0.875F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1875F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 2.375F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label129.Text = "Full";
			this.Label129.Top = 0.9375F;
			this.Label129.Width = 0.875F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1875F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 0.1875F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label130.Text = "LTT - Large  22 Years";
			this.Label130.Top = 1.125F;
			this.Label130.Width = 2.0625F;
			// 
			// fldLTTLargeUnits22Time
			// 
			this.fldLTTLargeUnits22Time.Height = 0.1875F;
			this.fldLTTLargeUnits22Time.Left = 4.8125F;
			this.fldLTTLargeUnits22Time.Name = "fldLTTLargeUnits22Time";
			this.fldLTTLargeUnits22Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits22Time.Text = "Field1";
			this.fldLTTLargeUnits22Time.Top = 1.125F;
			this.fldLTTLargeUnits22Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount22Time
			// 
			this.fldLTTLargeAmount22Time.Height = 0.1875F;
			this.fldLTTLargeAmount22Time.Left = 5.9375F;
			this.fldLTTLargeAmount22Time.Name = "fldLTTLargeAmount22Time";
			this.fldLTTLargeAmount22Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount22Time.Text = "Field13";
			this.fldLTTLargeAmount22Time.Top = 1.125F;
			this.fldLTTLargeAmount22Time.Width = 1.125F;
			// 
			// Field142
			// 
			this.Field142.Height = 0.1875F;
			this.Field142.Left = 3.625F;
			this.Field142.Name = "Field142";
			this.Field142.Style = "font-size: 9pt; text-align: right";
			this.Field142.Text = "36.00";
			this.Field142.Top = 1.125F;
			this.Field142.Width = 0.875F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1875F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 2.375F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label131.Text = "Time";
			this.Label131.Top = 1.125F;
			this.Label131.Width = 0.875F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1875F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0.1875F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label132.Text = "LTT - Large  21 Years";
			this.Label132.Top = 1.3125F;
			this.Label132.Width = 2.0625F;
			// 
			// fldLTTLargeUnits21Full
			// 
			this.fldLTTLargeUnits21Full.Height = 0.1875F;
			this.fldLTTLargeUnits21Full.Left = 4.8125F;
			this.fldLTTLargeUnits21Full.Name = "fldLTTLargeUnits21Full";
			this.fldLTTLargeUnits21Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits21Full.Text = "Field1";
			this.fldLTTLargeUnits21Full.Top = 1.3125F;
			this.fldLTTLargeUnits21Full.Width = 0.8125F;
			// 
			// fldLTTLargeAmount21Full
			// 
			this.fldLTTLargeAmount21Full.Height = 0.1875F;
			this.fldLTTLargeAmount21Full.Left = 5.9375F;
			this.fldLTTLargeAmount21Full.Name = "fldLTTLargeAmount21Full";
			this.fldLTTLargeAmount21Full.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount21Full.Text = "Field13";
			this.fldLTTLargeAmount21Full.Top = 1.3125F;
			this.fldLTTLargeAmount21Full.Width = 1.125F;
			// 
			// Field145
			// 
			this.Field145.Height = 0.1875F;
			this.Field145.Left = 3.625F;
			this.Field145.Name = "Field145";
			this.Field145.Style = "font-size: 9pt; text-align: right";
			this.Field145.Text = "252.00";
			this.Field145.Top = 1.3125F;
			this.Field145.Width = 0.875F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1875F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 2.375F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label133.Text = "Full";
			this.Label133.Top = 1.3125F;
			this.Label133.Width = 0.875F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.1875F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 0.1875F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label134.Text = "LTT - Large  21 Years";
			this.Label134.Top = 1.5F;
			this.Label134.Width = 2.0625F;
			// 
			// fldLTTLargeUnits21Time
			// 
			this.fldLTTLargeUnits21Time.Height = 0.1875F;
			this.fldLTTLargeUnits21Time.Left = 4.8125F;
			this.fldLTTLargeUnits21Time.Name = "fldLTTLargeUnits21Time";
			this.fldLTTLargeUnits21Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeUnits21Time.Text = "Field1";
			this.fldLTTLargeUnits21Time.Top = 1.5F;
			this.fldLTTLargeUnits21Time.Width = 0.8125F;
			// 
			// fldLTTLargeAmount21Time
			// 
			this.fldLTTLargeAmount21Time.Height = 0.1875F;
			this.fldLTTLargeAmount21Time.Left = 5.9375F;
			this.fldLTTLargeAmount21Time.Name = "fldLTTLargeAmount21Time";
			this.fldLTTLargeAmount21Time.Style = "font-size: 9pt; text-align: right";
			this.fldLTTLargeAmount21Time.Text = "Field13";
			this.fldLTTLargeAmount21Time.Top = 1.5F;
			this.fldLTTLargeAmount21Time.Width = 1.125F;
			// 
			// Field148
			// 
			this.Field148.Height = 0.1875F;
			this.Field148.Left = 3.625F;
			this.Field148.Name = "Field148";
			this.Field148.Style = "font-size: 9pt; text-align: right";
			this.Field148.Text = "36.00";
			this.Field148.Top = 1.5F;
			this.Field148.Width = 0.875F;
			// 
			// Label135
			// 
			this.Label135.Height = 0.1875F;
			this.Label135.HyperLink = null;
			this.Label135.Left = 2.375F;
			this.Label135.Name = "Label135";
			this.Label135.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label135.Text = "Time";
			this.Label135.Top = 1.5F;
			this.Label135.Width = 0.875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.1875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label20.Text = "Transfers - Large";
			this.Label20.Top = 10.875F;
			this.Label20.Width = 2.0625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.1875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label21.Text = "Transfers - Small";
			this.Label21.Top = 11.0625F;
			this.Label21.Width = 2.0625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.1875F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label22.Text = "Lost Plates";
			this.Label22.Top = 11.3125F;
			this.Label22.Width = 2.0625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.1875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label23.Text = "Duplicates";
			this.Label23.Top = 11.5F;
			this.Label23.Width = 2.0625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.1875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label24.Text = "Corrections";
			this.Label24.Top = 11.6875F;
			this.Label24.Width = 2.0625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.1875F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label25.Text = "Titles";
			this.Label25.Top = 12.125F;
			this.Label25.Width = 2.0625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.1875F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label26.Text = "Sales Tax - Paid";
			this.Label26.Top = 12.5F;
			this.Label26.Width = 2.0625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.1875F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label27.Text = "Sales Tax - No Fee";
			this.Label27.Top = 12.6875F;
			this.Label27.Width = 2.0625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.1875F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 1";
			this.Label28.Text = "Total";
			this.Label28.Top = 13F;
			this.Label28.Width = 2.0625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.1875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 12.9375F;
			this.Line2.Width = 6.875F;
			this.Line2.X1 = 0.1875F;
			this.Line2.X2 = 7.0625F;
			this.Line2.Y1 = 12.9375F;
			this.Line2.Y2 = 12.9375F;
			// 
			// fldTransfersLargeUnits
			// 
			this.fldTransfersLargeUnits.Height = 0.1875F;
			this.fldTransfersLargeUnits.Left = 4.8125F;
			this.fldTransfersLargeUnits.Name = "fldTransfersLargeUnits";
			this.fldTransfersLargeUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTransfersLargeUnits.Text = "Field4";
			this.fldTransfersLargeUnits.Top = 10.875F;
			this.fldTransfersLargeUnits.Width = 0.8125F;
			// 
			// fldTransfersSmallUnits
			// 
			this.fldTransfersSmallUnits.Height = 0.1875F;
			this.fldTransfersSmallUnits.Left = 4.8125F;
			this.fldTransfersSmallUnits.Name = "fldTransfersSmallUnits";
			this.fldTransfersSmallUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTransfersSmallUnits.Text = "Field5";
			this.fldTransfersSmallUnits.Top = 11.0625F;
			this.fldTransfersSmallUnits.Width = 0.8125F;
			// 
			// fldLostPlateUnits
			// 
			this.fldLostPlateUnits.Height = 0.1875F;
			this.fldLostPlateUnits.Left = 4.8125F;
			this.fldLostPlateUnits.Name = "fldLostPlateUnits";
			this.fldLostPlateUnits.Style = "font-size: 9pt; text-align: right";
			this.fldLostPlateUnits.Text = "Field6";
			this.fldLostPlateUnits.Top = 11.3125F;
			this.fldLostPlateUnits.Width = 0.8125F;
			// 
			// fldDuplicateUnits
			// 
			this.fldDuplicateUnits.Height = 0.1875F;
			this.fldDuplicateUnits.Left = 4.8125F;
			this.fldDuplicateUnits.Name = "fldDuplicateUnits";
			this.fldDuplicateUnits.Style = "font-size: 9pt; text-align: right";
			this.fldDuplicateUnits.Text = "Field7";
			this.fldDuplicateUnits.Top = 11.5F;
			this.fldDuplicateUnits.Width = 0.8125F;
			// 
			// fldCorrectionUnits
			// 
			this.fldCorrectionUnits.Height = 0.1875F;
			this.fldCorrectionUnits.Left = 4.8125F;
			this.fldCorrectionUnits.Name = "fldCorrectionUnits";
			this.fldCorrectionUnits.Style = "font-size: 9pt; text-align: right";
			this.fldCorrectionUnits.Text = "Field8";
			this.fldCorrectionUnits.Top = 11.6875F;
			this.fldCorrectionUnits.Width = 0.8125F;
			// 
			// fldTitleUnits
			// 
			this.fldTitleUnits.Height = 0.1875F;
			this.fldTitleUnits.Left = 4.8125F;
			this.fldTitleUnits.Name = "fldTitleUnits";
			this.fldTitleUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTitleUnits.Text = "Field9";
			this.fldTitleUnits.Top = 12.125F;
			this.fldTitleUnits.Width = 0.8125F;
			// 
			// fldSalesTaxPaidUnits
			// 
			this.fldSalesTaxPaidUnits.Height = 0.1875F;
			this.fldSalesTaxPaidUnits.Left = 4.8125F;
			this.fldSalesTaxPaidUnits.Name = "fldSalesTaxPaidUnits";
			this.fldSalesTaxPaidUnits.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxPaidUnits.Text = "Field10";
			this.fldSalesTaxPaidUnits.Top = 12.5F;
			this.fldSalesTaxPaidUnits.Width = 0.8125F;
			// 
			// fldSalesTaxNoFeeUnits
			// 
			this.fldSalesTaxNoFeeUnits.Height = 0.1875F;
			this.fldSalesTaxNoFeeUnits.Left = 4.8125F;
			this.fldSalesTaxNoFeeUnits.Name = "fldSalesTaxNoFeeUnits";
			this.fldSalesTaxNoFeeUnits.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxNoFeeUnits.Text = "Field11";
			this.fldSalesTaxNoFeeUnits.Top = 12.6875F;
			this.fldSalesTaxNoFeeUnits.Width = 0.8125F;
			// 
			// fldTotalUnits
			// 
			this.fldTotalUnits.Height = 0.1875F;
			this.fldTotalUnits.Left = 4.8125F;
			this.fldTotalUnits.Name = "fldTotalUnits";
			this.fldTotalUnits.Style = "font-weight: bold; text-align: right";
			this.fldTotalUnits.Text = "Field12";
			this.fldTotalUnits.Top = 13F;
			this.fldTotalUnits.Width = 0.8125F;
			// 
			// fldTransferLargeAmount
			// 
			this.fldTransferLargeAmount.Height = 0.1875F;
			this.fldTransferLargeAmount.Left = 5.9375F;
			this.fldTransferLargeAmount.Name = "fldTransferLargeAmount";
			this.fldTransferLargeAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTransferLargeAmount.Text = "Field16";
			this.fldTransferLargeAmount.Top = 10.875F;
			this.fldTransferLargeAmount.Width = 1.125F;
			// 
			// fldTransferSmallAmount
			// 
			this.fldTransferSmallAmount.Height = 0.1875F;
			this.fldTransferSmallAmount.Left = 5.9375F;
			this.fldTransferSmallAmount.Name = "fldTransferSmallAmount";
			this.fldTransferSmallAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTransferSmallAmount.Text = "Field17";
			this.fldTransferSmallAmount.Top = 11.0625F;
			this.fldTransferSmallAmount.Width = 1.125F;
			// 
			// fldLostPlateAmount
			// 
			this.fldLostPlateAmount.Height = 0.1875F;
			this.fldLostPlateAmount.Left = 5.9375F;
			this.fldLostPlateAmount.Name = "fldLostPlateAmount";
			this.fldLostPlateAmount.Style = "font-size: 9pt; text-align: right";
			this.fldLostPlateAmount.Text = "Field18";
			this.fldLostPlateAmount.Top = 11.3125F;
			this.fldLostPlateAmount.Width = 1.125F;
			// 
			// fldDuplicateAmount
			// 
			this.fldDuplicateAmount.Height = 0.1875F;
			this.fldDuplicateAmount.Left = 5.9375F;
			this.fldDuplicateAmount.Name = "fldDuplicateAmount";
			this.fldDuplicateAmount.Style = "font-size: 9pt; text-align: right";
			this.fldDuplicateAmount.Text = "Field19";
			this.fldDuplicateAmount.Top = 11.5F;
			this.fldDuplicateAmount.Width = 1.125F;
			// 
			// fldCorrectionAmount
			// 
			this.fldCorrectionAmount.Height = 0.1875F;
			this.fldCorrectionAmount.Left = 5.9375F;
			this.fldCorrectionAmount.Name = "fldCorrectionAmount";
			this.fldCorrectionAmount.Style = "font-size: 9pt; text-align: right";
			this.fldCorrectionAmount.Text = "Field20";
			this.fldCorrectionAmount.Top = 11.6875F;
			this.fldCorrectionAmount.Width = 1.125F;
			// 
			// fldTitleAmount
			// 
			this.fldTitleAmount.Height = 0.1875F;
			this.fldTitleAmount.Left = 5.9375F;
			this.fldTitleAmount.Name = "fldTitleAmount";
			this.fldTitleAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTitleAmount.Text = "Field21";
			this.fldTitleAmount.Top = 12.125F;
			this.fldTitleAmount.Width = 1.125F;
			// 
			// fldSalesTaxPaidAmount
			// 
			this.fldSalesTaxPaidAmount.Height = 0.1875F;
			this.fldSalesTaxPaidAmount.Left = 5.9375F;
			this.fldSalesTaxPaidAmount.Name = "fldSalesTaxPaidAmount";
			this.fldSalesTaxPaidAmount.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxPaidAmount.Text = "Field22";
			this.fldSalesTaxPaidAmount.Top = 12.5F;
			this.fldSalesTaxPaidAmount.Width = 1.125F;
			// 
			// fldSalesTaxNoFeeAmount
			// 
			this.fldSalesTaxNoFeeAmount.Height = 0.1875F;
			this.fldSalesTaxNoFeeAmount.Left = 5.9375F;
			this.fldSalesTaxNoFeeAmount.Name = "fldSalesTaxNoFeeAmount";
			this.fldSalesTaxNoFeeAmount.Style = "font-size: 9pt; text-align: right";
			this.fldSalesTaxNoFeeAmount.Text = "Field23";
			this.fldSalesTaxNoFeeAmount.Top = 12.6875F;
			this.fldSalesTaxNoFeeAmount.Width = 1.125F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 5.9375F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-weight: bold; text-align: right";
			this.fldTotalAmount.Text = "Field24";
			this.fldTotalAmount.Top = 13F;
			this.fldTotalAmount.Width = 1.125F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 3.625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-size: 9pt; text-align: right";
			this.Field4.Text = "8.00";
			this.Field4.Top = 10.875F;
			this.Field4.Width = 0.875F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 3.625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-size: 9pt; text-align: right";
			this.Field5.Text = "5.00";
			this.Field5.Top = 11.0625F;
			this.Field5.Width = 0.875F;
			// 
			// fldLostPlateFee
			// 
			this.fldLostPlateFee.Height = 0.1875F;
			this.fldLostPlateFee.Left = 3.625F;
			this.fldLostPlateFee.Name = "fldLostPlateFee";
			this.fldLostPlateFee.Style = "font-size: 9pt; text-align: right";
			this.fldLostPlateFee.Text = null;
			this.fldLostPlateFee.Top = 11.3125F;
			this.fldLostPlateFee.Width = 0.875F;
			// 
			// fldDuplicateFee
			// 
			this.fldDuplicateFee.Height = 0.1875F;
			this.fldDuplicateFee.Left = 3.625F;
			this.fldDuplicateFee.Name = "fldDuplicateFee";
			this.fldDuplicateFee.Style = "font-size: 9pt; text-align: right";
			this.fldDuplicateFee.Text = null;
			this.fldDuplicateFee.Top = 11.5F;
			this.fldDuplicateFee.Width = 0.875F;
			// 
			// fldTitleFee
			// 
			this.fldTitleFee.Height = 0.1875F;
			this.fldTitleFee.Left = 3.625F;
			this.fldTitleFee.Name = "fldTitleFee";
			this.fldTitleFee.Style = "font-size: 9pt; text-align: right";
			this.fldTitleFee.Text = null;
			this.fldTitleFee.Top = 12.125F;
			this.fldTitleFee.Width = 0.875F;
			// 
			// Label136
			// 
			this.Label136.Height = 0.1875F;
			this.Label136.HyperLink = null;
			this.Label136.Left = 0.1875F;
			this.Label136.Name = "Label136";
			this.Label136.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label136.Text = "Transfers - 25 Year";
			this.Label136.Top = 10.6875F;
			this.Label136.Width = 2.0625F;
			// 
			// fldTransfers25YearUnits
			// 
			this.fldTransfers25YearUnits.Height = 0.1875F;
			this.fldTransfers25YearUnits.Left = 4.8125F;
			this.fldTransfers25YearUnits.Name = "fldTransfers25YearUnits";
			this.fldTransfers25YearUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTransfers25YearUnits.Text = "Field4";
			this.fldTransfers25YearUnits.Top = 10.6875F;
			this.fldTransfers25YearUnits.Width = 0.8125F;
			// 
			// fldTransfer25YearAmount
			// 
			this.fldTransfer25YearAmount.Height = 0.1875F;
			this.fldTransfer25YearAmount.Left = 5.9375F;
			this.fldTransfer25YearAmount.Name = "fldTransfer25YearAmount";
			this.fldTransfer25YearAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTransfer25YearAmount.Text = "Field16";
			this.fldTransfer25YearAmount.Top = 10.6875F;
			this.fldTransfer25YearAmount.Width = 1.125F;
			// 
			// Field151
			// 
			this.Field151.Height = 0.1875F;
			this.Field151.Left = 3.625F;
			this.Field151.Name = "Field151";
			this.Field151.Style = "font-size: 9pt; text-align: right";
			this.Field151.Text = "20.00";
			this.Field151.Top = 10.6875F;
			this.Field151.Width = 0.875F;
			// 
			// Label137
			// 
			this.Label137.Height = 0.1875F;
			this.Label137.HyperLink = null;
			this.Label137.Left = 0.1875F;
			this.Label137.Name = "Label137";
			this.Label137.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label137.Text = "Titles - 25 Year";
			this.Label137.Top = 11.9375F;
			this.Label137.Width = 2.0625F;
			// 
			// fldTitle25YearUnits
			// 
			this.fldTitle25YearUnits.Height = 0.1875F;
			this.fldTitle25YearUnits.Left = 4.8125F;
			this.fldTitle25YearUnits.Name = "fldTitle25YearUnits";
			this.fldTitle25YearUnits.Style = "font-size: 9pt; text-align: right";
			this.fldTitle25YearUnits.Text = "Field9";
			this.fldTitle25YearUnits.Top = 11.9375F;
			this.fldTitle25YearUnits.Width = 0.8125F;
			// 
			// fldTitle25YearsAmount
			// 
			this.fldTitle25YearsAmount.Height = 0.1875F;
			this.fldTitle25YearsAmount.Left = 5.9375F;
			this.fldTitle25YearsAmount.Name = "fldTitle25YearsAmount";
			this.fldTitle25YearsAmount.Style = "font-size: 9pt; text-align: right";
			this.fldTitle25YearsAmount.Text = "Field21";
			this.fldTitle25YearsAmount.Top = 11.9375F;
			this.fldTitle25YearsAmount.Width = 1.125F;
			// 
			// Field154
			// 
			this.Field154.Height = 0.1875F;
			this.Field154.Left = 3.625F;
			this.Field154.Name = "Field154";
			this.Field154.Style = "font-size: 9pt; text-align: right";
			this.Field154.Text = "18.00";
			this.Field154.Top = 11.9375F;
			this.Field154.Width = 0.875F;
			// 
			// Label138
			// 
			this.Label138.Height = 0.1875F;
			this.Label138.HyperLink = null;
			this.Label138.Left = 0.1875F;
			this.Label138.Name = "Label138";
			this.Label138.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label138.Text = "Rush Titles";
			this.Label138.Top = 12.3125F;
			this.Label138.Width = 2.0625F;
			// 
			// fldRushTitleUnits
			// 
			this.fldRushTitleUnits.Height = 0.1875F;
			this.fldRushTitleUnits.Left = 4.8125F;
			this.fldRushTitleUnits.Name = "fldRushTitleUnits";
			this.fldRushTitleUnits.Style = "font-size: 9pt; text-align: right";
			this.fldRushTitleUnits.Text = "Field9";
			this.fldRushTitleUnits.Top = 12.3125F;
			this.fldRushTitleUnits.Width = 0.8125F;
			// 
			// fldRushTitleAmount
			// 
			this.fldRushTitleAmount.Height = 0.1875F;
			this.fldRushTitleAmount.Left = 5.9375F;
			this.fldRushTitleAmount.Name = "fldRushTitleAmount";
			this.fldRushTitleAmount.Style = "font-size: 9pt; text-align: right";
			this.fldRushTitleAmount.Text = "Field21";
			this.fldRushTitleAmount.Top = 12.3125F;
			this.fldRushTitleAmount.Width = 1.125F;
			// 
			// fldRushTitleFee
			// 
			this.fldRushTitleFee.Height = 0.1875F;
			this.fldRushTitleFee.Left = 3.625F;
			this.fldRushTitleFee.Name = "fldRushTitleFee";
			this.fldRushTitleFee.Style = "font-size: 9pt; text-align: right";
			this.fldRushTitleFee.Text = null;
			this.fldRushTitleFee.Top = 12.3125F;
			this.fldRushTitleFee.Width = 0.875F;
			// 
			// rptLTTAgentDetail
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.4375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits12Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionsUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount12Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtensionAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits11Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount11Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits10Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount10Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits9Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount9Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits8Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount8Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnitsSubTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmountSubTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnitsSubTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmountSubTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTUnitsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTSmallAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits20Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount20Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits20Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount20Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits19Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount19Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits19Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount19Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits18Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount18Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits18Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount18Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits17Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount17Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits17Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount17Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits16Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount16Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits16Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount16Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits15Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount15Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits15Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount15Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits14Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount14Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits14Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount14Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits13Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount13Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits13Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount13Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits12Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount12Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits11Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount11Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits10Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount10Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits9Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount9Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits8Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount8Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits25Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount25Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits24Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount24Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits24Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount24Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits23Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount23Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits23Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount23Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits22Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount22Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field139)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits22Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount22Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field142)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits21Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount21Full)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field145)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeUnits21Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLTTLargeAmount21Time)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field148)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label135)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersLargeUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfersSmallUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferLargeAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransferSmallAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxPaidAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSalesTaxNoFeeAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLostPlateFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitleFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label136)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfers25YearUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransfer25YearAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field151)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label137)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle25YearsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field154)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label138)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRushTitleFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits12Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExtensionsUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount12Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExtensionAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits11Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount11Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits10Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount10Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits9Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount9Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits8Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount8Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnitsSubTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmountSubTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnitsSubTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmountSubTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTUnitsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTSmallAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits20Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount20Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits20Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount20Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field61;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits19Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount19Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field64;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits19Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount19Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits18Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount18Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits18Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount18Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits17Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount17Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field76;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits17Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount17Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits16Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount16Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field82;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits16Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount16Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field85;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits15Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount15Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field88;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits15Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount15Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field91;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits14Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount14Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits14Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount14Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field97;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits13Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount13Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field100;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits13Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount13Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field103;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits12Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount12Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits11Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount11Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field109;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits10Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount10Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits9Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount9Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits8Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount8Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field118;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits25Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount25Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field121;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits24Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount24Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits24Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount24Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field130;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits23Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount23Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits23Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount23Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field136;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits22Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount22Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field139;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits22Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount22Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field142;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits21Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount21Full;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field145;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeUnits21Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLTTLargeAmount21Time;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field148;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label135;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfersLargeUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfersSmallUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLostPlateUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDuplicateUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitleUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxPaidUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxNoFeeUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferLargeAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransferSmallAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLostPlateAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDuplicateAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitleAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxPaidAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSalesTaxNoFeeAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLostPlateFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDuplicateFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitleFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label136;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfers25YearUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransfer25YearAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field151;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label137;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle25YearUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle25YearsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field154;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label138;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRushTitleUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRushTitleAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRushTitleFee;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
