﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubMonetary.
	/// </summary>
	partial class rptSubMonetary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSubMonetary));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldReciept = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldExplanation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldOpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAdjusted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalMoney = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fldReciept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExplanation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOpID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjusted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldReciept,
            this.fldClassPlate,
            this.fldAmount,
            this.fldExplanation,
            this.fldOpID,
            this.fldAdjusted,
            this.fldDate});
            this.Detail.Height = 0.2083333F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldReciept
            // 
            this.fldReciept.Height = 0.19F;
            this.fldReciept.Left = 0.875F;
            this.fldReciept.Name = "fldReciept";
            this.fldReciept.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldReciept.Text = "Field1";
            this.fldReciept.Top = 0.03125F;
            this.fldReciept.Width = 0.75F;
            // 
            // fldClassPlate
            // 
            this.fldClassPlate.Height = 0.19F;
            this.fldClassPlate.Left = 1.6875F;
            this.fldClassPlate.Name = "fldClassPlate";
            this.fldClassPlate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldClassPlate.Text = "PC 0000000";
            this.fldClassPlate.Top = 0.03125F;
            this.fldClassPlate.Width = 0.875F;
            // 
            // fldAmount
            // 
            this.fldAmount.Height = 0.19F;
            this.fldAmount.Left = 2.625F;
            this.fldAmount.Name = "fldAmount";
            this.fldAmount.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
            this.fldAmount.Text = "Field1";
            this.fldAmount.Top = 0.03125F;
            this.fldAmount.Width = 0.8125F;
            // 
            // fldExplanation
            // 
            this.fldExplanation.Height = 0.19F;
            this.fldExplanation.Left = 3.5F;
            this.fldExplanation.Name = "fldExplanation";
            this.fldExplanation.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldExplanation.Text = "Field1";
            this.fldExplanation.Top = 0.03125F;
            this.fldExplanation.Width = 2F;
            // 
            // fldOpID
            // 
            this.fldOpID.Height = 0.19F;
            this.fldOpID.Left = 7F;
            this.fldOpID.Name = "fldOpID";
            this.fldOpID.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldOpID.Text = "Field1";
            this.fldOpID.Top = 0.03125F;
            this.fldOpID.Width = 0.5F;
            // 
            // fldAdjusted
            // 
            this.fldAdjusted.Height = 0.19F;
            this.fldAdjusted.Left = 5.5625F;
            this.fldAdjusted.Name = "fldAdjusted";
            this.fldAdjusted.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldAdjusted.Text = "Field1";
            this.fldAdjusted.Top = 0.03125F;
            this.fldAdjusted.Width = 1.375F;
            // 
            // fldDate
            // 
            this.fldDate.Height = 0.19F;
            this.fldDate.Left = 0F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldDate.Text = "12/20/2020";
            this.fldDate.Top = 0.03125F;
            this.fldDate.Width = 0.75F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13});
            this.GroupHeader1.Height = 0.7083333F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // Label1
            // 
            this.Label1.Height = 0.19F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.875F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
            this.Label1.Text = "TAX RCPT";
            this.Label1.Top = 0.375F;
            this.Label1.Width = 0.75F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.19F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 1.6875F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.Label2.Text = "PLATE";
            this.Label2.Top = 0.53125F;
            this.Label2.Width = 0.8125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.19F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 2.625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
            this.Label3.Text = "AMOUNT";
            this.Label3.Top = 0.53125F;
            this.Label3.Width = 0.8125F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.19F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 3.5F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.Label5.Text = "EXPLANATION--------------";
            this.Label5.Top = 0.53125F;
            this.Label5.Width = 2F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.19F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 7F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
            this.Label6.Text = "OPID";
            this.Label6.Top = 0.53125F;
            this.Label6.Width = 0.5F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.19F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.875F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
            this.Label7.Text = "NUMBER";
            this.Label7.Top = 0.53125F;
            this.Label7.Width = 0.75F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 2.375F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
            this.Label10.Text = "---- MONETARY CORRECTIONS ----";
            this.Label10.Top = 0.15625F;
            this.Label10.Width = 2.3125F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.19F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 5.5625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
            this.Label11.Text = "CATEGORY ADJUSTED";
            this.Label11.Top = 0.53125F;
            this.Label11.Width = 1.1875F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.19F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 1.6875F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
            this.Label12.Text = "CLASS";
            this.Label12.Top = 0.375F;
            this.Label12.Width = 0.75F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.19F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 0F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
            this.Label13.Text = "DATE";
            this.Label13.Top = 0.53125F;
            this.Label13.Width = 0.75F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label9,
            this.fldTotalCount,
            this.fldTotalMoney});
            this.GroupFooter1.Height = 0.3125F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.03125F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.Label9.Text = "TOTAL MONETARY CORRECTIONS:";
            this.Label9.Top = 0.0625F;
            this.Label9.Width = 2.15625F;
            // 
            // fldTotalCount
            // 
            this.fldTotalCount.Height = 0.1875F;
            this.fldTotalCount.Left = 2.25F;
            this.fldTotalCount.Name = "fldTotalCount";
            this.fldTotalCount.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldTotalCount.Text = "Field1";
            this.fldTotalCount.Top = 0.0625F;
            this.fldTotalCount.Width = 0.75F;
            // 
            // fldTotalMoney
            // 
            this.fldTotalMoney.Height = 0.1875F;
            this.fldTotalMoney.Left = 3.34375F;
            this.fldTotalMoney.Name = "fldTotalMoney";
            this.fldTotalMoney.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
            this.fldTotalMoney.Text = "Field1";
            this.fldTotalMoney.Top = 0.0625F;
            this.fldTotalMoney.Width = 1.15625F;
            // 
            // rptSubMonetary
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.75F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.75F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.520833F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.fldReciept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExplanation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOpID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjusted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReciept;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExplanation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjusted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMoney;
	}
}
