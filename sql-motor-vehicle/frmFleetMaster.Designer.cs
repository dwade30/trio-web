//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmFleetMaster.
	/// </summary>
	partial class frmFleetMaster
	{
		public fecherFoundation.FCComboBox cmbFilterAnnual;
		public fecherFoundation.FCLabel lblFilterAnnual;
		public fecherFoundation.FCComboBox cmbOwner;
		public fecherFoundation.FCComboBox cmbAnnual;
		public fecherFoundation.FCLabel lblAnnual;
		public fecherFoundation.FCFrame fraVehiclesToAdd;
		public fecherFoundation.FCGrid vsVehicles;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCButton cmdCancelAdd;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCFrame fraFleetSearchResults;
		public fecherFoundation.FCButton cmdResultsCancel;
		public fecherFoundation.FCButton cmdResultsGetInfo;
		public fecherFoundation.FCGrid vsResults;
		public fecherFoundation.FCFrame fraSearchFleet;
		public fecherFoundation.FCButton cmdCancelFleetSearch;
		public fecherFoundation.FCButton cmdSearchFleetSearch;
		public fecherFoundation.FCTextBox txtSearchFleet;
		public fecherFoundation.FCFrame fraSearch;
		public fecherFoundation.FCTextBox txtCompanyName;
		public fecherFoundation.FCTextBox txtFirstName;
		public fecherFoundation.FCTextBox txtLastName;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel lblCompanyName;
		public fecherFoundation.FCLabel lblFirstName;
		public fecherFoundation.FCLabel lblLastName;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdAddVehicles;
		public fecherFoundation.FCButton cmdRemoveVehicles;
		public fecherFoundation.FCButton cmdUpdateEmail;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCComboBox cboStatus;
		public fecherFoundation.FCTextBox txtReg3PartyID;
		public fecherFoundation.FCButton cmdReg3Search;
		public fecherFoundation.FCButton cmdReg3Edit;
		public fecherFoundation.FCTextBox txtReg2PartyID;
		public fecherFoundation.FCButton cmdReg2Search;
		public fecherFoundation.FCButton cmdReg2Edit;
		public fecherFoundation.FCTextBox txtReg1PartyID;
		public fecherFoundation.FCButton cmdReg1Search;
		public fecherFoundation.FCButton cmdReg1Edit;
		public fecherFoundation.FCTextBox txtCompanyCode;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtOwner3Code;
		public fecherFoundation.FCTextBox txtIDNumber;
		public fecherFoundation.FCTextBox txtOwner2Code;
		public fecherFoundation.FCTextBox txtOwner1Code;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtEpiryMonth;
		public fecherFoundation.FCTextBox txtContact;
		public fecherFoundation.FCTextBox txtFleetNumber;
		public fecherFoundation.FCTextBox txtCountry;
		public Global.T2KDateBox txtDOB1;
		public Global.T2KPhoneNumberBox txtTelephone;
		public Global.T2KDateBox txtDOB2;
		public Global.T2KDateBox txtDOB3;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCPictureBox imgReg3Comments;
		public fecherFoundation.FCPictureBox imgReg2Comments;
		public fecherFoundation.FCLabel lblRegistrant3;
		public fecherFoundation.FCLabel lblRegistrant2;
		public fecherFoundation.FCLabel lblRegistrant1;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel lblCompanyCode;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCPictureBox imgReg1Comments;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblOwner1Info;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public FCGrid vsGroups;
		public FCGrid vsFleets;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddNewFleet;
		public fecherFoundation.FCToolStripMenuItem mnuAddNewGroup;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteFleet;
		public fecherFoundation.FCToolStripMenuItem mnuFileSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileComments;
		public fecherFoundation.FCToolStripMenuItem Separator1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveQuit;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFleetMaster));
			this.cmbFilterAnnual = new fecherFoundation.FCComboBox();
			this.lblFilterAnnual = new fecherFoundation.FCLabel();
			this.cmbOwner = new fecherFoundation.FCComboBox();
			this.cmbAnnual = new fecherFoundation.FCComboBox();
			this.lblAnnual = new fecherFoundation.FCLabel();
			this.fraVehiclesToAdd = new fecherFoundation.FCFrame();
			this.vsVehicles = new fecherFoundation.FCGrid();
			this.cmdAdd = new fecherFoundation.FCButton();
			this.cmdCancelAdd = new fecherFoundation.FCButton();
			this.cmdSelect = new fecherFoundation.FCButton();
			this.lblInstruct = new fecherFoundation.FCLabel();
			this.fraFleetSearchResults = new fecherFoundation.FCFrame();
			this.cmdResultsCancel = new fecherFoundation.FCButton();
			this.cmdResultsGetInfo = new fecherFoundation.FCButton();
			this.vsResults = new fecherFoundation.FCGrid();
			this.fraSearchFleet = new fecherFoundation.FCFrame();
			this.cmdCancelFleetSearch = new fecherFoundation.FCButton();
			this.cmdSearchFleetSearch = new fecherFoundation.FCButton();
			this.txtSearchFleet = new fecherFoundation.FCTextBox();
			this.fraSearch = new fecherFoundation.FCFrame();
			this.txtLastName = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.lblCompanyName = new fecherFoundation.FCLabel();
			this.lblFirstName = new fecherFoundation.FCLabel();
			this.lblLastName = new fecherFoundation.FCLabel();
			this.txtFirstName = new fecherFoundation.FCTextBox();
			this.txtCompanyName = new fecherFoundation.FCTextBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmdAddVehicles = new fecherFoundation.FCButton();
			this.cmdRemoveVehicles = new fecherFoundation.FCButton();
			this.cmdUpdateEmail = new fecherFoundation.FCButton();
			this.txtEmail = new fecherFoundation.FCTextBox();
			this.cboStatus = new fecherFoundation.FCComboBox();
			this.txtReg3PartyID = new fecherFoundation.FCTextBox();
			this.cmdReg3Search = new fecherFoundation.FCButton();
			this.cmdReg3Edit = new fecherFoundation.FCButton();
			this.txtReg2PartyID = new fecherFoundation.FCTextBox();
			this.cmdReg2Search = new fecherFoundation.FCButton();
			this.cmdReg2Edit = new fecherFoundation.FCButton();
			this.txtReg1PartyID = new fecherFoundation.FCTextBox();
			this.cmdReg1Search = new fecherFoundation.FCButton();
			this.cmdReg1Edit = new fecherFoundation.FCButton();
			this.txtCompanyCode = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.txtOwner3Code = new fecherFoundation.FCTextBox();
			this.txtIDNumber = new fecherFoundation.FCTextBox();
			this.txtOwner2Code = new fecherFoundation.FCTextBox();
			this.txtOwner1Code = new fecherFoundation.FCTextBox();
			this.txtAddress = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtEpiryMonth = new fecherFoundation.FCTextBox();
			this.txtContact = new fecherFoundation.FCTextBox();
			this.txtCountry = new fecherFoundation.FCTextBox();
			this.txtDOB1 = new Global.T2KDateBox();
			this.txtTelephone = new Global.T2KPhoneNumberBox();
			this.txtDOB2 = new Global.T2KDateBox();
			this.txtDOB3 = new Global.T2KDateBox();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Label17 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.imgReg3Comments = new fecherFoundation.FCPictureBox();
			this.imgReg2Comments = new fecherFoundation.FCPictureBox();
			this.lblRegistrant3 = new fecherFoundation.FCLabel();
			this.lblRegistrant2 = new fecherFoundation.FCLabel();
			this.lblRegistrant1 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.lblCompanyCode = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.imgReg1Comments = new fecherFoundation.FCPictureBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.lblOwner1Info = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.txtFleetNumber = new fecherFoundation.FCTextBox();
			this.vsGroups = new fecherFoundation.FCGrid();
			this.vsFleets = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddNewFleet = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddNewGroup = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteFleet = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeparator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileComments = new fecherFoundation.FCToolStripMenuItem();
			this.Separator1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveQuit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdAddNewFleet = new fecherFoundation.FCButton();
			this.cmdAddNewGroup = new fecherFoundation.FCButton();
			this.cmdFileSearch = new fecherFoundation.FCButton();
			this.cmdComments = new fecherFoundation.FCButton();
			this.cmdDeleteFleet = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVehiclesToAdd)).BeginInit();
			this.fraVehiclesToAdd.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFleetSearchResults)).BeginInit();
			this.fraFleetSearchResults.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdResultsCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdResultsGetInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResults)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchFleet)).BeginInit();
			this.fraSearchFleet.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelFleetSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchFleetSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
			this.fraSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddVehicles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRemoveVehicles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdateEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg3Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg3Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Search)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Edit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReg3Comments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReg2Comments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReg1Comments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsGroups)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFleets)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNewFleet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNewGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteFleet)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 892);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.vsGroups);
			this.ClientArea.Controls.Add(this.vsFleets);
			this.ClientArea.Controls.Add(this.fraFleetSearchResults);
			this.ClientArea.Controls.Add(this.cmbFilterAnnual);
			this.ClientArea.Controls.Add(this.lblFilterAnnual);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.fraSearchFleet);
			this.ClientArea.Controls.Add(this.fraSearch);
			this.ClientArea.Controls.Add(this.fraVehiclesToAdd);
			this.ClientArea.Size = new System.Drawing.Size(1014, 628);
			this.ClientArea.Controls.SetChildIndex(this.fraVehiclesToAdd, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraSearch, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraSearchFleet, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label8, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblFilterAnnual, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbFilterAnnual, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraFleetSearchResults, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsFleets, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsGroups, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDeleteFleet);
			this.TopPanel.Controls.Add(this.cmdComments);
			this.TopPanel.Controls.Add(this.cmdFileSearch);
			this.TopPanel.Controls.Add(this.cmdAddNewGroup);
			this.TopPanel.Controls.Add(this.cmdAddNewFleet);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddNewFleet, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddNewGroup, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdComments, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteFleet, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(283, 28);
			this.HeaderText.Text = "Fleet Master Add / Update";
			// 
			// cmbFilterAnnual
			// 
			this.cmbFilterAnnual.Items.AddRange(new object[] {
            "Annual",
            "Both",
            "Long Term"});
			this.cmbFilterAnnual.Location = new System.Drawing.Point(435, 30);
			this.cmbFilterAnnual.Name = "cmbFilterAnnual";
			this.cmbFilterAnnual.Size = new System.Drawing.Size(217, 40);
			this.cmbFilterAnnual.TabIndex = 30;
			this.cmbFilterAnnual.Text = "Both";
			this.cmbFilterAnnual.SelectedIndexChanged += new System.EventHandler(this.cmbFilterAnnual_SelectedIndexChanged);
			// 
			// lblFilterAnnual
			// 
			this.lblFilterAnnual.AutoSize = true;
			this.lblFilterAnnual.Location = new System.Drawing.Point(326, 44);
			this.lblFilterAnnual.Name = "lblFilterAnnual";
			this.lblFilterAnnual.Size = new System.Drawing.Size(95, 15);
			this.lblFilterAnnual.TabIndex = 31;
			this.lblFilterAnnual.Text = "FILTER TYPES";
			// 
			// cmbOwner
			// 
			this.cmbOwner.Items.AddRange(new object[] {
            "Company",
            "Individual"});
			this.cmbOwner.Location = new System.Drawing.Point(20, 30);
			this.cmbOwner.Name = "cmbOwner";
			this.cmbOwner.Size = new System.Drawing.Size(208, 40);
			this.cmbOwner.TabIndex = 50;
			this.cmbOwner.Text = "Company";
			this.cmbOwner.SelectedIndexChanged += new System.EventHandler(this.cmbOwner_SelectedIndexChanged);
			// 
			// cmbAnnual
			// 
			this.cmbAnnual.Items.AddRange(new object[] {
            "Annual",
            "Long Term"});
			this.cmbAnnual.Location = new System.Drawing.Point(604, 30);
			this.cmbAnnual.Name = "cmbAnnual";
			this.cmbAnnual.Size = new System.Drawing.Size(203, 40);
			this.cmbAnnual.TabIndex = 91;
			this.cmbAnnual.Text = "Annual";
			this.cmbAnnual.Visible = false;
			this.cmbAnnual.SelectedIndexChanged += new System.EventHandler(this.cmbAnnual_SelectedIndexChanged);
			// 
			// lblAnnual
			// 
			this.lblAnnual.AutoSize = true;
			this.lblAnnual.Location = new System.Drawing.Point(543, 44);
			this.lblAnnual.Name = "lblAnnual";
			this.lblAnnual.Size = new System.Drawing.Size(37, 15);
			this.lblAnnual.TabIndex = 92;
			this.lblAnnual.Text = "TYPE";
			this.lblAnnual.Visible = false;
			// 
			// fraVehiclesToAdd
			// 
			this.fraVehiclesToAdd.BackColor = System.Drawing.Color.White;
			this.fraVehiclesToAdd.Controls.Add(this.vsVehicles);
			this.fraVehiclesToAdd.Controls.Add(this.cmdAdd);
			this.fraVehiclesToAdd.Controls.Add(this.cmdCancelAdd);
			this.fraVehiclesToAdd.Controls.Add(this.cmdSelect);
			this.fraVehiclesToAdd.Controls.Add(this.lblInstruct);
			this.fraVehiclesToAdd.Location = new System.Drawing.Point(30, 30);
			this.fraVehiclesToAdd.Name = "fraVehiclesToAdd";
			this.fraVehiclesToAdd.Size = new System.Drawing.Size(1013, 610);
			this.fraVehiclesToAdd.TabIndex = 29;
			this.fraVehiclesToAdd.Text = "Vehicles To Add";
			this.fraVehiclesToAdd.Visible = false;
			// 
			// vsVehicles
			// 
			this.vsVehicles.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsVehicles.Cols = 10;
			this.vsVehicles.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsVehicles.ExtendLastCol = true;
			this.vsVehicles.FixedCols = 0;
			this.vsVehicles.Location = new System.Drawing.Point(20, 59);
			this.vsVehicles.Name = "vsVehicles";
			this.vsVehicles.RowHeadersVisible = false;
			this.vsVehicles.Rows = 17;
			this.vsVehicles.ShowFocusCell = false;
			this.vsVehicles.Size = new System.Drawing.Size(973, 471);
			this.vsVehicles.TabIndex = 33;
			this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
			this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
			// 
			// cmdAdd
			// 
			this.cmdAdd.AppearanceKey = "actionButton";
			this.cmdAdd.Location = new System.Drawing.Point(164, 550);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(140, 40);
			this.cmdAdd.TabIndex = 32;
			this.cmdAdd.Text = "Add to Fleet";
			this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
			// 
			// cmdCancelAdd
			// 
			this.cmdCancelAdd.AppearanceKey = "actionButton";
			this.cmdCancelAdd.Location = new System.Drawing.Point(334, 550);
			this.cmdCancelAdd.Name = "cmdCancelAdd";
			this.cmdCancelAdd.Size = new System.Drawing.Size(96, 40);
			this.cmdCancelAdd.TabIndex = 31;
			this.cmdCancelAdd.Text = "Cancel";
			this.cmdCancelAdd.Click += new System.EventHandler(this.cmdCancelAdd_Click);
			// 
			// cmdSelect
			// 
			this.cmdSelect.AppearanceKey = "actionButton";
			this.cmdSelect.Location = new System.Drawing.Point(20, 550);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(114, 40);
			this.cmdSelect.TabIndex = 30;
			this.cmdSelect.Text = "Select All";
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// lblInstruct
			// 
			this.lblInstruct.Location = new System.Drawing.Point(20, 30);
			this.lblInstruct.Name = "lblInstruct";
			this.lblInstruct.Size = new System.Drawing.Size(640, 18);
			this.lblInstruct.TabIndex = 34;
			this.lblInstruct.Text = "PLEASE CHECK ALL THE VEHICLES YOU WISH TO ADD TO THE FLEET AND CLICK THE \"ADD TO " +
    "FLEET\" BUTTON";
			// 
			// fraFleetSearchResults
			// 
			this.fraFleetSearchResults.BackColor = System.Drawing.Color.White;
			this.fraFleetSearchResults.Controls.Add(this.cmdResultsCancel);
			this.fraFleetSearchResults.Controls.Add(this.cmdResultsGetInfo);
			this.fraFleetSearchResults.Controls.Add(this.vsResults);
			this.fraFleetSearchResults.Location = new System.Drawing.Point(30, 30);
			this.fraFleetSearchResults.Name = "fraFleetSearchResults";
			this.fraFleetSearchResults.Size = new System.Drawing.Size(600, 450);
			this.fraFleetSearchResults.TabIndex = 65;
			this.fraFleetSearchResults.Text = "Results";
			this.fraFleetSearchResults.Visible = false;
			// 
			// cmdResultsCancel
			// 
			this.cmdResultsCancel.AppearanceKey = "actionButton";
			this.cmdResultsCancel.Location = new System.Drawing.Point(153, 390);
			this.cmdResultsCancel.Name = "cmdResultsCancel";
			this.cmdResultsCancel.Size = new System.Drawing.Size(94, 40);
			this.cmdResultsCancel.TabIndex = 68;
			this.cmdResultsCancel.Text = "Cancel";
			this.cmdResultsCancel.Click += new System.EventHandler(this.cmdResultsCancel_Click);
			// 
			// cmdResultsGetInfo
			// 
			this.cmdResultsGetInfo.AppearanceKey = "actionButton";
			this.cmdResultsGetInfo.Location = new System.Drawing.Point(20, 390);
			this.cmdResultsGetInfo.Name = "cmdResultsGetInfo";
			this.cmdResultsGetInfo.Size = new System.Drawing.Size(103, 40);
			this.cmdResultsGetInfo.TabIndex = 67;
			this.cmdResultsGetInfo.Text = "Get Info";
			this.cmdResultsGetInfo.Click += new System.EventHandler(this.cmdResultsGetInfo_Click);
			// 
			// vsResults
			// 
			this.vsResults.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsResults.Cols = 5;
			this.vsResults.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsResults.ExtendLastCol = true;
			this.vsResults.Location = new System.Drawing.Point(20, 30);
			this.vsResults.Name = "vsResults";
			this.vsResults.Rows = 17;
			this.vsResults.ShowFocusCell = false;
			this.vsResults.Size = new System.Drawing.Size(560, 340);
			this.vsResults.TabIndex = 66;
			this.vsResults.DoubleClick += new System.EventHandler(this.vsResults_DblClick);
			// 
			// fraSearchFleet
			// 
			this.fraSearchFleet.BackColor = System.Drawing.Color.White;
			this.fraSearchFleet.Controls.Add(this.cmdCancelFleetSearch);
			this.fraSearchFleet.Controls.Add(this.cmdSearchFleetSearch);
			this.fraSearchFleet.Controls.Add(this.txtSearchFleet);
			this.fraSearchFleet.Location = new System.Drawing.Point(30, 30);
			this.fraSearchFleet.Name = "fraSearchFleet";
			this.fraSearchFleet.Size = new System.Drawing.Size(248, 150);
			this.fraSearchFleet.TabIndex = 61;
			this.fraSearchFleet.Text = "Search Criteria";
			this.fraSearchFleet.Visible = false;
			// 
			// cmdCancelFleetSearch
			// 
			this.cmdCancelFleetSearch.AppearanceKey = "actionButton";
			this.cmdCancelFleetSearch.Location = new System.Drawing.Point(134, 90);
			this.cmdCancelFleetSearch.Name = "cmdCancelFleetSearch";
			this.cmdCancelFleetSearch.Size = new System.Drawing.Size(94, 40);
			this.cmdCancelFleetSearch.TabIndex = 64;
			this.cmdCancelFleetSearch.Text = "Cancel";
			this.cmdCancelFleetSearch.Click += new System.EventHandler(this.cmdCancelFleetSearch_Click);
			// 
			// cmdSearchFleetSearch
			// 
			this.cmdSearchFleetSearch.AppearanceKey = "actionButton";
			this.cmdSearchFleetSearch.Location = new System.Drawing.Point(20, 90);
			this.cmdSearchFleetSearch.Name = "cmdSearchFleetSearch";
			this.cmdSearchFleetSearch.Size = new System.Drawing.Size(94, 40);
			this.cmdSearchFleetSearch.TabIndex = 63;
			this.cmdSearchFleetSearch.Text = "Search";
			this.cmdSearchFleetSearch.Click += new System.EventHandler(this.cmdSearchFleetSearch_Click);
			// 
			// txtSearchFleet
			// 
			this.txtSearchFleet.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearchFleet.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtSearchFleet.Location = new System.Drawing.Point(20, 30);
			this.txtSearchFleet.Name = "txtSearchFleet";
			this.txtSearchFleet.Size = new System.Drawing.Size(208, 40);
			this.txtSearchFleet.TabIndex = 62;
			// 
			// fraSearch
			// 
			this.fraSearch.BackColor = System.Drawing.Color.White;
			this.fraSearch.Controls.Add(this.cmbOwner);
			this.fraSearch.Controls.Add(this.txtLastName);
			this.fraSearch.Controls.Add(this.cmdSearch);
			this.fraSearch.Controls.Add(this.cmdCancel);
			this.fraSearch.Controls.Add(this.lblCompanyName);
			this.fraSearch.Controls.Add(this.lblFirstName);
			this.fraSearch.Controls.Add(this.lblLastName);
			this.fraSearch.Controls.Add(this.txtFirstName);
			this.fraSearch.Controls.Add(this.txtCompanyName);
			this.fraSearch.Location = new System.Drawing.Point(30, 30);
			this.fraSearch.Name = "fraSearch";
			this.fraSearch.Size = new System.Drawing.Size(417, 240);
			this.fraSearch.TabIndex = 35;
			this.fraSearch.Text = "Search Criteria";
			this.fraSearch.Visible = false;
			// 
			// txtLastName
			// 
			this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
			this.txtLastName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtLastName.Location = new System.Drawing.Point(153, 130);
			this.txtLastName.Name = "txtLastName";
			this.txtLastName.Size = new System.Drawing.Size(244, 40);
			this.txtLastName.TabIndex = 52;
			this.txtLastName.Visible = false;
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Location = new System.Drawing.Point(20, 180);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(94, 40);
			this.cmdSearch.TabIndex = 54;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(134, 180);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(94, 40);
			this.cmdCancel.TabIndex = 55;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// lblCompanyName
			// 
			this.lblCompanyName.Location = new System.Drawing.Point(20, 94);
			this.lblCompanyName.Name = "lblCompanyName";
			this.lblCompanyName.Size = new System.Drawing.Size(105, 16);
			this.lblCompanyName.TabIndex = 38;
			this.lblCompanyName.Text = "COMPANY NAME";
			// 
			// lblFirstName
			// 
			this.lblFirstName.Location = new System.Drawing.Point(20, 94);
			this.lblFirstName.Name = "lblFirstName";
			this.lblFirstName.Size = new System.Drawing.Size(80, 16);
			this.lblFirstName.TabIndex = 37;
			this.lblFirstName.Text = "FIRST NAME";
			this.lblFirstName.Visible = false;
			// 
			// lblLastName
			// 
			this.lblLastName.Location = new System.Drawing.Point(20, 144);
			this.lblLastName.Name = "lblLastName";
			this.lblLastName.Size = new System.Drawing.Size(80, 16);
			this.lblLastName.TabIndex = 36;
			this.lblLastName.Text = "LAST NAME";
			this.lblLastName.Visible = false;
			// 
			// txtFirstName
			// 
			this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
			this.txtFirstName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtFirstName.Location = new System.Drawing.Point(152, 80);
			this.txtFirstName.Name = "txtFirstName";
			this.txtFirstName.Size = new System.Drawing.Size(244, 40);
			this.txtFirstName.TabIndex = 51;
			this.txtFirstName.Visible = false;
			// 
			// txtCompanyName
			// 
			this.txtCompanyName.BackColor = System.Drawing.SystemColors.Window;
			this.txtCompanyName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtCompanyName.Location = new System.Drawing.Point(152, 80);
			this.txtCompanyName.Name = "txtCompanyName";
			this.txtCompanyName.Size = new System.Drawing.Size(244, 40);
			this.txtCompanyName.TabIndex = 53;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cmdAddVehicles);
			this.Frame1.Controls.Add(this.cmbAnnual);
			this.Frame1.Controls.Add(this.lblAnnual);
			this.Frame1.Controls.Add(this.cmdRemoveVehicles);
			this.Frame1.Controls.Add(this.cmdUpdateEmail);
			this.Frame1.Controls.Add(this.txtEmail);
			this.Frame1.Controls.Add(this.cboStatus);
			this.Frame1.Controls.Add(this.txtReg3PartyID);
			this.Frame1.Controls.Add(this.cmdReg3Search);
			this.Frame1.Controls.Add(this.cmdReg3Edit);
			this.Frame1.Controls.Add(this.txtReg2PartyID);
			this.Frame1.Controls.Add(this.cmdReg2Search);
			this.Frame1.Controls.Add(this.cmdReg2Edit);
			this.Frame1.Controls.Add(this.txtReg1PartyID);
			this.Frame1.Controls.Add(this.cmdReg1Search);
			this.Frame1.Controls.Add(this.cmdReg1Edit);
			this.Frame1.Controls.Add(this.txtCompanyCode);
			this.Frame1.Controls.Add(this.txtAddress2);
			this.Frame1.Controls.Add(this.txtOwner3Code);
			this.Frame1.Controls.Add(this.txtIDNumber);
			this.Frame1.Controls.Add(this.txtOwner2Code);
			this.Frame1.Controls.Add(this.txtOwner1Code);
			this.Frame1.Controls.Add(this.txtAddress);
			this.Frame1.Controls.Add(this.txtCity);
			this.Frame1.Controls.Add(this.txtZip);
			this.Frame1.Controls.Add(this.txtState);
			this.Frame1.Controls.Add(this.txtEpiryMonth);
			this.Frame1.Controls.Add(this.txtContact);
			this.Frame1.Controls.Add(this.txtCountry);
			this.Frame1.Controls.Add(this.txtDOB1);
			this.Frame1.Controls.Add(this.txtTelephone);
			this.Frame1.Controls.Add(this.txtDOB2);
			this.Frame1.Controls.Add(this.txtDOB3);
			this.Frame1.Controls.Add(this.Image1);
			this.Frame1.Controls.Add(this.Label17);
			this.Frame1.Controls.Add(this.Label16);
			this.Frame1.Controls.Add(this.imgReg3Comments);
			this.Frame1.Controls.Add(this.imgReg2Comments);
			this.Frame1.Controls.Add(this.lblRegistrant3);
			this.Frame1.Controls.Add(this.lblRegistrant2);
			this.Frame1.Controls.Add(this.lblRegistrant1);
			this.Frame1.Controls.Add(this.Label15);
			this.Frame1.Controls.Add(this.lblCompanyCode);
			this.Frame1.Controls.Add(this.Label14);
			this.Frame1.Controls.Add(this.Label13);
			this.Frame1.Controls.Add(this.Label12);
			this.Frame1.Controls.Add(this.Label11);
			this.Frame1.Controls.Add(this.imgReg1Comments);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Controls.Add(this.Label5);
			this.Frame1.Controls.Add(this.Label6);
			this.Frame1.Controls.Add(this.Label7);
			this.Frame1.Controls.Add(this.lblOwner1Info);
			this.Frame1.Controls.Add(this.Label9);
			this.Frame1.Controls.Add(this.Label10);
			this.Frame1.Controls.Add(this.txtFleetNumber);
			this.Frame1.Enabled = false;
			this.Frame1.Location = new System.Drawing.Point(30, 237);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(1003, 655);
			this.Frame1.TabIndex = 39;
			this.Frame1.Text = "Fleet Information";
			// 
			// cmdAddVehicles
			// 
			this.cmdAddVehicles.AppearanceKey = "actionButton";
			this.cmdAddVehicles.Enabled = false;
			this.cmdAddVehicles.Location = new System.Drawing.Point(718, 505);
			this.cmdAddVehicles.Name = "cmdAddVehicles";
			this.cmdAddVehicles.Size = new System.Drawing.Size(178, 40);
			this.cmdAddVehicles.TabIndex = 90;
			this.cmdAddVehicles.Text = "Add Vehicles";
			this.cmdAddVehicles.Click += new System.EventHandler(this.cmdAddVehicles_Click);
			// 
			// cmdRemoveVehicles
			// 
			this.cmdRemoveVehicles.AppearanceKey = "actionButton";
			this.cmdRemoveVehicles.Enabled = false;
			this.cmdRemoveVehicles.Location = new System.Drawing.Point(718, 555);
			this.cmdRemoveVehicles.Name = "cmdRemoveVehicles";
			this.cmdRemoveVehicles.Size = new System.Drawing.Size(178, 40);
			this.cmdRemoveVehicles.TabIndex = 89;
			this.cmdRemoveVehicles.Text = "Remove Vehicles";
			this.cmdRemoveVehicles.Click += new System.EventHandler(this.cmdRemoveVehicles_Click);
			// 
			// cmdUpdateEmail
			// 
			this.cmdUpdateEmail.AppearanceKey = "actionButton";
			this.cmdUpdateEmail.Enabled = false;
			this.cmdUpdateEmail.Location = new System.Drawing.Point(718, 605);
			this.cmdUpdateEmail.Name = "cmdUpdateEmail";
			this.cmdUpdateEmail.Size = new System.Drawing.Size(178, 40);
			this.cmdUpdateEmail.TabIndex = 88;
			this.cmdUpdateEmail.Text = "Update E-Mail";
			this.ToolTip1.SetToolTip(this.cmdUpdateEmail, "Update the E-Mail address tied to all registrations on fleet vehicles other than " +
        "ones that have overridden the fleet E-Mail adress.");
			this.cmdUpdateEmail.Click += new System.EventHandler(this.cmdUpdateEmail_Click);
			// 
			// txtEmail
			// 
			this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmail.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtEmail.Location = new System.Drawing.Point(148, 605);
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(476, 40);
			this.txtEmail.TabIndex = 86;
			// 
			// cboStatus
			// 
			this.cboStatus.BackColor = System.Drawing.SystemColors.Window;
			this.cboStatus.Items.AddRange(new object[] {
            "Active",
            "Deleted"});
			this.cboStatus.Location = new System.Drawing.Point(384, 30);
			this.cboStatus.Name = "cboStatus";
			this.cboStatus.Size = new System.Drawing.Size(140, 40);
			this.cboStatus.TabIndex = 84;
			// 
			// txtReg3PartyID
			// 
			this.txtReg3PartyID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtReg3PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtReg3PartyID.Location = new System.Drawing.Point(230, 251);
			this.txtReg3PartyID.Name = "txtReg3PartyID";
			this.txtReg3PartyID.TabIndex = 15;
			this.txtReg3PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg3PartyID_Validating);
			this.txtReg3PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg3PartyID_KeyPress);
			// 
			// cmdReg3Search
			// 
			this.cmdReg3Search.AppearanceKey = "actionButton";
			this.cmdReg3Search.ImageSource = "icon - search";
			this.cmdReg3Search.Location = new System.Drawing.Point(338, 251);
			this.cmdReg3Search.Name = "cmdReg3Search";
			this.cmdReg3Search.Size = new System.Drawing.Size(40, 40);
			this.cmdReg3Search.TabIndex = 16;
			this.cmdReg3Search.TabStop = false;
			this.cmdReg3Search.Click += new System.EventHandler(this.cmdReg3Search_Click);
			// 
			// cmdReg3Edit
			// 
			this.cmdReg3Edit.AppearanceKey = "actionButton";
			this.cmdReg3Edit.ImageSource = "icon - edit";
			this.cmdReg3Edit.Location = new System.Drawing.Point(388, 251);
			this.cmdReg3Edit.Name = "cmdReg3Edit";
			this.cmdReg3Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdReg3Edit.TabIndex = 17;
			this.cmdReg3Edit.TabStop = false;
			this.cmdReg3Edit.Click += new System.EventHandler(this.cmdReg3Edit_Click);
			// 
			// txtReg2PartyID
			// 
			this.txtReg2PartyID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtReg2PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtReg2PartyID.Location = new System.Drawing.Point(230, 202);
			this.txtReg2PartyID.Name = "txtReg2PartyID";
			this.txtReg2PartyID.TabIndex = 10;
			this.txtReg2PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2PartyID_Validating);
			this.txtReg2PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg2PartyID_KeyPress);
			// 
			// cmdReg2Search
			// 
			this.cmdReg2Search.AppearanceKey = "actionButton";
			this.cmdReg2Search.ImageSource = "icon - search";
			this.cmdReg2Search.Location = new System.Drawing.Point(338, 202);
			this.cmdReg2Search.Name = "cmdReg2Search";
			this.cmdReg2Search.Size = new System.Drawing.Size(40, 40);
			this.cmdReg2Search.TabIndex = 11;
			this.cmdReg2Search.TabStop = false;
			this.cmdReg2Search.Click += new System.EventHandler(this.cmdReg2Search_Click);
			// 
			// cmdReg2Edit
			// 
			this.cmdReg2Edit.AppearanceKey = "actionButton";
			this.cmdReg2Edit.ImageSource = "icon - edit";
			this.cmdReg2Edit.Location = new System.Drawing.Point(388, 202);
			this.cmdReg2Edit.Name = "cmdReg2Edit";
			this.cmdReg2Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdReg2Edit.TabIndex = 12;
			this.cmdReg2Edit.TabStop = false;
			this.cmdReg2Edit.Click += new System.EventHandler(this.cmdReg2Edit_Click);
			// 
			// txtReg1PartyID
			// 
			this.txtReg1PartyID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtReg1PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtReg1PartyID.Location = new System.Drawing.Point(230, 152);
			this.txtReg1PartyID.Name = "txtReg1PartyID";
			this.txtReg1PartyID.TabIndex = 5;
			this.txtReg1PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1PartyID_Validating);
			this.txtReg1PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg1PartyID_KeyPress);
			// 
			// cmdReg1Search
			// 
			this.cmdReg1Search.AppearanceKey = "actionButton";
			this.cmdReg1Search.ImageSource = "icon - search";
			this.cmdReg1Search.Location = new System.Drawing.Point(338, 152);
			this.cmdReg1Search.Name = "cmdReg1Search";
			this.cmdReg1Search.Size = new System.Drawing.Size(40, 40);
			this.cmdReg1Search.TabIndex = 6;
			this.cmdReg1Search.TabStop = false;
			this.cmdReg1Search.Click += new System.EventHandler(this.cmdReg1Search_Click);
			// 
			// cmdReg1Edit
			// 
			this.cmdReg1Edit.AppearanceKey = "actionButton";
			this.cmdReg1Edit.ImageSource = "icon - edit";
			this.cmdReg1Edit.Location = new System.Drawing.Point(388, 152);
			this.cmdReg1Edit.Name = "cmdReg1Edit";
			this.cmdReg1Edit.Size = new System.Drawing.Size(40, 40);
			this.cmdReg1Edit.TabIndex = 7;
			this.cmdReg1Edit.TabStop = false;
			this.cmdReg1Edit.Click += new System.EventHandler(this.cmdReg1Edit_Click);
			// 
			// txtCompanyCode
			// 
			this.txtCompanyCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtCompanyCode.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtCompanyCode.Location = new System.Drawing.Point(148, 80);
			this.txtCompanyCode.MaxLength = 4;
			this.txtCompanyCode.Name = "txtCompanyCode";
			this.txtCompanyCode.Size = new System.Drawing.Size(90, 40);
			this.txtCompanyCode.TabIndex = 1;
			this.txtCompanyCode.TabStop = false;
			this.txtCompanyCode.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			// 
			// txtAddress2
			// 
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtAddress2.Location = new System.Drawing.Point(148, 355);
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Size = new System.Drawing.Size(476, 40);
			this.txtAddress2.TabIndex = 20;
			// 
			// txtOwner3Code
			// 
			this.txtOwner3Code.BackColor = System.Drawing.SystemColors.Window;
			this.txtOwner3Code.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtOwner3Code.Location = new System.Drawing.Point(148, 252);
			this.txtOwner3Code.MaxLength = 1;
			this.txtOwner3Code.Name = "txtOwner3Code";
			this.txtOwner3Code.Size = new System.Drawing.Size(70, 40);
			this.txtOwner3Code.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.txtOwner3Code, "N = None I = Individual  M= Municipal  C = Commercial  D = Doing Business As");
			this.txtOwner3Code.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwner3Code_Validating);
			this.txtOwner3Code.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOwner3Code_KeyPress);
			// 
			// txtIDNumber
			// 
			this.txtIDNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtIDNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtIDNumber.Location = new System.Drawing.Point(305, 455);
			this.txtIDNumber.Name = "txtIDNumber";
			this.txtIDNumber.Size = new System.Drawing.Size(319, 40);
			this.txtIDNumber.TabIndex = 27;
			this.ToolTip1.SetToolTip(this.txtIDNumber, "Do not enter any spaces or dashes in your Tax ID Number");
			// 
			// txtOwner2Code
			// 
			this.txtOwner2Code.BackColor = System.Drawing.SystemColors.Window;
			this.txtOwner2Code.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtOwner2Code.Location = new System.Drawing.Point(148, 202);
			this.txtOwner2Code.MaxLength = 1;
			this.txtOwner2Code.Name = "txtOwner2Code";
			this.txtOwner2Code.Size = new System.Drawing.Size(70, 40);
			this.txtOwner2Code.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtOwner2Code, "N = None I = Individual  M= Municipal  C = Commercial  D = Doing Business As");
			this.txtOwner2Code.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwner2Code_Validating);
			this.txtOwner2Code.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOwner2Code_KeyPress);
			// 
			// txtOwner1Code
			// 
			this.txtOwner1Code.BackColor = System.Drawing.SystemColors.Window;
			this.txtOwner1Code.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtOwner1Code.Location = new System.Drawing.Point(148, 152);
			this.txtOwner1Code.MaxLength = 1;
			this.txtOwner1Code.Name = "txtOwner1Code";
			this.txtOwner1Code.Size = new System.Drawing.Size(70, 40);
			this.txtOwner1Code.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtOwner1Code, "I = Individual  M= Municipal  C = Commercia");
			this.txtOwner1Code.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwner1Code_Validating);
			this.txtOwner1Code.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOwner1Code_KeyPress);
			// 
			// txtAddress
			// 
			this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtAddress.Location = new System.Drawing.Point(148, 305);
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Size = new System.Drawing.Size(476, 40);
			this.txtAddress.TabIndex = 19;
			// 
			// txtCity
			// 
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtCity.Location = new System.Drawing.Point(148, 405);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(200, 40);
			this.txtCity.TabIndex = 21;
			// 
			// txtZip
			// 
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtZip.Location = new System.Drawing.Point(438, 405);
			this.txtZip.MaxLength = 15;
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(106, 40);
			this.txtZip.TabIndex = 23;
			// 
			// txtState
			// 
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtState.Location = new System.Drawing.Point(358, 405);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(70, 40);
			this.txtState.TabIndex = 22;
			// 
			// txtEpiryMonth
			// 
			this.txtEpiryMonth.BackColor = System.Drawing.SystemColors.Window;
			this.txtEpiryMonth.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtEpiryMonth.Location = new System.Drawing.Point(148, 455);
			this.txtEpiryMonth.MaxLength = 2;
			this.txtEpiryMonth.Name = "txtEpiryMonth";
			this.txtEpiryMonth.Size = new System.Drawing.Size(60, 40);
			this.txtEpiryMonth.TabIndex = 25;
			// 
			// txtContact
			// 
			this.txtContact.BackColor = System.Drawing.SystemColors.Window;
			this.txtContact.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtContact.Location = new System.Drawing.Point(148, 505);
			this.txtContact.Name = "txtContact";
			this.txtContact.Size = new System.Drawing.Size(476, 40);
			this.txtContact.TabIndex = 26;
			// 
			// txtCountry
			// 
			this.txtCountry.BackColor = System.Drawing.SystemColors.Window;
			this.txtCountry.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtCountry.Location = new System.Drawing.Point(554, 405);
			this.txtCountry.Name = "txtCountry";
			this.txtCountry.Size = new System.Drawing.Size(70, 40);
			this.txtCountry.TabIndex = 24;
			// 
			// txtDOB1
			// 
			this.txtDOB1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.txtDOB1.Location = new System.Drawing.Point(868, 152);
			this.txtDOB1.Mask = "##/##/####";
			this.txtDOB1.MaxLength = 10;
			this.txtDOB1.Name = "txtDOB1";
			this.txtDOB1.Size = new System.Drawing.Size(115, 22);
			this.txtDOB1.TabIndex = 8;
			this.txtDOB1.Visible = false;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Location = new System.Drawing.Point(148, 555);
			this.txtTelephone.MaxLength = 13;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Size = new System.Drawing.Size(171, 22);
			this.txtTelephone.TabIndex = 28;
			// 
			// txtDOB2
			// 
			this.txtDOB2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.txtDOB2.Location = new System.Drawing.Point(868, 202);
			this.txtDOB2.Mask = "##/##/####";
			this.txtDOB2.MaxLength = 10;
			this.txtDOB2.Name = "txtDOB2";
			this.txtDOB2.Size = new System.Drawing.Size(115, 22);
			this.txtDOB2.TabIndex = 13;
			this.txtDOB2.Visible = false;
			// 
			// txtDOB3
			// 
			this.txtDOB3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.txtDOB3.Location = new System.Drawing.Point(868, 252);
			this.txtDOB3.Mask = "##/##/####";
			this.txtDOB3.MaxLength = 10;
			this.txtDOB3.Name = "txtDOB3";
			this.txtDOB3.Size = new System.Drawing.Size(115, 22);
			this.txtDOB3.TabIndex = 18;
			this.txtDOB3.Visible = false;
			// 
			// Image1
			// 
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.ImageSource = "imgnote?color=#707884";
			this.Image1.Location = new System.Drawing.Point(247, 35);
			this.Image1.Name = "Image1";
			this.Image1.Size = new System.Drawing.Size(30, 30);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.Visible = false;
			this.Image1.Click += new System.EventHandler(this.Image1_Click);
			// 
			// Label17
			// 
			this.Label17.Location = new System.Drawing.Point(20, 619);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(50, 16);
			this.Label17.TabIndex = 87;
			this.Label17.Text = "E-MAIL";
			// 
			// Label16
			// 
			this.Label16.Location = new System.Drawing.Point(308, 44);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(50, 15);
			this.Label16.TabIndex = 85;
			this.Label16.Text = "STATUS";
			// 
			// imgReg3Comments
			// 
			this.imgReg3Comments.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgReg3Comments.Image = ((System.Drawing.Image)(resources.GetObject("imgReg3Comments.Image")));
			this.imgReg3Comments.Location = new System.Drawing.Point(438, 251);
			this.imgReg3Comments.Name = "imgReg3Comments";
			this.imgReg3Comments.Size = new System.Drawing.Size(40, 40);
			this.imgReg3Comments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgReg3Comments.Visible = false;
			this.imgReg3Comments.Click += new System.EventHandler(this.imgReg3Comments_Click);
			// 
			// imgReg2Comments
			// 
			this.imgReg2Comments.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgReg2Comments.Image = ((System.Drawing.Image)(resources.GetObject("imgReg2Comments.Image")));
			this.imgReg2Comments.Location = new System.Drawing.Point(438, 202);
			this.imgReg2Comments.Name = "imgReg2Comments";
			this.imgReg2Comments.Size = new System.Drawing.Size(40, 40);
			this.imgReg2Comments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgReg2Comments.Visible = false;
			this.imgReg2Comments.Click += new System.EventHandler(this.imgReg2Comments_Click);
			// 
			// lblRegistrant3
			// 
			this.lblRegistrant3.Location = new System.Drawing.Point(493, 265);
			this.lblRegistrant3.Name = "lblRegistrant3";
			this.lblRegistrant3.Size = new System.Drawing.Size(366, 25);
			this.lblRegistrant3.TabIndex = 73;
			// 
			// lblRegistrant2
			// 
			this.lblRegistrant2.Location = new System.Drawing.Point(493, 216);
			this.lblRegistrant2.Name = "lblRegistrant2";
			this.lblRegistrant2.Size = new System.Drawing.Size(366, 25);
			this.lblRegistrant2.TabIndex = 72;
			// 
			// lblRegistrant1
			// 
			this.lblRegistrant1.Location = new System.Drawing.Point(493, 166);
			this.lblRegistrant1.Name = "lblRegistrant1";
			this.lblRegistrant1.Size = new System.Drawing.Size(366, 25);
			this.lblRegistrant1.TabIndex = 71;
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(228, 469);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(50, 16);
			this.Label15.TabIndex = 70;
			this.Label15.Text = "TAX ID#";
			// 
			// lblCompanyCode
			// 
			this.lblCompanyCode.Location = new System.Drawing.Point(20, 94);
			this.lblCompanyCode.Name = "lblCompanyCode";
			this.lblCompanyCode.Size = new System.Drawing.Size(103, 16);
			this.lblCompanyCode.TabIndex = 69;
			this.lblCompanyCode.Text = "COMPANY CODE";
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(20, 369);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(111, 16);
			this.Label14.TabIndex = 59;
			this.Label14.Text = "ADDRESS LINE 2";
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(228, 131);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(45, 16);
			this.Label13.TabIndex = 58;
			this.Label13.Text = "NAME";
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(148, 131);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(40, 16);
			this.Label12.TabIndex = 57;
			this.Label12.Text = "CODE";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 265);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(119, 16);
			this.Label11.TabIndex = 56;
			this.Label11.Text = "OWNER 3";
			// 
			// imgReg1Comments
			// 
			this.imgReg1Comments.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgReg1Comments.Image = ((System.Drawing.Image)(resources.GetObject("imgReg1Comments.Image")));
			this.imgReg1Comments.Location = new System.Drawing.Point(438, 152);
			this.imgReg1Comments.Name = "imgReg1Comments";
			this.imgReg1Comments.Size = new System.Drawing.Size(40, 40);
			this.imgReg1Comments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgReg1Comments.Visible = false;
			this.imgReg1Comments.Click += new System.EventHandler(this.imgReg1Comments_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 167);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(119, 21);
			this.Label2.TabIndex = 48;
			this.Label2.Text = "OWNER 1";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 216);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(119, 21);
			this.Label3.TabIndex = 47;
			this.Label3.Text = "OWNER 2 ";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 319);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(111, 16);
			this.Label4.TabIndex = 46;
			this.Label4.Text = "ADDRESS LINE 1";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 422);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(111, 21);
			this.Label5.TabIndex = 45;
			this.Label5.Text = "C/S/Z/C";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(20, 519);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(70, 16);
			this.Label6.TabIndex = 44;
			this.Label6.Text = "CONTACT";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(20, 569);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(111, 16);
			this.Label7.TabIndex = 43;
			this.Label7.Text = "TELEPHONE";
			// 
			// lblOwner1Info
			// 
			this.lblOwner1Info.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblOwner1Info.Location = new System.Drawing.Point(868, 131);
			this.lblOwner1Info.Name = "lblOwner1Info";
			this.lblOwner1Info.Size = new System.Drawing.Size(40, 16);
			this.lblOwner1Info.TabIndex = 42;
			this.lblOwner1Info.Text = "DOB";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 469);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(130, 16);
			this.Label9.TabIndex = 41;
			this.Label9.Text = "EXPIRATION MONTH";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 44);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(95, 16);
			this.Label10.TabIndex = 40;
			this.Label10.Text = "FLEET NUMBER";
			// 
			// txtFleetNumber
			// 
			this.txtFleetNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtFleetNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtFleetNumber.Enabled = false;
			this.txtFleetNumber.Location = new System.Drawing.Point(148, 30);
			this.txtFleetNumber.MaxLength = 5;
			this.txtFleetNumber.Name = "txtFleetNumber";
			this.txtFleetNumber.Size = new System.Drawing.Size(90, 40);
			this.txtFleetNumber.TabIndex = 93;
			this.txtFleetNumber.TabStop = false;
			this.txtFleetNumber.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			// 
			// vsGroups
			// 
			this.vsGroups.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsGroups.ExtendLastCol = true;
			this.vsGroups.FixedCols = 0;
			this.vsGroups.Location = new System.Drawing.Point(547, 50);
			this.vsGroups.Name = "vsGroups";
			this.vsGroups.RowHeadersVisible = false;
			this.vsGroups.Rows = 1;
			this.vsGroups.ShowFocusCell = false;
			this.vsGroups.Size = new System.Drawing.Size(485, 152);
			this.vsGroups.TabIndex = 76;
			this.vsGroups.Enter += new System.EventHandler(this.vsGroups_Enter);
			this.vsGroups.DoubleClick += new System.EventHandler(this.vsGroups_DblClick);
			// 
			// vsFleets
			// 
			this.vsFleets.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsFleets.ExtendLastCol = true;
			this.vsFleets.FixedCols = 0;
			this.vsFleets.Location = new System.Drawing.Point(30, 50);
			this.vsFleets.Name = "vsFleets";
			this.vsFleets.RowHeadersVisible = false;
			this.vsFleets.Rows = 1;
			this.vsFleets.ShowFocusCell = false;
			this.vsFleets.Size = new System.Drawing.Size(485, 152);
			this.vsFleets.TabIndex = 81;
			this.vsFleets.Enter += new System.EventHandler(this.vsFleets_Enter);
			this.vsFleets.DoubleClick += new System.EventHandler(this.vsFleets_DblClick);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(55, 15);
			this.Label1.TabIndex = 83;
			this.Label1.Text = "FLEETS";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(502, 30);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(55, 15);
			this.Label8.TabIndex = 82;
			this.Label8.Text = "GROUPS";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddNewFleet,
            this.mnuAddNewGroup,
            this.mnuDeleteFleet,
            this.mnuFileSearch,
            this.mnuSeparator2,
            this.mnuFileComments,
            this.Separator1,
            this.mnuSave,
            this.mnuSaveQuit,
            this.mnuProcessSeperator,
            this.mnuQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuAddNewFleet
			// 
			this.mnuAddNewFleet.Index = 0;
			this.mnuAddNewFleet.Name = "mnuAddNewFleet";
			this.mnuAddNewFleet.Text = "Add New Fleet";
			this.mnuAddNewFleet.Click += new System.EventHandler(this.mnuAddNewFleet_Click);
			// 
			// mnuAddNewGroup
			// 
			this.mnuAddNewGroup.Index = 1;
			this.mnuAddNewGroup.Name = "mnuAddNewGroup";
			this.mnuAddNewGroup.Text = "Add New Group";
			this.mnuAddNewGroup.Click += new System.EventHandler(this.mnuAddNewGroup_Click);
			// 
			// mnuDeleteFleet
			// 
			this.mnuDeleteFleet.Index = 2;
			this.mnuDeleteFleet.Name = "mnuDeleteFleet";
			this.mnuDeleteFleet.Text = "Delete Fleet / Group";
			this.mnuDeleteFleet.Visible = false;
			this.mnuDeleteFleet.Click += new System.EventHandler(this.mnuDeleteFleet_Click);
			// 
			// mnuFileSearch
			// 
			this.mnuFileSearch.Index = 3;
			this.mnuFileSearch.Name = "mnuFileSearch";
			this.mnuFileSearch.Shortcut = Wisej.Web.Shortcut.CtrlS;
			this.mnuFileSearch.Text = "Search";
			this.mnuFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// mnuSeparator2
			// 
			this.mnuSeparator2.Index = 4;
			this.mnuSeparator2.Name = "mnuSeparator2";
			this.mnuSeparator2.Text = "-";
			// 
			// mnuFileComments
			// 
			this.mnuFileComments.Enabled = false;
			this.mnuFileComments.Index = 5;
			this.mnuFileComments.Name = "mnuFileComments";
			this.mnuFileComments.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuFileComments.Text = "Comments";
			this.mnuFileComments.Click += new System.EventHandler(this.mnuFileComments_Click);
			// 
			// Separator1
			// 
			this.Separator1.Index = 6;
			this.Separator1.Name = "Separator1";
			this.Separator1.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 7;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveQuit
			// 
			this.mnuSaveQuit.Index = 8;
			this.mnuSaveQuit.Name = "mnuSaveQuit";
			this.mnuSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveQuit.Text = "Save and Quit";
			this.mnuSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
			// 
			// mnuProcessSeperator
			// 
			this.mnuProcessSeperator.Index = 9;
			this.mnuProcessSeperator.Name = "mnuProcessSeperator";
			this.mnuProcessSeperator.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 10;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Exit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(453, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(78, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdAddNewFleet
			// 
			this.cmdAddNewFleet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddNewFleet.Location = new System.Drawing.Point(532, 29);
			this.cmdAddNewFleet.Name = "cmdAddNewFleet";
			this.cmdAddNewFleet.Size = new System.Drawing.Size(110, 24);
			this.cmdAddNewFleet.TabIndex = 1;
			this.cmdAddNewFleet.Text = "Add New Fleet";
			this.cmdAddNewFleet.Click += new System.EventHandler(this.mnuAddNewFleet_Click);
			// 
			// cmdAddNewGroup
			// 
			this.cmdAddNewGroup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddNewGroup.Location = new System.Drawing.Point(648, 29);
			this.cmdAddNewGroup.Name = "cmdAddNewGroup";
			this.cmdAddNewGroup.Size = new System.Drawing.Size(116, 24);
			this.cmdAddNewGroup.TabIndex = 2;
			this.cmdAddNewGroup.Text = "Add New Group";
			this.cmdAddNewGroup.Click += new System.EventHandler(this.mnuAddNewGroup_Click);
			// 
			// cmdFileSearch
			// 
			this.cmdFileSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSearch.ImageSource = "button-search";
			this.cmdFileSearch.Location = new System.Drawing.Point(912, 29);
			this.cmdFileSearch.Name = "cmdFileSearch";
			this.cmdFileSearch.Shortcut = Wisej.Web.Shortcut.CtrlS;
			this.cmdFileSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdFileSearch.TabIndex = 3;
			this.cmdFileSearch.Text = "Search";
			this.cmdFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// cmdComments
			// 
			this.cmdComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdComments.Enabled = false;
			this.cmdComments.Location = new System.Drawing.Point(976, 29);
			this.cmdComments.Name = "cmdComments";
			this.cmdComments.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdComments.Size = new System.Drawing.Size(84, 24);
			this.cmdComments.TabIndex = 4;
			this.cmdComments.Text = "Comments";
			this.cmdComments.Click += new System.EventHandler(this.mnuFileComments_Click);
			// 
			// cmdDeleteFleet
			// 
			this.cmdDeleteFleet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteFleet.Location = new System.Drawing.Point(770, 29);
			this.cmdDeleteFleet.Name = "cmdDeleteFleet";
			this.cmdDeleteFleet.Size = new System.Drawing.Size(136, 24);
			this.cmdDeleteFleet.TabIndex = 5;
			this.cmdDeleteFleet.Text = "Delete Fleet/Group";
			this.cmdDeleteFleet.Visible = false;
			this.cmdDeleteFleet.Click += new System.EventHandler(this.mnuDeleteFleet_Click);
			// 
			// frmFleetMaster
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1014, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmFleetMaster";
			this.Text = "Fleet Master Add / Update";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmFleetMaster_Load);
			this.Activated += new System.EventHandler(this.frmFleetMaster_Activated);
			this.Resize += new System.EventHandler(this.frmFleetMaster_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFleetMaster_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFleetMaster_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVehiclesToAdd)).EndInit();
			this.fraVehiclesToAdd.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFleetSearchResults)).EndInit();
			this.fraFleetSearchResults.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdResultsCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdResultsGetInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResults)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearchFleet)).EndInit();
			this.fraSearchFleet.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelFleetSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchFleetSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
			this.fraSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddVehicles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRemoveVehicles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdateEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg3Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg3Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg2Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Search)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReg1Edit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReg3Comments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReg2Comments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgReg1Comments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsGroups)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFleets)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNewFleet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddNewGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteFleet)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAddNewFleet;
		private FCButton cmdFileSearch;
		private FCButton cmdAddNewGroup;
		private FCButton cmdComments;
		private FCButton cmdDeleteFleet;
	}
}
