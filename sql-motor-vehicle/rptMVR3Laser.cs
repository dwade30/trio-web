//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class rptMVR3Laser : BaseSectionReport
    {
		public rptMVR3Laser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "MVR3E";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMVR3Laser InstancePtr
		{
			get
			{
				return (rptMVR3Laser)Sys.GetInstance(typeof(rptMVR3Laser));
			}
		}

		protected rptMVR3Laser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMVR3Laser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public string strPrtCodes = "";
		public bool boolTest;
		int intCounter;
		bool blnFirstRecord;
		private bool boolIsPreReg;

		public bool IsPreReg
		{
			set
			{
				boolIsPreReg = value;
			}
			get
			{
				bool IsPreReg = false;
				IsPreReg = boolIsPreReg;
				return IsPreReg;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (MotorVehicle.Statics.gboolFromBatchRegistration)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= frmBatchProcessing.InstancePtr.intNumberToPrint)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp;
			if (MotorVehicle.Statics.gboolFromBatchRegistration)
			{
				blnFirstRecord = true;
				intCounter = 0;
			}

			strTemp = MotorVehicle.GetComputerMVSetting("MVR3ETopAdjustment");
			if (strTemp != "")
			{
				this.PageSettings.Margins.Top += FCConvert.ToSingle(Conversion.Val(strTemp) * 1440) / 1440F;
			}
			strTemp = MotorVehicle.GetComputerMVSetting("MVR3ELeftAdjustment");
			if (strTemp != "")
			{
				this.PageSettings.Margins.Left += FCConvert.ToSingle(Conversion.Val(strTemp) * 1440) / 1440F;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				if (Statics.intSectCnt == 0)
				{
					Statics.intSectCnt = 1;
					ResetFeeLines();
				}
				else
				{
					Statics.intSectCnt += 1;
				}
				if (Statics.intSectCnt == 1)
				{
					SetupReg();
				}

				if (Statics.intSectCnt == 2)
				{
					lblSOSURL.Visible = true;
				}
				else
				{
					lblSOSURL.Visible = false;
				}

				if (Statics.intSectCnt < 3)
				{
					LayoutAction = (GrapeCity.ActiveReports.LayoutAction)(FCConvert.ToInt32(GrapeCity.ActiveReports.LayoutAction.PrintSection + FCConvert.ToInt32(GrapeCity.ActiveReports.LayoutAction.MoveLayout)));
					Detail.Height = 5220 / 1440F;
					Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				else
				{
					Statics.intSectCnt = 0;
					Detail.Height = 4760 / 1440F;
					Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (fecherFoundation.Information.Err(ex).Number != 0)
				{
					MotorVehicle.PrinterError("MVR3 Printer");
					MotorVehicle.Statics.PrintMVR3 = false;
					rptMVR3Laser.InstancePtr.Cancel();
					this.Close();
				}
			}
		}

		private void AddFeeLine(string strFeeLabel, string strFeeValue, bool boolPrintMsg = false, bool boolForceRight = false, bool boolReset = false)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label objLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			GrapeCity.ActiveReports.SectionReportModel.TextBox objField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();

			if (Statics.intLineNo == 0 || boolReset)
			{
				Statics.intLineNo = 1;
				Statics.intRSkip = 0;
			}
			else
			{
				if (Statics.intLineNo < 18)
				{
					if (boolForceRight)
					{
						Statics.intRSkip += 1;
					}
					else
					{
						Statics.intLineNo += 1;
					}
				}
				else
                {
                    if (Statics.intLineNo == 18)
                    {
                        Statics.intLineNo += Statics.intRSkip + 1;
                        Statics.intRSkip = 0;
                    }
                    else
                    {
                        Statics.intLineNo += 1;
                    }
                }
            }

			if (Statics.intLineNo > 32)
			{
				return;
			}
			if (boolForceRight && Statics.intLineNo < 18)
			{
				objLabel = (Sections["Detail"].Controls["lblFee" + (18 + Statics.intRSkip)] as GrapeCity.ActiveReports.SectionReportModel.Label);
				objField = (Sections["Detail"].Controls["fldFee" + (18 + Statics.intRSkip)] as GrapeCity.ActiveReports.SectionReportModel.TextBox);
			}
			else
			{
				objLabel = (Sections["Detail"].Controls["lblFee" + Statics.intLineNo] as GrapeCity.ActiveReports.SectionReportModel.Label);
				objField = (Sections["Detail"].Controls["fldFee" + Statics.intLineNo] as GrapeCity.ActiveReports.SectionReportModel.TextBox);
			}


			if (boolPrintMsg)
			{
				objField.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				objField.Text = "-" + fecherFoundation.Strings.Trim(strFeeValue);
				objField.Visible = true;
				objLabel.Visible = false;
			}
			else
			{
				objField.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				objField.Text = strFeeValue;
				objField.Visible = true;
				objLabel.Text = strFeeLabel;
				objLabel.Visible = true;
			}
		}


		private void ResetFeeLines()
		{
			int i;
			for (i = 1; i <= 32; i++)
			{
				Sections["Detail"].Controls["lblFee" + i].Visible = false;
				Sections["Detail"].Controls["fldFee" + i].Visible = false;
			}
            Statics.intLineNo = 0;
            Statics.intRSkip = 0;
		}

		private void AddReasonCodeLines(string strReasonCodes)
		{
			string strTemp = "";
			string[] arrCodes = null;
			int i;
			bool boolSkip = false;

			if (fecherFoundation.Strings.Trim(FCConvert.ToString(strReasonCodes)) != "")
			{
				strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PrintPriorities")));
				arrCodes = Strings.Split(FCConvert.ToString(strReasonCodes), ",", -1, CompareConstants.vbBinaryCompare);
				strTemp = "";
				for (i = 0; i <= (Information.UBound(arrCodes, 1)); i++)
				{
					boolSkip = false;
					if (MotorVehicle.Statics.blnPrintingDupReg && i == 0)
					{
						strTemp = "D " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");
						if (Strings.Left(arrCodes[i], 1) == "3")
						{
							AddFeeLine("", arrCodes[i], true);
							boolSkip = true;
						}
						AddFeeLine("", strTemp, true);
						strTemp = "";
					}
					if (!boolSkip)
					{
						if (arrCodes[i].Length > 19)
						{
							if (strTemp != "")
							{
								AddFeeLine("", strTemp, true);
							}
							AddFeeLine("", arrCodes[i], true, true);
							strTemp = "";
						}
						else
						{
							if (strTemp.Length + arrCodes[i].Length > 19)
							{
								AddFeeLine("", strTemp, true);
								strTemp = arrCodes[i];
							}
							else
							{
								if (strTemp != "")
								{
									strTemp += "," + arrCodes[i];
								}
								else
								{
									strTemp = arrCodes[i];
								}
							}
						}
					}
				}
				if (strTemp != "")
				{
					AddFeeLine("", strTemp, true);
				}
			}
			else
			{
				if (MotorVehicle.Statics.blnPrintingDupReg)
				{
					strTemp = "D " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");
					AddFeeLine("", strTemp, true);
				}
			}
		}

		private string GetValidationAmountString()
		{
			string GetValidationAmountString = "";
			clsDRWrapper rsFees = new clsDRWrapper();
			string strFee = "";
			clsDRWrapper rsClass = new clsDRWrapper();

            const string currencyFormat = "$0.00";

            if (MotorVehicle.Statics.RegistrationType == "LOST")
			{
				if (MotorVehicle.Statics.gboolFromBatchRegistration)
				{
					strFee = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeRereg"), currencyFormat);
				}
				else
				{
					strFee = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee"), currencyFormat);
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "DUPREG")
			{
					rsFees.OpenRecordset("SELECT DupRegAgentFee, DuplicateRegistration FROM DefaultInfo");
					if (rsFees.EndOfFile() != true && rsFees.BeginningOfFile() != true)
					{
						rsClass.OpenRecordset("SELECT RegistrationFee FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
						if (FCConvert.ToDecimal(rsClass.Get_Fields("RegistrationFee")) != 0)
						{
							strFee = Strings.Format(rsFees.Get_Fields_Decimal("DupRegAgentFee") + rsFees.Get_Fields_Decimal("DuplicateRegistration"), currencyFormat);
						}
						else
						{
							strFee = Strings.Format(rsFees.Get_Fields_Decimal("DupRegAgentFee"), currencyFormat);
						}
					}
					else
					{
						strFee = "$3.00";
					}
				}
			else if (MotorVehicle.Statics.ExciseAP)
			{
				strFee = Strings.Format(MotorVehicle.Statics.GrandTotal, currencyFormat);
			}
			else if (MotorVehicle.Statics.RegistrationType == "CORR")
			{
				strFee = Strings.Format(MotorVehicle.Statics.ECTotal, currencyFormat);
			}
			else if (MotorVehicle.Statics.RegistrationType == "GVW")
			{
				if (MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") < 0)
					MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", 0);
				strFee = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid"), currencyFormat);
			}
			else
			{
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE COAST TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "PROCUREMENT SPR" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER")
				{
					strFee = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") + MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid") - MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee"), currencyFormat);
				}
				else
				{
					strFee = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") + MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid"), currencyFormat);
				}
			}
			// XXX    End If
            rsFees.DisposeOf();
            rsClass.DisposeOf();
			GetValidationAmountString = strFee;
			return GetValidationAmountString;
		}

		private void SetupReg()
		{
			string c = "";
			Decimal ExciseBal = 0;
			string MVTyp;
			string Info = "";
			string CL1 = "";
			string CL2 = "";
			string strOwner1;
			string strOwner2 = "";
			string strOwner3 = "";
			string strLessor = "";
			double Mrate = 0;
			Decimal Credit;
			Decimal RegFee;
			string Town = "";
			string SD = "";
			string printfile = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strColor;
			string strCSZ = "";
			int i;
			string strRebateFlag = "";
			Decimal curExciseCredit;
			bool boolShowFees;
			clsPrintCodes oPrtCodes = new clsPrintCodes();
			MotorVehicle.Statics.PrintMVR3 = false;
			fecherFoundation.Information.Err().Clear();

            try
            {
                fecherFoundation.Information.Err()
                                .Clear();

                if (boolTest)
                {
                    SetupTestReg(1);

                    return;
                }

                string countychecker = "";

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"))
                             .Length
                    < 5)
                {
                    countychecker = "0" + MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
                }
                else
                {
                    countychecker = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
                }

                if (Strings.Left(countychecker, 2) == "05")
                {
                    c = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));

                    if (c != "AQ" && c != "CL" && c != "CV" && c != "HC" && c != "IU" && c != "LS" && c != "MC" && c != "MM" && c != "MP" && c != "MQ" && c != "MX" && c != "PM" && c != "SE" && c != "SR" && c != "TC" && c != "TL" && c != "TR" && c != "TT" && c != "VM" && c != "XV")
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel")) != "D" && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("Year")) > 1973)
                        {
                            fldCounty.Visible = true;
                        }
                        else
                        {
                            fldCounty.Visible = false;
                        }
                    }
                    else
                    {
                        fldCounty.Visible = false;
                    }
                }
                else
                {
                    fldCounty.Visible = false;
                }

                if (!MotorVehicle.Statics.gboolFromBatchRegistration)
                { }
                else
                {
                    if (fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col)) != "")
                    {
                        if (Strings.Left(MotorVehicle.Statics.PP1Text, 4) == "UNIT")
                        {
                            MotorVehicle.Statics.PP1Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
                        }
                        else
                            if (Strings.Left(MotorVehicle.Statics.PP2Text, 4) == "UNIT")
                            {
                                MotorVehicle.Statics.PP2Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
                            }
                            else
                                if (Strings.Left(MotorVehicle.Statics.PP3Text, 4) == "UNIT")
                                {
                                    MotorVehicle.Statics.PP3Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
                                }
                                else
                                {
                                    if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP1Text) == "")
                                    {
                                        MotorVehicle.Statics.PP1Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
                                    }
                                    else
                                        if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP2Text) == "")
                                        {
                                            MotorVehicle.Statics.PP2Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
                                        }
                                        else
                                            if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.PP3Text) == "")
                                            {
                                                MotorVehicle.Statics.PP3Text = "UNIT# " + fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUserDefined1Col));
                                            }
                                }
                    }
                    else
                    {
                        if (Strings.Left(MotorVehicle.Statics.PP1Text, 4) == "UNIT")
                        {
                            MotorVehicle.Statics.PP1Text = "";
                        }
                        else
                            if (Strings.Left(MotorVehicle.Statics.PP2Text, 4) == "UNIT")
                            {
                                MotorVehicle.Statics.PP2Text = "";
                            }
                            else
                                if (Strings.Left(MotorVehicle.Statics.PP3Text, 4) == "UNIT")
                                {
                                    MotorVehicle.Statics.PP3Text = "";
                                }
                    }
                }

                MVTyp = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")), 3);
                MotorVehicle.Statics.RegType = "";

                var isAPClass = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AP";

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
                {
                    if (MVTyp == "NRR" || MVTyp == "NRT")
                    {
                        MotorVehicle.Statics.RegType = "E-New-Reg";
                    }
                    else
                        if (MVTyp == "RRR" || MVTyp == "RRT")
                        {
                            MotorVehicle.Statics.RegType = "E-Re-reg";
                        }
                        else
                            if (MVTyp == "ECO" || MVTyp == "ECR" || MVTyp == "LPS" || MVTyp == "GVW")
                            {
                                MotorVehicle.Statics.RegType = "E-Correct";
                            }
                            else
                                if (MVTyp == "DPR")
                                {
                                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "NRR" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "NRT")
                                    {
                                        MotorVehicle.Statics.RegType = "E-New-Reg";
                                    }
                                    else
                                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRR" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRT")
                                        {
                                            MotorVehicle.Statics.RegType = "E-Re-reg";
                                        }
                                        else
                                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "ECO" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "ECR" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "LPS" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "GVW")
                                            {
                                                MotorVehicle.Statics.RegType = "E-Correct";
                                            }
                                }
                }
                else
                {
                    if (MVTyp == "NRR" || MVTyp == "NRT")
                    {
                        if (isAPClass)
                        {
                            MotorVehicle.Statics.RegType = "New-Reg Excise Tax Only-IRP";
                        }
                        else
                        {
                            MotorVehicle.Statics.RegType = "New-Reg";
                        }
                    }
                    else
                        if (MVTyp == "RRR" || MVTyp == "RRT")
                        {
                            if (isAPClass)
                            {
                                MotorVehicle.Statics.RegType = "Re-reg Excise Tax Only-IRP";
                            }
                            else
                            {
                                MotorVehicle.Statics.RegType = "Re-reg";
                            }
                        }
                        else
                            if (MVTyp == "ECO")
                            {
                                if (isAPClass)
                                {
                                    MotorVehicle.Statics.RegType = "Correct Excise Tax Only-IRP";
                                }
                                else
                                {
                                    MotorVehicle.Statics.RegType = "Correct Excise Tax Receipt";
                                }
                            }
                }

                fldRegType.Text = MotorVehicle.Statics.RegType;

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) != true)
                {
                    if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer")) != 0)
                    {
                        lblMileage.Visible = true;
                        fldMileage.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Odometer"), "#,###,###");
                    }
                    else
                    {
                        lblMileage.Visible = false;
                        fldMileage.Visible = false;
                    }
                }
                else
                {
                    lblMileage.Visible = false;
                    fldMileage.Visible = false;
                }

                fldClass.Text = "";
                fldRegNumber.Text = "";

                if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))) != "NEW")
                {
                    fldClass.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");

                    if (MotorVehicle.Statics.gboolFromBatchRegistration)
                    {
                        fldRegNumber.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPlateCol);
                    }
                    else
                    {
                        fldRegNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
                    }
                }
                else
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "AP")
                    {
                        fldClass.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                    }

                const string dateFormat = "MM/dd/yyyy";

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) && (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP")
                {
                    fldEffective.Text = "";
                }
                else if (MotorVehicle.Statics.RegistrationType == "CORR" &&
                         MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO") &&
                         (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("TransactionType") == "NRR" ||
                          MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("TransactionType") == "NRT" ||
                          MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("TransactionType") == "RRT") &&
                         FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP")
                {
	                fldEffective.Text = "";
				}
				else
				{
                    if (MotorVehicle.Statics.rsFinal.IsFieldNull("EffectiveDate") || !MotorVehicle.Statics.rsFinal.Get_Fields_String("EffectiveDate").IsDate())
                        fldEffective.Text = "";
                    else
                        fldEffective.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"), dateFormat);
                }

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == true 
                    && MotorVehicle.Statics.RegistrationType == "NRR" && (!MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") || !FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")))
                    && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "AP"
                    && !MotorVehicle.IsProratedMotorcycle(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"), MotorVehicle.Statics.datMotorCycleEffective,
		                                                 ref MotorVehicle.Statics.datMotorCycleExpires, MotorVehicle.Statics.RegistrationType))
                {
                    fldExpires.Text = "";
                }
                else
                {
                    if (MotorVehicle.Statics.rsFinal.IsFieldNull("ExpireDate") || !MotorVehicle.Statics.rsFinal.Get_Fields_String("ExpireDate").IsDate())
                        fldExpires.Text = "";
                    else
                        fldExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), dateFormat);
                }

                if (MotorVehicle.Statics.gboolFromBatchRegistration)
                {
                    fldVIN.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intVINCol);
                    fldYear.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intYearCol);
                    fldMake.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMakeCol);
					fldModel.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intModelCol).Left(6);
                    fldStyle.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intStyleCol);
                    fldAxles.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intAxlesCol);
                }
                else
                {
                    fldVIN.Text = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")), 17);
                    fldYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
                    fldMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make"));
					fldModel.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("model")).Left(6);
                    fldStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
                    fldAxles.Text = "";

                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles")) != true)
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles")) != 0)
                        {
                            fldAxles.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles"));
                        }
                    }
                }
                fldColor.Text = "";
                if (MotorVehicle.Statics.Class != "AP")
                {
                    strColor = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color1"));

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2")) != "NA")
                    {
                        strColor += "/" + MotorVehicle.Statics.rsFinal.Get_Fields_String("color2");
                    }

                    fldColor.Text = strColor;
                }
                fldTires.Text = "";
                if (MotorVehicle.Statics.Class != "AP")
                {
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires")) != true)
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires")) != 0)
                        {
                            fldTires.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Tires"));
                        }
                    }
                }
                fldNetWeight.Text = "";

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "SE" || isAPClass)
                {
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != true)
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != 0)
                        {
                            fldNetWeight.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"), "######"), "@@@@@@");
                        }
                    }
                }

                fldRegWeight.Text = "";

                if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")) != true)
                {
                    if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew")) != 0)
                    {
                        fldRegWeight.Text = Strings.Format(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("RegisteredWeightNew"), "######"), "@@@@@@");
                    }
                }

                string vbPorterVar = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));

                if ((vbPorterVar == "SE") || (vbPorterVar == "TC") || (vbPorterVar == "TL") || (vbPorterVar == "CL") || (vbPorterVar == "HC"))
                {
                    fldFuel.Text = "";
                }
                else
                {
                    fldFuel.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Fuel"));
                }

                fldOwner1.Text = "";
                fldDOB1.Text = "";
                strOwner1 = MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName1");

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "E")
                {
                    strOwner1 += ", LESSEE";
                }
                else
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "T" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I")
                    {
                        strOwner1 += ", TRUSTEE";
                    }
                    else
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "P")
                        {
                            strOwner1 += ", PERSONAL REPRESENTATIVE";
                        }

                if (strOwner1.Length > 39) strOwner1 = Strings.Mid(strOwner1, 1, 39);
                fldOwner1.Text = strOwner1;

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I")
                {
                    fldDOB1.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB1"), dateFormat);
                }
                else
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber").Trim() != "")
                    {
                        fldDOB1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber"));
                    }
                }

                fldOwner2.Text = "";
                fldDOB2.Text = "";

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) != "N")
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "D")
                    {
                        strOwner2 = "DBA ";
                    }
                    else
                    {
                        strOwner2 = "";
                    }

                    strOwner2 += MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName2");

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "E")
                    {
                        strOwner2 += ", LESSEE";
                    }
                    else
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "T" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "I")
                        {
                            strOwner2 += ", TRUSTEE";
                        }
                        else
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "P")
                            {
                                strOwner2 += ", PERSONAL REPRESENTATIVE";
                            }

                    if (strOwner2.Length > 39) strOwner2 = Strings.Mid(strOwner2, 1, 39);
                    fldOwner2.Text = strOwner2;

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "I")
                    {
                        fldDOB2.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB2"), dateFormat);
                    }
                    else
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "R")
                        {
                            if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber")) == false && MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber").Trim() != "")
                            {
                                fldDOB2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber"));
                            }
                        }
                }

                fldOwner3.Text = "";
                fldDOB3.Text = "";

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) != "N")
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "D")
                    {
                        strOwner3 = "DBA ";
                    }
                    else
                    {
                        strOwner3 = "";
                    }

                    strOwner3 += MotorVehicle.Statics.rsFinal.Get_Fields_String("PartyName3");

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "E")
                    {
                        strOwner3 += ", LESSEE";
                    }
                    else
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "T" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "I")
                        {
                            strOwner3 += ", TRUSTEE";
                        }
                        else
                            if (MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3") == "P")
                            {
                                strOwner3 += ", PERSONAL REPRESENTATIVE";
                            }

                    if (strOwner3.Length > 39) strOwner3 = Strings.Mid(strOwner3, 1, 39);
                    fldOwner3.Text = strOwner3;

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode3")) == "I")
                    {
                        fldDOB3.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("DOB3"), dateFormat);
                    }
                    else
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1")) == "I" && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "I" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode2")) == "N"))
                        {
                            if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber")) == false && MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber").Trim() != "")
                            {
                                fldDOB3.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TaxIDNumber"));
                            }
                        }
                }

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
                {
                    if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("BaseIsSalePrice")))
                    {
                        AddFeeLine("Base", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"), "###,###,###.00"), false, false, true);
                    }
                    else
                    {
                        AddFeeLine("Base", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("BasePrice"), "###,###,###"), false, false, true);
                    }

                    Mrate = 0;
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "1") Mrate = 0.024;
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "2") Mrate = 0.0175;
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "3") Mrate = 0.0135;
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "4") Mrate = 0.01;
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "5") Mrate = 0.0065;
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MillYear")) == "6") Mrate = 0.004;
                    AddFeeLine("Mil. Rate", Strings.Format(Mrate, "####0.0000"));
                }

                AddFeeLine("Local", "");

                var muniName = fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName);
                var exciseCredit_Used = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
                var exciseTax_Charged = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"));

                const string numericFormat = "##,##0.00";

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseExempt")) == true && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
                {
                    if (muniName != "PROCUREMENT SPR")
                    {
                        AddFeeLine("Ex Tax", "EXEMPT");
                    }
                }
                else
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
                    {
                        if (MotorVehicle.Statics.ExciseAP)
                        {
                            AddFeeLine("Ex Tax", "AP " + Strings.Format(frmDataInput.InstancePtr.txtExciseTax.Text, numericFormat));
                        }
                        else
                        {
                            var exciseTax_ChargedString = Strings.Format(exciseTax_Charged, numericFormat);

                            if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")))
                            {
                                AddFeeLine("Ex Tax", "H/R " + exciseTax_ChargedString);
                            }
                            else
                                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")))
                                {
                                    AddFeeLine("Ex Tax", "P/R " + exciseTax_ChargedString);
                                }
                                else
                                {
                                    AddFeeLine("Ex Tax", exciseTax_ChargedString);
                                }
                        }
                    }
                    else
                    {
                        AddFeeLine("Ex Tax", "N/A");
                    }


                if (exciseCredit_Used != 0)
                {
                    strRebateFlag = "";
                    curExciseCredit = FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));

                    if (MVTyp == "RRT" || MVTyp == "NRT" || (MVTyp == "DPR" && (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "NRT")))
                    {
                        if (MotorVehicle.Statics.blnAllowExciseRebates)
                        {
                            if (exciseTax_Charged < exciseCredit_Used)
                            {
                                strRebateFlag = "# ";
                                curExciseCredit = FCConvert.ToDecimal(exciseTax_Charged);
                            }
                        }
                    }
                    var curExciseCreditString =  strRebateFlag + Strings.Format(curExciseCredit, numericFormat);

                    if (MotorVehicle.Statics.ExciseAP)
                    {
                        AddFeeLine("Credit", "AP " + curExciseCreditString);
                    }
                    else
                        if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")) == true && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U")
                        {
                            AddFeeLine("Credit", "H/R " + curExciseCreditString);
                        }
                        else
                            if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")) == true)
                            {
                                AddFeeLine("Credit", "P/R " + curExciseCreditString);
                            }
                            else
                            {
                                AddFeeLine("Credit", curExciseCreditString);
                            }
                }

                if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge")))
                {
                    if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge")) != 0)
                    {
                        var exciseTransferChargeString = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), numericFormat);

                        if (MotorVehicle.Statics.ExciseAP)
                        {
                            AddFeeLine("Trans. Chg.", "AP " + exciseTransferChargeString);
                        }
                        else
                        {
                            AddFeeLine("Trans. Chg.", exciseTransferChargeString);
                        }
                    }
                }

                if (MotorVehicle.Statics.ExciseAP)
                {
                    if (MotorVehicle.Statics.RegistrationType != "DUPREG")
                    {
                        ExciseBal = frmDataInput.InstancePtr.txtBalance.Text.ToDecimalValue();
                    }
                    else
                    {
                        decimal FeesAmount = FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
                        if (FeesAmount < 0) FeesAmount = 0;
                        FeesAmount += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("ExciseTransferCharge")));

                        if (FeesAmount < 0)
                        {
                            FeesAmount = 0;
                        }

                        ExciseBal = FeesAmount;
                    }
                }
                else
                {
                    ExciseBal = FCConvert.ToDecimal(exciseTax_Charged - exciseCredit_Used);
                }

                if (ExciseBal < 0) ExciseBal = 0;
                ExciseBal += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge")));

                if (MotorVehicle.Statics.ExciseAP)
                {
                    AddFeeLine("ExTx Bal", "AP " + Strings.Format(ExciseBal, numericFormat));
                }
                else
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Trailer")) != "U" && ExciseBal != 0)
                    {
                        if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseHalfRate")) == true)
                        {
                            AddFeeLine("ExTx Bal", "H/R " + Strings.Format(ExciseBal, numericFormat));
                        }
                        else
                            if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ExciseProrate")) == true)
                            {
                                AddFeeLine("ExTx Bal", "P/R " + Strings.Format(ExciseBal, numericFormat));
                            }
                            else
                            {
                                AddFeeLine("ExTx Bal", Strings.Format(ExciseBal, numericFormat));
                            }
                    }

                if (MVTyp == "RRT" || MVTyp == "NRT" || MVTyp == "ECR" || MVTyp == "ECO" || (MVTyp == "DPR" && (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "NRT")))
                {
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3")) != true && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3")) != 0)
                    {
                        if (MotorVehicle.Statics.ExciseAP)
                        {
                            AddFeeLine("Credit No.", "AP " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3"));
                        }
                        else
                        {
                            AddFeeLine("Credit No.", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3")));
                        }
                    }
                }

                if (MotorVehicle.Statics.ExciseAP)
                {
                    AddFeeLine("ExTx Date", "AP " + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"), dateFormat));
                }
                else
                {
                    AddFeeLine("ExTx Date", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"), dateFormat));
                }

                boolShowFees = false;
                RegFee = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")));
                if (RegFee < 0) RegFee = 0;

				if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull") != MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") || Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")) > 0)
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) != 0)
                        {
                            if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")))
                            {
                                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "NRT")
                                {
                                    AddFeeLine("Rate", "H/R " + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00"));
                                }
                                else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "ECR")
                                {
                                    if (RegFee != 0)
                                    {
                                        boolShowFees = true;
                                        AddFeeLine("Rate", "H/R " + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00"));
                                    }
                                    else
                                    {
										boolShowFees = true;
										AddFeeLine("Rate", "");
									}
                                }
                            }
                            else
							{
								if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "ECR")
								{
									if (RegFee != 0)
									{
										boolShowFees = true;
										AddFeeLine("Rate", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00"));
									}
									else
									{
										boolShowFees = true;
										AddFeeLine("Rate", "");
									}
								}
								else
								{
									AddFeeLine("Rate", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00"));
								}
							}
                        }
                        else
                        {
                            string vbPorterVar1 = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));

                            if ((vbPorterVar1 == "CI") || (vbPorterVar1 == "DV") || (vbPorterVar1 == "MM") || (vbPorterVar1 == "XV") || (vbPorterVar1 == "MO") || (vbPorterVar1 == "PH") || (vbPorterVar1 == "PM") || (vbPorterVar1 == "PO") || (vbPorterVar1 == "PS") || (vbPorterVar1 == "VX"))
                            {
                                AddFeeLine("Rate", "EXEMPT");
                            }
                        }
                    }
                }

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
                {
                    Credit = FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")));

                    if (Credit != 0)
                    {
                        if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")) == true)
                        {
                            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "NRT")
                            {
                                AddFeeLine("Credit", "H/R " + Strings.Format(Credit, "###0.00"));
                            }
                            else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "ECR")
                            {
	                            if (RegFee != 0)
	                            {
		                            boolShowFees = true;
									AddFeeLine("Credit", "H/R " + Strings.Format(Credit, "###0.00"));
								}
	                            else
	                            {
		                            boolShowFees = true;
		                            AddFeeLine("Credit", "");
	                            }
                            }
                            else
                            {
	                            boolShowFees = true;
	                            AddFeeLine("Credit", "H/R " + Strings.Format(Credit, "###0.00"));
							}
                        }
                        else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegProrate")) == true)
                        {
                            AddFeeLine("Credit", "P/R " + Strings.Format(Credit, "###0.00"));
                        }
						else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) == "ECR")
                        {
	                        if (RegFee != 0)
	                        {
		                        boolShowFees = true;
		                        AddFeeLine("Credit", Strings.Format(Credit, "###0.00"));
	                        }
	                        else
	                        {
		                        boolShowFees = true;
		                        AddFeeLine("Credit", "");
	                        }
                        }
						else
                        {
                            AddFeeLine("Credit", Strings.Format(Credit, "###0.00"));
                        }
                    }
                }

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
                {
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", 0);
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("GIFTCERTIFICATEAMOUNT", 0);
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("CommercialCredit", 0);
                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", 0);
                   
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher")) != "V")
                    {
                        RegFee += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GIFTCERTIFICATEAMOUNT")));
                    }
                    else
                    {
                        RegFee += FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")));
                    }

                    if (RegFee != 0 || boolShowFees)
                    {
                        if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegHalf")) == true)
                        {
                            AddFeeLine("Fees", "H/R " + Strings.Format(RegFee, "#,##0.00"));
                        }
                        else
                            if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegProrate")) == true)
                            {
                                AddFeeLine("Fees", "P/R " + Strings.Format(RegFee, "#,##0.00"));
                            }
                            else
                            {
                                AddFeeLine("Fees", Strings.Format(RegFee, "#,##0.00"));
                            }
                    }
                    else
                        if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RegExempt")) == true)
                        {
                            AddFeeLine("Fees", "EXEMPT");
                        }
                }

                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee")) != 0 && muniName != "PROCUREMENT SPR" && modGlobalConstants.Statics.MuniName != "MAINE TRAILER" && muniName != "MAINE COAST TRAILER" && muniName != "ACE REGISTRATION SERVICES LLC" && muniName != "COUNTRYWIDE TRAILER")
                {
                    AddFeeLine("Agent Fee", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee"), "###0.00"));
                }

                if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax")) != 0 || ((MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT") && (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") || MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("DealerSalesTax") || MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("SalesTaxExempt"))))
                {
                    if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("DealerSalesTax")))
                    {
                        AddFeeLine("Sales Tax", "DLR");
                    }
                    else
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax")) == 0)
                        {
                            AddFeeLine("Sales Tax", "NO FEE");
                        }
                        else
                        {
                            AddFeeLine("Sales Tax", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"), numericFormat));
                        }
                }

                if (MVTyp == "RRR" || MVTyp == "RRT" || MVTyp == "NRR" || MVTyp == "NRT" || MVTyp == "ECO" || MVTyp == "DPR")
                {
                    if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee")) != 0)
                    {
                        AddFeeLine("Title", Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"), "#,##0.00"));
                    }
                    else
                        if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeCTA")
                            && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber"))
                                         .Length
                                > 1
                                || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber"))
                                            .Length
                                > 1))
                        {
                            AddFeeLine("Title", "NO FEE");
                        }
                }

                if (MotorVehicle.Statics.gboolFromBatchRegistration)
                {
                    AddFeeLine("CTA #", frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intCTACol));
                }
                else
                {
                    if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber"))) == "")
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber"))
                                     .Length
                            > 1)
                        {
                            AddFeeLine("CTA #", MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber"));
                        }
                    }
                    else
                    {
                        AddFeeLine("CTA #", MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber"));
                        AddFeeLine("CTA #2", MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber"));
                    }
                }

                for (i = 1; i <= (Information.UBound(MotorVehicle.Statics.gPPriority, 1)); i++)
                {
                    if (MotorVehicle.Statics.blnPrintingDupReg)
                    {
                        if (string.Compare(MotorVehicle.Statics.gPPriNumber[i - 1], oPrtCodes.DupRegNoFee) < 0 && string.Compare(MotorVehicle.Statics.gPPriNumber[i], oPrtCodes.DupRegNoFee) > 0)
                        {
                            AddFeeLine("", oPrtCodes.ReturnPrintPriorityText(FCConvert.ToInt32(oPrtCodes.DupRegNoFee)), true);
                        }
                    }

                    if (MotorVehicle.Statics.gPPriority[i] != null)
                    {
                        if (MotorVehicle.Statics.gPPriority[i]
                                .Length
                            > 20)
                        {
                            AddFeeLine("", MotorVehicle.Statics.gPPriority[i], true, true);
                        }
                        else
                        {
                            AddFeeLine("", MotorVehicle.Statics.gPPriority[i], true);
                        }
                    }
                }

                // i
                if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.RRTextAll) != "" || MotorVehicle.Statics.blnPrintingDupReg)
                {
                    AddReasonCodeLines(MotorVehicle.Statics.RRTextAll);
                }

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "R")
                {
                    strLessor = fldOwner1.Text;
                    fldOwner1.Text = fldOwner2.Text;
                    fldOwner2.Text = fldOwner3.Text;
                    fldDOB1.Text = fldDOB2.Text;
                    fldDOB2.Text = fldDOB3.Text;
                    fldOwner3.Text = "";
                    fldDOB3.Text = "";
                }
                else
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "R")
                    {
                        strLessor = fldOwner2.Text;
                        fldOwner2.Text = fldOwner3.Text;
                        fldDOB2.Text = fldDOB3.Text;
                        fldOwner3.Text = "";
                        fldDOB3.Text = "";
                    }
                    else
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "R")
                        {
                            strLessor = fldOwner3.Text;
                            fldOwner3.Text = "";
                            fldDOB3.Text = "";
                        }
                        else
                        {
                            strLessor = "";
                        }

                if (strLessor != "")
                {
                    if (fecherFoundation.Strings.Trim(strLessor)
                                        .Length
                        > 33)
                    {
                        fldLessor.Text = Strings.Mid(strLessor, 1, 33);
                    }
                    else
                    {
                        fldLessor.Text = strLessor;
                    }
                }

                if (MotorVehicle.Statics.gboolFromBatchRegistration)
                {
                    if (frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUnitCol)
                                          .Length
                        > 12)
                    {
                        fldUnit.Text = Strings.Left(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUnitCol), 12);
                    }
                    else
                    {
                        fldUnit.Text = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intUnitCol);
                    }
                }
                else
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) != "...." && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) != "    ")
                    {
                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"))
                                     .Length
                            > 12)
                        {
                            fldUnit.Text = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")), 12);
                        }
                        else
                        {
                            fldUnit.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
                        }
                    }
                    else
                    {
                        fldUnit.Text = "";
                    }
                }

                if (Strings.Mid(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber")), 1, 1) != "." && fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber")) != true)
                {
                    fldDOTNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("DOTNumber"));
                }
                else
                {
                    fldDOTNumber.Text = "";
                }

                if (!MotorVehicle.Statics.blnSuspended)
                {
                    fldAddress1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
                    fldAddress2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address2"));
                    strCSZ = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State")) != "CC")
                    {
                        strCSZ += "  " + MotorVehicle.Statics.rsFinal.Get_Fields("State");
                    }

                    strCSZ += "  " + MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip");
                    fldCityStateZip.Text = strCSZ;
                }
                else
                {
                    fldAddress1.Text = "REGISTRATION SUSPENDED";
                    fldAddress2.Text = "CONTACT: (207) 624-9000 EXT 52143";
                    strCSZ = "";
                    fldCityStateZip.Text = "DO NOT ISSUE";
                }
                fldResidenceAddress.Text = (MotorVehicle.Statics.Class != "AP") ? FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceAddress")) : "";
                fldResidenceCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
                fldResidenceState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));

                if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode")) != "00000")
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"))
                                 .Length
                        < 5)
                    {
                        fldResidenceCode.Text = "0" + MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");
                    }
                    else
                    {
                        fldResidenceCode.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
                    }
                }
                else
                {
                    fldResidenceCode.Text = " N/A ";
                }

                if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                {
                    fldUserId.Text = MotorVehicle.Statics.UserID;
                }
                else
                {
                    fldUserId.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("InsuranceInitials");
                }

                if (muniName == "MAINE MOTOR TRANSPORT")
                {
                    Town = "MMTA SERVICES INC";
                }
                else
                    if (muniName == "HANCOCK COUNTY")
                    {
                        Town = "FLETCHERS LANDING";
                    }
                    else
                    {
                        Town = modGlobalConstants.Statics.MuniName;
                    }

                fldValidation1.Text = "";
                fldValidation2.Text = "";
                fldRegistrationCode.Text = "";
                fldRegistrationDate.Text = "";
                fldValidation4.Text = "";
                fldValidation5.Text = "";

                var rsFinalMVR3 = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false && MotorVehicle.Statics.TownLevel != 9 && MotorVehicle.Statics.TownLevel != 6)
                {
                    fldValidation1.Text = "VALIDATED REGISTRATION";
                    fldValidation2.Text = fecherFoundation.Strings.UCase(Town);

                    if (MotorVehicle.Statics.ResidenceCode.Length < 5)
                    {
                        fldRegistrationCode.Text = "0" + MotorVehicle.Statics.ResidenceCode;
                    }
                    else
                    {
                        fldRegistrationCode.Text = MotorVehicle.Statics.ResidenceCode;
                    }

                    if (!IsPreReg)
                    {
                        fldRegistrationDate.Text = Strings.Format(DateTime.Now, dateFormat);
                    }
                    else
                    {
                        fldRegistrationDate.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"), dateFormat);
                    }

                    if (MotorVehicle.Statics.blnPrintingDupReg)
                    {
                        fldValidation4.Text = "$0.00";
                        fldValidation5.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("NoFeeMVR3Number"));
                    }
                    else
                    {
                        fldValidation4.Text = GetValidationAmountString();
                        fldValidation5.Text = FCConvert.ToString(rsFinalMVR3);
                    }
                }
                else if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")))
                {
                    if (MotorVehicle.Statics.gboolFromBatchRegistration)
                    {
                        var textMatrixMVR3 = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3Col);

                        if (isAPClass)
                        {
                            fldValidation1.Text = "EXCISE TAX ONLY-IRP\\" + textMatrixMVR3;
                        }
                        else
                        {
                            fldValidation1.Text = "EXCISE TAX RECEIPT\\" + textMatrixMVR3;
                        }
                    }
                    else
                    {
                        if (isAPClass)
                        {
                            fldValidation1.Text = "EXCISE TAX ONLY-IRP " + rsFinalMVR3;
                        }
                        else
                        {
                            fldValidation1.Text = "EXCISE TAX RECEIPT " + rsFinalMVR3;
                        }
                    }

					if (MVTyp == "ECO" || MVTyp == "ECR")
					{
                        fldRegistrationDate.Text = Strings.Format(DateTime.Now, dateFormat);
					}
				}

                if (MotorVehicle.Statics.gboolFromBatchRegistration)
                {
                    fldMonthSticker.Text = Strings.Left(MotorVehicle.ReturnMonthStickerNumber(), 3) + FCConvert.ToString(MotorVehicle.strPadZeros(fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMonthStickerCol)), 7));
                    fldYearSticker.Text = Strings.Left(MotorVehicle.ReturnYearStickerNumber(), 3) + FCConvert.ToString(MotorVehicle.strPadZeros(fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intYearStickerCol)), 7));
                }
                else
                {
	                if (!MotorVehicle.Statics.blnPrintingDupReg)
	                {
		                fldMonthSticker.Text = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber")) == 0 ? "" : MotorVehicle.ReturnMonthStickerNumber();
		                fldYearSticker.Text = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber")) == 0 ? "" : MotorVehicle.ReturnYearStickerNumber();
                    }
	                else
	                {
		                fldMonthSticker.Text = "";
		                fldYearSticker.Text = "";
	                }
                }

                fldPriorTitle.Text = "";

                if (FCConvert.CBool(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO")) == false)
                {
                    var priorTitleNumber = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber")));
                    var hasPriorTitle = priorTitleNumber.Length > 0;

                    var priorTitleInfo = priorTitleNumber + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleState");

                    if (MVTyp == "NRR" || MVTyp == "NRT" || MVTyp == "DPR")
                    {
                        if (MotorVehicle.Statics.gboolFromBatchRegistration)
                        {
                            if (hasPriorTitle)
                            {
                                fldPriorTitle.Text = fecherFoundation.Strings.Trim(frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPriorTitleCol)) + " " + frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPriorStateCol);
                            }
                        }
                        else
                        {
                            if (hasPriorTitle)
                            {
                                fldPriorTitle.Text = priorTitleInfo;
                            }
                        }
                    }
                    else
                        if (MVTyp == "ECO" || MVTyp == "ECR")
                        {
                            if (hasPriorTitle)
                            {
                                if (priorTitleNumber != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("PriorTitleNumber"))))
                                {
                                    fldPriorTitle.Text = priorTitleInfo;
                                }
                            }
                        }
                }

                fecherFoundation.Information.Err().Clear();

                if (fecherFoundation.Information.Err().Number!= 0) return;

                MotorVehicle.Statics.PrintMVR3 = true;

                if (MotorVehicle.Statics.gboolFromBatchRegistration)
                {
                    frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intPrintedCol, FCConvert.ToString(true));

                    var textMatrixValue = frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol);

                    if (textMatrixValue == "") return;

                    rsTemp.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + FCConvert.ToString(Conversion.Val(textMatrixValue)));

                    if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                    {
                        rsTemp.Edit();
                        rsTemp.Set_Fields("Date", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                        rsTemp.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                        rsTemp.Set_Fields("MVR3", textMatrixValue);
                        rsTemp.Set_Fields("Status", "V");
                        rsTemp.Update();
                    }

                    rsTemp.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
                    rsTemp.AddNew();
                    rsTemp.Set_Fields("DateOfAdjustment", DateTime.Today);
                    rsTemp.Set_Fields("High", textMatrixValue);
                    rsTemp.Set_Fields("Low", textMatrixValue);
                    rsTemp.Set_Fields("OpID", MotorVehicle.Statics.rsFinal.Get_Fields_String("OpID"));
                    rsTemp.Set_Fields("reason", "Printer Alignment/Jam Problem");
                    rsTemp.Set_Fields("PeriodCloseoutKey", 0);
                    rsTemp.Set_Fields("QuantityAdjusted", 1);
                    rsTemp.Set_Fields("InventoryType", "MXS00");
                    rsTemp.Set_Fields("AdjustmentCode", "V");
                    rsTemp.Update();
                    frmBatchProcessing.InstancePtr.vsVehicles.TextMatrix(frmBatchProcessing.InstancePtr.intStartRow + intCounter, frmBatchProcessing.InstancePtr.intMVR3ToVoidCol, "");
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsTemp.DisposeOf();
            }
		}

		private void SetupTestReg(int intSampleNumber)
		{
			switch (intSampleNumber)
			{
				case 1:
					{
						fldMileage.Text = "28,750";
						fldEffective.Text = "12/10/2015";
						fldExpires.Text = "12/31/2016";
						fldClass.Text = "CO";
						fldRegNumber.Text = "797-307";
						fldVIN.Text = "1FTEX1EM1EFC58292";
						fldYear.Text = "2014";
						fldMake.Text = "FORD";
						fldModel.Text = "F150";
						fldColor.Text = "BK/WH";
						fldStyle.Text = "00";
						fldTires.Text = "4";
						fldAxles.Text = "2";
						fldNetWeight.Text = "";
						fldRegWeight.Text = "10000";
						fldFuel.Text = "G";
						fldOwner1.Text = "WILDE, MARTIN D, LESSEE";
						fldDOB1.Text = "02/08/1949";
						fldOwner2.Text = "";
						fldDOB2.Text = "";
						fldOwner3.Text = "";
						fldDOB3.Text = "";
						fldLessor.Text = "FORD LEASING INC";
						fldUnit.Text = "";
						fldDOTNumber.Text = "1127965";
						fldAddress1.Text = "15 WASHINGTON ST";
						fldAddress2.Text = "";
						fldCityStateZip.Text = "FALMOUTH  ME  04105";
						fldResidenceAddress.Text = "15 WASHINGTON ST";
						fldResidenceCode.Text = "05070";
						fldResidenceCity.Text = "FALMOUTH";
						fldResidenceState.Text = "ME";
						fldPriorTitle.Text = "13312021  ME";
						fldMonthSticker.Text = "12D 03969658";
						fldYearSticker.Text = "16D 05649642";
						fldValidation1.Text = "VALIDATED REGISTRATION";
						fldValidation2.Text = "FALMOUTH";
						fldRegistrationCode.Text = "05070";
						fldRegistrationDate.Text = "12/10/2015";
						fldValidation4.Text = "$2626.64";
						fldValidation5.Text = "12345678";
						fldUserId.Text = "NIC";
						fldRegType.Text = "E-New-Reg";
						AddFeeLine("Base", "35,865", false, true);
						AddFeeLine("Mil. Rate", ".0175");
						AddFeeLine("Local", "627.64");
						AddFeeLine("Ex Tax", "");
						AddFeeLine("ExTx Bal", "627.64");
						AddFeeLine("ExTx Date", "12/10/2015");
						AddFeeLine("Rate", "37.00");
						AddFeeLine("Fees", "37.00");
						AddFeeLine("Agent Fee", "4.00");
						AddFeeLine("Sales Tax", "1,925.00");
						AddFeeLine("Title", "33.00");
						AddFeeLine("CTA #", "GG31462");
						AddFeeLine("NPROP", "", true);
						break;
					}
				case 2:
					{
						fldMileage.Text = "182,320";
						fldEffective.Text = "10/01/2015";
						fldExpires.Text = "10/31/2016";
						fldClass.Text = "AW";
						fldRegNumber.Text = "314-AMM";
						fldVIN.Text = "2HKRL18682H502077";
						fldYear.Text = "2002";
						fldMake.Text = "HOND";
						fldModel.Text = "ODYSSE";
						fldColor.Text = "GY";
						fldStyle.Text = "VN";
						fldTires.Text = "";
						fldAxles.Text = "";
						fldNetWeight.Text = "";
						fldRegWeight.Text = "";
						fldFuel.Text = "G";
						fldOwner1.Text = "XYZ TOWING INC";
						fldDOB1.Text = "308205001";
						fldOwner2.Text = "";
						fldDOB2.Text = "";
						fldOwner3.Text = "";
						fldDOB3.Text = "";
						fldLessor.Text = "";
						fldUnit.Text = "";
						fldDOTNumber.Text = "";
						fldAddress1.Text = "PO BOX 280";
						fldAddress2.Text = "";
						fldCityStateZip.Text = "LEBANON  ME  04027";
						fldResidenceAddress.Text = "72 PATTEN RD";
						fldResidenceCode.Text = "31140";
						fldResidenceCity.Text = "LEBANON";
						fldResidenceState.Text = "ME";
						fldPriorTitle.Text = "";
						fldMonthSticker.Text = "";
						fldYearSticker.Text = "16d 05649643";
						fldValidation1.Text = "VALIDATED REGISTRATION";
						fldValidation2.Text = "LEBANON";
						fldRegistrationCode.Text = "31140";
						fldRegistrationDate.Text = "12/10/2015";
						fldValidation4.Text = "$160.00";
						fldValidation5.Text = "12345679";
						fldUserId.Text = "NIC";
						fldRegType.Text = "E-Re-reg";
						AddFeeLine("Base", "26,750", false, true);
						AddFeeLine("Mil. Rate", ".0040");
						AddFeeLine("Local", "107.00");
						AddFeeLine("Ex Tax", "");
						AddFeeLine("ExTx Bal", "107.00");
						AddFeeLine("ExTx Date", "12/10/2015");
						AddFeeLine("Rate", "35.00");
						AddFeeLine("Fees", "35.00");
						AddFeeLine("Agent Fee", "3.00");
						AddFeeLine("AW $15.00", "", true);
						break;
					}
				case 3:
					{
						fldMileage.Text = "26,756";
						fldEffective.Text = "12/10/2015";
						fldExpires.Text = "09/30/2016";
						fldClass.Text = "PC";
						fldRegNumber.Text = "STORM 78";
						fldVIN.Text = "1G1PC5SH1C7331907";
						fldYear.Text = "2012";
						fldMake.Text = "CHEV";
						fldModel.Text = "CRUZEL";
						fldColor.Text = "OR";
						fldStyle.Text = "4D";
						fldTires.Text = "";
						fldAxles.Text = "";
						fldNetWeight.Text = "";
						fldRegWeight.Text = "";
						fldFuel.Text = "G";
						fldOwner1.Text = "EMERSON, RALPH W";
						fldDOB1.Text = "11/06/1940";
						fldOwner2.Text = "EMERSON, SALLY M";
						fldDOB2.Text = "01/19/1942";
						fldOwner3.Text = "";
						fldDOB3.Text = "";
						fldLessor.Text = "";
						fldUnit.Text = "";
						fldDOTNumber.Text = "";
						fldAddress1.Text = "PO BOX 396";
						fldAddress2.Text = "";
						fldCityStateZip.Text = "CARRABASSETT VALLEY  ME  04947";
						fldResidenceAddress.Text = "46 CEMETERY RD";
						fldResidenceCode.Text = "07018";
						fldResidenceCity.Text = "CARRABASSETT VALLEY";
						fldResidenceState.Text = "ME";
						fldPriorTitle.Text = "13658707  NH";
						fldMonthSticker.Text = "";
						fldYearSticker.Text = "";
						fldValidation1.Text = "VALIDATED REGISTRATION";
						fldValidation2.Text = "CARRABASSETT VALLEY";
						fldRegistrationCode.Text = "07018";
						fldRegistrationDate.Text = "12/10/2015";
						fldValidation4.Text = "$2626.64";
						fldValidation5.Text = "12345680";
						fldUserId.Text = "NIC";
						fldRegType.Text = "E-New-Reg";
						AddFeeLine("Base", "18,865", false, true);
						AddFeeLine("Mil. Rate", ".0100");
						AddFeeLine("Local", "188.65");
						AddFeeLine("Ex Tax", "");
						AddFeeLine("Credit", "399.87");
						AddFeeLine("Trans. Chg.", "3.00");
						AddFeeLine("ExTx Bal", "3.00");
						AddFeeLine("Credit No.", "15696102");
						AddFeeLine("ExTx Date", "12/10/2015");
						AddFeeLine("Rate", "35.00");
						AddFeeLine("Credit", "35.00");
						AddFeeLine("Fees", "8.00");
						AddFeeLine("Agent Fee", "4.00");
						AddFeeLine("Sales Tax", "DLR");
						AddFeeLine("Title", "66.00");
						AddFeeLine("CTA #", "LL96209");
						AddFeeLine("CTA #2", "LL96210");
						AddFeeLine("TRANSFER", "", true);
						AddFeeLine("3 15696102", "", true);
						break;
					}
				case 4:
					{
						fldMileage.Text = "";
						fldMileage.Visible = false;
						lblMileage.Visible = false;
						fldEffective.Text = "11/01/2015";
						fldExpires.Text = "11/30/2016";
						fldClass.Text = "AP";
						fldRegNumber.Text = "919-364";
						fldVIN.Text = "1M1AA14Y04N156121";
						fldYear.Text = "2004";
						fldMake.Text = "MACK";
						fldModel.Text = "600CH6";
						fldColor.Text = "WH";
						fldStyle.Text = "35";
						fldTires.Text = "10";
						fldAxles.Text = "3";
						fldNetWeight.Text = "16500";
						fldRegWeight.Text = "";
						fldFuel.Text = "D";
						fldOwner1.Text = "L T MITCHELL CORP";
						fldDOB1.Text = "072483991";
						fldOwner2.Text = "";
						fldDOB2.Text = "";
						fldOwner3.Text = "";
						fldDOB3.Text = "";
						fldLessor.Text = "";
						fldUnit.Text = "";
						fldDOTNumber.Text = "3147829";
						fldAddress1.Text = "84 WALNUT ST";
						fldAddress2.Text = "";
						fldCityStateZip.Text = "NEEDHAM  MA  02194";
						fldResidenceAddress.Text = "100 MAIN ST";
						fldResidenceCode.Text = "05200";
						fldResidenceCity.Text = "SCARBOROUGH";
						fldResidenceState.Text = "ME";
						fldPriorTitle.Text = "13312021  ME";
						fldMonthSticker.Text = "";
						fldYearSticker.Text = "";
						fldValidation1.Text = "Excise Tax Only-IRP 12345681";
						fldValidation2.Text = "";
						fldRegistrationCode.Text = "";
						fldRegistrationDate.Text = "";
						fldValidation4.Text = "";
						fldValidation5.Text = "";
						fldUserId.Text = "NIC";
						fldRegType.Text = "Re-reg Excise Tax Only-IRP";
						AddFeeLine("Base", "68,256", false, true);
						AddFeeLine("Mil. Rate", ".0040");
						AddFeeLine("Local", "273.02");
						AddFeeLine("Ex Tax", "");
						AddFeeLine("ExTx Bal", "273.02");
						AddFeeLine("ExTx Date", "12/10/2015");
						break;
					}
				case 5:
					{
						fldMileage.Text = "";
						fldMileage.Visible = false;
						lblMileage.Visible = false;
						fldEffective.Text = "10/01/2015";
						fldExpires.Text = "10/31/2016";
						fldClass.Text = "VT";
						fldRegNumber.Text = "22540";
						fldVIN.Text = "JF1GF4854XH803755";
						fldYear.Text = "1999";
						fldMake.Text = "SUBA";
						fldModel.Text = "IMPREZ";
						fldColor.Text = "GR";
						fldStyle.Text = "4D";
						fldTires.Text = "";
						fldAxles.Text = "";
						fldNetWeight.Text = "";
						fldRegWeight.Text = "";
						fldFuel.Text = "G";
						fldOwner1.Text = "RICKER, ADAM P";
						fldDOB1.Text = "07/14/1969";
						fldOwner2.Text = "DBA THE COZY CAFE";
						fldDOB2.Text = "";
						fldOwner3.Text = "";
						fldDOB3.Text = "";
						fldLessor.Text = "";
						fldUnit.Text = "";
						fldDOTNumber.Text = "";
						fldAddress1.Text = "54 CENTER AVE";
						fldAddress2.Text = "";
						fldCityStateZip.Text = "UNITY  ME  04988";
						fldResidenceAddress.Text = "54 CENTER AVE";
						fldResidenceCode.Text = "27240";
						fldResidenceCity.Text = "UNITY";
						fldResidenceState.Text = "ME";
						fldPriorTitle.Text = "";
						fldMonthSticker.Text = "";
						fldYearSticker.Text = "";
						fldValidation1.Text = "Excise Tax Receipt 12345682";
						fldValidation2.Text = "";
						fldRegistrationCode.Text = "";
						fldRegistrationDate.Text = "";
						fldValidation4.Text = "";
						fldValidation5.Text = "";
						fldUserId.Text = "NIC";
						fldRegType.Text = "Re-reg";
						AddFeeLine("Base", "19,270", false, true);
						AddFeeLine("Mil. Rate", ".0040");
						AddFeeLine("Local", "77.08");
						AddFeeLine("Ex Tax", "");
						AddFeeLine("ExTx Bal", "77.08");
						AddFeeLine("ExTx Date", "12/10/2015");
						AddFeeLine("EAMECM", "", true);
						break;
					}
			}
		}

		public class StaticVariables
		{
			public int intSectCnt = 0;
			public int intLineNo = 0;
			public int intRSkip = 0;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
