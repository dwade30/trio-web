//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptExciseTaxOnly.
	/// </summary>
	public partial class rptExciseTaxOnly : BaseSectionReport
	{
		public rptExciseTaxOnly()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Excise Tax Only Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptExciseTaxOnly InstancePtr
		{
			get
			{
				return (rptExciseTaxOnly)Sys.GetInstance(typeof(rptExciseTaxOnly));
			}
		}

		protected rptExciseTaxOnly _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptExciseTaxOnly	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngPK1;
		int lngPK2;
		string strSQL = "";
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rsIRP = new clsDRWrapper();
		clsDRWrapper rsCorrections = new clsDRWrapper();
		clsDRWrapper rsETO = new clsDRWrapper();
		private bool etoComplete = false;
		private bool apComplete = false;
		private bool correctionsComplete = false;
		int intPageNumber;
		bool boolData;
		private string subReportTitle = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!rs.EndOfFile())
			{
				eArgs.EOF = false;
				if (!etoComplete)
				{
					subDetail.Report = new srptExciseTaxOnlyDetail("EXCISE TAX RECEIPT", BuildSubReportDetails(rsETO));
					etoComplete = true;
				}
				else if (!apComplete)
				{
					subDetail.Report = new srptExciseTaxOnlyDetail("EXCISE TAX ONLY - IRP", BuildSubReportDetails(rsIRP));
					apComplete = true;
				}
				else if (!correctionsComplete)
				{
					subDetail.Report = new srptExciseTaxOnlyDetail("EXCISE TAX CORRECTIONS", BuildSubReportDetails(rsCorrections));
					correctionsComplete = true;
				}
				else
				{
					subDetail.Report = null;
					eArgs.EOF = true;
				}
			}
			else
			{
				subDetail.Report = null;
				eArgs.EOF = true;
				return;
			}
			boolData = true;
		}

		private List<string> BuildSubReportDetails(clsDRWrapper rsDetail)
		{
			var result = new List<string>();

			do
			{
				result.Add(Strings.Format(Strings.Format(rsDetail.Get_Fields_Int32("MVR3"), "#######"), "@@@@@@@") + " " + Strings.StrDup(3 - FCConvert.ToString(rsDetail.Get_Fields_String("OpID")).Length, " ") + rsDetail.Get_Fields_String("OpID"));
				rsDetail.MoveNext();
			} while (rsDetail.EndOfFile() != true);

			return result;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
			}
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM ExciseTax WHERE PeriodCloseoutID < 1 ORDER BY DateOfTransaction, MVR3Number");
				rsCorrections.OpenRecordset("SELECT * FROM ActivityMaster WHERE ETO = 1 AND Status <> 'V' AND TransactionType = 'ECO' AND OldMVR3 < 1 ORDER BY DateUpdated, MVR3");
				rsIRP.OpenRecordset("SELECT * FROM ActivityMaster WHERE ETO = 1 AND Status <> 'V' AND TransactionType <> 'ECO' AND Class = 'AP' AND OldMVR3 < 1 ORDER BY DateUpdated, MVR3");
				rsETO.OpenRecordset("SELECT * FROM ActivityMaster WHERE ETO = 1 AND Status <> 'V' AND TransactionType <> 'ECO' AND Class <> 'AP' AND OldMVR3 < 1 ORDER BY DateUpdated, MVR3");
			}
			else
			{
				strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSQL);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
				rs.OpenRecordset("SELECT * FROM ExciseTax WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY DateOfTransaction, MVR3Number");
				rsCorrections.OpenRecordset("SELECT * FROM ActivityMaster WHERE ETO = 1 AND Status <> 'V' AND TransactionType = 'ECO' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " ORDER BY DateUpdated, MVR3");
				rsIRP.OpenRecordset("SELECT * FROM ActivityMaster WHERE ETO = 1 AND Status <> 'V' AND TransactionType <> 'ECO' AND Class = 'AP' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " ORDER BY DateUpdated, MVR3");
				rsETO.OpenRecordset("SELECT * FROM ActivityMaster WHERE ETO = 1 AND Status <> 'V' AND TransactionType <> 'ECO' AND Class <> 'AP' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " ORDER BY DateUpdated, MVR3");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				rsCorrections.MoveLast();
				rsCorrections.MoveFirst();
				rsETO.MoveLast();
				rsETO.MoveFirst();
				rsIRP.MoveLast();
				rsIRP.MoveFirst();

				correctionsComplete = rsCorrections.RecordCount() == 0;
				etoComplete = rsETO.RecordCount() == 0;
				apComplete = rsIRP.RecordCount() == 0;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			SubReport2.Report = new rptSubReportHeading();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (boolData == false)
			{
				txtNoInfo.Text = "No Information";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (rs.RecordCount() > 0)
			{
				lblTotalTransactions.Text = "TOTAL EXCISE TAX TRANSACTIONS: " + rs.RecordCount().ToString("#,##0");
			}
			else
			{
				lblTotalTransactions.Visible = false;
			}
		}
	}
}
