//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmGroupAddressOverride : BaseForm
	{
		public frmGroupAddressOverride()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGroupAddressOverride InstancePtr
		{
			get
			{
				return (frmGroupAddressOverride)Sys.GetInstance(typeof(frmGroupAddressOverride));
			}
		}

		protected frmGroupAddressOverride _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rs = new clsDRWrapper();

		private void frmGroupAddressOverride_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmGroupAddressOverride_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGroupAddressOverride properties;
			//frmGroupAddressOverride.FillStyle	= 0;
			//frmGroupAddressOverride.ScaleWidth	= 5880;
			//frmGroupAddressOverride.ScaleHeight	= 3960;
			//frmGroupAddressOverride.LinkTopic	= "Form2";
			//frmGroupAddressOverride.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			rs.OpenRecordset("SELECT * FROM GroupAddressOverride");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				txtAddress.Text = FCConvert.ToString(rs.Get_Fields_String("Address"));
				txtAddress2.Text = FCConvert.ToString(rs.Get_Fields_String("Address2"));
				txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("City"));
				txtState.Text = FCConvert.ToString(rs.Get_Fields("State"));
				txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("Zip"));
				txtCountry.Text = FCConvert.ToString(rs.Get_Fields_String("Country"));
			}
		}

		private void frmGroupAddressOverride_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			rs.OpenRecordset("SELECT * FROM GroupAddressOverride");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.Edit();
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("Address", txtAddress.Text);
			rs.Set_Fields("Address2", txtAddress2.Text);
			rs.Set_Fields("City", txtCity.Text);
			rs.Set_Fields("State", txtState.Text);
			rs.Set_Fields("Zip", txtZip.Text);
			rs.Set_Fields("Country", txtCountry.Text);
			rs.Update();
			Close();
		}
	}
}
