//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmLostPlate : BaseForm
	{
		public frmLostPlate()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			//FC:FINAL:DDU:#2265 - allow only specific characters in textboxes, as in original
			txtMonthCharge.AllowOnlyNumericInput();
			txtYearCharge.AllowOnlyNumericInput();
			txtMStickerNumber.AllowOnlyNumericInput();
			txtYStickerNumber.AllowOnlyNumericInput();
			txtYearNoCharge.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57,65..90,97..122");
			txtMonthNoCharge.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57,65..90,97..122");
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmLostPlate InstancePtr
		{
			get
			{
				return (frmLostPlate)Sys.GetInstance(typeof(frmLostPlate));
			}
		}

		protected frmLostPlate _InstancePtr = null;
		//=========================================================
		// vbPorter upgrade warning: OldColor As int	OnWrite(Color)
		int OldColor;
		int NewBackColor = ColorTranslator.ToOle(Color.Yellow);
		// &HD2FFFF
		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			lblAmount.Text = "";
			fraInstructions.Visible = false;
			modGNBas.Statics.Response = "OK";
            //FC:FINAL:SBE - #4583 - do not dispose Modal dialog, in order to access it's controls later
            frmLostPlate.InstancePtr.AvoidModalDialogDispose = true;
			frmLostPlate.InstancePtr.Hide();
			if (MotorVehicle.Statics.RegistrationType != "LOST")
			{
				//MDIParent.InstancePtr.GRID.Focus();
			}
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsFees = new clsDRWrapper();
			// vbPorter upgrade warning: total As Decimal	OnWrite(int, Decimal)
			Decimal total;
			// vbPorter upgrade warning: curPlateFee As Decimal	OnWrite
			Decimal curPlateFee;
			// Dim rsClass As New clsDRWrapper
			if ((Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text)) > 0)
			{
				if (fecherFoundation.Strings.Trim(txtMStickerNumber.Text) == "" || Conversion.Val(txtMStickerNumber.Text) == 0)
				{
					MessageBox.Show("You must first enter a month sticker number before you may proceed.", "No Month Sticker Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if ((Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text)) > 0)
			{
				if (fecherFoundation.Strings.Trim(txtYStickerNumber.Text) == "" || Conversion.Val(txtYStickerNumber.Text) == 0)
				{
					MessageBox.Show("You must first enter a year sticker number before you may proceed.", "No Year Sticker Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}

            if (MotorVehicle.Statics.RegistrationType == "DUPSTK")
            {
                if ((Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text)) +
                    (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text)) == 0)
                {
                    MessageBox.Show("You must issue at least 1 sticker before you may proceed.", "No Stickers Issued", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
				}
            }


            total = 0;
			rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
			if (Conversion.Val(rsFees.Get_Fields_Decimal("LongTermReplacementPlateFee")) == 0)
			{
				curPlateFee = 0;
			}
			else
			{
				curPlateFee = FCConvert.ToDecimal(rsFees.Get_Fields_Decimal("LongTermReplacementPlateFee"));
			}
			if (MotorVehicle.Statics.PlateType == 1)
			{
				if (cmb1.Visible)
				{
					if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
					{
						if (cmb1.Text == "1")
						{
							total = curPlateFee;
						}
						else
						{
							total = curPlateFee * 2;
						}
					}
					else
					{
						if (cmb1.Text == "1")
						{
							total = FCConvert.ToDecimal(rsFees.Get_Fields_Decimal("ReplacementPlate"));
						}
						else
						{
							total = rsFees.Get_Fields_Decimal("ReplacementPlate") * 2;
						}
					}
				}
			}

			total += (((!String.IsNullOrEmpty(txtMonthCharge.Text) ? FCConvert.ToDecimal(txtMonthCharge.Text) : 0) + (!String.IsNullOrEmpty(txtYearCharge.Text) ? FCConvert.ToDecimal(txtYearCharge.Text) : 0)) * rsFees.Get_Fields_Decimal("Stickers"));
			lblAmount.Text = Strings.Format(total, "#,##0.00");
			if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
			{
				lblInstructions.Text = "";
			}
			else
			{
				if (MotorVehicle.Statics.RegistrationType == "LOST")
				{
					lblInstructions.Text = "Please be sure to fill out an MV-9 form";
				}
				else
				{
					lblInstructions.Text = "Please be sure to fill out an MV-14 form";
				}
			}
			//FC:FINAL:DDU:#2273 - hide process button
			cmdProcess.Visible = false;
			fraInstructions.Visible = true;
			fraInstructions.BringToFront();
			cmdOK.Focus();
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.Response = "QUIT";
			frmLostPlate.InstancePtr.Hide();
		}

		public void cmdQuit_Click()
		{
			cmdQuit_Click(new object(), new System.EventArgs());
		}

		private void frmLostPlate_Activated(object sender, System.EventArgs e)
		{
			object ans;
            modGNBas.Statics.Response = "";
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			lblInfo.Visible = false;
			if (MotorVehicle.Statics.RegistrationType == "DUPSTK")
			{
				frmLostPlate.InstancePtr.Text = "Duplicate Stickers";
				lblInfo.Visible = true;
				lblInfo.Text = "Please enter the sticker number(s) as well as the number of stickers needed";
				frmWait.InstancePtr.Unload();
			}
			if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
			{
				cmb1.Text = "1";
				txtMonthCharge.Text = "";
				txtYearCharge.Text = "";
				txtMonthNoCharge.Text = "";
				txtYearNoCharge.Text = "";
				cmdProcess_Click();
			}
			OldColor = ColorTranslator.ToOle(txtMStickerNumber.BackColor);
		}

		private void frmLostPlate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmLostPlate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLostPlate properties;
			//frmLostPlate.ScaleWidth	= 5880;
			//frmLostPlate.ScaleHeight	= 4095;
			//frmLostPlate.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			//FC:FINAL:DDU:#2277 - clear texts from controls
			txtYStickerNumber.Clear();
			txtMStickerNumber.Clear();
            //FC:FINAL:SBE - #3964 - restore button visibility
            cmdProcess.Visible = true;
        }

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				cmdQuit_Click();
			}
		}

		private void opt1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) || !MotorVehicle.Statics.FixedMonth)
			{
				txtMonthCharge.Text = "1";
				txtMStickerNumber.Text = "";
			}
			else
			{
				txtMonthCharge.Text = "";
				txtMStickerNumber.Text = "";
			}
			txtYearCharge.Text = "1";
			txtYStickerNumber.Text = "";
		}

		private void opt2_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) || !MotorVehicle.Statics.FixedMonth)
			{
				txtMonthCharge.Text = "2";
				txtMStickerNumber.Text = "";
			}
			else
			{
				txtMonthCharge.Text = "";
				txtMStickerNumber.Text = "";
			}
			txtYearCharge.Text = "2";
			txtYStickerNumber.Text = "";
		}

		private void txtMonthCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) > 2)
			{
				MessageBox.Show("You may use a maximum of 2 Month Stickers.", "Too Many Stickers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}
		}

		private void txtMonthNoCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) > 2)
			{
				MessageBox.Show("You may use a maximum of 2 Month Stickers.", "Too Many Stickers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}
		}

		private void txtMStickerNumber_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtMonthCharge.Text) + Conversion.Val(txtMonthNoCharge.Text) == 0)
			{
				//FC:FINAL:DDU:#i1932 - send user to next tabindex control
				//Support.SendKeys("{TAB}", false);
				txtYearCharge.Focus();
				return;
			}
			OldColor = ColorTranslator.ToOle(txtMStickerNumber.BackColor);
			txtMStickerNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtMStickerNumber_Leave(object sender, System.EventArgs e)
		{
			txtMStickerNumber.BackColor = ColorTranslator.FromOle(OldColor);
		}

		private void txtMStickerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Back)
			{
				// backspace
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
				// number
			}
			else if (KeyAscii == Keys.C)
			{
				txtYStickerNumber.Text = "";
				KeyAscii = (Keys)0;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMStickerNumber_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Shift == 3 && KeyCode == Keys.F9)
			{
				txtMStickerNumber.Text = "";
				MotorVehicle.Statics.F9 = true;
				frmInventory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			}
		}

		public void txtMStickerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			MotorVehicle.Statics.MonthStickers = FCConvert.ToInt32(Math.Round(Conversion.Val(txtMStickerNumber.Text)));
			if (MotorVehicle.Statics.MonthStickers != 0)
			{
				if (MotorVehicle.CheckNumbers(this, "MS", MotorVehicle.Statics.MonthStickers) == false)
				{
					// Bp = Beep(1000, 250)
					MessageBox.Show("The stickers are not in Inventory.", "Stickers Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
				}
			}
		}

		private void txtYearCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) > 2)
			{
				MessageBox.Show("You may use a maximum of 2 Year Stickers.", "Too Many Stickers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}
		}

		private void txtYearNoCharge_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) > 2)
			{
				MessageBox.Show("You may use a maximum of 2 Year Stickers.", "Too Many Stickers", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
			}
		}

		private void txtYStickerNumber_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtYearCharge.Text) + Conversion.Val(txtYearNoCharge.Text) == 0)
			{
				//FC:FINAL:DDU:#i1932 - send user to next tabindex control
				//Support.SendKeys("{TAB}", false);
				cmdProcess.Focus();
				return;
			}
			OldColor = ColorTranslator.ToOle(txtYStickerNumber.BackColor);
			txtYStickerNumber.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtYStickerNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Back)
			{
				// backspace
			}
			else if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
			{
				// number
			}
			else if (KeyAscii == Keys.C)
			{
				txtYStickerNumber.Text = "";
				KeyAscii = (Keys)0;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtYStickerNumber_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Shift == 3 && KeyCode == Keys.F9)
			{
				txtYStickerNumber.Text = "";
				MotorVehicle.Statics.F9 = true;
				frmInventory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			}
		}

		private void txtYStickerNumber_Leave(object sender, System.EventArgs e)
		{
			txtYStickerNumber.BackColor = ColorTranslator.FromOle(OldColor);
		}

		public void txtYStickerNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			MotorVehicle.Statics.YearStickers = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYStickerNumber.Text)));
			if (MotorVehicle.Statics.YearStickers != 0)
			{
				if (MotorVehicle.CheckNumbers(this, "YS", 0, MotorVehicle.Statics.YearStickers) == false)
				{
					// Bp = Beep(1000, 250)
					MessageBox.Show("The stickers are not in Inventory.", "Stickers Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblAmount.ForeColor = Color.Blue;
			lblOwed.ForeColor = Color.Blue;
			lblOwed.Font = FCUtils.FontChangeSize(lblOwed.Font, 12);
			lblAmount.Font = FCUtils.FontChangeSize(lblAmount.Font, 12);
		}

		public void cmb1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmb1.Text == "1")
			{
				opt1_CheckedChanged(sender, e);
			}
			else if (cmb1.Text == "2")
			{
				opt2_CheckedChanged(sender, e);
			}
		}
	}
}
