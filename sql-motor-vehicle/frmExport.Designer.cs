//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmExport.
	/// </summary>
	partial class frmExport
	{
		public fecherFoundation.FCComboBox cmbExpDate;
		public fecherFoundation.FCLabel lblExpDate;
		public fecherFoundation.FCComboBox cmbExciseTaxDateRange;
		public fecherFoundation.FCComboBox cmbVehicles;
		public fecherFoundation.FCLabel lblVehicles;
		public fecherFoundation.FCFrame fraDates;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileProcess;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbExpDate = new fecherFoundation.FCComboBox();
            this.lblExpDate = new fecherFoundation.FCLabel();
            this.cmbExciseTaxDateRange = new fecherFoundation.FCComboBox();
            this.cmbVehicles = new fecherFoundation.FCComboBox();
            this.lblVehicles = new fecherFoundation.FCLabel();
            this.fraDates = new fecherFoundation.FCFrame();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.txtStartDate = new Global.T2KDateBox();
            this.txtEndDate = new Global.T2KDateBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileProcess = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDates)).BeginInit();
            this.fraDates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 416);
            this.BottomPanel.Size = new System.Drawing.Size(451, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbExpDate);
            this.ClientArea.Controls.Add(this.lblExpDate);
            this.ClientArea.Controls.Add(this.fraDates);
            this.ClientArea.Controls.Add(this.cmbVehicles);
            this.ClientArea.Controls.Add(this.lblVehicles);
            this.ClientArea.Size = new System.Drawing.Size(451, 356);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(451, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(142, 30);
            this.HeaderText.Text = "Data Export";
            // 
            // cmbExpDate
            // 
            this.cmbExpDate.AutoSize = false;
            this.cmbExpDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbExpDate.FormattingEnabled = true;
            this.cmbExpDate.Items.AddRange(new object[] {
			"Plate",
			"Exp Date"});
            this.cmbExpDate.Location = new System.Drawing.Point(212, 90);
            this.cmbExpDate.Name = "cmbExpDate";
            this.cmbExpDate.Size = new System.Drawing.Size(211, 40);
            this.cmbExpDate.TabIndex = 0;
            this.cmbExpDate.Text = "Plate";
            // 
            // lblExpDate
            // 
            this.lblExpDate.AutoSize = true;
            this.lblExpDate.Location = new System.Drawing.Point(30, 104);
            this.lblExpDate.Name = "lblExpDate";
            this.lblExpDate.Size = new System.Drawing.Size(72, 15);
            this.lblExpDate.TabIndex = 1;
            this.lblExpDate.Text = "ORDER BY";
            // 
            // cmbExciseTaxDateRange
            // 
            this.cmbExciseTaxDateRange.AutoSize = false;
            this.cmbExciseTaxDateRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbExciseTaxDateRange.FormattingEnabled = true;
            this.cmbExciseTaxDateRange.Items.AddRange(new object[] {
			"All",
			"Expiration Date Range",
            "Effective Date Range",
            "Excise Tax Date Range"});
            this.cmbExciseTaxDateRange.Location = new System.Drawing.Point(20, 30);
            this.cmbExciseTaxDateRange.Name = "cmbExciseTaxDateRange";
            this.cmbExciseTaxDateRange.Size = new System.Drawing.Size(338, 40);
            this.cmbExciseTaxDateRange.TabIndex = 5;
            this.cmbExciseTaxDateRange.Text = "All";
            this.cmbExciseTaxDateRange.SelectedIndexChanged += new System.EventHandler(this.cmbExciseTaxDateRange_SelectedIndexChanged);
            // 
            // cmbVehicles
            // 
            this.cmbVehicles.AutoSize = false;
            this.cmbVehicles.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbVehicles.FormattingEnabled = true;
            this.cmbVehicles.Items.AddRange(new object[] {
            "Vehicle Records",
            "Transactions"});
            this.cmbVehicles.Location = new System.Drawing.Point(212, 30);
            this.cmbVehicles.Name = "cmbVehicles";
            this.cmbVehicles.Size = new System.Drawing.Size(211, 40);
            this.cmbVehicles.TabIndex = 4;
			this.cmbVehicles.Text = "Vehicle Records";
			// 
			// lblVehicles
			// 
			this.lblVehicles.AutoSize = true;
            this.lblVehicles.Location = new System.Drawing.Point(30, 44);
            this.lblVehicles.Name = "lblVehicles";
            this.lblVehicles.Size = new System.Drawing.Size(130, 15);
            this.lblVehicles.TabIndex = 5;
            this.lblVehicles.Text = "INFORMATION TYPE";
            // 
            // fraDates
            // 
            this.fraDates.AppearanceKey = "groupBoxLeftBorder";
            this.fraDates.Controls.Add(this.fraDateRange);
            this.fraDates.Controls.Add(this.cmbExciseTaxDateRange);
            this.fraDates.Location = new System.Drawing.Point(30, 150);
            this.fraDates.Name = "fraDates";
            this.fraDates.Size = new System.Drawing.Size(358, 198);
            this.fraDates.TabIndex = 3;
            this.fraDates.Text = "Dates To Report";
            // 
            // fraDateRange
            // 
            this.fraDateRange.Controls.Add(this.txtStartDate);
            this.fraDateRange.Controls.Add(this.txtEndDate);
            this.fraDateRange.Controls.Add(this.lblTo);
            this.fraDateRange.Enabled = false;
            this.fraDateRange.Location = new System.Drawing.Point(20, 90);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(338, 90);
            this.fraDateRange.TabIndex = 4;
            this.fraDateRange.Text = "Select Date Range";
            // 
            // txtStartDate
            // 
            this.txtStartDate.Enabled = false;
            this.txtStartDate.Location = new System.Drawing.Point(20, 30);
            this.txtStartDate.Mask = "##/##/####";
            this.txtStartDate.MaxLength = 10;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate.TabIndex = 5;
            this.txtStartDate.Text = "  /  /";
            // 
            // txtEndDate
            // 
            this.txtEndDate.Enabled = false;
            this.txtEndDate.Location = new System.Drawing.Point(205, 30);
            this.txtEndDate.Mask = "##/##/####";
            this.txtEndDate.MaxLength = 10;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate.TabIndex = 6;
            this.txtEndDate.Text = "  /  /";
            // 
            // lblTo
            // 
            this.lblTo.Enabled = false;
            this.lblTo.Location = new System.Drawing.Point(153, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 16);
            this.lblTo.TabIndex = 7;
            this.lblTo.Text = "TO";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileProcess,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileProcess
            // 
            this.mnuFileProcess.Index = 0;
            this.mnuFileProcess.Name = "mnuFileProcess";
            this.mnuFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileProcess.Text = "Process";
            this.mnuFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(173, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(102, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // frmExport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(451, 524);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmExport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Data Export";
            this.Load += new System.EventHandler(this.frmExport_Load);
            this.Activated += new System.EventHandler(this.frmExport_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExport_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDates)).EndInit();
            this.fraDates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}