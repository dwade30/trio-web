﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptFleetReminderList.
	/// </summary>
	partial class rptFleetReminderList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFleetReminderList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExpirationMonth = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUnit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFleetNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFleetOwner = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFleetName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBadFees = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalAgentFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalLocalSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalRegistrationFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalInitialFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalStateSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalSpecialtyFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFleetName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBadFees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAgentFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocalSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegistrationFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInitialFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalStateSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalSpecialtyFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldClassPlate,
				this.fldVIN,
				this.fldYear,
				this.fldMake,
				this.fldModel,
				this.fldUnit,
				this.fldLocal,
				this.fldState,
				this.fldTotal,
				this.lblBadFees,
				this.fldExpires
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.lblExpirationMonth,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.lblUnit,
				this.Line2,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.lblFleetNumber,
				this.lblFleetOwner
			});
			this.PageHeader.Height = 0.7291667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Binder,
				this.fldFleetName
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.2604167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.fldTotalLocal,
				this.fldTotalState,
				this.fldTotalTotal,
				this.Label25,
				this.Label26,
				this.Label27,
				this.fldTotalAgentFee,
				this.Label28,
				this.fldTotalExcise,
				this.Line4,
				this.Label29,
				this.fldTotalLocalSummary,
				this.Label30,
				this.Label31,
				this.fldTotalRegistrationFee,
				this.Label32,
				this.fldTotalInitialFee,
				this.Line5,
				this.Label33,
				this.fldTotalStateSummary,
				this.Label34,
				this.fldTotalSpecialtyFee,
				this.Label35,
				this.fldGrandTotal,
				this.Label36
			});
			this.GroupFooter1.Height = 2.041667F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Fleet Reminder List";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.15625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label8.Text = "Label8";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.6875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.6875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label11.Text = "Label11";
			this.Label11.Top = 0F;
			this.Label11.Width = 1.3125F;
			// 
			// lblExpirationMonth
			// 
			this.lblExpirationMonth.Height = 0.1875F;
			this.lblExpirationMonth.HyperLink = null;
			this.lblExpirationMonth.Left = 1.5F;
			this.lblExpirationMonth.Name = "lblExpirationMonth";
			this.lblExpirationMonth.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblExpirationMonth.Text = "Fleet Registration Listing";
			this.lblExpirationMonth.Top = 0.21875F;
			this.lblExpirationMonth.Width = 5.15625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.3125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label15.Text = "Cl \\ Plate";
			this.Label15.Top = 0.53125F;
			this.Label15.Width = 0.75F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label16.Text = "VIN";
			this.Label16.Top = 0.53125F;
			this.Label16.Width = 1.1875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label17.Text = "Year";
			this.Label17.Top = 0.53125F;
			this.Label17.Width = 0.5F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.90625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label18.Text = "Make";
			this.Label18.Top = 0.53125F;
			this.Label18.Width = 0.375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.34375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label19.Text = "Model";
			this.Label19.Top = 0.53125F;
			this.Label19.Width = 0.5F;
			// 
			// lblUnit
			// 
			this.lblUnit.Height = 0.1875F;
			this.lblUnit.HyperLink = null;
			this.lblUnit.Left = 3.90625F;
			this.lblUnit.Name = "lblUnit";
			this.lblUnit.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblUnit.Text = "Unit";
			this.lblUnit.Top = 0.53125F;
			this.lblUnit.Width = 0.71875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.03125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.71875F;
			this.Line2.Width = 7.9375F;
			this.Line2.X1 = 0.03125F;
			this.Line2.X2 = 7.96875F;
			this.Line2.Y1 = 0.71875F;
			this.Line2.Y2 = 0.71875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 5.4375F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label21.Text = "Local";
			this.Label21.Top = 0.53125F;
			this.Label21.Width = 0.75F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 6.25F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label22.Text = "State";
			this.Label22.Top = 0.53125F;
			this.Label22.Width = 0.75F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 7.0625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label23.Text = "Total Fee";
			this.Label23.Top = 0.53125F;
			this.Label23.Width = 0.78125F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 4.6875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label24.Text = "Expires";
			this.Label24.Top = 0.53125F;
			this.Label24.Width = 0.71875F;
			// 
			// lblFleetNumber
			// 
			this.lblFleetNumber.Height = 0.1875F;
			this.lblFleetNumber.HyperLink = null;
			this.lblFleetNumber.Left = 1.5F;
			this.lblFleetNumber.Name = "lblFleetNumber";
			this.lblFleetNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblFleetNumber.Text = "Fleet Registration Listing";
			this.lblFleetNumber.Top = 0.09375F;
			this.lblFleetNumber.Visible = false;
			this.lblFleetNumber.Width = 5.15625F;
			// 
			// lblFleetOwner
			// 
			this.lblFleetOwner.Height = 0.1875F;
			this.lblFleetOwner.HyperLink = null;
			this.lblFleetOwner.Left = 1.53125F;
			this.lblFleetOwner.Name = "lblFleetOwner";
			this.lblFleetOwner.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblFleetOwner.Text = "Fleet Registration Listing";
			this.lblFleetOwner.Top = 0.15625F;
			this.lblFleetOwner.Visible = false;
			this.lblFleetOwner.Width = 5.15625F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 5.59375F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.71875F;
			// 
			// fldFleetName
			// 
			this.fldFleetName.Height = 0.1875F;
			this.fldFleetName.Left = 0F;
			this.fldFleetName.Name = "fldFleetName";
			this.fldFleetName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldFleetName.Text = null;
			this.fldFleetName.Top = 0.0625F;
			this.fldFleetName.Width = 4.5F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.19F;
			this.fldClassPlate.Left = 0.3125F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldClassPlate.Text = null;
			this.fldClassPlate.Top = 0F;
			this.fldClassPlate.Width = 0.75F;
			// 
			// fldVIN
			// 
			this.fldVIN.Height = 0.19F;
			this.fldVIN.Left = 1.125F;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldVIN.Text = null;
			this.fldVIN.Top = 0F;
			this.fldVIN.Width = 1.1875F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.19F;
			this.fldYear.Left = 2.34375F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.5F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.19F;
			this.fldMake.Left = 2.875F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 0F;
			this.fldMake.Width = 0.375F;
			// 
			// fldModel
			// 
			this.fldModel.Height = 0.19F;
			this.fldModel.Left = 3.3125F;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldModel.Text = "Field1";
			this.fldModel.Top = 0F;
			this.fldModel.Width = 0.5F;
			// 
			// fldUnit
			// 
			this.fldUnit.Height = 0.19F;
			this.fldUnit.Left = 3.875F;
			this.fldUnit.Name = "fldUnit";
			this.fldUnit.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldUnit.Text = "Field1";
			this.fldUnit.Top = 0F;
			this.fldUnit.Width = 0.75F;
			// 
			// fldLocal
			// 
			this.fldLocal.Height = 0.19F;
			this.fldLocal.Left = 5.4375F;
			this.fldLocal.Name = "fldLocal";
			this.fldLocal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldLocal.Text = "Field1";
			this.fldLocal.Top = 0F;
			this.fldLocal.Width = 0.75F;
			// 
			// fldState
			// 
			this.fldState.Height = 0.19F;
			this.fldState.Left = 6.1875F;
			this.fldState.Name = "fldState";
			this.fldState.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldState.Text = "Field1";
			this.fldState.Top = 0F;
			this.fldState.Width = 0.75F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.19F;
			this.fldTotal.Left = 7.03125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.78125F;
			// 
			// lblBadFees
			// 
			this.lblBadFees.Height = 0.19F;
			this.lblBadFees.HyperLink = null;
			this.lblBadFees.Left = 7.84375F;
			this.lblBadFees.Name = "lblBadFees";
			this.lblBadFees.Style = "font-size: 18pt";
			this.lblBadFees.Text = "*";
			this.lblBadFees.Top = 0F;
			this.lblBadFees.Visible = false;
			this.lblBadFees.Width = 0.15625F;
			// 
			// fldExpires
			// 
			this.fldExpires.Height = 0.19F;
			this.fldExpires.Left = 4.6875F;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldExpires.Text = "Field1";
			this.fldExpires.Top = 0F;
			this.fldExpires.Width = 0.71875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.46875F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 3.53125F;
			this.Line3.X1 = 4.46875F;
			this.Line3.X2 = 8F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// fldTotalLocal
			// 
			this.fldTotalLocal.Height = 0.19F;
			this.fldTotalLocal.Left = 5.375F;
			this.fldTotalLocal.Name = "fldTotalLocal";
			this.fldTotalLocal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalLocal.Text = "Field1";
			this.fldTotalLocal.Top = 0.0625F;
			this.fldTotalLocal.Width = 0.75F;
			// 
			// fldTotalState
			// 
			this.fldTotalState.Height = 0.19F;
			this.fldTotalState.Left = 6.1875F;
			this.fldTotalState.Name = "fldTotalState";
			this.fldTotalState.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalState.Text = "Field1";
			this.fldTotalState.Top = 0.0625F;
			this.fldTotalState.Width = 0.75F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.19F;
			this.fldTotalTotal.Left = 7F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalTotal.Text = "Field1";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 0.78125F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 4.53125F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label25.Text = "Totals";
			this.Label25.Top = 0.0625F;
			this.Label25.Width = 0.75F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 1.59375F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label26.Text = "LOCAL FEES";
			this.Label26.Top = 0.4375F;
			this.Label26.Width = 1.5625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.9375F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label27.Text = "AGENT FEE:";
			this.Label27.Top = 0.6875F;
			this.Label27.Width = 1.0625F;
			// 
			// fldTotalAgentFee
			// 
			this.fldTotalAgentFee.Height = 0.19F;
			this.fldTotalAgentFee.Left = 2.6875F;
			this.fldTotalAgentFee.Name = "fldTotalAgentFee";
			this.fldTotalAgentFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalAgentFee.Text = "Field1";
			this.fldTotalAgentFee.Top = 0.6875F;
			this.fldTotalAgentFee.Width = 1.0625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.9375F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label28.Text = "EXCISE TAX:";
			this.Label28.Top = 0.84375F;
			this.Label28.Width = 1.0625F;
			// 
			// fldTotalExcise
			// 
			this.fldTotalExcise.Height = 0.19F;
			this.fldTotalExcise.Left = 2.6875F;
			this.fldTotalExcise.Name = "fldTotalExcise";
			this.fldTotalExcise.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalExcise.Text = "Field1";
			this.fldTotalExcise.Top = 0.84375F;
			this.fldTotalExcise.Width = 1.0625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.9375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.03125F;
			this.Line4.Width = 2.8125F;
			this.Line4.X1 = 0.9375F;
			this.Line4.X2 = 3.75F;
			this.Line4.Y1 = 1.03125F;
			this.Line4.Y2 = 1.03125F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.19F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.9375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label29.Text = "TOTAL LOCAL:";
			this.Label29.Top = 1.0625F;
			this.Label29.Width = 1.0625F;
			// 
			// fldTotalLocalSummary
			// 
			this.fldTotalLocalSummary.Height = 0.19F;
			this.fldTotalLocalSummary.Left = 2.6875F;
			this.fldTotalLocalSummary.Name = "fldTotalLocalSummary";
			this.fldTotalLocalSummary.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalLocalSummary.Text = "Field1";
			this.fldTotalLocalSummary.Top = 1.0625F;
			this.fldTotalLocalSummary.Width = 1.0625F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 4.9375F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label30.Text = "STATE FEES";
			this.Label30.Top = 0.4375F;
			this.Label30.Width = 1.5625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.19F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 4.28125F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label31.Text = "REGISTRATION FEE:";
			this.Label31.Top = 0.6875F;
			this.Label31.Width = 1.6875F;
			// 
			// fldTotalRegistrationFee
			// 
			this.fldTotalRegistrationFee.Height = 0.19F;
			this.fldTotalRegistrationFee.Left = 6.03125F;
			this.fldTotalRegistrationFee.Name = "fldTotalRegistrationFee";
			this.fldTotalRegistrationFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalRegistrationFee.Text = "Field1";
			this.fldTotalRegistrationFee.Top = 0.6875F;
			this.fldTotalRegistrationFee.Width = 1.0625F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 4.28125F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label32.Text = "VANITY PLATE FEE:";
			this.Label32.Top = 0.84375F;
			this.Label32.Width = 1.6875F;
			// 
			// fldTotalInitialFee
			// 
			this.fldTotalInitialFee.Height = 0.19F;
			this.fldTotalInitialFee.Left = 6.03125F;
			this.fldTotalInitialFee.Name = "fldTotalInitialFee";
			this.fldTotalInitialFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalInitialFee.Text = "Field1";
			this.fldTotalInitialFee.Top = 0.84375F;
			this.fldTotalInitialFee.Width = 1.0625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 4.28125F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 1.1875F;
			this.Line5.Width = 2.8125F;
			this.Line5.X1 = 4.28125F;
			this.Line5.X2 = 7.09375F;
			this.Line5.Y1 = 1.1875F;
			this.Line5.Y2 = 1.1875F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 4.28125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label33.Text = "TOTAL STATE:";
			this.Label33.Top = 1.21875F;
			this.Label33.Width = 1.0625F;
			// 
			// fldTotalStateSummary
			// 
			this.fldTotalStateSummary.Height = 0.19F;
			this.fldTotalStateSummary.Left = 6.03125F;
			this.fldTotalStateSummary.Name = "fldTotalStateSummary";
			this.fldTotalStateSummary.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalStateSummary.Text = "Field1";
			this.fldTotalStateSummary.Top = 1.21875F;
			this.fldTotalStateSummary.Width = 1.0625F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 4.28125F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label34.Text = "SPECIALTY PLATE FEE:";
			this.Label34.Top = 1F;
			this.Label34.Width = 1.6875F;
			// 
			// fldTotalSpecialtyFee
			// 
			this.fldTotalSpecialtyFee.Height = 0.19F;
			this.fldTotalSpecialtyFee.Left = 6.03125F;
			this.fldTotalSpecialtyFee.Name = "fldTotalSpecialtyFee";
			this.fldTotalSpecialtyFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalSpecialtyFee.Text = "Field1";
			this.fldTotalSpecialtyFee.Top = 1F;
			this.fldTotalSpecialtyFee.Width = 1.0625F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.19F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 2.3125F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label35.Text = "GRAND TOTAL:";
			this.Label35.Top = 1.5625F;
			this.Label35.Width = 1.0625F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.19F;
			this.fldGrandTotal.Left = 4.0625F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGrandTotal.Text = "Field1";
			this.fldGrandTotal.Top = 1.5625F;
			this.fldGrandTotal.Width = 1.0625F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.40625F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label36.Text = "WARNING - Fees may not be accurate due to missing information";
			this.Label36.Top = 1.84375F;
			this.Label36.Width = 6.71875F;
			// 
			// rptFleetReminderList
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFleetName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBadFees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAgentFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocalSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegistrationFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInitialFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalStateSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalSpecialtyFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBadFees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExpirationMonth;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUnit;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFleetNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFleetOwner;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFleetName;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAgentFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalExcise;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalLocalSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegistrationFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInitialFee;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalStateSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalSpecialtyFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
	}
}
