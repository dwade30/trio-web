﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptDiskVerificationSummary.
	/// </summary>
	partial class rptDiskVerificationSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDiskVerificationSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtCL = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTransfer = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrections = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFeeT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnitsT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCL)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrections)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitsT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCL,
				this.txtFee,
				this.txtUnits,
				this.txtTotals,
				this.lblTransfer,
				this.lblCorrections
			});
			this.Detail.Height = 0.21875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotal,
				this.txtFeeT,
				this.txtUnitsT,
				this.txtTotalT,
				this.Line2
			});
			this.ReportFooter.Height = 0.4166667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label34,
				this.Label35,
				this.Label36,
				this.Label37,
				this.Line3
			});
			this.GroupHeader1.Height = 0.3020833F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.40625F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label34.Text = "CLASS";
			this.Label34.Top = 0.0625F;
			this.Label34.Width = 0.5625F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 1.59375F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.Label35.Text = "FEE";
			this.Label35.Top = 0.0625F;
			this.Label35.Width = 0.375F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 2.34375F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label36.Text = "UNITS";
			this.Label36.Top = 0.0625F;
			this.Label36.Width = 0.625F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 4.15625F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label37.Text = "TOTAL";
			this.Label37.Top = 0.0625F;
			this.Label37.Width = 0.6875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.25F;
			this.Line3.Width = 5.90625F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 5.90625F;
			this.Line3.Y1 = 0.25F;
			this.Line3.Y2 = 0.25F;
			// 
			// txtCL
			// 
			this.txtCL.Height = 0.1875F;
			this.txtCL.Left = 0.5625F;
			this.txtCL.Name = "txtCL";
			this.txtCL.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtCL.Text = "Field1";
			this.txtCL.Top = 0F;
			this.txtCL.Width = 0.5F;
			// 
			// txtFee
			// 
			this.txtFee.Height = 0.1875F;
			this.txtFee.Left = 1.375F;
			this.txtFee.Name = "txtFee";
			this.txtFee.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.txtFee.Text = "Field1";
			this.txtFee.Top = 0F;
			this.txtFee.Width = 0.6875F;
			// 
			// txtUnits
			// 
			this.txtUnits.Height = 0.1875F;
			this.txtUnits.Left = 2.5F;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtUnits.Text = "Field1";
			this.txtUnits.Top = 0F;
			this.txtUnits.Width = 0.625F;
			// 
			// txtTotals
			// 
			this.txtTotals.Height = 0.1875F;
			this.txtTotals.Left = 3.75F;
			this.txtTotals.Name = "txtTotals";
			this.txtTotals.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.txtTotals.Text = "Field1";
			this.txtTotals.Top = 0F;
			this.txtTotals.Width = 0.9375F;
			// 
			// lblTransfer
			// 
			this.lblTransfer.Height = 0.1875F;
			this.lblTransfer.HyperLink = null;
			this.lblTransfer.Left = 0.5F;
			this.lblTransfer.Name = "lblTransfer";
			this.lblTransfer.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblTransfer.Text = "Transfers";
			this.lblTransfer.Top = 0F;
			this.lblTransfer.Width = 0.625F;
			// 
			// lblCorrections
			// 
			this.lblCorrections.Height = 0.1875F;
			this.lblCorrections.HyperLink = null;
			this.lblCorrections.Left = 0.25F;
			this.lblCorrections.Name = "lblCorrections";
			this.lblCorrections.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblCorrections.Text = "Corrections";
			this.lblCorrections.Top = 0F;
			this.lblCorrections.Width = 0.875F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 0.125F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblTotal.Text = "TOTALS";
			this.lblTotal.Top = 0.1875F;
			this.lblTotal.Width = 0.625F;
			// 
			// txtFeeT
			// 
			this.txtFeeT.Height = 0.1875F;
			this.txtFeeT.Left = 1.25F;
			this.txtFeeT.Name = "txtFeeT";
			this.txtFeeT.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.txtFeeT.Text = "Field1";
			this.txtFeeT.Top = 0.1875F;
			this.txtFeeT.Visible = false;
			this.txtFeeT.Width = 0.8125F;
			// 
			// txtUnitsT
			// 
			this.txtUnitsT.Height = 0.1875F;
			this.txtUnitsT.Left = 2.5F;
			this.txtUnitsT.Name = "txtUnitsT";
			this.txtUnitsT.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtUnitsT.Text = "Field1";
			this.txtUnitsT.Top = 0.1875F;
			this.txtUnitsT.Visible = false;
			this.txtUnitsT.Width = 0.875F;
			// 
			// txtTotalT
			// 
			this.txtTotalT.Height = 0.1875F;
			this.txtTotalT.Left = 3.75F;
			this.txtTotalT.Name = "txtTotalT";
			this.txtTotalT.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.txtTotalT.Text = "Field1";
			this.txtTotalT.Top = 0.1875F;
			this.txtTotalT.Visible = false;
			this.txtTotalT.Width = 0.9375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.03125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.0625F;
			this.Line2.Width = 5.9375F;
			this.Line2.X1 = 0.03125F;
			this.Line2.X2 = 5.96875F;
			this.Line2.Y1 = 0.0625F;
			this.Line2.Y2 = 0.0625F;
			// 
			// rptDiskVerificationSummary
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCL)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrections)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFeeT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitsT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCL;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrections;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFeeT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnitsT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalT;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
