﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWMV0000
{
	public class clsGeoCode
	{
		//=========================================================
		public string Code = "";
		public string Desc = "";
		public string GType = "";
		// T - Organized Town, U - Unorganized Town, A - LTT Agent
		public string County = "";
	}
}
