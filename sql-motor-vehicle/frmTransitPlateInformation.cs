//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmTransitPlateInformation : BaseForm
	{
		public frmTransitPlateInformation()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTransitPlateInformation InstancePtr
		{
			get
			{
				return (frmTransitPlateInformation)Sys.GetInstance(typeof(frmTransitPlateInformation));
			}
		}

		protected frmTransitPlateInformation _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int PlateCol;
		int EffectiveDateCol;
		int VINCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int NameCol;

		private void frmTransitPlateInformation_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmTransitPlateInformation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransitPlateInformation properties;
			//frmTransitPlateInformation.FillStyle	= 0;
			//frmTransitPlateInformation.ScaleWidth	= 9045;
			//frmTransitPlateInformation.ScaleHeight	= 7170;
			//frmTransitPlateInformation.LinkTopic	= "Form2";
			//frmTransitPlateInformation.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsInfo = new clsDRWrapper();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			PlateCol = 0;
			EffectiveDateCol = 1;
			VINCol = 2;
			YearCol = 3;
			MakeCol = 4;
			ModelCol = 5;
			NameCol = 6;
			vsInfo.TextMatrix(0, PlateCol, "Plate");
			vsInfo.TextMatrix(0, EffectiveDateCol, "Effective Date");
			vsInfo.TextMatrix(0, VINCol, "VIN");
			vsInfo.TextMatrix(0, YearCol, "Year");
			vsInfo.TextMatrix(0, MakeCol, "Make");
			vsInfo.TextMatrix(0, ModelCol, "Model");
			vsInfo.TextMatrix(0, NameCol, "Registrant");
			vsInfo.ColWidth(PlateCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.09));
			vsInfo.ColWidth(EffectiveDateCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.15));
			vsInfo.ColWidth(VINCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.23));
			vsInfo.ColWidth(YearCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.08));
			vsInfo.ColWidth(MakeCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.08));
			vsInfo.ColWidth(ModelCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.12));
			vsInfo.ColWidth(NameCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.1));
			vsInfo.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(EffectiveDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(VINCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(MakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(NameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			rsInfo.OpenRecordset("SELECT * FROM TransitPlates ORDER BY Plate");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsInfo.Rows += 1;
					vsInfo.TextMatrix(vsInfo.Rows - 1, PlateCol, FCConvert.ToString(rsInfo.Get_Fields_String("Plate")));
					vsInfo.TextMatrix(vsInfo.Rows - 1, EffectiveDateCol, Strings.Format(rsInfo.Get_Fields_DateTime("EffectiveDate"), "MM/dd/yyyy"));
					vsInfo.TextMatrix(vsInfo.Rows - 1, VINCol, FCConvert.ToString(rsInfo.Get_Fields_String("VIN")));
					vsInfo.TextMatrix(vsInfo.Rows - 1, YearCol, FCConvert.ToString(rsInfo.Get_Fields("Year")));
					vsInfo.TextMatrix(vsInfo.Rows - 1, MakeCol, FCConvert.ToString(rsInfo.Get_Fields_String("Make")));
					vsInfo.TextMatrix(vsInfo.Rows - 1, ModelCol, FCConvert.ToString(rsInfo.Get_Fields_String("Model")));
					vsInfo.TextMatrix(vsInfo.Rows - 1, NameCol, MotorVehicle.GetPartyNameMiddleInitial(rsInfo.Get_Fields_Int32("PartyID")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void frmTransitPlateInformation_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTransitPlateInformation_Resize(object sender, System.EventArgs e)
		{
			vsInfo.ColWidth(PlateCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.09));
			vsInfo.ColWidth(EffectiveDateCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.15));
			vsInfo.ColWidth(VINCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.23));
			vsInfo.ColWidth(YearCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.08));
			vsInfo.ColWidth(MakeCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.08));
			vsInfo.ColWidth(ModelCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.12));
			vsInfo.ColWidth(NameCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.1));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
