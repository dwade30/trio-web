﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptGiftCertVoucherIssued.
	/// </summary>
	partial class srptGiftCertVoucherIssued
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptGiftCertVoucherIssued));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtCustomer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCertificate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOPID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblIssuedOrRedeemed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCLTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertificate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIssuedOrRedeemed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCustomer,
            this.txtCertificate,
            this.txtAmount,
            this.txtDT,
            this.txtOPID});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtCustomer
			// 
			this.txtCustomer.CanShrink = true;
			this.txtCustomer.Height = 0.1875F;
			this.txtCustomer.Left = 2.75F;
			this.txtCustomer.Name = "txtCustomer";
			this.txtCustomer.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtCustomer.Text = null;
			this.txtCustomer.Top = 0F;
			this.txtCustomer.Width = 2.5F;
			// 
			// txtCertificate
			// 
			this.txtCertificate.CanShrink = true;
			this.txtCertificate.Height = 0.1875F;
			this.txtCertificate.Left = 0.15625F;
			this.txtCertificate.Name = "txtCertificate";
			this.txtCertificate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtCertificate.Text = null;
			this.txtCertificate.Top = 0F;
			this.txtCertificate.Width = 0.875F;
			// 
			// txtAmount
			// 
			this.txtAmount.CanShrink = true;
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 1.375F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.875F;
			// 
			// txtDT
			// 
			this.txtDT.CanShrink = true;
			this.txtDT.Height = 0.1875F;
			this.txtDT.Left = 5.573125F;
			this.txtDT.Name = "txtDT";
			this.txtDT.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtDT.Text = null;
			this.txtDT.Top = 0F;
			this.txtDT.Width = 1.04125F;
			// 
			// txtOPID
			// 
			this.txtOPID.CanShrink = true;
			this.txtOPID.Height = 0.1875F;
			this.txtOPID.Left = 6.84375F;
			this.txtOPID.Name = "txtOPID";
			this.txtOPID.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOPID.Text = null;
			this.txtOPID.Top = 0F;
			this.txtOPID.Width = 0.4375F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label12,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Line1,
            this.Label18,
            this.lblIssuedOrRedeemed,
            this.Label19});
			this.GroupHeader2.Height = 1.072917F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.15625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.Label12.Text = "VOUCHER";
			this.Label12.Top = 0.6875F;
			this.Label12.Width = 1.0625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.75F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label14.Text = "NAME";
			this.Label14.Top = 0.875F;
			this.Label14.Width = 1.0625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.8125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label15.Text = "DATE";
			this.Label15.Top = 0.875F;
			this.Label15.Width = 0.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.Label16.Text = "AMOUNT";
			this.Label16.Top = 0.875F;
			this.Label16.Width = 1.125F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 6.84375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label17.Text = "OPID";
			this.Label17.Top = 0.875F;
			this.Label17.Width = 0.4375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.0625F;
			this.Line1.Width = 7.40625F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.0625F;
			this.Line1.Y2 = 1.0625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.15625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.Label18.Text = "GIFT CERT /";
			this.Label18.Top = 0.5F;
			this.Label18.Width = 1.0625F;
			// 
			// lblIssuedOrRedeemed
			// 
			this.lblIssuedOrRedeemed.Height = 0.1875F;
			this.lblIssuedOrRedeemed.HyperLink = null;
			this.lblIssuedOrRedeemed.Left = 3.1875F;
			this.lblIssuedOrRedeemed.Name = "lblIssuedOrRedeemed";
			this.lblIssuedOrRedeemed.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.lblIssuedOrRedeemed.Text = "ISSUED";
			this.lblIssuedOrRedeemed.Top = 0.21875F;
			this.lblIssuedOrRedeemed.Width = 1.0625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.15625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.Label19.Text = "NUMBER";
			this.Label19.Top = 0.875F;
			this.Label19.Width = 1.0625F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotals,
            this.txtCLTotal,
            this.lblTotals});
			this.GroupFooter2.Height = 0.3333333F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// txtTotals
			// 
			this.txtTotals.CanShrink = true;
			this.txtTotals.Height = 0.1875F;
			this.txtTotals.Left = 1.71875F;
			this.txtTotals.Name = "txtTotals";
			this.txtTotals.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtTotals.Text = null;
			this.txtTotals.Top = 0.125F;
			this.txtTotals.Width = 0.75F;
			// 
			// txtCLTotal
			// 
			this.txtCLTotal.CanShrink = true;
			this.txtCLTotal.Height = 0.1875F;
			this.txtCLTotal.Left = 2.53125F;
			this.txtCLTotal.Name = "txtCLTotal";
			this.txtCLTotal.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtCLTotal.Text = null;
			this.txtCLTotal.Top = 0.125F;
			this.txtCLTotal.Width = 0.6875F;
			// 
			// lblTotals
			// 
			this.lblTotals.CanShrink = true;
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.Left = 0.25F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.lblTotals.Text = null;
			this.lblTotals.Top = 0.125F;
			this.lblTotals.Width = 1.375F;
			// 
			// srptGiftCertVoucherIssued
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertificate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIssuedOrRedeemed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCustomer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCertificate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOPID;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIssuedOrRedeemed;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTotals;
	}
}
