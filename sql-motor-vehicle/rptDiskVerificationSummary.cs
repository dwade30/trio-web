//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptDiskVerificationSummary.
	/// </summary>
	public partial class rptDiskVerificationSummary : BaseSectionReport
	{
		public rptDiskVerificationSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Verification Report Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptDiskVerificationSummary_ReportEnd;
		}

        private void RptDiskVerificationSummary_ReportEnd(object sender, EventArgs e)
        {
			rsSummary.DisposeOf();
            rsTransfer.DisposeOf();
            rsTotal.DisposeOf();
            rs.DisposeOf();

		}

        public static rptDiskVerificationSummary InstancePtr
		{
			get
			{
				return (rptDiskVerificationSummary)Sys.GetInstance(typeof(rptDiskVerificationSummary));
			}
		}

		protected rptDiskVerificationSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDiskVerificationSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolFirst;
		bool boolTrans;
		bool boolTotal;
		bool boolClass;
		bool boolTransVisible;
		bool boolCorrVisible;
		int intIndexDone;
		//clsDRWrapper rsItemized = new clsDRWrapper();
		clsDRWrapper rsSummary = new clsDRWrapper();
		clsDRWrapper rsTransfer = new clsDRWrapper();
		clsDRWrapper rsTotal = new clsDRWrapper();
		Decimal TotalFee;
		int TotalUnits;
		Decimal TotalTotals;
		int lngPK1;
		int lngPK2;
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL;
		FCFixedString strLine = new FCFixedString(80);
		bool blnFirstRecord;

		private void setTransfer()
		{
			strSQL = "SELECT DISTINCT COUNT(Class) AS TotalCount, SUM(StatePaid) AS TotalRegRate, StatePaid FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " and OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND Class <> 'AP' AND Plate <> 'NEW' AND ETO = 0 GROUP BY StatePaid ORDER BY StatePaid";
			rsTransfer.OpenRecordset(strSQL);
			if (rsTransfer.EndOfFile() != true && rsTransfer.BeginningOfFile() != true)
			{
				rsTransfer.MoveLast();
				rsTransfer.MoveFirst();
			}
			boolTrans = true;
			intIndexDone = 2;
			blnFirstRecord = true;
		}

		private void setClass()
		{
			strSQL = "SELECT DISTINCT COUNT(Class) AS TotalCount, SUM(RegRateCharge) AS TotalRegRate, Class, RegrateCharge FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " and OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND TransactionType <> 'DPS' AND TransactionType <> 'LPS' AND TransactionType <> 'ETO' AND Class <> 'AP' AND Plate <> 'NEW' AND ETO = 0 AND TransactionType <> 'ECO' AND TransactionType <> 'NRT' AND TransactionType <> 'RRT' GROUP BY Class, RegRateCharge ORDER BY Class, RegRateCharge";
			rsSummary.OpenRecordset(strSQL);
			if (rsSummary.EndOfFile() != true && rsSummary.BeginningOfFile() != true)
			{
				rsSummary.MoveLast();
				rsSummary.MoveFirst();
			}
			boolFirst = true;
			intIndexDone = 1;
			blnFirstRecord = true;
		}

		private void setTotal()
		{
			strSQL = "SELECT DISTINCT COUNT(Class) AS TotalCount, (SUM(StatePaid) + SUM(StickerFee) + SUM(ReplacementFee)) AS TotalRegRate, StatePaid, ReplacementFee, StickerFee FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " and OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND (TransactionType = 'ECO' or TransactionType = 'LPS') AND Class <> 'AP' AND Plate <> 'NEW' AND ETO = 0 GROUP BY StatePaid, ReplacementFee, StickerFee ORDER BY StatePaid, ReplacementFee, StickerFee";
			rsTotal.OpenRecordset(strSQL);
			if (rsTotal.EndOfFile() != true && rsTotal.BeginningOfFile() != true)
			{
				rsTotal.MoveLast();
				rsTotal.MoveFirst();
			}
			intIndexDone = 3;
			blnFirstRecord = true;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckAgain:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (intIndexDone == 1)
				{
					if (rsSummary.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						setTransfer();
						goto CheckAgain;
					}
				}
				else if (intIndexDone == 2)
				{
					if (rsTransfer.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						setTotal();
						goto CheckAgain;
					}
				}
				else
				{
					if (rsTotal.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				if (intIndexDone == 1)
				{
					rsSummary.MoveNext();
					if (rsSummary.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						setTransfer();
						goto CheckAgain;
					}
				}
				else if (intIndexDone == 2)
				{
					rsTransfer.MoveNext();
					if (rsTransfer.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						setTotal();
						goto CheckAgain;
					}
				}
				else
				{
					rsTotal.MoveNext();
					if (rsTotal.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			intIndexDone = 1;
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			//Application.DoEvents();
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			intIndexDone = 0;
			setClass();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsSummary.EndOfFile() != true && rsSummary.BeginningOfFile() != true)
			{
				txtCL.Text = rsSummary.Get_Fields_String("Class");
				txtFee.Text = Strings.Format(rsSummary.Get_Fields_Decimal("RegRateCharge"), "currency");
				txtUnits.Text = rsSummary.Get_Fields_String("totalcount");
				txtTotals.Text = Strings.Format(rsSummary.Get_Fields("TotalRegRate"), "currency");
				TotalFee += rsSummary.Get_Fields_Decimal("RegRateCharge");
				TotalUnits += rsSummary.Get_Fields_Int32("totalcount");
				TotalTotals += rsSummary.Get_Fields_Decimal("TotalRegRate");
				lblTransfer.Visible = false;
				lblTotal.Visible = false;
				lblCorrections.Visible = false;
			}
			else if (intIndexDone == 2)
			{
				if (rsTransfer.EndOfFile() != true && rsTransfer.BeginningOfFile() != true)
				{
					txtFee.Text = Strings.Format(rsTransfer.Get_Fields("StatePaid"), "currency");
					txtUnits.Text = rsTransfer.Get_Fields_String("totalcount");
					txtTotals.Text = Strings.Format(rsTransfer.Get_Fields("TotalRegRate"), "currency");
					TotalFee += rsTransfer.Get_Fields_Decimal("StatePaid");
					TotalUnits += rsTransfer.Get_Fields_Int32("totalcount");
					TotalTotals += rsTransfer.Get_Fields_Decimal("TotalRegRate");
					if (boolTransVisible == false)
					{
						lblTransfer.Visible = true;
						boolTransVisible = true;
					}
					else
					{
						lblTransfer.Visible = false;
					}
					lblTotal.Visible = false;
					txtCL.Visible = false;
					lblCorrections.Visible = false;
				}
				else
				{
					boolTrans = true;
					txtFee.Text = "";
					txtUnits.Text = "";
					txtTotals.Text = "";
					txtCL.Text = "";
				}
			}
			else
			{
				boolTotal = true;
				if (rsTotal.EndOfFile() != true && rsTotal.BeginningOfFile() != true)
				{
					if (boolCorrVisible == false)
					{
						lblCorrections.Visible = true;
						boolCorrVisible = true;
					}
					else
					{
						lblCorrections.Visible = false;
					}
					lblTotal.Visible = false;
					txtCL.Visible = false;
					lblTransfer.Visible = false;
					txtFee.Text = Strings.Format(rsTotal.Get_Fields_Decimal("StatePaid") + rsTotal.Get_Fields_Decimal("ReplacementFee") + rsTotal.Get_Fields_Decimal("StickerFee"), "currency");
					txtUnits.Text = rsTotal.Get_Fields_String("totalcount");
					txtTotals.Text = Strings.Format(rsTotal.Get_Fields("TotalRegRate"), "currency");
					if (!fecherFoundation.FCUtils.IsNull(rsTotal.Get_Fields_Decimal("StatePaid") + rsTotal.Get_Fields_Decimal("ReplacementFee") + rsTotal.Get_Fields_Decimal("StickerFee")))
					{
						TotalFee += rsTotal.Get_Fields_Decimal("StatePaid") + rsTotal.Get_Fields_Decimal("ReplacementFee") + rsTotal.Get_Fields_Decimal("StickerFee");
					}
					TotalUnits += rsTotal.Get_Fields_Int32("totalcount");
					if (!fecherFoundation.FCUtils.IsNull(rsTotal.Get_Fields("TotalRegRate")))
					{
						TotalTotals += rsTotal.Get_Fields_Decimal("TotalRegRate");
					}
				}
				else
				{
					txtFee.Text = "";
					txtUnits.Text = "";
					txtTotals.Text = "";
					txtCL.Text = "";
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			lblTotal.Visible = true;
			txtFeeT.Visible = true;
			txtUnitsT.Visible = true;
			txtTotalT.Visible = true;
			txtFeeT.Text = Strings.Format(TotalFee, "currency");
			txtUnitsT.Text = TotalUnits.ToString();
			txtTotalT.Text = Strings.Format(TotalTotals, "currency");
		}

		private void rptDiskVerificationSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptDiskVerificationSummary properties;
			//rptDiskVerificationSummary.Caption	= "Verification Report Summary";
			//rptDiskVerificationSummary.Icon	= "rptDiskVerificationSummary.dsx":0000";
			//rptDiskVerificationSummary.Left	= 0;
			//rptDiskVerificationSummary.Top	= 0;
			//rptDiskVerificationSummary.Width	= 11880;
			//rptDiskVerificationSummary.Height	= 8595;
			//rptDiskVerificationSummary.StartUpPosition	= 3;
			//rptDiskVerificationSummary.SectionData	= "rptDiskVerificationSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
