//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptRegistration.
	/// </summary>
	public partial class rptRegistration : BaseSectionReport
	{
		public rptRegistration()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Registration Listing";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptRegistration InstancePtr
		{
			get
			{
				return (rptRegistration)Sys.GetInstance(typeof(rptRegistration));
			}
		}

		protected rptRegistration _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRegistration	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngPK1;
		int lngPK2;
		int intHeadingNumber;
		clsDRWrapper rsResCode = new clsDRWrapper();
		int tempcode;
		clsDRWrapper rsFees = new clsDRWrapper();
		int LineCount;
		int PageCount;
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		// vbPorter upgrade warning: strLine As FixedString	OnWrite(string)
		FCFixedString strLine = new FCFixedString(80);
		int PageCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: curAmountToSubtract As Decimal	OnWrite(double, int)
			Decimal curAmountToSubtract;
			if (!rs.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				//Application.DoEvents();
				// rs.Close
				rs.Reset();
				return;
			}
			txtClass.Text = "";
			txtPlate.Text = "";
			txtMVR3.Text = "";
			txtStatus.Text = "";
			txtMO.Text = "";
			txtYr.Text = "";
			txtFees.Text = "";
			txtClass.Text = rs.Get_Fields_String("Class");
			txtPlate.Text = rs.Get_Fields_String("plate");
			txtMVR3.Text = rs.Get_Fields_String("MVR3");
			txtStatus.Text = rs.Get_Fields_String("Status");
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("MonthStickerNumber")))
			{
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("MonthStickerNumber")) == 0)
				{
					// do nothing
				}
				else
				{
					txtMO.Text = rs.Get_Fields_String("MonthStickerNumber");
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("YearStickerNumber")))
			{
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("YearStickerNumber")) == 0)
				{
					// do nothing
				}
				else
				{
					txtYr.Text = rs.Get_Fields_String("YearStickerNumber");
				}
			}
			if (FCConvert.ToString(rs.Get_Fields_String("TransactionType")) == "DPR")
			{
				txtFees.Text = FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format(rs.Get_Fields_Decimal("DuplicateRegistrationFee") + rsFees.Get_Fields_Decimal("DupRegAgentFee"), "#,##0.00"), 8));
			}
			else if (FCConvert.ToString(rs.Get_Fields_String("TransactionType")) == "LPS" || FCConvert.ToString(rs.Get_Fields_String("TransactionType")) == "DPS")
			{
				txtFees.Text = FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format(rs.Get_Fields_Decimal("ReplacementFee") + rs.Get_Fields_Decimal("StickerFee"), "#,##0.00"), 8));
			}
			else
			{
				if (rs.Get_Fields_Boolean("EAP"))
				{
					if (rs.Get_Fields_Boolean("ExciseHalfRate"))
					{
						curAmountToSubtract = FCConvert.ToDecimal((rs.Get_Fields_Decimal("ExciseTaxFull") / 2) - (rs.Get_Fields_Decimal("ExciseCreditFull") / 2));
					}
					else
					{
						curAmountToSubtract = FCConvert.ToDecimal(rs.Get_Fields_Decimal("ExciseTaxFull") - rs.Get_Fields_Decimal("ExciseCreditFull"));
					}
					if (curAmountToSubtract < 0)
					{
						curAmountToSubtract = 0;
					}
					txtFees.Text = FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format((rs.Get_Fields_Decimal("LocalPaid") - curAmountToSubtract) + rs.Get_Fields_Decimal("StatePaid"), "#,##0.00"), 8));
				}
				else
				{
					txtFees.Text = FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format(rs.Get_Fields_Decimal("LocalPaid") + rs.Get_Fields_Decimal("StatePaid"), "#,##0.00"), 8));
				}
			}
			LineCount += 1;
			rs.MoveNext();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// 
			LineCount = 0;
			PageCount = 0;
			// 
			LineCount = 0;
			PageCount = 0;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label33.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label32.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSQL);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
			}
			// 
			if (frmReport.InstancePtr.cmbAllRegistrations.Text == "All Registrations")
			{
				if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
				{
					strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND ETO = 0 ORDER BY mvr3";
					rs.OpenRecordset(strSQL);
				}
				else
				{
					strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND ETO = 0 ORDER BY mvr3";
					rs.OpenRecordset(strSQL);
				}
			}
			else if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rsResCode.OpenRecordset("SELECT * FROM DefaultInfo");
				tempcode = FCConvert.ToInt32(rsResCode.Get_Fields_Int32("ResidenceCode"));
				if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
				{
					strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND ETO = 0 AND ResidenceCode = " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSQL);
				}
				else
				{
					strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND ETO = 0 AND ResidenceCode = " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSQL);
				}
			}
			else
			{
				rsResCode.OpenRecordset("SELECT * FROM DefaultInfo");
				tempcode = FCConvert.ToInt32(rsResCode.Get_Fields_Int32("ResidenceCode"));
				if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
				{
					strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND ETO = 0 AND ResidenceCode <> " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSQL);
				}
				else
				{
					strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND ETO = 0 AND ResidenceCode <> " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSQL);
				}
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine.Value = Strings.StrDup(80, " ");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptRegistration_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptRegistration properties;
			//rptRegistration.Caption	= "Registration Listing";
			//rptRegistration.Icon	= "rptRegistration.dsx":0000";
			//rptRegistration.Left	= 0;
			//rptRegistration.Top	= 0;
			//rptRegistration.Width	= 11880;
			//rptRegistration.Height	= 8595;
			//rptRegistration.StartUpPosition	= 3;
			//rptRegistration.SectionData	= "rptRegistration.dsx":058A;
			//End Unmaped Properties
		}
	}
}
