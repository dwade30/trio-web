//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubMonetary.
	/// </summary>
	public partial class rptSubMonetary : BaseSectionReport
	{
		public rptSubMonetary()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Exception Report";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptSubMonetary_ReportEnd;
		}

        private void RptSubMonetary_ReportEnd(object sender, EventArgs e)
        {
            rsAM.DisposeOf();
        }

        public static rptSubMonetary InstancePtr
		{
			get
			{
				return (rptSubMonetary)Sys.GetInstance(typeof(rptSubMonetary));
			}
		}

		protected rptSubMonetary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubMonetary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAM = new clsDRWrapper();
		string temp = "";
		string strSQL = "";
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rsAM.EndOfFile();
			}
			else
			{
				rsAM.MoveNext();
				eArgs.EOF = rsAM.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND Status <> 'V' AND (((TransactionType = 'ECO' OR TransactionType = 'GVW') AND StatePaid > 0) or (TransactionType = 'LPS' and (ReplacementFee <> 0 or StickerFee <> 0))) ORDER BY DateUpdated";
			}
			else
			{
				strSQL = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Status <> 'V' AND (((TransactionType = 'ECO' OR TransactionType = 'GVW') AND StatePaid > 0) or (TransactionType = 'LPS' and (ReplacementFee <> 0 or StickerFee <> 0))) ORDER BY DateUpdated";
			}
			rsAM.OpenRecordset(strSQL);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rptNewExceptionReport.InstancePtr.boolData = true;
			}
			else
			{
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rptNewExceptionReport.InstancePtr.MonCorrTotal += 1;
			fldDate.Text = Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy");
			fldReciept.Text = Strings.Format(rsAM.Get_Fields_Int32("MVR3"), "00000000");
			fldClassPlate.Text = rsAM.Get_Fields_String("Class") + " " + rsAM.Get_Fields_String("plate");
			if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS")
			{
				fldAmount.Text = Strings.Format(Strings.Format(rsAM.Get_Fields_Double("ReplacementFee") + rsAM.Get_Fields_Double("StickerFee"), "#,###.00"), "@@@@@@@@@");
				fldExplanation.Text = " Lost Plate/Stickers ";
				rptNewExceptionReport.InstancePtr.MonCorrMoneyTotal += FCConvert.ToSingle(rsAM.Get_Fields_Double("ReplacementFee") + rsAM.Get_Fields_Double("StickerFee"));
			}
			else
			{
				if (!fecherFoundation.FCUtils.IsNull(rsAM.Get_Fields("StatePaid")))
				{
					// And Not IsNull(.fields("LocalPaid")) Then
					fldAmount.Text = Strings.Format(Strings.Format(rsAM.Get_Fields("StatePaid"), "#,###.00"), "@@@@@@@@@");
					rptNewExceptionReport.InstancePtr.MonCorrMoneyTotal += FCConvert.ToSingle(rsAM.Get_Fields_Double("StatePaid"));
				}
				else
				{
					// If IsNull(.fields("StatePaid")) And IsNull(.fields("LocalPaid")) Then
					fldAmount.Text = Strings.Format(Strings.Format(0, "#,###.00"), "@@@@@@@@@");
					// ElseIf IsNull(.fields("StatePaid")) Then
					// fldAmount = Format(Format(.fields("LocalPaid, "#,###.00"), "@@@@@@@@@")
					// rptNewExceptionReport.MonCorrMoneyTotal = rptNewExceptionReport.MonCorrMoneyTotal + CSng(.fields("LocalPaid"))
					// Else
					// fldAmount = Format(Format(.fields("StatePaid"), "#,###.00"), "@@@@@@@@@")
					// rptNewExceptionReport.MonCorrMoneyTotal = rptNewExceptionReport.MonCorrMoneyTotal + CSng(.fields("StatePaid"))
				}
				if (fecherFoundation.FCUtils.IsNull(rsAM.Get_Fields_String("InfoMessage")) == false)
				{
					if (FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length <= 20)
					{
						fldExplanation.Text = rsAM.Get_Fields_String("InfoMessage") + Strings.StrDup(20 - FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length, " ");
					}
					else
					{
						fldExplanation.Text = Strings.Mid(FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")), 1, 20);
					}
				}
			}
			temp = frmReport.InstancePtr.BuildCategoryCode(ref rsAM);
			if (temp.Length < 20)
			{
				fldAdjusted.Text = Strings.Format(temp, "!@@@@@@@@@@@@@@@@@@@@");
			}
			else
			{
				fldAdjusted.Text = Strings.Format(Strings.Mid(temp, 1, 20), "!@@@@@@@@@@@@@@@@@@@@");
			}
			fldOpID.Text = rsAM.Get_Fields_String("OpID");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalCount.Text = FCConvert.ToString(rptNewExceptionReport.InstancePtr.MonCorrTotal);
			fldTotalMoney.Text = Strings.Format(rptNewExceptionReport.InstancePtr.MonCorrMoneyTotal, "#,##0.00");
		}

		private void rptSubMonetary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptSubMonetary properties;
			//rptSubMonetary.Caption	= "Exception Report";
			//rptSubMonetary.Icon	= "rptSubException.dsx":0000";
			//rptSubMonetary.Left	= 0;
			//rptSubMonetary.Top	= 0;
			//rptSubMonetary.Width	= 11880;
			//rptSubMonetary.Height	= 8595;
			//rptSubMonetary.StartUpPosition	= 3;
			//rptSubMonetary.SectionData	= "rptSubException.dsx":058A;
			//End Unmaped Properties
		}
	}
}
