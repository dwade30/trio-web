//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmVehicleHistory.
	/// </summary>
	partial class frmVehicleHistory
	{
		public fecherFoundation.FCComboBox cmbAnnual;
		public fecherFoundation.FCLabel lblAnnual;
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCFrame fraMulti;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOk;
		public FCGrid vsMulti;
		public FCGrid vsResults;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbAnnual = new fecherFoundation.FCComboBox();
            this.lblAnnual = new fecherFoundation.FCLabel();
            this.cmbSearchType = new fecherFoundation.FCComboBox();
            this.lblSearchType = new fecherFoundation.FCLabel();
            this.fraMulti = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOk = new fecherFoundation.FCButton();
            this.vsMulti = new fecherFoundation.FCGrid();
            this.vsResults = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.lblSearchInfo = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFilePreview = new fecherFoundation.FCButton();
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMulti)).BeginInit();
            this.fraMulti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMulti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFilePreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(898, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbAnnual);
            this.ClientArea.Controls.Add(this.lblAnnual);
            this.ClientArea.Controls.Add(this.fraMulti);
            this.ClientArea.Controls.Add(this.vsResults);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Size = new System.Drawing.Size(898, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFilePrint);
            this.TopPanel.Size = new System.Drawing.Size(898, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(176, 30);
            this.HeaderText.Text = "Vehicle History";
            // 
            // cmbAnnual
            // 
            this.cmbAnnual.AutoSize = false;
            this.cmbAnnual.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAnnual.FormattingEnabled = true;
            this.cmbAnnual.Items.AddRange(new object[] {
            "Annual",
            "Long Term"});
            this.cmbAnnual.Location = new System.Drawing.Point(127, 30);
            this.cmbAnnual.Name = "cmbAnnual";
            this.cmbAnnual.Size = new System.Drawing.Size(180, 40);
            this.cmbAnnual.TabIndex = 0;
            this.cmbAnnual.Text = "Annual";
            this.cmbAnnual.Visible = false;
            // 
            // lblAnnual
            // 
            this.lblAnnual.AutoSize = true;
            this.lblAnnual.Location = new System.Drawing.Point(30, 44);
            this.lblAnnual.Name = "lblAnnual";
            this.lblAnnual.Size = new System.Drawing.Size(40, 15);
            this.lblAnnual.TabIndex = 1;
            this.lblAnnual.Text = "TYPE";
            this.lblAnnual.Visible = false;
            // 
            // cmbSearchType
            // 
            this.cmbSearchType.AutoSize = false;
            this.cmbSearchType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSearchType.FormattingEnabled = true;
            this.cmbSearchType.Items.AddRange(new object[] {
            "VIN",
            "Plate"});
            this.cmbSearchType.Location = new System.Drawing.Point(230, 30);
            this.cmbSearchType.Name = "cmbSearchType";
            this.cmbSearchType.Size = new System.Drawing.Size(181, 40);
            this.cmbSearchType.TabIndex = 3;
            this.cmbSearchType.Text = "VIN";
            this.cmbSearchType.SelectedIndexChanged += new System.EventHandler(this.optSearchType_CheckedChanged);
            // 
            // lblSearchType
            // 
            this.lblSearchType.AutoSize = true;
            this.lblSearchType.Location = new System.Drawing.Point(20, 44);
            this.lblSearchType.Name = "lblSearchType";
            this.lblSearchType.Size = new System.Drawing.Size(79, 15);
            this.lblSearchType.TabIndex = 4;
            this.lblSearchType.Text = "SEARCH BY";
            // 
            // fraMulti
            // 
            this.fraMulti.Controls.Add(this.cmdCancel);
            this.fraMulti.Controls.Add(this.cmdOk);
            this.fraMulti.Controls.Add(this.vsMulti);
            this.fraMulti.Location = new System.Drawing.Point(144, 120);
            this.fraMulti.Name = "fraMulti";
            this.fraMulti.Size = new System.Drawing.Size(402, 341);
            this.fraMulti.TabIndex = 9;
            this.fraMulti.Text = "Multiple Matches";
            this.fraMulti.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(114, 281);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 40);
            this.cmdCancel.TabIndex = 12;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "actionButton";
            this.cmdOk.Location = new System.Drawing.Point(20, 281);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(64, 40);
            this.cmdOk.TabIndex = 11;
            this.cmdOk.Text = "OK";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // vsMulti
            // 
            this.vsMulti.AllowSelection = false;
            this.vsMulti.AllowUserToResizeColumns = false;
            this.vsMulti.AllowUserToResizeRows = false;
            this.vsMulti.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsMulti.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsMulti.BackColorBkg = System.Drawing.Color.Empty;
            this.vsMulti.BackColorFixed = System.Drawing.Color.Empty;
            this.vsMulti.BackColorSel = System.Drawing.Color.Empty;
            this.vsMulti.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsMulti.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsMulti.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsMulti.ColumnHeadersHeight = 30;
            this.vsMulti.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsMulti.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsMulti.DragIcon = null;
            this.vsMulti.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsMulti.ExtendLastCol = true;
            this.vsMulti.FixedCols = 0;
            this.vsMulti.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsMulti.FrozenCols = 0;
            this.vsMulti.GridColor = System.Drawing.Color.Empty;
            this.vsMulti.GridColorFixed = System.Drawing.Color.Empty;
            this.vsMulti.Location = new System.Drawing.Point(20, 30);
            this.vsMulti.Name = "vsMulti";
            this.vsMulti.OutlineCol = 0;
            this.vsMulti.ReadOnly = true;
            this.vsMulti.RowHeadersVisible = false;
            this.vsMulti.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsMulti.RowHeightMin = 0;
            this.vsMulti.Rows = 1;
            this.vsMulti.ScrollTipText = null;
            this.vsMulti.ShowColumnVisibilityMenu = false;
            this.vsMulti.ShowFocusCell = false;
            this.vsMulti.Size = new System.Drawing.Size(360, 231);
            this.vsMulti.StandardTab = true;
            this.vsMulti.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsMulti.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsMulti.TabIndex = 10;
            this.vsMulti.DoubleClick += new System.EventHandler(this.vsMulti_DblClick);
            // 
            // vsResults
            // 
            this.vsResults.AllowSelection = false;
            this.vsResults.AllowUserToResizeColumns = false;
            this.vsResults.AllowUserToResizeRows = false;
            this.vsResults.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsResults.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsResults.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsResults.BackColorBkg = System.Drawing.Color.Empty;
            this.vsResults.BackColorFixed = System.Drawing.Color.Empty;
            this.vsResults.BackColorSel = System.Drawing.Color.Empty;
            this.vsResults.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsResults.Cols = 12;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsResults.ColumnHeadersHeight = 30;
            this.vsResults.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsResults.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsResults.DragIcon = null;
            this.vsResults.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsResults.FixedCols = 0;
            this.vsResults.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsResults.FrozenCols = 0;
            this.vsResults.GridColor = System.Drawing.Color.Empty;
            this.vsResults.GridColorFixed = System.Drawing.Color.Empty;
            this.vsResults.Location = new System.Drawing.Point(30, 320);
            this.vsResults.Name = "vsResults";
            this.vsResults.OutlineCol = 0;
            this.vsResults.ReadOnly = true;
            this.vsResults.RowHeadersVisible = false;
            this.vsResults.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsResults.RowHeightMin = 0;
            this.vsResults.Rows = 1;
            this.vsResults.ScrollTipText = null;
            this.vsResults.ShowColumnVisibilityMenu = false;
            this.vsResults.ShowFocusCell = false;
            this.vsResults.Size = new System.Drawing.Size(745, 285);
            this.vsResults.StandardTab = true;
            this.vsResults.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsResults.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsResults.TabIndex = 8;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmdClear);
            this.Frame2.Controls.Add(this.cmbSearchType);
            this.Frame2.Controls.Add(this.lblSearchType);
            this.Frame2.Controls.Add(this.cmdSearch);
            this.Frame2.Controls.Add(this.txtSearch);
            this.Frame2.Controls.Add(this.lblSearchInfo);
            this.Frame2.Location = new System.Drawing.Point(30, 90);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(431, 210);
            this.Frame2.TabIndex = 3;
            this.Frame2.Text = "Search";
            // 
            // cmdClear
            // 
            this.cmdClear.AppearanceKey = "actionButton";
            this.cmdClear.Location = new System.Drawing.Point(20, 150);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(142, 40);
            this.cmdClear.TabIndex = 2;
            this.cmdClear.Text = "Clear Search";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Location = new System.Drawing.Point(192, 150);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(96, 40);
            this.cmdSearch.TabIndex = 1;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = false;
            this.txtSearch.CharacterCasing = CharacterCasing.Upper;
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.LinkItem = null;
            this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSearch.LinkTopic = null;
            this.txtSearch.Location = new System.Drawing.Point(230, 90);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(181, 40);
            this.txtSearch.TabIndex = 0;
            // 
            // lblSearchInfo
            // 
            this.lblSearchInfo.AutoSize = true;
            this.lblSearchInfo.Location = new System.Drawing.Point(20, 104);
            this.lblSearchInfo.Name = "lblSearchInfo";
            this.lblSearchInfo.Size = new System.Drawing.Size(165, 15);
            this.lblSearchInfo.TabIndex = 7;
            this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 1;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdFilePreview
            // 
            this.cmdFilePreview.AppearanceKey = "acceptButton";
            this.cmdFilePreview.Location = new System.Drawing.Point(363, 30);
            this.cmdFilePreview.Name = "cmdFilePreview";
            this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFilePreview.Size = new System.Drawing.Size(148, 48);
            this.cmdFilePreview.TabIndex = 0;
            this.cmdFilePreview.Text = "Print Preview";
            this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrint.AppearanceKey = "toolbarButton";
            this.cmdFilePrint.Location = new System.Drawing.Point(824, 29);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdFilePrint.Size = new System.Drawing.Size(46, 24);
            this.cmdFilePrint.TabIndex = 1;
            this.cmdFilePrint.Text = "Print";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // frmVehicleHistory
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(898, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmVehicleHistory";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Vehicle History";
            this.Load += new System.EventHandler(this.frmVehicleHistory_Load);
            this.Activated += new System.EventHandler(this.frmVehicleHistory_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVehicleHistory_KeyPress);
            this.Resize += new System.EventHandler(this.frmVehicleHistory_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMulti)).EndInit();
            this.fraMulti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMulti)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdFilePreview;
        private FCButton cmdFilePrint;
    }
}