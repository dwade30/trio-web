//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmSpecialPermit.
	/// </summary>
	partial class frmSpecialPermit
	{
		public fecherFoundation.FCTextBox txtPermit;
		public fecherFoundation.FCFrame fraApplicant;
		public fecherFoundation.FCTextBox txtResidenceCode;
		public fecherFoundation.FCTextBox txtCustomerNumber;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdEdit;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblCustomerNumber;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblCustomerInfo;
		public fecherFoundation.FCFrame fraVehicle;
		public fecherFoundation.FCTextBox txtColor2;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCTextBox txtMake;
		public fecherFoundation.FCTextBox txtColor;
		public fecherFoundation.FCTextBox txtVIN;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblMake;
		public fecherFoundation.FCLabel lblColor;
		public fecherFoundation.FCLabel lblVin;
		public Global.T2KDateBox txtExpireDate;
		public Global.T2KDateBox txtEffectiveDate;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblPermit;
		public fecherFoundation.FCButton cmdProcessSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSpecialPermit));
            this.txtPermit = new fecherFoundation.FCTextBox();
            this.fraApplicant = new fecherFoundation.FCFrame();
            this.txtResidenceCode = new fecherFoundation.FCTextBox();
            this.txtCustomerNumber = new fecherFoundation.FCTextBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.cmdEdit = new fecherFoundation.FCButton();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblCustomerNumber = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblCustomerInfo = new fecherFoundation.FCLabel();
            this.fraVehicle = new fecherFoundation.FCFrame();
            this.txtColor2 = new fecherFoundation.FCTextBox();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.txtMake = new fecherFoundation.FCTextBox();
            this.txtVIN = new fecherFoundation.FCTextBox();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblMake = new fecherFoundation.FCLabel();
            this.lblColor = new fecherFoundation.FCLabel();
            this.lblVin = new fecherFoundation.FCLabel();
            this.txtColor = new fecherFoundation.FCTextBox();
            this.txtExpireDate = new Global.T2KDateBox();
            this.txtEffectiveDate = new Global.T2KDateBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblPermit = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).BeginInit();
            this.fraApplicant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraVehicle)).BeginInit();
            this.fraVehicle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtPermit);
            this.ClientArea.Controls.Add(this.fraApplicant);
            this.ClientArea.Controls.Add(this.fraVehicle);
            this.ClientArea.Controls.Add(this.txtExpireDate);
            this.ClientArea.Controls.Add(this.txtEffectiveDate);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblPermit);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.TabIndex = 0;
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(310, 30);
            this.HeaderText.Text = "Special Registration Permit";
            // 
            // txtPermit
            // 
            this.txtPermit.BackColor = System.Drawing.SystemColors.Window;
            this.txtPermit.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPermit.Location = new System.Drawing.Point(192, 90);
            this.txtPermit.MaxLength = 6;
            this.txtPermit.Name = "txtPermit";
            this.txtPermit.Size = new System.Drawing.Size(120, 40);
            this.txtPermit.TabIndex = 5;
            this.txtPermit.Validating += new System.ComponentModel.CancelEventHandler(this.txtPermit_Validating);
            // 
            // fraApplicant
            // 
            this.fraApplicant.Controls.Add(this.txtResidenceCode);
            this.fraApplicant.Controls.Add(this.txtCustomerNumber);
            this.fraApplicant.Controls.Add(this.cmdSearch);
            this.fraApplicant.Controls.Add(this.cmdEdit);
            this.fraApplicant.Controls.Add(this.Label4);
            this.fraApplicant.Controls.Add(this.lblCustomerNumber);
            this.fraApplicant.Controls.Add(this.Label3);
            this.fraApplicant.Controls.Add(this.lblCustomerInfo);
            this.fraApplicant.Location = new System.Drawing.Point(30, 150);
            this.fraApplicant.Name = "fraApplicant";
            this.fraApplicant.Size = new System.Drawing.Size(445, 245);
            this.fraApplicant.TabIndex = 6;
            this.fraApplicant.Text = "Applicant Information";
            // 
            // txtResidenceCode
            // 
            this.txtResidenceCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtResidenceCode.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtResidenceCode.Location = new System.Drawing.Point(162, 185);
            this.txtResidenceCode.MaxLength = 5;
            this.txtResidenceCode.Name = "txtResidenceCode";
            this.txtResidenceCode.Size = new System.Drawing.Size(263, 40);
            this.txtResidenceCode.TabIndex = 7;
            // 
            // txtCustomerNumber
            // 
            this.txtCustomerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCustomerNumber.Location = new System.Drawing.Point(162, 30);
            this.txtCustomerNumber.Name = "txtCustomerNumber";
            this.txtCustomerNumber.Size = new System.Drawing.Size(120, 40);
            this.txtCustomerNumber.TabIndex = 1;
            this.txtCustomerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustomerNumber_Validating);
            this.txtCustomerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustomerNumber_KeyPress);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            //this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.ImageSource = "icon - search";
            this.cmdSearch.Location = new System.Drawing.Point(301, 30);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(40, 40);
            this.cmdSearch.TabIndex = 2;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdEdit
            // 
            this.cmdEdit.AppearanceKey = "actionButton";
            //this.cmdEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdEdit.Image")));
            this.cmdEdit.ImageSource = "icon - edit";
            this.cmdEdit.Location = new System.Drawing.Point(351, 30);
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Size = new System.Drawing.Size(40, 40);
            this.cmdEdit.TabIndex = 3;
            this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 199);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(125, 16);
            this.Label4.TabIndex = 6;
            this.Label4.Text = "RESIDENCE CODE";
            // 
            // lblCustomerNumber
            // 
            this.lblCustomerNumber.Location = new System.Drawing.Point(20, 44);
            this.lblCustomerNumber.Name = "lblCustomerNumber";
            this.lblCustomerNumber.Size = new System.Drawing.Size(102, 18);
            this.lblCustomerNumber.Text = "CUSTOMER #";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 90);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(125, 18);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "CUSTOMER";
            // 
            // lblCustomerInfo
            // 
            this.lblCustomerInfo.Location = new System.Drawing.Point(177, 90);
            this.lblCustomerInfo.Name = "lblCustomerInfo";
            this.lblCustomerInfo.Size = new System.Drawing.Size(248, 75);
            this.lblCustomerInfo.TabIndex = 5;
            // 
            // fraVehicle
            // 
            this.fraVehicle.Controls.Add(this.txtColor2);
            this.fraVehicle.Controls.Add(this.txtYear);
            this.fraVehicle.Controls.Add(this.txtMake);
            this.fraVehicle.Controls.Add(this.txtVIN);
            this.fraVehicle.Controls.Add(this.lblYear);
            this.fraVehicle.Controls.Add(this.lblMake);
            this.fraVehicle.Controls.Add(this.lblColor);
            this.fraVehicle.Controls.Add(this.lblVin);
            this.fraVehicle.Controls.Add(this.txtColor);
            this.fraVehicle.Location = new System.Drawing.Point(30, 425);
            this.fraVehicle.Name = "fraVehicle";
            this.fraVehicle.Size = new System.Drawing.Size(445, 270);
            this.fraVehicle.TabIndex = 7;
            this.fraVehicle.Text = "Vehicle Information";
            // 
            // txtColor2
            // 
            this.txtColor2.BackColor = System.Drawing.SystemColors.Window;
            this.txtColor2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor2.Location = new System.Drawing.Point(290, 150);
            this.txtColor2.MaxLength = 2;
            this.txtColor2.Name = "txtColor2";
            this.txtColor2.Size = new System.Drawing.Size(135, 40);
            this.txtColor2.TabIndex = 6;
            this.txtColor2.Validating += new System.ComponentModel.CancelEventHandler(this.txtColor2_Validating);
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYear.Location = new System.Drawing.Point(130, 30);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(295, 40);
            this.txtYear.TabIndex = 1;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // txtMake
            // 
            this.txtMake.BackColor = System.Drawing.SystemColors.Window;
            this.txtMake.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMake.Location = new System.Drawing.Point(130, 90);
            this.txtMake.MaxLength = 4;
            this.txtMake.Name = "txtMake";
            this.txtMake.Size = new System.Drawing.Size(295, 40);
            this.txtMake.TabIndex = 3;
            this.txtMake.Validating += new System.ComponentModel.CancelEventHandler(this.txtMake_Validating);
            // 
            // txtVIN
            // 
            this.txtVIN.BackColor = System.Drawing.SystemColors.Window;
            this.txtVIN.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtVIN.Location = new System.Drawing.Point(130, 210);
            this.txtVIN.MaxLength = 18;
            this.txtVIN.Name = "txtVIN";
            this.txtVIN.Size = new System.Drawing.Size(295, 40);
            this.txtVIN.TabIndex = 8;
            this.txtVIN.Validating += new System.ComponentModel.CancelEventHandler(this.txtVIN_Validating);
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(20, 44);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(47, 16);
            this.lblYear.Text = "YEAR";
            // 
            // lblMake
            // 
            this.lblMake.Location = new System.Drawing.Point(20, 104);
            this.lblMake.Name = "lblMake";
            this.lblMake.Size = new System.Drawing.Size(54, 16);
            this.lblMake.TabIndex = 2;
            this.lblMake.Text = "MAKE";
            // 
            // lblColor
            // 
            this.lblColor.Location = new System.Drawing.Point(20, 164);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(54, 16);
            this.lblColor.TabIndex = 4;
            this.lblColor.Text = "COLOR";
            // 
            // lblVin
            // 
            this.lblVin.Location = new System.Drawing.Point(20, 224);
            this.lblVin.Name = "lblVin";
            this.lblVin.Size = new System.Drawing.Size(31, 16);
            this.lblVin.TabIndex = 7;
            this.lblVin.Text = "VIN #";
            // 
            // txtColor
            // 
            this.txtColor.BackColor = System.Drawing.SystemColors.Window;
            this.txtColor.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor.Location = new System.Drawing.Point(130, 150);
            this.txtColor.MaxLength = 2;
            this.txtColor.Name = "txtColor";
            this.txtColor.Size = new System.Drawing.Size(140, 40);
            this.txtColor.TabIndex = 5;
            this.txtColor.Validating += new System.ComponentModel.CancelEventHandler(this.txtColor_Validating);
            // 
            // txtExpireDate
            // 
            this.txtExpireDate.Location = new System.Drawing.Point(502, 30);
            this.txtExpireDate.Mask = "##/##/####";
            this.txtExpireDate.Name = "txtExpireDate";
            this.txtExpireDate.Size = new System.Drawing.Size(120, 22);
            this.txtExpireDate.TabIndex = 3;
            this.txtExpireDate.TabStop = false;
            // 
            // txtEffectiveDate
            // 
            this.txtEffectiveDate.Location = new System.Drawing.Point(192, 30);
            this.txtEffectiveDate.Mask = "##/##/####";
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(120, 22);
            this.txtEffectiveDate.TabIndex = 1;
            this.txtEffectiveDate.TabStop = false;
            this.txtEffectiveDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtEffectiveDate_Validate);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(112, 16);
            this.Label1.Text = "EFFECTIVE DATE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(334, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(111, 16);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "EXPIRATION DATE";
            // 
            // lblPermit
            // 
            this.lblPermit.Location = new System.Drawing.Point(30, 104);
            this.lblPermit.Name = "lblPermit";
            this.lblPermit.Size = new System.Drawing.Size(62, 20);
            this.lblPermit.TabIndex = 4;
            this.lblPermit.Text = "PERMIT #";
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(430, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(127, 48);
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmSpecialPermit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSpecialPermit";
            this.Text = "Special Registration Permit";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmSpecialPermit_Load);
            this.Activated += new System.EventHandler(this.frmSpecialPermit_Activated);
            this.Resize += new System.EventHandler(this.frmSpecialPermit_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSpecialPermit_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSpecialPermit_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).EndInit();
            this.fraApplicant.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraVehicle)).EndInit();
            this.fraVehicle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
