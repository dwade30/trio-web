﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReminderForm.
	/// </summary>
	public partial class rptReminderForm : BaseSectionReport
	{
		public rptReminderForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Reminder Notices";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptReminderForm InstancePtr
		{
			get
			{
				return (rptReminderForm)Sys.GetInstance(typeof(rptReminderForm));
			}
		}

		protected rptReminderForm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderForm	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: counter As int	OnRead
		int counter;
		// kk05162017 tromv-1115  Change from Integer to Long
		// vbPorter upgrade warning: VehicleCounter As int	OnWriteFCConvert.ToInt32(
		int VehicleCounter;
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(int, Decimal)
		Decimal curTotal;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grouping");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			double Amount;
			Decimal amount1;
			Decimal amount2;
			Decimal amount3;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
				if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true)
				{
					VehicleCounter = counter;
					this.Fields["grouping"].Value = counter.ToString();
					eArgs.EOF = false;
				}
				else
				{
					counter += 1;
					while (counter < frmReminder.InstancePtr.vsVehicles.Rows)
					{
						//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
						//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true && FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 12)) != true)
						if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true && FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 12)) != true)
						{
							VehicleCounter = counter;
							this.Fields["grouping"].Value = counter.ToString();
							frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 12, FCConvert.ToString(true));
							eArgs.EOF = false;
							break;
						}
						else
						{
							counter += 1;
						}
					}
					if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
					{
						eArgs.EOF = true;
					}
				}
			}
			else
			{
				VehicleCounter += 1;
				while (VehicleCounter < frmReminder.InstancePtr.vsVehicles.Rows)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 1)) == true && FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 12)) != true && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 10) && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 13) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 13) && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 14) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 14) && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 15) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 15))
					if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 1)) == true && FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 12)) != true && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 10) && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 13) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 13) && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 14) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 14) && frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 15) == frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 15))
					{
						eArgs.EOF = false;
						this.Fields["grouping"].Value = counter.ToString();
						frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 12, FCConvert.ToString(true));
						break;
					}
					else
					{
						VehicleCounter += 1;
					}
				}
				if (VehicleCounter >= frmReminder.InstancePtr.vsVehicles.Rows)
				{
					counter += 1;
					while (counter < frmReminder.InstancePtr.vsVehicles.Rows)
					{
						//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
						//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true && FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 12)) != true)
						if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 1)) == true && FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 12)) != true)
						{
							VehicleCounter = counter;
							eArgs.EOF = false;
							this.Fields["grouping"].Value = counter.ToString();
							frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 12, FCConvert.ToString(true));
							break;
						}
						else
						{
							counter += 1;
						}
					}
					if (counter >= frmReminder.InstancePtr.vsVehicles.Rows)
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			fldMessage.Text = frmReminder.InstancePtr.txtMessage.Text;
			fldDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTownName.Text = frmReminder.InstancePtr.txtTownName.Text;
			lblTownAddress1.Text = frmReminder.InstancePtr.txtTownAddress[0].Text;
			lblTownAddress2.Text = frmReminder.InstancePtr.txtTownAddress[1].Text;
			lblTownAddress3.Text = frmReminder.InstancePtr.txtTownAddress[2].Text;
			lblTownAddress4.Text = frmReminder.InstancePtr.txtTownAddress[3].Text;
			fldStatement.Text = "Listed are motor vehicle registrations due for renewal in the month of " + frmReminder.InstancePtr.cboMonth.Text + ".";
			for (counter = 1; counter <= frmReminder.InstancePtr.vsVehicles.Rows - 1; counter++)
			{
				frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 12, FCConvert.ToString(false));
			}
			blnFirstRecord = true;
			curTotal = 0;
			counter = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			Decimal amount1 = 0;
			Decimal amount2 = 0;
			Decimal amount3 = 0;
			fldPlate.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 3);
			fldYear.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 4);
			fldMake.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 5);
			fldModel.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 6);
			if (frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 7) != "")
			{
				amount1 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 7));
			}
			if (frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 8) != "")
			{
				amount2 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 8));
			}
			if (frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 9) != "")
			{
				amount3 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 9));
			}
			if (frmReminder.InstancePtr.vsVehicles.TextMatrix(VehicleCounter, 7) != "")
			{
				curTotal = amount1 + amount2 + amount3;
				fldAmount.Text = Strings.Format(curTotal, "$#,##0.00");
			}
			else
			{
				fldAmount.Text = "Call Office for Amount";
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldName.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 10);
			fldAddress1.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 13);
			fldAddress2.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 14);
			fldAddress3.Text = frmReminder.InstancePtr.vsVehicles.TextMatrix(counter, 15);
		}

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void rptReminderForm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderForm properties;
			//rptReminderForm.Caption	= "Reminder Notices";
			//rptReminderForm.Icon	= "rptReminderForm.dsx":0000";
			//rptReminderForm.Left	= 0;
			//rptReminderForm.Top	= 0;
			//rptReminderForm.Width	= 21480;
			//rptReminderForm.Height	= 12990;
			//rptReminderForm.StartUpPosition	= 3;
			//rptReminderForm.SectionData	= "rptReminderForm.dsx":508A;
			//End Unmaped Properties
		}
	}
}
