//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmFleetExpiringListMMTA.
	/// </summary>
	partial class frmFleetExpiringListMMTA
	{
		public fecherFoundation.FCComboBox cmbOrderByPlate;
		public fecherFoundation.FCLabel lblOrderByPlate;
		public fecherFoundation.FCComboBox cmbFleetNameSelected;
		public fecherFoundation.FCComboBox cmbFleetTypeSelected;
		public fecherFoundation.FCLabel lblFleetTypeSelected;
		public fecherFoundation.FCComboBox cmbAnnual;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCComboBox cmbGroups;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraFleetNameSelected;
		public fecherFoundation.FCComboBox cboStartName;
		public fecherFoundation.FCComboBox cboEndName;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCCheckBox chkLongTermOnly;
		public fecherFoundation.FCFrame fraType;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCGrid vsFleets;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbOrderByPlate = new fecherFoundation.FCComboBox();
            this.lblOrderByPlate = new fecherFoundation.FCLabel();
            this.cmbFleetNameSelected = new fecherFoundation.FCComboBox();
            this.cmbFleetTypeSelected = new fecherFoundation.FCComboBox();
            this.lblFleetTypeSelected = new fecherFoundation.FCLabel();
            this.cmbAnnual = new fecherFoundation.FCComboBox();
            this.cmbAll = new fecherFoundation.FCComboBox();
            this.cmbGroups = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.fraFleetNameSelected = new fecherFoundation.FCFrame();
            this.cboStartName = new fecherFoundation.FCComboBox();
            this.cboEndName = new fecherFoundation.FCComboBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.cmdClearAll = new fecherFoundation.FCButton();
            this.chkLongTermOnly = new fecherFoundation.FCCheckBox();
            this.fraType = new fecherFoundation.FCFrame();
            this.txtStartDate = new Global.T2KDateBox();
            this.txtEndDate = new Global.T2KDateBox();
            this.vsFleets = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblTo = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFleetNameSelected)).BeginInit();
            this.fraFleetNameSelected.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLongTermOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
            this.fraType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFleets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(848, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbFleetTypeSelected);
            this.ClientArea.Controls.Add(this.lblFleetTypeSelected);
            this.ClientArea.Controls.Add(this.cmbOrderByPlate);
            this.ClientArea.Controls.Add(this.lblOrderByPlate);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmbAnnual);
            this.ClientArea.Controls.Add(this.chkLongTermOnly);
            this.ClientArea.Controls.Add(this.fraType);
            this.ClientArea.Controls.Add(this.txtStartDate);
            this.ClientArea.Controls.Add(this.txtEndDate);
            this.ClientArea.Controls.Add(this.vsFleets);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblTo);
            this.ClientArea.Size = new System.Drawing.Size(848, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClearAll);
            this.TopPanel.Controls.Add(this.cmdSelectAll);
            this.TopPanel.Size = new System.Drawing.Size(848, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelectAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClearAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(226, 30);
            this.HeaderText.Text = "Fleet Reminder List";
            // 
            // cmbOrderByPlate
            // 
            this.cmbOrderByPlate.AutoSize = false;
            this.cmbOrderByPlate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbOrderByPlate.FormattingEnabled = true;
            this.cmbOrderByPlate.Items.AddRange(new object[] {
            "Plate",
            "Unit"});
            this.cmbOrderByPlate.Location = new System.Drawing.Point(672, 160);
            this.cmbOrderByPlate.Name = "cmbOrderByPlate";
            this.cmbOrderByPlate.Size = new System.Drawing.Size(120, 40);
            this.cmbOrderByPlate.TabIndex = 0;
            this.cmbOrderByPlate.Text = "Plate";
            // 
            // lblOrderByPlate
            // 
            this.lblOrderByPlate.AutoSize = true;
            this.lblOrderByPlate.Location = new System.Drawing.Point(672, 130);
            this.lblOrderByPlate.Name = "lblOrderByPlate";
            this.lblOrderByPlate.Size = new System.Drawing.Size(126, 15);
            this.lblOrderByPlate.TabIndex = 1;
            this.lblOrderByPlate.Text = "SORT VEHICLES BY";
            // 
            // cmbFleetNameSelected
            // 
            this.cmbFleetNameSelected.AutoSize = false;
            this.cmbFleetNameSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFleetNameSelected.FormattingEnabled = true;
            this.cmbFleetNameSelected.Items.AddRange(new object[] {
			"All",
			"Selected"});
            this.cmbFleetNameSelected.Location = new System.Drawing.Point(20, 30);
            this.cmbFleetNameSelected.Name = "cmbFleetNameSelected";
            this.cmbFleetNameSelected.Size = new System.Drawing.Size(214, 40);
            this.cmbFleetNameSelected.TabIndex = 24;
            this.cmbFleetNameSelected.Text = "All";
            this.cmbFleetNameSelected.SelectedIndexChanged += new System.EventHandler(this.cmbFleetNameSelected_SelectedIndexChanged);
            // 
            // cmbFleetTypeSelected
            // 
            this.cmbFleetTypeSelected.AutoSize = false;
            this.cmbFleetTypeSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbFleetTypeSelected.FormattingEnabled = true;
            this.cmbFleetTypeSelected.Items.AddRange(new object[] {
            "Selected",
            "All"});
            this.cmbFleetTypeSelected.Location = new System.Drawing.Point(220, 160);
            this.cmbFleetTypeSelected.Name = "cmbFleetTypeSelected";
            this.cmbFleetTypeSelected.Size = new System.Drawing.Size(157, 40);
            this.cmbFleetTypeSelected.TabIndex = 0;
            this.cmbFleetTypeSelected.Text = "All";
            this.cmbFleetTypeSelected.SelectedIndexChanged += new System.EventHandler(this.cmbFleetTypeSelected_SelectedIndexChanged);
            // 
            // lblFleetTypeSelected
            // 
            this.lblFleetTypeSelected.AutoSize = true;
            this.lblFleetTypeSelected.Location = new System.Drawing.Point(220, 130);
            this.lblFleetTypeSelected.Name = "lblFleetTypeSelected";
            this.lblFleetTypeSelected.Size = new System.Drawing.Size(82, 15);
            this.lblFleetTypeSelected.TabIndex = 0;
            this.lblFleetTypeSelected.Text = "FLEET TYPE";
            // 
            // cmbAnnual
            // 
            this.cmbAnnual.AutoSize = false;
            this.cmbAnnual.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAnnual.FormattingEnabled = true;
            this.cmbAnnual.Items.AddRange(new object[] {
            "Annual",
            "Long Term"});
            this.cmbAnnual.Location = new System.Drawing.Point(220, 220);
            this.cmbAnnual.Name = "cmbAnnual";
            this.cmbAnnual.Size = new System.Drawing.Size(157, 40);
            this.cmbAnnual.TabIndex = 21;
            this.cmbAnnual.Text = "Annual";
            this.cmbAnnual.SelectedIndexChanged += new System.EventHandler(this.cmbAnnual_SelectedIndexChanged);
            // 
            // cmbAll
            // 
            this.cmbAll.AutoSize = false;
            this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbAll.FormattingEnabled = true;
            this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbAll.Location = new System.Drawing.Point(20, 30);
            this.cmbAll.Name = "cmbAll";
            this.cmbAll.Size = new System.Drawing.Size(130, 40);
            this.cmbAll.TabIndex = 2;
            this.cmbAll.Text = "All";
            this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
            // 
            // cmbGroups
            // 
            this.cmbGroups.AutoSize = false;
            this.cmbGroups.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbGroups.FormattingEnabled = true;
            this.cmbGroups.Items.AddRange(new object[] {
            "Groups",
            "Fleets"});
            this.cmbGroups.Location = new System.Drawing.Point(20, 90);
            this.cmbGroups.Name = "cmbGroups";
            this.cmbGroups.Size = new System.Drawing.Size(130, 40);
            this.cmbGroups.TabIndex = 0;
            this.cmbGroups.Text = "Fleets";
            this.cmbGroups.SelectedIndexChanged += new System.EventHandler(this.cmbGroups_SelectedIndexChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.fraFleetNameSelected);
            this.Frame1.Controls.Add(this.cmbFleetNameSelected);
            this.Frame1.Location = new System.Drawing.Point(397, 130);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(255, 152);
            this.Frame1.TabIndex = 20;
            this.Frame1.Text = "Fleet Name Selection";
            // 
            // fraFleetNameSelected
            // 
            this.fraFleetNameSelected.AppearanceKey = "groupBoxNoBorders";
            this.fraFleetNameSelected.Controls.Add(this.cboStartName);
            this.fraFleetNameSelected.Controls.Add(this.cboEndName);
            this.fraFleetNameSelected.Controls.Add(this.Label2);
            this.fraFleetNameSelected.Enabled = false;
            this.fraFleetNameSelected.Location = new System.Drawing.Point(1, 71);
            this.fraFleetNameSelected.Name = "fraFleetNameSelected";
            this.fraFleetNameSelected.Size = new System.Drawing.Size(243, 79);
            this.fraFleetNameSelected.TabIndex = 23;
            // 
            // cboStartName
            // 
            this.cboStartName.AutoSize = false;
            this.cboStartName.BackColor = System.Drawing.SystemColors.Window;
            this.cboStartName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboStartName.Enabled = false;
            this.cboStartName.FormattingEnabled = true;
            this.cboStartName.Location = new System.Drawing.Point(20, 19);
            this.cboStartName.Name = "cboStartName";
            this.cboStartName.Size = new System.Drawing.Size(80, 40);
            this.cboStartName.TabIndex = 25;
            this.cboStartName.SelectedIndexChanged += new System.EventHandler(this.cboStartName_SelectedIndexChanged);
            // 
            // cboEndName
            // 
            this.cboEndName.AutoSize = false;
            this.cboEndName.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboEndName.Enabled = false;
            this.cboEndName.FormattingEnabled = true;
            this.cboEndName.Location = new System.Drawing.Point(153, 19);
            this.cboEndName.Name = "cboEndName";
            this.cboEndName.Size = new System.Drawing.Size(80, 40);
            this.cboEndName.TabIndex = 24;
            this.cboEndName.SelectedIndexChanged += new System.EventHandler(this.cboEndName_SelectedIndexChanged);
            // 
            // Label2
            // 
            this.Label2.Enabled = false;
            this.Label2.Location = new System.Drawing.Point(119, 33);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(21, 22);
            this.Label2.TabIndex = 26;
            this.Label2.Text = "TO";
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelectAll.AppearanceKey = "toolbarButton";
            this.cmdSelectAll.Location = new System.Drawing.Point(672, 29);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(75, 24);
            this.cmdSelectAll.TabIndex = 19;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Visible = false;
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // cmdClearAll
            // 
            this.cmdClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClearAll.AppearanceKey = "toolbarButton";
            this.cmdClearAll.Location = new System.Drawing.Point(753, 29);
            this.cmdClearAll.Name = "cmdClearAll";
            this.cmdClearAll.Size = new System.Drawing.Size(67, 24);
            this.cmdClearAll.TabIndex = 18;
            this.cmdClearAll.Text = "Clear All";
            this.cmdClearAll.Visible = false;
            this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
            // 
            // chkLongTermOnly
            // 
            this.chkLongTermOnly.Location = new System.Drawing.Point(338, 75);
            this.chkLongTermOnly.Name = "chkLongTermOnly";
            this.chkLongTermOnly.Size = new System.Drawing.Size(204, 27);
            this.chkLongTermOnly.TabIndex = 11;
            this.chkLongTermOnly.Text = "Long Term Trailers Only";
            this.chkLongTermOnly.Visible = false;
            // 
            // fraType
            // 
            this.fraType.Controls.Add(this.cmbGroups);
            this.fraType.Controls.Add(this.cmbAll);
            this.fraType.Location = new System.Drawing.Point(30, 130);
            this.fraType.Name = "fraType";
            this.fraType.Size = new System.Drawing.Size(170, 148);
            this.fraType.TabIndex = 4;
            this.fraType.Text = "Type Selection";
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(30, 70);
            this.txtStartDate.Mask = "##/##/####";
            this.txtStartDate.MaxLength = 10;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate.TabIndex = 0;
            this.txtStartDate.Text = "  /  /";
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(202, 70);
            this.txtEndDate.Mask = "##/##/####";
            this.txtEndDate.MaxLength = 10;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate.TabIndex = 1;
            this.txtEndDate.Text = "  /  /";
            // 
            // vsFleets
            // 
            this.vsFleets.AllowSelection = false;
            this.vsFleets.AllowUserToResizeColumns = false;
            this.vsFleets.AllowUserToResizeRows = false;
            this.vsFleets.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsFleets.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsFleets.BackColorBkg = System.Drawing.Color.Empty;
            this.vsFleets.BackColorFixed = System.Drawing.Color.Empty;
            this.vsFleets.BackColorSel = System.Drawing.Color.Empty;
            this.vsFleets.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsFleets.Cols = 3;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsFleets.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsFleets.ColumnHeadersHeight = 30;
            this.vsFleets.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsFleets.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsFleets.DragIcon = null;
            this.vsFleets.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsFleets.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsFleets.ExtendLastCol = true;
            this.vsFleets.FixedCols = 0;
            this.vsFleets.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsFleets.FrozenCols = 0;
            this.vsFleets.GridColor = System.Drawing.Color.Empty;
            this.vsFleets.GridColorFixed = System.Drawing.Color.Empty;
            this.vsFleets.Location = new System.Drawing.Point(30, 298);
            this.vsFleets.Name = "vsFleets";
            this.vsFleets.OutlineCol = 0;
            this.vsFleets.ReadOnly = true;
            this.vsFleets.RowHeadersVisible = false;
            this.vsFleets.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsFleets.RowHeightMin = 0;
            this.vsFleets.Rows = 17;
            this.vsFleets.ScrollTipText = null;
            this.vsFleets.ShowColumnVisibilityMenu = false;
            this.vsFleets.ShowFocusCell = false;
            this.vsFleets.Size = new System.Drawing.Size(622, 300);
            this.vsFleets.StandardTab = true;
            this.vsFleets.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsFleets.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsFleets.TabIndex = 10;
            this.vsFleets.Visible = false;
            this.vsFleets.KeyDown += new Wisej.Web.KeyEventHandler(this.vsFleets_KeyDownEvent);
            this.vsFleets.Click += new System.EventHandler(this.vsFleets_ClickEvent);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(450, 16);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "PLEASE ENTER THE EXPIRATION DATE RANGE YOU WISH TO PRINT LISTS FOR";
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(164, 84);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(21, 22);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "TO";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(377, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(100, 48);
            this.cmdProcess.TabIndex = 0;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmFleetExpiringListMMTA
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(848, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmFleetExpiringListMMTA";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Fleet Reminder List";
            this.Load += new System.EventHandler(this.frmFleetExpiringListMMTA_Load);
            this.Activated += new System.EventHandler(this.frmFleetExpiringListMMTA_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFleetExpiringListMMTA_KeyPress);
            this.Resize += new System.EventHandler(this.frmFleetExpiringListMMTA_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraFleetNameSelected)).EndInit();
            this.fraFleetNameSelected.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLongTermOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
            this.fraType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFleets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcess;
	}
}