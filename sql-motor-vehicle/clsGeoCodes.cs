﻿//Fecher vbPorter - Version 1.0.0.59

using System;
using fecherFoundation;
using TWSharedLibrary;

namespace TWMV0000
{
	public class clsGeoCodes
	{
		//=========================================================
		// kk11292016 tromv-1194  Maine Geocodes as of 07/25/2016
		public FCCollection OrganizedTowns = new FCCollection();
		public FCCollection UnorganizedTowns = new FCCollection();
		public FCCollection Agents = new FCCollection();

		public clsGeoCodes() : base()
		{
			FillOrganizedTowns();
			FillUnorganizedTowns();
			FillAgents();
		}

		private void FillOrganizedTowns()
		{
			clsGeoCode tGC;
			// ********** Androscoggin County **********
			// 01010 = "Auburn"
			tGC = new clsGeoCode();
			tGC.Code = "01010";
			tGC.Desc = "Auburn";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01020 = "Durham"
			tGC = new clsGeoCode();
			tGC.Code = "01020";
			tGC.Desc = "Durham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01030 = "Greene"
			tGC = new clsGeoCode();
			tGC.Code = "01030";
			tGC.Desc = "Greene";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01040 = "Leeds"
			tGC = new clsGeoCode();
			tGC.Code = "01040";
			tGC.Desc = "Leeds";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01050 = "Lewiston"
			tGC = new clsGeoCode();
			tGC.Code = "01050";
			tGC.Desc = "Lewiston";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01060 = "Lisbon"
			tGC = new clsGeoCode();
			tGC.Code = "01060";
			tGC.Desc = "Lisbon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01070 = "Livermore"
			tGC = new clsGeoCode();
			tGC.Code = "01070";
			tGC.Desc = "Livermore";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01080 = "Livermore Falls"
			tGC = new clsGeoCode();
			tGC.Code = "01080";
			tGC.Desc = "Livermore Falls";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01090 = "Mechanic Falls"
			tGC = new clsGeoCode();
			tGC.Code = "01090";
			tGC.Desc = "Mechanic Falls";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01100 = "Minot"
			tGC = new clsGeoCode();
			tGC.Code = "01100";
			tGC.Desc = "Minot";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01110 = "Poland"
			tGC = new clsGeoCode();
			tGC.Code = "01110";
			tGC.Desc = "Poland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01120 = "Turner"
			tGC = new clsGeoCode();
			tGC.Code = "01120";
			tGC.Desc = "Turner";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01130 = "Wales"
			tGC = new clsGeoCode();
			tGC.Code = "01130";
			tGC.Desc = "Wales";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 01140 = "Sabattus"
			tGC = new clsGeoCode();
			tGC.Code = "01140";
			tGC.Desc = "Sabattus";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Aroostook County **********
			// 03010 = "Allagash"
			tGC = new clsGeoCode();
			tGC.Code = "03010";
			tGC.Desc = "Allagash";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03020 = "Amity"
			tGC = new clsGeoCode();
			tGC.Code = "03020";
			tGC.Desc = "Amity";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03030 = "Ashland"
			tGC = new clsGeoCode();
			tGC.Code = "03030";
			tGC.Desc = "Ashland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03040 = "Bancroft"
			tGC = new clsGeoCode();
			tGC.Code = "03040";
			tGC.Desc = "Bancroft";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03060 = "Blaine"
			tGC = new clsGeoCode();
			tGC.Code = "03060";
			tGC.Desc = "Blaine";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03070 = "Bridgewater"
			tGC = new clsGeoCode();
			tGC.Code = "03070";
			tGC.Desc = "Bridgewater";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03080 = "Caribou"
			tGC = new clsGeoCode();
			tGC.Code = "03080";
			tGC.Desc = "Caribou";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03090 = "Cary Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03090";
			tGC.Desc = "Cary Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03100 = "Castle Hill"
			tGC = new clsGeoCode();
			tGC.Code = "03100";
			tGC.Desc = "Castle Hill";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03110 = "Caswell"
			tGC = new clsGeoCode();
			tGC.Code = "03110";
			tGC.Desc = "Caswell";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03120 = "Chapman"
			tGC = new clsGeoCode();
			tGC.Code = "03120";
			tGC.Desc = "Chapman";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03130 = "Crystal"
			tGC = new clsGeoCode();
			tGC.Code = "03130";
			tGC.Desc = "Crystal";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03140 = "Cyr Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03140";
			tGC.Desc = "Cyr Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03150 = "Dyer Brook"
			tGC = new clsGeoCode();
			tGC.Code = "03150";
			tGC.Desc = "Dyer Brook";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03170 = "Eagle Lake"
			tGC = new clsGeoCode();
			tGC.Code = "03170";
			tGC.Desc = "Eagle Lake";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03180 = "Easton"
			tGC = new clsGeoCode();
			tGC.Code = "03180";
			tGC.Desc = "Easton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03190 = "Fort Fairfield"
			tGC = new clsGeoCode();
			tGC.Code = "03190";
			tGC.Desc = "Fort Fairfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03200 = "Fort Kent"
			tGC = new clsGeoCode();
			tGC.Code = "03200";
			tGC.Desc = "Fort Kent";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03210 = "Frenchville"
			tGC = new clsGeoCode();
			tGC.Code = "03210";
			tGC.Desc = "Frenchville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03220 = "Garfield Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03220";
			tGC.Desc = "Garfield Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03230 = "Glenwood Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03230";
			tGC.Desc = "Glenwood Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03240 = "Grand Isle"
			tGC = new clsGeoCode();
			tGC.Code = "03240";
			tGC.Desc = "Grand Isle";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03250 = "Hamlin"
			tGC = new clsGeoCode();
			tGC.Code = "03250";
			tGC.Desc = "Hamlin";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03260 = "Hammond"
			tGC = new clsGeoCode();
			tGC.Code = "03260";
			tGC.Desc = "Hammond";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03270 = "Haynesville"
			tGC = new clsGeoCode();
			tGC.Code = "03270";
			tGC.Desc = "Haynesville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03280 = "Hersey"
			tGC = new clsGeoCode();
			tGC.Code = "03280";
			tGC.Desc = "Hersey";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03290 = "Hodgdon"
			tGC = new clsGeoCode();
			tGC.Code = "03290";
			tGC.Desc = "Hodgdon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03300 = "Houlton"
			tGC = new clsGeoCode();
			tGC.Code = "03300";
			tGC.Desc = "Houlton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03310 = "Island Falls"
			tGC = new clsGeoCode();
			tGC.Code = "03310";
			tGC.Desc = "Island Falls";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03320 = "Limestone"
			tGC = new clsGeoCode();
			tGC.Code = "03320";
			tGC.Desc = "Limestone";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03330 = "Linneus"
			tGC = new clsGeoCode();
			tGC.Code = "03330";
			tGC.Desc = "Linneus";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03340 = "Littleton"
			tGC = new clsGeoCode();
			tGC.Code = "03340";
			tGC.Desc = "Littleton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03350 = "Ludlow"
			tGC = new clsGeoCode();
			tGC.Code = "03350";
			tGC.Desc = "Ludlow";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03360 = "Macwahoc Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03360";
			tGC.Desc = "Macwahoc Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03370 = "Madawaska"
			tGC = new clsGeoCode();
			tGC.Code = "03370";
			tGC.Desc = "Madawaska";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03380 = "Mapleton"
			tGC = new clsGeoCode();
			tGC.Code = "03380";
			tGC.Desc = "Mapleton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03390 = "Mars Hill"
			tGC = new clsGeoCode();
			tGC.Code = "03390";
			tGC.Desc = "Mars Hill";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03400 = "Masardis"
			tGC = new clsGeoCode();
			tGC.Code = "03400";
			tGC.Desc = "Masardis";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03410 = "Merrill"
			tGC = new clsGeoCode();
			tGC.Code = "03410";
			tGC.Desc = "Merrill";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03420 = "Monticello"
			tGC = new clsGeoCode();
			tGC.Code = "03420";
			tGC.Desc = "Monticello";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03430 = "Moro Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03430";
			tGC.Desc = "Moro Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03440 = "Nashville Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03440";
			tGC.Desc = "Nashville Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03450 = "New Canada"
			tGC = new clsGeoCode();
			tGC.Code = "03450";
			tGC.Desc = "New Canada";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03460 = "New Limerick"
			tGC = new clsGeoCode();
			tGC.Code = "03460";
			tGC.Desc = "New Limerick";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03470 = "New Sweden"
			tGC = new clsGeoCode();
			tGC.Code = "03470";
			tGC.Desc = "New Sweden";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03480 = "Oakfield"
			tGC = new clsGeoCode();
			tGC.Code = "03480";
			tGC.Desc = "Oakfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03490 = "Orient"
			tGC = new clsGeoCode();
			tGC.Code = "03490";
			tGC.Desc = "Orient";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03500 = "Oxbow Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03500";
			tGC.Desc = "Oxbow Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03510 = "Perham"
			tGC = new clsGeoCode();
			tGC.Code = "03510";
			tGC.Desc = "Perham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03520 = "Portage Lake"
			tGC = new clsGeoCode();
			tGC.Code = "03520";
			tGC.Desc = "Portage Lake";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03530 = "Presque Isle"
			tGC = new clsGeoCode();
			tGC.Code = "03530";
			tGC.Desc = "Presque Isle";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03540 = "Reed Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03540";
			tGC.Desc = "Reed Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03550 = "Saint Agatha"
			tGC = new clsGeoCode();
			tGC.Code = "03550";
			tGC.Desc = "Saint Agatha";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03560 = "Saint Francis"
			tGC = new clsGeoCode();
			tGC.Code = "03560";
			tGC.Desc = "Saint Francis";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03580 = "Sherman"
			tGC = new clsGeoCode();
			tGC.Code = "03580";
			tGC.Desc = "Sherman";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03590 = "Smyrna"
			tGC = new clsGeoCode();
			tGC.Code = "03590";
			tGC.Desc = "Smyrna";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03600 = "Stockholm"
			tGC = new clsGeoCode();
			tGC.Code = "03600";
			tGC.Desc = "Stockholm";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03610 = "Van Buren"
			tGC = new clsGeoCode();
			tGC.Code = "03610";
			tGC.Desc = "Van Buren";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03620 = "Wade"
			tGC = new clsGeoCode();
			tGC.Code = "03620";
			tGC.Desc = "Wade";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03630 = "Wallagrass"
			tGC = new clsGeoCode();
			tGC.Code = "03630";
			tGC.Desc = "Wallagrass";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03640 = "Washburn"
			tGC = new clsGeoCode();
			tGC.Code = "03640";
			tGC.Desc = "Washburn";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03650 = "Westfield"
			tGC = new clsGeoCode();
			tGC.Code = "03650";
			tGC.Desc = "Westfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03660 = "Westmanland"
			tGC = new clsGeoCode();
			tGC.Code = "03660";
			tGC.Desc = "Westmanland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03670 = "Weston"
			tGC = new clsGeoCode();
			tGC.Code = "03670";
			tGC.Desc = "Weston";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03680 = "Winterville Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03680";
			tGC.Desc = "Winterville Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 03690 = "Woodland"
			tGC = new clsGeoCode();
			tGC.Code = "03690";
			tGC.Desc = "Woodland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Cumberland County **********
			// 05010 = "Baldwin"
			tGC = new clsGeoCode();
			tGC.Code = "05010";
			tGC.Desc = "Baldwin";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05020 = "Bridgton"
			tGC = new clsGeoCode();
			tGC.Code = "05020";
			tGC.Desc = "Bridgton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05030 = "Brunswick"
			tGC = new clsGeoCode();
			tGC.Code = "05030";
			tGC.Desc = "Brunswick";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05040 = "Cape Elizabeth"
			tGC = new clsGeoCode();
			tGC.Code = "05040";
			tGC.Desc = "Cape Elizabeth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05050 = "Casco"
			tGC = new clsGeoCode();
			tGC.Code = "05050";
			tGC.Desc = "Casco";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05055 = "Chebeague Island"
			tGC = new clsGeoCode();
			tGC.Code = "05055";
			tGC.Desc = "Chebeague Island";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05060 = "Cumberland"
			tGC = new clsGeoCode();
			tGC.Code = "05060";
			tGC.Desc = "Cumberland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05070 = "Falmouth"
			tGC = new clsGeoCode();
			tGC.Code = "05070";
			tGC.Desc = "Falmouth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05080 = "Freeport"
			tGC = new clsGeoCode();
			tGC.Code = "05080";
			tGC.Desc = "Freeport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05085 = "Frye Island"
			tGC = new clsGeoCode();
			tGC.Code = "05085";
			tGC.Desc = "Frye Island";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05090 = "Gorham"
			tGC = new clsGeoCode();
			tGC.Code = "05090";
			tGC.Desc = "Gorham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05100 = "Gray"
			tGC = new clsGeoCode();
			tGC.Code = "05100";
			tGC.Desc = "Gray";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05110 = "Harpswell"
			tGC = new clsGeoCode();
			tGC.Code = "05110";
			tGC.Desc = "Harpswell";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05120 = "Harrison"
			tGC = new clsGeoCode();
			tGC.Code = "05120";
			tGC.Desc = "Harrison";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05125 = "Long Island"
			tGC = new clsGeoCode();
			tGC.Code = "05125";
			tGC.Desc = "Long Island";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05130 = "Naples"
			tGC = new clsGeoCode();
			tGC.Code = "05130";
			tGC.Desc = "Naples";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05140 = "New Gloucester"
			tGC = new clsGeoCode();
			tGC.Code = "05140";
			tGC.Desc = "New Gloucester";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05150 = "North Yarmouth"
			tGC = new clsGeoCode();
			tGC.Code = "05150";
			tGC.Desc = "North Yarmouth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05170 = "Portland"
			tGC = new clsGeoCode();
			tGC.Code = "05170";
			tGC.Desc = "Portland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05180 = "Pownal"
			tGC = new clsGeoCode();
			tGC.Code = "05180";
			tGC.Desc = "Pownal";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05190 = "Raymond"
			tGC = new clsGeoCode();
			tGC.Code = "05190";
			tGC.Desc = "Raymond";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05200 = "Scarborough"
			tGC = new clsGeoCode();
			tGC.Code = "05200";
			tGC.Desc = "Scarborough";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05210 = "Sebago"
			tGC = new clsGeoCode();
			tGC.Code = "05210";
			tGC.Desc = "Sebago";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05220 = "South Portland"
			tGC = new clsGeoCode();
			tGC.Code = "05220";
			tGC.Desc = "South Portland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05230 = "Standish"
			tGC = new clsGeoCode();
			tGC.Code = "05230";
			tGC.Desc = "Standish";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05240 = "Westbrook"
			tGC = new clsGeoCode();
			tGC.Code = "05240";
			tGC.Desc = "Westbrook";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05250 = "Windham"
			tGC = new clsGeoCode();
			tGC.Code = "05250";
			tGC.Desc = "Windham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 05260 = "Yarmouth"
			tGC = new clsGeoCode();
			tGC.Code = "05260";
			tGC.Desc = "Yarmouth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Franklin County **********
			// 07010 = "Avon"
			tGC = new clsGeoCode();
			tGC.Code = "07010";
			tGC.Desc = "Avon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07018 = "Carrabassett Valley"
			tGC = new clsGeoCode();
			tGC.Code = "07018";
			tGC.Desc = "Carrabassett Valley";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07020 = "Carthage"
			tGC = new clsGeoCode();
			tGC.Code = "07020";
			tGC.Desc = "Carthage";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07030 = "Chesterville"
			tGC = new clsGeoCode();
			tGC.Code = "07030";
			tGC.Desc = "Chesterville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07040 = "Coplin Plt"
			tGC = new clsGeoCode();
			tGC.Code = "07040";
			tGC.Desc = "Coplin Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07050 = "Dallas Plt"
			tGC = new clsGeoCode();
			tGC.Code = "07050";
			tGC.Desc = "Dallas Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07060 = "Eustis"
			tGC = new clsGeoCode();
			tGC.Code = "07060";
			tGC.Desc = "Eustis";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07070 = "Farmington"
			tGC = new clsGeoCode();
			tGC.Code = "07070";
			tGC.Desc = "Farmington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07080 = "Industry"
			tGC = new clsGeoCode();
			tGC.Code = "07080";
			tGC.Desc = "Industry";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07090 = "Jay"
			tGC = new clsGeoCode();
			tGC.Code = "07090";
			tGC.Desc = "Jay";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07100 = "Kingfield"
			tGC = new clsGeoCode();
			tGC.Code = "07100";
			tGC.Desc = "Kingfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07120 = "New Sharon"
			tGC = new clsGeoCode();
			tGC.Code = "07120";
			tGC.Desc = "New Sharon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07130 = "New Vineyard"
			tGC = new clsGeoCode();
			tGC.Code = "07130";
			tGC.Desc = "New Vineyard";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07140 = "Phillips"
			tGC = new clsGeoCode();
			tGC.Code = "07140";
			tGC.Desc = "Phillips";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07150 = "Rangeley"
			tGC = new clsGeoCode();
			tGC.Code = "07150";
			tGC.Desc = "Rangeley";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07160 = "Rangeley Plt"
			tGC = new clsGeoCode();
			tGC.Code = "07160";
			tGC.Desc = "Rangeley Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07170 = "Sandy River Plt"
			tGC = new clsGeoCode();
			tGC.Code = "07170";
			tGC.Desc = "Sandy River Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07180 = "Strong"
			tGC = new clsGeoCode();
			tGC.Code = "07180";
			tGC.Desc = "Strong";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07190 = "Temple"
			tGC = new clsGeoCode();
			tGC.Code = "07190";
			tGC.Desc = "Temple";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07200 = "Weld"
			tGC = new clsGeoCode();
			tGC.Code = "07200";
			tGC.Desc = "Weld";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 07210 = "Wilton"
			tGC = new clsGeoCode();
			tGC.Code = "07210";
			tGC.Desc = "Wilton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Hancock County **********
			// 09010 = "Amherst"
			tGC = new clsGeoCode();
			tGC.Code = "09010";
			tGC.Desc = "Amherst";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09020 = "Aurora"
			tGC = new clsGeoCode();
			tGC.Code = "09020";
			tGC.Desc = "Aurora";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09030 = "Bar Harbor"
			tGC = new clsGeoCode();
			tGC.Code = "09030";
			tGC.Desc = "Bar Harbor";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09040 = "Blue Hill"
			tGC = new clsGeoCode();
			tGC.Code = "09040";
			tGC.Desc = "Blue Hill";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09050 = "Brooklin"
			tGC = new clsGeoCode();
			tGC.Code = "09050";
			tGC.Desc = "Brooklin";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09060 = "Brooksville"
			tGC = new clsGeoCode();
			tGC.Code = "09060";
			tGC.Desc = "Brooksville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09070 = "Bucksport"
			tGC = new clsGeoCode();
			tGC.Code = "09070";
			tGC.Desc = "Bucksport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09080 = "Castine"
			tGC = new clsGeoCode();
			tGC.Code = "09080";
			tGC.Desc = "Castine";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09090 = "Cranberry Isles"
			tGC = new clsGeoCode();
			tGC.Code = "09090";
			tGC.Desc = "Cranberry Isles";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09100 = "Dedham"
			tGC = new clsGeoCode();
			tGC.Code = "09100";
			tGC.Desc = "Dedham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09110 = "Deer Isle"
			tGC = new clsGeoCode();
			tGC.Code = "09110";
			tGC.Desc = "Deer Isle";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09120 = "Eastbrook"
			tGC = new clsGeoCode();
			tGC.Code = "09120";
			tGC.Desc = "Eastbrook";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09130 = "Ellsworth"
			tGC = new clsGeoCode();
			tGC.Code = "09130";
			tGC.Desc = "Ellsworth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09140 = "Franklin"
			tGC = new clsGeoCode();
			tGC.Code = "09140";
			tGC.Desc = "Franklin";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09150 = "Gouldsboro"
			tGC = new clsGeoCode();
			tGC.Code = "09150";
			tGC.Desc = "Gouldsboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09160 = "Great Pond"
			tGC = new clsGeoCode();
			tGC.Code = "09160";
			tGC.Desc = "Great Pond";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09170 = "Hancock"
			tGC = new clsGeoCode();
			tGC.Code = "09170";
			tGC.Desc = "Hancock";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09180 = "Lamoine"
			tGC = new clsGeoCode();
			tGC.Code = "09180";
			tGC.Desc = "Lamoine";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09190 = "Frenchboro"
			tGC = new clsGeoCode();
			tGC.Code = "09190";
			tGC.Desc = "Frenchboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09200 = "Mariaville"
			tGC = new clsGeoCode();
			tGC.Code = "09200";
			tGC.Desc = "Mariaville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09210 = "Mount Desert"
			tGC = new clsGeoCode();
			tGC.Code = "09210";
			tGC.Desc = "Mount Desert";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09220 = "Orland"
			tGC = new clsGeoCode();
			tGC.Code = "09220";
			tGC.Desc = "Orland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09230 = "Osborn"
			tGC = new clsGeoCode();
			tGC.Code = "09230";
			tGC.Desc = "Osborn";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09240 = "Otis"
			tGC = new clsGeoCode();
			tGC.Code = "09240";
			tGC.Desc = "Otis";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09250 = "Penobscot"
			tGC = new clsGeoCode();
			tGC.Code = "09250";
			tGC.Desc = "Penobscot";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09260 = "Sedgwick"
			tGC = new clsGeoCode();
			tGC.Code = "09260";
			tGC.Desc = "Sedgwick";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09270 = "Sorrento"
			tGC = new clsGeoCode();
			tGC.Code = "09270";
			tGC.Desc = "Sorrento";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09280 = "Southwest Harbor"
			tGC = new clsGeoCode();
			tGC.Code = "09280";
			tGC.Desc = "Southwest Harbor";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09290 = "Stonington"
			tGC = new clsGeoCode();
			tGC.Code = "09290";
			tGC.Desc = "Stonington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09300 = "Sullivan"
			tGC = new clsGeoCode();
			tGC.Code = "09300";
			tGC.Desc = "Sullivan";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09310 = "Surry"
			tGC = new clsGeoCode();
			tGC.Code = "09310";
			tGC.Desc = "Surry";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09320 = "Swans Island"
			tGC = new clsGeoCode();
			tGC.Code = "09320";
			tGC.Desc = "Swans Island";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09330 = "Tremont"
			tGC = new clsGeoCode();
			tGC.Code = "09330";
			tGC.Desc = "Tremont";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09340 = "Trenton"
			tGC = new clsGeoCode();
			tGC.Code = "09340";
			tGC.Desc = "Trenton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09350 = "Verona"
			tGC = new clsGeoCode();
			tGC.Code = "09350";
			tGC.Desc = "Verona";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09360 = "Waltham"
			tGC = new clsGeoCode();
			tGC.Code = "09360";
			tGC.Desc = "Waltham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 09370 = "Winter Harbor"
			tGC = new clsGeoCode();
			tGC.Code = "09370";
			tGC.Desc = "Winter Harbor";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Kennebec County **********
			// 11010 = "Albion"
			tGC = new clsGeoCode();
			tGC.Code = "11010";
			tGC.Desc = "Albion";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11020 = "Augusta"
			tGC = new clsGeoCode();
			tGC.Code = "11020";
			tGC.Desc = "Augusta";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11030 = "Belgrade"
			tGC = new clsGeoCode();
			tGC.Code = "11030";
			tGC.Desc = "Belgrade";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11040 = "Benton"
			tGC = new clsGeoCode();
			tGC.Code = "11040";
			tGC.Desc = "Benton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11050 = "Chelsea"
			tGC = new clsGeoCode();
			tGC.Code = "11050";
			tGC.Desc = "Chelsea";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11060 = "China"
			tGC = new clsGeoCode();
			tGC.Code = "11060";
			tGC.Desc = "China";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11070 = "Clinton"
			tGC = new clsGeoCode();
			tGC.Code = "11070";
			tGC.Desc = "Clinton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11080 = "Farmingdale"
			tGC = new clsGeoCode();
			tGC.Code = "11080";
			tGC.Desc = "Farmingdale";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11090 = "Fayette"
			tGC = new clsGeoCode();
			tGC.Code = "11090";
			tGC.Desc = "Fayette";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11100 = "Gardiner"
			tGC = new clsGeoCode();
			tGC.Code = "11100";
			tGC.Desc = "Gardiner";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11110 = "Hallowell"
			tGC = new clsGeoCode();
			tGC.Code = "11110";
			tGC.Desc = "Hallowell";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11120 = "Litchfield"
			tGC = new clsGeoCode();
			tGC.Code = "11120";
			tGC.Desc = "Litchfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11130 = "Manchester"
			tGC = new clsGeoCode();
			tGC.Code = "11130";
			tGC.Desc = "Manchester";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11140 = "Monmouth"
			tGC = new clsGeoCode();
			tGC.Code = "11140";
			tGC.Desc = "Monmouth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11150 = "Mount Vernon"
			tGC = new clsGeoCode();
			tGC.Code = "11150";
			tGC.Desc = "Mount Vernon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11160 = "Oakland"
			tGC = new clsGeoCode();
			tGC.Code = "11160";
			tGC.Desc = "Oakland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11170 = "Pittston"
			tGC = new clsGeoCode();
			tGC.Code = "11170";
			tGC.Desc = "Pittston";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11180 = "Randolph"
			tGC = new clsGeoCode();
			tGC.Code = "11180";
			tGC.Desc = "Randolph";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11190 = "Readfield"
			tGC = new clsGeoCode();
			tGC.Code = "11190";
			tGC.Desc = "Readfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11200 = "Rome"
			tGC = new clsGeoCode();
			tGC.Code = "11200";
			tGC.Desc = "Rome";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11210 = "Sidney"
			tGC = new clsGeoCode();
			tGC.Code = "11210";
			tGC.Desc = "Sidney";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11220 = "Vassalboro"
			tGC = new clsGeoCode();
			tGC.Code = "11220";
			tGC.Desc = "Vassalboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11230 = "Vienna"
			tGC = new clsGeoCode();
			tGC.Code = "11230";
			tGC.Desc = "Vienna";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11240 = "Waterville"
			tGC = new clsGeoCode();
			tGC.Code = "11240";
			tGC.Desc = "Waterville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11250 = "Wayne"
			tGC = new clsGeoCode();
			tGC.Code = "11250";
			tGC.Desc = "Wayne";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11260 = "West Gardiner"
			tGC = new clsGeoCode();
			tGC.Code = "11260";
			tGC.Desc = "West Gardiner";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11270 = "Windsor"
			tGC = new clsGeoCode();
			tGC.Code = "11270";
			tGC.Desc = "Windsor";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11280 = "Winslow"
			tGC = new clsGeoCode();
			tGC.Code = "11280";
			tGC.Desc = "Winslow";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 11290 = "Winthrop"
			tGC = new clsGeoCode();
			tGC.Code = "11290";
			tGC.Desc = "Winthrop";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Knox County **********
			// 13010 = "Appleton"
			tGC = new clsGeoCode();
			tGC.Code = "13010";
			tGC.Desc = "Appleton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13020 = "Camden"
			tGC = new clsGeoCode();
			tGC.Code = "13020";
			tGC.Desc = "Camden";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13030 = "Cushing"
			tGC = new clsGeoCode();
			tGC.Code = "13030";
			tGC.Desc = "Cushing";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13040 = "Friendship"
			tGC = new clsGeoCode();
			tGC.Code = "13040";
			tGC.Desc = "Friendship";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13050 = "Hope"
			tGC = new clsGeoCode();
			tGC.Code = "13050";
			tGC.Desc = "Hope";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13060 = "Isle au Haut"
			tGC = new clsGeoCode();
			tGC.Code = "13060";
			tGC.Desc = "Isle au Haut";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13070 = "Matinicus Isle Plt"
			tGC = new clsGeoCode();
			tGC.Code = "13070";
			tGC.Desc = "Matinicus Isle Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13080 = "North Haven"
			tGC = new clsGeoCode();
			tGC.Code = "13080";
			tGC.Desc = "North Haven";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13090 = "Owls Head"
			tGC = new clsGeoCode();
			tGC.Code = "13090";
			tGC.Desc = "Owls Head";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13100 = "Rockland"
			tGC = new clsGeoCode();
			tGC.Code = "13100";
			tGC.Desc = "Rockland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13110 = "Rockport"
			tGC = new clsGeoCode();
			tGC.Code = "13110";
			tGC.Desc = "Rockport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13120 = "Saint George"
			tGC = new clsGeoCode();
			tGC.Code = "13120";
			tGC.Desc = "Saint George";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13130 = "South Thomaston"
			tGC = new clsGeoCode();
			tGC.Code = "13130";
			tGC.Desc = "South Thomaston";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13140 = "Thomaston"
			tGC = new clsGeoCode();
			tGC.Code = "13140";
			tGC.Desc = "Thomaston";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13150 = "Union"
			tGC = new clsGeoCode();
			tGC.Code = "13150";
			tGC.Desc = "Union";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13160 = "Vinalhaven"
			tGC = new clsGeoCode();
			tGC.Code = "13160";
			tGC.Desc = "Vinalhaven";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13170 = "Warren"
			tGC = new clsGeoCode();
			tGC.Code = "13170";
			tGC.Desc = "Warren";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 13180 = "Washington"
			tGC = new clsGeoCode();
			tGC.Code = "13180";
			tGC.Desc = "Washington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Lincoln County **********
			// 15010 = "Alna"
			tGC = new clsGeoCode();
			tGC.Code = "15010";
			tGC.Desc = "Alna";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15020 = "Boothbay"
			tGC = new clsGeoCode();
			tGC.Code = "15020";
			tGC.Desc = "Boothbay";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15030 = "Boothbay Harbor"
			tGC = new clsGeoCode();
			tGC.Code = "15030";
			tGC.Desc = "Boothbay Harbor";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15040 = "Bremen"
			tGC = new clsGeoCode();
			tGC.Code = "15040";
			tGC.Desc = "Bremen";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15050 = "Bristol"
			tGC = new clsGeoCode();
			tGC.Code = "15050";
			tGC.Desc = "Bristol";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15060 = "Damariscotta"
			tGC = new clsGeoCode();
			tGC.Code = "15060";
			tGC.Desc = "Damariscotta";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15070 = "Dresden"
			tGC = new clsGeoCode();
			tGC.Code = "15070";
			tGC.Desc = "Dresden";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15080 = "Edgecomb"
			tGC = new clsGeoCode();
			tGC.Code = "15080";
			tGC.Desc = "Edgecomb";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15090 = "Jefferson"
			tGC = new clsGeoCode();
			tGC.Code = "15090";
			tGC.Desc = "Jefferson";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15110 = "Newcastle"
			tGC = new clsGeoCode();
			tGC.Code = "15110";
			tGC.Desc = "Newcastle";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15120 = "Nobleboro"
			tGC = new clsGeoCode();
			tGC.Code = "15120";
			tGC.Desc = "Nobleboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15130 = "Somerville"
			tGC = new clsGeoCode();
			tGC.Code = "15130";
			tGC.Desc = "Somerville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15140 = "South Bristol"
			tGC = new clsGeoCode();
			tGC.Code = "15140";
			tGC.Desc = "South Bristol";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15150 = "Southport"
			tGC = new clsGeoCode();
			tGC.Code = "15150";
			tGC.Desc = "Southport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15160 = "Waldoboro"
			tGC = new clsGeoCode();
			tGC.Code = "15160";
			tGC.Desc = "Waldoboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15170 = "Westport"
			tGC = new clsGeoCode();
			tGC.Code = "15170";
			tGC.Desc = "Westport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15180 = "Whitefield"
			tGC = new clsGeoCode();
			tGC.Code = "15180";
			tGC.Desc = "Whitefield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 15190 = "Wiscasset"
			tGC = new clsGeoCode();
			tGC.Code = "15190";
			tGC.Desc = "Wiscasset";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Oxford County **********
			// 17010 = "Andover"
			tGC = new clsGeoCode();
			tGC.Code = "17010";
			tGC.Desc = "Andover";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17020 = "Bethel"
			tGC = new clsGeoCode();
			tGC.Code = "17020";
			tGC.Desc = "Bethel";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17030 = "Brownfield"
			tGC = new clsGeoCode();
			tGC.Code = "17030";
			tGC.Desc = "Brownfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17040 = "Buckfield"
			tGC = new clsGeoCode();
			tGC.Code = "17040";
			tGC.Desc = "Buckfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17050 = "Byron"
			tGC = new clsGeoCode();
			tGC.Code = "17050";
			tGC.Desc = "Byron";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17060 = "Canton"
			tGC = new clsGeoCode();
			tGC.Code = "17060";
			tGC.Desc = "Canton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17070 = "Denmark"
			tGC = new clsGeoCode();
			tGC.Code = "17070";
			tGC.Desc = "Denmark";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17080 = "Dixfield"
			tGC = new clsGeoCode();
			tGC.Code = "17080";
			tGC.Desc = "Dixfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17090 = "Fryeburg"
			tGC = new clsGeoCode();
			tGC.Code = "17090";
			tGC.Desc = "Fryeburg";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17100 = "Gilead"
			tGC = new clsGeoCode();
			tGC.Code = "17100";
			tGC.Desc = "Gilead";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17110 = "Greenwood"
			tGC = new clsGeoCode();
			tGC.Code = "17110";
			tGC.Desc = "Greenwood";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17120 = "Hanover"
			tGC = new clsGeoCode();
			tGC.Code = "17120";
			tGC.Desc = "Hanover";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17130 = "Hartford"
			tGC = new clsGeoCode();
			tGC.Code = "17130";
			tGC.Desc = "Hartford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17140 = "Hebron"
			tGC = new clsGeoCode();
			tGC.Code = "17140";
			tGC.Desc = "Hebron";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17150 = "Hiram"
			tGC = new clsGeoCode();
			tGC.Code = "17150";
			tGC.Desc = "Hiram";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17160 = "Lincoln Plt"
			tGC = new clsGeoCode();
			tGC.Code = "17160";
			tGC.Desc = "Lincoln Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17170 = "Lovell"
			tGC = new clsGeoCode();
			tGC.Code = "17170";
			tGC.Desc = "Lovell";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17180 = "Magalloway Plt"
			tGC = new clsGeoCode();
			tGC.Code = "17180";
			tGC.Desc = "Magalloway Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17190 = "Mexico"
			tGC = new clsGeoCode();
			tGC.Code = "17190";
			tGC.Desc = "Mexico";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17200 = "Newry"
			tGC = new clsGeoCode();
			tGC.Code = "17200";
			tGC.Desc = "Newry";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17210 = "Norway"
			tGC = new clsGeoCode();
			tGC.Code = "17210";
			tGC.Desc = "Norway";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17217 = "Otisfield"
			tGC = new clsGeoCode();
			tGC.Code = "17217";
			tGC.Desc = "Otisfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17220 = "Oxford"
			tGC = new clsGeoCode();
			tGC.Code = "17220";
			tGC.Desc = "Oxford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17230 = "Paris"
			tGC = new clsGeoCode();
			tGC.Code = "17230";
			tGC.Desc = "Paris";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17240 = "Peru"
			tGC = new clsGeoCode();
			tGC.Code = "17240";
			tGC.Desc = "Peru";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17250 = "Porter"
			tGC = new clsGeoCode();
			tGC.Code = "17250";
			tGC.Desc = "Porter";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17260 = "Roxbury"
			tGC = new clsGeoCode();
			tGC.Code = "17260";
			tGC.Desc = "Roxbury";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17270 = "Rumford"
			tGC = new clsGeoCode();
			tGC.Code = "17270";
			tGC.Desc = "Rumford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17280 = "Stoneham"
			tGC = new clsGeoCode();
			tGC.Code = "17280";
			tGC.Desc = "Stoneham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17290 = "Stow"
			tGC = new clsGeoCode();
			tGC.Code = "17290";
			tGC.Desc = "Stow";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17300 = "Sumner"
			tGC = new clsGeoCode();
			tGC.Code = "17300";
			tGC.Desc = "Sumner";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17310 = "Sweden"
			tGC = new clsGeoCode();
			tGC.Code = "17310";
			tGC.Desc = "Sweden";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17320 = "Upton"
			tGC = new clsGeoCode();
			tGC.Code = "17320";
			tGC.Desc = "Upton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17330 = "Waterford"
			tGC = new clsGeoCode();
			tGC.Code = "17330";
			tGC.Desc = "Waterford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17340 = "West Paris"
			tGC = new clsGeoCode();
			tGC.Code = "17340";
			tGC.Desc = "West Paris";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 17350 = "Woodstock"
			tGC = new clsGeoCode();
			tGC.Code = "17350";
			tGC.Desc = "Woodstock";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Penobscot County **********
			// 19010 = "Alton"
			tGC = new clsGeoCode();
			tGC.Code = "19010";
			tGC.Desc = "Alton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19020 = "Bangor"
			tGC = new clsGeoCode();
			tGC.Code = "19020";
			tGC.Desc = "Bangor";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19030 = "Bradford"
			tGC = new clsGeoCode();
			tGC.Code = "19030";
			tGC.Desc = "Bradford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19040 = "Bradley"
			tGC = new clsGeoCode();
			tGC.Code = "19040";
			tGC.Desc = "Bradley";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19050 = "Brewer"
			tGC = new clsGeoCode();
			tGC.Code = "19050";
			tGC.Desc = "Brewer";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19060 = "Burlington"
			tGC = new clsGeoCode();
			tGC.Code = "19060";
			tGC.Desc = "Burlington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19070 = "Carmel"
			tGC = new clsGeoCode();
			tGC.Code = "19070";
			tGC.Desc = "Carmel";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19080 = "Carroll Plt"
			tGC = new clsGeoCode();
			tGC.Code = "19080";
			tGC.Desc = "Carroll Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19090 = "Charleston"
			tGC = new clsGeoCode();
			tGC.Code = "19090";
			tGC.Desc = "Charleston";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19100 = "Chester"
			tGC = new clsGeoCode();
			tGC.Code = "19100";
			tGC.Desc = "Chester";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19110 = "Clifton"
			tGC = new clsGeoCode();
			tGC.Code = "19110";
			tGC.Desc = "Clifton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19120 = "Corinna"
			tGC = new clsGeoCode();
			tGC.Code = "19120";
			tGC.Desc = "Corinna";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19130 = "Corinth"
			tGC = new clsGeoCode();
			tGC.Code = "19130";
			tGC.Desc = "Corinth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19140 = "Dexter"
			tGC = new clsGeoCode();
			tGC.Code = "19140";
			tGC.Desc = "Dexter";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19150 = "Dixmont"
			tGC = new clsGeoCode();
			tGC.Code = "19150";
			tGC.Desc = "Dixmont";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19160 = "Drew Plt"
			tGC = new clsGeoCode();
			tGC.Code = "19160";
			tGC.Desc = "Drew Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19170 = "East Millinocket"
			tGC = new clsGeoCode();
			tGC.Code = "19170";
			tGC.Desc = "East Millinocket";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19180 = "Eddington"
			tGC = new clsGeoCode();
			tGC.Code = "19180";
			tGC.Desc = "Eddington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19190 = "Edinburg"
			tGC = new clsGeoCode();
			tGC.Code = "19190";
			tGC.Desc = "Edinburg";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19200 = "Enfield"
			tGC = new clsGeoCode();
			tGC.Code = "19200";
			tGC.Desc = "Enfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19210 = "Etna"
			tGC = new clsGeoCode();
			tGC.Code = "19210";
			tGC.Desc = "Etna";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19220 = "Exeter"
			tGC = new clsGeoCode();
			tGC.Code = "19220";
			tGC.Desc = "Exeter";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19230 = "Garland"
			tGC = new clsGeoCode();
			tGC.Code = "19230";
			tGC.Desc = "Garland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19240 = "Glenburn"
			tGC = new clsGeoCode();
			tGC.Code = "19240";
			tGC.Desc = "Glenburn";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19260 = "Greenbush"
			tGC = new clsGeoCode();
			tGC.Code = "19260";
			tGC.Desc = "Greenbush";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19280 = "Hampden"
			tGC = new clsGeoCode();
			tGC.Code = "19280";
			tGC.Desc = "Hampden";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19290 = "Hermon"
			tGC = new clsGeoCode();
			tGC.Code = "19290";
			tGC.Desc = "Hermon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19300 = "Holden"
			tGC = new clsGeoCode();
			tGC.Code = "19300";
			tGC.Desc = "Holden";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19310 = "Howland"
			tGC = new clsGeoCode();
			tGC.Code = "19310";
			tGC.Desc = "Howland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19320 = "Hudson"
			tGC = new clsGeoCode();
			tGC.Code = "19320";
			tGC.Desc = "Hudson";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19330 = "Kenduskeag"
			tGC = new clsGeoCode();
			tGC.Code = "19330";
			tGC.Desc = "Kenduskeag";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19340 = "Lagrange"
			tGC = new clsGeoCode();
			tGC.Code = "19340";
			tGC.Desc = "Lagrange";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19350 = "Lakeville"
			tGC = new clsGeoCode();
			tGC.Code = "19350";
			tGC.Desc = "Lakeville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19360 = "Lee"
			tGC = new clsGeoCode();
			tGC.Code = "19360";
			tGC.Desc = "Lee";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19370 = "Levant"
			tGC = new clsGeoCode();
			tGC.Code = "19370";
			tGC.Desc = "Levant";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19380 = "Lincoln"
			tGC = new clsGeoCode();
			tGC.Code = "19380";
			tGC.Desc = "Lincoln";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19390 = "Lowell"
			tGC = new clsGeoCode();
			tGC.Code = "19390";
			tGC.Desc = "Lowell";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19400 = "Mattawamkeag"
			tGC = new clsGeoCode();
			tGC.Code = "19400";
			tGC.Desc = "Mattawamkeag";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19410 = "Maxfield"
			tGC = new clsGeoCode();
			tGC.Code = "19410";
			tGC.Desc = "Maxfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19420 = "Medway"
			tGC = new clsGeoCode();
			tGC.Code = "19420";
			tGC.Desc = "Medway";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19430 = "Milford"
			tGC = new clsGeoCode();
			tGC.Code = "19430";
			tGC.Desc = "Milford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19440 = "Millinocket"
			tGC = new clsGeoCode();
			tGC.Code = "19440";
			tGC.Desc = "Millinocket";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19450 = "Mount Chase Plantation"
			tGC = new clsGeoCode();
			tGC.Code = "19450";
			tGC.Desc = "Mount Chase Plantation";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19460 = "Newburgh"
			tGC = new clsGeoCode();
			tGC.Code = "19460";
			tGC.Desc = "Newburgh";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19470 = "Newport"
			tGC = new clsGeoCode();
			tGC.Code = "19470";
			tGC.Desc = "Newport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19480 = "Old Town"
			tGC = new clsGeoCode();
			tGC.Code = "19480";
			tGC.Desc = "Old Town";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19490 = "Orono"
			tGC = new clsGeoCode();
			tGC.Code = "19490";
			tGC.Desc = "Orono";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19500 = "Orrington"
			tGC = new clsGeoCode();
			tGC.Code = "19500";
			tGC.Desc = "Orrington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19510 = "Passadumkeag"
			tGC = new clsGeoCode();
			tGC.Code = "19510";
			tGC.Desc = "Passadumkeag";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19520 = "Patten"
			tGC = new clsGeoCode();
			tGC.Code = "19520";
			tGC.Desc = "Patten";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19530 = "Plymouth"
			tGC = new clsGeoCode();
			tGC.Code = "19530";
			tGC.Desc = "Plymouth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19550 = "Seboeis Plt"
			tGC = new clsGeoCode();
			tGC.Code = "19550";
			tGC.Desc = "Seboeis Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19560 = "Springfield"
			tGC = new clsGeoCode();
			tGC.Code = "19560";
			tGC.Desc = "Springfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19570 = "Stacyville"
			tGC = new clsGeoCode();
			tGC.Code = "19570";
			tGC.Desc = "Stacyville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19580 = "Stetson"
			tGC = new clsGeoCode();
			tGC.Code = "19580";
			tGC.Desc = "Stetson";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19590 = "Veazie"
			tGC = new clsGeoCode();
			tGC.Code = "19590";
			tGC.Desc = "Veazie";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19600 = "Webster Plt"
			tGC = new clsGeoCode();
			tGC.Code = "19600";
			tGC.Desc = "Webster Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19610 = "Winn"
			tGC = new clsGeoCode();
			tGC.Code = "19610";
			tGC.Desc = "Winn";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19620 = "Woodville"
			tGC = new clsGeoCode();
			tGC.Code = "19620";
			tGC.Desc = "Woodville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 19630 = "Indian Island"
			tGC = new clsGeoCode();
			tGC.Code = "19630";
			tGC.Desc = "Indian Island";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Piscataquis County **********
			// 21010 = "Abbot"
			tGC = new clsGeoCode();
			tGC.Code = "21010";
			tGC.Desc = "Abbot";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21020 = "Atkinson"
			tGC = new clsGeoCode();
			tGC.Code = "21020";
			tGC.Desc = "Atkinson";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21037 = "Beaver Cove"
			tGC = new clsGeoCode();
			tGC.Code = "21037";
			tGC.Desc = "Beaver Cove";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21050 = "Bowerbank"
			tGC = new clsGeoCode();
			tGC.Code = "21050";
			tGC.Desc = "Bowerbank";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21060 = "Brownville"
			tGC = new clsGeoCode();
			tGC.Code = "21060";
			tGC.Desc = "Brownville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21070 = "Dover -Foxcroft"
			tGC = new clsGeoCode();
			tGC.Code = "21070";
			tGC.Desc = "Dover -Foxcroft";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21090 = "Greenville"
			tGC = new clsGeoCode();
			tGC.Code = "21090";
			tGC.Desc = "Greenville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21100 = "Guilford"
			tGC = new clsGeoCode();
			tGC.Code = "21100";
			tGC.Desc = "Guilford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21110 = "Kingsbury Plt"
			tGC = new clsGeoCode();
			tGC.Code = "21110";
			tGC.Desc = "Kingsbury Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21120 = "Lake View Plt"
			tGC = new clsGeoCode();
			tGC.Code = "21120";
			tGC.Desc = "Lake View Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21130 = "Medford"
			tGC = new clsGeoCode();
			tGC.Code = "21130";
			tGC.Desc = "Medford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21140 = "Milo"
			tGC = new clsGeoCode();
			tGC.Code = "21140";
			tGC.Desc = "Milo";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21150 = "Monson"
			tGC = new clsGeoCode();
			tGC.Code = "21150";
			tGC.Desc = "Monson";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21160 = "Parkman"
			tGC = new clsGeoCode();
			tGC.Code = "21160";
			tGC.Desc = "Parkman";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21170 = "Sangerville"
			tGC = new clsGeoCode();
			tGC.Code = "21170";
			tGC.Desc = "Sangerville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21180 = "Sebec"
			tGC = new clsGeoCode();
			tGC.Code = "21180";
			tGC.Desc = "Sebec";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21190 = "Shirley"
			tGC = new clsGeoCode();
			tGC.Code = "21190";
			tGC.Desc = "Shirley";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21200 = "Wellington"
			tGC = new clsGeoCode();
			tGC.Code = "21200";
			tGC.Desc = "Wellington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 21210 = "Willimantic"
			tGC = new clsGeoCode();
			tGC.Code = "21210";
			tGC.Desc = "Willimantic";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Sagadahoc County **********
			// 23010 = "Arrowsic"
			tGC = new clsGeoCode();
			tGC.Code = "23010";
			tGC.Desc = "Arrowsic";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23020 = "Bath"
			tGC = new clsGeoCode();
			tGC.Code = "23020";
			tGC.Desc = "Bath";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23030 = "Bowdoin"
			tGC = new clsGeoCode();
			tGC.Code = "23030";
			tGC.Desc = "Bowdoin";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23040 = "Bowdoinham"
			tGC = new clsGeoCode();
			tGC.Code = "23040";
			tGC.Desc = "Bowdoinham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23050 = "Georgetown"
			tGC = new clsGeoCode();
			tGC.Code = "23050";
			tGC.Desc = "Georgetown";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23060 = "Phippsburg"
			tGC = new clsGeoCode();
			tGC.Code = "23060";
			tGC.Desc = "Phippsburg";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23070 = "Richmond"
			tGC = new clsGeoCode();
			tGC.Code = "23070";
			tGC.Desc = "Richmond";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23080 = "Topsham"
			tGC = new clsGeoCode();
			tGC.Code = "23080";
			tGC.Desc = "Topsham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23090 = "West Bath"
			tGC = new clsGeoCode();
			tGC.Code = "23090";
			tGC.Desc = "West Bath";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 23100 = "Woolwich"
			tGC = new clsGeoCode();
			tGC.Code = "23100";
			tGC.Desc = "Woolwich";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Somerset County **********
			// 25010 = "Anson"
			tGC = new clsGeoCode();
			tGC.Code = "25010";
			tGC.Desc = "Anson";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25020 = "Athens"
			tGC = new clsGeoCode();
			tGC.Code = "25020";
			tGC.Desc = "Athens";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25030 = "Bingham"
			tGC = new clsGeoCode();
			tGC.Code = "25030";
			tGC.Desc = "Bingham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25040 = "Brighton Plt"
			tGC = new clsGeoCode();
			tGC.Code = "25040";
			tGC.Desc = "Brighton Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25050 = "Cambridge"
			tGC = new clsGeoCode();
			tGC.Code = "25050";
			tGC.Desc = "Cambridge";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25060 = "Canaan"
			tGC = new clsGeoCode();
			tGC.Code = "25060";
			tGC.Desc = "Canaan";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25070 = "Caratunk"
			tGC = new clsGeoCode();
			tGC.Code = "25070";
			tGC.Desc = "Caratunk";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25080 = "Cornville"
			tGC = new clsGeoCode();
			tGC.Code = "25080";
			tGC.Desc = "Cornville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25090 = "Dennistown Plt"
			tGC = new clsGeoCode();
			tGC.Code = "25090";
			tGC.Desc = "Dennistown Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25100 = "Detroit"
			tGC = new clsGeoCode();
			tGC.Code = "25100";
			tGC.Desc = "Detroit";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25110 = "Embden"
			tGC = new clsGeoCode();
			tGC.Code = "25110";
			tGC.Desc = "Embden";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25120 = "Fairfield"
			tGC = new clsGeoCode();
			tGC.Code = "25120";
			tGC.Desc = "Fairfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25130 = "Harmony"
			tGC = new clsGeoCode();
			tGC.Code = "25130";
			tGC.Desc = "Harmony";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25140 = "Hartland"
			tGC = new clsGeoCode();
			tGC.Code = "25140";
			tGC.Desc = "Hartland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25150 = "Highland Plt"
			tGC = new clsGeoCode();
			tGC.Code = "25150";
			tGC.Desc = "Highland Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25160 = "Jackman"
			tGC = new clsGeoCode();
			tGC.Code = "25160";
			tGC.Desc = "Jackman";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25170 = "Madison"
			tGC = new clsGeoCode();
			tGC.Code = "25170";
			tGC.Desc = "Madison";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25180 = "Mercer"
			tGC = new clsGeoCode();
			tGC.Code = "25180";
			tGC.Desc = "Mercer";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25190 = "Moose River"
			tGC = new clsGeoCode();
			tGC.Code = "25190";
			tGC.Desc = "Moose River";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25200 = "Moscow"
			tGC = new clsGeoCode();
			tGC.Code = "25200";
			tGC.Desc = "Moscow";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25210 = "New Portland"
			tGC = new clsGeoCode();
			tGC.Code = "25210";
			tGC.Desc = "New Portland";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25220 = "Norridgewock"
			tGC = new clsGeoCode();
			tGC.Code = "25220";
			tGC.Desc = "Norridgewock";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25230 = "Palmyra"
			tGC = new clsGeoCode();
			tGC.Code = "25230";
			tGC.Desc = "Palmyra";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25240 = "Pittsfield"
			tGC = new clsGeoCode();
			tGC.Code = "25240";
			tGC.Desc = "Pittsfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25250 = "Pleasant Ridge Plt"
			tGC = new clsGeoCode();
			tGC.Code = "25250";
			tGC.Desc = "Pleasant Ridge Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25260 = "Ripley"
			tGC = new clsGeoCode();
			tGC.Code = "25260";
			tGC.Desc = "Ripley";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25270 = "Saint Albans"
			tGC = new clsGeoCode();
			tGC.Code = "25270";
			tGC.Desc = "Saint Albans";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25280 = "Skowhegan"
			tGC = new clsGeoCode();
			tGC.Code = "25280";
			tGC.Desc = "Skowhegan";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25290 = "Smithfield"
			tGC = new clsGeoCode();
			tGC.Code = "25290";
			tGC.Desc = "Smithfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25300 = "Solon"
			tGC = new clsGeoCode();
			tGC.Code = "25300";
			tGC.Desc = "Solon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25310 = "Starks"
			tGC = new clsGeoCode();
			tGC.Code = "25310";
			tGC.Desc = "Starks";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25320 = "The Forks Plt"
			tGC = new clsGeoCode();
			tGC.Code = "25320";
			tGC.Desc = "The Forks Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 25330 = "West Forks Plt"
			tGC = new clsGeoCode();
			tGC.Code = "25330";
			tGC.Desc = "West Forks Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Waldo County **********
			// 27010 = "Belfast"
			tGC = new clsGeoCode();
			tGC.Code = "27010";
			tGC.Desc = "Belfast";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27020 = "Belmont"
			tGC = new clsGeoCode();
			tGC.Code = "27020";
			tGC.Desc = "Belmont";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27030 = "Brooks"
			tGC = new clsGeoCode();
			tGC.Code = "27030";
			tGC.Desc = "Brooks";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27040 = "Burnham"
			tGC = new clsGeoCode();
			tGC.Code = "27040";
			tGC.Desc = "Burnham";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27050 = "Frankfort"
			tGC = new clsGeoCode();
			tGC.Code = "27050";
			tGC.Desc = "Frankfort";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27060 = "Freedom"
			tGC = new clsGeoCode();
			tGC.Code = "27060";
			tGC.Desc = "Freedom";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27070 = "Islesboro"
			tGC = new clsGeoCode();
			tGC.Code = "27070";
			tGC.Desc = "Islesboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27080 = "Jackson"
			tGC = new clsGeoCode();
			tGC.Code = "27080";
			tGC.Desc = "Jackson";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27090 = "Knox"
			tGC = new clsGeoCode();
			tGC.Code = "27090";
			tGC.Desc = "Knox";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27100 = "Liberty"
			tGC = new clsGeoCode();
			tGC.Code = "27100";
			tGC.Desc = "Liberty";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27110 = "Lincolnville"
			tGC = new clsGeoCode();
			tGC.Code = "27110";
			tGC.Desc = "Lincolnville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27120 = "Monroe"
			tGC = new clsGeoCode();
			tGC.Code = "27120";
			tGC.Desc = "Monroe";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27130 = "Montville"
			tGC = new clsGeoCode();
			tGC.Code = "27130";
			tGC.Desc = "Montville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27140 = "Morrill"
			tGC = new clsGeoCode();
			tGC.Code = "27140";
			tGC.Desc = "Morrill";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27150 = "Northport"
			tGC = new clsGeoCode();
			tGC.Code = "27150";
			tGC.Desc = "Northport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27160 = "Palermo"
			tGC = new clsGeoCode();
			tGC.Code = "27160";
			tGC.Desc = "Palermo";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27170 = "Prospect"
			tGC = new clsGeoCode();
			tGC.Code = "27170";
			tGC.Desc = "Prospect";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27180 = "Searsmont"
			tGC = new clsGeoCode();
			tGC.Code = "27180";
			tGC.Desc = "Searsmont";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27190 = "Searsport"
			tGC = new clsGeoCode();
			tGC.Code = "27190";
			tGC.Desc = "Searsport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27200 = "Stockton Springs"
			tGC = new clsGeoCode();
			tGC.Code = "27200";
			tGC.Desc = "Stockton Springs";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27210 = "Swanville"
			tGC = new clsGeoCode();
			tGC.Code = "27210";
			tGC.Desc = "Swanville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27220 = "Thorndike"
			tGC = new clsGeoCode();
			tGC.Code = "27220";
			tGC.Desc = "Thorndike";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27230 = "Troy"
			tGC = new clsGeoCode();
			tGC.Code = "27230";
			tGC.Desc = "Troy";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27240 = "Unity"
			tGC = new clsGeoCode();
			tGC.Code = "27240";
			tGC.Desc = "Unity";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27250 = "Waldo"
			tGC = new clsGeoCode();
			tGC.Code = "27250";
			tGC.Desc = "Waldo";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 27260 = "Winterport"
			tGC = new clsGeoCode();
			tGC.Code = "27260";
			tGC.Desc = "Winterport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** Washington County **********
			// 29010 = "Addison"
			tGC = new clsGeoCode();
			tGC.Code = "29010";
			tGC.Desc = "Addison";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29020 = "Alexander"
			tGC = new clsGeoCode();
			tGC.Code = "29020";
			tGC.Desc = "Alexander";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29030 = "Baileyville"
			tGC = new clsGeoCode();
			tGC.Code = "29030";
			tGC.Desc = "Baileyville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29040 = "Baring Plt"
			tGC = new clsGeoCode();
			tGC.Code = "29040";
			tGC.Desc = "Baring Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29050 = "Beals"
			tGC = new clsGeoCode();
			tGC.Code = "29050";
			tGC.Desc = "Beals";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29060 = "Beddington"
			tGC = new clsGeoCode();
			tGC.Code = "29060";
			tGC.Desc = "Beddington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29070 = "Calais"
			tGC = new clsGeoCode();
			tGC.Code = "29070";
			tGC.Desc = "Calais";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29090 = "Charlotte"
			tGC = new clsGeoCode();
			tGC.Code = "29090";
			tGC.Desc = "Charlotte";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29100 = "Cherryfield"
			tGC = new clsGeoCode();
			tGC.Code = "29100";
			tGC.Desc = "Cherryfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29110 = "Codyville Plt"
			tGC = new clsGeoCode();
			tGC.Code = "29110";
			tGC.Desc = "Codyville Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29120 = "Columbia"
			tGC = new clsGeoCode();
			tGC.Code = "29120";
			tGC.Desc = "Columbia";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29130 = "Columbia Falls"
			tGC = new clsGeoCode();
			tGC.Code = "29130";
			tGC.Desc = "Columbia Falls";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29140 = "Cooper"
			tGC = new clsGeoCode();
			tGC.Code = "29140";
			tGC.Desc = "Cooper";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29150 = "Crawford"
			tGC = new clsGeoCode();
			tGC.Code = "29150";
			tGC.Desc = "Crawford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29160 = "Cutler"
			tGC = new clsGeoCode();
			tGC.Code = "29160";
			tGC.Desc = "Cutler";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29170 = "Danforth"
			tGC = new clsGeoCode();
			tGC.Code = "29170";
			tGC.Desc = "Danforth";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29180 = "Deblois"
			tGC = new clsGeoCode();
			tGC.Code = "29180";
			tGC.Desc = "Deblois";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29190 = "Dennysville"
			tGC = new clsGeoCode();
			tGC.Code = "29190";
			tGC.Desc = "Dennysville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29200 = "East Machias"
			tGC = new clsGeoCode();
			tGC.Code = "29200";
			tGC.Desc = "East Machias";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29210 = "Eastport"
			tGC = new clsGeoCode();
			tGC.Code = "29210";
			tGC.Desc = "Eastport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29220 = "Grand Lake Stream Plt"
			tGC = new clsGeoCode();
			tGC.Code = "29220";
			tGC.Desc = "Grand Lake Stream Plt";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29230 = "Harrington"
			tGC = new clsGeoCode();
			tGC.Code = "29230";
			tGC.Desc = "Harrington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29240 = "Jonesboro"
			tGC = new clsGeoCode();
			tGC.Code = "29240";
			tGC.Desc = "Jonesboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29250 = "Jonesport"
			tGC = new clsGeoCode();
			tGC.Code = "29250";
			tGC.Desc = "Jonesport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29260 = "Lubec"
			tGC = new clsGeoCode();
			tGC.Code = "29260";
			tGC.Desc = "Lubec";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29270 = "Machias"
			tGC = new clsGeoCode();
			tGC.Code = "29270";
			tGC.Desc = "Machias";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29280 = "Machiasport"
			tGC = new clsGeoCode();
			tGC.Code = "29280";
			tGC.Desc = "Machiasport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29290 = "Marshfield"
			tGC = new clsGeoCode();
			tGC.Code = "29290";
			tGC.Desc = "Marshfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29300 = "Meddybemps"
			tGC = new clsGeoCode();
			tGC.Code = "29300";
			tGC.Desc = "Meddybemps";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29310 = "Milbridge"
			tGC = new clsGeoCode();
			tGC.Code = "29310";
			tGC.Desc = "Milbridge";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29320 = "Northfield"
			tGC = new clsGeoCode();
			tGC.Code = "29320";
			tGC.Desc = "Northfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29350 = "Pembroke"
			tGC = new clsGeoCode();
			tGC.Code = "29350";
			tGC.Desc = "Pembroke";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29360 = "Perry"
			tGC = new clsGeoCode();
			tGC.Code = "29360";
			tGC.Desc = "Perry";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29370 = "Princeton"
			tGC = new clsGeoCode();
			tGC.Code = "29370";
			tGC.Desc = "Princeton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29380 = "Robbinston"
			tGC = new clsGeoCode();
			tGC.Code = "29380";
			tGC.Desc = "Robbinston";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29390 = "Roque Bluffs"
			tGC = new clsGeoCode();
			tGC.Code = "29390";
			tGC.Desc = "Roque Bluffs";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29400 = "Steuben"
			tGC = new clsGeoCode();
			tGC.Code = "29400";
			tGC.Desc = "Steuben";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29410 = "Talmadge"
			tGC = new clsGeoCode();
			tGC.Code = "29410";
			tGC.Desc = "Talmadge";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29420 = "Topsfield"
			tGC = new clsGeoCode();
			tGC.Code = "29420";
			tGC.Desc = "Topsfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29430 = "Vanceboro"
			tGC = new clsGeoCode();
			tGC.Code = "29430";
			tGC.Desc = "Vanceboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29440 = "Waite"
			tGC = new clsGeoCode();
			tGC.Code = "29440";
			tGC.Desc = "Waite";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29450 = "Wesley"
			tGC = new clsGeoCode();
			tGC.Code = "29450";
			tGC.Desc = "Wesley";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29460 = "Whiting"
			tGC = new clsGeoCode();
			tGC.Code = "29460";
			tGC.Desc = "Whiting";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 29470 = "Whitneyville"
			tGC = new clsGeoCode();
			tGC.Code = "29470";
			tGC.Desc = "Whitneyville";
			OrganizedTowns.Add(tGC, tGC.Code);
			// ********** York County **********
			// 31010 = "Acton"
			tGC = new clsGeoCode();
			tGC.Code = "31010";
			tGC.Desc = "Acton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31020 = "Alfred"
			tGC = new clsGeoCode();
			tGC.Code = "31020";
			tGC.Desc = "Alfred";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31030 = "Arundel"
			tGC = new clsGeoCode();
			tGC.Code = "31030";
			tGC.Desc = "Arundel";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31040 = "Berwick"
			tGC = new clsGeoCode();
			tGC.Code = "31040";
			tGC.Desc = "Berwick";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31050 = "Biddeford"
			tGC = new clsGeoCode();
			tGC.Code = "31050";
			tGC.Desc = "Biddeford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31060 = "Buxton"
			tGC = new clsGeoCode();
			tGC.Code = "31060";
			tGC.Desc = "Buxton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31070 = "Cornish"
			tGC = new clsGeoCode();
			tGC.Code = "31070";
			tGC.Desc = "Cornish";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31080 = "Dayton"
			tGC = new clsGeoCode();
			tGC.Code = "31080";
			tGC.Desc = "Dayton";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31090 = "Eliot"
			tGC = new clsGeoCode();
			tGC.Code = "31090";
			tGC.Desc = "Eliot";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31100 = "Hollis"
			tGC = new clsGeoCode();
			tGC.Code = "31100";
			tGC.Desc = "Hollis";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31110 = "Kennebunk"
			tGC = new clsGeoCode();
			tGC.Code = "31110";
			tGC.Desc = "Kennebunk";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31120 = "Kennebunkport"
			tGC = new clsGeoCode();
			tGC.Code = "31120";
			tGC.Desc = "Kennebunkport";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31130 = "Kittery"
			tGC = new clsGeoCode();
			tGC.Code = "31130";
			tGC.Desc = "Kittery";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31140 = "Lebanon"
			tGC = new clsGeoCode();
			tGC.Code = "31140";
			tGC.Desc = "Lebanon";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31150 = "Limerick"
			tGC = new clsGeoCode();
			tGC.Code = "31150";
			tGC.Desc = "Limerick";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31160 = "Limington"
			tGC = new clsGeoCode();
			tGC.Code = "31160";
			tGC.Desc = "Limington";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31170 = "Lyman"
			tGC = new clsGeoCode();
			tGC.Code = "31170";
			tGC.Desc = "Lyman";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31180 = "NewField"
			tGC = new clsGeoCode();
			tGC.Code = "31180";
			tGC.Desc = "NewField";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31190 = "North Berwick"
			tGC = new clsGeoCode();
			tGC.Code = "31190";
			tGC.Desc = "North Berwick";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31197 = "Ogunquit"
			tGC = new clsGeoCode();
			tGC.Code = "31197";
			tGC.Desc = "Ogunquit";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31200 = "Old Orchard Beach"
			tGC = new clsGeoCode();
			tGC.Code = "31200";
			tGC.Desc = "Old Orchard Beach";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31210 = "Parsonsfield"
			tGC = new clsGeoCode();
			tGC.Code = "31210";
			tGC.Desc = "Parsonsfield";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31220 = "Saco"
			tGC = new clsGeoCode();
			tGC.Code = "31220";
			tGC.Desc = "Saco";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31230 = "Sanford"
			tGC = new clsGeoCode();
			tGC.Code = "31230";
			tGC.Desc = "Sanford";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31240 = "Shapleigh"
			tGC = new clsGeoCode();
			tGC.Code = "31240";
			tGC.Desc = "Shapleigh";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31250 = "South Berwick"
			tGC = new clsGeoCode();
			tGC.Code = "31250";
			tGC.Desc = "South Berwick";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31260 = "Waterboro"
			tGC = new clsGeoCode();
			tGC.Code = "31260";
			tGC.Desc = "Waterboro";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31270 = "Wells"
			tGC = new clsGeoCode();
			tGC.Code = "31270";
			tGC.Desc = "Wells";
			OrganizedTowns.Add(tGC, tGC.Code);
			// 31280 = "York"
			tGC = new clsGeoCode();
			tGC.Code = "31280";
			tGC.Desc = "York";
			OrganizedTowns.Add(tGC, tGC.Code);
		}

		private void FillUnorganizedTowns()
		{
			clsGeoCode tGC;
			// ********** Androscoggin County **********
			// No Unorganized Towns
			// ********** Aroostook County **********
			// 03050 = "Benedicta Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03050";
			tGC.Desc = "Benedicta Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03160 = "E Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03160";
			tGC.Desc = "E Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03570 = "Saint John Plt"
			tGC = new clsGeoCode();
			tGC.Code = "03570";
			tGC.Desc = "Saint John Plt";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03801 = "Big Twenty Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03801";
			tGC.Desc = "Big Twenty Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03802 = "Connor Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03802";
			tGC.Desc = "Connor Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03803 = "Cox Patent"
			tGC = new clsGeoCode();
			tGC.Code = "03803";
			tGC.Desc = "Cox Patent";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03804 = "Dudley Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03804";
			tGC.Desc = "Dudley Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03805 = "Forkstown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03805";
			tGC.Desc = "Forkstown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03806 = "Molunkus Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03806";
			tGC.Desc = "Molunkus Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03807 = "North Yarmouth Academy Grant Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03807";
			tGC.Desc = "North Yarmouth Academy Grant Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03808 = "Saint Croix Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03808";
			tGC.Desc = "Saint Croix Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03809 = "Silver Ridge Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03809";
			tGC.Desc = "Silver Ridge Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03810 = "Squapan Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03810";
			tGC.Desc = "Squapan Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03811 = "Upper Molunkus Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03811";
			tGC.Desc = "Upper Molunkus Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03812 = "Webbertown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "03812";
			tGC.Desc = "Webbertown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03813 = "TA R2 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03813";
			tGC.Desc = "TA R2 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03814 = "TC R2 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03814";
			tGC.Desc = "TC R2 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03815 = "TD R2 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03815";
			tGC.Desc = "TD R2 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03816 = "T1 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03816";
			tGC.Desc = "T1 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03817 = "T2 R4 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03817";
			tGC.Desc = "T2 R4 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03818 = "T3 R3 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03818";
			tGC.Desc = "T3 R3 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03819 = "T3 R4 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03819";
			tGC.Desc = "T3 R4 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03820 = "T4 R3 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03820";
			tGC.Desc = "T4 R3 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03821 = "T7 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03821";
			tGC.Desc = "T7 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03822 = "T8 R3 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03822";
			tGC.Desc = "T8 R3 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03823 = "T8 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03823";
			tGC.Desc = "T8 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03824 = "T9 R3 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03824";
			tGC.Desc = "T9 R3 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03825 = "T9 R4 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03825";
			tGC.Desc = "T9 R4 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03826 = "T9 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03826";
			tGC.Desc = "T9 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03827 = "T9 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03827";
			tGC.Desc = "T9 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03828 = "T9 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03828";
			tGC.Desc = "T9 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03829 = "T10 R3 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03829";
			tGC.Desc = "T10 R3 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03830 = "T10 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03830";
			tGC.Desc = "T10 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03831 = "T10 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03831";
			tGC.Desc = "T10 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03832 = "T10 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03832";
			tGC.Desc = "T10 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03833 = "T11 R4 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03833";
			tGC.Desc = "T11 R4 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03834 = "T11 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03834";
			tGC.Desc = "T11 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03835 = "T11 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03835";
			tGC.Desc = "T11 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03836 = "T11 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03836";
			tGC.Desc = "T11 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03837 = "T11 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03837";
			tGC.Desc = "T11 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03838 = "T11 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03838";
			tGC.Desc = "T11 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03839 = "T11 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03839";
			tGC.Desc = "T11 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03840 = "T11 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03840";
			tGC.Desc = "T11 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03841 = "T11 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03841";
			tGC.Desc = "T11 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03842 = "T11 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03842";
			tGC.Desc = "T11 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03843 = "T11 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03843";
			tGC.Desc = "T11 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03844 = "T11 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03844";
			tGC.Desc = "T11 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03845 = "T12 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03845";
			tGC.Desc = "T12 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03846 = "T12 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03846";
			tGC.Desc = "T12 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03847 = "T12 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03847";
			tGC.Desc = "T12 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03848 = "T12 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03848";
			tGC.Desc = "T12 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03849 = "T12 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03849";
			tGC.Desc = "T12 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03850 = "T12 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03850";
			tGC.Desc = "T12 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03851 = "T12 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03851";
			tGC.Desc = "T12 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03852 = "T12 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03852";
			tGC.Desc = "T12 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03853 = "T12 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03853";
			tGC.Desc = "T12 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03854 = "T12 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03854";
			tGC.Desc = "T12 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03855 = "T12 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03855";
			tGC.Desc = "T12 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03856 = "T13 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03856";
			tGC.Desc = "T13 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03857 = "T13 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03857";
			tGC.Desc = "T13 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03858 = "T13 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03858";
			tGC.Desc = "T13 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03859 = "T13 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03859";
			tGC.Desc = "T13 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03860 = "T13 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03860";
			tGC.Desc = "T13 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03861 = "T13 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03861";
			tGC.Desc = "T13 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03862 = "T13 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03862";
			tGC.Desc = "T13 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03863 = "T13 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03863";
			tGC.Desc = "T13 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03864 = "T13 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03864";
			tGC.Desc = "T13 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03865 = "T13 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03865";
			tGC.Desc = "T13 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03866 = "T13 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03866";
			tGC.Desc = "T13 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03867 = "T14 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03867";
			tGC.Desc = "T14 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03868 = "T14 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03868";
			tGC.Desc = "T14 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03869 = "T14 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03869";
			tGC.Desc = "T14 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03870 = "T14 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03870";
			tGC.Desc = "T14 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03871 = "T14 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03871";
			tGC.Desc = "T14 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03872 = "T14 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03872";
			tGC.Desc = "T14 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03873 = "T14 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03873";
			tGC.Desc = "T14 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03874 = "T14 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03874";
			tGC.Desc = "T14 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03875 = "T14 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03875";
			tGC.Desc = "T14 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03876 = "T14 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03876";
			tGC.Desc = "T14 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03877 = "T14 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03877";
			tGC.Desc = "T14 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03878 = "T14 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03878";
			tGC.Desc = "T14 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03879 = "T15 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03879";
			tGC.Desc = "T15 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03880 = "T15 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03880";
			tGC.Desc = "T15 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03881 = "T15 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03881";
			tGC.Desc = "T15 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03882 = "T15 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03882";
			tGC.Desc = "T15 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03883 = "T15 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03883";
			tGC.Desc = "T15 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03884 = "T15 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03884";
			tGC.Desc = "T15 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03885 = "T15 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03885";
			tGC.Desc = "T15 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03886 = "T15 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03886";
			tGC.Desc = "T15 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03887 = "T15 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03887";
			tGC.Desc = "T15 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03888 = "T15 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03888";
			tGC.Desc = "T15 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03889 = "Madawaska Lake"
			tGC = new clsGeoCode();
			tGC.Code = "03889";
			tGC.Desc = "Madawaska Lake";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03890 = "T16 R5 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03890";
			tGC.Desc = "T16 R5 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03891 = "T16 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03891";
			tGC.Desc = "T16 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03892 = "T16 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03892";
			tGC.Desc = "T16 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03893 = "T16 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03893";
			tGC.Desc = "T16 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03894 = "T16 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03894";
			tGC.Desc = "T16 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03895 = "T16 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03895";
			tGC.Desc = "T16 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03896 = "T16 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03896";
			tGC.Desc = "T16 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03897 = "T17 R3 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03897";
			tGC.Desc = "T17 R3 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03898 = "T17 R4 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03898";
			tGC.Desc = "T17 R4 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03899 = "CROSS LAKE TOWNSHIP"
			tGC = new clsGeoCode();
			tGC.Code = "03899";
			tGC.Desc = "CROSS LAKE TOWNSHIP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03900 = "T17 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03900";
			tGC.Desc = "T17 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03901 = "T17 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03901";
			tGC.Desc = "T17 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03902 = "T17 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03902";
			tGC.Desc = "T17 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03903 = "T18 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03903";
			tGC.Desc = "T18 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03904 = "T18 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03904";
			tGC.Desc = "T18 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03905 = "T18 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03905";
			tGC.Desc = "T18 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03906 = "T18 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03906";
			tGC.Desc = "T18 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03907 = "T19 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03907";
			tGC.Desc = "T19 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 03908 = "T19 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "03908";
			tGC.Desc = "T19 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Cumberland County **********
			// No Unorganized Towns
			// ********** Franklin County **********
			// 07110 = "Madrid Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07110";
			tGC.Desc = "Madrid Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07801 = "Alder Stream Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07801";
			tGC.Desc = "Alder Stream Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07802 = "Beattie Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07802";
			tGC.Desc = "Beattie Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07803 = "Chain of Ponds Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07803";
			tGC.Desc = "Chain of Ponds Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07804 = "Coburn Gore"
			tGC = new clsGeoCode();
			tGC.Code = "07804";
			tGC.Desc = "Coburn Gore";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07805 = "Township D"
			tGC = new clsGeoCode();
			tGC.Code = "07805";
			tGC.Desc = "Township D";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07806 = "Davis Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07806";
			tGC.Desc = "Davis Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07807 = "Township E"
			tGC = new clsGeoCode();
			tGC.Code = "07807";
			tGC.Desc = "Township E";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07808 = "Freeman Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07808";
			tGC.Desc = "Freeman Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07809 = "Gorham Gore"
			tGC = new clsGeoCode();
			tGC.Code = "07809";
			tGC.Desc = "Gorham Gore";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07811 = "Jim Pond Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07811";
			tGC.Desc = "Jim Pond Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07812 = "Kibby Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07812";
			tGC.Desc = "Kibby Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07813 = "Lang Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07813";
			tGC.Desc = "Lang Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07814 = "Lowelltown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07814";
			tGC.Desc = "Lowelltown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07815 = "Massachusetts Gore"
			tGC = new clsGeoCode();
			tGC.Code = "07815";
			tGC.Desc = "Massachusetts Gore";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07816 = "Merrill Strip Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07816";
			tGC.Desc = "Merrill Strip Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07817 = "Mount Abram Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07817";
			tGC.Desc = "Mount Abram Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07818 = "Perkins Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07818";
			tGC.Desc = "Perkins Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07819 = "Redington Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07819";
			tGC.Desc = "Redington Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07820 = "Salem Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07820";
			tGC.Desc = "Salem Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07821 = "Seven Ponds Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07821";
			tGC.Desc = "Seven Ponds Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07822 = "Skinner Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07822";
			tGC.Desc = "Skinner Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07823 = "Stetsontown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07823";
			tGC.Desc = "Stetsontown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07825 = "Tim Pond Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07825";
			tGC.Desc = "Tim Pond Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07826 = "Township 6 North of Weld"
			tGC = new clsGeoCode();
			tGC.Code = "07826";
			tGC.Desc = "Township 6 North of Weld";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07827 = "Washington Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07827";
			tGC.Desc = "Washington Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 07828 = "Wyman Twp"
			tGC = new clsGeoCode();
			tGC.Code = "07828";
			tGC.Desc = "Wyman Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Hancock County **********
			// 09801 = "T3 ND"
			tGC = new clsGeoCode();
			tGC.Code = "09801";
			tGC.Desc = "T3 ND";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09802 = "Oqiton Twp"
			tGC = new clsGeoCode();
			tGC.Code = "09802";
			tGC.Desc = "Oqiton Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09803 = "T7 SD"
			tGC = new clsGeoCode();
			tGC.Code = "09803";
			tGC.Desc = "T7 SD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09804 = "FLETCHERS LANDING"
			tGC = new clsGeoCode();
			tGC.Code = "09804";
			tGC.Desc = "FLETCHERS LANDING";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09805 = "T9 SD"
			tGC = new clsGeoCode();
			tGC.Code = "09805";
			tGC.Desc = "T9 SD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09806 = "T10 SD"
			tGC = new clsGeoCode();
			tGC.Code = "09806";
			tGC.Desc = "T10 SD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09807 = "T16 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09807";
			tGC.Desc = "T16 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09808 = "T22 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09808";
			tGC.Desc = "T22 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09809 = "T28 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09809";
			tGC.Desc = "T28 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09810 = "T32 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09810";
			tGC.Desc = "T32 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09811 = "T34 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09811";
			tGC.Desc = "T34 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09812 = "T35 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09812";
			tGC.Desc = "T35 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09813 = "T39 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09813";
			tGC.Desc = "T39 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09814 = "T40 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09814";
			tGC.Desc = "T40 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 09815 = "T41 MD"
			tGC = new clsGeoCode();
			tGC.Code = "09815";
			tGC.Desc = "T41 MD";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Kennebec County **********
			// 11801 = "Unity Twp"
			tGC = new clsGeoCode();
			tGC.Code = "11801";
			tGC.Desc = "Unity Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Knox County **********
			// 13801 = "Criehaven Twp / Ragged Island"
			tGC = new clsGeoCode();
			tGC.Code = "13801";
			tGC.Desc = "Criehaven Twp / Ragged Island";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 13803 = "Muscle Ridge Shoals Twp"
			tGC = new clsGeoCode();
			tGC.Code = "13803";
			tGC.Desc = "Muscle Ridge Shoals Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Lincoln County **********
			// 15100 = "Monhegan Island Plt"
			tGC = new clsGeoCode();
			tGC.Code = "15100";
			tGC.Desc = "Monhegan Island Plt";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 15801 = "Hibberts Gore"
			tGC = new clsGeoCode();
			tGC.Code = "15801";
			tGC.Desc = "Hibberts Gore";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Oxford County **********
			// 17801 = "Adamstown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17801";
			tGC.Desc = "Adamstown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17802 = "Albany Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17802";
			tGC.Desc = "Albany Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17803 = "Andover North Surplus"
			tGC = new clsGeoCode();
			tGC.Code = "17803";
			tGC.Desc = "Andover North Surplus";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17804 = "Andover West Surplus Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17804";
			tGC.Desc = "Andover West Surplus Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17805 = "Batchelders Grant Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17805";
			tGC.Desc = "Batchelders Grant Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17806 = "Bowmantown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17806";
			tGC.Desc = "Bowmantown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17807 = "C Surplus"
			tGC = new clsGeoCode();
			tGC.Code = "17807";
			tGC.Desc = "C Surplus";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17808 = "Grafton Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17808";
			tGC.Desc = "Grafton Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17809 = "Lower Cupsuptic Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17809";
			tGC.Desc = "Lower Cupsuptic Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17810 = "Lynchtown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17810";
			tGC.Desc = "Lynchtown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17811 = "Mason Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17811";
			tGC.Desc = "Mason Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17812 = "Milton Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17812";
			tGC.Desc = "Milton Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17813 = "Oxbow Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17813";
			tGC.Desc = "Oxbow Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17814 = "Parkertown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17814";
			tGC.Desc = "Parkertown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17815 = "Parmachenee Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17815";
			tGC.Desc = "Parmachenee Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17816 = "Richardsontown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17816";
			tGC.Desc = "Richardsontown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17817 = "Riley Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17817";
			tGC.Desc = "Riley Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17818 = "Township C"
			tGC = new clsGeoCode();
			tGC.Code = "17818";
			tGC.Desc = "Township C";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 17819 = "Upper Cupsuptic Twp"
			tGC = new clsGeoCode();
			tGC.Code = "17819";
			tGC.Desc = "Upper Cupsuptic Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Penobscot County **********
			// 19250 = "Grand Falls Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19250";
			tGC.Desc = "Grand Falls Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19270 = "Greenfield Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19270";
			tGC.Desc = "Greenfield Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19540 = "Prentiss Twp T7 R3 NBPP"
			tGC = new clsGeoCode();
			tGC.Code = "19540";
			tGC.Desc = "Prentiss Twp T7 R3 NBPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19801 = "Argyle Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19801";
			tGC.Desc = "Argyle Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19802 = "Grindstone Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19802";
			tGC.Desc = "Grindstone Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19803 = "Herseytown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19803";
			tGC.Desc = "Herseytown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19804 = "Hopkins Academy Grant Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19804";
			tGC.Desc = "Hopkins Academy Grant Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19806 = "T3 Indian Purchase Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19806";
			tGC.Desc = "T3 Indian Purchase Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19807 = "T4 Indian Purchase Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19807";
			tGC.Desc = "T4 Indian Purchase Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19808 = "Kingman Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19808";
			tGC.Desc = "Kingman Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19809 = "Long A Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19809";
			tGC.Desc = "Long A Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19810 = "Mattamiscontis Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19810";
			tGC.Desc = "Mattamiscontis Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19811 = "Soldiertown Twp T2 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19811";
			tGC.Desc = "Soldiertown Twp T2 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19812 = "Summit Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19812";
			tGC.Desc = "Summit Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19813 = "Veazie Gore"
			tGC = new clsGeoCode();
			tGC.Code = "19813";
			tGC.Desc = "Veazie Gore";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19814 = "TA R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19814";
			tGC.Desc = "TA R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19815 = "T1 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19815";
			tGC.Desc = "T1 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19816 = "T1 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19816";
			tGC.Desc = "T1 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19817 = "T2 R8 NWP"
			tGC = new clsGeoCode();
			tGC.Code = "19817";
			tGC.Desc = "T2 R8 NWP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19818 = "T2 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19818";
			tGC.Desc = "T2 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19819 = "T2 R9 NWP"
			tGC = new clsGeoCode();
			tGC.Code = "19819";
			tGC.Desc = "T2 R9 NWP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19820 = "T3 R1 NBPP"
			tGC = new clsGeoCode();
			tGC.Code = "19820";
			tGC.Desc = "T3 R1 NBPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19821 = "T3 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19821";
			tGC.Desc = "T3 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19822 = "T3 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19822";
			tGC.Desc = "T3 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19823 = "T3 R9 NWP"
			tGC = new clsGeoCode();
			tGC.Code = "19823";
			tGC.Desc = "T3 R9 NWP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19824 = "T4 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19824";
			tGC.Desc = "T4 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19825 = "T4 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19825";
			tGC.Desc = "T4 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19826 = "Pukakon Twp"
			tGC = new clsGeoCode();
			tGC.Code = "19826";
			tGC.Desc = "Pukakon Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19827 = "T5 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19827";
			tGC.Desc = "T5 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19828 = "T5 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19828";
			tGC.Desc = "T5 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19829 = "T6 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19829";
			tGC.Desc = "T6 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19830 = "T6 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19830";
			tGC.Desc = "T6 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19831 = "T6 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19831";
			tGC.Desc = "T6 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19832 = "T7 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19832";
			tGC.Desc = "T7 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19833 = "T7 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19833";
			tGC.Desc = "T7 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19834 = "T7 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19834";
			tGC.Desc = "T7 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19835 = "T8 R6 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19835";
			tGC.Desc = "T8 R6 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19836 = "T8 R7 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19836";
			tGC.Desc = "T8 R7 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 19837 = "T8 R8 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "19837";
			tGC.Desc = "T8 R8 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Piscataquis County **********
			// 21000 = "Islands of Moosehead Lake"
			tGC = new clsGeoCode();
			tGC.Code = "21000";
			tGC.Desc = "Islands of Moosehead Lake";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21030 = "Barnard Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21030";
			tGC.Desc = "Barnard Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21040 = "Blanchard Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21040";
			tGC.Desc = "Blanchard Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21080 = "Elliottsville Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21080";
			tGC.Desc = "Elliottsville Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21801 = "Big Moose Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21801";
			tGC.Desc = "Big Moose Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21802 = "Bowdoin College Grant East Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21802";
			tGC.Desc = "Bowdoin College Grant East Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21803 = "Bowdoin College Grant West Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21803";
			tGC.Desc = "Bowdoin College Grant West Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21804 = "Chesuncook Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21804";
			tGC.Desc = "Chesuncook Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21805 = "Cove Point Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21805";
			tGC.Desc = "Cove Point Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21806 = "Days Academy Grant Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21806";
			tGC.Desc = "Days Academy Grant Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21807 = "Eagle Lake Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21807";
			tGC.Desc = "Eagle Lake Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21808 = "East Middlesex Canal Grant Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21808";
			tGC.Desc = "East Middlesex Canal Grant Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21809 = "Frenchtown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21809";
			tGC.Desc = "Frenchtown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21811 = "Harfords Point Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21811";
			tGC.Desc = "Harfords Point Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21812 = "Katahdin Iron Works Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21812";
			tGC.Desc = "Katahdin Iron Works Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21813 = "Kineo Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21813";
			tGC.Desc = "Kineo Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21814 = "T1 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21814";
			tGC.Desc = "T1 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21815 = "Lily Bay Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21815";
			tGC.Desc = "Lily Bay Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21816 = "Moosehead Junction Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21816";
			tGC.Desc = "Moosehead Junction Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21817 = "Lobster Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21817";
			tGC.Desc = "Lobster Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21818 = "Mount Katahdin Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21818";
			tGC.Desc = "Mount Katahdin Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21819 = "Nesourdnahunk Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21819";
			tGC.Desc = "Nesourdnahunk Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21820 = "Northeast Carry Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21820";
			tGC.Desc = "Northeast Carry Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21821 = "Orneville Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21821";
			tGC.Desc = "Orneville Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21822 = "Rainbow Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21822";
			tGC.Desc = "Rainbow Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21823 = "Shawtown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21823";
			tGC.Desc = "Shawtown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21824 = "Soper Mountain Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21824";
			tGC.Desc = "Soper Mountain Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21825 = "Spencer Bay Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21825";
			tGC.Desc = "Spencer Bay Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21826 = "Trout Brook Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21826";
			tGC.Desc = "Trout Brook Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21827 = "Williamsburg Twp"
			tGC = new clsGeoCode();
			tGC.Code = "21827";
			tGC.Desc = "Williamsburg Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21828 = "TA R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21828";
			tGC.Desc = "TA R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21829 = "TA R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21829";
			tGC.Desc = "TA R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21830 = "TB R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21830";
			tGC.Desc = "TB R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21831 = "TB R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21831";
			tGC.Desc = "TB R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21832 = "TX R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21832";
			tGC.Desc = "TX R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21833 = "T1 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21833";
			tGC.Desc = "T1 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21834 = "T1 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21834";
			tGC.Desc = "T1 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21835 = "T1 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21835";
			tGC.Desc = "T1 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21836 = "T1 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21836";
			tGC.Desc = "T1 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21837 = "T2 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21837";
			tGC.Desc = "T2 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21838 = "T2 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21838";
			tGC.Desc = "T2 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21839 = "T2 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21839";
			tGC.Desc = "T2 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21840 = "T2 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21840";
			tGC.Desc = "T2 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21841 = "T3 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21841";
			tGC.Desc = "T3 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21842 = "T3 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21842";
			tGC.Desc = "T3 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21843 = "T3 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21843";
			tGC.Desc = "T3 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21844 = "T3 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21844";
			tGC.Desc = "T3 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21845 = "T4 R9 NWP"
			tGC = new clsGeoCode();
			tGC.Code = "21845";
			tGC.Desc = "T4 R9 NWP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21846 = "T4 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21846";
			tGC.Desc = "T4 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21847 = "T4 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21847";
			tGC.Desc = "T4 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21848 = "T4 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21848";
			tGC.Desc = "T4 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21849 = "T4 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21849";
			tGC.Desc = "T4 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21850 = "T4 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21850";
			tGC.Desc = "T4 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21851 = "T4 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21851";
			tGC.Desc = "T4 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21852 = "T4 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21852";
			tGC.Desc = "T4 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21853 = "T5 R9 NWP"
			tGC = new clsGeoCode();
			tGC.Code = "21853";
			tGC.Desc = "T5 R9 NWP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21854 = "T5 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21854";
			tGC.Desc = "T5 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21855 = "T5 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21855";
			tGC.Desc = "T5 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21856 = "T5 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21856";
			tGC.Desc = "T5 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21857 = "T5 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21857";
			tGC.Desc = "T5 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21858 = "T5 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21858";
			tGC.Desc = "T5 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21859 = "T6 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21859";
			tGC.Desc = "T6 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21860 = "T6 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21860";
			tGC.Desc = "T6 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21861 = "T6 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21861";
			tGC.Desc = "T6 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21862 = "T6 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21862";
			tGC.Desc = "T6 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21863 = "T6 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21863";
			tGC.Desc = "T6 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21864 = "T6 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21864";
			tGC.Desc = "T6 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21865 = "T7 R9 NWP"
			tGC = new clsGeoCode();
			tGC.Code = "21865";
			tGC.Desc = "T7 R9 NWP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21866 = "T7 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21866";
			tGC.Desc = "T7 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21867 = "T7 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21867";
			tGC.Desc = "T7 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21868 = "T7 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21868";
			tGC.Desc = "T7 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21869 = "T7 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21869";
			tGC.Desc = "T7 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21870 = "T7 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21870";
			tGC.Desc = "T7 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21871 = "T7 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21871";
			tGC.Desc = "T7 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21872 = "T7 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21872";
			tGC.Desc = "T7 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21873 = "T8 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21873";
			tGC.Desc = "T8 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21874 = "T8 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21874";
			tGC.Desc = "T8 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21875 = "T8 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21875";
			tGC.Desc = "T8 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21876 = "T8 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21876";
			tGC.Desc = "T8 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21877 = "T8 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21877";
			tGC.Desc = "T8 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21878 = "T9 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21878";
			tGC.Desc = "T9 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21879 = "T9 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21879";
			tGC.Desc = "T9 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21880 = "T9 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21880";
			tGC.Desc = "T9 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21881 = "T9 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21881";
			tGC.Desc = "T9 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21882 = "T9 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21882";
			tGC.Desc = "T9 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21883 = "T9 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21883";
			tGC.Desc = "T9 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21884 = "T9 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21884";
			tGC.Desc = "T9 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21885 = "T10 R9 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21885";
			tGC.Desc = "T10 R9 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21886 = "T10 R10 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21886";
			tGC.Desc = "T10 R10 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21887 = "T10 R11 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21887";
			tGC.Desc = "T10 R11 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21888 = "T10 R12 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21888";
			tGC.Desc = "T10 R12 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21889 = "T10 R13 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21889";
			tGC.Desc = "T10 R13 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21890 = "T10 R14 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21890";
			tGC.Desc = "T10 R14 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 21891 = "T10 R15 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "21891";
			tGC.Desc = "T10 R15 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Sagadahoc County **********
			// 23801 = "Perkins Twp Swan Island"
			tGC = new clsGeoCode();
			tGC.Code = "23801";
			tGC.Desc = "Perkins Twp Swan Island";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Somerset County **********
			// 25801 = "Alder Brook Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25801";
			tGC.Desc = "Alder Brook Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25802 = "Appleton Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25802";
			tGC.Desc = "Appleton Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25803 = "Taunton & Raynham Academy Grant"
			tGC = new clsGeoCode();
			tGC.Code = "25803";
			tGC.Desc = "Taunton & Raynham Academy Grant";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25804 = "Attean Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25804";
			tGC.Desc = "Attean Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25805 = "Bald Mountain Twp T2 R3"
			tGC = new clsGeoCode();
			tGC.Code = "25805";
			tGC.Desc = "Bald Mountain Twp T2 R3";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25806 = "Bald Mountain Twp T4 R3"
			tGC = new clsGeoCode();
			tGC.Code = "25806";
			tGC.Desc = "Bald Mountain Twp T4 R3";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25807 = "Bigelow Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25807";
			tGC.Desc = "Bigelow Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25808 = "Big Six Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25808";
			tGC.Desc = "Big Six Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25809 = "Big Ten Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25809";
			tGC.Desc = "Big Ten Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25810 = "Big W Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25810";
			tGC.Desc = "Big W Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25811 = "Blake Gore"
			tGC = new clsGeoCode();
			tGC.Code = "25811";
			tGC.Desc = "Blake Gore";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25812 = "Bowtown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25812";
			tGC.Desc = "Bowtown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25813 = "Bradstreet Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25813";
			tGC.Desc = "Bradstreet Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25814 = "Brassua Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25814";
			tGC.Desc = "Brassua Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25815 = "Carrying Place Town Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25815";
			tGC.Desc = "Carrying Place Town Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25816 = "Chase Stream Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25816";
			tGC.Desc = "Chase Stream Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25817 = "Comstock Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25817";
			tGC.Desc = "Comstock Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25818 = "Concord Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25818";
			tGC.Desc = "Concord Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25819 = "Dead River Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25819";
			tGC.Desc = "Dead River Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25820 = "Dole Brook Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25820";
			tGC.Desc = "Dole Brook Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25821 = "East Moxie Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25821";
			tGC.Desc = "East Moxie Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25822 = "Elm Stream Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25822";
			tGC.Desc = "Elm Stream Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25823 = "Flagstaff Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25823";
			tGC.Desc = "Flagstaff Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25824 = "Forsyth Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25824";
			tGC.Desc = "Forsyth Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25825 = "Hammond Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25825";
			tGC.Desc = "Hammond Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25826 = "Hobbstown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25826";
			tGC.Desc = "Hobbstown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25827 = "Holeb Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25827";
			tGC.Desc = "Holeb Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25828 = "Indian Stream Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25828";
			tGC.Desc = "Indian Stream Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25829 = "Johnson Mountain Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25829";
			tGC.Desc = "Johnson Mountain Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25830 = "King & Bartlett Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25830";
			tGC.Desc = "King & Bartlett Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25831 = "Lexington Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25831";
			tGC.Desc = "Lexington Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25832 = "Little W Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25832";
			tGC.Desc = "Little W Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25833 = "Long Pond Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25833";
			tGC.Desc = "Long Pond Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25834 = "Lower Enchanted Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25834";
			tGC.Desc = "Lower Enchanted Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25835 = "Mayfield Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25835";
			tGC.Desc = "Mayfield Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25836 = "Misery Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25836";
			tGC.Desc = "Misery Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25837 = "Misery Gore Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25837";
			tGC.Desc = "Misery Gore Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25838 = "Moxie Gore"
			tGC = new clsGeoCode();
			tGC.Code = "25838";
			tGC.Desc = "Moxie Gore";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25839 = "Parlin Pond Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25839";
			tGC.Desc = "Parlin Pond Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25840 = "Pierce Pond Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25840";
			tGC.Desc = "Pierce Pond Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25841 = "Pittston Academy Grant"
			tGC = new clsGeoCode();
			tGC.Code = "25841";
			tGC.Desc = "Pittston Academy Grant";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25842 = "Plymouth Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25842";
			tGC.Desc = "Plymouth Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25843 = "Prentiss Twp T4 R4 NBKP"
			tGC = new clsGeoCode();
			tGC.Code = "25843";
			tGC.Desc = "Prentiss Twp T4 R4 NBKP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25844 = "Rockwood Strip T1 R1 NBKP"
			tGC = new clsGeoCode();
			tGC.Code = "25844";
			tGC.Desc = "Rockwood Strip T1 R1 NBKP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25845 = "Rockwood Strip T2 R1 NBKP"
			tGC = new clsGeoCode();
			tGC.Code = "25845";
			tGC.Desc = "Rockwood Strip T2 R1 NBKP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25846 = "Russell Pond Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25846";
			tGC.Desc = "Russell Pond Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25847 = "Saint John Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25847";
			tGC.Desc = "Saint John Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25848 = "Sandbar Tract Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25848";
			tGC.Desc = "Sandbar Tract Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25849 = "Sandwich Academy Grant Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25849";
			tGC.Desc = "Sandwich Academy Grant Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25850 = "Sandy Bay Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25850";
			tGC.Desc = "Sandy Bay Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25851 = "Sapling Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25851";
			tGC.Desc = "Sapling Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25852 = "Seboomook Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25852";
			tGC.Desc = "Seboomook Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25853 = "Soldiertown Twp T2 R3 NBKP"
			tGC = new clsGeoCode();
			tGC.Code = "25853";
			tGC.Desc = "Soldiertown Twp T2 R3 NBKP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25854 = "Squaretown Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25854";
			tGC.Desc = "Squaretown Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25856 = "Thorndike Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25856";
			tGC.Desc = "Thorndike Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25857 = "Tomhegan Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25857";
			tGC.Desc = "Tomhegan Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25858 = "Upper Enchanted Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25858";
			tGC.Desc = "Upper Enchanted Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25859 = "West Middlesex Canal Grant"
			tGC = new clsGeoCode();
			tGC.Code = "25859";
			tGC.Desc = "West Middlesex Canal Grant";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25860 = "Carrying Place Twp"
			tGC = new clsGeoCode();
			tGC.Code = "25860";
			tGC.Desc = "Carrying Place Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25861 = "T3 R4 BKP WKR"
			tGC = new clsGeoCode();
			tGC.Code = "25861";
			tGC.Desc = "T3 R4 BKP WKR";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25862 = "T3 R5 BKP WKR"
			tGC = new clsGeoCode();
			tGC.Code = "25862";
			tGC.Desc = "T3 R5 BKP WKR";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25863 = "T4 R5 NBKP"
			tGC = new clsGeoCode();
			tGC.Code = "25863";
			tGC.Desc = "T4 R5 NBKP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25864 = "T4 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25864";
			tGC.Desc = "T4 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25865 = "T5 R6 BKP WKR"
			tGC = new clsGeoCode();
			tGC.Code = "25865";
			tGC.Desc = "T5 R6 BKP WKR";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25866 = "T5 R7 BKP WKR"
			tGC = new clsGeoCode();
			tGC.Code = "25866";
			tGC.Desc = "T5 R7 BKP WKR";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25867 = "T5 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25867";
			tGC.Desc = "T5 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25868 = "T5 R18 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25868";
			tGC.Desc = "T5 R18 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25869 = "T5 R19 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25869";
			tGC.Desc = "T5 R19 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25870 = "T5 R20 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25870";
			tGC.Desc = "T5 R20 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25871 = "T6 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25871";
			tGC.Desc = "T6 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25872 = "T6 R18 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25872";
			tGC.Desc = "T6 R18 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25873 = "T7 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25873";
			tGC.Desc = "T7 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25874 = "T7 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25874";
			tGC.Desc = "T7 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25875 = "T7 R18 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25875";
			tGC.Desc = "T7 R18 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25876 = "T7 R19 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25876";
			tGC.Desc = "T7 R19 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25877 = "T8 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25877";
			tGC.Desc = "T8 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25878 = "T8 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25878";
			tGC.Desc = "T8 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25879 = "T8 R18 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25879";
			tGC.Desc = "T8 R18 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25880 = "T8 R19 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25880";
			tGC.Desc = "T8 R19 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25881 = "T9 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25881";
			tGC.Desc = "T9 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25882 = "T9 R17 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25882";
			tGC.Desc = "T9 R17 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25883 = "T9 R18 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25883";
			tGC.Desc = "T9 R18 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 25884 = "T10 R16 WELS"
			tGC = new clsGeoCode();
			tGC.Code = "25884";
			tGC.Desc = "T10 R16 WELS";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** Waldo County **********
			// No Unorganized Towns
			// ********** Washington County **********
			// 29080 = "Centerville Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29080";
			tGC.Desc = "Centerville Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29330 = "No 14 Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29330";
			tGC.Desc = "No 14 Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29340 = "Big Lake Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29340";
			tGC.Desc = "Big Lake Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29480 = "Pleasant Point"
			tGC = new clsGeoCode();
			tGC.Code = "29480";
			tGC.Desc = "Pleasant Point";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29801 = "Brookton Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29801";
			tGC.Desc = "Brookton Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29802 = "Devereaux Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29802";
			tGC.Desc = "Devereaux Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29803 = "Dyer Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29803";
			tGC.Desc = "Dyer Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29804 = "Edmunds Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29804";
			tGC.Desc = "Edmunds Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29805 = "Forest Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29805";
			tGC.Desc = "Forest Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29806 = "Forest City Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29806";
			tGC.Desc = "Forest City Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29807 = "Fowler Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29807";
			tGC.Desc = "Fowler Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29808 = "Kossuth Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29808";
			tGC.Desc = "Kossuth Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29809 = "Lambert Lake Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29809";
			tGC.Desc = "Lambert Lake Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29810 = "Marion Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29810";
			tGC.Desc = "Marion Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29811 = "Trescott Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29811";
			tGC.Desc = "Trescott Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29812 = "Sakom Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29812";
			tGC.Desc = "Sakom Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29813 = "T6 ND BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29813";
			tGC.Desc = "T6 ND BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29814 = "T6 R1 NBPP"
			tGC = new clsGeoCode();
			tGC.Code = "29814";
			tGC.Desc = "T6 R1 NBPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29815 = "T8 R3 NBPP"
			tGC = new clsGeoCode();
			tGC.Code = "29815";
			tGC.Desc = "T8 R3 NBPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29816 = "T8 R4 NBPP"
			tGC = new clsGeoCode();
			tGC.Code = "29816";
			tGC.Desc = "T8 R4 NBPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29817 = "T11 R3 NBPP"
			tGC = new clsGeoCode();
			tGC.Code = "29817";
			tGC.Desc = "T11 R3 NBPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29818 = "T18 ED BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29818";
			tGC.Desc = "T18 ED BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29819 = "T18 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29819";
			tGC.Desc = "T18 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29820 = "T19 ED BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29820";
			tGC.Desc = "T19 ED BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29821 = "T19 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29821";
			tGC.Desc = "T19 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29822 = "T24 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29822";
			tGC.Desc = "T24 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29823 = "T25 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29823";
			tGC.Desc = "T25 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29824 = "T26 ED BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29824";
			tGC.Desc = "T26 ED BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29825 = "T27 ED BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29825";
			tGC.Desc = "T27 ED BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29826 = "T30 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29826";
			tGC.Desc = "T30 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29827 = "T31 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29827";
			tGC.Desc = "T31 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29828 = "T36 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29828";
			tGC.Desc = "T36 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29829 = "T37 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29829";
			tGC.Desc = "T37 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29830 = "T42 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29830";
			tGC.Desc = "T42 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29831 = "T43 MD BPP"
			tGC = new clsGeoCode();
			tGC.Code = "29831";
			tGC.Desc = "T43 MD BPP";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29832 = "Indian Twp Res"
			tGC = new clsGeoCode();
			tGC.Code = "29832";
			tGC.Desc = "Indian Twp Res";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// 29834 = "No 21 Twp"
			tGC = new clsGeoCode();
			tGC.Code = "29834";
			tGC.Desc = "No 21 Twp";
			UnorganizedTowns.Add(tGC, tGC.Code);
			// ********** York County **********
			// No Unorganized Towns
		}

		private void FillAgents()
		{
			clsGeoCode tGC;
			// ********** LTT Agents **********
			// 44001 = "Alco Company"
			tGC = new clsGeoCode();
			tGC.Code = "44001";
			tGC.Desc = "Alco Company";
			Agents.Add(tGC, tGC.Code);
			// 44002 = "Haskell Agency"
			tGC = new clsGeoCode();
			tGC.Code = "44002";
			tGC.Desc = "Haskell Agency";
			Agents.Add(tGC, tGC.Code);
			// 44003 = "AB Ledue Enterprises"
			tGC = new clsGeoCode();
			tGC.Code = "44003";
			tGC.Desc = "AB Ledue Enterprises";
			Agents.Add(tGC, tGC.Code);
			// 44004 = "Maine Motor Transport Association"
			tGC = new clsGeoCode();
			tGC.Code = "44004";
			tGC.Desc = "Maine Motor Transport Association";
			Agents.Add(tGC, tGC.Code);
			// 44005 = "Maine Trailer"
			tGC = new clsGeoCode();
			tGC.Code = "44005";
			tGC.Desc = "Maine Trailer";
			Agents.Add(tGC, tGC.Code);
			// 44007 = "David Mcloon"
			tGC = new clsGeoCode();
			tGC.Code = "44007";
			tGC.Desc = "David Mcloon";
			Agents.Add(tGC, tGC.Code);
			// 44008 = "Robert Quellette"
			tGC = new clsGeoCode();
			tGC.Code = "44008";
			tGC.Desc = "Robert Quellette";
			Agents.Add(tGC, tGC.Code);
			// 44009 = "Xtra"
			tGC = new clsGeoCode();
			tGC.Code = "44009";
			tGC.Desc = "Xtra";
			Agents.Add(tGC, tGC.Code);
			// 44010 = "Trucks Unlimited"
			tGC = new clsGeoCode();
			tGC.Code = "44010";
			tGC.Desc = "Trucks Unlimited";
			Agents.Add(tGC, tGC.Code);
			// 44011 = "Thomas Hallett"
			tGC = new clsGeoCode();
			tGC.Code = "44011";
			tGC.Desc = "Thomas Hallett";
			Agents.Add(tGC, tGC.Code);
			// 44012 = "Maine-ly Titles"
			tGC = new clsGeoCode();
			tGC.Code = "44012";
			tGC.Desc = "Maine-ly Titles";
			Agents.Add(tGC, tGC.Code);
			// 44013 = "State of Maine"
			tGC = new clsGeoCode();
			tGC.Code = "44013";
			tGC.Desc = "State of Maine";
			Agents.Add(tGC, tGC.Code);
			// 44014 = "PREST, FLAHERTY, BELIVEAU, and PACHIOS, LLP"
			tGC = new clsGeoCode();
			tGC.Code = "44014";
			tGC.Desc = "PREST, FLAHERTY, BELIVEAU, and PACHIOS, LLP";
			Agents.Add(tGC, tGC.Code);
			// 44015 = "THE MAROON GROUP, LLC"
			tGC = new clsGeoCode();
			tGC.Code = "44015";
			tGC.Desc = "THE MAROON GROUP, LLC";
			Agents.Add(tGC, tGC.Code);
			// 44016 = "Northern Pines, LLC"
			tGC = new clsGeoCode();
			tGC.Code = "44016";
			tGC.Desc = "Northern Pines, LLC";
			Agents.Add(tGC, tGC.Code);
			// 44017 = "Anthony Galli"
			tGC = new clsGeoCode();
			tGC.Code = "44017";
			tGC.Desc = "Anthony Galli";
			Agents.Add(tGC, tGC.Code);
			// 44018 = "Countrywide Trailer Registrations"
			tGC = new clsGeoCode();
			tGC.Code = "44018";
			tGC.Desc = "Countrywide Trailer Registrations";
			Agents.Add(tGC, tGC.Code);
			// 44019 = "Maine Tags"
			tGC = new clsGeoCode();
			tGC.Code = "44019";
			tGC.Desc = "Maine Tags";
			Agents.Add(tGC, tGC.Code);
			// 44020 = "L A Delano Agency LLC"
			tGC = new clsGeoCode();
			tGC.Code = "44020";
			tGC.Desc = "L A Delano Agency LLC";
			Agents.Add(tGC, tGC.Code);
			// 44021 = "Ace Registration Services, LLC"
			tGC = new clsGeoCode();
			tGC.Code = "44021";
			tGC.Desc = "Ace Registration Services, LLC";
			Agents.Add(tGC, tGC.Code);
			// 44022 = "Calculations Inc"
			tGC = new clsGeoCode();
			tGC.Code = "44022";
			tGC.Desc = "Calculations Inc";
			Agents.Add(tGC, tGC.Code);
			// 44023 = "Maine Coast Trailers"
			tGC = new clsGeoCode();
			tGC.Code = "44023";
			tGC.Desc = "Maine Coast Trailers";
			Agents.Add(tGC, tGC.Code);
		}

		public string GetDescription(ref string strGeoCode)
		{
			string GetDescription = "";
			int lngErr;
			string strDesc;
			strDesc = "";
			// Check Organized Towns Collection
			/*? On Error Resume Next  */
			//FC:FINAL:MSH - i.issue #1858: resume work after error (as in original app)
			//strDesc = (OrganizedTowns[strGeoCode] as clsGeoCode).Desc;
			try
			{
				strDesc = (OrganizedTowns[strGeoCode] as clsGeoCode).Desc;
			}
            catch (Exception ex) {StaticSettings.GlobalTelemetryService.TrackException(ex); }
			/*? On Error GoTo 0 */
			if (strDesc == "")
			{
				// Check Unorganized Towns for Correct Code and Town
				/*? On Error Resume Next  */
				//FC:FINAL:MSH - i.issue #1858: resume work after error (as in original app)
				//strDesc = (UnorganizedTowns[strGeoCode] as clsGeoCode).Desc;
				try
				{
					strDesc = (UnorganizedTowns[strGeoCode] as clsGeoCode).Desc;
				}
                catch (Exception ex) {StaticSettings.GlobalTelemetryService.TrackException(ex); }
				/*? On Error GoTo 0 */
				if (strDesc == "")
				{
					// Check Agents for Correct Code and Town
					/*? On Error Resume Next  */
					//FC:FINAL:MSH - i.issue #1858: resume work after error (as in original app)
					//strDesc = (Agents[strGeoCode] as clsGeoCode).Desc;
					try
					{
						strDesc = (Agents[strGeoCode] as clsGeoCode).Desc;
					}
                    catch (Exception ex) {StaticSettings.GlobalTelemetryService.TrackException(ex); }
					/*? On Error GoTo 0 */
				}
			}
			GetDescription = fecherFoundation.Strings.UCase(strDesc);
			return GetDescription;
		}
	}
}
