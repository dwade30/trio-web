//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmVehicleAddressList.
	/// </summary>
	partial class frmVehicleAddressList
	{
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCTextBox txtStreetName;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public FCGrid vsResults;
		public fecherFoundation.FCButton cmdFilePrint;
		public fecherFoundation.FCButton cmdFilePreview;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmdClearAll = new fecherFoundation.FCButton();
			this.cmdSelectAll = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtLow = new fecherFoundation.FCTextBox();
			this.txtHigh = new fecherFoundation.FCTextBox();
			this.txtStreetName = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.vsResults = new fecherFoundation.FCGrid();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResults)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(651, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSelectAll);
			this.ClientArea.Controls.Add(this.cmdClearAll);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.vsResults);
			this.ClientArea.Size = new System.Drawing.Size(651, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(651, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(235, 30);
			this.HeaderText.Text = "Vehicle Address List";
			// 
			// cmdClearAll
			// 
			this.cmdClearAll.AppearanceKey = "actionButton";
			this.cmdClearAll.Location = new System.Drawing.Point(164, 602);
			this.cmdClearAll.Name = "cmdClearAll";
			this.cmdClearAll.Size = new System.Drawing.Size(106, 40);
			this.cmdClearAll.TabIndex = 6;
			this.cmdClearAll.Text = "Clear All";
			this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
			// 
			// cmdSelectAll
			// 
			this.cmdSelectAll.AppearanceKey = "actionButton";
			this.cmdSelectAll.Location = new System.Drawing.Point(30, 602);
			this.cmdSelectAll.Name = "cmdSelectAll";
			this.cmdSelectAll.Size = new System.Drawing.Size(114, 40);
			this.cmdSelectAll.TabIndex = 5;
			this.cmdSelectAll.Text = "Select All";
			this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtLow);
			this.Frame1.Controls.Add(this.txtHigh);
			this.Frame1.Controls.Add(this.txtStreetName);
			this.Frame1.Controls.Add(this.cmdSearch);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(381, 210);
			this.Frame1.TabIndex = 7;
			this.Frame1.Text = "Search Criteria";
			// 
			// txtLow
			// 
			this.txtLow.AutoSize = false;
			this.txtLow.BackColor = System.Drawing.SystemColors.Window;
			this.txtLow.LinkItem = null;
			this.txtLow.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLow.LinkTopic = null;
			this.txtLow.Location = new System.Drawing.Point(147, 30);
			this.txtLow.MaxLength = 5;
			this.txtLow.Name = "txtLow";
			this.txtLow.Size = new System.Drawing.Size(80, 40);
			this.txtLow.TabIndex = 0;
			this.txtLow.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLow_KeyPress);
			// 
			// txtHigh
			// 
			this.txtHigh.AutoSize = false;
			this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
			this.txtHigh.LinkItem = null;
			this.txtHigh.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtHigh.LinkTopic = null;
			this.txtHigh.Location = new System.Drawing.Point(281, 30);
			this.txtHigh.MaxLength = 5;
			this.txtHigh.Name = "txtHigh";
			this.txtHigh.Size = new System.Drawing.Size(80, 40);
			this.txtHigh.TabIndex = 1;
			this.txtHigh.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtHigh_KeyPress);
			// 
			// txtStreetName
			// 
			this.txtStreetName.AutoSize = false;
			this.txtStreetName.BackColor = System.Drawing.SystemColors.Window;
			this.txtStreetName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtStreetName.LinkItem = null;
			this.txtStreetName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStreetName.LinkTopic = null;
			this.txtStreetName.Location = new System.Drawing.Point(147, 90);
			this.txtStreetName.MaxLength = 50;
			this.txtStreetName.Name = "txtStreetName";
			this.txtStreetName.Size = new System.Drawing.Size(214, 40);
			this.txtStreetName.TabIndex = 2;
			this.txtStreetName.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStreetName_KeyPress);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Location = new System.Drawing.Point(20, 150);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(96, 40);
			this.cmdSearch.TabIndex = 3;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(98, 16);
			this.Label1.TabIndex = 10;
			this.Label1.Text = "STREET NUMBER";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(246, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(19, 14);
			this.Label2.TabIndex = 9;
			this.Label2.Text = "TO";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 104);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(98, 16);
			this.Label3.TabIndex = 8;
			this.Label3.Text = "STREET NAME";
			// 
			// vsResults
			// 
			this.vsResults.AllowSelection = false;
			this.vsResults.AllowUserToResizeColumns = false;
			this.vsResults.AllowUserToResizeRows = false;
			this.vsResults.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsResults.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsResults.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsResults.BackColorBkg = System.Drawing.Color.Empty;
			this.vsResults.BackColorFixed = System.Drawing.Color.Empty;
			this.vsResults.BackColorSel = System.Drawing.Color.Empty;
			this.vsResults.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsResults.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsResults.ColumnHeadersHeight = 30;
			this.vsResults.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsResults.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsResults.DragIcon = null;
			this.vsResults.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsResults.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsResults.ExtendLastCol = true;
			this.vsResults.FixedCols = 0;
			this.vsResults.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsResults.FrozenCols = 0;
			this.vsResults.GridColor = System.Drawing.Color.Empty;
			this.vsResults.GridColorFixed = System.Drawing.Color.Empty;
			this.vsResults.Location = new System.Drawing.Point(30, 260);
			this.vsResults.Name = "vsResults";
			this.vsResults.OutlineCol = 0;
			this.vsResults.ReadOnly = true;
			this.vsResults.RowHeadersVisible = false;
			this.vsResults.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsResults.RowHeightMin = 0;
			this.vsResults.Rows = 1;
			this.vsResults.ScrollTipText = null;
			this.vsResults.ShowColumnVisibilityMenu = false;
			this.vsResults.ShowFocusCell = false;
			this.vsResults.Size = new System.Drawing.Size(592, 322);
			this.vsResults.StandardTab = true;
			this.vsResults.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsResults.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsResults.TabIndex = 4;
			this.vsResults.KeyDown += new Wisej.Web.KeyEventHandler(this.vsResults_KeyDownEvent);
			this.vsResults.Click += new System.EventHandler(this.vsResults_ClickEvent);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(576, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdFilePrint.Size = new System.Drawing.Size(46, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(217, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(145, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmVehicleAddressList
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(651, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVehicleAddressList";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Vehicle Address List";
			this.Load += new System.EventHandler(this.frmVehicleAddressList_Load);
			this.Activated += new System.EventHandler(this.frmVehicleAddressList_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVehicleAddressList_KeyPress);
			this.Resize += new System.EventHandler(this.frmVehicleAddressList_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResults)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
