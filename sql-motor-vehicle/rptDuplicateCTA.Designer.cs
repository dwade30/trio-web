﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptDuplicateCTA.
	/// </summary>
	partial class rptDuplicateCTA
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDuplicateCTA));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBodyType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPurchaseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFirstLienHolder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateofLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1Address = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1City = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1State = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien1Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecondLienHolder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Address = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2City = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2State = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Zip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMile = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKilometer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtActual = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInExcess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotActual = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometerChanged = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOdometerBroken = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLien2Date = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDoublCTATitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRushTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIllegible = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDestroyed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStolen = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewUsedRebuilt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDealerPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBodyType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLienHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Address)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1City)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1State)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Zip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondLienHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Address)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2City)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2State)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Zip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKilometer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActual)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInExcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotActual)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerChanged)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerBroken)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Date)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoublCTATitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRushTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIllegible)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDestroyed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStolen)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewUsedRebuilt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDealerPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName1,
				this.txtName2,
				this.txtDOB1,
				this.txtDOB2,
				this.txtTelephone,
				this.txtAddress,
				this.txtCity,
				this.txtState,
				this.txtZip,
				this.txtYear,
				this.txtMake,
				this.txtModel,
				this.txtVIN,
				this.txtBodyType,
				this.txtPurchaseDate,
				this.txtFirstLienHolder,
				this.txtDateofLien,
				this.txtLien1Address,
				this.txtLien1City,
				this.txtLien1State,
				this.txtLien1Zip,
				this.txtSecondLienHolder,
				this.txtLien2Address,
				this.txtLien2City,
				this.txtLien2State,
				this.txtLien2Zip,
				this.txtOdometer,
				this.txtMile,
				this.txtKilometer,
				this.txtActual,
				this.txtInExcess,
				this.txtNotActual,
				this.txtOdometerChanged,
				this.txtOdometerBroken,
				this.txtLien2Date,
				this.fldDoublCTATitle,
				this.txtRushTitle,
				this.txtIllegible,
				this.txtDestroyed,
				this.txtStolen,
				this.txtLost,
				this.txtNewUsedRebuilt,
				this.fldD,
				this.fldUC,
				this.fldMC,
				this.fldDealerPlate
			});
			this.Detail.Height = 8.375F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtName1
			// 
			this.txtName1.Height = 0.1875F;
			this.txtName1.Left = 1.1875F;
			this.txtName1.MultiLine = false;
			this.txtName1.Name = "txtName1";
			this.txtName1.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtName1.Text = null;
			this.txtName1.Top = 2.0625F;
			this.txtName1.Width = 3F;
			// 
			// txtName2
			// 
			this.txtName2.Height = 0.1875F;
			this.txtName2.Left = 1.1875F;
			this.txtName2.MultiLine = false;
			this.txtName2.Name = "txtName2";
			this.txtName2.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtName2.Text = null;
			this.txtName2.Top = 2.3125F;
			this.txtName2.Width = 3F;
			// 
			// txtDOB1
			// 
			this.txtDOB1.Height = 0.1875F;
			this.txtDOB1.Left = 5.3125F;
			this.txtDOB1.MultiLine = false;
			this.txtDOB1.Name = "txtDOB1";
			this.txtDOB1.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDOB1.Text = null;
			this.txtDOB1.Top = 2.0625F;
			this.txtDOB1.Width = 1F;
			// 
			// txtDOB2
			// 
			this.txtDOB2.Height = 0.1875F;
			this.txtDOB2.Left = 5.3125F;
			this.txtDOB2.MultiLine = false;
			this.txtDOB2.Name = "txtDOB2";
			this.txtDOB2.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDOB2.Text = null;
			this.txtDOB2.Top = 2.3125F;
			this.txtDOB2.Width = 1F;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Height = 0.1875F;
			this.txtTelephone.Left = 6.75F;
			this.txtTelephone.MultiLine = false;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtTelephone.Text = null;
			this.txtTelephone.Top = 2.0625F;
			this.txtTelephone.Width = 1.25F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1.1875F;
			this.txtAddress.MultiLine = false;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 2.625F;
			this.txtAddress.Width = 3.25F;
			// 
			// txtCity
			// 
			this.txtCity.Height = 0.1875F;
			this.txtCity.Left = 1.1875F;
			this.txtCity.MultiLine = false;
			this.txtCity.Name = "txtCity";
			this.txtCity.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtCity.Text = null;
			this.txtCity.Top = 2.9375F;
			this.txtCity.Width = 2F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1875F;
			this.txtState.Left = 3.875F;
			this.txtState.MultiLine = false;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtState.Text = null;
			this.txtState.Top = 2.9375F;
			this.txtState.Width = 0.4375F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.1875F;
			this.txtZip.Left = 6.125F;
			this.txtZip.MultiLine = false;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtZip.Text = null;
			this.txtZip.Top = 2.9375F;
			this.txtZip.Width = 1.125F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1875F;
			this.txtYear.Left = 1.125F;
			this.txtYear.MultiLine = false;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtYear.Text = null;
			this.txtYear.Top = 3.25F;
			this.txtYear.Width = 0.4375F;
			// 
			// txtMake
			// 
			this.txtMake.Height = 0.1875F;
			this.txtMake.Left = 1.6875F;
			this.txtMake.MultiLine = false;
			this.txtMake.Name = "txtMake";
			this.txtMake.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtMake.Text = null;
			this.txtMake.Top = 3.25F;
			this.txtMake.Width = 0.75F;
			// 
			// txtModel
			// 
			this.txtModel.Height = 0.1875F;
			this.txtModel.Left = 2.5625F;
			this.txtModel.MultiLine = false;
			this.txtModel.Name = "txtModel";
			this.txtModel.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtModel.Text = null;
			this.txtModel.Top = 3.25F;
			this.txtModel.Width = 0.8125F;
			// 
			// txtVIN
			// 
			this.txtVIN.Height = 0.1875F;
			this.txtVIN.Left = 3.4375F;
			this.txtVIN.MultiLine = false;
			this.txtVIN.Name = "txtVIN";
			this.txtVIN.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtVIN.Text = null;
			this.txtVIN.Top = 3.25F;
			this.txtVIN.Width = 2.375F;
			// 
			// txtBodyType
			// 
			this.txtBodyType.Height = 0.1875F;
			this.txtBodyType.Left = 6.8125F;
			this.txtBodyType.MultiLine = false;
			this.txtBodyType.Name = "txtBodyType";
			this.txtBodyType.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtBodyType.Text = null;
			this.txtBodyType.Top = 3.25F;
			this.txtBodyType.Width = 0.4375F;
			// 
			// txtPurchaseDate
			// 
			this.txtPurchaseDate.Height = 0.1875F;
			this.txtPurchaseDate.Left = 1.125F;
			this.txtPurchaseDate.MultiLine = false;
			this.txtPurchaseDate.Name = "txtPurchaseDate";
			this.txtPurchaseDate.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtPurchaseDate.Text = null;
			this.txtPurchaseDate.Top = 3.6875F;
			this.txtPurchaseDate.Width = 1F;
			// 
			// txtFirstLienHolder
			// 
			this.txtFirstLienHolder.Height = 0.1875F;
			this.txtFirstLienHolder.Left = 1.125F;
			this.txtFirstLienHolder.MultiLine = false;
			this.txtFirstLienHolder.Name = "txtFirstLienHolder";
			this.txtFirstLienHolder.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtFirstLienHolder.Text = null;
			this.txtFirstLienHolder.Top = 4.125F;
			this.txtFirstLienHolder.Width = 3.25F;
			// 
			// txtDateofLien
			// 
			this.txtDateofLien.Height = 0.1875F;
			this.txtDateofLien.Left = 6.5F;
			this.txtDateofLien.MultiLine = false;
			this.txtDateofLien.Name = "txtDateofLien";
			this.txtDateofLien.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtDateofLien.Text = null;
			this.txtDateofLien.Top = 4.125F;
			this.txtDateofLien.Width = 1.0625F;
			// 
			// txtLien1Address
			// 
			this.txtLien1Address.Height = 0.1875F;
			this.txtLien1Address.Left = 1.125F;
			this.txtLien1Address.MultiLine = false;
			this.txtLien1Address.Name = "txtLien1Address";
			this.txtLien1Address.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1Address.Text = null;
			this.txtLien1Address.Top = 4.5F;
			this.txtLien1Address.Width = 1.9375F;
			// 
			// txtLien1City
			// 
			this.txtLien1City.Height = 0.1875F;
			this.txtLien1City.Left = 3.8125F;
			this.txtLien1City.MultiLine = false;
			this.txtLien1City.Name = "txtLien1City";
			this.txtLien1City.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1City.Text = null;
			this.txtLien1City.Top = 4.5F;
			this.txtLien1City.Width = 1.6875F;
			// 
			// txtLien1State
			// 
			this.txtLien1State.Height = 0.1875F;
			this.txtLien1State.Left = 5.5625F;
			this.txtLien1State.MultiLine = false;
			this.txtLien1State.Name = "txtLien1State";
			this.txtLien1State.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1State.Text = null;
			this.txtLien1State.Top = 4.5F;
			this.txtLien1State.Width = 0.75F;
			// 
			// txtLien1Zip
			// 
			this.txtLien1Zip.Height = 0.1875F;
			this.txtLien1Zip.Left = 6.875F;
			this.txtLien1Zip.MultiLine = false;
			this.txtLien1Zip.Name = "txtLien1Zip";
			this.txtLien1Zip.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien1Zip.Text = null;
			this.txtLien1Zip.Top = 4.5F;
			this.txtLien1Zip.Width = 1F;
			// 
			// txtSecondLienHolder
			// 
			this.txtSecondLienHolder.Height = 0.1875F;
			this.txtSecondLienHolder.Left = 1.125F;
			this.txtSecondLienHolder.MultiLine = false;
			this.txtSecondLienHolder.Name = "txtSecondLienHolder";
			this.txtSecondLienHolder.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtSecondLienHolder.Text = null;
			this.txtSecondLienHolder.Top = 4.875F;
			this.txtSecondLienHolder.Width = 3.25F;
			// 
			// txtLien2Address
			// 
			this.txtLien2Address.Height = 0.1875F;
			this.txtLien2Address.Left = 1.125F;
			this.txtLien2Address.MultiLine = false;
			this.txtLien2Address.Name = "txtLien2Address";
			this.txtLien2Address.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Address.Text = null;
			this.txtLien2Address.Top = 5.1875F;
			this.txtLien2Address.Width = 1.9375F;
			// 
			// txtLien2City
			// 
			this.txtLien2City.Height = 0.1875F;
			this.txtLien2City.Left = 3.8125F;
			this.txtLien2City.MultiLine = false;
			this.txtLien2City.Name = "txtLien2City";
			this.txtLien2City.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2City.Text = null;
			this.txtLien2City.Top = 5.1875F;
			this.txtLien2City.Width = 1.6875F;
			// 
			// txtLien2State
			// 
			this.txtLien2State.Height = 0.1875F;
			this.txtLien2State.Left = 5.5625F;
			this.txtLien2State.MultiLine = false;
			this.txtLien2State.Name = "txtLien2State";
			this.txtLien2State.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2State.Text = null;
			this.txtLien2State.Top = 5.1875F;
			this.txtLien2State.Width = 0.75F;
			// 
			// txtLien2Zip
			// 
			this.txtLien2Zip.Height = 0.1875F;
			this.txtLien2Zip.Left = 6.875F;
			this.txtLien2Zip.MultiLine = false;
			this.txtLien2Zip.Name = "txtLien2Zip";
			this.txtLien2Zip.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Zip.Text = null;
			this.txtLien2Zip.Top = 5.1875F;
			this.txtLien2Zip.Width = 1F;
			// 
			// txtOdometer
			// 
			this.txtOdometer.Height = 0.1875F;
			this.txtOdometer.Left = 2.375F;
			this.txtOdometer.MultiLine = false;
			this.txtOdometer.Name = "txtOdometer";
			this.txtOdometer.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtOdometer.Text = null;
			this.txtOdometer.Top = 3.6875F;
			this.txtOdometer.Width = 1.0625F;
			// 
			// txtMile
			// 
			this.txtMile.Height = 0.125F;
			this.txtMile.Left = 3.4375F;
			this.txtMile.MultiLine = false;
			this.txtMile.Name = "txtMile";
			this.txtMile.Style = "font-family: \'Arial\'; font-size: 8pt; vertical-align: bottom; ddo-char-set: 1";
			this.txtMile.Text = null;
			this.txtMile.Top = 3.638889F;
			this.txtMile.Width = 0.25F;
			// 
			// txtKilometer
			// 
			this.txtKilometer.Height = 0.125F;
			this.txtKilometer.Left = 3.4375F;
			this.txtKilometer.MultiLine = false;
			this.txtKilometer.Name = "txtKilometer";
			this.txtKilometer.Style = "font-family: \'Arial\'; font-size: 8pt; vertical-align: bottom; ddo-char-set: 1";
			this.txtKilometer.Text = null;
			this.txtKilometer.Top = 3.763889F;
			this.txtKilometer.Width = 0.25F;
			// 
			// txtActual
			// 
			this.txtActual.Height = 0.125F;
			this.txtActual.Left = 4.0625F;
			this.txtActual.MultiLine = false;
			this.txtActual.Name = "txtActual";
			this.txtActual.Style = "font-family: \'Arial\'; font-size: 8pt; vertical-align: bottom; ddo-char-set: 1";
			this.txtActual.Text = null;
			this.txtActual.Top = 3.638889F;
			this.txtActual.Width = 0.25F;
			// 
			// txtInExcess
			// 
			this.txtInExcess.Height = 0.125F;
			this.txtInExcess.Left = 4.0625F;
			this.txtInExcess.MultiLine = false;
			this.txtInExcess.Name = "txtInExcess";
			this.txtInExcess.Style = "font-family: \'Arial\'; font-size: 8pt; vertical-align: bottom; ddo-char-set: 1";
			this.txtInExcess.Text = null;
			this.txtInExcess.Top = 3.763889F;
			this.txtInExcess.Width = 0.25F;
			// 
			// txtNotActual
			// 
			this.txtNotActual.Height = 0.125F;
			this.txtNotActual.Left = 4.0625F;
			this.txtNotActual.MultiLine = false;
			this.txtNotActual.Name = "txtNotActual";
			this.txtNotActual.Style = "font-family: \'Arial\'; font-size: 8pt; vertical-align: bottom; ddo-char-set: 1";
			this.txtNotActual.Text = null;
			this.txtNotActual.Top = 3.888889F;
			this.txtNotActual.Width = 0.25F;
			// 
			// txtOdometerChanged
			// 
			this.txtOdometerChanged.Height = 0.125F;
			this.txtOdometerChanged.Left = 6.625F;
			this.txtOdometerChanged.Name = "txtOdometerChanged";
			this.txtOdometerChanged.Style = "font-family: \'Arial\'; font-size: 8pt; ddo-char-set: 1";
			this.txtOdometerChanged.Text = null;
			this.txtOdometerChanged.Top = 3.6875F;
			this.txtOdometerChanged.Width = 0.25F;
			// 
			// txtOdometerBroken
			// 
			this.txtOdometerBroken.Height = 0.125F;
			this.txtOdometerBroken.Left = 6.625F;
			this.txtOdometerBroken.Name = "txtOdometerBroken";
			this.txtOdometerBroken.Style = "font-family: \'Arial\'; font-size: 8pt; ddo-char-set: 1";
			this.txtOdometerBroken.Text = null;
			this.txtOdometerBroken.Top = 3.8125F;
			this.txtOdometerBroken.Width = 0.25F;
			// 
			// txtLien2Date
			// 
			this.txtLien2Date.Height = 0.1875F;
			this.txtLien2Date.Left = 6.5F;
			this.txtLien2Date.Name = "txtLien2Date";
			this.txtLien2Date.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.txtLien2Date.Text = null;
			this.txtLien2Date.Top = 4.875F;
			this.txtLien2Date.Width = 1.0625F;
			// 
			// fldDoublCTATitle
			// 
			this.fldDoublCTATitle.CanGrow = false;
			this.fldDoublCTATitle.Height = 0.1875F;
			this.fldDoublCTATitle.Left = 2F;
			this.fldDoublCTATitle.MultiLine = false;
			this.fldDoublCTATitle.Name = "fldDoublCTATitle";
			this.fldDoublCTATitle.Style = "font-family: \'Arial\'; text-align: center; vertical-align: bottom; ddo-char-set: 1" + "";
			this.fldDoublCTATitle.Text = "DOUBLE TITLE APPLICATION";
			this.fldDoublCTATitle.Top = 1.375F;
			this.fldDoublCTATitle.Visible = false;
			this.fldDoublCTATitle.Width = 3F;
			// 
			// txtRushTitle
			// 
			this.txtRushTitle.Height = 0.1875F;
			this.txtRushTitle.Left = 5.3125F;
			this.txtRushTitle.MultiLine = false;
			this.txtRushTitle.Name = "txtRushTitle";
			this.txtRushTitle.Style = "font-family: \'Arial\'; ddo-char-set: 1";
			this.txtRushTitle.Text = null;
			this.txtRushTitle.Top = 0.6875F;
			this.txtRushTitle.Width = 0.1875F;
			// 
			// txtIllegible
			// 
			this.txtIllegible.Height = 0.1875F;
			this.txtIllegible.Left = 7.0625F;
			this.txtIllegible.MultiLine = false;
			this.txtIllegible.Name = "txtIllegible";
			this.txtIllegible.Style = "font-family: \'Arial\'; ddo-char-set: 1";
			this.txtIllegible.Text = null;
			this.txtIllegible.Top = 1.6875F;
			this.txtIllegible.Width = 0.1875F;
			// 
			// txtDestroyed
			// 
			this.txtDestroyed.Height = 0.1875F;
			this.txtDestroyed.Left = 5.25F;
			this.txtDestroyed.MultiLine = false;
			this.txtDestroyed.Name = "txtDestroyed";
			this.txtDestroyed.Style = "font-family: \'Arial\'; ddo-char-set: 1";
			this.txtDestroyed.Text = null;
			this.txtDestroyed.Top = 1.6875F;
			this.txtDestroyed.Width = 0.1875F;
			// 
			// txtStolen
			// 
			this.txtStolen.Height = 0.1875F;
			this.txtStolen.Left = 3.3125F;
			this.txtStolen.MultiLine = false;
			this.txtStolen.Name = "txtStolen";
			this.txtStolen.Style = "font-family: \'Arial\'; ddo-char-set: 1";
			this.txtStolen.Text = null;
			this.txtStolen.Top = 1.6875F;
			this.txtStolen.Width = 0.1875F;
			// 
			// txtLost
			// 
			this.txtLost.Height = 0.1875F;
			this.txtLost.Left = 1.5625F;
			this.txtLost.MultiLine = false;
			this.txtLost.Name = "txtLost";
			this.txtLost.Style = "font-family: \'Arial\'; ddo-char-set: 1";
			this.txtLost.Text = null;
			this.txtLost.Top = 1.6875F;
			this.txtLost.Width = 0.1875F;
			// 
			// txtNewUsedRebuilt
			// 
			this.txtNewUsedRebuilt.Height = 0.1875F;
			this.txtNewUsedRebuilt.Left = 7.6875F;
			this.txtNewUsedRebuilt.MultiLine = false;
			this.txtNewUsedRebuilt.Name = "txtNewUsedRebuilt";
			this.txtNewUsedRebuilt.Style = "font-family: \'Roman 10cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.txtNewUsedRebuilt.Text = null;
			this.txtNewUsedRebuilt.Top = 3.0625F;
			this.txtNewUsedRebuilt.Width = 0.25F;
			// 
			// fldD
			// 
			this.fldD.Height = 0.125F;
			this.fldD.Left = 7.6875F;
			this.fldD.MultiLine = false;
			this.fldD.Name = "fldD";
			this.fldD.Style = "font-family: \'Roman 10cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.fldD.Text = null;
			this.fldD.Top = 7.0625F;
			this.fldD.Width = 0.25F;
			// 
			// fldUC
			// 
			this.fldUC.Height = 0.125F;
			this.fldUC.Left = 7.6875F;
			this.fldUC.MultiLine = false;
			this.fldUC.Name = "fldUC";
			this.fldUC.Style = "font-family: \'Roman 10cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.fldUC.Text = null;
			this.fldUC.Top = 7.1875F;
			this.fldUC.Width = 0.25F;
			// 
			// fldMC
			// 
			this.fldMC.Height = 0.125F;
			this.fldMC.Left = 7.6875F;
			this.fldMC.MultiLine = false;
			this.fldMC.Name = "fldMC";
			this.fldMC.Style = "font-family: \'Roman 10cpi\'; font-size: 8pt; vertical-align: bottom; ddo-char-set:" + " 1";
			this.fldMC.Text = null;
			this.fldMC.Top = 7.3125F;
			this.fldMC.Width = 0.25F;
			// 
			// fldDealerPlate
			// 
			this.fldDealerPlate.Height = 0.1875F;
			this.fldDealerPlate.Left = 6.6875F;
			this.fldDealerPlate.Name = "fldDealerPlate";
			this.fldDealerPlate.Style = "font-family: \'Arial\'; vertical-align: bottom; ddo-char-set: 1";
			this.fldDealerPlate.Text = null;
			this.fldDealerPlate.Top = 7.1875F;
			this.fldDealerPlate.Width = 0.75F;
			// 
			// rptDuplicateCTA
			//
			// 
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.6875F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBodyType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPurchaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFirstLienHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateofLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Address)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1City)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1State)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien1Zip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondLienHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Address)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2City)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2State)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Zip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKilometer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActual)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInExcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotActual)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerChanged)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOdometerBroken)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLien2Date)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDoublCTATitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRushTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIllegible)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDestroyed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStolen)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewUsedRebuilt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDealerPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBodyType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPurchaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFirstLienHolder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateofLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1Address;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1City;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1State;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien1Zip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondLienHolder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Address;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2City;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2State;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Zip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMile;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKilometer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtActual;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInExcess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotActual;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometerChanged;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOdometerBroken;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLien2Date;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDoublCTATitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRushTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIllegible;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDestroyed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStolen;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewUsedRebuilt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDealerPlate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
