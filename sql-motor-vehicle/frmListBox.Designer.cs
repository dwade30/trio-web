//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmListBox.
	/// </summary>
	partial class frmListBox
	{
		public fecherFoundation.FCListBox lstSelectPlate;
		public fecherFoundation.FCLabel lblHeader;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lstSelectPlate = new fecherFoundation.FCListBox();
			this.lblHeader = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 490);
			this.BottomPanel.Size = new System.Drawing.Size(601, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lstSelectPlate);
			this.ClientArea.Controls.Add(this.lblHeader);
			this.ClientArea.Location = new System.Drawing.Point(0, 0);
			this.ClientArea.Size = new System.Drawing.Size(601, 490);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(601, 0);
			this.TopPanel.Visible = false;
			// 
			// lstSelectPlate
			// 
			this.lstSelectPlate.Appearance = 0;
			this.lstSelectPlate.BackColor = System.Drawing.SystemColors.Window;
			this.lstSelectPlate.Location = new System.Drawing.Point(30, 80);
			this.lstSelectPlate.MultiSelect = 0;
			this.lstSelectPlate.Name = "lstSelectPlate";
			this.lstSelectPlate.Size = new System.Drawing.Size(542, 380);
			this.lstSelectPlate.Sorted = false;
			this.lstSelectPlate.TabIndex = 1;
			this.lstSelectPlate.ToolTipText = null;
			this.lstSelectPlate.DoubleClick += new System.EventHandler(this.lstSelectPlate_DoubleClick);
			// 
			// lblHeader
			// 
			this.lblHeader.Location = new System.Drawing.Point(30, 30);
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Size = new System.Drawing.Size(542, 30);
			this.lblHeader.TabIndex = 0;
			this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblHeader.ToolTipText = null;
			// 
			// frmListBox
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(601, 490);
			this.ControlBox = false;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmListBox";
			this.Text = "";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmListBox_Load);
			this.Activated += new System.EventHandler(this.frmListBox_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmListBox_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);

		}
		#endregion
	}
}