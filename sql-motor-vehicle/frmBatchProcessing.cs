//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Interfaces;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmBatchProcessing : BaseForm
	{
		public frmBatchProcessing()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmBatchProcessing InstancePtr
		{
			get
			{
				return (frmBatchProcessing)Sys.GetInstance(typeof(frmBatchProcessing));
			}
		}

		protected frmBatchProcessing _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public int intPrintedCol;
		public int intVINCol;
		public int intPlateCol;
		public int intYearCol;
		public int intMakeCol;
		public int intModelCol;
		public int intStyleCol;
		public int intAxlesCol;
		public int intUnitCol;
		public int intCTACol;
		public int intMonthStickerCol;
		public int intYearStickerCol;
		public int intMVR3Col;
		public int intMVR3ToVoidCol;
		public int intUserDefined1Col;
		public int intUserDefined2Col;
		public int intUserDefined3Col;
		public int intPriorStateCol;
		public int intPriorTitleCol;
		int intNumberCol;
		string strOldValue = "";
		bool blnBatchPrinted;
		bool blnPartiallyPrinted;
		public int intStartRow;
		// vbPorter upgrade warning: intNumberToPrint As int	OnWrite(double, int)
		public int intNumberToPrint;
		int intReprintPlateCol;
		int intReprintOldMVR3Col;
		int intReprintNewMVR3Col;
		int LastMVR3;
        private IStyleCodeService styleCodeService;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			fraReprint.Visible = false;
			blnPartiallyPrinted = false;
			blnBatchPrinted = true;
			for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
				{
					blnPartiallyPrinted = true;
				}
				else
				{
					blnBatchPrinted = false;
				}
			}
			if (blnBatchPrinted)
			{
				blnPartiallyPrinted = false;
			}
			if (blnBatchPrinted)
			{
				cmdFilePrint.Enabled = true;
				cmdSave.Enabled = true;
			}
			else
			{
				if (blnPartiallyPrinted == false)
				{
					mnuProcessQuit.Enabled = true;
					vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					txtNumberOfVehicles.Enabled = true;
				}
				cmdFilePrint.Enabled = true;
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					cmdFileQueue.Enabled = true;
				}
			}
		}

		private void cmdCancelReprint_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			fraReprintMVR3Entry.Visible = false;
			blnPartiallyPrinted = false;
			blnBatchPrinted = true;
			for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
				{
					blnPartiallyPrinted = true;
				}
				else
				{
					blnBatchPrinted = false;
				}
			}
			if (blnBatchPrinted)
			{
				blnPartiallyPrinted = false;
			}
			if (blnBatchPrinted)
			{
				cmdFilePrint.Enabled = true;
				cmdSave.Enabled = true;
			}
			else
			{
				if (blnPartiallyPrinted == false)
				{
					mnuProcessQuit.Enabled = true;
					vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					txtNumberOfVehicles.Enabled = true;
				}
				cmdFilePrint.Enabled = true;
			}
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int lngFormCheck = 0;
			bool blnFoundLow = false;
			bool blnFoundHigh = false;
			int intStart = 0;
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			try
			{
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					if (fecherFoundation.Strings.Trim(txtLow.Text) == "" || fecherFoundation.Strings.Trim(txtHigh.Text) == "")
					{
						MessageBox.Show("You must enter a low and a high vehicle number before you may continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}

                    if (Conversion.Val(txtLow.Text) < 1 || Conversion.Val(txtHigh.Text) < 1)
                    {
                        MessageBox.Show("You must enter a low and a high vehicle number that are greater than or equal to 1.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (Conversion.Val(txtLow.Text) > Conversion.Val(vsVehicles.TextMatrix(vsVehicles.Rows - 1, intNumberCol)) || Conversion.Val(txtHigh.Text) > Conversion.Val(vsVehicles.TextMatrix(vsVehicles.Rows - 1, intNumberCol)))
                    {
                        MessageBox.Show("You must enter a low and a high vehicle number that are less than or equal to the number of vehicles you are trying to register.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (Conversion.Val(txtLow.Text) > Conversion.Val(txtHigh.Text))
                    {
                        MessageBox.Show("The high vehicle number must be greater than the low number.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    fraReprint.Visible = false;
					intStartRow = FCConvert.ToInt32(Math.Round(Conversion.Val(txtLow.Text)));
					intNumberToPrint = FCConvert.ToInt16((Conversion.Val(txtHigh.Text) - intStartRow) + 1);
					if (!MotorVehicle.Statics.blnLongTermLaserForms)
					{
						if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to the top as possible) next to the line titled:" + "\r\n" + "STATE OF MAINE", 23, 1, MotorVehicle.Statics.MVR10PrinterName) == DialogResult.Cancel)
						{
							MotorVehicle.Statics.PrintMVR3 = false;
						}
						else
						{
                            rptMVRT10.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");

                        }
					}
					else
					{
                        rptMVRT10Laser.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                    }
					if (!MotorVehicle.Statics.blnLongTermLaserForms)
					{
						rptMVRT10.InstancePtr.Unload();
					}
					else
					{
						rptMVRT10Laser.InstancePtr.Unload();
					}

					if (MotorVehicle.Statics.PrintMVR3 == true)
					{
						if (fecherFoundation.Information.Err().Number == 0)
						{
							mnuProcessQuit.Enabled = false;
						}
					}
					else
					{
						cmdFilePrint.Enabled = true;
						return;
					}

					blnPartiallyPrinted = false;
					blnBatchPrinted = true;
					for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
					{
						//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
						//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
						if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
						{
							blnPartiallyPrinted = true;
						}
						else
						{
							blnBatchPrinted = false;
						}
					}
					if (blnBatchPrinted)
					{
						blnPartiallyPrinted = false;
					}
					if (blnBatchPrinted)
					{
						cmdFilePrint.Enabled = true;
						cmdSave.Enabled = true;
					}
					else
					{
						if (blnPartiallyPrinted == false)
						{
							mnuProcessQuit.Enabled = true;
							vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							txtNumberOfVehicles.Enabled = true;
						}
						cmdFilePrint.Enabled = true;
						if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
						{
							cmdFileQueue.Enabled = true;
						}
					}
				}
				else
				{
					if (fecherFoundation.Strings.Trim(txtLow.Text) == "" || fecherFoundation.Strings.Trim(txtHigh.Text) == "")
					{
						MessageBox.Show("You must enter a low and a high MVR3 number before you may continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					else if (Conversion.Val(txtLow.Text) > Conversion.Val(txtHigh.Text))
					{
						MessageBox.Show("The high form number must be greater than the low number.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					blnFoundHigh = false;
					blnFoundLow = false;
					lngFormCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(txtLow.Text)));
					vsReprint.Rows = 1;
					for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
					{
						if (Conversion.Val(vsVehicles.TextMatrix(counter, intMVR3Col)) == lngFormCheck)
						{
							intStart = counter;
							blnFoundLow = true;
							break;
						}
					}
					for (counter = intStart; counter <= (vsVehicles.Rows - 1); counter++)
					{
						if (Conversion.Val(vsVehicles.TextMatrix(counter, intMVR3Col)) == lngFormCheck)
						{
							vsReprint.Rows += 1;
							vsReprint.TextMatrix(vsReprint.Rows - 1, intReprintPlateCol, vsVehicles.TextMatrix(counter, intPlateCol));
							vsReprint.TextMatrix(vsReprint.Rows - 1, intReprintOldMVR3Col, vsVehicles.TextMatrix(counter, intMVR3Col));
							if (lngFormCheck == Conversion.Val(txtHigh.Text))
							{
								blnFoundHigh = true;
								break;
							}
							else if (lngFormCheck > Conversion.Val(txtHigh.Text))
							{
								MessageBox.Show("Could not find the entire range you entered.  Please check your numbers and try again.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
							else
							{
								lngFormCheck += 1;
							}
						}
						else
						{
							MessageBox.Show("Could not find the entire range you entered.  Please check your numbers and try again.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
					}
					if (blnFoundLow && blnFoundHigh)
					{
						fraReprint.Visible = false;
						fraReprintMVR3Entry.Visible = true;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				blnPartiallyPrinted = false;
				blnBatchPrinted = true;
				for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					{
						blnPartiallyPrinted = true;
					}
					else
					{
						blnBatchPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					blnPartiallyPrinted = false;
				}
				if (blnBatchPrinted)
				{
					cmdFilePrint.Enabled = true;
					cmdSave.Enabled = true;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = true;
						vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						txtNumberOfVehicles.Enabled = true;
					}
					cmdFilePrint.Enabled = true;
					if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
					{
						cmdFileQueue.Enabled = true;
					}
				}
			}
		}

		private void cmdOKReprint_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			object intRepeatCounter;
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			int lngFormCheck = 0;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				fraReprintMVR3Entry.Visible = false;
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
				}
				else
				{
					for (counter = 1; counter <= (vsReprint.Rows - 1); counter++)
					{
						if (vsReprint.TextMatrix(counter, intReprintNewMVR3Col) == "")
						{
							MessageBox.Show("You have 1 or more vehicles with no MVR3 Number assigned to it.", "Missing MVR3 Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
							vsReprint.Select(counter, intReprintNewMVR3Col);
							return;
						}
					}
					intStartRow = 0;
					intNumberToPrint = 0;
					for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
					{
						if (vsVehicles.TextMatrix(counter, intMVR3Col) == vsReprint.TextMatrix(1 + intNumberToPrint, intReprintOldMVR3Col))
						{
							if (intNumberToPrint == 0)
							{
								intStartRow = counter;
							}
							vsVehicles.TextMatrix(counter, intMVR3ToVoidCol, vsVehicles.TextMatrix(counter, intMVR3Col));
							vsVehicles.TextMatrix(counter, intMVR3Col, vsReprint.TextMatrix(1 + intNumberToPrint, intReprintNewMVR3Col));
							intNumberToPrint += 1;
							if (1 + intNumberToPrint == vsReprint.Rows)
							{
								break;
							}
						}
					}
					intNumberToPrint = 0;
					for (intCounter = intStartRow; intCounter <= (intStartRow + vsReprint.Rows - 2); intCounter++)
					{
						if (intCounter == intStartRow)
						{
							lngFormCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(vsVehicles.TextMatrix(intCounter, intMVR3Col))));
							intNumberToPrint = 1;
						}
						else
						{
							lngFormCheck += 1;
							if (lngFormCheck == Conversion.Val(vsVehicles.TextMatrix(intCounter, intMVR3Col)))
							{
								intNumberToPrint += 1;
							}
							else
							{
                                rptMVR3Laser.InstancePtr.PrintReportOnDotMatrix("MVR3PrinterName");
                                rptMVR3Laser.InstancePtr.Unload();
								if (MotorVehicle.Statics.PrintMVR3 == true)
								{
									if (fecherFoundation.Information.Err().Number == 0)
									{
										mnuProcessQuit.Enabled = false;
									}
								}
								else
								{
									cmdFilePrint.Enabled = true;
									return;
								}
								intStartRow = intCounter;
								intNumberToPrint = 1;
								lngFormCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(vsVehicles.TextMatrix(intCounter, intMVR3Col))));
								MessageBox.Show("Please insert your next batch of MVR3 forms starting with the number " + FCConvert.ToString(lngFormCheck) + " and click OK when you are ready to continue printing.", "Insert MVR3 Forms", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
					if (intNumberToPrint == vsReprint.Rows - 1)
					{
						rptMVR3Laser.InstancePtr.PrintReportOnDotMatrix("MVR3PrinterName");
						rptMVR3Laser.InstancePtr.Unload();
						if (MotorVehicle.Statics.PrintMVR3 == true)
						{
							if (fecherFoundation.Information.Err().Number == 0)
							{
								mnuProcessQuit.Enabled = false;
							}
						}
						else
						{
							cmdFilePrint.Enabled = true;
							return;
						}
					}
					LastMVR3 = FCConvert.ToInt32(Math.Round(Conversion.Val(vsReprint.TextMatrix(vsReprint.Rows - 1, intReprintNewMVR3Col))));
					blnPartiallyPrinted = false;
					blnBatchPrinted = true;
					for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
					{
						//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
						//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
						if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
						{
							blnPartiallyPrinted = true;
						}
						else
						{
							blnBatchPrinted = false;
						}
					}
					if (blnBatchPrinted)
					{
						blnPartiallyPrinted = false;
					}
					if (blnBatchPrinted)
					{
						cmdFilePrint.Enabled = true;
						cmdSave.Enabled = true;
					}
					else
					{
						if (blnPartiallyPrinted == false)
						{
							mnuProcessQuit.Enabled = true;
							vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							txtNumberOfVehicles.Enabled = true;
						}
						cmdFilePrint.Enabled = true;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				blnPartiallyPrinted = false;
				blnBatchPrinted = true;
				for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					{
						blnPartiallyPrinted = true;
					}
					else
					{
						blnBatchPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					blnPartiallyPrinted = false;
				}
				if (blnBatchPrinted)
				{
					cmdFilePrint.Enabled = true;
					cmdSave.Enabled = true;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = true;
						vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						txtNumberOfVehicles.Enabled = true;
					}
					cmdFilePrint.Enabled = true;
				}
			}
		}

		private void frmBatchProcessing_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			txtNumberOfVehicles.SelectionStart = 0;
			txtNumberOfVehicles.SelectionLength = 1;
			this.Refresh();
		}

		private void frmBatchProcessing_Load(object sender, System.EventArgs e)
		{
            if (styleCodeService == null)
            {
                styleCodeService = TWSharedLibrary.StaticSettings.GlobalCommandDispatcher.Send(new GetStyleCodeService()).Result;
            }

			if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
			{
				vsVehicles.Cols = 12;
				intNumberCol = 0;
				intPrintedCol = 1;
				intVINCol = 2;
				intPlateCol = 3;
				intYearCol = 4;
				intMakeCol = 5;
				intStyleCol = 6;
				intUnitCol = 7;
				intCTACol = 8;
				intUserDefined1Col = 9;
				intUserDefined2Col = 10;
				intUserDefined3Col = 11;
				intMVR3ToVoidCol = -1;
				intModelCol = -1;
				intAxlesCol = -1;
				intMonthStickerCol = -1;
				intYearStickerCol = -1;
				intMVR3Col = -1;
				intPriorTitleCol = -1;
				intPriorStateCol = -1;
			}
			else
			{
				intReprintPlateCol = 0;
				intReprintOldMVR3Col = 1;
				intReprintNewMVR3Col = 2;
				vsVehicles.Cols = 20;
				intNumberCol = 0;
				intPrintedCol = 1;
				intMVR3ToVoidCol = 2;
				intVINCol = 3;
				intPlateCol = 4;
				intYearCol = 5;
				intMakeCol = 6;
				intModelCol = 7;
				intStyleCol = 8;
				intAxlesCol = 9;
				intUnitCol = 10;
				intCTACol = 11;
				intPriorTitleCol = 12;
				intPriorStateCol = 13;
				intMonthStickerCol = 14;
				intYearStickerCol = 15;
				intMVR3Col = 16;
				intUserDefined1Col = 17;
				intUserDefined2Col = 18;
				intUserDefined3Col = 19;
			}
			FormatGrid();
			LoadGrid();
			blnBatchPrinted = false;
			blnPartiallyPrinted = false;
			MotorVehicle.Statics.gboolFromBatchRegistration = true;
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry && (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION"))
			{
				cmdFileQueue.Visible = true;
			}
			else
			{
				cmdFileQueue.Visible = false;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmBatchProcessing_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (blnBatchPrinted || blnPartiallyPrinted)
				{
					MessageBox.Show("You must complete the batch process now that the forms have been printed.", "Complete Process", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
				{
					if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
					{
						for (counter = 2; counter <= (vsVehicles.Rows - 1); counter++)
						{
							if (vsVehicles.TextMatrix(counter, intPlateCol) != "")
							{
								MotorVehicle.ReturnLongTermPlate(vsVehicles.TextMatrix(counter, intPlateCol));
							}
						}
					}
				}
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				if (blnBatchPrinted || blnPartiallyPrinted)
				{
					MessageBox.Show("You must complete the batch process now that the forms have been printed.", "Complete Process", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
				{
					if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
					{
						for (counter = 2; counter <= (vsVehicles.Rows - 1); counter++)
						{
							if (vsVehicles.TextMatrix(counter, intPlateCol) != "")
							{
								MotorVehicle.ReturnLongTermPlate(vsVehicles.TextMatrix(counter, intPlateCol));
							}
						}
					}
				}
			}
			MotorVehicle.Statics.gboolFromBatchRegistration = false;
		}

		private void frmBatchProcessing_Resize(object sender, System.EventArgs e)
		{
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
			{
				vsVehicles.ColWidth(intNumberCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.05));
				vsVehicles.ColWidth(intVINCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.24));
				vsVehicles.ColWidth(intPlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intYearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intMakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intStyleCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.06));
				vsVehicles.ColWidth(intUnitCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
				vsVehicles.ColWidth(intCTACol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intUserDefined1Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
				vsVehicles.ColWidth(intUserDefined2Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
				vsVehicles.ColWidth(intUserDefined3Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
			}
			else
			{
				vsVehicles.ColWidth(intNumberCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.05));
				vsVehicles.ColWidth(intVINCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.24));
				vsVehicles.ColWidth(intPlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intYearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intMakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intModelCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.09));
				vsVehicles.ColWidth(intStyleCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.06));
				vsVehicles.ColWidth(intAxlesCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.06));
				vsVehicles.ColWidth(intUnitCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intCTACol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intPriorTitleCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intPriorStateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intMonthStickerCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intYearStickerCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intMVR3Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intUserDefined1Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
				vsVehicles.ColWidth(intUserDefined2Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
				vsVehicles.ColWidth(intUserDefined3Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
				vsReprint.ColWidth(intReprintPlateCol, FCConvert.ToInt32(vsReprint.WidthOriginal * 0.33));
				vsReprint.ColWidth(intReprintOldMVR3Col, FCConvert.ToInt32(vsReprint.WidthOriginal * 0.33));
				vsReprint.ColWidth(intReprintNewMVR3Col, FCConvert.ToInt32(vsReprint.WidthOriginal * 0.05));
			}

            fraReprint.CenterToContainer(this.ClientArea);
            fraReprintMVR3Entry.CenterToContainer(this.ClientArea);
        }

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (txtNumberOfVehicles.Enabled == true)
			{
				txtNumberOfVehicles.Focus();
			}
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
			{
				for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
				{
					if (vsVehicles.TextMatrix(counter, intPlateCol) == "")
					{
						MessageBox.Show("You have 1 or more vehicles with no Plate assigned to it.", "Missing Plate", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsVehicles.Select(counter, intPlateCol);
						return;
					}
					else if (VerifyMake(vsVehicles.TextMatrix(counter, intMakeCol)) == false)
					{
						vsVehicles.Select(counter, intMakeCol);
						return;
					}
					else if (VerifyStyle(vsVehicles.TextMatrix(counter, intStyleCol), vsVehicles.TextMatrix(counter, intPlateCol)) == false)
					{
						vsVehicles.Select(counter, intStyleCol);
						return;
					}
					else if (VerifyVIN(vsVehicles.TextMatrix(counter, intVINCol)) == false)
					{
						vsVehicles.Select(counter, intVINCol);
						return;
					}
					else if (VerifyYear(vsVehicles.TextMatrix(counter, intYearCol), vsVehicles.TextMatrix(counter, intVINCol)) == false)
					{
						vsVehicles.Select(counter, intYearCol);
						return;
					}
				}
				PrintMVR10Batch();
			}
			else
			{
				for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
				{
					if (vsVehicles.TextMatrix(counter, intMVR3Col) == "")
					{
						MessageBox.Show("You have 1 or more vehicles with no MVR3 Number assigned to it.", "Missing MVR3 Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsVehicles.Select(counter, intMVR3Col);
						return;
					}
					else if (vsVehicles.TextMatrix(counter, intPlateCol) == "")
					{
						MessageBox.Show("You have 1 or more vehicles with no Plate assigned to it.", "Missing Plate", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsVehicles.Select(counter, intPlateCol);
						return;
					}
					else if (vsVehicles.TextMatrix(counter, intYearStickerCol) == "")
					{
						MessageBox.Show("You have 1 or more vehicles with no Year Sticker assigned to it.", "Missing Year Sticker", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsVehicles.Select(counter, intYearStickerCol);
						return;
					}
					else if (vsVehicles.TextMatrix(counter, intMonthStickerCol) == "")
					{
						MessageBox.Show("You have 1 or more vehicles with no Month Sticker assigned to it.", "Missing Month Sticker", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsVehicles.Select(counter, intMonthStickerCol);
						return;
					}
					else if (VerifyMake(vsVehicles.TextMatrix(counter, intMakeCol)) == false)
					{
						vsVehicles.Select(counter, intMakeCol);
						return;
					}
					else if (VerifyModel(vsVehicles.TextMatrix(counter, intModelCol)) == false)
					{
						vsVehicles.Select(counter, intModelCol);
						return;
					}
					else if (VerifyStyle(vsVehicles.TextMatrix(counter, intStyleCol), vsVehicles.TextMatrix(counter, intPlateCol)) == false)
					{
						vsVehicles.Select(counter, intStyleCol);
						return;
					}
					else if (VerifyVIN(vsVehicles.TextMatrix(counter, intVINCol)) == false)
					{
						vsVehicles.Select(counter, intVINCol);
						return;
					}
					else if (VerifyYear(vsVehicles.TextMatrix(counter, intYearCol), vsVehicles.TextMatrix(counter, intVINCol)) == false)
					{
						vsVehicles.Select(counter, intYearCol);
						return;
					}
				}
				PrintMVR3Batch();
			}
		}

		private void mnuFileQueue_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (txtNumberOfVehicles.Enabled == true)
			{
				txtNumberOfVehicles.Focus();
			}
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
			{
				for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
				{
					if (vsVehicles.TextMatrix(counter, intPlateCol) == "")
					{
						MessageBox.Show("You have 1 or more vehicles with no Plate assigned to it.", "Missing Plate", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsVehicles.Select(counter, intPlateCol);
						return;
					}
					else if (VerifyMake(vsVehicles.TextMatrix(counter, intMakeCol)) == false)
					{
						vsVehicles.Select(counter, intMakeCol);
						return;
					}
					else if (VerifyStyle(vsVehicles.TextMatrix(counter, intStyleCol), vsVehicles.TextMatrix(counter, intPlateCol)) == false)
					{
						vsVehicles.Select(counter, intStyleCol);
						return;
					}
					else if (VerifyVIN(vsVehicles.TextMatrix(counter, intVINCol)) == false)
					{
						vsVehicles.Select(counter, intVINCol);
						return;
					}
					else if (VerifyYear(vsVehicles.TextMatrix(counter, intYearCol), vsVehicles.TextMatrix(counter, intVINCol)) == false)
					{
						vsVehicles.Select(counter, intYearCol);
						return;
					}
				}
				QueueMVR10Batch();
			}
		}

		private void QueueMVR10Batch()
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			int fnx;
			double TitleFee = 0;
			int intLockCount = 0;
			// vbPorter upgrade warning: intChoice As int	OnWrite(DialogResult)
			DialogResult intChoice = 0;
			// vbPorter upgrade warning: intRndCount As int	OnWriteFCConvert.ToInt32(
			int intRndCount = 0;
			int x;
			clsDRWrapper rsTestFleetCTA = new clsDRWrapper();
			clsDRWrapper rsUpdate = new clsDRWrapper();
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			clsDRWrapper rsTitle = new clsDRWrapper();
			clsDRWrapper rsInventory = new clsDRWrapper();
			clsDRWrapper rsBatchInfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ShowLineNumber
				fecherFoundation.Information.Err().Clear();
				rsBatchInfo.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
				rsBatchInfo.AddNew();
				for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
				{
					if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "ID")
					{
						// do nothing
					}
					else
					{
						rsBatchInfo.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
					}
				}
				rsBatchInfo.Set_Fields("Plate", vsVehicles.TextMatrix(1, intPlateCol));
				rsBatchInfo.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(rsBatchInfo.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
				rsBatchInfo.Set_Fields("VIN", vsVehicles.TextMatrix(1, intVINCol));
				rsBatchInfo.Set_Fields("Year", vsVehicles.TextMatrix(1, intYearCol));
				rsBatchInfo.Set_Fields("Make", vsVehicles.TextMatrix(1, intMakeCol));
				rsBatchInfo.Set_Fields("Style", vsVehicles.TextMatrix(1, intStyleCol));
				rsBatchInfo.Set_Fields("UnitNumber", vsVehicles.TextMatrix(1, intUnitCol));
				rsBatchInfo.Set_Fields("TitleNumber", vsVehicles.TextMatrix(1, intCTACol).ToUpper());
				rsBatchInfo.Set_Fields("UserField1", vsVehicles.TextMatrix(1, intUserDefined1Col));
				rsBatchInfo.Set_Fields("UserField2", vsVehicles.TextMatrix(1, intUserDefined2Col));
				rsBatchInfo.Set_Fields("UserField3", vsVehicles.TextMatrix(1, intUserDefined3Col));
				this.Hide();
				frmSavePrint.InstancePtr.Hide();
				//App.DoEvents();
				for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
				{
					MotorVehicle.Statics.rsFinal.AddNew();
					for (fnx = 1; fnx <= rsBatchInfo.FieldsCount - 1; fnx++)
					{
						if (rsBatchInfo.Get_FieldsIndexName(fnx) == "ID")
						{
							// do nothing
						}
						else
						{
							MotorVehicle.Statics.rsFinal.Set_Fields(rsBatchInfo.Get_FieldsIndexName(fnx), rsBatchInfo.Get_FieldsIndexValue(fnx));
						}
					}
					MotorVehicle.Statics.rsFinal.Set_Fields("Plate", vsVehicles.TextMatrix(counter, intPlateCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
					MotorVehicle.Statics.rsFinal.Set_Fields("VIN", vsVehicles.TextMatrix(counter, intVINCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Year", vsVehicles.TextMatrix(counter, intYearCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Make", vsVehicles.TextMatrix(counter, intMakeCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Style", vsVehicles.TextMatrix(counter, intStyleCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber", vsVehicles.TextMatrix(counter, intUnitCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("TitleNumber", vsVehicles.TextMatrix(counter, intCTACol).ToUpper());
					MotorVehicle.Statics.rsFinal.Set_Fields("UserField1", vsVehicles.TextMatrix(counter, intUserDefined1Col));
					MotorVehicle.Statics.rsFinal.Set_Fields("UserField2", vsVehicles.TextMatrix(counter, intUserDefined2Col));
					MotorVehicle.Statics.rsFinal.Set_Fields("UserField3", vsVehicles.TextMatrix(counter, intUserDefined3Col));
					if (frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.ListIndex)  == 24)
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", true);
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", false);
					}
					rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = 0");
					rsAM.AddNew();
					rsAM.Set_Fields("RegistrationType", MotorVehicle.Statics.RegistrationType);
					rsAM.Set_Fields("DateUpdated", DateTime.Now);
					rsAM.Set_Fields("TitleDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"));
					rsAM.Set_Fields("UseTaxDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone"));
					if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("UseTaxDone")))
					{
						rsAM.Set_Fields("UseTaxID", MotorVehicle.Statics.lngUTCAddNewID);
					}
					rsAM.Set_Fields("Make", MotorVehicle.Statics.rsFinal.Get_Fields_String("Make"));
					rsAM.Set_Fields("Unit", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
					rsAM.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
					rsAM.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
					rsAM.Set_Fields("Color", MotorVehicle.Statics.rsFinal.Get_Fields_String("Color1") + MotorVehicle.Statics.rsFinal.Get_Fields_String("Color2"));
					rsAM.Set_Fields("MaineReregistration", false);
					rsAM.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
					rsAM.Set_Fields("VIN", MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN"));
					rsAM.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
					rsAM.Set_Fields("NetWeight", MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
					rsAM.Set_Fields("OwnerName", MotorVehicle.Statics.rsFinal.Get_Fields("Owner1"));
					rsAM.Set_Fields("Address1", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
					rsAM.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
					rsAM.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
					rsAM.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
					rsAM.Set_Fields("FleetNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
					rsAM.Set_Fields("OldPlate", "");
					rsAM.Set_Fields("LegalResAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
					rsAM.Set_Fields("LegalResCity", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
					rsAM.Set_Fields("LegalResState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
					rsAM.Set_Fields("LegalResZip", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResZip"));
					rsAM.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
					rsAM.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
					rsAM.Set_Fields("NumberOfYears", frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.ListIndex));
					rsAM.Set_Fields("RegistrationFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERegFee.Text);
					rsAM.Set_Fields("RegistrationCredit", frmDataInputLongTermTrailers.InstancePtr.txtFEECreditTransfer.Text);
					rsAM.Set_Fields("TitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETitleFee.Text);
					rsAM.Set_Fields("RushTitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERushTitleFee.Text);
					rsAM.Set_Fields("SalesTax", frmDataInputLongTermTrailers.InstancePtr.txtFEESalesTax.Text);
					rsAM.Set_Fields("StateFee", frmDataInputLongTermTrailers.InstancePtr.txtFees.Text);
					rsAM.Set_Fields("AgentFee", frmDataInputLongTermTrailers.InstancePtr.txtFEEAgentFeeReg.Text);
					rsAM.Set_Fields("AgentFeeTitle", frmDataInputLongTermTrailers.InstancePtr.txtFeeAgentFeeCTA.Text);
					rsAM.Set_Fields("TransferFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETransferFee.Text);
					rsAM.Set_Fields("Status", "A");
					if (frmDataInputLongTermTrailers.InstancePtr.chkTimePayment.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsAM.Set_Fields("TimePayment", true);
					}
					else
					{
						rsAM.Set_Fields("TimePayment", false);
					}
					rsAM.Set_Fields("TwentyFiveYearPlate", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwentyFiveYearPlate"));
					MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", rsAM.Get_Fields_DateTime("DateUpdated"));
					MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveID", 0);
					if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
					{
						MotorVehicle.Write_PDS_Work_Record_Stuff_MVRT10();
					}
					rsAM.Update();
					MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegID", rsAM.Get_Fields_Int32("ID"));
					if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") == true)
					{
						rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
						if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
						{
							rsTitle.MoveLast();
							rsTitle.MoveFirst();
							rsUpdate.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE ID = 0");
							if (MotorVehicle.Statics.FleetCTA)
							{
								if (MotorVehicle.Statics.VehiclesCovered > 1)
								{
									rsTestFleetCTA.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber") + "'");
									if (rsTestFleetCTA.EndOfFile() != true && rsTestFleetCTA.BeginningOfFile() != true)
									{
										goto SkipLTCTA;
									}
								}
								TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered;
							}
							else
							{
								TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee");
							}
							rsUpdate.AddNew();
							rsUpdate.Set_Fields("CTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
							rsUpdate.Set_Fields("Fee", TitleFee);
							rsUpdate.Set_Fields("RushFee", MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee"));
							rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
							rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
							rsUpdate.Set_Fields("RegistrationDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							rsUpdate.Set_Fields("CustomerName", rsTitle.Get_Fields_String("Name1"));
							rsUpdate.Set_Fields("OPID", MotorVehicle.Statics.UserID);
							rsUpdate.Set_Fields("DoubleIfApplicable", rsTitle.Get_Fields_String("DoubleNumber"));
							rsUpdate.Set_Fields("PeriodCloseoutID", 0);
							rsUpdate.Set_Fields("MSRP", rsTitle.Get_Fields_String("MSRP"));
							rsUpdate.Update();
						}
						if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
						{
							MotorVehicle.Statics.rsDoubleTitle.Update();
						}
					}
					SkipLTCTA:
					;
					if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
					{
						// And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
						if (MotorVehicle.Statics.strCodeP == "")
						{
							MakeCodeP();
						}
						rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("AdjustmentCode", "I");
						rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
						rsUpdate.Set_Fields("QuantityAdjusted", 1);
						rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
						rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
						rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "- MVR10 Form");
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Set_Fields("TellerCloseoutID", 0);
						rsUpdate.Update();
					}
					if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
					{
						// And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
						MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory & '' = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
						rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
						if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
						{
							rsInventory.Edit();
							if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
							{
								rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							}
							rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
							// .Fields("MVR3") =
							rsInventory.Set_Fields("Status", "I");
							rsInventory.Set_Fields("PeriodCloseoutID", 0);
							rsInventory.Set_Fields("TellerCloseoutID", 0);
							rsInventory.Update();
						}
					}
					MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
					clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
					MotorVehicle.TransferRecordToPending(ref temp);
					MotorVehicle.Statics.rsFinal = temp;
					MotorVehicle.Statics.rsFinal.Update();
				}
				if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
				{
					MotorVehicle.Write_Fleet_PDS_Work_Record_Stuff();
				}
				//App.DoEvents();
				MotorVehicle.Statics.rsFinalCompare.Reset();
				if (fecherFoundation.Information.Err().Number == 0)
				{
					MotorVehicle.Statics.MVR3saved = true;
				}
				else
				{
					MotorVehicle.Statics.MVR3saved = false;
				}
				MotorVehicle.ClearAllVariables();
				frmWait.InstancePtr.Unload();
				frmDataInput.InstancePtr.Unload();
				frmDataInputLongTermTrailers.InstancePtr.Unload();
				frmPreview.InstancePtr.Unload();
				if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
				{
					//frmPlateInfo.InstancePtr.Show(App.MainForm);
					frmPlateInfo.InstancePtr.ShowForm();
				}
				else if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
				{
					//App.DoEvents();
					//MDIParent.InstancePtr.GetMainMenu();
					MDIParent.InstancePtr.Menu18();
					Close();
					//Application.Exit();
					App.MainForm.EndWaitMVModule();
					return;
				}
				frmSavePrint.InstancePtr.Unload();
				Close();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "An error occurred on line " + fecherFoundation.Information.Erl(), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume FailedUpdate; */
				SaveError:
				;
				switch (fecherFoundation.Information.Err(ex).Number)
				{
					case 3260:
						{
							intLockCount += 1;
							if (intLockCount > 5)
							{
								intChoice = MessageBox.Show("Error Trying to Save.  This record is currently locked by another user.  Would you like to try to save again?", "Record Locked", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);
								if (intChoice == DialogResult.Retry)
								{
									intLockCount = 1;
								}
								else
								{
									/*? Resume FailedUpdate; */
								}
							}
							//App.DoEvents();
							intRndCount = (FCConvert.ToInt32(Math.Pow(intLockCount, 2)) * 4000);
							for (x = 1; x <= intRndCount; x++)
							{
							}
							// x
							/*? Resume; */
							break;
						}
					default:
						{
							MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "A file called SaveErr.txt has been created in the " + Environment.CurrentDirectory + " directory of your server.  Please notify TRIO of your problem and E-mail the SaveErr.txt file so we may more quickly find the problem you are encountering.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							/*? Resume FailedUpdate; */
							break;
						}
				}
				//end switch
				FailedUpdate:
				;
				MotorVehicle.ClearAllVariables();
				frmDataInput.InstancePtr.Unload();
				frmPreview.InstancePtr.Unload();
				frmSavePrint.InstancePtr.Unload();
				Close();
				MotorVehicle.Statics.rsFinal = null;
				MotorVehicle.Statics.rsFinalCompare = null;
				if (!MotorVehicle.Statics.bolFromDosTrio && !MotorVehicle.Statics.bolFromWindowsCR)
				{
					//frmPlateInfo.InstancePtr.Show(App.MainForm);
					frmPlateInfo.InstancePtr.ShowForm();
				}
				else
				{
					////MDIParent.InstancePtr.GRID.Focus();
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					for (counter = 2; counter <= (vsVehicles.Rows - 1); counter++)
					{
						if (vsVehicles.TextMatrix(counter, intPlateCol) != "")
						{
							MotorVehicle.ReturnLongTermPlate(vsVehicles.TextMatrix(counter, intPlateCol));
						}
					}
				}
			}
			Close();
		}

		private void PrintMVR3Batch()
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			int lngFormCheck = 0;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				mnuProcessQuit.Enabled = false;
				cmdFilePrint.Enabled = false;
				cmdSave.Enabled = false;
				vsVehicles.Editable = FCGrid.EditableSettings.flexEDNone;
				txtNumberOfVehicles.Enabled = false;
				if (blnBatchPrinted || blnPartiallyPrinted)
				{
					txtHigh.Text = "";
					txtLow.Text = "";
					fraReprint.Visible = true;
					txtLow.Focus();
				}
				else
				{
					intNumberToPrint = 0;
					intStartRow = 1;
					for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
					{
						if (intCounter == 1)
						{
							lngFormCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(vsVehicles.TextMatrix(intCounter, intMVR3Col))));
							intNumberToPrint += 1;
						}
						else
						{
							lngFormCheck += 1;
							if (lngFormCheck == Conversion.Val(vsVehicles.TextMatrix(intCounter, intMVR3Col)))
							{
								intNumberToPrint += 1;
							}
							else
							{
                                rptMVR3Laser.InstancePtr.PrintReportOnDotMatrix("MVR3PrinterName");
                                rptMVR3Laser.InstancePtr.Unload();
								if (MotorVehicle.Statics.PrintMVR3 == true)
								{
									if (fecherFoundation.Information.Err().Number == 0)
									{
										mnuProcessQuit.Enabled = false;
									}
								}
								else
								{
									cmdFilePrint.Enabled = true;
									return;
								}
								intStartRow = intCounter;
								intNumberToPrint = 1;
								lngFormCheck = FCConvert.ToInt32(Math.Round(Conversion.Val(vsVehicles.TextMatrix(intCounter, intMVR3Col))));
								MessageBox.Show("Please insert your next batch of MVR3 forms starting with the number " + FCConvert.ToString(lngFormCheck) + " and click OK when you are ready to continue printing.", "Insert MVR3 Forms", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
					if (intStartRow == 1)
					{
                        rptMVR3Laser.InstancePtr.PrintReportOnDotMatrix("MVR3PrinterName");
                        rptMVR3Laser.InstancePtr.Unload();
						if (MotorVehicle.Statics.PrintMVR3 == true)
						{
							if (fecherFoundation.Information.Err().Number == 0)
							{
								mnuProcessQuit.Enabled = false;
							}
						}
						else
						{
							cmdFilePrint.Enabled = true;
							return;
						}
					}
					blnPartiallyPrinted = false;
					blnBatchPrinted = true;
					for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
					{
						if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
						{
							blnPartiallyPrinted = true;
						}
						else
						{
							blnBatchPrinted = false;
						}
					}
					if (blnBatchPrinted)
					{
						blnPartiallyPrinted = false;
					}
					LastMVR3 = FCConvert.ToInt32(Math.Round(Conversion.Val(vsVehicles.TextMatrix(vsVehicles.Rows - 1, intMVR3Col))));
				}
				if (blnBatchPrinted)
				{
					cmdFilePrint.Enabled = true;
					cmdSave.Enabled = true;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = true;
						vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						txtNumberOfVehicles.Enabled = true;
					}
					cmdFilePrint.Enabled = true;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				blnPartiallyPrinted = false;
				blnBatchPrinted = true;
				for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					{
						blnPartiallyPrinted = true;
					}
					else
					{
						blnBatchPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					blnPartiallyPrinted = false;
				}
				if (blnBatchPrinted)
				{
					cmdFilePrint.Enabled = true;
					cmdSave.Enabled = true;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = true;
						vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						txtNumberOfVehicles.Enabled = true;
					}
					cmdFilePrint.Enabled = true;
				}
			}
		}

		private void PrintMVR10Batch()
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			int lngFormCheck;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				mnuProcessQuit.Enabled = false;
				cmdFilePrint.Enabled = false;
				cmdFileQueue.Enabled = false;
				cmdSave.Enabled = false;
				vsVehicles.Editable = FCGrid.EditableSettings.flexEDNone;
				txtNumberOfVehicles.Enabled = false;
				if (blnBatchPrinted || blnPartiallyPrinted)
				{
					txtHigh.Text = "";
					txtLow.Text = "";
					fraReprint.Visible = true;
					txtLow.Focus();
					Label2.Text = "Please enter the range of vehicle numbers you wish to reprint";
				}
				else
				{
					intStartRow = 1;
					intNumberToPrint = (vsVehicles.Rows - 1);
					if (!MotorVehicle.Statics.blnLongTermLaserForms)
					{
						if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to the top as possible) next to the line titled:" + "\r\n" + "STATE OF MAINE", 23, 1, MotorVehicle.Statics.MVR10PrinterName) == DialogResult.Cancel)
						{
							MotorVehicle.Statics.PrintMVR3 = false;
						}
						else
						{
                            rptMVRT10.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                        }
					}
					else
					{
                        rptMVRT10Laser.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                    }
					if (!MotorVehicle.Statics.blnLongTermLaserForms)
					{
						rptMVRT10.InstancePtr.Unload();
					}
					else
					{
						rptMVRT10Laser.InstancePtr.Unload();
					}
					if (MotorVehicle.Statics.PrintMVR3 == true)
					{
						if (fecherFoundation.Information.Err().Number == 0)
						{
							mnuProcessQuit.Enabled = false;
						}
					}
					else
					{
						cmdFilePrint.Enabled = true;
						cmdFileQueue.Enabled = true;
						return;
					}
					blnPartiallyPrinted = false;
					blnBatchPrinted = true;
					for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
					{
						if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
						{
							blnPartiallyPrinted = true;
						}
						else
						{
							blnBatchPrinted = false;
						}
					}
					if (blnBatchPrinted)
					{
						blnPartiallyPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					cmdFilePrint.Enabled = true;
					cmdSave.Enabled = true;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = true;
						vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						txtNumberOfVehicles.Enabled = true;
					}
					cmdFilePrint.Enabled = true;
					cmdFileQueue.Enabled = true;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				blnPartiallyPrinted = false;
				blnBatchPrinted = true;
				for (intCounter = 1; intCounter <= (vsVehicles.Rows - 1); intCounter++)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					if (FCConvert.CBool(vsVehicles.TextMatrix(intCounter, intPrintedCol)) == true)
					{
						blnPartiallyPrinted = true;
					}
					else
					{
						blnBatchPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					blnPartiallyPrinted = false;
				}
				if (blnBatchPrinted)
				{
					cmdFilePrint.Enabled = true;
					cmdSave.Enabled = true;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = true;
						vsVehicles.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						txtNumberOfVehicles.Enabled = true;
					}
					cmdFilePrint.Enabled = true;
					cmdFileQueue.Enabled = true;
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			int fnx;
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngTemp;
			string strValues = "";
			string strFields = "";
			double TitleFee = 0;
			int intLockCount = 0;
			DialogResult intChoice = 0;
			int intRndCount = 0;
			int x;
			clsDRWrapper rsException = new clsDRWrapper();
			clsDRWrapper rsMVR3 = new clsDRWrapper();
			string strErrorTag = "";
			clsDRWrapper rsID = new clsDRWrapper();
			clsDRWrapper ArchiveRecord = new clsDRWrapper();
			clsDRWrapper rsTestFleetCTA = new clsDRWrapper();
			clsDRWrapper rsMVRecord = new clsDRWrapper();
			string strVin = "";
			clsDRWrapper rsUpdate = new clsDRWrapper();
			clsDRWrapper CheckRS = new clsDRWrapper();
			clsDRWrapper rsHeld = new clsDRWrapper();
			int counter;
			clsDRWrapper rsBatchInfo = new clsDRWrapper();
			clsDRWrapper rsTitle = new clsDRWrapper();
			clsDRWrapper rsInventory = new clsDRWrapper();
			string NP = "";
			string tempString = "";
			try
			{
				// On Error GoTo ShowLineNumber
				fecherFoundation.Information.Err().Clear();
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					rsBatchInfo.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
					rsBatchInfo.AddNew();
					for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
					{
						if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "ID")
						{
							// do nothing
						}
						else
						{
							rsBatchInfo.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
						}
					}
					rsBatchInfo.Set_Fields("Plate", vsVehicles.TextMatrix(1, intPlateCol));
					rsBatchInfo.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
					rsBatchInfo.Set_Fields("VIN", vsVehicles.TextMatrix(1, intVINCol));
					rsBatchInfo.Set_Fields("Year", vsVehicles.TextMatrix(1, intYearCol));
					rsBatchInfo.Set_Fields("Make", vsVehicles.TextMatrix(1, intMakeCol));
					rsBatchInfo.Set_Fields("Style", vsVehicles.TextMatrix(1, intStyleCol));
					rsBatchInfo.Set_Fields("UnitNumber", vsVehicles.TextMatrix(1, intUnitCol));
					rsBatchInfo.Set_Fields("TitleNumber", vsVehicles.TextMatrix(1, intCTACol).ToUpper());
					rsBatchInfo.Set_Fields("UserField1", vsVehicles.TextMatrix(1, intUserDefined1Col));
					rsBatchInfo.Set_Fields("UserField2", vsVehicles.TextMatrix(1, intUserDefined2Col));
					rsBatchInfo.Set_Fields("UserField3", vsVehicles.TextMatrix(1, intUserDefined3Col));
					this.Hide();
					frmSavePrint.InstancePtr.Hide();
					//App.DoEvents();
					for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
					{
						MotorVehicle.Statics.rsFinal.AddNew();
						for (fnx = 1; fnx <= rsBatchInfo.FieldsCount - 1; fnx++)
						{
							if (rsBatchInfo.Get_FieldsIndexName(fnx) == "ID")
							{
								// do nothing
							}
							else
							{
								MotorVehicle.Statics.rsFinal.Set_Fields(rsBatchInfo.Get_FieldsIndexName(fnx), rsBatchInfo.Get_FieldsIndexValue(fnx));
							}
						}
						MotorVehicle.Statics.rsFinal.Set_Fields("Plate", vsVehicles.TextMatrix(counter, intPlateCol));
						MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
						MotorVehicle.Statics.rsFinal.Set_Fields("VIN", vsVehicles.TextMatrix(counter, intVINCol));
						MotorVehicle.Statics.rsFinal.Set_Fields("Year", vsVehicles.TextMatrix(counter, intYearCol));
						MotorVehicle.Statics.rsFinal.Set_Fields("Make", vsVehicles.TextMatrix(counter, intMakeCol));
						MotorVehicle.Statics.rsFinal.Set_Fields("Style", vsVehicles.TextMatrix(counter, intStyleCol));
						MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber", vsVehicles.TextMatrix(counter, intUnitCol));
						MotorVehicle.Statics.rsFinal.Set_Fields("TitleNumber", vsVehicles.TextMatrix(counter, intCTACol).ToUpper());
						MotorVehicle.Statics.rsFinal.Set_Fields("UserField1", vsVehicles.TextMatrix(counter, intUserDefined1Col));
						MotorVehicle.Statics.rsFinal.Set_Fields("UserField2", vsVehicles.TextMatrix(counter, intUserDefined2Col));
						MotorVehicle.Statics.rsFinal.Set_Fields("UserField3", vsVehicles.TextMatrix(counter, intUserDefined3Col));
						if (frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.ListIndex) == 24)
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", true);
						}
						else
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", false);
						}
						rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = 0");
						rsAM.AddNew();
						rsAM.Set_Fields("RegistrationType", MotorVehicle.Statics.RegistrationType);
						rsAM.Set_Fields("DateUpdated", DateTime.Now);
						rsAM.Set_Fields("TitleDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"));
						rsAM.Set_Fields("UseTaxDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone"));
						if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("UseTaxDone")))
						{
							rsAM.Set_Fields("UseTaxID", MotorVehicle.Statics.lngUTCAddNewID);
						}
						rsAM.Set_Fields("Make", MotorVehicle.Statics.rsFinal.Get_Fields_String("Make"));
						rsAM.Set_Fields("Unit", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
						rsAM.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
						rsAM.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
						rsAM.Set_Fields("Color", MotorVehicle.Statics.rsFinal.Get_Fields_String("Color1") + MotorVehicle.Statics.rsFinal.Get_Fields_String("Color2"));
						rsAM.Set_Fields("MaineReregistration", (frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires this year" || frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires this year"));
						rsAM.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
						rsAM.Set_Fields("VIN", MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN"));
						rsAM.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
						rsAM.Set_Fields("NetWeight", MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
						rsAM.Set_Fields("OwnerName", MotorVehicle.Statics.rsFinal.Get_Fields("Owner1"));
						rsAM.Set_Fields("Address1", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
						rsAM.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
						rsAM.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
						rsAM.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
						rsAM.Set_Fields("FleetNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
						rsAM.Set_Fields("OldPlate", "");
						rsAM.Set_Fields("LegalResAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
						rsAM.Set_Fields("LegalResCity", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
						rsAM.Set_Fields("LegalResState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
						rsAM.Set_Fields("LegalResZip", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResZip"));
						rsAM.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
						rsAM.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
						rsAM.Set_Fields("NumberOfYears", frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.ListIndex));
						rsAM.Set_Fields("RegistrationFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERegFee.Text);
						rsAM.Set_Fields("RegistrationCredit", frmDataInputLongTermTrailers.InstancePtr.txtFEECreditTransfer.Text);
						rsAM.Set_Fields("TitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETitleFee.Text);
						rsAM.Set_Fields("RushTitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERushTitleFee.Text);
						rsAM.Set_Fields("SalesTax", frmDataInputLongTermTrailers.InstancePtr.txtFEESalesTax.Text);
						rsAM.Set_Fields("StateFee", frmDataInputLongTermTrailers.InstancePtr.txtFees.Text);
						rsAM.Set_Fields("AgentFee", frmDataInputLongTermTrailers.InstancePtr.txtFEEAgentFeeReg.Text);
						rsAM.Set_Fields("AgentFeeTitle", frmDataInputLongTermTrailers.InstancePtr.txtFeeAgentFeeCTA.Text);
						rsAM.Set_Fields("TransferFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETransferFee.Text);
						rsAM.Set_Fields("Status", "A");
						if (frmDataInputLongTermTrailers.InstancePtr.chkTimePayment.CheckState == Wisej.Web.CheckState.Checked)
						{
							rsAM.Set_Fields("TimePayment", true);
						}
						else
						{
							rsAM.Set_Fields("TimePayment", false);
						}
						rsAM.Set_Fields("TwentyFiveYearPlate", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwentyFiveYearPlate"));
						MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", rsAM.Get_Fields_DateTime("DateUpdated"));
						MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveID", 0);
						if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
						{
							MotorVehicle.Write_PDS_Work_Record_Stuff_MVRT10();
						}
						rsAM.Update();
						MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegID", rsAM.Get_Fields_Int32("ID"));
						if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") == true)
						{
							rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
							if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
							{
								rsTitle.MoveLast();
								rsTitle.MoveFirst();
								rsUpdate.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE ID = 0");
								if (MotorVehicle.Statics.FleetCTA)
								{
									if (MotorVehicle.Statics.VehiclesCovered > 1)
									{
										rsTestFleetCTA.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber") + "'");
										if (rsTestFleetCTA.EndOfFile() != true && rsTestFleetCTA.BeginningOfFile() != true)
										{
											goto SkipLTCTA;
										}
									}
									TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered;
								}
								else
								{
									TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee");
								}
								rsUpdate.AddNew();
								rsUpdate.Set_Fields("CTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper().ToUpper());
								rsUpdate.Set_Fields("Fee", TitleFee);
								rsUpdate.Set_Fields("RushFee", MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee"));
								rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
								rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
								rsUpdate.Set_Fields("RegistrationDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
								rsUpdate.Set_Fields("CustomerName", rsTitle.Get_Fields_String("Name1"));
								rsUpdate.Set_Fields("OPID", MotorVehicle.Statics.UserID);
								rsUpdate.Set_Fields("DoubleIfApplicable", rsTitle.Get_Fields_String("DoubleNumber"));
								rsUpdate.Set_Fields("PeriodCloseoutID", 0);
								rsUpdate.Set_Fields("MSRP", rsTitle.Get_Fields_String("MSRP"));
								rsUpdate.Update();
							}
							if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
							{
								MotorVehicle.Statics.rsDoubleTitle.Update();
							}
						}
						SkipLTCTA:
						;
						if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
						{
							// And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
							if (MotorVehicle.Statics.strCodeP == "")
							{
								MakeCodeP();
							}
							rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
							rsUpdate.AddNew();
							rsUpdate.Set_Fields("AdjustmentCode", "I");
							rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
							rsUpdate.Set_Fields("QuantityAdjusted", 1);
							rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
							rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
							rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
							rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
							rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "- MVR10 Form");
							rsUpdate.Set_Fields("PeriodCloseoutID", 0);
							rsUpdate.Set_Fields("TellerCloseoutID", 0);
							rsUpdate.Update();
						}
						if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
						{
							// And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
							MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory & '' = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
							rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
							if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
							{
								rsInventory.Edit();
								if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
								{
									rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
								}
								rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
								// .Fields("MVR3") =
								rsInventory.Set_Fields("Status", "I");
								rsInventory.Set_Fields("PeriodCloseoutID", 0);
								rsInventory.Set_Fields("TellerCloseoutID", 0);
								rsInventory.Update();
							}
						}
						MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
						MotorVehicle.Statics.rsFinal.Update();
					}
					if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
					{
						MotorVehicle.Write_Fleet_PDS_Work_Record_Stuff();
					}
					//App.DoEvents();
					MotorVehicle.Statics.rsFinalCompare.Reset();
					if (fecherFoundation.Information.Err().Number == 0)
					{
						MotorVehicle.Statics.MVR3saved = true;
					}
					else
					{
						MotorVehicle.Statics.MVR3saved = false;
					}
					MotorVehicle.ClearAllVariables();
					frmWait.InstancePtr.Unload();
					frmDataInput.InstancePtr.Unload();
					frmDataInputLongTermTrailers.InstancePtr.Unload();
					frmPreview.InstancePtr.Unload();
					if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
					{
						//frmPlateInfo.InstancePtr.Show(App.MainForm);
						frmPlateInfo.InstancePtr.ShowForm();
					}
					else if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
					{
						//App.DoEvents();
						//MDIParent.InstancePtr.GetMainMenu();
						MDIParent.InstancePtr.Menu18();
						Close();
						//Application.Exit();
						App.MainForm.EndWaitMVModule();
						return;
					}
					frmSavePrint.InstancePtr.Unload();
					Close();
					return;
				}
				if (MotorVehicle.Statics.blnChangeToFleet)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull"));
					MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
					MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditFull", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditUsed", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", 0);
				}
				rsBatchInfo.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
				rsBatchInfo.AddNew();
				for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
				{
					if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "ID")
					{
						// do nothing
					}
					else
					{
						rsBatchInfo.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
					}
				}
				rsBatchInfo.Set_Fields("Plate", vsVehicles.TextMatrix(1, intPlateCol));
				rsBatchInfo.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(rsBatchInfo.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
				rsBatchInfo.Set_Fields("VIN", vsVehicles.TextMatrix(1, intVINCol));
				rsBatchInfo.Set_Fields("Year", vsVehicles.TextMatrix(1, intYearCol));
				rsBatchInfo.Set_Fields("Make", vsVehicles.TextMatrix(1, intMakeCol));
				rsBatchInfo.Set_Fields("Model", vsVehicles.TextMatrix(1, intModelCol));
				rsBatchInfo.Set_Fields("Style", vsVehicles.TextMatrix(1, intStyleCol));
				rsBatchInfo.Set_Fields("Axles", vsVehicles.TextMatrix(1, intAxlesCol));
				rsBatchInfo.Set_Fields("MonthStickerNumber", vsVehicles.TextMatrix(1, intMonthStickerCol));
				rsBatchInfo.Set_Fields("YearStickerNumber", vsVehicles.TextMatrix(1, intYearStickerCol));
				rsBatchInfo.Set_Fields("UnitNumber", vsVehicles.TextMatrix(1, intUnitCol));
				rsBatchInfo.Set_Fields("CTANumber", vsVehicles.TextMatrix(1, intCTACol));
				rsBatchInfo.Set_Fields("PriorTitleNumber", vsVehicles.TextMatrix(1, intPriorTitleCol));
				rsBatchInfo.Set_Fields("PriorTitleState", vsVehicles.TextMatrix(1, intPriorStateCol));
				rsBatchInfo.Set_Fields("UserField1", vsVehicles.TextMatrix(1, intUserDefined1Col));
				rsBatchInfo.Set_Fields("UserField2", vsVehicles.TextMatrix(1, intUserDefined2Col));
				rsBatchInfo.Set_Fields("UserField3", vsVehicles.TextMatrix(1, intUserDefined3Col));
				rsBatchInfo.Set_Fields("MVR3", vsVehicles.TextMatrix(1, intMVR3Col));
				this.Hide();
				frmSavePrint.InstancePtr.Hide();
				//App.DoEvents();
				for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
				{
					strErrorTag = "Beginning";
					MotorVehicle.Statics.rsFinal.AddNew();
					for (fnx = 1; fnx <= rsBatchInfo.FieldsCount - 1; fnx++)
					{
						if (rsBatchInfo.Get_FieldsIndexName(fnx) == "ID")
						{
							// do nothing
						}
						else
						{
							MotorVehicle.Statics.rsFinal.Set_Fields(rsBatchInfo.Get_FieldsIndexName(fnx), rsBatchInfo.Get_FieldsIndexValue(fnx));
						}
					}
					MotorVehicle.Statics.rsFinal.Set_Fields("Plate", vsVehicles.TextMatrix(counter, intPlateCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
					MotorVehicle.Statics.rsFinal.Set_Fields("VIN", vsVehicles.TextMatrix(counter, intVINCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Year", vsVehicles.TextMatrix(counter, intYearCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Make", vsVehicles.TextMatrix(counter, intMakeCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Model", vsVehicles.TextMatrix(counter, intModelCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Style", vsVehicles.TextMatrix(counter, intStyleCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("Axles", vsVehicles.TextMatrix(counter, intAxlesCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNumber", vsVehicles.TextMatrix(counter, intMonthStickerCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNumber", vsVehicles.TextMatrix(counter, intYearStickerCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber", vsVehicles.TextMatrix(counter, intUnitCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("CTANumber", vsVehicles.TextMatrix(counter, intCTACol).ToUpper());
					MotorVehicle.Statics.rsFinal.Set_Fields("PriorTitleNumber", vsVehicles.TextMatrix(counter, intPriorTitleCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("PriorTitleState", vsVehicles.TextMatrix(counter, intPriorStateCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("UserField1", vsVehicles.TextMatrix(counter, intUserDefined1Col));
					MotorVehicle.Statics.rsFinal.Set_Fields("UserField2", vsVehicles.TextMatrix(counter, intUserDefined2Col));
					MotorVehicle.Statics.rsFinal.Set_Fields("UserField3", vsVehicles.TextMatrix(counter, intUserDefined3Col));
					MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", vsVehicles.TextMatrix(counter, intMVR3Col));
					MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", DateTime.Now);
					strErrorTag = "ActivityMaster";
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Saving Data";
					frmWait.InstancePtr.Refresh();
					rsUpdate.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
					rsUpdate.AddNew();
					for (fnx = 1; fnx <= rsBatchInfo.FieldsCount - 1; fnx++)
					{
						if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "OldMVR3")
						{
							rsUpdate.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), 0);
							/*? 71 */
						}
						else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "TellerCloseoutID")
						{
							rsUpdate.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), 0);
						}
						else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "ID")
						{
							// do nothing
							/*? 73 */
						}
						else
						{
							rsUpdate.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
							/*? 75 */
						}
						/*? 76 */
					}
					rsUpdate.Update();
					// MsgBox "Saved Activity Master record"
					//App.DoEvents();
					DoneFillingTag:
					;
					fecherFoundation.Information.Err().Clear();
					// On Error GoTo 0
					// 
					rsID.OpenRecordset("SELECT * FROM ActivityMaster WHERE MVR3 = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") + " AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "'");
					if (rsID.EndOfFile() != true && rsID.BeginningOfFile() != true)
					{
						MotorVehicle.Statics.TransactionID = FCConvert.ToString(rsID.Get_Fields_Int32("ID"));
						/*? 84 */
					}
					else
					{
						MotorVehicle.Statics.TransactionID = "";
						/*? 86 */
					}
					if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
					{
						MotorVehicle.Write_PDS_Work_Record_Stuff();
					}
					if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") == true)
					{
						rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
						if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
						{
							rsTitle.MoveLast();
							rsTitle.MoveFirst();
							rsUpdate.OpenRecordset("SELECT * FROM TitleApplications WHERE ID = 0");
							if (MotorVehicle.Statics.FleetCTA)
							{
								if (MotorVehicle.Statics.VehiclesCovered > 1)
								{
									rsTestFleetCTA.OpenRecordset("SELECT * FROM TitleApplications WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber") + "'");
									if (rsTestFleetCTA.EndOfFile() != true && rsTestFleetCTA.BeginningOfFile() != true)
									{
										/*? 105 */
										goto SkipCTA;
										/*? 106 */
									}
									/*? 107 */
								}
								TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered;
								/*? 109 */
							}
							else
							{
								TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee");
								/*? 111 */
							}
							rsUpdate.AddNew();
							rsUpdate.Set_Fields("CTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber").ToUpper());
							rsUpdate.Set_Fields("Fee", TitleFee);
							rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
							rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
							rsUpdate.Set_Fields("RegistrationDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							rsUpdate.Set_Fields("CustomerName", rsTitle.Get_Fields_String("Name1"));
							rsUpdate.Set_Fields("OPID", MotorVehicle.Statics.UserID);
							rsUpdate.Set_Fields("DoubleIfApplicable", rsTitle.Get_Fields_String("DoubleNumber"));
							rsUpdate.Set_Fields("PeriodCloseoutID", 0);
							rsUpdate.Set_Fields("MSRP", rsTitle.Get_Fields_String("MSRP"));
							rsUpdate.Update();
							/*? 125 */
						}
						if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
						{
							MotorVehicle.Statics.rsDoubleTitle.Update();
							/*? 128 */
						}
						/*? 129 */
					}
					// MsgBox "Saved Title Information"
					//App.DoEvents();
					// 
					SkipCTA:
					;
					// DJW 09/13/08 Added for new state legislation
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"))) != "" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"))) != fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("GiftCertificateNumber"))))
					{
						rsUpdate.OpenRecordset("SELECT * FROM GiftCertificateRegister WHERE ID = 0");
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("DateRedeemed", DateTime.Today);
						rsUpdate.Set_Fields("CertificateNumber", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"))));
						rsUpdate.Set_Fields("Name", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"))));
						rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
						rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
						rsUpdate.Set_Fields("Amount", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GiftCertificateAmount"));
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.OpID);
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Set_Fields("GiftCertOrVoucher", MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher"));
						rsUpdate.Set_Fields("RegistrationID", MotorVehicle.Statics.TransactionID);
						rsUpdate.Update();
					}
					if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO") == true)
					{
						rsUpdate.OpenRecordset("SELECT * FROM ExciseTax WHERE ID = 0");
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("MVR3Number", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
						rsUpdate.Set_Fields("DateofTransaction", DateTime.Today);
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Update();
						// MsgBox "Saved ETO Information"
						//App.DoEvents();
					}
					// ****   INVENTORY ADJUSTMENT STARTS HERE ****
					rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
					if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber")) != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
					{
						if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
						{
							MakeCode("M");
							/*? 147 */
						}
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("AdjustmentCode", "I");
						rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
						rsUpdate.Set_Fields("QuantityAdjusted", 1);
						rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeM);
						rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
						rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Set_Fields("TellerCloseoutID", 0);
						rsUpdate.Update();
						/*? 166 */
					}
					if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != 0)
					{
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("AdjustmentCode", "I");
						rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
						rsUpdate.Set_Fields("QuantityAdjusted", 1);
						rsUpdate.Set_Fields("InventoryType", "MXS00");
						rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Set_Fields("TellerCloseoutID", 0);
						rsUpdate.Update();
					}
					if ((FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber")) != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
					{
						if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
						{
							if (MotorVehicle.Statics.Class == "IU")
							{
								MotorVehicle.Statics.strCodeY = "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
								/*? 250 */
							}
							else
							{
								MakeCode("Y");
								/*? 252 */
							}
							/*? 253 */
						}
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("AdjustmentCode", "I");
						rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
						rsUpdate.Set_Fields("QuantityAdjusted", 1);
						rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeY);
						if (MotorVehicle.Statics.Class == "IU")
						{
							rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
							rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
							/*? 267 */
						}
						else
						{
							rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
							rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
							/*? 270 */
						}
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Set_Fields("TellerCloseoutID", 0);
						rsUpdate.Update();
						/*? 277 */
					}
					if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
					{
						// And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
						if (MotorVehicle.Statics.strCodeP == "")
						{
							MakeCodeP();
							/*? 281 */
						}
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("AdjustmentCode", "I");
						rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
						rsUpdate.Set_Fields("QuantityAdjusted", 1);
						rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
						rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
						rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Set_Fields("TellerCloseoutID", 0);
						rsUpdate.Update();
						/*? 300 */
					}
					// MsgBox "Saved Inventory Adjustments Info"
					//App.DoEvents();
					// ****   INVENTORY ADJUSTMENT ENDS HERE ****
					// 
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "T")
					{
						string st1 = "";
						rsUpdate.OpenRecordset("SELECT * FROM TemporaryPlateRegister WHERE ID = 0");
						st1 = MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), true);
						rsUpdate.AddNew();
						rsUpdate.Set_Fields("TemporaryPlateNumber", MotorVehicle.Statics.TempPlateNumber);
						rsUpdate.Set_Fields("OwnerCode1", MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1"));
						rsUpdate.Set_Fields("Owner1", st1);
						rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
						rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
						rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsUpdate.Set_Fields("PeriodCloseoutID", 0);
						rsUpdate.Update();
						// MsgBox "Saved Temporary Plate Info"
						//App.DoEvents();
						/*? 314 */
					}
					// MsgBox "2"
					// **** START OF INVENTORY UPDATE ****
					if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber")) != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
					{
						if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
						{
							MakeCode("M");
							/*? 342 */
						}
						rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeM + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
						if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
						{
							rsInventory.Edit();
							if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
							{
								rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							}
							rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
							rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
							rsInventory.Set_Fields("Status", "I");
							rsInventory.Set_Fields("PeriodCloseoutID", 0);
							rsInventory.Set_Fields("TellerCloseoutID", 0);
							rsInventory.Update();
							/*? 358 *//*? 359 */
						}
						/*? 360 */
					}
					// 
					strErrorTag = "MVR3 Inventory";
					if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != 0)
					{
						rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
						if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
						{
							rsInventory.Edit();
							if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
							{
								rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							}
							rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
							rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
							rsInventory.Set_Fields("Status", "I");
							rsInventory.Set_Fields("PeriodCloseoutID", 0);
							rsInventory.Set_Fields("TellerCloseoutID", 0);
							rsInventory.Update();
							/*? 402 *//*? 403 */
						}
						/*? 404 */
					}
					// 
					strErrorTag = "Year Inventory";
					if ((FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber")) != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
					{
						if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
						{
							if (MotorVehicle.Statics.Class == "IU")
							{
								MotorVehicle.Statics.strCodeY = "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
							}
							else
							{
								MakeCode("Y");
							}
						}
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "IU")
						{
							rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
						}
						else
						{
							rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
						}
						if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
						{
							rsInventory.Edit();
							if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
							{
								rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							}
							rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
							rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
							rsInventory.Set_Fields("Status", "I");
							rsInventory.Set_Fields("PeriodCloseoutID", 0);
							rsInventory.Set_Fields("TellerCloseoutID", 0);
							rsInventory.Update();
							/*? 503 *//*? 504 */
						}
						/*? 505 */
					}
					// 
					strErrorTag = "Plate Inventory";
					if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU")
					{
						// And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
						MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory & '' = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
						rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
						if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
						{
							rsInventory.Edit();
							if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
							{
								rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							}
							rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
							rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
							rsInventory.Set_Fields("Status", "I");
							rsInventory.Set_Fields("PeriodCloseoutID", 0);
							rsInventory.Set_Fields("TellerCloseoutID", 0);
							rsInventory.Update();
							/*? 559 *//*? 560 */
						}
						/*? 561 */
					}
					// 
					strErrorTag = "Temporary Plate Inventory";
					if (MotorVehicle.Statics.ForcedPlate == "T")
					{
						rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE SUBSTRING(Code,4,5) = 'TM' AND Number = " + FCConvert.ToString(MotorVehicle.Statics.TempPlateNumber));
						if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
						{
							rsInventory.Edit();
							if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
							{
								rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
							}
							rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
							rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
							rsInventory.Set_Fields("Status", "I");
							rsInventory.Set_Fields("PeriodCloseoutID", 0);
							rsInventory.Set_Fields("TellerCloseoutID", 0);
							rsInventory.Update();
							/*? 580 *//*? 581 */
						}
						/*? 582 */
					}
					else
					{
						// no record
						/*? 583 */
					}
					// **** END OF INVENTORY UPDATE ****
					MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
					MotorVehicle.Statics.rsFinal.Update();
					//App.DoEvents();
				}
				if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
				{
					MotorVehicle.Write_Fleet_PDS_Work_Record_Stuff();
				}
				modRegistry.SaveRegistryKey("LastMVR3Used", FCConvert.ToString(LastMVR3));
				int tempPrinter = StaticSettings.GlobalCommandDispatcher.Send(new GetPrinterGroup()).Result;

				rsMVR3.OpenRecordset("SELECT * FROM LastMVRNumber WHERE PrinterNumber = " + FCConvert.ToString(tempPrinter));
				if (rsMVR3.EndOfFile() != true && rsMVR3.BeginningOfFile() != true)
				{
					rsMVR3.Edit();
					rsMVR3.Set_Fields("LastMVRNumber", LastMVR3);
				}
				else
				{
					rsMVR3.AddNew();
					rsMVR3.Set_Fields("PrinterNumber", tempPrinter);
					rsMVR3.Set_Fields("LastMVRNumber", LastMVR3);
				}
				rsMVR3.Update();
				MotorVehicle.Statics.rsFinalCompare.Reset();
				if (fecherFoundation.Information.Err().Number == 0)
				{
					MotorVehicle.Statics.MVR3saved = true;
					/*? 689 */
				}
				else
				{
					MotorVehicle.Statics.MVR3saved = false;
					/*? 691 */
				}
				MotorVehicle.ClearAllVariables();
				frmWait.InstancePtr.Unload();
				frmDataInput.InstancePtr.Unload();
				frmPreview.InstancePtr.Unload();
				if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
				{
					//frmPlateInfo.InstancePtr.Show(App.MainForm);
					frmPlateInfo.InstancePtr.ShowForm();
					/*? 698 */
				}
				else if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
				{
					//App.DoEvents();
					//MDIParent.InstancePtr.GetMainMenu();
					MDIParent.InstancePtr.Menu18();
					Close();
					/*? 703 */
					//Application.Exit();
					App.MainForm.EndWaitMVModule();
					/*? 704 */
					return;
					/*? 705 */
				}
				else if (!MotorVehicle.Statics.RegisteringFleet)
				{
					//MDIParent.InstancePtr.GRID.Focus();
					/*? 707 */
				}
				frmSavePrint.InstancePtr.Unload();
				Close();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "An error occurred on line " + fecherFoundation.Information.Erl(), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume FailedUpdate; */
				SaveError:
				;
				switch (fecherFoundation.Information.Err(ex).Number)
				{
					case 3260:
						{
							intLockCount += 1;
							if (intLockCount > 5)
							{
								intChoice = MessageBox.Show("Error Trying to Save.  This record is currently locked by another user.  Would you like to try to save again?", "Record Locked", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);
								if (intChoice == DialogResult.Retry)
								{
									intLockCount = 1;
								}
								else
								{
									/*? Resume FailedUpdate; */
								}
							}
							//App.DoEvents();
							intRndCount = (FCConvert.ToInt32(Math.Pow(intLockCount, 2)) * 4000);
							for (x = 1; x <= intRndCount; x++)
							{
							}
							// x
							/*? Resume; */
							break;
						}
					default:
						{
							MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "A file called SaveErr.txt has been created in the " + Environment.CurrentDirectory + " directory of your server.  Please notify TRIO of your problem and E-mail the SaveErr.txt file so we may more quickly find the problem you are encountering.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							/*? Resume FailedUpdate; */
							break;
						}
				}
				//end switch
				FailedUpdate:
				;
				MotorVehicle.ClearAllVariables();
				frmDataInput.InstancePtr.Unload();
				frmPreview.InstancePtr.Unload();
				frmSavePrint.InstancePtr.Unload();
				Close();
				MotorVehicle.Statics.rsFinal = null;
				MotorVehicle.Statics.rsFinalCompare = null;
				if (!MotorVehicle.Statics.bolFromDosTrio && !MotorVehicle.Statics.bolFromWindowsCR)
				{
					//frmPlateInfo.InstancePtr.Show(App.MainForm);
					frmPlateInfo.InstancePtr.ShowForm();
				}
				else
				{
					//MDIParent.InstancePtr.GRID.Focus();
				}
			}
		}

		private void txtHigh_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLow_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNumberOfVehicles_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNumberOfVehicles_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: intAnswer As int	OnWrite(DialogResult)
			DialogResult intAnswer = 0;
			if (Conversion.Val(txtNumberOfVehicles.Text) < 1)
			{
				MessageBox.Show("You must at least have 1 vehicle to register before you may proceed", "Invalid Number of Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
				return;
			}
			if (vsVehicles.Rows - 1 != Conversion.Val(txtNumberOfVehicles.Text))
			{
				if (Conversion.Val(txtNumberOfVehicles.Text) > vsVehicles.Rows - 1)
				{
					LoadGrid();
				}
				else
				{
					intAnswer = MessageBox.Show("You will lose any data you entered for the vehicles you are removing.  Are you sure you wish to continue?", "Remove Vehicles?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intAnswer == DialogResult.Yes)
					{
						vsVehicles.Rows = FCConvert.ToInt32(Conversion.Val(txtNumberOfVehicles.Text) + 1);
					}
					else
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void LoadGrid()
		{
			// vbPorter upgrade warning: counter As int	OnWrite(int, double)
			int counter;
			string strTemp = "";
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
			{
				for (counter = (vsVehicles.Rows); counter <= FCConvert.ToInt16(Conversion.Val(txtNumberOfVehicles.Text)); counter++)
				{
					vsVehicles.Rows += 1;
					if (counter == 1)
					{
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intVINCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPlateCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUnitCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined1Col, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined2Col, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField2")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined3Col, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField3")));
					}
					else
					{
						// strTemp = rsFinal.Fields("Vin")
						// If Len(strTemp) = 17 Then
						// strTemp = Left(strTemp, 8) & "_" & Mid(strTemp, 10, 6) & "_"
						// vsVehicles.TextMatrix(vsVehicles.Rows - 1, intVINCol) = strTemp
						// End If
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intVINCol, GetNextVIN(vsVehicles.TextMatrix(vsVehicles.Rows - 2, intVINCol)));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPlateCol, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUnitCol, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined1Col, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined2Col, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined3Col, "");
					}
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intNumberCol, FCConvert.ToString(counter));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPrintedCol, FCConvert.ToString(false));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intYearCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intMakeCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intStyleCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intCTACol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber")));
				}
				AutoFillColumns();
			}
			else
			{
				for (counter = (vsVehicles.Rows); counter <= FCConvert.ToInt16(Conversion.Val(txtNumberOfVehicles.Text)); counter++)
				{
					vsVehicles.Rows += 1;
					if (counter == 1)
					{
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intVINCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPlateCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUnitCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intMonthStickerCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intYearStickerCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intMVR3Col, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined1Col, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined2Col, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField2")));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined3Col, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField3")));
					}
					else
					{
						// strTemp = rsFinal.Fields("Vin")
						// If Len(strTemp) = 17 Then
						// strTemp = Left(strTemp, 8) & "_" & Mid(strTemp, 10, 6) & "_"
						// vsVehicles.TextMatrix(vsVehicles.Rows - 1, intVINCol) = strTemp
						// End If
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intVINCol, GetNextVIN(vsVehicles.TextMatrix(vsVehicles.Rows - 2, intVINCol)));
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPlateCol, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUnitCol, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intMonthStickerCol, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intYearStickerCol, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intMVR3Col, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined1Col, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined2Col, "");
						vsVehicles.TextMatrix(vsVehicles.Rows - 1, intUserDefined3Col, "");
					}
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intNumberCol, FCConvert.ToString(counter));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPrintedCol, FCConvert.ToString(false));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intYearCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intMakeCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intModelCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("model")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intStyleCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intAxlesCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("Axles")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intCTACol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPriorTitleCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleNumber")));
					vsVehicles.TextMatrix(vsVehicles.Rows - 1, intPriorStateCol, FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PriorTitleState")));
				}
				AutoFillColumns();
			}
		}

		private string GetNextVIN(string strVin)
		{
			string GetNextVIN = "";
			// vbPorter upgrade warning: lngSeq As int	OnWrite(string, int)
			int lngSeq = 0;
			// vbPorter upgrade warning: strTest As string	OnReadFCConvert.ToInt32(
			string strTest = "";
			string strNewSeq = "";
			string strCheckDigit = "";
			int counter;
			for (counter = 6; counter >= 1; counter--)
			{
				strTest = Strings.Right(strVin, counter);
				if (Information.IsNumeric(strTest))
					break;
			}
			if (Information.IsNumeric(strTest) && Information.IsNumeric(Strings.Left(strTest, 1)) && Information.IsNumeric(Strings.Mid(strTest, 2, 1)) && strTest != Strings.StrDup(strTest.Length, "9"))
			{
				lngSeq = FCConvert.ToInt32(strTest);
				lngSeq += 1;
				strNewSeq = Strings.Format(lngSeq, Strings.StrDup(counter, "0"));
				strCheckDigit = CalculateVINCheckDigit(Strings.Left(strVin, 8) + "_" + Strings.Mid(strVin, 10, 2 + (6 - counter)) + strNewSeq);
				GetNextVIN = Strings.Left(strVin, 8) + strCheckDigit + Strings.Mid(strVin, 10, 2 + (6 - counter)) + strNewSeq;
			}
			else
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")).Length == 17)
				{
					GetNextVIN = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")), 8) + "_" + Strings.Mid(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")), 10, 6) + "_";
				}
			}
			return GetNextVIN;
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(int, string)
		private string CalculateVINCheckDigit(string strVin)
		{
			string CalculateVINCheckDigit = "";
			int counter;
			string strTranslatedVIN = "";
			int[] intWeight = new int[17 + 1];
			int lngSum;
			// vbPorter upgrade warning: intVal As int	OnWrite(string, int)
			int intVal = 0;
			intWeight[1] = 8;
			intWeight[2] = 7;
			intWeight[3] = 6;
			intWeight[4] = 5;
			intWeight[5] = 4;
			intWeight[6] = 3;
			intWeight[7] = 2;
			intWeight[8] = 10;
			intWeight[9] = 0;
			intWeight[10] = 9;
			intWeight[11] = 8;
			intWeight[12] = 7;
			intWeight[13] = 6;
			intWeight[14] = 5;
			intWeight[15] = 4;
			intWeight[16] = 3;
			intWeight[17] = 2;
			for (counter = 1; counter <= 17; counter++)
			{
				if ((Strings.Mid(strVin, counter, 1) == "1") || (Strings.Mid(strVin, counter, 1) == "2") || (Strings.Mid(strVin, counter, 1) == "3") || (Strings.Mid(strVin, counter, 1) == "4") || (Strings.Mid(strVin, counter, 1) == "5") || (Strings.Mid(strVin, counter, 1) == "6") || (Strings.Mid(strVin, counter, 1) == "7") || (Strings.Mid(strVin, counter, 1) == "8") || (Strings.Mid(strVin, counter, 1) == "9") || (Strings.Mid(strVin, counter, 1) == "0"))
				{
					strTranslatedVIN += Strings.Mid(strVin, counter, 1);
				}
				else if ((Strings.Mid(strVin, counter, 1) == "A") || (Strings.Mid(strVin, counter, 1) == "J"))
				{
					strTranslatedVIN += "1";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "B") || (Strings.Mid(strVin, counter, 1) == "K") || (Strings.Mid(strVin, counter, 1) == "S"))
				{
					strTranslatedVIN += "2";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "C") || (Strings.Mid(strVin, counter, 1) == "L") || (Strings.Mid(strVin, counter, 1) == "T"))
				{
					strTranslatedVIN += "3";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "D") || (Strings.Mid(strVin, counter, 1) == "M") || (Strings.Mid(strVin, counter, 1) == "U"))
				{
					strTranslatedVIN += "4";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "E") || (Strings.Mid(strVin, counter, 1) == "N") || (Strings.Mid(strVin, counter, 1) == "V"))
				{
					strTranslatedVIN += "5";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "F") || (Strings.Mid(strVin, counter, 1) == "W"))
				{
					strTranslatedVIN += "6";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "G") || (Strings.Mid(strVin, counter, 1) == "P") || (Strings.Mid(strVin, counter, 1) == "X"))
				{
					strTranslatedVIN += "7";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "H") || (Strings.Mid(strVin, counter, 1) == "Y"))
				{
					strTranslatedVIN += "8";
				}
				else if ((Strings.Mid(strVin, counter, 1) == "R") || (Strings.Mid(strVin, counter, 1) == "Z"))
				{
					strTranslatedVIN += "9";
				}
				else
				{
					strTranslatedVIN += "0";
				}
			}
			lngSum = 0;
			for (counter = 1; counter <= 17; counter++)
			{
				intVal = FCConvert.ToInt32(Strings.Mid(strTranslatedVIN, counter, 1));
				intVal *= intWeight[counter];
				lngSum += intVal;
			}
			CalculateVINCheckDigit = FCConvert.ToString(lngSum % 11);
			if (CalculateVINCheckDigit == "10")
				CalculateVINCheckDigit = "X";
			return CalculateVINCheckDigit;
		}

		private void AutoFillColumns()
		{
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
			{
				AutoFillPlate();
				if (MotorVehicle.Statics.blnLongTermAutoPopulateUnit)
				{
					AutoFillUnit();
				}
			}
			else
			{
				AutoFillMVR3();
				AutoFillMonthSticker();
				AutoFillYearSticker();
				AutoFillPlate();
			}
		}

		private void AutoFillUnit(int intRow = 1)
		{
			string strprefix = "";
			int lngUnit = 0;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(intRow, intUnitCol)) != "")
			{
				strprefix = "";
				lngUnit = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(intRow, intUnitCol)))));
				for (counter = (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(intRow, intUnitCol)).Length); counter >= 1; counter--)
				{
					if (!Information.IsNumeric(Strings.Mid(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(intRow, intUnitCol)), counter, 1)))
					{
						strprefix = Strings.Left(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(intRow, intUnitCol)), counter);
						lngUnit = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(intRow, intUnitCol)), fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(intRow, intUnitCol)).Length - counter))));
						break;
					}
				}
			}
			else
			{
				return;
			}
			for (counter = intRow + 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				vsVehicles.TextMatrix(counter, intUnitCol, strprefix + FCConvert.ToString(lngUnit + 1));
				lngUnit += 1;
			}
		}

		private void AutoFillMVR3(int intRow = 1)
		{
			int lngMVR3 = 0;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int intMVR3Count = 0;
			int lngHighMVR3 = 0;
			// vbPorter upgrade warning: counter2 As int	OnWriteFCConvert.ToInt32(
			int counter2;
			int intFilledToRow;
			intFilledToRow = 0;
			for (counter = intRow; counter <= (vsVehicles.Rows - 1); counter++)
			{
				if (intFilledToRow < counter)
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intMVR3Col)) != "")
					{
						lngMVR3 = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intMVR3Col)))));
						if (VerifyMVR3Number(lngMVR3, intMVR3Count))
						{
							lngHighMVR3 = lngMVR3 + intMVR3Count;
							for (counter2 = 1; counter2 <= (vsVehicles.Rows - 1); counter2++)
							{
								if (counter2 != counter)
								{
									if (Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intMVR3Col))) >= lngMVR3 && Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intMVR3Col))) <= lngHighMVR3)
									{
										vsVehicles.TextMatrix(counter2, intMVR3Col, "");
									}
								}
							}
							if (intMVR3Count > 0)
							{
								for (counter2 = 1; counter2 <= intMVR3Count; counter2++)
								{
									if (vsVehicles.Rows > counter + counter2)
									{
										vsVehicles.TextMatrix(counter + counter2, intMVR3Col, FCConvert.ToString(lngMVR3 + counter2));
										intFilledToRow = counter + counter2;
									}
									else
									{
										break;
									}
								}
							}
							else
							{
								intFilledToRow = counter;
							}
						}
						else
						{
							vsVehicles.TextMatrix(counter, intMVR3Col, "");
							intFilledToRow = counter;
						}
					}
				}
			}
		}

		private void AutoFillMonthSticker(int intRow = 1)
		{
			int lngMonthSticker = 0;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int intMonthStickerCount = 0;
			int lngHighMonthSticker = 0;
			// vbPorter upgrade warning: counter2 As int	OnWriteFCConvert.ToInt32(
			int counter2;
			int intFilledToRow;
			intFilledToRow = 0;
			for (counter = intRow; counter <= (vsVehicles.Rows - 1); counter++)
			{
				if (intFilledToRow < counter)
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intMonthStickerCol)) != "")
					{
						lngMonthSticker = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intMonthStickerCol)))));
						if (VerifyMonthStickerNumber(lngMonthSticker, intMonthStickerCount))
						{
							lngHighMonthSticker = lngMonthSticker + intMonthStickerCount;
							for (counter2 = 1; counter2 <= (vsVehicles.Rows - 1); counter2++)
							{
								if (counter2 != counter)
								{
									if (Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intMonthStickerCol))) >= lngMonthSticker && Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intMonthStickerCol))) <= lngHighMonthSticker)
									{
										vsVehicles.TextMatrix(counter2, intMonthStickerCol, "");
									}
								}
							}
							if (intMonthStickerCount > 0)
							{
								for (counter2 = 1; counter2 <= intMonthStickerCount; counter2++)
								{
									if (vsVehicles.Rows > counter + counter2)
									{
										vsVehicles.TextMatrix(counter + counter2, intMonthStickerCol, FCConvert.ToString(lngMonthSticker + counter2));
										intFilledToRow = counter + counter2;
									}
									else
									{
										break;
									}
								}
							}
							else
							{
								intFilledToRow = counter;
							}
						}
						else
						{
							vsVehicles.TextMatrix(counter, intMonthStickerCol, "");
							intFilledToRow = counter;
						}
					}
				}
			}
		}

		private void AutoFillYearSticker(int intRow = 1)
		{
			int lngYearSticker = 0;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int intYearStickerCount = 0;
			int lngHighYearSticker = 0;
			// vbPorter upgrade warning: counter2 As int	OnWriteFCConvert.ToInt32(
			int counter2;
			int intFilledToRow;
			intFilledToRow = 0;
			for (counter = intRow; counter <= (vsVehicles.Rows - 1); counter++)
			{
				if (intFilledToRow < counter)
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intYearStickerCol)) != "")
					{
						lngYearSticker = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intYearStickerCol)))));
						if (VerifyYearStickerNumber(lngYearSticker, intYearStickerCount))
						{
							lngHighYearSticker = lngYearSticker + intYearStickerCount;
							for (counter2 = 1; counter2 <= (vsVehicles.Rows - 1); counter2++)
							{
								if (counter2 != counter)
								{
									if (Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intYearStickerCol))) >= lngYearSticker && Conversion.Val(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intYearStickerCol))) <= lngHighYearSticker)
									{
										vsVehicles.TextMatrix(counter2, intYearStickerCol, "");
									}
								}
							}
							if (intYearStickerCount > 0)
							{
								for (counter2 = 1; counter2 <= intYearStickerCount; counter2++)
								{
									if (vsVehicles.Rows > counter + counter2)
									{
										vsVehicles.TextMatrix(counter + counter2, intYearStickerCol, FCConvert.ToString(lngYearSticker + counter2));
										intFilledToRow = counter + counter2;
									}
									else
									{
										break;
									}
								}
							}
							else
							{
								intFilledToRow = counter;
							}
						}
						else
						{
							vsVehicles.TextMatrix(counter, intYearStickerCol, "");
							intFilledToRow = counter;
						}
					}
				}
			}
		}

		private void AutoFillPlate(int intRow = 1)
		{
			int lngPlate = 0;
			string strSuff = "";
			string strPre = "";
			string strPlate = "";
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int intPlateCount = 0;
			int lngHighPlate = 0;
			// vbPorter upgrade warning: counter2 As int	OnWriteFCConvert.ToInt32(
			int counter2;
			int intFilledToRow;
			string High = "";
			string Low = "";
			string strPlateCheck = "";
			string strPlateType = "";
			bool blnShowMessage;
			intFilledToRow = 0;
			blnShowMessage = true;
			for (counter = intRow; counter <= (vsVehicles.Rows - 1); counter++)
			{
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry && (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION"))
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intPlateCol)) != "")
					{
						intFilledToRow = 2000 + FCConvert.ToInt32(Strings.Left(fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intPlateCol)), 2));
					}
					else
					{
						vsVehicles.TextMatrix(counter, intPlateCol, MotorVehicle.GetLongTermPlate(intFilledToRow, blnShowMessage));
						if (vsVehicles.TextMatrix(counter, intPlateCol) == "")
						{
							blnShowMessage = false;
						}
					}
				}
				else
				{
					if (intFilledToRow < counter)
					{
						if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intPlateCol)) != "")
						{
							strPlate = fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter, intPlateCol));
							if (VerifyPlateNumber(strPlate, intPlateCount, strPre, strSuff, lngPlate, strPlateType))
							{
								lngHighPlate = lngPlate + intPlateCount;
								for (counter2 = 1; counter2 <= (vsVehicles.Rows - 1); counter2++)
								{
									if (counter2 != counter)
									{
										if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intPlateCol)) != "")
										{
											High = fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intPlateCol));
											Low = fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(counter2, intPlateCol));
											High += Strings.StrDup(8 - fecherFoundation.Strings.Trim(High).Length, " ");
											Low += Strings.StrDup(8 - fecherFoundation.Strings.Trim(Low).Length, " ");
											strPlateCheck = Low + High;
											MotorVehicle.GetPlateRange(strPlateCheck);
											if (MotorVehicle.Statics.Prefix1 == strPre && MotorVehicle.Statics.Suffix1 == strSuff && (FCConvert.ToDouble(MotorVehicle.Statics.Numeric1) >= lngPlate && FCConvert.ToDouble(MotorVehicle.Statics.Numeric1) <= lngHighPlate))
											{
												vsVehicles.TextMatrix(counter2, intPlateCol, "");
											}
										}
									}
								}
								if (intPlateCount > 0)
								{
									for (counter2 = 1; counter2 <= intPlateCount; counter2++)
									{
										if (vsVehicles.Rows > counter + counter2)
										{
											vsVehicles.TextMatrix(counter + counter2, intPlateCol, MotorVehicle.CreateFormattedInventory(strPre, fecherFoundation.Strings.Trim((lngPlate + counter2).ToString()), strSuff, strPlateType));
											intFilledToRow = counter + counter2;
										}
										else
										{
											break;
										}
									}
								}
								else
								{
									intFilledToRow = counter;
								}
							}
							else
							{
								vsVehicles.TextMatrix(counter, intPlateCol, "");
								intFilledToRow = counter;
							}
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: lngMVR3 As int	OnWrite(int, string)
		private bool VerifyMVR3Number(int lngMVR3, int intCount)
		{
			bool VerifyMVR3Number = false;
			clsDRWrapper rsMVR3 = new clsDRWrapper();
			if (MotorVehicle.Statics.MVR3Group == 0)
			{
				rsMVR3.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + "MXS00" + "'  AND Status = 'A' AND Number >= " + FCConvert.ToString(lngMVR3) + " ORDER BY Number");
			}
			else
			{
				rsMVR3.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + "MXS00" + "'  AND Status = 'A' AND Number >= " + FCConvert.ToString(lngMVR3) + " AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.MVR3Group) + " OR [Group] = 0) ORDER BY Number");
			}
			if (rsMVR3.EndOfFile() != true && rsMVR3.BeginningOfFile() != true)
			{
				VerifyMVR3Number = true;
				intCount = 0;
				while (lngMVR3 + intCount == FCConvert.ToInt32(rsMVR3.Get_Fields_Int32("Number")))
				{
					intCount += 1;
					rsMVR3.MoveNext();
					if (rsMVR3.EndOfFile())
					{
						break;
					}
				}
				if (intCount > 0)
				{
					intCount -= 1;
				}
			}
			else
			{
				VerifyMVR3Number = false;
				intCount = -1;
			}
			return VerifyMVR3Number;
		}
		// vbPorter upgrade warning: lngMonthSticker As int	OnWrite(int, string)
		private bool VerifyMonthStickerNumber(int lngMonthSticker, int intCount)
		{
			bool VerifyMonthStickerNumber = false;
			clsDRWrapper rsMStickers = new clsDRWrapper();
			string QtyM = "";
			string strCodeM;
			VerifyMonthStickerNumber = false;
			if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge")) == 1)
			{
				QtyM = "S";
			}
			else
			{
				QtyM = "D";
			}
			strCodeM = "SM" + QtyM + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Month, "00");
			if (MotorVehicle.Statics.StickerGroup == 0)
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE code = '" + strCodeM + "' AND Number >= " + FCConvert.ToString(lngMonthSticker) + " AND Status = 'A'";
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE code = '" + strCodeM + "' AND Number >= " + FCConvert.ToString(lngMonthSticker) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.StickerGroup) + " OR [Group] = 0)";
			}
			rsMStickers.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsMStickers.EndOfFile() != true && rsMStickers.BeginningOfFile() != true)
			{
				VerifyMonthStickerNumber = true;
				intCount = 0;
				while (lngMonthSticker + intCount == FCConvert.ToInt32(rsMStickers.Get_Fields_Int32("Number")))
				{
					intCount += 1;
					rsMStickers.MoveNext();
					if (rsMStickers.EndOfFile())
					{
						break;
					}
				}
				if (intCount > 0)
				{
					intCount -= 1;
				}
			}
			else
			{
				VerifyMonthStickerNumber = false;
				intCount = -1;
			}
			return VerifyMonthStickerNumber;
		}
		// vbPorter upgrade warning: lngYearSticker As int	OnWrite(int, string)
		private bool VerifyYearStickerNumber(int lngYearSticker, int intCount)
		{
			bool VerifyYearStickerNumber = false;
			clsDRWrapper rsStickers = new clsDRWrapper();
			string Qty = "";
			string strCode;
			VerifyYearStickerNumber = false;
			if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) + Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge")) == 1)
			{
				Qty = "S";
			}
			else
			{
				Qty = "D";
			}
			strCode = "SY" + Qty + Strings.Right(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "MM/dd/yy"), 2);
			if (MotorVehicle.Statics.StickerGroup == 0)
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + strCode + "' AND Number >= " + FCConvert.ToString(lngYearSticker) + " AND Status = 'A'";
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + strCode + "' AND Number >= " + FCConvert.ToString(lngYearSticker) + " AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.StickerGroup) + " OR [Group] = 0)";
			}
			rsStickers.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsStickers.EndOfFile() != true && rsStickers.BeginningOfFile() != true)
			{
				VerifyYearStickerNumber = true;
				intCount = 0;
				while (lngYearSticker + intCount == FCConvert.ToInt32(rsStickers.Get_Fields_Int32("Number")))
				{
					intCount += 1;
					rsStickers.MoveNext();
					if (rsStickers.EndOfFile())
					{
						break;
					}
				}
				if (intCount > 0)
				{
					intCount -= 1;
				}
			}
			else
			{
				VerifyYearStickerNumber = false;
				intCount = -1;
			}
			return VerifyYearStickerNumber;
		}

		private bool VerifyPlateNumber(string strPlate, int intCount, string strprefix, string strSuffix, int lngPlate, string strPlateType)
		{
			bool VerifyPlateNumber = false;
			clsDRWrapper rsPL = new clsDRWrapper();
			if (MotorVehicle.Statics.PlateGroup == 0)
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE (FormattedInventory) = '" + strPlate + "' AND Status = 'A' ORDER BY Prefix, Number, Suffix";
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE (FormattedInventory) = '" + strPlate + "' AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.PlateGroup) + " OR [Group] = 0) ORDER BY Prefix, Number, Suffix";
			}
			rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
			{
				VerifyPlateNumber = true;
				strprefix = FCConvert.ToString(rsPL.Get_Fields_String("Prefix"));
				strSuffix = FCConvert.ToString(rsPL.Get_Fields_String("Suffix"));
				lngPlate = FCConvert.ToInt32(rsPL.Get_Fields_Int32("Number"));
				strPlateType = FCConvert.ToString(rsPL.Get_Fields("Code"));
				intCount = 0;
				if (MotorVehicle.Statics.PlateGroup == 0)
				{
					MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Prefix = '" + strprefix + "' AND Number >= " + FCConvert.ToString(lngPlate) + " AND Suffix = '" + strSuffix + "' AND Status = 'A' ORDER BY Prefix, Number, Suffix";
				}
				else
				{
					MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Prefix = '" + strprefix + "' AND Number >= " + FCConvert.ToString(lngPlate) + " AND Suffix = '" + strSuffix + "' AND Status = 'A' AND ([Group] = " + FCConvert.ToString(MotorVehicle.Statics.PlateGroup) + " OR [Group] = 0) ORDER BY Prefix, Number, Suffix";
				}
				rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
				while (lngPlate + intCount == FCConvert.ToInt32(rsPL.Get_Fields_Int32("Number")))
				{
					intCount += 1;
					rsPL.MoveNext();
					if (rsPL.EndOfFile())
					{
						break;
					}
				}
				if (intCount > 0)
				{
					intCount -= 1;
				}
			}
			else
			{
				VerifyPlateNumber = false;
				intCount = -1;
			}
			return VerifyPlateNumber;
		}

		private void FormatGrid()
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
			{
				vsVehicles.TextMatrix(0, intNumberCol, "");
				vsVehicles.TextMatrix(0, intPrintedCol, "");
				vsVehicles.TextMatrix(0, intVINCol, "VIN");
				vsVehicles.TextMatrix(0, intPlateCol, "Plate");
				vsVehicles.TextMatrix(0, intYearCol, "Year");
				vsVehicles.TextMatrix(0, intMakeCol, "Make");
				vsVehicles.TextMatrix(0, intStyleCol, "Style");
				vsVehicles.TextMatrix(0, intUnitCol, "Unit");
				vsVehicles.TextMatrix(0, intCTACol, "CTA#");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))) != "")
				{
					vsVehicles.TextMatrix(0, intUserDefined1Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))));
				}
				else
				{
					vsVehicles.ColHidden(intUserDefined1Col, true);
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))) != "")
				{
					vsVehicles.TextMatrix(0, intUserDefined2Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))));
				}
				else
				{
					vsVehicles.ColHidden(intUserDefined2Col, true);
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))) != "")
				{
					vsVehicles.TextMatrix(0, intUserDefined3Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))));
				}
				else
				{
					vsVehicles.ColHidden(intUserDefined3Col, true);
				}
				vsVehicles.ColWidth(intNumberCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.05));
				vsVehicles.ColWidth(intVINCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.24));
				vsVehicles.ColWidth(intPlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intYearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intMakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intStyleCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.06));
				vsVehicles.ColWidth(intUnitCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
				vsVehicles.ColWidth(intCTACol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intUserDefined1Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
				vsVehicles.ColWidth(intUserDefined2Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
				vsVehicles.ColWidth(intUserDefined3Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.13));
				vsVehicles.ColHidden(intPrintedCol, true);
				vsVehicles.ColAlignment(intNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intVINCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intPlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intYearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intMakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intStyleCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUnitCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intCTACol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUserDefined1Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUserDefined2Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUserDefined3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColDataType(intPrintedCol, FCGrid.DataTypeSettings.flexDTBoolean);
			}
			else
			{
				vsVehicles.TextMatrix(0, intNumberCol, "");
				vsVehicles.TextMatrix(0, intPrintedCol, "");
				vsVehicles.TextMatrix(0, intMVR3ToVoidCol, "");
				vsVehicles.TextMatrix(0, intVINCol, "VIN");
				vsVehicles.TextMatrix(0, intPlateCol, "Plate");
				vsVehicles.TextMatrix(0, intYearCol, "Year");
				vsVehicles.TextMatrix(0, intMakeCol, "Make");
				vsVehicles.TextMatrix(0, intModelCol, "Model");
				vsVehicles.TextMatrix(0, intStyleCol, "Style");
				vsVehicles.TextMatrix(0, intAxlesCol, "Axles");
				vsVehicles.TextMatrix(0, intUnitCol, "Unit");
				vsVehicles.TextMatrix(0, intCTACol, "CTA#");
				vsVehicles.TextMatrix(0, intPriorTitleCol, "Prior Title");
				vsVehicles.TextMatrix(0, intPriorStateCol, "Prior State");
				vsVehicles.TextMatrix(0, intMonthStickerCol, "Month St");
				vsVehicles.TextMatrix(0, intYearStickerCol, "Year St");
				vsVehicles.TextMatrix(0, intMVR3Col, "MVR3");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))) != "")
				{
					vsVehicles.TextMatrix(0, intUserDefined1Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))));
				}
				else
				{
					vsVehicles.ColHidden(intUserDefined1Col, true);
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))) != "")
				{
					vsVehicles.TextMatrix(0, intUserDefined2Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))));
				}
				else
				{
					vsVehicles.ColHidden(intUserDefined2Col, true);
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))) != "")
				{
					vsVehicles.TextMatrix(0, intUserDefined3Col, fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))));
				}
				else
				{
					vsVehicles.ColHidden(intUserDefined3Col, true);
				}
				vsReprint.TextMatrix(0, intReprintPlateCol, "Plate");
				vsReprint.TextMatrix(0, intReprintOldMVR3Col, "Old MVR3");
				vsReprint.TextMatrix(0, intReprintNewMVR3Col, "New MVR3");
				vsVehicles.ColWidth(intNumberCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.05));
				vsVehicles.ColWidth(intVINCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.24));
				vsVehicles.ColWidth(intPlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intYearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intMakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intModelCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.09));
				vsVehicles.ColWidth(intStyleCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.06));
				vsVehicles.ColWidth(intAxlesCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.06));
				vsVehicles.ColWidth(intUnitCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.07));
				vsVehicles.ColWidth(intCTACol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intPriorTitleCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intPriorStateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
				vsVehicles.ColWidth(intMonthStickerCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intYearStickerCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intMVR3Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
				vsVehicles.ColWidth(intUserDefined1Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
				vsVehicles.ColWidth(intUserDefined2Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
				vsVehicles.ColWidth(intUserDefined3Col, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
				vsVehicles.ColHidden(intPrintedCol, true);
				vsVehicles.ColHidden(intMVR3ToVoidCol, true);
				vsReprint.ColWidth(intReprintPlateCol, FCConvert.ToInt32(vsReprint.WidthOriginal * 0.33));
				vsReprint.ColWidth(intReprintOldMVR3Col, FCConvert.ToInt32(vsReprint.WidthOriginal * 0.33));
				vsReprint.ColWidth(intReprintNewMVR3Col, FCConvert.ToInt32(vsReprint.WidthOriginal * 0.05));
				vsVehicles.ColAlignment(intNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intVINCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intPlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intYearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intMakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intStyleCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intAxlesCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUnitCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intCTACol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intPriorTitleCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intPriorStateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intMonthStickerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intYearStickerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intMVR3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUserDefined1Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUserDefined2Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColAlignment(intUserDefined3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsVehicles.ColDataType(intPrintedCol, FCGrid.DataTypeSettings.flexDTBoolean);
				vsReprint.ColAlignment(intReprintPlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsReprint.ColAlignment(intReprintOldMVR3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsReprint.ColAlignment(intReprintNewMVR3Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
		}

		private void vsReprint_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsReprint.Col == intReprintNewMVR3Col)
			{
				if (fecherFoundation.Strings.Trim(vsReprint.TextMatrix(vsReprint.Row, vsReprint.Col)) != strOldValue)
				{
					AutoFillReprintMVR3();
				}
			}
		}

		private void vsReprint_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			strOldValue = vsReprint.TextMatrix(vsReprint.Row, vsReprint.Col);
		}

		private void vsReprint_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsReprint_RowColChange(object sender, System.EventArgs e)
		{
			vsReprint.EditCell();
		}

		private void vsVehicles_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsVehicles.Col == intPlateCol)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(vsVehicles.Row, vsVehicles.Col)) != strOldValue)
				{
					AutoFillPlate();
				}
			}
			else if (vsVehicles.Col == intYearStickerCol)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(vsVehicles.Row, vsVehicles.Col)) != strOldValue)
				{
					AutoFillYearSticker();
				}
			}
			else if (vsVehicles.Col == intMonthStickerCol)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(vsVehicles.Row, vsVehicles.Col)) != strOldValue)
				{
					AutoFillMonthSticker();
				}
			}
			else if (vsVehicles.Col == intMVR3Col)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(vsVehicles.Row, vsVehicles.Col)) != strOldValue)
				{
					AutoFillMVR3();
				}
			}
		}

		private void vsVehicles_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			strOldValue = vsVehicles.TextMatrix(vsVehicles.Row, vsVehicles.Col);
			if (MotorVehicle.Statics.gboolFromLongTermDataEntry && vsVehicles.Col == intPlateCol)
			{
				e.Cancel = true;
			}
		}

		private void vsVehicles_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsVehicles.Col == intAxlesCol || vsVehicles.Col == intMVR3Col || vsVehicles.Col == intMonthStickerCol || vsVehicles.Col == intYearCol || vsVehicles.Col == intYearStickerCol)
			{
				if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsVehicles_RowColChange(object sender, System.EventArgs e)
		{
			if (vsVehicles.Col == intVINCol)
			{
				vsVehicles.EditMaxLength = 17;
			}
			else if (vsVehicles.Col == intPlateCol)
			{
				vsVehicles.EditMaxLength = 7;
			}
			else if (vsVehicles.Col == intMakeCol)
			{
				vsVehicles.EditMaxLength = 4;
			}
			else if (vsVehicles.Col == intModelCol)
			{
				vsVehicles.EditMaxLength = 6;
			}
			else if (vsVehicles.Col == intStyleCol)
			{
				vsVehicles.EditMaxLength = 2;
			}
			else if (vsVehicles.Col == intAxlesCol)
			{
				vsVehicles.EditMaxLength = 1;
			}
			else if (vsVehicles.Col == intUnitCol)
			{
				if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
				{
					vsVehicles.EditMaxLength = 12;
				}
				else
				{
					vsVehicles.EditMaxLength = 4;
				}
			}
			else if (vsVehicles.Col == intCTACol)
			{
				vsVehicles.EditMaxLength = 11;
			}
			else if (vsVehicles.Col == intPriorTitleCol)
			{
				vsVehicles.EditMaxLength = 20;
			}
			else if (vsVehicles.Col == intPriorStateCol)
			{
				vsVehicles.EditMaxLength = 2;
			}
			else if (vsVehicles.Col == intMVR3Col)
			{
				vsVehicles.EditMaxLength = 7;
			}
			else
			{
				vsVehicles.EditMaxLength = 0;
			}
			if (vsVehicles.Editable != FCGrid.EditableSettings.flexEDNone)
			{
				vsVehicles.EditCell();
			}
		}

		private void vsVehicles_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsVehicles.Col == intMakeCol)
			{
				e.Cancel = !VerifyMake(vsVehicles.EditText);
			}
			else if (vsVehicles.Col == intVINCol)
			{
				e.Cancel = !VerifyVIN(vsVehicles.EditText);
			}
			else if (vsVehicles.Col == intModelCol)
			{
				e.Cancel = !VerifyModel(vsVehicles.EditText);
			}
			else if (vsVehicles.Col == intStyleCol)
			{
				e.Cancel = !VerifyStyle(vsVehicles.EditText, vsVehicles.TextMatrix(vsVehicles.Row, intPlateCol));
			}
			else if (vsVehicles.Col == intYearCol)
			{
				e.Cancel = !VerifyYear(vsVehicles.EditText, vsVehicles.TextMatrix(vsVehicles.Row, intVINCol));
			}
			else if (vsVehicles.Col == intPlateCol)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != "")
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != strOldValue)
					{
						if (VerifyPlateNumber(vsVehicles.EditText, 0, "", "", 0, ""))
						{
							// do nothing
						}
						else
						{
							MessageBox.Show("The plate you entered was not found in inventory.", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
						}
					}
				}
			}
			else if (vsVehicles.Col == intYearStickerCol)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != "")
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != strOldValue)
					{
						if (VerifyYearStickerNumber(FCConvert.ToInt32(vsVehicles.EditText), 0))
						{
							// do nothing
						}
						else
						{
							MessageBox.Show("The year sticker you entered was not found in inventory.", "Invalid Year Sticker", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
						}
					}
				}
			}
			else if (vsVehicles.Col == intMonthStickerCol)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != "")
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != strOldValue)
					{
						if (VerifyMonthStickerNumber(FCConvert.ToInt32(vsVehicles.EditText), 0))
						{
							// do nothing
						}
						else
						{
							MessageBox.Show("The month sticker you entered was not found in inventory.", "Invalid Month Sticker", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
						}
					}
				}
			}
			else if (vsVehicles.Col == intMVR3Col)
			{
				if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != "")
				{
					if (fecherFoundation.Strings.Trim(vsVehicles.EditText) != strOldValue)
					{
						if (VerifyMVR3Number(FCConvert.ToInt32(vsVehicles.EditText), 0))
						{
							// do nothing
						}
						else
						{
							MessageBox.Show("The MVR3 number you entered was not found in inventory.", "Invalid MVR3 Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
						}
					}
				}
			}
		}

		private bool VerifyMake(string strMake)
		{
			bool VerifyMake = false;
			clsDRWrapper rsMake = new clsDRWrapper();
			// 
			VerifyMake = true;
			// 
			if ((MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "TC") || (MotorVehicle.Statics.Class == "TL") || (MotorVehicle.Statics.Class == "CL") || (MotorVehicle.Statics.Class == "CI") || (MotorVehicle.Statics.Class == "CV"))
			{
				// do nothing
			}
			else
			{
				if (strMake == "")
				{
					MessageBox.Show("There must be a Make entered for this vehicle.", "Make Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					VerifyMake = false;
					return VerifyMake;
				}
				MotorVehicle.Statics.strSql = "SELECT RTRIM(Code) AS Code FROM Make ORDER BY Code";
				// kk08232016 tromvs-57  Add RTRIM to Correct 3 char codes
				rsMake.OpenRecordset(MotorVehicle.Statics.strSql);
				if (rsMake.EndOfFile() != true && rsMake.BeginningOfFile() != true)
				{
					rsMake.MoveLast();
					rsMake.MoveFirst();
					if (!rsMake.FindFirstRecord("Code", strMake))
					{
						VerifyMake = false;
						MessageBox.Show("That is not a valid make.", "Invalid Make", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						if (MotorVehicle.Statics.Class == "MH" || MotorVehicle.Statics.Class == "RV" || MotorVehicle.Statics.Class == "X")
						{
							// msgbox for body
						}
					}
				}
				else
				{
					MessageBox.Show("There are no Entries in the Make Table.", "Table Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					VerifyMake = false;
					return VerifyMake;
				}
			}
			rsMake.Reset();
			return VerifyMake;
		}

		private bool VerifyModel(string strModel)
		{
			bool VerifyModel = false;
			VerifyModel = true;
			if ((MotorVehicle.Statics.Class == "SE") || (MotorVehicle.Statics.Class == "TC") || (MotorVehicle.Statics.Class == "TL") || (MotorVehicle.Statics.Class == "CL"))
			{
				// do nothing
			}
			else
			{
				if (FCConvert.CBool(strModel))
				{
					MessageBox.Show("There must be a Model Type for this vehicle.", "Model Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					VerifyModel = false;
					return VerifyModel;
				}
			}
			return VerifyModel;
		}

		private bool VerifyStyle(string strStyle, string strPlate)
		{
            if (strStyle == "")
            {
                MessageBox.Show("There must be a style entered for this vehicle.", "Style Required",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                if (styleCodeService.ValidateStyleCode(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"),
                    MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"),
                    strPlate, strStyle))
                {
                    return true;
                }
                else
                {
                    MessageBox.Show(
                        "The style that has been entered is invalid for this vehicle type.",
                        "Invalid Style", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    
                    return false;
                }
            }
		}

		private bool VerifyVIN(string strVin)
		{
			bool VerifyVIN = false;
			int ans;
			VerifyVIN = true;
			if (fecherFoundation.Strings.Trim(strVin) == "")
			{
				MessageBox.Show("You must enter a VIN before you may proceed.", "No VIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				VerifyVIN = false;
				return VerifyVIN;
			}
			if (Strings.InStr(1, strVin, "_", CompareConstants.vbBinaryCompare) != 0)
			{
				MessageBox.Show("You must enter a valid VIN before you may proceed.", "Invalid VIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				VerifyVIN = false;
				return VerifyVIN;
			}
			return VerifyVIN;
		}

		private bool VerifyYear(string strYear, string strVin)
		{
			bool VerifyYear = false;
			string VinText = "";
			int YearNumber;
			bool blnOK = false;
			VerifyYear = true;
			if (strYear.Length < 4)
			{
				VerifyYear = false;
				MessageBox.Show("There must be a valid Year.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return VerifyYear;
			}
			if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CV")
			{
				if (Conversion.Val(strYear) + 25 > DateTime.Now.Year || Conversion.Val(strYear) < 1949)
				{
					MessageBox.Show("Custom Vehicles must be at least 25 years old and cannot be older than model year 1949.", "Modified Vehicle", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					VerifyYear = false;
					return VerifyYear;
				}
			}
			else
			{
				if (Conversion.Val(strYear) < 1981 && Conversion.Val(strYear) != 0)
					return VerifyYear;
				if (strVin != "")
				{
					VinText = strVin;
					VinText = Strings.Mid(VinText, 10, 1);
					blnOK = false;
					if (VinText == "1")
					{
						if (Conversion.Val(strYear) == 2001)
						{
							blnOK = true;
						}
					}
					else if (VinText == "2")
					{
						if (Conversion.Val(strYear) == 2002)
						{
							blnOK = true;
						}
					}
					else if (VinText == "3")
					{
						if (Conversion.Val(strYear) == 2003)
						{
							blnOK = true;
						}
					}
					else if (VinText == "4")
					{
						if (Conversion.Val(strYear) == 2004)
						{
							blnOK = true;
						}
					}
					else if (VinText == "5")
					{
						if (Conversion.Val(strYear) == 2005)
						{
							blnOK = true;
						}
					}
					else if (VinText == "6")
					{
						if (Conversion.Val(strYear) == 2006)
						{
							blnOK = true;
						}
					}
					else if (VinText == "7")
					{
						if (Conversion.Val(strYear) == 2007)
						{
							blnOK = true;
						}
					}
					else if (VinText == "8")
					{
						if (Conversion.Val(strYear) == 2008)
						{
							blnOK = true;
						}
					}
					else if (VinText == "9")
					{
						if (Conversion.Val(strYear) == 2009)
						{
							blnOK = true;
						}
					}
					else if (VinText == "A")
					{
						if (Conversion.Val(strYear) == 2010 || Conversion.Val(strYear) == 1980)
						{
							blnOK = true;
						}
					}
					else if (VinText == "B")
					{
						if (Conversion.Val(strYear) == 2011 || Conversion.Val(strYear) == 1981)
						{
							blnOK = true;
						}
					}
					else if (VinText == "C")
					{
						if (Conversion.Val(strYear) == 2012 || Conversion.Val(strYear) == 1982)
						{
							blnOK = true;
						}
					}
					else if (VinText == "D")
					{
						if (Conversion.Val(strYear) == 2013 || Conversion.Val(strYear) == 1983)
						{
							blnOK = true;
						}
					}
					else if (VinText == "E")
					{
						if (Conversion.Val(strYear) == 2014 || Conversion.Val(strYear) == 1984)
						{
							blnOK = true;
						}
					}
					else if (VinText == "F")
					{
						if (Conversion.Val(strYear) == 2015 || Conversion.Val(strYear) == 1985)
						{
							blnOK = true;
						}
					}
					else if (VinText == "G")
					{
						if (Conversion.Val(strYear) == 2016 || Conversion.Val(strYear) == 1986)
						{
							blnOK = true;
						}
					}
					else if (VinText == "H")
					{
						if (Conversion.Val(strYear) == 2017 || Conversion.Val(strYear) == 1987)
						{
							blnOK = true;
						}
					}
					else if (VinText == "J")
					{
						if (Conversion.Val(strYear) == 2018 || Conversion.Val(strYear) == 1988)
						{
							blnOK = true;
						}
					}
					else if (VinText == "K")
					{
						if (Conversion.Val(strYear) == 2019 || Conversion.Val(strYear) == 1989)
						{
							blnOK = true;
						}
					}
					else if (VinText == "L")
					{
						if (Conversion.Val(strYear) == 2020 || Conversion.Val(strYear) == 1990)
						{
							blnOK = true;
						}
					}
					else if (VinText == "M")
					{
						if (Conversion.Val(strYear) == 2021 || Conversion.Val(strYear) == 1991)
						{
							blnOK = true;
						}
					}
					else if (VinText == "N")
					{
						if (Conversion.Val(strYear) == 2022 || Conversion.Val(strYear) == 1992)
						{
							blnOK = true;
						}
					}
					else if (VinText == "P")
					{
						if (Conversion.Val(strYear) == 2023 || Conversion.Val(strYear) == 1993)
						{
							blnOK = true;
						}
					}
					else if (VinText == "R")
					{
						if (Conversion.Val(strYear) == 2024 || Conversion.Val(strYear) == 1994)
						{
							blnOK = true;
						}
					}
					else if (VinText == "S")
					{
						if (Conversion.Val(strYear) == 2025 || Conversion.Val(strYear) == 1995)
						{
							blnOK = true;
						}
					}
					else if (VinText == "T")
					{
						if (Conversion.Val(strYear) == 2026 || Conversion.Val(strYear) == 1996)
						{
							blnOK = true;
						}
					}
					else if (VinText == "V")
					{
						if (Conversion.Val(strYear) == 2027 || Conversion.Val(strYear) == 1997)
						{
							blnOK = true;
						}
					}
					else if (VinText == "W")
					{
						if (Conversion.Val(strYear) == 2028 || Conversion.Val(strYear) == 1998)
						{
							blnOK = true;
						}
					}
					else if (VinText == "X")
					{
						if (Conversion.Val(strYear) == 2029 || Conversion.Val(strYear) == 1999)
						{
							blnOK = true;
						}
					}
					else if (VinText == "Y")
					{
						if (Conversion.Val(strYear) == 2030 || Conversion.Val(strYear) == 2000)
						{
							blnOK = true;
						}
					}
					if (blnOK)
					{
						if (MotorVehicle.Statics.RegistrationType == "AQ" || MotorVehicle.Statics.RegistrationType == "MQ")
						{
							if (Conversion.Val(strYear) + 25 > DateTime.Now.Year)
							{
								MessageBox.Show("The Vehicle must be at least 25 years old to be an Antique.", "Antique Vehicle", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								VerifyYear = false;
								return VerifyYear;
							}
						}
					}
					else
					{
						if (MessageBox.Show("The Year Digit in the VIN does not match the keyed in Vehicle Year." + "\r\n" + "Do you wish to continue?", "Year Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
						{
							VerifyYear = false;
						}
					}
				}
			}
			return VerifyYear;
		}

		private void AutoFillReprintMVR3(int intRow = 1)
		{
			int lngMVR3 = 0;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int intMVR3Count = 0;
			int lngHighMVR3 = 0;
			// vbPorter upgrade warning: counter2 As int	OnWriteFCConvert.ToInt32(
			int counter2;
			int intFilledToRow;
			intFilledToRow = 0;
			for (counter = intRow; counter <= (vsReprint.Rows - 1); counter++)
			{
				if (intFilledToRow < counter)
				{
					if (fecherFoundation.Strings.Trim(vsReprint.TextMatrix(counter, intReprintNewMVR3Col)) != "")
					{
						lngMVR3 = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(vsReprint.TextMatrix(counter, intReprintNewMVR3Col)))));
						if (VerifyMVR3Number(lngMVR3, intMVR3Count))
						{
							lngHighMVR3 = lngMVR3 + intMVR3Count;
							for (counter2 = 1; counter2 <= (vsReprint.Rows - 1); counter2++)
							{
								if (counter2 != counter)
								{
									if (Conversion.Val(fecherFoundation.Strings.Trim(vsReprint.TextMatrix(counter2, intReprintNewMVR3Col))) >= lngMVR3 && Conversion.Val(fecherFoundation.Strings.Trim(vsReprint.TextMatrix(counter2, intReprintNewMVR3Col))) <= lngHighMVR3)
									{
										vsReprint.TextMatrix(counter2, intReprintNewMVR3Col, "");
									}
								}
							}
							if (intMVR3Count > 0)
							{
								for (counter2 = 1; counter2 <= intMVR3Count; counter2++)
								{
									if (vsReprint.Rows > counter + counter2)
									{
										vsReprint.TextMatrix(counter + counter2, intReprintNewMVR3Col, FCConvert.ToString(lngMVR3 + counter2));
										intFilledToRow = counter + counter2;
									}
									else
									{
										break;
									}
								}
							}
							else
							{
								intFilledToRow = counter;
							}
						}
						else
						{
							vsReprint.TextMatrix(counter, intReprintNewMVR3Col, "");
							intFilledToRow = counter;
						}
					}
				}
			}
		}

		private void MakeCode(string x)
		{
			if (x == "M")
			{
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge") > 1)
				{
					MotorVehicle.Statics.strCodeM = "SMD";
					if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth") < 10)
					{
						MotorVehicle.Statics.strCodeM += "0" + fecherFoundation.Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
					}
					else
					{
						MotorVehicle.Statics.strCodeM += fecherFoundation.Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
					}
				}
				else
				{
					MotorVehicle.Statics.strCodeM = "SMS";
					if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth") < 10)
					{
						MotorVehicle.Statics.strCodeM += "0" + fecherFoundation.Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
					}
					else
					{
						MotorVehicle.Statics.strCodeM += fecherFoundation.Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
					}
				}
			}
			else
			{
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge") > 1)
				{
					MotorVehicle.Statics.strCodeY = "SYD" + Strings.Right(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")), 2);
				}
				else
				{
					MotorVehicle.Statics.strCodeY = "SYS" + Strings.Right(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")), 2);
				}
			}
		}

		private void MakeCodeP()
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM Inventory WHERE (FormattedInventory) = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				MotorVehicle.Statics.strCodeP = FCConvert.ToString(rs.Get_Fields("Code"));
			}
		}
	}
}
