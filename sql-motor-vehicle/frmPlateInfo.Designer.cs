//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;
using fecherFoundation.VisualBasicLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPlateInfo.
	/// </summary>
	partial class frmPlateInfo
	{
		public fecherFoundation.FCComboBox cmbLongTerm;
		public fecherFoundation.FCLabel lblLongTerm;
		public fecherFoundation.FCComboBox cmbPreReg;
		public fecherFoundation.FCLabel lblPreReg;
		public fecherFoundation.FCComboBox cmbGroup;
		public fecherFoundation.FCComboBox cmbSearchBy;
		public fecherFoundation.FCComboBox cmbRegType1;
		public fecherFoundation.FCComboBox cmbAnnualReg;
		public fecherFoundation.FCLabel lblAnnualReg;
		public fecherFoundation.FCComboBox cmbPlate;
		public System.Collections.Generic.List<T2KOverTypeBox> txtPlateType;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblPlateType;
		public fecherFoundation.FCFrame fraFleetRegistration;
		public fecherFoundation.FCComboBox cboFleets;
		public fecherFoundation.FCButton cmdRegister;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdRegisterNew;
		public fecherFoundation.FCButton cmdPreRegister;
		public FCGrid vsVehicles;
		public fecherFoundation.FCLabel lblFleetReg;
		public fecherFoundation.FCLabel Label10;
        public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCFrame fraResults;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdGetInfo;
		public fecherFoundation.FCGrid vsResults;
		public fecherFoundation.FCLabel lblRecordsReturned;
		public fecherFoundation.FCFrame fraSelection;
		public fecherFoundation.FCTextBox txtCriteria;
		public fecherFoundation.FCLabel lblVehicle;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCComboBox cboLongTermRegPlateYear;
		public fecherFoundation.FCTimer Timer1;
		public fecherFoundation.FCButton cmdStartOver;
		public fecherFoundation.FCCheckBox chkExciseTaxPaid;
		public fecherFoundation.FCComboBox cboClass;
		public fecherFoundation.FCComboBox cboClass2;
		public fecherFoundation.FCButton cmdProcess;
		public Global.T2KOverTypeBox txtPlateType_1;
		public Global.T2KOverTypeBox txtPlateType_2;
		public Global.T2KOverTypeBox txtPlateType_0;
		public fecherFoundation.FCFrame fraSubClass;
		public fecherFoundation.FCListBox lstSubClass;
		public fecherFoundation.FCLabel lblPlateType_0;
		public fecherFoundation.FCLabel lblPlateType_1;
		public fecherFoundation.FCLabel lblPlateType_2;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSearchSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuRRR;
		public fecherFoundation.FCToolStripMenuItem mnuRRRSame;
		public fecherFoundation.FCToolStripMenuItem mnuRRRNew;
		public fecherFoundation.FCToolStripMenuItem mnuRRROld;
		public fecherFoundation.FCToolStripMenuItem mnuRRT;
		public fecherFoundation.FCToolStripMenuItem mnuRRTTransferPlate;
		public fecherFoundation.FCToolStripMenuItem mnuRRTReRegPlate;
		public fecherFoundation.FCToolStripMenuItem mnuRRTNewPlate;
		public fecherFoundation.FCToolStripMenuItem mnuRRTOldPlate;
		public fecherFoundation.FCToolStripMenuItem mnuNRR;
		public fecherFoundation.FCToolStripMenuItem mnuNRRNewPlate;
		public fecherFoundation.FCToolStripMenuItem mnuNRROldPlate;
		public fecherFoundation.FCToolStripMenuItem mnuNRRReplacementPlate;
		public fecherFoundation.FCToolStripMenuItem mnuNRT;
		public fecherFoundation.FCToolStripMenuItem mnuNRTTransferPlate;
		public fecherFoundation.FCToolStripMenuItem mnuNRTNewClass;
		public fecherFoundation.FCToolStripMenuItem mnuNRTNewLost;
		public fecherFoundation.FCToolStripMenuItem mnuNRTOldPlate;
		public fecherFoundation.FCToolStripMenuItem mnuHELD;
		public fecherFoundation.FCToolStripMenuItem mnuSpace;
		public fecherFoundation.FCToolStripMenuItem mnuCORR;
		public fecherFoundation.FCToolStripMenuItem mnuCORRwoPlateChange;
		public fecherFoundation.FCToolStripMenuItem mnuCORRPlateChange;
		public fecherFoundation.FCToolStripMenuItem mnuDUP;
		public fecherFoundation.FCToolStripMenuItem mnuLOST;
		public fecherFoundation.FCToolStripMenuItem mnuLOSTNewPlate;
		public fecherFoundation.FCToolStripMenuItem mnuLOSTOldPlate;
		public fecherFoundation.FCToolStripMenuItem mnuDUPSTK;
		public fecherFoundation.FCToolStripMenuItem mnuGVW;
		public fecherFoundation.FCToolStripMenuItem mnuSpace2;
		public fecherFoundation.FCToolStripMenuItem mnuTRANSIT;
		public fecherFoundation.FCToolStripMenuItem mnuBOOSTER;
		public fecherFoundation.FCToolStripMenuItem mnuSPECIAL;
		public fecherFoundation.FCToolStripMenuItem mnuSpace3;
		public fecherFoundation.FCToolStripMenuItem mnuVOID;
		public fecherFoundation.FCToolStripMenuItem mnuUPDATE;
		public fecherFoundation.FCToolStripMenuItem mnuREDBOOK;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCLabel lblRegType;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPlateInfo));
            this.cmbLongTerm = new fecherFoundation.FCComboBox();
            this.lblLongTerm = new fecherFoundation.FCLabel();
            this.cmbPreReg = new fecherFoundation.FCComboBox();
            this.lblPreReg = new fecherFoundation.FCLabel();
            this.cmbGroup = new fecherFoundation.FCComboBox();
            this.cmbSearchBy = new fecherFoundation.FCComboBox();
            this.cmbRegType1 = new fecherFoundation.FCComboBox();
            this.cmbAnnualReg = new fecherFoundation.FCComboBox();
            this.lblAnnualReg = new fecherFoundation.FCLabel();
            this.cmbPlate = new fecherFoundation.FCComboBox();
            this.fraFleetRegistration = new fecherFoundation.FCFrame();
            this.txtEffectiveDate = new fecherFoundation.FCDateTimePicker();
            this.cboFleets = new fecherFoundation.FCComboBox();
            this.cmdRegister = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdRegisterNew = new fecherFoundation.FCButton();
            this.cmdPreRegister = new fecherFoundation.FCButton();
            this.vsVehicles = new fecherFoundation.FCGrid();
            this.lblFleetReg = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.fraResults = new fecherFoundation.FCFrame();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.cmdGetInfo = new fecherFoundation.FCButton();
            this.vsResults = new fecherFoundation.FCGrid();
            this.lblRecordsReturned = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraSelection = new fecherFoundation.FCFrame();
            this.txtCriteria = new fecherFoundation.FCTextBox();
            this.lblVehicle = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.cboLongTermRegPlateYear = new fecherFoundation.FCComboBox();
            this.Timer1 = new fecherFoundation.FCTimer(this.components);
            this.cmdStartOver = new fecherFoundation.FCButton();
            this.chkExciseTaxPaid = new fecherFoundation.FCCheckBox();
            this.cboClass = new fecherFoundation.FCComboBox();
            this.cboClass2 = new fecherFoundation.FCComboBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.txtPlateType_1 = new Global.T2KOverTypeBox();
            this.txtPlateType_2 = new Global.T2KOverTypeBox();
            this.txtPlateType_0 = new Global.T2KOverTypeBox();
            this.fraSubClass = new fecherFoundation.FCFrame();
            this.lstSubClass = new fecherFoundation.FCListBox();
            this.lblPlateType_0 = new fecherFoundation.FCLabel();
            this.lblPlateType_1 = new fecherFoundation.FCLabel();
            this.lblPlateType_2 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRR = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRRSame = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRRNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRROld = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRT = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRTTransferPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRTReRegPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRTNewPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRRTOldPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRR = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRRNewPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRROldPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRRReplacementPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRT = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRTTransferPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRTNewClass = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRTNewLost = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNRTOldPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuHELD = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSpace = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCORR = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCORRwoPlateChange = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCORRPlateChange = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDUP = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLOST = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLOSTNewPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLOSTOldPlate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDUPSTK = new fecherFoundation.FCToolStripMenuItem();
            this.mnuGVW = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSpace2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuTRANSIT = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBOOSTER = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSPECIAL = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSpace3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuVOID = new fecherFoundation.FCToolStripMenuItem();
            this.mnuUPDATE = new fecherFoundation.FCToolStripMenuItem();
            this.mnuREDBOOK = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearchSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.lblRegType = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.lblRegType1 = new fecherFoundation.FCLabel();
            this.lblPlate = new fecherFoundation.FCLabel();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFleetRegistration)).BeginInit();
            this.fraFleetRegistration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRegister)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRegisterNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreRegister)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraResults)).BeginInit();
            this.fraResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelection)).BeginInit();
            this.fraSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartOver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExciseTaxPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateType_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateType_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateType_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSubClass)).BeginInit();
            this.fraSubClass.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 694);
            this.BottomPanel.Size = new System.Drawing.Size(1068, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraFleetRegistration);
            this.ClientArea.Controls.Add(this.fraResults);
            this.ClientArea.Controls.Add(this.fraSubClass);
            this.ClientArea.Controls.Add(this.fraSelection);
            this.ClientArea.Controls.Add(this.lblPlate);
            this.ClientArea.Controls.Add(this.lblRegType);
            this.ClientArea.Controls.Add(this.cmbPlate);
            this.ClientArea.Controls.Add(this.lblRegType1);
            this.ClientArea.Controls.Add(this.cmbRegType1);
            this.ClientArea.Controls.Add(this.cboLongTermRegPlateYear);
            this.ClientArea.Controls.Add(this.cmbAnnualReg);
            this.ClientArea.Controls.Add(this.lblAnnualReg);
            this.ClientArea.Controls.Add(this.cmdStartOver);
            this.ClientArea.Controls.Add(this.chkExciseTaxPaid);
            this.ClientArea.Controls.Add(this.cboClass);
            this.ClientArea.Controls.Add(this.cboClass2);
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.txtPlateType_1);
            this.ClientArea.Controls.Add(this.txtPlateType_2);
            this.ClientArea.Controls.Add(this.txtPlateType_0);
            this.ClientArea.Controls.Add(this.lblPlateType_0);
            this.ClientArea.Controls.Add(this.lblPlateType_1);
            this.ClientArea.Controls.Add(this.lblPlateType_2);
            this.ClientArea.Size = new System.Drawing.Size(1088, 640);
            this.ClientArea.Controls.SetChildIndex(this.lblPlateType_2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPlateType_1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPlateType_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPlateType_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPlateType_2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPlateType_1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdProcess, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboClass2, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboClass, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkExciseTaxPaid, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdStartOver, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAnnualReg, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbAnnualReg, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboLongTermRegPlateYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbRegType1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblRegType1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPlate, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblRegType, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPlate, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraSelection, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraSubClass, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraResults, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraFleetRegistration, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSearch);
            this.TopPanel.Size = new System.Drawing.Size(1088, 60);
            this.TopPanel.TabIndex = 0;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(199, 28);
            this.HeaderText.Text = "Registration Menu";
            // 
            // cmbLongTerm
            // 
            this.cmbLongTerm.Items.AddRange(new object[] {
            "Long Term",
            "Annual"});
            this.cmbLongTerm.Location = new System.Drawing.Point(170, 290);
            this.cmbLongTerm.Name = "cmbLongTerm";
            this.cmbLongTerm.Size = new System.Drawing.Size(262, 40);
            this.cmbLongTerm.TabIndex = 8;
            this.cmbLongTerm.Text = "Annual";
            this.cmbLongTerm.Visible = false;
            this.cmbLongTerm.SelectedIndexChanged += new System.EventHandler(this.cmbLongTerm_SelectedIndexChanged);
            // 
            // lblLongTerm
            // 
            this.lblLongTerm.AutoSize = true;
            this.lblLongTerm.Location = new System.Drawing.Point(20, 304);
            this.lblLongTerm.Name = "lblLongTerm";
            this.lblLongTerm.Size = new System.Drawing.Size(38, 15);
            this.lblLongTerm.TabIndex = 7;
            this.lblLongTerm.Text = "TYPE";
            this.lblLongTerm.Visible = false;
            // 
            // cmbPreReg
            // 
            this.cmbPreReg.Items.AddRange(new object[] {
            "Pre Reg",
            "Regular"});
            this.cmbPreReg.Location = new System.Drawing.Point(170, 90);
            this.cmbPreReg.Name = "cmbPreReg";
            this.cmbPreReg.Size = new System.Drawing.Size(176, 40);
            this.cmbPreReg.TabIndex = 2;
            this.cmbPreReg.Text = "Regular";
            // 
            // lblPreReg
            // 
            this.lblPreReg.AutoSize = true;
            this.lblPreReg.Location = new System.Drawing.Point(20, 104);
            this.lblPreReg.Name = "lblPreReg";
            this.lblPreReg.Size = new System.Drawing.Size(139, 15);
            this.lblPreReg.TabIndex = 1;
            this.lblPreReg.Text = "REGISTRATION TYPE";
            // 
            // cmbGroup
            // 
            this.cmbGroup.Items.AddRange(new object[] {
            "Group",
            "Fleet"});
            this.cmbGroup.Location = new System.Drawing.Point(170, 190);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(262, 40);
            this.cmbGroup.TabIndex = 5;
            this.cmbGroup.Text = "Fleet";
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            // 
            // cmbSearchBy
            // 
            this.cmbSearchBy.Items.AddRange(new object[] {
            "Name",
            "Plate",
            "VIN",
            "MVR3 / Tax Receipt #",
            "Vehicle Search (Year/Make/Model)",
            "Year Sticker",
            "Month Sticker"});
            this.cmbSearchBy.Location = new System.Drawing.Point(20, 30);
            this.cmbSearchBy.Name = "cmbSearchBy";
            this.cmbSearchBy.Size = new System.Drawing.Size(404, 40);
            this.cmbSearchBy.TabIndex = 3;
            this.cmbSearchBy.Text = "Name";
            this.cmbSearchBy.SelectedIndexChanged += new System.EventHandler(this.optSearchBy_CheckedChanged);
            // 
            // cmbRegType1
            // 
            this.cmbRegType1.Items.AddRange(new object[] {
            "Re-Registration Regular",
            "Re-Registration Transfer",
            "New Registration Regular",
            "New Registration Transfer",
            "Complete Held Registration",
            "Correction",
            "Duplicate Registration",
            "Lost Plate / Stickers",
            "Duplicate Stickers",
            "RVW Change",
            "Transit Plate",
            "Booster Permit",
            "Special Reg Permit",
            "Void MVR",
            "New To System / Update",
            "Register Fleet / Group",
            "Void Permit",
            "Process CTA"});
            this.cmbRegType1.Location = new System.Drawing.Point(235, 105);
            this.cmbRegType1.Name = "cmbRegType1";
            this.cmbRegType1.Size = new System.Drawing.Size(337, 40);
            this.cmbRegType1.TabIndex = 4;
            this.cmbRegType1.SelectedIndexChanged += new System.EventHandler(this.optRegType_CheckedChanged);
            // 
            // cmbAnnualReg
            // 
            this.cmbAnnualReg.Items.AddRange(new object[] {
            "Annual",
            "Long Term"});
            this.cmbAnnualReg.Location = new System.Drawing.Point(235, 30);
            this.cmbAnnualReg.Name = "cmbAnnualReg";
            this.cmbAnnualReg.Size = new System.Drawing.Size(337, 40);
            this.cmbAnnualReg.TabIndex = 1;
            this.cmbAnnualReg.Text = "Annual";
            this.cmbAnnualReg.SelectedIndexChanged += new System.EventHandler(this.cmbAnnualReg_SelectedIndexChanged);
            // 
            // lblAnnualReg
            // 
            this.lblAnnualReg.AutoSize = true;
            this.lblAnnualReg.Location = new System.Drawing.Point(30, 44);
            this.lblAnnualReg.Name = "lblAnnualReg";
            this.lblAnnualReg.Size = new System.Drawing.Size(38, 15);
            this.lblAnnualReg.TabIndex = 22;
            this.lblAnnualReg.Text = "TYPE";
            // 
            // cmbPlate
            // 
            this.cmbPlate.Items.AddRange(new object[] {
            "",
            "",
            "",
            ""});
            this.cmbPlate.Location = new System.Drawing.Point(235, 155);
            this.cmbPlate.Name = "cmbPlate";
            this.cmbPlate.Size = new System.Drawing.Size(337, 40);
            this.cmbPlate.TabIndex = 6;
            this.cmbPlate.Visible = false;
            this.cmbPlate.SelectedIndexChanged += new System.EventHandler(this.optPlate_CheckedChanged);
            // 
            // fraFleetRegistration
            // 
            this.fraFleetRegistration.BackColor = System.Drawing.Color.White;
            this.fraFleetRegistration.Controls.Add(this.txtEffectiveDate);
            this.fraFleetRegistration.Controls.Add(this.cmbPreReg);
            this.fraFleetRegistration.Controls.Add(this.lblPreReg);
            this.fraFleetRegistration.Controls.Add(this.cmbLongTerm);
            this.fraFleetRegistration.Controls.Add(this.lblLongTerm);
            this.fraFleetRegistration.Controls.Add(this.cmbGroup);
            this.fraFleetRegistration.Controls.Add(this.cboFleets);
            this.fraFleetRegistration.Controls.Add(this.cmdRegister);
            this.fraFleetRegistration.Controls.Add(this.cmdCancel);
            this.fraFleetRegistration.Controls.Add(this.cmdRegisterNew);
            this.fraFleetRegistration.Controls.Add(this.cmdPreRegister);
            this.fraFleetRegistration.Controls.Add(this.vsVehicles);
            this.fraFleetRegistration.Controls.Add(this.lblFleetReg);
            this.fraFleetRegistration.Controls.Add(this.Label10);
            this.fraFleetRegistration.Location = new System.Drawing.Point(30, 30);
            this.fraFleetRegistration.Name = "fraFleetRegistration";
            this.fraFleetRegistration.Size = new System.Drawing.Size(1021, 664);
            this.fraFleetRegistration.TabIndex = 21;
            this.fraFleetRegistration.Text = "Register Fleet / Group";
            this.fraFleetRegistration.Visible = false;
            // 
            // txtEffectiveDate
            // 
            this.txtEffectiveDate.AutoSize = false;
            this.txtEffectiveDate.Format = Wisej.Web.DateTimePickerFormat.Short;
            this.txtEffectiveDate.Location = new System.Drawing.Point(170, 140);
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(137, 40);
            this.txtEffectiveDate.TabIndex = 3;
            // 
            // cboFleets
            // 
            this.cboFleets.BackColor = System.Drawing.SystemColors.Window;
            this.cboFleets.Location = new System.Drawing.Point(20, 240);
            this.cboFleets.Name = "cboFleets";
            this.cboFleets.Size = new System.Drawing.Size(412, 40);
            this.cboFleets.TabIndex = 6;
            this.cboFleets.SelectedIndexChanged += new System.EventHandler(this.cboFleets_SelectedIndexChanged);
            // 
            // cmdRegister
            // 
            this.cmdRegister.AppearanceKey = "actionButton";
            this.cmdRegister.Location = new System.Drawing.Point(20, 600);
            this.cmdRegister.Name = "cmdRegister";
            this.cmdRegister.Size = new System.Drawing.Size(214, 40);
            this.cmdRegister.TabIndex = 10;
            this.cmdRegister.Text = "Register Next Vehicle";
            this.cmdRegister.Click += new System.EventHandler(this.cmdRegister_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(689, 600);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(128, 40);
            this.cmdCancel.TabIndex = 13;
            this.cmdCancel.Text = "Completed";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdRegisterNew
            // 
            this.cmdRegisterNew.AppearanceKey = "actionButton";
            this.cmdRegisterNew.Location = new System.Drawing.Point(254, 600);
            this.cmdRegisterNew.Name = "cmdRegisterNew";
            this.cmdRegisterNew.Size = new System.Drawing.Size(212, 40);
            this.cmdRegisterNew.TabIndex = 11;
            this.cmdRegisterNew.Text = "Register New Vehicle";
            this.cmdRegisterNew.Click += new System.EventHandler(this.cmdRegisterNew_Click);
            // 
            // cmdPreRegister
            // 
            this.cmdPreRegister.AppearanceKey = "actionButton";
            this.cmdPreRegister.Location = new System.Drawing.Point(486, 600);
            this.cmdPreRegister.Name = "cmdPreRegister";
            this.cmdPreRegister.Size = new System.Drawing.Size(184, 40);
            this.cmdPreRegister.TabIndex = 12;
            this.cmdPreRegister.Text = "Complete Pre Reg";
            this.cmdPreRegister.Click += new System.EventHandler(this.cmdPreRegister_Click);
            // 
            // vsVehicles
            // 
            this.vsVehicles.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsVehicles.AutoSizeColumnsMode = Wisej.Web.DataGridViewAutoSizeColumnsMode.Fill;
            this.vsVehicles.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
            this.vsVehicles.Cols = 10;
            this.vsVehicles.Location = new System.Drawing.Point(20, 340);
            this.vsVehicles.Name = "vsVehicles";
            this.vsVehicles.Rows = 15;
            this.vsVehicles.ShowFocusCell = false;
            this.vsVehicles.Size = new System.Drawing.Size(981, 250);
            this.vsVehicles.TabIndex = 9;
            this.vsVehicles.Visible = false;
            this.vsVehicles.Click += new System.EventHandler(this.vsVehicles_ClickEvent);
            this.vsVehicles.KeyDown += new Wisej.Web.KeyEventHandler(this.vsVehicles_KeyDownEvent);
            // 
            // lblFleetReg
            // 
            this.lblFleetReg.Location = new System.Drawing.Point(20, 30);
            this.lblFleetReg.Name = "lblFleetReg";
            this.lblFleetReg.Size = new System.Drawing.Size(690, 50);
            this.lblFleetReg.TabIndex = 14;
            this.lblFleetReg.Text = resources.GetString("lblFleetReg.Text");
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 154);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(110, 16);
            this.Label10.TabIndex = 3;
            this.Label10.Text = "EFFECTIVE DATE";
            // 
            // fraResults
            // 
            this.fraResults.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraResults.BackColor = System.Drawing.Color.White;
            this.fraResults.Controls.Add(this.cmdReturn);
            this.fraResults.Controls.Add(this.cmdGetInfo);
            this.fraResults.Controls.Add(this.vsResults);
            this.fraResults.Controls.Add(this.lblRecordsReturned);
            this.fraResults.Controls.Add(this.Label1);
            this.fraResults.Location = new System.Drawing.Point(30, 30);
            this.fraResults.Name = "fraResults";
            this.fraResults.Size = new System.Drawing.Size(783, 528);
            this.fraResults.TabIndex = 20;
            this.fraResults.Text = "Results";
            this.fraResults.Visible = false;
            // 
            // cmdReturn
            // 
            this.cmdReturn.Anchor = Wisej.Web.AnchorStyles.Bottom;
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(291, 440);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(94, 40);
            this.cmdReturn.TabIndex = 3;
            this.cmdReturn.Text = "Return";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // cmdGetInfo
            // 
            this.cmdGetInfo.Anchor = Wisej.Web.AnchorStyles.Bottom;
            this.cmdGetInfo.AppearanceKey = "actionButton";
            this.cmdGetInfo.Enabled = false;
            this.cmdGetInfo.Location = new System.Drawing.Point(405, 440);
            this.cmdGetInfo.Name = "cmdGetInfo";
            this.cmdGetInfo.Size = new System.Drawing.Size(104, 40);
            this.cmdGetInfo.TabIndex = 4;
            this.cmdGetInfo.Text = "Get Info";
            this.cmdGetInfo.Click += new System.EventHandler(this.cmdGetInfo_Click);
            // 
            // vsResults
            // 
            this.vsResults.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsResults.Cols = 8;
            this.vsResults.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsResults.ExtendLastCol = true;
            this.vsResults.FixedCols = 0;
            this.vsResults.Location = new System.Drawing.Point(20, 80);
            this.vsResults.Name = "vsResults";
            this.vsResults.RowHeadersVisible = false;
            this.vsResults.Rows = 50;
            this.vsResults.ShowFocusCell = false;
            this.vsResults.Size = new System.Drawing.Size(729, 331);
            this.vsResults.TabIndex = 1;
            this.vsResults.DoubleClick += new System.EventHandler(this.vsResults_DblClick);
            // 
            // lblRecordsReturned
            // 
            this.lblRecordsReturned.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.lblRecordsReturned.Location = new System.Drawing.Point(20, 440);
            this.lblRecordsReturned.Name = "lblRecordsReturned";
            this.lblRecordsReturned.Size = new System.Drawing.Size(191, 19);
            this.lblRecordsReturned.TabIndex = 2;
            // 
            // Label1
            // 
            this.Label1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(743, 30);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "DOUBLE CLICK A SELECTION BELOW OR HIGHLIGHT THE MATCH YOU WANT TO USE AND PRESS T" +
    "HE \"GET INFO\" BUTTON. \'**\' INDICATES SEARCH CRITERIA WAS FOUND IN SECOND OWNER I" +
    "NFORMATION";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fraSelection
            // 
            this.fraSelection.BackColor = System.Drawing.Color.White;
            this.fraSelection.Controls.Add(this.txtCriteria);
            this.fraSelection.Controls.Add(this.cmbSearchBy);
            this.fraSelection.Controls.Add(this.lblVehicle);
            this.fraSelection.Controls.Add(this.Label3);
            this.fraSelection.Location = new System.Drawing.Point(30, 30);
            this.fraSelection.Name = "fraSelection";
            this.fraSelection.Size = new System.Drawing.Size(444, 210);
            this.fraSelection.TabIndex = 19;
            this.fraSelection.Text = "Search By";
            this.fraSelection.Visible = false;
            // 
            // txtCriteria
            // 
            this.txtCriteria.BackColor = System.Drawing.SystemColors.Window;
            this.txtCriteria.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCriteria.Location = new System.Drawing.Point(20, 115);
            this.txtCriteria.MaxLength = 20;
            this.txtCriteria.Name = "txtCriteria";
            this.txtCriteria.Size = new System.Drawing.Size(404, 40);
            this.txtCriteria.TabIndex = 2;
            this.txtCriteria.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCriteria_KeyPress);
            // 
            // lblVehicle
            // 
            this.lblVehicle.Location = new System.Drawing.Point(20, 165);
            this.lblVehicle.Name = "lblVehicle";
            this.lblVehicle.Size = new System.Drawing.Size(404, 25);
            this.lblVehicle.TabIndex = 3;
            this.lblVehicle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblVehicle.Visible = false;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 80);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(404, 25);
            this.Label3.TabIndex = 1;
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cboLongTermRegPlateYear
            // 
            this.cboLongTermRegPlateYear.BackColor = System.Drawing.SystemColors.Window;
            this.cboLongTermRegPlateYear.Location = new System.Drawing.Point(276, 242);
            this.cboLongTermRegPlateYear.Name = "cboLongTermRegPlateYear";
            this.cboLongTermRegPlateYear.Size = new System.Drawing.Size(104, 40);
            this.cboLongTermRegPlateYear.TabIndex = 9;
            this.cboLongTermRegPlateYear.Visible = false;
            this.cboLongTermRegPlateYear.SelectedIndexChanged += new System.EventHandler(this.cboLongTermRegPlateYear_SelectedIndexChanged);
            this.cboLongTermRegPlateYear.Leave += new System.EventHandler(this.cboLongTermRegPlateYear_Leave);
            this.cboLongTermRegPlateYear.Validating += new System.ComponentModel.CancelEventHandler(this.cboLongTermRegPlateYear_Validating);
            // 
            // Timer1
            // 
            this.Timer1.Enabled = false;
            this.Timer1.Interval = 200;
            this.Timer1.Location = new System.Drawing.Point(0, 0);
            this.Timer1.Name = null;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // cmdStartOver
            // 
            this.cmdStartOver.AppearanceKey = "actionButton";
            this.cmdStartOver.Location = new System.Drawing.Point(30, 416);
            this.cmdStartOver.Name = "cmdStartOver";
            this.cmdStartOver.Size = new System.Drawing.Size(124, 40);
            this.cmdStartOver.TabIndex = 18;
            this.cmdStartOver.Text = "Start Over";
            this.ToolTip1.SetToolTip(this.cmdStartOver, "If you get a plate validation error you must clear out the plate number before tr" +
        "ying to start over");
            this.cmdStartOver.Click += new System.EventHandler(this.cmdStartOver_Click);
            // 
            // chkExciseTaxPaid
            // 
            this.chkExciseTaxPaid.Location = new System.Drawing.Point(30, 205);
            this.chkExciseTaxPaid.Name = "chkExciseTaxPaid";
            this.chkExciseTaxPaid.Size = new System.Drawing.Size(138, 22);
            this.chkExciseTaxPaid.TabIndex = 7;
            this.chkExciseTaxPaid.Text = "Excise Tax is Paid";
            this.chkExciseTaxPaid.Visible = false;
            // 
            // cboClass
            // 
            this.cboClass.BackColor = System.Drawing.SystemColors.Window;
            this.cboClass.DropDownHeight = 380;
            this.cboClass.Location = new System.Drawing.Point(452, 242);
            this.cboClass.Name = "cboClass";
            this.cboClass.TabIndex = 11;
            this.cboClass.Visible = false;
            this.cboClass.DoubleClick += new System.EventHandler(this.cboClass_DoubleClick);
            this.cboClass.Validating += new System.ComponentModel.CancelEventHandler(this.cboClass_Validating);
            // 
            // cboClass2
            // 
            this.cboClass2.BackColor = System.Drawing.SystemColors.Window;
            this.cboClass2.Location = new System.Drawing.Point(452, 292);
            this.cboClass2.Name = "cboClass2";
            this.cboClass2.TabIndex = 14;
            this.cboClass2.Visible = false;
            this.cboClass2.DoubleClick += new System.EventHandler(this.cboClass2_DoubleClick);
            this.cboClass2.Validating += new System.ComponentModel.CancelEventHandler(this.cboClass2_Validating);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "actionButton";
            this.cmdProcess.Enabled = false;
            this.cmdProcess.Location = new System.Drawing.Point(174, 416);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(80, 40);
            this.cmdProcess.TabIndex = 17;
            this.cmdProcess.Text = "Next";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // txtPlateType_1
            // 
            this.txtPlateType_1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPlateType_1.Location = new System.Drawing.Point(276, 292);
            this.txtPlateType_1.MaxLength = 8;
            this.txtPlateType_1.Name = "txtPlateType_1";
            this.txtPlateType_1.Size = new System.Drawing.Size(156, 40);
            this.txtPlateType_1.TabIndex = 13;
            this.txtPlateType_1.Visible = false;
            this.txtPlateType_1.Enter += new System.EventHandler(this.txtPlateType_Enter);
            this.txtPlateType_1.Leave += new System.EventHandler(this.txtPlateType_Leave);
            this.txtPlateType_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtPlateType_Validate);
            this.txtPlateType_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPlateType_KeyDownEvent);
            // 
            // txtPlateType_2
            // 
            this.txtPlateType_2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPlateType_2.Location = new System.Drawing.Point(276, 342);
            this.txtPlateType_2.MaxLength = 8;
            this.txtPlateType_2.Name = "txtPlateType_2";
            this.txtPlateType_2.Size = new System.Drawing.Size(156, 40);
            this.txtPlateType_2.TabIndex = 16;
            this.txtPlateType_2.Visible = false;
            this.txtPlateType_2.Enter += new System.EventHandler(this.txtPlateType_Enter);
            this.txtPlateType_2.Leave += new System.EventHandler(this.txtPlateType_Leave);
            this.txtPlateType_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPlateType_Validate);
            this.txtPlateType_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPlateType_KeyDownEvent);
            // 
            // txtPlateType_0
            // 
            this.txtPlateType_0.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPlateType_0.Location = new System.Drawing.Point(276, 242);
            this.txtPlateType_0.MaxLength = 8;
            this.txtPlateType_0.Name = "txtPlateType_0";
            this.txtPlateType_0.Size = new System.Drawing.Size(156, 40);
            this.txtPlateType_0.TabIndex = 10;
            this.txtPlateType_0.Visible = false;
            this.txtPlateType_0.Enter += new System.EventHandler(this.txtPlateType_Enter);
            this.txtPlateType_0.Leave += new System.EventHandler(this.txtPlateType_Leave);
            this.txtPlateType_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtPlateType_Validate);
            this.txtPlateType_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPlateType_KeyDownEvent);
            // 
            // fraSubClass
            // 
            this.fraSubClass.BackColor = System.Drawing.Color.White;
            this.fraSubClass.Controls.Add(this.lstSubClass);
            this.fraSubClass.Location = new System.Drawing.Point(30, 226);
            this.fraSubClass.Name = "fraSubClass";
            this.fraSubClass.Size = new System.Drawing.Size(333, 138);
            this.fraSubClass.TabIndex = 19;
            this.fraSubClass.Text = "Please Select A Vehicle Subclass";
            this.fraSubClass.Visible = false;
            // 
            // lstSubClass
            // 
            this.lstSubClass.BackColor = System.Drawing.SystemColors.Window;
            this.lstSubClass.Location = new System.Drawing.Point(20, 30);
            this.lstSubClass.Name = "lstSubClass";
            this.lstSubClass.Size = new System.Drawing.Size(294, 89);
            this.lstSubClass.Sorted = true;
            this.lstSubClass.Sorting = Wisej.Web.SortOrder.Ascending;
            this.lstSubClass.TabIndex = 0;
            // 
            // lblPlateType_0
            // 
            this.lblPlateType_0.Location = new System.Drawing.Point(30, 256);
            this.lblPlateType_0.Name = "lblPlateType_0";
            this.lblPlateType_0.Size = new System.Drawing.Size(211, 40);
            this.lblPlateType_0.TabIndex = 8;
            // 
            // lblPlateType_1
            // 
            this.lblPlateType_1.Location = new System.Drawing.Point(30, 306);
            this.lblPlateType_1.Name = "lblPlateType_1";
            this.lblPlateType_1.Size = new System.Drawing.Size(211, 40);
            this.lblPlateType_1.TabIndex = 12;
            // 
            // lblPlateType_2
            // 
            this.lblPlateType_2.Location = new System.Drawing.Point(30, 356);
            this.lblPlateType_2.Name = "lblPlateType_2";
            this.lblPlateType_2.Size = new System.Drawing.Size(211, 40);
            this.lblPlateType_2.TabIndex = 15;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRRR,
            this.mnuRRT,
            this.mnuNRR,
            this.mnuNRT,
            this.mnuHELD,
            this.mnuSpace,
            this.mnuCORR,
            this.mnuDUP,
            this.mnuLOST,
            this.mnuDUPSTK,
            this.mnuGVW,
            this.mnuSpace2,
            this.mnuTRANSIT,
            this.mnuBOOSTER,
            this.mnuSPECIAL,
            this.mnuSpace3,
            this.mnuVOID,
            this.mnuUPDATE,
            this.mnuREDBOOK,
            this.mnuSeperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuRRR
            // 
            this.mnuRRR.Index = 0;
            this.mnuRRR.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRRRSame,
            this.mnuRRRNew,
            this.mnuRRROld});
            this.mnuRRR.Name = "mnuRRR";
            this.mnuRRR.Text = "Re-Reg Regular";
            this.mnuRRR.Visible = false;
            // 
            // mnuRRRSame
            // 
            this.mnuRRRSame.Index = 0;
            this.mnuRRRSame.Name = "mnuRRRSame";
            this.mnuRRRSame.Text = "Same Plate";
            this.mnuRRRSame.Click += new System.EventHandler(this.mnuRRRSame_Click);
            // 
            // mnuRRRNew
            // 
            this.mnuRRRNew.Index = 1;
            this.mnuRRRNew.Name = "mnuRRRNew";
            this.mnuRRRNew.Text = "New Plate";
            this.mnuRRRNew.Click += new System.EventHandler(this.mnuRRRNew_Click);
            // 
            // mnuRRROld
            // 
            this.mnuRRROld.Index = 2;
            this.mnuRRROld.Name = "mnuRRROld";
            this.mnuRRROld.Text = "Old Plate";
            this.mnuRRROld.Click += new System.EventHandler(this.mnuRRROld_Click);
            // 
            // mnuRRT
            // 
            this.mnuRRT.Index = 1;
            this.mnuRRT.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRRTTransferPlate,
            this.mnuRRTReRegPlate,
            this.mnuRRTNewPlate,
            this.mnuRRTOldPlate});
            this.mnuRRT.Name = "mnuRRT";
            this.mnuRRT.Text = "Re-Reg Transfer";
            this.mnuRRT.Visible = false;
            // 
            // mnuRRTTransferPlate
            // 
            this.mnuRRTTransferPlate.Index = 0;
            this.mnuRRTTransferPlate.Name = "mnuRRTTransferPlate";
            this.mnuRRTTransferPlate.Text = "Plate From Transfer Vehicle";
            this.mnuRRTTransferPlate.Click += new System.EventHandler(this.mnuRRTTransferPlate_Click);
            // 
            // mnuRRTReRegPlate
            // 
            this.mnuRRTReRegPlate.Index = 1;
            this.mnuRRTReRegPlate.Name = "mnuRRTReRegPlate";
            this.mnuRRTReRegPlate.Text = "Plate From Re-Reg Vehicle";
            this.mnuRRTReRegPlate.Click += new System.EventHandler(this.mnuRRTReRegPlate_Click);
            // 
            // mnuRRTNewPlate
            // 
            this.mnuRRTNewPlate.Index = 2;
            this.mnuRRTNewPlate.Name = "mnuRRTNewPlate";
            this.mnuRRTNewPlate.Text = "New Plate";
            this.mnuRRTNewPlate.Click += new System.EventHandler(this.mnuRRTNewPlate_Click);
            // 
            // mnuRRTOldPlate
            // 
            this.mnuRRTOldPlate.Index = 3;
            this.mnuRRTOldPlate.Name = "mnuRRTOldPlate";
            this.mnuRRTOldPlate.Text = "Old Plate";
            this.mnuRRTOldPlate.Click += new System.EventHandler(this.mnuRRTOldPlate_Click);
            // 
            // mnuNRR
            // 
            this.mnuNRR.Index = 2;
            this.mnuNRR.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNRRNewPlate,
            this.mnuNRROldPlate,
            this.mnuNRRReplacementPlate});
            this.mnuNRR.Name = "mnuNRR";
            this.mnuNRR.Text = "New Reg Regular";
            this.mnuNRR.Visible = false;
            // 
            // mnuNRRNewPlate
            // 
            this.mnuNRRNewPlate.Index = 0;
            this.mnuNRRNewPlate.Name = "mnuNRRNewPlate";
            this.mnuNRRNewPlate.Text = "New Plate";
            this.mnuNRRNewPlate.Click += new System.EventHandler(this.mnuNRRNewPlate_Click);
            // 
            // mnuNRROldPlate
            // 
            this.mnuNRROldPlate.Index = 1;
            this.mnuNRROldPlate.Name = "mnuNRROldPlate";
            this.mnuNRROldPlate.Text = "Old Plate";
            this.mnuNRROldPlate.Click += new System.EventHandler(this.mnuNRROldPlate_Click);
            // 
            // mnuNRRReplacementPlate
            // 
            this.mnuNRRReplacementPlate.Index = 2;
            this.mnuNRRReplacementPlate.Name = "mnuNRRReplacementPlate";
            this.mnuNRRReplacementPlate.Text = "Replacement Plate";
            this.mnuNRRReplacementPlate.Click += new System.EventHandler(this.mnuNRRReplacementPlate_Click);
            // 
            // mnuNRT
            // 
            this.mnuNRT.Index = 3;
            this.mnuNRT.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNRTTransferPlate,
            this.mnuNRTNewClass,
            this.mnuNRTNewLost,
            this.mnuNRTOldPlate});
            this.mnuNRT.Name = "mnuNRT";
            this.mnuNRT.Text = "New Reg Transfer";
            this.mnuNRT.Visible = false;
            // 
            // mnuNRTTransferPlate
            // 
            this.mnuNRTTransferPlate.Index = 0;
            this.mnuNRTTransferPlate.Name = "mnuNRTTransferPlate";
            this.mnuNRTTransferPlate.Text = "Plate From Transfer Vehicle";
            this.mnuNRTTransferPlate.Click += new System.EventHandler(this.mnuNRTTransferPlate_Click);
            // 
            // mnuNRTNewClass
            // 
            this.mnuNRTNewClass.Index = 1;
            this.mnuNRTNewClass.Name = "mnuNRTNewClass";
            this.mnuNRTNewClass.Text = "New Plate - Class Change";
            this.mnuNRTNewClass.Click += new System.EventHandler(this.mnuNRTNewClass_Click);
            // 
            // mnuNRTNewLost
            // 
            this.mnuNRTNewLost.Index = 2;
            this.mnuNRTNewLost.Name = "mnuNRTNewLost";
            this.mnuNRTNewLost.Text = "New Plate - Lost";
            this.mnuNRTNewLost.Click += new System.EventHandler(this.mnuNRTNewLost_Click);
            // 
            // mnuNRTOldPlate
            // 
            this.mnuNRTOldPlate.Index = 3;
            this.mnuNRTOldPlate.Name = "mnuNRTOldPlate";
            this.mnuNRTOldPlate.Text = "Old Plate";
            this.mnuNRTOldPlate.Click += new System.EventHandler(this.mnuNRTOldPlate_Click);
            // 
            // mnuHELD
            // 
            this.mnuHELD.Index = 4;
            this.mnuHELD.Name = "mnuHELD";
            this.mnuHELD.Text = "Complete Held Registration";
            this.mnuHELD.Visible = false;
            this.mnuHELD.Click += new System.EventHandler(this.mnuHELD_Click);
            // 
            // mnuSpace
            // 
            this.mnuSpace.Index = 5;
            this.mnuSpace.Name = "mnuSpace";
            this.mnuSpace.Text = "-";
            this.mnuSpace.Visible = false;
            // 
            // mnuCORR
            // 
            this.mnuCORR.Index = 6;
            this.mnuCORR.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuCORRwoPlateChange,
            this.mnuCORRPlateChange});
            this.mnuCORR.Name = "mnuCORR";
            this.mnuCORR.Text = "Corrections";
            this.mnuCORR.Visible = false;
            // 
            // mnuCORRwoPlateChange
            // 
            this.mnuCORRwoPlateChange.Index = 0;
            this.mnuCORRwoPlateChange.Name = "mnuCORRwoPlateChange";
            this.mnuCORRwoPlateChange.Text = "without Plate Change";
            this.mnuCORRwoPlateChange.Click += new System.EventHandler(this.mnuCORRwoPlateChange_Click);
            // 
            // mnuCORRPlateChange
            // 
            this.mnuCORRPlateChange.Index = 1;
            this.mnuCORRPlateChange.Name = "mnuCORRPlateChange";
            this.mnuCORRPlateChange.Text = "with Plate Change";
            this.mnuCORRPlateChange.Click += new System.EventHandler(this.mnuCORRPlateChange_Click);
            // 
            // mnuDUP
            // 
            this.mnuDUP.Index = 7;
            this.mnuDUP.Name = "mnuDUP";
            this.mnuDUP.Text = "Duplicate Registration";
            this.mnuDUP.Visible = false;
            this.mnuDUP.Click += new System.EventHandler(this.mnuDUP_Click);
            // 
            // mnuLOST
            // 
            this.mnuLOST.Index = 8;
            this.mnuLOST.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuLOSTNewPlate,
            this.mnuLOSTOldPlate});
            this.mnuLOST.Name = "mnuLOST";
            this.mnuLOST.Text = "Lost Plate / Stickers";
            this.mnuLOST.Visible = false;
            // 
            // mnuLOSTNewPlate
            // 
            this.mnuLOSTNewPlate.Index = 0;
            this.mnuLOSTNewPlate.Name = "mnuLOSTNewPlate";
            this.mnuLOSTNewPlate.Text = "New Plate";
            this.mnuLOSTNewPlate.Click += new System.EventHandler(this.mnuLOSTNewPlate_Click);
            // 
            // mnuLOSTOldPlate
            // 
            this.mnuLOSTOldPlate.Index = 1;
            this.mnuLOSTOldPlate.Name = "mnuLOSTOldPlate";
            this.mnuLOSTOldPlate.Text = "Old Plate";
            this.mnuLOSTOldPlate.Click += new System.EventHandler(this.mnuLOSTOldPlate_Click);
            // 
            // mnuDUPSTK
            // 
            this.mnuDUPSTK.Index = 9;
            this.mnuDUPSTK.Name = "mnuDUPSTK";
            this.mnuDUPSTK.Text = "Duplicate Stickers";
            this.mnuDUPSTK.Visible = false;
            this.mnuDUPSTK.Click += new System.EventHandler(this.mnuDUPSTK_Click);
            // 
            // mnuGVW
            // 
            this.mnuGVW.Index = 10;
            this.mnuGVW.Name = "mnuGVW";
            this.mnuGVW.Text = "RVW Change";
            this.mnuGVW.Visible = false;
            this.mnuGVW.Click += new System.EventHandler(this.mnuGVW_Click);
            // 
            // mnuSpace2
            // 
            this.mnuSpace2.Index = 11;
            this.mnuSpace2.Name = "mnuSpace2";
            this.mnuSpace2.Text = "-";
            this.mnuSpace2.Visible = false;
            // 
            // mnuTRANSIT
            // 
            this.mnuTRANSIT.Index = 12;
            this.mnuTRANSIT.Name = "mnuTRANSIT";
            this.mnuTRANSIT.Text = "Transit Plate";
            this.mnuTRANSIT.Visible = false;
            this.mnuTRANSIT.Click += new System.EventHandler(this.mnuTRANSIT_Click);
            // 
            // mnuBOOSTER
            // 
            this.mnuBOOSTER.Index = 13;
            this.mnuBOOSTER.Name = "mnuBOOSTER";
            this.mnuBOOSTER.Text = "Booster";
            this.mnuBOOSTER.Visible = false;
            this.mnuBOOSTER.Click += new System.EventHandler(this.mnuBOOSTER_Click);
            // 
            // mnuSPECIAL
            // 
            this.mnuSPECIAL.Index = 14;
            this.mnuSPECIAL.Name = "mnuSPECIAL";
            this.mnuSPECIAL.Text = "Special Reg Permit";
            this.mnuSPECIAL.Visible = false;
            this.mnuSPECIAL.Click += new System.EventHandler(this.mnuSPECIAL_Click);
            // 
            // mnuSpace3
            // 
            this.mnuSpace3.Index = 15;
            this.mnuSpace3.Name = "mnuSpace3";
            this.mnuSpace3.Text = "-";
            this.mnuSpace3.Visible = false;
            // 
            // mnuVOID
            // 
            this.mnuVOID.Index = 16;
            this.mnuVOID.Name = "mnuVOID";
            this.mnuVOID.Text = "Void MVR3 - Return Stickers";
            this.mnuVOID.Visible = false;
            this.mnuVOID.Click += new System.EventHandler(this.mnuVOID_Click);
            // 
            // mnuUPDATE
            // 
            this.mnuUPDATE.Index = 17;
            this.mnuUPDATE.Name = "mnuUPDATE";
            this.mnuUPDATE.Text = "New To System / Update";
            this.mnuUPDATE.Visible = false;
            this.mnuUPDATE.Click += new System.EventHandler(this.mnuUPDATE_Click);
            // 
            // mnuREDBOOK
            // 
            this.mnuREDBOOK.Index = 18;
            this.mnuREDBOOK.Name = "mnuREDBOOK";
            this.mnuREDBOOK.Text = "Blue Book";
            this.mnuREDBOOK.Visible = false;
            this.mnuREDBOOK.Click += new System.EventHandler(this.mnuREDBOOK_Click);
            // 
            // mnuSeperator
            // 
            this.mnuSeperator.Index = 19;
            this.mnuSeperator.Name = "mnuSeperator";
            this.mnuSeperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 20;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuSearch
            // 
            this.mnuSearch.Enabled = false;
            this.mnuSearch.Index = -1;
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuSearch.Text = "Search";
            this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // mnuSearchSeperator
            // 
            this.mnuSearchSeperator.Index = -1;
            this.mnuSearchSeperator.Name = "mnuSearchSeperator";
            this.mnuSearchSeperator.Text = "-";
            this.mnuSearchSeperator.Visible = false;
            // 
            // lblRegType
            // 
            this.lblRegType.BackColor = System.Drawing.Color.Transparent;
            this.lblRegType.Location = new System.Drawing.Point(235, 80);
            this.lblRegType.Name = "lblRegType";
            this.lblRegType.Size = new System.Drawing.Size(337, 15);
            this.lblRegType.TabIndex = 3;
            // 
            // lblRegType1
            // 
            this.lblRegType1.AutoSize = true;
            this.lblRegType1.Location = new System.Drawing.Point(30, 119);
            this.lblRegType1.Name = "lblRegType1";
            this.lblRegType1.Size = new System.Drawing.Size(105, 15);
            this.lblRegType1.TabIndex = 2;
            this.lblRegType1.Text = "PROCESS TYPE";
            // 
            // lblPlate
            // 
            this.lblPlate.AutoSize = true;
            this.lblPlate.Location = new System.Drawing.Point(30, 169);
            this.lblPlate.Name = "lblPlate";
            this.lblPlate.Size = new System.Drawing.Size(203, 15);
            this.lblPlate.TabIndex = 5;
            this.lblPlate.Text = "LICENSE PLATE TO BE USED IS";
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSearch.Enabled = false;
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(1008, 29);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdSearch.TabIndex = 1;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // frmPlateInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1088, 700);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPlateInfo";
            this.Text = "Registration Menu";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmPlateInfo_Load);
            this.Activated += new System.EventHandler(this.frmPlateInfo_Activated);
            this.Resize += new System.EventHandler(this.frmPlateInfo_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPlateInfo_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPlateInfo_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFleetRegistration)).EndInit();
            this.fraFleetRegistration.ResumeLayout(false);
            this.fraFleetRegistration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRegister)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRegisterNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreRegister)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraResults)).EndInit();
            this.fraResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelection)).EndInit();
            this.fraSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartOver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExciseTaxPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateType_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateType_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateType_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSubClass)).EndInit();
            this.fraSubClass.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCLabel lblRegType1;
		public FCLabel lblPlate;
		private FCButton cmdSearch;
		private FCDateTimePicker txtEffectiveDate;
	}
}
