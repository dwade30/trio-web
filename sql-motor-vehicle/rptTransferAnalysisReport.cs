﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTransferAnalysisReport.
	/// </summary>
	public partial class rptTransferAnalysisReport : BaseSectionReport
	{
		public rptTransferAnalysisReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Transfer Analysis Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptTransferAnalysisReport InstancePtr
		{
			get
			{
				return (rptTransferAnalysisReport)Sys.GetInstance(typeof(rptTransferAnalysisReport));
			}
		}

		protected rptTransferAnalysisReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTransferAnalysisReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldExciseFirstYear.Text = frmTransferAnalysis.InstancePtr.lblExciseTax1Year.Text;
			fldRegistrationFirstYear.Text = frmTransferAnalysis.InstancePtr.lblRegistrationTax1Year.Text;
			fldLocalFirstYear.Text = frmTransferAnalysis.InstancePtr.lblLocalTax1Year.Text;
			fldExciseCredit.Text = frmTransferAnalysis.InstancePtr.lblExciseTaxCredit.Text;
			fldRegistrationCredit.Text = frmTransferAnalysis.InstancePtr.lblRegistrationCredit.Text;
			fldLocalSecondYear.Text = frmTransferAnalysis.InstancePtr.lblLocalTax2Year.Text;
			fldExciseSecondYear.Text = frmTransferAnalysis.InstancePtr.lblExciseTax2Year.Text;
			fldRegistrationTransfer.Text = frmTransferAnalysis.InstancePtr.lblRegistrationTransfer.Text;
			fldLocalTotal.Text = frmTransferAnalysis.InstancePtr.lblLocalTotal.Text;
			fldExciseTotal.Text = frmTransferAnalysis.InstancePtr.lblExciseTaxTotal.Text;
			fldRegistrationSecondYear.Text = frmTransferAnalysis.InstancePtr.lblRegistrationTax2Year.Text;
			fldRegistrationTotal.Text = frmTransferAnalysis.InstancePtr.lblRegistrationTotal.Text;
			fldTransferExcise.Text = frmTransferAnalysis.InstancePtr.lblExciseT.Text;
			fldRegExcise.Text = frmTransferAnalysis.InstancePtr.lblExciseNR.Text;
			fldTransferRegistration.Text = frmTransferAnalysis.InstancePtr.lblRegT.Text;
			fldRegRegistration.Text = frmTransferAnalysis.InstancePtr.lblRegNR.Text;
			fldTransferLocal.Text = frmTransferAnalysis.InstancePtr.lblLocalT.Text;
			fldRegLocal.Text = frmTransferAnalysis.InstancePtr.lblLocalNR.Text;
			fldTransferTotal.Text = frmTransferAnalysis.InstancePtr.lblTotalT.Text;
			fldRegTotal.Text = frmTransferAnalysis.InstancePtr.lblTotalNR.Text;
			fldTransferMonths.Text = frmTransferAnalysis.InstancePtr.lblMonthsT.Text;
			fldRegMonths.Text = frmTransferAnalysis.InstancePtr.lblMonthsNR.Text;
			fldTransferPerMonth.Text = frmTransferAnalysis.InstancePtr.lblPerMonthT.Text;
			fldRegPerMonth.Text = frmTransferAnalysis.InstancePtr.lblPerMonthNR.Text;
			fldTransferPayToday.Text = frmTransferAnalysis.InstancePtr.lblPayTodayT.Text;
			fldRegPayToday.Text = frmTransferAnalysis.InstancePtr.lblPayTodayNR.Text;
			fldTransferPayLater.Text = frmTransferAnalysis.InstancePtr.lblPayLaterT.Text;
			fldRegPayLater.Text = frmTransferAnalysis.InstancePtr.lblPayLaterNR.Text;
			txtTransferCommentLine.Font = new Font("Courier New", txtTransferCommentLine.Font.Size);
			txtTransferCommentLine.Font = new Font(txtTransferCommentLine.Font.Name, 10);
			//FC:FINAL:DSE WordWrapping not working in RichTextBox
			//txtTransferCommentLine.Text = MotorVehicle.GetMVVariable("TransferCommentLine");
			txtTransferCommentLine.SetHtmlText(MotorVehicle.GetMVVariable("TransferCommentLine"));
			if (Strings.Left(MotorVehicle.Statics.RegistrationType, 1) == "N")
			{
				lblRegType.Text = "New Reg";
			}
			else
			{
				lblRegType.Text = "Re Reg";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label10.Text = "Page " + this.PageNumber;
		}

		private void rptTransferAnalysisReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTransferAnalysisReport properties;
			//rptTransferAnalysisReport.Caption	= "Transfer Analysis Report";
			//rptTransferAnalysisReport.Icon	= "rptTransferAnalysisReport.dsx":0000";
			//rptTransferAnalysisReport.Left	= 0;
			//rptTransferAnalysisReport.Top	= 0;
			//rptTransferAnalysisReport.Width	= 11880;
			//rptTransferAnalysisReport.Height	= 8595;
			//rptTransferAnalysisReport.StartUpPosition	= 3;
			//rptTransferAnalysisReport.SectionData	= "rptTransferAnalysisReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
