//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmVehicleUpdate.
	/// </summary>
	partial class frmVehicleUpdate
	{
		public fecherFoundation.FCComboBox cmbtNo;
		public fecherFoundation.FCLabel lbltNo;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label31;
		public fecherFoundation.FCFrame fraAdditionalInfo;
		public fecherFoundation.FCCheckBox chkOverrideFleetEmail;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCComboBox cboMessageCodes;
		public fecherFoundation.FCButton cmdErase;
		public Global.T2KOverTypeBox txtMessage;
		public fecherFoundation.FCLabel lblMesageCode;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCFrame fraUserFields;
		public Global.T2KOverTypeBox txtUserReason1;
		public Global.T2KOverTypeBox txtUserField1;
		public Global.T2KOverTypeBox txtUserField2;
		public Global.T2KOverTypeBox txtUserField3;
		public Global.T2KOverTypeBox txtUserReason2;
		public Global.T2KOverTypeBox txtUserReason3;
		public fecherFoundation.FCLabel lblUserField1;
		public fecherFoundation.FCLabel lblUserField2;
		public fecherFoundation.FCLabel lblUserField3;
		public fecherFoundation.FCLabel lblInfo1;
		public fecherFoundation.FCLabel lblInfo2;
		public fecherFoundation.FCLabel lblInfo3;
		public Global.T2KOverTypeBox txtEMail;
		public fecherFoundation.FCLabel lblEMail;
		public fecherFoundation.FCPanel Frame1;
		public fecherFoundation.FCTextBox txtFleetNumber;
		public fecherFoundation.FCPictureBox imgFleetComment;
		public fecherFoundation.FCTextBox txtReg3PartyID;
		public fecherFoundation.FCTextBox txtReg2PartyID;
		public fecherFoundation.FCTextBox txtReg1PartyID;
		public fecherFoundation.FCTextBox txtResCountry;
		public fecherFoundation.FCTextBox txtResState;
		public fecherFoundation.FCTextBox txtReg2LR;
		public fecherFoundation.FCTextBox txtReg1LR;
		public fecherFoundation.FCTextBox txtReg3LR;
		public fecherFoundation.FCComboBox cboSubClass;
		public fecherFoundation.FCComboBox cboClass;
		public Global.T2KOverTypeBox txtMVR3;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtReg3ICM;
		public fecherFoundation.FCTextBox txtReg1ICM;
		public fecherFoundation.FCTextBox txtReg2ICM;
		public fecherFoundation.FCTextBox txtNetWeight;
		public fecherFoundation.FCTextBox txtRegWeight;
		public fecherFoundation.FCTextBox txtFuel;
		public fecherFoundation.FCTextBox txtMileage;
		public fecherFoundation.FCFrame FraSystemSettings;
		public fecherFoundation.FCLabel LblForced;
		public fecherFoundation.FCLabel lblBattle;
		public fecherFoundation.FCLabel LblExciseExempt;
		public fecherFoundation.FCLabel LblAgentExempt;
		public fecherFoundation.FCLabel LblStateExempt;
		public fecherFoundation.FCLabel LblTransferExempt;
		public fecherFoundation.FCLabel txtBattle;
		public fecherFoundation.FCLabel txtAgentExempt;
		public fecherFoundation.FCLabel txtTransferExempt;
		public fecherFoundation.FCLabel txtStateExempt;
		public fecherFoundation.FCLabel txtExciseExempt;
		public fecherFoundation.FCLabel txtForced;
		public fecherFoundation.FCTextBox txtMonthStickerNumber;
		public fecherFoundation.FCTextBox txtYearStickerNumber;
		public fecherFoundation.FCFrame Frame2;
		public Global.T2KBackFillDecimal txtPermit;
		public Global.T2KBackFillDecimal txtBoosterEffective;
		public Global.T2KBackFillDecimal txtBoosterExpiration;
		public Global.T2KOverTypeBox txtBoosterWeight;
		public fecherFoundation.FCLabel Label31_14;
		public fecherFoundation.FCLabel Label31_13;
		public fecherFoundation.FCLabel Label31_12;
		public fecherFoundation.FCLabel Label31_11;
		public Global.T2KOverTypeBox txtVin;
		public Global.T2KOverTypeBox txtYear;
		public Global.T2KOverTypeBox txtMake;
		public Global.T2KOverTypeBox txtModel;
		public Global.T2KOverTypeBox txtColor1;
		public Global.T2KOverTypeBox txtColor2;
		public Global.T2KOverTypeBox txtStyle;
		public Global.T2KOverTypeBox txtTires;
		public Global.T2KOverTypeBox txtAxles;
		public Global.T2KOverTypeBox txtRegistrationType;
		public Global.T2KOverTypeBox txtStatus;
		public Global.T2KDateBox txtExpireDate;
		public Global.T2KDateBox txtExciseTaxDate;
		public Global.T2KOverTypeBox txtPlate;
		public Global.T2KDateBox txtReg2DOB;
		public Global.T2KDateBox txtReg1DOB;
		public Global.T2KOverTypeBox txtUnitNumber;
		public Global.T2KOverTypeBox txtDOTNumber;
		public Global.T2KOverTypeBox txtCity;
		public Global.T2KOverTypeBox txtAddress1;
		public Global.T2KOverTypeBox txtLegalres;
		public Global.T2KOverTypeBox txtresidenceCode;
		public Global.T2KOverTypeBox txtZip;
		public Global.T2KDateBox txtReg3DOB;
		public Global.T2KOverTypeBox txtAddress2;
		public Global.T2KOverTypeBox txtCountry;
		public Global.T2KOverTypeBox txtLegalResStreet;
		public Global.T2KOverTypeBox txtTaxIDNumber;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel lblRegistrant3;
		public fecherFoundation.FCLabel lblRegistrant2;
		public fecherFoundation.FCLabel lblRegistrant1;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label38;
		public fecherFoundation.FCLabel Label47;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label33;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label44;
		public fecherFoundation.FCLabel Label45;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel lblNumberofYear;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblMVR3;
		public fecherFoundation.FCLabel lblYSD;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		//public fecherFoundation.FCLabel Label41;
		public fecherFoundation.FCLabel Label34;
		public fecherFoundation.FCButton cmdFleetGroupComment;
		public fecherFoundation.FCButton cmdProcessSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVehicleUpdate));
            this.cmbtNo = new fecherFoundation.FCComboBox();
            this.lbltNo = new fecherFoundation.FCLabel();
            this.fraAdditionalInfo = new fecherFoundation.FCFrame();
            this.fraUserFields = new fecherFoundation.FCFrame();
            this.txtUserReason1 = new Global.T2KOverTypeBox();
            this.txtUserField1 = new Global.T2KOverTypeBox();
            this.txtUserField2 = new Global.T2KOverTypeBox();
            this.txtUserField3 = new Global.T2KOverTypeBox();
            this.txtUserReason2 = new Global.T2KOverTypeBox();
            this.txtUserReason3 = new Global.T2KOverTypeBox();
            this.lblUserField1 = new fecherFoundation.FCLabel();
            this.lblUserField2 = new fecherFoundation.FCLabel();
            this.lblUserField3 = new fecherFoundation.FCLabel();
            this.lblInfo1 = new fecherFoundation.FCLabel();
            this.lblInfo2 = new fecherFoundation.FCLabel();
            this.lblInfo3 = new fecherFoundation.FCLabel();
            this.chkOverrideFleetEmail = new fecherFoundation.FCCheckBox();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.cboMessageCodes = new fecherFoundation.FCComboBox();
            this.cmdErase = new fecherFoundation.FCButton();
            this.txtMessage = new Global.T2KOverTypeBox();
            this.lblMesageCode = new fecherFoundation.FCLabel();
            this.lblMessage = new fecherFoundation.FCLabel();
            this.cmdDone = new fecherFoundation.FCButton();
            this.txtEMail = new Global.T2KOverTypeBox();
            this.lblEMail = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCPanel();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.imgEditRegistrant3 = new fecherFoundation.FCPictureBox();
            this.imgEditRegistrant2 = new fecherFoundation.FCPictureBox();
            this.imgEditRegistrant1 = new fecherFoundation.FCPictureBox();
            this.imgCopyToLegalAddress = new fecherFoundation.FCPictureBox();
            this.imgCopyToMailingAddress = new fecherFoundation.FCPictureBox();
            this.imgFleetGroupSearch = new fecherFoundation.FCPictureBox();
            this.imgReg3Edit = new fecherFoundation.FCPictureBox();
            this.imgReg3Search = new fecherFoundation.FCPictureBox();
            this.imgReg2Edit = new fecherFoundation.FCPictureBox();
            this.imgReg2Search = new fecherFoundation.FCPictureBox();
            this.imgReg1Edit = new fecherFoundation.FCPictureBox();
            this.imgReg1Search = new fecherFoundation.FCPictureBox();
            this.FraSystemSettings = new fecherFoundation.FCFrame();
            this.LblForced = new fecherFoundation.FCLabel();
            this.lblBattle = new fecherFoundation.FCLabel();
            this.LblExciseExempt = new fecherFoundation.FCLabel();
            this.LblAgentExempt = new fecherFoundation.FCLabel();
            this.LblStateExempt = new fecherFoundation.FCLabel();
            this.LblTransferExempt = new fecherFoundation.FCLabel();
            this.txtBattle = new fecherFoundation.FCLabel();
            this.txtAgentExempt = new fecherFoundation.FCLabel();
            this.txtTransferExempt = new fecherFoundation.FCLabel();
            this.txtStateExempt = new fecherFoundation.FCLabel();
            this.txtExciseExempt = new fecherFoundation.FCLabel();
            this.txtForced = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtPermit = new Global.T2KBackFillDecimal();
            this.txtBoosterEffective = new Global.T2KBackFillDecimal();
            this.txtBoosterExpiration = new Global.T2KBackFillDecimal();
            this.txtBoosterWeight = new Global.T2KOverTypeBox();
            this.Label31_14 = new fecherFoundation.FCLabel();
            this.Label31_13 = new fecherFoundation.FCLabel();
            this.Label31_12 = new fecherFoundation.FCLabel();
            this.Label31_11 = new fecherFoundation.FCLabel();
            this.txtLegalres = new Global.T2KOverTypeBox();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.txtCity = new Global.T2KOverTypeBox();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.Label48 = new fecherFoundation.FCLabel();
            this.Label52 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.imgFleetComment = new fecherFoundation.FCPictureBox();
            this.lblFleetName = new fecherFoundation.FCLabel();
            this.txtFleetNumber = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtReg3PartyID = new fecherFoundation.FCTextBox();
            this.lblFleet = new fecherFoundation.FCLabel();
            this.txtReg2PartyID = new fecherFoundation.FCTextBox();
            this.txtReg1PartyID = new fecherFoundation.FCTextBox();
            this.txtResCountry = new fecherFoundation.FCTextBox();
            this.txtResState = new fecherFoundation.FCTextBox();
            this.txtReg2LR = new fecherFoundation.FCTextBox();
            this.txtReg1LR = new fecherFoundation.FCTextBox();
            this.txtReg3LR = new fecherFoundation.FCTextBox();
            this.cboSubClass = new fecherFoundation.FCComboBox();
            this.cboClass = new fecherFoundation.FCComboBox();
            this.txtReg3ICM = new fecherFoundation.FCTextBox();
            this.txtReg1ICM = new fecherFoundation.FCTextBox();
            this.txtReg2ICM = new fecherFoundation.FCTextBox();
            this.txtNetWeight = new fecherFoundation.FCTextBox();
            this.txtRegWeight = new fecherFoundation.FCTextBox();
            this.txtFuel = new fecherFoundation.FCTextBox();
            this.txtMileage = new fecherFoundation.FCTextBox();
            this.txtMonthStickerNumber = new fecherFoundation.FCTextBox();
            this.txtYearStickerNumber = new fecherFoundation.FCTextBox();
            this.txtVin = new Global.T2KOverTypeBox();
            this.txtYear = new Global.T2KOverTypeBox();
            this.txtMake = new Global.T2KOverTypeBox();
            this.txtModel = new Global.T2KOverTypeBox();
            this.txtColor1 = new Global.T2KOverTypeBox();
            this.txtColor2 = new Global.T2KOverTypeBox();
            this.txtStyle = new Global.T2KOverTypeBox();
            this.txtTires = new Global.T2KOverTypeBox();
            this.txtAxles = new Global.T2KOverTypeBox();
            this.txtRegistrationType = new Global.T2KOverTypeBox();
            this.txtStatus = new Global.T2KOverTypeBox();
            this.txtExpireDate = new Global.T2KDateBox();
            this.txtEffectiveDate = new Global.T2KDateBox();
            this.txtExciseTaxDate = new Global.T2KDateBox();
            this.txtPlate = new Global.T2KOverTypeBox();
            this.txtReg2DOB = new Global.T2KDateBox();
            this.txtReg1DOB = new Global.T2KDateBox();
            this.txtUnitNumber = new Global.T2KOverTypeBox();
            this.txtDOTNumber = new Global.T2KOverTypeBox();
            this.txtAddress1 = new Global.T2KOverTypeBox();
            this.txtresidenceCode = new Global.T2KOverTypeBox();
            this.txtZip = new Global.T2KOverTypeBox();
            this.txtReg3DOB = new Global.T2KDateBox();
            this.txtAddress2 = new Global.T2KOverTypeBox();
            this.txtCountry = new Global.T2KOverTypeBox();
            this.txtLegalResStreet = new Global.T2KOverTypeBox();
            this.txtTaxIDNumber = new Global.T2KOverTypeBox();
            this.Label28 = new fecherFoundation.FCLabel();
            this.lblRegistrant3 = new fecherFoundation.FCLabel();
            this.lblRegistrant2 = new fecherFoundation.FCLabel();
            this.lblRegistrant1 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label38 = new fecherFoundation.FCLabel();
            this.Label47 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label33 = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Label29 = new fecherFoundation.FCLabel();
            this.Label27 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label44 = new fecherFoundation.FCLabel();
            this.Label45 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.lblNumberofYear = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblMVR3 = new fecherFoundation.FCLabel();
            this.lblYSD = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label34 = new fecherFoundation.FCLabel();
            this.Shape3 = new fecherFoundation.FCFrame();
            this.Label35 = new fecherFoundation.FCLabel();
            this.Label36 = new fecherFoundation.FCLabel();
            this.txtMilRate = new fecherFoundation.FCTextBox();
            this.Label37 = new fecherFoundation.FCLabel();
            this.txtAgentFee = new Global.T2KBackFillDecimal();
            this.lblExciseTax = new fecherFoundation.FCLabel();
            this.txtAmountOfTax = new Global.T2KBackFillDecimal();
            this.Label39 = new fecherFoundation.FCLabel();
            this.txtCredit = new Global.T2KBackFillDecimal();
            this.Label30 = new fecherFoundation.FCLabel();
            this.txtSubtotal = new Global.T2KBackFillDecimal();
            this.txtTransferCharge = new Global.T2KBackFillDecimal();
            this.Label32 = new fecherFoundation.FCLabel();
            this.txtExciseTaxBalance = new Global.T2KBackFillDecimal();
            this.Label43 = new fecherFoundation.FCLabel();
            this.txtCreditNumber = new fecherFoundation.FCTextBox();
            this.chkExciseHalfRate = new fecherFoundation.FCCheckBox();
            this.Label46 = new fecherFoundation.FCLabel();
            this.txtRegRate = new Global.T2KBackFillDecimal();
            this.lblStateCredit = new fecherFoundation.FCLabel();
            this.txtRegCredit = new Global.T2KBackFillDecimal();
            this.txtRBYear = new fecherFoundation.FCTextBox();
            this.lblFees = new fecherFoundation.FCLabel();
            this.txtRegFeeCharged = new Global.T2KBackFillDecimal();
            this.lblUseTax = new fecherFoundation.FCLabel();
            this.txtSalesTax = new Global.T2KBackFillDecimal();
            this.Label40 = new fecherFoundation.FCLabel();
            this.txtTitleFee = new Global.T2KBackFillDecimal();
            this.Label42 = new fecherFoundation.FCLabel();
            this.txtOpID = new fecherFoundation.FCTextBox();
            this.chkRegHalf = new fecherFoundation.FCCheckBox();
            this.txtPrice = new Global.T2KBackFillWhole();
            this.Label31_0 = new fecherFoundation.FCLabel();
            this.txtMVR3 = new Global.T2KOverTypeBox();
            this.cmdFleetGroupComment = new fecherFoundation.FCButton();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdMoreInfo = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).BeginInit();
            this.fraAdditionalInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).BeginInit();
            this.fraUserFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdErase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToLegalAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToMailingAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetGroupSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FraSystemSettings)).BeginInit();
            this.FraSystemSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoosterEffective)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoosterExpiration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoosterWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAxles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg2DOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg1DOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOTNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresidenceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg3DOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxIDNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Shape3)).BeginInit();
            this.Shape3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAgentFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountOfTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubtotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExciseHalfRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegFeeCharged)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegHalf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMVR3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFleetGroupComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 649);
            this.BottomPanel.Size = new System.Drawing.Size(994, 59);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraAdditionalInfo);
            this.ClientArea.Size = new System.Drawing.Size(1014, 518);
            this.ClientArea.Controls.SetChildIndex(this.fraAdditionalInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdMoreInfo);
            this.TopPanel.Controls.Add(this.cmdFleetGroupComment);
            this.TopPanel.Controls.SetChildIndex(this.cmdFleetGroupComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdMoreInfo, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(271, 28);
            this.HeaderText.Text = "New To System / Update ";
            // 
            // cmbtNo
            // 
            this.cmbtNo.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.cmbtNo.Location = new System.Drawing.Point(148, 30);
            this.cmbtNo.Name = "cmbtNo";
            this.cmbtNo.Size = new System.Drawing.Size(218, 22);
            this.cmbtNo.TabIndex = 185;
            this.cmbtNo.Text = "Yes";
            // 
            // lbltNo
            // 
            this.lbltNo.AutoSize = true;
            this.lbltNo.Location = new System.Drawing.Point(20, 34);
            this.lbltNo.Name = "lbltNo";
            this.lbltNo.Size = new System.Drawing.Size(107, 15);
            this.lbltNo.TabIndex = 186;
            this.lbltNo.Text = "MAIL REMINDER";
            // 
            // fraAdditionalInfo
            // 
            this.fraAdditionalInfo.BackColor = System.Drawing.Color.White;
            this.fraAdditionalInfo.Controls.Add(this.fraUserFields);
            this.fraAdditionalInfo.Controls.Add(this.chkOverrideFleetEmail);
            this.fraAdditionalInfo.Controls.Add(this.cmbtNo);
            this.fraAdditionalInfo.Controls.Add(this.lbltNo);
            this.fraAdditionalInfo.Controls.Add(this.fraMessage);
            this.fraAdditionalInfo.Controls.Add(this.cmdDone);
            this.fraAdditionalInfo.Controls.Add(this.txtEMail);
            this.fraAdditionalInfo.Controls.Add(this.lblEMail);
            this.fraAdditionalInfo.Location = new System.Drawing.Point(65, 72);
            this.fraAdditionalInfo.Name = "fraAdditionalInfo";
            this.fraAdditionalInfo.Size = new System.Drawing.Size(578, 577);
            this.fraAdditionalInfo.TabIndex = 101;
            this.fraAdditionalInfo.Text = "Additional Information";
            this.fraAdditionalInfo.Visible = false;
            // 
            // fraUserFields
            // 
            this.fraUserFields.BackColor = System.Drawing.Color.White;
            this.fraUserFields.Controls.Add(this.txtUserReason1);
            this.fraUserFields.Controls.Add(this.txtUserField1);
            this.fraUserFields.Controls.Add(this.txtUserField2);
            this.fraUserFields.Controls.Add(this.txtUserField3);
            this.fraUserFields.Controls.Add(this.txtUserReason2);
            this.fraUserFields.Controls.Add(this.txtUserReason3);
            this.fraUserFields.Controls.Add(this.lblUserField1);
            this.fraUserFields.Controls.Add(this.lblUserField2);
            this.fraUserFields.Controls.Add(this.lblUserField3);
            this.fraUserFields.Controls.Add(this.lblInfo1);
            this.fraUserFields.Controls.Add(this.lblInfo2);
            this.fraUserFields.Controls.Add(this.lblInfo3);
            this.fraUserFields.Location = new System.Drawing.Point(20, 255);
            this.fraUserFields.Name = "fraUserFields";
            this.fraUserFields.Size = new System.Drawing.Size(538, 257);
            this.fraUserFields.TabIndex = 88;
            this.fraUserFields.Text = "User Defined Fields";
            this.fraUserFields.Visible = false;
            // 
            // txtUserReason1
            // 
            this.txtUserReason1.Cursor = Wisej.Web.Cursors.Default;
            this.txtUserReason1.Location = new System.Drawing.Point(135, 67);
            this.txtUserReason1.Name = "txtUserReason1";
            this.txtUserReason1.Size = new System.Drawing.Size(383, 22);
            this.txtUserReason1.TabIndex = 80;
            this.txtUserReason1.Visible = false;
            // 
            // txtUserField1
            // 
            this.txtUserField1.Cursor = Wisej.Web.Cursors.Default;
            this.txtUserField1.Location = new System.Drawing.Point(135, 30);
            this.txtUserField1.Name = "txtUserField1";
            this.txtUserField1.Size = new System.Drawing.Size(383, 22);
            this.txtUserField1.TabIndex = 79;
            this.txtUserField1.Visible = false;
            // 
            // txtUserField2
            // 
            this.txtUserField2.Cursor = Wisej.Web.Cursors.Default;
            this.txtUserField2.Location = new System.Drawing.Point(135, 104);
            this.txtUserField2.Name = "txtUserField2";
            this.txtUserField2.Size = new System.Drawing.Size(383, 22);
            this.txtUserField2.TabIndex = 81;
            this.txtUserField2.Visible = false;
            // 
            // txtUserField3
            // 
            this.txtUserField3.Cursor = Wisej.Web.Cursors.Default;
            this.txtUserField3.Location = new System.Drawing.Point(135, 182);
            this.txtUserField3.Name = "txtUserField3";
            this.txtUserField3.Size = new System.Drawing.Size(383, 22);
            this.txtUserField3.TabIndex = 83;
            this.txtUserField3.Visible = false;
            // 
            // txtUserReason2
            // 
            this.txtUserReason2.Cursor = Wisej.Web.Cursors.Default;
            this.txtUserReason2.Location = new System.Drawing.Point(135, 143);
            this.txtUserReason2.Name = "txtUserReason2";
            this.txtUserReason2.Size = new System.Drawing.Size(383, 22);
            this.txtUserReason2.TabIndex = 82;
            this.txtUserReason2.Visible = false;
            // 
            // txtUserReason3
            // 
            this.txtUserReason3.Cursor = Wisej.Web.Cursors.Default;
            this.txtUserReason3.Location = new System.Drawing.Point(135, 220);
            this.txtUserReason3.Name = "txtUserReason3";
            this.txtUserReason3.Size = new System.Drawing.Size(383, 22);
            this.txtUserReason3.TabIndex = 84;
            this.txtUserReason3.Visible = false;
            // 
            // lblUserField1
            // 
            this.lblUserField1.Location = new System.Drawing.Point(20, 33);
            this.lblUserField1.Name = "lblUserField1";
            this.lblUserField1.Size = new System.Drawing.Size(122, 20);
            this.lblUserField1.TabIndex = 94;
            this.lblUserField1.Visible = false;
            // 
            // lblUserField2
            // 
            this.lblUserField2.Location = new System.Drawing.Point(20, 107);
            this.lblUserField2.Name = "lblUserField2";
            this.lblUserField2.Size = new System.Drawing.Size(122, 20);
            this.lblUserField2.TabIndex = 93;
            this.lblUserField2.Visible = false;
            // 
            // lblUserField3
            // 
            this.lblUserField3.Location = new System.Drawing.Point(20, 185);
            this.lblUserField3.Name = "lblUserField3";
            this.lblUserField3.Size = new System.Drawing.Size(122, 20);
            this.lblUserField3.TabIndex = 92;
            this.lblUserField3.Visible = false;
            // 
            // lblInfo1
            // 
            this.lblInfo1.Location = new System.Drawing.Point(20, 70);
            this.lblInfo1.Name = "lblInfo1";
            this.lblInfo1.Size = new System.Drawing.Size(92, 16);
            this.lblInfo1.TabIndex = 91;
            this.lblInfo1.Text = "INFORMATION";
            this.lblInfo1.Visible = false;
            // 
            // lblInfo2
            // 
            this.lblInfo2.Location = new System.Drawing.Point(20, 146);
            this.lblInfo2.Name = "lblInfo2";
            this.lblInfo2.Size = new System.Drawing.Size(92, 16);
            this.lblInfo2.TabIndex = 90;
            this.lblInfo2.Text = "INFORMATION";
            this.lblInfo2.Visible = false;
            // 
            // lblInfo3
            // 
            this.lblInfo3.Location = new System.Drawing.Point(20, 223);
            this.lblInfo3.Name = "lblInfo3";
            this.lblInfo3.Size = new System.Drawing.Size(92, 16);
            this.lblInfo3.TabIndex = 89;
            this.lblInfo3.Text = "INFORMATION";
            this.lblInfo3.Visible = false;
            // 
            // chkOverrideFleetEmail
            // 
            this.chkOverrideFleetEmail.Location = new System.Drawing.Point(384, 70);
            this.chkOverrideFleetEmail.Name = "chkOverrideFleetEmail";
            this.chkOverrideFleetEmail.Size = new System.Drawing.Size(150, 22);
            this.chkOverrideFleetEmail.TabIndex = 184;
            this.chkOverrideFleetEmail.TabStop = false;
            this.chkOverrideFleetEmail.Text = "Override Fleet Email";
            this.chkOverrideFleetEmail.Visible = false;
            this.chkOverrideFleetEmail.CheckedChanged += new System.EventHandler(this.chkOverrideFleetEmail_CheckedChanged);
            // 
            // fraMessage
            // 
            this.fraMessage.BackColor = System.Drawing.Color.White;
            this.fraMessage.Controls.Add(this.cboMessageCodes);
            this.fraMessage.Controls.Add(this.cmdErase);
            this.fraMessage.Controls.Add(this.txtMessage);
            this.fraMessage.Controls.Add(this.lblMesageCode);
            this.fraMessage.Controls.Add(this.lblMessage);
            this.fraMessage.Location = new System.Drawing.Point(20, 109);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(538, 116);
            this.fraMessage.TabIndex = 95;
            this.fraMessage.Text = "User Message";
            // 
            // cboMessageCodes
            // 
            this.cboMessageCodes.Items.AddRange(new object[] {
            "1 - Message ",
            "2 - Warning",
            "3 - Stop Process"});
            this.cboMessageCodes.Location = new System.Drawing.Point(143, 30);
            this.cboMessageCodes.Name = "cboMessageCodes";
            this.cboMessageCodes.Size = new System.Drawing.Size(196, 22);
            this.cboMessageCodes.TabIndex = 76;
            // 
            // cmdErase
            // 
            this.cmdErase.AppearanceKey = "actionButton";
            this.cmdErase.Cursor = Wisej.Web.Cursors.Default;
            this.cmdErase.Location = new System.Drawing.Point(359, 30);
            this.cmdErase.Name = "cmdErase";
            this.cmdErase.Size = new System.Drawing.Size(159, 40);
            this.cmdErase.TabIndex = 78;
            this.cmdErase.Text = "Erase Message";
            this.cmdErase.Click += new System.EventHandler(this.cmdErase_Click);
            // 
            // txtMessage
            // 
            this.txtMessage.Cursor = Wisej.Web.Cursors.Default;
            this.txtMessage.Location = new System.Drawing.Point(143, 80);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(375, 22);
            this.txtMessage.TabIndex = 77;
            // 
            // lblMesageCode
            // 
            this.lblMesageCode.Location = new System.Drawing.Point(20, 34);
            this.lblMesageCode.Name = "lblMesageCode";
            this.lblMesageCode.TabIndex = 97;
            this.lblMesageCode.Text = "MESSAGE CODE";
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(20, 83);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(81, 16);
            this.lblMessage.TabIndex = 96;
            this.lblMessage.Text = "MESSAGE";
            // 
            // cmdDone
            // 
            this.cmdDone.AppearanceKey = "actionButton";
            this.cmdDone.Cursor = Wisej.Web.Cursors.Default;
            this.cmdDone.Location = new System.Drawing.Point(20, 528);
            this.cmdDone.Name = "cmdDone";
            this.cmdDone.Size = new System.Drawing.Size(68, 40);
            this.cmdDone.TabIndex = 85;
            this.cmdDone.Text = "OK";
            this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
            // 
            // txtEMail
            // 
            this.txtEMail.Cursor = Wisej.Web.Cursors.Default;
            this.txtEMail.Location = new System.Drawing.Point(148, 64);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Size = new System.Drawing.Size(221, 22);
            this.txtEMail.TabIndex = 75;
            // 
            // lblEMail
            // 
            this.lblEMail.Location = new System.Drawing.Point(20, 67);
            this.lblEMail.Name = "lblEMail";
            this.lblEMail.Size = new System.Drawing.Size(106, 16);
            this.lblEMail.TabIndex = 99;
            this.lblEMail.Text = "E-MAIL ADDRESS";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.fcLabel5);
            this.Frame1.Controls.Add(this.fcLabel1);
            this.Frame1.Controls.Add(this.imgEditRegistrant3);
            this.Frame1.Controls.Add(this.imgEditRegistrant2);
            this.Frame1.Controls.Add(this.imgEditRegistrant1);
            this.Frame1.Controls.Add(this.imgCopyToLegalAddress);
            this.Frame1.Controls.Add(this.imgCopyToMailingAddress);
            this.Frame1.Controls.Add(this.imgFleetGroupSearch);
            this.Frame1.Controls.Add(this.imgReg3Edit);
            this.Frame1.Controls.Add(this.imgReg3Search);
            this.Frame1.Controls.Add(this.imgReg2Edit);
            this.Frame1.Controls.Add(this.imgReg2Search);
            this.Frame1.Controls.Add(this.imgReg1Edit);
            this.Frame1.Controls.Add(this.imgReg1Search);
            this.Frame1.Controls.Add(this.FraSystemSettings);
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.txtLegalres);
            this.Frame1.Controls.Add(this.fcLabel4);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.fcLabel3);
            this.Frame1.Controls.Add(this.Label48);
            this.Frame1.Controls.Add(this.Label52);
            this.Frame1.Controls.Add(this.fcLabel2);
            this.Frame1.Controls.Add(this.imgFleetComment);
            this.Frame1.Controls.Add(this.lblFleetName);
            this.Frame1.Controls.Add(this.txtFleetNumber);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtReg3PartyID);
            this.Frame1.Controls.Add(this.lblFleet);
            this.Frame1.Controls.Add(this.txtReg2PartyID);
            this.Frame1.Controls.Add(this.txtReg1PartyID);
            this.Frame1.Controls.Add(this.txtResCountry);
            this.Frame1.Controls.Add(this.txtResState);
            this.Frame1.Controls.Add(this.txtReg2LR);
            this.Frame1.Controls.Add(this.txtReg1LR);
            this.Frame1.Controls.Add(this.txtReg3LR);
            this.Frame1.Controls.Add(this.cboSubClass);
            this.Frame1.Controls.Add(this.cboClass);
            this.Frame1.Controls.Add(this.txtReg3ICM);
            this.Frame1.Controls.Add(this.txtReg1ICM);
            this.Frame1.Controls.Add(this.txtReg2ICM);
            this.Frame1.Controls.Add(this.txtNetWeight);
            this.Frame1.Controls.Add(this.txtRegWeight);
            this.Frame1.Controls.Add(this.txtFuel);
            this.Frame1.Controls.Add(this.txtMileage);
            this.Frame1.Controls.Add(this.txtMonthStickerNumber);
            this.Frame1.Controls.Add(this.txtYearStickerNumber);
            this.Frame1.Controls.Add(this.txtVin);
            this.Frame1.Controls.Add(this.txtYear);
            this.Frame1.Controls.Add(this.txtMake);
            this.Frame1.Controls.Add(this.txtModel);
            this.Frame1.Controls.Add(this.txtColor1);
            this.Frame1.Controls.Add(this.txtColor2);
            this.Frame1.Controls.Add(this.txtStyle);
            this.Frame1.Controls.Add(this.txtTires);
            this.Frame1.Controls.Add(this.txtAxles);
            this.Frame1.Controls.Add(this.txtRegistrationType);
            this.Frame1.Controls.Add(this.txtStatus);
            this.Frame1.Controls.Add(this.txtExpireDate);
            this.Frame1.Controls.Add(this.txtEffectiveDate);
            this.Frame1.Controls.Add(this.txtExciseTaxDate);
            this.Frame1.Controls.Add(this.txtPlate);
            this.Frame1.Controls.Add(this.txtReg2DOB);
            this.Frame1.Controls.Add(this.txtReg1DOB);
            this.Frame1.Controls.Add(this.txtUnitNumber);
            this.Frame1.Controls.Add(this.txtDOTNumber);
            this.Frame1.Controls.Add(this.txtAddress1);
            this.Frame1.Controls.Add(this.txtresidenceCode);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtReg3DOB);
            this.Frame1.Controls.Add(this.txtAddress2);
            this.Frame1.Controls.Add(this.txtCountry);
            this.Frame1.Controls.Add(this.txtLegalResStreet);
            this.Frame1.Controls.Add(this.txtTaxIDNumber);
            this.Frame1.Controls.Add(this.Label28);
            this.Frame1.Controls.Add(this.lblRegistrant3);
            this.Frame1.Controls.Add(this.lblRegistrant2);
            this.Frame1.Controls.Add(this.lblRegistrant1);
            this.Frame1.Controls.Add(this.Label22);
            this.Frame1.Controls.Add(this.Label38);
            this.Frame1.Controls.Add(this.Label47);
            this.Frame1.Controls.Add(this.Label23);
            this.Frame1.Controls.Add(this.Label33);
            this.Frame1.Controls.Add(this.Label24);
            this.Frame1.Controls.Add(this.Label29);
            this.Frame1.Controls.Add(this.Label27);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Controls.Add(this.Label15);
            this.Frame1.Controls.Add(this.Label16);
            this.Frame1.Controls.Add(this.Label17);
            this.Frame1.Controls.Add(this.Label18);
            this.Frame1.Controls.Add(this.Label19);
            this.Frame1.Controls.Add(this.Label20);
            this.Frame1.Controls.Add(this.Label44);
            this.Frame1.Controls.Add(this.Label45);
            this.Frame1.Controls.Add(this.Label14);
            this.Frame1.Controls.Add(this.lblNumberofYear);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.lblMVR3);
            this.Frame1.Controls.Add(this.lblYSD);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label9);
            this.Frame1.Controls.Add(this.Label34);
            this.Frame1.Controls.Add(this.Shape3);
            this.Frame1.Controls.Add(this.txtMVR3);
            this.Frame1.Location = new System.Drawing.Point(4, 6);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1056, 643);
            this.Frame1.TabIndex = 100;
            this.Frame1.Text = "1";
            // 
            // fcLabel5
            // 
            this.fcLabel5.Location = new System.Drawing.Point(483, 210);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(117, 13);
            this.fcLabel5.TabIndex = 239;
            this.fcLabel5.Text = "DATE OF BIRTH";
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(266, 210);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(141, 13);
            this.fcLabel1.TabIndex = 238;
            this.fcLabel1.Text = "REGISTRANT NAME";
            // 
            // imgEditRegistrant3
            // 
            this.imgEditRegistrant3.AppearanceKey = "actionButton";
            this.imgEditRegistrant3.FillColor = 16777215;
            this.imgEditRegistrant3.ImageSource = "icon-edit-border";
            this.imgEditRegistrant3.Location = new System.Drawing.Point(440, 298);
            this.imgEditRegistrant3.Name = "imgEditRegistrant3";
            this.imgEditRegistrant3.Size = new System.Drawing.Size(23, 22);
            this.imgEditRegistrant3.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgEditRegistrant3.Click += new System.EventHandler(this.imgEditRegistrant3_Click);
            // 
            // imgEditRegistrant2
            // 
            this.imgEditRegistrant2.AppearanceKey = "actionButton";
            this.imgEditRegistrant2.FillColor = 16777215;
            this.imgEditRegistrant2.ImageSource = "icon-edit-border";
            this.imgEditRegistrant2.Location = new System.Drawing.Point(440, 267);
            this.imgEditRegistrant2.Name = "imgEditRegistrant2";
            this.imgEditRegistrant2.Size = new System.Drawing.Size(23, 22);
            this.imgEditRegistrant2.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgEditRegistrant2.Click += new System.EventHandler(this.imgEditRegistrant2_Click);
            // 
            // imgEditRegistrant1
            // 
            this.imgEditRegistrant1.AppearanceKey = "actionButton";
            this.imgEditRegistrant1.FillColor = 16777215;
            this.imgEditRegistrant1.ImageSource = "icon-edit-border";
            this.imgEditRegistrant1.Location = new System.Drawing.Point(440, 236);
            this.imgEditRegistrant1.Name = "imgEditRegistrant1";
            this.imgEditRegistrant1.Size = new System.Drawing.Size(23, 22);
            this.imgEditRegistrant1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgEditRegistrant1.Click += new System.EventHandler(this.imgEditRegistrant1_Click);
            // 
            // imgCopyToLegalAddress
            // 
            this.imgCopyToLegalAddress.AppearanceKey = "actionButton";
            this.imgCopyToLegalAddress.FillColor = 16777215;
            this.imgCopyToLegalAddress.ImageSource = "icon-copy";
            this.imgCopyToLegalAddress.Location = new System.Drawing.Point(473, 393);
            this.imgCopyToLegalAddress.Name = "imgCopyToLegalAddress";
            this.imgCopyToLegalAddress.Size = new System.Drawing.Size(23, 22);
            this.imgCopyToLegalAddress.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgCopyToLegalAddress, "Copy from mailing address");
            this.imgCopyToLegalAddress.Click += new System.EventHandler(this.imgCopyToLegalAddress_Click);
            // 
            // imgCopyToMailingAddress
            // 
            this.imgCopyToMailingAddress.AppearanceKey = "actionButton";
            this.imgCopyToMailingAddress.FillColor = 16777215;
            this.imgCopyToMailingAddress.ImageSource = "icon-copy";
            this.imgCopyToMailingAddress.Location = new System.Drawing.Point(165, 393);
            this.imgCopyToMailingAddress.Name = "imgCopyToMailingAddress";
            this.imgCopyToMailingAddress.Size = new System.Drawing.Size(23, 22);
            this.imgCopyToMailingAddress.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgCopyToMailingAddress, "Copy address from primary registrant");
            this.imgCopyToMailingAddress.Click += new System.EventHandler(this.imgCopyToMailingAddress_Click);
            // 
            // imgFleetGroupSearch
            // 
            this.imgFleetGroupSearch.AppearanceKey = "actionButton";
            this.imgFleetGroupSearch.FillColor = 16777215;
            this.imgFleetGroupSearch.ImageSource = "icon-search-border";
            this.imgFleetGroupSearch.Location = new System.Drawing.Point(190, 329);
            this.imgFleetGroupSearch.Name = "imgFleetGroupSearch";
            this.imgFleetGroupSearch.Size = new System.Drawing.Size(23, 22);
            this.imgFleetGroupSearch.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgFleetGroupSearch.Click += new System.EventHandler(this.imgFleetGroupSearch_Click);
            // 
            // imgReg3Edit
            // 
            this.imgReg3Edit.AppearanceKey = "actionButton";
            this.imgReg3Edit.FillColor = 16777215;
            this.imgReg3Edit.ImageSource = "icon-edit-border";
            this.imgReg3Edit.Location = new System.Drawing.Point(223, 298);
            this.imgReg3Edit.Name = "imgReg3Edit";
            this.imgReg3Edit.Size = new System.Drawing.Size(23, 22);
            this.imgReg3Edit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg3Edit.Click += new System.EventHandler(this.imgReg3Edit_Click);
            // 
            // imgReg3Search
            // 
            this.imgReg3Search.AppearanceKey = "actionButton";
            this.imgReg3Search.FillColor = 16777215;
            this.imgReg3Search.ImageSource = "icon-search-border";
            this.imgReg3Search.Location = new System.Drawing.Point(190, 298);
            this.imgReg3Search.Name = "imgReg3Search";
            this.imgReg3Search.Size = new System.Drawing.Size(23, 22);
            this.imgReg3Search.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg3Search.Click += new System.EventHandler(this.imgReg3Search_Click);
            // 
            // imgReg2Edit
            // 
            this.imgReg2Edit.AppearanceKey = "actionButton";
            this.imgReg2Edit.FillColor = 16777215;
            this.imgReg2Edit.ImageSource = "icon-edit-border";
            this.imgReg2Edit.Location = new System.Drawing.Point(223, 267);
            this.imgReg2Edit.Name = "imgReg2Edit";
            this.imgReg2Edit.Size = new System.Drawing.Size(23, 22);
            this.imgReg2Edit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg2Edit.Click += new System.EventHandler(this.imgReg2Edit_Click);
            // 
            // imgReg2Search
            // 
            this.imgReg2Search.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.imgReg2Search.AppearanceKey = "actionButton";
            this.imgReg2Search.FillColor = 16777215;
            this.imgReg2Search.ImageSource = "icon-search-border";
            this.imgReg2Search.Location = new System.Drawing.Point(190, 267);
            this.imgReg2Search.Name = "imgReg2Search";
            this.imgReg2Search.Size = new System.Drawing.Size(23, 22);
            this.imgReg2Search.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg2Search.Click += new System.EventHandler(this.imgReg2Search_Click);
            // 
            // imgReg1Edit
            // 
            this.imgReg1Edit.AppearanceKey = "actionButton";
            this.imgReg1Edit.FillColor = 16777215;
            this.imgReg1Edit.ImageSource = "icon-edit-border";
            this.imgReg1Edit.Location = new System.Drawing.Point(223, 236);
            this.imgReg1Edit.Name = "imgReg1Edit";
            this.imgReg1Edit.Size = new System.Drawing.Size(23, 22);
            this.imgReg1Edit.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg1Edit.Click += new System.EventHandler(this.imgReg1Edit_Click);
            // 
            // imgReg1Search
            // 
            this.imgReg1Search.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.imgReg1Search.AppearanceKey = "actionButton";
            this.imgReg1Search.FillColor = 16777215;
            this.imgReg1Search.ImageSource = "icon-search-border";
            this.imgReg1Search.Location = new System.Drawing.Point(190, 236);
            this.imgReg1Search.Name = "imgReg1Search";
            this.imgReg1Search.Size = new System.Drawing.Size(23, 22);
            this.imgReg1Search.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgReg1Search.Click += new System.EventHandler(this.imgReg1Search_Click);
            // 
            // FraSystemSettings
            // 
            this.FraSystemSettings.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.FraSystemSettings.Controls.Add(this.LblForced);
            this.FraSystemSettings.Controls.Add(this.lblBattle);
            this.FraSystemSettings.Controls.Add(this.LblExciseExempt);
            this.FraSystemSettings.Controls.Add(this.LblAgentExempt);
            this.FraSystemSettings.Controls.Add(this.LblStateExempt);
            this.FraSystemSettings.Controls.Add(this.LblTransferExempt);
            this.FraSystemSettings.Controls.Add(this.txtBattle);
            this.FraSystemSettings.Controls.Add(this.txtAgentExempt);
            this.FraSystemSettings.Controls.Add(this.txtTransferExempt);
            this.FraSystemSettings.Controls.Add(this.txtStateExempt);
            this.FraSystemSettings.Controls.Add(this.txtExciseExempt);
            this.FraSystemSettings.Controls.Add(this.txtForced);
            this.FraSystemSettings.Location = new System.Drawing.Point(319, 547);
            this.FraSystemSettings.Name = "FraSystemSettings";
            this.FraSystemSettings.Size = new System.Drawing.Size(694, 82);
            this.FraSystemSettings.TabIndex = 112;
            this.FraSystemSettings.Text = "System Settings";
            // 
            // LblForced
            // 
            this.LblForced.Location = new System.Drawing.Point(17, 25);
            this.LblForced.Name = "LblForced";
            this.LblForced.Size = new System.Drawing.Size(95, 15);
            this.LblForced.TabIndex = 124;
            this.LblForced.Text = "FORCED PLATE";
            // 
            // lblBattle
            // 
            this.lblBattle.Location = new System.Drawing.Point(17, 51);
            this.lblBattle.Name = "lblBattle";
            this.lblBattle.Size = new System.Drawing.Size(119, 15);
            this.lblBattle.TabIndex = 123;
            this.lblBattle.Text = "BATTLE DECAL";
            // 
            // LblExciseExempt
            // 
            this.LblExciseExempt.Location = new System.Drawing.Point(253, 51);
            this.LblExciseExempt.Name = "LblExciseExempt";
            this.LblExciseExempt.Size = new System.Drawing.Size(87, 15);
            this.LblExciseExempt.TabIndex = 122;
            this.LblExciseExempt.Text = "EXC. EXEMPT";
            // 
            // LblAgentExempt
            // 
            this.LblAgentExempt.Location = new System.Drawing.Point(253, 27);
            this.LblAgentExempt.Name = "LblAgentExempt";
            this.LblAgentExempt.Size = new System.Drawing.Size(101, 15);
            this.LblAgentExempt.TabIndex = 121;
            this.LblAgentExempt.Text = "AGENT EXEMPT";
            // 
            // LblStateExempt
            // 
            this.LblStateExempt.Location = new System.Drawing.Point(467, 27);
            this.LblStateExempt.Name = "LblStateExempt";
            this.LblStateExempt.Size = new System.Drawing.Size(96, 15);
            this.LblStateExempt.TabIndex = 120;
            this.LblStateExempt.Text = "STATE EXEMPT";
            // 
            // LblTransferExempt
            // 
            this.LblTransferExempt.Location = new System.Drawing.Point(467, 53);
            this.LblTransferExempt.Name = "LblTransferExempt";
            this.LblTransferExempt.Size = new System.Drawing.Size(119, 15);
            this.LblTransferExempt.TabIndex = 119;
            this.LblTransferExempt.Text = "TRANSFER EXEMPT";
            // 
            // txtBattle
            // 
            this.txtBattle.Location = new System.Drawing.Point(124, 51);
            this.txtBattle.Name = "txtBattle";
            this.txtBattle.Size = new System.Drawing.Size(104, 15);
            this.txtBattle.TabIndex = 118;
            this.txtBattle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.txtBattle, "X = No decal issued   K = Korean decal");
            // 
            // txtAgentExempt
            // 
            this.txtAgentExempt.Location = new System.Drawing.Point(330, 27);
            this.txtAgentExempt.Name = "txtAgentExempt";
            this.txtAgentExempt.Size = new System.Drawing.Size(104, 15);
            this.txtAgentExempt.TabIndex = 117;
            this.txtAgentExempt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.txtAgentExempt, "N = Not exempt   Y = Exempt from agent fees");
            // 
            // txtTransferExempt
            // 
            this.txtTransferExempt.Location = new System.Drawing.Point(555, 53);
            this.txtTransferExempt.Name = "txtTransferExempt";
            this.txtTransferExempt.Size = new System.Drawing.Size(104, 15);
            this.txtTransferExempt.TabIndex = 116;
            this.txtTransferExempt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.txtTransferExempt, "N = Not exempt   Y = Exempt from transfer fees");
            // 
            // txtStateExempt
            // 
            this.txtStateExempt.Location = new System.Drawing.Point(555, 27);
            this.txtStateExempt.Name = "txtStateExempt";
            this.txtStateExempt.Size = new System.Drawing.Size(104, 15);
            this.txtStateExempt.TabIndex = 115;
            this.txtStateExempt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.txtStateExempt, "N = Not exempt   Y = Exempt from state fees");
            // 
            // txtExciseExempt
            // 
            this.txtExciseExempt.Location = new System.Drawing.Point(330, 51);
            this.txtExciseExempt.Name = "txtExciseExempt";
            this.txtExciseExempt.Size = new System.Drawing.Size(104, 15);
            this.txtExciseExempt.TabIndex = 114;
            this.txtExciseExempt.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.txtExciseExempt, "N = Not exempt   Y = Exempt from excise tax");
            // 
            // txtForced
            // 
            this.txtForced.Location = new System.Drawing.Point(124, 27);
            this.txtForced.Name = "txtForced";
            this.txtForced.Size = new System.Drawing.Size(104, 15);
            this.txtForced.TabIndex = 113;
            this.txtForced.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.txtForced, "N = Not forced   S = Special request plate   I - Issue as coded   P - Prepaid fee" +
        "s plate   T - Temporary plate");
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtPermit);
            this.Frame2.Controls.Add(this.txtBoosterEffective);
            this.Frame2.Controls.Add(this.txtBoosterExpiration);
            this.Frame2.Controls.Add(this.txtBoosterWeight);
            this.Frame2.Controls.Add(this.Label31_14);
            this.Frame2.Controls.Add(this.Label31_13);
            this.Frame2.Controls.Add(this.Label31_12);
            this.Frame2.Controls.Add(this.Label31_11);
            this.Frame2.Location = new System.Drawing.Point(786, 4);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(274, 134);
            this.Frame2.TabIndex = 101;
            this.Frame2.Text = "Booster Information";
            this.Frame2.Visible = false;
            // 
            // txtPermit
            // 
            this.txtPermit.Location = new System.Drawing.Point(151, 20);
            this.txtPermit.MaxLength = 10;
            this.txtPermit.Name = "txtPermit";
            this.txtPermit.Size = new System.Drawing.Size(117, 22);
            this.txtPermit.TabIndex = 102;
            this.txtPermit.Visible = false;
            // 
            // txtBoosterEffective
            // 
            this.txtBoosterEffective.Location = new System.Drawing.Point(151, 48);
            this.txtBoosterEffective.MaxLength = 10;
            this.txtBoosterEffective.Name = "txtBoosterEffective";
            this.txtBoosterEffective.Size = new System.Drawing.Size(117, 22);
            this.txtBoosterEffective.TabIndex = 103;
            this.txtBoosterEffective.Visible = false;
            // 
            // txtBoosterExpiration
            // 
            this.txtBoosterExpiration.Location = new System.Drawing.Point(151, 77);
            this.txtBoosterExpiration.MaxLength = 10;
            this.txtBoosterExpiration.Name = "txtBoosterExpiration";
            this.txtBoosterExpiration.Size = new System.Drawing.Size(117, 22);
            this.txtBoosterExpiration.TabIndex = 104;
            this.txtBoosterExpiration.Visible = false;
            // 
            // txtBoosterWeight
            // 
            this.txtBoosterWeight.Cursor = Wisej.Web.Cursors.Default;
            this.txtBoosterWeight.Location = new System.Drawing.Point(151, 105);
            this.txtBoosterWeight.Name = "txtBoosterWeight";
            this.txtBoosterWeight.Size = new System.Drawing.Size(117, 22);
            this.txtBoosterWeight.TabIndex = 105;
            // 
            // Label31_14
            // 
            this.Label31_14.Location = new System.Drawing.Point(15, 108);
            this.Label31_14.Name = "Label31_14";
            this.Label31_14.Size = new System.Drawing.Size(120, 22);
            this.Label31_14.TabIndex = 109;
            this.Label31_14.Text = "BOOSTED WEIGHT";
            this.Label31_14.Visible = false;
            // 
            // Label31_13
            // 
            this.Label31_13.Location = new System.Drawing.Point(15, 80);
            this.Label31_13.Name = "Label31_13";
            this.Label31_13.Size = new System.Drawing.Size(120, 22);
            this.Label31_13.TabIndex = 108;
            this.Label31_13.Text = "EXPIRATION DATE";
            this.Label31_13.Visible = false;
            // 
            // Label31_12
            // 
            this.Label31_12.Location = new System.Drawing.Point(15, 51);
            this.Label31_12.Name = "Label31_12";
            this.Label31_12.Size = new System.Drawing.Size(120, 22);
            this.Label31_12.TabIndex = 107;
            this.Label31_12.Text = "EFFECTIVE DATE";
            this.Label31_12.Visible = false;
            // 
            // Label31_11
            // 
            this.Label31_11.Location = new System.Drawing.Point(15, 23);
            this.Label31_11.Name = "Label31_11";
            this.Label31_11.Size = new System.Drawing.Size(120, 22);
            this.Label31_11.TabIndex = 106;
            this.Label31_11.Text = "PERMIT NUMBER";
            this.Label31_11.Visible = false;
            // 
            // txtLegalres
            // 
            this.txtLegalres.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalres.Cursor = Wisej.Web.Cursors.Default;
            this.txtLegalres.Location = new System.Drawing.Point(318, 468);
            this.txtLegalres.MaxLength = 20;
            this.txtLegalres.Name = "txtLegalres";
            this.txtLegalres.Size = new System.Drawing.Size(281, 22);
            this.txtLegalres.TabIndex = 51;
            this.txtLegalres.Enter += new System.EventHandler(this.txtLegalres_Enter);
            this.txtLegalres.Leave += new System.EventHandler(this.txtLegalres_Leave);
            this.txtLegalres.Validating += new System.ComponentModel.CancelEventHandler(this.txtLegalres_Validate);
            // 
            // fcLabel4
            // 
            this.fcLabel4.Location = new System.Drawing.Point(318, 448);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(206, 22);
            this.fcLabel4.TabIndex = 236;
            this.fcLabel4.Text = "CITY";
            // 
            // txtCity
            // 
            this.txtCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCity.Cursor = Wisej.Web.Cursors.Default;
            this.txtCity.Location = new System.Drawing.Point(5, 494);
            this.txtCity.MaxLength = 20;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(305, 22);
            this.txtCity.TabIndex = 46;
            this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
            this.txtCity.Leave += new System.EventHandler(this.txtCity_Leave);
            // 
            // fcLabel3
            // 
            this.fcLabel3.Location = new System.Drawing.Point(74, 526);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(82, 13);
            this.fcLabel3.TabIndex = 235;
            this.fcLabel3.Text = "ZIP";
            // 
            // Label48
            // 
            this.Label48.Location = new System.Drawing.Point(186, 526);
            this.Label48.Name = "Label48";
            this.Label48.Size = new System.Drawing.Size(74, 13);
            this.Label48.TabIndex = 234;
            this.Label48.Text = "COUNTRY";
            // 
            // Label52
            // 
            this.Label52.Location = new System.Drawing.Point(8, 526);
            this.Label52.Name = "Label52";
            this.Label52.Size = new System.Drawing.Size(48, 13);
            this.Label52.TabIndex = 233;
            this.Label52.Text = "STATE";
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(5, 475);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(206, 22);
            this.fcLabel2.TabIndex = 194;
            this.fcLabel2.Text = "CITY";
            // 
            // imgFleetComment
            // 
            this.imgFleetComment.Anchor = Wisej.Web.AnchorStyles.Left;
            this.imgFleetComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgFleetComment.Cursor = Wisej.Web.Cursors.Default;
            this.imgFleetComment.FillColor = 16777215;
            this.imgFleetComment.Image = ((System.Drawing.Image)(resources.GetObject("imgFleetComment.Image")));
            this.imgFleetComment.Location = new System.Drawing.Point(520, -73);
            this.imgFleetComment.Name = "imgFleetComment";
            this.imgFleetComment.Size = new System.Drawing.Size(40, 22);
            this.imgFleetComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgFleetComment.Visible = false;
            this.imgFleetComment.Click += new System.EventHandler(this.imgFleetComment_Click);
            // 
            // lblFleetName
            // 
            this.lblFleetName.Location = new System.Drawing.Point(305, 329);
            this.lblFleetName.Name = "lblFleetName";
            this.lblFleetName.Size = new System.Drawing.Size(175, 22);
            this.lblFleetName.TabIndex = 192;
            this.lblFleetName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFleetNumber
            // 
            this.txtFleetNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtFleetNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtFleetNumber.Location = new System.Drawing.Point(106, 329);
            this.txtFleetNumber.Name = "txtFleetNumber";
            this.txtFleetNumber.Size = new System.Drawing.Size(90, 22);
            this.txtFleetNumber.TabIndex = 190;
            this.txtFleetNumber.TabStop = false;
            this.txtFleetNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtFleetNumber_Validating);
            // 
            // txtState
            // 
            this.txtState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtState.Cursor = Wisej.Web.Cursors.Default;
            this.txtState.Location = new System.Drawing.Point(8, 542);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(50, 22);
            this.txtState.TabIndex = 47;
            this.txtState.Enter += new System.EventHandler(this.txtState_Enter);
            this.txtState.Leave += new System.EventHandler(this.txtState_Leave);
            // 
            // txtReg3PartyID
            // 
            this.txtReg3PartyID.Anchor = Wisej.Web.AnchorStyles.Left;
            this.txtReg3PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg3PartyID.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg3PartyID.Location = new System.Drawing.Point(106, 298);
            this.txtReg3PartyID.Name = "txtReg3PartyID";
            this.txtReg3PartyID.Size = new System.Drawing.Size(90, 22);
            this.txtReg3PartyID.TabIndex = 37;
            this.txtReg3PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg3PartyID_Validating);
            this.txtReg3PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg3PartyID_KeyPress);
            // 
            // lblFleet
            // 
            this.lblFleet.Location = new System.Drawing.Point(20, 333);
            this.lblFleet.Name = "lblFleet";
            this.lblFleet.Size = new System.Drawing.Size(90, 14);
            this.lblFleet.TabIndex = 191;
            this.lblFleet.Text = "FLEET/GROUP";
            this.lblFleet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtReg2PartyID
            // 
            this.txtReg2PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg2PartyID.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg2PartyID.Location = new System.Drawing.Point(106, 267);
            this.txtReg2PartyID.Name = "txtReg2PartyID";
            this.txtReg2PartyID.Size = new System.Drawing.Size(90, 22);
            this.txtReg2PartyID.TabIndex = 31;
            this.txtReg2PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2PartyID_Validating);
            this.txtReg2PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg2PartyID_KeyPress);
            // 
            // txtReg1PartyID
            // 
            this.txtReg1PartyID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg1PartyID.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg1PartyID.Location = new System.Drawing.Point(106, 236);
            this.txtReg1PartyID.Name = "txtReg1PartyID";
            this.txtReg1PartyID.Size = new System.Drawing.Size(90, 22);
            this.txtReg1PartyID.TabIndex = 25;
            this.txtReg1PartyID.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1PartyID_Validating);
            this.txtReg1PartyID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReg1PartyID_KeyPress);
            // 
            // txtResCountry
            // 
            this.txtResCountry.Anchor = Wisej.Web.AnchorStyles.Left;
            this.txtResCountry.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtResCountry.Cursor = Wisej.Web.Cursors.Default;
            this.txtResCountry.Location = new System.Drawing.Point(382, 517);
            this.txtResCountry.MaxLength = 2;
            this.txtResCountry.Name = "txtResCountry";
            this.txtResCountry.Size = new System.Drawing.Size(84, 22);
            this.txtResCountry.TabIndex = 53;
            this.txtResCountry.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtResCountry.Enter += new System.EventHandler(this.txtResCountry_Enter);
            this.txtResCountry.Leave += new System.EventHandler(this.txtResCountry_Leave);
            // 
            // txtResState
            // 
            this.txtResState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtResState.Cursor = Wisej.Web.Cursors.Default;
            this.txtResState.Location = new System.Drawing.Point(318, 517);
            this.txtResState.MaxLength = 2;
            this.txtResState.Name = "txtResState";
            this.txtResState.Size = new System.Drawing.Size(53, 22);
            this.txtResState.TabIndex = 52;
            this.txtResState.Enter += new System.EventHandler(this.txtResState_Enter);
            this.txtResState.Leave += new System.EventHandler(this.txtResState_Leave);
            // 
            // txtReg2LR
            // 
            this.txtReg2LR.Anchor = Wisej.Web.AnchorStyles.Left;
            this.txtReg2LR.Appearance = 0;
            this.txtReg2LR.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtReg2LR.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg2LR.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg2LR.Location = new System.Drawing.Point(6, 268);
            this.txtReg2LR.MaxLength = 1;
            this.txtReg2LR.Name = "txtReg2LR";
            this.txtReg2LR.Size = new System.Drawing.Size(40, 22);
            this.txtReg2LR.TabIndex = 29;
            this.txtReg2LR.Text = "N";
            this.txtReg2LR.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg2LR, "N = None  E = Lessee   R = Lessor  T = Trustee  P = Personal Representative");
            this.txtReg2LR.Enter += new System.EventHandler(this.txtReg2LR_Enter);
            this.txtReg2LR.Leave += new System.EventHandler(this.txtReg2LR_Leave);
            this.txtReg2LR.DoubleClick += new System.EventHandler(this.txtReg2LR_DoubleClick);
            this.txtReg2LR.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2LR_Validating);
            // 
            // txtReg1LR
            // 
            this.txtReg1LR.Appearance = 0;
            this.txtReg1LR.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtReg1LR.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg1LR.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg1LR.Location = new System.Drawing.Point(6, 236);
            this.txtReg1LR.MaxLength = 1;
            this.txtReg1LR.Name = "txtReg1LR";
            this.txtReg1LR.Size = new System.Drawing.Size(40, 22);
            this.txtReg1LR.TabIndex = 23;
            this.txtReg1LR.Text = "N";
            this.txtReg1LR.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg1LR, "N = None  E = Lessee   R = Lessor  T = Trustee  P = Personal Representative");
            this.txtReg1LR.Enter += new System.EventHandler(this.txtReg1LR_Enter);
            this.txtReg1LR.Leave += new System.EventHandler(this.txtReg1LR_Leave);
            this.txtReg1LR.DoubleClick += new System.EventHandler(this.txtReg1LR_DoubleClick);
            this.txtReg1LR.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1LR_Validating);
            // 
            // txtReg3LR
            // 
            this.txtReg3LR.Appearance = 0;
            this.txtReg3LR.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtReg3LR.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg3LR.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg3LR.Location = new System.Drawing.Point(6, 298);
            this.txtReg3LR.LockedOriginal = true;
            this.txtReg3LR.MaxLength = 1;
            this.txtReg3LR.Name = "txtReg3LR";
            this.txtReg3LR.ReadOnly = true;
            this.txtReg3LR.Size = new System.Drawing.Size(40, 22);
            this.txtReg3LR.TabIndex = 35;
            this.txtReg3LR.Text = "N";
            this.txtReg3LR.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg3LR, "N = None  E = Lessee   R = Lessor  T = Trustee  P = Personal Representative");
            this.txtReg3LR.Enter += new System.EventHandler(this.txtReg3LR_Enter);
            this.txtReg3LR.Leave += new System.EventHandler(this.txtReg3LR_Leave);
            this.txtReg3LR.DoubleClick += new System.EventHandler(this.txtReg3LR_DoubleClick);
            this.txtReg3LR.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg3LR_Validating);
            // 
            // cboSubClass
            // 
            this.cboSubClass.Location = new System.Drawing.Point(580, 83);
            this.cboSubClass.Name = "cboSubClass";
            this.cboSubClass.Size = new System.Drawing.Size(70, 22);
            this.cboSubClass.TabIndex = 8;
            this.cboSubClass.DropDown += new System.EventHandler(this.cboSubClass_DropDown);
            this.cboSubClass.Enter += new System.EventHandler(this.cboSubClass_Enter);
            this.cboSubClass.Leave += new System.EventHandler(this.cboSubClass_Leave);
            // 
            // cboClass
            // 
            this.cboClass.Location = new System.Drawing.Point(487, 83);
            this.cboClass.Name = "cboClass";
            this.cboClass.Size = new System.Drawing.Size(77, 22);
            this.cboClass.TabIndex = 7;
            this.cboClass.SelectedIndexChanged += new System.EventHandler(this.cboClass_SelectedIndexChanged);
            this.cboClass.Enter += new System.EventHandler(this.cboClass_Enter);
            this.cboClass.Leave += new System.EventHandler(this.cboClass_Leave);
            // 
            // txtReg3ICM
            // 
            this.txtReg3ICM.Appearance = 0;
            this.txtReg3ICM.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtReg3ICM.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg3ICM.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg3ICM.Location = new System.Drawing.Point(56, 298);
            this.txtReg3ICM.LockedOriginal = true;
            this.txtReg3ICM.MaxLength = 1;
            this.txtReg3ICM.Name = "txtReg3ICM";
            this.txtReg3ICM.ReadOnly = true;
            this.txtReg3ICM.Size = new System.Drawing.Size(40, 22);
            this.txtReg3ICM.TabIndex = 36;
            this.txtReg3ICM.Text = "N";
            this.txtReg3ICM.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg3ICM, "N = None I = Individual  M= Municipal  C = Commercial  D = Doing Business As");
            this.txtReg3ICM.Enter += new System.EventHandler(this.txtReg3ICM_Enter);
            this.txtReg3ICM.Leave += new System.EventHandler(this.txtReg3ICM_Leave);
            this.txtReg3ICM.DoubleClick += new System.EventHandler(this.txtReg3ICM_DoubleClick);
            this.txtReg3ICM.TextChanged += new System.EventHandler(this.txtReg3ICM_TextChanged);
            this.txtReg3ICM.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg3ICM_Validating);
            // 
            // txtReg1ICM
            // 
            this.txtReg1ICM.Appearance = 0;
            this.txtReg1ICM.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtReg1ICM.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg1ICM.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg1ICM.Location = new System.Drawing.Point(56, 236);
            this.txtReg1ICM.MaxLength = 1;
            this.txtReg1ICM.Name = "txtReg1ICM";
            this.txtReg1ICM.Size = new System.Drawing.Size(40, 22);
            this.txtReg1ICM.TabIndex = 24;
            this.txtReg1ICM.Text = "I";
            this.txtReg1ICM.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg1ICM, "I = Individual  M= Municipal  C = Commercial");
            this.txtReg1ICM.Enter += new System.EventHandler(this.txtReg1ICM_Enter);
            this.txtReg1ICM.Leave += new System.EventHandler(this.txtReg1ICM_Leave);
            this.txtReg1ICM.DoubleClick += new System.EventHandler(this.txtReg1ICM_DoubleClick);
            this.txtReg1ICM.TextChanged += new System.EventHandler(this.txtReg1ICM_TextChanged);
            this.txtReg1ICM.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg1ICM_Validating);
            // 
            // txtReg2ICM
            // 
            this.txtReg2ICM.Appearance = 0;
            this.txtReg2ICM.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtReg2ICM.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReg2ICM.Cursor = Wisej.Web.Cursors.Default;
            this.txtReg2ICM.Location = new System.Drawing.Point(56, 267);
            this.txtReg2ICM.MaxLength = 1;
            this.txtReg2ICM.Name = "txtReg2ICM";
            this.txtReg2ICM.Size = new System.Drawing.Size(40, 22);
            this.txtReg2ICM.TabIndex = 30;
            this.txtReg2ICM.Text = "N";
            this.txtReg2ICM.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtReg2ICM, "N = None I = Individual  M= Municipal  C = Commercial  D = Doing Business As");
            this.txtReg2ICM.Enter += new System.EventHandler(this.txtReg2ICM_Enter);
            this.txtReg2ICM.Leave += new System.EventHandler(this.txtReg2ICM_Leave);
            this.txtReg2ICM.DoubleClick += new System.EventHandler(this.txtReg2ICM_DoubleClick);
            this.txtReg2ICM.TextChanged += new System.EventHandler(this.txtReg2ICM_TextChanged);
            this.txtReg2ICM.Validating += new System.ComponentModel.CancelEventHandler(this.txtReg2ICM_Validating);
            // 
            // txtNetWeight
            // 
            this.txtNetWeight.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtNetWeight.Cursor = Wisej.Web.Cursors.Default;
            this.txtNetWeight.Location = new System.Drawing.Point(807, 168);
            this.txtNetWeight.MaxLength = 6;
            this.txtNetWeight.Name = "txtNetWeight";
            this.txtNetWeight.Size = new System.Drawing.Size(70, 22);
            this.txtNetWeight.TabIndex = 20;
            this.txtNetWeight.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtNetWeight.Enter += new System.EventHandler(this.txtNetWeight_Enter);
            this.txtNetWeight.Leave += new System.EventHandler(this.txtNetWeight_Leave);
            // 
            // txtRegWeight
            // 
            this.txtRegWeight.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRegWeight.Cursor = Wisej.Web.Cursors.Default;
            this.txtRegWeight.Location = new System.Drawing.Point(883, 168);
            this.txtRegWeight.MaxLength = 6;
            this.txtRegWeight.Name = "txtRegWeight";
            this.txtRegWeight.Size = new System.Drawing.Size(80, 22);
            this.txtRegWeight.TabIndex = 21;
            this.txtRegWeight.Enter += new System.EventHandler(this.txtRegWeight_Enter);
            this.txtRegWeight.Leave += new System.EventHandler(this.txtRegWeight_Leave);
            // 
            // txtFuel
            // 
            this.txtFuel.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtFuel.Cursor = Wisej.Web.Cursors.Default;
            this.txtFuel.Location = new System.Drawing.Point(974, 168);
            this.txtFuel.MaxLength = 1;
            this.txtFuel.Name = "txtFuel";
            this.txtFuel.Size = new System.Drawing.Size(50, 22);
            this.txtFuel.TabIndex = 22;
            this.txtFuel.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtFuel.Enter += new System.EventHandler(this.txtFuel_Enter);
            this.txtFuel.Leave += new System.EventHandler(this.txtFuel_Leave);
            // 
            // txtMileage
            // 
            this.txtMileage.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMileage.Cursor = Wisej.Web.Cursors.Default;
            this.txtMileage.Location = new System.Drawing.Point(387, 83);
            this.txtMileage.MaxLength = 8;
            this.txtMileage.Name = "txtMileage";
            this.txtMileage.Size = new System.Drawing.Size(91, 22);
            this.txtMileage.TabIndex = 6;
            this.txtMileage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMileage, "Shows the mileage from the last registration");
            this.txtMileage.Enter += new System.EventHandler(this.txtMileage_Enter);
            this.txtMileage.Leave += new System.EventHandler(this.txtMileage_Leave);
            // 
            // txtMonthStickerNumber
            // 
            this.txtMonthStickerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMonthStickerNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtMonthStickerNumber.Location = new System.Drawing.Point(64, 580);
            this.txtMonthStickerNumber.Name = "txtMonthStickerNumber";
            this.txtMonthStickerNumber.Size = new System.Drawing.Size(199, 22);
            this.txtMonthStickerNumber.TabIndex = 111;
            this.txtMonthStickerNumber.TabStop = false;
            // 
            // txtYearStickerNumber
            // 
            this.txtYearStickerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYearStickerNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtYearStickerNumber.Location = new System.Drawing.Point(64, 611);
            this.txtYearStickerNumber.Name = "txtYearStickerNumber";
            this.txtYearStickerNumber.Size = new System.Drawing.Size(199, 22);
            this.txtYearStickerNumber.TabIndex = 110;
            this.txtYearStickerNumber.TabStop = false;
            // 
            // txtVin
            // 
            this.txtVin.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtVin.Cursor = Wisej.Web.Cursors.Default;
            this.txtVin.Location = new System.Drawing.Point(8, 167);
            this.txtVin.MaxLength = 17;
            this.txtVin.Name = "txtVin";
            this.txtVin.Size = new System.Drawing.Size(200, 22);
            this.txtVin.TabIndex = 10;
            this.txtVin.Enter += new System.EventHandler(this.txtVin_Enter);
            this.txtVin.Leave += new System.EventHandler(this.txtVin_Leave);
            this.txtVin.Validating += new System.ComponentModel.CancelEventHandler(this.txtVin_Validate);
            // 
            // txtYear
            // 
            this.txtYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtYear.Cursor = Wisej.Web.Cursors.Default;
            this.txtYear.Location = new System.Drawing.Point(219, 167);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(70, 22);
            this.txtYear.TabIndex = 11;
            this.txtYear.Enter += new System.EventHandler(this.txtYear_Enter);
            this.txtYear.Leave += new System.EventHandler(this.txtYear_Leave);
            // 
            // txtMake
            // 
            this.txtMake.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMake.Cursor = Wisej.Web.Cursors.Default;
            this.txtMake.Location = new System.Drawing.Point(300, 167);
            this.txtMake.MaxLength = 4;
            this.txtMake.Name = "txtMake";
            this.txtMake.Size = new System.Drawing.Size(77, 22);
            this.txtMake.TabIndex = 12;
            this.txtMake.Enter += new System.EventHandler(this.txtMake_Enter);
            this.txtMake.Leave += new System.EventHandler(this.txtMake_Leave);
            // 
            // txtModel
            // 
            this.txtModel.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtModel.Cursor = Wisej.Web.Cursors.Default;
            this.txtModel.Location = new System.Drawing.Point(387, 167);
            this.txtModel.MaxLength = 6;
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(90, 22);
            this.txtModel.TabIndex = 13;
            this.txtModel.Enter += new System.EventHandler(this.txtModel_Enter);
            this.txtModel.Leave += new System.EventHandler(this.txtModel_Leave);
            // 
            // txtColor1
            // 
            this.txtColor1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor1.Cursor = Wisej.Web.Cursors.Default;
            this.txtColor1.Location = new System.Drawing.Point(490, 167);
            this.txtColor1.MaxLength = 2;
            this.txtColor1.Name = "txtColor1";
            this.txtColor1.Size = new System.Drawing.Size(50, 22);
            this.txtColor1.TabIndex = 14;
            this.txtColor1.Enter += new System.EventHandler(this.txtColor1_Enter);
            this.txtColor1.Leave += new System.EventHandler(this.txtColor1_Leave);
            // 
            // txtColor2
            // 
            this.txtColor2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColor2.Cursor = Wisej.Web.Cursors.Default;
            this.txtColor2.Location = new System.Drawing.Point(546, 167);
            this.txtColor2.MaxLength = 2;
            this.txtColor2.Name = "txtColor2";
            this.txtColor2.Size = new System.Drawing.Size(50, 22);
            this.txtColor2.TabIndex = 15;
            this.txtColor2.Enter += new System.EventHandler(this.txtColor2_Enter);
            this.txtColor2.Leave += new System.EventHandler(this.txtColor2_Leave);
            // 
            // txtStyle
            // 
            this.txtStyle.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtStyle.Cursor = Wisej.Web.Cursors.Default;
            this.txtStyle.Location = new System.Drawing.Point(613, 168);
            this.txtStyle.MaxLength = 2;
            this.txtStyle.Name = "txtStyle";
            this.txtStyle.Size = new System.Drawing.Size(50, 22);
            this.txtStyle.TabIndex = 17;
            this.txtStyle.Enter += new System.EventHandler(this.txtStyle_Enter);
            this.txtStyle.Leave += new System.EventHandler(this.txtStyle_Leave);
            // 
            // txtTires
            // 
            this.txtTires.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTires.Cursor = Wisej.Web.Cursors.Default;
            this.txtTires.Location = new System.Drawing.Point(676, 168);
            this.txtTires.MaxLength = 2;
            this.txtTires.Name = "txtTires";
            this.txtTires.Size = new System.Drawing.Size(60, 22);
            this.txtTires.TabIndex = 18;
            this.txtTires.Enter += new System.EventHandler(this.txtTires_Enter);
            this.txtTires.Leave += new System.EventHandler(this.txtTires_Leave);
            // 
            // txtAxles
            // 
            this.txtAxles.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAxles.Cursor = Wisej.Web.Cursors.Default;
            this.txtAxles.Location = new System.Drawing.Point(747, 168);
            this.txtAxles.MaxLength = 2;
            this.txtAxles.Name = "txtAxles";
            this.txtAxles.Size = new System.Drawing.Size(55, 22);
            this.txtAxles.TabIndex = 19;
            this.txtAxles.Enter += new System.EventHandler(this.txtAxles_Enter);
            this.txtAxles.Leave += new System.EventHandler(this.txtAxles_Leave);
            // 
            // txtRegistrationType
            // 
            this.txtRegistrationType.Cursor = Wisej.Web.Cursors.Default;
            this.txtRegistrationType.Location = new System.Drawing.Point(255, 28);
            this.txtRegistrationType.Name = "txtRegistrationType";
            this.txtRegistrationType.Size = new System.Drawing.Size(378, 22);
            this.txtRegistrationType.TabIndex = 126;
            this.txtRegistrationType.TabStop = false;
            // 
            // txtStatus
            // 
            this.txtStatus.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtStatus.Cursor = Wisej.Web.Cursors.Default;
            this.txtStatus.Location = new System.Drawing.Point(131, 28);
            this.txtStatus.MaxLength = 1;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(113, 22);
            this.txtStatus.TabIndex = 2;
            this.txtStatus.Enter += new System.EventHandler(this.txtStatus_Enter);
            this.txtStatus.Leave += new System.EventHandler(this.txtStatus_Leave);
            this.txtStatus.Validating += new System.ComponentModel.CancelEventHandler(this.txtStatus_Validating);
            // 
            // txtExpireDate
            // 
            this.txtExpireDate.AutoSize = false;
            this.txtExpireDate.Location = new System.Drawing.Point(131, 84);
            this.txtExpireDate.Mask = "##/##/####";
            this.txtExpireDate.MaxLength = 10;
            this.txtExpireDate.Name = "txtExpireDate";
            this.txtExpireDate.Size = new System.Drawing.Size(115, 22);
            this.txtExpireDate.TabIndex = 4;
            this.txtExpireDate.Enter += new System.EventHandler(this.txtExpireDate_Enter);
            this.txtExpireDate.Leave += new System.EventHandler(this.txtExpireDate_Leave);
            this.txtExpireDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtExpireDate_Validate);
            // 
            // txtEffectiveDate
            // 
            this.txtEffectiveDate.AutoSize = false;
            this.txtEffectiveDate.Location = new System.Drawing.Point(5, 83);
            this.txtEffectiveDate.Mask = "##/##/####";
            this.txtEffectiveDate.MaxLength = 10;
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(115, 22);
            this.txtEffectiveDate.TabIndex = 3;
            this.txtEffectiveDate.Enter += new System.EventHandler(this.txtEffectiveDate_Enter);
            this.txtEffectiveDate.Leave += new System.EventHandler(this.txtEffectiveDate_Leave);
            // 
            // txtExciseTaxDate
            // 
            this.txtExciseTaxDate.AutoSize = false;
            this.txtExciseTaxDate.Location = new System.Drawing.Point(259, 83);
            this.txtExciseTaxDate.Mask = "##/##/####";
            this.txtExciseTaxDate.MaxLength = 10;
            this.txtExciseTaxDate.Name = "txtExciseTaxDate";
            this.txtExciseTaxDate.Size = new System.Drawing.Size(115, 22);
            this.txtExciseTaxDate.TabIndex = 5;
            this.txtExciseTaxDate.Enter += new System.EventHandler(this.txtExciseTaxDate_Enter);
            this.txtExciseTaxDate.Leave += new System.EventHandler(this.txtExciseTaxDate_Leave);
            // 
            // txtPlate
            // 
            this.txtPlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPlate.Cursor = Wisej.Web.Cursors.Default;
            this.txtPlate.Location = new System.Drawing.Point(665, 83);
            this.txtPlate.MaxLength = 8;
            this.txtPlate.Name = "txtPlate";
            this.txtPlate.Size = new System.Drawing.Size(110, 22);
            this.txtPlate.TabIndex = 9;
            this.txtPlate.Enter += new System.EventHandler(this.txtPlate_Enter);
            this.txtPlate.Leave += new System.EventHandler(this.txtPlate_Leave);
            this.txtPlate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPlate_KeyPressEvent);
            // 
            // txtReg2DOB
            // 
            this.txtReg2DOB.AutoSize = false;
            this.txtReg2DOB.Location = new System.Drawing.Point(486, 267);
            this.txtReg2DOB.Mask = "##/##/####";
            this.txtReg2DOB.MaxLength = 10;
            this.txtReg2DOB.Name = "txtReg2DOB";
            this.txtReg2DOB.Size = new System.Drawing.Size(114, 22);
            this.txtReg2DOB.TabIndex = 34;
            this.txtReg2DOB.Enter += new System.EventHandler(this.txtReg2DOB_Enter);
            this.txtReg2DOB.Leave += new System.EventHandler(this.txtReg2DOB_Leave);
            // 
            // txtReg1DOB
            // 
            this.txtReg1DOB.AutoSize = false;
            this.txtReg1DOB.Location = new System.Drawing.Point(486, 236);
            this.txtReg1DOB.Mask = "##/##/####";
            this.txtReg1DOB.MaxLength = 10;
            this.txtReg1DOB.Name = "txtReg1DOB";
            this.txtReg1DOB.Size = new System.Drawing.Size(114, 22);
            this.txtReg1DOB.TabIndex = 28;
            this.txtReg1DOB.Enter += new System.EventHandler(this.txtReg1DOB_Enter);
            this.txtReg1DOB.Leave += new System.EventHandler(this.txtReg1DOB_Leave);
            // 
            // txtUnitNumber
            // 
            this.txtUnitNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUnitNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtUnitNumber.Location = new System.Drawing.Point(54, 360);
            this.txtUnitNumber.MaxLength = 10;
            this.txtUnitNumber.Name = "txtUnitNumber";
            this.txtUnitNumber.Size = new System.Drawing.Size(130, 22);
            this.txtUnitNumber.TabIndex = 41;
            this.txtUnitNumber.Enter += new System.EventHandler(this.txtUnitNumber_Enter);
            this.txtUnitNumber.Leave += new System.EventHandler(this.txtUnitNumber_Leave);
            // 
            // txtDOTNumber
            // 
            this.txtDOTNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtDOTNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtDOTNumber.Location = new System.Drawing.Point(247, 360);
            this.txtDOTNumber.MaxLength = 11;
            this.txtDOTNumber.Name = "txtDOTNumber";
            this.txtDOTNumber.Size = new System.Drawing.Size(122, 22);
            this.txtDOTNumber.TabIndex = 42;
            this.txtDOTNumber.Enter += new System.EventHandler(this.txtDOTNumber_Enter);
            this.txtDOTNumber.Leave += new System.EventHandler(this.txtDOTNumber_Leave);
            // 
            // txtAddress1
            // 
            this.txtAddress1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAddress1.Cursor = Wisej.Web.Cursors.Default;
            this.txtAddress1.Location = new System.Drawing.Point(5, 417);
            this.txtAddress1.MaxLength = 29;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(305, 22);
            this.txtAddress1.TabIndex = 44;
            this.txtAddress1.Enter += new System.EventHandler(this.txtAddress1_Enter);
            this.txtAddress1.Leave += new System.EventHandler(this.txtAddress1_Leave);
            // 
            // txtresidenceCode
            // 
            this.txtresidenceCode.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtresidenceCode.Cursor = Wisej.Web.Cursors.Default;
            this.txtresidenceCode.Location = new System.Drawing.Point(472, 517);
            this.txtresidenceCode.MaxLength = 5;
            this.txtresidenceCode.Name = "txtresidenceCode";
            this.txtresidenceCode.Size = new System.Drawing.Size(138, 22);
            this.txtresidenceCode.TabIndex = 54;
            this.txtresidenceCode.Enter += new System.EventHandler(this.txtresidenceCode_Enter);
            this.txtresidenceCode.Leave += new System.EventHandler(this.txtresidenceCode_Leave);
            this.txtresidenceCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtresidenceCode_KeyPressEvent);
            // 
            // txtZip
            // 
            this.txtZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtZip.Cursor = Wisej.Web.Cursors.Default;
            this.txtZip.Location = new System.Drawing.Point(74, 542);
            this.txtZip.MaxLength = 15;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(95, 22);
            this.txtZip.TabIndex = 48;
            this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
            this.txtZip.Leave += new System.EventHandler(this.txtZip_Leave);
            // 
            // txtReg3DOB
            // 
            this.txtReg3DOB.AutoSize = false;
            this.txtReg3DOB.Location = new System.Drawing.Point(486, 298);
            this.txtReg3DOB.Mask = "##/##/####";
            this.txtReg3DOB.MaxLength = 10;
            this.txtReg3DOB.Name = "txtReg3DOB";
            this.txtReg3DOB.Size = new System.Drawing.Size(114, 22);
            this.txtReg3DOB.TabIndex = 40;
            this.txtReg3DOB.Enter += new System.EventHandler(this.txtReg3DOB_Enter);
            this.txtReg3DOB.Leave += new System.EventHandler(this.txtReg3DOB_Leave);
            // 
            // txtAddress2
            // 
            this.txtAddress2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAddress2.Cursor = Wisej.Web.Cursors.Default;
            this.txtAddress2.Location = new System.Drawing.Point(5, 446);
            this.txtAddress2.MaxLength = 29;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(305, 22);
            this.txtAddress2.TabIndex = 45;
            this.txtAddress2.Enter += new System.EventHandler(this.txtAddress2_Enter);
            this.txtAddress2.Leave += new System.EventHandler(this.txtAddress2_Leave);
            // 
            // txtCountry
            // 
            this.txtCountry.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCountry.Cursor = Wisej.Web.Cursors.Default;
            this.txtCountry.Location = new System.Drawing.Point(186, 542);
            this.txtCountry.MaxLength = 2;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(84, 22);
            this.txtCountry.TabIndex = 49;
            this.txtCountry.Enter += new System.EventHandler(this.txtCountry_Enter);
            this.txtCountry.Leave += new System.EventHandler(this.txtCountry_Leave);
            // 
            // txtLegalResStreet
            // 
            this.txtLegalResStreet.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLegalResStreet.Cursor = Wisej.Web.Cursors.Default;
            this.txtLegalResStreet.Location = new System.Drawing.Point(318, 417);
            this.txtLegalResStreet.MaxLength = 50;
            this.txtLegalResStreet.Name = "txtLegalResStreet";
            this.txtLegalResStreet.Size = new System.Drawing.Size(281, 22);
            this.txtLegalResStreet.TabIndex = 50;
            this.txtLegalResStreet.Enter += new System.EventHandler(this.txtLegalResStreet_Enter);
            this.txtLegalResStreet.Leave += new System.EventHandler(this.txtLegalResStreet_Leave);
            // 
            // txtTaxIDNumber
            // 
            this.txtTaxIDNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTaxIDNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtTaxIDNumber.Location = new System.Drawing.Point(450, 360);
            this.txtTaxIDNumber.MaxLength = 11;
            this.txtTaxIDNumber.Name = "txtTaxIDNumber";
            this.txtTaxIDNumber.Size = new System.Drawing.Size(122, 22);
            this.txtTaxIDNumber.TabIndex = 43;
            this.txtTaxIDNumber.Enter += new System.EventHandler(this.txtTaxIDNumber_Enter);
            this.txtTaxIDNumber.Leave += new System.EventHandler(this.txtTaxIDNumber_Leave);
            // 
            // Label28
            // 
            this.Label28.Location = new System.Drawing.Point(394, 366);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(42, 13);
            this.Label28.TabIndex = 182;
            this.Label28.Text = "TAX ID #";
            // 
            // lblRegistrant3
            // 
            this.lblRegistrant3.AutoEllipsis = true;
            this.lblRegistrant3.Location = new System.Drawing.Point(266, 298);
            this.lblRegistrant3.Name = "lblRegistrant3";
            this.lblRegistrant3.Size = new System.Drawing.Size(200, 22);
            this.lblRegistrant3.TabIndex = 181;
            this.lblRegistrant3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRegistrant2
            // 
            this.lblRegistrant2.AutoEllipsis = true;
            this.lblRegistrant2.Location = new System.Drawing.Point(266, 267);
            this.lblRegistrant2.Name = "lblRegistrant2";
            this.lblRegistrant2.Size = new System.Drawing.Size(200, 22);
            this.lblRegistrant2.TabIndex = 180;
            this.lblRegistrant2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRegistrant1
            // 
            this.lblRegistrant1.AutoEllipsis = true;
            this.lblRegistrant1.Location = new System.Drawing.Point(266, 236);
            this.lblRegistrant1.Name = "lblRegistrant1";
            this.lblRegistrant1.Size = new System.Drawing.Size(200, 22);
            this.lblRegistrant1.TabIndex = 179;
            this.lblRegistrant1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(106, 211);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(135, 13);
            this.Label22.TabIndex = 178;
            this.Label22.Text = "CENTRAL PARTY";
            // 
            // Label38
            // 
            this.Label38.Location = new System.Drawing.Point(319, 498);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(47, 22);
            this.Label38.TabIndex = 176;
            this.Label38.Text = "STATE";
            // 
            // Label47
            // 
            this.Label47.Location = new System.Drawing.Point(382, 498);
            this.Label47.Name = "Label47";
            this.Label47.Size = new System.Drawing.Size(84, 22);
            this.Label47.TabIndex = 175;
            this.Label47.Text = "COUNTRY";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(318, 398);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(286, 22);
            this.Label23.TabIndex = 177;
            this.Label23.Text = " LEGAL RESIDENCE";
            // 
            // Label33
            // 
            this.Label33.Location = new System.Drawing.Point(200, 366);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(41, 13);
            this.Label33.TabIndex = 174;
            this.Label33.Text = "DOT #";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(8, 366);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(34, 13);
            this.Label24.TabIndex = 173;
            this.Label24.Text = "UNIT #";
            // 
            // Label29
            // 
            this.Label29.Location = new System.Drawing.Point(473, 498);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(170, 22);
            this.Label29.TabIndex = 172;
            this.Label29.Text = " LEGAL RESIDENCE CODE";
            // 
            // Label27
            // 
            this.Label27.Location = new System.Drawing.Point(5, 398);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(206, 22);
            this.Label27.TabIndex = 171;
            this.Label27.Text = " MAILING ADDRESS";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(489, 67);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(51, 13);
            this.Label2.TabIndex = 166;
            this.Label2.Text = "CLASS";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(665, 67);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(150, 13);
            this.Label4.TabIndex = 165;
            this.Label4.Text = "REGISTRATION NUMBER";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(131, 67);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(127, 13);
            this.Label7.TabIndex = 164;
            this.Label7.Text = "EXPIRATION DATE";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(8, 150);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(51, 15);
            this.Label10.TabIndex = 163;
            this.Label10.Text = "VIN";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(219, 150);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(56, 15);
            this.Label11.TabIndex = 162;
            this.Label11.Text = "YEAR";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(300, 150);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(43, 15);
            this.Label12.TabIndex = 161;
            this.Label12.Text = "MAKE";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(387, 150);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(58, 15);
            this.Label13.TabIndex = 160;
            this.Label13.Text = " MODEL";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(613, 150);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(37, 15);
            this.Label15.TabIndex = 159;
            this.Label15.Text = "STYLE";
            this.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(676, 150);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(37, 15);
            this.Label16.TabIndex = 158;
            this.Label16.Text = "TIRES";
            this.Label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(747, 150);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(37, 15);
            this.Label17.TabIndex = 157;
            this.Label17.Text = "AXLES";
            this.Label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(807, 150);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(70, 15);
            this.Label18.TabIndex = 156;
            this.Label18.Text = "NET WGT";
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(883, 149);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(76, 15);
            this.Label19.TabIndex = 155;
            this.Label19.Text = "REG WGT";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(974, 150);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(39, 15);
            this.Label20.TabIndex = 154;
            this.Label20.Text = "FUEL";
            // 
            // Label44
            // 
            this.Label44.Location = new System.Drawing.Point(13, 580);
            this.Label44.Name = "Label44";
            this.Label44.Size = new System.Drawing.Size(22, 22);
            this.Label44.TabIndex = 140;
            this.Label44.Text = "(M)";
            // 
            // Label45
            // 
            this.Label45.Location = new System.Drawing.Point(13, 614);
            this.Label45.Name = "Label45";
            this.Label45.Size = new System.Drawing.Size(22, 22);
            this.Label45.TabIndex = 139;
            this.Label45.Text = "(Y)";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(490, 150);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(60, 15);
            this.Label14.TabIndex = 16;
            this.Label14.Text = " COLOR";
            // 
            // lblNumberofYear
            // 
            this.lblNumberofYear.Location = new System.Drawing.Point(42, 551);
            this.lblNumberofYear.Name = "lblNumberofYear";
            this.lblNumberofYear.Size = new System.Drawing.Size(15, 13);
            this.lblNumberofYear.TabIndex = 138;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(387, 65);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(64, 13);
            this.Label3.TabIndex = 137;
            this.Label3.Text = "MILEAGE";
            // 
            // lblMVR3
            // 
            this.lblMVR3.Location = new System.Drawing.Point(8, 11);
            this.lblMVR3.Name = "lblMVR3";
            this.lblMVR3.Size = new System.Drawing.Size(64, 13);
            this.lblMVR3.TabIndex = 136;
            this.lblMVR3.Text = "MVR3#";
            // 
            // lblYSD
            // 
            this.lblYSD.Location = new System.Drawing.Point(102, 551);
            this.lblYSD.Name = "lblYSD";
            this.lblYSD.Size = new System.Drawing.Size(9, 14);
            this.lblYSD.TabIndex = 135;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(580, 67);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(70, 13);
            this.Label6.TabIndex = 134;
            this.Label6.Text = "SUB-CLASS";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(131, 11);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(63, 13);
            this.Label8.TabIndex = 133;
            this.Label8.Text = "STATUS";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(8, 67);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(107, 13);
            this.Label5.TabIndex = 129;
            this.Label5.Text = "EFFECTIVE DATE";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(255, 11);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(122, 13);
            this.Label9.TabIndex = 128;
            this.Label9.Text = "LAST REGISTRATION";
            // 
            // Label34
            // 
            this.Label34.Location = new System.Drawing.Point(259, 67);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(107, 13);
            this.Label34.TabIndex = 131;
            this.Label34.Text = "EXCISE TAX DATE";
            // 
            // Shape3
            // 
            this.Shape3.AppearanceKey = "groupBoxNoBorders";
            this.Shape3.Controls.Add(this.Label35);
            this.Shape3.Controls.Add(this.Label36);
            this.Shape3.Controls.Add(this.txtMilRate);
            this.Shape3.Controls.Add(this.Label37);
            this.Shape3.Controls.Add(this.txtAgentFee);
            this.Shape3.Controls.Add(this.lblExciseTax);
            this.Shape3.Controls.Add(this.txtAmountOfTax);
            this.Shape3.Controls.Add(this.Label39);
            this.Shape3.Controls.Add(this.txtCredit);
            this.Shape3.Controls.Add(this.Label30);
            this.Shape3.Controls.Add(this.txtSubtotal);
            this.Shape3.Controls.Add(this.txtTransferCharge);
            this.Shape3.Controls.Add(this.Label32);
            this.Shape3.Controls.Add(this.txtExciseTaxBalance);
            this.Shape3.Controls.Add(this.Label43);
            this.Shape3.Controls.Add(this.txtCreditNumber);
            this.Shape3.Controls.Add(this.chkExciseHalfRate);
            this.Shape3.Controls.Add(this.Label46);
            this.Shape3.Controls.Add(this.txtRegRate);
            this.Shape3.Controls.Add(this.lblStateCredit);
            this.Shape3.Controls.Add(this.txtRegCredit);
            this.Shape3.Controls.Add(this.txtRBYear);
            this.Shape3.Controls.Add(this.lblFees);
            this.Shape3.Controls.Add(this.txtRegFeeCharged);
            this.Shape3.Controls.Add(this.lblUseTax);
            this.Shape3.Controls.Add(this.txtSalesTax);
            this.Shape3.Controls.Add(this.Label40);
            this.Shape3.Controls.Add(this.txtTitleFee);
            this.Shape3.Controls.Add(this.Label42);
            this.Shape3.Controls.Add(this.txtOpID);
            this.Shape3.Controls.Add(this.chkRegHalf);
            this.Shape3.Controls.Add(this.txtPrice);
            this.Shape3.Controls.Add(this.Label31_0);
            this.Shape3.Location = new System.Drawing.Point(622, 203);
            this.Shape3.Name = "Shape3";
            this.Shape3.Size = new System.Drawing.Size(433, 313);
            this.Shape3.TabIndex = 186;
            // 
            // Label35
            // 
            this.Label35.Location = new System.Drawing.Point(6, 7);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(82, 14);
            this.Label35.TabIndex = 150;
            this.Label35.Text = "BASE PRICE";
            this.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label36
            // 
            this.Label36.Location = new System.Drawing.Point(6, 38);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(61, 14);
            this.Label36.TabIndex = 149;
            this.Label36.Text = "MIL. RATE";
            this.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMilRate
            // 
            this.txtMilRate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMilRate.Cursor = Wisej.Web.Cursors.Default;
            this.txtMilRate.Location = new System.Drawing.Point(139, 34);
            this.txtMilRate.LockedOriginal = true;
            this.txtMilRate.MaxLength = 6;
            this.txtMilRate.Name = "txtMilRate";
            this.txtMilRate.ReadOnly = true;
            this.txtMilRate.Size = new System.Drawing.Size(80, 22);
            this.txtMilRate.TabIndex = 125;
            this.txtMilRate.TabStop = false;
            this.txtMilRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label37
            // 
            this.Label37.Location = new System.Drawing.Point(6, 69);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(79, 14);
            this.Label37.TabIndex = 148;
            this.Label37.Text = "AGENT FEE";
            this.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAgentFee
            // 
            this.txtAgentFee.AutoSize = false;
            this.txtAgentFee.Location = new System.Drawing.Point(94, 65);
            this.txtAgentFee.MaxLength = 7;
            this.txtAgentFee.Name = "txtAgentFee";
            this.txtAgentFee.Size = new System.Drawing.Size(125, 22);
            this.txtAgentFee.TabIndex = 57;
            this.txtAgentFee.Enter += new System.EventHandler(this.txtAgentFee_Enter);
            this.txtAgentFee.Leave += new System.EventHandler(this.txtAgentFee_Leave);
            // 
            // lblExciseTax
            // 
            this.lblExciseTax.Location = new System.Drawing.Point(6, 99);
            this.lblExciseTax.Name = "lblExciseTax";
            this.lblExciseTax.Size = new System.Drawing.Size(70, 14);
            this.lblExciseTax.TabIndex = 147;
            this.lblExciseTax.Text = "EXCISE TAX";
            this.lblExciseTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAmountOfTax
            // 
            this.txtAmountOfTax.AutoSize = false;
            this.txtAmountOfTax.Location = new System.Drawing.Point(94, 95);
            this.txtAmountOfTax.MaxLength = 10;
            this.txtAmountOfTax.Name = "txtAmountOfTax";
            this.txtAmountOfTax.Size = new System.Drawing.Size(125, 22);
            this.txtAmountOfTax.TabIndex = 58;
            this.txtAmountOfTax.Enter += new System.EventHandler(this.txtAmountOfTax_Enter);
            this.txtAmountOfTax.Leave += new System.EventHandler(this.txtAmountOfTax_Leave);
            // 
            // Label39
            // 
            this.Label39.Location = new System.Drawing.Point(6, 130);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(61, 14);
            this.Label39.TabIndex = 146;
            this.Label39.Text = "CREDIT";
            this.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCredit
            // 
            this.txtCredit.AutoSize = false;
            this.txtCredit.Location = new System.Drawing.Point(94, 126);
            this.txtCredit.MaxLength = 10;
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.Size = new System.Drawing.Size(125, 22);
            this.txtCredit.TabIndex = 59;
            this.txtCredit.Enter += new System.EventHandler(this.txtCredit_Enter);
            this.txtCredit.Leave += new System.EventHandler(this.txtCredit_Leave);
            // 
            // Label30
            // 
            this.Label30.Location = new System.Drawing.Point(6, 160);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(61, 14);
            this.Label30.TabIndex = 153;
            this.Label30.Text = "SUBTOTAL";
            this.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSubtotal
            // 
            this.txtSubtotal.AutoSize = false;
            this.txtSubtotal.Location = new System.Drawing.Point(94, 156);
            this.txtSubtotal.MaxLength = 10;
            this.txtSubtotal.Name = "txtSubtotal";
            this.txtSubtotal.Size = new System.Drawing.Size(125, 22);
            this.txtSubtotal.TabIndex = 60;
            this.txtSubtotal.Enter += new System.EventHandler(this.txtSubtotal_Enter);
            this.txtSubtotal.Leave += new System.EventHandler(this.txtSubtotal_Leave);
            // 
            // txtTransferCharge
            // 
            this.txtTransferCharge.AutoSize = false;
            this.txtTransferCharge.Location = new System.Drawing.Point(94, 187);
            this.txtTransferCharge.MaxLength = 8;
            this.txtTransferCharge.Name = "txtTransferCharge";
            this.txtTransferCharge.Size = new System.Drawing.Size(125, 22);
            this.txtTransferCharge.TabIndex = 61;
            this.txtTransferCharge.Enter += new System.EventHandler(this.txtTransferCharge_Enter);
            this.txtTransferCharge.Leave += new System.EventHandler(this.txtTransferCharge_Leave);
            // 
            // Label32
            // 
            this.Label32.Location = new System.Drawing.Point(6, 221);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(62, 14);
            this.Label32.TabIndex = 151;
            this.Label32.Text = "BALANCE";
            this.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtExciseTaxBalance
            // 
            this.txtExciseTaxBalance.AutoSize = false;
            this.txtExciseTaxBalance.Location = new System.Drawing.Point(94, 217);
            this.txtExciseTaxBalance.MaxLength = 10;
            this.txtExciseTaxBalance.Name = "txtExciseTaxBalance";
            this.txtExciseTaxBalance.Size = new System.Drawing.Size(125, 22);
            this.txtExciseTaxBalance.TabIndex = 62;
            this.txtExciseTaxBalance.Enter += new System.EventHandler(this.txtExciseTaxBalance_Enter);
            this.txtExciseTaxBalance.Leave += new System.EventHandler(this.txtExciseTaxBalance_Leave);
            // 
            // Label43
            // 
            this.Label43.Location = new System.Drawing.Point(6, 252);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(82, 14);
            this.Label43.TabIndex = 193;
            this.Label43.Text = "CREDIT NO";
            this.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCreditNumber
            // 
            this.txtCreditNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCreditNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtCreditNumber.ForeColor = System.Drawing.Color.FromArgb(0, 0, 64);
            this.txtCreditNumber.Location = new System.Drawing.Point(94, 248);
            this.txtCreditNumber.MaxLength = 8;
            this.txtCreditNumber.Name = "txtCreditNumber";
            this.txtCreditNumber.Size = new System.Drawing.Size(125, 22);
            this.txtCreditNumber.TabIndex = 63;
            this.txtCreditNumber.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCreditNumber.Enter += new System.EventHandler(this.txtCreditNumber_Enter);
            this.txtCreditNumber.Leave += new System.EventHandler(this.txtCreditNumber_Leave);
            // 
            // chkExciseHalfRate
            // 
            this.chkExciseHalfRate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.chkExciseHalfRate.Location = new System.Drawing.Point(54, 282);
            this.chkExciseHalfRate.Name = "chkExciseHalfRate";
            this.chkExciseHalfRate.Size = new System.Drawing.Size(128, 22);
            this.chkExciseHalfRate.TabIndex = 64;
            this.chkExciseHalfRate.Text = "Excise Half Rate";
            // 
            // Label46
            // 
            this.Label46.Location = new System.Drawing.Point(237, 7);
            this.Label46.Name = "Label46";
            this.Label46.Size = new System.Drawing.Size(40, 14);
            this.Label46.TabIndex = 144;
            this.Label46.Text = "RATE";
            this.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRegRate
            // 
            this.txtRegRate.AutoSize = false;
            this.txtRegRate.Location = new System.Drawing.Point(307, 3);
            this.txtRegRate.MaxLength = 10;
            this.txtRegRate.Name = "txtRegRate";
            this.txtRegRate.Size = new System.Drawing.Size(125, 22);
            this.txtRegRate.TabIndex = 65;
            this.txtRegRate.Enter += new System.EventHandler(this.txtRegRate_Enter);
            this.txtRegRate.Leave += new System.EventHandler(this.txtRegRate_Leave);
            // 
            // lblStateCredit
            // 
            this.lblStateCredit.Location = new System.Drawing.Point(237, 38);
            this.lblStateCredit.Name = "lblStateCredit";
            this.lblStateCredit.Size = new System.Drawing.Size(47, 14);
            this.lblStateCredit.TabIndex = 143;
            this.lblStateCredit.Text = "CREDIT";
            this.lblStateCredit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRegCredit
            // 
            this.txtRegCredit.AutoSize = false;
            this.txtRegCredit.Location = new System.Drawing.Point(307, 34);
            this.txtRegCredit.MaxLength = 10;
            this.txtRegCredit.Name = "txtRegCredit";
            this.txtRegCredit.Size = new System.Drawing.Size(125, 22);
            this.txtRegCredit.TabIndex = 66;
            this.txtRegCredit.Enter += new System.EventHandler(this.txtRegCredit_Enter);
            this.txtRegCredit.Leave += new System.EventHandler(this.txtRegCredit_Leave);
            // 
            // txtRBYear
            // 
            this.txtRBYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRBYear.Cursor = Wisej.Web.Cursors.Default;
            this.txtRBYear.Location = new System.Drawing.Point(94, 34);
            this.txtRBYear.MaxLength = 1;
            this.txtRBYear.Name = "txtRBYear";
            this.txtRBYear.Size = new System.Drawing.Size(38, 22);
            this.txtRBYear.TabIndex = 56;
            this.txtRBYear.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtRBYear, "Mill Rates   1 = 0.024   2 = 0.0175   3 = 0.0135   4 = 0.010   5 = 0.0065   6 = 0" +
        ".004");
            this.txtRBYear.Enter += new System.EventHandler(this.txtRBYear_Enter);
            this.txtRBYear.Leave += new System.EventHandler(this.txtRBYear_Leave);
            this.txtRBYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtRBYear_Validating);
            // 
            // lblFees
            // 
            this.lblFees.Location = new System.Drawing.Point(237, 69);
            this.lblFees.Name = "lblFees";
            this.lblFees.Size = new System.Drawing.Size(40, 14);
            this.lblFees.TabIndex = 142;
            this.lblFees.Text = "FEES";
            this.lblFees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRegFeeCharged
            // 
            this.txtRegFeeCharged.AutoSize = false;
            this.txtRegFeeCharged.Location = new System.Drawing.Point(307, 65);
            this.txtRegFeeCharged.MaxLength = 10;
            this.txtRegFeeCharged.Name = "txtRegFeeCharged";
            this.txtRegFeeCharged.Size = new System.Drawing.Size(125, 22);
            this.txtRegFeeCharged.TabIndex = 67;
            this.txtRegFeeCharged.Enter += new System.EventHandler(this.txtRegFeeCharged_Enter);
            this.txtRegFeeCharged.Leave += new System.EventHandler(this.txtRegFeeCharged_Leave);
            // 
            // lblUseTax
            // 
            this.lblUseTax.Location = new System.Drawing.Point(237, 99);
            this.lblUseTax.Name = "lblUseTax";
            this.lblUseTax.Size = new System.Drawing.Size(32, 14);
            this.lblUseTax.TabIndex = 141;
            this.lblUseTax.Text = "S. T";
            this.lblUseTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSalesTax
            // 
            this.txtSalesTax.AutoSize = false;
            this.txtSalesTax.Location = new System.Drawing.Point(307, 95);
            this.txtSalesTax.MaxLength = 8;
            this.txtSalesTax.Name = "txtSalesTax";
            this.txtSalesTax.Size = new System.Drawing.Size(125, 22);
            this.txtSalesTax.TabIndex = 68;
            this.txtSalesTax.Enter += new System.EventHandler(this.txtSalesTax_Enter);
            this.txtSalesTax.Leave += new System.EventHandler(this.txtSalesTax_Leave);
            // 
            // Label40
            // 
            this.Label40.Location = new System.Drawing.Point(237, 130);
            this.Label40.Name = "Label40";
            this.Label40.Size = new System.Drawing.Size(40, 14);
            this.Label40.TabIndex = 145;
            this.Label40.Text = "TITLE";
            this.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitleFee
            // 
            this.txtTitleFee.AutoSize = false;
            this.txtTitleFee.Location = new System.Drawing.Point(307, 126);
            this.txtTitleFee.MaxLength = 8;
            this.txtTitleFee.Name = "txtTitleFee";
            this.txtTitleFee.Size = new System.Drawing.Size(125, 22);
            this.txtTitleFee.TabIndex = 69;
            this.txtTitleFee.Enter += new System.EventHandler(this.txtTitleFee_Enter);
            this.txtTitleFee.Leave += new System.EventHandler(this.txtTitleFee_Leave);
            // 
            // Label42
            // 
            this.Label42.Location = new System.Drawing.Point(237, 160);
            this.Label42.Name = "Label42";
            this.Label42.Size = new System.Drawing.Size(62, 14);
            this.Label42.TabIndex = 132;
            this.Label42.Text = "USER ID";
            this.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOpID
            // 
            this.txtOpID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtOpID.Cursor = Wisej.Web.Cursors.Default;
            this.txtOpID.Enabled = false;
            this.txtOpID.ForeColor = System.Drawing.Color.FromArgb(0, 0, 64);
            this.txtOpID.Location = new System.Drawing.Point(307, 156);
            this.txtOpID.LockedOriginal = true;
            this.txtOpID.Name = "txtOpID";
            this.txtOpID.ReadOnly = true;
            this.txtOpID.Size = new System.Drawing.Size(125, 22);
            this.txtOpID.TabIndex = 70;
            this.txtOpID.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtOpID, "SHows the ID of the processing agent");
            this.txtOpID.Enter += new System.EventHandler(this.txtOpID_Enter);
            this.txtOpID.Leave += new System.EventHandler(this.txtOpID_Leave);
            // 
            // chkRegHalf
            // 
            this.chkRegHalf.Location = new System.Drawing.Point(275, 191);
            this.chkRegHalf.Name = "chkRegHalf";
            this.chkRegHalf.Size = new System.Drawing.Size(114, 22);
            this.chkRegHalf.TabIndex = 71;
            this.chkRegHalf.Text = "Reg Half Rate";
            // 
            // txtPrice
            // 
            this.txtPrice.AutoSize = false;
            this.txtPrice.Location = new System.Drawing.Point(94, 3);
            this.txtPrice.MaxLength = 7;
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(125, 22);
            this.txtPrice.TabIndex = 55;
            this.ToolTip1.SetToolTip(this.txtPrice, "Shows the vehicle value when purchased new");
            this.txtPrice.Enter += new System.EventHandler(this.txtPrice_Enter);
            this.txtPrice.Leave += new System.EventHandler(this.txtPrice_Leave);
            // 
            // Label31_0
            // 
            this.Label31_0.Location = new System.Drawing.Point(6, 191);
            this.Label31_0.Name = "Label31_0";
            this.Label31_0.Size = new System.Drawing.Size(99, 14);
            this.Label31_0.TabIndex = 152;
            this.Label31_0.Text = "TRANSFER CHG";
            this.Label31_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMVR3
            // 
            this.txtMVR3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMVR3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMVR3.Location = new System.Drawing.Point(5, 28);
            this.txtMVR3.MaxLength = 8;
            this.txtMVR3.Name = "txtMVR3";
            this.txtMVR3.Size = new System.Drawing.Size(111, 22);
            this.txtMVR3.TabIndex = 1;
            this.txtMVR3.Enter += new System.EventHandler(this.txtMVR3_Enter);
            this.txtMVR3.Leave += new System.EventHandler(this.txtMVR3_Leave);
            // 
            // cmdFleetGroupComment
            // 
            this.cmdFleetGroupComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFleetGroupComment.Cursor = Wisej.Web.Cursors.Default;
            this.cmdFleetGroupComment.Enabled = false;
            this.cmdFleetGroupComment.Location = new System.Drawing.Point(888, 29);
            this.cmdFleetGroupComment.Name = "cmdFleetGroupComment";
            this.cmdFleetGroupComment.Size = new System.Drawing.Size(162, 24);
            this.cmdFleetGroupComment.TabIndex = 1;
            this.cmdFleetGroupComment.Text = "Fleet / Group Comment";
            this.cmdFleetGroupComment.Click += new System.EventHandler(this.mnuFleetGroupComment_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Cursor = Wisej.Web.Cursors.Default;
            this.cmdProcessSave.Location = new System.Drawing.Point(455, 8);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(80, 41);
            this.cmdProcessSave.TabIndex = 0;
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // cmdMoreInfo
            // 
            this.cmdMoreInfo.Cursor = Wisej.Web.Cursors.Default;
            this.cmdMoreInfo.Location = new System.Drawing.Point(688, 29);
            this.cmdMoreInfo.Name = "cmdMoreInfo";
            this.cmdMoreInfo.Size = new System.Drawing.Size(195, 24);
            this.cmdMoreInfo.TabIndex = 73;
            this.cmdMoreInfo.Text = "Additional Information";
            this.ToolTip1.SetToolTip(this.cmdMoreInfo, "If additional information needs to be entered but not appear on the registration");
            this.cmdMoreInfo.Click += new System.EventHandler(this.cmdMoreInfo_Click);
            // 
            // frmVehicleUpdate
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.Cursor = Wisej.Web.Cursors.Default;
            this.FillColor = 0;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVehicleUpdate";
            this.ShowInTaskbar = false;
            this.Text = "New To System / Update ";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmVehicleUpdate_Load);
            this.Activated += new System.EventHandler(this.frmVehicleUpdate_Activated);
            this.Resize += new System.EventHandler(this.frmVehicleUpdate_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmVehicleUpdate_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVehicleUpdate_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmVehicleUpdate_KeyUp);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdditionalInfo)).EndInit();
            this.fraAdditionalInfo.ResumeLayout(false);
            this.fraAdditionalInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraUserFields)).EndInit();
            this.fraUserFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserField3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserReason3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOverrideFleetEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdErase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEditRegistrant1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToLegalAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCopyToMailingAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetGroupSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg3Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg2Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReg1Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FraSystemSettings)).EndInit();
            this.FraSystemSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoosterEffective)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoosterExpiration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoosterWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFleetComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAxles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg2DOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg1DOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOTNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtresidenceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReg3DOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLegalResStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxIDNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Shape3)).EndInit();
            this.Shape3.ResumeLayout(false);
            this.Shape3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAgentFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountOfTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubtotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExciseTaxBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExciseHalfRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegFeeCharged)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegHalf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMVR3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFleetGroupComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMoreInfo)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
		public FCFrame Shape3;
		public FCLabel Label35;
		public FCLabel Label36;
		public FCTextBox txtMilRate;
		public FCLabel Label37;
		public T2KBackFillDecimal txtAgentFee;
		public FCLabel lblExciseTax;
		public T2KBackFillDecimal txtAmountOfTax;
		public FCLabel Label39;
		public T2KBackFillDecimal txtCredit;
		public FCLabel Label30;
		public T2KBackFillDecimal txtSubtotal;
		public FCLabel Label31_0;
		public T2KBackFillDecimal txtTransferCharge;
		public FCLabel Label32;
		public T2KBackFillDecimal txtExciseTaxBalance;
		public FCLabel Label43;
		public FCTextBox txtCreditNumber;
		public FCCheckBox chkExciseHalfRate;
		public FCLabel Label46;
		public T2KBackFillDecimal txtRegRate;
		public FCLabel lblStateCredit;
		public T2KBackFillDecimal txtRegCredit;
		public FCTextBox txtRBYear;
		public FCLabel lblFees;
		public T2KBackFillDecimal txtRegFeeCharged;
		public FCLabel lblUseTax;
		public T2KBackFillDecimal txtSalesTax;
		public FCLabel Label40;
		public T2KBackFillDecimal txtTitleFee;
		public FCLabel Label42;
		public FCTextBox txtOpID;
		public FCCheckBox chkRegHalf;
		public T2KBackFillWhole txtPrice;
		public T2KDateBox txtEffectiveDate;
		public FCLabel Label5;
		public FCLabel fcLabel2;
		public FCLabel fcLabel3;
		public FCLabel Label48;
		public FCLabel Label52;
		public FCLabel fcLabel4;
		private FCLabel lblFleet;
		public FCLabel lblFleetName;
		public FCButton cmdMoreInfo;
        public FCPictureBox imgFleetGroupSearch;
        public FCPictureBox imgReg3Edit;
        public FCPictureBox imgReg3Search;
        public FCPictureBox imgReg2Edit;
        public FCPictureBox imgReg2Search;
        public FCPictureBox imgReg1Edit;
        public FCPictureBox imgReg1Search;
        public FCPictureBox imgCopyToLegalAddress;
        public FCPictureBox imgCopyToMailingAddress;
        public FCPictureBox imgEditRegistrant3;
        public FCPictureBox imgEditRegistrant2;
        public FCPictureBox imgEditRegistrant1;
        public FCLabel fcLabel5;
        public FCLabel fcLabel1;
    }
}
