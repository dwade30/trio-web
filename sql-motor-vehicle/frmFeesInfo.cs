﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmFeesInfo : BaseForm
	{
		public frmFeesInfo()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			Label2.AddControlArrayElement(Label2_0, 0);
			Label2.AddControlArrayElement(Label2_1, 1);
			Label2.AddControlArrayElement(Label2_2, 2);
			Label2.AddControlArrayElement(Label2_3, 3);
			Label2.AddControlArrayElement(Label2_5, 4);
			Label2.AddControlArrayElement(Label2_6, 5);
			Label2.AddControlArrayElement(Label2_7, 6);
			Label2.AddControlArrayElement(Label2_8, 7);
			Label2.AddControlArrayElement(Label2_9, 8);
			Label2.AddControlArrayElement(Label2_10, 9);
			Label2.AddControlArrayElement(Label2_11, 10);
			Label2.AddControlArrayElement(Label2_12, 11);

			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFeesInfo InstancePtr
		{
			get
			{
				return (frmFeesInfo)Sys.GetInstance(typeof(frmFeesInfo));
			}
		}

		protected frmFeesInfo _InstancePtr = null;
		//=========================================================

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			if (VerifyVoucherInfo())
			{
				MotorVehicle.Statics.FromFeesScreen = true;
				//FC:FINAL:MSH - i.issue #1804: save values from fields before hiding form (modal form will be disposed after hiding and all data will be missed)
				frmFeesInfoStaticsValues.InstancePtr.UpdateValuesInStorage();
				frmFeesInfo.InstancePtr.Hide();
				frmDataInput.InstancePtr.blnFeesFlag = false;
			}
		}

		private bool VerifyVoucherInfo()
		{
			if (txtFEEGiftCertificate.Text.ToDecimalValue() != 0)
			{
				if (txtGCNumber.Text.Trim() == "")
				{
					MessageBox.Show("You must enter a gift certificate number before you may continue.", "Check Fees", MessageBoxButtons.OK,
						MessageBoxIcon.Warning);
					txtGCNumber.Focus();
					return false;
				}
			}

			return true;
		}

		private void cmdReturn_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				txtFEERegFee.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = (Keys)0;
				txtFEETotal.Focus();
			}
		}

		private void frmFeesInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFeesInfo_Load(object sender, System.EventArgs e)
		{
			frmFeesInfo.InstancePtr.TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal - frmFeesInfo.InstancePtr.HeightOriginal) / 2.0);
			frmFeesInfo.InstancePtr.LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal - frmFeesInfo.InstancePtr.WidthOriginal) / 2.0);
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
		}

		private void txtDupRegFee_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtIncreaseRVW.Focus();
			}
			else if (e.KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtGCNumber.Focus();
			}
		}

		private void txtDupRegFee_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEECreditComm_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEECreditTransfer.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEERegFee.Focus();
			}
		}

		private void txtFEECreditComm_Leave(object sender, System.EventArgs e)
		{
			if (MotorVehicle.Statics.Class != "CO" && MotorVehicle.Statics.Class != "CC" && MotorVehicle.Statics.Class != "TT")
			{
				txtFEECreditComm.Text = "0.00";
			}
			total();
		}

		public void total()
		{
			Decimal total = 0;
			// vbPorter upgrade warning: CreditTotal As Decimal	OnWrite(int, Decimal)
			Decimal CreditTotal;
			if (Conversion.Val(txtFEERegFee.Text) != 0)
				total += FCConvert.ToDecimal(txtFEERegFee.Text);
			if (Conversion.Val(txtFEEInitialPlate.Text) != 0)
				total += FCConvert.ToDecimal(txtFEEInitialPlate.Text);
			if (Conversion.Val(txtFEEOutOfRotation.Text) != 0)
				total += FCConvert.ToDecimal(txtFEEOutOfRotation.Text);
			if (Conversion.Val(txtFEEReservePlate.Text) != 0)
				total += FCConvert.ToDecimal(txtFEEReservePlate.Text);
			if (Conversion.Val(txtFEEPlateClass.Text) != 0)
				total += FCConvert.ToDecimal(txtFEEPlateClass.Text);
			if (Conversion.Val(txtFEEReplacementFee.Text) != 0)
				total += FCConvert.ToDecimal(txtFEEReplacementFee.Text);
			if (Conversion.Val(txtFEEStickerFee.Text) != 0)
				total += FCConvert.ToDecimal(txtFEEStickerFee.Text);
			if (Conversion.Val(txtFEETransferFee.Text) != 0)
				total += FCConvert.ToDecimal(txtFEETransferFee.Text);
			if (Conversion.Val(txtDupRegFee.Text) != 0)
				total += FCConvert.ToDecimal(txtDupRegFee.Text);
			if (Conversion.Val(txtIncreaseRVW.Text) != 0)
				total += FCConvert.ToDecimal(txtIncreaseRVW.Text);
			if (Conversion.Val(txtFEECreditComm.Text) != 0)
				total -= FCConvert.ToDecimal(txtFEECreditComm.Text);
			if (Conversion.Val(txtFEECreditTransfer.Text) != 0)
			{
				if (FCConvert.ToDecimal(txtFEECreditTransfer.Text) > FCConvert.ToDecimal(txtFEERegFee.Text))
				{
					total -= FCConvert.ToDecimal(txtFEERegFee.Text);
				}
				else
				{
					total -= FCConvert.ToDecimal(txtFEECreditTransfer.Text);
				}
			}
			if (Conversion.Val(txtFEEGiftCertificate.Text) != 0)
				total -= FCConvert.ToDecimal(txtFEEGiftCertificate.Text);
			txtFEETotal.Text = Strings.Format(total, "#,##0.00");
			CreditTotal = 0;
			if (Conversion.Val(txtFEECreditComm.Text) != 0)
				CreditTotal = FCConvert.ToDecimal(txtFEECreditComm.Text);
			if (Conversion.Val(txtFEECreditTransfer.Text) != 0)
				CreditTotal += FCConvert.ToDecimal(txtFEECreditComm.Text) + FCConvert.ToDecimal(txtFEECreditTransfer.Text);
		}

		private void txtFEECreditTransfer_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEEInitialPlate.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEECreditComm.Focus();
			}
		}

		private void txtFEECreditTransfer_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEEGiftCertificate_Change(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtFEEGiftCertificate.Text) != "")
			{
				if (FCConvert.ToDecimal(txtFEEGiftCertificate.Text) != 0)
				{
					txtGCNumber.Enabled = true;
					Label2_9.Enabled = true;
				}
				else
				{
					txtGCNumber.Text = "";
					txtGCNumber.Enabled = false;
					Label2_9.Enabled = false;
				}
			}
		}

		private void txtFEEGiftCertificate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtGCNumber.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEETransferFee.Focus();
			}
		}

		private void txtFEEGiftCertificate_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEEInitialPlate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEECreditTransfer.Focus();
			}
			else if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEEOutOfRotation.Focus();
			}
		}

		private void txtFEEInitialPlate_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEEOutOfRotation_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEEReservePlate.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEEInitialPlate.Focus();
			}
		}

		private void txtFEEOutOfRotation_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEEPlateClass_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEEReplacementFee.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEEReservePlate.Focus();
			}
		}

		private void txtFEEPlateClass_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEERegFee_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEECreditComm.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				cmdReturn.Focus();
			}
		}

		private void txtFEERegFee_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEEReplacementFee_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEEStickerFee.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEEPlateClass.Focus();
			}
		}

		private void txtFEEReplacementFee_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEEReservePlate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEEPlateClass.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEEOutOfRotation.Focus();
			}
		}

		private void txtFEEReservePlate_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEEStickerFee_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEETransferFee.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEEReplacementFee.Focus();
			}
		}

		private void txtFEEStickerFee_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtFEETotal_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				cmdReturn.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtIncreaseRVW.Focus();
			}
		}

		private void txtFEETransferFee_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEEGiftCertificate.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtFEEStickerFee.Focus();
			}
		}

		private void txtFEETransferFee_Leave(object sender, System.EventArgs e)
		{
			total();
		}

		private void txtGCNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				txtDupRegFee.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = (Keys)0;
				txtFEEGiftCertificate.Focus();
			}
		}

		private void txtGCNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
				KeyAscii = KeyAscii - 32;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtIncreaseRVW_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Down)
			{
				KeyCode = 0;
				txtFEETotal.Focus();
			}
			else if (KeyCode == Keys.Up)
			{
				KeyCode = 0;
				txtDupRegFee.Focus();
			}
		}

		private void FrmFeesInfo_VisibleChanged(object sender, EventArgs e)
		{
			//FC:FINAL:DDU:#2366 - fixed closing form from x
			if (!Visible)
			{
				MotorVehicle.Statics.FromFeesScreen = true;
				frmFeesInfoStaticsValues.InstancePtr.UpdateValuesInStorage();
				frmDataInput.InstancePtr.blnFeesFlag = false;
			}
		}

		private void frmFeesInfo_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (!VerifyVoucherInfo())
			{
				e.Cancel = true;
			}
		}
	}

	class frmFeesInfoStaticsValues
	{
		public string txtFEERegFeeValue = string.Empty;
		public string txtFEECreditCommValue = string.Empty;
		public string txtFEECreditTransferValue = string.Empty;
		public string txtFEEInitialPlateValue = string.Empty;
		public string txtFEEOutOfRotationValue = string.Empty;
		public string txtFEEReservePlateValue = string.Empty;
		public string txtFEEPlateClassValue = string.Empty;
		public string txtFEEReplacementFeeValue = string.Empty;
		public string txtFEEStickerFeeValue = string.Empty;
		public string txtFEETransferFeeValue = string.Empty;
		public string txtFEEGiftCertificateValue = string.Empty;
		public string txtGCNumberValue = string.Empty;
		public string txtDupRegFeeValue = string.Empty;
		public string txtIncreaseRVWValue = string.Empty;
		public string txtFEETotalValue = string.Empty;
		public string cmbVoucherValue = string.Empty;

		public void UpdateValuesInStorage()
		{
			txtFEERegFeeValue = frmFeesInfo.InstancePtr.txtFEERegFee.Text;
			txtFEECreditCommValue = frmFeesInfo.InstancePtr.txtFEECreditComm.Text;
			txtFEECreditTransferValue = frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text;
			txtFEEInitialPlateValue = frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text;
			txtFEEOutOfRotationValue = frmFeesInfo.InstancePtr.txtFEEOutOfRotation.Text;
			txtFEEReservePlateValue = frmFeesInfo.InstancePtr.txtFEEReservePlate.Text;
			txtFEEPlateClassValue = frmFeesInfo.InstancePtr.txtFEEPlateClass.Text;
			txtFEEReplacementFeeValue = frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text;
			txtFEEStickerFeeValue = frmFeesInfo.InstancePtr.txtFEEStickerFee.Text;
			txtFEETransferFeeValue = frmFeesInfo.InstancePtr.txtFEETransferFee.Text;
			txtFEEGiftCertificateValue = frmFeesInfo.InstancePtr.txtFEEGiftCertificate.Text;
			txtGCNumberValue = frmFeesInfo.InstancePtr.txtGCNumber.Text;
			txtDupRegFeeValue = frmFeesInfo.InstancePtr.txtDupRegFee.Text;
			txtIncreaseRVWValue = frmFeesInfo.InstancePtr.txtIncreaseRVW.Text;
			txtFEETotalValue = frmFeesInfo.InstancePtr.txtFEETotal.Text;
			cmbVoucherValue = frmFeesInfo.InstancePtr.cmbVoucher.Text;
		}

		public void UpdateValuesFromStorage()
		{
			frmFeesInfo.InstancePtr.txtFEERegFee.Text = txtFEERegFeeValue;
			frmFeesInfo.InstancePtr.txtFEECreditComm.Text = txtFEECreditCommValue;
			frmFeesInfo.InstancePtr.txtFEECreditTransfer.Text = txtFEECreditTransferValue;
			frmFeesInfo.InstancePtr.txtFEEInitialPlate.Text = txtFEEInitialPlateValue;
			frmFeesInfo.InstancePtr.txtFEEOutOfRotation.Text = txtFEEOutOfRotationValue;
			frmFeesInfo.InstancePtr.txtFEEReservePlate.Text = txtFEEReservePlateValue;
			frmFeesInfo.InstancePtr.txtFEEPlateClass.Text = txtFEEPlateClassValue;
			frmFeesInfo.InstancePtr.txtFEEReplacementFee.Text = txtFEEReplacementFeeValue;
			frmFeesInfo.InstancePtr.txtFEEStickerFee.Text = txtFEEStickerFeeValue;
			frmFeesInfo.InstancePtr.txtFEETransferFee.Text = txtFEETransferFeeValue;
			frmFeesInfo.InstancePtr.txtFEEGiftCertificate.Text = txtFEEGiftCertificateValue;
			frmFeesInfo.InstancePtr.txtGCNumber.Text = txtGCNumberValue;
			frmFeesInfo.InstancePtr.txtDupRegFee.Text = txtDupRegFeeValue;
			frmFeesInfo.InstancePtr.txtIncreaseRVW.Text = txtIncreaseRVWValue;
			frmFeesInfo.InstancePtr.txtFEETotal.Text = txtFEETotalValue;
			frmFeesInfo.InstancePtr.cmbVoucher.Text = cmbVoucherValue;
		}

        public void Unload()
        {
            Sys.ClearInstance(this);
        }

		public static frmFeesInfoStaticsValues InstancePtr
		{
			get
			{
				return (frmFeesInfoStaticsValues)Sys.GetInstance(typeof(frmFeesInfoStaticsValues));
			}
		}
	}
}
