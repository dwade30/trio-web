//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmVoidBooster : BaseForm
	{
		public frmVoidBooster()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVoidBooster InstancePtr
		{
			get
			{
				return (frmVoidBooster)Sys.GetInstance(typeof(frmVoidBooster));
			}
		}

		protected frmVoidBooster _InstancePtr = null;
		//=========================================================
		int lngHeldID;
		clsDRWrapper rsVoid = new clsDRWrapper();
		string strSQL = "";
		string strHoldPlate = "";
		string strHoldClass = "";
		bool UnIssued;
		int VoidMVR3;
		int ArchiveMVR3;
		bool PreviousPeriod;
		bool blnRRTrans;
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rsBooster = new clsDRWrapper();

		private void cmdNO_Click(object sender, System.EventArgs e)
		{
			cmdProcess.Enabled = false;
			Frame1.Visible = false;
			txtVoidedMVR3.Focus();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			// call process routine here
			// when form is unloaded and focus returns to the
			// plate info screen the plateinfo screen is going to unload
			// and automatically return to the menu.
			if (txtReason.Text == "")
			{
				MessageBox.Show("You must enter a reason before you can continue with the void.", "Reason Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Process_Routine() == true)
			{
				MessageBox.Show("The void of this Permit was successful.", "Sucessful Void", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				if (MotorVehicle.Statics.bolFromWindowsCR)
				{
					//MDIParent.InstancePtr.GetMainMenu();
					MDIParent.InstancePtr.Menu18();
				}
			}
			else
			{
				MessageBox.Show("There is an error Voiding this Booster.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			UnIssued = false;
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.Response = "QUIT";
			Close();
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}


        private void FrmVoidBooster_FormClosed(object sender, FormClosedEventArgs e)
        {
            //FC:FINAL:SBE - #2389 - execute the same code as Quit button did
            modGNBas.Statics.Response = "QUIT";
        }

		private void cmdYes_Click(object sender, System.EventArgs e)
		{
			cmdProcess.Enabled = true;
		}

		private void frmVoidBooster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVoidBooster properties;
			//frmVoidBooster.ScaleWidth	= 9045;
			//frmVoidBooster.ScaleHeight	= 6930;
			//frmVoidBooster.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			if (cmdProcess.Enabled)
			{
				cmdProcess_Click();
			}
		}

		private void optBooster_CheckedChanged(object sender, System.EventArgs e)
		{
			lblDescription.Text = "Enter the Booster Number To Void";
			txtVoidedMVR3.Text = "";
			Frame1.Visible = false;
			txtVoidedMVR3.Focus();
		}

		private void optSpecialRegPermit_CheckedChanged(object sender, System.EventArgs e)
		{
			lblDescription.Text = "Enter the Special Reg Permit Number To Void";
			txtVoidedMVR3.Text = "";
			Frame1.Visible = false;
			txtVoidedMVR3.Focus();
		}

		private void optTransitPlate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblDescription.Text = "Enter the Transit Plate Number To Void";
			txtVoidedMVR3.Text = "";
			Frame1.Visible = false;
			txtVoidedMVR3.Focus();
		}

		private void txtVoidedMVR3_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void Get_Master_Record()
		{
			UnIssued = false;
			if (cmbBooster.Text == "Booster")
			{
				rsBooster.OpenRecordset("SELECT * FROM Booster WHERE ExpireDate >= '" + DateTime.Today.ToShortDateString() + "' AND Isnull(Inactive, 0) = 0 AND Isnull(Voided, 0) = 0 AND Permit = " + FCConvert.ToString(Conversion.Val(txtVoidedMVR3.Text)));
				if (rsBooster.EndOfFile() != true)
				{
					strSQL = "SELECT * FROM Master WHERE ID = " + rsBooster.Get_Fields_Int32("VehicleID") + " ORDER BY Status DESC";
					rsVoid.OpenRecordset(strSQL);
					if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
					{
						rsVoid.MoveLast();
						rsVoid.MoveFirst();
						if (FCConvert.ToString(rsVoid.Get_Fields_String("Status")) == "T")
							rsVoid.MoveNext();
						lblName.Text = "OWNER 1    = " + MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID1"), true);
						lblAddress.Text = "ADDRESS    = " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVoid.Get_Fields_String("Address")));
						lblPlateClass.Text = "PLATE / CL = " + rsVoid.Get_Fields_String("plate") + " / " + rsVoid.Get_Fields_String("Class");
						lblVehicle.Text = "MAKE / MDL = " + rsVoid.Get_Fields_String("make") + " / " + rsVoid.Get_Fields_String("model");
						lblDate.Text = "TRANS DATE = " + FCConvert.ToString(rsVoid.Get_Fields("DateUpdated"));
						lblStickers.Text = "MO/YR STKS = " + "M=" + rsVoid.Get_Fields_Int32("MonthStickerNumber") + "  /  Y=" + rsVoid.Get_Fields_Int32("YearStickerNumber");
						Frame1.Visible = true;
						cmdProcess.Enabled = false;
					}
					else
					{
						MessageBox.Show("There are no records in the Master File with this Booster Permit Number.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						UnIssued = false;
						return;
					}
				}
				else
				{
					MessageBox.Show("There are no records in the Master File with this Booster Permit Number.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					UnIssued = false;
					return;
				}
			}
			else if (cmbBooster.Text == "Special Reg Permit")
			{
				if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
				{
					rsVoid.InsertAddress("PartyID", "", "MV");
					rsVoid.MoveLast();
					rsVoid.MoveFirst();
					lblName.Text = "OWNER 1    = " + fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID")));
					lblAddress.Text = "ADDRESS    = " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVoid.Get_Fields_String("Address1")));
					lblPlateClass.Text = "";
					lblVehicle.Text = "MAKE / YR = " + rsVoid.Get_Fields_String("make") + " / " + rsVoid.Get_Fields("year");
					lblDate.Text = "TRANS DATE = " + FCConvert.ToString(rsVoid.Get_Fields("DateUpdated"));
					lblStickers.Text = "";
					Frame1.Visible = true;
					cmdProcess.Enabled = false;
				}
				else
				{
					MessageBox.Show("There are no records with this Special Registration Permit Number.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					UnIssued = false;
					return;
				}
			}
			else
			{
				if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
				{
					rsVoid.InsertAddress("PartyID", "", "MV");
					rsVoid.MoveLast();
					rsVoid.MoveFirst();
					lblName.Text = "OWNER 1    = " + fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID")));
					lblAddress.Text = "ADDRESS    = " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVoid.Get_Fields_String("Address1")));
					lblPlateClass.Text = "";
					lblVehicle.Text = "MAKE / MDL = " + rsVoid.Get_Fields_String("make") + " / " + rsVoid.Get_Fields_String("model");
					lblDate.Text = "TRANS DATE = " + FCConvert.ToString(rsVoid.Get_Fields("DateUpdated"));
					lblStickers.Text = "";
					Frame1.Visible = true;
					cmdProcess.Enabled = false;
				}
				else
				{
					MessageBox.Show("There are no records with this Transit Plate Number.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					UnIssued = false;
					return;
				}
			}
		}

		private bool Process_Routine()
		{
			bool Process_Routine = false;
			clsDRWrapper rsOldInfo = new clsDRWrapper();
			bool Deletion;
			clsDRWrapper rsMO = new clsDRWrapper();
			clsDRWrapper rsExceptionInfo = new clsDRWrapper();
			Process_Routine = true;
			try
			{
				// On Error GoTo ErrorTag1
				fecherFoundation.Information.Err().Clear();
				if (cmbBooster.Text == "Booster")
				{
					if (UnIssued == false)
					{
						if (PreviousPeriod)
						{
							rsMO.OpenRecordset("SELECT * FROM ExceptionReport");
							rsMO.AddNew();
							rsMO.Set_Fields("Type", "4CN");
							rsMO.Set_Fields("MVR3Number", rsVoid.Get_Fields_Int32("MVR3"));
							rsMO.Set_Fields("NewPlate", rsVoid.Get_Fields_String("plate"));
							rsMO.Set_Fields("newclass", rsVoid.Get_Fields_String("Class"));
							rsMO.Set_Fields("reason", "Voiding Booster Permit from Prior Closeout Period");
							rsMO.Set_Fields("OpID", MotorVehicle.Statics.OpID);
							rsMO.Set_Fields("Fee", 0);
							rsMO.Set_Fields("PeriodCloseoutID", 0);
							rsMO.Set_Fields("TellerCloseoutID", 0);
							rsMO.Set_Fields("ExceptionReportDate", DateTime.Today);
							rsMO.Update();
							PreviousPeriod = false;
						}
						else
						{
							rsExceptionInfo.OpenRecordset("SELECT * FROM ExceptionReport WHERE ExceptionReportDate = '" + rsBooster.Get_Fields_DateTime("DateUpdated") + "' AND PermitType = 'BOOSTER' AND Fee = " + rsBooster.Get_Fields("StatePaid"));
							if (rsExceptionInfo.EndOfFile() != true)
							{
								rsExceptionInfo.Delete();
								rsExceptionInfo.Update();
								rsExceptionInfo.MoveNext();
							}
						}
						if (MotorVehicle.Statics.bolFromWindowsCR)
						{
							Write_PDS_Work_Record_Stuff_Void_Booster();
						}
						rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE Low = '" + fecherFoundation.Strings.Trim(txtVoidedMVR3.Text) + "' AND InventoryType = 'BXSXX' AND AdjustmentCode = 'I'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							rsAM.MoveLast();
							rsAM.MoveFirst();
							rsAM.Edit();
							rsAM.Set_Fields("AdjustmentCode", "V");
							rsAM.Set_Fields("PeriodCloseoutID", 0);
							rsAM.Set_Fields("TellerCloseoutID", 0);
							rsAM.Set_Fields("reason", txtReason.Text);
							rsAM.Update();
						}
						else
						{
							rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments");
							rsAM.AddNew();
							rsAM.Set_Fields("DateOfAdjustment", DateTime.Today);
							rsAM.Set_Fields("High", txtVoidedMVR3.Text);
							rsAM.Set_Fields("Low", txtVoidedMVR3.Text);
							rsAM.Set_Fields("OpID", MotorVehicle.Statics.OpID);
							rsAM.Set_Fields("reason", txtReason.Text);
							rsAM.Set_Fields("PeriodCloseoutID", 0);
							rsAM.Set_Fields("TellerCloseoutID", 0);
							rsAM.Set_Fields("QuantityAdjusted", 1);
							rsAM.Set_Fields("InventoryType", "BXSXX");
							rsAM.Set_Fields("AdjustmentCode", "V");
							rsAM.Update();
						}
						rsAM.OpenRecordset("SELECT * FROM Inventory WHERE Number = " + fecherFoundation.Strings.Trim(txtVoidedMVR3.Text) + " AND Code = 'BXSXX'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							rsAM.Delete();
							rsAM.Update();
							rsAM.MoveNext();
						}
						// 
						// With rsVoid
						// .MoveLast
						// .MoveFirst
						// .Edit
						// .Fields("BoosterKey") = 0
						// .Update
						// End With
						rsBooster.Edit();
						rsBooster.Set_Fields("Voided", true);
						rsBooster.Update();
					}
				}
				else if (cmbBooster.Text == "Special Reg Permit")
				{
					if (UnIssued == false)
					{
						if (PreviousPeriod)
						{
							rsMO.OpenRecordset("SELECT * FROM ExceptionReport");
							rsMO.AddNew();
							rsMO.Set_Fields("Type", "4CN");
							rsMO.Set_Fields("MVR3Number", "");
							rsMO.Set_Fields("NewPlate", "");
							rsMO.Set_Fields("newclass", "");
							rsMO.Set_Fields("reason", "Voiding Spec Reg Permit from Prior Closeout Period");
							rsMO.Set_Fields("OpID", MotorVehicle.Statics.OpID);
							rsMO.Set_Fields("Fee", 0);
							rsMO.Set_Fields("PeriodCloseoutID", 0);
							rsMO.Set_Fields("TellerCloseoutID", 0);
							rsMO.Set_Fields("ExceptionReportDate", DateTime.Today);
							rsMO.Update();
							PreviousPeriod = false;
						}
						else
						{
							rsExceptionInfo.OpenRecordset("SELECT * FROM ExceptionReport WHERE Date = '" + rsVoid.Get_Fields_DateTime("DateUpdated") + "' AND PermitType = 'MVR10' AND Fee = " + rsVoid.Get_Fields("StatePaid"));
							if (rsExceptionInfo.EndOfFile() != true)
							{
								rsExceptionInfo.Delete();
								rsExceptionInfo.Update();
								rsExceptionInfo.MoveNext();
							}
						}
						if (MotorVehicle.Statics.bolFromWindowsCR)
						{
							Write_PDS_Work_Record_Stuff_Void_SpecReg();
						}
						rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE Low = '" + fecherFoundation.Strings.Trim(txtVoidedMVR3.Text) + "' AND InventoryType = 'RXSXX' AND AdjustmentCode = 'I'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							rsAM.MoveLast();
							rsAM.MoveFirst();
							rsAM.Edit();
							rsAM.Set_Fields("AdjustmentCode", "V");
							rsAM.Set_Fields("PeriodCloseoutID", 0);
							rsAM.Set_Fields("TellerCloseoutID", 0);
							rsAM.Set_Fields("reason", txtReason.Text);
							rsAM.Update();
						}
						else
						{
							rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments");
							rsAM.AddNew();
							rsAM.Set_Fields("DateOfAdjustment", DateTime.Today);
							rsAM.Set_Fields("High", txtVoidedMVR3.Text);
							rsAM.Set_Fields("Low", txtVoidedMVR3.Text);
							rsAM.Set_Fields("OpID", MotorVehicle.Statics.OpID);
							rsAM.Set_Fields("reason", txtReason.Text);
							rsAM.Set_Fields("PeriodCloseoutID", 0);
							rsAM.Set_Fields("TellerCloseoutID", 0);
							rsAM.Set_Fields("QuantityAdjusted", 1);
							rsAM.Set_Fields("InventoryType", "RXSXX");
							rsAM.Set_Fields("AdjustmentCode", "V");
							rsAM.Update();
						}
						rsAM.OpenRecordset("SELECT * FROM Inventory WHERE Number = " + fecherFoundation.Strings.Trim(txtVoidedMVR3.Text) + " AND Code = 'RXSXX'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							rsAM.Delete();
							rsAM.Update();
							rsAM.MoveNext();
						}
						// 
						rsVoid.MoveLast();
						rsVoid.MoveFirst();
						rsVoid.Edit();
						rsVoid.Set_Fields("Voided", true);
						rsVoid.Update();
					}
				}
				else
				{
					if (UnIssued == false)
					{
						if (PreviousPeriod)
						{
							rsMO.OpenRecordset("SELECT * FROM ExceptionReport");
							rsMO.AddNew();
							rsMO.Set_Fields("Type", "4CN");
							rsMO.Set_Fields("MVR3Number", "");
							rsMO.Set_Fields("NewPlate", "");
							rsMO.Set_Fields("newclass", "");
							rsMO.Set_Fields("reason", "Voiding Transit Plate from Prior Closeout Period");
							rsMO.Set_Fields("OpID", MotorVehicle.Statics.OpID);
							rsMO.Set_Fields("Fee", 0);
							rsMO.Set_Fields("PeriodCloseoutID", 0);
							rsMO.Set_Fields("TellerCloseoutID", 0);
							rsMO.Set_Fields("ExceptionReportDate", DateTime.Today);
							rsMO.Update();
							PreviousPeriod = false;
						}
						else
						{
							rsExceptionInfo.OpenRecordset("SELECT * FROM ExceptionReport WHERE Date = '" + rsVoid.Get_Fields_DateTime("DateUpdated") + "' AND left(PermitType, 7) = 'TRANSIT' AND Fee = " + rsVoid.Get_Fields("StatePaid"));
							if (rsExceptionInfo.EndOfFile() != true)
							{
								rsExceptionInfo.Delete();
								rsExceptionInfo.Update();
								rsExceptionInfo.MoveNext();
							}
						}
						if (MotorVehicle.Statics.bolFromWindowsCR)
						{
							Write_PDS_Work_Record_Stuff_Void_Transit();
						}
						rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE Low = '" + fecherFoundation.Strings.Trim(txtVoidedMVR3.Text) + "' AND InventoryType = 'PXSXX' AND AdjustmentCode = 'I'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							rsAM.MoveLast();
							rsAM.MoveFirst();
							rsAM.Edit();
							rsAM.Set_Fields("AdjustmentCode", "V");
							rsAM.Set_Fields("PeriodCloseoutID", 0);
							rsAM.Set_Fields("TellerCloseoutID", 0);
							rsAM.Set_Fields("reason", txtReason.Text);
							rsAM.Update();
						}
						else
						{
							rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments");
							rsAM.AddNew();
							rsAM.Set_Fields("DateOfAdjustment", DateTime.Today);
							rsAM.Set_Fields("High", txtVoidedMVR3.Text);
							rsAM.Set_Fields("Low", txtVoidedMVR3.Text);
							rsAM.Set_Fields("OpID", MotorVehicle.Statics.OpID);
							rsAM.Set_Fields("reason", txtReason.Text);
							rsAM.Set_Fields("PeriodCloseoutID", 0);
							rsAM.Set_Fields("TellerCloseoutID", 0);
							rsAM.Set_Fields("QuantityAdjusted", 1);
							rsAM.Set_Fields("InventoryType", "PXSXX");
							rsAM.Set_Fields("AdjustmentCode", "V");
							rsAM.Update();
						}
						rsAM.OpenRecordset("SELECT * FROM Inventory WHERE Number = " + fecherFoundation.Strings.Trim(txtVoidedMVR3.Text) + " AND Code = 'PXSXX'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							rsAM.Delete();
							rsAM.Update();
							rsAM.MoveNext();
						}
						// 
						rsVoid.MoveLast();
						rsVoid.MoveFirst();
						rsVoid.Edit();
						rsVoid.Set_Fields("Voided", true);
						rsVoid.Update();
					}
				}
				return Process_Routine;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				fecherFoundation.Information.Err(ex).Clear();
				Process_Routine = false;
			}
			return Process_Routine;
		}

		private void txtVoidedMVR3_Leave(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			if (this.ActiveControl.GetName() != "cmdQuit" && Conversion.Val(txtVoidedMVR3.Text) != 0)
			{
				if (Conversion.Val(txtVoidedMVR3.Text) != 0)
				{
					if (cmbBooster.Text == "Booster")
					{
						strSQL = "SELECT * FROM Booster WHERE Permit = " + FCConvert.ToString(Conversion.Val(txtVoidedMVR3.Text));
						rsVoid.OpenRecordset(strSQL);
						if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
						{
							rsVoid.MoveLast();
							rsVoid.MoveFirst();
							if (FCConvert.ToInt32(rsVoid.Get_Fields_Int32("PeriodCloseoutID")) != 0)
							{
								answer = MessageBox.Show("The Booster Permit you are trying to void is from a previous period.  Do you wish to continue?", "Previous Period", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (answer == DialogResult.No)
								{
									return;
								}
								else
								{
									PreviousPeriod = true;
								}
							}
							else
							{
								PreviousPeriod = false;
							}
						}
					}
					else if (cmbBooster.Text == "Special Reg Permit")
					{
						strSQL = "SELECT * FROM SpecialRegistration WHERE PermitNumber = '" + txtVoidedMVR3.Text + "'";
						rsVoid.OpenRecordset(strSQL);
						if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
						{
							rsVoid.MoveLast();
							rsVoid.MoveFirst();
							if (FCConvert.ToInt32(rsVoid.Get_Fields_Int32("PeriodCloseoutID")) != 0)
							{
								answer = MessageBox.Show("The Special Reg Permit you are trying to void is from a previous period.  Do you wish to continue?", "Previous Period", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (answer == DialogResult.No)
								{
									return;
								}
								else
								{
									PreviousPeriod = true;
								}
							}
							else
							{
								PreviousPeriod = false;
							}
						}
					}
					else
					{
						strSQL = "SELECT * FROM TransitPlates WHERE Plate = '" + txtVoidedMVR3.Text + "'";
						rsVoid.OpenRecordset(strSQL);
						if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
						{
							rsVoid.MoveLast();
							rsVoid.MoveFirst();
							if (FCConvert.ToInt32(rsVoid.Get_Fields_Int32("PeriodCloseoutID")) != 0)
							{
								answer = MessageBox.Show("The Transit Plate you are trying to void is from a previous period.  Do you wish to continue?", "Previous Period", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (answer == DialogResult.No)
								{
									return;
								}
								else
								{
									PreviousPeriod = true;
								}
							}
							else
							{
								PreviousPeriod = false;
							}
						}
					}
					if (rsVoid.EndOfFile() != true)
					{
						if (rsVoid.Get_Fields_Boolean("Voided") != true)
						{
							Get_Master_Record();
						}
						else
						{
							MessageBox.Show("This permit has already been voided.", "Permit Voided Already", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						Get_Master_Record();
					}
				}
				if (cmdYes.Visible == true)
				{
					cmdYes.Focus();
				}
				else
				{
					//cmdQuit.Focus();
				}
			}
		}

		private void SetCustomFormColors()
		{
			// Label2.ForeColor = &HFF0000
		}

		private void Write_PDS_Work_Record_Stuff_Void_Booster()
		{
			FCFixedString strRec3 = new FCFixedString(241);
			FCFixedString strRec5A = new FCFixedString(241);
			clsDRWrapper rsCheck = new clsDRWrapper();
			bool AddToExcise;
			string strM = "";
			string strY = "";
			bool blnTest;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsResCheck = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			blnTest = modRegistry.GetKeyValues2(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Test", "");
			rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
			if (FCConvert.ToString(rsCheck.Get_Fields_String("ExciseVSAgent")) == "A")
			{
				AddToExcise = false;
			}
			else
			{
				AddToExcise = true;
			}
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				if (modRegionalTown.IsRegionalTown())
				{
					rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + rsBooster.Get_Fields_String("ResidenceCode") + "'", "CentralData");
					if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", rsResCheck.Get_Fields("TownNumber"));
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
					}
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
				}
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsBooster.Get_Fields("StatePaid") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsBooster.Get_Fields("LocalPaid") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID1"), true));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(rsVoid.Get_Fields_Int32("PartyID1")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", FCConvert.ToString(rsVoid.Get_Fields_String("plate")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", FCConvert.ToString(rsVoid.Get_Fields_Int32("MVR3")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", "BOOSTER");

				if (MotorVehicle.Statics.correlationId != new Guid())
				{
					MotorVehicle.CompleteMVTransaction();
				}
			}
		}

		private void Write_PDS_Work_Record_Stuff_Void_SpecReg()
		{
			FCFixedString strRec3 = new FCFixedString(241);
			FCFixedString strRec5A = new FCFixedString(241);
			clsDRWrapper rsCheck = new clsDRWrapper();
			bool AddToExcise;
			string strM = "";
			string strY = "";
			bool blnTest;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsResCheck = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			blnTest = modRegistry.GetKeyValues2(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Test", "");
			rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
			if (FCConvert.ToString(rsCheck.Get_Fields_String("ExciseVSAgent")) == "A")
			{
				AddToExcise = false;
			}
			else
			{
				AddToExcise = true;
			}
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				if (modRegionalTown.IsRegionalTown())
				{
					rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + rsVoid.Get_Fields_String("ResidenceCode") + "'", "CentralData");
					if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", rsResCheck.Get_Fields("TownNumber"));
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
					}
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
				}
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsVoid.Get_Fields("StatePaid") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsVoid.Get_Fields("LocalPaid") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID"))));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(rsVoid.Get_Fields_Int32("PartyID")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", FCConvert.ToString(rsVoid.Get_Fields("PermitNumber")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", "");
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", "SPECIAL REG PERMIT");

				if (MotorVehicle.Statics.correlationId != new Guid())
				{
					MotorVehicle.CompleteMVTransaction();
				}
			}
		}

		private void Write_PDS_Work_Record_Stuff_Void_Transit()
		{
			FCFixedString strRec3 = new FCFixedString(241);
			FCFixedString strRec5A = new FCFixedString(241);
			clsDRWrapper rsCheck = new clsDRWrapper();
			bool AddToExcise;
			string strM = "";
			string strY = "";
			bool blnTest;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsResCheck = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			blnTest = modRegistry.GetKeyValues2(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Test", "");
			rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
			if (FCConvert.ToString(rsCheck.Get_Fields_String("ExciseVSAgent")) == "A")
			{
				AddToExcise = false;
			}
			else
			{
				AddToExcise = true;
			}
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				if (modRegionalTown.IsRegionalTown())
				{
					rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + rsVoid.Get_Fields_String("ResidenceCode") + "'", "CentralData");
					if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", rsResCheck.Get_Fields("TownNumber"));
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
					}
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
				}
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsVoid.Get_Fields("StatePaid") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsVoid.Get_Fields("LocalPaid") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID"))));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(rsVoid.Get_Fields_Int32("PartyID")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", FCConvert.ToString(rsVoid.Get_Fields_String("plate")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", "");
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", "TRANSIT");

				if (MotorVehicle.Statics.correlationId != new Guid())
				{
					MotorVehicle.CompleteMVTransaction();
				}
			}
		}

		public void cmbBooster_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbBooster.Text == "Booster")
			{
				optBooster_CheckedChanged(sender, e);
			}
			else if (cmbBooster.Text == "Transit Plate")
			{
				optTransitPlate_CheckedChanged(sender, e);
			}
			if (cmbBooster.Text == "Special Reg Permit")
			{
				optSpecialRegPermit_CheckedChanged(sender, e);
			}
		}
	}
}
