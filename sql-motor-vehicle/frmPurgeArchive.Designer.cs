//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPurgeArchive.
	/// </summary>
	partial class frmPurgeArchive
	{
		public fecherFoundation.FCButton cmdPurge;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdPurge = new fecherFoundation.FCButton();
            this.txtDate = new Global.T2KDateBox();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 172);
            this.BottomPanel.Size = new System.Drawing.Size(440, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPurge);
            this.ClientArea.Controls.Add(this.txtDate);
            this.ClientArea.Controls.Add(this.lblInstructions);
            this.ClientArea.Size = new System.Drawing.Size(440, 112);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(440, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(297, 30);
            this.HeaderText.Text = "Purge Archive Information";
            // 
            // cmdPurge
            // 
            this.cmdPurge.AppearanceKey = "acceptButton";
            this.cmdPurge.Location = new System.Drawing.Point(30, 143);
            this.cmdPurge.Name = "cmdPurge";
            this.cmdPurge.Size = new System.Drawing.Size(90, 48);
            this.cmdPurge.TabIndex = 0;
            this.cmdPurge.Text = "Purge";
            this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(30, 83);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(115, 40);
            this.txtDate.TabIndex = 2;
            this.txtDate.Text = "  /  /";
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(379, 40);
            this.lblInstructions.TabIndex = 3;
            this.lblInstructions.Text = "PLEASE ENTER A DATE AT LEAST FIVE YEARS IN THE PAST AND CLICK THE \'PURGE\' BUTTON." +
    "  ALL ARCHIVE INFORMATION THAT THAT WAS SAVED ON OR BEFORE THAT DATE WILL BE DEL" +
    "ETED";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 0;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmPurgeArchive
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(440, 280);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPurgeArchive";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Purge Archive Information";
            this.Load += new System.EventHandler(this.frmPurgeArchive_Load);
            this.Activated += new System.EventHandler(this.frmPurgeArchive_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeArchive_KeyPress);
            this.Resize += new System.EventHandler(this.frmPurgeArchive_Resize);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}