﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReconDecal.
	/// </summary>
	partial class rptReconDecal
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReconDecal));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtOnHand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRecorded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIssues = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAdjusted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtItem = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtItem3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtItem2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtItem1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDescTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtOnHand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecorded)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssues)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtOnHand,
            this.txtRecorded,
            this.txtIssues,
            this.txtAdjusted,
            this.txtEnd,
            this.txtItem,
            this.txtMonth,
            this.txtItem3,
            this.txtItem2,
            this.txtItem1});
			this.Detail.Height = 0.625F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtOnHand
			// 
			this.txtOnHand.CanShrink = true;
			this.txtOnHand.Height = 0.25F;
			this.txtOnHand.Left = 0.84375F;
			this.txtOnHand.Name = "txtOnHand";
			this.txtOnHand.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.txtOnHand.Text = null;
			this.txtOnHand.Top = 0F;
			this.txtOnHand.Width = 0.75F;
			// 
			// txtRecorded
			// 
			this.txtRecorded.CanShrink = true;
			this.txtRecorded.Height = 0.25F;
			this.txtRecorded.Left = 2.125F;
			this.txtRecorded.Name = "txtRecorded";
			this.txtRecorded.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtRecorded.Text = null;
			this.txtRecorded.Top = 0F;
			this.txtRecorded.Width = 1.0625F;
			// 
			// txtIssues
			// 
			this.txtIssues.CanShrink = true;
			this.txtIssues.Height = 0.25F;
			this.txtIssues.Left = 3.5F;
			this.txtIssues.Name = "txtIssues";
			this.txtIssues.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtIssues.Text = null;
			this.txtIssues.Top = 0F;
			this.txtIssues.Width = 1.0625F;
			// 
			// txtAdjusted
			// 
			this.txtAdjusted.CanShrink = true;
			this.txtAdjusted.Height = 0.25F;
			this.txtAdjusted.Left = 4.90625F;
			this.txtAdjusted.Name = "txtAdjusted";
			this.txtAdjusted.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtAdjusted.Text = null;
			this.txtAdjusted.Top = 0F;
			this.txtAdjusted.Width = 1.0625F;
			// 
			// txtEnd
			// 
			this.txtEnd.CanShrink = true;
			this.txtEnd.Height = 0.25F;
			this.txtEnd.Left = 6.28125F;
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtEnd.Text = null;
			this.txtEnd.Top = 0F;
			this.txtEnd.Width = 1.0625F;
			// 
			// txtItem
			// 
			this.txtItem.CanShrink = true;
			this.txtItem.Height = 0.1875F;
			this.txtItem.Left = 0.0625F;
			this.txtItem.Name = "txtItem";
			this.txtItem.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtItem.Text = null;
			this.txtItem.Top = 0.25F;
			this.txtItem.Width = 3.0625F;
			// 
			// txtMonth
			// 
			this.txtMonth.CanShrink = true;
			this.txtMonth.Height = 0.25F;
			this.txtMonth.Left = 0.0625F;
			this.txtMonth.Name = "txtMonth";
			this.txtMonth.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.txtMonth.Text = null;
			this.txtMonth.Top = 0F;
			this.txtMonth.Width = 0.625F;
			// 
			// txtItem3
			// 
			this.txtItem3.CanShrink = true;
			this.txtItem3.Height = 0.1875F;
			this.txtItem3.Left = 4.875F;
			this.txtItem3.Name = "txtItem3";
			this.txtItem3.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtItem3.Text = null;
			this.txtItem3.Top = 0.4375F;
			this.txtItem3.Width = 2F;
			// 
			// txtItem2
			// 
			this.txtItem2.CanShrink = true;
			this.txtItem2.Height = 0.1875F;
			this.txtItem2.Left = 2.5625F;
			this.txtItem2.Name = "txtItem2";
			this.txtItem2.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtItem2.Text = null;
			this.txtItem2.Top = 0.4375F;
			this.txtItem2.Width = 2F;
			// 
			// txtItem1
			// 
			this.txtItem1.CanShrink = true;
			this.txtItem1.Height = 0.1875F;
			this.txtItem1.Left = 0.25F;
			this.txtItem1.Name = "txtItem1";
			this.txtItem1.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtItem1.Text = null;
			this.txtItem1.Top = 0.4375F;
			this.txtItem1.Width = 2F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport2,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label16,
            this.txtDescTitle,
            this.Label15,
            this.Label1,
            this.lblPage,
            this.Label33});
			this.GroupHeader1.Height = 2.1875F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.5F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.375F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label12.Text = "Amount on Hand Start of Period";
			this.Label12.Top = 1.8125F;
			this.Label12.Width = 1.1875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.375F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 6.25F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label13.Text = "Amount on Hand End of Period";
			this.Label13.Top = 1.8125F;
			this.Label13.Width = 1.1875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.375F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.8125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label14.Text = "Amount Adjusted During Period";
			this.Label14.Top = 1.8125F;
			this.Label14.Width = 1.1875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.375F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label16.Text = "Amount Received During Period";
			this.Label16.Top = 1.8125F;
			this.Label16.Width = 1.1875F;
			// 
			// txtDescTitle
			// 
			this.txtDescTitle.Height = 0.25F;
			this.txtDescTitle.Left = 0.125F;
			this.txtDescTitle.Name = "txtDescTitle";
			this.txtDescTitle.Style = "font-family: \'Roman 10cpi\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.txtDescTitle.Text = "Field1";
			this.txtDescTitle.Top = 1.5F;
			this.txtDescTitle.Width = 4.1875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.375F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.5F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label15.Text = "Valid Issues During Period";
			this.Label15.Top = 1.8125F;
			this.Label15.Width = 1.125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label1.Text = "**** INVENTORY RECONCILIATION REPORT ****";
			this.Label1.Top = 0F;
			this.Label1.Width = 5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.75F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// rptReconDecal
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtOnHand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecorded)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssues)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOnHand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRecorded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssues;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAdjusted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItem;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItem3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItem2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItem1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
