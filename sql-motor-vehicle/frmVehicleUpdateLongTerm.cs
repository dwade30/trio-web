//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmVehicleUpdateLongTerm : BaseForm
	{
		public frmVehicleUpdateLongTerm()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            Label2.AddControlArrayElement(Label2_0, 0);
            Label2.AddControlArrayElement(Label2_1, 1);
            Label2.AddControlArrayElement(Label2_2, 2);
            Label2.AddControlArrayElement(Label2_3, 3);
            Label2.AddControlArrayElement(Label2_4, 4);
            Label2.AddControlArrayElement(Label2_5, 5);
            Label2.AddControlArrayElement(Label2_6, 6);
            Label2.AddControlArrayElement(Label2_7, 7);
            Label2.AddControlArrayElement(Label2_8, 8);
            Label2.AddControlArrayElement(Label2_10, 9);



            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVehicleUpdateLongTerm InstancePtr
		{
			get
			{
				return (frmVehicleUpdateLongTerm)Sys.GetInstance(typeof(frmVehicleUpdateLongTerm));
			}
		}

		protected frmVehicleUpdateLongTerm _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int NewBackColor = ColorTranslator.ToOle(Color.Yellow);
		bool bolAddNewRecord;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtMessage.Text) != "" && cboMessageCodes.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a message code to go with your message before you continue to process this registration.", "Invalid Message Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == "")
			{
				MessageBox.Show("You must input a message to go with your code before you continue processing this registration.", "No Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			fraAdditionalInfo.Visible = false;
		}

		private void cmdErase_Click(object sender, System.EventArgs e)
		{
			cboMessageCodes.SelectedIndex = -1;
			txtMessage.Text = "";
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = false;
			fraDataInput.Enabled = true;
			txtMake.Focus();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdMoreInfo_Click(object sender, System.EventArgs e)
		{
			fraAdditionalInfo.Visible = true;
			cmbNo.Focus();
		}

		private void CmdQuit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			CalculateTotal();
			txtRate.Text = Strings.Format(txtFEERegFee.Text, "#,##0.00");
			txtCredit.Text = Strings.Format(txtFEECreditTransfer.Text, "#,##0.00");
			txtFees.Text = Strings.Format(FCConvert.ToDecimal(txtFEETotal.Text) - FCConvert.ToDecimal(txtFEEAgentFeeReg.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCTA.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCorrection.Text) - FCConvert.ToDecimal(txtFEETitleFee.Text) - FCConvert.ToDecimal(txtFEERushTitleFee.Text) - FCConvert.ToDecimal(txtFEESalesTax.Text), "#,##0.00");
			cmdExit_Click();
		}

		private void CmdSave_Click(object sender, System.EventArgs e)
		{
			mnuProcessSave_Click();
		}

		private void frmVehicleUpdateLongTerm_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.VEHICLEUPDATE))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			frmPlateInfo.InstancePtr.Unload();
			this.Refresh();
		}

		private void frmVehicleUpdateLongTerm_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				if (!(FCGlobal.Screen.ActiveControl is FCComboBox) && !(FCGlobal.Screen.ActiveControl is FCCheckBox) && !(FCGlobal.Screen.ActiveControl is FCRadioButton))
				{
					if (((FCGlobal.Screen.ActiveControl as T2KBackFillWhole).MaxLength == (FCGlobal.Screen.ActiveControl as T2KBackFillWhole).SelectionStart && (FCGlobal.Screen.ActiveControl as T2KBackFillWhole).MaxLength != 0) && KeyCode != Keys.Return && KeyCode != Keys.Down && KeyCode != Keys.Up && KeyCode != Keys.Left)
					{
						KeyCode = (Keys)0;
						//App.DoEvents();
						Support.SendKeys("{TAB}", false);
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		private void frmVehicleUpdateLongTerm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVehicleUpdateLongTerm properties;
			//frmVehicleUpdateLongTerm.FillStyle	= 0;
			//frmVehicleUpdateLongTerm.ScaleWidth	= 9045;
			//frmVehicleUpdateLongTerm.ScaleHeight	= 7350;
			//frmVehicleUpdateLongTerm.LinkTopic	= "Form2";
			//frmVehicleUpdateLongTerm.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper defRS = new clsDRWrapper();
			clsDRWrapper rsResCode = new clsDRWrapper();
			clsDRWrapper rsFlInfo = new clsDRWrapper();
			int counter;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetCustomFormColors();
			chk25YearPlate.Enabled = MotorVehicle.Statics.gintLongTermYearsAllowed == 24;
			defRS.OpenRecordset("SELECT * FROM DefaultInfo");
			// Set Defaults
			SetUserFields();
			FillExtraInfo();
			if (MotorVehicle.Statics.rsPlateForReg.EndOfFile() != true && MotorVehicle.Statics.rsPlateForReg.BeginningOfFile() != true)
			{
				// found a record fill screen
				bolAddNewRecord = false;
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Status")) == "")
				{
					txtStatus.Text = "A";
				}
				else
				{
					txtStatus.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Status"));
				}
				txtOpID.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OpID"));
				txtExpireDate.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExpireDate"), "MM/dd/yyyy");
				txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("EffectiveDate"), "MM/dd/yyyy");
				chkOverrideFleetEmail.Visible = false;
				chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
				txtEMail.Enabled = true;
				imgFleetComment.Visible = false;
				mnuFleetGroupComment.Enabled = false;
				txtFleetNumber.Enabled = false;
				if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber")))
				{
					if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber")) != 0)
					{
						rsFlInfo.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = 0 AND FleetNumber = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber"));
						if (rsFlInfo.EndOfFile() != true && rsFlInfo.BeginningOfFile() != true)
						{
							rsFlInfo.MoveLast();
							rsFlInfo.MoveFirst();
							if (FCConvert.ToInt32(rsFlInfo.Get_Fields_Int32("ExpiryMonth")) == 99)
							{
								cmbFleet.Text = "Group";
							}
							else
							{
								cmbFleet.Text = "Fleet";
							}
							txtFleetNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber"));
							txtFleetName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFlInfo.Get_Fields("Owner1Name")));
							chkOverrideFleetEmail.Visible = true;
							if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("OverrideFleetEmail")))
							{
								chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								txtEMail.Enabled = false;
							}
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFlInfo.Get_Fields_String("Comment"))) != "")
							{
								imgFleetComment.Visible = true;
							}
							else
							{
								imgFleetComment.Visible = false;
							}
							mnuFleetGroupComment.Enabled = true;
							txtFleetNumber.Enabled = true;
						}
						else
						{
							cmbFleet.Text = "No";
						}
					}
					else
					{
						cmbFleet.Text = "No";
					}
				}
				else
				{
					cmbFleet.Text = "No";
				}
				txtAddress1.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Address"));
				txtCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("City"));
				txtState.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("State"));
				txtZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Zip"));
				txtFeeAgentFeeCorrection.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeCorrection"))), "#,##0.00");
				txtFEEAgentFeeReg.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("AgentFee"), "#,##0.00");
				txtFeeAgentFeeCTA.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeCTA"))), "#,##0.00");
				txtFeeAgentFeeTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeTransfer"))), "#,##0.00");
				txtReg1PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyId1"));
				GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), 1);
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2")) != 0)
				{
					txtReg2PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
					GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"), 2);
				}
				else
				{
					ClearPartyInfo(2);
				}
				// 
				txtLegalResCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Residence"));
				txtLegalResState.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ResidenceState"));
				txtLegalZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("ResidenceCode"));
				txtUnit.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UnitNumber"));
				txtStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Style"));
				txtColor1.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("color1"));
				txtColor2.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("color2"));
				txtMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("make"));
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("Year"));
				txtVIN.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Vin"));
				txtTitle.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("TitleNumber"));
				txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("NetWeight"));
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TitleFee")) == "")
				{
					txtFEETitleFee.Text = Strings.Format(0, "#,##0.00");
				}
				else
				{
					txtFEETitleFee.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TitleFee"), "#,##0.00");
				}
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("RushTitleFee")) == "")
				{
					txtFEERushTitleFee.Text = Strings.Format(0, "#,##0.00");
				}
				else
				{
					txtFEERushTitleFee.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("RushTitleFee"), "#,##0.00");
				}
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("SalesTax")) == "")
				{
					txtFEESalesTax.Text = Strings.Format(0, "#,##0.00");
				}
				else
				{
					txtFEESalesTax.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("SalesTax"), "#,##0.00");
				}
				if (MotorVehicle.Statics.rsPlateForReg.IsFieldNull("RegRateCharge"))
				{
					txtFEERegFee.Text = Strings.Format(0, "#,##0.00");
				}
				else
				{
					txtFEERegFee.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("RegRateCharge"), "#,##0.00");
				}
				if (MotorVehicle.Statics.rsPlateForReg.IsFieldNull("TransferCreditUsed"))
				{
					txtFEECreditTransfer.Text = Strings.Format(0, "#,##0.00");
				}
				else
				{
					txtFEECreditTransfer.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("TransferCreditUsed"), "#,##0.00");
				}
				txtFEETransferFee.Text = Strings.Format(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransferFee"), "#,##0.00");
				txtPlate.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwentyFiveYearPlate")))
				{
					chk25YearPlate.CheckState = Wisej.Web.CheckState.Checked;
				}
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType")) == "RRR")
				{
					txtRegistrationType.Text = "Re-Registration Regular";
				}
				else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "NRR")
				{
					txtRegistrationType.Text = "New-Registration Regular";
				}
				else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "NRT")
				{
					txtRegistrationType.Text = "New-Registration Transfer";
				}
				else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "RRT")
				{
					txtRegistrationType.Text = "Re-Registration Transfer";
				}
				else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "DPS")
				{
					txtRegistrationType.Text = "Duplicate Stickers";
				}
				else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "DPR")
				{
					txtRegistrationType.Text = "Duplicate Registration";
				}
				else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "ECO")
				{
					txtRegistrationType.Text = "E-Correct";
				}
				else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType") == "LPS")
				{
					txtRegistrationType.Text = "Lost Plate Stickers";
				}
				if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType")) != "LPS" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType")) != "DPS" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType")) != "ECO" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType")) != "DPR")
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("Transfer")) == "S")
					{
						txtPlateType.Text = "Same Plate";
					}
					else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("Transfer") == "N")
					{
						txtPlateType.Text = "New Plate";
					}
					else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("Transfer") == "R")
					{
						txtPlateType.Text = "Replacement Plate";
					}
					else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields("Transfer") == "O")
					{
						txtPlateType.Text = "Old Plate";
					}
				}
				txtRegistrationType.Enabled = false;
				txtPlateType.Enabled = false;
			}
			else
			{
				// no record user is adding
				bolAddNewRecord = true;
				txtStatus.Text = "A";
				txtOpID.Text = "";
				txtExpireDate.Text = "";
				txtEffectiveDate.Text = "";
				cmbFleet.Text = "No";
				txtFleetNumber.Enabled = false;
				txtFeeAgentFeeCorrection.Text = "0.00";
				txtFEEAgentFeeReg.Text = "0.00";
				txtFeeAgentFeeCTA.Text = "0.00";
				txtFeeAgentFeeTransfer.Text = "0.00";
				// 
				txtFEETitleFee.Text = Strings.Format(0, "#,##0.00");
				txtFEERushTitleFee.Text = Strings.Format(0, "#,##0.00");
				txtFEESalesTax.Text = Strings.Format(0, "#,##0.00");
				txtFEERegFee.Text = Strings.Format(0, "#,##0.00");
				txtFEECreditTransfer.Text = Strings.Format(0, "#,##0.00");
				txtFEETransferFee.Text = "0.00";
				txtPlate.Text = frmPlateInfo.InstancePtr.txtPlateType[0].Text;
				txtRegistrationType.Enabled = false;
				txtPlateType.Enabled = false;
			}
			CalculateTotal();
			txtRate.Text = Strings.Format(txtFEERegFee.Text, "#,##0.00");
			txtCredit.Text = Strings.Format(txtFEECreditTransfer.Text, "#,##0.00");
			txtFees.Text = Strings.Format(FCConvert.ToDecimal(txtFEETotal.Text) - FCConvert.ToDecimal(txtFEEAgentFeeReg.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCTA.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCorrection.Text) - FCConvert.ToDecimal(txtFEETitleFee.Text) - FCConvert.ToDecimal(txtFEERushTitleFee.Text) - FCConvert.ToDecimal(txtFEESalesTax.Text), "#,##0.00");
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form.  This is where you manually let the class know which controls you want to keep track of
			FillControlInformationClass();
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
		}

		private void frmVehicleUpdateLongTerm_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraAdditionalInfo.Visible == true)
				{
					fraAdditionalInfo.Visible = false;
				}
				else if (fraFeesInfo.Visible == true)
				{
					cmdExit_Click();
				}
				else
				{
					mnuProcessQuit_Click();
				}
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				if (FCGlobal.Screen.ActiveControl is Global.T2KOverTypeBox || FCGlobal.Screen.ActiveControl is FCTextBox)
				{
					KeyAscii = KeyAscii - 32;
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Label27_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = true;
			fraDataInput.Enabled = false;
			txtFEERegFee.Focus();
		}

		private void Label29_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = true;
			fraDataInput.Enabled = false;
			txtFEECreditTransfer.Focus();
		}

		private void Label31_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = true;
			fraDataInput.Enabled = false;
			txtFEEAgentFeeReg.Focus();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			clsDRWrapper rsClass = new clsDRWrapper();
			clsDRWrapper rsLongTerm = new clsDRWrapper();
			int answer;
			bool blnMissing;
			int x;
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "TWGN0000.vb1");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			// Save Data
			if (txtStatus.Text != "T")
			{
				if (txtStatus.Text == "A")
				{
					if (!Information.IsDate(txtExpireDate.Text))
					{
						MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtExpireDate.Focus();
						return;
					}
					else
					{
						if (chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked)
						{
							MotorVehicle.Statics.gintFixedMonthMonth = 12;
						}
						else
						{
							MotorVehicle.Statics.gintFixedMonthMonth = 2;
						}
						if (fecherFoundation.DateAndTime.DateValue(txtExpireDate.Text).Month != MotorVehicle.Statics.gintFixedMonthMonth)
						{
							MessageBox.Show("This type of vehicle requires an expiration date ending in " + Strings.Format(FCConvert.ToString(MotorVehicle.Statics.gintFixedMonthMonth) + "/1/2011", "MMMM") + ".  You must enter a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtExpireDate.Focus();
							return;
						}
					}
					if (!Information.IsDate(txtEffectiveDate.Text))
					{
						MessageBox.Show("You must have a valid effective date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtEffectiveDate.Focus();
						return;
					}
					// vbPorter upgrade warning: NM As int	OnWrite(long)
					int NM = 0;
					NM = FCConvert.ToInt32((fecherFoundation.DateAndTime.DateDiff("m", FCConvert.ToDateTime(txtEffectiveDate.Text), FCConvert.ToDateTime(txtExpireDate.Text))));
					if (NM > (MotorVehicle.Statics.gintLongTermYearsAllowed * 12))
					{
						MessageBox.Show("You must have a valid expiration date before you may save this information.", "Invalid Expiration Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtExpireDate.Focus();
						return;
					}
				}
				if (fecherFoundation.Strings.Trim(txtPlate.Text) == "")
				{
					MessageBox.Show("You must enter a plate number before you may save.", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtPlate.Focus();
					return;
				}
				if (Conversion.Val(fecherFoundation.Strings.Trim(txtReg1PartyID.Text)) == 0)
				{
					MessageBox.Show("You must enter information for the primary registrant befor eyou may save.", "Invalid Primary Registrant", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtReg1PartyID.Focus();
					return;
				}
				if (FCConvert.ToDecimal(txtFees.Text) != 0 && FCConvert.ToDecimal(txtRate.Text) == 0)
				{
					if (MessageBox.Show("You have entered an amount into fees but not into  the rate.  Is this correct?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						txtRate.Focus();
						return;
					}
				}
			}
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillNewValue(this);
			}
			// Thsi function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_3(intTotalNumberOfControls - 1, ref clsControlInfo, ref clsReportChanges);
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("LTT New To System / Update", txtPlate.Text, txtVIN.Text, MotorVehicle.Statics.OpID);
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClass();
			FillControlInformationClassFromGrid();
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillOldValue(this);
			}
			// ----------------------------------------------------------------
			if (bolAddNewRecord == true)
				MotorVehicle.Statics.rsPlateForReg.AddNew();
			else
				MotorVehicle.Statics.rsPlateForReg.Edit();
			if (bolAddNewRecord)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OpID", MotorVehicle.Statics.OpID);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("DateUpdated", DateTime.Today);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("TimeUpdated", fecherFoundation.DateAndTime.TimeOfDay);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OldMVR3", 0);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExcisePaidMVR3", 0);
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Battle", "X");
			if (cmbNo.Text == "Yes")
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Mail", true);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Mail", false);
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("EMailAddress", fecherFoundation.Strings.Trim(txtEMail.Text));
			if (chkOverrideFleetEmail.CheckState == Wisej.Web.CheckState.Checked)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OverrideFleetEmail", true);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OverrideFleetEmail", false);
			}
			if ((cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) != "") || (cboMessageCodes.SelectedIndex == -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == ""))
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("MessageFlag", cboMessageCodes.SelectedIndex + 1);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserMessage", fecherFoundation.Strings.Trim(txtMessage.Text));
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserField1", fecherFoundation.Strings.Trim(txtUserField1.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserReason1", fecherFoundation.Strings.Trim(txtUserReason1.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserField2", fecherFoundation.Strings.Trim(txtUserField2.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserReason2", fecherFoundation.Strings.Trim(txtUserReason2.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserField3", fecherFoundation.Strings.Trim(txtUserField3.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UserReason3", fecherFoundation.Strings.Trim(txtUserReason3.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Status", txtStatus.Text);
			if (txtStatus.Text == "P")
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ETO", true);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ETO", false);
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("plate", txtPlate.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Class", "TL");
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Subclass", "L9");
			if (Conversion.Val(fecherFoundation.Strings.Trim(txtFleetNumber.Text)) == 0)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNumber", 0);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetOld", false);
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNew", false);
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNumber", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtFleetNumber.Text))));
				if (!FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("FleetOld")))
				{
					MotorVehicle.Statics.rsPlateForReg.Set_Fields("FleetNew", true);
				}
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Status", fecherFoundation.Strings.Trim(txtStatus.Text));
			if (!MotorVehicle.Statics.blnSuspended)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Address", txtAddress1.Text);
			}
			else if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Address")))
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("Address", "");
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("City", txtCity.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("State", txtState.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Zip", txtZip.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("OwnerCode1", "C");
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyID1", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg1PartyID.Text))));
			if (Conversion.Val(fecherFoundation.Strings.Trim(txtReg2PartyID.Text)) != 0)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OwnerCode2", "C");
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyID2", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg2PartyID.Text))));
			}
			else
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("OwnerCode2", "N");
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("PartyID2", 0);
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			// 
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Residence", txtLegalResCity.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ResidenceState", txtLegalResState.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ResidenceCode", txtLegalZip.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("UnitNumber", txtUnit.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Style", txtStyle.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("color1", txtColor1.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("color2", txtColor2.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("make", txtMake.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Year", FCConvert.ToString(Conversion.Val(txtYear.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Vin", txtVIN.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(txtNetWeight.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("AgentFee", FCConvert.ToString(Conversion.Val(txtFEEAgentFeeReg.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("AgentFeeCTA", FCConvert.ToString(Conversion.Val(txtFeeAgentFeeCTA.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("AgentFeeTransfer", FCConvert.ToString(Conversion.Val(txtFeeAgentFeeTransfer.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("AgentFeeCorrection", FCConvert.ToString(Conversion.Val(txtFeeAgentFeeCorrection.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TitleFee", FCConvert.ToString(Conversion.Val(txtFEETitleFee.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("RushTitleFee", FCConvert.ToString(Conversion.Val(txtFEERushTitleFee.Text)));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("SalesTax", FCConvert.ToDecimal(txtFEESalesTax.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtFEERegFee.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtFEERegFee.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TransferCreditUsed", FCConvert.ToDecimal(txtFEECreditTransfer.Text));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TransferCreditFull", FCConvert.ToDecimal(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("TransferCreditUsed")));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ExpireDate", txtExpireDate.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("EffectiveDate", txtEffectiveDate.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("CommercialCredit", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("InitialFee", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("outofrotation", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ReservePlate", 0);
			if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ReservePlate") > 0)
			{
				MotorVehicle.Statics.rsPlateForReg.Set_Fields("ReservePlateYN", -1);
			}
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PlateFeeNew", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("PlateFeeReReg", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ReplacementFee", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("StickerFee", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TransferFee", txtFEETransferFee.Text);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("DuplicateRegistrationFee", 0);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("StatePaid", FCConvert.ToDecimal(txtFEETotal.Text) - Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("AgentFee")) - Conversion.Val(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeCTA")) - Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeTransfer")) - Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeCorrection"))));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("LocalPaid", MotorVehicle.Statics.rsPlateForReg.Get_Fields("AgentFee") + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeCTA") + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("AgentFeeTransfer"));
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("ForcedPlate", MotorVehicle.Statics.ForcedPlate);
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TitleNumber", fecherFoundation.Strings.Trim(txtTitle.Text).ToUpper());
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("Leased", "N");
			MotorVehicle.Statics.rsPlateForReg.Set_Fields("TwentyFiveYearPlate", chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked);
			if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields("LongTermTrailerRegKey")) != 0 && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "L9" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")) != "NEW")
			{
				rsLongTerm.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields("LongTermTrailerRegKey"));
				if (rsLongTerm.EndOfFile() != true && rsLongTerm.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsLongTerm.Get_Fields_String("Plate")) == "NEW")
					{
						rsLongTerm.Edit();
						rsLongTerm.Set_Fields("Plate", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
						rsLongTerm.Update();
					}
				}
			}
			MotorVehicle.Statics.rsPlateForReg.Update();
			if (MotorVehicle.Statics.FromInventory)
			{
				frmInventoryStatus.InstancePtr.Show(App.MainForm);
			}
			MotorVehicle.Statics.blnTryCorrectAgain = false;
			MotorVehicle.Statics.blnTryReRegAgain = false;
			frmPlateInfo.InstancePtr.blnCameFromVehicleUpdate = false;
			Close();
			//frmPlateInfo.InstancePtr.Show(App.MainForm);
			frmPlateInfo.InstancePtr.ShowForm();
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void txtAddress1_Enter(object sender, System.EventArgs e)
		{
			txtAddress1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtAddress1_Leave(object sender, System.EventArgs e)
		{
			txtAddress1.BackColor = Color.White;
		}

		private void txtCity_Enter(object sender, System.EventArgs e)
		{
			txtCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCity_Leave(object sender, System.EventArgs e)
		{
			txtCity.BackColor = Color.White;
		}

		private void txtColor1_Enter(object sender, System.EventArgs e)
		{
			txtColor1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtColor1_Leave(object sender, System.EventArgs e)
		{
			txtColor1.BackColor = Color.White;
		}

		private void txtColor2_Enter(object sender, System.EventArgs e)
		{
			txtColor2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtColor2_Leave(object sender, System.EventArgs e)
		{
			txtColor2.BackColor = Color.White;
		}

		private void txtCredit_Enter(object sender, System.EventArgs e)
		{
			txtCredit.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCredit_Leave(object sender, System.EventArgs e)
		{
			txtCredit.BackColor = Color.White;
		}

		private void txtCredit_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtFEECreditTransfer.Text = txtCredit.Text;
		}

		private void txtEMail_Enter(object sender, System.EventArgs e)
		{
			txtEMail.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtEMail_Leave(object sender, System.EventArgs e)
		{
			txtEMail.BackColor = Color.White;
		}

		private void txtExpireDate_Enter(object sender, System.EventArgs e)
		{
			txtExpireDate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtExpireDate_Leave(object sender, System.EventArgs e)
		{
			txtExpireDate.BackColor = SystemColors.Control;
		}

		private void txtFEEAgentFeeReg_Enter(object sender, System.EventArgs e)
		{
			txtFEEAgentFeeReg.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEEAgentFeeReg_Leave(object sender, System.EventArgs e)
		{
			txtFEEAgentFeeReg.BackColor = Color.White;
		}

		private void txtFEEAgentFeeReg_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEEAgentFeeReg.Text))
			{
				txtFEEAgentFeeReg.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEEAgentFeeReg.Text) == "")
			{
				txtFEEAgentFeeReg.Text = "0.00";
			}
		}

		private void txtFeeAgentFeeCTA_Enter(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCTA.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFeeAgentFeeCTA_Leave(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCTA.BackColor = Color.White;
		}

		private void txtFeeAgentFeeCTA_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFeeAgentFeeCTA.Text))
			{
				txtFeeAgentFeeCTA.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFeeAgentFeeCTA.Text) == "")
			{
				txtFeeAgentFeeCTA.Text = "0.00";
			}
		}

		private void txtFeeAgentFeeTransfer_Enter(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeTransfer.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFeeAgentFeeTransfer_Leave(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeTransfer.BackColor = Color.White;
		}

		private void txtFeeAgentFeeTransfer_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFeeAgentFeeTransfer.Text))
			{
				txtFeeAgentFeeTransfer.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFeeAgentFeeTransfer.Text) == "")
			{
				txtFeeAgentFeeTransfer.Text = "0.00";
			}
		}

		private void txtFeeAgentFeeCorrection_Enter(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCorrection.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFeeAgentFeeCorrection_Leave(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCorrection.BackColor = Color.White;
		}

		private void txtFeeAgentFeeCorrection_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFeeAgentFeeCorrection.Text))
			{
				txtFeeAgentFeeCorrection.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFeeAgentFeeCorrection.Text) == "")
			{
				txtFeeAgentFeeCorrection.Text = "0.00";
			}
		}

		private void txtFEECreditTransfer_Enter(object sender, System.EventArgs e)
		{
			txtFEECreditTransfer.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEECreditTransfer_Leave(object sender, System.EventArgs e)
		{
			txtFEECreditTransfer.BackColor = Color.White;
		}

		private void txtFEECreditTransfer_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEECreditTransfer.Text))
			{
				txtFEECreditTransfer.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEECreditTransfer.Text) == "")
			{
				txtFEECreditTransfer.Text = "0.00";
			}
		}

		private void txtFEERegFee_Enter(object sender, System.EventArgs e)
		{
			txtFEERegFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEERegFee_Leave(object sender, System.EventArgs e)
		{
			txtFEERegFee.BackColor = Color.White;
		}

		private void txtFEERegFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEERegFee.Text))
			{
				txtFEERegFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEERegFee.Text) == "")
			{
				txtFEERegFee.Text = "0.00";
			}
		}

		private void txtFees_Enter(object sender, System.EventArgs e)
		{
			txtFees.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFees_Leave(object sender, System.EventArgs e)
		{
			txtFees.BackColor = Color.White;
		}

		private void txtFEESalesTax_Enter(object sender, System.EventArgs e)
		{
			txtFEESalesTax.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEESalesTax_Leave(object sender, System.EventArgs e)
		{
			txtFEESalesTax.BackColor = Color.White;
		}

		private void txtFEESalesTax_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEESalesTax.Text))
			{
				txtFEESalesTax.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEESalesTax.Text) == "")
			{
				txtFEESalesTax.Text = "0.00";
			}
		}

		private void txtFEETitleFee_Enter(object sender, System.EventArgs e)
		{
			txtFEETitleFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEETitleFee_Leave(object sender, System.EventArgs e)
		{
			txtFEETitleFee.BackColor = Color.White;
		}

		private void txtFEETitleFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEETitleFee.Text))
			{
				txtFEETitleFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEETitleFee.Text) == "")
			{
				txtFEETitleFee.Text = "0.00";
			}
		}

		private void txtFEERushTitleFee_Enter(object sender, System.EventArgs e)
		{
			txtFEERushTitleFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEERushTitleFee_Leave(object sender, System.EventArgs e)
		{
			txtFEERushTitleFee.BackColor = Color.White;
		}

		private void txtFEERushTitleFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEERushTitleFee.Text))
			{
				txtFEERushTitleFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEERushTitleFee.Text) == "")
			{
				txtFEERushTitleFee.Text = "0.00";
			}
		}

		private void txtFEETransferFee_Enter(object sender, System.EventArgs e)
		{
			txtFEETransferFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEETransferFee_Leave(object sender, System.EventArgs e)
		{
			txtFEETransferFee.BackColor = Color.White;
		}

		private void txtFEETransferFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEETransferFee.Text))
			{
				txtFEETransferFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEETransferFee.Text) == "")
			{
				txtFEETransferFee.Text = "0.00";
			}
		}

		private void txtLegalResCity_Enter(object sender, System.EventArgs e)
		{
			txtLegalResCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtLegalResCity_Leave(object sender, System.EventArgs e)
		{
			txtLegalResCity.BackColor = Color.White;
		}

		private void txtLegalResState_Enter(object sender, System.EventArgs e)
		{
			txtLegalResState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtLegalResState_Leave(object sender, System.EventArgs e)
		{
			txtLegalResState.BackColor = Color.White;
		}

		private void txtMake_Enter(object sender, System.EventArgs e)
		{
			txtMake.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtMake_Leave(object sender, System.EventArgs e)
		{
			txtMake.BackColor = Color.White;
		}

		private void txtMessage_Enter(object sender, System.EventArgs e)
		{
			txtMessage.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtMessage_Leave(object sender, System.EventArgs e)
		{
			txtMessage.BackColor = Color.White;
		}

		private void txtNetWeight_Enter(object sender, System.EventArgs e)
		{
			txtNetWeight.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtNetWeight_Leave(object sender, System.EventArgs e)
		{
			txtNetWeight.BackColor = Color.White;
		}

		private void txtPlate_Enter(object sender, System.EventArgs e)
		{
			txtPlate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtPlate_Leave(object sender, System.EventArgs e)
		{
			txtPlate.BackColor = Color.White;
		}

		private void txtRate_Enter(object sender, System.EventArgs e)
		{
			txtRate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtRate_Leave(object sender, System.EventArgs e)
		{
			txtRate.BackColor = Color.White;
		}

		private void txtRate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtFEERegFee.Text = txtRate.Text;
		}

		private void txtReg1PartyID_Enter(object sender, System.EventArgs e)
		{
			txtReg1PartyID.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtReg1PartyID_Leave(object sender, System.EventArgs e)
		{
			txtReg1PartyID.BackColor = Color.White;
		}

		private void txtReg2PartyID_Enter(object sender, System.EventArgs e)
		{
			txtReg2PartyID.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtReg2PartyID_Leave(object sender, System.EventArgs e)
		{
			txtReg2PartyID.BackColor = Color.White;
		}

		private void txtState_Enter(object sender, System.EventArgs e)
		{
			txtState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtState_Leave(object sender, System.EventArgs e)
		{
			txtState.BackColor = Color.White;
		}

		private void txtStyle_Enter(object sender, System.EventArgs e)
		{
			txtStyle.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtStyle_Leave(object sender, System.EventArgs e)
		{
			txtStyle.BackColor = Color.White;
		}

		private void txtTitle_Enter(object sender, System.EventArgs e)
		{
			txtTitle.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtTitle_Leave(object sender, System.EventArgs e)
		{
			txtTitle.BackColor = Color.White;
		}

		private void txtUnit_Enter(object sender, System.EventArgs e)
		{
			txtUnit.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUnit_Leave(object sender, System.EventArgs e)
		{
			txtUnit.BackColor = Color.White;
		}

		private void txtUserField1_Enter(object sender, System.EventArgs e)
		{
			txtUserField1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserField1_Leave(object sender, System.EventArgs e)
		{
			txtUserField1.BackColor = Color.White;
		}

		private void txtUserField2_Enter(object sender, System.EventArgs e)
		{
			txtUserField2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserField2_Leave(object sender, System.EventArgs e)
		{
			txtUserField2.BackColor = Color.White;
		}

		private void txtUserField3_Enter(object sender, System.EventArgs e)
		{
			txtUserField3.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserField3_Leave(object sender, System.EventArgs e)
		{
			txtUserField3.BackColor = Color.White;
		}

		private void txtUserReason1_Enter(object sender, System.EventArgs e)
		{
			txtUserReason1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserReason1_Leave(object sender, System.EventArgs e)
		{
			txtUserReason1.BackColor = Color.White;
		}

		private void txtUserReason2_Enter(object sender, System.EventArgs e)
		{
			txtUserReason2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserReason2_Leave(object sender, System.EventArgs e)
		{
			txtUserReason2.BackColor = Color.White;
		}

		private void txtUserReason3_Enter(object sender, System.EventArgs e)
		{
			txtUserReason3.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserReason3_Leave(object sender, System.EventArgs e)
		{
			txtUserReason3.BackColor = Color.White;
		}

		private void txtVIN_Enter(object sender, System.EventArgs e)
		{
			txtVIN.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtVIN_Leave(object sender, System.EventArgs e)
		{
			txtVIN.BackColor = Color.White;
		}

		private void txtYear_Enter(object sender, System.EventArgs e)
		{
			txtYear.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtYear_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtYear_Leave(object sender, System.EventArgs e)
		{
			txtYear.BackColor = Color.White;
		}

		private void txtZip_Enter(object sender, System.EventArgs e)
		{
			txtZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtZip_Leave(object sender, System.EventArgs e)
		{
			txtZip.BackColor = Color.White;
		}

		private void txtLegalZip_Enter(object sender, System.EventArgs e)
		{
			txtLegalZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtLegalZip_Leave(object sender, System.EventArgs e)
		{
			txtLegalZip.BackColor = Color.White;
		}

		private void CalculateTotal()
		{
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(Decimal, int)
			Decimal curTotal;
			if (fecherFoundation.Strings.Trim(txtFEECreditTransfer.Text) == "")
			{
				txtFEECreditTransfer.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEEAgentFeeReg.Text) == "")
			{
				txtFEEAgentFeeReg.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEESalesTax.Text) == "")
			{
				txtFEESalesTax.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEETitleFee.Text) == "")
			{
				txtFEETitleFee.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEERushTitleFee.Text) == "")
			{
				txtFEERushTitleFee.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEERegFee.Text) == "")
			{
				txtFEERegFee.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEETransferFee.Text) == "")
			{
				txtFEETransferFee.Text = "0.00";
			}
			curTotal = FCConvert.ToDecimal(txtFEERegFee.Text) - FCConvert.ToDecimal(txtFEECreditTransfer.Text);
			if (curTotal < 0)
				curTotal = 0;
			curTotal += FCConvert.ToDecimal(txtFEEAgentFeeReg.Text);
			curTotal += FCConvert.ToDecimal(txtFeeAgentFeeCTA.Text);
			curTotal += FCConvert.ToDecimal(txtFeeAgentFeeTransfer.Text);
			curTotal += FCConvert.ToDecimal(txtFeeAgentFeeCorrection.Text);
			curTotal += FCConvert.ToDecimal(txtFEETransferFee.Text);
			curTotal += FCConvert.ToDecimal(txtFEESalesTax.Text);
			curTotal += FCConvert.ToDecimal(txtFEETitleFee.Text);
			curTotal += FCConvert.ToDecimal(txtFEERushTitleFee.Text);
			txtFEETotal.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void SetCustomFormColors()
		{
			Label5.BackColor = Color.White;
			Label6.BackColor = Color.White;
			Label7.BackColor = Color.White;
			Label8.BackColor = Color.White;
			Label9.BackColor = Color.White;
			Label10.BackColor = Color.White;
			Label11.BackColor = Color.White;
			Label12.BackColor = Color.White;
			Label13.BackColor = Color.White;
			Label14.BackColor = Color.White;
			Label15.BackColor = Color.White;
			Label16.BackColor = Color.White;
			Label17.BackColor = Color.White;
			Label18.BackColor = Color.White;
			Label19.BackColor = Color.White;
			Label20.BackColor = Color.White;
			Label21.BackColor = Color.White;
			Label22.BackColor = Color.White;
			Label23.BackColor = Color.White;
			Label24.BackColor = Color.White;
			Label25.BackColor = Color.White;
			Label26.BackColor = Color.White;
			Label27.BackColor = Color.White;
			Label28.BackColor = Color.White;
			Label29.BackColor = Color.White;
			Label30.BackColor = Color.White;
			Label31.BackColor = Color.White;
			lblRegistrant1.BackColor = Color.White;
			lblRegistrant2.BackColor = Color.White;
			Label27.ForeColor = Color.Blue;
			Label29.ForeColor = Color.Blue;
			Label31.ForeColor = Color.Blue;
		}

		private void SetUserFields()
		{
			clsDRWrapper rsDefaults = new clsDRWrapper();
			rsDefaults.OpenRecordset("SELECT * FROM DefaultInfo");
			if (!fecherFoundation.FCUtils.IsNull(rsDefaults.Get_Fields_String("UserLabel1")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("UserLabel1"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField1.Visible = true;
					txtUserField1.Visible = true;
					lblInfo1.Visible = true;
					txtUserReason1.Visible = true;
					lblUserField1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("UserLabel1")));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rsDefaults.Get_Fields_String("UserLabel2")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("UserLabel2"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField2.Visible = true;
					txtUserField2.Visible = true;
					lblInfo2.Visible = true;
					txtUserReason2.Visible = true;
					lblUserField2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("UserLabel2")));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rsDefaults.Get_Fields_String("UserLabel3")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("UserLabel3"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField3.Visible = true;
					txtUserField3.Visible = true;
					lblInfo3.Visible = true;
					txtUserReason3.Visible = true;
					lblUserField3.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaults.Get_Fields_String("UserLabel3")));
				}
			}
			rsDefaults.Reset();
		}

		private void FillExtraInfo()
		{
			if (FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("Mail")))
			{
				cmbNo.Text = "Yes";
			}
			else
			{
				cmbNo.Text = "No";
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("EMailAddress")))
			{
				txtEMail.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("EMailAddress"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserMessage")) && !fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag")))
			{
				if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag")) > 0)
				{
					cboMessageCodes.SelectedIndex = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag")) - 1);
					txtMessage.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserMessage"));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField1")))
			{
				txtUserField1.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField1"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason1")))
			{
				txtUserReason1.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason1"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField2")))
			{
				txtUserField2.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField2"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason2")))
			{
				txtUserReason2.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason2"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField3")))
			{
				txtUserField3.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserField3"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason3")))
			{
				txtUserReason3.Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserReason3"));
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// Thsi function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "cboMessageCodes";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Message Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "chk25YearPlate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "25 Year Plate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "optYes";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.OptionButton;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Mail Reminder";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtAddress1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtCity";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "City";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtColor1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Color 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtColor2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Color 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtCredit";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Excise Credit";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtEffectiveDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Effective Date";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtEmail";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "E-Mail";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtExpireDate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Address 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFeeAgentFeeCorrection";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Correction Agent Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFeeAgentFeeCTA";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Title Agent Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFeeAgentFeeReg";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Registration Agent Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFeeAgentFeeTransfer";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Transfer Agent Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFEECreditTransfer";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Transfer Credit";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtRate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Registration Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFEERushTitleFee";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Rush Title Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFees";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Total Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFEESalesTax";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Sales Tax";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFEETitleFee";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Title Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFEETransferFee";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Transfer Fee";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFleetNumber";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Fleet / Group Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "optFleet";
			clsControlInfo[intTotalNumberOfControls].Index = 0;
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.OptionButton;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Fleet";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "optFleet";
			clsControlInfo[intTotalNumberOfControls].Index = 1;
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.OptionButton;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "No Fleet / Group";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "optFleet";
			clsControlInfo[intTotalNumberOfControls].Index = 2;
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.OptionButton;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Group";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtFleetName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Fleet / Group  Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtLegalResCity";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Legal Residence City";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtLegalResState";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Legal Residence State";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtLegalZip";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Legal Residence Zip";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMake";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Make";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtMessage";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Message";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg1PartyID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Owner 1 Party ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtReg2PartyID";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Owner 2 Party ID";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtNetWeight";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Net Weight";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtPlate";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Plate";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtPlateType";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Plate Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtState";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "State";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtStatus";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Status";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtStyle";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Style";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtTitle";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Title Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUnit";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Unit #";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserField1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Field 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserField2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Field 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserField3";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Field 3";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserReason1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Reason 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserReason2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Reason 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtUserReason3";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "User Reason 3";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtVIN";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "VIN";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtYear";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Year";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
            clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
            clsControlInfo[intTotalNumberOfControls].ControlName = "txtZip";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Zip Code";
			intTotalNumberOfControls += 1;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			// Dim intRows As Integer
			// Dim intCols As Integer
			// 
			// For intRows = 2 To Grid.Rows - 1
			// For intCols = 1 To Grid.Cols - 1
			// If intCols <> 0 And intCols <> CNSTGRIDCOLTAXTYPE And intCols <> 6 And intCols <> 12 And intCols <> CNSTGRIDCOLGROSS And intCols <> CNSTGRIDCOLAUTOID And intCols <> CNSTGRIDCOLCHANGES And intCols <> CNSTGRIDCOLPERIODTAXES And intCols <> CNSTGRIDCOLPERIODDEDS And intCols <> CNSTGRIDCOLEXISTING Then
			// ReDim Preserve clsControlInfo(intTotalNumberOfControls)
			// clsControlInfo(intTotalNumberOfControls).ControlName = "Grid"
			// clsControlInfo(intTotalNumberOfControls).ControlType = FlexGrid
			// clsControlInfo(intTotalNumberOfControls).GridRow = intRows
			// clsControlInfo(intTotalNumberOfControls).GridCol = intCols
			// clsControlInfo(intTotalNumberOfControls).DataDescription = "Row " & intRows & " " & Grid.TextMatrix(1, intCols)
			// intTotalNumberOfControls = intTotalNumberOfControls + 1
			// End If
			// Next
			// Next
		}

		private void cmdReg1Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtReg1PartyID.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtReg1PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 1);
			}
			cmdReg1Edit.Focus();
		}

		private void cmdReg1Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtReg1PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 1);
			}
			cmdReg1Search.Focus();
		}

		private void cmdReg2Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtReg2PartyID.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtReg2PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 2);
			}
			cmdReg2Edit.Focus();
		}

		private void cmdReg2Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtReg2PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 2);
			}
			cmdReg2Search.Focus();
		}

		private void txtReg1PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg1PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtReg1PartyID.Text) && Conversion.Val(txtReg1PartyID.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt16(FCConvert.ToDouble(txtReg1PartyID.Text)), 1);
			}
			else
			{
				ClearPartyInfo(1);
			}
		}

		private void txtReg2PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg2PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtReg2PartyID.Text) && Conversion.Val(txtReg2PartyID.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt16(FCConvert.ToDouble(txtReg2PartyID.Text)), 2);
			}
			else
			{
				ClearPartyInfo(2);
			}
		}

		public void ClearPartyInfo(int intReg)
		{
			switch (intReg)
			{
				case 1:
					{
						lblRegistrant1.Text = "";
						break;
					}
				case 2:
					{
						lblRegistrant2.Text = "";
						break;
					}
				default:
					{
						// do nothing
						break;
					}
			}
			//end switch
		}

		public void GetPartyInfo(int intPartyID, int intReg)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				switch (intReg)
				{
					case 1:
						{
							lblRegistrant1.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
							break;
						}
					case 2:
						{
							lblRegistrant2.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
		}

		private void chkOverrideFleetEmail_CheckedChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (chkOverrideFleetEmail.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtEMail.Enabled = true;
			}
			else
			{
				if (cmbFleet.Text == "Fleet")
				{
					rsTemp.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = false AND Type = 'L' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " AND ExpiryMonth = 99 ORDER BY Owner1Name");
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = false AND Type = 'L' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " AND ExpiryMonth <> 99");
				}
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					txtEMail.Text = FCConvert.ToString(rsTemp.Get_Fields_String("EmailAddress"));
				}
				txtEMail.Enabled = false;
			}
		}

		private void imgFleetComment_Click(object sender, System.EventArgs e)
		{
			mnuFleetGroupComment_Click();
		}

		private void mnuFleetGroupComment_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtFleetNumber.Text) > 0)
			{
				if (!frmCommentOld.InstancePtr.Init("MV", "FleetMaster", "Comment", "FleetNumber", FCConvert.ToInt32(FCConvert.ToDouble(txtFleetNumber.Text)), boolModal: true))
				{
					imgFleetComment.Visible = true;
				}
				else
				{
					imgFleetComment.Visible = false;
				}
			}
		}

		public void mnuFleetGroupComment_Click()
		{
			mnuFleetGroupComment_Click(mnuFleetGroupComment, new System.EventArgs());
		}

		private void optFleet_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						txtFleetNumber.Enabled = true;
						lblFleetName.Enabled = true;
						lblFleetNumber.Enabled = true;
						txtFleetName.Text = "";
						txtFleetNumber.Text = "";
						lblFleetNumber.Text = "Enter Fleet Number  (Enter 'A' to Add/Search)";
						lblFleetName.Text = "Fleet Name";
						break;
					}
				case 1:
					{
						txtFleetNumber.Enabled = false;
						lblFleetName.Enabled = false;
						lblFleetNumber.Enabled = false;
						txtFleetName.Text = "";
						txtFleetNumber.Text = "";
						lblFleetNumber.Text = "Enter Fleet Number  (Enter 'A' to Add/Search)";
						lblFleetName.Text = "Fleet Name";
						break;
					}
				case 2:
					{
						txtFleetNumber.Enabled = true;
						lblFleetName.Enabled = true;
						lblFleetNumber.Enabled = true;
						txtFleetName.Text = "";
						txtFleetNumber.Text = "";
						lblFleetNumber.Text = "Enter Group Number  (Enter 'A' to Add/Search)";
						lblFleetName.Text = "Group Name";
						break;
					}
			}
			//end switch
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbFleet.SelectedIndex;
			optFleet_CheckedChanged(index, sender, e);
		}

		private void txtFleetNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rsFT = new clsDRWrapper();
			DateTime xdate;
			string tempdate = "";
			clsDRWrapper rsClassInfo = new clsDRWrapper();
			clsDRWrapper rsOverride = new clsDRWrapper();
			int intHolder1;
			int intHolder2;
			string strMI = "";
			if (txtFleetNumber.Text == "A")
			{
				MotorVehicle.Statics.bolFromLongTermVehicleUpdate = true;
				frmFleetMaster.InstancePtr.Unload();
				frmFleetMaster.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				MotorVehicle.Statics.bolFromLongTermVehicleUpdate = false;
			}
			if (Conversion.Val(txtFleetNumber.Text) != 0)
			{
				if (cmbFleet.Text == "Group")
				{
					rsFT.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = false AND Type = 'L' AND ExpiryMonth = 99 ORDER BY Owner1Name");
				}
				else
				{
					rsFT.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = false AND Type = 'L' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " AND ExpiryMonth <> 99");
					rsClassInfo.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
					if (rsClassInfo.EndOfFile() != true && rsClassInfo.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rsClassInfo.Get_Fields_String("RenewalDate")) == "F")
						{
							if (Conversion.Val(rsFT.Get_Fields_Int32("ExpiryMonth")) != MotorVehicle.Statics.gintFixedMonthMonth)
							{
								MessageBox.Show("You may not add a vehicle with a fixed renewal date to a fleet that has an expiration month other than the required expiration month", "Invalid Expiration Month", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								txtFleetNumber.Text = "";
								cmbFleet.Text = "No";
								e.Cancel = true;
								return;
							}
						}
					}
				}
				if (rsFT.EndOfFile() != true && rsFT.BeginningOfFile() != true)
				{
					rsFT.MoveLast();
					rsFT.MoveFirst();
					if (!rsFT.FindFirstRecord("FleetNumber", Conversion.Val(txtFleetNumber.Text)))
					{
						if (cmbFleet.Text == "Group")
						{
							MessageBox.Show("There was no match found for Group Master Number " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Invalid Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							MessageBox.Show("There was no match found for Fleet Master Number " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Invalid Fleet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else
					{
						txtFleetName.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFT.Get_Fields("Party1FullName")));
						if (MessageBox.Show("Would you like to update the name and address on this registration to match the fleet / group info?", "Update Info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							if (!MotorVehicle.Statics.blnSuspended)
							{
								if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Address")))
									txtAddress1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Address")));
							}
							else
							{
								txtAddress1.Text = "REGISTRATION SUSPENDED";
							}
							if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("City")))
								txtCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("City")));
							if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields("State")))
								txtState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields("State")));
							if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Zip")))
							{
								if (FCConvert.ToString(rsFT.Get_Fields_String("Zip")).Length < 5)
								{
									txtZip.Text = "0" + rsFT.Get_Fields_String("Zip");
								}
								else
								{
									txtZip.Text = FCConvert.ToString(rsFT.Get_Fields_String("Zip"));
								}
							}
							txtReg1PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID1"));
							lblRegistrant1.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFT.Get_Fields("Party1FullName")));
							if (Conversion.Val(rsFT.Get_Fields_Int32("PartyID2")) != 0)
							{
								txtReg2PartyID.Text = FCConvert.ToString(rsFT.Get_Fields("Party2ID"));
								lblRegistrant2.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFT.Get_Fields("Party2FullName")));
							}
							else
							{
								ClearPartyInfo(2);
							}
							txtEMail.Text = FCConvert.ToString(rsFT.Get_Fields_String("EmailAddress"));
							txtEMail.Enabled = false;
							chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
							chkOverrideFleetEmail.Visible = true;
							if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Comment"))) != "")
							{
								imgFleetComment.Visible = true;
							}
							else
							{
								imgFleetComment.Visible = false;
							}
							mnuFleetGroupComment.Enabled = true;
						}
					}
				}
				else
				{
					chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
					chkOverrideFleetEmail.Visible = false;
					txtEMail.Enabled = true;
					imgFleetComment.Visible = false;
					mnuFleetGroupComment.Enabled = false;
					if (cmbFleet.Text == "Group")
					{
						MessageBox.Show("There are no Groups.", "No Groups", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("There are no Fleets.", "No Fleets", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
				chkOverrideFleetEmail.Visible = false;
				txtEMail.Text = "";
				txtEMail.Enabled = true;
				imgFleetComment.Visible = false;
				mnuFleetGroupComment.Enabled = false;
			}
		}
	}
}
