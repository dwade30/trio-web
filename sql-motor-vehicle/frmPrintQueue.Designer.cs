//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmPrintQueue.
	/// </summary>
	partial class frmPrintQueue
	{
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCGrid vsPrintQueue;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdClearAll = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.vsPrintQueue = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPrintQueue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 508);
            this.BottomPanel.Size = new System.Drawing.Size(826, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmdClearAll);
            this.ClientArea.Controls.Add(this.cmdSelectAll);
            this.ClientArea.Controls.Add(this.vsPrintQueue);
            this.ClientArea.Size = new System.Drawing.Size(846, 592);
            this.ClientArea.Controls.SetChildIndex(this.vsPrintQueue, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSelectAll, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdClearAll, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(846, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(143, 30);
            this.HeaderText.Text = "Print Queue";
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "actionButton";
            this.cmdPrint.Location = new System.Drawing.Point(312, 468);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(78, 40);
            this.cmdPrint.TabIndex = 3;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdClearAll
            // 
            this.cmdClearAll.AppearanceKey = "actionButton";
            this.cmdClearAll.Location = new System.Drawing.Point(174, 468);
            this.cmdClearAll.Name = "cmdClearAll";
            this.cmdClearAll.Size = new System.Drawing.Size(108, 40);
            this.cmdClearAll.TabIndex = 2;
            this.cmdClearAll.Text = "Clear All";
            this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.AppearanceKey = "actionButton";
            this.cmdSelectAll.Location = new System.Drawing.Point(30, 468);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(114, 40);
            this.cmdSelectAll.TabIndex = 1;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // vsPrintQueue
            // 
            this.vsPrintQueue.Cols = 13;
            this.vsPrintQueue.ExtendLastCol = true;
            this.vsPrintQueue.Location = new System.Drawing.Point(30, 30);
            this.vsPrintQueue.Name = "vsPrintQueue";
            this.vsPrintQueue.Rows = 50;
            this.vsPrintQueue.ShowFocusCell = false;
            this.vsPrintQueue.Size = new System.Drawing.Size(768, 418);
            this.vsPrintQueue.Click += new System.EventHandler(this.vsPrintQueue_ClickEvent);
            this.vsPrintQueue.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPrintQueue_KeyDownEvent);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(334, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(130, 48);
            this.cmdProcessSave.Text = "Save & Exit";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmPrintQueue
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(846, 652);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPrintQueue";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Print Queue";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmPrintQueue_Load);
            this.Activated += new System.EventHandler(this.frmPrintQueue_Activated);
            this.Resize += new System.EventHandler(this.frmPrintQueue_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintQueue_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPrintQueue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdProcessSave;
    }
}