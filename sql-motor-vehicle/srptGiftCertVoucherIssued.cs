//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptGiftCertVoucherIssued.
	/// </summary>
	public partial class srptGiftCertVoucherIssued : FCSectionReport
	{
		public srptGiftCertVoucherIssued()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Title Application Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SrptGiftCertVoucherIssued_ReportEnd;
		}

        private void SrptGiftCertVoucherIssued_ReportEnd(object sender, EventArgs e)
        {
            rsAM.DisposeOf();
        }

        public static srptGiftCertVoucherIssued InstancePtr
		{
			get
			{
				return (srptGiftCertVoucherIssued)Sys.GetInstance(typeof(srptGiftCertVoucherIssued));
			}
		}

		protected srptGiftCertVoucherIssued _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptGiftCertVoucherIssued	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double MoneyTotal;
		int total;
		clsDRWrapper rsAM = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord == true)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				rsAM.MoveNext();
				if (rsAM.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rsAM.OpenRecordset("SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID < 1 AND Issued = 1 ORDER BY CertificateNumber");
			}
			else
			{
				rsAM.OpenRecordset("SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID > " + FCConvert.ToString(rptGiftCertVoucher.InstancePtr.lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(rptGiftCertVoucher.InstancePtr.lngPK2) + " AND Issued = 1 ORDER BY CertificateNumber");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			total += 1;
			MoneyTotal += FCConvert.ToDouble(rsAM.Get_Fields("Amount"));
			txtCertificate.Text = Strings.Format(rsAM.Get_Fields_String("CertificateNumber"), "!@@@@@@@@@@");
			txtAmount.Text = Strings.Format(rsAM.Get_Fields("Amount"), "#,##0.00");
			txtDT.Text = Strings.Format(rsAM.Get_Fields_DateTime("DateIssued"), "MM/dd/yyyy");
			txtCustomer.Text = Strings.Format(rsAM.Get_Fields_String("Name"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			txtOPID.Text = Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@");
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			lblTotals.Text = "Totals";
			txtTotals.Text = Strings.Format(Strings.Format(MoneyTotal, "#,##0.00"), "@@@@@@");
			txtCLTotal.Text = Strings.Format(total, "@@");
		}

		private void srptGiftCertVoucherIssued_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptGiftCertVoucherIssued properties;
			//srptGiftCertVoucherIssued.Caption	= "Title Application Report";
			//srptGiftCertVoucherIssued.Icon	= "srptGiftCertVoucherIssued.dsx":0000";
			//srptGiftCertVoucherIssued.Left	= 0;
			//srptGiftCertVoucherIssued.Top	= 0;
			//srptGiftCertVoucherIssued.Width	= 11880;
			//srptGiftCertVoucherIssued.Height	= 8595;
			//srptGiftCertVoucherIssued.StartUpPosition	= 3;
			//srptGiftCertVoucherIssued.SectionData	= "srptGiftCertVoucherIssued.dsx":058A;
			//End Unmaped Properties
		}
	}
}
