//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmFleetGroupLabels.
	/// </summary>
	partial class frmFleetGroupLabels
	{
		public fecherFoundation.FCComboBox cmbFleetNameSelected;
		public fecherFoundation.FCComboBox cmbDateRangeAll;
		public fecherFoundation.FCComboBox cmbFleetTypeAll;
		public fecherFoundation.FCLabel lblFleetTypeAll;
		public fecherFoundation.FCComboBox cmbLongTerm;
		public fecherFoundation.FCComboBox cmbStatusSelect;
		public fecherFoundation.FCComboBox cmbFleet;
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCComboBox cmbSelected;
		public fecherFoundation.FCLabel lblSelected;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraFleetNameSelected;
		public fecherFoundation.FCComboBox cboEndName;
		public fecherFoundation.FCComboBox cboStartName;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraDateRangeSelected;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboLabelType;
		public fecherFoundation.FCLabel lblLabelDescription;
		public fecherFoundation.FCGrid vsFleets;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbFleetNameSelected = new fecherFoundation.FCComboBox();
			this.cmbDateRangeAll = new fecherFoundation.FCComboBox();
			this.cmbFleetTypeAll = new fecherFoundation.FCComboBox();
			this.lblFleetTypeAll = new fecherFoundation.FCLabel();
			this.cmbLongTerm = new fecherFoundation.FCComboBox();
			this.cmbStatusSelect = new fecherFoundation.FCComboBox();
			this.cmbFleet = new fecherFoundation.FCComboBox();
			this.cmbName = new fecherFoundation.FCComboBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.cmbSelected = new fecherFoundation.FCComboBox();
			this.lblSelected = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.fraFleetNameSelected = new fecherFoundation.FCFrame();
			this.cboEndName = new fecherFoundation.FCComboBox();
			this.cboStartName = new fecherFoundation.FCComboBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmdClearAll = new fecherFoundation.FCButton();
			this.cmdSelectAll = new fecherFoundation.FCButton();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraDateRangeSelected = new fecherFoundation.FCFrame();
			this.txtStartDate = new Global.T2KDateBox();
			this.txtEndDate = new Global.T2KDateBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraType = new fecherFoundation.FCFrame();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboLabelType = new fecherFoundation.FCComboBox();
			this.lblLabelDescription = new fecherFoundation.FCLabel();
			this.vsFleets = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraFleetNameSelected)).BeginInit();
			this.fraFleetNameSelected.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRangeSelected)).BeginInit();
			this.fraDateRangeSelected.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
			this.fraType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsFleets)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(870, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblFleetTypeAll);
			this.ClientArea.Controls.Add(this.cmbFleetTypeAll);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmbLongTerm);
			this.ClientArea.Controls.Add(this.fraType);
			this.ClientArea.Controls.Add(this.cmbName);
			this.ClientArea.Controls.Add(this.lblName);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.cmbSelected);
			this.ClientArea.Controls.Add(this.lblSelected);
			this.ClientArea.Controls.Add(this.vsFleets);
			this.ClientArea.Size = new System.Drawing.Size(870, 520);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClearAll);
			this.TopPanel.Controls.Add(this.cmdSelectAll);
			this.TopPanel.Size = new System.Drawing.Size(870, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.cmdSelectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClearAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(235, 30);
			this.HeaderText.Text = "Fleet / Group Labels";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbFleetNameSelected
			// 
			this.cmbFleetNameSelected.AutoSize = false;
			this.cmbFleetNameSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFleetNameSelected.FormattingEnabled = true;
			this.cmbFleetNameSelected.Items.AddRange(new object[] {
            "Selected",
            "All"});
			this.cmbFleetNameSelected.Location = new System.Drawing.Point(14, 32);
			this.cmbFleetNameSelected.Name = "cmbFleetNameSelected";
			this.cmbFleetNameSelected.Size = new System.Drawing.Size(255, 40);
			this.cmbFleetNameSelected.TabIndex = 36;
			this.cmbFleetNameSelected.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbFleetNameSelected, null);
			this.cmbFleetNameSelected.SelectedIndexChanged += new System.EventHandler(this.cmbFleetNameSelected_SelectedIndexChanged);
			// 
			// cmbDateRangeAll
			// 
			this.cmbDateRangeAll.AutoSize = false;
			this.cmbDateRangeAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDateRangeAll.FormattingEnabled = true;
			this.cmbDateRangeAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
			this.cmbDateRangeAll.Location = new System.Drawing.Point(20, 30);
			this.cmbDateRangeAll.Name = "cmbDateRangeAll";
			this.cmbDateRangeAll.Size = new System.Drawing.Size(287, 40);
			this.cmbDateRangeAll.TabIndex = 26;
			this.cmbDateRangeAll.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbDateRangeAll, null);
			this.cmbDateRangeAll.SelectedIndexChanged += new System.EventHandler(this.cmbDateRangeAll_SelectedIndexChanged);
			// 
			// cmbFleetTypeAll
			// 
			this.cmbFleetTypeAll.AutoSize = false;
			this.cmbFleetTypeAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFleetTypeAll.FormattingEnabled = true;
			this.cmbFleetTypeAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
			this.cmbFleetTypeAll.Location = new System.Drawing.Point(392, 242);
			this.cmbFleetTypeAll.Name = "cmbFleetTypeAll";
			this.cmbFleetTypeAll.Size = new System.Drawing.Size(146, 40);
			this.cmbFleetTypeAll.TabIndex = 0;
			this.cmbFleetTypeAll.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbFleetTypeAll, null);
			this.cmbFleetTypeAll.SelectedIndexChanged += new System.EventHandler(this.cmbFleetTypeAll_SelectedIndexChanged);
			// 
			// lblFleetTypeAll
			// 
			this.lblFleetTypeAll.AutoSize = true;
			this.lblFleetTypeAll.Location = new System.Drawing.Point(392, 211);
			this.lblFleetTypeAll.Name = "lblFleetTypeAll";
			this.lblFleetTypeAll.Size = new System.Drawing.Size(155, 15);
			this.lblFleetTypeAll.TabIndex = 0;
			this.lblFleetTypeAll.Text = "FLEET TYPE SELECTION";
			this.ToolTip1.SetToolTip(this.lblFleetTypeAll, null);
			// 
			// cmbLongTerm
			// 
			this.cmbLongTerm.AutoSize = false;
			this.cmbLongTerm.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLongTerm.FormattingEnabled = true;
			this.cmbLongTerm.Items.AddRange(new object[] {
            "Long Term",
            "Annual"});
			this.cmbLongTerm.Location = new System.Drawing.Point(392, 302);
			this.cmbLongTerm.Name = "cmbLongTerm";
			this.cmbLongTerm.Size = new System.Drawing.Size(146, 40);
			this.cmbLongTerm.TabIndex = 33;
			this.cmbLongTerm.Text = "Annual";
			this.ToolTip1.SetToolTip(this.cmbLongTerm, null);
			this.cmbLongTerm.SelectedIndexChanged += new System.EventHandler(this.cmbLongTerm_SelectedIndexChanged);
			// 
			// cmbStatusSelect
			// 
			this.cmbStatusSelect.AutoSize = false;
			this.cmbStatusSelect.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbStatusSelect.FormattingEnabled = true;
			this.cmbStatusSelect.Items.AddRange(new object[] {
            "Selected",
            "All"});
			this.cmbStatusSelect.Location = new System.Drawing.Point(20, 30);
			this.cmbStatusSelect.Name = "cmbStatusSelect";
			this.cmbStatusSelect.Size = new System.Drawing.Size(120, 40);
			this.cmbStatusSelect.TabIndex = 2;
			this.cmbStatusSelect.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbStatusSelect, null);
			this.cmbStatusSelect.SelectedIndexChanged += new System.EventHandler(this.cmbStatusSelect_SelectedIndexChanged);
			// 
			// cmbFleet
			// 
			this.cmbFleet.AutoSize = false;
			this.cmbFleet.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFleet.Enabled = false;
			this.cmbFleet.FormattingEnabled = true;
			this.cmbFleet.Items.AddRange(new object[] {
            "Fleets",
            "Groups"});
			this.cmbFleet.Location = new System.Drawing.Point(20, 90);
			this.cmbFleet.Name = "cmbFleet";
			this.cmbFleet.Size = new System.Drawing.Size(120, 40);
			this.cmbFleet.TabIndex = 0;
			this.cmbFleet.Text = "Fleets";
			this.ToolTip1.SetToolTip(this.cmbFleet, null);
			this.cmbFleet.SelectedIndexChanged += new System.EventHandler(this.cmbFleet_SelectedIndexChanged);
			// 
			// cmbName
			// 
			this.cmbName.AutoSize = false;
			this.cmbName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbName.FormattingEnabled = true;
			this.cmbName.Items.AddRange(new object[] {
            "Name",
            "Number"});
			this.cmbName.Location = new System.Drawing.Point(558, 242);
			this.cmbName.Name = "cmbName";
			this.cmbName.Size = new System.Drawing.Size(146, 40);
			this.cmbName.TabIndex = 35;
			this.cmbName.Text = "Name";
			this.ToolTip1.SetToolTip(this.cmbName, null);
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(558, 211);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(72, 15);
			this.lblName.TabIndex = 36;
			this.lblName.Text = "ORDER BY";
			this.ToolTip1.SetToolTip(this.lblName, null);
			// 
			// cmbSelected
			// 
			this.cmbSelected.AutoSize = false;
			this.cmbSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSelected.FormattingEnabled = true;
			this.cmbSelected.Items.AddRange(new object[] {
            "Selected",
            "Complete List"});
			this.cmbSelected.Location = new System.Drawing.Point(30, 241);
			this.cmbSelected.Name = "cmbSelected";
			this.cmbSelected.Size = new System.Drawing.Size(163, 40);
			this.cmbSelected.TabIndex = 37;
			this.cmbSelected.Text = "Complete List";
			this.ToolTip1.SetToolTip(this.cmbSelected, null);
			this.cmbSelected.SelectedIndexChanged += new System.EventHandler(this.cmbSelected_SelectedIndexChanged);
			// 
			// lblSelected
			// 
			this.lblSelected.AutoSize = true;
			this.lblSelected.Location = new System.Drawing.Point(30, 211);
			this.lblSelected.Name = "lblSelected";
			this.lblSelected.Size = new System.Drawing.Size(105, 15);
			this.lblSelected.TabIndex = 38;
			this.lblSelected.Text = "LABEL OPTIONS";
			this.ToolTip1.SetToolTip(this.lblSelected, null);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.fraFleetNameSelected);
			this.Frame3.Controls.Add(this.cmbFleetNameSelected);
			this.Frame3.Location = new System.Drawing.Point(527, 382);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(293, 152);
			this.Frame3.TabIndex = 32;
			this.Frame3.Text = "Fleet Name Select";
			this.ToolTip1.SetToolTip(this.Frame3, null);
			this.Frame3.Visible = false;
			// 
			// fraFleetNameSelected
			// 
			this.fraFleetNameSelected.AppearanceKey = "groupBoxNoBorders";
			this.fraFleetNameSelected.Controls.Add(this.cboEndName);
			this.fraFleetNameSelected.Controls.Add(this.cboStartName);
			this.fraFleetNameSelected.Controls.Add(this.Label2);
			this.fraFleetNameSelected.Enabled = false;
			this.fraFleetNameSelected.Location = new System.Drawing.Point(1, 72);
			this.fraFleetNameSelected.Name = "fraFleetNameSelected";
			this.fraFleetNameSelected.Size = new System.Drawing.Size(286, 74);
			this.fraFleetNameSelected.TabIndex = 35;
			this.ToolTip1.SetToolTip(this.fraFleetNameSelected, null);
			// 
			// cboEndName
			// 
			this.cboEndName.AutoSize = false;
			this.cboEndName.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndName.Enabled = false;
			this.cboEndName.FormattingEnabled = true;
			this.cboEndName.Location = new System.Drawing.Point(168, 19);
			this.cboEndName.Name = "cboEndName";
			this.cboEndName.Size = new System.Drawing.Size(100, 40);
			this.cboEndName.TabIndex = 37;
			this.ToolTip1.SetToolTip(this.cboEndName, null);
			this.cboEndName.SelectedIndexChanged += new System.EventHandler(this.cboEndName_SelectedIndexChanged);
			// 
			// cboStartName
			// 
			this.cboStartName.AutoSize = false;
			this.cboStartName.BackColor = System.Drawing.SystemColors.Window;
			this.cboStartName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStartName.Enabled = false;
			this.cboStartName.FormattingEnabled = true;
			this.cboStartName.Location = new System.Drawing.Point(10, 19);
			this.cboStartName.Name = "cboStartName";
			this.cboStartName.Size = new System.Drawing.Size(100, 40);
			this.cboStartName.TabIndex = 36;
			this.ToolTip1.SetToolTip(this.cboStartName, null);
			this.cboStartName.SelectedIndexChanged += new System.EventHandler(this.cboStartName_SelectedIndexChanged);
			// 
			// Label2
			// 
			this.Label2.Enabled = false;
			this.Label2.Location = new System.Drawing.Point(132, 33);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(21, 22);
			this.Label2.TabIndex = 38;
			this.Label2.Text = "TO";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// cmdClearAll
			// 
			this.cmdClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClearAll.AppearanceKey = "toolbarButton";
			this.cmdClearAll.Location = new System.Drawing.Point(771, 29);
			this.cmdClearAll.Name = "cmdClearAll";
			this.cmdClearAll.Size = new System.Drawing.Size(71, 24);
			this.cmdClearAll.TabIndex = 31;
			this.cmdClearAll.Text = "Clear All";
			this.ToolTip1.SetToolTip(this.cmdClearAll, null);
			this.cmdClearAll.Visible = false;
			this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
			// 
			// cmdSelectAll
			// 
			this.cmdSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSelectAll.AppearanceKey = "toolbarButton";
			this.cmdSelectAll.Location = new System.Drawing.Point(691, 29);
			this.cmdSelectAll.Name = "cmdSelectAll";
			this.cmdSelectAll.Size = new System.Drawing.Size(74, 24);
			this.cmdSelectAll.TabIndex = 30;
			this.cmdSelectAll.Text = "Select All";
			this.ToolTip1.SetToolTip(this.cmdSelectAll, null);
			this.cmdSelectAll.Visible = false;
			this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.fraDateRangeSelected);
			this.Frame1.Controls.Add(this.cmbDateRangeAll);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(390, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(327, 165);
			this.Frame1.TabIndex = 22;
			this.Frame1.Text = "Vehicle Expiration Selection";
			this.ToolTip1.SetToolTip(this.Frame1, "If a date range is specified labels will only be printed for selected fleets / gr" +
        "oups if they contain at least 1 vehicle expiring within the date range selected." +
        "");
			// 
			// fraDateRangeSelected
			// 
			this.fraDateRangeSelected.AppearanceKey = "groupBoxNoBorders";
			this.fraDateRangeSelected.Controls.Add(this.txtStartDate);
			this.fraDateRangeSelected.Controls.Add(this.txtEndDate);
			this.fraDateRangeSelected.Controls.Add(this.lblTo);
			this.fraDateRangeSelected.Enabled = false;
			this.fraDateRangeSelected.Location = new System.Drawing.Point(1, 71);
			this.fraDateRangeSelected.Name = "fraDateRangeSelected";
			this.fraDateRangeSelected.Size = new System.Drawing.Size(309, 77);
			this.fraDateRangeSelected.TabIndex = 25;
			this.ToolTip1.SetToolTip(this.fraDateRangeSelected, null);
			// 
			// txtStartDate
			// 
			this.txtStartDate.Location = new System.Drawing.Point(19, 19);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.SelLength = 0;
			this.txtStartDate.SelStart = 0;
			this.txtStartDate.Size = new System.Drawing.Size(115, 40);
			this.txtStartDate.TabIndex = 26;
			this.txtStartDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtStartDate, null);
			// 
			// txtEndDate
			// 
			this.txtEndDate.Location = new System.Drawing.Point(191, 19);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.SelLength = 0;
			this.txtEndDate.SelStart = 0;
			this.txtEndDate.Size = new System.Drawing.Size(115, 40);
			this.txtEndDate.TabIndex = 27;
			this.txtEndDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtEndDate, null);
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(151, 33);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(21, 22);
			this.lblTo.TabIndex = 28;
			this.lblTo.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 71);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(290, 77);
			this.Label1.TabIndex = 29;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// fraType
			// 
			this.fraType.Controls.Add(this.cmbFleet);
			this.fraType.Controls.Add(this.cmbStatusSelect);
			this.fraType.Location = new System.Drawing.Point(212, 211);
			this.fraType.Name = "fraType";
			this.fraType.Size = new System.Drawing.Size(160, 151);
			this.fraType.TabIndex = 9;
			this.fraType.Text = "Type Selection";
			this.ToolTip1.SetToolTip(this.fraType, null);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboLabelType);
			this.Frame2.Controls.Add(this.lblLabelDescription);
			this.Frame2.Location = new System.Drawing.Point(30, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(343, 165);
			this.Frame2.TabIndex = 3;
			this.Frame2.Text = "Select Label Type";
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// cboLabelType
			// 
			this.cboLabelType.AutoSize = false;
			this.cboLabelType.BackColor = System.Drawing.SystemColors.Window;
			this.cboLabelType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboLabelType.FormattingEnabled = true;
			this.cboLabelType.Location = new System.Drawing.Point(20, 30);
			this.cboLabelType.Name = "cboLabelType";
			this.cboLabelType.Size = new System.Drawing.Size(303, 40);
			this.cboLabelType.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.cboLabelType, null);
			this.cboLabelType.SelectedIndexChanged += new System.EventHandler(this.cboLabelType_SelectedIndexChanged);
			// 
			// lblLabelDescription
			// 
			this.lblLabelDescription.Location = new System.Drawing.Point(20, 90);
			this.lblLabelDescription.Name = "lblLabelDescription";
			this.lblLabelDescription.Size = new System.Drawing.Size(303, 52);
			this.lblLabelDescription.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.lblLabelDescription, null);
			// 
			// vsFleets
			// 
			this.vsFleets.AllowSelection = false;
			this.vsFleets.AllowUserToResizeColumns = false;
			this.vsFleets.AllowUserToResizeRows = false;
			this.vsFleets.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsFleets.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsFleets.BackColorBkg = System.Drawing.Color.Empty;
			this.vsFleets.BackColorFixed = System.Drawing.Color.Empty;
			this.vsFleets.BackColorSel = System.Drawing.Color.Empty;
			this.vsFleets.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsFleets.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsFleets.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsFleets.ColumnHeadersHeight = 30;
			this.vsFleets.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsFleets.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsFleets.DragIcon = null;
			this.vsFleets.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsFleets.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsFleets.ExtendLastCol = true;
			this.vsFleets.FixedCols = 0;
			this.vsFleets.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsFleets.FrozenCols = 0;
			this.vsFleets.GridColor = System.Drawing.Color.Empty;
			this.vsFleets.GridColorFixed = System.Drawing.Color.Empty;
			this.vsFleets.Location = new System.Drawing.Point(30, 382);
			this.vsFleets.Name = "vsFleets";
			this.vsFleets.OutlineCol = 0;
			this.vsFleets.ReadOnly = true;
			this.vsFleets.RowHeadersVisible = false;
			this.vsFleets.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsFleets.RowHeightMin = 0;
			this.vsFleets.Rows = 17;
			this.vsFleets.ScrollTipText = null;
			this.vsFleets.ShowColumnVisibilityMenu = false;
			this.vsFleets.ShowFocusCell = false;
			this.vsFleets.Size = new System.Drawing.Size(478, 208);
			this.vsFleets.StandardTab = true;
			this.vsFleets.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsFleets.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsFleets.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.vsFleets, null);
			this.vsFleets.Visible = false;
			this.vsFleets.KeyDown += new Wisej.Web.KeyEventHandler(this.vsFleets_KeyDownEvent);
			this.vsFleets.Click += new System.EventHandler(this.vsFleets_ClickEvent);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(365, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(78, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmFleetGroupLabels
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(870, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmFleetGroupLabels";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Fleet / Group Labels";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmFleetGroupLabels_Load);
			this.Activated += new System.EventHandler(this.frmFleetGroupLabels_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFleetGroupLabels_KeyPress);
			this.Resize += new System.EventHandler(this.frmFleetGroupLabels_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraFleetNameSelected)).EndInit();
			this.fraFleetNameSelected.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDateRangeSelected)).EndInit();
			this.fraDateRangeSelected.ResumeLayout(false);
			this.fraDateRangeSelected.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
			this.fraType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsFleets)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}