﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Models;

namespace TWMV0000.RegistrantInfo
{
    public class EditRegistrantNameHandler : CommandHandler<EditRegistrantName, RegistrantNameEditInfo>
    {
        private IModalView<IEditRegistrantNameViewModel> editRegistrantNameView;
        public EditRegistrantNameHandler(IModalView<IEditRegistrantNameViewModel> editRegistrantNameView)
        {
            this.editRegistrantNameView = editRegistrantNameView;
        }
        protected override RegistrantNameEditInfo Handle(EditRegistrantName command)
        {
            editRegistrantNameView.ViewModel.RegistrantInfo = command.CurrentInfo;
            editRegistrantNameView.ShowModal();
            return editRegistrantNameView.ViewModel.RegistrantInfo;
        }
    }
}
