﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTellerID.
	/// </summary>
	partial class frmEditRegistrantName : BaseForm
	{

		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdSave = new fecherFoundation.FCButton();
            this.RegistrantNamePanel = new Wisej.Web.FlexLayoutPanel();
            this.FirstNamePanel = new Wisej.Web.TableLayoutPanel();
            this.txtFirstName = new Wisej.Web.TextBox();
            this.lblFirstName = new Wisej.Web.Label();
            this.MiddleNamePanel = new Wisej.Web.TableLayoutPanel();
            this.txtMiddleInitial = new Wisej.Web.TextBox();
            this.lblMiddleInitial = new Wisej.Web.Label();
            this.LastNamePanel = new Wisej.Web.TableLayoutPanel();
            this.txtLastName = new Wisej.Web.TextBox();
            this.lblLastName = new Wisej.Web.Label();
            this.DesignationPanel = new Wisej.Web.TableLayoutPanel();
            this.txtDesignation = new Wisej.Web.TextBox();
            this.lblDesignation = new Wisej.Web.Label();
            this.CompanyNamePanel = new Wisej.Web.TableLayoutPanel();
            this.txtCompanyName = new Wisej.Web.TextBox();
            this.lblCompanyName = new Wisej.Web.Label();
            this.btnCancel = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.RegistrantNamePanel.SuspendLayout();
            this.FirstNamePanel.SuspendLayout();
            this.MiddleNamePanel.SuspendLayout();
            this.LastNamePanel.SuspendLayout();
            this.DesignationPanel.SuspendLayout();
            this.CompanyNamePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnCancel);
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 220);
            this.BottomPanel.Size = new System.Drawing.Size(437, 95);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.ClientArea.Controls.Add(this.RegistrantNamePanel);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(457, 321);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.RegistrantNamePanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Dock = Wisej.Web.DockStyle.None;
            this.TopPanel.Size = new System.Drawing.Size(457, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(232, 28);
            this.HeaderText.Text = "Edit Registrant Name";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(103, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 1;
            this.cmdSave.Text = "OK";
            // 
            // RegistrantNamePanel
            // 
            this.RegistrantNamePanel.AutoSize = true;
            this.RegistrantNamePanel.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowAndShrink;
            this.RegistrantNamePanel.Controls.Add(this.FirstNamePanel);
            this.RegistrantNamePanel.Controls.Add(this.MiddleNamePanel);
            this.RegistrantNamePanel.Controls.Add(this.LastNamePanel);
            this.RegistrantNamePanel.Controls.Add(this.DesignationPanel);
            this.RegistrantNamePanel.Controls.Add(this.CompanyNamePanel);
            this.RegistrantNamePanel.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
            this.RegistrantNamePanel.Location = new System.Drawing.Point(21, 21);
            this.RegistrantNamePanel.Name = "RegistrantNamePanel";
            this.RegistrantNamePanel.Size = new System.Drawing.Size(420, 199);
            this.RegistrantNamePanel.Spacing = 1;
            this.RegistrantNamePanel.TabIndex = 1001;
            this.RegistrantNamePanel.TabStop = true;
            // 
            // FirstNamePanel
            // 
            this.FirstNamePanel.ColumnCount = 2;
            this.FirstNamePanel.ColumnStyles.Clear();
            this.FirstNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 28F));
            this.FirstNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 72F));
            this.FirstNamePanel.Controls.Add(this.txtFirstName, 1, 0);
            this.FirstNamePanel.Controls.Add(this.lblFirstName, 0, 0);
            this.FirstNamePanel.Location = new System.Drawing.Point(3, 3);
            this.FirstNamePanel.Name = "FirstNamePanel";
            this.FirstNamePanel.RowCount = 1;
            this.FirstNamePanel.RowStyles.Clear();
            this.FirstNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.FirstNamePanel.Size = new System.Drawing.Size(414, 33);
            this.FirstNamePanel.TabIndex = 0;
            this.FirstNamePanel.TabStop = true;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtFirstName.Location = new System.Drawing.Point(134, 3);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(277, 27);
            this.txtFirstName.TabIndex = 1;
            // 
            // lblFirstName
            // 
            this.lblFirstName.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(3, 3);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(125, 27);
            this.lblFirstName.TabIndex = 1;
            this.lblFirstName.Text = "FIRST NAME";
            this.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MiddleNamePanel
            // 
            this.MiddleNamePanel.ColumnCount = 2;
            this.MiddleNamePanel.ColumnStyles.Clear();
            this.MiddleNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 28F));
            this.MiddleNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 72F));
            this.MiddleNamePanel.Controls.Add(this.txtMiddleInitial, 1, 0);
            this.MiddleNamePanel.Controls.Add(this.lblMiddleInitial, 0, 0);
            this.MiddleNamePanel.Location = new System.Drawing.Point(3, 43);
            this.MiddleNamePanel.Name = "MiddleNamePanel";
            this.MiddleNamePanel.RowCount = 1;
            this.MiddleNamePanel.RowStyles.Clear();
            this.MiddleNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.MiddleNamePanel.Size = new System.Drawing.Size(414, 33);
            this.MiddleNamePanel.TabIndex = 1;
            this.MiddleNamePanel.TabStop = true;
            // 
            // txtMiddleInitial
            // 
            this.txtMiddleInitial.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtMiddleInitial.Location = new System.Drawing.Point(134, 3);
            this.txtMiddleInitial.Name = "txtMiddleInitial";
            this.txtMiddleInitial.Size = new System.Drawing.Size(277, 27);
            this.txtMiddleInitial.TabIndex = 1;
            // 
            // lblMiddleInitial
            // 
            this.lblMiddleInitial.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblMiddleInitial.AutoSize = true;
            this.lblMiddleInitial.Location = new System.Drawing.Point(3, 3);
            this.lblMiddleInitial.Name = "lblMiddleInitial";
            this.lblMiddleInitial.Size = new System.Drawing.Size(125, 27);
            this.lblMiddleInitial.TabIndex = 1;
            this.lblMiddleInitial.Text = "MIDDLE INITIAL";
            this.lblMiddleInitial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LastNamePanel
            // 
            this.LastNamePanel.ColumnCount = 2;
            this.LastNamePanel.ColumnStyles.Clear();
            this.LastNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 28F));
            this.LastNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 72F));
            this.LastNamePanel.Controls.Add(this.txtLastName, 1, 0);
            this.LastNamePanel.Controls.Add(this.lblLastName, 0, 0);
            this.LastNamePanel.Location = new System.Drawing.Point(3, 83);
            this.LastNamePanel.Name = "LastNamePanel";
            this.LastNamePanel.RowCount = 1;
            this.LastNamePanel.RowStyles.Clear();
            this.LastNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.LastNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 20F));
            this.LastNamePanel.Size = new System.Drawing.Size(414, 33);
            this.LastNamePanel.TabIndex = 2;
            this.LastNamePanel.TabStop = true;
            // 
            // txtLastName
            // 
            this.txtLastName.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtLastName.Location = new System.Drawing.Point(134, 3);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(277, 27);
            this.txtLastName.TabIndex = 1;
            // 
            // lblLastName
            // 
            this.lblLastName.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(3, 3);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(125, 27);
            this.lblLastName.TabIndex = 1;
            this.lblLastName.Text = "LAST NAME";
            this.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DesignationPanel
            // 
            this.DesignationPanel.ColumnCount = 2;
            this.DesignationPanel.ColumnStyles.Clear();
            this.DesignationPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 28F));
            this.DesignationPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 72F));
            this.DesignationPanel.Controls.Add(this.txtDesignation, 1, 0);
            this.DesignationPanel.Controls.Add(this.lblDesignation, 0, 0);
            this.DesignationPanel.Location = new System.Drawing.Point(3, 123);
            this.DesignationPanel.Name = "DesignationPanel";
            this.DesignationPanel.RowCount = 1;
            this.DesignationPanel.RowStyles.Clear();
            this.DesignationPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.DesignationPanel.Size = new System.Drawing.Size(414, 33);
            this.DesignationPanel.TabIndex = 3;
            this.DesignationPanel.TabStop = true;
            // 
            // txtDesignation
            // 
            this.txtDesignation.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtDesignation.Location = new System.Drawing.Point(134, 3);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(277, 27);
            this.txtDesignation.TabIndex = 1;
            // 
            // lblDesignation
            // 
            this.lblDesignation.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblDesignation.AutoSize = true;
            this.lblDesignation.Location = new System.Drawing.Point(3, 3);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(125, 27);
            this.lblDesignation.TabIndex = 1;
            this.lblDesignation.Text = "DESIGNATION";
            this.lblDesignation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CompanyNamePanel
            // 
            this.CompanyNamePanel.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.CompanyNamePanel.ColumnCount = 2;
            this.CompanyNamePanel.ColumnStyles.Clear();
            this.CompanyNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 28F));
            this.CompanyNamePanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 72F));
            this.CompanyNamePanel.Controls.Add(this.txtCompanyName, 1, 0);
            this.CompanyNamePanel.Controls.Add(this.lblCompanyName, 0, 0);
            this.CompanyNamePanel.Location = new System.Drawing.Point(3, 163);
            this.CompanyNamePanel.Name = "CompanyNamePanel";
            this.CompanyNamePanel.RowCount = 1;
            this.CompanyNamePanel.RowStyles.Clear();
            this.CompanyNamePanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.CompanyNamePanel.Size = new System.Drawing.Size(414, 33);
            this.CompanyNamePanel.TabIndex = 4;
            this.CompanyNamePanel.TabStop = true;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtCompanyName.Location = new System.Drawing.Point(134, 3);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(277, 27);
            this.txtCompanyName.TabIndex = 1;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Location = new System.Drawing.Point(3, 3);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(125, 27);
            this.lblCompanyName.TabIndex = 1;
            this.lblCompanyName.Text = "NAME";
            this.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "actionButton";
            this.btnCancel.Location = new System.Drawing.Point(233, 30);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnCancel.Size = new System.Drawing.Size(100, 48);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmEditRegistrantName
            // 
            this.ClientSize = new System.Drawing.Size(459, 381);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditRegistrantName";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Registrant Name";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.RegistrantNamePanel.ResumeLayout(false);
            this.FirstNamePanel.ResumeLayout(false);
            this.FirstNamePanel.PerformLayout();
            this.MiddleNamePanel.ResumeLayout(false);
            this.MiddleNamePanel.PerformLayout();
            this.LastNamePanel.ResumeLayout(false);
            this.LastNamePanel.PerformLayout();
            this.DesignationPanel.ResumeLayout(false);
            this.DesignationPanel.PerformLayout();
            this.CompanyNamePanel.ResumeLayout(false);
            this.CompanyNamePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion
        private FCButton cmdSave;
        private FCButton btnCancel;
        private FlexLayoutPanel RegistrantNamePanel;
        private TableLayoutPanel FirstNamePanel;
        private TextBox txtFirstName;
        private Label lblFirstName;
        private TableLayoutPanel LastNamePanel;
        private TextBox txtLastName;
        private Label lblLastName;
        private TableLayoutPanel MiddleNamePanel;
        private TextBox txtMiddleInitial;
        private Label lblMiddleInitial;
        private TableLayoutPanel DesignationPanel;
        private TextBox txtDesignation;
        private Label lblDesignation;
        private TableLayoutPanel CompanyNamePanel;
        private TextBox txtCompanyName;
        private Label lblCompanyName;
    }
}
