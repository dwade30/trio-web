﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.MotorVehicle;
using SharedApplication.MotorVehicle.Enums;

namespace TWMV0000
{
    /// <summary>
    /// Summary description for frmTellerID.
    /// </summary>
    public partial class frmEditRegistrantName : BaseForm, IModalView<IEditRegistrantNameViewModel>
    {
        public frmEditRegistrantName()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
           
        }

        public frmEditRegistrantName(IEditRegistrantNameViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }


        private void InitializeComponentEx()
        {
            this.cmdSave.Click += CmdSave_Click;
            this.Load += FrmEditRegistrantName_Load;

        }

        private void FrmEditRegistrantName_Load(object sender, EventArgs e)
        {
            FormatPanels();
            BottomPanel.Top = RegistrantNamePanel.Bottom + 10;

            if (ViewModel.RegistrantInfo.Type == RegistrantType.Individual)
            {
                txtFirstName.MaxLength = 20;
                txtLastName.MaxLength = 50;
                txtMiddleInitial.MaxLength = 1;
                txtDesignation.MaxLength = 3;

                txtFirstName.Text = ViewModel.RegistrantInfo.FirstName;
                txtLastName.Text = ViewModel.RegistrantInfo.LastName;
                txtMiddleInitial.Text = ViewModel.RegistrantInfo.MiddleInitial;
                txtDesignation.Text = ViewModel.RegistrantInfo.Designation;

                FirstNamePanel.Visible = true;
                MiddleNamePanel.Visible = true;
                LastNamePanel.Visible = true;
                DesignationPanel.Visible = true;
                CompanyNamePanel.Visible = false;
            }
            else
            {
                txtCompanyName.MaxLength = 100;

                txtCompanyName.Text = ViewModel.RegistrantInfo.CompanyName;

                FirstNamePanel.Visible = false;
                MiddleNamePanel.Visible = false;
                LastNamePanel.Visible = false;
                DesignationPanel.Visible = false;
                CompanyNamePanel.Visible = true;
            }
        }

        private void FormatPanels()
        {
            foreach (TableLayoutPanel panel in RegistrantNamePanel.Controls.Where(x => x is TableLayoutPanel))
            {
                panel.Left = 0;
                panel.ColumnStyles[0].SizeType = SizeType.Percent;
                panel.ColumnStyles[0].Width = 33;
                panel.ColumnStyles[1].SizeType = SizeType.Percent;
                panel.ColumnStyles[1].Width = 66;
            }
        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
            SetRegistrantInfo();
        }

        private void SetRegistrantInfo()
        {
            if (ViewModel.RegistrantInfo.Type == RegistrantType.Individual)
            {
                if (String.IsNullOrWhiteSpace(txtFirstName.Text))
                {
                    MessageBox.Show("You didn't enter a first name", "Invalid first name", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
                if (String.IsNullOrWhiteSpace(txtLastName.Text))
                {
                    MessageBox.Show("You didn't enter a last name", "Invalid last name", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }

                ViewModel.RegistrantInfo.FirstName = txtFirstName.Text.Trim().ToUpper();
                ViewModel.RegistrantInfo.LastName = txtLastName.Text.Trim().ToUpper();
                ViewModel.RegistrantInfo.MiddleInitial = txtMiddleInitial.Text.Trim().ToUpper();
                ViewModel.RegistrantInfo.Designation = txtDesignation.Text.Trim().ToUpper();
            }
            else
            {
                if (String.IsNullOrWhiteSpace(txtCompanyName.Text))
                {
                    MessageBox.Show("You didn't enter a name", "Invalid name", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }

                ViewModel.RegistrantInfo.CompanyName = txtCompanyName.Text.Trim();
            }
            
            Close();
        }

        public IEditRegistrantNameViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

}
