//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubPlateException.
	/// </summary>
	public partial class rptSubPlateException : BaseSectionReport
	{
		public rptSubPlateException()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Exception Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptSubPlateException_ReportEnd;
		}

        private void RptSubPlateException_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
        }

        public static rptSubPlateException InstancePtr
		{
			get
			{
				return (rptSubPlateException)Sys.GetInstance(typeof(rptSubPlateException));
			}
		}

		protected rptSubPlateException _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubPlateException	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rs.EndOfFile();
			}
			else
			{
				rs.MoveNext();
				eArgs.EOF = rs.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND Type = '2P' ORDER BY ExceptionReportDate");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Type = '2P' ORDER BY ExceptionReportDate");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rptNewExceptionReport.InstancePtr.boolData = true;
			}
			else
			{
				this.Close();
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldOriginal As object	OnWrite(string)
			// vbPorter upgrade warning: fldIssued As object	OnWrite(string)
			// vbPorter upgrade warning: fldReplacement As object	OnWrite(string)
			// vbPorter upgrade warning: fldExplanation As object	OnWrite(string)
			rptNewExceptionReport.InstancePtr.PlateTotal += 1;
			fldOriginal.Text = rs.Get_Fields_String("OriginalClass") + " " + rs.Get_Fields_String("OriginalPlate");
			fldIssued.Text = Strings.Format(rs.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yy");
			fldReplacement.Text = rs.Get_Fields_String("newclass") + " " + rs.Get_Fields_String("NewPlate");
			if (fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("reason")) == false)
			{
				if (FCConvert.ToString(rs.Get_Fields_String("reason")).Length <= 20)
				{
					fldExplanation.Text = rs.Get_Fields_String("reason") + Strings.StrDup(20 - FCConvert.ToString(rs.Get_Fields_String("reason")).Length, " ");
				}
				else
				{
					fldExplanation.Text = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 20);
				}
			}
			fldOpID.Text = rs.Get_Fields_String("OpID");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotal As object	OnWrite
			FLDtOTAL.Text = FCConvert.ToString(rptNewExceptionReport.InstancePtr.PlateTotal);
		}

		private void rptSubPlateException_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptSubPlateException properties;
			//rptSubPlateException.Caption	= "Exception Report";
			//rptSubPlateException.Icon	= "rptSubPlateException.dsx":0000";
			//rptSubPlateException.Left	= 0;
			//rptSubPlateException.Top	= 0;
			//rptSubPlateException.Width	= 11880;
			//rptSubPlateException.Height	= 8595;
			//rptSubPlateException.StartUpPosition	= 3;
			//rptSubPlateException.SectionData	= "rptSubPlateException.dsx":058A;
			//End Unmaped Properties
		}
	}
}
