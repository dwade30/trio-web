//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmUseTax.
	/// </summary>
	partial class frmUseTax
	{
		public fecherFoundation.FCComboBox cmbUseTaxPrint;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCTextBox txtE1;
		public fecherFoundation.FCComboBox cmbExemptCode;
		public fecherFoundation.FCTextBox txtRate;
		public Global.T2KBackFillDecimal txtUTCPrice;
		public Global.T2KOverTypeBox txtA1;
		public fecherFoundation.FCCheckBox chkFleet;
		public Global.T2KOverTypeBox txtB2;
		public fecherFoundation.FCTextBox txtPurchasedType;
		public fecherFoundation.FCTextBox txtTradedType;
		public fecherFoundation.FCTextBox txtUTCMakeTraded;
		public fecherFoundation.FCTextBox txtUTCModelTraded;
		public fecherFoundation.FCTextBox txtUTCYearTraded;
		public fecherFoundation.FCTextBox txtUTCVINTraded;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtUTCPurchaserLName;
		public fecherFoundation.FCCheckBox chkRegistrant1;
		public fecherFoundation.FCTextBox txtUTCSSN;
		public fecherFoundation.FCTextBox txtUTCPurchaserFName;
		public fecherFoundation.FCTextBox txtUTCPurchaserCity;
		public fecherFoundation.FCTextBox txtUTCPurcahserAddress;
		public fecherFoundation.FCTextBox txtUTCPurchaserZip;
		public fecherFoundation.FCTextBox txtUTCPurchaserState;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtUTCSellerName;
		public fecherFoundation.FCTextBox txtUTCSellerZip;
		public fecherFoundation.FCTextBox txtUTCSellerState;
		public fecherFoundation.FCTextBox txtUTCSellerCity;
		public fecherFoundation.FCTextBox txtUTCSellerAddress;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtUTCLienAddress;
		public fecherFoundation.FCTextBox txtUTCLienCity;
		public fecherFoundation.FCTextBox txtUTCLienState;
		public fecherFoundation.FCTextBox txtUTCLienZip;
		public fecherFoundation.FCTextBox txtUTCLienName;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCTextBox txtB1;
		public fecherFoundation.FCTextBox txtC1;
		public fecherFoundation.FCTextBox txtX1;
		public Global.T2KBackFillDecimal txtC2;
		public Global.T2KBackFillDecimal txtUTCUseTax;
		public Global.T2KDateBox txtUTCDate;
		public Global.T2KBackFillWhole txtUTCNetAmount;
		public Global.T2KDateBox txtB3;
		public Global.T2KBackFillWhole txtD1;
		public Global.T2KBackFillDecimal txtUTCAllowance;
		public fecherFoundation.FCLabel lblE1;
		public fecherFoundation.FCLabel LabelExemptCode;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel lblA1;
		public fecherFoundation.FCLabel lblB2;
		public fecherFoundation.FCLabel lblB3;
		public fecherFoundation.FCLabel lblB1;
		public fecherFoundation.FCLabel lblC1;
		public fecherFoundation.FCLabel lblC2;
		public fecherFoundation.FCLabel lblD1;
		public fecherFoundation.FCLabel lblX1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbUseTaxPrint = new fecherFoundation.FCComboBox();
            this.txtE1 = new fecherFoundation.FCTextBox();
            this.cmbExemptCode = new fecherFoundation.FCComboBox();
            this.txtRate = new fecherFoundation.FCTextBox();
            this.txtUTCPrice = new Global.T2KBackFillDecimal();
            this.txtA1 = new Global.T2KOverTypeBox();
            this.chkFleet = new fecherFoundation.FCCheckBox();
            this.txtB2 = new Global.T2KOverTypeBox();
            this.txtPurchasedType = new fecherFoundation.FCTextBox();
            this.txtTradedType = new fecherFoundation.FCTextBox();
            this.txtUTCMakeTraded = new fecherFoundation.FCTextBox();
            this.txtUTCModelTraded = new fecherFoundation.FCTextBox();
            this.txtUTCYearTraded = new fecherFoundation.FCTextBox();
            this.txtUTCVINTraded = new fecherFoundation.FCTextBox();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtUTCPurchaserLName = new fecherFoundation.FCTextBox();
            this.chkRegistrant1 = new fecherFoundation.FCCheckBox();
            this.txtUTCSSN = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserFName = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserCity = new fecherFoundation.FCTextBox();
            this.txtUTCPurcahserAddress = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserZip = new fecherFoundation.FCTextBox();
            this.txtUTCPurchaserState = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtUTCSellerName = new fecherFoundation.FCTextBox();
            this.txtUTCSellerZip = new fecherFoundation.FCTextBox();
            this.txtUTCSellerState = new fecherFoundation.FCTextBox();
            this.txtUTCSellerCity = new fecherFoundation.FCTextBox();
            this.txtUTCSellerAddress = new fecherFoundation.FCTextBox();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtUTCLienAddress = new fecherFoundation.FCTextBox();
            this.txtUTCLienCity = new fecherFoundation.FCTextBox();
            this.txtUTCLienState = new fecherFoundation.FCTextBox();
            this.txtUTCLienZip = new fecherFoundation.FCTextBox();
            this.txtUTCLienName = new fecherFoundation.FCTextBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.txtB1 = new fecherFoundation.FCTextBox();
            this.txtC1 = new fecherFoundation.FCTextBox();
            this.txtX1 = new fecherFoundation.FCTextBox();
            this.txtC2 = new Global.T2KBackFillDecimal();
            this.txtUTCUseTax = new Global.T2KBackFillDecimal();
            this.txtUTCDate = new Global.T2KDateBox();
            this.txtUTCNetAmount = new Global.T2KBackFillWhole();
            this.txtB3 = new Global.T2KDateBox();
            this.txtD1 = new Global.T2KBackFillWhole();
            this.txtUTCAllowance = new Global.T2KBackFillDecimal();
            this.lblE1 = new fecherFoundation.FCLabel();
            this.LabelExemptCode = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.lblA1 = new fecherFoundation.FCLabel();
            this.lblB2 = new fecherFoundation.FCLabel();
            this.lblB3 = new fecherFoundation.FCLabel();
            this.lblB1 = new fecherFoundation.FCLabel();
            this.lblC1 = new fecherFoundation.FCLabel();
            this.lblC2 = new fecherFoundation.FCLabel();
            this.lblD1 = new fecherFoundation.FCLabel();
            this.lblX1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFileSaveExit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFleet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegistrant1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCUseTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCNetAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCAllowance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 932);
            this.BottomPanel.Size = new System.Drawing.Size(1018, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbUseTaxPrint);
            this.ClientArea.Controls.Add(this.txtE1);
            this.ClientArea.Controls.Add(this.cmbExemptCode);
            this.ClientArea.Controls.Add(this.txtRate);
            this.ClientArea.Controls.Add(this.txtUTCPrice);
            this.ClientArea.Controls.Add(this.txtA1);
            this.ClientArea.Controls.Add(this.chkFleet);
            this.ClientArea.Controls.Add(this.txtB2);
            this.ClientArea.Controls.Add(this.txtPurchasedType);
            this.ClientArea.Controls.Add(this.txtTradedType);
            this.ClientArea.Controls.Add(this.txtUTCMakeTraded);
            this.ClientArea.Controls.Add(this.txtUTCModelTraded);
            this.ClientArea.Controls.Add(this.txtUTCYearTraded);
            this.ClientArea.Controls.Add(this.txtUTCVINTraded);
            this.ClientArea.Controls.Add(this.cmdReturn);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.txtB1);
            this.ClientArea.Controls.Add(this.txtC1);
            this.ClientArea.Controls.Add(this.txtX1);
            this.ClientArea.Controls.Add(this.txtC2);
            this.ClientArea.Controls.Add(this.txtUTCUseTax);
            this.ClientArea.Controls.Add(this.txtUTCDate);
            this.ClientArea.Controls.Add(this.txtUTCNetAmount);
            this.ClientArea.Controls.Add(this.txtB3);
            this.ClientArea.Controls.Add(this.txtD1);
            this.ClientArea.Controls.Add(this.txtUTCAllowance);
            this.ClientArea.Controls.Add(this.lblE1);
            this.ClientArea.Controls.Add(this.LabelExemptCode);
            this.ClientArea.Controls.Add(this.Label18);
            this.ClientArea.Controls.Add(this.Label1_1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label1_0);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label19);
            this.ClientArea.Controls.Add(this.Label20);
            this.ClientArea.Controls.Add(this.Label21);
            this.ClientArea.Controls.Add(this.Label23);
            this.ClientArea.Controls.Add(this.lblA1);
            this.ClientArea.Controls.Add(this.lblB2);
            this.ClientArea.Controls.Add(this.lblB3);
            this.ClientArea.Controls.Add(this.lblB1);
            this.ClientArea.Controls.Add(this.lblC1);
            this.ClientArea.Controls.Add(this.lblC2);
            this.ClientArea.Controls.Add(this.lblD1);
            this.ClientArea.Controls.Add(this.lblX1);
            this.ClientArea.Size = new System.Drawing.Size(1038, 628);
            this.ClientArea.Controls.SetChildIndex(this.lblX1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblD1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblC2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblC1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblB1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblB3, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblB2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblA1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label23, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label21, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label20, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label19, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1_1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label18, 0);
            this.ClientArea.Controls.SetChildIndex(this.LabelExemptCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblE1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCAllowance, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtD1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCNetAmount, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCUseTax, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtC2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtX1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtC1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtB1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdReturn, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCVINTraded, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCYearTraded, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCModelTraded, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCMakeTraded, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTradedType, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPurchasedType, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtB2, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkFleet, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtA1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtUTCPrice, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtRate, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbExemptCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtE1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbUseTaxPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1038, 60);
            this.TopPanel.TabIndex = 0;
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(103, 30);
            this.HeaderText.Text = "Use Tax";
            // 
            // cmbUseTaxPrint
            // 
            this.cmbUseTaxPrint.Items.AddRange(new object[] {
            "This Form NEEDS to be printed",
            "This Form does NOT need to be printed"});
            this.cmbUseTaxPrint.Location = new System.Drawing.Point(30, 842);
            this.cmbUseTaxPrint.Name = "cmbUseTaxPrint";
            this.cmbUseTaxPrint.Size = new System.Drawing.Size(546, 40);
            this.cmbUseTaxPrint.TabIndex = 46;
            // 
            // txtE1
            // 
            this.txtE1.BackColor = System.Drawing.SystemColors.Window;
            this.txtE1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtE1.Location = new System.Drawing.Point(766, 455);
            this.txtE1.MaxLength = 30;
            this.txtE1.Name = "txtE1";
            this.txtE1.Size = new System.Drawing.Size(231, 40);
            this.txtE1.TabIndex = 34;
            this.txtE1.Visible = false;
            this.txtE1.Enter += new System.EventHandler(this.txtE1_Enter);
            this.txtE1.Leave += new System.EventHandler(this.txtE1_Leave);
            // 
            // cmbExemptCode
            // 
            this.cmbExemptCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbExemptCode.Location = new System.Drawing.Point(766, 405);
            this.cmbExemptCode.Name = "cmbExemptCode";
            this.cmbExemptCode.Size = new System.Drawing.Size(231, 40);
            this.cmbExemptCode.TabIndex = 32;
            this.cmbExemptCode.SelectedIndexChanged += new System.EventHandler(this.cmbExemptCode_SelectedIndexChanged);
            // 
            // txtRate
            // 
            this.txtRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtRate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtRate.Location = new System.Drawing.Point(766, 305);
            this.txtRate.MaxLength = 6;
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(231, 40);
            this.txtRate.TabIndex = 28;
            this.txtRate.Enter += new System.EventHandler(this.txtRate_Enter);
            this.txtRate.Leave += new System.EventHandler(this.txtRate_Leave);
            this.txtRate.Validating += new System.ComponentModel.CancelEventHandler(this.txtRate_Validating);
            this.txtRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRate_KeyPress);
            // 
            // txtUTCPrice
            // 
            this.txtUTCPrice.Location = new System.Drawing.Point(766, 155);
            this.txtUTCPrice.MaxLength = 10;
            this.txtUTCPrice.Name = "txtUTCPrice";
            this.txtUTCPrice.Size = new System.Drawing.Size(231, 40);
            this.txtUTCPrice.TabIndex = 22;
            this.txtUTCPrice.Enter += new System.EventHandler(this.txtUTCPrice_Enter);
            this.txtUTCPrice.Leave += new System.EventHandler(this.txtUTCPrice_Leave);
            this.txtUTCPrice.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCPrice_Validate);
            // 
            // txtA1
            // 
            this.txtA1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtA1.Location = new System.Drawing.Point(766, 455);
            this.txtA1.MaxLength = 5;
            this.txtA1.Name = "txtA1";
            this.txtA1.Size = new System.Drawing.Size(231, 40);
            this.txtA1.TabIndex = 21;
            this.txtA1.Visible = false;
            this.txtA1.Enter += new System.EventHandler(this.txtA1_Enter);
            this.txtA1.Leave += new System.EventHandler(this.txtA1_Leave);
            // 
            // chkFleet
            // 
            this.chkFleet.Location = new System.Drawing.Point(215, 900);
            this.chkFleet.Name = "chkFleet";
            this.chkFleet.Size = new System.Drawing.Size(189, 27);
            this.chkFleet.TabIndex = 48;
            this.chkFleet.Text = "For Multiple Vehicles?";
            this.chkFleet.CheckedChanged += new System.EventHandler(this.chkFleet_CheckedChanged);
            // 
            // txtB2
            // 
            this.txtB2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtB2.Location = new System.Drawing.Point(766, 505);
            this.txtB2.MaxLength = 8;
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(231, 40);
            this.txtB2.TabIndex = 36;
            this.txtB2.Visible = false;
            this.txtB2.Enter += new System.EventHandler(this.txtB2_Enter);
            this.txtB2.Leave += new System.EventHandler(this.txtB2_Leave);
            // 
            // txtPurchasedType
            // 
            this.txtPurchasedType.BackColor = System.Drawing.SystemColors.Window;
            this.txtPurchasedType.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPurchasedType.Location = new System.Drawing.Point(185, 105);
            this.txtPurchasedType.Name = "txtPurchasedType";
            this.txtPurchasedType.Size = new System.Drawing.Size(150, 40);
            this.txtPurchasedType.TabIndex = 14;
            this.txtPurchasedType.Text = "AUTO";
            this.txtPurchasedType.Enter += new System.EventHandler(this.txtPurchasedType_Enter);
            this.txtPurchasedType.Leave += new System.EventHandler(this.txtPurchasedType_Leave);
            // 
            // txtTradedType
            // 
            this.txtTradedType.BackColor = System.Drawing.SystemColors.Window;
            this.txtTradedType.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTradedType.Location = new System.Drawing.Point(185, 55);
            this.txtTradedType.Name = "txtTradedType";
            this.txtTradedType.Size = new System.Drawing.Size(150, 40);
            this.txtTradedType.TabIndex = 2;
            this.txtTradedType.Enter += new System.EventHandler(this.txtTradedType_Enter);
            this.txtTradedType.Leave += new System.EventHandler(this.txtTradedType_Leave);
            // 
            // txtUTCMakeTraded
            // 
            this.txtUTCMakeTraded.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCMakeTraded.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCMakeTraded.Location = new System.Drawing.Point(355, 55);
            this.txtUTCMakeTraded.Name = "txtUTCMakeTraded";
            this.txtUTCMakeTraded.Size = new System.Drawing.Size(150, 40);
            this.txtUTCMakeTraded.TabIndex = 4;
            this.txtUTCMakeTraded.Enter += new System.EventHandler(this.txtUTCMakeTraded_Enter);
            this.txtUTCMakeTraded.Leave += new System.EventHandler(this.txtUTCMakeTraded_Leave);
            // 
            // txtUTCModelTraded
            // 
            this.txtUTCModelTraded.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCModelTraded.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCModelTraded.Location = new System.Drawing.Point(525, 55);
            this.txtUTCModelTraded.Name = "txtUTCModelTraded";
            this.txtUTCModelTraded.Size = new System.Drawing.Size(75, 40);
            this.txtUTCModelTraded.TabIndex = 6;
            this.txtUTCModelTraded.Enter += new System.EventHandler(this.txtUTCModelTraded_Enter);
            this.txtUTCModelTraded.Leave += new System.EventHandler(this.txtUTCModelTraded_Leave);
            // 
            // txtUTCYearTraded
            // 
            this.txtUTCYearTraded.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCYearTraded.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCYearTraded.Location = new System.Drawing.Point(620, 55);
            this.txtUTCYearTraded.Name = "txtUTCYearTraded";
            this.txtUTCYearTraded.Size = new System.Drawing.Size(75, 40);
            this.txtUTCYearTraded.TabIndex = 8;
            this.txtUTCYearTraded.Enter += new System.EventHandler(this.txtUTCYearTraded_Enter);
            this.txtUTCYearTraded.Leave += new System.EventHandler(this.txtUTCYearTraded_Leave);
            this.txtUTCYearTraded.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtUTCYearTraded_KeyPress);
            // 
            // txtUTCVINTraded
            // 
            this.txtUTCVINTraded.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCVINTraded.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCVINTraded.Location = new System.Drawing.Point(715, 55);
            this.txtUTCVINTraded.MaxLength = 17;
            this.txtUTCVINTraded.Name = "txtUTCVINTraded";
            this.txtUTCVINTraded.Size = new System.Drawing.Size(150, 40);
            this.txtUTCVINTraded.TabIndex = 10;
            this.txtUTCVINTraded.Enter += new System.EventHandler(this.txtUTCVINTraded_Enter);
            this.txtUTCVINTraded.Leave += new System.EventHandler(this.txtUTCVINTraded_Leave);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(30, 892);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(165, 40);
            this.cmdReturn.TabIndex = 47;
            this.cmdReturn.Text = "Return To Input";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtUTCPurchaserLName);
            this.Frame1.Controls.Add(this.chkRegistrant1);
            this.Frame1.Controls.Add(this.txtUTCSSN);
            this.Frame1.Controls.Add(this.txtUTCPurchaserFName);
            this.Frame1.Controls.Add(this.txtUTCPurchaserCity);
            this.Frame1.Controls.Add(this.txtUTCPurcahserAddress);
            this.Frame1.Controls.Add(this.txtUTCPurchaserZip);
            this.Frame1.Controls.Add(this.txtUTCPurchaserState);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.Label13);
            this.Frame1.Location = new System.Drawing.Point(30, 155);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(546, 277);
            this.Frame1.TabIndex = 15;
            this.Frame1.Text = "Purchaser Information";
            // 
            // txtUTCPurchaserLName
            // 
            this.txtUTCPurchaserLName.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserLName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCPurchaserLName.Location = new System.Drawing.Point(351, 117);
            this.txtUTCPurchaserLName.Name = "txtUTCPurchaserLName";
            this.txtUTCPurchaserLName.Size = new System.Drawing.Size(175, 40);
            this.txtUTCPurchaserLName.TabIndex = 5;
            this.txtUTCPurchaserLName.Enter += new System.EventHandler(this.txtUTCPurchaserLName_Enter);
            this.txtUTCPurchaserLName.Leave += new System.EventHandler(this.txtUTCPurchaserLName_Leave);
            // 
            // chkRegistrant1
            // 
            this.chkRegistrant1.Location = new System.Drawing.Point(20, 30);
            this.chkRegistrant1.Name = "chkRegistrant1";
            this.chkRegistrant1.Size = new System.Drawing.Size(423, 27);
            this.chkRegistrant1.TabIndex = 1;
            this.chkRegistrant1.Text = "Purchaser is the same as Registrant #1 on registration";
            this.chkRegistrant1.CheckedChanged += new System.EventHandler(this.chkRegistrant1_CheckedChanged);
            // 
            // txtUTCSSN
            // 
            this.txtUTCSSN.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSSN.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSSN.Location = new System.Drawing.Point(141, 67);
            this.txtUTCSSN.Name = "txtUTCSSN";
            this.txtUTCSSN.Size = new System.Drawing.Size(385, 40);
            this.txtUTCSSN.TabIndex = 2;
            this.txtUTCSSN.Enter += new System.EventHandler(this.txtUTCSSN_Enter);
            this.txtUTCSSN.Leave += new System.EventHandler(this.txtUTCSSN_Leave);
            // 
            // txtUTCPurchaserFName
            // 
            this.txtUTCPurchaserFName.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserFName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCPurchaserFName.Location = new System.Drawing.Point(141, 117);
            this.txtUTCPurchaserFName.Name = "txtUTCPurchaserFName";
            this.txtUTCPurchaserFName.Size = new System.Drawing.Size(200, 40);
            this.txtUTCPurchaserFName.TabIndex = 4;
            this.txtUTCPurchaserFName.Enter += new System.EventHandler(this.txtUTCPurchaserFName_Enter);
            this.txtUTCPurchaserFName.Leave += new System.EventHandler(this.txtUTCPurchaserFName_Leave);
            // 
            // txtUTCPurchaserCity
            // 
            this.txtUTCPurchaserCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCPurchaserCity.Location = new System.Drawing.Point(141, 217);
            this.txtUTCPurchaserCity.Name = "txtUTCPurchaserCity";
            this.txtUTCPurchaserCity.Size = new System.Drawing.Size(175, 40);
            this.txtUTCPurchaserCity.TabIndex = 9;
            this.txtUTCPurchaserCity.Enter += new System.EventHandler(this.txtUTCPurchaserCity_Enter);
            this.txtUTCPurchaserCity.Leave += new System.EventHandler(this.txtUTCPurchaserCity_Leave);
            // 
            // txtUTCPurcahserAddress
            // 
            this.txtUTCPurcahserAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurcahserAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCPurcahserAddress.Location = new System.Drawing.Point(141, 167);
            this.txtUTCPurcahserAddress.Name = "txtUTCPurcahserAddress";
            this.txtUTCPurcahserAddress.Size = new System.Drawing.Size(385, 40);
            this.txtUTCPurcahserAddress.TabIndex = 7;
            this.txtUTCPurcahserAddress.Enter += new System.EventHandler(this.txtUTCPurcahserAddress_Enter);
            this.txtUTCPurcahserAddress.Leave += new System.EventHandler(this.txtUTCPurcahserAddress_Leave);
            // 
            // txtUTCPurchaserZip
            // 
            this.txtUTCPurchaserZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCPurchaserZip.Location = new System.Drawing.Point(399, 217);
            this.txtUTCPurchaserZip.Name = "txtUTCPurchaserZip";
            this.txtUTCPurchaserZip.Size = new System.Drawing.Size(127, 40);
            this.txtUTCPurchaserZip.TabIndex = 11;
            this.txtUTCPurchaserZip.Enter += new System.EventHandler(this.txtUTCPurchaserZip_Enter);
            this.txtUTCPurchaserZip.Leave += new System.EventHandler(this.txtUTCPurchaserZip_Leave);
            // 
            // txtUTCPurchaserState
            // 
            this.txtUTCPurchaserState.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCPurchaserState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCPurchaserState.Location = new System.Drawing.Point(326, 217);
            this.txtUTCPurchaserState.Name = "txtUTCPurchaserState";
            this.txtUTCPurchaserState.Size = new System.Drawing.Size(63, 40);
            this.txtUTCPurchaserState.TabIndex = 10;
            this.txtUTCPurchaserState.Enter += new System.EventHandler(this.txtUTCPurchaserState_Enter);
            this.txtUTCPurchaserState.Leave += new System.EventHandler(this.txtUTCPurchaserState_Leave);
            this.txtUTCPurchaserState.KeyUp += new Wisej.Web.KeyEventHandler(this.txtUTCPurchaserState_KeyUp);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 131);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(78, 18);
            this.Label10.TabIndex = 3;
            this.Label10.Text = "NAME";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 181);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(94, 18);
            this.Label11.TabIndex = 6;
            this.Label11.Text = "ADDRESS";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 231);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(115, 18);
            this.Label12.TabIndex = 8;
            this.Label12.Text = "CITY/STATE/ZIP";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 81);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(63, 18);
            this.Label13.TabIndex = 1;
            this.Label13.Text = "SSN";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtUTCSellerName);
            this.Frame2.Controls.Add(this.txtUTCSellerZip);
            this.Frame2.Controls.Add(this.txtUTCSellerState);
            this.Frame2.Controls.Add(this.txtUTCSellerCity);
            this.Frame2.Controls.Add(this.txtUTCSellerAddress);
            this.Frame2.Controls.Add(this.Label14);
            this.Frame2.Controls.Add(this.Label15);
            this.Frame2.Controls.Add(this.Label16);
            this.Frame2.Location = new System.Drawing.Point(30, 442);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(546, 190);
            this.Frame2.TabIndex = 42;
            this.Frame2.Text = "Seller";
            // 
            // txtUTCSellerName
            // 
            this.txtUTCSellerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerName.Location = new System.Drawing.Point(141, 30);
            this.txtUTCSellerName.Name = "txtUTCSellerName";
            this.txtUTCSellerName.Size = new System.Drawing.Size(385, 40);
            this.txtUTCSellerName.TabIndex = 1;
            this.txtUTCSellerName.Enter += new System.EventHandler(this.txtUTCSellerName_Enter);
            this.txtUTCSellerName.Leave += new System.EventHandler(this.txtUTCSellerName_Leave);
            // 
            // txtUTCSellerZip
            // 
            this.txtUTCSellerZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerZip.Location = new System.Drawing.Point(399, 130);
            this.txtUTCSellerZip.Name = "txtUTCSellerZip";
            this.txtUTCSellerZip.Size = new System.Drawing.Size(127, 40);
            this.txtUTCSellerZip.TabIndex = 7;
            this.txtUTCSellerZip.Enter += new System.EventHandler(this.txtUTCSellerZip_Enter);
            this.txtUTCSellerZip.Leave += new System.EventHandler(this.txtUTCSellerZip_Leave);
            // 
            // txtUTCSellerState
            // 
            this.txtUTCSellerState.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerState.Location = new System.Drawing.Point(326, 130);
            this.txtUTCSellerState.Name = "txtUTCSellerState";
            this.txtUTCSellerState.Size = new System.Drawing.Size(63, 40);
            this.txtUTCSellerState.TabIndex = 6;
            this.txtUTCSellerState.Enter += new System.EventHandler(this.txtUTCSellerState_Enter);
            this.txtUTCSellerState.Leave += new System.EventHandler(this.txtUTCSellerState_Leave);
            // 
            // txtUTCSellerCity
            // 
            this.txtUTCSellerCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerCity.Location = new System.Drawing.Point(141, 130);
            this.txtUTCSellerCity.Name = "txtUTCSellerCity";
            this.txtUTCSellerCity.Size = new System.Drawing.Size(175, 40);
            this.txtUTCSellerCity.TabIndex = 5;
            this.txtUTCSellerCity.Enter += new System.EventHandler(this.txtUTCSellerCity_Enter);
            this.txtUTCSellerCity.Leave += new System.EventHandler(this.txtUTCSellerCity_Leave);
            // 
            // txtUTCSellerAddress
            // 
            this.txtUTCSellerAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCSellerAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCSellerAddress.Location = new System.Drawing.Point(141, 80);
            this.txtUTCSellerAddress.Name = "txtUTCSellerAddress";
            this.txtUTCSellerAddress.Size = new System.Drawing.Size(385, 40);
            this.txtUTCSellerAddress.TabIndex = 3;
            this.txtUTCSellerAddress.Enter += new System.EventHandler(this.txtUTCSellerAddress_Enter);
            this.txtUTCSellerAddress.Leave += new System.EventHandler(this.txtUTCSellerAddress_Leave);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(20, 44);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(49, 18);
            this.Label14.TabIndex = 8;
            this.Label14.Text = "NAME";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(20, 94);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(94, 18);
            this.Label15.TabIndex = 2;
            this.Label15.Text = "ADDRESS";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(20, 147);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(37, 18);
            this.Label16.TabIndex = 4;
            this.Label16.Text = "C/S/Z";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtUTCLienAddress);
            this.Frame3.Controls.Add(this.txtUTCLienCity);
            this.Frame3.Controls.Add(this.txtUTCLienState);
            this.Frame3.Controls.Add(this.txtUTCLienZip);
            this.Frame3.Controls.Add(this.txtUTCLienName);
            this.Frame3.Controls.Add(this.Label8);
            this.Frame3.Controls.Add(this.Label9);
            this.Frame3.Controls.Add(this.Label17);
            this.Frame3.Location = new System.Drawing.Point(30, 642);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(546, 190);
            this.Frame3.TabIndex = 44;
            this.Frame3.Text = "Lienholder";
            // 
            // txtUTCLienAddress
            // 
            this.txtUTCLienAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCLienAddress.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCLienAddress.Location = new System.Drawing.Point(141, 80);
            this.txtUTCLienAddress.Name = "txtUTCLienAddress";
            this.txtUTCLienAddress.Size = new System.Drawing.Size(385, 40);
            this.txtUTCLienAddress.TabIndex = 3;
            this.txtUTCLienAddress.Enter += new System.EventHandler(this.txtUTCLienAddress_Enter);
            this.txtUTCLienAddress.Leave += new System.EventHandler(this.txtUTCLienAddress_Leave);
            // 
            // txtUTCLienCity
            // 
            this.txtUTCLienCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCLienCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCLienCity.Location = new System.Drawing.Point(141, 130);
            this.txtUTCLienCity.Name = "txtUTCLienCity";
            this.txtUTCLienCity.Size = new System.Drawing.Size(175, 40);
            this.txtUTCLienCity.TabIndex = 5;
            this.txtUTCLienCity.Enter += new System.EventHandler(this.txtUTCLienCity_Enter);
            this.txtUTCLienCity.Leave += new System.EventHandler(this.txtUTCLienCity_Leave);
            // 
            // txtUTCLienState
            // 
            this.txtUTCLienState.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCLienState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCLienState.Location = new System.Drawing.Point(326, 130);
            this.txtUTCLienState.Name = "txtUTCLienState";
            this.txtUTCLienState.Size = new System.Drawing.Size(63, 40);
            this.txtUTCLienState.TabIndex = 6;
            this.txtUTCLienState.Enter += new System.EventHandler(this.txtUTCLienState_Enter);
            this.txtUTCLienState.Leave += new System.EventHandler(this.txtUTCLienState_Leave);
            // 
            // txtUTCLienZip
            // 
            this.txtUTCLienZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCLienZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCLienZip.Location = new System.Drawing.Point(399, 130);
            this.txtUTCLienZip.Name = "txtUTCLienZip";
            this.txtUTCLienZip.Size = new System.Drawing.Size(127, 40);
            this.txtUTCLienZip.TabIndex = 7;
            this.txtUTCLienZip.Enter += new System.EventHandler(this.txtUTCLienZip_Enter);
            this.txtUTCLienZip.Leave += new System.EventHandler(this.txtUTCLienZip_Leave);
            // 
            // txtUTCLienName
            // 
            this.txtUTCLienName.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCLienName.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtUTCLienName.Location = new System.Drawing.Point(141, 30);
            this.txtUTCLienName.Name = "txtUTCLienName";
            this.txtUTCLienName.Size = new System.Drawing.Size(385, 40);
            this.txtUTCLienName.TabIndex = 1;
            this.txtUTCLienName.Enter += new System.EventHandler(this.txtUTCLienName_Enter);
            this.txtUTCLienName.Leave += new System.EventHandler(this.txtUTCLienName_Leave);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 144);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(38, 18);
            this.Label8.TabIndex = 4;
            this.Label8.Text = "C/S/Z";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 94);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(94, 18);
            this.Label9.TabIndex = 2;
            this.Label9.Text = "ADDRESS";
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(20, 44);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(78, 18);
            this.Label17.TabIndex = 8;
            this.Label17.Text = "NAME";
            // 
            // txtB1
            // 
            this.txtB1.BackColor = System.Drawing.SystemColors.Window;
            this.txtB1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtB1.Location = new System.Drawing.Point(766, 455);
            this.txtB1.MaxLength = 2;
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(231, 40);
            this.txtB1.TabIndex = 22;
            this.txtB1.Visible = false;
            this.txtB1.Enter += new System.EventHandler(this.txtB1_Enter);
            this.txtB1.Leave += new System.EventHandler(this.txtB1_Leave);
            // 
            // txtC1
            // 
            this.txtC1.BackColor = System.Drawing.SystemColors.Window;
            this.txtC1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtC1.Location = new System.Drawing.Point(766, 455);
            this.txtC1.MaxLength = 2;
            this.txtC1.Name = "txtC1";
            this.txtC1.Size = new System.Drawing.Size(231, 40);
            this.txtC1.TabIndex = 25;
            this.txtC1.Visible = false;
            this.txtC1.Enter += new System.EventHandler(this.txtC1_Enter);
            this.txtC1.Leave += new System.EventHandler(this.txtC1_Leave);
            // 
            // txtX1
            // 
            this.txtX1.BackColor = System.Drawing.SystemColors.Window;
            this.txtX1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtX1.Location = new System.Drawing.Point(766, 605);
            this.txtX1.MaxLength = 30;
            this.txtX1.Name = "txtX1";
            this.txtX1.Size = new System.Drawing.Size(231, 40);
            this.txtX1.TabIndex = 40;
            this.txtX1.Visible = false;
            this.txtX1.Enter += new System.EventHandler(this.txtX1_Enter);
            this.txtX1.Leave += new System.EventHandler(this.txtX1_Leave);
            // 
            // txtC2
            // 
            this.txtC2.Location = new System.Drawing.Point(766, 505);
            this.txtC2.MaxLength = 8;
            this.txtC2.Name = "txtC2";
            this.txtC2.Size = new System.Drawing.Size(231, 40);
            this.txtC2.TabIndex = 26;
            this.txtC2.Visible = false;
            this.txtC2.Enter += new System.EventHandler(this.txtC2_Enter);
            this.txtC2.Leave += new System.EventHandler(this.txtC2_Leave);
            this.txtC2.Validating += new System.ComponentModel.CancelEventHandler(this.txtC2_Validate);
            // 
            // txtUTCUseTax
            // 
            this.txtUTCUseTax.Location = new System.Drawing.Point(766, 355);
            this.txtUTCUseTax.MaxLength = 10;
            this.txtUTCUseTax.Name = "txtUTCUseTax";
            this.txtUTCUseTax.Size = new System.Drawing.Size(231, 40);
            this.txtUTCUseTax.TabIndex = 30;
            this.txtUTCUseTax.Enter += new System.EventHandler(this.txtUTCUseTax_Enter);
            this.txtUTCUseTax.Leave += new System.EventHandler(this.txtUTCUseTax_Leave);
            // 
            // txtUTCDate
            // 
            this.txtUTCDate.Location = new System.Drawing.Point(885, 55);
            this.txtUTCDate.Mask = "##/##/####";
            this.txtUTCDate.Name = "txtUTCDate";
            this.txtUTCDate.Size = new System.Drawing.Size(120, 22);
            this.txtUTCDate.TabIndex = 12;
            this.txtUTCDate.Enter += new System.EventHandler(this.txtUTCDate_Enter);
            this.txtUTCDate.Leave += new System.EventHandler(this.txtUTCDate_Leave);
            // 
            // txtUTCNetAmount
            // 
            this.txtUTCNetAmount.Location = new System.Drawing.Point(766, 255);
            this.txtUTCNetAmount.MaxLength = 11;
            this.txtUTCNetAmount.Name = "txtUTCNetAmount";
            this.txtUTCNetAmount.Size = new System.Drawing.Size(231, 40);
            this.txtUTCNetAmount.TabIndex = 26;
            this.txtUTCNetAmount.Enter += new System.EventHandler(this.txtUTCNetAmount_Enter);
            this.txtUTCNetAmount.Leave += new System.EventHandler(this.txtUTCNetAmount_Leave);
            this.txtUTCNetAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCNetAmount_Validate);
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(766, 555);
            this.txtB3.Mask = "##/##/####";
            this.txtB3.MaxLength = 10;
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(115, 22);
            this.txtB3.TabIndex = 38;
            this.txtB3.Visible = false;
            this.txtB3.Enter += new System.EventHandler(this.txtB3_Enter);
            this.txtB3.Leave += new System.EventHandler(this.txtB3_Leave);
            // 
            // txtD1
            // 
            this.txtD1.Location = new System.Drawing.Point(766, 455);
            this.txtD1.MaxLength = 17;
            this.txtD1.Name = "txtD1";
            this.txtD1.Size = new System.Drawing.Size(231, 40);
            this.txtD1.TabIndex = 27;
            this.txtD1.Visible = false;
            this.txtD1.Enter += new System.EventHandler(this.txtD1_Enter);
            this.txtD1.Leave += new System.EventHandler(this.txtD1_Leave);
            // 
            // txtUTCAllowance
            // 
            this.txtUTCAllowance.Location = new System.Drawing.Point(766, 205);
            this.txtUTCAllowance.MaxLength = 10;
            this.txtUTCAllowance.Name = "txtUTCAllowance";
            this.txtUTCAllowance.Size = new System.Drawing.Size(231, 40);
            this.txtUTCAllowance.TabIndex = 24;
            this.txtUTCAllowance.Enter += new System.EventHandler(this.txtUTCAllowance_Enter);
            this.txtUTCAllowance.Leave += new System.EventHandler(this.txtUTCAllowance_Leave);
            this.txtUTCAllowance.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCAllowance_Validate);
            // 
            // lblE1
            // 
            this.lblE1.Location = new System.Drawing.Point(596, 469);
            this.lblE1.Name = "lblE1";
            this.lblE1.Size = new System.Drawing.Size(123, 16);
            this.lblE1.TabIndex = 33;
            this.lblE1.Text = "SALES TAX NUMBER";
            this.lblE1.Visible = false;
            // 
            // LabelExemptCode
            // 
            this.LabelExemptCode.Location = new System.Drawing.Point(596, 419);
            this.LabelExemptCode.Name = "LabelExemptCode";
            this.LabelExemptCode.Size = new System.Drawing.Size(51, 16);
            this.LabelExemptCode.TabIndex = 31;
            this.LabelExemptCode.Text = "EXEMPT";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(596, 319);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(85, 16);
            this.Label18.TabIndex = 27;
            this.Label18.Text = "    TAX RATE";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(185, 30);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(42, 14);
            this.Label1_1.TabIndex = 1;
            this.Label1_1.Text = "TYPE";
            this.Label1_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 69);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(101, 16);
            this.Label2.TabIndex = 41;
            this.Label2.Text = "VEHICLE TRADED";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 119);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(123, 16);
            this.Label3.TabIndex = 13;
            this.Label3.Text = "VEHICLE PURCHASED";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(355, 30);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(42, 14);
            this.Label1_0.TabIndex = 3;
            this.Label1_0.Text = "MAKE";
            this.Label1_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(525, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(42, 14);
            this.Label4.TabIndex = 5;
            this.Label4.Text = "MODEL";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(620, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(42, 14);
            this.Label5.TabIndex = 7;
            this.Label5.Text = "YEAR";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(715, 30);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(27, 14);
            this.Label6.TabIndex = 9;
            this.Label6.Text = "VIN";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(885, 30);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(100, 14);
            this.Label7.TabIndex = 11;
            this.Label7.Text = "PURCHASE DATE";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(596, 169);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(39, 16);
            this.Label19.TabIndex = 21;
            this.Label19.Text = "PRICE";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(596, 219);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(81, 16);
            this.Label20.TabIndex = 23;
            this.Label20.Text = "ALLOWANCE";
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(596, 369);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(85, 16);
            this.Label21.TabIndex = 29;
            this.Label21.Text = "USE TAX";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(596, 269);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(85, 16);
            this.Label23.TabIndex = 25;
            this.Label23.Text = "NET AMOUNT";
            // 
            // lblA1
            // 
            this.lblA1.Location = new System.Drawing.Point(596, 469);
            this.lblA1.Name = "lblA1";
            this.lblA1.Size = new System.Drawing.Size(133, 16);
            this.lblA1.TabIndex = 64;
            this.lblA1.Text = "EXEMPTION NUMBER";
            this.lblA1.Visible = false;
            // 
            // lblB2
            // 
            this.lblB2.Location = new System.Drawing.Point(596, 519);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(141, 16);
            this.lblB2.TabIndex = 35;
            this.lblB2.Text = "REGISTRATION NUMBER";
            this.lblB2.Visible = false;
            // 
            // lblB3
            // 
            this.lblB3.Location = new System.Drawing.Point(596, 569);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(123, 16);
            this.lblB3.TabIndex = 37;
            this.lblB3.Text = "REGISTRATION DATE";
            this.lblB3.Visible = false;
            // 
            // lblB1
            // 
            this.lblB1.Location = new System.Drawing.Point(596, 469);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(102, 16);
            this.lblB1.TabIndex = 61;
            this.lblB1.Text = "PREVIOUS STATE";
            this.lblB1.Visible = false;
            // 
            // lblC1
            // 
            this.lblC1.Location = new System.Drawing.Point(596, 469);
            this.lblC1.Name = "lblC1";
            this.lblC1.Size = new System.Drawing.Size(46, 16);
            this.lblC1.TabIndex = 60;
            this.lblC1.Text = "STATE";
            this.lblC1.Visible = false;
            // 
            // lblC2
            // 
            this.lblC2.Location = new System.Drawing.Point(596, 519);
            this.lblC2.Name = "lblC2";
            this.lblC2.Size = new System.Drawing.Size(61, 16);
            this.lblC2.TabIndex = 59;
            this.lblC2.Text = "AMOUNT";
            this.lblC2.Visible = false;
            // 
            // lblD1
            // 
            this.lblD1.Location = new System.Drawing.Point(596, 469);
            this.lblD1.Name = "lblD1";
            this.lblD1.Size = new System.Drawing.Size(90, 16);
            this.lblD1.TabIndex = 58;
            this.lblD1.Text = "V.A.C. NUMBER";
            this.lblD1.Visible = false;
            // 
            // lblX1
            // 
            this.lblX1.Location = new System.Drawing.Point(596, 617);
            this.lblX1.Name = "lblX1";
            this.lblX1.Size = new System.Drawing.Size(90, 16);
            this.lblX1.TabIndex = 39;
            this.lblX1.Text = "OTHER  (DETAIL)";
            this.lblX1.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSaveExit,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 0;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdFileSaveExit
            // 
            this.cmdFileSaveExit.AppearanceKey = "acceptButton";
            this.cmdFileSaveExit.Location = new System.Drawing.Point(421, 30);
            this.cmdFileSaveExit.Name = "cmdFileSaveExit";
            this.cmdFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileSaveExit.Size = new System.Drawing.Size(130, 48);
            this.cmdFileSaveExit.Text = "Save & Exit";
            this.cmdFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // frmUseTax
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1038, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmUseTax";
            this.Text = "Use Tax";
            this.Load += new System.EventHandler(this.frmUseTax_Load);
            this.Activated += new System.EventHandler(this.frmUseTax_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUseTax_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUseTax_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFleet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegistrant1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCUseTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCNetAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUTCAllowance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdFileSaveExit;
    }
}
