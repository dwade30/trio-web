namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptExciseTaxOnlyDetail.
	/// </summary>
	partial class srptExciseTaxOnlyDetail
	{
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(srptExciseTaxOnlyDetail));
			this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Height = 0F;
			this.pageHeader.Name = "pageHeader";
			// 
			// detail
			// 
			this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5});
			this.detail.Height = 0.1979167F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			// 
			// groupHeader1
			// 
			this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.lblTitle});
			this.groupHeader1.Height = 0.59375F;
			this.groupHeader1.Name = "groupHeader1";
			// 
			// groupFooter1
			// 
			this.groupFooter1.Height = 0F;
			this.groupFooter1.Name = "groupFooter1";
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.562F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 10cpi\'; font-weight: bold; ddo-char-set: 1";
			this.Label12.Text = "FORM# USED";
			this.Label12.Top = 0.406F;
			this.Label12.Width = 1.25F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.812F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 10cpi\'; font-weight: bold; ddo-char-set: 1";
			this.Label13.Text = "FORM# USED";
			this.Label13.Top = 0.406F;
			this.Label13.Width = 1.25F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.687F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 10cpi\'; font-weight: bold; ddo-char-set: 1";
			this.Label14.Text = "FORM# USED";
			this.Label14.Top = 0.406F;
			this.Label14.Width = 1.25F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 10cpi\'; font-weight: bold; ddo-char-set: 1";
			this.Label15.Text = "FORM# USED";
			this.Label15.Top = 0.406F;
			this.Label15.Width = 1.25F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.124F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 10cpi\'; font-weight: bold; ddo-char-set: 1";
			this.Label16.Text = "FORM# USED";
			this.Label16.Top = 0.406F;
			this.Label16.Width = 1.25F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.562F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field1.Text = "Field1";
			this.Field1.Top = 0F;
			this.Field1.Width = 1.25F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 1.812F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field2.Text = "Field2";
			this.Field2.Top = 0F;
			this.Field2.Width = 1.25F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 3.1245F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field3.Text = "Field3";
			this.Field3.Top = 0F;
			this.Field3.Width = 1.25F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 4.3745F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field4.Text = "Field4";
			this.Field4.Top = 0F;
			this.Field4.Width = 1.25F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 5.687F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Field5.Text = "Field5";
			this.Field5.Top = 0F;
			this.Field5.Width = 1.25F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2.625F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Roman 10cpi\'; font-weight: bold; text-align: center; ddo-char-set: " +
    "1";
			this.lblTitle.Text = "FORM# USED";
			this.lblTitle.Top = 0.07300001F;
			this.lblTitle.Width = 2.25F;
			// 
			// srptExciseTaxOnlyDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.pageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.srptExciseTaxOnlyDetail_FetchData);
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
	}
}
