﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptInventoryAdjustmentsPlateSummary.
	/// </summary>
	partial class srptInventoryAdjustmentsPlateSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptInventoryAdjustmentsPlateSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPlateClass = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPlateCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlateClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlateCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblPlateClass,
				this.fldPlateCount
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanShrink = true;
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.lblHeader.Text = "PLATES:";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 0.75F;
			// 
			// lblPlateClass
			// 
			this.lblPlateClass.Height = 0.1875F;
			this.lblPlateClass.HyperLink = null;
			this.lblPlateClass.Left = 1F;
			this.lblPlateClass.Name = "lblPlateClass";
			this.lblPlateClass.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.lblPlateClass.Text = "MVR-3";
			this.lblPlateClass.Top = 0F;
			this.lblPlateClass.Width = 1.3125F;
			// 
			// fldPlateCount
			// 
			this.fldPlateCount.CanShrink = true;
			this.fldPlateCount.Height = 0.1875F;
			this.fldPlateCount.Left = 2.9375F;
			this.fldPlateCount.Name = "fldPlateCount";
			this.fldPlateCount.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.fldPlateCount.Text = null;
			this.fldPlateCount.Top = 0F;
			this.fldPlateCount.Width = 0.5625F;
			// 
			// srptInventoryAdjustmentsPlateSummary
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlateClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlateCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPlateClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlateCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
