﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTitleApplication.
	/// </summary>
	partial class rptTitleApplication
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTitleApplication));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCustomer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOPID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFee2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCustomer2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOPID2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCLTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNoInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.txtFee,
				this.txtClass,
				this.txtPlate,
				this.txtDT,
				this.txtCustomer,
				this.txtOPID,
				this.txtTitle2,
				this.txtFee2,
				this.txtPlate2,
				this.txtDate2,
				this.txtCustomer2,
				this.txtOPID2,
				this.txtTotals,
				this.txtCLTotal,
				this.lblTotals
			});
			this.Detail.Height = 0.5520833F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtNoInfo
			});
			this.ReportFooter.Height = 0.625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label1,
				this.lblPage,
				this.Line1,
				this.Label18,
				this.Label33
			});
			this.GroupHeader1.Height = 1.822917F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.4375F;
			this.SubReport2.Width = 7.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.1875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label12.Text = "APP #";
			this.Label12.Top = 1.625F;
			this.Label12.Width = 1.0625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.3125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "FEE";
			this.Label13.Top = 1.625F;
			this.Label13.Width = 0.4375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.25F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label14.Text = "CUSTOMER";
			this.Label14.Top = 1.625F;
			this.Label14.Width = 1.0625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label15.Text = "DATE";
			this.Label15.Top = 1.625F;
			this.Label15.Width = 0.5625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.9375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 10cpi\'; text-align: left; ddo-char-set: 1";
			this.Label16.Text = "CLASS/PLATE";
			this.Label16.Top = 1.625F;
			this.Label16.Width = 1.125F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 6.875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label17.Text = "OPID";
			this.Label17.Top = 1.625F;
			this.Label17.Width = 0.4375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.Label1.Text = "**** TITLE APPLICATION SUMMARY REPORT****";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.8125F;
			this.Line1.Width = 7.375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1.8125F;
			this.Line1.Y2 = 1.8125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.1875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label18.Text = "TITLE";
			this.Label18.Top = 1.4375F;
			this.Label18.Width = 1.0625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 1.9375F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
			this.Label33.Top = 0.1875F;
			this.Label33.Width = 2.3125F;
			// 
			// txtTitle
			// 
			this.txtTitle.CanShrink = true;
			this.txtTitle.Height = 0.1875F;
			this.txtTitle.Left = 0.25F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtTitle.Text = null;
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 0.875F;
			// 
			// txtFee
			// 
			this.txtFee.CanShrink = true;
			this.txtFee.Height = 0.1875F;
			this.txtFee.Left = 1.125F;
			this.txtFee.Name = "txtFee";
			this.txtFee.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.txtFee.Text = null;
			this.txtFee.Top = 0F;
			this.txtFee.Width = 0.75F;
			// 
			// txtClass
			// 
			this.txtClass.CanShrink = true;
			this.txtClass.Height = 0.1875F;
			this.txtClass.Left = 1.9375F;
			this.txtClass.Name = "txtClass";
			this.txtClass.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtClass.Text = null;
			this.txtClass.Top = 0F;
			this.txtClass.Width = 0.375F;
			// 
			// txtPlate
			// 
			this.txtPlate.CanShrink = true;
			this.txtPlate.Height = 0.1875F;
			this.txtPlate.Left = 2.3125F;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtPlate.Text = null;
			this.txtPlate.Top = 0F;
			this.txtPlate.Width = 0.875F;
			// 
			// txtDT
			// 
			this.txtDT.CanShrink = true;
			this.txtDT.Height = 0.1875F;
			this.txtDT.Left = 3.25F;
			this.txtDT.Name = "txtDT";
			this.txtDT.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtDT.Text = null;
			this.txtDT.Top = 0F;
			this.txtDT.Width = 0.8125F;
			// 
			// txtCustomer
			// 
			this.txtCustomer.CanShrink = true;
			this.txtCustomer.Height = 0.1875F;
			this.txtCustomer.Left = 4.21875F;
			this.txtCustomer.Name = "txtCustomer";
			this.txtCustomer.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtCustomer.Text = null;
			this.txtCustomer.Top = 0F;
			this.txtCustomer.Width = 2.46875F;
			// 
			// txtOPID
			// 
			this.txtOPID.CanShrink = true;
			this.txtOPID.Height = 0.1875F;
			this.txtOPID.Left = 6.84375F;
			this.txtOPID.Name = "txtOPID";
			this.txtOPID.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOPID.Text = null;
			this.txtOPID.Top = 0F;
			this.txtOPID.Width = 0.4375F;
			// 
			// txtTitle2
			// 
			this.txtTitle2.CanShrink = true;
			this.txtTitle2.Height = 0.1875F;
			this.txtTitle2.Left = 0.25F;
			this.txtTitle2.Name = "txtTitle2";
			this.txtTitle2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtTitle2.Text = null;
			this.txtTitle2.Top = 0.1875F;
			this.txtTitle2.Width = 0.875F;
			// 
			// txtFee2
			// 
			this.txtFee2.CanShrink = true;
			this.txtFee2.Height = 0.1875F;
			this.txtFee2.Left = 1.125F;
			this.txtFee2.Name = "txtFee2";
			this.txtFee2.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.txtFee2.Text = null;
			this.txtFee2.Top = 0.1875F;
			this.txtFee2.Width = 0.75F;
			// 
			// txtPlate2
			// 
			this.txtPlate2.CanShrink = true;
			this.txtPlate2.Height = 0.1875F;
			this.txtPlate2.Left = 1.9375F;
			this.txtPlate2.Name = "txtPlate2";
			this.txtPlate2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtPlate2.Text = null;
			this.txtPlate2.Top = 0.1875F;
			this.txtPlate2.Width = 1.125F;
			// 
			// txtDate2
			// 
			this.txtDate2.CanShrink = true;
			this.txtDate2.Height = 0.1875F;
			this.txtDate2.Left = 3.25F;
			this.txtDate2.Name = "txtDate2";
			this.txtDate2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtDate2.Text = null;
			this.txtDate2.Top = 0.1875F;
			this.txtDate2.Width = 0.8125F;
			// 
			// txtCustomer2
			// 
			this.txtCustomer2.CanShrink = true;
			this.txtCustomer2.Height = 0.1875F;
			this.txtCustomer2.Left = 4.21875F;
			this.txtCustomer2.Name = "txtCustomer2";
			this.txtCustomer2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtCustomer2.Text = null;
			this.txtCustomer2.Top = 0.1875F;
			this.txtCustomer2.Width = 2.46875F;
			// 
			// txtOPID2
			// 
			this.txtOPID2.CanShrink = true;
			this.txtOPID2.Height = 0.1875F;
			this.txtOPID2.Left = 6.84375F;
			this.txtOPID2.Name = "txtOPID2";
			this.txtOPID2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtOPID2.Text = null;
			this.txtOPID2.Top = 0.1875F;
			this.txtOPID2.Width = 0.4375F;
			// 
			// txtTotals
			// 
			this.txtTotals.CanShrink = true;
			this.txtTotals.Height = 0.1875F;
			this.txtTotals.Left = 1.1875F;
			this.txtTotals.Name = "txtTotals";
			this.txtTotals.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtTotals.Text = null;
			this.txtTotals.Top = 0.375F;
			this.txtTotals.Width = 0.75F;
			// 
			// txtCLTotal
			// 
			this.txtCLTotal.CanShrink = true;
			this.txtCLTotal.Height = 0.1875F;
			this.txtCLTotal.Left = 2F;
			this.txtCLTotal.Name = "txtCLTotal";
			this.txtCLTotal.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.txtCLTotal.Text = null;
			this.txtCLTotal.Top = 0.375F;
			this.txtCLTotal.Width = 0.6875F;
			// 
			// lblTotals
			// 
			this.lblTotals.CanShrink = true;
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.Left = 0.25F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.lblTotals.Text = null;
			this.lblTotals.Top = 0.375F;
			this.lblTotals.Width = 0.875F;
			// 
			// txtNoInfo
			// 
			this.txtNoInfo.CanShrink = true;
			this.txtNoInfo.Height = 0.3125F;
			this.txtNoInfo.Left = 1.96875F;
			this.txtNoInfo.Name = "txtNoInfo";
			this.txtNoInfo.Style = "font-family: \'Roman 10cpi\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.txtNoInfo.Text = null;
			this.txtNoInfo.Top = 0.15625F;
			this.txtNoInfo.Width = 2.6875F;
			// 
			// rptTitleApplication
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.75F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCustomer2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOPID2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCustomer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOPID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFee2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCustomer2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOPID2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoInfo;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
