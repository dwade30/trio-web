//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmSettings.
	/// </summary>
	partial class frmSettings
	{
		public fecherFoundation.FCComboBox cmbLeaseUseTaxV1;
		public fecherFoundation.FCComboBox cmbPrintUseTaxNo;
		public fecherFoundation.FCLabel lblPrintUseTaxNo;
		public fecherFoundation.FCComboBox cmbReminderNo;
		public fecherFoundation.FCLabel lblReminderNo;
		public fecherFoundation.FCComboBox cmbPrinting;
		public fecherFoundation.FCLabel lblPrinting;
		public fecherFoundation.FCComboBox cmbLongTermLaserFormsYes;
		public fecherFoundation.FCLabel lblLongTermLaserFormsYes;
		public fecherFoundation.FCComboBox cmbLongTermPrintCTAUseTaxYes;
		public fecherFoundation.FCLabel lblLongTermPrintCTAUseTaxYes;
		public fecherFoundation.FCComboBox cmbTellerYes;
		public fecherFoundation.FCLabel lblTellerYes;
		public fecherFoundation.FCComboBox cmbCheckDigits;
		public fecherFoundation.FCLabel lblCheckDigits;
		public fecherFoundation.FCComboBox cmbExciseRebateNo;
		public fecherFoundation.FCLabel lblExciseRebateNo;
		public fecherFoundation.FCComboBox cmbYes;
		public fecherFoundation.FCLabel lblYes;
		public fecherFoundation.FCFrame fraCTAUseTaxOptions;
		public fecherFoundation.FCFrame fraLeaseUseTaxVersion;
		public fecherFoundation.FCButton cmdPrintLeasedUseTaxTest;
		public fecherFoundation.FCFrame fraUseTaxOptions;
		public fecherFoundation.FCButton cmdPrintUseTaxTest;
		public fecherFoundation.FCCheckBox chkUTCPrintForm;
		public fecherFoundation.FCCheckBox chkUTCDuplex;
		public fecherFoundation.FCFrame fraUTPrintAdj;
		public fecherFoundation.FCTextBox txtUTCTopAdj;
		public fecherFoundation.FCTextBox txtUTCLeftAdj;
		public fecherFoundation.FCLabel lblUTCTopAdj;
		public fecherFoundation.FCLabel lblUTCLeftAdj;
		public fecherFoundation.FCFrame fraPrintingOptions;
		public fecherFoundation.FCFrame fraMVR3E_Adjustments;
		public fecherFoundation.FCTextBox txtMVR3E_LeftAdj;
		public fecherFoundation.FCTextBox txtMVR3E_TopAdj;
		public fecherFoundation.FCLabel lblMVR3E_LeftAdj;
		public fecherFoundation.FCLabel lblMVR3E_TopAdj;
		public fecherFoundation.FCFrame fraLaserAdjustmentOptions;
		public fecherFoundation.FCTextBox txtLabelAdjustment;
		public fecherFoundation.FCTextBox txtLongTermLaserAdjustment;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCTextBox txtPrinterGroup;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCFrame fraLongTermOptions;
		public fecherFoundation.FCCheckBox chkAutoPopulateUnit;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtFTPPassword;
		public fecherFoundation.FCTextBox txtFTPUser;
		public fecherFoundation.FCTextBox txtFTPAddress;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraGeneralOptions;
		public fecherFoundation.FCButton cmdClearLocks;
		public fecherFoundation.FCFrame fraRapidRenewal;
		public fecherFoundation.FCCheckBox chkCreateJournal;
		public fecherFoundation.FCFrame fraRevAcct;
		public FCGrid vsRevAcct;
		public FCGrid vsExpAcct;
		public fecherFoundation.FCLabel lblRevAcct;
		public fecherFoundation.FCLabel lblExpAcct;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbLeaseUseTaxV1 = new fecherFoundation.FCComboBox();
            this.cmbPrintUseTaxNo = new fecherFoundation.FCComboBox();
            this.lblPrintUseTaxNo = new fecherFoundation.FCLabel();
            this.cmbReminderNo = new fecherFoundation.FCComboBox();
            this.lblReminderNo = new fecherFoundation.FCLabel();
            this.cmbPrinting = new fecherFoundation.FCComboBox();
            this.lblPrinting = new fecherFoundation.FCLabel();
            this.cmbLongTermLaserFormsYes = new fecherFoundation.FCComboBox();
            this.lblLongTermLaserFormsYes = new fecherFoundation.FCLabel();
            this.cmbLongTermPrintCTAUseTaxYes = new fecherFoundation.FCComboBox();
            this.lblLongTermPrintCTAUseTaxYes = new fecherFoundation.FCLabel();
            this.cmbTellerYes = new fecherFoundation.FCComboBox();
            this.lblTellerYes = new fecherFoundation.FCLabel();
            this.cmbCheckDigits = new fecherFoundation.FCComboBox();
            this.lblCheckDigits = new fecherFoundation.FCLabel();
            this.cmbExciseRebateNo = new fecherFoundation.FCComboBox();
            this.lblExciseRebateNo = new fecherFoundation.FCLabel();
            this.cmbYes = new fecherFoundation.FCComboBox();
            this.lblYes = new fecherFoundation.FCLabel();
            this.fraCTAUseTaxOptions = new fecherFoundation.FCFrame();
            this.fraLeaseUseTaxVersion = new fecherFoundation.FCFrame();
            this.cmdPrintLeasedUseTaxTest = new fecherFoundation.FCButton();
            this.fraUseTaxOptions = new fecherFoundation.FCFrame();
            this.cmdPrintUseTaxTest = new fecherFoundation.FCButton();
            this.chkUTCPrintForm = new fecherFoundation.FCCheckBox();
            this.chkUTCDuplex = new fecherFoundation.FCCheckBox();
            this.fraUTPrintAdj = new fecherFoundation.FCFrame();
            this.txtUTCTopAdj = new fecherFoundation.FCTextBox();
            this.txtUTCLeftAdj = new fecherFoundation.FCTextBox();
            this.lblUTCTopAdj = new fecherFoundation.FCLabel();
            this.lblUTCLeftAdj = new fecherFoundation.FCLabel();
            this.fraPrintingOptions = new fecherFoundation.FCFrame();
            this.fraMVR3E_Adjustments = new fecherFoundation.FCFrame();
            this.txtMVR3E_LeftAdj = new fecherFoundation.FCTextBox();
            this.txtMVR3E_TopAdj = new fecherFoundation.FCTextBox();
            this.lblMVR3E_LeftAdj = new fecherFoundation.FCLabel();
            this.lblMVR3E_TopAdj = new fecherFoundation.FCLabel();
            this.fraLaserAdjustmentOptions = new fecherFoundation.FCFrame();
            this.txtLabelAdjustment = new fecherFoundation.FCTextBox();
            this.txtLongTermLaserAdjustment = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.lblInstruct = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.txtPrinterGroup = new fecherFoundation.FCTextBox();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.fraLongTermOptions = new fecherFoundation.FCFrame();
            this.chkAutoPopulateUnit = new fecherFoundation.FCCheckBox();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtFTPPassword = new fecherFoundation.FCTextBox();
            this.txtFTPUser = new fecherFoundation.FCTextBox();
            this.txtFTPAddress = new fecherFoundation.FCTextBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraGeneralOptions = new fecherFoundation.FCFrame();
            this.fraRapidRenewal = new fecherFoundation.FCFrame();
            this.chkCreateJournal = new fecherFoundation.FCCheckBox();
            this.fraRevAcct = new fecherFoundation.FCFrame();
            this.vsRevAcct = new fecherFoundation.FCGrid();
            this.vsExpAcct = new fecherFoundation.FCGrid();
            this.lblRevAcct = new fecherFoundation.FCLabel();
            this.lblExpAcct = new fecherFoundation.FCLabel();
            this.cmdClearLocks = new fecherFoundation.FCButton();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdFileSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraCTAUseTaxOptions)).BeginInit();
            this.fraCTAUseTaxOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLeaseUseTaxVersion)).BeginInit();
            this.fraLeaseUseTaxVersion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLeasedUseTaxTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUseTaxOptions)).BeginInit();
            this.fraUseTaxOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintUseTaxTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUTCPrintForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUTCDuplex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUTPrintAdj)).BeginInit();
            this.fraUTPrintAdj.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPrintingOptions)).BeginInit();
            this.fraPrintingOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMVR3E_Adjustments)).BeginInit();
            this.fraMVR3E_Adjustments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLaserAdjustmentOptions)).BeginInit();
            this.fraLaserAdjustmentOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLongTermOptions)).BeginInit();
            this.fraLongTermOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoPopulateUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraGeneralOptions)).BeginInit();
            this.fraGeneralOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRapidRenewal)).BeginInit();
            this.fraRapidRenewal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateJournal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRevAcct)).BeginInit();
            this.fraRevAcct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRevAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsExpAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearLocks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 599);
            this.BottomPanel.Size = new System.Drawing.Size(938, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraCTAUseTaxOptions);
            this.ClientArea.Controls.Add(this.cmbPrinting);
            this.ClientArea.Controls.Add(this.lblPrinting);
            this.ClientArea.Controls.Add(this.fraLongTermOptions);
            this.ClientArea.Controls.Add(this.fraGeneralOptions);
            this.ClientArea.Controls.Add(this.fraPrintingOptions);
            this.ClientArea.Size = new System.Drawing.Size(958, 606);
            this.ClientArea.Controls.SetChildIndex(this.fraPrintingOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraGeneralOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraLongTermOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPrinting, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPrinting, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraCTAUseTaxOptions, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(958, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(117, 28);
            this.HeaderText.Text = "Customize";
            // 
            // cmbLeaseUseTaxV1
            // 
            this.cmbLeaseUseTaxV1.Items.AddRange(new object[] {
            "Original",
            "Newer (Bigger margins)"});
            this.cmbLeaseUseTaxV1.Location = new System.Drawing.Point(20, 30);
            this.cmbLeaseUseTaxV1.Name = "cmbLeaseUseTaxV1";
            this.cmbLeaseUseTaxV1.Size = new System.Drawing.Size(351, 40);
            this.cmbLeaseUseTaxV1.TabIndex = 84;
            this.cmbLeaseUseTaxV1.Text = "Original";
            // 
            // cmbPrintUseTaxNo
            // 
            this.cmbPrintUseTaxNo.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.cmbPrintUseTaxNo.Location = new System.Drawing.Point(208, 464);
            this.cmbPrintUseTaxNo.Name = "cmbPrintUseTaxNo";
            this.cmbPrintUseTaxNo.Size = new System.Drawing.Size(148, 40);
            this.cmbPrintUseTaxNo.TabIndex = 83;
            this.cmbPrintUseTaxNo.Text = "No";
            this.cmbPrintUseTaxNo.SelectedIndexChanged += new System.EventHandler(this.cmbPrintUseTaxNo_SelectedIndexChanged);
            // 
            // lblPrintUseTaxNo
            // 
            this.lblPrintUseTaxNo.AutoSize = true;
            this.lblPrintUseTaxNo.Location = new System.Drawing.Point(20, 478);
            this.lblPrintUseTaxNo.Name = "lblPrintUseTaxNo";
            this.lblPrintUseTaxNo.Size = new System.Drawing.Size(153, 15);
            this.lblPrintUseTaxNo.TabIndex = 84;
            this.lblPrintUseTaxNo.Text = "PRINT USE TAX FORMS";
            // 
            // cmbReminderNo
            // 
            this.cmbReminderNo.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.cmbReminderNo.Location = new System.Drawing.Point(208, 414);
            this.cmbReminderNo.Name = "cmbReminderNo";
            this.cmbReminderNo.Size = new System.Drawing.Size(148, 40);
            this.cmbReminderNo.TabIndex = 85;
            this.cmbReminderNo.Text = "No";
            this.cmbReminderNo.SelectedIndexChanged += new System.EventHandler(this.cmbReminderNo_SelectedIndexChanged);
            // 
            // lblReminderNo
            // 
            this.lblReminderNo.AutoSize = true;
            this.lblReminderNo.Location = new System.Drawing.Point(20, 428);
            this.lblReminderNo.Name = "lblReminderNo";
            this.lblReminderNo.Size = new System.Drawing.Size(183, 15);
            this.lblReminderNo.TabIndex = 86;
            this.lblReminderNo.Text = "CTA & USE TAX REMINDERS";
            // 
            // cmbPrinting
            // 
            this.cmbPrinting.Items.AddRange(new object[] {
            "General",
            "Printing",
            "Long Term",
            "CTA & Use Tax"});
            this.cmbPrinting.Location = new System.Drawing.Point(211, 30);
            this.cmbPrinting.Name = "cmbPrinting";
            this.cmbPrinting.Size = new System.Drawing.Size(260, 40);
            this.cmbPrinting.TabIndex = 49;
            this.cmbPrinting.Text = "General";
            this.cmbPrinting.SelectedIndexChanged += new System.EventHandler(this.cmbPrinting_SelectedIndexChanged);
            // 
            // lblPrinting
            // 
            this.lblPrinting.AutoSize = true;
            this.lblPrinting.Location = new System.Drawing.Point(30, 44);
            this.lblPrinting.Name = "lblPrinting";
            this.lblPrinting.Size = new System.Drawing.Size(174, 15);
            this.lblPrinting.TabIndex = 50;
            this.lblPrinting.Text = "MOTOR VEHICLE OPTIONS";
            // 
            // cmbLongTermLaserFormsYes
            // 
            this.cmbLongTermLaserFormsYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbLongTermLaserFormsYes.Location = new System.Drawing.Point(226, 317);
            this.cmbLongTermLaserFormsYes.Name = "cmbLongTermLaserFormsYes";
            this.cmbLongTermLaserFormsYes.Size = new System.Drawing.Size(111, 40);
            this.cmbLongTermLaserFormsYes.TabIndex = 2;
            this.cmbLongTermLaserFormsYes.Text = "No";
            // 
            // lblLongTermLaserFormsYes
            // 
            this.lblLongTermLaserFormsYes.AutoSize = true;
            this.lblLongTermLaserFormsYes.Location = new System.Drawing.Point(20, 331);
            this.lblLongTermLaserFormsYes.Name = "lblLongTermLaserFormsYes";
            this.lblLongTermLaserFormsYes.Size = new System.Drawing.Size(195, 15);
            this.lblLongTermLaserFormsYes.TabIndex = 3;
            this.lblLongTermLaserFormsYes.Text = "PRINT CTA & USE TAX FORMS";
            // 
            // cmbLongTermPrintCTAUseTaxYes
            // 
            this.cmbLongTermPrintCTAUseTaxYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbLongTermPrintCTAUseTaxYes.Location = new System.Drawing.Point(226, 30);
            this.cmbLongTermPrintCTAUseTaxYes.Name = "cmbLongTermPrintCTAUseTaxYes";
            this.cmbLongTermPrintCTAUseTaxYes.Size = new System.Drawing.Size(111, 40);
            this.cmbLongTermPrintCTAUseTaxYes.TabIndex = 0;
            this.cmbLongTermPrintCTAUseTaxYes.Text = "Yes";
            // 
            // lblLongTermPrintCTAUseTaxYes
            // 
            this.lblLongTermPrintCTAUseTaxYes.AutoSize = true;
            this.lblLongTermPrintCTAUseTaxYes.Location = new System.Drawing.Point(20, 44);
            this.lblLongTermPrintCTAUseTaxYes.Name = "lblLongTermPrintCTAUseTaxYes";
            this.lblLongTermPrintCTAUseTaxYes.Size = new System.Drawing.Size(195, 15);
            this.lblLongTermPrintCTAUseTaxYes.TabIndex = 1;
            this.lblLongTermPrintCTAUseTaxYes.Text = "PRINT CTA & USE TAX FORMS";
            // 
            // cmbTellerYes
            // 
            this.cmbTellerYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbTellerYes.Location = new System.Drawing.Point(233, 180);
            this.cmbTellerYes.Name = "cmbTellerYes";
            this.cmbTellerYes.Size = new System.Drawing.Size(137, 40);
            this.cmbTellerYes.TabIndex = 72;
            this.cmbTellerYes.Text = "No";
            this.cmbTellerYes.SelectedIndexChanged += new System.EventHandler(this.cmbTellerYes_SelectedIndexChanged);
            // 
            // lblTellerYes
            // 
            this.lblTellerYes.AutoSize = true;
            this.lblTellerYes.Location = new System.Drawing.Point(20, 194);
            this.lblTellerYes.Name = "lblTellerYes";
            this.lblTellerYes.Size = new System.Drawing.Size(137, 15);
            this.lblTellerYes.TabIndex = 73;
            this.lblTellerYes.Text = "TELLER CLOSEOUTS";
            // 
            // cmbCheckDigits
            // 
            this.cmbCheckDigits.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.cmbCheckDigits.Location = new System.Drawing.Point(233, 80);
            this.cmbCheckDigits.Name = "cmbCheckDigits";
            this.cmbCheckDigits.Size = new System.Drawing.Size(137, 40);
            this.cmbCheckDigits.TabIndex = 74;
            this.cmbCheckDigits.Text = "2";
            this.cmbCheckDigits.SelectedIndexChanged += new System.EventHandler(this.optCheckDigits_CheckedChanged);
            // 
            // lblCheckDigits
            // 
            this.lblCheckDigits.AutoSize = true;
            this.lblCheckDigits.Location = new System.Drawing.Point(20, 94);
            this.lblCheckDigits.Name = "lblCheckDigits";
            this.lblCheckDigits.Size = new System.Drawing.Size(136, 15);
            this.lblCheckDigits.TabIndex = 75;
            this.lblCheckDigits.Text = "MVR3 CHECK DIGITS";
            // 
            // cmbExciseRebateNo
            // 
            this.cmbExciseRebateNo.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.cmbExciseRebateNo.Location = new System.Drawing.Point(233, 130);
            this.cmbExciseRebateNo.Name = "cmbExciseRebateNo";
            this.cmbExciseRebateNo.Size = new System.Drawing.Size(137, 40);
            this.cmbExciseRebateNo.TabIndex = 76;
            this.cmbExciseRebateNo.Text = "No";
            // 
            // lblExciseRebateNo
            // 
            this.lblExciseRebateNo.AutoSize = true;
            this.lblExciseRebateNo.Location = new System.Drawing.Point(20, 144);
            this.lblExciseRebateNo.Name = "lblExciseRebateNo";
            this.lblExciseRebateNo.Size = new System.Drawing.Size(107, 15);
            this.lblExciseRebateNo.TabIndex = 77;
            this.lblExciseRebateNo.Text = "EXCISE REBATE";
            // 
            // cmbYes
            // 
            this.cmbYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbYes.Location = new System.Drawing.Point(233, 30);
            this.cmbYes.Name = "cmbYes";
            this.cmbYes.Size = new System.Drawing.Size(137, 40);
            this.cmbYes.TabIndex = 78;
            this.cmbYes.Text = "No";
            this.cmbYes.SelectedIndexChanged += new System.EventHandler(this.cmbYes_SelectedIndexChanged);
            // 
            // lblYes
            // 
            this.lblYes.AutoSize = true;
            this.lblYes.Location = new System.Drawing.Point(20, 44);
            this.lblYes.Name = "lblYes";
            this.lblYes.Size = new System.Drawing.Size(210, 15);
            this.lblYes.TabIndex = 79;
            this.lblYes.Text = "OUT OF ORDER MVR3 WARNING";
            // 
            // fraCTAUseTaxOptions
            // 
            this.fraCTAUseTaxOptions.AppearanceKey = "groupBoxLeftBorder";
            this.fraCTAUseTaxOptions.Controls.Add(this.fraLeaseUseTaxVersion);
            this.fraCTAUseTaxOptions.Controls.Add(this.cmbPrintUseTaxNo);
            this.fraCTAUseTaxOptions.Controls.Add(this.lblPrintUseTaxNo);
            this.fraCTAUseTaxOptions.Controls.Add(this.cmbReminderNo);
            this.fraCTAUseTaxOptions.Controls.Add(this.lblReminderNo);
            this.fraCTAUseTaxOptions.Controls.Add(this.fraUseTaxOptions);
            this.fraCTAUseTaxOptions.FormatCaption = false;
            this.fraCTAUseTaxOptions.Location = new System.Drawing.Point(30, 80);
            this.fraCTAUseTaxOptions.Name = "fraCTAUseTaxOptions";
            this.fraCTAUseTaxOptions.Size = new System.Drawing.Size(567, 519);
            this.fraCTAUseTaxOptions.TabIndex = 48;
            this.fraCTAUseTaxOptions.Text = "CTA & Use Tax";
            this.fraCTAUseTaxOptions.Visible = false;
            // 
            // fraLeaseUseTaxVersion
            // 
            this.fraLeaseUseTaxVersion.Controls.Add(this.cmdPrintLeasedUseTaxTest);
            this.fraLeaseUseTaxVersion.Controls.Add(this.cmbLeaseUseTaxV1);
            this.fraLeaseUseTaxVersion.Location = new System.Drawing.Point(20, 314);
            this.fraLeaseUseTaxVersion.Name = "fraLeaseUseTaxVersion";
            this.fraLeaseUseTaxVersion.Size = new System.Drawing.Size(527, 90);
            this.fraLeaseUseTaxVersion.TabIndex = 82;
            this.fraLeaseUseTaxVersion.Text = "Lease Use Tax Form Version";
            // 
            // cmdPrintLeasedUseTaxTest
            // 
            this.cmdPrintLeasedUseTaxTest.AppearanceKey = "actionButton";
            this.cmdPrintLeasedUseTaxTest.Location = new System.Drawing.Point(391, 30);
            this.cmdPrintLeasedUseTaxTest.Name = "cmdPrintLeasedUseTaxTest";
            this.cmdPrintLeasedUseTaxTest.Size = new System.Drawing.Size(116, 40);
            this.cmdPrintLeasedUseTaxTest.TabIndex = 83;
            this.cmdPrintLeasedUseTaxTest.Text = "Print Test";
            this.cmdPrintLeasedUseTaxTest.Click += new System.EventHandler(this.cmdPrintLeasedUseTaxTest_Click);
            // 
            // fraUseTaxOptions
            // 
            this.fraUseTaxOptions.AppearanceKey = "groupBoxLeftBorder";
            this.fraUseTaxOptions.Controls.Add(this.cmdPrintUseTaxTest);
            this.fraUseTaxOptions.Controls.Add(this.chkUTCPrintForm);
            this.fraUseTaxOptions.Controls.Add(this.chkUTCDuplex);
            this.fraUseTaxOptions.Controls.Add(this.fraUTPrintAdj);
            this.fraUseTaxOptions.Location = new System.Drawing.Point(20, 30);
            this.fraUseTaxOptions.Name = "fraUseTaxOptions";
            this.fraUseTaxOptions.Size = new System.Drawing.Size(527, 264);
            this.fraUseTaxOptions.TabIndex = 58;
            this.fraUseTaxOptions.Text = "Use Tax Form Options";
            // 
            // cmdPrintUseTaxTest
            // 
            this.cmdPrintUseTaxTest.AppearanceKey = "actionButton";
            this.cmdPrintUseTaxTest.Location = new System.Drawing.Point(391, 30);
            this.cmdPrintUseTaxTest.Name = "cmdPrintUseTaxTest";
            this.cmdPrintUseTaxTest.Size = new System.Drawing.Size(118, 40);
            this.cmdPrintUseTaxTest.TabIndex = 103;
            this.cmdPrintUseTaxTest.Text = "Print Test";
            this.cmdPrintUseTaxTest.Click += new System.EventHandler(this.cmdPrintUseTaxTest_Click);
            // 
            // chkUTCPrintForm
            // 
            this.chkUTCPrintForm.Location = new System.Drawing.Point(20, 67);
            this.chkUTCPrintForm.Name = "chkUTCPrintForm";
            this.chkUTCPrintForm.Size = new System.Drawing.Size(262, 22);
            this.chkUTCPrintForm.TabIndex = 102;
            this.chkUTCPrintForm.Text = "Print Use Tax Certificate to Blank Paper";
            // 
            // chkUTCDuplex
            // 
            this.chkUTCDuplex.Location = new System.Drawing.Point(20, 30);
            this.chkUTCDuplex.Name = "chkUTCDuplex";
            this.chkUTCDuplex.Size = new System.Drawing.Size(275, 22);
            this.chkUTCDuplex.TabIndex = 101;
            this.chkUTCDuplex.Text = "Printer Supports Duplex (2-Sided) Printing";
            // 
            // fraUTPrintAdj
            // 
            this.fraUTPrintAdj.Controls.Add(this.txtUTCTopAdj);
            this.fraUTPrintAdj.Controls.Add(this.txtUTCLeftAdj);
            this.fraUTPrintAdj.Controls.Add(this.lblUTCTopAdj);
            this.fraUTPrintAdj.Controls.Add(this.lblUTCLeftAdj);
            this.fraUTPrintAdj.Location = new System.Drawing.Point(20, 104);
            this.fraUTPrintAdj.Name = "fraUTPrintAdj";
            this.fraUTPrintAdj.Size = new System.Drawing.Size(507, 140);
            this.fraUTPrintAdj.TabIndex = 96;
            this.fraUTPrintAdj.Text = "Printer Adjustments";
            // 
            // txtUTCTopAdj
            // 
            this.txtUTCTopAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCTopAdj.Location = new System.Drawing.Point(292, 30);
            this.txtUTCTopAdj.Name = "txtUTCTopAdj";
            this.txtUTCTopAdj.Size = new System.Drawing.Size(195, 40);
            this.txtUTCTopAdj.TabIndex = 98;
            this.txtUTCTopAdj.TextChanged += new System.EventHandler(this.txtUTCTopAdj_TextChanged);
            this.txtUTCTopAdj.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCTopAdj_Validating);
            this.txtUTCTopAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtUTCTopAdj_KeyPress);
            // 
            // txtUTCLeftAdj
            // 
            this.txtUTCLeftAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtUTCLeftAdj.Location = new System.Drawing.Point(292, 80);
            this.txtUTCLeftAdj.Name = "txtUTCLeftAdj";
            this.txtUTCLeftAdj.Size = new System.Drawing.Size(195, 40);
            this.txtUTCLeftAdj.TabIndex = 97;
            this.txtUTCLeftAdj.TextChanged += new System.EventHandler(this.txtUTCLeftAdj_TextChanged);
            this.txtUTCLeftAdj.Validating += new System.ComponentModel.CancelEventHandler(this.txtUTCLeftAdj_Validating);
            this.txtUTCLeftAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtUTCLeftAdj_KeyPress);
            // 
            // lblUTCTopAdj
            // 
            this.lblUTCTopAdj.Location = new System.Drawing.Point(20, 44);
            this.lblUTCTopAdj.Name = "lblUTCTopAdj";
            this.lblUTCTopAdj.Size = new System.Drawing.Size(242, 15);
            this.lblUTCTopAdj.TabIndex = 100;
            this.lblUTCTopAdj.Text = "TOP ADJUSTMENT IN INCHES (-0.5 TO 0.5)";
            // 
            // lblUTCLeftAdj
            // 
            this.lblUTCLeftAdj.Location = new System.Drawing.Point(20, 94);
            this.lblUTCLeftAdj.Name = "lblUTCLeftAdj";
            this.lblUTCLeftAdj.Size = new System.Drawing.Size(242, 15);
            this.lblUTCLeftAdj.TabIndex = 99;
            this.lblUTCLeftAdj.Text = "LEFT ADJUSTMENT IN INCHES (-0.3 TO 0.3)";
            // 
            // fraPrintingOptions
            // 
            this.fraPrintingOptions.AppearanceKey = "groupBoxLeftBorder";
            this.fraPrintingOptions.Controls.Add(this.fraMVR3E_Adjustments);
            this.fraPrintingOptions.Controls.Add(this.fraLaserAdjustmentOptions);
            this.fraPrintingOptions.Controls.Add(this.txtPrinterGroup);
            this.fraPrintingOptions.Controls.Add(this.lblInstructions);
            this.fraPrintingOptions.Location = new System.Drawing.Point(30, 80);
            this.fraPrintingOptions.Name = "fraPrintingOptions";
            this.fraPrintingOptions.Size = new System.Drawing.Size(605, 455);
            this.fraPrintingOptions.TabIndex = 31;
            this.fraPrintingOptions.Text = "Printing";
            this.fraPrintingOptions.Visible = false;
            // 
            // fraMVR3E_Adjustments
            // 
            this.fraMVR3E_Adjustments.Controls.Add(this.txtMVR3E_LeftAdj);
            this.fraMVR3E_Adjustments.Controls.Add(this.txtMVR3E_TopAdj);
            this.fraMVR3E_Adjustments.Controls.Add(this.lblMVR3E_LeftAdj);
            this.fraMVR3E_Adjustments.Controls.Add(this.lblMVR3E_TopAdj);
            this.fraMVR3E_Adjustments.FormatCaption = false;
            this.fraMVR3E_Adjustments.Location = new System.Drawing.Point(20, 295);
            this.fraMVR3E_Adjustments.Name = "fraMVR3E_Adjustments";
            this.fraMVR3E_Adjustments.Size = new System.Drawing.Size(396, 140);
            this.fraMVR3E_Adjustments.TabIndex = 91;
            this.fraMVR3E_Adjustments.Text = "MVR3E Adjustments";
            // 
            // txtMVR3E_LeftAdj
            // 
            this.txtMVR3E_LeftAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtMVR3E_LeftAdj.Location = new System.Drawing.Point(291, 80);
            this.txtMVR3E_LeftAdj.Name = "txtMVR3E_LeftAdj";
            this.txtMVR3E_LeftAdj.Size = new System.Drawing.Size(85, 40);
            this.txtMVR3E_LeftAdj.TabIndex = 93;
            this.txtMVR3E_LeftAdj.TextChanged += new System.EventHandler(this.txtMVR3E_LeftAdj_TextChanged);
            this.txtMVR3E_LeftAdj.Validating += new System.ComponentModel.CancelEventHandler(this.txtMVR3E_LeftAdj_Validating);
            this.txtMVR3E_LeftAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMVR3E_LeftAdj_KeyPress);
            // 
            // txtMVR3E_TopAdj
            // 
            this.txtMVR3E_TopAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtMVR3E_TopAdj.Location = new System.Drawing.Point(291, 30);
            this.txtMVR3E_TopAdj.Name = "txtMVR3E_TopAdj";
            this.txtMVR3E_TopAdj.Size = new System.Drawing.Size(85, 40);
            this.txtMVR3E_TopAdj.TabIndex = 92;
            this.txtMVR3E_TopAdj.TextChanged += new System.EventHandler(this.txtMVR3E_TopAdj_TextChanged);
            this.txtMVR3E_TopAdj.Validating += new System.ComponentModel.CancelEventHandler(this.txtMVR3E_TopAdj_Validating);
            this.txtMVR3E_TopAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMVR3E_TopAdj_KeyPress);
            // 
            // lblMVR3E_LeftAdj
            // 
            this.lblMVR3E_LeftAdj.Location = new System.Drawing.Point(20, 94);
            this.lblMVR3E_LeftAdj.Name = "lblMVR3E_LeftAdj";
            this.lblMVR3E_LeftAdj.Size = new System.Drawing.Size(242, 20);
            this.lblMVR3E_LeftAdj.TabIndex = 95;
            this.lblMVR3E_LeftAdj.Text = "LEFT ADJUSTMENT IN INCHES (-0.5 TO 0.5)";
            // 
            // lblMVR3E_TopAdj
            // 
            this.lblMVR3E_TopAdj.Location = new System.Drawing.Point(20, 44);
            this.lblMVR3E_TopAdj.Name = "lblMVR3E_TopAdj";
            this.lblMVR3E_TopAdj.Size = new System.Drawing.Size(242, 20);
            this.lblMVR3E_TopAdj.TabIndex = 94;
            this.lblMVR3E_TopAdj.Text = "TOP ADJUSTMENT IN INCHES (-0.5 TO 0.5)";
            // 
            // fraLaserAdjustmentOptions
            // 
            this.fraLaserAdjustmentOptions.Controls.Add(this.txtLabelAdjustment);
            this.fraLaserAdjustmentOptions.Controls.Add(this.txtLongTermLaserAdjustment);
            this.fraLaserAdjustmentOptions.Controls.Add(this.Label3);
            this.fraLaserAdjustmentOptions.Controls.Add(this.Label5);
            this.fraLaserAdjustmentOptions.Controls.Add(this.lblInstruct);
            this.fraLaserAdjustmentOptions.Controls.Add(this.Label6);
            this.fraLaserAdjustmentOptions.Location = new System.Drawing.Point(20, 80);
            this.fraLaserAdjustmentOptions.Name = "fraLaserAdjustmentOptions";
            this.fraLaserAdjustmentOptions.Size = new System.Drawing.Size(417, 205);
            this.fraLaserAdjustmentOptions.TabIndex = 75;
            this.fraLaserAdjustmentOptions.Text = "Laser Printer Line Adjustment";
            // 
            // txtLabelAdjustment
            // 
            this.txtLabelAdjustment.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabelAdjustment.Location = new System.Drawing.Point(176, 30);
            this.txtLabelAdjustment.Name = "txtLabelAdjustment";
            this.txtLabelAdjustment.Size = new System.Drawing.Size(81, 40);
            this.txtLabelAdjustment.TabIndex = 77;
            this.txtLabelAdjustment.TextChanged += new System.EventHandler(this.txtLabelAdjustment_TextChanged);
            this.txtLabelAdjustment.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLabelAdjustment_KeyPress);
            // 
            // txtLongTermLaserAdjustment
            // 
            this.txtLongTermLaserAdjustment.BackColor = System.Drawing.SystemColors.Window;
            this.txtLongTermLaserAdjustment.Location = new System.Drawing.Point(176, 80);
            this.txtLongTermLaserAdjustment.Name = "txtLongTermLaserAdjustment";
            this.txtLongTermLaserAdjustment.Size = new System.Drawing.Size(81, 40);
            this.txtLongTermLaserAdjustment.TabIndex = 76;
            this.txtLongTermLaserAdjustment.TextChanged += new System.EventHandler(this.txtLongTermLaserAdjustment_TextChanged);
            this.txtLongTermLaserAdjustment.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLongTermLaserAdjustment_KeyPress);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(145, 20);
            this.Label3.TabIndex = 81;
            this.Label3.Text = "LASER LABELS";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 170);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(181, 15);
            this.Label5.TabIndex = 80;
            this.Label5.Text = "EXAMPLES: (1, 1.5, 2)";
            // 
            // lblInstruct
            // 
            this.lblInstruct.Location = new System.Drawing.Point(20, 130);
            this.lblInstruct.Name = "lblInstruct";
            this.lblInstruct.Size = new System.Drawing.Size(377, 30);
            this.lblInstruct.TabIndex = 79;
            this.lblInstruct.Text = "ENTER THE NUMBER OF LINES THAT THE DATA ON YOUR REPORT NEEDS TO BE MOVED.  FRACTI" +
    "ONAL NUMBERS CAN BE ENTERED";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 94);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(145, 20);
            this.Label6.TabIndex = 78;
            this.Label6.Text = "LASER MVRT-10 FORM";
            // 
            // txtPrinterGroup
            // 
            this.txtPrinterGroup.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrinterGroup.Location = new System.Drawing.Point(496, 30);
            this.txtPrinterGroup.MaxLength = 2;
            this.txtPrinterGroup.Name = "txtPrinterGroup";
            this.txtPrinterGroup.Size = new System.Drawing.Size(82, 40);
            this.txtPrinterGroup.TabIndex = 32;
            this.txtPrinterGroup.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtPrinterGroup.TextChanged += new System.EventHandler(this.txtPrinterGroup_TextChanged);
            this.txtPrinterGroup.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPrinterGroup_KeyPress);
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(20, 44);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(449, 15);
            this.lblInstructions.TabIndex = 33;
            this.lblInstructions.Text = "PLEASE ENTER THE NUMBER OF THE PRINTER GROUP YOU WISH TO USE:  1 - 99";
            // 
            // fraLongTermOptions
            // 
            this.fraLongTermOptions.AppearanceKey = "groupBoxLeftBorder";
            this.fraLongTermOptions.Controls.Add(this.cmbLongTermPrintCTAUseTaxYes);
            this.fraLongTermOptions.Controls.Add(this.lblLongTermPrintCTAUseTaxYes);
            this.fraLongTermOptions.Controls.Add(this.cmbLongTermLaserFormsYes);
            this.fraLongTermOptions.Controls.Add(this.lblLongTermLaserFormsYes);
            this.fraLongTermOptions.Controls.Add(this.chkAutoPopulateUnit);
            this.fraLongTermOptions.Controls.Add(this.Frame7);
            this.fraLongTermOptions.Location = new System.Drawing.Point(30, 80);
            this.fraLongTermOptions.Name = "fraLongTermOptions";
            this.fraLongTermOptions.Size = new System.Drawing.Size(487, 377);
            this.fraLongTermOptions.TabIndex = 59;
            this.fraLongTermOptions.Text = "Long Term";
            this.fraLongTermOptions.Visible = false;
            // 
            // chkAutoPopulateUnit
            // 
            this.chkAutoPopulateUnit.Location = new System.Drawing.Point(20, 280);
            this.chkAutoPopulateUnit.Name = "chkAutoPopulateUnit";
            this.chkAutoPopulateUnit.Size = new System.Drawing.Size(304, 22);
            this.chkAutoPopulateUnit.TabIndex = 70;
            this.chkAutoPopulateUnit.Text = "Auto-populate unit numbers on New Reg Batch";
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.txtFTPPassword);
            this.Frame7.Controls.Add(this.txtFTPUser);
            this.Frame7.Controls.Add(this.txtFTPAddress);
            this.Frame7.Controls.Add(this.Label4);
            this.Frame7.Controls.Add(this.Label2);
            this.Frame7.Controls.Add(this.Label1);
            this.Frame7.Location = new System.Drawing.Point(20, 80);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(447, 190);
            this.Frame7.TabIndex = 63;
            this.Frame7.Text = "Bmv Ftp Site Information";
            // 
            // txtFTPPassword
            // 
            this.txtFTPPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtFTPPassword.Location = new System.Drawing.Point(119, 130);
            this.txtFTPPassword.MaxLength = 30;
            this.txtFTPPassword.Name = "txtFTPPassword";
            this.txtFTPPassword.Size = new System.Drawing.Size(308, 40);
            this.txtFTPPassword.TabIndex = 66;
            // 
            // txtFTPUser
            // 
            this.txtFTPUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtFTPUser.Location = new System.Drawing.Point(119, 80);
            this.txtFTPUser.MaxLength = 30;
            this.txtFTPUser.Name = "txtFTPUser";
            this.txtFTPUser.Size = new System.Drawing.Size(308, 40);
            this.txtFTPUser.TabIndex = 65;
            // 
            // txtFTPAddress
            // 
            this.txtFTPAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtFTPAddress.Location = new System.Drawing.Point(119, 30);
            this.txtFTPAddress.MaxLength = 30;
            this.txtFTPAddress.Name = "txtFTPAddress";
            this.txtFTPAddress.Size = new System.Drawing.Size(308, 40);
            this.txtFTPAddress.TabIndex = 64;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 15);
            this.Label4.TabIndex = 69;
            this.Label4.Text = "ADDRESS";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(66, 15);
            this.Label2.TabIndex = 68;
            this.Label2.Text = "USER";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 144);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(66, 15);
            this.Label1.TabIndex = 67;
            this.Label1.Text = "PASSWORD";
            // 
            // fraGeneralOptions
            // 
            this.fraGeneralOptions.AppearanceKey = "groupBoxLeftBorder";
            this.fraGeneralOptions.Controls.Add(this.fraRapidRenewal);
            this.fraGeneralOptions.Controls.Add(this.cmdClearLocks);
            this.fraGeneralOptions.Controls.Add(this.cmbTellerYes);
            this.fraGeneralOptions.Controls.Add(this.lblTellerYes);
            this.fraGeneralOptions.Controls.Add(this.cmbCheckDigits);
            this.fraGeneralOptions.Controls.Add(this.lblCheckDigits);
            this.fraGeneralOptions.Controls.Add(this.cmbExciseRebateNo);
            this.fraGeneralOptions.Controls.Add(this.lblExciseRebateNo);
            this.fraGeneralOptions.Controls.Add(this.cmbYes);
            this.fraGeneralOptions.Controls.Add(this.lblYes);
            this.fraGeneralOptions.Location = new System.Drawing.Point(30, 80);
            this.fraGeneralOptions.Name = "fraGeneralOptions";
            this.fraGeneralOptions.Size = new System.Drawing.Size(857, 381);
            this.fraGeneralOptions.TabIndex = 5;
            this.fraGeneralOptions.Text = "General";
            // 
            // fraRapidRenewal
            // 
            this.fraRapidRenewal.Controls.Add(this.chkCreateJournal);
            this.fraRapidRenewal.Controls.Add(this.fraRevAcct);
            this.fraRapidRenewal.Enabled = false;
            this.fraRapidRenewal.Location = new System.Drawing.Point(390, 30);
            this.fraRapidRenewal.Name = "fraRapidRenewal";
            this.fraRapidRenewal.Size = new System.Drawing.Size(447, 181);
            this.fraRapidRenewal.TabIndex = 15;
            this.fraRapidRenewal.Text = "Rapid Renewal Options";
            // 
            // chkCreateJournal
            // 
            this.chkCreateJournal.Enabled = false;
            this.chkCreateJournal.Location = new System.Drawing.Point(20, 30);
            this.chkCreateJournal.Name = "chkCreateJournal";
            this.chkCreateJournal.Size = new System.Drawing.Size(143, 22);
            this.chkCreateJournal.TabIndex = 16;
            this.chkCreateJournal.Text = "Create C/R Journal";
            this.chkCreateJournal.CheckedChanged += new System.EventHandler(this.chkCreateJournal_CheckedChanged);
            // 
            // fraRevAcct
            // 
            this.fraRevAcct.AppearanceKey = "groupBoxNoBorders";
            this.fraRevAcct.Controls.Add(this.vsRevAcct);
            this.fraRevAcct.Controls.Add(this.vsExpAcct);
            this.fraRevAcct.Controls.Add(this.lblRevAcct);
            this.fraRevAcct.Controls.Add(this.lblExpAcct);
            this.fraRevAcct.Enabled = false;
            this.fraRevAcct.Location = new System.Drawing.Point(20, 67);
            this.fraRevAcct.Name = "fraRevAcct";
            this.fraRevAcct.Size = new System.Drawing.Size(407, 94);
            this.fraRevAcct.TabIndex = 17;
            // 
            // vsRevAcct
            // 
            this.vsRevAcct.ColumnHeadersVisible = false;
            this.vsRevAcct.Enabled = false;
            this.vsRevAcct.ExtendLastCol = true;
            this.vsRevAcct.FixedCols = 0;
            this.vsRevAcct.FixedRows = 0;
            this.vsRevAcct.Location = new System.Drawing.Point(182, 0);
            this.vsRevAcct.Name = "vsRevAcct";
            this.vsRevAcct.RowHeadersVisible = false;
            this.vsRevAcct.Rows = 1;
            this.vsRevAcct.ShowFocusCell = false;
            this.vsRevAcct.Size = new System.Drawing.Size(225, 42);
            this.vsRevAcct.TabIndex = 18;
            this.vsRevAcct.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsRevAcct_ChangeEdit);
            // 
            // vsExpAcct
            // 
            this.vsExpAcct.ColumnHeadersVisible = false;
            this.vsExpAcct.Enabled = false;
            this.vsExpAcct.ExtendLastCol = true;
            this.vsExpAcct.FixedCols = 0;
            this.vsExpAcct.FixedRows = 0;
            this.vsExpAcct.Location = new System.Drawing.Point(182, 52);
            this.vsExpAcct.Name = "vsExpAcct";
            this.vsExpAcct.RowHeadersVisible = false;
            this.vsExpAcct.Rows = 1;
            this.vsExpAcct.ShowFocusCell = false;
            this.vsExpAcct.Size = new System.Drawing.Size(225, 42);
            this.vsExpAcct.TabIndex = 19;
            this.vsExpAcct.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsExpAcct_ChangeEdit);
            // 
            // lblRevAcct
            // 
            this.lblRevAcct.Enabled = false;
            this.lblRevAcct.Location = new System.Drawing.Point(0, 14);
            this.lblRevAcct.Name = "lblRevAcct";
            this.lblRevAcct.Size = new System.Drawing.Size(116, 18);
            this.lblRevAcct.TabIndex = 21;
            this.lblRevAcct.Text = "REVENUE ACCOUNT";
            // 
            // lblExpAcct
            // 
            this.lblExpAcct.Enabled = false;
            this.lblExpAcct.Location = new System.Drawing.Point(0, 66);
            this.lblExpAcct.Name = "lblExpAcct";
            this.lblExpAcct.Size = new System.Drawing.Size(179, 18);
            this.lblExpAcct.TabIndex = 20;
            this.lblExpAcct.Text = "MERCHANT FEE ACCOUNT";
            // 
            // cmdClearLocks
            // 
            this.cmdClearLocks.AppearanceKey = "actionButton";
            this.cmdClearLocks.Location = new System.Drawing.Point(390, 221);
            this.cmdClearLocks.Name = "cmdClearLocks";
            this.cmdClearLocks.Size = new System.Drawing.Size(234, 40);
            this.cmdClearLocks.TabIndex = 71;
            this.cmdClearLocks.Text = "Clear Fleet / Group Lock";
            this.cmdClearLocks.Click += new System.EventHandler(this.cmdClearLocks_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 0;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 1;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdFileSave
            // 
            this.cmdFileSave.AppearanceKey = "acceptButton";
            this.cmdFileSave.Location = new System.Drawing.Point(408, 30);
            this.cmdFileSave.Name = "cmdFileSave";
            this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileSave.Size = new System.Drawing.Size(80, 48);
            this.cmdFileSave.TabIndex = 0;
            this.cmdFileSave.Text = "Save";
            this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmSettings
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(958, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSettings";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Customize";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.Activated += new System.EventHandler(this.frmSettings_Activated);
            this.Resize += new System.EventHandler(this.frmSettings_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSettings_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSettings_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraCTAUseTaxOptions)).EndInit();
            this.fraCTAUseTaxOptions.ResumeLayout(false);
            this.fraCTAUseTaxOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLeaseUseTaxVersion)).EndInit();
            this.fraLeaseUseTaxVersion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintLeasedUseTaxTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUseTaxOptions)).EndInit();
            this.fraUseTaxOptions.ResumeLayout(false);
            this.fraUseTaxOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintUseTaxTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUTCPrintForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUTCDuplex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUTPrintAdj)).EndInit();
            this.fraUTPrintAdj.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraPrintingOptions)).EndInit();
            this.fraPrintingOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMVR3E_Adjustments)).EndInit();
            this.fraMVR3E_Adjustments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraLaserAdjustmentOptions)).EndInit();
            this.fraLaserAdjustmentOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraLongTermOptions)).EndInit();
            this.fraLongTermOptions.ResumeLayout(false);
            this.fraLongTermOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoPopulateUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraGeneralOptions)).EndInit();
            this.fraGeneralOptions.ResumeLayout(false);
            this.fraGeneralOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRapidRenewal)).EndInit();
            this.fraRapidRenewal.ResumeLayout(false);
            this.fraRapidRenewal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateJournal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRevAcct)).EndInit();
            this.fraRevAcct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsRevAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsExpAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearLocks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdFileSave;
    }
}
