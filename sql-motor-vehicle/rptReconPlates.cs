//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReconPlates.
	/// </summary>
	public partial class rptReconPlates : BaseSectionReport
	{
		public rptReconPlates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Reconcilliation Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptReconPlates InstancePtr
		{
			get
			{
				return (rptReconPlates)Sys.GetInstance(typeof(rptReconPlates));
			}
		}

		protected rptReconPlates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReconPlates	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngSOP;
		int lngAR;
		int lngVI;
		int lngAA;
		int lngEOP;
		int lngEndPK;
		int lng1;
		int lng2;
		int lngPK;
		int lngPK1;
		int lngPK2;
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		clsDRWrapper rs1 = new clsDRWrapper();
		clsDRWrapper rsP = new clsDRWrapper();
		clsDRWrapper rsQ = new clsDRWrapper();
		// Amount On Hand At Start
		clsDRWrapper rsR = new clsDRWrapper();
		// Amount Received
		clsDRWrapper rsS = new clsDRWrapper();
		// Amount Issued
		clsDRWrapper rsT = new clsDRWrapper();
		// Amount Adjusted
		clsDRWrapper rsU = new clsDRWrapper();
		// Amount On Hand At End
		int lngQ;
		int lngR;
		int lngS;
		int lngT;
		int lngU;
		//
		string strSSS;
		string strBMVCode = "";
		bool boolFirst;
		bool boolItem;
		string strTC = "";
		string strMM = "";
		int intPageNumber;
		bool blnDontChange;

		private void strPlateReconciliation()
		{
			if (blnDontChange)
			{
				blnDontChange = false;
			}
			else
			{
				strBMVCode = FCConvert.ToString(rsP.Get_Fields_String("BMVCode"));
			}
			TM_XX_Tag:
			;
			lngQ = 0;
			lngR = 0;
			lngS = 0;
			lngT = 0;
			lngU = 0;
			rsQ.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE substring(Type,1,1) = 'P' And substring(Type,4,2) = '" + strBMVCode + "' And PeriodCloseoutID = " + FCConvert.ToString(lngPK));
			if (rsQ.EndOfFile() != true && rsQ.BeginningOfFile() != true)
			{
				rsQ.MoveLast();
				rsQ.MoveFirst();
				while (!rsQ.EndOfFile())
				{
					lngQ += rsQ.Get_Fields_Int32("Number");
					rsQ.MoveNext();
				}
			}
			rsR.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = '" + strBMVCode + "' And AdjustmentCode = 'R' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsR.EndOfFile() != true && rsR.BeginningOfFile() != true)
			{
				rsR.MoveLast();
				rsR.MoveFirst();
				while (!rsR.EndOfFile())
				{
					lngR += rsR.Get_Fields_Int32("QuantityAdjusted");
					rsR.MoveNext();
				}
			}
			rsS.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = '" + strBMVCode + "' And AdjustmentCode = 'I' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsS.EndOfFile() != true && rsS.BeginningOfFile() != true)
			{
				rsS.MoveLast();
				rsS.MoveFirst();
				while (!rsS.EndOfFile())
				{
					lngS += rsS.Get_Fields_Int32("QuantityAdjusted");
					rsS.MoveNext();
				}
			}
			rsT.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' And substring(InventoryType,4,2) = '" + strBMVCode + "' And AdjustmentCode = 'A' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsT.EndOfFile() != true && rsT.BeginningOfFile() != true)
			{
				rsT.MoveLast();
				rsT.MoveFirst();
				while (!rsT.EndOfFile())
				{
					lngT += rsT.Get_Fields_Int32("QuantityAdjusted");
					rsT.MoveNext();
				}
			}
			rsU.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE substring(Type,1,1) = 'P' And substring(Type,4,2) = '" + strBMVCode + "' And PeriodCloseoutID = " + FCConvert.ToString(lngEndPK));
			if (rsU.EndOfFile() != true && rsU.BeginningOfFile() != true)
			{
				rsU.MoveLast();
				rsU.MoveFirst();
				while (!rsU.EndOfFile())
				{
					lngU += rsU.Get_Fields_Int32("Number");
					rsU.MoveNext();
				}
			}
			// 
			txtMonth.Text = strBMVCode;
			txtOnHand.Text = Strings.Format(lngQ, "@@@@@@@@");
			txtRecorded.Text = Strings.Format(lngR, "@@@@@@@@@@");
			txtIssues.Text = Strings.Format(lngS, "@@@@@@@@@@");
			txtAdjusted.Text = Strings.Format(lngT, "@@@@@@@@@@");
			txtEnd.Text = Strings.Format(lngU, "@@@@@@@@@@");
			if (lngQ + lngR - lngS - lngT != lngU)
			{
				txtEnd.Text = txtEnd.Text + "  **";
			}
			lngSOP += lngQ;
			lngAR += lngR;
			lngVI += lngS;
			lngAA += lngT;
			lngEOP += lngU;
			if (strBMVCode == "TL")
			{
				// Put the TM plates next cause they arent in the class table
				strBMVCode = "TM";
				blnDontChange = true;
				return;
			}
			rsP.MoveNext();
		}

		private void strItem()
		{
			boolItem = true;
			strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1);
			if (strTC != "P")
				goto NextTag4;
			if (rs.EndOfFile() != true)
			{
				if (strTC != "P")
					goto GotOne5;
				strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
				txtItem1.Text = Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@");
				txtItem1.Text = txtItem1.Text + "  -  ";
				txtItem1.Text = txtItem1.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");
				rs.MoveNext();
			}
			// 
			if (rs.EndOfFile() != true)
			{
				strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
				strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1);
				if (strTC != "P")
					goto GotOne5;
				txtItem2.Text = Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@");
				txtItem2.Text = txtItem2.Text + "  -  ";
				txtItem2.Text = txtItem2.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");
				rs.MoveNext();
			}
			// 
			if (rs.EndOfFile() != true)
			{
				strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1);
				strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
				if (strTC != "P")
					goto GotOne5;
				txtItem3.Text = Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@");
				txtItem3.Text = txtItem3.Text + "  -  ";
				txtItem3.Text = txtItem3.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");
			}
			GotOne5:
			;
			NextTag4:
			;
			if (rs.EndOfFile() != true)
				rs.MoveNext();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			txtMonth.Text = "";
			txtOnHand.Text = "";
			txtRecorded.Text = "";
			txtIssues.Text = "";
			txtAdjusted.Text = "";
			txtEnd.Text = "";
			txtItem.Text = "";
			txtItem1.Text = "";
			txtItem2.Text = "";
			txtItem3.Text = "";
			if (!rsP.EndOfFile())
			{
				eArgs.EOF = false;
				strPlateReconciliation();
			}
			else if (!rs.EndOfFile())
			{
				if (boolItem == false)
				{
					txtMonth.Text = "TOTAL--";
					txtOnHand.Text = Strings.Format(lngSOP, "@@@@@@@@");
					txtRecorded.Text = Strings.Format(lngAR, "@@@@@@@@@@");
					txtIssues.Text = Strings.Format(lngVI, "@@@@@@@@@@");
					txtAdjusted.Text = Strings.Format(lngAA, "@@@@@@@@@@");
					txtEnd.Text = Strings.Format(lngEOP, "@@@@@@@@@@");
					txtItem.Text = "ITEMIZED LISTING----------------";
				}
				eArgs.EOF = false;
				strItem();
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				// txtVendorID.Text = "TRIO"
				// txtMuni.Text = .fields("Town
				// txtTownCounty.Text = Format(.fields("ResidenceCode"), "00000")
				// txtAgent.Text = Trim$(.fields("ReportAgent"))
				// txtVersion.Text = Format(App.Major, "00") & "." & App.Minor & "." & App.Revision
				// txtPhone.Text = .fields("ReportTelephone
				// lblPage.Caption = "Page " & rptDiskVerification.PageNumber
				// txtDate.Text = Format(Now, "MM/dd/yyyy")
				// txtProcess.Text = strProcessDate
				// txtAuthType.Text = strLevel
				// txtDateReceived.Text = "___/___/___"
				txtDescTitle.Text = "Plates---------------------";
			}
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngPK = lngPK1;
				lng1 = lngPK1;
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngEndPK = lngPK2;
				lng2 = lngPK2;
			}
			// 
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				strSQL = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID < 1 AND InventoryType <> 'PXSXX' AND InventoryType <> 'PXSLT' ORDER BY substring(InventoryType,4,2), Low";
			}
			else
			{
				strSQL = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " AND InventoryType <> 'PXSXX' AND InventoryType <> 'PXSLT' ORDER BY substring(InventoryType,4,2), Low";
			}
			rs.OpenRecordset(strSQL);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
			}
			strSSS = "SELECT DISTINCT BMVCode FROM Class ORDER BY BMVCode";
			rsP.OpenRecordset(strSSS);
			rsP.MoveLast();
			rsP.MoveFirst();
			lngSOP = 0;
			lngAR = 0;
			lngVI = 0;
			lngAA = 0;
			lngEOP = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (txtItem.Text != "")
			{
				txtItem.Top += 180 / 1440F;
				txtItem1.Top += 180 / 1440F;
				txtItem2.Top += 180 / 1440F;
				txtItem3.Top += 180 / 1440F;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			frmReport.InstancePtr.lngInventoryCounter += 1;
			lblPage.Text = "Page " + FCConvert.ToString(frmReport.InstancePtr.lngInventoryCounter);
		}

		private void rptReconPlates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReconPlates properties;
			//rptReconPlates.Caption	= "Inventory Reconcilliation Report";
			//rptReconPlates.Icon	= "rptReconPlates.dsx":0000";
			//rptReconPlates.Left	= 0;
			//rptReconPlates.Top	= 0;
			//rptReconPlates.Width	= 11880;
			//rptReconPlates.Height	= 8595;
			//rptReconPlates.StartUpPosition	= 3;
			//rptReconPlates.SectionData	= "rptReconPlates.dsx":058A;
			//End Unmaped Properties
		}
	}
}
