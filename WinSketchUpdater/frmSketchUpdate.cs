﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinSketchUpdater
{
    public partial class frmSketchUpdate : Form
    {
        private winskt.WinSkt appWinSketch;
        public frmSketchUpdate()
        {
            InitializeComponent();
            this.btnUpdate.Click += BtnUpdate_Click;
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            UpdateSketches(txtSketchPath.Text);
        }

        private void UpdateSketches(string sketchPath)
        {
            if (!Directory.Exists(sketchPath))
            {
                MessageBox.Show("Directory not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
            {
                process.Kill();
            }
            appWinSketch = null;
            var sketchFiles = Directory.GetFiles(sketchPath, "*.skt").ToList();
            foreach (var filename in sketchFiles)
            {
                if (Path.GetExtension(filename).ToLower() == ".skt")
                {
                    rtbOutput.AppendText("\nUpdating " + filename);
                    UpdateSketch(sketchPath, Path.GetFileName(filename));
                }
            }
            foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
            {
                process.Kill();
            }

            appWinSketch = null;
            MessageBox.Show("Sketch images updated", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void UpdateSketch(string sketchFolder,string filename)
        {

            //foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
            //{
            //    process.Kill();                
            //}
            //appWinSketch = null;
            if (appWinSketch == null)
            {
                appWinSketch = new winskt.WinSkt();
            }

            appWinSketch.OpenSketch(Path.Combine(sketchFolder,filename));
           // appWinSketch.SaveSketch(Path.Combine(sketchFolder, filename), 0);
            //appWinSketch.UpdateImage -= AppWinSketch_UpdateImage;
            //appWinSketch.UpdateImage += AppWinSketch_UpdateImage;
            if (appWinSketch.GetImage(0))
            {
                try
                {
                    Bitmap newImage = Utils.GetImageFromClipboard();
                    if (newImage != null )
                    {
                        //save to file
                        string bitmapFileName = filename.Replace(".skt", ".bmp");
                        string bitmapFilePath = Path.Combine(sketchFolder, bitmapFileName);
                        if (File.Exists(bitmapFilePath))
                        {
                            File.Delete(bitmapFilePath);
                        }
                        using (var croppedBitmap = Utils.CropWhiteSpace(newImage))
                        {
                            croppedBitmap.Save(Path.Combine(sketchFolder, bitmapFileName));
                        }
                    }
                }
                catch (Exception ex)
                {
                    rtbOutput.AppendText("\n" + ex.Message);
                }
            }
          
        }

        private void btnSketchPath_Click(object sender, EventArgs e)
        {
            Browse(txtSketchPath.Text);
        }

        private void Browse(string initialPath)
        {
            var path = BrowsePath(initialPath);
            if (!string.IsNullOrWhiteSpace(path))
            {
               txtSketchPath.Text = path;
            }
        }

        private string BrowsePath(string initialPath)
        {
            var fbd = new FolderBrowserDialog();
            fbd.Description = "Choose sketch location";
            fbd.ShowNewFolderButton = false;
            var thePath = "";
            if (initialPath.Trim() != string.Empty)
            {
                if (System.IO.Directory.Exists(initialPath))
                {
                    thePath = initialPath;
                    fbd.SelectedPath = thePath;
                }
            }
            if (string.IsNullOrWhiteSpace(thePath))
            {
                fbd.RootFolder = Environment.SpecialFolder.MyComputer;
            }
            var result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                return fbd.SelectedPath;
            }

            return "";
        }
    }
}
