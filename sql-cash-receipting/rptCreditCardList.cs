﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptCreditCardList.
	/// </summary>
	public partial class rptCreditCardList : BaseSectionReport
	{
		public static rptCreditCardList InstancePtr
		{
			get
			{
				return (rptCreditCardList)Sys.GetInstance(typeof(rptCreditCardList));
			}
		}

		protected rptCreditCardList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			rsCC.DisposeOf();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCreditCardList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/12/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/09/2005              *
		// ********************************************************
		clsDRWrapper rsCC = new clsDRWrapper();
		int lngCT;

		public rptCreditCardList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Credit/Debit Card List";
            this.ReportEnd += RptCreditCardList_ReportEnd;
		}

        private void RptCreditCardList_ReportEnd(object sender, EventArgs e)
        {
            rsCC.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsCC.EndOfFile();
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCT = 1;
			rsCC.OpenRecordset("SELECT * FROM CCType WHERE SystemDefined = 0 ORDER BY Type", modExtraModules.strCRDatabase);
			if (rsCC.EndOfFile())
			{
				MessageBox.Show("No credit/debit cards setup.", "Setup Credit/Debit Cards", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				// show a message on the report if there are no banks in the table
				lblName.Left = 0;
				lblName.Width = this.PrintWidth;
				lblName.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
				lblName.Text = "There are no credit/debit cards setup in this system.  To add a credit/debit card, go to 'Credit/Debit Card Maintenance'.";
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will show the next bank information
			if (rsCC.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				fldName.Text = FCConvert.ToString(rsCC.Get_Fields("Type"));
				fldNumber.Text = FCConvert.ToString(lngCT);
				lngCT += 1;
				rsCC.MoveNext();
			}
			else
			{
				fldName.Text = "";
				fldNumber.Text = "";
			}
		}

		private void rptCreditCardList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCreditCardList.Caption	= "Credit/Debit Card List";
			//rptCreditCardList.Icon	= "rptCreditCardList.dsx":0000";
			//rptCreditCardList.Left	= 0;
			//rptCreditCardList.Top	= 0;
			//rptCreditCardList.Width	= 11880;
			//rptCreditCardList.Height	= 8595;
			//rptCreditCardList.StartUpPosition	= 3;
			//rptCreditCardList.SectionData	= "rptCreditCardList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
