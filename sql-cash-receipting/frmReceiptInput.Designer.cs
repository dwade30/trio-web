//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using GrapeCity.ActiveReports;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmReceiptInput.
	/// </summary>
	partial class frmReceiptInput : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCashOutType;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtTitle;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTitle;
		public fecherFoundation.FCPanel fraPaidBy;
		public fecherFoundation.FCTextBox txtCustNum_PaidBy;
		public fecherFoundation.FCButton cmdSearch_PaidBy;
		public fecherFoundation.FCButton cmdEdit_PaidBy;
		public fecherFoundation.FCTextBox txtPaidBy;
		public fecherFoundation.FCLabel lblCustNum_PaidBy;
		public fecherFoundation.FCPictureBox imgMemo_PaidBy;
		//public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCLabel lblPaidBy;
		public fecherFoundation.FCPanel fraReceipt;
		public fecherFoundation.FCTextBox txtCustNum_Name;
		public fecherFoundation.FCButton cmdSearch_Name;
		public fecherFoundation.FCButton cmdEdit_Name;
		public fecherFoundation.FCFrame fraAcct;
		public FCGrid txtDefaultCashAccount;
		public fecherFoundation.FCCheckBox chkAffectCash;
		public fecherFoundation.FCCheckBox chkPrint;
		public fecherFoundation.FCComboBox cmbCopies;
		public fecherFoundation.FCCheckBox chkZeroReceipt;
		public FCGrid txtAcct1;
		public fecherFoundation.FCLabel lblAcctDescription;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCComboBox cmbResCode;
		public fecherFoundation.FCTextBox txtPercentageAmount;
		public fecherFoundation.FCComboBox cmbMultiple;
		public fecherFoundation.FCTextBox txtFirst;
		public fecherFoundation.FCTextBox txtComment;
		public FCGrid vsFees;
		//public FCGrid cmbType;
        public fecherFoundation.FCListViewComboBox cmbType;
        public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraAutoAction;
		public fecherFoundation.FCLabel lblAutoCode;
		public fecherFoundation.FCTextBox txtTitle_2;
		public fecherFoundation.FCTextBox txtTitle_1;
		public fecherFoundation.FCTextBox txtTitle_0;
		public fecherFoundation.FCTextBox txtTitle_3;
		public fecherFoundation.FCLabel lblTitle_2;
		public fecherFoundation.FCLabel lblTitle_1;
		public fecherFoundation.FCLabel lblTitle_0;
		public fecherFoundation.FCLabel lblTitle_3;
		public fecherFoundation.FCLabel lblCustNum_Name;
		public fecherFoundation.FCPictureBox imgMemo_Name;
		public fecherFoundation.FCLabel lblResCode;
		public fecherFoundation.FCLabel lblPercentAmount;
		public fecherFoundation.FCLabel lblMultiple;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel label5;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblTypeDescription;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblArrayIndex;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCLabel lblTellerNumber;
		public fecherFoundation.FCPanel fraCashOut;
		public fecherFoundation.FCTextBox txtTransactions;
		public fecherFoundation.FCTextBox txtConvenienceFee;
		public fecherFoundation.FCFrame fraTransDetailsCC;
		public fecherFoundation.FCComboBox cboExpYear;
		public fecherFoundation.FCComboBox cboExpMonth;
		public fecherFoundation.FCButton cmdClearCC;
		public fecherFoundation.FCTextBox txtCVV2;
		public fecherFoundation.FCTextBox txtCCNum;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblCVV;
		public fecherFoundation.FCLabel lblExpDate;
		public fecherFoundation.FCLabel lblCCNum;
		public fecherFoundation.FCFrame fraTransDetailsCheck;
		public fecherFoundation.FCTextBox txtChkAcctName;
		public fecherFoundation.FCComboBox cmbCheckType;
		public fecherFoundation.FCComboBox cmbCheckAcctType;
		public fecherFoundation.FCTextBox txtCheckAcctNum;
		public fecherFoundation.FCLabel lblChkAcctName;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblCheckAcctType;
		public fecherFoundation.FCLabel lblAcctNum;
		public fecherFoundation.FCTextBox txtCashOutCashPaid;
		public fecherFoundation.FCTextBox txtCashOutCheckPaid;
		public fecherFoundation.FCTextBox txtCashOutChange;
		public fecherFoundation.FCButton cmdCashOut;
		public fecherFoundation.FCTextBox txtCashOutTotalDue;
		public fecherFoundation.FCTextBox txtCashOutCreditPaid;
		public fecherFoundation.FCTextBox txtCashOutTotal;
		public fecherFoundation.FCTextBox txtCashOutCheckNumber;
		public fecherFoundation.FCTextBox txtPayment;
		public fecherFoundation.FCComboBox cmbCashOutCC;
		public fecherFoundation.FCComboBox cmbBank;
		public fecherFoundation.FCTextBox txtTrack2;
		public FCGrid vsPayments;
		public fecherFoundation.FCLabel lblTransactions;
		//public fecherFoundation.FCLine Line3;
		public fecherFoundation.FCLabel lblConvenienceFee;
		public fecherFoundation.FCLabel lblCashOutCashPaid;
		public fecherFoundation.FCLabel lblCashOutCheckPaid;
		public fecherFoundation.FCLabel lblCashOutChange;
		public fecherFoundation.FCLabel lblCardBank;
		public fecherFoundation.FCLabel lblCashOutType;
		//public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel lblCashOutPayment;
		public fecherFoundation.FCLabel lblCashOutTotalDue;
		public fecherFoundation.FCLabel lblCashOutCreditPaid;
		public fecherFoundation.FCLabel lblCashOutTotal;
		public fecherFoundation.FCFrame fraVoid;
		public fecherFoundation.FCTextBox txtReceiptNumber;
		public fecherFoundation.FCButton cmdVoidProcess;
		public fecherFoundation.FCButton cmdVoidCancel;
		public fecherFoundation.FCLabel lblReceiptNumber;
		public fecherFoundation.FCPanel fraTotal;
		public fecherFoundation.FCTextBox txtTotalDue;
		public fecherFoundation.FCLabel lblRunningCount;
		public fecherFoundation.FCLabel lblTotal;
		public fecherFoundation.FCLabel lblCount;
		public fecherFoundation.FCFrame fraBank;
		public fecherFoundation.FCButton cmdBankCancel;
		public fecherFoundation.FCButton cmdBankDone;
		public fecherFoundation.FCTextBox txtBankName;
		public fecherFoundation.FCTextBox txtBankRT;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCFrame fraPaymentInfo;
		public FCGrid vsPaymentInfo;
		public fecherFoundation.FCFrame fraTeller;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCTextBox txtTellerID;
		public fecherFoundation.FCPanel fraSummary;
		public FCGrid vsSummary;
		public fecherFoundation.FCFrame fraCreditReceiptNum;
		public fecherFoundation.FCButton cmdCreditReceiptCancel;
		public fecherFoundation.FCButton cmdCreditReceiptOK;
		public fecherFoundation.FCTextBox txtCreditReceiptNum;
		public fecherFoundation.FCLabel lblCreditReceiptNum;
		public fecherFoundation.FCFrame fraMultiAccts;
		public fecherFoundation.FCButton cmdMultiAcctsOK;
		public FCGrid vsMultiAccts;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileBack;
		public fecherFoundation.FCToolStripMenuItem mnuFileVoidByReceipt;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPayment;
		public fecherFoundation.FCToolStripMenuItem mnuFileNext;
		public fecherFoundation.FCToolStripMenuItem mnuFileEdit;
		public fecherFoundation.FCToolStripMenuItem mnuFileCashOut;
		public fecherFoundation.FCToolStripMenuItem mnuSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
        public fecherFoundation.FCToolStripMenuItem mnuImportMooringReceipts;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiptInput));
			this.cmbCashOutType = new fecherFoundation.FCComboBox();
			this.fraPaidBy = new fecherFoundation.FCPanel();
			this.txtCustNum_PaidBy = new fecherFoundation.FCTextBox();
			this.cmdSearch_PaidBy = new fecherFoundation.FCButton();
			this.cmdEdit_PaidBy = new fecherFoundation.FCButton();
			this.txtPaidBy = new fecherFoundation.FCTextBox();
			this.lblCustNum_PaidBy = new fecherFoundation.FCLabel();
			this.imgMemo_PaidBy = new fecherFoundation.FCPictureBox();
			this.lblPaidBy = new fecherFoundation.FCLabel();
			this.fraReceipt = new fecherFoundation.FCPanel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraAutoAction = new fecherFoundation.FCFrame();
			this.lblAutoCode = new fecherFoundation.FCLabel();
			this.txtTitle_0 = new fecherFoundation.FCTextBox();
			this.txtTitle_2 = new fecherFoundation.FCTextBox();
			this.txtTitle_1 = new fecherFoundation.FCTextBox();
			this.txtTitle_3 = new fecherFoundation.FCTextBox();
			this.lblTitle_2 = new fecherFoundation.FCLabel();
			this.lblTitle_1 = new fecherFoundation.FCLabel();
			this.lblTitle_0 = new fecherFoundation.FCLabel();
			this.lblTitle_3 = new fecherFoundation.FCLabel();
			this.txtCustNum_Name = new fecherFoundation.FCTextBox();
			this.cmdSearch_Name = new fecherFoundation.FCButton();
			this.cmdEdit_Name = new fecherFoundation.FCButton();
			this.fraAcct = new fecherFoundation.FCFrame();
			this.lblAcctDescription = new fecherFoundation.FCLabel();
			this.txtDefaultCashAccount = new fecherFoundation.FCGrid();
			this.chkAffectCash = new fecherFoundation.FCCheckBox();
			this.chkPrint = new fecherFoundation.FCCheckBox();
			this.cmbCopies = new fecherFoundation.FCComboBox();
			this.chkZeroReceipt = new fecherFoundation.FCCheckBox();
			this.txtAcct1 = new fecherFoundation.FCGrid();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmbResCode = new fecherFoundation.FCComboBox();
			this.txtPercentageAmount = new fecherFoundation.FCTextBox();
			this.cmbMultiple = new fecherFoundation.FCComboBox();
			this.txtFirst = new fecherFoundation.FCTextBox();
			this.txtComment = new fecherFoundation.FCTextBox();
			this.vsFees = new fecherFoundation.FCGrid();
			this.cmbType = new fecherFoundation.FCListViewComboBox();
			this.lblCustNum_Name = new fecherFoundation.FCLabel();
			this.imgMemo_Name = new fecherFoundation.FCPictureBox();
			this.lblResCode = new fecherFoundation.FCLabel();
			this.lblPercentAmount = new fecherFoundation.FCLabel();
			this.lblMultiple = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.label5 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblTypeDescription = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblArrayIndex = new fecherFoundation.FCLabel();
			this.lblDate = new fecherFoundation.FCLabel();
			this.lblComment = new fecherFoundation.FCLabel();
			this.lblTellerNumber = new fecherFoundation.FCLabel();
			this.fraCashOut = new fecherFoundation.FCPanel();
			this.txtTransactions = new fecherFoundation.FCTextBox();
			this.txtConvenienceFee = new fecherFoundation.FCTextBox();
			this.fraTransDetailsCC = new fecherFoundation.FCFrame();
			this.cboExpYear = new fecherFoundation.FCComboBox();
			this.cboExpMonth = new fecherFoundation.FCComboBox();
			this.cmdClearCC = new fecherFoundation.FCButton();
			this.txtCVV2 = new fecherFoundation.FCTextBox();
			this.txtCCNum = new fecherFoundation.FCTextBox();
			this.Label7 = new fecherFoundation.FCLabel();
			this.lblCVV = new fecherFoundation.FCLabel();
			this.lblExpDate = new fecherFoundation.FCLabel();
			this.lblCCNum = new fecherFoundation.FCLabel();
			this.fraTransDetailsCheck = new fecherFoundation.FCFrame();
			this.txtChkAcctName = new fecherFoundation.FCTextBox();
			this.cmbCheckType = new fecherFoundation.FCComboBox();
			this.cmbCheckAcctType = new fecherFoundation.FCComboBox();
			this.txtCheckAcctNum = new fecherFoundation.FCTextBox();
			this.lblChkAcctName = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.lblCheckAcctType = new fecherFoundation.FCLabel();
			this.lblAcctNum = new fecherFoundation.FCLabel();
			this.txtCashOutCashPaid = new fecherFoundation.FCTextBox();
			this.txtCashOutCheckPaid = new fecherFoundation.FCTextBox();
			this.txtCashOutChange = new fecherFoundation.FCTextBox();
			this.txtCashOutTotalDue = new fecherFoundation.FCTextBox();
			this.txtCashOutCreditPaid = new fecherFoundation.FCTextBox();
			this.txtCashOutTotal = new fecherFoundation.FCTextBox();
			this.txtCashOutCheckNumber = new fecherFoundation.FCTextBox();
			this.txtPayment = new fecherFoundation.FCTextBox();
			this.cmbCashOutCC = new fecherFoundation.FCComboBox();
			this.cmbBank = new fecherFoundation.FCComboBox();
			this.txtTrack2 = new fecherFoundation.FCTextBox();
			this.vsPayments = new fecherFoundation.FCGrid();
			this.lblTransactions = new fecherFoundation.FCLabel();
			this.lblConvenienceFee = new fecherFoundation.FCLabel();
			this.lblCashOutCashPaid = new fecherFoundation.FCLabel();
			this.lblCashOutCheckPaid = new fecherFoundation.FCLabel();
			this.lblCashOutChange = new fecherFoundation.FCLabel();
			this.lblCardBank = new fecherFoundation.FCLabel();
			this.lblCashOutType = new fecherFoundation.FCLabel();
			this.lblCashOutPayment = new fecherFoundation.FCLabel();
			this.lblCashOutTotalDue = new fecherFoundation.FCLabel();
			this.lblCashOutCreditPaid = new fecherFoundation.FCLabel();
			this.lblCashOutTotal = new fecherFoundation.FCLabel();
			this.cmdCashOut = new fecherFoundation.FCButton();
			this.fraVoid = new fecherFoundation.FCFrame();
			this.txtReceiptNumber = new fecherFoundation.FCTextBox();
			this.cmdVoidProcess = new fecherFoundation.FCButton();
			this.cmdVoidCancel = new fecherFoundation.FCButton();
			this.lblReceiptNumber = new fecherFoundation.FCLabel();
			this.fraTotal = new fecherFoundation.FCPanel();
			this.txtTotalDue = new fecherFoundation.FCTextBox();
			this.lblRunningCount = new fecherFoundation.FCLabel();
			this.lblTotal = new fecherFoundation.FCLabel();
			this.lblCount = new fecherFoundation.FCLabel();
			this.fraBank = new fecherFoundation.FCFrame();
			this.cmdBankCancel = new fecherFoundation.FCButton();
			this.cmdBankDone = new fecherFoundation.FCButton();
			this.txtBankName = new fecherFoundation.FCTextBox();
			this.txtBankRT = new fecherFoundation.FCTextBox();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.fraPaymentInfo = new fecherFoundation.FCFrame();
			this.vsPaymentInfo = new fecherFoundation.FCGrid();
			this.fraTeller = new fecherFoundation.FCFrame();
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.txtTellerID = new fecherFoundation.FCTextBox();
			this.fraSummary = new fecherFoundation.FCPanel();
			this.vsSummary = new fecherFoundation.FCGrid();
			this.fraCreditReceiptNum = new fecherFoundation.FCFrame();
			this.cmdCreditReceiptCancel = new fecherFoundation.FCButton();
			this.cmdCreditReceiptOK = new fecherFoundation.FCButton();
			this.txtCreditReceiptNum = new fecherFoundation.FCTextBox();
			this.lblCreditReceiptNum = new fecherFoundation.FCLabel();
			this.fraMultiAccts = new fecherFoundation.FCFrame();
			this.cmdMultiAcctsOK = new fecherFoundation.FCButton();
			this.vsMultiAccts = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileBack = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileVoidByReceipt = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessPayment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileNext = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileEdit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileCashOut = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImportMooringReceipts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSpacer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdVoidByReceipt = new fecherFoundation.FCButton();
			this.cmdAddReceipt = new fecherFoundation.FCButton();
			this.cmdSummary = new fecherFoundation.FCButton();
			this.cmdBack = new fecherFoundation.FCButton();
			this.cmdEdit = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPaidBy)).BeginInit();
			this.fraPaidBy.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch_PaidBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit_PaidBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMemo_PaidBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReceipt)).BeginInit();
			this.fraReceipt.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAutoAction)).BeginInit();
			this.fraAutoAction.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch_Name)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit_Name)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAcct)).BeginInit();
			this.fraAcct.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultCashAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAffectCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkZeroReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMemo_Name)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCashOut)).BeginInit();
			this.fraCashOut.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTransDetailsCC)).BeginInit();
			this.fraTransDetailsCC.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearCC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTransDetailsCheck)).BeginInit();
			this.fraTransDetailsCheck.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCashOut)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVoid)).BeginInit();
			this.fraVoid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTotal)).BeginInit();
			this.fraTotal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBank)).BeginInit();
			this.fraBank.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBankCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBankDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPaymentInfo)).BeginInit();
			this.fraPaymentInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPaymentInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTeller)).BeginInit();
			this.fraTeller.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSummary)).BeginInit();
			this.fraSummary.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCreditReceiptNum)).BeginInit();
			this.fraCreditReceiptNum.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreditReceiptCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreditReceiptOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMultiAccts)).BeginInit();
			this.fraMultiAccts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdMultiAcctsOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsMultiAccts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidByReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBack)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSummary);
			this.BottomPanel.Location = new System.Drawing.Point(0, 740);
			this.BottomPanel.Size = new System.Drawing.Size(1174, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPaidBy);
			this.ClientArea.Controls.Add(this.fraBank);
			this.ClientArea.Controls.Add(this.fraVoid);
			this.ClientArea.Controls.Add(this.fraCreditReceiptNum);
			this.ClientArea.Controls.Add(this.fraMultiAccts);
			this.ClientArea.Controls.Add(this.fraPaymentInfo);
			this.ClientArea.Controls.Add(this.fraSummary);
			this.ClientArea.Controls.Add(this.fraTotal);
			this.ClientArea.Controls.Add(this.fraTeller);
			this.ClientArea.Controls.Add(this.fraCashOut);
			this.ClientArea.Controls.Add(this.fraReceipt);
			this.ClientArea.Size = new System.Drawing.Size(1174, 680);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdEdit);
			this.TopPanel.Controls.Add(this.cmdBack);
			this.TopPanel.Controls.Add(this.cmdAddReceipt);
			this.TopPanel.Controls.Add(this.cmdVoidByReceipt);
			this.TopPanel.Controls.Add(this.cmdCashOut);
			this.TopPanel.Size = new System.Drawing.Size(1174, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdCashOut, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdVoidByReceipt, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddReceipt, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdBack, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEdit, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(157, 30);
			this.HeaderText.Text = "Receipt Input";
			// 
			// cmbCashOutType
			// 
			this.cmbCashOutType.Items.AddRange(new object[] {
            "Cash",
            "Check",
            "Credit Card"});
			this.cmbCashOutType.Location = new System.Drawing.Point(426, 80);
			this.cmbCashOutType.Name = "cmbCashOutType";
			this.cmbCashOutType.Size = new System.Drawing.Size(180, 40);
			this.cmbCashOutType.TabIndex = 124;
			this.cmbCashOutType.SelectedIndexChanged += new System.EventHandler(this.optCashOutType_CheckedChanged);
			// 
			// fraPaidBy
			// 
			this.fraPaidBy.AppearanceKey = "groupBoxNoBorders";
			this.fraPaidBy.Controls.Add(this.txtCustNum_PaidBy);
			this.fraPaidBy.Controls.Add(this.cmdSearch_PaidBy);
			this.fraPaidBy.Controls.Add(this.cmdEdit_PaidBy);
			this.fraPaidBy.Controls.Add(this.txtPaidBy);
			this.fraPaidBy.Controls.Add(this.lblCustNum_PaidBy);
			this.fraPaidBy.Controls.Add(this.imgMemo_PaidBy);
			this.fraPaidBy.Controls.Add(this.lblPaidBy);
			this.fraPaidBy.Name = "fraPaidBy";
			this.fraPaidBy.Size = new System.Drawing.Size(804, 78);
			this.fraPaidBy.TabIndex = 81;
			this.fraPaidBy.Visible = false;
			// 
			// txtCustNum_PaidBy
			// 
			this.txtCustNum_PaidBy.BackColor = System.Drawing.SystemColors.Window;
			this.txtCustNum_PaidBy.Location = new System.Drawing.Point(549, 30);
			this.txtCustNum_PaidBy.Name = "txtCustNum_PaidBy";
			this.txtCustNum_PaidBy.Size = new System.Drawing.Size(85, 40);
			this.txtCustNum_PaidBy.TabIndex = 131;
			this.txtCustNum_PaidBy.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustNum_PaidBy_Validating);
			this.txtCustNum_PaidBy.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustNum_PaidBy_KeyPress);
			// 
			// cmdSearch_PaidBy
			// 
			this.cmdSearch_PaidBy.AppearanceKey = "actionButton";
			this.cmdSearch_PaidBy.ImageSource = "icon - search";
			this.cmdSearch_PaidBy.Location = new System.Drawing.Point(638, 30);
			this.cmdSearch_PaidBy.Name = "cmdSearch_PaidBy";
			this.cmdSearch_PaidBy.Size = new System.Drawing.Size(40, 40);
			this.cmdSearch_PaidBy.TabIndex = 128;
			this.cmdSearch_PaidBy.Click += new System.EventHandler(this.cmdSearch_PaidBy_Click);
			// 
			// cmdEdit_PaidBy
			// 
			this.cmdEdit_PaidBy.Anchor = Wisej.Web.AnchorStyles.Top;
			this.cmdEdit_PaidBy.AppearanceKey = "actionButton";
			this.cmdEdit_PaidBy.Enabled = false;
			this.cmdEdit_PaidBy.ImageSource = "icon - edit";
			this.cmdEdit_PaidBy.Location = new System.Drawing.Point(683, 30);
			this.cmdEdit_PaidBy.Name = "cmdEdit_PaidBy";
			this.cmdEdit_PaidBy.Size = new System.Drawing.Size(40, 40);
			this.cmdEdit_PaidBy.TabIndex = 127;
			this.cmdEdit_PaidBy.Click += new System.EventHandler(this.cmdEdit_PaidBy_Click);
			// 
			// txtPaidBy
			// 
			this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
			this.txtPaidBy.Location = new System.Drawing.Point(107, 30);
			this.txtPaidBy.MaxLength = 50;
			this.txtPaidBy.Name = "txtPaidBy";
			this.txtPaidBy.Size = new System.Drawing.Size(337, 40);
			this.txtPaidBy.TabIndex = 82;
			this.txtPaidBy.Enter += new System.EventHandler(this.txtPaidBy_Enter);
			this.txtPaidBy.Leave += new System.EventHandler(this.txtPaidBy_Leave);
			// 
			// lblCustNum_PaidBy
			// 
			this.lblCustNum_PaidBy.Location = new System.Drawing.Point(489, 44);
			this.lblCustNum_PaidBy.Name = "lblCustNum_PaidBy";
			this.lblCustNum_PaidBy.Size = new System.Drawing.Size(25, 16);
			this.lblCustNum_PaidBy.TabIndex = 132;
			this.lblCustNum_PaidBy.Text = "ID#";
			// 
			// imgMemo_PaidBy
			// 
			this.imgMemo_PaidBy.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgMemo_PaidBy.Image = ((System.Drawing.Image)(resources.GetObject("imgMemo_PaidBy.Image")));
			this.imgMemo_PaidBy.Location = new System.Drawing.Point(728, 30);
			this.imgMemo_PaidBy.Name = "imgMemo_PaidBy";
			this.imgMemo_PaidBy.Size = new System.Drawing.Size(40, 40);
			this.imgMemo_PaidBy.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgMemo_PaidBy.Visible = false;
			this.imgMemo_PaidBy.Click += new System.EventHandler(this.imgMemo_PaidBy_Click);
			// 
			// lblPaidBy
			// 
			this.lblPaidBy.Location = new System.Drawing.Point(20, 44);
			this.lblPaidBy.Name = "lblPaidBy";
			this.lblPaidBy.Size = new System.Drawing.Size(52, 16);
			this.lblPaidBy.TabIndex = 83;
			this.lblPaidBy.Text = "PAID BY";
			// 
			// fraReceipt
			// 
			this.fraReceipt.Anchor = Wisej.Web.AnchorStyles.Top;
			this.fraReceipt.Controls.Add(this.Frame1);
			this.fraReceipt.Controls.Add(this.txtCustNum_Name);
			this.fraReceipt.Controls.Add(this.cmdSearch_Name);
			this.fraReceipt.Controls.Add(this.cmdEdit_Name);
			this.fraReceipt.Controls.Add(this.fraAcct);
			this.fraReceipt.Controls.Add(this.cmbResCode);
			this.fraReceipt.Controls.Add(this.txtPercentageAmount);
			this.fraReceipt.Controls.Add(this.cmbMultiple);
			this.fraReceipt.Controls.Add(this.txtFirst);
			this.fraReceipt.Controls.Add(this.txtComment);
			this.fraReceipt.Controls.Add(this.vsFees);
			this.fraReceipt.Controls.Add(this.cmbType);
			this.fraReceipt.Controls.Add(this.lblCustNum_Name);
			this.fraReceipt.Controls.Add(this.imgMemo_Name);
			this.fraReceipt.Controls.Add(this.lblResCode);
			this.fraReceipt.Controls.Add(this.lblPercentAmount);
			this.fraReceipt.Controls.Add(this.lblMultiple);
			this.fraReceipt.Controls.Add(this.Label1);
			this.fraReceipt.Controls.Add(this.label5);
			this.fraReceipt.Controls.Add(this.Label3);
			this.fraReceipt.Controls.Add(this.lblTypeDescription);
			this.fraReceipt.Controls.Add(this.Label4);
			this.fraReceipt.Controls.Add(this.lblArrayIndex);
			this.fraReceipt.Controls.Add(this.lblDate);
			this.fraReceipt.Controls.Add(this.lblComment);
			this.fraReceipt.Controls.Add(this.lblTellerNumber);
			this.fraReceipt.Location = new System.Drawing.Point(-90, 0);
			this.fraReceipt.Name = "fraReceipt";
			this.fraReceipt.Size = new System.Drawing.Size(1171, 716);
			this.fraReceipt.TabIndex = 19;
			this.fraReceipt.Visible = false;
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.fraAutoAction);
			this.Frame1.Controls.Add(this.txtTitle_0);
			this.Frame1.Controls.Add(this.txtTitle_2);
			this.Frame1.Controls.Add(this.txtTitle_1);
			this.Frame1.Controls.Add(this.txtTitle_3);
			this.Frame1.Controls.Add(this.lblTitle_2);
			this.Frame1.Controls.Add(this.lblTitle_1);
			this.Frame1.Controls.Add(this.lblTitle_0);
			this.Frame1.Controls.Add(this.lblTitle_3);
			this.Frame1.Location = new System.Drawing.Point(10, 166);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(629, 146);
			this.Frame1.TabIndex = 3;
			// 
			// fraAutoAction
			// 
			this.fraAutoAction.Controls.Add(this.lblAutoCode);
			this.fraAutoAction.Location = new System.Drawing.Point(499, 20);
			this.fraAutoAction.Name = "fraAutoAction";
			this.fraAutoAction.Size = new System.Drawing.Size(121, 66);
			this.fraAutoAction.TabIndex = 94;
			this.fraAutoAction.Text = "Re Account";
			this.fraAutoAction.Visible = false;
			// 
			// lblAutoCode
			// 
			this.lblAutoCode.Location = new System.Drawing.Point(20, 30);
			this.lblAutoCode.Name = "lblAutoCode";
			this.lblAutoCode.Size = new System.Drawing.Size(81, 16);
			this.lblAutoCode.TabIndex = 95;
			this.lblAutoCode.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// txtTitle_0
			// 
			this.txtTitle_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_0.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtTitle_0.Location = new System.Drawing.Point(135, 20);
			this.txtTitle_0.MaxLength = 10;
			this.txtTitle_0.Name = "txtTitle_0";
			this.txtTitle_0.Size = new System.Drawing.Size(329, 40);
			this.txtTitle_0.TabIndex = 3;
			this.txtTitle_0.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			this.txtTitle_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			// 
			// txtTitle_2
			// 
			this.txtTitle_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtTitle_2.Location = new System.Drawing.Point(170, 103);
			this.txtTitle_2.MaxLength = 10;
			this.txtTitle_2.Name = "txtTitle_2";
			this.txtTitle_2.Size = new System.Drawing.Size(145, 40);
			this.txtTitle_2.TabIndex = 5;
			this.txtTitle_2.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			this.txtTitle_2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			// 
			// txtTitle_1
			// 
			this.txtTitle_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtTitle_1.Location = new System.Drawing.Point(20, 103);
			this.txtTitle_1.MaxLength = 10;
			this.txtTitle_1.Name = "txtTitle_1";
			this.txtTitle_1.Size = new System.Drawing.Size(145, 40);
			this.txtTitle_1.TabIndex = 4;
			this.txtTitle_1.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			this.txtTitle_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			// 
			// txtTitle_3
			// 
			this.txtTitle_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle_3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtTitle_3.Location = new System.Drawing.Point(322, 103);
			this.txtTitle_3.MaxLength = 10;
			this.txtTitle_3.Name = "txtTitle_3";
			this.txtTitle_3.Size = new System.Drawing.Size(145, 40);
			this.txtTitle_3.TabIndex = 6;
			this.txtTitle_3.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
			this.txtTitle_3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTitle_KeyDown);
			this.txtTitle_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTitle_KeyPress);
			// 
			// lblTitle_2
			// 
			this.lblTitle_2.Location = new System.Drawing.Point(170, 79);
			this.lblTitle_2.Name = "lblTitle_2";
			this.lblTitle_2.Size = new System.Drawing.Size(123, 17);
			this.lblTitle_2.TabIndex = 32;
			this.lblTitle_2.Text = "CONTROL2";
			// 
			// lblTitle_1
			// 
			this.lblTitle_1.Location = new System.Drawing.Point(20, 79);
			this.lblTitle_1.Name = "lblTitle_1";
			this.lblTitle_1.Size = new System.Drawing.Size(135, 17);
			this.lblTitle_1.TabIndex = 31;
			this.lblTitle_1.Text = "CONTROL1";
			// 
			// lblTitle_0
			// 
			this.lblTitle_0.Location = new System.Drawing.Point(20, 34);
			this.lblTitle_0.Name = "lblTitle_0";
			this.lblTitle_0.Size = new System.Drawing.Size(109, 32);
			this.lblTitle_0.TabIndex = 30;
			this.lblTitle_0.Text = "REF";
			// 
			// lblTitle_3
			// 
			this.lblTitle_3.Location = new System.Drawing.Point(322, 79);
			this.lblTitle_3.Name = "lblTitle_3";
			this.lblTitle_3.Size = new System.Drawing.Size(132, 17);
			this.lblTitle_3.TabIndex = 29;
			this.lblTitle_3.Text = "CONTROL3";
			// 
			// txtCustNum_Name
			// 
			this.txtCustNum_Name.BackColor = System.Drawing.SystemColors.Window;
			this.txtCustNum_Name.Location = new System.Drawing.Point(545, 126);
			this.txtCustNum_Name.Name = "txtCustNum_Name";
			this.txtCustNum_Name.Size = new System.Drawing.Size(96, 40);
			this.txtCustNum_Name.TabIndex = 2;
			this.txtCustNum_Name.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustNum_Name_Validating);
			this.txtCustNum_Name.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustNum_Name_KeyPress);
			// 
			// cmdSearch_Name
			// 
			this.cmdSearch_Name.AppearanceKey = "actionButton";
			this.cmdSearch_Name.ImageSource = "icon - search";
			this.cmdSearch_Name.Location = new System.Drawing.Point(647, 126);
			this.cmdSearch_Name.Name = "cmdSearch_Name";
			this.cmdSearch_Name.Size = new System.Drawing.Size(40, 40);
			this.cmdSearch_Name.TabIndex = 126;
			this.cmdSearch_Name.Click += new System.EventHandler(this.cmdSearch_Name_Click);
			// 
			// cmdEdit_Name
			// 
			this.cmdEdit_Name.AppearanceKey = "actionButton";
			this.cmdEdit_Name.Enabled = false;
			this.cmdEdit_Name.ImageSource = "icon - edit";
			this.cmdEdit_Name.Location = new System.Drawing.Point(691, 126);
			this.cmdEdit_Name.Name = "cmdEdit_Name";
			this.cmdEdit_Name.Size = new System.Drawing.Size(40, 40);
			this.cmdEdit_Name.TabIndex = 125;
			this.cmdEdit_Name.Click += new System.EventHandler(this.cmdEdit_Name_Click);
			// 
			// fraAcct
			// 
			this.fraAcct.AppearanceKey = "groupBoxNoBorders";
			this.fraAcct.Controls.Add(this.lblAcctDescription);
			this.fraAcct.Controls.Add(this.txtDefaultCashAccount);
			this.fraAcct.Controls.Add(this.chkAffectCash);
			this.fraAcct.Controls.Add(this.chkPrint);
			this.fraAcct.Controls.Add(this.cmbCopies);
			this.fraAcct.Controls.Add(this.chkZeroReceipt);
			this.fraAcct.Controls.Add(this.txtAcct1);
			this.fraAcct.Controls.Add(this.lblAccount);
			this.fraAcct.Controls.Add(this.Label2);
			this.fraAcct.Location = new System.Drawing.Point(665, 216);
			this.fraAcct.Name = "fraAcct";
			this.fraAcct.Size = new System.Drawing.Size(441, 251);
			this.fraAcct.TabIndex = 33;
			// 
			// lblAcctDescription
			// 
			this.lblAcctDescription.Location = new System.Drawing.Point(19, 211);
			this.lblAcctDescription.Name = "lblAcctDescription";
			this.lblAcctDescription.Size = new System.Drawing.Size(402, 28);
			this.lblAcctDescription.TabIndex = 16;
			this.lblAcctDescription.MouseMove += new Wisej.Web.MouseEventHandler(this.lblAcctDescription_MouseMove);
			// 
			// txtDefaultCashAccount
			// 
			this.txtDefaultCashAccount.Cols = 1;
			this.txtDefaultCashAccount.ColumnHeadersVisible = false;
			this.txtDefaultCashAccount.FixedCols = 0;
			this.txtDefaultCashAccount.FixedRows = 0;
			this.txtDefaultCashAccount.Location = new System.Drawing.Point(20, 168);
			this.txtDefaultCashAccount.Name = "txtDefaultCashAccount";
			this.txtDefaultCashAccount.RowHeadersVisible = false;
			this.txtDefaultCashAccount.Rows = 1;
			this.txtDefaultCashAccount.ShowFocusCell = false;
			this.txtDefaultCashAccount.Size = new System.Drawing.Size(401, 42);
			this.txtDefaultCashAccount.TabIndex = 13;
			this.txtDefaultCashAccount.Visible = false;
			// 
			// chkAffectCash
			// 
			this.chkAffectCash.Checked = true;
			this.chkAffectCash.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkAffectCash.Location = new System.Drawing.Point(154, 64);
			this.chkAffectCash.Name = "chkAffectCash";
			this.chkAffectCash.Size = new System.Drawing.Size(104, 23);
			this.chkAffectCash.TabIndex = 11;
			this.chkAffectCash.Text = "Cash Drawer";
			this.ToolTip1.SetToolTip(this.chkAffectCash, "Affect the cash drawer.");
			// 
			// chkPrint
			// 
			this.chkPrint.Location = new System.Drawing.Point(20, 64);
			this.chkPrint.Name = "chkPrint";
			this.chkPrint.Size = new System.Drawing.Size(53, 23);
			this.chkPrint.TabIndex = 10;
			this.chkPrint.Text = "Print";
			this.ToolTip1.SetToolTip(this.chkPrint, "Print this receipt.");
			// 
			// cmbCopies
			// 
			this.cmbCopies.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCopies.Location = new System.Drawing.Point(227, 18);
			this.cmbCopies.Name = "cmbCopies";
			this.cmbCopies.Size = new System.Drawing.Size(76, 40);
			this.cmbCopies.Sorted = true;
			this.cmbCopies.TabIndex = 9;
			this.cmbCopies.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCopies_KeyDown);
			// 
			// chkZeroReceipt
			// 
			this.chkZeroReceipt.Location = new System.Drawing.Point(20, 24);
			this.chkZeroReceipt.Name = "chkZeroReceipt";
			this.chkZeroReceipt.Size = new System.Drawing.Size(85, 23);
			this.chkZeroReceipt.TabIndex = 8;
			this.chkZeroReceipt.Text = "Zero Rcpt";
			this.ToolTip1.SetToolTip(this.chkZeroReceipt, "This is to allow a zero value receipt.");
			this.chkZeroReceipt.CheckedChanged += new System.EventHandler(this.chkZeroReceipt_CheckedChanged);
			// 
			// txtAcct1
			// 
			this.txtAcct1.Cols = 1;
			this.txtAcct1.ColumnHeadersVisible = false;
			this.txtAcct1.FixedCols = 0;
			this.txtAcct1.FixedRows = 0;
			this.txtAcct1.Location = new System.Drawing.Point(123, 108);
			this.txtAcct1.Name = "txtAcct1";
			this.txtAcct1.RowHeadersVisible = false;
			this.txtAcct1.Rows = 1;
			this.txtAcct1.ShowFocusCell = false;
			this.txtAcct1.Size = new System.Drawing.Size(298, 42);
			this.txtAcct1.TabIndex = 12;
			this.txtAcct1.Visible = false;
			this.txtAcct1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.txtAcct1_ChangeEdit);
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(20, 116);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(75, 16);
			this.lblAccount.TabIndex = 34;
			this.lblAccount.Text = "ACCOUNT #";
			this.lblAccount.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(154, 32);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(47, 16);
			this.Label2.TabIndex = 35;
			this.Label2.Text = "COPIES";
			// 
			// cmbResCode
			// 
			this.cmbResCode.BackColor = System.Drawing.SystemColors.Window;
			this.cmbResCode.Location = new System.Drawing.Point(991, 66);
			this.cmbResCode.Name = "cmbResCode";
			this.cmbResCode.Size = new System.Drawing.Size(98, 40);
			this.cmbResCode.Sorted = true;
			this.cmbResCode.TabIndex = 2;
			this.cmbResCode.Visible = false;
			this.cmbResCode.SelectedIndexChanged += new System.EventHandler(this.cmbResCode_SelectedIndexChanged);
			this.cmbResCode.DoubleClick += new System.EventHandler(this.cmbResCode_DoubleClick);
			// 
			// txtPercentageAmount
			// 
			this.txtPercentageAmount.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercentageAmount.Location = new System.Drawing.Point(445, 611);
			this.txtPercentageAmount.MaxLength = 13;
			this.txtPercentageAmount.Name = "txtPercentageAmount";
			this.txtPercentageAmount.Size = new System.Drawing.Size(108, 40);
			this.txtPercentageAmount.TabIndex = 91;
			this.txtPercentageAmount.Text = "0.00";
			this.txtPercentageAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtPercentageAmount.Visible = false;
			this.txtPercentageAmount.Enter += new System.EventHandler(this.txtPercentageAmount_Enter);
			this.txtPercentageAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtPercentageAmount_Validating);
			this.txtPercentageAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPercentageAmount_KeyPress);
			// 
			// cmbMultiple
			// 
			this.cmbMultiple.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMultiple.Location = new System.Drawing.Point(123, 611);
			this.cmbMultiple.Name = "cmbMultiple";
			this.cmbMultiple.Size = new System.Drawing.Size(97, 40);
			this.cmbMultiple.TabIndex = 14;
			this.cmbMultiple.SelectedIndexChanged += new System.EventHandler(this.cmbMultiple_SelectedIndexChanged);
			// 
			// txtFirst
			// 
			this.txtFirst.BackColor = System.Drawing.SystemColors.Window;
			this.txtFirst.Location = new System.Drawing.Point(97, 126);
			this.txtFirst.MaxLength = 50;
			this.txtFirst.Name = "txtFirst";
			this.txtFirst.Size = new System.Drawing.Size(377, 40);
			this.txtFirst.TabIndex = 1;
			this.txtFirst.Enter += new System.EventHandler(this.txtFirst_Enter);
			this.txtFirst.Leave += new System.EventHandler(this.txtFirst_Leave);
			// 
			// txtComment
			// 
			this.txtComment.BackColor = System.Drawing.SystemColors.Window;
			this.txtComment.Location = new System.Drawing.Point(123, 667);
			this.txtComment.MaxLength = 255;
			this.txtComment.Name = "txtComment";
			this.txtComment.Size = new System.Drawing.Size(894, 40);
			this.txtComment.TabIndex = 15;
			this.txtComment.Enter += new System.EventHandler(this.txtComment_Enter);
			// 
			// vsFees
			// 
			this.vsFees.Cols = 3;
			this.vsFees.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsFees.FixedCols = 2;
			this.vsFees.FrozenCols = 1;
			this.vsFees.Location = new System.Drawing.Point(30, 324);
			this.vsFees.Name = "vsFees";
			this.vsFees.ReadOnly = false;
			this.vsFees.Rows = 7;
			this.vsFees.ShowFocusCell = false;
			this.vsFees.Size = new System.Drawing.Size(609, 275);
			this.vsFees.StandardTab = false;
			this.vsFees.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsFees.TabIndex = 7;
			this.vsFees.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsFees_KeyDownEdit);
			this.vsFees.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsFees_KeyPressEdit);
			this.vsFees.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsFees_AfterEdit);
			this.vsFees.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsFees_ValidateEdit);
			this.vsFees.CurrentCellChanged += new System.EventHandler(this.vsFees_RowColChange);
			this.vsFees.Enter += new System.EventHandler(this.vsFees_Enter);
			this.vsFees.Click += new System.EventHandler(this.vsFees_ClickEvent);
			this.vsFees.DoubleClick += new System.EventHandler(this.vsFees_DblClick);
			// 
			// cmbType
			// 
			this.cmbType.Location = new System.Drawing.Point(97, 66);
			this.cmbType.Name = "cmbType";
			this.cmbType.Size = new System.Drawing.Size(377, 42);
			this.cmbType.TabIndex = 127;
			this.cmbType.Click += new System.EventHandler(this.cmbType_ClickEvent);
			this.cmbType.DoubleClick += new System.EventHandler(this.cmbType_DblClick);
			this.cmbType.Validating += new System.ComponentModel.CancelEventHandler(this.cmbType_Validating);
			this.cmbType.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbType_KeyDownEvent);
			this.cmbType.KeyPress += new Wisej.Web.KeyPressEventHandler(this.cmbType_KeyPressEvent);
			// 
			// lblCustNum_Name
			// 
			this.lblCustNum_Name.Location = new System.Drawing.Point(504, 140);
			this.lblCustNum_Name.Name = "lblCustNum_Name";
			this.lblCustNum_Name.Size = new System.Drawing.Size(25, 16);
			this.lblCustNum_Name.TabIndex = 130;
			this.lblCustNum_Name.Text = "ID#";
			// 
			// imgMemo_Name
			// 
			this.imgMemo_Name.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgMemo_Name.Image = ((System.Drawing.Image)(resources.GetObject("imgMemo_Name.Image")));
			this.imgMemo_Name.Location = new System.Drawing.Point(736, 126);
			this.imgMemo_Name.Name = "imgMemo_Name";
			this.imgMemo_Name.Size = new System.Drawing.Size(40, 40);
			this.imgMemo_Name.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgMemo_Name.Visible = false;
			// 
			// lblResCode
			// 
			this.lblResCode.Location = new System.Drawing.Point(886, 80);
			this.lblResCode.Name = "lblResCode";
			this.lblResCode.Size = new System.Drawing.Size(77, 16);
			this.lblResCode.TabIndex = 96;
			this.lblResCode.Text = "TOWN CODE";
			this.lblResCode.Visible = false;
			// 
			// lblPercentAmount
			// 
			this.lblPercentAmount.Location = new System.Drawing.Point(279, 625);
			this.lblPercentAmount.Name = "lblPercentAmount";
			this.lblPercentAmount.Size = new System.Drawing.Size(135, 16);
			this.lblPercentAmount.TabIndex = 90;
			this.lblPercentAmount.Text = "PERCENTAGE AMOUNT";
			this.lblPercentAmount.Visible = false;
			// 
			// lblMultiple
			// 
			this.lblMultiple.Location = new System.Drawing.Point(30, 625);
			this.lblMultiple.Name = "lblMultiple";
			this.lblMultiple.Size = new System.Drawing.Size(72, 16);
			this.lblMultiple.TabIndex = 89;
			this.lblMultiple.Text = "QUANTITY";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 80);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(35, 16);
			this.Label1.TabIndex = 88;
			this.Label1.Text = "TYPE";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(30, 140);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 16);
			this.label5.TabIndex = 87;
			this.label5.Text = "NAME";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 30);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(38, 16);
			this.Label3.TabIndex = 86;
			this.Label3.Text = "DATE";
			// 
			// lblTypeDescription
			// 
			this.lblTypeDescription.Location = new System.Drawing.Point(501, 80);
			this.lblTypeDescription.Name = "lblTypeDescription";
			this.lblTypeDescription.Size = new System.Drawing.Size(279, 16);
			this.lblTypeDescription.TabIndex = 80;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(298, 30);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(47, 16);
			this.Label4.TabIndex = 27;
			this.Label4.Text = "TELLER";
			// 
			// lblArrayIndex
			// 
			this.lblArrayIndex.Location = new System.Drawing.Point(30, 161);
			this.lblArrayIndex.Name = "lblArrayIndex";
			this.lblArrayIndex.Size = new System.Drawing.Size(51, 18);
			this.lblArrayIndex.TabIndex = 26;
			// 
			// lblDate
			// 
			this.lblDate.Location = new System.Drawing.Point(97, 30);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(79, 16);
			this.lblDate.TabIndex = 22;
			// 
			// lblComment
			// 
			this.lblComment.Location = new System.Drawing.Point(30, 679);
			this.lblComment.Name = "lblComment";
			this.lblComment.Size = new System.Drawing.Size(63, 16);
			this.lblComment.TabIndex = 20;
			this.lblComment.Text = "COMMENT";
			// 
			// lblTellerNumber
			// 
			this.lblTellerNumber.BackColor = System.Drawing.Color.Transparent;
			this.lblTellerNumber.Location = new System.Drawing.Point(380, 30);
			this.lblTellerNumber.Name = "lblTellerNumber";
			this.lblTellerNumber.Size = new System.Drawing.Size(50, 16);
			this.lblTellerNumber.TabIndex = 21;
			// 
			// fraCashOut
			// 
			this.fraCashOut.AppearanceKey = "groupBoxNoBorders";
			this.fraCashOut.Controls.Add(this.txtTransactions);
			this.fraCashOut.Controls.Add(this.cmbCashOutType);
			this.fraCashOut.Controls.Add(this.txtConvenienceFee);
			this.fraCashOut.Controls.Add(this.fraTransDetailsCC);
			this.fraCashOut.Controls.Add(this.fraTransDetailsCheck);
			this.fraCashOut.Controls.Add(this.txtCashOutCashPaid);
			this.fraCashOut.Controls.Add(this.txtCashOutCheckPaid);
			this.fraCashOut.Controls.Add(this.txtCashOutChange);
			this.fraCashOut.Controls.Add(this.txtCashOutTotalDue);
			this.fraCashOut.Controls.Add(this.txtCashOutCreditPaid);
			this.fraCashOut.Controls.Add(this.txtCashOutTotal);
			this.fraCashOut.Controls.Add(this.txtCashOutCheckNumber);
			this.fraCashOut.Controls.Add(this.txtPayment);
			this.fraCashOut.Controls.Add(this.cmbCashOutCC);
			this.fraCashOut.Controls.Add(this.cmbBank);
			this.fraCashOut.Controls.Add(this.txtTrack2);
			this.fraCashOut.Controls.Add(this.vsPayments);
			this.fraCashOut.Controls.Add(this.lblTransactions);
			this.fraCashOut.Controls.Add(this.lblConvenienceFee);
			this.fraCashOut.Controls.Add(this.lblCashOutCashPaid);
			this.fraCashOut.Controls.Add(this.lblCashOutCheckPaid);
			this.fraCashOut.Controls.Add(this.lblCashOutChange);
			this.fraCashOut.Controls.Add(this.lblCardBank);
			this.fraCashOut.Controls.Add(this.lblCashOutType);
			this.fraCashOut.Controls.Add(this.lblCashOutPayment);
			this.fraCashOut.Controls.Add(this.lblCashOutTotalDue);
			this.fraCashOut.Controls.Add(this.lblCashOutCreditPaid);
			this.fraCashOut.Controls.Add(this.lblCashOutTotal);
			this.fraCashOut.Name = "fraCashOut";
			this.fraCashOut.Size = new System.Drawing.Size(1033, 492);
			this.fraCashOut.TabIndex = 39;
			this.fraCashOut.Text = "Cash Out";
			this.fraCashOut.Visible = false;
			this.fraCashOut.Click += new System.EventHandler(this.fraCashOut_Click);
			// 
			// txtTransactions
			// 
			this.txtTransactions.BackColor = System.Drawing.SystemColors.Window;
			this.txtTransactions.Location = new System.Drawing.Point(176, 30);
			this.txtTransactions.LockedOriginal = true;
			this.txtTransactions.Name = "txtTransactions";
			this.txtTransactions.ReadOnly = true;
			this.txtTransactions.Size = new System.Drawing.Size(103, 40);
			this.txtTransactions.TabIndex = 123;
			this.txtTransactions.TabStop = false;
			this.txtTransactions.Text = "0.00";
			this.txtTransactions.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtConvenienceFee
			// 
			this.txtConvenienceFee.BackColor = System.Drawing.SystemColors.Window;
			this.txtConvenienceFee.Location = new System.Drawing.Point(176, 80);
			this.txtConvenienceFee.LockedOriginal = true;
			this.txtConvenienceFee.Name = "txtConvenienceFee";
			this.txtConvenienceFee.ReadOnly = true;
			this.txtConvenienceFee.Size = new System.Drawing.Size(103, 40);
			this.txtConvenienceFee.TabIndex = 121;
			this.txtConvenienceFee.TabStop = false;
			this.txtConvenienceFee.Text = "0.00";
			this.txtConvenienceFee.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// fraTransDetailsCC
			// 
			this.fraTransDetailsCC.AppearanceKey = "groupBoxNoBorders";
			this.fraTransDetailsCC.Controls.Add(this.cboExpYear);
			this.fraTransDetailsCC.Controls.Add(this.cboExpMonth);
			this.fraTransDetailsCC.Controls.Add(this.cmdClearCC);
			this.fraTransDetailsCC.Controls.Add(this.txtCVV2);
			this.fraTransDetailsCC.Controls.Add(this.txtCCNum);
			this.fraTransDetailsCC.Controls.Add(this.Label7);
			this.fraTransDetailsCC.Controls.Add(this.lblCVV);
			this.fraTransDetailsCC.Controls.Add(this.lblExpDate);
			this.fraTransDetailsCC.Controls.Add(this.lblCCNum);
			this.fraTransDetailsCC.Location = new System.Drawing.Point(632, 230);
			this.fraTransDetailsCC.Name = "fraTransDetailsCC";
			this.fraTransDetailsCC.Size = new System.Drawing.Size(263, 171);
			this.fraTransDetailsCC.TabIndex = 102;
			this.fraTransDetailsCC.Visible = false;
			// 
			// cboExpYear
			// 
			this.cboExpYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboExpYear.Location = new System.Drawing.Point(190, 79);
			this.cboExpYear.Name = "cboExpYear";
			this.cboExpYear.Size = new System.Drawing.Size(72, 40);
			this.cboExpYear.Sorted = true;
			this.cboExpYear.TabIndex = 57;
			// 
			// cboExpMonth
			// 
			this.cboExpMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboExpMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
			this.cboExpMonth.Location = new System.Drawing.Point(88, 80);
			this.cboExpMonth.Name = "cboExpMonth";
			this.cboExpMonth.Size = new System.Drawing.Size(72, 40);
			this.cboExpMonth.Sorted = true;
			this.cboExpMonth.TabIndex = 56;
			this.cboExpMonth.Enter += new System.EventHandler(this.cboExpMonth_Enter);
			// 
			// cmdClearCC
			// 
			this.cmdClearCC.AppearanceKey = "actionButton";
			this.cmdClearCC.Location = new System.Drawing.Point(190, 130);
			this.cmdClearCC.Name = "cmdClearCC";
			this.cmdClearCC.Size = new System.Drawing.Size(72, 40);
			this.cmdClearCC.TabIndex = 59;
			this.cmdClearCC.Text = "Clear";
			this.cmdClearCC.Click += new System.EventHandler(this.cmdClearCC_Click);
			// 
			// txtCVV2
			// 
			this.txtCVV2.BackColor = System.Drawing.SystemColors.Window;
			this.txtCVV2.Location = new System.Drawing.Point(88, 130);
			this.txtCVV2.MaxLength = 20;
			this.txtCVV2.Name = "txtCVV2";
			this.txtCVV2.Size = new System.Drawing.Size(76, 40);
			this.txtCVV2.TabIndex = 58;
			// 
			// txtCCNum
			// 
			this.txtCCNum.BackColor = System.Drawing.SystemColors.Window;
			this.txtCCNum.Location = new System.Drawing.Point(88, 30);
			this.txtCCNum.MaxLength = 200;
			this.txtCCNum.Name = "txtCCNum";
			this.txtCCNum.Size = new System.Drawing.Size(174, 40);
			this.txtCCNum.TabIndex = 55;
			this.txtCCNum.Validating += new System.ComponentModel.CancelEventHandler(this.txtCCNum_Validating);
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(170, 94);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(10, 16);
			this.Label7.TabIndex = 120;
			this.Label7.Text = "\\";
			// 
			// lblCVV
			// 
			this.lblCVV.Location = new System.Drawing.Point(1, 144);
			this.lblCVV.Name = "lblCVV";
			this.lblCVV.Size = new System.Drawing.Size(36, 16);
			this.lblCVV.TabIndex = 105;
			this.lblCVV.Text = "CVV2";
			// 
			// lblExpDate
			// 
			this.lblExpDate.Location = new System.Drawing.Point(1, 94);
			this.lblExpDate.Name = "lblExpDate";
			this.lblExpDate.Size = new System.Drawing.Size(52, 16);
			this.lblExpDate.TabIndex = 104;
			this.lblExpDate.Text = "EXPIRES";
			// 
			// lblCCNum
			// 
			this.lblCCNum.Location = new System.Drawing.Point(1, 44);
			this.lblCCNum.Name = "lblCCNum";
			this.lblCCNum.Size = new System.Drawing.Size(50, 18);
			this.lblCCNum.TabIndex = 103;
			this.lblCCNum.Text = "CARD #";
			// 
			// fraTransDetailsCheck
			// 
			this.fraTransDetailsCheck.AppearanceKey = "groupBoxNoBorders";
			this.fraTransDetailsCheck.Controls.Add(this.txtChkAcctName);
			this.fraTransDetailsCheck.Controls.Add(this.cmbCheckType);
			this.fraTransDetailsCheck.Controls.Add(this.cmbCheckAcctType);
			this.fraTransDetailsCheck.Controls.Add(this.txtCheckAcctNum);
			this.fraTransDetailsCheck.Controls.Add(this.lblChkAcctName);
			this.fraTransDetailsCheck.Controls.Add(this.Label9);
			this.fraTransDetailsCheck.Controls.Add(this.lblCheckAcctType);
			this.fraTransDetailsCheck.Controls.Add(this.lblAcctNum);
			this.fraTransDetailsCheck.Location = new System.Drawing.Point(300, 271);
			this.fraTransDetailsCheck.Name = "fraTransDetailsCheck";
			this.fraTransDetailsCheck.Size = new System.Drawing.Size(318, 205);
			this.fraTransDetailsCheck.TabIndex = 106;
			this.fraTransDetailsCheck.Visible = false;
			// 
			// txtChkAcctName
			// 
			this.txtChkAcctName.BackColor = System.Drawing.SystemColors.Window;
			this.txtChkAcctName.Location = new System.Drawing.Point(126, 9);
			this.txtChkAcctName.MaxLength = 20;
			this.txtChkAcctName.Name = "txtChkAcctName";
			this.txtChkAcctName.Size = new System.Drawing.Size(180, 40);
			this.txtChkAcctName.TabIndex = 51;
			// 
			// cmbCheckType
			// 
			this.cmbCheckType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCheckType.Location = new System.Drawing.Point(126, 159);
			this.cmbCheckType.Name = "cmbCheckType";
			this.cmbCheckType.Size = new System.Drawing.Size(180, 40);
			this.cmbCheckType.TabIndex = 54;
			// 
			// cmbCheckAcctType
			// 
			this.cmbCheckAcctType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCheckAcctType.Location = new System.Drawing.Point(126, 109);
			this.cmbCheckAcctType.Name = "cmbCheckAcctType";
			this.cmbCheckAcctType.Size = new System.Drawing.Size(180, 40);
			this.cmbCheckAcctType.TabIndex = 53;
			// 
			// txtCheckAcctNum
			// 
			this.txtCheckAcctNum.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckAcctNum.Location = new System.Drawing.Point(126, 59);
			this.txtCheckAcctNum.MaxLength = 20;
			this.txtCheckAcctNum.Name = "txtCheckAcctNum";
			this.txtCheckAcctNum.Size = new System.Drawing.Size(180, 40);
			this.txtCheckAcctNum.TabIndex = 52;
			// 
			// lblChkAcctName
			// 
			this.lblChkAcctName.Location = new System.Drawing.Point(25, 25);
			this.lblChkAcctName.Name = "lblChkAcctName";
			this.lblChkAcctName.Size = new System.Drawing.Size(42, 16);
			this.lblChkAcctName.TabIndex = 110;
			this.lblChkAcctName.Text = "NAME";
			this.ToolTip1.SetToolTip(this.lblChkAcctName, "Account Holder Name");
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(25, 173);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(65, 16);
			this.Label9.TabIndex = 109;
			this.Label9.Text = "CHK TYPE";
			// 
			// lblCheckAcctType
			// 
			this.lblCheckAcctType.Location = new System.Drawing.Point(25, 123);
			this.lblCheckAcctType.Name = "lblCheckAcctType";
			this.lblCheckAcctType.Size = new System.Drawing.Size(70, 16);
			this.lblCheckAcctType.TabIndex = 108;
			this.lblCheckAcctType.Text = "ACCT TYPE";
			// 
			// lblAcctNum
			// 
			this.lblAcctNum.Location = new System.Drawing.Point(25, 73);
			this.lblAcctNum.Name = "lblAcctNum";
			this.lblAcctNum.Size = new System.Drawing.Size(47, 16);
			this.lblAcctNum.TabIndex = 107;
			this.lblAcctNum.Text = "ACCT #";
			// 
			// txtCashOutCashPaid
			// 
			this.txtCashOutCashPaid.BackColor = System.Drawing.SystemColors.Window;
			this.txtCashOutCashPaid.Location = new System.Drawing.Point(176, 180);
			this.txtCashOutCashPaid.LockedOriginal = true;
			this.txtCashOutCashPaid.Name = "txtCashOutCashPaid";
			this.txtCashOutCashPaid.ReadOnly = true;
			this.txtCashOutCashPaid.Size = new System.Drawing.Size(103, 40);
			this.txtCashOutCashPaid.TabIndex = 63;
			this.txtCashOutCashPaid.TabStop = false;
			this.txtCashOutCashPaid.Text = "0.00";
			this.txtCashOutCashPaid.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtCashOutCheckPaid
			// 
			this.txtCashOutCheckPaid.BackColor = System.Drawing.SystemColors.Window;
			this.txtCashOutCheckPaid.Location = new System.Drawing.Point(176, 230);
			this.txtCashOutCheckPaid.LockedOriginal = true;
			this.txtCashOutCheckPaid.Name = "txtCashOutCheckPaid";
			this.txtCashOutCheckPaid.ReadOnly = true;
			this.txtCashOutCheckPaid.Size = new System.Drawing.Size(103, 40);
			this.txtCashOutCheckPaid.TabIndex = 64;
			this.txtCashOutCheckPaid.TabStop = false;
			this.txtCashOutCheckPaid.Text = "0.00";
			this.txtCashOutCheckPaid.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtCashOutChange
			// 
			this.txtCashOutChange.BackColor = System.Drawing.SystemColors.Window;
			this.txtCashOutChange.Location = new System.Drawing.Point(176, 380);
			this.txtCashOutChange.LockedOriginal = true;
			this.txtCashOutChange.Name = "txtCashOutChange";
			this.txtCashOutChange.ReadOnly = true;
			this.txtCashOutChange.Size = new System.Drawing.Size(103, 40);
			this.txtCashOutChange.TabIndex = 67;
			this.txtCashOutChange.TabStop = false;
			this.txtCashOutChange.Text = "0.00";
			this.txtCashOutChange.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtCashOutChange.Visible = false;
			// 
			// txtCashOutTotalDue
			// 
			this.txtCashOutTotalDue.BackColor = System.Drawing.SystemColors.Window;
			this.txtCashOutTotalDue.Location = new System.Drawing.Point(176, 330);
			this.txtCashOutTotalDue.LockedOriginal = true;
			this.txtCashOutTotalDue.Name = "txtCashOutTotalDue";
			this.txtCashOutTotalDue.ReadOnly = true;
			this.txtCashOutTotalDue.Size = new System.Drawing.Size(103, 40);
			this.txtCashOutTotalDue.TabIndex = 66;
			this.txtCashOutTotalDue.TabStop = false;
			this.txtCashOutTotalDue.Text = "0.00";
			this.txtCashOutTotalDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtCashOutCreditPaid
			// 
			this.txtCashOutCreditPaid.BackColor = System.Drawing.SystemColors.Window;
			this.txtCashOutCreditPaid.Location = new System.Drawing.Point(176, 280);
			this.txtCashOutCreditPaid.LockedOriginal = true;
			this.txtCashOutCreditPaid.Name = "txtCashOutCreditPaid";
			this.txtCashOutCreditPaid.ReadOnly = true;
			this.txtCashOutCreditPaid.Size = new System.Drawing.Size(103, 40);
			this.txtCashOutCreditPaid.TabIndex = 65;
			this.txtCashOutCreditPaid.TabStop = false;
			this.txtCashOutCreditPaid.Text = "0.00";
			this.txtCashOutCreditPaid.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtCashOutTotal
			// 
			this.txtCashOutTotal.BackColor = System.Drawing.SystemColors.Window;
			this.txtCashOutTotal.Location = new System.Drawing.Point(176, 130);
			this.txtCashOutTotal.LockedOriginal = true;
			this.txtCashOutTotal.Name = "txtCashOutTotal";
			this.txtCashOutTotal.ReadOnly = true;
			this.txtCashOutTotal.Size = new System.Drawing.Size(103, 40);
			this.txtCashOutTotal.TabIndex = 62;
			this.txtCashOutTotal.TabStop = false;
			this.txtCashOutTotal.Text = "0.00";
			this.txtCashOutTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtCashOutCheckNumber
			// 
			this.txtCashOutCheckNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtCashOutCheckNumber.Location = new System.Drawing.Point(426, 180);
			this.txtCashOutCheckNumber.MaxLength = 20;
			this.txtCashOutCheckNumber.Name = "txtCashOutCheckNumber";
			this.txtCashOutCheckNumber.Size = new System.Drawing.Size(180, 40);
			this.txtCashOutCheckNumber.TabIndex = 50;
			this.txtCashOutCheckNumber.Visible = false;
			// 
			// txtPayment
			// 
			this.txtPayment.BackColor = System.Drawing.SystemColors.Window;
			this.txtPayment.Location = new System.Drawing.Point(426, 30);
			this.txtPayment.Name = "txtPayment";
			this.txtPayment.Size = new System.Drawing.Size(180, 40);
			this.txtPayment.TabIndex = 44;
			this.txtPayment.Text = "0.00";
			this.txtPayment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtPayment.Enter += new System.EventHandler(this.txtPayment_Enter);
			this.txtPayment.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPayment_KeyPress);
			// 
			// cmbCashOutCC
			// 
			this.cmbCashOutCC.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCashOutCC.Location = new System.Drawing.Point(426, 130);
			this.cmbCashOutCC.Name = "cmbCashOutCC";
			this.cmbCashOutCC.Size = new System.Drawing.Size(180, 40);
			this.cmbCashOutCC.Sorted = true;
			this.cmbCashOutCC.TabIndex = 49;
			this.cmbCashOutCC.Visible = false;
			this.cmbCashOutCC.SelectedIndexChanged += new System.EventHandler(this.cmbCashOutCC_SelectedIndexChanged);
			this.cmbCashOutCC.DropDown += new System.EventHandler(this.cmbCashOutCC_DropDown);
			this.cmbCashOutCC.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCashOutCC_KeyDown);
			// 
			// cmbBank
			// 
			this.cmbBank.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBank.Location = new System.Drawing.Point(426, 130);
			this.cmbBank.Name = "cmbBank";
			this.cmbBank.Size = new System.Drawing.Size(130, 40);
			this.cmbBank.Sorted = true;
			this.cmbBank.TabIndex = 48;
			this.cmbBank.Visible = false;
			this.cmbBank.SelectedIndexChanged += new System.EventHandler(this.cmbBank_SelectedIndexChanged);
			this.cmbBank.DropDown += new System.EventHandler(this.cmbBank_DropDown);
			this.cmbBank.Enter += new System.EventHandler(this.cmbBank_Enter);
			this.cmbBank.TextChanged += new System.EventHandler(this.cmbBank_TextChanged);
			this.cmbBank.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBank_KeyDown);
			// 
			// txtTrack2
			// 
			this.txtTrack2.BackColor = System.Drawing.SystemColors.Window;
			this.txtTrack2.Location = new System.Drawing.Point(426, 230);
			this.txtTrack2.MaxLength = 200;
			this.txtTrack2.Name = "txtTrack2";
			this.txtTrack2.Size = new System.Drawing.Size(180, 40);
			this.txtTrack2.TabIndex = 119;
			this.txtTrack2.Visible = false;
			// 
			// vsPayments
			// 
			this.vsPayments.Cols = 6;
			this.vsPayments.FixedCols = 0;
			this.vsPayments.Location = new System.Drawing.Point(632, 30);
			this.vsPayments.Name = "vsPayments";
			this.vsPayments.RowHeadersVisible = false;
			this.vsPayments.Rows = 1;
			this.vsPayments.ShowFocusCell = false;
			this.vsPayments.Size = new System.Drawing.Size(368, 190);
			this.vsPayments.TabIndex = 78;
			this.vsPayments.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsPayments_MouseMoveEvent);
			this.vsPayments.Enter += new System.EventHandler(this.vsPayments_Enter);
			this.vsPayments.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPayments_KeyDownEvent);
			// 
			// lblTransactions
			// 
			this.lblTransactions.Location = new System.Drawing.Point(30, 44);
			this.lblTransactions.Name = "lblTransactions";
			this.lblTransactions.Size = new System.Drawing.Size(97, 16);
			this.lblTransactions.TabIndex = 124;
			this.lblTransactions.Text = "TRANSACTIONS";
			// 
			// lblConvenienceFee
			// 
			this.lblConvenienceFee.Location = new System.Drawing.Point(30, 94);
			this.lblConvenienceFee.Name = "lblConvenienceFee";
			this.lblConvenienceFee.Size = new System.Drawing.Size(114, 16);
			this.lblConvenienceFee.TabIndex = 122;
			this.lblConvenienceFee.Text = "CONVENIENCE FEE";
			// 
			// lblCashOutCashPaid
			// 
			this.lblCashOutCashPaid.Location = new System.Drawing.Point(30, 194);
			this.lblCashOutCashPaid.Name = "lblCashOutCashPaid";
			this.lblCashOutCashPaid.Size = new System.Drawing.Size(110, 16);
			this.lblCashOutCashPaid.TabIndex = 71;
			this.lblCashOutCashPaid.Text = "TOTAL CASH PAID";
			// 
			// lblCashOutCheckPaid
			// 
			this.lblCashOutCheckPaid.Location = new System.Drawing.Point(30, 244);
			this.lblCashOutCheckPaid.Name = "lblCashOutCheckPaid";
			this.lblCashOutCheckPaid.Size = new System.Drawing.Size(115, 16);
			this.lblCashOutCheckPaid.TabIndex = 70;
			this.lblCashOutCheckPaid.Text = "TOTAL CHECK PAID";
			// 
			// lblCashOutChange
			// 
			this.lblCashOutChange.Location = new System.Drawing.Point(30, 394);
			this.lblCashOutChange.Name = "lblCashOutChange";
			this.lblCashOutChange.Size = new System.Drawing.Size(57, 16);
			this.lblCashOutChange.TabIndex = 69;
			this.lblCashOutChange.Text = "CHANGE";
			this.lblCashOutChange.Visible = false;
			// 
			// lblCardBank
			// 
			this.lblCardBank.Location = new System.Drawing.Point(326, 144);
			this.lblCardBank.Name = "lblCardBank";
			this.lblCardBank.Size = new System.Drawing.Size(94, 16);
			this.lblCardBank.TabIndex = 68;
			this.lblCardBank.Text = "CARD TYPE";
			this.lblCardBank.Visible = false;
			// 
			// lblCashOutType
			// 
			this.lblCashOutType.Location = new System.Drawing.Point(326, 194);
			this.lblCashOutType.Name = "lblCashOutType";
			this.lblCashOutType.Size = new System.Drawing.Size(57, 16);
			this.lblCashOutType.TabIndex = 61;
			this.lblCashOutType.Text = "CHECK #";
			this.lblCashOutType.Visible = false;
			// 
			// lblCashOutPayment
			// 
			this.lblCashOutPayment.Location = new System.Drawing.Point(326, 44);
			this.lblCashOutPayment.Name = "lblCashOutPayment";
			this.lblCashOutPayment.Size = new System.Drawing.Size(79, 16);
			this.lblCashOutPayment.TabIndex = 43;
			this.lblCashOutPayment.Text = "PAYMENT";
			// 
			// lblCashOutTotalDue
			// 
			this.lblCashOutTotalDue.Location = new System.Drawing.Point(30, 344);
			this.lblCashOutTotalDue.Name = "lblCashOutTotalDue";
			this.lblCashOutTotalDue.Size = new System.Drawing.Size(72, 16);
			this.lblCashOutTotalDue.TabIndex = 42;
			this.lblCashOutTotalDue.Text = "TOTAL DUE";
			// 
			// lblCashOutCreditPaid
			// 
			this.lblCashOutCreditPaid.Location = new System.Drawing.Point(30, 294);
			this.lblCashOutCreditPaid.Name = "lblCashOutCreditPaid";
			this.lblCashOutCreditPaid.Size = new System.Drawing.Size(117, 16);
			this.lblCashOutCreditPaid.TabIndex = 41;
			this.lblCashOutCreditPaid.Text = "TOTAL CREDIT PAID";
			// 
			// lblCashOutTotal
			// 
			this.lblCashOutTotal.Location = new System.Drawing.Point(30, 144);
			this.lblCashOutTotal.Name = "lblCashOutTotal";
			this.lblCashOutTotal.Size = new System.Drawing.Size(92, 16);
			this.lblCashOutTotal.TabIndex = 40;
			this.lblCashOutTotal.Text = "RECEIPT TOTAL";
			// 
			// cmdCashOut
			// 
			this.cmdCashOut.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdCashOut.Location = new System.Drawing.Point(565, 29);
			this.cmdCashOut.Name = "cmdCashOut";
			this.cmdCashOut.Size = new System.Drawing.Size(55, 24);
			this.cmdCashOut.TabIndex = 60;
			this.cmdCashOut.Text = "Enter";
			this.cmdCashOut.Click += new System.EventHandler(this.cmdCashOut_Click);
			// 
			// fraVoid
			// 
			this.fraVoid.AppearanceKey = "groupBoxNoBorders";
			this.fraVoid.Controls.Add(this.txtReceiptNumber);
			this.fraVoid.Controls.Add(this.cmdVoidProcess);
			this.fraVoid.Controls.Add(this.cmdVoidCancel);
			this.fraVoid.Controls.Add(this.lblReceiptNumber);
			this.fraVoid.Location = new System.Drawing.Point(10, 0);
			this.fraVoid.Name = "fraVoid";
			this.fraVoid.Size = new System.Drawing.Size(296, 131);
			this.fraVoid.TabIndex = 97;
			this.fraVoid.Visible = false;
			// 
			// txtReceiptNumber
			// 
			this.txtReceiptNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtReceiptNumber.Location = new System.Drawing.Point(160, 30);
			this.txtReceiptNumber.Name = "txtReceiptNumber";
			this.txtReceiptNumber.Size = new System.Drawing.Size(135, 40);
			this.txtReceiptNumber.TabIndex = 98;
			// 
			// cmdVoidProcess
			// 
			this.cmdVoidProcess.AppearanceKey = "actionButton";
			this.cmdVoidProcess.Location = new System.Drawing.Point(20, 90);
			this.cmdVoidProcess.Name = "cmdVoidProcess";
			this.cmdVoidProcess.Size = new System.Drawing.Size(75, 40);
			this.cmdVoidProcess.TabIndex = 99;
			this.cmdVoidProcess.Text = "OK";
			this.cmdVoidProcess.Click += new System.EventHandler(this.cmdVoidProcess_Click);
			// 
			// cmdVoidCancel
			// 
			this.cmdVoidCancel.AppearanceKey = "actionButton";
			this.cmdVoidCancel.Location = new System.Drawing.Point(160, 90);
			this.cmdVoidCancel.Name = "cmdVoidCancel";
			this.cmdVoidCancel.Size = new System.Drawing.Size(75, 40);
			this.cmdVoidCancel.TabIndex = 100;
			this.cmdVoidCancel.Text = "Cancel";
			this.cmdVoidCancel.Click += new System.EventHandler(this.cmdVoidCancel_Click);
			// 
			// lblReceiptNumber
			// 
			this.lblReceiptNumber.Location = new System.Drawing.Point(20, 44);
			this.lblReceiptNumber.Name = "lblReceiptNumber";
			this.lblReceiptNumber.Size = new System.Drawing.Size(105, 16);
			this.lblReceiptNumber.TabIndex = 101;
			this.lblReceiptNumber.Text = "RECEIPT NUMBER";
			// 
			// fraTotal
			// 
			this.fraTotal.AppearanceKey = "groupBoxNoBorders";
			this.fraTotal.Controls.Add(this.txtTotalDue);
			this.fraTotal.Controls.Add(this.lblRunningCount);
			this.fraTotal.Controls.Add(this.lblTotal);
			this.fraTotal.Controls.Add(this.lblCount);
			this.fraTotal.Location = new System.Drawing.Point(10, 131);
			this.fraTotal.Name = "fraTotal";
			this.fraTotal.Size = new System.Drawing.Size(277, 120);
			this.fraTotal.TabIndex = 36;
			this.fraTotal.Visible = false;
			// 
			// txtTotalDue
			// 
			this.txtTotalDue.Appearance = 0;
			this.txtTotalDue.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtTotalDue.Location = new System.Drawing.Point(135, 62);
			this.txtTotalDue.LockedOriginal = true;
			this.txtTotalDue.Name = "txtTotalDue";
			this.txtTotalDue.ReadOnly = true;
			this.txtTotalDue.Size = new System.Drawing.Size(115, 40);
			this.txtTotalDue.TabIndex = 84;
			this.txtTotalDue.TabStop = false;
			this.txtTotalDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtTotalDue.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTotalDue_KeyDown);
			// 
			// lblRunningCount
			// 
			this.lblRunningCount.Location = new System.Drawing.Point(135, 30);
			this.lblRunningCount.Name = "lblRunningCount";
			this.lblRunningCount.Size = new System.Drawing.Size(94, 16);
			this.lblRunningCount.TabIndex = 38;
			this.lblRunningCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblTotal
			// 
			this.lblTotal.Location = new System.Drawing.Point(20, 76);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Size = new System.Drawing.Size(73, 16);
			this.lblTotal.TabIndex = 85;
			this.lblTotal.Text = "TOTAL DUE";
			// 
			// lblCount
			// 
			this.lblCount.Location = new System.Drawing.Point(20, 30);
			this.lblCount.Name = "lblCount";
			this.lblCount.Size = new System.Drawing.Size(80, 16);
			this.lblCount.TabIndex = 37;
			this.lblCount.Text = "ITEM COUNT";
			// 
			// fraBank
			// 
			this.fraBank.Controls.Add(this.cmdBankCancel);
			this.fraBank.Controls.Add(this.cmdBankDone);
			this.fraBank.Controls.Add(this.txtBankName);
			this.fraBank.Controls.Add(this.txtBankRT);
			this.fraBank.Controls.Add(this.Label8);
			this.fraBank.Controls.Add(this.Label6);
			this.fraBank.Location = new System.Drawing.Point(30, 30);
			this.fraBank.Name = "fraBank";
			this.fraBank.Size = new System.Drawing.Size(260, 170);
			this.fraBank.TabIndex = 72;
			this.fraBank.Text = "Add New Bank";
			this.fraBank.Visible = false;
			// 
			// cmdBankCancel
			// 
			this.cmdBankCancel.AppearanceKey = "actionButton";
			this.cmdBankCancel.Location = new System.Drawing.Point(140, 126);
			this.cmdBankCancel.Name = "cmdBankCancel";
			this.cmdBankCancel.Size = new System.Drawing.Size(100, 40);
			this.cmdBankCancel.TabIndex = 79;
			this.cmdBankCancel.Text = "Cancel";
			this.cmdBankCancel.Click += new System.EventHandler(this.cmdBankCancel_Click);
			// 
			// cmdBankDone
			// 
			this.cmdBankDone.AppearanceKey = "actionButton";
			this.cmdBankDone.Location = new System.Drawing.Point(20, 126);
			this.cmdBankDone.Name = "cmdBankDone";
			this.cmdBankDone.Size = new System.Drawing.Size(100, 40);
			this.cmdBankDone.TabIndex = 77;
			this.cmdBankDone.Text = "Enter";
			this.cmdBankDone.Click += new System.EventHandler(this.cmdBankDone_Click);
			// 
			// txtBankName
			// 
			this.txtBankName.BackColor = System.Drawing.SystemColors.Window;
			this.txtBankName.Location = new System.Drawing.Point(20, 66);
			this.txtBankName.MaxLength = 30;
			this.txtBankName.Name = "txtBankName";
			this.txtBankName.Size = new System.Drawing.Size(100, 40);
			this.txtBankName.TabIndex = 75;
			this.txtBankName.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBankName_KeyPress);
			// 
			// txtBankRT
			// 
			this.txtBankRT.BackColor = System.Drawing.SystemColors.Window;
			this.txtBankRT.Location = new System.Drawing.Point(140, 66);
			this.txtBankRT.MaxLength = 20;
			this.txtBankRT.Name = "txtBankRT";
			this.txtBankRT.Size = new System.Drawing.Size(100, 40);
			this.txtBankRT.TabIndex = 76;
			this.txtBankRT.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBankRT_KeyPress);
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(140, 30);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(110, 16);
			this.Label8.TabIndex = 74;
			this.Label8.Text = "ROUTING TRANSIT";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(20, 30);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(40, 16);
			this.Label6.TabIndex = 73;
			this.Label6.Text = "NAME";
			// 
			// fraPaymentInfo
			// 
			this.fraPaymentInfo.AppearanceKey = "groupBoxNoBorders";
			this.fraPaymentInfo.BackColor = System.Drawing.SystemColors.Window;
			this.fraPaymentInfo.Controls.Add(this.vsPaymentInfo);
			this.fraPaymentInfo.Location = new System.Drawing.Point(30, 30);
			this.fraPaymentInfo.Name = "fraPaymentInfo";
			this.fraPaymentInfo.Size = new System.Drawing.Size(845, 215);
			this.fraPaymentInfo.TabIndex = 92;
			this.fraPaymentInfo.Text = "Payment Information";
			this.fraPaymentInfo.Visible = false;
			this.fraPaymentInfo.Click += new System.EventHandler(this.fraPaymentInfo_Click);
			// 
			// vsPaymentInfo
			// 
			this.vsPaymentInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsPaymentInfo.Cols = 5;
			this.vsPaymentInfo.ColumnHeadersVisible = false;
			this.vsPaymentInfo.FixedCols = 0;
			this.vsPaymentInfo.FixedRows = 0;
			this.vsPaymentInfo.Location = new System.Drawing.Point(0, 30);
			this.vsPaymentInfo.Name = "vsPaymentInfo";
			this.vsPaymentInfo.RowHeadersVisible = false;
			this.vsPaymentInfo.Rows = 13;
			this.vsPaymentInfo.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
			this.vsPaymentInfo.ShowFocusCell = false;
			this.vsPaymentInfo.Size = new System.Drawing.Size(845, 182);
			this.vsPaymentInfo.TabIndex = 93;
			// 
			// fraTeller
			// 
			this.fraTeller.Controls.Add(this.txtPassword);
			this.fraTeller.Controls.Add(this.txtTellerID);
			this.fraTeller.FormatCaption = false;
			this.fraTeller.Location = new System.Drawing.Point(30, 30);
			this.fraTeller.Name = "fraTeller";
			this.fraTeller.Size = new System.Drawing.Size(204, 140);
			this.fraTeller.TabIndex = 23;
			this.fraTeller.Text = "Enter Teller ID";
			this.ToolTip1.SetToolTip(this.fraTeller, "Operator ID");
			this.fraTeller.Visible = false;
			// 
			// txtPassword
			// 
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtPassword.Location = new System.Drawing.Point(20, 80);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(170, 40);
			this.txtPassword.TabIndex = 25;
			this.txtPassword.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtPassword, "Password");
			this.txtPassword.Visible = false;
			this.txtPassword.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPassword_KeyDown);
			// 
			// txtTellerID
			// 
			this.txtTellerID.BackColor = System.Drawing.SystemColors.Window;
			this.txtTellerID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtTellerID.Location = new System.Drawing.Point(20, 30);
			this.txtTellerID.MaxLength = 3;
			this.txtTellerID.Name = "txtTellerID";
			this.txtTellerID.Size = new System.Drawing.Size(170, 40);
			this.txtTellerID.TabIndex = 24;
			this.ToolTip1.SetToolTip(this.txtTellerID, "Teller ID");
			this.txtTellerID.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTellerID_KeyDown);
			this.txtTellerID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTellerID_KeyPress);
			// 
			// fraSummary
			// 
			this.fraSummary.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraSummary.AppearanceKey = "groupBoxNoBorders";
			this.fraSummary.Controls.Add(this.vsSummary);
			this.fraSummary.Location = new System.Drawing.Point(10, 0);
			this.fraSummary.Name = "fraSummary";
			this.fraSummary.Size = new System.Drawing.Size(415, 178);
			this.fraSummary.TabIndex = 18;
			this.fraSummary.Visible = false;
			this.fraSummary.Click += new System.EventHandler(this.fraSummary_Click);
			// 
			// vsSummary
			// 
			this.vsSummary.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsSummary.Cols = 3;
			this.vsSummary.FixedCols = 0;
			this.vsSummary.Location = new System.Drawing.Point(20, 30);
			this.vsSummary.Name = "vsSummary";
			this.vsSummary.RowHeadersVisible = false;
			this.vsSummary.Rows = 1;
			this.vsSummary.ShowFocusCell = false;
			this.vsSummary.Size = new System.Drawing.Size(375, 128);
			this.vsSummary.TabIndex = 17;
			this.vsSummary.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsSummary_KeyPressEdit);
			this.vsSummary.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsSummary_AfterEdit);
			this.vsSummary.CurrentCellChanged += new System.EventHandler(this.vsSummary_RowColChange);
			this.vsSummary.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsSummary_MouseDownEvent);
			this.vsSummary.Enter += new System.EventHandler(this.vsSummary_Enter);
			this.vsSummary.DoubleClick += new System.EventHandler(this.vsSummary_DblClick);
			this.vsSummary.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSummary_KeyDownEvent);
			// 
			// fraCreditReceiptNum
			// 
			this.fraCreditReceiptNum.AppearanceKey = "groupBoxNoBorders";
			this.fraCreditReceiptNum.Controls.Add(this.cmdCreditReceiptCancel);
			this.fraCreditReceiptNum.Controls.Add(this.cmdCreditReceiptOK);
			this.fraCreditReceiptNum.Controls.Add(this.txtCreditReceiptNum);
			this.fraCreditReceiptNum.Controls.Add(this.lblCreditReceiptNum);
			this.fraCreditReceiptNum.Location = new System.Drawing.Point(30, 0);
			this.fraCreditReceiptNum.Name = "fraCreditReceiptNum";
			this.fraCreditReceiptNum.Size = new System.Drawing.Size(361, 121);
			this.fraCreditReceiptNum.TabIndex = 111;
			this.fraCreditReceiptNum.Visible = false;
			// 
			// cmdCreditReceiptCancel
			// 
			this.cmdCreditReceiptCancel.AppearanceKey = "actionButton";
			this.cmdCreditReceiptCancel.Location = new System.Drawing.Point(210, 80);
			this.cmdCreditReceiptCancel.Name = "cmdCreditReceiptCancel";
			this.cmdCreditReceiptCancel.Size = new System.Drawing.Size(75, 40);
			this.cmdCreditReceiptCancel.TabIndex = 114;
			this.cmdCreditReceiptCancel.Text = "Cancel";
			this.cmdCreditReceiptCancel.Click += new System.EventHandler(this.cmdCreditReceiptCancel_Click);
			// 
			// cmdCreditReceiptOK
			// 
			this.cmdCreditReceiptOK.AppearanceKey = "actionButton";
			this.cmdCreditReceiptOK.Location = new System.Drawing.Point(100, 80);
			this.cmdCreditReceiptOK.Name = "cmdCreditReceiptOK";
			this.cmdCreditReceiptOK.Size = new System.Drawing.Size(75, 40);
			this.cmdCreditReceiptOK.TabIndex = 113;
			this.cmdCreditReceiptOK.Text = "OK";
			this.cmdCreditReceiptOK.Click += new System.EventHandler(this.cmdCreditReceiptOK_Click);
			// 
			// txtCreditReceiptNum
			// 
			this.txtCreditReceiptNum.BackColor = System.Drawing.SystemColors.Window;
			this.txtCreditReceiptNum.Location = new System.Drawing.Point(225, 30);
			this.txtCreditReceiptNum.Name = "txtCreditReceiptNum";
			this.txtCreditReceiptNum.Size = new System.Drawing.Size(135, 40);
			this.txtCreditReceiptNum.TabIndex = 112;
			// 
			// lblCreditReceiptNum
			// 
			this.lblCreditReceiptNum.Location = new System.Drawing.Point(30, 44);
			this.lblCreditReceiptNum.Name = "lblCreditReceiptNum";
			this.lblCreditReceiptNum.Size = new System.Drawing.Size(160, 16);
			this.lblCreditReceiptNum.TabIndex = 115;
			this.lblCreditReceiptNum.Text = "ORIGINAL RECEIPT NUMBER";
			// 
			// fraMultiAccts
			// 
			this.fraMultiAccts.AppearanceKey = "groupBoxNoBorders";
			this.fraMultiAccts.Controls.Add(this.cmdMultiAcctsOK);
			this.fraMultiAccts.Controls.Add(this.vsMultiAccts);
			this.fraMultiAccts.Location = new System.Drawing.Point(30, 0);
			this.fraMultiAccts.Name = "fraMultiAccts";
			this.fraMultiAccts.Size = new System.Drawing.Size(333, 177);
			this.fraMultiAccts.TabIndex = 116;
			this.fraMultiAccts.Visible = false;
			// 
			// cmdMultiAcctsOK
			// 
			this.cmdMultiAcctsOK.AppearanceKey = "actionButton";
			this.cmdMultiAcctsOK.Location = new System.Drawing.Point(30, 65);
			this.cmdMultiAcctsOK.Name = "cmdMultiAcctsOK";
			this.cmdMultiAcctsOK.Size = new System.Drawing.Size(80, 40);
			this.cmdMultiAcctsOK.TabIndex = 118;
			this.cmdMultiAcctsOK.Text = "OK";
			this.cmdMultiAcctsOK.Click += new System.EventHandler(this.cmdMultiAcctsOK_Click);
			// 
			// vsMultiAccts
			// 
			this.vsMultiAccts.Cols = 3;
			this.vsMultiAccts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsMultiAccts.FixedCols = 2;
			this.vsMultiAccts.FrozenCols = 1;
			this.vsMultiAccts.Location = new System.Drawing.Point(30, 30);
			this.vsMultiAccts.Name = "vsMultiAccts";
			this.vsMultiAccts.ReadOnly = false;
			this.vsMultiAccts.Rows = 7;
			this.vsMultiAccts.ShowFocusCell = false;
			this.vsMultiAccts.Size = new System.Drawing.Size(275, 124);
			this.vsMultiAccts.StandardTab = false;
			this.vsMultiAccts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsMultiAccts.TabIndex = 117;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileBack,
            this.mnuFileVoidByReceipt,
            this.mnuProcessPayment,
            this.mnuFileNext,
            this.mnuFileEdit,
            this.mnuFileCashOut,
            this.mnuImportMooringReceipts,
            this.mnuSpacer,
            this.mnuProcessExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileBack
			// 
			this.mnuFileBack.Index = 0;
			this.mnuFileBack.Name = "mnuFileBack";
			this.mnuFileBack.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuFileBack.Text = "Back to Summary";
			this.mnuFileBack.Visible = false;
			this.mnuFileBack.Click += new System.EventHandler(this.mnuFileBack_Click);
			// 
			// mnuFileVoidByReceipt
			// 
			this.mnuFileVoidByReceipt.Index = 1;
			this.mnuFileVoidByReceipt.Name = "mnuFileVoidByReceipt";
			this.mnuFileVoidByReceipt.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuFileVoidByReceipt.Text = "Void By Receipt Number";
			this.mnuFileVoidByReceipt.Click += new System.EventHandler(this.mnuFileVoidByReceipt_Click);
			// 
			// mnuProcessPayment
			// 
			this.mnuProcessPayment.Index = 2;
			this.mnuProcessPayment.Name = "mnuProcessPayment";
			this.mnuProcessPayment.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuProcessPayment.Text = "Add Reciept";
			this.mnuProcessPayment.Click += new System.EventHandler(this.mnuProcessPayment_Click);
			// 
			// mnuFileNext
			// 
			this.mnuFileNext.Index = 3;
			this.mnuFileNext.Name = "mnuFileNext";
			this.mnuFileNext.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileNext.Text = "Goto Summary";
			this.mnuFileNext.Click += new System.EventHandler(this.mnuFileNext_Click);
			// 
			// mnuFileEdit
			// 
			this.mnuFileEdit.Index = 4;
			this.mnuFileEdit.Name = "mnuFileEdit";
			this.mnuFileEdit.Text = "Edit Receipt Information";
			this.mnuFileEdit.Visible = false;
			this.mnuFileEdit.Click += new System.EventHandler(this.mnuFileEdit_Click);
			// 
			// mnuFileCashOut
			// 
			this.mnuFileCashOut.Index = 5;
			this.mnuFileCashOut.Name = "mnuFileCashOut";
			this.mnuFileCashOut.Text = "Cash Out";
			this.mnuFileCashOut.Visible = false;
			this.mnuFileCashOut.Click += new System.EventHandler(this.mnuFileCashOut_Click);
			// 
			// mnuImportMooringReceipts
			// 
			this.mnuImportMooringReceipts.Index = 6;
			this.mnuImportMooringReceipts.Name = "mnuImportMooringReceipts";
			this.mnuImportMooringReceipts.Text = "Import Mooring Receipts";
			this.mnuImportMooringReceipts.Visible = false;
			this.mnuImportMooringReceipts.Click += new System.EventHandler(this.mnuImportMooringReceipts_Click);
			// 
			// mnuSpacer
			// 
			this.mnuSpacer.Index = 7;
			this.mnuSpacer.Name = "mnuSpacer";
			this.mnuSpacer.Text = "-";
			// 
			// mnuProcessExit
			// 
			this.mnuProcessExit.Index = 8;
			this.mnuProcessExit.Name = "mnuProcessExit";
			this.mnuProcessExit.Text = "Exit";
			this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
			// 
			// cmdVoidByReceipt
			// 
			this.cmdVoidByReceipt.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdVoidByReceipt.Location = new System.Drawing.Point(903, 29);
			this.cmdVoidByReceipt.Name = "cmdVoidByReceipt";
			this.cmdVoidByReceipt.Shortcut = Wisej.Web.Shortcut.F8;
			this.cmdVoidByReceipt.Size = new System.Drawing.Size(168, 24);
			this.cmdVoidByReceipt.TabIndex = 1;
			this.cmdVoidByReceipt.Text = "Void By Receipt Number";
			this.cmdVoidByReceipt.Click += new System.EventHandler(this.mnuFileVoidByReceipt_Click);
			// 
			// cmdAddReceipt
			// 
			this.cmdAddReceipt.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddReceipt.Location = new System.Drawing.Point(1073, 29);
			this.cmdAddReceipt.Name = "cmdAddReceipt";
			this.cmdAddReceipt.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdAddReceipt.Size = new System.Drawing.Size(91, 24);
			this.cmdAddReceipt.TabIndex = 2;
			this.cmdAddReceipt.Text = "Add Receipt";
			this.cmdAddReceipt.Click += new System.EventHandler(this.mnuProcessPayment_Click);
			// 
			// cmdSummary
			// 
			this.cmdSummary.AppearanceKey = "acceptButton";
			this.cmdSummary.Location = new System.Drawing.Point(498, 30);
			this.cmdSummary.Name = "cmdSummary";
			this.cmdSummary.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSummary.Size = new System.Drawing.Size(188, 48);
			this.cmdSummary.TabIndex = 100;
			this.cmdSummary.Text = "Go To Summary";
			this.cmdSummary.Click += new System.EventHandler(this.mnuFileNext_Click);
			// 
			// cmdBack
			// 
			this.cmdBack.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdBack.Location = new System.Drawing.Point(786, 29);
			this.cmdBack.Name = "cmdBack";
			this.cmdBack.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdBack.Size = new System.Drawing.Size(113, 24);
			this.cmdBack.TabIndex = 3;
			this.cmdBack.Text = "Previous Screen";
			this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
			// 
			// cmdEdit
			// 
			this.cmdEdit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEdit.Location = new System.Drawing.Point(623, 29);
			this.cmdEdit.Name = "cmdEdit";
			this.cmdEdit.Size = new System.Drawing.Size(160, 24);
			this.cmdEdit.TabIndex = 4;
			this.cmdEdit.Text = "Edit Receipt Information";
			this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
			// 
			// frmReceiptInput
			// 
			this.ClientSize = new System.Drawing.Size(1174, 848);
			this.Cursor = Wisej.Web.Cursors.Default;
			this.KeyPreview = true;
			this.Name = "frmReceiptInput";
			this.Text = "Receipt Input";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmReceiptInput_Load);
			this.Activated += new System.EventHandler(this.frmReceiptInput_Activated);
			this.Click += new System.EventHandler(this.frmReceiptInput_Click);
			this.Resize += new System.EventHandler(this.frmReceiptInput_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReceiptInput_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPaidBy)).EndInit();
			this.fraPaidBy.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch_PaidBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit_PaidBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMemo_PaidBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReceipt)).EndInit();
			this.fraReceipt.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraAutoAction)).EndInit();
			this.fraAutoAction.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch_Name)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit_Name)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAcct)).EndInit();
			this.fraAcct.ResumeLayout(false);
			this.fraAcct.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultCashAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAffectCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkZeroReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsFees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgMemo_Name)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCashOut)).EndInit();
			this.fraCashOut.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTransDetailsCC)).EndInit();
			this.fraTransDetailsCC.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdClearCC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTransDetailsCheck)).EndInit();
			this.fraTransDetailsCheck.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCashOut)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVoid)).EndInit();
			this.fraVoid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTotal)).EndInit();
			this.fraTotal.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraBank)).EndInit();
			this.fraBank.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdBankCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBankDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPaymentInfo)).EndInit();
			this.fraPaymentInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsPaymentInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTeller)).EndInit();
			this.fraTeller.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSummary)).EndInit();
			this.fraSummary.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCreditReceiptNum)).EndInit();
			this.fraCreditReceiptNum.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCreditReceiptCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreditReceiptOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMultiAccts)).EndInit();
			this.fraMultiAccts.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdMultiAcctsOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsMultiAccts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVoidByReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBack)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
			this.ResumeLayout(false);

		}

   
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdVoidByReceipt;
		public FCButton cmdSummary;
		private FCButton cmdAddReceipt;
		public FCButton cmdBack;
		private FCButton cmdEdit;
	}
}