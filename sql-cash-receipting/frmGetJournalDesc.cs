﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmGetJournalDesc.
	/// </summary>
	public partial class frmGetJournalDesc : BaseForm
	{
		public frmGetJournalDesc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetJournalDesc InstancePtr
		{
			get
			{
				return (frmGetJournalDesc)Sys.GetInstance(typeof(frmGetJournalDesc));
			}
		}

		protected frmGetJournalDesc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/16/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/27/2006              *
		// ********************************************************
		// vbPorter upgrade warning: dtHoldDate As DateTime	OnWrite(DateTime, string)
		DateTime dtHoldDate;
		string strHoldJDesc;
		bool boolHoldChk;

		public void Init(ref DateTime dtDate, ref string strJDesc, ref bool boolJournalEntryDescription)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intError;
				intError = 0;
				// set the variables
				dtHoldDate = dtDate;
				intError = 1;
				strHoldJDesc = strJDesc;
				intError = 2;
				//Application.DoEvents();
				this.Show(FormShowEnum.Modal, App.MainForm);
				intError = 5;
				// pass the variables back
				dtDate = dtHoldDate;
				intError = 6;
				strJDesc = strHoldJDesc;
				intError = 7;
				boolJournalEntryDescription = boolHoldChk;
				intError = 8;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Prepayment Keydown Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmGetJournalDesc_Activated(object sender, System.EventArgs e)
		{
			txtDate.Text = Strings.Format(dtHoldDate, "MM/dd/yyyy");
			txtDesc.Text = strHoldJDesc;
		}

		private void frmGetJournalDesc_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyCode == Keys.Escape)
				{
					if (SetDefaultInfo())
					{
						Close();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Prepayment Keydown Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmGetJournalDesc_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyAscii == Keys.Return)
				{
					Support.SendKeys("{tab}", false);
				}
				// this will make sure that the characters are all uppercase
				if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
				{
					KeyAscii = KeyAscii - 32;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Prepayment Keypress Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
		}

		private bool SetDefaultInfo()
		{
			bool SetDefaultInfo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (Information.IsDate(txtDate.Text))
				{
					dtHoldDate = FCConvert.ToDateTime(txtDate.Text);
				}
				else
				{
					MessageBox.Show("Please enter a valid date.", "Enter Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SetDefaultInfo;
				}
				if (Strings.Trim(txtDesc.Text) != "")
				{
					strHoldJDesc = txtDesc.Text;
				}
				else
				{
					MessageBox.Show("Please enter a valid description.", "Enter Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SetDefaultInfo;
				}
				boolHoldChk = FCConvert.CBool(chkEntryDescription.CheckState == Wisej.Web.CheckState.Checked);
				SetDefaultInfo = true;
				return SetDefaultInfo;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SetDefaultInfo;
		}

		private void frmGetJournalDesc_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetJournalDesc.ScaleWidth	= 3885;
			//frmGetJournalDesc.ScaleHeight	= 2595;
			//frmGetJournalDesc.LinkTopic	= "Form1";
			//frmGetJournalDesc.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (SetDefaultInfo())
			{
				Close();
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, e);
		}
	}
}
