﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmMVR3Listing.
	/// </summary>
	public partial class frmMVR3Listing : BaseForm
	{
		public frmMVR3Listing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkPrint = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkPrint.AddControlArrayElement(chkPrint_4, 4);
			this.chkPrint.AddControlArrayElement(chkPrint_3, 3);
			this.chkPrint.AddControlArrayElement(chkPrint_2, 2);
			this.chkPrint.AddControlArrayElement(chkPrint_1, 1);
			this.chkPrint.AddControlArrayElement(chkPrint_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMVR3Listing InstancePtr
		{
			get
			{
				return (frmMVR3Listing)Sys.GetInstance(typeof(frmMVR3Listing));
			}
		}

		protected frmMVR3Listing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/10/2005              *
		// ********************************************************
		// vbPorter upgrade warning: StartNumber As DateTime	OnWrite(string, DateTime, short)
		public DateTime StartNumber;
		// vbPorter upgrade warning: EndNumber As DateTime	OnWrite(string, DateTime, short)
		public DateTime EndNumber;
		bool boolNoClick;
		clsDRWrapper rsTemp = new clsDRWrapper();
		bool boolFirstScreen;

		public void Init()
		{
			if (rsTemp.EndOfFile())
			{
				Close();
			}
			else
			{
				if (chkPrint[0].Visible == true)
				{
					chkPrint[0].Focus();
				}
			}
		}

		private void chkPrint_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			// when the user clicks a choice, make sure that all others are not checked
			int intCT;
			if (!boolNoClick)
			{
				boolNoClick = true;
				for (intCT = 0; intCT <= 4; intCT++)
				{
					if (intCT != Index)
					{
						chkPrint[FCConvert.ToInt16(intCT)].CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				boolNoClick = false;
			}
		}

		private void chkPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = chkPrint.GetIndex((FCCheckBox)sender);
			chkPrint_CheckedChanged(index, sender, e);
		}

		private void cmbMonth_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbMonth.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbMonth.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMVCOStart_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbMVCOStart.SelectedIndex >= 0)
			{
				// this will set the end to the same
				cmbMVCOEnd.SelectedIndex = cmbMVCOStart.SelectedIndex;
			}
		}

		private void cmbYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmdPreview_Click(object sender, System.EventArgs e)
		{
			// set the range for the arctive report
			if (chkPrint[0].CheckState == Wisej.Web.CheckState.Checked)
			{
				if (cmbYear.SelectedIndex != -1)
				{
					// specific year
					StartNumber = FCConvert.ToDateTime("01/01/" + FCConvert.ToString(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
					EndNumber = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
				}
				else
				{
					MessageBox.Show("Please choose a Year.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else if (chkPrint[1].CheckState == Wisej.Web.CheckState.Checked)
			{
				if (cmbYear.SelectedIndex != -1 && cmbMonth.SelectedIndex != -1)
				{
					// specific month
					// first day of the month
					StartNumber = FCConvert.ToDateTime(FCConvert.ToString(Conversion.Val(Strings.Left(cmbMonth.Items[cmbMonth.SelectedIndex].ToString(), 2))) + "/01/" + FCConvert.ToString(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
					// last day of the same month
					EndNumber = StartNumber;
					EndNumber = DateAndTime.DateAdd("M", 1, EndNumber);
					EndNumber = DateAndTime.DateAdd("D", -1, EndNumber);
				}
				else if (cmbMonth.Visible)
				{
					MessageBox.Show("Please choose a Month and a Year.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					if (cmbMonth.SelectedIndex == -1 && cmbMonth.Visible)
					{
						cmbMonth.Focus();
					}
					else if (cmbYear.Visible)
					{
						cmbYear.Focus();
					}
					return;
				}
			}
			else if (chkPrint[2].CheckState == Wisej.Web.CheckState.Checked)
			{
				if (ValidateDates())
				{
					StartNumber = DateAndTime.DateValue(txtStart.Text);
					EndNumber = DateAndTime.DateValue(txtEnd.Text);
				}
				else
				{
					StartNumber = DateTime.FromOADate(0);
					EndNumber = DateTime.FromOADate(0);
				}
			}
			else if (chkPrint[3].CheckState == Wisej.Web.CheckState.Checked)
			{
				// this is code to show the MV closeouts
				if (ValidateCloseOut())
				{
					StartNumber = MVCLoseOut_8(true, cmbMVCOStart.ItemData(cmbMVCOStart.SelectedIndex));
					// get the beginning mv closeout date
					EndNumber = MVCLoseOut_8(false, cmbMVCOEnd.ItemData(cmbMVCOEnd.SelectedIndex));
					// get the last mv closeout date
				}
				else
				{
					StartNumber = DateTime.FromOADate(0);
					EndNumber = DateTime.FromOADate(0);
				}
			}
			else
			{
				// all listings
				StartNumber = FCConvert.ToDateTime("01/01/" + FCConvert.ToString(Conversion.Val(Strings.Left(cmbYear.Items[0].ToString(), 4))));
				EndNumber = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(Conversion.Val(Strings.Left(cmbYear.Items[cmbYear.Items.Count - 1].ToString(), 4))));
			}
			if (cmbMonth.Visible || fraRange.Visible || fraMVCO.Visible)
			{
				// check to see if the combo boxes are shown yet
				if (StartNumber.ToOADate() != 0 && EndNumber.ToOADate() != 0)
				{
                    frmReportViewer.InstancePtr.Init(arMVR3Listing.InstancePtr);
                    //FC:FINAL:AM:#3493 - don't close the form because the params are lost
                    mnuProcessQuit_Click();
				}
			}
			else
			{
				boolFirstScreen = true;
			}
		}

		public void cmdPreview_Click()
		{
			cmdPreview_Click(cmdSave, new System.EventArgs());
		}

		private void frmMVR3Listing_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						if (fraOptions.Visible == true)
						{
							KeyCode = (Keys)0;
							Close();
						}
						else
						{
							MoveBack();
						}
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
		}

		private void frmMVR3Listing_Load(object sender, System.EventArgs e)
		{
			boolFirstScreen = true;
			modGlobalFunctions.SetTRIOColors(this);
			rsTemp.OpenRecordset("SELECT * FROM Archive ORDER BY ArchiveDate");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				FillCombos();
			}
			else
			{
				MessageBox.Show("No receipts of this type found.  Please enter a receipt before using this option.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void FillCombos()
		{
			// this sub will fill the combo boxes for the user to pick the dates
			// and the starting combo will be set the first code and the ending combo will be set to the last
			int intStartYear = 0;
			int intEndYear = 0;
			int I;
			// this will load the MV Closeout combos
			LoadMVCOCombos();
			// clear the combo boxes before filling them
			cmbMonth.Clear();
			cmbYear.Clear();
			// fill month combo
			cmbMonth.AddItem("01 - January");
			cmbMonth.AddItem("02 - February");
			cmbMonth.AddItem("03 - March");
			cmbMonth.AddItem("04 - April");
			cmbMonth.AddItem("05 - May");
			cmbMonth.AddItem("06 - June");
			cmbMonth.AddItem("07 - July");
			cmbMonth.AddItem("08 - August");
			cmbMonth.AddItem("09 - September");
			cmbMonth.AddItem("10 - October");
			cmbMonth.AddItem("11 - November");
			cmbMonth.AddItem("12 - December");
			// this will fill the year combo box
			rsTemp.OpenRecordset("SELECT * FROM Archive ORDER BY ArchiveDate");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				intStartYear = ((DateTime)rsTemp.Get_Fields_DateTime("ArchiveDate")).Year;
				rsTemp.MoveLast();
				intEndYear = ((DateTime)rsTemp.Get_Fields_DateTime("ArchiveDate")).Year;
				for (I = intStartYear; I <= intEndYear; I++)
				{
					cmbYear.AddItem(I.ToString());
				}
			}
			else
			{
				MessageBox.Show("No receipts of this type found.  Please enter a receipt before using this option.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			}
		}

		private bool ValidateDates()
		{
			bool ValidateDates = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				ValidateDates = true;
				// check to make sure the date entered is correct and valid
				if (Information.IsDate(txtStart.Text) == false)
				{
					MessageBox.Show("Please enter a valid start date.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStart.SelStart = 0;
					txtStart.SelLength = txtStart.Text.Length;
					ValidateDates = false;
					return ValidateDates;
				}
				// check to make sure the date entered is correct and valid
				if (Information.IsDate(txtEnd.Text) == false)
				{
					MessageBox.Show("Please enter a valid end date.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEnd.SelStart = 0;
					txtEnd.SelLength = txtEnd.Text.Length;
					ValidateDates = false;
					return ValidateDates;
				}
				// check to make sure that the dates are in the correct order
				if (DateAndTime.DateValue(txtStart.Text).ToOADate() > DateAndTime.DateValue(txtEnd.Text).ToOADate())
				{
					MessageBox.Show("Please enter a date that is earlier than the ending date.", "Date Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					ValidateDates = false;
					return ValidateDates;
				}
				return ValidateDates;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				ValidateDates = false;
				MessageBox.Show("Please enter a valid dates.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return ValidateDates;
		}

		private void mnuFileBack_Click(object sender, System.EventArgs e)
		{
			if (fraOptions.Visible != true)
			{
				MoveBack();
			}
		}

		private void mnuFileNext_Click(object sender, System.EventArgs e)
		{
			if (fraOptions.Visible && !chkPrint[4].Checked)
			{
				MoveNext();
			}
			else
			{
				cmdPreview_Click();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void MoveBack()
		{
			// this sub will go back to the entry screen
			boolNoClick = true;
			fraChoice.Visible = false;
			fraMVCO.Visible = false;
			chkPrint[0].CheckState = Wisej.Web.CheckState.Unchecked;
			chkPrint[1].CheckState = Wisej.Web.CheckState.Unchecked;
			chkPrint[2].CheckState = Wisej.Web.CheckState.Unchecked;
			chkPrint[3].CheckState = Wisej.Web.CheckState.Unchecked;
			fraOptions.Visible = true;
			cmdBack.Visible = false;
			mnuFileNext.Visible = true;
			boolFirstScreen = true;
			boolNoClick = false;
		}

		private void MoveNext()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int Index;
				int intCT;
				fraOptions.Visible = false;
				fraChoice.Visible = false;
				fraRange.Visible = false;
				fraDate.Visible = false;
				fraMVCO.Visible = false;
				Index = -1;
				for (intCT = 0; intCT <= 4; intCT++)
				{
					if (chkPrint[FCConvert.ToInt16(intCT)].CheckState == Wisej.Web.CheckState.Checked)
					{
						Index = intCT;
						break;
					}
				}
				switch (Index)
				{
					case 0:
						{
							// year
							fraChoice.Visible = true;
							fraDate.Visible = true;
							cmbYear.Enabled = true;
							cmbYear.BackColor = Color.White;
							cmbMonth.Enabled = false;
							cmbMonth.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							cmbYear.Focus();
							break;
						}
					case 1:
						{
							// month
							fraChoice.Visible = true;
							fraDate.Visible = true;
							cmbYear.Enabled = true;
							cmbYear.BackColor = Color.White;
							cmbMonth.Enabled = true;
							cmbMonth.BackColor = Color.White;
							cmbMonth.Focus();
							break;
						}
					case 2:
						{
							// date range
							fraChoice.Visible = true;
							fraRange.Visible = true;
							txtStart.Focus();
							break;
						}
					case 3:
						{
							// MV Closeout
							fraMVCO.Visible = true;
							if (cmbMVCOStart.Enabled)
							{
								cmbMVCOStart.Focus();
							}
							break;
						}
					case 4:
						{
							// all files
							cmdPreview_Click();
							// mnuProcessQuit_Click
							break;
						}
					default:
						{
							MessageBox.Show("Please select an option.", "No Selection Made", MessageBoxButtons.OK, MessageBoxIcon.Information);
							fraOptions.Visible = true;
							boolFirstScreen = true;
							return;
						}
				}
				//end switch
				if (Index >= 0)
				{
					cmdBack.Visible = true;
					boolFirstScreen = false;
					// mnuFileNext.Visible = False
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error in Move Next", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool ValidateCloseOut()
		{
			bool ValidateCloseOut = false;
			// this will check the MV closeout combos to make sure that the user selected at least one
			if (cmbMVCOStart.SelectedIndex >= 0)
			{
				ValidateCloseOut = true;
			}
			else
			{
				ValidateCloseOut = false;
				MessageBox.Show("Please select a closeout.", "No Close Out Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			return ValidateCloseOut;
		}

		private DateTime MVCLoseOut_8(bool boolStart, int lngKey)
		{
			return MVCLoseOut(ref boolStart, ref lngKey);
		}

		private DateTime MVCLoseOut(ref bool boolStart, ref int lngKey)
		{
			DateTime MVCLoseOut = System.DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will take the MV Closeout and return the date and time to use
				clsDRWrapper rsMV = new clsDRWrapper();
				rsMV.OpenRecordset("SELECT * FROM PeriodCloseOut ORDER BY ID desc", modExtraModules.strMVDatabase);
				rsMV.FindFirstRecord("ID", lngKey);
				if (!rsMV.NoMatch)
				{
					// found
					if (boolStart)
					{
						// needs to find the last one
						rsMV.MoveNext();
						if (!rsMV.EndOfFile())
						{
							// found
							// TODO Get_Fields: Field [IssueDate] not found!! (maybe it is an alias?)
							MVCLoseOut = (DateTime)rsMV.Get_Fields("IssueDate");
						}
						else
						{
							// that was the first closeout so return the time right now
							MVCLoseOut = DateTime.Now;
						}
					}
					else
					{
						// return date
						// TODO Get_Fields: Field [IssueDate] not found!! (maybe it is an alias?)
						MVCLoseOut = (DateTime)rsMV.Get_Fields("IssueDate");
					}
				}
				else
				{
				}
				return MVCLoseOut;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MVCLoseOut = DateTime.Now;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Close Out Date - " + FCConvert.ToString(lngKey), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MVCLoseOut;
		}

		private void LoadMVCOCombos()
		{
			// this routine will fill the two combos so the user can select the close out that they need
			clsDRWrapper rsMV = new clsDRWrapper();
			int lngCount = 0;
			cmbMVCOStart.Clear();
			cmbMVCOEnd.Clear();
			rsMV.OpenRecordset("SELECT * FROM PeriodCloseOut ORDER BY ID desc", modExtraModules.strMVDatabase);
			while (!(rsMV.EndOfFile() || lngCount > 20))
			{
				// this will add the date and time to the combo boxes
				// TODO Get_Fields: Field [IssueDate] not found!! (maybe it is an alias?)
				cmbMVCOStart.AddItem(Strings.Format(rsMV.Get_Fields("IssueDate"), "MM/dd/yyyy   h:mm:ss tt"));
				// TODO Get_Fields: Field [IssueDate] not found!! (maybe it is an alias?)
				cmbMVCOEnd.AddItem(Strings.Format(rsMV.Get_Fields("IssueDate"), "MM/dd/yyyy   h:mm:ss tt"));
				cmbMVCOStart.ItemData(cmbMVCOStart.NewIndex, FCConvert.ToInt32(rsMV.Get_Fields_Int32("ID")));
				cmbMVCOEnd.ItemData(cmbMVCOEnd.NewIndex, FCConvert.ToInt32(rsMV.Get_Fields_Int32("ID")));
				lngCount += 1;
				rsMV.MoveNext();
			}
		}

        private void cmdBack_Click(object sender, EventArgs e)
        {
            mnuFileBack_Click(cmdBack, EventArgs.Empty);
        }
    }
}
