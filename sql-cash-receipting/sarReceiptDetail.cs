﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarReceiptDetail.
	/// </summary>
	public partial class sarReceiptDetail : FCSectionReport
	{
		public static sarReceiptDetail InstancePtr
		{
			get
			{
				return (sarReceiptDetail)Sys.GetInstance(typeof(sarReceiptDetail));
			}
		}

		protected sarReceiptDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarReceiptDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/21/2006              *
		// ********************************************************
		float[] lngLeftCol = new float[5 + 1];
		float lngTotalWidth;
		int lngCurrentPayment;
		float[] lngTop = new float[6 + 1];
		int intRowsUsed;
		float lngFieldWid;
		string strNarrowInfo = "";
		clsDRWrapper rsBatch = new clsDRWrapper();
		bool boolOpened;
		int lngReceiptArrayIndex;
		float lngLineHeight;

		public sarReceiptDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (Conversion.Val(this.UserData) == 0)
			{
				eArgs.EOF = true;
				lngReceiptArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			}
			else
			{
				lngReceiptArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				this.UserData = 0;
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetArraySizes();
			SetupReceiptFormat();
			lngCurrentPayment = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			lngLineHeight = 270 / 1440F;
			lngTop[0] = 0;
			lngTop[1] = 270 / 1440F;
			lngTop[2] = 540 / 1440F;
			// MAL@20080108
			lngTop[3] = 810 / 1440F;
			lngTop[4] = 1080 / 1440F;
			lngTop[5] = 1350 / 1440F;
			lngTop[6] = 1620 / 1440F;
			if (!modGlobal.Statics.gboolNarrowReceipt)
			{
				SetReceiptFontSize();
			}
			if (Conversion.Val(this.UserData) == 0)
			{
				lngReceiptArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			}
			else
			{
				lngReceiptArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				this.UserData = 0;
				//Detail_Format();
			}

            Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblLocation");
            for (int i = 0; i < 15; i++)
            {
                Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(i));
            }
        }

		private void SetArraySizes()
		{
			// this sub will set the array elements to the left of each column and it will set
			// the total size of the report depending on the size of the receipt
			if (!modGlobal.Statics.gboolNarrowReceipt)
			{
				// wide format
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 1170 / 1440F;
				lngLeftCol[2] = 2880 / 1440F;
				lngLeftCol[3] = 3060 / 1440F;
				lngLeftCol[4] = 4950 / 1440F;
				// MAL@20080423
				// lngLeftCol(5) = 5130
				lngLeftCol[5] = 5030 / 1440F;
				lngTotalWidth = 7215 / 1440F;
				lngFieldWid = 2880 / 1440F;
				PageSettings.Margins.Top = 1440 / 1440F;
				PageSettings.Margins.Bottom = 1440 / 1440F;
				// If gboolRecieptThin Then
				// add another column
				// Me.Detail.ColumnCount = 2
				// End If
			}
			else
			{
				// narrow format
				//Document.Printer.RenderMode = 0; // 1
				//FC:FINAL:SBE - #3235 - adjust report width based on printer paper width
				//lngLeftCol[0] = 0;
				//lngLeftCol[1] = 100 / 1440F;
				//lngLeftCol[2] = 0;
				//lngLeftCol[3] = 200 / 1440F;
				//lngLeftCol[4] = 500 / 1440F;
				//lngLeftCol[5] = 300 / 1440F;
				//lngTotalWidth = 4200 / 1440F;
				//lngFieldWid = 1440 / 1440F;
				//PageSettings.Margins.Left = 360 / 1440F;
				//PageSettings.Margins.Right = 0;
				//PageSettings.Margins.Top = 0;
				//PageSettings.Margins.Bottom = 0;
                lngLeftCol[0] = 0;
                lngLeftCol[1] = 88.875f / 1440F;
                lngLeftCol[2] = 0;
                lngLeftCol[3] = 177.75f / 1440F;
                lngLeftCol[4] = 444.375f / 1440F;
                lngLeftCol[5] = 266.375f / 1440F;
                lngTotalWidth = 3555f / 1440F;
                lngFieldWid = 1440f / 1440F;
                PageSettings.Margins.Left = 319.95f / 1440F;
                PageSettings.Margins.Right = 0;
                PageSettings.Margins.Top = 0;
                PageSettings.Margins.Bottom = 0;
            }
		}

		private void SetupReceiptFormat()
		{
			// set the lefts
			// 0 column
			fldName.Left = lngLeftCol[0];
			fldModule.Left = lngLeftCol[0];
			lblComment.Left = lngLeftCol[0];
			// 1 column
			// 2 column
			// 3 column
			fldRef.Left = lngLeftCol[3];
			// 4 column
			// 5 column
			fldAmt.Left = lngLeftCol[5];
			// set the widths
			this.PrintWidth = lngTotalWidth + 1200 / 1440F;
			if (!modGlobal.Statics.gboolNarrowReceipt)
			{
				// wide
				fldAmt.Width = 1440 / 1440F;
				fldRef.Width = 1890 / 1440F;
				// MAL@20080602
				fldName.Width = lngTotalWidth - fldName.Left;
			}
			else
			{
				// narrow
				fldAmt.Width = 1000 / 1440F;
				fldRef.Width = 1440 / 1440F;
			}
			// MAL@20080602: Moved to receipt size check
			// Tracker Reference: 13603
			// fldRef.Width = 1440
			fldModule.Width = lngFieldWid;
			fldNarrow.Width = lngTotalWidth;
			// MAL@20080826: Adjust comment label width
			// Tracker Reference: 13725
			lblComment.Width = lngTotalWidth;
			// MAL@20080423: Add check for width for Control fields
			// Tracker Reference: 13402
			fldControlDetail1.Width = lngTotalWidth - fldControlDetail1.Left;
			fldControlDetail2.Width = lngTotalWidth - fldControlDetail2.Left;
			fldControlDetail3.Width = lngTotalWidth - fldControlDetail3.Left;
		}

        private void Detail_Format(object sender, EventArgs e)
        {
            bool boolBatchDone;
            bool boolDetail = false;
            int intCT;
            string strResString = "";
            bool boolREType = false;
            int intCommentLine = 0;
            if (lngReceiptArrayIndex != 0)
            {
                intRowsUsed = 6;
                modGlobal.Statics.grsType.FindFirstRecord2("Typecode, TownKey", modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type.ToString() + "," + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ResCode.ToString(), ",");
                if (modGlobal.Statics.grsType.NoMatch)
                {
                    modGlobal.Statics.grsType.FindFirstRecord2("Typecode,TownKey", modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type.ToString() + ",0", ",");
                    if (modGlobal.Statics.grsType.NoMatch)
                    {
                        boolDetail = false;
                    }
                    else if (modGlobal.Statics.grsType.Get_Fields_Boolean("ShowFeeDetailOnReceipt"))
                    {
                        boolDetail = true;
                    }
                    else
                    {
                        boolDetail = false;
                    }
                }
                else if (modGlobal.Statics.grsType.Get_Fields_Boolean("ShowFeeDetailOnReceipt"))
                {
                    boolDetail = true;
                }
                else
                {
                    boolDetail = false;
                }
                if (Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Comment) == "")
                {
                    // check to see if there is a comment
                    lblComment.Top = lngTop[0];
                    // if not then move the comment label to the top of the detail section
                    lblComment.Visible = false;
                    // and make it visible = false
                    intRowsUsed -= 1;
                    // row count is decreased by one line
                }
                else
                {
                    // else
                    lblComment.Visible = true;
                    // show comment line
                    lblComment.Top = lngTop[2];
                    // set the top to the third line
                    intCommentLine = 2;
                    lblComment.Text = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + "   " + modGlobalFunctions.PadStringWithSpaces(Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Comment), FCConvert.ToInt16(37 - modGlobal.Statics.gintReceiptOffSet), false);
                    // fill comment line
                }
                switch (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type)
                {
                    case 90:
                    case 91:
                    case 92:
                    case 890:
                    case 891:
                    {
                        boolREType = true;
                        break;
                    }
                    default:
                    {
                        boolREType = false;
                        break;
                    }
                }
                //end switch
                if (boolREType)
                {
                    // RE or PP type
                    fldName.Visible = true;
                    fldName.Top = lngTop[0];
                    fldModule.Top = lngTop[1];
                    fldRef.Top = lngTop[1];
                    fldAmt.Top = lngTop[1];
                    fldName.Text = " " + modGlobalFunctions.PadStringWithSpaces(Strings.Left(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Name, 37 - modGlobal.Statics.gintReceiptOffSet), FCConvert.ToInt16(37 - modGlobal.Statics.gintReceiptOffSet), false);
                    // Call #88730
                    fldNarrow.Top = lngTop[1];
                    intCommentLine = 1;
                }
                else if (Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Name) == Strings.Trim(Strings.Right(arReceipt.InstancePtr.lblPaidBy.Text, arReceipt.InstancePtr.lblPaidBy.Text.Length - 8)))
                {
                    // check to see if the name matches the paid by name
                    intRowsUsed -= 1;
                    // if it is the same then decrease the number of rows needed
                    fldModule.Top = lngTop[0];
                    // move these labels to the top
                    fldRef.Top = lngTop[0];
                    fldNarrow.Top = lngTop[0];
                    fldAmt.Top = lngTop[0];
                    fldName.Text = "";
                    fldName.Visible = false;
                    if (Strings.Trim(lblComment.Text) != "")
                    {
                        // check to see if there is a comment
                        lblComment.Top = lngTop[1];
                        // if not then move the comment label to the 2nd line of the detail section
                        intCommentLine = 1;
                    }
                }
                else
                {
                    fldName.Visible = true;
                    fldName.Top = lngTop[0];
                    fldModule.Top = lngTop[1];
                    fldRef.Top = lngTop[1];
                    fldAmt.Top = lngTop[1];
                    fldName.Text = "  **" + modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Name, FCConvert.ToInt16(37 - modGlobal.Statics.gintReceiptOffSet), false);
                    fldNarrow.Top = lngTop[1];
                    // kgk 03-31-11 trocr-280  comment and control1 on same line    intCommentLine = 1
                }
                // use the narrow receipt format or use the multiple field format
                if (!modGlobal.Statics.gboolNarrowReceipt)
                {
                    // wide
                    fldModule.Visible = true;
                    fldRef.Visible = true;
                    fldNarrow.Visible = false;
                    if (boolDetail)
                    {
                        fldAmt.Visible = false;
                    }
                    else
                    {
                        fldAmt.Visible = true;
                    }
                }
                else
                {
                    // narrow
                    fldModule.Visible = false;
                    fldRef.Visible = false;
                    fldAmt.Visible = false;
                    fldNarrow.Visible = true;
                    if (boolDetail)
                    {
                        // if I am showing the detail, then do not put the total amount at the end
                        strNarrowInfo = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].TypeDescription, FCConvert.ToInt16(17 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces(modGlobalFunctions.PadStringWithSpaces(strResString + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Reference, 10, false), 12);
                    }
                    else
                    {
                        strNarrowInfo = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].TypeDescription, FCConvert.ToInt16(17 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces(modGlobalFunctions.PadStringWithSpaces(strResString + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Reference, FCConvert.ToInt16(9 - modGlobal.Statics.gintReceiptOffSet), false), 10) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Total, "#,##0.00"), 12);
                    }
                    fldNarrow.Text = strNarrowInfo;
                }
                // MAL@20080107; Tracker Reference: 11158
                if (!modGlobalConstants.Statics.gblnShowControlFields)
                {
                    fldControlDetail1.Top = lngTop[0];
                    // Set the subreport to the top of the detail
                    fldControlDetail1.Visible = false;
                    // Set visible = false
                    fldControlDetail2.Top = lngTop[0];
                    // Set the subreport to the top of the detail
                    fldControlDetail2.Visible = false;
                    // Set visible = false
                    fldControlDetail3.Top = lngTop[0];
                    // Set the subreport to the top of the detail
                    fldControlDetail3.Visible = false;
                    // Set visible = false
                    intRowsUsed -= 3;
                }
                else
                {
                    FillControlFields(intCommentLine);
                }
                // check the rescode
                if (modGlobal.Statics.gboolMultipleTowns)
                {
                    strResString = modRegionalTown.GetTownAbbreviation(ref modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ResCode) + "-";
                }
                fldModule.Text = modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].TypeDescription;
                fldRef.Text = strResString + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Reference;
                fldAmt.Text = Strings.Format(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Total, "#,##0.00");
                if (boolDetail)
                {
                    // show the breakdown of the fees
                    // MAL@20080812: Changed to not cut off the fee description
                    // Tracker Reference: 13725
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type >= 90 && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type < 96 && Strings.UCase(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2) == "D" || Strings.UCase(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2) == "A")
                    {
                        if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2 == "D")
                        {
                            AddFeeRow_74("DISCOUNT", modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1, intRowsUsed * lngLineHeight, 1);
                            intRowsUsed += 1;
                        }
                        else if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2 == "A")
                        {
                            AddFeeRow_74("ABATEMENT", modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1, intRowsUsed * lngLineHeight, 1);
                            intRowsUsed += 1;
                        }
                    }
                    else
                    {
                        if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD1) != "")
                        {
                            // AddFeeRow Left(.FeeD1, 10), .Fee1, intRowsUsed * lngLineHeight, 1
                            AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD1, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee1, intRowsUsed * lngLineHeight, 1);
                            intRowsUsed += 1;
                        }
                    }
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee2 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD2) != "")
                    {
                        // AddFeeRow Left(.FeeD2, 10), .Fee2, intRowsUsed * lngLineHeight, 2
                        AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD2, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee2, intRowsUsed * lngLineHeight, 2);
                        intRowsUsed += 1;
                    }
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee3 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD3) != "")
                    {
                        // AddFeeRow Left(.FeeD3, 10), .Fee3, intRowsUsed * lngLineHeight, 3
                        AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD3, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee3, intRowsUsed * lngLineHeight, 3);
                        intRowsUsed += 1;
                    }
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee4 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD4) != "")
                    {
                        // AddFeeRow Left(.FeeD4, 10), .Fee4, intRowsUsed * lngLineHeight, 4
                        AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD4, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee4, intRowsUsed * lngLineHeight, 4);
                        intRowsUsed += 1;
                    }
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee5 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD5) != "")
                    {
                        // AddFeeRow Left(.FeeD5, 10), .Fee5, intRowsUsed * lngLineHeight, 5
                        AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD5, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee5, intRowsUsed * lngLineHeight, 5);
                        intRowsUsed += 1;
                    }
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee6 != 0 && Strings.Trim(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD6) != "")
                    {
                        // AddFeeRow Left(.FeeD6, 10), .Fee6, intRowsUsed * lngLineHeight, 6
                        AddFeeRow_72(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].FeeD6, modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Fee6, intRowsUsed * lngLineHeight, 6);
                        intRowsUsed += 1;
                    }
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee != 0)
                    {
                        // AddFeeRow Left(.FeeD6, 10), .Fee6, intRowsUsed * lngLineHeight, 6
                        AddFeeRow_80("CNV FEE", FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee), intRowsUsed * lngLineHeight, 7);
                        intRowsUsed += 1;
                    }
                }
                else
                {
                    if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee != 0)
                    {
                        // AddFeeRow Left(.FeeD6, 10), .Fee6, intRowsUsed * lngLineHeight, 6
                        AddFeeRow_242("CNV FEE", FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ConvenienceFee), intRowsUsed * lngLineHeight, 1, true);
                        intRowsUsed += 1;
                    }
                }
                // this will create a location line for RE payments
                if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type == 90 || modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type == 91)
                {
                    AddLocationRow_6(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3, intRowsUsed * lngLineHeight);
                    intRowsUsed += 1;
                }
                Detail.Height = intRowsUsed * lngLineHeight;
            }
        }

		private void AddFeeRow_72(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow_74(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow_80(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow_242(string strFeeDesc, double dblFee, float lngTop, short intNum, bool blnNoIndent = false)
		{
			AddFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, blnNoIndent);
		}

		private void AddFeeRow(ref string strFeeDesc, ref double dblFee, ref float lngTop, ref short intNum, bool blnNoIndent = false)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			string strTemp;
			string strCaption;
			int intSpaces = 0;
            // add a label for a description
            obNew = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(intNum));
			//obNew.Name = "lblFeeD" + FCConvert.ToString(intNum);
			obNew.Top = lngTop;
			if (!blnNoIndent)
			{
				obNew.Left = fldModule.Left + 200 / 1440F;
			}
			else
			{
				obNew.Left = fldModule.Left;
			}
			// MAL@20080527 ; Tracker Reference: 13603
			// obNew.Width = Me.PrintWidth
			obNew.Width = lngTotalWidth - obNew.Left;
			obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			//strTemp = obNew.Font;
			obNew.Font = fldNarrow.Font;
			// this sets the font to the same as the field that is already created
			obNew.Font = new Font(obNew.Font.Name, fldNarrow.Font.Size);
			// MAL@20080514 ; Tracker Reference: 13603
			obNew.MultiLine = false;
			obNew.WordWrap = false;
			if (modGlobal.Statics.gboolNarrowReceipt)
			{
				intSpaces = 17;
				// obNew.Caption = String(gintReceiptOffSet, " ") & PadStringWithSpaces(strFeeDesc, 20 - gintReceiptOffSet, False) & PadStringWithSpaces(Format(dblFee, "#,##0.00"), 17 - gintReceiptOffSet)
			}
			else
			{
				if (obNew.Font.Size >= 10)
				{
					// MAL@20080527: Adjust spacing if font is greater than 9 ; Tracker Reference: 13603
					// intSpaces = 23
					intSpaces = 13;
				}
				else
				{
					// intSpaces = 45
					intSpaces = 35;
					// obNew.Caption = String(gintReceiptOffSet, " ") & PadStringWithSpaces(strFeeDesc, 20 - gintReceiptOffSet, False) & PadStringWithSpaces(Format(dblFee, "#,##0.00"), 45)
				}
			}
			if (blnNoIndent)
			{
				intSpaces += 2;
			}
			strCaption = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces(strFeeDesc, (15 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblFee, "#,##0.00"), intSpaces);
			obNew.Text = strCaption;
		}

		private void AddLocationRow_6(string strLocation, float lngTop)
		{
			AddLocationRow(ref strLocation, ref lngTop);
		}

		private void AddLocationRow(ref string strLocation, ref float lngTop)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			string strTemp;
            // add a label for a description
            obNew = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblLocation");
			//obNew.Name = "lblLocation";
			obNew.Top = lngTop;
			obNew.Left = fldModule.Left + 200 / 1440F;
			obNew.Width = this.PrintWidth;
			obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			//strTemp = obNew.Font;
			obNew.Font = fldNarrow.Font;
			// this sets the font to the same as the field that is already created
			// MAL@20080514: Add to make sure that font size matches
			// Tracker Reference: 13603
			obNew.Font = new Font(obNew.Font.Name, fldNarrow.Font.Size);
			if (modGlobal.Statics.gboolNarrowReceipt)
			{
				obNew.Text = Strings.Left(strLocation, 35);
			}
			else
			{
				obNew.Text = strLocation;
			}
		}

		private void SetReceiptFontSize()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngFSize;
				string strTemp = "";
				// this function will get the font size set for wide receipts and adjust the fields accordingly
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideFontSize", ref strTemp);
				lngFSize = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				if (lngFSize <= 0)
				{
					return;
				}
				fldModule.Font = new Font(fldModule.Font.Name, lngFSize);
				fldAmt.Font = new Font(fldAmt.Font.Name, lngFSize);
				fldName.Font = new Font(fldName.Font.Name, lngFSize);
				fldRef.Font = new Font(fldRef.Font.Name, lngFSize);
				lblComment.Font = new Font(lblComment.Font.Name, lngFSize);
				fldNarrow.Font = new Font(fldNarrow.Font.Name, lngFSize);
				fldControlDetail1.Font = new Font(fldControlDetail1.Font.Name, lngFSize);
				fldControlDetail2.Font = new Font(fldControlDetail2.Font.Name, lngFSize);
				fldControlDetail3.Font = new Font(fldControlDetail3.Font.Name, lngFSize);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// do nothing
			}
		}

		private void FillControlFields(int intCommentLine)
		{
			clsDRWrapper rsType = new clsDRWrapper();
			bool blnCont1 = false;
			bool blnCont2 = false;
			bool blnCont3 = false;
			int intLine;
			intLine = intCommentLine + 1;
			// MAL@20080611: Add support for AR Types
			// Tracker Reference: 14057
			if (modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type == 97)
			{
				rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].ARBillType), modExtraModules.strARDatabase);
			}
			else
			{
				rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Type, modExtraModules.strCRDatabase);
			}
			if (rsType.RecordCount() > 0)
			{
				// Set boolean values for control values
				if (FCConvert.ToString(rsType.Get_Fields_String("Control1")) != "" && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1 != "")
				{
					blnCont1 = true;
				}
				else
				{
					blnCont1 = false;
				}
				if (FCConvert.ToString(rsType.Get_Fields_String("Control2")) != "" && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2 != "")
				{
					blnCont2 = true;
				}
				else
				{
					blnCont2 = false;
				}
				if (FCConvert.ToString(rsType.Get_Fields_String("Control3")) != "" && modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3 != "")
				{
					blnCont3 = true;
				}
				else
				{
					blnCont3 = false;
				}
				if (blnCont1)
				{
					if (blnCont2)
					{
						if (blnCont3)
						{
							// All Three Have Data
							// Show Fields
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = true;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail3.Top = lngTop[intLine + 2];
						}
						else
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
					else
					{
						if (blnCont3)
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
						else
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control1") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control1;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
				}
				else
				{
					if (blnCont2)
					{
						if (blnCont3)
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = true;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail2.Top = lngTop[intLine + 1];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
						else
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control2") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control2;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
					else
					{
						if (blnCont3)
						{
							fldControlDetail1.Visible = true;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = rsType.Get_Fields_String("Control3") + ":" + modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Control3;
							fldControlDetail1.Top = lngTop[intLine];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
						else
						{
							fldControlDetail1.Visible = false;
							fldControlDetail2.Visible = false;
							fldControlDetail3.Visible = false;
							fldControlDetail1.Text = "";
							fldControlDetail1.Top = lngTop[0];
							fldControlDetail2.Text = "";
							fldControlDetail2.Top = lngTop[0];
							fldControlDetail3.Text = "";
							fldControlDetail3.Top = lngTop[0];
						}
					}
				}
			}
			else
			{
				// Do Nothing - No Data
			}
		}

		private void sarReceiptDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarReceiptDetail.Caption	= "Receipt Detail";
			//sarReceiptDetail.Icon	= "sarReceiptDetail.dsx":0000";
			//sarReceiptDetail.Left	= 0;
			//sarReceiptDetail.Top	= 0;
			//sarReceiptDetail.Width	= 11880;
			//sarReceiptDetail.Height	= 8445;
			//sarReceiptDetail.StartUpPosition	= 3;
			//sarReceiptDetail.SectionData	= "sarReceiptDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
