﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheckCR.
	/// </summary>
	public partial class rptPaymentCrossCheckCR : BaseSectionReport
	{
		public static rptPaymentCrossCheckCR InstancePtr
		{
			get
			{
				return (rptPaymentCrossCheckCR)Sys.GetInstance(typeof(rptPaymentCrossCheckCR));
			}
		}

		protected rptPaymentCrossCheckCR _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPaymentCrossCheckCR	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/18/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		double dblTotalAmt;
		int[] arrMissingPayments = null;
		bool boolDone;

		public rptPaymentCrossCheckCR()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "CR Payment Cross Check - CL";
            this.ReportEnd += RptPaymentCrossCheckCR_ReportEnd;
		}

        private void RptPaymentCrossCheckCR_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			CreateArrayElements();
			if (lngCount == 0)
			{
				rptPaymentCrossCheck.InstancePtr.PageBreak1.Enabled = false;
				MessageBox.Show("No CR (CL) records to report.", "No Missing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
			}
			else
			{
				rptPaymentCrossCheck.InstancePtr.PageBreak1.Enabled = true;
				lngCount = 0;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// MAL@20071012
			rptPaymentCrossCheck.InstancePtr.lblHeader2.Text = "Receipts appearing on this report do not have corresponding entries in the Cash Receipting database";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			fldReceiptNumber.Text = "";
			fldDate.Text = "";
			fldName.Text = "";
			fldTeller.Text = "";
			fldPrin.Text = "";
			fldCurInt.Text = "";
			fldCost.Text = "";
			fldPLI.Text = "";
			fldType.Text = "";
			if (lngCount <= Information.UBound(arrMissingPayments, 1))
			{
				rsData.FindFirstRecord("ID", arrMissingPayments[lngCount]);
				if (!rsData.NoMatch)
				{
					if (FCConvert.ToString(rsData.Get_Fields_String("BillCode")) == "R")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						fldPrin.Text = Strings.Format(rsData.Get_Fields("Principal"), "#,##0.00");
						fldCurInt.Text = Strings.Format(rsData.Get_Fields_Decimal("CurrentInterest"), "#,##0.00");
						fldCost.Text = Strings.Format(rsData.Get_Fields_Decimal("LienCost"), "#,##0.00");
						fldPLI.Text = "0.00";
						fldType.Text = "R";
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						fldPrin.Text = Strings.Format(rsData.Get_Fields("Principal"), "#,##0.00");
						fldCurInt.Text = Strings.Format(rsData.Get_Fields_Decimal("CurrentInterest"), "#,##0.00");
						fldPLI.Text = Strings.Format(rsData.Get_Fields_Decimal("PreLienInterest"), "#,##0.00");
						fldCost.Text = Strings.Format(rsData.Get_Fields_Decimal("LienCost"), "#,##0.00");
						fldType.Text = "L";
					}
					fldReceiptNumber.Text = GetActualReceiptNumber_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber")));
					fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy");
					fldName.Text = rsData.Get_Fields_String("PaidBy");
					fldTeller.Text = rsData.Get_Fields_String("Teller");
				}
				else
				{
					ClearBoxes();
				}
				lngCount += 1;
			}
			else
			{
				boolDone = true;
			}
		}

		private void CreateArrayElements()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCL = new clsDRWrapper();
				string strSQL;
				int lngFM = 0;
				// vbPorter upgrade warning: lngYR As int	OnWrite(int, double)
				int lngYR = 0;
				// vbPorter upgrade warning: strCCDate As DateTime	OnWrite(string)
				DateTime strCCDate = DateTime.FromOADate(0);
				int lngType = 0;
				// vbPorter upgrade warning: strLastEOY As string	OnWrite(DateTime)	OnRead(DateTime)
				string strLastEOY;
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsCL.OpenRecordset("SELECT FiscalStart FROM Budgetary", modExtraModules.strBDDatabase);
					if (!rsCL.EndOfFile())
					{
						lngFM = FCConvert.ToInt32(rsCL.Get_Fields_String("FiscalStart"));
					}
					else
					{
						lngFM = 1;
					}
				}
				else
				{
					lngFM = 1;
				}
				if (DateTime.Today.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(DateTime.Today.Year)).ToOADate() && lngFM <= DateTime.Today.Month)
				{
					lngYR = DateTime.Today.Year;
				}
				else
				{
					lngYR = DateTime.Today.Year - 1;
				}
				strLastEOY = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(lngFM) + "/01/" + FCConvert.ToString(lngYR)));
				rsCL.OpenRecordset("SELECT * FROM CashRec");
				if (!rsCL.EndOfFile())
				{
					lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCL.Get_Fields_Int32("CrossCheckDefault"))));
				}
				switch (lngType)
				{
					case 0:
						{
							// last EOY
							strCCDate = FCConvert.ToDateTime(strLastEOY);
							break;
						}
					case 1:
						{
							// last cross check report
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("LastCrossCheck"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("LastCrossCheck"), "MM/dd/yyyy"));
							}
							break;
						}
					case 2:
						{
							// specific date
							if (DateAndTime.DateDiff("D", (DateTime)rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), DateAndTime.DateValue(strLastEOY)) > 0)
							{
								strCCDate = FCConvert.ToDateTime(strLastEOY);
							}
							else
							{
								strCCDate = FCConvert.ToDateTime(Strings.Format(rsCL.Get_Fields_DateTime("CrossCheckDefaultDate"), "MM/dd/yyyy"));
							}
							break;
						}
				}
				//end switch
				strSQL = "SELECT * FROM PaymentRec WHERE (BillCode = 'R' OR BillCode = 'L') AND (DOSReceiptNumber = 0 OR DOSReceiptNumber IS NULL) AND ReceiptNumber > 0 AND ActualSystemDate > '" + FCConvert.ToString(strCCDate) + "' ORDER BY ReceiptNumber";
				lngCount = 0;
				rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				while (!rsData.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					lngYR = FCConvert.ToInt32(rsData.Get_Fields("Year"));
					if (lngYR > 10000)
					{
						lngYR /= 10;
					}
					if (lngYR > 0)
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsData.Get_Fields("Code")) == "I" && (FCConvert.ToString(rsData.Get_Fields_String("Reference")) == "CHGINT" || Strings.UCase(FCConvert.ToString(rsData.Get_Fields_String("Reference"))) == "INTEREST") || FCConvert.ToInt32(rsData.Get_Fields_Boolean("Reversal")) == -1)
						{
							// Skip Record
						}
						else
						{
							if (FCConvert.ToString(rsData.Get_Fields_String("BillCode")) == "R")
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								rsCL.OpenRecordset("SELECT * FROM Archive WHERE AccountNumber = " + rsData.Get_Fields("Account") + " AND (ReceiptType = 90 OR ReceiptType = 890) AND Right(RTrim(Ref), 4) = '" + FCConvert.ToString(lngYR) + "' AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Control2 = '" + rsData.Get_Fields("Code") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND (Amount1 + Amount3 + Amount6) = " + rsData.Get_Fields("Principal") + " AND Amount2 = " + rsData.Get_Fields_Decimal("CurrentInterest") + " AND Amount4 = " + rsData.Get_Fields_Decimal("LienCost"), modExtraModules.strCRDatabase);
								// 
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								rsCL.OpenRecordset("SELECT * FROM Archive WHERE AccountNumber = " + rsData.Get_Fields("Account") + " AND (ReceiptType = 91 OR ReceiptType = 891) AND Right(RTrim(Ref), 4) = '" + FCConvert.ToString(lngYR) + "' AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Control2 = '" + rsData.Get_Fields("Code") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND (Amount1 + Amount6) = " + rsData.Get_Fields("Principal") + " AND Amount2 = " + rsData.Get_Fields_Decimal("CurrentInterest") + " AND Amount3 = " + rsData.Get_Fields_Decimal("PreLienInterest") + " AND Amount4 = " + rsData.Get_Fields_Decimal("LienCost"), modExtraModules.strCRDatabase);
							}
							if (rsCL.EndOfFile())
							{
								Array.Resize(ref arrMissingPayments, lngCount + 1);
								arrMissingPayments[lngCount] = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
								lngCount += 1;
							}
						}
					}
					rsData.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				rsCL.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Array Elements", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearBoxes()
		{
			fldCost.Text = "";
			fldDate.Text = "";
			fldCurInt.Text = "";
			fldName.Text = "";
			fldPLI.Text = "";
			fldPrin.Text = "";
			fldReceiptNumber.Text = "";
			fldTeller.Text = "";
			fldType.Text = "";
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_2(int lngRN, DateTime? dtDate = null)
		{
			return GetActualReceiptNumber(ref lngRN, dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, DateTime? dtDateTemp = null)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number ID in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				clsDRWrapper rsRN = new clsDRWrapper();
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN), modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
							if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				rsRN.Reset();
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetActualReceiptNumber;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (lngCount != 1)
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " CR transactions (CL).";
			}
			else
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " CR transaction (CL).";
			}
		}

		private void rptPaymentCrossCheckCR_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPaymentCrossCheckCR.Caption	= "CR Payment Cross Check - CL";
			//rptPaymentCrossCheckCR.Icon	= "rptPaymentCrossCheckCR.dsx":0000";
			//rptPaymentCrossCheckCR.Left	= 0;
			//rptPaymentCrossCheckCR.Top	= 0;
			//rptPaymentCrossCheckCR.Width	= 11880;
			//rptPaymentCrossCheckCR.Height	= 8595;
			//rptPaymentCrossCheckCR.StartUpPosition	= 3;
			//rptPaymentCrossCheckCR.SectionData	= "rptPaymentCrossCheckCR.dsx":058A;
			//End Unmaped Properties
		}
	}
}
