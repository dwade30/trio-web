﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using GrapeCity.ActiveReports;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmReprint.
	/// </summary>
	public partial class frmReprint : BaseForm
	{
		public frmReprint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			// FC:FINAL:VGE - #347 Extending grid and parent panel width
			vsPaymentInfo.Width = FCConvert.ToInt32(vsPaymentInfo.Width * 1.5);
			fraPaymentInfo.Width = FCConvert.ToInt32(fraPaymentInfo.Width * 1.5);
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReprint InstancePtr
		{
			get
			{
				return (frmReprint)Sys.GetInstance(typeof(frmReprint));
			}
		}

		protected frmReprint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/17/2005              *
		// ********************************************************
		public int lngReceiptNumber;
		public int intType;
		// vbPorter upgrade warning: strPassTag As string	OnWriteFCConvert.ToInt32(
		public string strPassTag = "";
		public int intRAIndex;
		public string strTellerID = "";
		bool boolCurrentYear;
		string strFrom = "";
		int lngReceiptKey;
		bool boolLoad;
		bool boolLoaded;
		public DateTime dtReceiptDate;
		public int lngMultiTown;

		private void cmbSplit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						cmdSplitPrint_Click();
						break;
					}
			}
			//end switch
		}

		private void cmdPrintReceipt_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strRecNum;
				strRecNum = txtReceipt.Text;
				if (strRecNum != "")
				{
					if (Conversion.Val(strRecNum) > 0)
					{
						lngReceiptNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(strRecNum)));
						ShowReceipt();
					}
					else
					{
						MessageBox.Show("Please enter a numeric value as the receipt number.", "Invalid Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					MessageBox.Show("Please enter a receipt number then press Show Receipt or F12.", "Missing Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Receipt Value", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdPrintReceipt_Click()
		{
			cmdPrintReceipt_Click(cmdPrintReceipt, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdSplitExit_Click(object sender, System.EventArgs e)
		{
			fraSplit.Visible = false;
			fraCashOut.Visible = true;
			fraSummary.Visible = true;
		}

		private void cmdSplitPrint_Click(object sender, System.EventArgs e)
		{
			if (cmbSplit.SelectedIndex != -1)
			{
				if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
				{
					return;
				}

				PrintSplit_2(FCConvert.ToInt32(cmbSplit.Items[cmbSplit.SelectedIndex].ToString()));
			}
			else
			{
				MessageBox.Show("Please select a split number.", "Choose Split", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdSplitPrint_Click()
		{
			cmdSplitPrint_Click(cmdSplitPrint, new System.EventArgs());
		}

		private void frmReprint_Activated(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (boolLoad && !boolLoaded)
			{
				cmdPrintReceipt.Tag = 0;
				cmdPrintReceipt.Text = "Show Receipt";
				// FC: FINAL: RPU: #i514: Set the position of the form directly to center of container
				//fraReceiptNumber.Top = FCConvert.ToInt32((this.Height - fraReceiptNumber.Height) / 3.0);
				//fraReceiptNumber.Left = FCConvert.ToInt32((this.Width - fraReceiptNumber.Width) / 2.0);
                //FC:FINAL:BSE:#4578 - should not be centered
				//fraReceiptNumber.CenterToContainer(this.ClientArea);
				FormatGrid();
				// this gets the last receipt printed on this computer from the registry
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "LastReceiptGenerated", ref strTemp);
				switch (intType)
				{
					case 0:
						{
							// Select the Receipt Number to show
							if (strTemp != "")
							{
								txtReceipt.Text = strTemp;
								// Show the number in the textbox
								txtReceipt.SelectionStart = 0;
								txtReceipt.SelectionLength = txtReceipt.Text.Length;
							}
							fraReceiptNumber.Visible = true;
							this.Text = "Redisplay Any Receipt";
							break;
						}
					case 1:
						{
							// Last Receipt to Print
							if (strTemp != "")
							{
								lngReceiptNumber = FCConvert.ToInt32(FCConvert.ToDouble(strTemp));
								ShowReceipt();
							}
							this.Text = "Redisplay Last Receipt";
							break;
						}
					case 2:
						{
							// Last Receipt to Show
							if (strTemp != "")
							{
								lngReceiptNumber = FCConvert.ToInt32(FCConvert.ToDouble(strTemp));
								ShowReceipt();
							}
							this.Text = "Redisplay Last Receipt";
							break;
						}
					default:
						{
							fraReceiptNumber.Visible = true;
							this.Text = "Redisplay Receipt";
							break;
						}
				}
				//end switch
				boolLoaded = true;
				//Application.DoEvents();
				// Me.SetFocus
			}
			else
			{
				if (!boolLoaded)
				{
					Close();
				}
			}
		}

		private void frmReprint_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						if (fraPaymentInfo.Visible)
						{
							fraPaymentInfo.Visible = false;
						}
						else
						{
							KeyCode = (Keys)0;
							Close();
						}
						break;
					}
			}
			//end switch
		}

		private void frmReprint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReprint.FillStyle	= 0;
			//frmReprint.ScaleWidth	= 9045;
			//frmReprint.ScaleHeight	= 7050;
			//frmReprint.LinkTopic	= "Form2";
			//frmReprint.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				boolLoad = true;
				foreach (Form frm in FCGlobal.Statics.Forms)
				{
					if (frm.Name == "frmReceiptInput")
					{
						if (frm.Visible == true)
						{
							MessageBox.Show("You cannot reprint while Receipt Input is open.", "Multiple Window Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							boolLoad = false;
						}
						break;
					}
				}
				if (boolLoad)
				{
					// this will size the form to the MDIParent
					modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
					modGlobalFunctions.SetTRIOColors(this);
					SetCustomFormColors();
					FCUtils.EraseSafe(modUseCR.Statics.ReceiptArray);
					modUseCR.Statics.PaymentBreakdownInfoArray = new modUseCR.PaymentBreakdownInfo[0 + 1];
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					modUseCR.Statics.PaymentBreakdownInfoArray[0] = new modUseCR.PaymentBreakdownInfo(0);
					modUseCR.Statics.intPaymentBreakdownInfoCounter = 0;
					if (modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
					{
						txtConvenienceFee.Enabled = false;
						lblConvenienceFee.Enabled = false;
					}
				}
				cmbArchive.SelectedIndex = 0;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmReprint_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				// KeyAscii = 0
				// Unload Me
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			MDIParent.InstancePtr.boolLockReceiptForms = false;
		}

		private void frmReprint_Resize(object sender, System.EventArgs e)
		{
			if (fraPaymentInfo.Visible)
			{
				FormatPaymentInfoGrid_2(false);
				// show the frame
				// vsPaymentInfo.Top = 300
				// vsPaymentInfo.Height = (vsPaymentInfo.rows * vsPaymentInfo.RowHeight(0)) + 70
				// fraPaymentInfo.Height = vsPaymentInfo.Height + 500
				// fraPaymentInfo.Left = (frmReprint.Width - fraPaymentInfo.Width) / 2
				// fraPaymentInfo.Top = (frmReprint.Height - fraPaymentInfo.Height) / 3
				// fraPaymentInfo.Visible = True
			}
			if (fraReceiptList.Visible)
			{
				// format the frame to be shows
				// FC: FINAL: RPU: #i514: Set the position of the form directly to center of container
				//fraReceiptList.Top = FCConvert.ToInt32((this.Height - fraReceiptList.Height) / 3.0);
				//fraReceiptList.Left = FCConvert.ToInt32((this.Width - fraReceiptList.Width) / 2.0);
				fraReceiptList.CenterToContainer(this.ClientArea);
				fraReceiptList.Visible = true;
			}
			if (fraSummary.Visible)
			{
				// FC: FINAL: RPU: #i514: Set fraSuumary.Left and Top directly in designer
				//fraSummary.Left = FCConvert.ToInt32((this.Width - fraSummary.Width) / 2.0);
				//fraSummary.Top = FCConvert.ToInt32((this.Height - fraSummary.Height - fraCashOut.Height) / 3.0);
			}
			if (fraCashOut.Visible)
			{
				// FC: FINAL: RPU: #i514: Set fraCashOut.Left and Top directly in designer
				//fraCashOut.Left = FCConvert.ToInt32((this.Width - fraCashOut.Width) / 2.0);
				//fraCashOut.Top = fraSummary.Top + fraSummary.Height;
			}
			if (fraSplit.Visible)
			{
				// this will show the frame in the center of the screen
				// FC: FINAL: RPU: #i514: Set the position of the form directly to center of container
				//fraSplit.Top = FCConvert.ToInt32((this.Height - fraSplit.Height) / 3.0);
				//fraSplit.Left = FCConvert.ToInt32((this.Width - fraSplit.Width) / 2.0);
				fraSplit.CenterToContainer(this.ClientArea);
				fraSplit.Visible = true;
			}
			if (fraReceiptNumber.Visible)
			{
                // FC: FINAL: RPU: #i514: Set the position of the form directly to center of container
                //fraReceiptNumber.Top = FCConvert.ToInt32((this.Height - fraReceiptNumber.Height) / 3.0);
                //fraReceiptNumber.Left = FCConvert.ToInt32((this.Width - fraReceiptNumber.Width) / 2.0);
                //FC:FINAL:BSE:#4578 - should not be centered
                //fraReceiptNumber.CenterToContainer(this.ClientArea);
            }
        }

		private void mnuProcessPrint_Click(object sender, System.EventArgs e)
		{
			if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
			{
				return;
			}

			if (Conversion.Val(cmdPrintReceipt.Tag) == 0)
			{
				// show receipt
				cmdPrintReceipt_Click();
			}
			else
			{
				// print receipt
				PrintReceipt();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}
		// vbPorter upgrade warning: lngKey As int	OnWriteFCConvert.ToDouble(
		private void ShowReceipt_8(int lngKey, DateTime? dtDate = null)
		{
			ShowReceipt(lngKey, dtDate);
		}

		private void ShowReceipt(int lngKey = 0, DateTime? dtDateTemp = null)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show the receipt and will give the user the ability to print it
				clsDRWrapper rsReceipt = new clsDRWrapper();
				string strSQL = "";
				string strDate = "";
				modGlobal.Statics.gboolDetail = false;
				if (cmbArchive.SelectedIndex == 0)
				{
					boolCurrentYear = true;
				}
				else
				{
					boolCurrentYear = false;
				}
				if (boolCurrentYear)
				{
					strFrom = "Receipt";
				}
				else
				{
					strFrom = "LastYearReceipt";
				}
				if (lngKey == 0)
				{
					// Or Not boolCurrentYear Then
					// kk 101212   strSQL = "SELECT * FROM " & strFrom & " WHERE ReceiptNumber = " & lngReceiptNumber
					if (boolCurrentYear)
					{
						strSQL = "SELECT ReceiptDate as RDate, * FROM " + strFrom + " WHERE ReceiptNumber = " + FCConvert.ToString(lngReceiptNumber);
					}
					else
					{
						strSQL = "SELECT LastYearReceiptDate as RDate, * FROM " + strFrom + " WHERE ReceiptNumber = " + FCConvert.ToString(lngReceiptNumber);
					}
				}
				else
				{
					if (dtDate.ToOADate() != 0)
					{
						if (boolCurrentYear)
						{
							strDate = " AND ((ReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00AM' AND ReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59PM') OR ReceiptDate = '" + FCConvert.ToString(dtDate) + "')";
						}
						else
						{
							strDate = " AND ((LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59PM') OR LastYearReceiptDate = '" + FCConvert.ToString(dtDate) + "')";
						}
					}
					if (boolCurrentYear)
					{
						strSQL = "SELECT ReceiptDate as RDate, * FROM " + strFrom + " WHERE ReceiptKey = " + FCConvert.ToString(lngKey) + strDate;
					}
					else
					{
						strSQL = "SELECT LastYearReceiptDate as RDate, * FROM " + strFrom + " WHERE ReceiptKey = " + FCConvert.ToString(lngKey) + strDate;
					}
				}
				rsReceipt.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				if (rsReceipt.EndOfFile() != true && rsReceipt.BeginningOfFile() != true)
				{
					fraReceiptNumber.Visible = false;
					bool executeOLDDUPLICATE = false;
					if (rsReceipt.RecordCount() > 1)
					{
						if (lngKey != 0)
						{
							// pick the latest one
							rsReceipt.MoveLast();
							// move to the last receipt in the list on multiples
							executeOLDDUPLICATE = true;
							goto OLDDUPLICATE;
						}
						else
						{
							// show a list of the receipts that match the number and let the user choose
							ShowReceiptList(ref rsReceipt);
						}
					}
					else
					{
						executeOLDDUPLICATE = true;
						goto OLDDUPLICATE;
					}
					OLDDUPLICATE:
					;
					if (executeOLDDUPLICATE)
					{
						// fill the grid with the only receipt that matched the number
						lngReceiptKey = FCConvert.ToInt32(rsReceipt.Get_Fields_Int32("ReceiptKey"));
						if (boolCurrentYear)
						{
							FillGrid();
						}
						else
						{
							// TODO Get_Fields: Field [RDate] not found!! (maybe it is an alias?)
							FillGrid_2(rsReceipt.Get_Fields("RDate"));
						}
						lblSummaryTitle.Text = "Summary for Receipt #" + FCConvert.ToString(lngReceiptNumber);
						fraSummary.Visible = true;
						fraCashOut.Visible = true;
						mnuProcessSplit.Enabled = true;
						cmdPrintReceipt.Tag = 1;
						cmdPrintReceipt.Text = "Print Receipt";
						if (FCConvert.ToBoolean(rsReceipt.Get_Fields_Boolean("EFT")))
						{
							lblEFT.Visible = true;
						}
						else
						{
							lblEFT.Visible = false;
						}
					}
				}
				else
				{
					MessageBox.Show("This receipt number could not be found in the current record.", "Receipt Number Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Receipt", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowReceiptList(ref clsDRWrapper rsR)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngWid = 0;
				int lngCT = 0;
				// hide all frames
				fraSummary.Visible = false;
				fraCashOut.Visible = false;
				fraPaymentInfo.Visible = false;
				fraReceiptNumber.Visible = false;
				fraSplit.Visible = false;
				// format the grid
				lngWid = vsReceiptList.WidthOriginal;
				vsReceiptList.Cols = 5;
				vsReceiptList.ColWidth(0, 0);
				vsReceiptList.ColWidth(1, FCConvert.ToInt32(lngWid * 0.2));
				vsReceiptList.ColWidth(2, FCConvert.ToInt32(lngWid * 0.35));
				vsReceiptList.ColWidth(3, FCConvert.ToInt32(lngWid * 0.2));
				vsReceiptList.ColWidth(4, FCConvert.ToInt32(lngWid * 0.2));
				vsReceiptList.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// fill the header row
				vsReceiptList.TextMatrix(lngCT, 0, "");
				vsReceiptList.TextMatrix(lngCT, 1, "Date");
				vsReceiptList.TextMatrix(lngCT, 2, "Paid By");
				vsReceiptList.TextMatrix(lngCT, 3, "Teller ID");
				vsReceiptList.TextMatrix(lngCT, 4, "Receipt ID");
				lngCT = 1;
				// fill the grid
				while (!rsR.EndOfFile())
				{
					vsReceiptList.AddItem("");
					vsReceiptList.TextMatrix(lngCT, 0, rsR.Get_Fields_Int32("ReceiptKey"));
					// TODO Get_Fields: Field [RDate] not found!! (maybe it is an alias?)
					vsReceiptList.TextMatrix(lngCT, 1, Strings.Format(rsR.Get_Fields("RDate"), "MM/dd/yyyy"));
					vsReceiptList.TextMatrix(lngCT, 2, rsR.Get_Fields_String("PaidBy"));
					vsReceiptList.TextMatrix(lngCT, 3, rsR.Get_Fields_String("TellerID"));
					vsReceiptList.TextMatrix(lngCT, 4, rsR.Get_Fields_Int32("ReceiptKey"));
					lngCT += 1;
					rsR.MoveNext();
				}
				// adjust the height
				//if (lngCT * vsReceiptList.RowHeight(0) > fraReceiptList.Height - 600)
				//{
				//    vsReceiptList.Height = fraReceiptList.Height - 600;
				//}
				//else
				//{
				//    vsReceiptList.Height = (lngCT * vsReceiptList.RowHeight(0)) + 70;
				//}
				// format the frame to be shows
				fraReceiptList.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				// FC: FINAL: RPU: #i514: Set the position of the form directly to center of container
				//fraReceiptList.Top = FCConvert.ToInt32((this.Height - fraReceiptList.Height) / 3.0);
				//fraReceiptList.Left = FCConvert.ToInt32((this.Width - fraReceiptList.Width) / 2.0);
				fraReceiptList.CenterToContainer(this.ClientArea);
				fraReceiptList.Visible = true;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Receipt List Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatGrid()
		{
			int lngWid = 0;
			vsSummary.Cols = 9;
			lngWid = vsSummary.WidthOriginal;
			vsSummary.ColWidth(0, FCConvert.ToInt32(lngWid * 0.06));
			// Type Code
			vsSummary.ColWidth(1, FCConvert.ToInt32(lngWid * 0.22));
			// Type Name
			vsSummary.ColWidth(2, FCConvert.ToInt32(lngWid * 0.06));
			// Account
			vsSummary.ColWidth(3, FCConvert.ToInt32(lngWid * 0.11));
			// Year/Date of Bill/Plate/Nothing for modules REPP/UT/MV/CR or Other respectively
			vsSummary.ColWidth(4, FCConvert.ToInt32(lngWid * 0.22));
			// Name
			vsSummary.ColWidth(5, FCConvert.ToInt32(lngWid * 0.07));
			// Copies
			vsSummary.ColWidth(6, FCConvert.ToInt32(lngWid * 0.15));
			// Total
			vsSummary.ColWidth(7, FCConvert.ToInt32(lngWid * 0.07));
			// Combo box for receipt split up
			vsSummary.ColWidth(8, 0);
			// Hidden Field - this is the index of the element in the receipt array
			vsSummary.ColFormat(6, "#,##0.00");
			vsSummary.ColComboList(7, "#1;1|#2;2|#3;3|#4;4|#5;5");
			vsSummary.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //FC:FINAL:BSE #3506 column should be  right aligned
            vsSummary.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsSummary.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 7, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSummary.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 7, true);
			// sets the titles for the flexgrid
			vsSummary.TextMatrix(0, 0, "Type");
			vsSummary.TextMatrix(0, 1, "Type Description");
			vsSummary.TextMatrix(0, 2, "Acct");
			vsSummary.TextMatrix(0, 3, "Info");
			vsSummary.TextMatrix(0, 4, "Name");
			vsSummary.TextMatrix(0, 5, "Copies");
			vsSummary.TextMatrix(0, 6, "Amount");
			vsSummary.TextMatrix(0, 7, "Split");
			vsSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			vsSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			vsPayments.Cols = 7;
			lngWid = vsPayments.WidthOriginal;
			vsPayments.ColWidth(0, FCConvert.ToInt32(lngWid * 0.21));
			// Type of Payment
			vsPayments.ColWidth(1, FCConvert.ToInt32(lngWid * 0.31));
			// Amount
			vsPayments.ColWidth(2, FCConvert.ToInt32(lngWid * 0.45));
			// If Check then Check Number, if Credit Card then Card Type
			vsPayments.ColWidth(3, 0);
			// Hidden Field --> Array Index
			vsPayments.ColWidth(4, 0);
			// Hidden Field --> Convenience Fee
			vsPayments.ColWidth(5, 0);
			// Hidden Field --> Masked Card Number
			vsPayments.ColWidth(6, 0);
			// Hidden Field --> Expiration Date
			vsPayments.ColFormat(1, "#,##0.00");
			//vsPayments.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 6, true);
			vsPayments.ExtendLastCol = true;
			// sets the titles for the flexgrid
			vsPayments.TextMatrix(0, 0, "Type");
			vsPayments.TextMatrix(0, 1, "Amount");
			vsPayments.TextMatrix(0, 2, "Ref");
			vsPayments.TextMatrix(0, 3, "");
			vsPayments.TextMatrix(0, 4, "");
			vsPayments.TextMatrix(0, 5, "");
			vsPayments.TextMatrix(0, 6, "");
            //FC:FINAL:BSE #3508 column should be algned right
            vsPayments.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// FC: FINAL: RPU: #i514: Set these directly in designer
			//fraSummary.Left = FCConvert.ToInt32((this.Width - fraSummary.Width) / 2.0);
			//fraCashOut.Left = FCConvert.ToInt32((this.Width - fraCashOut.Width) / 2.0);
			//fraSummary.Top = FCConvert.ToInt32((this.Height - fraSummary.Height - fraCashOut.Height) / 3.0);
			//fraCashOut.Top = fraSummary.Top + fraSummary.Height;
		}

		private void FillGrid_2(DateTime dtOldReceiptDate)
		{
			FillGrid(dtOldReceiptDate);
		}

		private void FillGrid(DateTime? dtOldReceiptDateTemp = null)
		{
			DateTime dtOldReceiptDate = dtOldReceiptDateTemp ?? DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will put all of the information into the grid and fill the receiptarray
				int intCT;
				string strSQL = "";
				clsDRWrapper rsReceipt = new clsDRWrapper();
				clsDRWrapper rsPayments = new clsDRWrapper();
				clsDRWrapper rsAR = new clsDRWrapper();
				bool boolCheck = false;
				bool boolCredit = false;
				int lngRecNumber = 0;
				// kk 101212  Dim strBeginningString  As String
				string strDateString = "";
				if (boolCurrentYear)
				{
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						strSQL = "SELECT *, Receipt.TellerID AS ReceiptTellerID, Archive.TellerID AS ArchiveTellerID, Archive.TownKey AS ArchiveTownKey, Archive.ReceiptNumber AS ArchiveReceiptNumber, Archive.ConvenienceFeeGLAccount as ArchiveCNVFeeAcct, Archive.ConvenienceFee as ArchiveCNVFee, Receipt.ConvenienceFee as ReceiptCNVFee FROM (Archive INNER JOIN Receipt ON Archive.ReceiptNumber = Receipt.ReceiptKey) INNER JOIN Type ON Archive.ReceiptType = Type.TypeCode AND Archive.TownKey = Type.Townkey WHERE Receipt.ReceiptKey = " + FCConvert.ToString(lngReceiptKey);
					}
					else
					{
						strSQL = "SELECT *, Receipt.TellerID AS ReceiptTellerID, Archive.TellerID AS ArchiveTellerID, Archive.TownKey AS ArchiveTownKey, Archive.ReceiptNumber AS ArchiveReceiptNumber, Archive.ConvenienceFeeGLAccount as ArchiveCNVFeeAcct, Archive.ConvenienceFee as ArchiveCNVFee, Receipt.ConvenienceFee as ReceiptCNVFee FROM (Archive INNER JOIN Receipt ON Archive.ReceiptNumber = Receipt.ReceiptKey) INNER JOIN Type ON Archive.ReceiptType = Type.TypeCode WHERE Receipt.ReceiptKey = " + FCConvert.ToString(lngReceiptKey);
					}
					// kk 101212  strBeginningString = ""
				}
				else
				{
					if (dtOldReceiptDate.ToOADate() != 0)
					{
						strDateString = " AND (((LastYearArchive.LastYearArchiveDate > '" + Strings.Format(dtOldReceiptDate, "MM/dd/yyyy") + " 12:00AM' AND LastYearArchive.LastYearArchiveDate < '" + Strings.Format(dtOldReceiptDate, "MM/dd/yyyy") + " 11:59PM') OR LastYearArchive.LastYearArchiveDate = '" + Strings.Format(dtOldReceiptDate, "MM/dd/yyyy") + "') OR ((ActualSystemDate > '" + Strings.Format(dtOldReceiptDate, "MM/dd/yyyy") + " 12:00AM' AND ActualSystemDate < '" + Strings.Format(dtOldReceiptDate, "MM/dd/yyyy") + " 11:59PM') OR ActualSystemDate = '" + Strings.Format(dtOldReceiptDate, "MM/dd/yyyy") + "'))";
					}
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						strSQL = "SELECT *, LastYearReceipt.TellerID AS ReceiptTellerID, LastYearArchive.TellerID AS ArchiveTellerID, LastYearArchive.TownKey AS ArchiveTownKey, LastYearArchive.ReceiptNumber AS ArchiveReceiptNumber, LastYearArchive.ConvenienceFeeGLAccount as ArchiveCNVFeeAcct, LastYearArchive.ConvenienceFee as ArchiveCNVFee, LastYearReceipt.ConvenienceFee as ReceiptCNVFee FROM (LastYearArchive INNER JOIN LastYearReceipt ON LastYearArchive.ReceiptNumber = LastYearReceipt.ReceiptKey AND (LastYearArchive.ActualSystemDate >= LastYearReceipt.LastYearReceiptDate)) INNER JOIN Type ON LastYearArchive.ReceiptType = Type.TypeCode AND LastYearArchive.TownKey = Type.TownKey WHERE LastYearReceipt.ReceiptKey = " + FCConvert.ToString(lngReceiptKey) + " AND LastYearReceipt.LastYearReceiptDate = '" + FCConvert.ToString(dtOldReceiptDate) + "' " + strDateString;
					}
					else
					{
						strSQL = "SELECT *, LastYearReceipt.TellerID AS ReceiptTellerID, LastYearArchive.TellerID AS ArchiveTellerID, LastYearArchive.TownKey AS ArchiveTownKey, LastYearArchive.ReceiptNumber AS ArchiveReceiptNumber, LastYearArchive.ConvenienceFeeGLAccount as ArchiveCNVFeeAcct, LastYearArchive.ConvenienceFee as ArchiveCNVFee, LastYearReceipt.ConvenienceFee as ReceiptCNVFee FROM (LastYearArchive INNER JOIN LastYearReceipt ON LastYearArchive.ReceiptNumber = LastYearReceipt.ReceiptKey AND (LastYearArchive.ActualSystemDate >= LastYearReceipt.LastYearReceiptDate)) INNER JOIN Type ON LastYearArchive.ReceiptType = Type.TypeCode WHERE LastYearReceipt.ReceiptKey = " + FCConvert.ToString(lngReceiptKey) + " AND LastYearReceipt.LastYearReceiptDate = '" + FCConvert.ToString(dtOldReceiptDate) + "' " + strDateString;
					}
					// kk 101212  strBeginningString = "LastYear"
				}
				if (rsReceipt.OpenRecordset(strSQL, modExtraModules.strCRDatabase))
				{
					if (rsReceipt.EndOfFile() != true && rsReceipt.BeginningOfFile() != true)
					{
						if (boolCurrentYear)
						{
							dtReceiptDate = (DateTime)rsReceipt.Get_Fields_DateTime("ReceiptDate");
						}
						else
						{
							dtReceiptDate = (DateTime)rsReceipt.Get_Fields_DateTime("LastYearReceiptDate");
						}
						if (Strings.Trim(txtTellerID.Text) == "")
						{
							// TODO Get_Fields: Field [ArchiveTellerID] not found!! (maybe it is an alias?)
							strTellerID = FCConvert.ToString(rsReceipt.Get_Fields("ArchiveTellerID"));
							// kk 101212  (strBeginningString & "TellerID")
						}
						// TODO Get_Fields: Field [ArchiveReceiptNumber] not found!! (maybe it is an alias?)
						lngRecNumber = FCConvert.ToInt32(rsReceipt.Get_Fields("ArchiveReceiptNumber"));
						// kk 101212  (strBeginningString & "ReceiptNumber")
						for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
						{
							// fill the Receipt Array
							if (rsReceipt.EndOfFile() != true)
							{
								// Used                As Boolean      'if True then I know that this array index is currently being used
								modUseCR.Statics.ReceiptArray[intCT].Used = true;
								// Type                As Integer
								modUseCR.Statics.ReceiptArray[intCT].Type = FCConvert.ToInt32(rsReceipt.Get_Fields_Int32("ReceiptType"));
								// Res Code
								// TODO Get_Fields: Field [ArchviveTownKey] not found!! (maybe it is an alias?)
								modUseCR.Statics.ReceiptArray[intCT].ResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rsReceipt.Get_Fields("ArchviveTownKey"))));
								// kk 101212   (strBeginningString & "TownKey")
								lngMultiTown = modUseCR.Statics.ReceiptArray[intCT].ResCode;
								// Module              As String       'P = Personal Property, R = Real Estate, W = UT Water, S = UT Sewer, X = Cash Receipting
								// .Module = rsReceipt.Fields("From")
								// CollectionCode      As String       'this will bring back the type of transaction from the module
								modUseCR.Statics.ReceiptArray[intCT].CollectionCode = FCConvert.ToString(rsReceipt.Get_Fields_String("CollectionCode"));
								// Account             As String
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Account = FCConvert.ToString(rsReceipt.Get_Fields("AccountNumber"));
								// TypeDescription     As String
								// MAL@20080611: Add support for AR Types
								// Tracker Reference: 14057
								if (modUseCR.Statics.ReceiptArray[intCT].Type == 97)
								{
									modUseCR.Statics.ReceiptArray[intCT].ARBillType = FCConvert.ToInt32(rsReceipt.Get_Fields_Int32("ARBillType"));
									rsAR.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].ARBillType), modExtraModules.strARDatabase);
									if (rsAR.RecordCount() > 0)
									{
										modUseCR.Statics.ReceiptArray[intCT].TypeDescription = FCConvert.ToString(rsAR.Get_Fields_String("TypeTitle"));
									}
									else
									{
										// Default
										modUseCR.Statics.ReceiptArray[intCT].TypeDescription = FCConvert.ToString(rsReceipt.Get_Fields_String("TypeTitle"));
									}
								}
								else
								{
									modUseCR.Statics.ReceiptArray[intCT].TypeDescription = FCConvert.ToString(rsReceipt.Get_Fields_String("TypeTitle"));
								}
								// Reference           As String
								modUseCR.Statics.ReceiptArray[intCT].Reference = FCConvert.ToString(rsReceipt.Get_Fields_String("Ref"));
								// Control1            As String
								modUseCR.Statics.ReceiptArray[intCT].Control1 = FCConvert.ToString(rsReceipt.Get_Fields_String("Control1"));
								// kk 101212   (strBeginningString & "Archive.Control1")
								// Control2            As String
								modUseCR.Statics.ReceiptArray[intCT].Control2 = FCConvert.ToString(rsReceipt.Get_Fields_String("Control2"));
								// kk 101212   (strBeginningString & "Archive.Control2")
								// Control3            As String
								modUseCR.Statics.ReceiptArray[intCT].Control3 = FCConvert.ToString(rsReceipt.Get_Fields_String("Control3"));
								// kk 101212   (strBeginningString & "Archive.Control3")
								// Fee1                As Double       'fee amounts
								// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Fee1 = rsReceipt.Get_Fields("Amount1");
								// Fee2                As Double
								// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Fee2 = rsReceipt.Get_Fields("Amount2");
								// Fee3                As Double
								// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Fee3 = rsReceipt.Get_Fields("Amount3");
								// Fee4                As Double
								// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Fee4 = rsReceipt.Get_Fields("Amount4");
								// Fee5                As Double
								// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Fee5 = rsReceipt.Get_Fields("Amount5");
								// Fee6                As Double
								// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Fee6 = rsReceipt.Get_Fields("Amount6");
								// FeeD1               As String       'descriptions for the fees
								modUseCR.Statics.ReceiptArray[intCT].FeeD1 = FCConvert.ToString(rsReceipt.Get_Fields_String("Title1"));
								// FeeD2               As String
								modUseCR.Statics.ReceiptArray[intCT].FeeD2 = FCConvert.ToString(rsReceipt.Get_Fields_String("Title2"));
								// FeeD3               As String
								modUseCR.Statics.ReceiptArray[intCT].FeeD3 = FCConvert.ToString(rsReceipt.Get_Fields_String("Title3"));
								// FeeD4               As String
								modUseCR.Statics.ReceiptArray[intCT].FeeD4 = FCConvert.ToString(rsReceipt.Get_Fields_String("Title4"));
								// FeeD5               As String
								modUseCR.Statics.ReceiptArray[intCT].FeeD5 = FCConvert.ToString(rsReceipt.Get_Fields_String("Title5"));
								// FeeD6               As String
								modUseCR.Statics.ReceiptArray[intCT].FeeD6 = FCConvert.ToString(rsReceipt.Get_Fields_String("Title6"));
								// Total               As Double       'total of all the fees
								modUseCR.Statics.ReceiptArray[intCT].Total = modUseCR.Statics.ReceiptArray[intCT].Fee1 + modUseCR.Statics.ReceiptArray[intCT].Fee2 + modUseCR.Statics.ReceiptArray[intCT].Fee3 + modUseCR.Statics.ReceiptArray[intCT].Fee4 + modUseCR.Statics.ReceiptArray[intCT].Fee5 + modUseCR.Statics.ReceiptArray[intCT].Fee6;
								// Name                As String
								modUseCR.Statics.ReceiptArray[intCT].Name = FCConvert.ToString(rsReceipt.Get_Fields_String("Name"));
								// PaidBy              As String
								modUseCR.Statics.ReceiptArray[intCT].PaidBy = FCConvert.ToString(rsReceipt.Get_Fields_String("PaidBy"));
								// PrintReceipt
								modUseCR.Statics.ReceiptArray[intCT].PrintReceipt = true;
								// always default to true
								// Date                As Date
								// kk 101212  .Date = rsReceipt.Fields(strBeginningString & "ArchiveDate")
								if (boolCurrentYear)
								{
									modUseCR.Statics.ReceiptArray[intCT].Date = (DateTime)rsReceipt.Get_Fields_DateTime("ArchiveDate");
								}
								else
								{
									modUseCR.Statics.ReceiptArray[intCT].Date = (DateTime)rsReceipt.Get_Fields_DateTime("LastYearArchiveDate");
								}
								// Copies              As Integer
								// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
								modUseCR.Statics.ReceiptArray[intCT].Copies = FCConvert.ToInt16(rsReceipt.Get_Fields("Copies"));
								// Comment             As String
								modUseCR.Statics.ReceiptArray[intCT].Comment = FCConvert.ToString(rsReceipt.Get_Fields_String("Comment"));
								// TODO Get_Fields: Field [ArchiveCNVFee] not found!! (maybe it is an alias?)
								modUseCR.Statics.ReceiptArray[intCT].ConvenienceFee = FCConvert.ToDecimal(rsReceipt.Get_Fields("ArchiveCNVFee"));
								// TODO Get_Fields: Field [ArchiveCNVFeeAcct] not found!! (maybe it is an alias?)
								modUseCR.Statics.ReceiptArray[intCT].ConvenienceFeeAccount = FCConvert.ToString(rsReceipt.Get_Fields("ArchiveCNVFeeAcct"));
								modUseCR.Statics.ReceiptArray[intCT].SeperateCreditCardGLAccount = FCConvert.ToString(rsReceipt.Get_Fields_String("SeperateCreditCardGLAccount"));
								modUseCR.Statics.ReceiptArray[intCT].CashPaidAmount = FCConvert.ToDecimal(rsReceipt.Get_Fields_Decimal("CashPaidAmount"));
								modUseCR.Statics.ReceiptArray[intCT].CheckPaidAmount = FCConvert.ToDecimal(rsReceipt.Get_Fields_Decimal("CheckPaidAmount"));
								modUseCR.Statics.ReceiptArray[intCT].CardPaidAmount = FCConvert.ToDecimal(rsReceipt.Get_Fields_Decimal("CardPaidAmount"));
								// RecordKey           As Long         'this is the ID from whichever database it came from 0 if not from another module
								// cannont obtain at this point...not needed since nothing can be changed here
								// ArrayIndex          As Integer
								// Affect Cash
								modUseCR.Statics.ReceiptArray[intCT].AffectCash = FCConvert.ToBoolean(rsReceipt.Get_Fields_Boolean("AffectCash"));
								// Affect Cash Drawer
								modUseCR.Statics.ReceiptArray[intCT].AffectCashDrawer = FCConvert.ToBoolean(rsReceipt.Get_Fields_Boolean("AffectCashDrawer"));
								// BillingYear         As Integer
								if (modUseCR.Statics.ReceiptArray[intCT].Type >= 90 && modUseCR.Statics.ReceiptArray[intCT].Type <= 95)
								{
									if (FCConvert.ToString(rsReceipt.Get_Fields_String("Ref")).Length >= 4)
									{
										modUseCR.Statics.ReceiptArray[intCT].BillingYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsReceipt.Get_Fields_String("Ref")), 4))));
									}
								}
								// CashAlternativeAcct As String
								modUseCR.Statics.ReceiptArray[intCT].CashAlternativeAcct = FCConvert.ToString(rsReceipt.Get_Fields_String("DefaultAccount"));
								// kk 101212   strBeginningString & "DefaultAccount")
								// Split
								modUseCR.Statics.ReceiptArray[intCT].Split = FCConvert.ToInt32(rsReceipt.Get_Fields_Int16("Split"));
								if (modUseCR.Statics.ReceiptArray[intCT].Split == 0)
									modUseCR.Statics.ReceiptArray[intCT].Split = 1;
								if (!boolCurrentYear)
								{
									// check for duplicates
								}
								rsReceipt.MoveNext();
							}
							else
							{
								rsReceipt.MoveFirst();
								break;
							}
						}
					}
					else
					{
						// This receipt record does not have any archive records with it
						MessageBox.Show("This receipt is missing archive records.", "Missing Archive Records", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					intCT = 1;
					// fill the Summary
					do
					{
						// test to see if this split has been added before...if it has not, then add it to the combo box of splits
						if (vsSummary.FindRow(FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].Split), 1, 7) < 1)
						{
							cmbSplit.AddItem(modUseCR.Statics.ReceiptArray[intCT].Split.ToString());
						}
						vsSummary.AddItem("");
						// 0 - Type Code
						vsSummary.TextMatrix(intCT, 0, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].Type));
						// 1 - Type Name
						vsSummary.TextMatrix(intCT, 1, modUseCR.Statics.ReceiptArray[intCT].TypeDescription);
						// 2 - Account
						vsSummary.TextMatrix(intCT, 2, modUseCR.Statics.ReceiptArray[intCT].Account);
						// 3 - Year/Date of Bill/Plate/Nothing for modules REPP/UT/MV/CR or Other respectively
						vsSummary.TextMatrix(intCT, 3, modUseCR.Statics.ReceiptArray[intCT].Reference);
						// 4 - Name
						vsSummary.TextMatrix(intCT, 4, modUseCR.Statics.ReceiptArray[intCT].Name);
						// 5 - Copies
						vsSummary.TextMatrix(intCT, 5, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].Copies));
						// 6 - Total
						vsSummary.TextMatrix(intCT, 6, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].Total));
						// 7 - Combo box for receipt split up
						vsSummary.TextMatrix(intCT, 7, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].Split));
						// 8 - Hidden Field - this is the index of the element in the receipt array
						vsSummary.TextMatrix(intCT, 8, FCConvert.ToString(intCT));
						// check to see if it is a N/N payment then color the line if it is
						if (modUseCR.Statics.ReceiptArray[intCT].AffectCash == false && modUseCR.Statics.ReceiptArray[intCT].AffectCashDrawer == false)
						{
							// change backcolor of row to gray
							vsSummary.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCT, 0, intCT, vsSummary.Cols - 1, 0xE0E0E0);
						}
						else
						{
							vsSummary.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCT, 0, intCT, vsSummary.Cols - 1, Color.White);
						}
						// increment the counter
						intCT += 1;
					}
					while (!(modUseCR.Statics.ReceiptArray[intCT].Used == false));
					// reinitialize the counter
					intCT = 1;
					// use the first record
					rsReceipt.MoveFirst();
					if (rsReceipt.EndOfFile() != true && rsReceipt.BeginningOfFile() != true)
					{
						// show the PaidBy Name
						txtPaidBy.Text = FCConvert.ToString(rsReceipt.Get_Fields_String("PaidBy"));
						// TODO Get_Fields: Field [ReceiptTellerID] not found!! (maybe it is an alias?)
						txtTellerID.Text = FCConvert.ToString(rsReceipt.Get_Fields("ReceiptTellerID"));
						// kk 101212   (strBeginningString & "Receipt.TellerID")
						// set the textbox totals
						txtCashOutChange.Text = Strings.Format(rsReceipt.Get_Fields_Double("Change"), "#,##0.00");
						if (Conversion.Val(rsReceipt.Get_Fields_Double("Change")) != 0)
						{
							txtCashOutChange.Visible = true;
							lblCashOutChange.Visible = true;
						}
						else
						{
							txtCashOutChange.Visible = false;
							lblCashOutChange.Visible = false;
						}
						txtCashOutTotal.Text = txtCashOutChange.Text;
						// check to see if there are any payments made by Check or CC
						if (rsReceipt.Get_Fields_Double("CardAmount") > 0)
						{
							boolCredit = true;
							if (rsReceipt.Get_Fields_Double("CashAmount") == 0)
							{
								// TODO Get_Fields: Field [ReceiptCNVFee] not found!! (maybe it is an alias?)
								txtCashOutCreditPaid.Text = Strings.Format(rsReceipt.Get_Fields_Double("CardAmount") + rsReceipt.Get_Fields_Double("ReceiptCNVFee") + rsReceipt.Get_Fields_Double("Change"), "#,##0.00");
							}
							else
							{
								// TODO Get_Fields: Field [ReceiptCNVFee] not found!! (maybe it is an alias?)
								txtCashOutCreditPaid.Text = Strings.Format(rsReceipt.Get_Fields_Double("CardAmount") + rsReceipt.Get_Fields("ReceiptCNVFee"), "#,##0.00");
							}
							// txtCashOutTotal.Text = Format(CDbl(txtCashOutTotal.Text) + CDbl(txtCashOutCreditPaid.Text), "#,##0.00")
						}
						if (rsReceipt.Get_Fields_Double("CheckAmount") > 0)
						{
							boolCheck = true;
							if (rsReceipt.Get_Fields_Double("CashAmount") == 0 && !boolCredit)
							{
								txtCashOutCheckPaid.Text = Strings.Format(rsReceipt.Get_Fields_Double("CheckAmount") + rsReceipt.Get_Fields_Double("Change"), "#,##0.00");
							}
							else
							{
								txtCashOutCheckPaid.Text = Strings.Format(rsReceipt.Get_Fields_Double("CheckAmount"), "#,##0.00");
							}
							// txtCashOutTotal.Text = Format(CDbl(txtCashOutTotal.Text) + CDbl(txtCashOutCheckPaid.Text), "#,##0.00")
						}
						txtCashOutTotal.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCashPaid.Text) + FCConvert.ToDouble(txtCashOutCheckPaid.Text) + FCConvert.ToDouble(txtCashOutCreditPaid.Text) - FCConvert.ToDouble(txtCashOutChange.Text), "#,##0.00");
						// TODO Get_Fields: Field [ReceiptCNVFee] not found!! (maybe it is an alias?)
						txtTransactions.Text = Strings.Format(FCConvert.ToDouble(txtCashOutTotal.Text) - FCConvert.ToDouble(rsReceipt.Get_Fields("ReceiptCNVFee")), "#,##0.00");
						// TODO Get_Fields: Field [ReceiptCNVFee] not found!! (maybe it is an alias?)
						txtConvenienceFee.Text = Strings.Format(rsReceipt.Get_Fields("ReceiptCNVFee"), "#,##0.00");
						// fill the Payments
						if (rsReceipt.Get_Fields_Double("CashAmount") != 0)
						{
							txtCashOutCashPaid.Text = Strings.Format(rsReceipt.Get_Fields_Double("CashAmount") + rsReceipt.Get_Fields_Double("Change"), "#,##0.00");
							// txtCashOutTotal.Text = Format(CDbl(txtCashOutTotal.Text) + CDbl(txtCashOutCashPaid.Text), "#,##0.00")
							txtCashOutTotal.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCashPaid.Text) + FCConvert.ToDouble(txtCashOutCheckPaid.Text) + FCConvert.ToDouble(txtCashOutCreditPaid.Text) - rsReceipt.Get_Fields_Double("Change"), "#,##0.00");
							// TODO Get_Fields: Field [ReceiptCNVFee] not found!! (maybe it is an alias?)
							txtTransactions.Text = Strings.Format(FCConvert.ToDouble(txtCashOutTotal.Text) - FCConvert.ToDouble(rsReceipt.Get_Fields("ReceiptCNVFee")), "#,##0.00");
							// TODO Get_Fields: Field [ReceiptCNVFee] not found!! (maybe it is an alias?)
							txtConvenienceFee.Text = Strings.Format(rsReceipt.Get_Fields("ReceiptCNVFee"), "#,##0.00");
							vsPayments.AddItem("");
							// 0 - Type of Payment
							vsPayments.TextMatrix(intCT, 0, "CSH");
							// 1 - Amount
							vsPayments.TextMatrix(intCT, 1, Strings.Format(rsReceipt.Get_Fields_Double("CashAmount"), "#,##0.00"));
							// 2 - If Check then Check Number, if Credit Card then Card Type
							vsPayments.TextMatrix(intCT, 2, "CASH");
							// 3 - Hidden Field --> Array Index
							vsPayments.TextMatrix(intCT, 3, "CASH");
							// 4 - Hidden Field
							intCT += 1;
						}
						if (boolCheck)
						{
							strSQL = "SELECT * FROM CheckMaster INNER JOIN Bank ON CheckMaster.BankNumber = Bank.ID WHERE ReceiptNumber = " + FCConvert.ToString(lngRecNumber);
							rsPayments.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
							if (rsPayments.EndOfFile() != true && rsPayments.BeginningOfFile() != true)
							{
								do
								{
									vsPayments.AddItem("");
									// 0 - Type of Payment
									vsPayments.TextMatrix(intCT, 0, "CHK");
									// 1 - Amount
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vsPayments.TextMatrix(intCT, 1, Strings.Format(rsPayments.Get_Fields("Amount"), "#,##0.00"));
									// 2 - If Check then Check Number, if Credit Card then Card Type
									// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
									vsPayments.TextMatrix(intCT, 2, FCConvert.ToString(rsPayments.Get_Fields("CheckNumber")));
									// 3 - Hidden Field --> Array Index
									vsPayments.TextMatrix(intCT, 3, rsPayments.Get_Fields_String("Name") + " - " + rsPayments.Get_Fields_String("RoutingTransit"));
									// 4 - Hidden Field
									intCT += 1;
									rsPayments.MoveNext();
								}
								while (!rsPayments.EndOfFile());
							}
						}
						if (boolCredit)
						{
							strSQL = "SELECT * FROM CCMaster INNER JOIN CCType ON CCMaster.CCType = CCType.ID WHERE ReceiptNumber = " + FCConvert.ToString(lngRecNumber);
							rsPayments.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
							if (rsPayments.EndOfFile() != true && rsPayments.BeginningOfFile() != true)
							{
								do
								{
									vsPayments.AddItem("");
									// 0 - Type of Payment
									vsPayments.TextMatrix(intCT, 0, "CC");
									// 1 - Amount
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vsPayments.TextMatrix(intCT, 1, Strings.Format(rsPayments.Get_Fields("Amount"), "#,##0.00"));
									// 2 - If Check then Check Number, if Credit Card then Card Type
									// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
									vsPayments.TextMatrix(intCT, 2, FCConvert.ToString(rsPayments.Get_Fields("Type")));
									// 3 - Hidden Field --> Array Index
									// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
									vsPayments.TextMatrix(intCT, 3, FCConvert.ToString(rsPayments.Get_Fields("Type")));
									// 4 - Hidden Field
									vsPayments.TextMatrix(intCT, 4, FCConvert.ToString(rsPayments.Get_Fields_Decimal("ConvenienceFee")));
									vsPayments.TextMatrix(intCT, 5, FCConvert.ToString(rsPayments.Get_Fields_String("MaskedCreditCardNumber")));
									vsPayments.TextMatrix(intCT, 6, FCConvert.ToString(rsPayments.Get_Fields_String("CryptCardExpiration")));
									intCT += 1;
									rsPayments.MoveNext();
								}
								while (!rsPayments.EndOfFile());
							}
						}
					}
					else
					{
						MessageBox.Show("Error while loading payments for this receipt.  This may occur for receipts that were created before conversion.", "Payment Loading Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				else
				{
					MessageBox.Show("", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngSplit As int	OnWrite(string)	OnRead(string)
		private void PrintSplit_2(int lngSplit)
		{
			PrintSplit(ref lngSplit);
		}

		private void PrintSplit(ref int lngSplit)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (vsSummary.FindRow(lngSplit, -1, 7) != -1)
				{
					// print only the splits that have payments
					strPassTag = FCConvert.ToString(lngSplit);
					// arReceipt.StartUp
					modGlobal.Statics.gboolReprint = true;
					// arReceipt.StartUp
					if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
					{
                        //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                        //arReceipt.InstancePtr.PrintReport();
                        arReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
                        //arReceipt.InstancePtr.Hide();
                        // arReceipt.Show , MDIParent
                    }
					else
					{
                        arGenericReceipt.InstancePtr.Unload();
                        //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                        //arGenericReceipt.InstancePtr.PrintReport();
                        arGenericReceipt.InstancePtr.PrintReportOnDotMatrix("RcptPrinterName");
						// arGenericReceipt.Show
						//arGenericReceipt.InstancePtr.Hide();
					}
				}
				else
				{
					MessageBox.Show("Split #" + FCConvert.ToString(lngSplit) + " not found on this receipt.", "Split Number Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Printing Split", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PrintReceipt()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will do all the work to print the receipt
				// vbPorter upgrade warning: intCT As short --> As int	OnRead(string, int)
				int intCT;
				int lngRows;
				bool boolPrint = false;
				bool boolBatch = false;
				for (intCT = 1; intCT <= vsSummary.Rows - 1; intCT++)
				{
					// check for a batch update
					if (FCConvert.ToDouble(vsSummary.TextMatrix(intCT, 7)) > 5)
					{
						boolBatch = true;
						break;
					}
				}
				// reinitialize the totals
				for (intCT = 1; intCT <= 5; intCT++)
				{
					modUseCR.Statics.dblSplitTotals[intCT] = 0;
				}
				// fill the totals
				for (intCT = 1; intCT <= 5; intCT++)
				{
					for (lngRows = 1; lngRows <= vsSummary.Rows - 1; lngRows++)
					{
						if (Conversion.Val(vsSummary.TextMatrix(lngRows, 7)) == intCT)
						{
							// is this on the same split
							if (modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(this.vsSummary.TextMatrix(lngRows, 8))].CashAlternativeAcct == "" || Strings.Trim(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(this.vsSummary.TextMatrix(lngRows, 8))].CashAlternativeAcct) == "M")
							{
								// if this row should be counted
								modUseCR.Statics.dblSplitTotals[intCT] += FCConvert.ToDouble(vsSummary.TextMatrix(lngRows, 6)) + FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(this.vsSummary.TextMatrix(lngRows, 8))].ConvenienceFee);
							}
						}
					}
				}
				// check all of the splits
				for (intCT = 1; intCT <= 5; intCT++)
				{
					if (vsSummary.FindRow(FCConvert.ToString(intCT), -1, 7) != -1)
					{
						// print only the splits that have payments
						strPassTag = FCConvert.ToString(intCT);
						if (boolPrint)
						{
							if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
							{
								arReceipt.InstancePtr.StartUp();
							}
							else
							{
								arGenericReceipt.InstancePtr.StartUp();
							}
						}
						boolPrint = true;
						modGlobal.Statics.gboolReprint = true;
						arReceipt.InstancePtr.StartUp();
						if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
						{
                            //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                            //arReceipt.InstancePtr.PrintReport();
                            arReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
							// arReceipt.Show vbModal, MDIParent
							//arReceipt.InstancePtr.Hide();
						}
						else
						{
                            arGenericReceipt.InstancePtr.Unload();
                            //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                            //arGenericReceipt.InstancePtr.PrintReport();
                            arGenericReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
                            // arGenericReceipt.Show vbModal, MDIParent
                            //arGenericReceipt.InstancePtr.Hide();
                        }
					}
				}
				intCT = 1;
				if (boolBatch)
				{
					do
					{
						// this will assume that the receipt array has been filled with both the payments from
						// the grid and the payments from the batch update...ALL of the split numbers for a batch payment
						// will be greater than 5
						// individually print each batch line item
						if (modUseCR.Statics.ReceiptArray[intCT].Split > 5 && modUseCR.Statics.ReceiptArray[intCT].Used && modUseCR.Statics.ReceiptArray[intCT].Copies > 0)
						{
							strPassTag = FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].Split);
							intRAIndex = intCT;
							if (boolPrint)
							{
								if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
								{
									arReceipt.InstancePtr.StartUp();
								}
								else
								{
									arGenericReceipt.InstancePtr.StartUp();
								}
							}
							boolPrint = true;
							modGlobal.Statics.gboolReprint = true;
							if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
							{
                                //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                                //arReceipt.InstancePtr.PrintReport();
                                arReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
                                //arReceipt.InstancePtr.Hide();
                            }
							else
							{
                                arGenericReceipt.InstancePtr.Unload();
                                //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                                //arGenericReceipt.InstancePtr.PrintReport();
                                arGenericReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
                                // arGenericReceipt.Show
                                //arGenericReceipt.InstancePtr.Hide();
                            }
							intCT += 1;
						}
						else
						{
							intCT += 1;
						}
					}
					while (!(modUseCR.Statics.ReceiptArray[intCT].Used == false));
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Printing Receipt", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuProcessSplit_Click(object sender, System.EventArgs e)
		{
			// this will let the user choose which split to print
			// and will only print that split
			int lngRow;
			clsDRWrapper rsSplit = new clsDRWrapper();
			// this will hide the other frames
			fraCashOut.Visible = false;
			fraReceiptNumber.Visible = false;
			fraSummary.Visible = false;
			mnuProcessSplit.Enabled = false;
			// this will show the frame in the center of the screen
			// FC: FINAL: RPU: #i514: Set the position of the form directly to center of container
			//fraSplit.Top = FCConvert.ToInt32((this.Height - fraSplit.Height) / 3.0);
			//fraSplit.Left = FCConvert.ToInt32((this.Width - fraSplit.Width) / 2.0);
			fraSplit.CenterToContainer(this.ClientArea);
			fraSplit.Visible = true;
		}

		private void txtReceipt_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						cmdPrintReceipt_Click();
						break;
					}
			}
			//end switch
		}

		private void txtReceipt_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsPayments_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngMR;
			int lngArrIndex;
			lngMR = vsPayments.MouseRow;
			// what row is being clicked on
			if (lngMR < 0)
				return;
			if (e.Button == MouseButtons.Right)
			{
				// right button
				ToolTip1.SetToolTip(vsPayments, vsPayments.TextMatrix(lngMR, 3));
			}
		}

		private void vsReceiptList_DblClick(object sender, System.EventArgs e)
		{
			int lngRW;
			lngRW = vsReceiptList.Row;
			if (lngRW > 0 && lngRW < vsReceiptList.Rows)
			{
				fraReceiptList.Visible = false;
				// this will pass the ID in forcing the showreceipt to only find one receipt
				ShowReceipt_8(FCConvert.ToInt32(Conversion.Val(vsReceiptList.TextMatrix(lngRW, 0))), DateAndTime.DateValue(vsReceiptList.TextMatrix(lngRW, 1)));
			}
		}

		private void vsSummary_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngMR;
			int lngArrIndex = 0;
			lngMR = vsSummary.MouseRow;
			// what row is being clicked on
			if (e.Button == MouseButtons.Right)
			{
				// right button
				lngArrIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSummary.TextMatrix(lngMR, 8))));
				ShowPaymentRowInfo(ref lngArrIndex);
			}
			else
			{
				if (fraPaymentInfo.Visible)
				{
					fraPaymentInfo.Visible = false;
				}
			}
		}

		private void ShowPaymentRowInfo(ref int lngIndex)
		{
			clsDRWrapper rsType = new clsDRWrapper();
			if (lngIndex != 0)
			{
				FormatPaymentInfoGrid_2(true);
				// first col
				vsPaymentInfo.TextMatrix(0, 0, "Type");
				vsPaymentInfo.TextMatrix(1, 0, "Reference");
				vsPaymentInfo.TextMatrix(2, 0, "Control 1");
				vsPaymentInfo.TextMatrix(3, 0, "Control 2");
				vsPaymentInfo.TextMatrix(4, 0, "Control 3");
				vsPaymentInfo.TextMatrix(5, 0, "");
				vsPaymentInfo.TextMatrix(6, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD1);
				vsPaymentInfo.TextMatrix(7, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD2);
				vsPaymentInfo.TextMatrix(8, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD3);
				vsPaymentInfo.TextMatrix(9, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD4);
				vsPaymentInfo.TextMatrix(10, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD5);
				vsPaymentInfo.TextMatrix(11, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD6);
				vsPaymentInfo.TextMatrix(12, 0, "Total");
				// second col
				vsPaymentInfo.TextMatrix(0, 1, modUseCR.Statics.ReceiptArray[lngIndex].Type + " - " + modUseCR.Statics.ReceiptArray[lngIndex].TypeDescription);
				vsPaymentInfo.TextMatrix(1, 1, modUseCR.Statics.ReceiptArray[lngIndex].Reference);
				vsPaymentInfo.TextMatrix(2, 1, modUseCR.Statics.ReceiptArray[lngIndex].Control1);
				vsPaymentInfo.TextMatrix(3, 1, modUseCR.Statics.ReceiptArray[lngIndex].Control2);
				vsPaymentInfo.TextMatrix(4, 1, modUseCR.Statics.ReceiptArray[lngIndex].Control3);
				vsPaymentInfo.TextMatrix(5, 1, "");
				vsPaymentInfo.TextMatrix(6, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee1, "#,##0.00"));
				vsPaymentInfo.TextMatrix(7, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee2, "#,##0.00"));
				vsPaymentInfo.TextMatrix(8, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee3, "#,##0.00"));
				vsPaymentInfo.TextMatrix(9, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee4, "#,##0.00"));
				vsPaymentInfo.TextMatrix(10, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee5, "#,##0.00"));
				vsPaymentInfo.TextMatrix(11, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee6, "#,##0.00"));
				vsPaymentInfo.TextMatrix(12, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Total, "#,##0.00"));
				// fourth col
				vsPaymentInfo.TextMatrix(0, 3, "Name");
				vsPaymentInfo.TextMatrix(1, 3, "Date");
				vsPaymentInfo.TextMatrix(2, 3, "Module");
				vsPaymentInfo.TextMatrix(3, 3, "Alternative Cash Account");
				vsPaymentInfo.TextMatrix(4, 3, "Default Cash Account");
				vsPaymentInfo.TextMatrix(5, 3, "Manual Input Account");
				vsPaymentInfo.TextMatrix(6, 3, "Collection Code");
				vsPaymentInfo.TextMatrix(7, 3, "Quantity");
				vsPaymentInfo.TextMatrix(8, 3, "Show Detail");
				vsPaymentInfo.TextMatrix(9, 3, "Comment");
				vsPaymentInfo.TextMatrix(10, 3, "Record ID");
				vsPaymentInfo.TextMatrix(11, 3, "Affect Cash");
				vsPaymentInfo.TextMatrix(12, 3, "Affect Cash Drawer");
				vsPaymentInfo.TextMatrix(13, 3, "Teller ID");
				// fifth col
				vsPaymentInfo.TextMatrix(0, 4, modUseCR.Statics.ReceiptArray[lngIndex].Name);
				vsPaymentInfo.TextMatrix(1, 4, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Date, "MM/dd/yyyy"));
				vsPaymentInfo.TextMatrix(2, 4, modUseCR.Statics.ReceiptArray[lngIndex].Module);
				vsPaymentInfo.TextMatrix(3, 4, modUseCR.Statics.ReceiptArray[lngIndex].CashAlternativeAcct);
				vsPaymentInfo.TextMatrix(4, 4, modUseCR.Statics.ReceiptArray[lngIndex].DefaultCashAccount);
				vsPaymentInfo.TextMatrix(5, 4, modUseCR.Statics.ReceiptArray[lngIndex].DefaultMIAccount);
				vsPaymentInfo.TextMatrix(6, 4, modUseCR.Statics.ReceiptArray[lngIndex].CollectionCode);
				vsPaymentInfo.TextMatrix(7, 4, FCConvert.ToString(modUseCR.Statics.ReceiptArray[lngIndex].Quantity));
				if (modUseCR.Statics.ReceiptArray[lngIndex].ShowDetail)
				{
					vsPaymentInfo.TextMatrix(8, 4, "True");
				}
				else
				{
					vsPaymentInfo.TextMatrix(8, 4, "False");
				}
				vsPaymentInfo.TextMatrix(9, 4, modUseCR.Statics.ReceiptArray[lngIndex].Comment);
				vsPaymentInfo.TextMatrix(10, 4, FCConvert.ToString(modUseCR.Statics.ReceiptArray[lngIndex].RecordKey));
				if (modUseCR.Statics.ReceiptArray[lngIndex].AffectCash)
				{
					vsPaymentInfo.TextMatrix(11, 4, "True");
				}
				else
				{
					vsPaymentInfo.TextMatrix(11, 4, "False");
				}
				if (modUseCR.Statics.ReceiptArray[lngIndex].AffectCashDrawer)
				{
					vsPaymentInfo.TextMatrix(12, 4, "True");
				}
				else
				{
					vsPaymentInfo.TextMatrix(12, 4, "False");
				}
				vsPaymentInfo.TextMatrix(13, 4, strTellerID);
				// fields not shown
				// Used                As Boolean      'if True then I know that this array index is currently being used
				// Account             As String
				// PaidBy              As String
				// Copies              As Integer
				// ArrayIndex          As Integer
				// BillingYear         As Integer
				// Split               As Integer
				// PrintReceipt        As Boolean      'this will tell if the user wants to print this payment...if there are multiple payments, then this will still print, but if all are turned off, then none will print
				// format the grid
				// With vsPaymentInfo
				// .rows = 0          'this will clear the grid
				// .rows = 7
				// .Cols = 6
				// 
				// .ExtendLastCol = True
				// .ScrollBars = flexScrollBarNone
				// 
				// .ColWidth(0) = 0.24 * .Width
				// .ColWidth(1) = 0.24 * .Width
				// .ColWidth(2) = 0.032 * .Width
				// .ColWidth(3) = 0.24 * .Width
				// .ColWidth(4) = 0.24 * .Width
				// .ColWidth(5) = 0.1 * .Width
				// 
				// fill the titles
				// .TextMatrix(0, 0) = "Type"
				// .TextMatrix(1, 0) = "Reference"
				// .TextMatrix(2, 0) = "Control 1"
				// .TextMatrix(3, 0) = "Control 2"
				// .TextMatrix(4, 0) = "Control 3"
				// .TextMatrix(5, 0) = "Date"
				// 
				// fill the data in the first column
				// .TextMatrix(0, 0) = ReceiptArray(lngIndex).TypeDescription
				// .TextMatrix(1, 0) = ReceiptArray(lngIndex).Reference
				// .TextMatrix(2, 0) = ReceiptArray(lngIndex).Control1
				// .TextMatrix(3, 0) = ReceiptArray(lngIndex).Control2
				// .TextMatrix(4, 0) = ReceiptArray(lngIndex).Control3
				// .TextMatrix(5, 0) = ReceiptArray(lngIndex).Date
				// 
				// second column
				// .TextMatrix(0, 3) = "Account"
				// .TextMatrix(0, 4) = "Desc"
				// .TextMatrix(0, 5) = "Fees"
				// .MergeCells = flexMergeFree
				// 
				// .TextMatrix(1, 4) = ReceiptArray(lngIndex).FeeD1
				// .TextMatrix(2, 4) = ReceiptArray(lngIndex).FeeD2
				// .TextMatrix(3, 4) = ReceiptArray(lngIndex).FeeD3
				// .TextMatrix(4, 4) = ReceiptArray(lngIndex).FeeD4
				// .TextMatrix(5, 4) = ReceiptArray(lngIndex).FeeD5
				// .TextMatrix(6, 4) = ReceiptArray(lngIndex).FeeD6
				// 
				// rsType.OpenRecordset "SELECT * FROM Type WHERE TypeCode = " & ReceiptArray(lngIndex).Type
				// If Not rsType.EndOfFile Then
				// 
				// .TextMatrix(1, 3) = rsType.Fields("Account1")
				// .TextMatrix(2, 3) = rsType.Fields("Account2")
				// .TextMatrix(3, 3) = rsType.Fields("Account3")
				// .TextMatrix(4, 3) = rsType.Fields("Account4")
				// .TextMatrix(5, 3) = rsType.Fields("Account5")
				// .TextMatrix(6, 3) = rsType.Fields("Account6")
				// 
				// if I find the type, then fill in the adjusted titles
				// .TextMatrix(1, 0) = rsType.Fields("Reference")
				// .TextMatrix(2, 0) = rsType.Fields("Control1")
				// .TextMatrix(3, 0) = rsType.Fields("Control2")
				// .TextMatrix(4, 0) = rsType.Fields("Control3")
				// If rsType.Fields("ShowFeeDetailOnReceipt") Then
				// gboolDetail = True
				// End If
				// Else
				// .TextMatrix(1, 3) = ""
				// .TextMatrix(2, 3) = ""
				// .TextMatrix(3, 3) = ""
				// .TextMatrix(4, 3) = ""
				// .TextMatrix(4, 3) = ""
				// .TextMatrix(4, 3) = ""
				// End If
				// 
				// .TextMatrix(1, 5) = Format(ReceiptArray(lngIndex).Fee1, "#,##0.00")
				// .TextMatrix(2, 5) = Format(ReceiptArray(lngIndex).Fee2, "#,##0.00")
				// .TextMatrix(3, 5) = Format(ReceiptArray(lngIndex).Fee3, "#,##0.00")
				// .TextMatrix(4, 5) = Format(ReceiptArray(lngIndex).Fee4, "#,##0.00")
				// .TextMatrix(5, 5) = Format(ReceiptArray(lngIndex).Fee5, "#,##0.00")
				// .TextMatrix(6, 5) = Format(ReceiptArray(lngIndex).Fee6, "#,##0.00")
				// End With
				// show the frame
				//vsPaymentInfo.Top = 30;
				vsPaymentInfo.Height = (vsPaymentInfo.Rows * 40) + 70;
				fraPaymentInfo.Height = vsPaymentInfo.Height + 90;
				// FC: FINAL: RPU: #i514: Set the position of the form directly to center of container
				//fraPaymentInfo.Left = FCConvert.ToInt32((frmReprint.InstancePtr.Width - fraPaymentInfo.Width) / 2.0);
				//fraPaymentInfo.Top = FCConvert.ToInt32((frmReprint.InstancePtr.Height - fraPaymentInfo.Height) / 3.0);
				fraPaymentInfo.CenterToContainer(this.ClientArea);
				fraPaymentInfo.Visible = true;
			}
			else
			{
				// hide the frame
				fraPaymentInfo.Visible = false;
			}
		}

		private void FormatPaymentInfoGrid_2(bool boolReset)
		{
			FormatPaymentInfoGrid(ref boolReset);
		}

		private void FormatPaymentInfoGrid(ref bool boolReset)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngWid = 0;
				lngWid = vsPaymentInfo.WidthOriginal;
				vsPaymentInfo.Cols = 5;
				if (boolReset)
				{
					// this will clear all the cells
					vsPaymentInfo.Rows = 0;
					vsPaymentInfo.Rows = 14;
				}
				// resize the columns
				vsPaymentInfo.ColWidth(0, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColWidth(1, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColWidth(2, FCConvert.ToInt32(lngWid * 0.032));
				vsPaymentInfo.ColWidth(3, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColWidth(4, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsPaymentInfo.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				//vsPaymentInfo.Height = (vsPaymentInfo.Rows * vsPaymentInfo.RowHeight(0)) + 90;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formating Payment Info Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetCustomFormColors()
		{
			lblEFT.ForeColor = Color.Red;
		}
	}
}
