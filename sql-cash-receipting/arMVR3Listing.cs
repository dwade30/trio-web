﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for arMVR3Listing.
	/// </summary>
	public partial class arMVR3Listing : BaseSectionReport
	{
		public static arMVR3Listing InstancePtr
		{
			get
			{
				return (arMVR3Listing)Sys.GetInstance(typeof(arMVR3Listing));
			}
		}

		protected arMVR3Listing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            rsData.DisposeOf();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arMVR3Listing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/07/2003              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		double dblTotalAmt;

		public arMVR3Listing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "MVR3 Listing";
            this.ReportEnd += ArMVR3Listing_ReportEnd;
		}

        private void ArMVR3Listing_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			lblDateNow.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			// data1.DatabaseName "TWCR0000.vb1"
			// need to check types somehow...
			// this wants to check the actual system date '02/10/2005
			strSQL = "SELECT * FROM Archive INNER JOIN Receipt ON Archive.ReceiptNumber = Receipt.ReceiptKey WHERE ReceiptType = 99 AND ActualSystemDate >= '" + FCConvert.ToString(frmMVR3Listing.InstancePtr.StartNumber) + "' AND ActualSystemDate <= '" + FCConvert.ToString(frmMVR3Listing.InstancePtr.EndNumber) + "' ORDER BY Control3, ActualSystemDate, Archive.TellerID, Name";
			// strSQL = "SELECT * FROM Archive INNER JOIN Receipt ON Archive.ReceiptNumber = Receipt.ReceiptKey WHERE ReceiptType = 99 AND Archive.Date >= #" & frmMVR3Listing.StartNumber & "# AND Archive.Date <= #" & frmMVR3Listing.EndNumber & "# ORDER BY Control3, Archive.Date, Archive.TellerID, Name"
            if (rsData == null) rsData = new clsDRWrapper();
			rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			if (rsData.EndOfFile() != true)
			{
				fldName.Text = rsData.Get_Fields_String("Name");
				fldMVR3.Text = rsData.Get_Fields_String("Control3");
				// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				fldExcise.Text = Strings.Format(rsData.Get_Fields("Amount1"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				dblTotalAmt += FCConvert.ToDouble(rsData.Get_Fields("Amount1"));
				fldPlate.Text = rsData.Get_Fields_String("Ref");
				// TODO Get_Fields: Field [Archive.TellerID] not found!! (maybe it is an alias?)
				fldTeller.Text = FCConvert.ToString(rsData.Get_Fields("Archive.TellerID"));
				fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ArchiveDate"), "MM/dd/yyyy");
				// TODO Get_Fields: Field [Receipt.ReceiptNumber] not found!! (maybe it is an alias?)
				fldReceipt.Text = FCConvert.ToString(rsData.Get_Fields("Receipt.ReceiptNumber"));
				lngCount += 1;
				rsData.MoveNext();
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lngCount != 1)
			{
				lblFooterTotal.Text = "Total for " + FCConvert.ToString(lngCount) + " transactions.";
			}
			else
			{
				lblFooterTotal.Text = "Total for " + FCConvert.ToString(lngCount) + " transaction.";
			}
			lblFooterExcise.Text = Strings.Format(dblTotalAmt, "#,##0.00");
		}

		private void arMVR3Listing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arMVR3Listing.Caption	= "MVR3 Listing";
			//arMVR3Listing.Icon	= "arMVR3Listing.dsx":0000";
			//arMVR3Listing.Left	= 0;
			//arMVR3Listing.Top	= 0;
			//arMVR3Listing.Width	= 11880;
			//arMVR3Listing.Height	= 8595;
			//arMVR3Listing.StartUpPosition	= 3;
			//arMVR3Listing.SectionData	= "arMVR3Listing.dsx":058A;
			//End Unmaped Properties
		}
	}
}
