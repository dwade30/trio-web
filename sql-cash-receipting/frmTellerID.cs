﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmTellerID.
	/// </summary>
	public partial class frmTellerID : BaseForm
	{
		public frmTellerID()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTellerID InstancePtr
		{
			get
			{
				return (frmTellerID)Sys.GetInstance(typeof(frmTellerID));
			}
		}

		protected frmTellerID _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               David Wade              *
		// DATE           :               04/03/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/03/2003              *
		// ********************************************************
		string OpID = "";
		clsDRWrapper rsAgent = new clsDRWrapper();
		string strOpID = "";
		string strAgent = "";

		public string Init()
		{
			string Init = "";
			// this function will return the operator ID
			rsAgent.OpenRecordset("SELECT * FROM Operators WHERE Level = '1' OR Level = '2'", "SystemSettings");
			if (rsAgent.EndOfFile() != true && rsAgent.BeginningOfFile() != true)
			{
			}
			lblInstuctions.Text = "Please enter the Primary Agent's Operator ID";
			this.Show(FormShowEnum.Modal, App.MainForm);
			Init = OpID;
			return Init;
		}

		private void frmTellerID_Activated(object sender, System.EventArgs e)
		{
			if (txtOpId.Visible && txtOpId.Enabled)
			{
				txtOpId.Focus();
				txtOpId.SelectionStart = 1;
				txtOpId.SelectionLength = txtOpId.Text.Length;
			}
		}

		private void frmTellerID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			bool boolFound = false;
			OpID = Strings.Trim(strOpID);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				OpID = "";
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (OpID.Length != 3)
				{
					MessageBox.Show("Operator ID must be 3 characters in length.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					OpID = "";
					txtOpId.Text = "";
					txtOpId.Focus();
				}
				else
				{
					boolFound = false;
					if (rsAgent.RecordCount() > 0)
					{
						rsAgent.MoveFirst();
					}
					while (!(rsAgent.EndOfFile() || boolFound))
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (Strings.UCase(OpID) == Strings.UCase(FCConvert.ToString(rsAgent.Get_Fields("Code"))))
						{
							boolFound = true;
						}
						rsAgent.MoveNext();
					}
					if (!boolFound)
					{
						MessageBox.Show("This operator does not have permissions to close out and Motor Vehicle period.", "Invalid Agent Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						strOpID = "";
						OpID = "";
						txtOpId.Text = "";
						txtOpId.Focus();
						return;
					}
					else
					{
						Close();
					}
				}
				strOpID = "";
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTellerID_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTellerID.ScaleWidth	= 2895;
			//frmTellerID.ScaleHeight	= 1275;
			//frmTellerID.LinkTopic	= "Form1";
			//frmTellerID.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this, false);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
		}

		private void txtOpId_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			else if (KeyAscii == Keys.Back)
			{
				if (strOpID.Length > 1)
				{
					strOpID = Strings.Mid(strOpID, 1, strOpID.Length - 1);
				}
				else if (strOpID.Length == 1)
				{
					strOpID = "";
				}
			}
			else
			{
				if (strOpID.Length >= 3)
				{
					// Do Nothing
				}
				else
				{
					strOpID += FCConvert.ToString(Convert.ToChar(KeyAscii));
					KeyAscii = Keys.Separator;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
