﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheck.
	/// </summary>
	partial class rptPaymentCrossCheck
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPaymentCrossCheck));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateNow = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.sarCLCross = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarPPCross = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarUTCross = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarARCross = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarCRCross = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarCRCrossUT = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.PageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateNow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.AcrossDown;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.sarCLCross,
				this.sarPPCross,
				this.sarUTCross,
				this.sarARCross,
				this.sarCRCross,
				this.sarCRCrossUT,
				this.PageBreak1
			});
			this.Detail.Height = 0.6875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDateNow,
				this.lblMuniName,
				this.lblTime,
				this.lblPage,
				this.lblHeader2
			});
			this.PageHeader.Height = 0.5F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Payment Cross Check";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblDateNow
			// 
			this.lblDateNow.Height = 0.1875F;
			this.lblDateNow.HyperLink = null;
			this.lblDateNow.Left = 5.875F;
			this.lblDateNow.Name = "lblDateNow";
			this.lblDateNow.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblDateNow.Text = null;
			this.lblDateNow.Top = 0F;
			this.lblDateNow.Width = 1.625F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.6875F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.625F;
			// 
			// lblHeader2
			// 
			this.lblHeader2.Height = 0.1875F;
			this.lblHeader2.HyperLink = null;
			this.lblHeader2.Left = 0F;
			this.lblHeader2.Name = "lblHeader2";
			this.lblHeader2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblHeader2.Text = null;
			this.lblHeader2.Top = 0.3125F;
			this.lblHeader2.Width = 7.5F;
			// 
			// sarCLCross
			// 
			this.sarCLCross.CloseBorder = false;
			this.sarCLCross.Height = 0.0625F;
			this.sarCLCross.Left = 0F;
			this.sarCLCross.Name = "sarCLCross";
			this.sarCLCross.Report = null;
			this.sarCLCross.Top = 0F;
			this.sarCLCross.Visible = false;
			this.sarCLCross.Width = 7.5F;
			// 
			// sarPPCross
			// 
			this.sarPPCross.CloseBorder = false;
			this.sarPPCross.Height = 0.0625F;
			this.sarPPCross.Left = 0F;
			this.sarPPCross.Name = "sarPPCross";
			this.sarPPCross.Report = null;
			this.sarPPCross.Top = 0.125F;
			this.sarPPCross.Visible = false;
			this.sarPPCross.Width = 7.5F;
			// 
			// sarUTCross
			// 
			this.sarUTCross.CloseBorder = false;
			this.sarUTCross.Height = 0.0625F;
			this.sarUTCross.Left = 0F;
			this.sarUTCross.Name = "sarUTCross";
			this.sarUTCross.Report = null;
			this.sarUTCross.Top = 0.25F;
			this.sarUTCross.Visible = false;
			this.sarUTCross.Width = 7.5F;
			// 
			// sarARCross
			// 
			this.sarARCross.CloseBorder = false;
			this.sarARCross.Height = 0.0625F;
			this.sarARCross.Left = 0F;
			this.sarARCross.Name = "sarARCross";
			this.sarARCross.Report = null;
			this.sarARCross.Top = 0.375F;
			this.sarARCross.Visible = false;
			this.sarARCross.Width = 7.5F;
			// 
			// sarCRCross
			// 
			this.sarCRCross.CloseBorder = false;
			this.sarCRCross.Height = 0.0625F;
			this.sarCRCross.Left = 0F;
			this.sarCRCross.Name = "sarCRCross";
			this.sarCRCross.Report = null;
			this.sarCRCross.Top = 0.5F;
			this.sarCRCross.Visible = false;
			this.sarCRCross.Width = 7.5F;
			// 
			// sarCRCrossUT
			// 
			this.sarCRCrossUT.CloseBorder = false;
			this.sarCRCrossUT.Height = 0.0625F;
			this.sarCRCrossUT.Left = 0F;
			this.sarCRCrossUT.Name = "sarCRCrossUT";
			this.sarCRCrossUT.Report = null;
			this.sarCRCrossUT.Top = 0.625F;
			this.sarCRCrossUT.Visible = false;
			this.sarCRCrossUT.Width = 7.5F;
			// 
			// PageBreak1
			// 
			this.PageBreak1.Height = 0.05555556F;
			this.PageBreak1.Left = 0F;
			this.PageBreak1.Name = "PageBreak1";
			this.PageBreak1.Size = new System.Drawing.SizeF(7.5F, 0.05555556F);
			this.PageBreak1.Top = 0.5F;
			this.PageBreak1.Width = 7.5F;
			// 
			// rptPaymentCrossCheck
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateNow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarCLCross;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarPPCross;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarUTCross;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarARCross;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarCRCross;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarCRCrossUT;
		public GrapeCity.ActiveReports.SectionReportModel.PageBreak PageBreak1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateNow;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		public GrapeCity.ActiveReports.SectionReportModel.Label lblHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
