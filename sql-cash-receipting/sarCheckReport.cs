﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarCheckReport.
	/// </summary>
	public partial class sarCheckReport : FCSectionReport
	{
		public static sarCheckReport InstancePtr
		{
			get
			{
				return (sarCheckReport)Sys.GetInstance(typeof(sarCheckReport));
			}
		}

		protected sarCheckReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarCheckReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/01/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int intCallCount;
		// MAL@20071023: Counts number of time detail report is called
		string strSQL;
		private bool boolUsePrevYears = false;

		public sarCheckReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarCheckReport_ReportEnd;
		}

        private void SarCheckReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				switch (modGlobal.Statics.gintCheckReportOrder)
				{
					case 0:
					case 1:
						{
							// Don't Check Call Count
							break;
						}
					case 2:
						{
							if (intCallCount > 0)
							{
								return;
							}
							else
							{
								// Do Nothing
							}
							break;
						}
				}
				//end switch
				// TODO Get_Fields: Field [BankKey] not found!! (maybe it is an alias?)
				sarCheckReportDetailOB.Report.UserData = rsData.Get_Fields("BankKey");
				intCallCount += 1;
				// SetData
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strMaster;
			string strArchive;

			boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
			if (boolUsePrevYears)
			{
				strMaster = "LastYearCheckMaster";
				strArchive = "LastYearArchive";
			}
			else
			{
				strMaster = "CheckMaster";
				strArchive = "Archive";
			}

			// This sql statement will find the totals for each bank and display them at the bottom of each detail
			strSQL = "SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL;
			// kk   rsData.OpenRecordset "SELECT * FROM Bank WHERE ID = 0"
			strSQL = "SELECT * FROM Bank INNER JOIN (SELECT sum(ckmast.Amount) as sumAmt, Bank.ID as BankKey FROM (" + strMaster + " ckmast INNER JOIN Bank ON ckmast.BankNumber = Bank.ID) INNER JOIN (" + strSQL + ") as NonClosedOutReceiptNumbers ON ckmast.ReceiptNumber = NonClosedOutReceiptNumbers.ReceiptNumber WHERE ISNULL(EFT,0) = 0 GROUP BY Bank.ID ) AS BK ON Bank.ID = BK.Bankkey ORDER BY Name";
			sarCheckReportDetailOB.Report = new sarCheckReportDetail();
			rsData.OpenRecordset(strSQL);
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [BankKey] not found!! (maybe it is an alias?)
				sarCheckReportDetailOB.Report.UserData = rsData.Get_Fields("BankKey");
			}
			else
			{
				// this will leave a blank page
				// HideGroupFooter
				ShowFakeHeaders();
			}
		}

		private void GroupFooter1_AfterPrint(object sender, EventArgs e)
		{
            if (modGlobal.Statics.gblnPrintCCReport)
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 5;
			}
			else if (modGlobal.Statics.gboolTellerReportWithAudit)
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 3;
			}
			else
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 4;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (arDailyAuditReport.InstancePtr.fldTotalCash.Text != "")
			{
				fldTotalCash.Text = arDailyAuditReport.InstancePtr.fldTotalCash.Text;
			}
			else
			{
				fldTotalCash.Text = "0.00";
			}
			if (arDailyAuditReport.InstancePtr.fldTotalCheck.Text != "")
			{
				fldTotalCheck.Text = arDailyAuditReport.InstancePtr.fldTotalCheck.Text;
			}
			else
			{
				fldTotalCheck.Text = "0.00";
			}
			if (arDailyAuditReport.InstancePtr.fldTotalCredit.Text != "")
			{
				fldTotalCredit.Text = arDailyAuditReport.InstancePtr.fldTotalCredit.Text;
			}
			else
			{
				fldTotalCredit.Text = "0.00";
			}
			if (arDailyAuditReport.InstancePtr.fldTotalOther.Text != "")
			{
				fldTotalOther.Text = arDailyAuditReport.InstancePtr.fldTotalOther.Text;
			}
			else
			{
				fldTotalOther.Text = "0.00";
			}
			fldTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalCash.Text) + FCConvert.ToDouble(fldTotalCheck.Text) + FCConvert.ToDouble(fldTotalCredit.Text) + FCConvert.ToDouble(fldTotalOther.Text), "#,##0.00");
		}

		public void HideGroupFooter()
		{
			// fldTotal.Visible = False
			// fldTotalCash.Visible = False
			// fldTotalCheck.Visible = False
			// fldTotalCredit.Visible = False
			// fldTotalOther.Visible = False
			// lblTotal.Visible = False
			// lblTotalCash.Visible = False
			// lblTotalCheck.Visible = False
			// lbltotalCredit.Visible = False
			// lblTotalOther.Visible = False
			// Line1.Visible = False
			// Me.GroupFooter1.Height = 0
		}

		private void ShowFakeHeaders()
		{
			// this routine will show a display of zero checks and zero check amount
			Line2.Visible = true;
			lblBankNumber.Visible = true;
			lblBankNumberTotal.Visible = true;
			lblCheckNumber.Visible = true;
			lblReceiptNumber.Visible = true;
			lblAmount.Visible = true;
			fldBankTotal.Visible = true;
		}

		private void sarCheckReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarCheckReport.Caption	= "Check Report";
			//sarCheckReport.Icon	= "sarCheckReport.dsx":0000";
			//sarCheckReport.Left	= 0;
			//sarCheckReport.Top	= 0;
			//sarCheckReport.Width	= 11880;
			//sarCheckReport.Height	= 8580;
			//sarCheckReport.StartUpPosition	= 3;
			//sarCheckReport.SectionData	= "sarCheckReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
