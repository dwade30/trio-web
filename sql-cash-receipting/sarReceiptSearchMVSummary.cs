﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Extensions;
using TWCR0000.Reports;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarReceiptSearchMVSummary.
	/// </summary>
	public partial class sarReceiptSearchMVSummary : FCSectionReport
	{
		private bool firstRecord = true;
		private int counter = 0;
		private List<ReceiptSearchMotorVehicleSummaryInformation> reportData = new List<ReceiptSearchMotorVehicleSummaryInformation>();

		public sarReceiptSearchMVSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				if (firstRecord)
				{
					firstRecord = false;
					counter = 0;
				}
				else
				{
					counter++;
				}

				if (counter < reportData.Count())
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fetch Data Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				reportData =
					((arReceiptSearch)ParentReport).reportService.SummarizeMotorVehicleData(((arReceiptSearch)ParentReport)
						.reportData).ToList();

				firstRecord = true;
				lblHeader.Text = "Registration Type";
				frmWait.InstancePtr.IncrementProgress();

				if (reportData.Count() == 0)
				{
					((arReceiptSearch)ParentReport).NoRecords();
				}
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldAmount.Text = reportData[counter].Amount.ToString("#,##0.00");
			fldCount.Text = reportData[counter].Count.ToString("#,##0");

			var searchType = reportData[counter].RegistrationType.Left(3);

			switch (searchType)
			{
				case "DPR":
					fldHeader.Text = searchType + " - " + "Duplicate Registration";
					break;
				case "DPS":
					fldHeader.Text = searchType + " - " + "Duplicate Sticker";
					break;
				case "LPS":
					fldHeader.Text = searchType + " - " + "Lost Plate or Sticker";
					break;
				case "ECR":
				case "ECO":
					fldHeader.Text = searchType + " - " + "E-Correct";
					break;
				case "GVW":
					fldHeader.Text = searchType + " - " + "Gross Vehichle Weight Change";
					break;
				case "NRR":
					fldHeader.Text = searchType + " - " + "New Registration Regular";
					break;
				case "NRT":
					fldHeader.Text = searchType + " - " + "New Registration Transfer";
					break;
				case "RRR":
					fldHeader.Text = searchType + " - " + "Reregistration Regular";
					break;
				case "RRT":
					fldHeader.Text = searchType + " - " + "Reregistration Transfer";
					break;
				default:
					fldHeader.Text = searchType + " - Other";
					break;
			}

			if (((arReceiptSearch)ParentReport).reportService.ReportSetup.SummaryDetailLevel == SummaryDetailOption.Detail)
			{
				var subReportData = ((arReceiptSearch)ParentReport).reportService
					.GetMotorVehicleSummaryDetailData(reportData[counter], ((arReceiptSearch)ParentReport).reportData).ToList();
				if (subReportData.Count > 0)
				{
					sarSummaryDetail.Report = new sarReceiptSearchSummaryDetail(subReportData);
					sarSummaryDetail.Visible = true;
				}
				else
				{
					sarSummaryDetail.Report = null;
					sarSummaryDetail.Visible = false;
				}
			}
			else
			{
				sarSummaryDetail.Report = null;
				sarSummaryDetail.Visible = false;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalCount.Text = reportData.Select(x => x.Count).DefaultIfEmpty(0).Sum().ToString("#,##0");
			fldTotalAmount.Text = reportData.Select(x => x.Amount).DefaultIfEmpty(0).Sum().ToString("#,##0.00");
		}
	}
}
