﻿function AttachToIframe(handle,parentId) {
    var webbrowser = Wisej.Core.getComponent("id_" + handle);
    if (webbrowser) {     
        var iframe = webbrowser.getChildControl("iframe");
        if (iframe) {
            var domElement = iframe.getContentElement().getDomElement();
            if (domElement) {
                var data = { browserId: handle, parentPageId: parentId };
                domElement.contentWindow.postMessage(data,"*");
            }
        }
    }
}

window.addEventListener("message", receiveMessageFCWebBrowser, false);
function receiveMessageFCWebBrowser(event) {
    //if (!event.origin.startsWith("http://localhost"))  // put valid clients here; not secure and can be changed by bad guys
    //    return;
    try {
        //Wisej.Core.getComponent("id_" + event.data.wisejID).fireDataEvent('SendMessage', event.data.value);
        Wisej.Core.getComponent("id_" + event.data.parentPageId).fireDataEvent('SendMessage', event.data.value);
    } catch (e) {
        console.log(e);
    } 

}


function InjectElement(handle, elementId, value) {
    var webbrowser = Wisej.Core.getComponent("id_" + handle);
    if (webbrowser) {
        var iframe = webbrowser.getChildControl("iframe");
        if (iframe) {
            var domElement = iframe.getContentElement().getDomElement();
            if (domElement) {
                var tElem = domElement.getElementById(elementId);
                if (tElem) {
                    tElem.setAttribute("value", value);
                }
                else {
                    domElement.createElement(elementId);
                    // tElem.setAttribute("type", "hidden");
                    tElem.Name = elementId;
                    tElem.setAttribute("value", value);
                    domElement.appendChild(tElem);
                }
            }
        }
    }
}