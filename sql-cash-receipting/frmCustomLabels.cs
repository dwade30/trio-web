﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	public partial class frmCustomLabels : BaseForm
	{
		public frmCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomLabels InstancePtr
		{
			get
			{
				return (frmCustomLabels)Sys.GetInstance(typeof(frmCustomLabels));
			}
		}

		protected frmCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public void Init(string str1, short intType)
		{
		}

		private void frmCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomLabels.ScaleWidth	= 4680;
			//frmCustomLabels.ScaleHeight	= 3090;
			//frmCustomLabels.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
