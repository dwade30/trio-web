﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptBankSummary.
	/// </summary>
	public partial class rptBankSummary : BaseSectionReport
	{
		public static rptBankSummary InstancePtr
		{
			get
			{
				return (rptBankSummary)Sys.GetInstance(typeof(rptBankSummary));
			}
		}

		protected rptBankSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			rsBank.Dispose();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBankSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/12/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/20/2003              *
		// ********************************************************
		clsDRWrapper rsBank = new clsDRWrapper();

		public rptBankSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bank Summary";
            this.ReportEnd += RptBankSummary_ReportEnd;
		}

        private void RptBankSummary_ReportEnd(object sender, EventArgs e)
        {
            rsBank.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsBank.EndOfFile();
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			rsBank.OpenRecordset("SELECT * FROM Bank ORDER BY Name", modExtraModules.strCRDatabase);
			if (rsBank.EndOfFile())
			{
				// show a message on the report if there are no banks in the table
				lblName.Left = 0;
				lblName.Width = this.PrintWidth;
				lblName.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
				lblName.Text = "There are no banks setup in this system.  To add a bank, go to 'Bank Maintenance'.";
				lblRT.Visible = false;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will show the next bank information
			if (rsBank.EndOfFile() != true)
			{
				fldName.Text = rsBank.Get_Fields_String("Name");
				fldRT.Text = rsBank.Get_Fields_String("RoutingTransit");
				rsBank.MoveNext();
			}
			else
			{
				fldName.Text = "";
				fldRT.Text = "";
			}
		}

		private void rptBankSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBankSummary.Caption	= "Bank Summary";
			//rptBankSummary.Icon	= "rptBankSummary.dsx":0000";
			//rptBankSummary.Left	= 0;
			//rptBankSummary.Top	= 0;
			//rptBankSummary.Width	= 11880;
			//rptBankSummary.Height	= 8595;
			//rptBankSummary.StartUpPosition	= 3;
			//rptBankSummary.SectionData	= "rptBankSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
