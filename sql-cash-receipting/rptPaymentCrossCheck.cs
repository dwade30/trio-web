﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheck.
	/// </summary>
	public partial class rptPaymentCrossCheck : BaseSectionReport
	{
		public static rptPaymentCrossCheck InstancePtr
		{
			get
			{
				return (rptPaymentCrossCheck)Sys.GetInstance(typeof(rptPaymentCrossCheck));
			}
		}

		protected rptPaymentCrossCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPaymentCrossCheck	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/17/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/12/2006              *
		// ********************************************************
		bool boolDone;

		public rptPaymentCrossCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Payment Cross Check Master";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// This will allow one pass
			if (boolDone)
			{
				eArgs.EOF = boolDone;
			}
			else
			{
				boolDone = true;
			}
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDateNow.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			if (modGlobalConstants.Statics.gboolCL)
			{
				sarCLCross.Visible = true;
				sarCLCross.Report = new rptPaymentCrossCheckCL();
				sarPPCross.Visible = true;
				sarPPCross.Report = new rptPaymentCrossCheckPP();
			}
			if (modGlobalConstants.Statics.gboolUT)
			{
				sarUTCross.Visible = true;
				sarUTCross.Report = new rptPaymentCrossCheckUT();
			}
			// THERE'S NOTHING IN THE REF FIELD FROM AR TO USE FOR CROSS CHECK
			// If gboolAR Then
			// sarARCross.Visible = True
			// Set sarARCross = New rptPaymentCrossCheckAR
			// End If
			// MAL@20071005: Add check for missing CR records
			sarCRCross.Visible = true;
			sarCRCrossUT.Visible = true;
			sarCRCross.Report = new rptPaymentCrossCheckCR();
			sarCRCrossUT.Report = new rptPaymentCrossCheckCRUT();
			// Set sarCRCrossAR = New rptPaymentCrossCheckCRAR
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			clsDRWrapper rsU = new clsDRWrapper();
			rsU.OpenRecordset("SELECT * FROM CashRec");
			if (!rsU.EndOfFile())
			{
				rsU.Edit();
				rsU.Set_Fields("LastCrossCheck", DateTime.Today);
				rsU.Update();
			}
			rsU.DisposeOf();
		}

		private void rptPaymentCrossCheck_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPaymentCrossCheck.Caption	= "Payment Cross Check Master";
			//rptPaymentCrossCheck.Icon	= "rptPaymentCrossCheck.dsx":0000";
			//rptPaymentCrossCheck.Left	= 0;
			//rptPaymentCrossCheck.Top	= 0;
			//rptPaymentCrossCheck.Width	= 11880;
			//rptPaymentCrossCheck.Height	= 8595;
			//rptPaymentCrossCheck.StartUpPosition	= 3;
			//rptPaymentCrossCheck.SectionData	= "rptPaymentCrossCheck.dsx":058A;
			//End Unmaped Properties
		}
	}
}
