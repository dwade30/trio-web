﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	partial class frmCustomize : BaseForm
	{
		public fecherFoundation.FCLabel lblCheckReportOrder;
		public fecherFoundation.FCComboBox cmbCheckReportOrder;
		public fecherFoundation.FCLabel lblCCReportSort;
		public fecherFoundation.FCComboBox cmbCCReportSort;
		public fecherFoundation.FCLabel lblClearPaidBy;
		public fecherFoundation.FCComboBox cmbClearPaidBy;
		public fecherFoundation.FCComboBox cmbCrossCheckDate;
		public fecherFoundation.FCLabel lblOptions;
		public fecherFoundation.FCComboBox cmbOptions;
		public fecherFoundation.FCLabel lblSignatureNo;
		public fecherFoundation.FCComboBox cmbSignatureNo;
		public fecherFoundation.FCLabel lblReceiptWidth;
		public fecherFoundation.FCComboBox cmbReceiptWidth;
		public fecherFoundation.FCFrame fraEPayments;
		public fecherFoundation.FCFrame fraGatewayInfo_IME;
		public fecherFoundation.FCLabel lblSrvcCodes;
		public fecherFoundation.FCFrame fraGatewayInfo_ICL;
		public fecherFoundation.FCTextBox txtBillerGUID_ICL;
		public fecherFoundation.FCLabel lblICL_BillerGUID;
		public fecherFoundation.FCCheckBox chkSeperateCreditCardCashAccount;
		public fecherFoundation.FCFrame fraSeperateCreditCardCashAccount;
		public fecherFoundation.FCTextBox txtSeperateCreditCardCashAcct;
		public fecherFoundation.FCCheckBox chkEPymtTestMode;
		public fecherFoundation.FCLabel lblPortal;
		public fecherFoundation.FCLabel txtPymtPortal;
		public fecherFoundation.FCFrame fraAuditPassword;
		public fecherFoundation.FCCheckBox chkDeptCloseOut;
		public fecherFoundation.FCCheckBox chkPrintCCReport;
		public fecherFoundation.FCComboBox cmbCrossCheck;
		public fecherFoundation.FCCheckBox chkCrossCheck;
		public fecherFoundation.FCCheckBox chkPrintAuditAR;
		public fecherFoundation.FCCheckBox chkPrintAuditUT;
		public fecherFoundation.FCCheckBox chkPrintAuditRE;
		public fecherFoundation.FCCheckBox chkUSLExport;
		public fecherFoundation.FCCheckBox chkExclusive;
		public fecherFoundation.FCCheckBox chkDepositEntry;
		public fecherFoundation.FCCheckBox chkPartialAudit;
		public fecherFoundation.FCCheckBox chkCloseMV;
		public fecherFoundation.FCCheckBox chkDailyAuditByAlpha;
		public fecherFoundation.FCTextBox txtAuditPassword;
		public fecherFoundation.FCCheckBox chkAuditPassword;
		public fecherFoundation.FCFrame fraOther;
		public fecherFoundation.FCCheckBox chkRoutNumsInBnkLst;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtMVDelay;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCCheckBox chkSortTypeAlpha;
		public fecherFoundation.FCCheckBox chkClearPaidBy;
		public fecherFoundation.FCFrame fraCrossCheck;
		public Global.T2KDateBox txtCrossCheckDate;
		public fecherFoundation.FCCheckBox chkMVCheck;
		public fecherFoundation.FCCheckBox chkBreakdownCK;
		public fecherFoundation.FCFrame fraAlign;
		public fecherFoundation.FCTextBox txtAuditMarginTop;
		public fecherFoundation.FCLabel lblAuditMarginTop;
		public fecherFoundation.FCLabel lblLines;
		public fecherFoundation.FCFrame fraReset;
		public fecherFoundation.FCTextBox txtResetReceipt;
		public fecherFoundation.FCLabel lblResetReceipt;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCFrame fraCD;
		public fecherFoundation.FCComboBox cmbPrinterPort;
		public fecherFoundation.FCComboBox cmbRepeat;
		public fecherFoundation.FCCheckBox chkCDConnected;
		public fecherFoundation.FCTextBox txtCDCode;
		public fecherFoundation.FCLabel lblCDCode;
		public fecherFoundation.FCLabel lblCDRepeat;
		public fecherFoundation.FCLabel lblPortID;
		public fecherFoundation.FCFrame fraComment;
		public fecherFoundation.FCTextBox txtTopComment;
		public fecherFoundation.FCTextBox txtBottomComment;
		public fecherFoundation.FCLabel lblTopComment;
		public fecherFoundation.FCLabel lblBottom;
		public fecherFoundation.FCFrame fraWidth;
		public fecherFoundation.FCCheckBox chkReceiptPrinterPrompt;
		public fecherFoundation.FCCheckBox chkShowControlFields;
		public fecherFoundation.FCCheckBox chkThinReceipt;
		public fecherFoundation.FCCheckBox chkUppercaseTypes;
		public fecherFoundation.FCComboBox cmbFontSize;
		public fecherFoundation.FCCheckBox chkCompactReceipt;
		public fecherFoundation.FCCheckBox chkNoMargin;
		public fecherFoundation.FCComboBox cmbReceiptOffset;
		public fecherFoundation.FCCheckBox chkSP200;
		public fecherFoundation.FCComboBox cmbExtraLines;
		public fecherFoundation.FCComboBox cmbWideLines;
		public fecherFoundation.FCLabel lblFontSize;
		public fecherFoundation.FCLabel lblReceiptOffset;
		public fecherFoundation.FCLabel lblExtraLines;
		public fecherFoundation.FCLabel lblWideLines;
		public fecherFoundation.FCFrame fraTeller;
		public fecherFoundation.FCCheckBox chkTellerCheckReport;
		public fecherFoundation.FCCheckBox chkTellerNewPage;
		public fecherFoundation.FCComboBox cmbTellerUsed;
		public fecherFoundation.FCCheckBox chkTellerWithAudit;
		public fecherFoundation.FCCheckBox chkTellerSeparate;
		public fecherFoundation.FCCheckBox chkTellerReportSeq;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomize));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))), "Portrait");
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images1"))), "Landscape");
            this.cmbCheckReportOrder = new fecherFoundation.FCComboBox();
            this.lblCCReportSort = new fecherFoundation.FCLabel();
            this.cmbCCReportSort = new fecherFoundation.FCComboBox();
            this.lblClearPaidBy = new fecherFoundation.FCLabel();
            this.cmbClearPaidBy = new fecherFoundation.FCComboBox();
            this.cmbCrossCheckDate = new fecherFoundation.FCComboBox();
            this.lblOptions = new fecherFoundation.FCLabel();
            this.cmbOptions = new fecherFoundation.FCComboBox();
            this.lblSignatureNo = new fecherFoundation.FCLabel();
            this.cmbSignatureNo = new fecherFoundation.FCComboBox();
            this.cmbReceiptWidth = new fecherFoundation.FCComboBox();
            this.fraEPayments = new fecherFoundation.FCFrame();
            this.btnRemoveServiceCode = new fecherFoundation.FCButton();
            this.btnAddServiceCode = new fecherFoundation.FCButton();
            this.chkShowServiceCode = new fecherFoundation.FCCheckBox();
            this.fraSeperateCreditCardCashAccount = new fecherFoundation.FCFrame();
            this.txtSeperateCreditCardCashAcct = new fecherFoundation.FCTextBox();
            this.chkSeperateCreditCardCashAccount = new fecherFoundation.FCCheckBox();
            this.fraGatewayInfo_IME = new fecherFoundation.FCFrame();
            this.ServiceCodesGrid = new fecherFoundation.FCGrid();
            this.lblSrvcCodes = new fecherFoundation.FCLabel();
            this.fraGatewayInfo_ICL = new fecherFoundation.FCFrame();
            this.txtBillerGUID_ICL = new fecherFoundation.FCTextBox();
            this.lblICL_BillerGUID = new fecherFoundation.FCLabel();
            this.chkEPymtTestMode = new fecherFoundation.FCCheckBox();
            this.lblPortal = new fecherFoundation.FCLabel();
            this.txtPymtPortal = new fecherFoundation.FCLabel();
            this.fraAuditPassword = new fecherFoundation.FCFrame();
            this.chkDeptCloseOut = new fecherFoundation.FCCheckBox();
            this.chkPrintCCReport = new fecherFoundation.FCCheckBox();
            this.cmbCrossCheck = new fecherFoundation.FCComboBox();
            this.chkCrossCheck = new fecherFoundation.FCCheckBox();
            this.chkPrintAuditAR = new fecherFoundation.FCCheckBox();
            this.chkPrintAuditUT = new fecherFoundation.FCCheckBox();
            this.chkPrintAuditRE = new fecherFoundation.FCCheckBox();
            this.chkUSLExport = new fecherFoundation.FCCheckBox();
            this.chkExclusive = new fecherFoundation.FCCheckBox();
            this.chkDepositEntry = new fecherFoundation.FCCheckBox();
            this.chkPartialAudit = new fecherFoundation.FCCheckBox();
            this.chkCloseMV = new fecherFoundation.FCCheckBox();
            this.chkDailyAuditByAlpha = new fecherFoundation.FCCheckBox();
            this.txtAuditPassword = new fecherFoundation.FCTextBox();
            this.chkAuditPassword = new fecherFoundation.FCCheckBox();
            this.lblCheckReportOrder = new fecherFoundation.FCLabel();
            this.fraOther = new fecherFoundation.FCFrame();
            this.chkRoutNumsInBnkLst = new fecherFoundation.FCCheckBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtMVDelay = new fecherFoundation.FCTextBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.chkSortTypeAlpha = new fecherFoundation.FCCheckBox();
            this.chkClearPaidBy = new fecherFoundation.FCCheckBox();
            this.fraCrossCheck = new fecherFoundation.FCFrame();
            this.txtCrossCheckDate = new Global.T2KDateBox();
            this.chkMVCheck = new fecherFoundation.FCCheckBox();
            this.chkBreakdownCK = new fecherFoundation.FCCheckBox();
            this.fraAlign = new fecherFoundation.FCFrame();
            this.txtAuditMarginTop = new fecherFoundation.FCTextBox();
            this.lblAuditMarginTop = new fecherFoundation.FCLabel();
            this.lblLines = new fecherFoundation.FCLabel();
            this.fraReset = new fecherFoundation.FCFrame();
            this.txtResetReceipt = new fecherFoundation.FCTextBox();
            this.lblResetReceipt = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList();
            this.fraCD = new fecherFoundation.FCFrame();
            this.cmbPrinterPort = new fecherFoundation.FCComboBox();
            this.cmbRepeat = new fecherFoundation.FCComboBox();
            this.chkCDConnected = new fecherFoundation.FCCheckBox();
            this.txtCDCode = new fecherFoundation.FCTextBox();
            this.lblCDCode = new fecherFoundation.FCLabel();
            this.lblCDRepeat = new fecherFoundation.FCLabel();
            this.lblPortID = new fecherFoundation.FCLabel();
            this.fraComment = new fecherFoundation.FCFrame();
            this.txtTopComment = new fecherFoundation.FCTextBox();
            this.txtBottomComment = new fecherFoundation.FCTextBox();
            this.lblTopComment = new fecherFoundation.FCLabel();
            this.lblBottom = new fecherFoundation.FCLabel();
            this.fraWidth = new fecherFoundation.FCFrame();
            this.lblReceiptWidth = new fecherFoundation.FCLabel();
            this.chkReceiptPrinterPrompt = new fecherFoundation.FCCheckBox();
            this.chkShowControlFields = new fecherFoundation.FCCheckBox();
            this.chkThinReceipt = new fecherFoundation.FCCheckBox();
            this.chkUppercaseTypes = new fecherFoundation.FCCheckBox();
            this.cmbFontSize = new fecherFoundation.FCComboBox();
            this.chkCompactReceipt = new fecherFoundation.FCCheckBox();
            this.chkNoMargin = new fecherFoundation.FCCheckBox();
            this.cmbReceiptOffset = new fecherFoundation.FCComboBox();
            this.chkSP200 = new fecherFoundation.FCCheckBox();
            this.cmbExtraLines = new fecherFoundation.FCComboBox();
            this.cmbWideLines = new fecherFoundation.FCComboBox();
            this.lblFontSize = new fecherFoundation.FCLabel();
            this.lblReceiptOffset = new fecherFoundation.FCLabel();
            this.lblExtraLines = new fecherFoundation.FCLabel();
            this.lblWideLines = new fecherFoundation.FCLabel();
            this.fraTeller = new fecherFoundation.FCFrame();
            this.chkTellerCheckReport = new fecherFoundation.FCCheckBox();
            this.chkTellerNewPage = new fecherFoundation.FCCheckBox();
            this.cmbTellerUsed = new fecherFoundation.FCComboBox();
            this.chkTellerWithAudit = new fecherFoundation.FCCheckBox();
            this.chkTellerSeparate = new fecherFoundation.FCCheckBox();
            this.chkTellerReportSeq = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSpacer = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraEPayments)).BeginInit();
            this.fraEPayments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveServiceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddServiceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowServiceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSeperateCreditCardCashAccount)).BeginInit();
            this.fraSeperateCreditCardCashAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeperateCreditCardCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraGatewayInfo_IME)).BeginInit();
            this.fraGatewayInfo_IME.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceCodesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraGatewayInfo_ICL)).BeginInit();
            this.fraGatewayInfo_ICL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEPymtTestMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAuditPassword)).BeginInit();
            this.fraAuditPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptCloseOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintCCReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCrossCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAuditAR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAuditUT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAuditRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUSLExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExclusive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepositEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPartialAudit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCloseMV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDailyAuditByAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuditPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOther)).BeginInit();
            this.fraOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRoutNumsInBnkLst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSortTypeAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkClearPaidBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCrossCheck)).BeginInit();
            this.fraCrossCheck.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCrossCheckDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMVCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBreakdownCK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAlign)).BeginInit();
            this.fraAlign.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReset)).BeginInit();
            this.fraReset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraCD)).BeginInit();
            this.fraCD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCDConnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraComment)).BeginInit();
            this.fraComment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWidth)).BeginInit();
            this.fraWidth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceiptPrinterPrompt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowControlFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkThinReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUppercaseTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCompactReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSP200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTeller)).BeginInit();
            this.fraTeller.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerCheckReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerNewPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerWithAudit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerSeparate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerReportSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 713);
            this.BottomPanel.Size = new System.Drawing.Size(1048, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblOptions);
            this.ClientArea.Controls.Add(this.cmbOptions);
            this.ClientArea.Controls.Add(this.fraWidth);
            this.ClientArea.Controls.Add(this.fraTeller);
            this.ClientArea.Controls.Add(this.fraEPayments);
            this.ClientArea.Controls.Add(this.fraAuditPassword);
            this.ClientArea.Controls.Add(this.fraComment);
            this.ClientArea.Controls.Add(this.fraOther);
            this.ClientArea.Controls.Add(this.fraAlign);
            this.ClientArea.Controls.Add(this.fraCD);
            this.ClientArea.Controls.Add(this.fraReset);
            this.ClientArea.Size = new System.Drawing.Size(1068, 588);
            this.ClientArea.Controls.SetChildIndex(this.fraReset, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraCD, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraAlign, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraOther, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraAuditPassword, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraEPayments, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraTeller, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraWidth, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblOptions, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1068, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(117, 28);
            this.HeaderText.Text = "Customize";
            // 
            // cmbCheckReportOrder
            // 
            this.cmbCheckReportOrder.Items.AddRange(new object[] {
            "By Amount",
            "By Check # within Bank",
            "By Receipt within Bank"});
            this.cmbCheckReportOrder.Location = new System.Drawing.Point(278, 301);
            this.cmbCheckReportOrder.Name = "cmbCheckReportOrder";
            this.cmbCheckReportOrder.Size = new System.Drawing.Size(227, 40);
            this.cmbCheckReportOrder.TabIndex = 191;
            // 
            // lblCCReportSort
            // 
            this.lblCCReportSort.Location = new System.Drawing.Point(20, 94);
            this.lblCCReportSort.Name = "lblCCReportSort";
            this.lblCCReportSort.Size = new System.Drawing.Size(95, 0);
            this.lblCCReportSort.TabIndex = 192;
            this.lblCCReportSort.Text = "LBLCCREPORTSORT";
            // 
            // cmbCCReportSort
            // 
            this.cmbCCReportSort.Items.AddRange(new object[] {
            "By Receipt Within Type",
            "By Amount"});
            this.cmbCCReportSort.Location = new System.Drawing.Point(278, 358);
            this.cmbCCReportSort.Name = "cmbCCReportSort";
            this.cmbCCReportSort.Size = new System.Drawing.Size(227, 40);
            this.cmbCCReportSort.TabIndex = 193;
            // 
            // lblClearPaidBy
            // 
            this.lblClearPaidBy.Location = new System.Drawing.Point(20, 44);
            this.lblClearPaidBy.Name = "lblClearPaidBy";
            this.lblClearPaidBy.Size = new System.Drawing.Size(87, 0);
            this.lblClearPaidBy.TabIndex = 181;
            this.lblClearPaidBy.Text = "LBLCLEARPAIDBY";
            // 
            // cmbClearPaidBy
            // 
            this.cmbClearPaidBy.Items.AddRange(new object[] {
            "All Payments",
            "Multiple Payments Only"});
            this.cmbClearPaidBy.Location = new System.Drawing.Point(286, 95);
            this.cmbClearPaidBy.Name = "cmbClearPaidBy";
            this.cmbClearPaidBy.Size = new System.Drawing.Size(300, 40);
            this.cmbClearPaidBy.TabIndex = 182;
            // 
            // cmbCrossCheckDate
            // 
            this.cmbCrossCheckDate.Items.AddRange(new object[] {
            "Last End Of Year",
            "Last Cross Check Report",
            "Specific Date"});
            this.cmbCrossCheckDate.Location = new System.Drawing.Point(20, 30);
            this.cmbCrossCheckDate.Name = "cmbCrossCheckDate";
            this.cmbCrossCheckDate.Size = new System.Drawing.Size(300, 40);
            this.cmbCrossCheckDate.TabIndex = 75;
            this.cmbCrossCheckDate.SelectedIndexChanged += new System.EventHandler(this.optCrossCheckDate_CheckedChanged);
            // 
            // lblOptions
            // 
            this.lblOptions.Location = new System.Drawing.Point(30, 44);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(175, 16);
            this.lblOptions.TabIndex = 82;
            this.lblOptions.Text = "CUSTOMIZATION OPTIONS";
            // 
            // cmbOptions
            // 
            this.cmbOptions.Items.AddRange(new object[] {
            "Receipt Information",
            "Daily Audit Options",
            "Receipt Comments",
            "Report Alignment",
            "Teller Options",
            "Cash Drawer",
            "Reset Receipt Number",
            "Misc. Options",
            "Electronic Payment Processing"});
            this.cmbOptions.Location = new System.Drawing.Point(275, 30);
            this.cmbOptions.Name = "cmbOptions";
            this.cmbOptions.Size = new System.Drawing.Size(309, 40);
            this.cmbOptions.TabIndex = 83;
            this.cmbOptions.SelectedIndexChanged += new System.EventHandler(this.optOptions_CheckedChanged);
            // 
            // lblSignatureNo
            // 
            this.lblSignatureNo.Location = new System.Drawing.Point(20, 498);
            this.lblSignatureNo.Name = "lblSignatureNo";
            this.lblSignatureNo.Size = new System.Drawing.Size(135, 16);
            this.lblSignatureNo.TabIndex = 51;
            this.lblSignatureNo.Text = "PRINT SIGNATURE LINE";
            // 
            // cmbSignatureNo
            // 
            this.cmbSignatureNo.Items.AddRange(new object[] {
            "Never",
            "Only on CC Payments",
            "Always"});
            this.cmbSignatureNo.Location = new System.Drawing.Point(280, 484);
            this.cmbSignatureNo.Name = "cmbSignatureNo";
            this.cmbSignatureNo.Size = new System.Drawing.Size(200, 40);
            this.cmbSignatureNo.TabIndex = 52;
            // 
            // cmbReceiptWidth
            // 
            this.cmbReceiptWidth.Items.AddRange(new object[] {
            "Narrow",
            "Wide"});
            this.cmbReceiptWidth.Location = new System.Drawing.Point(280, 30);
            this.cmbReceiptWidth.Name = "cmbReceiptWidth";
            this.cmbReceiptWidth.Size = new System.Drawing.Size(200, 40);
            this.cmbReceiptWidth.TabIndex = 50;
            this.cmbReceiptWidth.SelectedIndexChanged += new System.EventHandler(this.optReceiptWidth_CheckedChanged);
            // 
            // fraEPayments
            // 
            this.fraEPayments.Controls.Add(this.btnRemoveServiceCode);
            this.fraEPayments.Controls.Add(this.btnAddServiceCode);
            this.fraEPayments.Controls.Add(this.chkShowServiceCode);
            this.fraEPayments.Controls.Add(this.fraSeperateCreditCardCashAccount);
            this.fraEPayments.Controls.Add(this.chkSeperateCreditCardCashAccount);
            this.fraEPayments.Controls.Add(this.fraGatewayInfo_IME);
            this.fraEPayments.Controls.Add(this.fraGatewayInfo_ICL);
            this.fraEPayments.Controls.Add(this.chkEPymtTestMode);
            this.fraEPayments.Controls.Add(this.lblPortal);
            this.fraEPayments.Controls.Add(this.txtPymtPortal);
            this.fraEPayments.Location = new System.Drawing.Point(30, 90);
            this.fraEPayments.Name = "fraEPayments";
            this.fraEPayments.Size = new System.Drawing.Size(719, 538);
            this.fraEPayments.TabIndex = 81;
            this.fraEPayments.Text = "Electronic Payment Processing";
            this.fraEPayments.Visible = false;
            // 
            // btnRemoveServiceCode
            // 
            this.btnRemoveServiceCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnRemoveServiceCode.Location = new System.Drawing.Point(355, 374);
            this.btnRemoveServiceCode.Name = "btnRemoveServiceCode";
            this.btnRemoveServiceCode.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.btnRemoveServiceCode.Size = new System.Drawing.Size(61, 24);
            this.btnRemoveServiceCode.TabIndex = 110;
            this.btnRemoveServiceCode.Text = "Remove";
            // 
            // btnAddServiceCode
            // 
            this.btnAddServiceCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnAddServiceCode.Location = new System.Drawing.Point(290, 374);
            this.btnAddServiceCode.Name = "btnAddServiceCode";
            this.btnAddServiceCode.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.btnAddServiceCode.Size = new System.Drawing.Size(45, 24);
            this.btnAddServiceCode.TabIndex = 109;
            this.btnAddServiceCode.Text = "Add";
            // 
            // chkShowServiceCode
            // 
            this.chkShowServiceCode.Location = new System.Drawing.Point(20, 80);
            this.chkShowServiceCode.Name = "chkShowServiceCode";
            this.chkShowServiceCode.Size = new System.Drawing.Size(255, 22);
            this.chkShowServiceCode.TabIndex = 105;
            this.chkShowServiceCode.Text = "Show service code on payment screen";
            // 
            // fraSeperateCreditCardCashAccount
            // 
            this.fraSeperateCreditCardCashAccount.AppearanceKey = "groupBoxNoBorders";
            this.fraSeperateCreditCardCashAccount.Controls.Add(this.txtSeperateCreditCardCashAcct);
            this.fraSeperateCreditCardCashAccount.Enabled = false;
            this.fraSeperateCreditCardCashAccount.Location = new System.Drawing.Point(303, 410);
            this.fraSeperateCreditCardCashAccount.Name = "fraSeperateCreditCardCashAccount";
            this.fraSeperateCreditCardCashAccount.Size = new System.Drawing.Size(141, 55);
            this.fraSeperateCreditCardCashAccount.TabIndex = 112;
            // 
            // txtSeperateCreditCardCashAcct
            // 
            this.txtSeperateCreditCardCashAcct.Anchor = Wisej.Web.AnchorStyles.Top;
            this.txtSeperateCreditCardCashAcct.BackColor = System.Drawing.SystemColors.Window;
            this.txtSeperateCreditCardCashAcct.Location = new System.Drawing.Point(7, 8);
            this.txtSeperateCreditCardCashAcct.Name = "txtSeperateCreditCardCashAcct";
            this.txtSeperateCreditCardCashAcct.Size = new System.Drawing.Size(116, 40);
            this.txtSeperateCreditCardCashAcct.TabIndex = 113;
            this.txtSeperateCreditCardCashAcct.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSeperateCreditCardCashAcct_KeyPress);
            // 
            // chkSeperateCreditCardCashAccount
            // 
            this.chkSeperateCreditCardCashAccount.Location = new System.Drawing.Point(20, 426);
            this.chkSeperateCreditCardCashAccount.Name = "chkSeperateCreditCardCashAccount";
            this.chkSeperateCreditCardCashAccount.Size = new System.Drawing.Size(239, 22);
            this.chkSeperateCreditCardCashAccount.TabIndex = 111;
            this.chkSeperateCreditCardCashAccount.Text = "Separate Credit Card Cash Account";
            this.chkSeperateCreditCardCashAccount.CheckedChanged += new System.EventHandler(this.chkSeperateCreditCardCashAccount_CheckedChanged);
            // 
            // fraGatewayInfo_IME
            // 
            this.fraGatewayInfo_IME.AppearanceKey = "groupBoxNoBorders";
            this.fraGatewayInfo_IME.Controls.Add(this.ServiceCodesGrid);
            this.fraGatewayInfo_IME.Controls.Add(this.lblSrvcCodes);
            this.fraGatewayInfo_IME.Location = new System.Drawing.Point(20, 152);
            this.fraGatewayInfo_IME.Name = "fraGatewayInfo_IME";
            this.fraGatewayInfo_IME.Size = new System.Drawing.Size(683, 210);
            this.fraGatewayInfo_IME.TabIndex = 107;
            // 
            // ServiceCodesGrid
            // 
            this.ServiceCodesGrid.Cols = 3;
            this.ServiceCodesGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.ServiceCodesGrid.ExtendLastCol = true;
            this.ServiceCodesGrid.FixedCols = 0;
            this.ServiceCodesGrid.Location = new System.Drawing.Point(0, 32);
            this.ServiceCodesGrid.Name = "ServiceCodesGrid";
            this.ServiceCodesGrid.ReadOnly = false;
            this.ServiceCodesGrid.RowHeadersVisible = false;
            this.ServiceCodesGrid.Rows = 1;
            this.ServiceCodesGrid.Size = new System.Drawing.Size(674, 166);
            this.ServiceCodesGrid.StandardTab = false;
            this.ServiceCodesGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.ServiceCodesGrid.TabIndex = 108;
            // 
            // lblSrvcCodes
            // 
            this.lblSrvcCodes.Location = new System.Drawing.Point(0, 6);
            this.lblSrvcCodes.Name = "lblSrvcCodes";
            this.lblSrvcCodes.Size = new System.Drawing.Size(145, 16);
            this.lblSrvcCodes.TabIndex = 186;
            this.lblSrvcCodes.Text = "SERVICE CODES";
            // 
            // fraGatewayInfo_ICL
            // 
            this.fraGatewayInfo_ICL.Controls.Add(this.txtBillerGUID_ICL);
            this.fraGatewayInfo_ICL.Controls.Add(this.lblICL_BillerGUID);
            this.fraGatewayInfo_ICL.Enabled = false;
            this.fraGatewayInfo_ICL.Location = new System.Drawing.Point(20, 196);
            this.fraGatewayInfo_ICL.Name = "fraGatewayInfo_ICL";
            this.fraGatewayInfo_ICL.Size = new System.Drawing.Size(520, 140);
            this.fraGatewayInfo_ICL.TabIndex = 159;
            this.fraGatewayInfo_ICL.Visible = false;
            // 
            // txtBillerGUID_ICL
            // 
            this.txtBillerGUID_ICL.Appearance = 0;
            this.txtBillerGUID_ICL.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtBillerGUID_ICL.Location = new System.Drawing.Point(20, 80);
            this.txtBillerGUID_ICL.Name = "txtBillerGUID_ICL";
            this.txtBillerGUID_ICL.Size = new System.Drawing.Size(268, 40);
            this.txtBillerGUID_ICL.TabIndex = 161;
            // 
            // lblICL_BillerGUID
            // 
            this.lblICL_BillerGUID.Location = new System.Drawing.Point(20, 44);
            this.lblICL_BillerGUID.Name = "lblICL_BillerGUID";
            this.lblICL_BillerGUID.Size = new System.Drawing.Size(71, 18);
            this.lblICL_BillerGUID.TabIndex = 160;
            this.lblICL_BillerGUID.Text = "BILLER GUID";
            // 
            // chkEPymtTestMode
            // 
            this.chkEPymtTestMode.Location = new System.Drawing.Point(20, 112);
            this.chkEPymtTestMode.Name = "chkEPymtTestMode";
            this.chkEPymtTestMode.Size = new System.Drawing.Size(93, 22);
            this.chkEPymtTestMode.TabIndex = 106;
            this.chkEPymtTestMode.Text = "Test Mode";
            // 
            // lblPortal
            // 
            this.lblPortal.Location = new System.Drawing.Point(20, 44);
            this.lblPortal.Name = "lblPortal";
            this.lblPortal.TabIndex = 133;
            this.lblPortal.Text = "PORTAL TYPE";
            // 
            // txtPymtPortal
            // 
            this.txtPymtPortal.BackColor = System.Drawing.Color.Transparent;
            this.txtPymtPortal.Location = new System.Drawing.Point(190, 44);
            this.txtPymtPortal.Name = "txtPymtPortal";
            this.txtPymtPortal.Size = new System.Drawing.Size(350, 16);
            this.txtPymtPortal.TabIndex = 132;
            // 
            // fraAuditPassword
            // 
            this.fraAuditPassword.Controls.Add(this.chkDeptCloseOut);
            this.fraAuditPassword.Controls.Add(this.cmbCheckReportOrder);
            this.fraAuditPassword.Controls.Add(this.lblCCReportSort);
            this.fraAuditPassword.Controls.Add(this.cmbCCReportSort);
            this.fraAuditPassword.Controls.Add(this.chkPrintCCReport);
            this.fraAuditPassword.Controls.Add(this.cmbCrossCheck);
            this.fraAuditPassword.Controls.Add(this.chkCrossCheck);
            this.fraAuditPassword.Controls.Add(this.chkPrintAuditAR);
            this.fraAuditPassword.Controls.Add(this.chkPrintAuditUT);
            this.fraAuditPassword.Controls.Add(this.chkPrintAuditRE);
            this.fraAuditPassword.Controls.Add(this.chkUSLExport);
            this.fraAuditPassword.Controls.Add(this.chkExclusive);
            this.fraAuditPassword.Controls.Add(this.chkDepositEntry);
            this.fraAuditPassword.Controls.Add(this.chkPartialAudit);
            this.fraAuditPassword.Controls.Add(this.chkCloseMV);
            this.fraAuditPassword.Controls.Add(this.chkDailyAuditByAlpha);
            this.fraAuditPassword.Controls.Add(this.txtAuditPassword);
            this.fraAuditPassword.Controls.Add(this.chkAuditPassword);
            this.fraAuditPassword.Controls.Add(this.lblCheckReportOrder);
            this.fraAuditPassword.Location = new System.Drawing.Point(30, 90);
            this.fraAuditPassword.Name = "fraAuditPassword";
            this.fraAuditPassword.Size = new System.Drawing.Size(719, 623);
            this.fraAuditPassword.TabIndex = 64;
            this.fraAuditPassword.Text = "Daily Audit Options";
            this.fraAuditPassword.Visible = false;
            // 
            // chkDeptCloseOut
            // 
            this.chkDeptCloseOut.Location = new System.Drawing.Point(20, 80);
            this.chkDeptCloseOut.Name = "chkDeptCloseOut";
            this.chkDeptCloseOut.Size = new System.Drawing.Size(180, 22);
            this.chkDeptCloseOut.TabIndex = 190;
            this.chkDeptCloseOut.Text = "Close Out By Department";
            // 
            // chkPrintCCReport
            // 
            this.chkPrintCCReport.Location = new System.Drawing.Point(20, 365);
            this.chkPrintCCReport.Name = "chkPrintCCReport";
            this.chkPrintCCReport.Size = new System.Drawing.Size(205, 22);
            this.chkPrintCCReport.TabIndex = 124;
            this.chkPrintCCReport.Text = "Print Credit/Debit Card Report";
            this.chkPrintCCReport.CheckedChanged += new System.EventHandler(this.chkPrintCCReport_CheckedChanged);
            // 
            // cmbCrossCheck
            // 
            this.cmbCrossCheck.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCrossCheck.Location = new System.Drawing.Point(353, 563);
            this.cmbCrossCheck.Name = "cmbCrossCheck";
            this.cmbCrossCheck.Size = new System.Drawing.Size(226, 40);
            this.cmbCrossCheck.TabIndex = 20;
            // 
            // chkCrossCheck
            // 
            this.chkCrossCheck.Location = new System.Drawing.Point(20, 570);
            this.chkCrossCheck.Name = "chkCrossCheck";
            this.chkCrossCheck.Size = new System.Drawing.Size(269, 22);
            this.chkCrossCheck.TabIndex = 19;
            this.chkCrossCheck.Text = "Automatically run the Cross Check report";
            this.chkCrossCheck.CheckedChanged += new System.EventHandler(this.chkCrossCheck_CheckedChanged);
            // 
            // chkPrintAuditAR
            // 
            this.chkPrintAuditAR.Location = new System.Drawing.Point(20, 529);
            this.chkPrintAuditAR.Name = "chkPrintAuditAR";
            this.chkPrintAuditAR.Size = new System.Drawing.Size(239, 22);
            this.chkPrintAuditAR.TabIndex = 18;
            this.chkPrintAuditAR.Text = "Print the Accounts Receivable Audit";
            // 
            // chkPrintAuditUT
            // 
            this.chkPrintAuditUT.Location = new System.Drawing.Point(20, 488);
            this.chkPrintAuditUT.Name = "chkPrintAuditUT";
            this.chkPrintAuditUT.Size = new System.Drawing.Size(187, 22);
            this.chkPrintAuditUT.TabIndex = 17;
            this.chkPrintAuditUT.Text = "Print the Utility Billing Audit";
            this.chkPrintAuditUT.CheckedChanged += new System.EventHandler(this.chkPrintAuditUT_CheckedChanged);
            // 
            // chkPrintAuditRE
            // 
            this.chkPrintAuditRE.Location = new System.Drawing.Point(20, 406);
            this.chkPrintAuditRE.Name = "chkPrintAuditRE";
            this.chkPrintAuditRE.Size = new System.Drawing.Size(181, 22);
            this.chkPrintAuditRE.TabIndex = 16;
            this.chkPrintAuditRE.Text = "Print the Collections Audit";
            this.chkPrintAuditRE.CheckedChanged += new System.EventHandler(this.chkPrintAuditRE_CheckedChanged);
            // 
            // chkUSLExport
            // 
            this.chkUSLExport.Enabled = false;
            this.chkUSLExport.Location = new System.Drawing.Point(20, 447);
            this.chkUSLExport.Name = "chkUSLExport";
            this.chkUSLExport.Size = new System.Drawing.Size(98, 22);
            this.chkUSLExport.TabIndex = 15;
            this.chkUSLExport.Text = "USL Export";
            // 
            // chkExclusive
            // 
            this.chkExclusive.Location = new System.Drawing.Point(20, 116);
            this.chkExclusive.Name = "chkExclusive";
            this.chkExclusive.Size = new System.Drawing.Size(200, 22);
            this.chkExclusive.TabIndex = 10;
            this.chkExclusive.Text = "Check For BD Multiple Users";
            this.ToolTip1.SetToolTip(this.chkExclusive, "Check For BD Multiple Users");
            this.chkExclusive.CheckedChanged += new System.EventHandler(this.chkExclusive_CheckedChanged);
            // 
            // chkDepositEntry
            // 
            this.chkDepositEntry.Location = new System.Drawing.Point(20, 260);
            this.chkDepositEntry.Name = "chkDepositEntry";
            this.chkDepositEntry.Size = new System.Drawing.Size(572, 22);
            this.chkDepositEntry.TabIndex = 14;
            this.chkDepositEntry.Text = "Automatically add Deposit Entries into Check Reconciliation File during Cash Rece" +
    "ipting Audit";
            this.chkDepositEntry.Visible = false;
            this.chkDepositEntry.CheckedChanged += new System.EventHandler(this.chkDepositEntry_CheckedChanged);
            // 
            // chkPartialAudit
            // 
            this.chkPartialAudit.Location = new System.Drawing.Point(20, 188);
            this.chkPartialAudit.Name = "chkPartialAudit";
            this.chkPartialAudit.Size = new System.Drawing.Size(165, 22);
            this.chkPartialAudit.TabIndex = 12;
            this.chkPartialAudit.Text = "Allow Partial Close Out";
            this.ToolTip1.SetToolTip(this.chkPartialAudit, "Allow a partial close out.");
            this.chkPartialAudit.CheckedChanged += new System.EventHandler(this.chkPartialAudit_CheckedChanged);
            // 
            // chkCloseMV
            // 
            this.chkCloseMV.Location = new System.Drawing.Point(20, 224);
            this.chkCloseMV.Name = "chkCloseMV";
            this.chkCloseMV.Size = new System.Drawing.Size(270, 22);
            this.chkCloseMV.TabIndex = 13;
            this.chkCloseMV.Text = "Close Motor Vehicle Period Automatically";
            this.chkCloseMV.CheckedChanged += new System.EventHandler(this.chkCloseMV_CheckedChanged);
            this.chkCloseMV.MouseUp += new Wisej.Web.MouseEventHandler(this.chkCloseMV_MouseUp);
            this.chkCloseMV.KeyUp += new Wisej.Web.KeyEventHandler(this.chkCloseMV_KeyUp);
            // 
            // chkDailyAuditByAlpha
            // 
            this.chkDailyAuditByAlpha.Location = new System.Drawing.Point(20, 152);
            this.chkDailyAuditByAlpha.Name = "chkDailyAuditByAlpha";
            this.chkDailyAuditByAlpha.Size = new System.Drawing.Size(187, 22);
            this.chkDailyAuditByAlpha.TabIndex = 11;
            this.chkDailyAuditByAlpha.Text = "Order Daily Audit by Name";
            this.ToolTip1.SetToolTip(this.chkDailyAuditByAlpha, "Order the Daily Audit by alphanumeric characters within the Type breakdown.");
            this.chkDailyAuditByAlpha.CheckedChanged += new System.EventHandler(this.chkDailyAuditByAlpha_CheckedChanged);
            // 
            // txtAuditPassword
            // 
            this.txtAuditPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtAuditPassword.Enabled = false;
            this.txtAuditPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
            this.txtAuditPassword.Location = new System.Drawing.Point(321, 37);
            this.txtAuditPassword.Name = "txtAuditPassword";
            this.txtAuditPassword.PasswordChar = '*';
            this.txtAuditPassword.Size = new System.Drawing.Size(261, 40);
            this.txtAuditPassword.TabIndex = 9;
            this.txtAuditPassword.TextChanged += new System.EventHandler(this.txtAuditPassword_TextChanged);
            // 
            // chkAuditPassword
            // 
            this.chkAuditPassword.Location = new System.Drawing.Point(20, 44);
            this.chkAuditPassword.Name = "chkAuditPassword";
            this.chkAuditPassword.Size = new System.Drawing.Size(241, 22);
            this.chkAuditPassword.TabIndex = 8;
            this.chkAuditPassword.Text = "Password Protect Your Audit Report";
            this.chkAuditPassword.CheckedChanged += new System.EventHandler(this.chkAuditPassword_CheckedChanged);
            // 
            // lblCheckReportOrder
            // 
            this.lblCheckReportOrder.Location = new System.Drawing.Point(20, 315);
            this.lblCheckReportOrder.Name = "lblCheckReportOrder";
            this.lblCheckReportOrder.Size = new System.Drawing.Size(140, 16);
            this.lblCheckReportOrder.TabIndex = 136;
            this.lblCheckReportOrder.Text = "CHECK REPORT ORDER";
            // 
            // fraOther
            // 
            this.fraOther.AppearanceKey = "groupBoxLeftBorder";
            this.fraOther.Controls.Add(this.chkRoutNumsInBnkLst);
            this.fraOther.Controls.Add(this.lblClearPaidBy);
            this.fraOther.Controls.Add(this.cmbClearPaidBy);
            this.fraOther.Controls.Add(this.Frame3);
            this.fraOther.Controls.Add(this.chkSortTypeAlpha);
            this.fraOther.Controls.Add(this.chkClearPaidBy);
            this.fraOther.Controls.Add(this.fraCrossCheck);
            this.fraOther.Controls.Add(this.chkMVCheck);
            this.fraOther.Controls.Add(this.chkBreakdownCK);
            this.fraOther.Location = new System.Drawing.Point(30, 90);
            this.fraOther.Name = "fraOther";
            this.fraOther.Size = new System.Drawing.Size(603, 490);
            this.fraOther.TabIndex = 70;
            this.fraOther.Text = "Other Options";
            this.fraOther.Visible = false;
            // 
            // chkRoutNumsInBnkLst
            // 
            this.chkRoutNumsInBnkLst.Location = new System.Drawing.Point(20, 174);
            this.chkRoutNumsInBnkLst.Name = "chkRoutNumsInBnkLst";
            this.chkRoutNumsInBnkLst.Size = new System.Drawing.Size(195, 22);
            this.chkRoutNumsInBnkLst.TabIndex = 180;
            this.chkRoutNumsInBnkLst.Text = "Show Routing # in Bank List";
            this.chkRoutNumsInBnkLst.CheckedChanged += new System.EventHandler(this.chkRoutNumsInBnkLst_CheckedChanged);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtMVDelay);
            this.Frame3.Controls.Add(this.Label7);
            this.Frame3.Location = new System.Drawing.Point(20, 380);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(220, 90);
            this.Frame3.TabIndex = 156;
            this.Frame3.Text = "Motor Vehicle Delay";
            // 
            // txtMVDelay
            // 
            this.txtMVDelay.BackColor = System.Drawing.SystemColors.Window;
            this.txtMVDelay.Location = new System.Drawing.Point(20, 30);
            this.txtMVDelay.MaxLength = 1;
            this.txtMVDelay.Name = "txtMVDelay";
            this.txtMVDelay.Size = new System.Drawing.Size(80, 40);
            this.txtMVDelay.TabIndex = 158;
            this.txtMVDelay.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMVDelay_KeyPress);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(135, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(65, 16);
            this.Label7.TabIndex = 157;
            this.Label7.Text = "SECOND(S)";
            // 
            // chkSortTypeAlpha
            // 
            this.chkSortTypeAlpha.Location = new System.Drawing.Point(20, 138);
            this.chkSortTypeAlpha.Name = "chkSortTypeAlpha";
            this.chkSortTypeAlpha.Size = new System.Drawing.Size(200, 22);
            this.chkSortTypeAlpha.TabIndex = 79;
            this.chkSortTypeAlpha.Text = "Sort Receipt Types By Name";
            this.chkSortTypeAlpha.CheckedChanged += new System.EventHandler(this.chkSortTypeAlpha_CheckedChanged);
            // 
            // chkClearPaidBy
            // 
            this.chkClearPaidBy.Location = new System.Drawing.Point(20, 102);
            this.chkClearPaidBy.Name = "chkClearPaidBy";
            this.chkClearPaidBy.Size = new System.Drawing.Size(216, 22);
            this.chkClearPaidBy.TabIndex = 77;
            this.chkClearPaidBy.Text = "Clear Paid By on Tax Payments";
            this.chkClearPaidBy.CheckedChanged += new System.EventHandler(this.chkClearPaidBy_CheckedChanged);
            // 
            // fraCrossCheck
            // 
            this.fraCrossCheck.Controls.Add(this.txtCrossCheckDate);
            this.fraCrossCheck.Controls.Add(this.cmbCrossCheckDate);
            this.fraCrossCheck.Location = new System.Drawing.Point(20, 220);
            this.fraCrossCheck.Name = "fraCrossCheck";
            this.fraCrossCheck.Size = new System.Drawing.Size(340, 140);
            this.fraCrossCheck.TabIndex = 78;
            this.fraCrossCheck.Text = "Cross Check Date";
            // 
            // txtCrossCheckDate
            // 
            this.txtCrossCheckDate.Location = new System.Drawing.Point(20, 80);
            this.txtCrossCheckDate.Mask = "##/##/####";
            this.txtCrossCheckDate.Name = "txtCrossCheckDate";
            this.txtCrossCheckDate.Size = new System.Drawing.Size(115, 22);
            this.txtCrossCheckDate.TabIndex = 73;
            // 
            // chkMVCheck
            // 
            this.chkMVCheck.Location = new System.Drawing.Point(20, 66);
            this.chkMVCheck.Name = "chkMVCheck";
            this.chkMVCheck.Size = new System.Drawing.Size(184, 22);
            this.chkMVCheck.TabIndex = 72;
            this.chkMVCheck.Text = "Verification screen for MV.";
            this.chkMVCheck.Visible = false;
            // 
            // chkBreakdownCK
            // 
            this.chkBreakdownCK.Location = new System.Drawing.Point(20, 30);
            this.chkBreakdownCK.Name = "chkBreakdownCK";
            this.chkBreakdownCK.Size = new System.Drawing.Size(316, 22);
            this.chkBreakdownCK.TabIndex = 40;
            this.chkBreakdownCK.Text = "Breakdown Clerk receipt types to 800-804 series.";
            // 
            // fraAlign
            // 
            this.fraAlign.Controls.Add(this.txtAuditMarginTop);
            this.fraAlign.Controls.Add(this.lblAuditMarginTop);
            this.fraAlign.Controls.Add(this.lblLines);
            this.fraAlign.Location = new System.Drawing.Point(30, 90);
            this.fraAlign.Name = "fraAlign";
            this.fraAlign.Size = new System.Drawing.Size(464, 90);
            this.fraAlign.TabIndex = 57;
            this.fraAlign.Text = "Default Report Alignment";
            this.fraAlign.Visible = false;
            // 
            // txtAuditMarginTop
            // 
            this.txtAuditMarginTop.BackColor = System.Drawing.SystemColors.Window;
            this.txtAuditMarginTop.Location = new System.Drawing.Point(240, 30);
            this.txtAuditMarginTop.MaxLength = 5;
            this.txtAuditMarginTop.Name = "txtAuditMarginTop";
            this.txtAuditMarginTop.Size = new System.Drawing.Size(130, 40);
            this.txtAuditMarginTop.TabIndex = 39;
            this.txtAuditMarginTop.Text = "0";
            this.txtAuditMarginTop.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAuditMarginTop.TextChanged += new System.EventHandler(this.txtAuditMarginTop_TextChanged);
            this.txtAuditMarginTop.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAuditMarginTop_KeyPress);
            // 
            // lblAuditMarginTop
            // 
            this.lblAuditMarginTop.Location = new System.Drawing.Point(20, 44);
            this.lblAuditMarginTop.Name = "lblAuditMarginTop";
            this.lblAuditMarginTop.Size = new System.Drawing.Size(150, 16);
            this.lblAuditMarginTop.TabIndex = 68;
            this.lblAuditMarginTop.Text = "ADJUST THE TOP MARGIN";
            // 
            // lblLines
            // 
            this.lblLines.Location = new System.Drawing.Point(406, 44);
            this.lblLines.Name = "lblLines";
            this.lblLines.Size = new System.Drawing.Size(38, 16);
            this.lblLines.TabIndex = 67;
            this.lblLines.Text = "LINES";
            // 
            // fraReset
            // 
            this.fraReset.Controls.Add(this.txtResetReceipt);
            this.fraReset.Controls.Add(this.lblResetReceipt);
            this.fraReset.Location = new System.Drawing.Point(30, 90);
            this.fraReset.Name = "fraReset";
            this.fraReset.Size = new System.Drawing.Size(500, 90);
            this.fraReset.TabIndex = 60;
            this.fraReset.Text = "Reset Receipt Number";
            this.fraReset.Visible = false;
            // 
            // txtResetReceipt
            // 
            this.txtResetReceipt.BackColor = System.Drawing.SystemColors.Window;
            this.txtResetReceipt.Location = new System.Drawing.Point(195, 30);
            this.txtResetReceipt.MaxLength = 9;
            this.txtResetReceipt.Name = "txtResetReceipt";
            this.txtResetReceipt.Size = new System.Drawing.Size(285, 40);
            this.txtResetReceipt.TabIndex = 47;
            this.txtResetReceipt.TextChanged += new System.EventHandler(this.txtResetReceipt_TextChanged);
            // 
            // lblResetReceipt
            // 
            this.lblResetReceipt.Location = new System.Drawing.Point(20, 44);
            this.lblResetReceipt.Name = "lblResetReceipt";
            this.lblResetReceipt.Size = new System.Drawing.Size(105, 16);
            this.lblResetReceipt.TabIndex = 61;
            this.lblResetReceipt.Text = "RECEIPT NUMBER";
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1,
            imageListEntry2});
            this.ImageList1.ImageSize = new System.Drawing.Size(32, 32);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // fraCD
            // 
            this.fraCD.Controls.Add(this.cmbPrinterPort);
            this.fraCD.Controls.Add(this.cmbRepeat);
            this.fraCD.Controls.Add(this.chkCDConnected);
            this.fraCD.Controls.Add(this.txtCDCode);
            this.fraCD.Controls.Add(this.lblCDCode);
            this.fraCD.Controls.Add(this.lblCDRepeat);
            this.fraCD.Controls.Add(this.lblPortID);
            this.fraCD.Location = new System.Drawing.Point(30, 90);
            this.fraCD.Name = "fraCD";
            this.fraCD.Size = new System.Drawing.Size(500, 244);
            this.fraCD.TabIndex = 50;
            this.fraCD.Text = "Cash Drawer";
            this.fraCD.Visible = false;
            // 
            // cmbPrinterPort
            // 
            this.cmbPrinterPort.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPrinterPort.Enabled = false;
            this.cmbPrinterPort.Location = new System.Drawing.Point(210, 184);
            this.cmbPrinterPort.Name = "cmbPrinterPort";
            this.cmbPrinterPort.Size = new System.Drawing.Size(270, 40);
            this.cmbPrinterPort.Sorted = true;
            this.cmbPrinterPort.TabIndex = 44;
            this.cmbPrinterPort.TextChanged += new System.EventHandler(this.cmbPrinterPort_TextChanged);
            // 
            // cmbRepeat
            // 
            this.cmbRepeat.BackColor = System.Drawing.SystemColors.Window;
            this.cmbRepeat.Enabled = false;
            this.cmbRepeat.Location = new System.Drawing.Point(210, 124);
            this.cmbRepeat.Name = "cmbRepeat";
            this.cmbRepeat.Size = new System.Drawing.Size(270, 40);
            this.cmbRepeat.Sorted = true;
            this.cmbRepeat.TabIndex = 43;
            this.cmbRepeat.TextChanged += new System.EventHandler(this.cmbRepeat_TextChanged);
            // 
            // chkCDConnected
            // 
            this.chkCDConnected.Location = new System.Drawing.Point(20, 30);
            this.chkCDConnected.Name = "chkCDConnected";
            this.chkCDConnected.Size = new System.Drawing.Size(177, 22);
            this.chkCDConnected.TabIndex = 41;
            this.chkCDConnected.Text = "Cash drawer connected?";
            this.chkCDConnected.CheckedChanged += new System.EventHandler(this.chkCDConnected_CheckedChanged);
            // 
            // txtCDCode
            // 
            this.txtCDCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtCDCode.Enabled = false;
            this.txtCDCode.Location = new System.Drawing.Point(210, 64);
            this.txtCDCode.Name = "txtCDCode";
            this.txtCDCode.Size = new System.Drawing.Size(270, 40);
            this.txtCDCode.TabIndex = 42;
            this.txtCDCode.TextChanged += new System.EventHandler(this.txtCDCode_TextChanged);
            // 
            // lblCDCode
            // 
            this.lblCDCode.Enabled = false;
            this.lblCDCode.Location = new System.Drawing.Point(20, 78);
            this.lblCDCode.Name = "lblCDCode";
            this.lblCDCode.Size = new System.Drawing.Size(130, 16);
            this.lblCDCode.TabIndex = 53;
            this.lblCDCode.Text = "CASH DRAWER CODE";
            // 
            // lblCDRepeat
            // 
            this.lblCDRepeat.Enabled = false;
            this.lblCDRepeat.Location = new System.Drawing.Point(20, 138);
            this.lblCDRepeat.Name = "lblCDRepeat";
            this.lblCDRepeat.Size = new System.Drawing.Size(95, 16);
            this.lblCDRepeat.TabIndex = 52;
            this.lblCDRepeat.Text = "REPEAT COUNT";
            // 
            // lblPortID
            // 
            this.lblPortID.Enabled = false;
            this.lblPortID.Location = new System.Drawing.Point(20, 198);
            this.lblPortID.Name = "lblPortID";
            this.lblPortID.Size = new System.Drawing.Size(54, 16);
            this.lblPortID.TabIndex = 51;
            this.lblPortID.Text = "PORT ID";
            // 
            // fraComment
            // 
            this.fraComment.Controls.Add(this.txtTopComment);
            this.fraComment.Controls.Add(this.txtBottomComment);
            this.fraComment.Controls.Add(this.lblTopComment);
            this.fraComment.Controls.Add(this.lblBottom);
            this.fraComment.Location = new System.Drawing.Point(30, 90);
            this.fraComment.Name = "fraComment";
            this.fraComment.Size = new System.Drawing.Size(583, 154);
            this.fraComment.TabIndex = 54;
            this.fraComment.Text = "Receipt Comments";
            this.fraComment.Visible = false;
            // 
            // txtTopComment
            // 
            this.txtTopComment.BackColor = System.Drawing.SystemColors.Window;
            this.txtTopComment.Location = new System.Drawing.Point(103, 30);
            this.txtTopComment.MaxLength = 38;
            this.txtTopComment.Name = "txtTopComment";
            this.txtTopComment.Size = new System.Drawing.Size(460, 40);
            this.txtTopComment.TabIndex = 45;
            this.txtTopComment.TextChanged += new System.EventHandler(this.txtTopComment_TextChanged);
            // 
            // txtBottomComment
            // 
            this.txtBottomComment.BackColor = System.Drawing.SystemColors.Window;
            this.txtBottomComment.Location = new System.Drawing.Point(103, 90);
            this.txtBottomComment.MaxLength = 38;
            this.txtBottomComment.Name = "txtBottomComment";
            this.txtBottomComment.Size = new System.Drawing.Size(460, 40);
            this.txtBottomComment.TabIndex = 46;
            this.txtBottomComment.TextChanged += new System.EventHandler(this.txtBottomComment_TextChanged);
            // 
            // lblTopComment
            // 
            this.lblTopComment.Location = new System.Drawing.Point(20, 44);
            this.lblTopComment.Name = "lblTopComment";
            this.lblTopComment.Size = new System.Drawing.Size(31, 16);
            this.lblTopComment.TabIndex = 56;
            this.lblTopComment.Text = "TOP";
            // 
            // lblBottom
            // 
            this.lblBottom.Location = new System.Drawing.Point(20, 104);
            this.lblBottom.Name = "lblBottom";
            this.lblBottom.Size = new System.Drawing.Size(57, 16);
            this.lblBottom.TabIndex = 55;
            this.lblBottom.Text = "BOTTOM";
            // 
            // fraWidth
            // 
            this.fraWidth.Controls.Add(this.lblReceiptWidth);
            this.fraWidth.Controls.Add(this.cmbReceiptWidth);
            this.fraWidth.Controls.Add(this.lblSignatureNo);
            this.fraWidth.Controls.Add(this.cmbSignatureNo);
            this.fraWidth.Controls.Add(this.chkReceiptPrinterPrompt);
            this.fraWidth.Controls.Add(this.chkShowControlFields);
            this.fraWidth.Controls.Add(this.chkThinReceipt);
            this.fraWidth.Controls.Add(this.chkUppercaseTypes);
            this.fraWidth.Controls.Add(this.cmbFontSize);
            this.fraWidth.Controls.Add(this.chkCompactReceipt);
            this.fraWidth.Controls.Add(this.chkNoMargin);
            this.fraWidth.Controls.Add(this.cmbReceiptOffset);
            this.fraWidth.Controls.Add(this.chkSP200);
            this.fraWidth.Controls.Add(this.cmbExtraLines);
            this.fraWidth.Controls.Add(this.cmbWideLines);
            this.fraWidth.Controls.Add(this.lblFontSize);
            this.fraWidth.Controls.Add(this.lblReceiptOffset);
            this.fraWidth.Controls.Add(this.lblExtraLines);
            this.fraWidth.Controls.Add(this.lblWideLines);
            this.fraWidth.Location = new System.Drawing.Point(30, 90);
            this.fraWidth.Name = "fraWidth";
            this.fraWidth.Size = new System.Drawing.Size(500, 544);
            this.fraWidth.TabIndex = 48;
            this.fraWidth.Text = "Receipt Information";
            this.fraWidth.Visible = false;
            // 
            // lblReceiptWidth
            // 
            this.lblReceiptWidth.Location = new System.Drawing.Point(20, 44);
            this.lblReceiptWidth.Name = "lblReceiptWidth";
            this.lblReceiptWidth.Size = new System.Drawing.Size(95, 16);
            this.lblReceiptWidth.TabIndex = 49;
            this.lblReceiptWidth.Text = "RECEIPT WIDTH";
            // 
            // chkReceiptPrinterPrompt
            // 
            this.chkReceiptPrinterPrompt.Location = new System.Drawing.Point(20, 450);
            this.chkReceiptPrinterPrompt.Name = "chkReceiptPrinterPrompt";
            this.chkReceiptPrinterPrompt.Size = new System.Drawing.Size(213, 22);
            this.chkReceiptPrinterPrompt.TabIndex = 38;
            this.chkReceiptPrinterPrompt.Text = "Prompt User for Receipt Printer";
            // 
            // chkShowControlFields
            // 
            this.chkShowControlFields.Location = new System.Drawing.Point(20, 416);
            this.chkShowControlFields.Name = "chkShowControlFields";
            this.chkShowControlFields.Size = new System.Drawing.Size(214, 22);
            this.chkShowControlFields.TabIndex = 37;
            this.chkShowControlFields.Text = "Show Control Fields on Receipt";
            // 
            // chkThinReceipt
            // 
            this.chkThinReceipt.Location = new System.Drawing.Point(280, 148);
            this.chkThinReceipt.Name = "chkThinReceipt";
            this.chkThinReceipt.Size = new System.Drawing.Size(105, 22);
            this.chkThinReceipt.TabIndex = 71;
            this.chkThinReceipt.Text = "Thin Receipt";
            // 
            // chkUppercaseTypes
            // 
            this.chkUppercaseTypes.Location = new System.Drawing.Point(20, 382);
            this.chkUppercaseTypes.Name = "chkUppercaseTypes";
            this.chkUppercaseTypes.Size = new System.Drawing.Size(348, 22);
            this.chkUppercaseTypes.TabIndex = 36;
            this.chkUppercaseTypes.Text = "Force restricted receipt type descriptions to uppercase.";
            // 
            // cmbFontSize
            // 
            this.cmbFontSize.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFontSize.Enabled = false;
            this.cmbFontSize.Location = new System.Drawing.Point(280, 332);
            this.cmbFontSize.Name = "cmbFontSize";
            this.cmbFontSize.Size = new System.Drawing.Size(200, 40);
            this.cmbFontSize.TabIndex = 35;
            this.cmbFontSize.TextChanged += new System.EventHandler(this.cmbFontSize_TextChanged);
            // 
            // chkCompactReceipt
            // 
            this.chkCompactReceipt.Location = new System.Drawing.Point(280, 114);
            this.chkCompactReceipt.Name = "chkCompactReceipt";
            this.chkCompactReceipt.Size = new System.Drawing.Size(132, 22);
            this.chkCompactReceipt.TabIndex = 31;
            this.chkCompactReceipt.Text = "Compact Receipt";
            // 
            // chkNoMargin
            // 
            this.chkNoMargin.Location = new System.Drawing.Point(280, 80);
            this.chkNoMargin.Name = "chkNoMargin";
            this.chkNoMargin.Size = new System.Drawing.Size(128, 22);
            this.chkNoMargin.TabIndex = 30;
            this.chkNoMargin.Text = "Zero Top Margin";
            this.chkNoMargin.CheckedChanged += new System.EventHandler(this.chkNoMargin_CheckedChanged);
            // 
            // cmbReceiptOffset
            // 
            this.cmbReceiptOffset.BackColor = System.Drawing.SystemColors.Window;
            this.cmbReceiptOffset.Enabled = false;
            this.cmbReceiptOffset.Location = new System.Drawing.Point(280, 182);
            this.cmbReceiptOffset.Name = "cmbReceiptOffset";
            this.cmbReceiptOffset.Size = new System.Drawing.Size(200, 40);
            this.cmbReceiptOffset.Sorted = true;
            this.cmbReceiptOffset.TabIndex = 32;
            this.cmbReceiptOffset.TextChanged += new System.EventHandler(this.cmbReceiptOffset_TextChanged);
            // 
            // chkSP200
            // 
            this.chkSP200.Location = new System.Drawing.Point(20, 80);
            this.chkSP200.Name = "chkSP200";
            this.chkSP200.Size = new System.Drawing.Size(119, 22);
            this.chkSP200.TabIndex = 29;
            this.chkSP200.Text = "Star SP Printer";
            this.chkSP200.CheckedChanged += new System.EventHandler(this.chkSP200_CheckedChanged);
            // 
            // cmbExtraLines
            // 
            this.cmbExtraLines.BackColor = System.Drawing.SystemColors.Window;
            this.cmbExtraLines.Enabled = false;
            this.cmbExtraLines.Location = new System.Drawing.Point(280, 232);
            this.cmbExtraLines.Name = "cmbExtraLines";
            this.cmbExtraLines.Size = new System.Drawing.Size(200, 40);
            this.cmbExtraLines.Sorted = true;
            this.cmbExtraLines.TabIndex = 33;
            this.cmbExtraLines.TextChanged += new System.EventHandler(this.cmbExtraLines_TextChanged);
            // 
            // cmbWideLines
            // 
            this.cmbWideLines.BackColor = System.Drawing.SystemColors.Window;
            this.cmbWideLines.Enabled = false;
            this.cmbWideLines.Location = new System.Drawing.Point(280, 282);
            this.cmbWideLines.Name = "cmbWideLines";
            this.cmbWideLines.Size = new System.Drawing.Size(200, 40);
            this.cmbWideLines.Sorted = true;
            this.cmbWideLines.TabIndex = 34;
            this.cmbWideLines.TextChanged += new System.EventHandler(this.cmbWideLines_TextChanged);
            // 
            // lblFontSize
            // 
            this.lblFontSize.Enabled = false;
            this.lblFontSize.Location = new System.Drawing.Point(20, 346);
            this.lblFontSize.Name = "lblFontSize";
            this.lblFontSize.Size = new System.Drawing.Size(64, 16);
            this.lblFontSize.TabIndex = 69;
            this.lblFontSize.Text = "FONT SIZE";
            // 
            // lblReceiptOffset
            // 
            this.lblReceiptOffset.Enabled = false;
            this.lblReceiptOffset.Location = new System.Drawing.Point(20, 196);
            this.lblReceiptOffset.Name = "lblReceiptOffset";
            this.lblReceiptOffset.TabIndex = 65;
            this.lblReceiptOffset.Text = "RECEIPT OFFSET";
            // 
            // lblExtraLines
            // 
            this.lblExtraLines.Enabled = false;
            this.lblExtraLines.Location = new System.Drawing.Point(20, 246);
            this.lblExtraLines.Name = "lblExtraLines";
            this.lblExtraLines.Size = new System.Drawing.Size(80, 16);
            this.lblExtraLines.TabIndex = 63;
            this.lblExtraLines.Text = "EXTRA LINES";
            // 
            // lblWideLines
            // 
            this.lblWideLines.Enabled = false;
            this.lblWideLines.Location = new System.Drawing.Point(20, 296);
            this.lblWideLines.Name = "lblWideLines";
            this.lblWideLines.Size = new System.Drawing.Size(190, 16);
            this.lblWideLines.TabIndex = 62;
            this.lblWideLines.Text = "NUMBER OF LINES PER RECEIPT?";
            // 
            // fraTeller
            // 
            this.fraTeller.Controls.Add(this.chkTellerCheckReport);
            this.fraTeller.Controls.Add(this.chkTellerNewPage);
            this.fraTeller.Controls.Add(this.cmbTellerUsed);
            this.fraTeller.Controls.Add(this.chkTellerWithAudit);
            this.fraTeller.Controls.Add(this.chkTellerSeparate);
            this.fraTeller.Controls.Add(this.chkTellerReportSeq);
            this.fraTeller.Controls.Add(this.Label1);
            this.fraTeller.Location = new System.Drawing.Point(30, 90);
            this.fraTeller.Name = "fraTeller";
            this.fraTeller.Size = new System.Drawing.Size(500, 312);
            this.fraTeller.TabIndex = 58;
            this.fraTeller.Text = "Teller Options";
            this.fraTeller.Visible = false;
            // 
            // chkTellerCheckReport
            // 
            this.chkTellerCheckReport.Enabled = false;
            this.chkTellerCheckReport.Location = new System.Drawing.Point(20, 266);
            this.chkTellerCheckReport.Name = "chkTellerCheckReport";
            this.chkTellerCheckReport.Size = new System.Drawing.Size(234, 22);
            this.chkTellerCheckReport.TabIndex = 26;
            this.chkTellerCheckReport.Text = "Print Check Report For Each Teller";
            this.chkTellerCheckReport.CheckedChanged += new System.EventHandler(this.chkTellerCheckReport_CheckedChanged);
            // 
            // chkTellerNewPage
            // 
            this.chkTellerNewPage.Enabled = false;
            this.chkTellerNewPage.Location = new System.Drawing.Point(20, 134);
            this.chkTellerNewPage.Name = "chkTellerNewPage";
            this.chkTellerNewPage.Size = new System.Drawing.Size(177, 22);
            this.chkTellerNewPage.TabIndex = 23;
            this.chkTellerNewPage.Text = "New page for each Teller";
            this.chkTellerNewPage.CheckedChanged += new System.EventHandler(this.chkTellerNewPage_CheckedChanged);
            // 
            // cmbTellerUsed
            // 
            this.cmbTellerUsed.BackColor = System.Drawing.SystemColors.Window;
            this.cmbTellerUsed.Location = new System.Drawing.Point(210, 30);
            this.cmbTellerUsed.Name = "cmbTellerUsed";
            this.cmbTellerUsed.Size = new System.Drawing.Size(270, 40);
            this.cmbTellerUsed.Sorted = true;
            this.cmbTellerUsed.TabIndex = 21;
            this.cmbTellerUsed.SelectedIndexChanged += new System.EventHandler(this.cmbTellerUsed_SelectedIndexChanged);
            // 
            // chkTellerWithAudit
            // 
            this.chkTellerWithAudit.Enabled = false;
            this.chkTellerWithAudit.Location = new System.Drawing.Point(20, 90);
            this.chkTellerWithAudit.Name = "chkTellerWithAudit";
            this.chkTellerWithAudit.Size = new System.Drawing.Size(243, 22);
            this.chkTellerWithAudit.TabIndex = 22;
            this.chkTellerWithAudit.Text = "Teller Report printed with Daily Audit";
            this.chkTellerWithAudit.CheckedChanged += new System.EventHandler(this.chkTellerWithAudit_CheckedChanged);
            // 
            // chkTellerSeparate
            // 
            this.chkTellerSeparate.Enabled = false;
            this.chkTellerSeparate.Location = new System.Drawing.Point(20, 178);
            this.chkTellerSeparate.Name = "chkTellerSeparate";
            this.chkTellerSeparate.Size = new System.Drawing.Size(190, 22);
            this.chkTellerSeparate.TabIndex = 24;
            this.chkTellerSeparate.Text = "Cash Tellers out separately";
            this.chkTellerSeparate.CheckedChanged += new System.EventHandler(this.chkTellerSeparate_CheckedChanged);
            // 
            // chkTellerReportSeq
            // 
            this.chkTellerReportSeq.Enabled = false;
            this.chkTellerReportSeq.Location = new System.Drawing.Point(20, 222);
            this.chkTellerReportSeq.Name = "chkTellerReportSeq";
            this.chkTellerReportSeq.Size = new System.Drawing.Size(213, 22);
            this.chkTellerReportSeq.TabIndex = 25;
            this.chkTellerReportSeq.Text = "Print Teller Report by Receipt #";
            this.chkTellerReportSeq.CheckedChanged += new System.EventHandler(this.chkTellerReportSeq_CheckedChanged);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(105, 16);
            this.Label1.TabIndex = 59;
            this.Label1.Text = "TELLER ID USED?";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.mnuProcessSaveExit,
            this.mnuProcessSpacer,
            this.mnuProcessExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuProcessSave.Text = "Save";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // mnuProcessSaveExit
            // 
            this.mnuProcessSaveExit.Index = 1;
            this.mnuProcessSaveExit.Name = "mnuProcessSaveExit";
            this.mnuProcessSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSaveExit.Text = "Save & Exit";
            this.mnuProcessSaveExit.Click += new System.EventHandler(this.mnuProcessSaveExit_Click);
            // 
            // mnuProcessSpacer
            // 
            this.mnuProcessSpacer.Index = 2;
            this.mnuProcessSpacer.Name = "mnuProcessSpacer";
            this.mnuProcessSpacer.Text = "-";
            // 
            // mnuProcessExit
            // 
            this.mnuProcessExit.Index = 3;
            this.mnuProcessExit.Name = "mnuProcessExit";
            this.mnuProcessExit.Text = "Exit";
            this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(484, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSaveExit_Click);
            // 
            // frmCustomize
            // 
            this.ClientSize = new System.Drawing.Size(1068, 648);
            this.KeyPreview = true;
            this.Name = "frmCustomize";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Customize";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomize_Load);
            this.Activated += new System.EventHandler(this.frmCustomize_Activated);
            this.Resize += new System.EventHandler(this.frmCustomize_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomize_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraEPayments)).EndInit();
            this.fraEPayments.ResumeLayout(false);
            this.fraEPayments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveServiceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddServiceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowServiceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSeperateCreditCardCashAccount)).EndInit();
            this.fraSeperateCreditCardCashAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSeperateCreditCardCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraGatewayInfo_IME)).EndInit();
            this.fraGatewayInfo_IME.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ServiceCodesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraGatewayInfo_ICL)).EndInit();
            this.fraGatewayInfo_ICL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEPymtTestMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAuditPassword)).EndInit();
            this.fraAuditPassword.ResumeLayout(false);
            this.fraAuditPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptCloseOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintCCReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCrossCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAuditAR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAuditUT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAuditRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUSLExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExclusive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepositEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPartialAudit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCloseMV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDailyAuditByAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAuditPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOther)).EndInit();
            this.fraOther.ResumeLayout(false);
            this.fraOther.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRoutNumsInBnkLst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSortTypeAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkClearPaidBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCrossCheck)).EndInit();
            this.fraCrossCheck.ResumeLayout(false);
            this.fraCrossCheck.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCrossCheckDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMVCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBreakdownCK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAlign)).EndInit();
            this.fraAlign.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraReset)).EndInit();
            this.fraReset.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraCD)).EndInit();
            this.fraCD.ResumeLayout(false);
            this.fraCD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCDConnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraComment)).EndInit();
            this.fraComment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraWidth)).EndInit();
            this.fraWidth.ResumeLayout(false);
            this.fraWidth.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceiptPrinterPrompt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowControlFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkThinReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUppercaseTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCompactReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNoMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSP200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTeller)).EndInit();
            this.fraTeller.ResumeLayout(false);
            this.fraTeller.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerCheckReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerNewPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerWithAudit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerSeparate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTellerReportSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
        public FCCheckBox chkShowServiceCode;
        public FCGrid ServiceCodesGrid;
        private FCButton btnRemoveServiceCode;
        private FCButton btnAddServiceCode;
    }
}
