﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using TWCR0000.Reports;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarReceiptSearchSummary.
	/// </summary>
	public partial class sarReceiptSearchSummary : FCSectionReport
	{
		private bool firstRecord = true;
		private int counter = 0;
		private List<ReceiptSearchSummaryInformation> reportData = new List<ReceiptSearchSummaryInformation>();

		public sarReceiptSearchSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				if (firstRecord)
				{
					firstRecord = false;
					counter = 0;
				}
				else
				{
					counter++;
				}

				if (counter < reportData.Count())
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show(
					"Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
					Information.Err(ex).Description + ".", "Fetch Data Error", MessageBoxButtons.OK,
					MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				reportData =
					((arReceiptSearch) ParentReport).reportService.SummarizeData(((arReceiptSearch) ParentReport)
						.reportData).ToList();

				firstRecord = true;
				SetTitles();
				frmWait.InstancePtr.IncrementProgress();

				if (reportData.Count() == 0)
				{
					((arReceiptSearch) ParentReport).NoRecords();
				}
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show(
					"Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
					Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK,
					MessageBoxIcon.Hand);
			}
		}

		private void SetTitles()
		{
			try
			{
				switch (((arReceiptSearch) ParentReport).reportService.ReportSetup.SummarizeBy)
				{
					case ReceiptSearchSummarizeByOption.Type:
					{
						// type
						lblHeader.Text = "Type";
						lblHeader2.Text = "";
						break;
					}

					case ReceiptSearchSummarizeByOption.Date:
					{
						lblHeader.Text = "Date";
						lblHeader2.Text = "";
						break;
					}

					case ReceiptSearchSummarizeByOption.Teller:
					{
						lblHeader.Text = "Teller";
						lblHeader2.Text = "";
						break;
					}

					case ReceiptSearchSummarizeByOption.TypeAndDate:
					{
						// type and date
						lblHeader.Text = "Type";
						lblHeader.Text = "Date";
						break;
					}

					case ReceiptSearchSummarizeByOption.TypeAndTeller:
					{
						// type and teller
						lblHeader.Text = "Type";
						lblHeader2.Text = "Teller";
						break;
					}

					case ReceiptSearchSummarizeByOption.DateAndTeller:
					{
						// date and teller
						lblHeader.Text = "Date";
						lblHeader2.Text = "Teller";
						break;
					}
				}
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show(
					"Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
					Information.Err(ex).Description + ".", "Set Title Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			switch (((arReceiptSearch) ParentReport).reportService.ReportSetup.SummarizeBy)
			{
				case ReceiptSearchSummarizeByOption.Type:
				{
					// type
					fldHeader.Text = reportData[counter].TypeCode.ToString() + " - " + reportData[counter].TypeTitle;
					fldHeader2.Text = "";
					break;
				}

				case ReceiptSearchSummarizeByOption.Date:
				{
					fldHeader.Text = reportData[counter].TransactionDate.Value.ToShortDateString();
					fldHeader2.Text = "";
					break;
				}

				case ReceiptSearchSummarizeByOption.Teller:
				{
					fldHeader.Text = reportData[counter].TellerId;
					fldHeader2.Text = "";
					break;
				}

				case ReceiptSearchSummarizeByOption.TypeAndDate:
				{
					// type and date
					fldHeader.Text = reportData[counter].TypeCode.ToString() + " - " + reportData[counter].TypeTitle;
					fldHeader2.Text = reportData[counter].TransactionDate.Value.ToShortDateString();
					break;
				}

				case ReceiptSearchSummarizeByOption.TypeAndTeller:
				{
					// type and teller
					fldHeader.Text = reportData[counter].TypeCode.ToString() + " - " + reportData[counter].TypeTitle;
					fldHeader2.Text = reportData[counter].TellerId;
					break;
				}

				case ReceiptSearchSummarizeByOption.DateAndTeller:
				{
					// date and teller
					fldHeader.Text = reportData[counter].TransactionDate.Value.ToShortDateString();
					fldHeader2.Text = reportData[counter].TellerId;
					break;
				}
			}

			fldCount.Text = reportData[counter].Count.ToString("#,##0");
			fldAmount.Text = reportData[counter].Amount.ToString("#,##0.00");

			if (((arReceiptSearch) ParentReport).reportService.ReportSetup.SummaryDetailLevel ==
			    SummaryDetailOption.Detail)
			{
				var subReportData = ((arReceiptSearch) ParentReport).reportService
					.GetSummaryDetailData(reportData[counter], ((arReceiptSearch) ParentReport).reportData).ToList();
				if (subReportData.Count > 0)
				{
					sarSummaryDetail.Report = new sarReceiptSearchSummaryDetail(subReportData);
					sarSummaryDetail.Visible = true;
				}
				else
				{
					sarSummaryDetail.Report = null;
					sarSummaryDetail.Visible = false;
				}
			}
			else
			{
				sarSummaryDetail.Report = null;
				sarSummaryDetail.Visible = false;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalCount.Text = reportData.Select(x => x.Count).DefaultIfEmpty(0).Sum().ToString("#,##0");
			fldTotalAmount.Text = reportData.Select(x => x.Amount).DefaultIfEmpty(0).Sum().ToString("#,##0.00");
		}
	}
}