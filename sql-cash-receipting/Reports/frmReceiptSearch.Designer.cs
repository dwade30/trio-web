//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmReceiptSearch : BaseForm
	{
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCListBox lstTypes;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCLabel lblReportName;
		public fecherFoundation.FCButton cmdClear;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileNew;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiptSearch));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.lblReport = new fecherFoundation.FCLabel();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.lstTypes = new fecherFoundation.FCListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.lblReportName = new fecherFoundation.FCLabel();
            this.cmdClear = new fecherFoundation.FCButton();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.lblTimePeriod = new fecherFoundation.FCLabel();
            this.lblReportDetail = new fecherFoundation.FCLabel();
            this.lblReceiptDetail = new fecherFoundation.FCLabel();
            this.lblSummaryDetailLevel = new fecherFoundation.FCLabel();
            this.lblSummarizeBy = new fecherFoundation.FCLabel();
            this.chkShowTenderedAmounts = new fecherFoundation.FCCheckBox();
            this.cboTimePeriod = new Wisej.Web.ComboBox();
            this.cboReceiptDetail = new Wisej.Web.ComboBox();
            this.cboReportDetail = new Wisej.Web.ComboBox();
            this.cboSummaryDetail = new Wisej.Web.ComboBox();
            this.cboSummarizeBy = new Wisej.Web.ComboBox();
            this.lblReceiptTypes = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.lblActualDate = new fecherFoundation.FCLabel();
            this.cboCloseoutEnd = new Wisej.Web.ComboBox();
            this.cboCloseoutStart = new Wisej.Web.ComboBox();
            this.lblCloseout = new fecherFoundation.FCLabel();
            this.cboTellerId = new Wisej.Web.ComboBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.txtReceiptNumberStart = new Wisej.Web.TextBox();
            this.label3 = new fecherFoundation.FCLabel();
            this.txtReceiptNumberEnd = new Wisej.Web.TextBox();
            this.cboReceiptTypeEnd = new Wisej.Web.ComboBox();
            this.cboReceiptTypeStart = new Wisej.Web.ComboBox();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.txtReference = new Wisej.Web.TextBox();
            this.lblReference = new fecherFoundation.FCLabel();
            this.cboNameSearchOption = new Wisej.Web.ComboBox();
            this.lblName = new fecherFoundation.FCLabel();
            this.txtName = new Wisej.Web.TextBox();
            this.txtControl1 = new Wisej.Web.TextBox();
            this.lblControl1 = new fecherFoundation.FCLabel();
            this.txtControl2 = new Wisej.Web.TextBox();
            this.lblControl2 = new fecherFoundation.FCLabel();
            this.txtControl3 = new Wisej.Web.TextBox();
            this.lblControl3 = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.cboMotorVehicleType = new Wisej.Web.ComboBox();
            this.lblMotorVehicleType = new fecherFoundation.FCLabel();
            this.txtReceiptTotal = new Wisej.Web.TextBox();
            this.cboReceiptTotalOption = new Wisej.Web.ComboBox();
            this.lblReceiptTotal = new fecherFoundation.FCLabel();
            this.txtTenderedTotal = new Wisej.Web.TextBox();
            this.cboTenderedTotalOption = new Wisej.Web.ComboBox();
            this.lblTenderedTotal = new fecherFoundation.FCLabel();
            this.txtPaidBy = new Wisej.Web.TextBox();
            this.cboPaidByOption = new Wisej.Web.ComboBox();
            this.lblPaidBy = new fecherFoundation.FCLabel();
            this.txtCheckNumber = new Wisej.Web.TextBox();
            this.lblCheckNumber = new fecherFoundation.FCLabel();
            this.cboDateToShow = new Wisej.Web.ComboBox();
            this.lblDateToShow = new fecherFoundation.FCLabel();
            this.cboBank = new Wisej.Web.ComboBox();
            this.lblBank = new fecherFoundation.FCLabel();
            this.txtComment = new Wisej.Web.TextBox();
            this.lblComment = new fecherFoundation.FCLabel();
            this.cboPaymentType = new Wisej.Web.ComboBox();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.vsAccount = new fecherFoundation.FCGrid();
            this.txtActualDateStart = new fecherFoundation.FCDateTimePicker();
            this.txtActualTimeStart = new fecherFoundation.FCDateTimePicker();
            this.txtActualTimeEnd = new fecherFoundation.FCDateTimePicker();
            this.txtActualDateEnd = new fecherFoundation.FCDateTimePicker();
            this.lblActualTime = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTenderedAmounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 1017);
            this.BottomPanel.Size = new System.Drawing.Size(1024, 94);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblActualTime);
            this.ClientArea.Controls.Add(this.txtActualDateEnd);
            this.ClientArea.Controls.Add(this.txtActualTimeEnd);
            this.ClientArea.Controls.Add(this.txtActualTimeStart);
            this.ClientArea.Controls.Add(this.txtActualDateStart);
            this.ClientArea.Controls.Add(this.vsAccount);
            this.ClientArea.Controls.Add(this.cboPaymentType);
            this.ClientArea.Controls.Add(this.fcLabel4);
            this.ClientArea.Controls.Add(this.txtComment);
            this.ClientArea.Controls.Add(this.lblComment);
            this.ClientArea.Controls.Add(this.cboBank);
            this.ClientArea.Controls.Add(this.lblBank);
            this.ClientArea.Controls.Add(this.cboDateToShow);
            this.ClientArea.Controls.Add(this.lblDateToShow);
            this.ClientArea.Controls.Add(this.txtCheckNumber);
            this.ClientArea.Controls.Add(this.lblCheckNumber);
            this.ClientArea.Controls.Add(this.txtPaidBy);
            this.ClientArea.Controls.Add(this.cboPaidByOption);
            this.ClientArea.Controls.Add(this.lblPaidBy);
            this.ClientArea.Controls.Add(this.txtTenderedTotal);
            this.ClientArea.Controls.Add(this.cboTenderedTotalOption);
            this.ClientArea.Controls.Add(this.lblTenderedTotal);
            this.ClientArea.Controls.Add(this.txtReceiptTotal);
            this.ClientArea.Controls.Add(this.cboReceiptTotalOption);
            this.ClientArea.Controls.Add(this.lblReceiptTotal);
            this.ClientArea.Controls.Add(this.cboMotorVehicleType);
            this.ClientArea.Controls.Add(this.lblMotorVehicleType);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Controls.Add(this.txtControl3);
            this.ClientArea.Controls.Add(this.lblControl3);
            this.ClientArea.Controls.Add(this.txtControl2);
            this.ClientArea.Controls.Add(this.lblControl2);
            this.ClientArea.Controls.Add(this.txtControl1);
            this.ClientArea.Controls.Add(this.lblControl1);
            this.ClientArea.Controls.Add(this.txtName);
            this.ClientArea.Controls.Add(this.cboNameSearchOption);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.txtReference);
            this.ClientArea.Controls.Add(this.lblReference);
            this.ClientArea.Controls.Add(this.cboReceiptTypeEnd);
            this.ClientArea.Controls.Add(this.cboReceiptTypeStart);
            this.ClientArea.Controls.Add(this.fcLabel2);
            this.ClientArea.Controls.Add(this.txtReceiptNumberStart);
            this.ClientArea.Controls.Add(this.label3);
            this.ClientArea.Controls.Add(this.txtReceiptNumberEnd);
            this.ClientArea.Controls.Add(this.cboTellerId);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.cboCloseoutEnd);
            this.ClientArea.Controls.Add(this.cboCloseoutStart);
            this.ClientArea.Controls.Add(this.lblCloseout);
            this.ClientArea.Controls.Add(this.lblActualDate);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.fcLabel3);
            this.ClientArea.Controls.Add(this.lstSort);
            this.ClientArea.Controls.Add(this.lstTypes);
            this.ClientArea.Controls.Add(this.lblReceiptTypes);
            this.ClientArea.Controls.Add(this.cboSummarizeBy);
            this.ClientArea.Controls.Add(this.cboSummaryDetail);
            this.ClientArea.Controls.Add(this.cboReportDetail);
            this.ClientArea.Controls.Add(this.cboReceiptDetail);
            this.ClientArea.Controls.Add(this.cboTimePeriod);
            this.ClientArea.Controls.Add(this.chkShowTenderedAmounts);
            this.ClientArea.Controls.Add(this.lblSummaryDetailLevel);
            this.ClientArea.Controls.Add(this.lblSummarizeBy);
            this.ClientArea.Controls.Add(this.lblReceiptDetail);
            this.ClientArea.Controls.Add(this.lblReportDetail);
            this.ClientArea.Controls.Add(this.lblTimePeriod);
            this.ClientArea.Size = new System.Drawing.Size(1044, 621);
            this.ClientArea.Controls.SetChildIndex(this.lblTimePeriod, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReportDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReceiptDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSummarizeBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSummaryDetailLevel, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkShowTenderedAmounts, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboTimePeriod, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboReceiptDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboReportDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboSummaryDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboSummarizeBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReceiptTypes, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstTypes, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstSort, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblActualDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCloseout, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboCloseoutStart, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboCloseoutEnd, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboTellerId, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReceiptNumberEnd, 0);
            this.ClientArea.Controls.SetChildIndex(this.label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReceiptNumberStart, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel2, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboReceiptTypeStart, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboReceiptTypeEnd, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReference, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReference, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblName, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboNameSearchOption, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtName, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblControl1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtControl1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblControl2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtControl2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblControl3, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtControl3, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblMotorVehicleType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboMotorVehicleType, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReceiptTotal, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboReceiptTotalOption, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReceiptTotal, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTenderedTotal, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboTenderedTotalOption, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTenderedTotal, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboPaidByOption, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCheckNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCheckNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDateToShow, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboDateToShow, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblBank, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboBank, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel4, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboPaymentType, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtActualDateStart, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtActualTimeStart, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtActualTimeEnd, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtActualDateEnd, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblActualTime, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(1044, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(168, 28);
            this.HeaderText.Text = "Receipt Search";
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Location = new System.Drawing.Point(5, 19);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(87, 17);
            this.lblReport.Text = "LBLREPORT";
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Save Custom Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(250, 40);
            this.cmbReport.TabIndex = 55;
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // lstTypes
            // 
            this.lstTypes.BackColor = System.Drawing.SystemColors.Window;
            this.lstTypes.CheckBoxes = true;
            this.lstTypes.Location = new System.Drawing.Point(683, 223);
            this.lstTypes.Name = "lstTypes";
            this.lstTypes.Size = new System.Drawing.Size(294, 138);
            this.lstTypes.Sorted = true;
            this.lstTypes.Sorting = Wisej.Web.SortOrder.Ascending;
            this.lstTypes.Style = 1;
            this.lstTypes.TabIndex = 45;
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(407, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(126, 40);
            this.cmdPrint.TabIndex = 42;
            this.cmdPrint.Text = "Print Preview";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(687, 403);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(290, 138);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 50;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cboSavedReport);
            this.Frame2.Controls.Add(this.cmbReport);
            this.Frame2.Controls.Add(this.cmdAdd);
            this.Frame2.Controls.Add(this.lblReportName);
            this.Frame2.Location = new System.Drawing.Point(687, 561);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(290, 185);
            this.Frame2.TabIndex = 54;
            this.Frame2.Text = "Report";
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(250, 40);
            this.cboSavedReport.TabIndex = 56;
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(20, 130);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(250, 40);
            this.cmdAdd.TabIndex = 57;
            this.cmdAdd.Text = "Add Custom Report to Library";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // lblReportName
            // 
            this.lblReportName.Location = new System.Drawing.Point(20, 94);
            this.lblReportName.Name = "lblReportName";
            this.lblReportName.Size = new System.Drawing.Size(263, 22);
            this.lblReportName.TabIndex = 20;
            this.lblReportName.Visible = false;
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(900, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(141, 24);
            this.cmdClear.TabIndex = 4;
            this.cmdClear.Text = "Clear Search Criteria";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileNew,
            this.mnuLayout,
            this.mnuFileSeperator2,
            this.mnuClear,
            this.mnuPrint,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileNew
            // 
            this.mnuFileNew.Index = 0;
            this.mnuFileNew.Name = "mnuFileNew";
            this.mnuFileNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.mnuFileNew.Text = "New";
            // 
            // mnuLayout
            // 
            this.mnuLayout.Enabled = false;
            this.mnuLayout.Index = 1;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Options";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 2;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 3;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 2;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 3;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 4;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print Preview";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 5;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            // 
            // lblTimePeriod
            // 
            this.lblTimePeriod.AutoSize = true;
            this.lblTimePeriod.Location = new System.Drawing.Point(40, 34);
            this.lblTimePeriod.Name = "lblTimePeriod";
            this.lblTimePeriod.Size = new System.Drawing.Size(168, 15);
            this.lblTimePeriod.TabIndex = 1002;
            this.lblTimePeriod.Text = "TIME PERIOD TO SEARCH";
            this.lblTimePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReportDetail
            // 
            this.lblReportDetail.AutoSize = true;
            this.lblReportDetail.Location = new System.Drawing.Point(40, 87);
            this.lblReportDetail.Name = "lblReportDetail";
            this.lblReportDetail.Size = new System.Drawing.Size(151, 15);
            this.lblReportDetail.TabIndex = 1004;
            this.lblReportDetail.Text = "REPORT DETAIL LEVEL";
            this.lblReportDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReceiptDetail
            // 
            this.lblReceiptDetail.AutoSize = true;
            this.lblReceiptDetail.Location = new System.Drawing.Point(40, 139);
            this.lblReceiptDetail.Name = "lblReceiptDetail";
            this.lblReceiptDetail.Size = new System.Drawing.Size(154, 15);
            this.lblReceiptDetail.TabIndex = 1006;
            this.lblReceiptDetail.Text = "RECEIPT DETAIL LEVEL";
            this.lblReceiptDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSummaryDetailLevel
            // 
            this.lblSummaryDetailLevel.AutoSize = true;
            this.lblSummaryDetailLevel.Location = new System.Drawing.Point(559, 87);
            this.lblSummaryDetailLevel.Name = "lblSummaryDetailLevel";
            this.lblSummaryDetailLevel.Size = new System.Drawing.Size(164, 15);
            this.lblSummaryDetailLevel.TabIndex = 1012;
            this.lblSummaryDetailLevel.Text = "SUMMARY DETAIL LEVEL";
            this.lblSummaryDetailLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSummarizeBy
            // 
            this.lblSummarizeBy.AutoSize = true;
            this.lblSummarizeBy.Location = new System.Drawing.Point(559, 35);
            this.lblSummarizeBy.Name = "lblSummarizeBy";
            this.lblSummarizeBy.Size = new System.Drawing.Size(103, 15);
            this.lblSummarizeBy.TabIndex = 1010;
            this.lblSummarizeBy.Text = "SUMMARIZE BY";
            this.lblSummarizeBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkShowTenderedAmounts
            // 
            this.chkShowTenderedAmounts.Location = new System.Drawing.Point(231, 183);
            this.chkShowTenderedAmounts.Name = "chkShowTenderedAmounts";
            this.chkShowTenderedAmounts.Size = new System.Drawing.Size(179, 22);
            this.chkShowTenderedAmounts.TabIndex = 4;
            this.chkShowTenderedAmounts.Text = "Show Tendered Amounts";
            // 
            // cboTimePeriod
            // 
            this.cboTimePeriod.AutoSize = false;
            this.cboTimePeriod.DisplayMember = "Description";
            this.cboTimePeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboTimePeriod.Location = new System.Drawing.Point(231, 22);
            this.cboTimePeriod.Name = "cboTimePeriod";
            this.cboTimePeriod.Size = new System.Drawing.Size(201, 40);
            this.cboTimePeriod.TabIndex = 1;
            this.cboTimePeriod.ValueMember = "ID";
            // 
            // cboReceiptDetail
            // 
            this.cboReceiptDetail.AutoSize = false;
            this.cboReceiptDetail.DisplayMember = "Description";
            this.cboReceiptDetail.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboReceiptDetail.Location = new System.Drawing.Point(231, 127);
            this.cboReceiptDetail.Name = "cboReceiptDetail";
            this.cboReceiptDetail.Size = new System.Drawing.Size(201, 40);
            this.cboReceiptDetail.TabIndex = 3;
            this.cboReceiptDetail.ValueMember = "ID";
            this.cboReceiptDetail.SelectedIndexChanged += new System.EventHandler(this.cboReceiptDetail_SelectedIndexChanged);
            // 
            // cboReportDetail
            // 
            this.cboReportDetail.AutoSize = false;
            this.cboReportDetail.DisplayMember = "Description";
            this.cboReportDetail.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboReportDetail.Location = new System.Drawing.Point(231, 75);
            this.cboReportDetail.Name = "cboReportDetail";
            this.cboReportDetail.Size = new System.Drawing.Size(201, 40);
            this.cboReportDetail.TabIndex = 2;
            this.cboReportDetail.ValueMember = "ID";
            this.cboReportDetail.SelectedIndexChanged += new System.EventHandler(this.cboReportDetail_SelectedIndexChanged);
            // 
            // cboSummaryDetail
            // 
            this.cboSummaryDetail.AutoSize = false;
            this.cboSummaryDetail.DisplayMember = "Description";
            this.cboSummaryDetail.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboSummaryDetail.Location = new System.Drawing.Point(751, 75);
            this.cboSummaryDetail.Name = "cboSummaryDetail";
            this.cboSummaryDetail.Size = new System.Drawing.Size(201, 40);
            this.cboSummaryDetail.TabIndex = 6;
            this.cboSummaryDetail.ValueMember = "ID";
            // 
            // cboSummarizeBy
            // 
            this.cboSummarizeBy.AutoSize = false;
            this.cboSummarizeBy.DisplayMember = "Description";
            this.cboSummarizeBy.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboSummarizeBy.Location = new System.Drawing.Point(751, 21);
            this.cboSummarizeBy.Name = "cboSummarizeBy";
            this.cboSummarizeBy.Size = new System.Drawing.Size(201, 40);
            this.cboSummarizeBy.TabIndex = 5;
            this.cboSummarizeBy.ValueMember = "ID";
            this.cboSummarizeBy.SelectedIndexChanged += new System.EventHandler(this.cboSummarizeBy_SelectedIndexChanged);
            // 
            // lblReceiptTypes
            // 
            this.lblReceiptTypes.AutoSize = true;
            this.lblReceiptTypes.Location = new System.Drawing.Point(683, 201);
            this.lblReceiptTypes.Name = "lblReceiptTypes";
            this.lblReceiptTypes.Size = new System.Drawing.Size(107, 15);
            this.lblReceiptTypes.TabIndex = 1020;
            this.lblReceiptTypes.Text = "RECEIPT TYPES";
            this.lblReceiptTypes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(687, 382);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(132, 15);
            this.fcLabel3.TabIndex = 1021;
            this.fcLabel3.Text = "FIELDS TO SORT BY";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblActualDate
            // 
            this.lblActualDate.Location = new System.Drawing.Point(40, 238);
            this.lblActualDate.Name = "lblActualDate";
            this.lblActualDate.Size = new System.Drawing.Size(160, 40);
            this.lblActualDate.TabIndex = 1022;
            this.lblActualDate.Text = "ACTUAL DATE";
            this.lblActualDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboCloseoutEnd
            // 
            this.cboCloseoutEnd.AutoSize = false;
            this.cboCloseoutEnd.DisplayMember = "Description";
            this.cboCloseoutEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboCloseoutEnd.Location = new System.Drawing.Point(451, 344);
            this.cboCloseoutEnd.Name = "cboCloseoutEnd";
            this.cboCloseoutEnd.Size = new System.Drawing.Size(201, 40);
            this.cboCloseoutEnd.TabIndex = 14;
            this.cboCloseoutEnd.ValueMember = "ID";
            // 
            // cboCloseoutStart
            // 
            this.cboCloseoutStart.AutoSize = false;
            this.cboCloseoutStart.DisplayMember = "Description";
            this.cboCloseoutStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboCloseoutStart.Location = new System.Drawing.Point(231, 344);
            this.cboCloseoutStart.Name = "cboCloseoutStart";
            this.cboCloseoutStart.Size = new System.Drawing.Size(201, 40);
            this.cboCloseoutStart.TabIndex = 13;
            this.cboCloseoutStart.ValueMember = "ID";
            // 
            // lblCloseout
            // 
            this.lblCloseout.Location = new System.Drawing.Point(40, 344);
            this.lblCloseout.Name = "lblCloseout";
            this.lblCloseout.Size = new System.Drawing.Size(180, 40);
            this.lblCloseout.TabIndex = 1024;
            this.lblCloseout.Text = "CLOSE OUT DATE";
            this.lblCloseout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboTellerId
            // 
            this.cboTellerId.AutoSize = false;
            this.cboTellerId.DisplayMember = "Description";
            this.cboTellerId.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboTellerId.Location = new System.Drawing.Point(231, 870);
            this.cboTellerId.Name = "cboTellerId";
            this.cboTellerId.Size = new System.Drawing.Size(201, 40);
            this.cboTellerId.TabIndex = 36;
            this.cboTellerId.ValueMember = "ID";
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(40, 870);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(180, 40);
            this.fcLabel1.TabIndex = 1027;
            this.fcLabel1.Text = "TELLER ID";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtReceiptNumberStart
            // 
            this.txtReceiptNumberStart.AutoSize = false;
            this.txtReceiptNumberStart.Location = new System.Drawing.Point(231, 448);
            this.txtReceiptNumberStart.Name = "txtReceiptNumberStart";
            this.txtReceiptNumberStart.Size = new System.Drawing.Size(201, 40);
            this.txtReceiptNumberStart.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(40, 448);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 40);
            this.label3.TabIndex = 1029;
            this.label3.Text = "RECEIPT NUMBER";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtReceiptNumberEnd
            // 
            this.txtReceiptNumberEnd.AutoSize = false;
            this.txtReceiptNumberEnd.Location = new System.Drawing.Point(451, 448);
            this.txtReceiptNumberEnd.Name = "txtReceiptNumberEnd";
            this.txtReceiptNumberEnd.Size = new System.Drawing.Size(201, 40);
            this.txtReceiptNumberEnd.TabIndex = 19;
            // 
            // cboReceiptTypeEnd
            // 
            this.cboReceiptTypeEnd.AutoSize = false;
            this.cboReceiptTypeEnd.DisplayMember = "Description";
            this.cboReceiptTypeEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboReceiptTypeEnd.Location = new System.Drawing.Point(451, 396);
            this.cboReceiptTypeEnd.Name = "cboReceiptTypeEnd";
            this.cboReceiptTypeEnd.Size = new System.Drawing.Size(201, 40);
            this.cboReceiptTypeEnd.TabIndex = 16;
            this.cboReceiptTypeEnd.ValueMember = "ID";
            // 
            // cboReceiptTypeStart
            // 
            this.cboReceiptTypeStart.AutoSize = false;
            this.cboReceiptTypeStart.DisplayMember = "Description";
            this.cboReceiptTypeStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboReceiptTypeStart.Location = new System.Drawing.Point(231, 396);
            this.cboReceiptTypeStart.Name = "cboReceiptTypeStart";
            this.cboReceiptTypeStart.Size = new System.Drawing.Size(201, 40);
            this.cboReceiptTypeStart.TabIndex = 15;
            this.cboReceiptTypeStart.ValueMember = "ID";
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(40, 396);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(180, 40);
            this.fcLabel2.TabIndex = 1032;
            this.fcLabel2.Text = "RECEIPT TYPE";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtReference
            // 
            this.txtReference.AutoSize = false;
            this.txtReference.Location = new System.Drawing.Point(231, 763);
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(201, 40);
            this.txtReference.TabIndex = 32;
            // 
            // lblReference
            // 
            this.lblReference.Location = new System.Drawing.Point(40, 763);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(160, 40);
            this.lblReference.TabIndex = 1035;
            this.lblReference.Text = "REFERENCE";
            this.lblReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboNameSearchOption
            // 
            this.cboNameSearchOption.AutoSize = false;
            this.cboNameSearchOption.DisplayMember = "Description";
            this.cboNameSearchOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboNameSearchOption.Location = new System.Drawing.Point(231, 500);
            this.cboNameSearchOption.Name = "cboNameSearchOption";
            this.cboNameSearchOption.Size = new System.Drawing.Size(201, 40);
            this.cboNameSearchOption.TabIndex = 20;
            this.cboNameSearchOption.ValueMember = "ID";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(40, 500);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(180, 40);
            this.lblName.TabIndex = 1037;
            this.lblName.Text = "NAME";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtName
            // 
            this.txtName.AutoSize = false;
            this.txtName.Location = new System.Drawing.Point(451, 500);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(201, 40);
            this.txtName.TabIndex = 21;
            // 
            // txtControl1
            // 
            this.txtControl1.AutoSize = false;
            this.txtControl1.Location = new System.Drawing.Point(751, 763);
            this.txtControl1.Name = "txtControl1";
            this.txtControl1.Size = new System.Drawing.Size(201, 40);
            this.txtControl1.TabIndex = 33;
            // 
            // lblControl1
            // 
            this.lblControl1.Location = new System.Drawing.Point(559, 763);
            this.lblControl1.Name = "lblControl1";
            this.lblControl1.Size = new System.Drawing.Size(160, 40);
            this.lblControl1.TabIndex = 1040;
            this.lblControl1.Text = "CONTROL 1";
            this.lblControl1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtControl2
            // 
            this.txtControl2.AutoSize = false;
            this.txtControl2.Location = new System.Drawing.Point(231, 817);
            this.txtControl2.Name = "txtControl2";
            this.txtControl2.Size = new System.Drawing.Size(201, 40);
            this.txtControl2.TabIndex = 34;
            // 
            // lblControl2
            // 
            this.lblControl2.Location = new System.Drawing.Point(40, 817);
            this.lblControl2.Name = "lblControl2";
            this.lblControl2.Size = new System.Drawing.Size(160, 40);
            this.lblControl2.TabIndex = 1042;
            this.lblControl2.Text = "CONTROL 2";
            this.lblControl2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtControl3
            // 
            this.txtControl3.AutoSize = false;
            this.txtControl3.Location = new System.Drawing.Point(751, 817);
            this.txtControl3.Name = "txtControl3";
            this.txtControl3.Size = new System.Drawing.Size(201, 40);
            this.txtControl3.TabIndex = 35;
            // 
            // lblControl3
            // 
            this.lblControl3.Location = new System.Drawing.Point(559, 817);
            this.lblControl3.Name = "lblControl3";
            this.lblControl3.Size = new System.Drawing.Size(160, 40);
            this.lblControl3.TabIndex = 1044;
            this.lblControl3.Text = "CONTROL 3";
            this.lblControl3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(40, 923);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(160, 40);
            this.lblAccount.TabIndex = 1046;
            this.lblAccount.Text = "ACCOUNT";
            this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboMotorVehicleType
            // 
            this.cboMotorVehicleType.AutoSize = false;
            this.cboMotorVehicleType.DisplayMember = "Description";
            this.cboMotorVehicleType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboMotorVehicleType.Location = new System.Drawing.Point(751, 870);
            this.cboMotorVehicleType.Name = "cboMotorVehicleType";
            this.cboMotorVehicleType.Size = new System.Drawing.Size(201, 40);
            this.cboMotorVehicleType.TabIndex = 37;
            this.cboMotorVehicleType.ValueMember = "ID";
            // 
            // lblMotorVehicleType
            // 
            this.lblMotorVehicleType.Location = new System.Drawing.Point(559, 870);
            this.lblMotorVehicleType.Name = "lblMotorVehicleType";
            this.lblMotorVehicleType.Size = new System.Drawing.Size(180, 40);
            this.lblMotorVehicleType.TabIndex = 1048;
            this.lblMotorVehicleType.Text = "MOTOR VEHICLE TYPE";
            this.lblMotorVehicleType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtReceiptTotal
            // 
            this.txtReceiptTotal.AutoSize = false;
            this.txtReceiptTotal.Location = new System.Drawing.Point(451, 604);
            this.txtReceiptTotal.Name = "txtReceiptTotal";
            this.txtReceiptTotal.Size = new System.Drawing.Size(201, 40);
            this.txtReceiptTotal.TabIndex = 25;
            // 
            // cboReceiptTotalOption
            // 
            this.cboReceiptTotalOption.AutoSize = false;
            this.cboReceiptTotalOption.DisplayMember = "Description";
            this.cboReceiptTotalOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboReceiptTotalOption.Location = new System.Drawing.Point(231, 604);
            this.cboReceiptTotalOption.Name = "cboReceiptTotalOption";
            this.cboReceiptTotalOption.Size = new System.Drawing.Size(201, 40);
            this.cboReceiptTotalOption.TabIndex = 24;
            this.cboReceiptTotalOption.ValueMember = "ID";
            // 
            // lblReceiptTotal
            // 
            this.lblReceiptTotal.Location = new System.Drawing.Point(40, 604);
            this.lblReceiptTotal.Name = "lblReceiptTotal";
            this.lblReceiptTotal.Size = new System.Drawing.Size(180, 40);
            this.lblReceiptTotal.TabIndex = 1050;
            this.lblReceiptTotal.Text = "RECEIPT TOTAL";
            this.lblReceiptTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTenderedTotal
            // 
            this.txtTenderedTotal.AutoSize = false;
            this.txtTenderedTotal.Location = new System.Drawing.Point(451, 657);
            this.txtTenderedTotal.Name = "txtTenderedTotal";
            this.txtTenderedTotal.Size = new System.Drawing.Size(201, 40);
            this.txtTenderedTotal.TabIndex = 30;
            // 
            // cboTenderedTotalOption
            // 
            this.cboTenderedTotalOption.AutoSize = false;
            this.cboTenderedTotalOption.DisplayMember = "Description";
            this.cboTenderedTotalOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboTenderedTotalOption.Location = new System.Drawing.Point(231, 657);
            this.cboTenderedTotalOption.Name = "cboTenderedTotalOption";
            this.cboTenderedTotalOption.Size = new System.Drawing.Size(201, 40);
            this.cboTenderedTotalOption.TabIndex = 29;
            this.cboTenderedTotalOption.ValueMember = "ID";
            // 
            // lblTenderedTotal
            // 
            this.lblTenderedTotal.Location = new System.Drawing.Point(40, 657);
            this.lblTenderedTotal.Name = "lblTenderedTotal";
            this.lblTenderedTotal.Size = new System.Drawing.Size(180, 40);
            this.lblTenderedTotal.TabIndex = 1053;
            this.lblTenderedTotal.Text = "TENDERED TOTAL";
            this.lblTenderedTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.AutoSize = false;
            this.txtPaidBy.Location = new System.Drawing.Point(451, 552);
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.Size = new System.Drawing.Size(201, 40);
            this.txtPaidBy.TabIndex = 23;
            // 
            // cboPaidByOption
            // 
            this.cboPaidByOption.AutoSize = false;
            this.cboPaidByOption.DisplayMember = "Description";
            this.cboPaidByOption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPaidByOption.Location = new System.Drawing.Point(231, 552);
            this.cboPaidByOption.Name = "cboPaidByOption";
            this.cboPaidByOption.Size = new System.Drawing.Size(201, 40);
            this.cboPaidByOption.TabIndex = 22;
            this.cboPaidByOption.ValueMember = "ID";
            // 
            // lblPaidBy
            // 
            this.lblPaidBy.Location = new System.Drawing.Point(40, 552);
            this.lblPaidBy.Name = "lblPaidBy";
            this.lblPaidBy.Size = new System.Drawing.Size(180, 40);
            this.lblPaidBy.TabIndex = 1056;
            this.lblPaidBy.Text = "PAID BY";
            this.lblPaidBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCheckNumber
            // 
            this.txtCheckNumber.AutoSize = false;
            this.txtCheckNumber.Location = new System.Drawing.Point(231, 977);
            this.txtCheckNumber.Name = "txtCheckNumber";
            this.txtCheckNumber.Size = new System.Drawing.Size(201, 40);
            this.txtCheckNumber.TabIndex = 40;
            // 
            // lblCheckNumber
            // 
            this.lblCheckNumber.Location = new System.Drawing.Point(40, 977);
            this.lblCheckNumber.Name = "lblCheckNumber";
            this.lblCheckNumber.Size = new System.Drawing.Size(160, 40);
            this.lblCheckNumber.TabIndex = 1059;
            this.lblCheckNumber.Text = "CHECK NUMBER";
            this.lblCheckNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboDateToShow
            // 
            this.cboDateToShow.AutoSize = false;
            this.cboDateToShow.DisplayMember = "Description";
            this.cboDateToShow.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboDateToShow.Location = new System.Drawing.Point(751, 127);
            this.cboDateToShow.Name = "cboDateToShow";
            this.cboDateToShow.Size = new System.Drawing.Size(201, 40);
            this.cboDateToShow.TabIndex = 7;
            this.cboDateToShow.ValueMember = "ID";
            // 
            // lblDateToShow
            // 
            this.lblDateToShow.Location = new System.Drawing.Point(559, 127);
            this.lblDateToShow.Name = "lblDateToShow";
            this.lblDateToShow.Size = new System.Drawing.Size(180, 40);
            this.lblDateToShow.TabIndex = 1061;
            this.lblDateToShow.Text = "DATE TO SHOW";
            this.lblDateToShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboBank
            // 
            this.cboBank.AutoSize = false;
            this.cboBank.DisplayMember = "Description";
            this.cboBank.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboBank.Location = new System.Drawing.Point(751, 977);
            this.cboBank.Name = "cboBank";
            this.cboBank.Size = new System.Drawing.Size(201, 40);
            this.cboBank.TabIndex = 41;
            this.cboBank.ValueMember = "ID";
            // 
            // lblBank
            // 
            this.lblBank.Location = new System.Drawing.Point(559, 977);
            this.lblBank.Name = "lblBank";
            this.lblBank.Size = new System.Drawing.Size(180, 40);
            this.lblBank.TabIndex = 1063;
            this.lblBank.Text = "BANK";
            this.lblBank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtComment
            // 
            this.txtComment.AutoSize = false;
            this.txtComment.Location = new System.Drawing.Point(231, 710);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(201, 40);
            this.txtComment.TabIndex = 31;
            // 
            // lblComment
            // 
            this.lblComment.Location = new System.Drawing.Point(40, 710);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(160, 40);
            this.lblComment.TabIndex = 1065;
            this.lblComment.Text = "COMMENT";
            this.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboPaymentType
            // 
            this.cboPaymentType.AutoSize = false;
            this.cboPaymentType.DisplayMember = "Description";
            this.cboPaymentType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboPaymentType.Location = new System.Drawing.Point(751, 923);
            this.cboPaymentType.Name = "cboPaymentType";
            this.cboPaymentType.Size = new System.Drawing.Size(201, 40);
            this.cboPaymentType.TabIndex = 39;
            this.cboPaymentType.ValueMember = "ID";
            // 
            // fcLabel4
            // 
            this.fcLabel4.Location = new System.Drawing.Point(559, 923);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(180, 40);
            this.fcLabel4.TabIndex = 1067;
            this.fcLabel4.Text = "PAYMENT TYPE";
            this.fcLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vsAccount
            // 
            this.vsAccount.Cols = 1;
            this.vsAccount.ColumnHeadersVisible = false;
            this.vsAccount.FixedCols = 0;
            this.vsAccount.FixedRows = 0;
            this.vsAccount.Location = new System.Drawing.Point(231, 923);
            this.vsAccount.Name = "vsAccount";
            this.vsAccount.RowHeadersVisible = false;
            this.vsAccount.Rows = 1;
            this.vsAccount.Size = new System.Drawing.Size(201, 42);
            this.vsAccount.TabIndex = 38;
            // 
            // txtActualDateStart
            // 
            this.txtActualDateStart.AutoSize = false;
            this.txtActualDateStart.Format = Wisej.Web.DateTimePickerFormat.Short;
            this.txtActualDateStart.Location = new System.Drawing.Point(231, 238);
            this.txtActualDateStart.Name = "txtActualDateStart";
            this.txtActualDateStart.Size = new System.Drawing.Size(199, 40);
            this.txtActualDateStart.TabIndex = 8;
            // 
            // txtActualTimeStart
            // 
            this.txtActualTimeStart.AutoSize = false;
            this.txtActualTimeStart.Format = Wisej.Web.DateTimePickerFormat.Time;
            this.txtActualTimeStart.Location = new System.Drawing.Point(231, 291);
            this.txtActualTimeStart.Name = "txtActualTimeStart";
            this.txtActualTimeStart.ShowCalendar = false;
            this.txtActualTimeStart.ShowUpDown = true;
            this.txtActualTimeStart.Size = new System.Drawing.Size(199, 40);
            this.txtActualTimeStart.TabIndex = 11;
            // 
            // txtActualTimeEnd
            // 
            this.txtActualTimeEnd.AutoSize = false;
            this.txtActualTimeEnd.Format = Wisej.Web.DateTimePickerFormat.Time;
            this.txtActualTimeEnd.Location = new System.Drawing.Point(451, 291);
            this.txtActualTimeEnd.Name = "txtActualTimeEnd";
            this.txtActualTimeEnd.ShowCalendar = false;
            this.txtActualTimeEnd.ShowUpDown = true;
            this.txtActualTimeEnd.Size = new System.Drawing.Size(199, 40);
            this.txtActualTimeEnd.TabIndex = 12;
            // 
            // txtActualDateEnd
            // 
            this.txtActualDateEnd.AutoSize = false;
            this.txtActualDateEnd.Format = Wisej.Web.DateTimePickerFormat.Short;
            this.txtActualDateEnd.Location = new System.Drawing.Point(451, 238);
            this.txtActualDateEnd.Name = "txtActualDateEnd";
            this.txtActualDateEnd.Size = new System.Drawing.Size(199, 40);
            this.txtActualDateEnd.TabIndex = 9;
            // 
            // lblActualTime
            // 
            this.lblActualTime.Location = new System.Drawing.Point(40, 291);
            this.lblActualTime.Name = "lblActualTime";
            this.lblActualTime.Size = new System.Drawing.Size(160, 40);
            this.lblActualTime.TabIndex = 1068;
            this.lblActualTime.Text = "ACTUAL TIME";
            this.lblActualTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmReceiptSearch
            // 
            this.ClientSize = new System.Drawing.Size(1044, 681);
            this.KeyPreview = true;
            this.Name = "frmReceiptSearch";
            this.Text = "Receipt Search";
            this.Load += new System.EventHandler(this.frmReceiptSearch_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReceiptSearch_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTenderedAmounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAccount)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCLabel lblTimePeriod;
		private FCLabel lblSummaryDetailLevel;
		private FCLabel lblSummarizeBy;
		private FCLabel lblReceiptDetail;
		private FCLabel lblReportDetail;
		public FCCheckBox chkShowTenderedAmounts;
		private ComboBox cboReportDetail;
		private ComboBox cboReceiptDetail;
		private ComboBox cboTimePeriod;
		private ComboBox cboSummarizeBy;
		private ComboBox cboSummaryDetail;
		private FCLabel lblReceiptTypes;
		private FCLabel fcLabel3;
		private FCLabel lblActualDate;
		private ComboBox cboCloseoutEnd;
		private ComboBox cboCloseoutStart;
		private FCLabel lblCloseout;
		private ComboBox cboTellerId;
		private FCLabel fcLabel1;
		private TextBox txtReceiptNumberStart;
		private FCLabel label3;
		private TextBox txtReceiptNumberEnd;
		private FCLabel lblAccount;
		private TextBox txtControl3;
		private FCLabel lblControl3;
		private TextBox txtControl2;
		private FCLabel lblControl2;
		private TextBox txtControl1;
		private FCLabel lblControl1;
		private TextBox txtName;
		private ComboBox cboNameSearchOption;
		private FCLabel lblName;
		private TextBox txtReference;
		private FCLabel lblReference;
		private ComboBox cboReceiptTypeEnd;
		private ComboBox cboReceiptTypeStart;
		private FCLabel fcLabel2;
		private ComboBox cboMotorVehicleType;
		private FCLabel lblMotorVehicleType;
		private TextBox txtReceiptTotal;
		private ComboBox cboReceiptTotalOption;
		private FCLabel lblReceiptTotal;
		private TextBox txtTenderedTotal;
		private ComboBox cboTenderedTotalOption;
		private FCLabel lblTenderedTotal;
		private TextBox txtCheckNumber;
		private FCLabel lblCheckNumber;
		private TextBox txtPaidBy;
		private ComboBox cboPaidByOption;
		private FCLabel lblPaidBy;
		private ComboBox cboBank;
		private FCLabel lblBank;
		private ComboBox cboDateToShow;
		private FCLabel lblDateToShow;
		private TextBox txtComment;
		private FCLabel lblComment;
		private ComboBox cboPaymentType;
		private FCLabel fcLabel4;
		public FCGrid vsAccount;
		private FCDateTimePicker txtActualTimeEnd;
		private FCDateTimePicker txtActualTimeStart;
		private FCDateTimePicker txtActualDateStart;
		private FCDateTimePicker txtActualDateEnd;
		private FCLabel lblActualTime;
	}
}