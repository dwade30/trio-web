using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using fecherFoundation;
using SharedApplication.CashReceipts.Models;

namespace TWCR0000.Reports
{
	/// <summary>
	/// Summary description for sarReceiptSearchSummaryDetail.
	/// </summary>
	public partial class sarReceiptSearchSummaryDetail : FCSectionReport
	{
		private bool firstRecord = true;
		private int counter = 0;
		private List<ReceiptSearchSummaryDetailInformation> reportData = new List<ReceiptSearchSummaryDetailInformation>();

		public sarReceiptSearchSummaryDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public sarReceiptSearchSummaryDetail(List<ReceiptSearchSummaryDetailInformation> data) : this()
		{
			reportData = data;
			firstRecord = true;
			counter = 0;
		}

		private void sarReceiptSearchSummaryDetail_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (firstRecord)
			{
				firstRecord = false;
			}
			else
			{
				counter++;
			}

			if (counter < reportData.Count())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void detail_Format(object sender, EventArgs e)
		{
			fldTitle.Text = reportData[counter].Title;
			fldAccount.Text = reportData[counter].GLAccount;
			fldAmount.Text = reportData[counter].Amount.ToString("#,##0.00");
		}
	}
}
