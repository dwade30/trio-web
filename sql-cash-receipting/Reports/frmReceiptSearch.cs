﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.AccountPayment;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	public partial class frmReceiptSearch : BaseForm, IView<IReceiptSearchViewModel>
	{
		clsGridAccount clsAcct = new clsGridAccount();
		private bool loadingScreen = false;
		public frmReceiptSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public frmReceiptSearch(IReceiptSearchViewModel viewModel) : this()
		{
			ViewModel = viewModel;
		}

		public IReceiptSearchViewModel ViewModel { get; set; }
		
		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboSavedReport.SelectedIndex < 0)
				return;

			if (cmbReport.SelectedIndex == 2)
			{
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					ViewModel.DeleteReport(cboSavedReport.ItemData(cboSavedReport.SelectedIndex));
					LoadSavedReportCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				LoadSavedReportOptions(cboSavedReport.ItemData(cboSavedReport.SelectedIndex));
				cmbReport.SelectedIndex = 0;
			}
		}

		private void LoadSavedReportOptions(int id)
		{
			var report = ViewModel.GetSavedReportInformation(id);

			if (report != null)
			{
				ClearSelections();
				lblReportName.Text = report.ReportName;

				foreach (var receiptType in report.ReceiptTypes)
				{
					SelectReceiptTypeOption(receiptType);
				}

				foreach (var sortOption in report.SortOptions)
				{
					SelectSortOption(sortOption);
				}

				if (report.ActualDateStart != null)
				{
					txtActualDateStart.Text = report.ActualDateStart.Value.ToShortDateString();
				}

				if (report.ActualDateEnd != null)
				{
					txtActualDateEnd.Text = report.ActualDateEnd.Value.ToShortDateString();
				}

				if (report.CloseOutDateStartId > 0)
				{
					SetCloseoutStartComboTo(report.CloseOutDateStartId);
					SetCloseoutEndComboTo(report.CloseOutDateEndId);
				}

				if (report.TellerId != "")
				{
					SetTellerIdComboTo(report.TellerId);
				}

				if (report.ReceiptTypeCodeStart > 0)
				{
					SetReceiptTypeStartComboTo(report.ReceiptTypeCodeStart);
					SetReceiptTypeEndComboTo(report.ReceiptTypeCodeEnd);
				}

				if (report.ReceiptNumberStart > 0)
				{
					txtReceiptNumberStart.Text = report.ReceiptNumberStart.ToString();
					txtReceiptNumberEnd.Text = report.ReceiptNumberEnd.ToString();
				}

				txtReference.Text = report.Reference.Trim();

				if (report.Name.Trim() != "")
				{
					SetNameSearchByOptionComboTo(report.NameSearchType);
					txtName.Text = report.Name.Trim();
				}

				txtControl1.Text = report.Control1.Trim();
				txtControl2.Text = report.Control2.Trim();
				txtControl3.Text = report.Control3.Trim();
				vsAccount.TextMatrix(0, 0, report.GLAccount.Trim());

				chkShowTenderedAmounts.Checked = report.ShowTenderedAmounts;
				SetReceiptDetailComboTo(report.ReceiptDetailLevel);
				SetReportDetailComboTo(report.ReportDetailLevel);
				SetSummarizeByComboTo(report.SummarizeBy);
				SetSummaryDetailComboTo(report.SummaryDetailLevel);
			}
			else
			{
				MessageBox.Show("There was an error while opening this file.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			if (ValidateReportOptions())
			{
				string strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report", null);
				strReturn = modGlobalFunctions.RemoveApostrophe(strReturn);
				if (strReturn == string.Empty)
				{
					// DO NOT SAVE REPORT
					MessageBox.Show("Report not saved.", "No Report Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					if (ViewModel.ReportExists(strReturn))
					{
						if (MessageBox.Show("A report by that name already exists. Do you wish to overwrite this report?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) != DialogResult.Yes)
						{
							return;
						}
					}

					lblReportName.Text = strReturn;
					lblReportName.Visible = true;

					if (ViewModel.SaveReport(BuildReportSetupInformation(strReturn)))
					{
						MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					LoadSavedReportCombo();
				}
			}
			else
			{
				MessageBox.Show("Please answer the questions before saving this report.", "Not Enough Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private ReceiptSearchReportSetupInformation BuildReportSetupInformation(string reportTitle)
		{
			var result = new ReceiptSearchReportSetupInformation();

			result.ReportName = reportTitle;

			result.ReportDetailLevel = ((GenericDescriptionPair<SummaryDetailBothOption>) cboReportDetail.SelectedItem).ID;
			result.TimePeriod = ((GenericDescriptionPair<TimePeriodOption>)cboTimePeriod.SelectedItem).ID;
			result.ReceiptDetailLevel = ((GenericDescriptionPair<SummaryDetailOption>)cboReceiptDetail.SelectedItem).ID;
			result.SummaryDetailLevel = ((GenericDescriptionPair<SummaryDetailOption>)cboSummaryDetail.SelectedItem).ID;
			result.SummarizeBy = ((GenericDescriptionPair<ReceiptSearchSummarizeByOption>)cboSummarizeBy.SelectedItem).ID;
			result.ShowTenderedAmounts = chkShowTenderedAmounts.Checked;

			result.ReceiptTypes = BuildReceiptTypesList();
			result.SortOptions = BuildSortOptionsList();

			if (txtActualDateStart.Text != "")
			{
				 result.ActualDateStart = txtActualDateStart.Value;
			}
			if (txtActualDateEnd.Text != "")
			{
				result.ActualDateEnd = txtActualDateEnd.Value;
			}

			result.CloseOutDateStartId = cboCloseoutStart.SelectedIndex >= 0 ? ((DescriptionIDPair) cboCloseoutStart.SelectedItem).ID : 0;
			result.CloseOutDateEndId = cboCloseoutEnd.SelectedIndex >= 0 ? ((DescriptionIDPair)cboCloseoutEnd.SelectedItem).ID : 0;

			result.CloseOutDateStartDescription = cboCloseoutStart.SelectedIndex >= 0 ? ((DescriptionIDPair)cboCloseoutStart.SelectedItem).Description : "";
			result.CloseOutDateEndDescription = cboCloseoutEnd.SelectedIndex >= 0 ? ((DescriptionIDPair)cboCloseoutEnd.SelectedItem).Description : "";

			if (cboTellerId.SelectedIndex >= 0)
			{
				result.TellerId = cboTellerId.Text;
			}

			result.ReceiptTypeCodeStart = cboReceiptTypeStart.SelectedIndex >= 0 ? ((DescriptionIDPair)cboReceiptTypeStart.SelectedItem).ID : 0;
			result.ReceiptTypeCodeEnd = cboReceiptTypeEnd.SelectedIndex >= 0 ? ((DescriptionIDPair)cboReceiptTypeEnd.SelectedItem).ID : 0;

			result.ReceiptNumberStart = txtReceiptNumberStart.Text.ToIntegerValue();
			result.ReceiptNumberEnd = txtReceiptNumberEnd.Text.ToIntegerValue();

			result.Reference = txtReference.Text.Trim();

			if (txtName.Text.Trim() != "")
			{
				result.NameSearchType = ((GenericDescriptionPair<NameSearchOptions>)cboNameSearchOption.SelectedItem).ID;
				result.Name = txtName.Text.Trim();
			}

			result.Control1 = txtControl1.Text.Trim();
			result.Control2 = txtControl2.Text.Trim();
			result.Control3 = txtControl3.Text.Trim();

			if (vsAccount.TextMatrix(0, 0) != "")
			{
				result.GLAccount = vsAccount.TextMatrix(0, 0);
			}

			if (cboMotorVehicleType.SelectedIndex >= 0)
			{
				result.MotorVehicleTransactionType = ((GenericDescriptionPair<MotorVehicleTransactionTypes>)cboMotorVehicleType.SelectedItem).ID;
			}

			if (txtReceiptTotal.Text != "")
			{
				result.ReceiptTotal = txtReceiptTotal.Text.ToDecimalValue();
				result.ReceiptTotalSearchType = ((GenericDescriptionPair<AmountSearchOptions>)cboReceiptTotalOption.SelectedItem).ID;
			}

			if (txtTenderedTotal.Text != "")
			{
				result.TenderedTotal = txtTenderedTotal.Text.ToDecimalValue();
				result.TenderedTotalSearchType = ((GenericDescriptionPair<AmountSearchOptions>)cboTenderedTotalOption.SelectedItem).ID;
			}

			if (txtPaidBy.Text.Trim() != "")
			{
				result.PaidBySearchType = ((GenericDescriptionPair<NameSearchOptions>)cboPaidByOption.SelectedItem).ID;
				result.PaidBy = txtPaidBy.Text.Trim();
			}

			result.CheckNumber = txtCheckNumber.Text.Trim();
			result.Comment = txtComment.Text.Trim();

			result.Bankid = cboBank.SelectedIndex >= 0 ? ((DescriptionIDPair)cboBank.SelectedItem).ID : 0;
			result.BankDescription = cboBank.SelectedIndex >= 0 ? ((DescriptionIDPair)cboBank.SelectedItem).Description : "";

			if (cboPaymentType.SelectedIndex >= 0)
			{
				result.PaymentType = ((GenericDescriptionPair<PaymentMethod>)cboPaymentType.SelectedItem).ID;
			}
			else
			{
				result.PaymentType = PaymentMethod.None;
			}

			result.DateToShow = ((GenericDescriptionPair<DateToShowOptions>)cboDateToShow.SelectedItem).ID;

			if (txtActualTimeStart.Text != "")
			{
				result.ActualTimeStart = txtActualTimeStart.Value;
			}

			if (txtActualTimeEnd.Text != "")
			{
				result.ActualTimeEnd = txtActualTimeEnd.Value;
			}

			return result;
		}

		private IEnumerable<ReceiptSearchSortByOptions> BuildSortOptionsList()
		{
			var result = new List<ReceiptSearchSortByOptions>();

			for (int counter = 0; counter < lstSort.Items.Count; counter++)
			{
				if (lstSort.Items[counter].Checked)
				{
					result.Add((ReceiptSearchSortByOptions)lstSort.ItemData(counter));
				}
			}

			return result;
		}

		private IEnumerable<int> BuildReceiptTypesList()
		{
			var result = new List<int>();

			for (int counter = 0; counter < lstTypes.Items.Count; counter++)
			{
				if (lstTypes.Items[counter].Checked)
				{
					result.Add(lstTypes.ItemData(counter));
				}
			}

			return result;
		}


		private void ClearSelections()
		{
			txtActualTimeStart.Text = "";
			txtActualDateStart.Text = "";
			txtActualTimeEnd.Text = "";
			txtActualDateEnd.Text = "";
			vsAccount.TextMatrix(0, 0, "");
			txtReference.Text = "";
			txtCheckNumber.Text = "";
			txtComment.Text = "";
			txtControl1.Text = "";
			txtControl2.Text = "";
			txtControl3.Text = "";
			txtName.Text = "";
			txtPaidBy.Text = "";
			txtReceiptNumberEnd.Text = "";
			txtReceiptNumberStart.Text = "";
			txtReceiptTotal.Text = "";
			txtTenderedTotal.Text = "";

			cboBank.SelectedIndex = -1;
			cboCloseoutEnd.SelectedIndex = -1;
			cboCloseoutStart.SelectedIndex = -1;
			SetDateToShowComboTo(DateToShowOptions.ActualSystemDate);
			cboMotorVehicleType.SelectedIndex = -1;
			cboNameSearchOption.SelectedIndex = -1;
			cboPaidByOption.SelectedIndex = -1;
			cboPaymentType.SelectedIndex = -1;
			cboReceiptTotalOption.SelectedIndex = -1;
			cboReceiptTypeEnd.SelectedIndex = -1;
			cboReceiptTypeStart.SelectedIndex = -1;
			cboTellerId.SelectedIndex = -1;
			cboTenderedTotalOption.SelectedIndex = -1;

			for (int counter = 0; counter <= lstSort.Items.Count - 1; counter++)
			{
				lstSort.SetSelected(counter, false);
			}
			for (int counter = 0; counter <= lstTypes.Items.Count - 1; counter++)
			{
				lstTypes.SetSelected(counter, false);
			}
			lblReportName.Text = "";
		}
		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			ClearSelections();
		}

		private void PrintReport()
		{
			if (ValidateReportOptions())
			{
				var report = new arReceiptSearch();
				var reportService = ViewModel.ReceiptSearchReportService;

				reportService.ReportSetup = BuildReportSetupInformation("");
				
				report.Init(reportService);
				frmReportViewer.InstancePtr.Init(report, allowCancel: true);
				frmReportViewer.InstancePtr.Focus();
			}
		}

		private void SetupView()
		{
			clsAcct.GRID7Light = vsAccount;
			clsAcct.DefaultAccountType = "G";
			vsAccount.ExtendLastCol = true;

			cmbReport.SelectedIndex = 0;

			ToolTip1.SetToolTip(cboReceiptDetail, "To show line items for each receipt, select Detail. Select Summary Breakdown to show each receipt in the summary with the breakdown of fees.");
			ToolTip1.SetToolTip(cboReportDetail, "Select Detail to show each line item of the receipt, Summary to only show the summary.");

			FillTimePeriodCombo();
			FillReportDetailCombo();
			FillReceiptDetailCombo();
			FillSummarizeByCombo();
			FillSummaryDetailCombo();
			FillCloseoutDateCombos();
			FillTellerIdCombo();
			FillBankCombo();
			FillReceiptTypeCombos();
			FillAmountSearchOptionCombos();
			FillNameSearchOptionCombos();
			FillDateToShowCombo();
			FillMotorVehicleTransactionTypeCombo();
			FillSortList();
			FillPaymentTypeCombo();

			SetTimePeriodComboTo(TimePeriodOption.CurrentYear);
			SetReportDetailComboTo(SummaryDetailBothOption.Detail);
			SetReceiptDetailComboTo(SummaryDetailOption.Summary);
			SetSummarizeByComboTo(ReceiptSearchSummarizeByOption.Type);
			SetSummaryDetailComboTo(SummaryDetailOption.Summary);
			SetDateToShowComboTo(DateToShowOptions.ActualSystemDate);

			chkShowTenderedAmounts.Value = FCCheckBox.ValueSettings.Unchecked;

			FillTypeList();

			LoadSavedReportCombo();

			ClearSelections();
		}

		private void frmReceiptSearch_Load(object sender, System.EventArgs e)
		{
			loadingScreen = true;
			SetupView();
			loadingScreen = false;
			EnableDisableReportOptions();
		}

		public void LoadSavedReportCombo()
		{
			cboSavedReport.Clear();
			foreach (var report in ViewModel.SavedReports)
			{
				cboSavedReport.AddItem(report.ReportName);
				cboSavedReport.ItemData(cboSavedReport.NewIndex, report.Id);
			}
		}

		private void SelectSortOption(ReceiptSearchSortByOptions selectedOption)
		{
			int intCounter;

			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.ItemData(intCounter) == (int)selectedOption)
				{
					lstSort.SetSelected(intCounter, true);
				}
			}
		}

		private void SelectReceiptTypeOption(int receiptTypeCode)
		{
			int intCounter;

			for (intCounter = 0; intCounter <= lstTypes.Items.Count - 1; intCounter++)
			{
				if (lstTypes.ItemData(intCounter) == (int)receiptTypeCode)
				{
					lstTypes.SetSelected(intCounter, true);
				}
			}
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			ClearSelections();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			if (ValidateReportOptions())
			{
				PrintReport();
			}
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			cmdAdd.Visible = Index == 0;
			lstSort.Enabled = Index == 0;
			lblReportName.Visible = Index == 0;
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}
		
		private bool ValidateReportOptions()
		{
			this.ActiveControl = cmdPrint;

			try
			{
				if (vsAccount.TextMatrix(0, 0) != "")
				{
					if (!ViewModel.ValidateGLAccount(vsAccount.TextMatrix(0, 0)))
					{
						MessageBox.Show("Please enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
				}

				if (txtActualDateStart.Text != "")
				{
					if (!Information.IsDate(txtActualDateStart.Text))
					{
						MessageBox.Show("Please enter a valid date in the Actual Date field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
				}

				if (txtReceiptNumberStart.Text != "")
				{
					// the first column is used
					if (Information.IsNumeric(txtReceiptNumberStart.Text))
					{
						// if this is numeric, then check the second field in the range
						if (txtReceiptNumberEnd.Text != "")
						{
							// check to see if the second column is used as well
							if (!Information.IsNumeric(txtReceiptNumberEnd.Text))
							{
								MessageBox.Show("Please enter a valid number in the ending receipt number field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return false;
							}
						}
					}
					else
					{
						MessageBox.Show("Please enter a valid number in the beginning receipt number field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
				}
				else
				{
					// only the second column is used
					if (txtReceiptNumberEnd.Text != "")
					{
						// check to see if the second column is used as well
						if (!Information.IsNumeric(txtReceiptNumberEnd.Text))
						{
							MessageBox.Show("Please enter a valid number in the ending receipt number field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return false;
						}
					}
				}

				if (txtName.Text.Trim() != "")
				{
					if (cboNameSearchOption.SelectedIndex < 0)
					{
						MessageBox.Show("Please select a name search option.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
				}

				if (txtPaidBy.Text.Trim() != "")
				{
					if (cboPaidByOption.SelectedIndex < 0)
					{
						MessageBox.Show("Please select a paid by search option.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
				}

				if (cboReportDetail.SelectedIndex < 0)
				{
					MessageBox.Show("Please select a report detail level.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (cboReceiptDetail.SelectedIndex < 0)
				{
					MessageBox.Show("Please select a receipt detail level.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (cboTimePeriod.SelectedIndex < 0)
				{
					MessageBox.Show("Please select a time period option.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (cboSummaryDetail.SelectedIndex < 0)
				{
					MessageBox.Show("Please select a summary detail level.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (cboSummarizeBy.SelectedIndex < 0)
				{
					MessageBox.Show("Please select a summarize option.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return false;
			}
		}

		private void FillTypeList()
		{
			try
			{
				lstTypes.Clear();
				lstTypes.Columns.Add("");
				lstTypes.Columns[0].Width = 80;
				lstTypes.Columns[1].Width = lstTypes.Width - 100;
				lstTypes.GridLineStyle = GridLineStyle.None;
				foreach (var type in ViewModel.ReceiptTypes)
				{
					lstTypes.AddItem(Strings.Format(type.TypeCode, "000") + "\t" + type.TypeTitle);
					lstTypes.ItemData(lstTypes.NewIndex, FCConvert.ToInt32(type.TypeCode));
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Type List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}


		private void SetTimePeriodComboTo(TimePeriodOption selectedOption)
		{
			foreach (GenericDescriptionPair<TimePeriodOption> option in cboTimePeriod.Items)
			{
				if (option.ID == selectedOption)
				{
					cboTimePeriod.SelectedItem = option;
					return;
				}
			}
		}

		private void SetReportDetailComboTo(SummaryDetailBothOption selectedOption)
		{
			foreach (GenericDescriptionPair<SummaryDetailBothOption> option in cboReportDetail.Items)
			{
				if (option.ID == selectedOption)
				{
					cboReportDetail.SelectedItem = option;
					return;
				}
			}
		}

		private void SetReceiptDetailComboTo(SummaryDetailOption selectedOption)
		{
			foreach (GenericDescriptionPair<SummaryDetailOption> option in cboReceiptDetail.Items)
			{
				if (option.ID == selectedOption)
				{
					cboReceiptDetail.SelectedItem = option;
					return;
				}
			}
		}

		private void SetSummaryDetailComboTo(SummaryDetailOption selectedOption)
		{
			foreach (GenericDescriptionPair<SummaryDetailOption> option in cboSummaryDetail.Items)
			{
				if (option.ID == selectedOption)
				{
					cboSummaryDetail.SelectedItem = option;
					return;
				}
			}
		}

		private void SetSummarizeByComboTo(ReceiptSearchSummarizeByOption selectedOption)
		{
			foreach (GenericDescriptionPair<ReceiptSearchSummarizeByOption> option in cboSummarizeBy.Items)
			{
				if (option.ID == selectedOption)
				{
					cboSummarizeBy.SelectedItem = option;
					return;
				}
			}
		}

		private void SetNameSearchByOptionComboTo(NameSearchOptions selectedOption)
		{
			foreach (GenericDescriptionPair<NameSearchOptions> option in cboNameSearchOption.Items)
			{
				if (option.ID == selectedOption)
				{
					cboNameSearchOption.SelectedItem = option;
					return;
				}
			}
		}

		private void SetPaidBySearchByOptionComboTo(NameSearchOptions selectedOption)
		{
			foreach (GenericDescriptionPair<NameSearchOptions> option in cboPaidByOption.Items)
			{
				if (option.ID == selectedOption)
				{
					cboPaidByOption.SelectedItem = option;
					return;
				}
			}
		}

		private void SetReceiptTotalSearchByOptionComboTo(AmountSearchOptions selectedOption)
		{
			foreach (GenericDescriptionPair<AmountSearchOptions> option in cboReceiptTotalOption.Items)
			{
				if (option.ID == selectedOption)
				{
					cboReceiptTotalOption.SelectedItem = option;
					return;
				}
			}
		}

		private void SetTenderedTotalSearchByOptionComboTo(AmountSearchOptions selectedOption)
		{
			foreach (GenericDescriptionPair<AmountSearchOptions> option in cboTenderedTotalOption.Items)
			{
				if (option.ID == selectedOption)
				{
					cboTenderedTotalOption.SelectedItem = option;
					return;
				}
			}
		}

		private void SetDateToShowComboTo(DateToShowOptions selectedOption)
		{
			foreach (GenericDescriptionPair<DateToShowOptions> option in cboDateToShow.Items)
			{
				if (option.ID == selectedOption)
				{
					cboDateToShow.SelectedItem = option;
					return;
				}
			}
		}

		private void SetCloseoutStartComboTo(int id)
		{
			foreach (DescriptionIDPair option in cboCloseoutStart.Items)
			{
				if (option.ID == id)
				{
					cboCloseoutStart.SelectedItem = option;
					return;
				}
			}
		}

		private void SetCloseoutEndComboTo(int id)
		{
			foreach (DescriptionIDPair option in cboCloseoutEnd.Items)
			{
				if (option.ID == id)
				{
					cboCloseoutEnd.SelectedItem = option;
					return;
				}
			}
		}

		private void SetTellerIdComboTo(string tellerId)
		{
			foreach (DescriptionIDPair option in cboTellerId.Items)
			{
				if (option.Description == tellerId)
				{
					cboTellerId.SelectedItem = option;
					return;
				}
			}
		}

		private void SetBankComboTo(int bankId)
		{
			foreach (DescriptionIDPair option in cboBank.Items)
			{
				if (option.ID == bankId)
				{
					cboBank.SelectedItem = option;
					return;
				}
			}
		}

		private void SetReceiptTypeStartComboTo(int typeCode)
		{
			foreach (DescriptionIDPair option in cboReceiptTypeStart.Items)
			{
				if (option.ID == typeCode)
				{
					cboReceiptTypeStart.SelectedItem = option;
					return;
				}
			}
		}

		private void SetReceiptTypeEndComboTo(int typeCode)
		{
			foreach (DescriptionIDPair option in cboReceiptTypeEnd.Items)
			{
				if (option.ID == typeCode)
				{
					cboReceiptTypeEnd.SelectedItem = option;
					return;
				}
			}
		}

		private void SetMotorVehicleTypeComboTo(MotorVehicleTransactionTypes selectedOption)
		{
			foreach (GenericDescriptionPair<MotorVehicleTransactionTypes> option in cboMotorVehicleType.Items)
			{
				if (option.ID == selectedOption)
				{
					cboMotorVehicleType.SelectedItem = option;
					return;
				}
			}
		}

		private void FillNameSearchOptionCombos()
		{
			cboNameSearchOption.Items.Clear();
			cboPaidByOption.Items.Clear();
			var options = ViewModel.GetNameSearchOptions();
			foreach (var option in options)
			{
				cboNameSearchOption.Items.Add(option);
				cboPaidByOption.Items.Add(option);
			}
		}

		private void FillAmountSearchOptionCombos()
		{
			cboReceiptTotalOption.Items.Clear();
			cboTenderedTotalOption.Items.Clear();
			var options = ViewModel.GetAmountSearchOptions();
			foreach (var option in options)
			{
				cboReceiptTotalOption.Items.Add(option);
				cboTenderedTotalOption.Items.Add(option);
			}
		}

		private void FillDateToShowCombo()
		{
			cboDateToShow.Items.Clear();
			var options = ViewModel.GetDateToShowOptions();
			foreach (var option in options)
			{
				cboDateToShow.Items.Add(option);
			}
		}

		private void FillMotorVehicleTransactionTypeCombo()
		{
			cboMotorVehicleType.Items.Clear();
			var options = ViewModel.GetMotorVehicleTransactionTypeOptions();
			foreach (var option in options)
			{
				cboMotorVehicleType.Items.Add(option);
			}
		}

		private void FillTimePeriodCombo()
		{
			cboTimePeriod.Items.Clear();
			var options = ViewModel.GetTimePeriodOptions();
			foreach (var option in options)
			{
				cboTimePeriod.Items.Add(option);
			}
		}

		private void FillReportDetailCombo()
		{
			cboReportDetail.Items.Clear();
			var options = ViewModel.GetSummaryDetailBothOptions();
			foreach (var option in options)
			{
				cboReportDetail.Items.Add(option);
			}
		}

		private void FillPaymentTypeCombo()
		{
			cboPaymentType.Items.Clear();
			var options = ViewModel.GetPaymentMethods();
			foreach (var option in options)
			{
				cboPaymentType.Items.Add(option);
			}
		}

		private void FillReceiptDetailCombo()
		{
			cboReceiptDetail.Items.Clear();
			var options = ViewModel.GetSummaryDetailOptions();
			foreach (var option in options)
			{
				cboReceiptDetail.Items.Add(option);
			}
		}

		private void FillSummaryDetailCombo()
		{
			cboSummaryDetail.Items.Clear();
			var options = ViewModel.GetSummaryDetailOptions();
			foreach (var option in options)
			{
				cboSummaryDetail.Items.Add(option);
			}
		}

		private void FillSummarizeByCombo()
		{
			cboSummarizeBy.Items.Clear();
			var options = ViewModel.GetSummarizeByOptions();
			foreach (var option in options)
			{
				cboSummarizeBy.Items.Add(option);
			}
		}

		private void FillCloseoutDateCombos()
		{
			cboCloseoutEnd.Items.Clear();
			cboCloseoutStart.Items.Clear();
			var options = ViewModel.CloseOuts.OrderByDescending(x => x.Id).Select(y => new DescriptionIDPair
			{
				ID = y.Id,
				Description = y.CloseOutDate.Value.ToShortDateString()
			});
			foreach (var option in options)
			{
				cboCloseoutEnd.Items.Add(option);
				cboCloseoutStart.Items.Add(option);
			}
		}

		private void FillTellerIdCombo()
		{
			cboTellerId.Items.Clear();
			var options = ViewModel.TellerIds.Where(x => x.Code != "617" || StaticSettings.gGlobalSettings.IsHarrisStaffComputer).OrderBy(x => x.Code).Select(y => new DescriptionIDPair
			{
				ID = y.ID,
				Description = y.Code
			});
			foreach (var option in options)
			{
				cboTellerId.Items.Add(option);
			}
		}

		private void FillBankCombo()
		{
			cboBank.Items.Clear();
			var options = ViewModel.Banks.OrderBy(x => x.Name).Select(y => new DescriptionIDPair
			{
				ID = y.ID,
				Description = y.Name
			});
			foreach (var option in options)
			{
				cboBank.Items.Add(option);
			}
		}

		private void FillReceiptTypeCombos()
		{
			cboReceiptTypeStart.Items.Clear();
			cboReceiptTypeEnd.Items.Clear();
			var options = ViewModel.ReceiptTypes.OrderBy(x => x.TypeCode).Select(y => new DescriptionIDPair
			{
				ID = (int)y.TypeCode,
				Description = y.TypeCode.ToString().PadLeft(3, '0') + " - " + y.TypeTitle
			});
			foreach (var option in options)
			{
				cboReceiptTypeStart.Items.Add(option);
				cboReceiptTypeEnd.Items.Add(option);
			}
		}

		private void FillSortList()
		{
			lstSort.Items.Clear();

			foreach (var option in ViewModel.GetSortByOptions())
			{
				lstSort.AddItem(option.Description);
				lstSort.ItemData(lstSort.NewIndex, (int)option.ID);
			}
		}

		private void frmReceiptSearch_KeyDown(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsAccount, vsAccount.Row, vsAccount.Col, KeyCode, Shift, vsAccount.EditSelStart, vsAccount.EditText, vsAccount.EditSelLength);
			}
		}

		private void cboReportDetail_SelectedIndexChanged(object sender, EventArgs e)
		{
			EnableDisableReportOptions();
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			ClearSelections();
		}

		private void cboSummarizeBy_SelectedIndexChanged(object sender, EventArgs e)
		{
			EnableDisableReportOptions();
		}

		private void EnableDisableReportOptions()
		{
			if (!loadingScreen)
			{
				var option = ((GenericDescriptionPair<ReceiptSearchSummarizeByOption>)cboSummarizeBy.SelectedItem).ID;
				var reportDetailOption = ((GenericDescriptionPair<SummaryDetailBothOption>)cboReportDetail.SelectedItem).ID;
				var receiptDetailOption = ((GenericDescriptionPair<SummaryDetailOption>)cboReceiptDetail.SelectedItem).ID;

				cboReceiptDetail.Enabled = reportDetailOption == SummaryDetailBothOption.Detail || reportDetailOption == SummaryDetailBothOption.Both;
				chkShowTenderedAmounts.Enabled = (reportDetailOption == SummaryDetailBothOption.Detail || reportDetailOption == SummaryDetailBothOption.Both) && receiptDetailOption == SummaryDetailOption.Detail;

				if (reportDetailOption == SummaryDetailBothOption.Summary ||
				    reportDetailOption == SummaryDetailBothOption.Both)
				{
					if (option == ReceiptSearchSummarizeByOption.Type || option == ReceiptSearchSummarizeByOption.TypeAndDate || option == ReceiptSearchSummarizeByOption.TypeAndTeller)
					{
						cboSummaryDetail.Enabled = true;
					}
					else
					{
						cboSummaryDetail.Enabled = false;
					}
				}
				else
				{
					cboSummaryDetail.Enabled = false;
				}
			}
		}

		private void cboReceiptDetail_SelectedIndexChanged(object sender, EventArgs e)
		{
			EnableDisableReportOptions();
		}
	}
}
