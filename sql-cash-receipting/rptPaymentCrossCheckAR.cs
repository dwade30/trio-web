﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for rptPaymentCrossCheckAR.
	/// </summary>
	public partial class rptPaymentCrossCheckAR : BaseSectionReport
	{
		public static rptPaymentCrossCheckAR InstancePtr
		{
			get
			{
				return (rptPaymentCrossCheckAR)Sys.GetInstance(typeof(rptPaymentCrossCheckAR));
			}
		}

		protected rptPaymentCrossCheckAR _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPaymentCrossCheckAR	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/18/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		double dblTotalAmt;
		int[] arrMissingPayments = null;
		bool boolDone;

		public rptPaymentCrossCheckAR()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "AR Payment Cross Check";
            this.ReportEnd += RptPaymentCrossCheckAR_ReportEnd;
		}

        private void RptPaymentCrossCheckAR_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_Initialize()
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			CreateArrayElements();
			if (lngCount == 0)
			{
				MessageBox.Show("No records to report.", "No Missing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
			}
			else
			{
				lngCount = 0;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// MAL@20071012
			rptPaymentCrossCheck.InstancePtr.lblHeader2.Text = "Receipts appearing on this report do not have corresponding entries in the Tax Collections databases";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			fldReceiptNumber.Text = "";
			fldDate.Text = "";
			fldName.Text = "";
			fldTeller.Text = "";
			fldPrin.Text = "";
			fldCurInt.Text = "";
			fldCost.Text = "";
			fldPLI.Text = "";
			fldType.Text = "";
			if (lngCount <= Information.UBound(arrMissingPayments, 1))
			{
				rsData.FindFirstRecord("ID", arrMissingPayments[lngCount]);
				if (!rsData.NoMatch)
				{
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptType")) == 90)
					{
						// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
						fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6"), "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
						fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
						fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
						fldPLI.Text = "0.00";
						fldType.Text = "R";
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
						fldPrin.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6"), "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
						fldCurInt.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
						fldPLI.Text = Strings.Format(rsData.Get_Fields("Amount3"), "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
						fldCost.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
						fldType.Text = "L";
					}
					fldReceiptNumber.Text = GetActualReceiptNumber_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber")));
					fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy");
					fldName.Text = rsData.Get_Fields_String("Name");
					fldTeller.Text = rsData.Get_Fields_String("TellerID");
				}
				else
				{
					ClearBoxes();
				}
				lngCount += 1;
			}
			else
			{
				boolDone = true;
			}
		}

		private void CreateArrayElements()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCL = new clsDRWrapper();
				string strSQL;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Receipts");
				strSQL = "SELECT * FROM Archive WHERE ReceiptType = 97 ORDER BY ReceiptNumber";
				lngCount = 0;
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking AR Receipts", true, rsData.RecordCount(), true);
				while (!rsData.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					if (Conversion.Val(Strings.Right(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), 4)) > 0)
					{
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptType")) == 97)
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("AccountNumber") + " AND BillCode = 'R' AND (Year \\ 10) = " + FCConvert.ToString(Conversion.Val(Strings.Right(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), 4))) + " AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND LienCost = " + rsData.Get_Fields("Amount4"), modExtraModules.strCLDatabase);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							rsCL.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("AccountNumber") + " AND BillCode = 'L' AND (Year \\ 10) = " + FCConvert.ToString(Conversion.Val(Strings.Right(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref"))), 4))) + " AND ReceiptNumber = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND Code = '" + rsData.Get_Fields_String("Control2") + "' AND Month(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "MM") + " AND Day(ActualSystemDate) = " + Strings.Format(rsData.Get_Fields_DateTime("ActualSystemDate"), "dd") + " AND Principal = " + FCConvert.ToString(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount6")) + " AND CurrentInterest = " + rsData.Get_Fields("Amount2") + " AND PreLienInterest = " + rsData.Get_Fields("Amount3") + " AND LienCost = " + rsData.Get_Fields("Amount4"), modExtraModules.strCLDatabase);
						}
						if (rsCL.EndOfFile())
						{
							Array.Resize(ref arrMissingPayments, lngCount + 1);
							arrMissingPayments[lngCount] = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
							lngCount += 1;
						}
					}
					rsData.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Array Elements", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearBoxes()
		{
			fldCost.Text = "";
			fldDate.Text = "";
			fldCurInt.Text = "";
			fldName.Text = "";
			fldPLI.Text = "";
			fldPrin.Text = "";
			fldReceiptNumber.Text = "";
			fldTeller.Text = "";
			fldType.Text = "";
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_2(int lngRN, DateTime? dtDate = null)
		{
			return GetActualReceiptNumber(ref lngRN, dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, DateTime? dtDateTemp = null)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number ID in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				clsDRWrapper rsRN = new clsDRWrapper();
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN), modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
							if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				rsRN.Reset();
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetActualReceiptNumber;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (lngCount != 1)
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " AR transactions.";
			}
			else
			{
				lblFooter.Text = "Missing " + FCConvert.ToString(lngCount) + " AR transaction.";
			}
		}

		private void rptPaymentCrossCheckAR_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPaymentCrossCheckAR.Caption	= "AR Payment Cross Check";
			//rptPaymentCrossCheckAR.Icon	= "rptPaymentCrossCheckAR.dsx":0000";
			//rptPaymentCrossCheckAR.Left	= 0;
			//rptPaymentCrossCheckAR.Top	= 0;
			//rptPaymentCrossCheckAR.Width	= 11880;
			//rptPaymentCrossCheckAR.Height	= 8595;
			//rptPaymentCrossCheckAR.StartUpPosition	= 3;
			//rptPaymentCrossCheckAR.SectionData	= "rptPaymentCrossCheckAR.dsx":058A;
			//End Unmaped Properties
		}
	}
}
