﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTypeDetailAR.
	/// </summary>
	public partial class sarTypeDetailAR : FCSectionReport
	{
		public static sarTypeDetailAR InstancePtr
		{
			get
			{
				return (sarTypeDetailAR)Sys.GetInstance(typeof(sarTypeDetailAR));
			}
		}

		protected sarTypeDetailAR _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTypeDetailAR	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/22/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsRec = new clsDRWrapper();
		clsDRWrapper rsType = new clsDRWrapper();
		clsDRWrapper rsARType = new clsDRWrapper();
		string strLastName = "";
		int lngLastReceiptNumber;
		int lngLastReceiptType;
		// MAL@20071008
		bool boolShowNameLine;
		bool boolShowAccount;
		int intMIFeeNumber;

		public sarTypeDetailAR()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Type Detail";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				BindFields();
				eArgs.EOF = false;
				rsData.MoveNext();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void BindFields()
		{
			string strTemp = "";
			bool boolNonEntry = false;
			string strType;
			// if this is a RE, PP or UT receipt then show the type under other fields
			// this will fill all of the fields with the data from the recordset in the detail section
			fldName.Text = rsData.Get_Fields_String("Name");
			if (strLastName == FCConvert.ToString(rsData.Get_Fields_String("Name")) && FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber")) == lngLastReceiptNumber && FCConvert.ToInt32(rsData.Get_Fields_Int32("ARBillType")) == lngLastReceiptType)
			{
				boolShowNameLine = false;
			}
			else
			{
				boolShowNameLine = true;
			}
			// set the values for the next time
			strLastName = FCConvert.ToString(rsData.Get_Fields_String("Name"));
			lngLastReceiptNumber = FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber"));
			lngLastReceiptType = FCConvert.ToInt32(rsData.Get_Fields_Int32("ARBillType"));
			//FC:FINAL:MSH - Issue #706: Added FCConvert.ToString, because system can't implicitly convert int to string.
			//fldAcct.Text = rsData.Get_Fields("AccountNumber");
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
			if (Strings.Trim(fldAcct.Text) == "0")
				fldAcct.Text = "";
			strType = "";
			boolShowAccount = false;
			int vbPorterVar = rsData.Get_Fields_Int32("ReceiptType");
			if ((vbPorterVar >= 90 && vbPorterVar <= 95) || (vbPorterVar == 890) || (vbPorterVar == 891) || (vbPorterVar >= 893 && vbPorterVar <= 896))
			{
				// kgk trocr-277 04-25-2011  Added 894 and 895
				// sets the code if the receipttype is 90-95
				strType = FCConvert.ToString(rsData.Get_Fields_String("Control2"));
				if (FCConvert.ToString(rsData.Get_Fields_String("Control2")) == "D")
				{
					boolShowAccount = false;
				}
				else
				{
					boolShowAccount = true;
				}
				fldCode.Text = rsData.Get_Fields_String("Control1") + rsData.Get_Fields_String("Control2");
				fldBill.Text = Strings.Right(FCConvert.ToString(rsData.Get_Fields_String("Ref")), 4);
				// this will show the year only
				// Case 890, 891
				// fldCode.Text = rsData.Fields("Control1") & rsData.Fields("Control2")
				// fldBill.Text = Right(rsData.Fields("Ref"), 4)   'this will show the year only
			}
			else
			{
				fldCode.Text = "";
				fldBill.Text = "";
			}
			//FC:FINAL:MSH - Issue #706: Added FCConvert.ToString, because system can't implicitly convert int to string.
			//fldReceipt.Text = GetExternalRN_2(FCConvert.ToInt32(rsData.Get_Fields("ReceiptNumber")));
			fldReceipt.Text = FCConvert.ToString(GetExternalRN_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("ReceiptNumber"))));
			fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ArchiveDate"), "MM/dd/yy");
			fldTeller.Text = rsData.Get_Fields_String("TellerID");
			// MAL@20080408: Check for Daily Audit Preview and Adjust Header Accordingly
			// Tracker Reference: 13160
			if (arDailyAuditReport.InstancePtr.blnFullAuditPreview && arDailyAuditReport.InstancePtr.intReportHeader == 3)
			{
				if (!frmDailyReceiptAudit.InstancePtr.boolCloseOut)
				{
					arDailyAuditReport.InstancePtr.lblTellerID.Text = "PREVIEW";
					// & vbCrLf & "Teller ID : " & rsData.Fields("TellerID")
				}
				else
				{
					// arDailyAuditReport.lblTellerID.Text = vbCrLf & "Teller ID : " & rsData.Fields("TellerID")
				}
			}
			// Cash Drawer/Affect Cash
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AffectCashDrawer")))
			{
				strTemp = "Y";
				boolNonEntry = false;
			}
			else
			{
				strTemp = "N/";
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AffectCash")))
				{
					strTemp += "Y";
					boolNonEntry = false;
				}
				else
				{
					strTemp += "N";
					// frmDailyReceiptAudit.boolNonEntry = True
					// boolNonEntry = True
				}
			}
			if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount"))) != "")
			{
				fldAltCashAccount.Text = "ALT: " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount")));
				fldAltCashAccount.Visible = true;
			}
			else
			{
				fldAltCashAccount.Text = "";
				fldAltCashAccount.Visible = false;
			}
			// If Trim(rsType.Fields("DefaultAccount")) <> "" Then
			// fldAltCashAccount.Text = "ALT: " & Trim(rsType.Fields("DefaultAccount"))
			// fldAltCashAccount.Visible = True
			// Else
			// fldAltCashAccount.Text = ""
			// fldAltCashAccount.Visible = False
			// End If
			if (boolShowNameLine)
			{
				// this will show the name line first, then the amount line, then the account line if needed
				// show the name line
				fldName.Visible = true;
				fldDate.Visible = true;
				if (strTemp == "N/N" && boolShowAccount)
				{
					// Detail.Height = 450
					lblAccountNumber.Visible = true;
					lblAccountNumber.Text = "Account:";
					fldAccountNumber.Visible = true;
					lblAccountNumber.Top = fldTitle2.Top + 270 / 1440F;
					fldAccountNumber.Top = fldTitle2.Top + 270 / 1440F;
					fldAccountNumber.Text = rsData.Get_Fields_String("DefaultAccount");
				}
				else if (intMIFeeNumber != 0)
				{
					lblAccountNumber.Visible = true;
					lblAccountNumber.Text = "M I Account:";
					fldAccountNumber.Visible = true;
					lblAccountNumber.Top = fldTitle2.Top + 270 / 1440F;
					fldAccountNumber.Top = fldTitle2.Top + 270 / 1440F;
					switch (intMIFeeNumber)
					{
						case 1:
							{
								fldAccountNumber.Text = rsData.Get_Fields_String("Account1");
								break;
							}
						case 2:
							{
								fldAccountNumber.Text = rsData.Get_Fields_String("Account2");
								break;
							}
						case 3:
							{
								fldAccountNumber.Text = rsData.Get_Fields_String("Account3");
								break;
							}
						case 4:
							{
								fldAccountNumber.Text = rsData.Get_Fields_String("Account4");
								break;
							}
						case 5:
							{
								fldAccountNumber.Text = rsData.Get_Fields_String("Account5");
								break;
							}
						case 6:
							{
								fldAccountNumber.Text = rsData.Get_Fields_String("Account6");
								break;
							}
					}
					//end switch
				}
				else
				{
					// Detail.Height = 270
					lblAccountNumber.Visible = false;
					lblAccountNumber.Top = 0;
					fldAccountNumber.Visible = false;
					fldAccountNumber.Top = 0;
				}
			}
			else
			{
				// this will not show the name line because it is the same as the last one, but it will
				// show the amount line, then the account line if needed
				// hide the name line
				fldName.Visible = false;
				fldDate.Visible = false;
				// move all the other fields up
				fldPayType.Top = 0;
				fldAcct.Top = 0;
				fldBill.Top = 0;
				fldReceipt.Top = 0;
				fldTeller.Top = 0;
				fldNC.Top = 0;
				fldCode.Top = 0;
				fldTitle1.Top = 0;
				fldTitle2.Top -= 270 / 1440F;
				fldTitle3.Top = fldTitle1.Top;
				fldTitle4.Top = fldTitle2.Top;
				fldTitle5.Top = fldTitle1.Top;
				fldTitle6.Top = fldTitle2.Top;
				fldTotal.Top = fldTitle1.Top;
				fldConvenienceFee.Top = fldTitle2.Top;
				if ((strTemp == "N/N" && boolShowAccount) || intMIFeeNumber != 0)
				{
					// Detail.Height = 450
					lblAccountNumber.Visible = true;
					fldAccountNumber.Visible = true;
					lblAccountNumber.Top = fldTitle2.Top + 270 / 1440F;
					fldAccountNumber.Top = fldTitle2.Top + 270 / 1440F;
					fldAccountNumber.Text = rsData.Get_Fields_String("DefaultAccount");
					if (intMIFeeNumber != 0)
					{
						lblAccountNumber.Text = "M I Account:";
						switch (intMIFeeNumber)
						{
							case 1:
								{
									fldAccountNumber.Text = rsData.Get_Fields_String("Account1");
									break;
								}
							case 2:
								{
									fldAccountNumber.Text = rsData.Get_Fields_String("Account2");
									break;
								}
							case 3:
								{
									fldAccountNumber.Text = rsData.Get_Fields_String("Account3");
									break;
								}
							case 4:
								{
									fldAccountNumber.Text = rsData.Get_Fields_String("Account4");
									break;
								}
							case 5:
								{
									fldAccountNumber.Text = rsData.Get_Fields_String("Account5");
									break;
								}
							case 6:
								{
									fldAccountNumber.Text = rsData.Get_Fields_String("Account6");
									break;
								}
						}
						//end switch
					}
				}
				else
				{
					// Detail.Height = 270
					lblAccountNumber.Visible = false;
					lblAccountNumber.Top = 0;
					fldAccountNumber.Visible = false;
					fldAccountNumber.Top = 0;
				}
			}
			fldNC.Text = strTemp;
			// PayType
			rsRec.FindFirstRecord("ReceiptKey", rsData.Get_Fields_Int32("ReceiptNumber"));
			if (rsRec.EndOfFile() != true && rsRec.BeginningOfFile() != true)
			{
				if (rsRec.Get_Fields_Double("CashAmount") != 0)
				{
					if (rsRec.Get_Fields_Double("CardAmount") != 0)
					{
						if (rsRec.Get_Fields_Double("CheckAmount") != 0)
						{
							strTemp = "CCC";
						}
						else
						{
							strTemp = "CS/CC";
						}
					}
					else
					{
						if (rsRec.Get_Fields_Double("CheckAmount") != 0)
						{
							strTemp = "CS/CK";
						}
						else
						{
							strTemp = "CS";
						}
					}
				}
				else
				{
					if (rsRec.Get_Fields_Double("CardAmount") != 0)
					{
						if (rsRec.Get_Fields_Double("CheckAmount") != 0)
						{
							strTemp = "CC/CK";
						}
						else
						{
							strTemp = "CC";
						}
					}
					else
					{
						if (rsRec.Get_Fields_Double("CheckAmount") != 0)
						{
							strTemp = "CK";
						}
						else
						{
							strTemp = "";
						}
					}
				}
				if (Strings.Trim(strTemp) == "")
					strTemp = "N/A";
			}
			else
			{
				strTemp = "N/A";
			}
			fldPayType.Text = strTemp;
			// End If
			if (strType == "D" || strType == "A" || (strType == "R" && fldCode.Text == "AR"))
			{
				if (strType == "D")
				{
					fldTitle1.Text = "0.00";
					// Format(rsData.Fields("Amount1"), "#,##0.00")
					fldTitle2.Text = "0.00";
					// "*" & Format(rsData.Fields("Amount2"), "#,##0.00")
					// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					fldTitle3.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount2"), "#,##0.00");
					fldTitle4.Text = "0.00";
					fldTitle5.Text = "0.00";
					fldTitle6.Text = "0.00";
					// set this to zero because it is a non entry
					fldTotal.Text = "0.00";
				}
				else if (strType == "A")
				{
					fldTitle1.Text = "0.00";
					// Format(rsData.Fields("Amount1"), "#,##0.00")
					fldTitle2.Text = "0.00";
					// "*" & Format(rsData.Fields("Amount2"), "#,##0.00")
					fldTitle3.Text = "0.00";
					fldTitle4.Text = "0.00";
					fldTitle5.Text = "0.00";
					// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					fldTitle6.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount2") + rsData.Get_Fields("Amount4"), "#,##0.00");
					// set this to zero because it is a non entry
					fldTotal.Text = "0.00";
				}
				else if (strType == "R")
				{
					fldTitle1.Text = "0.00";
					// Format(rsData.Fields("Amount1"), "#,##0.00")
					fldTitle2.Text = "0.00";
					// "*" & Format(rsData.Fields("Amount2"), "#,##0.00")
					fldTitle3.Text = "0.00";
					fldTitle4.Text = "0.00";
					fldTitle5.Text = "0.00";
					// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					fldTitle6.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount2") + rsData.Get_Fields("Amount4"), "#,##0.00");
					// set this to zero because it is a non entry
					fldTotal.Text = "0.00";
				}
			}
			else
			{
				if (boolNonEntry)
				{
					// put an asterisk when it is a non entry
					// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					fldTitle1.Text = Strings.Format(rsData.Get_Fields("Amount1"), "#,##0.00");
					// If rsData.Fields("Amount2") > 0 Then
					// fldTitle2.Text = "*" & Format(rsData.Fields("Amount2"), "#,##0.00")
					// Else
					// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					fldTitle2.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
					// End If
					// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
					fldTitle3.Text = Strings.Format(rsData.Get_Fields("Amount3"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					fldTitle4.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
					fldTitle5.Text = Strings.Format(rsData.Get_Fields("Amount5"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
					fldTitle6.Text = Strings.Format(rsData.Get_Fields("Amount6"), "#,##0.00");
					// fldConvenienceFee.Text = Format(rsData.Fields("ConvenienceFee"), "#,##0.00")
					// set this to zero because it is a non entry
					fldTotal.Text = "0.00";
					// Format(CDbl(fldTitle1.Text) + CDbl(fldTitle2.Text) + CDbl(fldTitle3.Text) + CDbl(fldTitle4.Text) + CDbl(fldTitle5.Text) + CDbl(fldTitle6.Text), "#,##0.00")
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					fldTitle1.Text = Strings.Format(rsData.Get_Fields("Amount1"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					fldTitle2.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
					fldTitle3.Text = Strings.Format(rsData.Get_Fields("Amount3"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					fldTitle4.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
					fldTitle5.Text = Strings.Format(rsData.Get_Fields("Amount5"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
					fldTitle6.Text = Strings.Format(rsData.Get_Fields("Amount6"), "#,##0.00");
					// fldConvenienceFee.Text = Format(rsData.Fields("ConvenienceFee"), "#,##0.00")
					fldTotal.Text = Strings.Format(FCConvert.ToDouble(fldTitle1.Text) + FCConvert.ToDouble(fldTitle2.Text) + FCConvert.ToDouble(fldTitle3.Text) + FCConvert.ToDouble(fldTitle4.Text) + FCConvert.ToDouble(fldTitle5.Text) + FCConvert.ToDouble(fldTitle6.Text), "#,##0.00");
				}
			}
		}

		private void ActiveReport_Initialize()
		{
			SetupForm();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			rsARType.DisposeOf();
			rsData.DisposeOf();
			rsRec.DisposeOf();
			rsType.DisposeOf();
			rsARType = null;
			rsData = null;
			rsRec = null;
			rsType = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				ActiveReport_Initialize();
				string strSQL = "";
				int intType;
				string strOrderBy = "";
				bool boolUsePrevYears = false;

				boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
				if (modGlobal.Statics.gboolDailyAuditByAlpha)
				{
					strOrderBy = " ORDER BY Name, ReceiptNumber, ID asc";
				}
				else
				{
					strOrderBy = " ORDER BY ReceiptType, ReceiptNumber, ID asc, Name";
				}
				intType = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(FCConvert.ToString(this.UserData), 3))));
				rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intType), modExtraModules.strARDatabase);
				if (!rsType.EndOfFile())
				{
					if (FCConvert.ToString(rsType.Get_Fields_String("Account1")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account2")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account3")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account4")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account5")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account6")) == "M I")
					{
						if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields_String("Account1"))) == "M I")
						{
							intMIFeeNumber = 1;
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields_String("Account2"))) == "M I")
							{
								intMIFeeNumber = 2;
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields_String("Account3"))) == "M I")
								{
									intMIFeeNumber = 3;
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields_String("Account4"))) == "M I")
									{
										intMIFeeNumber = 4;
									}
									else
									{
										if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields_String("Account5"))) == "M I")
										{
											intMIFeeNumber = 5;
										}
										else
										{
											if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields_String("Account6"))) == "M I")
											{
												intMIFeeNumber = 6;
											}
											else
											{
												intMIFeeNumber = 0;
											}
										}
									}
								}
							}
						}
					}
					else
					{
						intMIFeeNumber = 0;
					}
				}
				else
				{
					intMIFeeNumber = 0;
				}
				if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
				{
					if (boolUsePrevYears)
					{
						strSQL = "SELECT * FROM LastYearArchive WHERE ReceiptType = 97" + " AND ARBillType = " + FCConvert.ToString(intType) + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOrderBy;
					}
					else
					{
						strSQL = "SELECT * FROM Archive WHERE ReceiptType = 97" + " AND ARBillType = " + FCConvert.ToString(intType) + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOrderBy;
					}
				}
				else
				{
					if (boolUsePrevYears)
					{
						strSQL = "SELECT * FROM LastYearArchive WHERE ReceiptType = 97" + " AND ARBillType = " + FCConvert.ToString(intType) + " AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOrderBy;
					}
					else
					{
						strSQL = "SELECT * FROM Archive WHERE ReceiptType = 97" + " AND ARBillType = " + FCConvert.ToString(intType) + " AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOrderBy;
					}
				}
				rsData.OpenRecordset(strSQL);
				if (boolUsePrevYears)
				{
					rsRec.OpenRecordset("SELECT * FROM LastYearReceipt");
				}
				else
				{
					rsRec.OpenRecordset("SELECT * FROM Receipt");
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Type Detail", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupForm()
		{
			bool boolLand = false;
			if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
			{
				boolLand = FCConvert.CBool(arDailyTellerAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			else
			{
				boolLand = FCConvert.CBool(arDailyAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			// If gstrEPaymentPortal = "P" Then
			// fldConvenienceFee.Visible = True
			// Else
			fldConvenienceFee.Visible = false;
			// End If
			if (!boolLand)
			{
				// normal
				fldName.Width = 3500 / 1440F;
				fldAcct.Width = 720 / 1440F;
				fldBill.Width = 540 / 1440F;
				fldReceipt.Width = 930 / 1440F;
				fldDate.Width = 1530 / 1440F;
				fldTeller.Width = 540 / 1440F;
				fldNC.Width = 720 / 1440F;
				fldPayType.Width = 630 / 1440F;
				fldCode.Width = 630 / 1440F;
				fldTitle1.Width = 1390 / 1440F;
				fldTitle2.Width = 1390 / 1440F;
				fldTitle3.Width = 1390 / 1440F;
				fldTitle4.Width = 1390 / 1440F;
				fldTitle5.Width = 1390 / 1440F;
				fldTitle6.Width = 1390 / 1440F;
				fldConvenienceFee.Width = 1390 / 1440F;
				fldTotal.Width = 1440 / 1440F;
				this.PrintWidth = 10800 / 1440F;
				fldName.Left = 0;
				fldAcct.Left = 720 / 1440F;
				fldBill.Left = 1440 / 1440F;
				fldReceipt.Left = 1980 / 1440F;
				fldDate.Left = 3500 / 1440F;
				fldTeller.Left = 2910 / 1440F;
				fldNC.Left = 3360 / 1440F;
				fldPayType.Left = 90 / 1440F;
				fldCode.Left = 3990 / 1440F;
				fldConvenienceFee.Left = 4040 / 1440F;
				fldTitle1.Left = 4620 / 1440F;
				fldTitle2.Left = 5560 / 1440F;
				fldTitle3.Left = 6320 / 1440F;
				fldTitle4.Left = 7080 / 1440F;
				fldTitle5.Left = 7840 / 1440F;
				fldTitle6.Left = 8600 / 1440F;
				fldTotal.Left = 9360 / 1440F;
				fldTitle2.Top = 540 / 1440F;
				fldTitle4.Top = 540 / 1440F;
				fldTitle6.Top = 540 / 1440F;
				fldConvenienceFee.Top = 540 / 1440F;
				Detail.Height = 810 / 1440F;
			}
			else
			{
				// landscape
				fldName.Width = 3500 / 1440F;
				fldAcct.Width = 720 / 1440F;
				fldBill.Width = 540 / 1440F;
				fldReceipt.Width = 930 / 1440F;
				fldDate.Width = 1530 / 1440F;
				fldTeller.Width = 540 / 1440F;
				fldNC.Width = 720 / 1440F;
				fldPayType.Width = 630 / 1440F;
				fldCode.Width = 630 / 1440F;
				fldTitle1.Width = 1320 / 1440F;
				fldTitle2.Width = 1320 / 1440F;
				fldTitle3.Width = 1320 / 1440F;
				fldTitle4.Width = 1320 / 1440F;
				fldTitle5.Width = 1320 / 1440F;
				fldTitle6.Width = 1320 / 1440F;
				fldConvenienceFee.Width = 1200 / 1440F;
				fldTotal.Width = 1370 / 1440F;
				this.PrintWidth = 15120 / 1440F;
				fldName.Left = 0;
				fldAcct.Left = 720 / 1440F;
				fldBill.Left = 1440 / 1440F;
				fldReceipt.Left = 1980 / 1440F;
				fldDate.Left = 3500 / 1440F;
				fldTeller.Left = 2910 / 1440F;
				fldNC.Left = 3360 / 1440F;
				fldPayType.Left = 90 / 1440F;
				fldCode.Left = 3990 / 1440F;
				fldConvenienceFee.Left = 4620 / 1440F;
				fldTitle1.Left = 5820 / 1440F;
				fldTitle2.Left = 7140 / 1440F;
				fldTitle3.Left = 8460 / 1440F;
				fldTitle4.Left = 9780 / 1440F;
				fldTitle5.Left = 11100 / 1440F;
				fldTitle6.Left = 12420 / 1440F;
				fldTotal.Left = 13740 / 1440F;
				fldTitle2.Top = 270 / 1440F;
				fldTitle4.Top = 270 / 1440F;
				fldTitle6.Top = 270 / 1440F;
				fldConvenienceFee.Top = 270 / 1440F;
				Detail.Height = 540 / 1440F;
			}
		}

		private int GetExternalRN_2(int lngRN)
		{
			return GetExternalRN(ref lngRN);
		}

		private int GetExternalRN(ref int lngRN)
		{
			int GetExternalRN = 0;
			// this will take the internal receipt number and get the external one for the user
			rsRec.FindFirstRecord("ReceiptKey", lngRN);
			if (rsRec.NoMatch)
			{
				GetExternalRN = 0;
			}
			else
			{
				GetExternalRN = FCConvert.ToInt32(Math.Round(Conversion.Val(rsRec.Get_Fields_Int32("ReceiptNumber"))));
			}
			return GetExternalRN;
		}

		private void sarTypeDetailAR_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarTypeDetailAR.Caption	= "Type Detail";
			//sarTypeDetailAR.Icon	= "sarTypeDetailAR.dsx":0000";
			//sarTypeDetailAR.Left	= 0;
			//sarTypeDetailAR.Top	= 0;
			//sarTypeDetailAR.Width	= 11880;
			//sarTypeDetailAR.Height	= 7830;
			//sarTypeDetailAR.SectionData	= "sarTypeDetailAR.dsx":058A;
			//End Unmaped Properties
		}
	}
}
