﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TWCR0000
{
    public class modGlobal
    {
        public static double Round(double dValue, short iDigits)
        {
            double Round = 0;
            // Round = Int(dValue * (10 ^ iDigits) + 0.5) / (10 ^ iDigits)
            Round = Conversion.Int(Convert.ToDouble(dValue * (Math.Pow(10, iDigits)) + 0.5)) / (Math.Pow(10, iDigits));
            return Round;
        }
    }
}