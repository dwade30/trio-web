﻿namespace TWCR0000.ReceiptProcess
{
	/// <summary>
	/// Summary description for sarGenericReceiptDetail.
	partial class sarNewGenericReceiptDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarNewGenericReceiptDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblComment = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNarrow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControlDetail1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControlDetail2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControlDetail3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNarrow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblComment,
				this.fldName,
				this.fldNarrow,
				this.fldControlDetail1,
				this.fldControlDetail2,
				this.fldControlDetail3
			});
			this.Detail.Height = 0.7291667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblComment
			// 
			this.lblComment.Height = 0.125F;
			this.lblComment.HyperLink = null;
			this.lblComment.Left = 0F;
			this.lblComment.MultiLine = false;
			this.lblComment.Name = "lblComment";
			this.lblComment.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblComment.Text = null;
			this.lblComment.Top = 0F;
			this.lblComment.Width = 3.75F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.125F;
			this.fldName.Left = 0F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0.625F;
			this.fldName.Width = 3.75F;
			// 
			// fldNarrow
			// 
			this.fldNarrow.CanGrow = false;
			this.fldNarrow.Height = 0.125F;
			this.fldNarrow.Left = 0F;
			this.fldNarrow.MultiLine = false;
			this.fldNarrow.Name = "fldNarrow";
			this.fldNarrow.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldNarrow.Text = null;
			this.fldNarrow.Top = 0.5F;
			this.fldNarrow.Visible = false;
			this.fldNarrow.Width = 3.75F;
			// 
			// fldControlDetail1
			// 
			this.fldControlDetail1.CanShrink = true;
			this.fldControlDetail1.Height = 0.125F;
			this.fldControlDetail1.Left = 0F;
			this.fldControlDetail1.MultiLine = false;
			this.fldControlDetail1.Name = "fldControlDetail1";
			this.fldControlDetail1.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldControlDetail1.Text = null;
			this.fldControlDetail1.Top = 0.125F;
			this.fldControlDetail1.Width = 3.75F;
			// 
			// fldControlDetail2
			// 
			this.fldControlDetail2.CanShrink = true;
			this.fldControlDetail2.Height = 0.125F;
			this.fldControlDetail2.Left = 0F;
			this.fldControlDetail2.MultiLine = false;
			this.fldControlDetail2.Name = "fldControlDetail2";
			this.fldControlDetail2.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldControlDetail2.Text = null;
			this.fldControlDetail2.Top = 0.25F;
			this.fldControlDetail2.Width = 3.75F;
			// 
			// fldControlDetail3
			// 
			this.fldControlDetail3.CanShrink = true;
			this.fldControlDetail3.Height = 0.125F;
			this.fldControlDetail3.Left = 0F;
			this.fldControlDetail3.MultiLine = false;
			this.fldControlDetail3.Name = "fldControlDetail3";
			this.fldControlDetail3.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldControlDetail3.Text = null;
			this.fldControlDetail3.Top = 0.375F;
			this.fldControlDetail3.Width = 3.75F;
			// 
			// sarGenericReceiptDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 3.75F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNarrow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblComment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNarrow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControlDetail1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControlDetail2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControlDetail3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
