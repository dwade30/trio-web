﻿using System;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCR0000.ReceiptProcess
{
    public partial class frmChooseReceipt : BaseForm, IModalView<IChooseExistingReceiptViewModel>
    {
        private bool cancelling = true;
        public frmChooseReceipt()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            cmdOk.Click += CmdOk_Click;
            this.Closing += FrmChooseReceipt_Closing;
            this.Load += FrmChooseReceipt_Load;
        }

        private void FrmChooseReceipt_Load(object sender, EventArgs e)
        {
            cmbArchive.SelectedIndex = 0;
            if (ViewModel.ReceiptNumber > 0)
            {
                txtReceipt.Text = ViewModel.ReceiptNumber.ToString();
            }
        }

        private void FrmChooseReceipt_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancelled = true;
            }
        }

        private void CloseWithoutCancelling()
        {
            cancelling = false;
            ViewModel.Cancelled = false;
            Close();
        }
        private void CmdOk_Click(object sender, EventArgs e)
        {
            SelectReceipt();
        }

        public frmChooseReceipt(IChooseExistingReceiptViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        public IChooseExistingReceiptViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        private void SelectReceipt()
        {
            var receiptNumber = txtReceipt.Text.ToIntegerValue();
            if (receiptNumber < 1)
            {
                FCMessageBox.Show("You must select a receipt number to continue",
                    MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly, "No Receipt");
                return;
            }

            if (!ViewModel.SelectReceipt(receiptNumber,IsCurrentYear()))
            {
                FCMessageBox.Show("That receipt was not found", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly,
                    "Receipt Not Found");
                return;
            }
            CloseWithoutCancelling();
        }

        private bool IsCurrentYear()
        {
            return cmbArchive.SelectedIndex == 0;
        }
    }
}
