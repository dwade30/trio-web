﻿namespace TWCR0000.ReceiptProcess
{
	/// <summary>
	/// Summary description for sarReceiptDetail.
	partial class sarNewReceiptDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarNewReceiptDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldNarrow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblComment = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModule = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControlDetail1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControlDetail2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldControlDetail3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldNarrow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModule)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldNarrow,
            this.lblComment,
            this.fldName,
            this.fldModule,
            this.fldRef,
            this.fldAmt,
            this.fldControlDetail1,
            this.fldControlDetail2,
            this.fldControlDetail3});
			this.Detail.Height = 1.156249F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldNarrow
			// 
			this.fldNarrow.CanGrow = false;
			this.fldNarrow.Height = 0.1875F;
			this.fldNarrow.Left = 0F;
			this.fldNarrow.MultiLine = false;
			this.fldNarrow.Name = "fldNarrow";
			this.fldNarrow.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldNarrow.Text = null;
			this.fldNarrow.Top = 0.75F;
			this.fldNarrow.Visible = false;
			this.fldNarrow.Width = 2.5F;
			// 
			// lblComment
			// 
			this.lblComment.Height = 0.1666667F;
			this.lblComment.HyperLink = null;
			this.lblComment.Left = 0F;
			this.lblComment.MultiLine = false;
			this.lblComment.Name = "lblComment";
			this.lblComment.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblComment.Text = null;
			this.lblComment.Top = 0.006944444F;
			this.lblComment.Width = 2.5F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0.9375F;
			this.fldName.Width = 2.5F;
			// 
			// fldModule
			// 
			this.fldModule.CanGrow = false;
			this.fldModule.Height = 0.1875F;
			this.fldModule.Left = 0F;
			this.fldModule.MultiLine = false;
			this.fldModule.Name = "fldModule";
			this.fldModule.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldModule.Text = null;
			this.fldModule.Top = 0.75F;
			this.fldModule.Width = 1F;
			// 
			// fldRef
			// 
			this.fldRef.CanGrow = false;
			this.fldRef.Height = 0.1875F;
			this.fldRef.Left = 1F;
			this.fldRef.MultiLine = false;
			this.fldRef.Name = "fldRef";
			this.fldRef.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldRef.Text = null;
			this.fldRef.Top = 0.75F;
			this.fldRef.Width = 0.6875F;
			// 
			// fldAmt
			// 
			this.fldAmt.CanGrow = false;
			this.fldAmt.Height = 0.1875F;
			this.fldAmt.Left = 1.6875F;
			this.fldAmt.MultiLine = false;
			this.fldAmt.Name = "fldAmt";
			this.fldAmt.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; white-space: now" +
    "rap";
			this.fldAmt.Text = null;
			this.fldAmt.Top = 0.75F;
			this.fldAmt.Width = 0.75F;
			// 
			// fldControlDetail1
			// 
			this.fldControlDetail1.CanShrink = true;
			this.fldControlDetail1.Height = 0.1875F;
			this.fldControlDetail1.Left = 0F;
			this.fldControlDetail1.MultiLine = false;
			this.fldControlDetail1.Name = "fldControlDetail1";
			this.fldControlDetail1.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldControlDetail1.Text = null;
			this.fldControlDetail1.Top = 0.1875F;
			this.fldControlDetail1.Width = 2.5F;
			// 
			// fldControlDetail2
			// 
			this.fldControlDetail2.CanShrink = true;
			this.fldControlDetail2.Height = 0.1875F;
			this.fldControlDetail2.Left = 0F;
			this.fldControlDetail2.MultiLine = false;
			this.fldControlDetail2.Name = "fldControlDetail2";
			this.fldControlDetail2.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldControlDetail2.Text = null;
			this.fldControlDetail2.Top = 0.375F;
			this.fldControlDetail2.Width = 2.5F;
			// 
			// fldControlDetail3
			// 
			this.fldControlDetail3.CanShrink = true;
			this.fldControlDetail3.Height = 0.1875F;
			this.fldControlDetail3.Left = 0F;
			this.fldControlDetail3.MultiLine = false;
			this.fldControlDetail3.Name = "fldControlDetail3";
			this.fldControlDetail3.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.fldControlDetail3.Text = null;
			this.fldControlDetail3.Top = 0.5625F;
			this.fldControlDetail3.Width = 2.5F;
			// 
			// sarNewReceiptDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 2.625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldNarrow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModule)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldControlDetail3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNarrow;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblComment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModule;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControlDetail1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControlDetail2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldControlDetail3;
	}
}
