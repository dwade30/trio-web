﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.Extensions;
using SharedApplication.Receipting;

namespace TWCR0000
{
    /// <summary>
    /// Summary description for frmTellerID.
    /// </summary>
    public partial class frmGetTellerID : BaseForm, IModalView<IGetTellerIdViewModel>
    {
        public frmGetTellerID()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
           
        }

        public frmGetTellerID(IGetTellerIdViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }


        private void InitializeComponentEx()
        {
            this.cmdSave.Click += CmdSave_Click;
            this.Load += FrmGetTellerID_Load;
            this.txtTellerID.KeyPress += TxtTellerID_KeyPress;
            this.txtPassword.KeyPress += TxtPassword_KeyPress;
        }

        private void TxtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            Keys keyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (keyAscii == Keys.Return || keyAscii == Keys.Enter)
            {
                SelectTeller();
            }
        }

        private void TxtTellerID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Keys keyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (keyAscii == Keys.Return || keyAscii == Keys.Enter)
            {
                if (ViewModel.RequirePassword)
                {
                    txtPassword.Focus();
                }
                else
                {
                    SelectTeller();
                }
            }
        }

        private void FrmGetTellerID_Load(object sender, EventArgs e)
        {
            if (ViewModel.RequirePassword)
            {
                txtPassword.Visible = true;
                lblPassword.Visible = true;
            }

            if (ViewModel.ShowDefaultTeller)
            {
                txtTellerID.Text = ViewModel.DefaultTeller;
            }
        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
            SelectTeller();
        }

        private void SelectTeller()
        {
            var tellerId = txtTellerID.Text;
            if (String.IsNullOrWhiteSpace(tellerId))
            {
                MessageBox.Show("You didn't enter a teller id", "Invalid teller id", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            var result = ViewModel.ValidateTeller(txtTellerID.Text, txtPassword.Text);
            if (result == TellerValidationResult.Invalid)
            {
                if (ViewModel.RequirePassword)
                {
                    MessageBox.Show("You didn't enter a valid teller / password", "Invalid teller credentials", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("You didn't enter a valid teller ID", "Invalid teller ID", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }

                return;
            }

            if (result == TellerValidationResult.LoggedIn)
            {
                if (MessageBox.Show("Teller " + tellerId + " is already logged in, would you like to continue?", "Teller Logged In", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {                    
                    return;
                }
            }
            ViewModel.ChosenTeller = tellerId;
            Close();
        }

        public IGetTellerIdViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }
    }

}
