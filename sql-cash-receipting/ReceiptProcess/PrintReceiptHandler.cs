﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;

namespace TWCR0000.ReceiptProcess
{
	public class PrintReceiptHandler : CommandHandler<PrintReceipt>
	{
		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private DataEnvironmentSettings dataEnvironmentSettings;
		private IRegionalTownService regionalTownService;
		private Setting cashReceiptPrinter;
		private ISettingService settingService;
		private IReceiptTypesService receiptTypesService;

		public PrintReceiptHandler(GlobalCashReceiptSettings globalCashReceiptSettings, ISettingService settingService, DataEnvironmentSettings dataEnvironmentSettings, IRegionalTownService regionalTownService, IReceiptTypesService receiptTypesService)
		{
			this.globalCashReceiptSettings = globalCashReceiptSettings;
			this.settingService = settingService;
			this.dataEnvironmentSettings = dataEnvironmentSettings;
			this.regionalTownService = regionalTownService;
			this.receiptTypesService = receiptTypesService;

			cashReceiptPrinter = settingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName");
            if (cashReceiptPrinter == null)
            {
                cashReceiptPrinter = new Setting(){SettingName = "RCPTPrinterName"};
            }
		}

		protected override void Handle(PrintReceipt command)
		{
			try
			{
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();

				var splits = command.TransactionGroup.GetTransactions().Select(x => x.Split).Distinct();
				var townName = dataEnvironmentSettings.CityTownClientName;
				var townAbbreviation = "";

				decimal convenienceFee = 0;

				bool firstSplit = true;
				bool firstCopy = true;

				for (int counter = 1; counter <= command.Copies; counter++)
				{
					if (splits != null)
					{
						foreach (var split in splits)
						{
							var splitTransactions = command.TransactionGroup.GetTransactions().Where(x => x.Split == split);
							if (dataEnvironmentSettings.IsMultiTown)
							{
								townName = regionalTownService.TownName(splitTransactions.FirstOrDefault()?.TownId ?? 1);
								townAbbreviation = regionalTownService.TownAbbreviation(splitTransactions.FirstOrDefault()?.TownId ?? 1);
							}

							convenienceFee = firstSplit ? command.ConvenienceFee : 0;
							firstSplit = false;

							FCSectionReport report;

							if (globalCashReceiptSettings.SP200Printer || !globalCashReceiptSettings.NarrowReceipt)
							{
								report = new arNewReceipt(splitTransactions, command.Payments, townName, convenienceFee,
									command.Reprint, globalCashReceiptSettings, command.PaidBy, settingService, command.ReceiptNumber,
									command.Change, firstCopy, command.TransactionGroup.TellerId, command.ReceiptDate,
									receiptTypesService, townAbbreviation, cashReceiptPrinter.SettingValue);
							}
							else
							{
								report = new arNewGenericReceipt(splitTransactions, command.Payments, townName, convenienceFee,
									command.Reprint, globalCashReceiptSettings, command.PaidBy, settingService, command.ReceiptNumber,
									command.Change, firstCopy, command.TransactionGroup.TellerId, command.ReceiptDate,
									receiptTypesService, townAbbreviation, cashReceiptPrinter.SettingValue);
							}

							if (SetupAsPrinterFile(cashReceiptPrinter.SettingValue))
							{
								ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
								report.Run(false);
								string fileName = "PRNT" + split + ".TXT";
								ARTemp.Export(report.Document, fileName);
							}
							else
							{
								report.PrintReportOnDotMatrix("RCPTPrinterName");
							}
						}

						firstCopy = false;
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
			
		}

		private bool SetupAsPrinterFile(string printerName)
		{
			if (printerName.ToUpper() == "PRNT.TXT")
			{
				return true;
			}

			return false;
		}
	}
}
