﻿namespace TWCR0000.ReceiptProcess
{
    partial class frmChooseReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseReceipt));
            this.cmbArchive = new fecherFoundation.FCComboBox();
            this.txtReceipt = new fecherFoundation.FCTextBox();
            this.lblArchive = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.cmdOk = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdOk);
            this.BottomPanel.Location = new System.Drawing.Point(3, 144);
            this.BottomPanel.Size = new System.Drawing.Size(454, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.lblArchive);
            this.ClientArea.Controls.Add(this.cmbArchive);
            this.ClientArea.Controls.Add(this.txtReceipt);
            this.ClientArea.Size = new System.Drawing.Size(474, 257);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReceipt, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbArchive, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblArchive, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(474, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // cmbArchive
            // 
            this.cmbArchive.Items.AddRange(new object[] {
            "Current Year",
            "Archive Year"});
            this.cmbArchive.Location = new System.Drawing.Point(159, 104);
            this.cmbArchive.Name = "cmbArchive";
            this.cmbArchive.Size = new System.Drawing.Size(147, 40);
            this.cmbArchive.TabIndex = 2;
            // 
            // txtReceipt
            // 
            this.txtReceipt.BackColor = System.Drawing.SystemColors.Window;
            this.javaScript1.SetJavaScript(this.txtReceipt, resources.GetString("txtReceipt.JavaScript"));
            this.txtReceipt.Location = new System.Drawing.Point(159, 21);
            this.txtReceipt.MaxLength = 9;
            this.txtReceipt.Name = "txtReceipt";
            this.txtReceipt.Size = new System.Drawing.Size(78, 40);
            this.txtReceipt.TabIndex = 1;
            // 
            // lblArchive
            // 
            this.lblArchive.Location = new System.Drawing.Point(21, 115);
            this.lblArchive.Name = "lblArchive";
            this.lblArchive.Size = new System.Drawing.Size(60, 16);
            this.lblArchive.TabIndex = 1001;
            this.lblArchive.Text = "YEAR";
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(21, 37);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(132, 16);
            this.fcLabel1.TabIndex = 1002;
            this.fcLabel1.Text = "RECEIPT NUMBER";
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "acceptButton";
            this.cmdOk.Location = new System.Drawing.Point(177, 30);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdOk.Size = new System.Drawing.Size(100, 48);
            this.cmdOk.TabIndex = 2;
            this.cmdOk.Text = "OK";
            // 
            // frmChooseReceipt
            // 
            this.ClientSize = new System.Drawing.Size(474, 317);
            this.ForeColor = System.Drawing.Color.FromName("@appWorkspace");
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChooseReceipt";
            this.Text = "Choose Receipt";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCComboBox cmbArchive;
        public fecherFoundation.FCTextBox txtReceipt;
        public fecherFoundation.FCLabel lblArchive;
        public fecherFoundation.FCLabel fcLabel1;
        private fecherFoundation.FCButton cmdOk;
        private Wisej.Web.JavaScript javaScript1;
    }
}