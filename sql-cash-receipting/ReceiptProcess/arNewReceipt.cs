﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using System.Drawing;
using System.Linq;
using GrapeCity.ActiveReports.SectionReportModel;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.UtilityBilling.Commands;
using TWSharedLibrary;
using Label = GrapeCity.ActiveReports.SectionReportModel.Label;

namespace TWCR0000.ReceiptProcess
{
	/// <summary>
	/// Summary description for arReceipt.
	/// </summary>
	public partial class arNewReceipt : BaseSectionReport
	{
		float[] lngLeftCol = new float[5 + 1];
		float lngTotalWidth;
		int intPaymentNumber;
		string strNarrowHeader = "";
		string strNarrowReceipt;
		bool boolSmallVersion;
		bool bool1STPage;
        bool stopReport = false;
		bool blnShowSigLine;
		private IEnumerable<TransactionBase> transactions;
		private string clientName;
		private decimal convenienceFee;
		private bool reprint;
		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private IEnumerable<ReceiptPayment> payments;
		private string paidBy;
		private bool isBatchPayment = false;
		private ISettingService settingService;
		private int receiptNumber;
		private decimal change;
		private bool firstCopy;
		private string tellerId;
		private DateTime receiptDate;
		private IReceiptTypesService receiptTypesService;
		private string townAbbreviation;
		private string cashReceiptPrinter;
		private TransactionBase currentTransaction;
		public arNewReceipt()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			
		}

		public arNewReceipt(IEnumerable<TransactionBase> transactions, IEnumerable<ReceiptPayment> payments,
			string clientName, decimal convenienceFee, bool reprint,
			GlobalCashReceiptSettings globalCashReceiptSettings, string paidBy,
			ISettingService settingService, int receiptNumber, decimal change, bool firstCopy, string tellerId,
			DateTime receiptDate, IReceiptTypesService receiptTypesService, string townAbbreviation,
			string cashReceiptPrinter) : this()
		{
			try
			{
				this.transactions = transactions;
				this.payments = payments;
				this.clientName = clientName;
				this.convenienceFee = convenienceFee;
				this.reprint = reprint;
				this.globalCashReceiptSettings = globalCashReceiptSettings;
				this.paidBy = paidBy;
				this.settingService = settingService;
				this.receiptNumber = receiptNumber;
				this.change = change;
				this.firstCopy = firstCopy;
				this.tellerId = tellerId;
				this.receiptDate = receiptDate;
				this.receiptTypesService = receiptTypesService;
				this.townAbbreviation = townAbbreviation;
				this.cashReceiptPrinter = cashReceiptPrinter;

				isBatchPayment = (this.transactions.FirstOrDefault()?.Split ?? 0) > 5;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				if (intPaymentNumber >= transactions.Count())
				{
					eArgs.EOF = true;
                    stopReport = true;
				}
				else
				{
					currentTransaction = transactions.ToList()[intPaymentNumber];
					eArgs.EOF = false;
				}

				intPaymentNumber += 1;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Fetch Data ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetArraySizes()
		{
			try
			{
				if (!globalCashReceiptSettings.NarrowReceipt)
				{
					lngLeftCol[0] = 0;
					lngLeftCol[1] = 1170 / 1440f;
					lngLeftCol[2] = 1170 / 1440f;
					lngLeftCol[3] = 3060 / 1440f;
					lngLeftCol[4] = 4950 / 1440f;
					lngLeftCol[5] = 5130 / 1440f;
                    lngTotalWidth = 5.5f;// 7215 / 1440f;

                    //if (globalCashReceiptSettings.ReceiptLength > 0)
                    //{
                    //	this.PageSettings.PaperHeight = 247 * globalCashReceiptSettings.ReceiptLength / 1440f;
                    //}
                    // this should set the paper height
                    PageSettings.Margins.Left = .5f; // 720 / 1440f;
                    PageSettings.Margins.Right = .5f; // 720 / 1440f;
					if (globalCashReceiptSettings.WideReceiptNoMargins)
					{
						PageSettings.Margins.Top = 0;
					}
					else
					{
                        PageSettings.Margins.Top = .25f; // 360 / 1440f;
					}
					if (modGlobal.Statics.gboolRecieptThin)
					{
                        PageSettings.Margins.Bottom = 1;// 1440 / 1440f;
					}
					else
					{
						PageSettings.Margins.Bottom = 0;
					}

					if (globalCashReceiptSettings.ReceiptLength == 66)
					{
                        this.PageSettings.PaperWidth = 8; // 11520 / 1440f;
					}
				}
				else
				{
                    lngLeftCol[0] = 0;
                    lngLeftCol[1] = 88.875f / 1440f;
                    lngLeftCol[2] = 0;
                    lngLeftCol[3] = 177.75f / 1440f;
                    lngLeftCol[4] = 1066.5f / 1440f;
                    lngLeftCol[5] = 266.625f / 1440f;
                    lngTotalWidth = 3555 / 1440f;
                    //PageSettings.PaperHeight = 1440 * 10 / 1440f; // 22
                    PageSettings.PaperWidth = (lngTotalWidth + 2);//  / 1440f;
                    PageSettings.Margins.Top = 0;
					PageSettings.Margins.Bottom = 0;
					PageSettings.Margins.Left = 0;
					PageSettings.Margins.Right = 0;
					strNarrowHeader = "";
					strNarrowReceipt = "";
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Array Size", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupReceiptFormat()
		{
			try
			{
				lblHeader.Left = lngLeftCol[0];
				lblTopComment.Left = lngLeftCol[0];
				lblDate.Left = lngLeftCol[0];
				lblHeaderType.Left = lngLeftCol[0];
				sarReceiptDetailOb.Left = lngLeftCol[0];
				lblBottomComment.Left = lngLeftCol[0];
				lblMuniName.Left = lngLeftCol[0];
				lblTime.Left = lngLeftCol[1];
				lblPaidBy.Left = 0;
				if (reprint)
				{
					lblOtherAccounts.Left = 0;
					lblReprint.Left = 0;
				}
				else
				{
					if (!globalCashReceiptSettings.NarrowReceipt)
					{
						lblOtherAccounts.Left = lblPaidBy.Left;
						lblOtherAccounts.Width = this.PrintWidth - lblOtherAccounts.Left;
					}
					else
					{
						lblOtherAccounts.Left = lngLeftCol[2];
					}
				}

				lblTeller.Left = lngLeftCol[3];
				lblHeaderRef.Left = lngLeftCol[3];
				if (globalCashReceiptSettings.NarrowReceipt)
					lblTeller.Left = lblTime.Left + lblTime.Width;

				lblHeaderAmt.Left = lngLeftCol[5];
				lblReceiptNumber.Left = lngLeftCol[5] - 444.375f / 1440f;
				fldTotal.Left = lngLeftCol[5] - (globalCashReceiptSettings.ReceiptOffset * 140 / 1440f);
				if (fldTotal.Left - lblTotal.Width < 0)
				{
					lblTotal.Left = 0;
					fldTotal.Left = lblTotal.Width;
				}
				else
				{
					lblTotal.Left = fldTotal.Left - lblTotal.Width;
				}
				// set the widths
				if (!globalCashReceiptSettings.NarrowReceipt)
				{
					// wide
					lblHeaderAmt.Visible = true;
					lblHeaderType.Visible = true;
					lblHeaderRef.Visible = true;
					lblDate.Visible = true;
					lblTime.Visible = true;
					lblTeller.Visible = true;
					lblReceiptNumber.Visible = true;
					lblHeaderReceipt.Visible = false;
					lblHeaderTitle.Visible = false;
					lblHeaderAmt.Width = 1440 / 1440f;
					lblTeller.Width = 2100 / 1440f;
					lblReceiptNumber.Width = 2100 / 1440f;
					fldTotal.Width = 1440 / 1440f;
					lblMuniName.Width = lngTotalWidth;
					lblHeader.Width = lngTotalWidth;
					lblTopComment.Width = lngTotalWidth;
					lblBottomComment.Width = lngTotalWidth;
					lblPaidBy.Width = lngTotalWidth - lblPaidBy.Left;
					lblCheck1.Width = lngTotalWidth - lblCheck1.Left;
					lblTime.Left = lblDate.Left + lblDate.Width;
					if (globalCashReceiptSettings.CompactedReceipt || (globalCashReceiptSettings.ReceiptLength == 22 && !globalCashReceiptSettings.NarrowReceipt))
					{
						// this will shrink the header
						lblMuniName.Top = 0;
						lblHeader.Top = 240 / 1440f;
						lblTopComment.Top = 480 / 1440f;
						lblReprint.Top = 720 / 1440f;
						lblHeaderReceipt.Top = 960 / 1440f;
						lblTeller.Top = 960 / 1440f;
						lblTime.Top = 960 / 1440f;
						lblDate.Top = 960 / 1440f;
						lblReceiptNumber.Top = 960 / 1440f;
						lblHeaderTitle.Top = 1200 / 1440f;
						lblHeaderAmt.Top = 1200 / 1440f;
						lblHeaderRef.Top = 1200 / 1440f;
						lblHeaderType.Top = 1200 / 1440f;
						lnHeader.Y1 = 1440 / 1440f;
						lnHeader.Y2 = 1440 / 1440f;
						PageHeader.Height = 1440 / 1440f;
						// this will shrink the footer
						lblTotal.Top = 0;
						fldTotal.Top = 0;
						lblPaidBy.Top = 240 / 1440f;
						lblOtherAccounts.Top = 480 / 1440f;
						if (globalCashReceiptSettings.ReceiptLength == 22)
						{
							if (blnShowSigLine)
							{
								lblSignature.Left = lblPaidBy.Left + lblPaidBy.Width + 100 / 1440f;
								lblSignature.Top = lblPaidBy.Top;
								lblSignatureLine.Left = lblSignature.Left;
								lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
								lblBottomComment.Top = 720 / 1440f;
								lblCheck1.Top = 960 / 1440f;
								ReportFooter.Height = 1200 / 1440f;
							}
							else
							{
								lblBottomComment.Top = 720 / 1440f;
								lblCheck1.Top = 960 / 1440f;
								ReportFooter.Height = 1200 / 1440f;
							}
						}
						else
						{
							if (blnShowSigLine)
							{
								lblSignature.Top = 720 / 1440f;
								lblSignatureLine.Top = 1220 / 1440f;
								lblBottomComment.Top = 1460 / 1440f;
								lblCheck1.Top = 1700 / 1440f;
								ReportFooter.Height = 1940 / 1440f;
							}
							else
							{
								lblBottomComment.Top = 720 / 1440f;
								lblCheck1.Top = 960 / 1440f;
								ReportFooter.Height = 1200 / 1440f;
							}
						}
					}
				}
				else
				{
					// narrow
					lblHeaderAmt.Visible = false;
					lblHeaderType.Visible = false;
					lblHeaderRef.Visible = false;
					lblDate.Visible = false;
					lblTime.Visible = false;
					lblTeller.Visible = false;
					lblReceiptNumber.Visible = false;
					lblHeaderReceipt.Visible = true;
					lblHeaderTitle.Visible = true;
					lblTotal.Width = lngTotalWidth;
					lblTotal.Left = 0;
					lblHeaderAmt.Width = 533.25f / 1440f;
                    lblTeller.Width = 533.25f / 1440f;
                    lblReceiptNumber.Width = 533.25f / 1440f;
                    lblMuniName.Width = lngTotalWidth - 177.75f / 1440f;
                    lblHeader.Width = lngTotalWidth - 177.75f / 1440f;
                    lblTopComment.Width = lngTotalWidth - 177.75f / 1440f;
                    lblBottomComment.Width = lngTotalWidth - 177.75f / 1440f;
                }
				lblHeaderTitle.Width = lngTotalWidth;
				lblHeaderReceipt.Width = lngTotalWidth;
				if (reprint)
				{
					lblReprint.Width = lngTotalWidth - 177.75f / 1440f;
                    lblOtherAccounts.Width = lngTotalWidth - 177.75f / 1440f;
                }

				sarReceiptDetailOb.Width = lngTotalWidth;
                if (!globalCashReceiptSettings.SP200Printer || !globalCashReceiptSettings.NarrowReceipt)
                {
                    PrintWidth = lngTotalWidth + 2; // / 1440f;
                }
                else
                {
                    PrintWidth = lngTotalWidth;
                }

                PageSettings.PaperWidth = this.PrintWidth + this.PageSettings.Margins.Left + this.PageSettings.Margins.Right;
                lnHeader.X1 = 0;
				lnHeader.X2 = lngTotalWidth - 200 / 1440f;
			}
			catch (Exception ex)
			{
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Setting Receipt Format");
			}
		}

		private void SetMainLabels()
		{
			try
			{
				// 1st line
				lblHeader.Text = "-----  R e c e i p t  -----";
				// 2nd line
				if (globalCashReceiptSettings.SP200Printer)
				{
					lblTopComment.Text = Strings.Trim(globalCashReceiptSettings.TopComment) + "     ";
				}
				else
				{
					lblTopComment.Text = Strings.Trim(globalCashReceiptSettings.TopComment);
				}
				// 3nd line
				lblTime.Text = receiptDate.ToString("h:mm tt");
				strNarrowReceipt = new string(' ', globalCashReceiptSettings.ReceiptOffset) + receiptDate.ToString("MM/dd/yy").PadRight(9);
				strNarrowReceipt += receiptDate.ToString("h:mm tt").PadRight(9);
				
				if (globalCashReceiptSettings.ReceiptOffset < 3)
				{
					strNarrowReceipt += "ID:" + tellerId.PadRight(9 - globalCashReceiptSettings.ReceiptOffset);
				}
				else
				{
					strNarrowReceipt += " " + tellerId.PadRight(8 - globalCashReceiptSettings.ReceiptOffset);
				}

				strNarrowReceipt += "#" + receiptNumber.ToString().PadRight(8) + "-" + transactions.FirstOrDefault()?.Split;
				strNarrowHeader = new string(' ', globalCashReceiptSettings.ReceiptOffset) + "TYPE------".PadRight(18 - globalCashReceiptSettings.ReceiptOffset) + "REF---".PadRight(9 - globalCashReceiptSettings.ReceiptOffset) + "AMOUNT".PadRight(7);
				
				lblHeaderTitle.Text = strNarrowHeader;
				lblHeaderReceipt.Text = strNarrowReceipt;
				lblMuniName.Text = clientName;

				if (!globalCashReceiptSettings.NarrowReceipt)
				{
					// wide
					lblDate.Text = receiptDate.ToString("MM/dd/yyyy");
					lblReceiptNumber.Text = "#" + receiptNumber;

					lblTeller.Text = reprint || globalCashReceiptSettings.TellerIdUsage == TellerIDUsage.NotRequired ? "" : "Teller: " + tellerId;
					lblReprint.Visible = reprint;

					fldTotal.Visible = true;
					lblTotal.Visible = true;
					lblTotal.Text = "Total: ";
				}
				else
				{
					// narrow
					lblDate.Text = receiptDate.ToString("MM/dd/yy");
					
					lblReceiptNumber.Text = "#" + receiptNumber;
					lblReprint.Visible = reprint;
					if (globalCashReceiptSettings.TellerIdUsage == TellerIDUsage.NotRequired)
					{
						lblTeller.Text = "";
					}
					else
					{
						lblTeller.Text = "ID:" + tellerId;
					}

					fldTotal.Visible = true;
					lblTotal.Visible = true;
					lblTotal.Text = "Total: ";
				}
				
				if (isBatchPayment)
				{
					lblOtherAccounts.Visible = false;
					lblSignature.Visible = false;
					lblSignatureLine.Visible = false;
					lblPaidBy.Top = 450 / 1440F;
					lblBottomComment.Top = 810 / 1440F;
				}

				// 4rd line
				lblHeaderType.Text = "Type";
				lblHeaderRef.Text = "Reference";
				lblHeaderAmt.Text = "Amount";
				if (!isBatchPayment)
				{
					fldTotal.Text = transactions.Select(x => x.ToReceiptSummaryItem()).Sum(a => a.Amount).ToString("#,##0.00") + "*";
				}
				if (globalCashReceiptSettings.SP200Printer)
				{
					lblBottomComment.Text = Strings.Trim(globalCashReceiptSettings.BottomComment) + "      ";
				}
				else
				{
					lblBottomComment.Text = Strings.Trim(globalCashReceiptSettings.BottomComment);
				}
				return;
			}
			catch (Exception ex)
			{
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Setting Main Labels");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			
		}

		private decimal TotalPaid
		{
			get { return payments.Sum(p => p.Amount); }
		}

        public void StartUp()
        {
            this.ReportFooter.Height = 3000 / 1440F + (globalCashReceiptSettings.ReceiptExtraLines * 550) / 1440F;
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            try
            {
                if (globalCashReceiptSettings.PrintSignature == PrintSignatureOption.Yes)
                {
                    blnShowSigLine = true;
                }
                else if (globalCashReceiptSettings.PrintSignature == PrintSignatureOption.OnlyOnCreditCardPayments)
                {
                    blnShowSigLine = TotalCreditPaid != 0;
                }
                else
                {
                    blnShowSigLine = false;
                }
                SetArraySizes();
                SetupReceiptFormat();
                intPaymentNumber = 0;
                SetMainLabels();
                StartUp();

				lblPaidBy.Text = new string(' ', globalCashReceiptSettings.ReceiptOffset) + "Paid By: " + paidBy;

                if (!globalCashReceiptSettings.NarrowReceipt)
                {
                    SetReceiptFontSize();
                }
                if (globalCashReceiptSettings.NarrowReceipt)
                {
                    boolSmallVersion = true;
                    bool1STPage = true;
                }
                if (cashReceiptPrinter == "")
                {
                    MessageBox.Show("No Receipt Printer is setup, please check the setup of your receipt printer in the printer setup on General Entry.", "Printer Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this.Close();
                    return;
                }

                var controlsNeeded = payments.Count() + (convenienceFee != 0 ? 1 : 0);
                for (int counter = 1; counter <= controlsNeeded; counter++)
                {
                    GrapeCity.ActiveReports.SectionReportModel.Label obNew = ReportFooter.AddControlWithName<Label>("lblCheck" + FCConvert.ToString(counter));
                    obNew.Visible = false;
                }
				
                ReportFooter_Format();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Receipt", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
		}

		private void AddFooterFeeRow(string strFeeDesc, decimal dblFee, float lngTop, int counter, bool boolOverride = false)
		{
			try
			{
				GrapeCity.ActiveReports.SectionReportModel.Label obNew = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(counter));
				string strTemp;

				obNew.Top = lngTop;
				obNew.Left = lngLeftCol[2];
				obNew.Width = this.PrintWidth - lngLeftCol[2];
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				obNew.Font = fldTotal.Font;
				if (dblFee != 0 || boolOverride)
				{
					if (boolOverride)
					{
						obNew.Text = new string(' ', globalCashReceiptSettings.ReceiptOffset) + strFeeDesc.PadRight(7);
					}
					else
					{
						obNew.Text = new string(' ', globalCashReceiptSettings.ReceiptOffset) + strFeeDesc.PadRight(7) + ":" + dblFee.ToString("#,##0.00").PadLeft(13);
					}
				}
				else
				{
					obNew.Text = " ";
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Footer Row", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_AfterPrint(object sender, EventArgs e)
		{
			try
			{
				sarReceiptDetailOb.Report = null;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error After Detail", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				var wideFontSize = settingService.GetSettingValue(SettingOwnerType.User, "CRWideFontSize");
				sarReceiptDetailOb.Report = new sarNewReceiptDetail(globalCashReceiptSettings, townAbbreviation, paidBy, wideFontSize == null ? 0 : wideFontSize.SettingValue.ToIntegerValue());

                //var showFeeDetail = receiptTypesService.GetReceiptTypeByCode(currentTransaction.TransactionTypeCode.ToIntegerValue()).ShowFeeDetailOnReceipt ?? false;
                var showFeeDetail = receiptTypesService.GetReceiptTypeFromTransaction(currentTransaction).ShowFeeDetailOnReceipt ?? false;
                var receiptDetailsUserData = new ReceiptDetailsUserData
				{
					transaction = currentTransaction,
					ShowFeeDetails = showFeeDetail
				};

				sarReceiptDetailOb.Report.UserData = receiptDetailsUserData;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_BeforePrint(object sender, EventArgs e)
		{
            if (stopReport)
            {
                this.Stop();
            }
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolSmallVersion)
				{
					if (bool1STPage)
					{
                        //FC:FINAL:SBE - #3235 - for unknown reason export to TIFF generates multiple pages, and the Visible and Height remains with previous values
                        PageHeader.Height = 1.229167F;
                        PageHeader.Visible = true;
                        bool1STPage = false;
					}
					else
					{
						PageHeader.Height = 0;
						PageHeader.Visible = false;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Page Header", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupFooterFields()
		{
			if (globalCashReceiptSettings.CompactedReceipt && !globalCashReceiptSettings.NarrowReceipt)
			{
				lblPaidBy.Top = 240 / 1440f;
				lblOtherAccounts.Top = 480 / 1440f;
				if (globalCashReceiptSettings.ReceiptLength == 22)
				{
					if (blnShowSigLine)
					{
						lblSignature.Left = 4320 / 1440f;
						lblSignature.Top = lblPaidBy.Top + 240 / 1440f;
						lblSignatureLine.Left = lblSignature.Left;
						lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
						lblBottomComment.Top = 720 / 1440f;
					}
					else
					{
						lblBottomComment.Top = 720 / 1440f;
					}
				}
				else
				{
					if (blnShowSigLine)
					{
						lblSignature.Top = 720 / 1440f;
						lblSignatureLine.Top = 1220 / 1440f;
						lblBottomComment.Top = 1460 / 1440f;
					}
					else
					{
						lblBottomComment.Top = 720 / 1440f;
					}
				}
			}
			else
			{
				if (globalCashReceiptSettings.ReceiptLength == 22)
				{
					lblPaidBy.Top = 240 / 1440f;
					lblOtherAccounts.Top = 480 / 1440f;
					if (blnShowSigLine)
					{
						lblSignature.Top = 960 / 1440f;
						lblSignatureLine.Left = lblSignature.Left;
						lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
						lblBottomComment.Top = 1700 / 1440f;
					}
					else
					{
						lblBottomComment.Top = 720 / 1440f;
					}
				}
				else
				{
					lblPaidBy.Top = 480 / 1440f;
					lblOtherAccounts.Top = 960 / 1440f;
					if (blnShowSigLine)
					{
						lblSignature.Top = 1440 / 1440f;
						lblSignatureLine.Top = 1940 / 1440f;
						lblBottomComment.Top = 2180 / 1440f;
					}
					else
					{
						lblBottomComment.Top = 1440 / 1440f;
					}
				}
			}
		}

		private void ReportFooter_Format()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				// this will count all the rows used in the footer
				float lngStart = 0;
				string strTemp = "";
				bool boolShowTotalLine;
				int intXLines;
				float lngFieldHeight;
				string strRegValue = "";
				lngFieldHeight = 240 / 1440f;

				fldTotal.Top = 0;
				lblTotal.Top = 0;
				if (!blnShowSigLine)
				{
					lblSignature.Visible = false;
					lblSignatureLine.Visible = false;
					lblSignature.Top = 0;
					lblSignatureLine.Top = 0;
				}

				if (reprint || isBatchPayment)
				{
					fldTotal.Visible = true;
					lblTotal.Visible = true;

					lblOtherAccounts.Width = lblBottomComment.Width - lblOtherAccounts.Left;
					lblCheck1.Visible = false;
				}
				else
				{
					var showFeeDetail =
						receiptTypesService
							 .GetReceiptTypeByCode(receiptTypesService.GetReceiptTypeCodeFromTransaction( transactions.FirstOrDefault()))
							 .ShowFeeDetailOnReceipt ?? false;
					//strTemp = modUseCR.GetInfoAboutOtherAccounts(transactions, firstCopy, reprint, isBatchPayment);
                    strTemp = StaticSettings.GlobalCommandDispatcher.Send(new GetReceiptRemainingBalanceText
                    {
                        Transactions = transactions,
                        IsFirstCopy = firstCopy,
                        IsReprint = reprint,
                        IsBatch = isBatchPayment
                    }).Result;

                    if (transactions.Count() == 1 && convenienceFee == 0 && !showFeeDetail)
					{
						lblTotal.Text = "   ";
						fldTotal.Visible = false;
					}
				}

				SetupFooterFields();

				intCT = 0;
				if (globalCashReceiptSettings.CompactedReceipt && !globalCashReceiptSettings.NarrowReceipt)
				{
					lngStart = lblBottomComment.Top + lngFieldHeight;
				}
				else
				{
					if (globalCashReceiptSettings.ReceiptLength == 22)
					{
						lngStart = lblBottomComment.Top + lngFieldHeight;
					}
					else
					{
						lngStart = lblBottomComment.Top + (lngFieldHeight * 2);
					}
				}
				// footer
				if (reprint)
				{
					lblPaidBy.Text = new string(' ', globalCashReceiptSettings.ReceiptOffset) + "Paid By: " +
					                 paidBy.Trim();

					lblReprint.Text = "***  REPRINT  ***";
					lblOtherAccounts.Text = "***  REPRINT  ***";

					var lastReceiptSetting = settingService.GetSettingValue(SettingOwnerType.Machine, "CRLastReceiptNumber");
					
					if (lastReceiptSetting != null)
					{
						if (lastReceiptSetting.SettingValue.ToIntegerValue() == receiptNumber)
						{
							var lastReceiptDetailsSetting = settingService.GetSettingValue(SettingOwnerType.Machine, "CRLastRemainingBalance");

							if (lastReceiptDetailsSetting != null)
							{
								lblOtherAccounts.Text = lastReceiptDetailsSetting.SettingValue;
							}
						}
					}
				}
				else
				{
					lblOtherAccounts.Text = strTemp + " ";
					settingService.SaveSetting(SettingOwnerType.Machine, "CRLastReceiptNumber", lblReceiptNumber.Text);
					settingService.SaveSetting(SettingOwnerType.Machine, "CRLastRemainingBalance", lblOtherAccounts.Text);
				}

				if (TotalCashPaid != 0)
				{
					AddFooterFeeRow("Cash", TotalCashPaid, lngStart, 1);
					intCT += 1;
				}
				if (TotalCheckPaid != 0)
				{
					// Check
					AddFooterFeeRow("Check", TotalCheckPaid, lngStart + (lngFieldHeight * intCT), 2);
					intCT += 1;
				}
				if (TotalCreditPaid != 0)
				{
					// Credit Card
					AddFooterFeeRow("Credit/Debit", TotalCreditPaid, lngStart + (lngFieldHeight * intCT), 3);
					intCT += 1;
				}
				if (change != 0)
				{
					// Change
					AddFooterFeeRow("Change", change, lngStart + (lngFieldHeight * intCT), 4);
					intCT += 1;
				}

				
				lblCheck1.Top = lngStart + (lngFieldHeight * intCT);
				intXLines = globalCashReceiptSettings.ReceiptExtraLines + AddCheckNumbers();
				if (globalCashReceiptSettings.NarrowReceipt)
				{
					ReportFooter.Height = lngStart + (lngFieldHeight * (intCT + intXLines)) + 240;
					AddFooterFeeRow(". ", 0, lngStart + (lngFieldHeight * (intCT + intXLines)), 5, true);
				}
				else
				{
					if (globalCashReceiptSettings.CompactedReceipt || (globalCashReceiptSettings.ReceiptLength == 22 && !globalCashReceiptSettings.NarrowReceipt))
					{
						ReportFooter.Height = lngStart + (lngFieldHeight * (intCT + intXLines));
						AddFooterFeeRow(". ", 0, lngStart + (lngFieldHeight * (intCT + intXLines)), 5, true);
					}
					else
					{
						ReportFooter.Height = lngStart + (lngFieldHeight * (intCT + intXLines + 2));
						AddFooterFeeRow(". ", 0, lngStart + (lngFieldHeight * (intCT + intXLines + 1)), 5, true);
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Footer Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private decimal TotalCashPaid
		{
			get { return payments.Where(p => p.PaymentType == PaymentMethod.Cash).Sum(p => p.Amount); }
		}

		private decimal TotalCheckPaid
		{
			get { return payments.Where(p => p.PaymentType == PaymentMethod.Check).Sum(p => p.Amount); }
		}

		private decimal TotalCreditPaid
		{
			get { return payments.Where(p => p.PaymentType == PaymentMethod.Credit).Sum(p => p.Amount); }
		}

		private Label GetCheckNumberLabel(int counter)
		{
			if (counter == 1)
			{
				lblCheck1.Visible = true;
				return lblCheck1;
			}
			else
			{
				var obNew = ReportFooter.Controls["lblCheck" + FCConvert.ToString(counter)] as GrapeCity.ActiveReports.SectionReportModel.Label;
				obNew.Visible = true;
				obNew.Top = lblCheck1.Top + ((counter - 1) * lblCheck1.Height);
				obNew.Left = lblCheck1.Left;
				obNew.Width = lblCheck1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				obNew.Font = lblCheck1.Font;
				// this sets the font to the same as the field that is already created
				obNew.MultiLine = false;
				obNew.WordWrap = false;
				return obNew;
			}
		}

		private int AddCheckNumbers()
		{
			try
			{
				Label obNew;
				int counter = 1;
				foreach (var payment in payments.Where(x => x.PaymentType == PaymentMethod.Check))
				{
					obNew = GetCheckNumberLabel(counter);
					obNew.Text = ((CheckReceiptPayment)payment).CheckNumber.ToString().PadRight(10) + " -" + payment.Amount.ToString("#,##0.00").PadLeft(13); 
					counter += 1;
				}

				foreach (var payment in payments.Where(x => x.PaymentType == PaymentMethod.Credit))
				{
					obNew = GetCheckNumberLabel(counter);
					obNew.Text = ((CreditCardReceiptPayment)payment).TypeDescription + " -" + payment.Amount.ToString("#,##0.00").PadLeft(13);
					counter += 1;
				}
				
				if (convenienceFee != 0)
				{
					obNew = GetCheckNumberLabel(counter);
					
					if (globalCashReceiptSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort)
					{
						obNew.Text = "Portal Charge  -" + convenienceFee.ToString("#,##0.00").PadLeft(13);
					}
					else
					{
						obNew.Text = "Convenience Fee  -" + convenienceFee.ToString("#,##0.00").PadLeft(13);
					}
				}

				return counter - 1;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Check Numbers", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return 0;
		}

		private void SetReceiptFontSize()
		{
			try
			{
				int lngFSize;
				var wideFontSize = settingService.GetSettingValue(SettingOwnerType.User, "CRWideFontSize");
				lngFSize = wideFontSize == null ? 0 : wideFontSize.SettingValue.ToIntegerValue();
				if (lngFSize <= 0)
				{
					return;
				}
				lblMuniName.Font = new Font(lblMuniName.Font.Name, lngFSize);
				lblHeader.Font = new Font(lblHeader.Font.Name, lngFSize);
				lblTopComment.Font = new Font(lblTopComment.Font.Name, lngFSize);
				lblReprint.Font = new Font(lblReprint.Font.Name, lngFSize);
				lblTeller.Font = new Font(lblTeller.Font.Name, lngFSize);
				lblDate.Font = new Font(lblDate.Font.Name, lngFSize);
				lblTime.Font = new Font(lblTime.Font.Name, lngFSize);
				lblReceiptNumber.Font = new Font(lblReceiptNumber.Font.Name, lngFSize);
				lblHeaderAmt.Font = new Font(lblHeaderAmt.Font.Name, lngFSize);
				lblHeaderType.Font = new Font(lblHeaderType.Font.Name, lngFSize);
				lblHeaderRef.Font = new Font(lblHeaderRef.Font.Name, lngFSize);
				lblTotal.Font = new Font(lblTotal.Font.Name, lngFSize);
				fldTotal.Font = new Font(fldTotal.Font.Name, lngFSize);
				lblPaidBy.Font = new Font(lblPaidBy.Font.Name, lngFSize);
				lblOtherAccounts.Font = new Font(lblOtherAccounts.Font.Name, lngFSize);
				lblBottomComment.Font = new Font(lblBottomComment.Font.Name, lngFSize);
				lblCheck1.Font = new Font(lblCheck1.Font.Name, lngFSize);
				lblMuniName.Height = 300 / 1440F;
				lblHeader.Height = 300 / 1440F;
				lblTopComment.Height = 300 / 1440F;
				lblReprint.Height = 300 / 1440F;
				lblTeller.Height = 300 / 1440F;
				lblDate.Height = 300 / 1440F;
				lblTime.Height = 300 / 1440F;
				lblReceiptNumber.Height = 300 / 1440F;
				lblHeaderAmt.Height = 300 / 1440F;
				lblHeaderType.Height = 300 / 1440F;
				lblHeaderRef.Height = 300 / 1440F;
				lblTotal.Height = 300 / 1440F;
				fldTotal.Height = 300 / 1440F;
				lblPaidBy.Height = 300 / 1440F;
				lblOtherAccounts.Height = 300 / 1440F;
				lblBottomComment.Height = 300 / 1440F;
			}
			catch (Exception ex)
			{

			}
		}
	}
}
