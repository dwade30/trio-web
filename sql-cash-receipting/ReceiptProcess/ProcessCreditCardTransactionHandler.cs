﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace TWCR0000.ReceiptProcess
{
	public class ProcessCreditCardTransactionHandler : CommandHandler<ProcessCreditCardTransaction, ProcessCreditCardTransactionResult>
	{
		private IModalView<IProcessCreditCardTransactionViewModel> processCreditCardTransactionView;
		public ProcessCreditCardTransactionHandler(IModalView<IProcessCreditCardTransactionViewModel> processCreditCardTransactionView)
		{
			this.processCreditCardTransactionView = processCreditCardTransactionView;
		}

		protected override ProcessCreditCardTransactionResult Handle(ProcessCreditCardTransaction command)
		{
			processCreditCardTransactionView.ShowModal();
			return processCreditCardTransactionView.ViewModel.Result;
		}
	}
}
