﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmTellerID.
	/// </summary>
	partial class frmGetTellerID : BaseForm
	{

		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtPassword = new fecherFoundation.FCTextBox();
            this.txtTellerID = new fecherFoundation.FCTextBox();
            this.cmdSave = new fecherFoundation.FCButton();
            this.lblPassword = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 147);
            this.BottomPanel.Size = new System.Drawing.Size(230, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblPassword);
            this.ClientArea.Controls.Add(this.txtPassword);
            this.ClientArea.Controls.Add(this.txtTellerID);
            this.ClientArea.Size = new System.Drawing.Size(250, 280);
            this.ClientArea.Controls.SetChildIndex(this.txtTellerID, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPassword, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPassword, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(250, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(94, 28);
            this.HeaderText.Text = "Teller ID";
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
            this.txtPassword.Location = new System.Drawing.Point(41, 107);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(170, 40);
            this.txtPassword.TabIndex = 27;
            this.txtPassword.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtPassword.Visible = false;
            // 
            // txtTellerID
            // 
            this.txtTellerID.BackColor = System.Drawing.SystemColors.Window;
            this.txtTellerID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTellerID.Location = new System.Drawing.Point(41, 28);
            this.txtTellerID.MaxLength = 3;
            this.txtTellerID.Name = "txtTellerID";
            this.txtTellerID.Size = new System.Drawing.Size(170, 40);
            this.txtTellerID.TabIndex = 26;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(75, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 1;
            this.cmdSave.Text = "OK";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(41, 82);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(79, 15);
            this.lblPassword.TabIndex = 28;
            this.lblPassword.Text = "PASSWORD";
            this.lblPassword.Visible = false;
            // 
            // frmGetTellerID
            // 
            this.ClientSize = new System.Drawing.Size(250, 340);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGetTellerID";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Teller ID";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        public FCTextBox txtPassword;
        public FCTextBox txtTellerID;
        private FCButton cmdSave;
        private FCLabel lblPassword;
    }
}
