﻿using System;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using Wisej.Web;

namespace TWCR0000.ReceiptProcess
{
	public partial class frmProcessCreditCardTransaction : BaseForm, IModalView<IProcessCreditCardTransactionViewModel>
	{
		public frmProcessCreditCardTransaction()
		{
			InitializeComponent();
		}

		public frmProcessCreditCardTransaction(IProcessCreditCardTransactionViewModel viewModel) : this()
		{
			ViewModel = viewModel;
		}

		public IProcessCreditCardTransactionViewModel ViewModel { get; set; }
		public void ShowModal()
		{
			this.Show(FormShowEnum.Modal);
		}
	}
}
