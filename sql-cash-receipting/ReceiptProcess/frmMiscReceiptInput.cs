﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Extensions;
using Wisej.Web;
using Region = SharedApplication.CentralData.Models.Region;

namespace TWCR0000.ReceiptProcess
{
    public partial class frmMiscReceiptInput : BaseForm, IView<IMiscTransactionViewModel>
    {
	    private IMiscTransactionViewModel viewModel { get; set; }
	    private bool cancelling = true;
		private bool boolAddingResCodes;
		private int lngQuanity = 1;
		clsGridAccount clsAcctBox = new clsGridAccount();

		public frmMiscReceiptInput()
        {
            InitializeComponent();
		}

		public frmMiscReceiptInput(IMiscTransactionViewModel viewModel) : this()
		{
			this.ViewModel = viewModel;
			viewModel.ReceiptTypeChanged += ViewModel_ReceiptTypeChanged;
		}

		private void frmMiscReceiptInput_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (cancelling)
			{
				ViewModel.Cancel();
			}
		}

		public IMiscTransactionViewModel ViewModel
		{
			get { return viewModel; }
			set
			{
				viewModel = value;
				SetUpView();
				viewModel.ReceiptTypeChanged += ViewModel_ReceiptTypeChanged;
			}
		}

		private void ViewModel_ReceiptTypeChanged(object sender, EventArgs e)
		{
			UpdateView();
			FillView();
		}

		private void FillView()
		{
			SetResCodeCombo(viewModel.TownKey);
			txtReference.Text = viewModel.Transaction.Reference ?? "";
			txtControl1.Text = viewModel.GetControlItemValue("Control1") ?? "";
			txtControl2.Text = viewModel.GetControlItemValue("Control2") ?? "";
			txtControl3.Text = viewModel.GetControlItemValue("Control3") ?? "";

			txtAcct1.TextMatrix(0, 0, viewModel.Transaction.GlAccount);
			txtQuantity.Text = viewModel.Transaction.Quantity.ToString();
			txtPercentageAmount.Text = viewModel.Transaction.PercentageAmount.ToString("g");
			txtComment.Text = viewModel.Transaction.Comment;
			chkAffectCash.Checked = viewModel.Transaction.AffectCashDrawer;
			txtReference.Text = viewModel.Transaction.Reference;
			txtName.Text = viewModel.Transaction.PaidBy;
			if (viewModel.IsExistingTransaction)
			{
				for (int line = 1; line <= 6; line++)
				{
					decimal amount = viewModel.GetAmountItemValue("Amount" + line);
					if (viewModel.LinePercentage(line))
					{
						vsFees.TextMatrix(line, 2, amount.ToString("g") + "%");
					}
					else
					{
						vsFees.TextMatrix(line, 2, amount.ToString("g"));
					}
				}
			}
		}

		private void SetResCodeCombo(int townCode)
		{
			for (int index = 0; index < cmbResCode.Items.Count; index++)
			{
				if (cmbResCode.Items[cmbResCode.SelectedIndex].ToString().Left(3).ToIntegerValue() == townCode)
				{
					cmbResCode.SelectedIndex = index;
				}
			}
		}

		private void UpdateView()
		{
			this.Text = viewModel.ReceiptTypeInfo().TypeTitle;

			txtAcct1.Visible = false;
			lblAccount.Visible = false;
			toolTip1.SetToolTip(txtAcct1, "");

			if (viewModel.ShowAccountEntry())
			{
				txtAcct1.Visible = true;
				lblAccount.Visible = true;
			}
					
			for (int line = 1; line <= 6; line++)
			{
				if (viewModel.ShowLine(line))
				{
					vsFees.TextMatrix(line, 1, viewModel.LineTitle(line));    
					if (viewModel.LinePercentage(line))
					{
						vsFees.TextMatrix(line, 2, Strings.Format(viewModel.LineDefaultAmount(line), "#0.00") + "%");
						vsFees.TextMatrix(line, 4, "%");
					}
					else
					{
						vsFees.TextMatrix(line, 2, Strings.Format(viewModel.LineDefaultAmount(line), "#,##0.00"));
						vsFees.TextMatrix(line, 4, "$");
					}
					vsFees.TextMatrix(line, 3, viewModel.LineYear(line));
					vsFees.RowHidden(line, false);
				}
				else
				{
					vsFees.TextMatrix(line, 1, "");
					vsFees.TextMatrix(line, 2, FCConvert.ToString(0));
					vsFees.TextMatrix(line, 3, FCConvert.ToString(false));
					vsFees.RowHidden(line, true);
				}
			}

			txtName.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);

			SetupReferenceControlFields();

			txtQuantity.Text = "1";

			lblPercentAmount.Visible = viewModel.ShowPercentage();
			txtPercentageAmount.Visible = viewModel.ShowPercentage();
			txtPercentageAmount.Text = "0.00";

		}

		private void SetupReferenceControlFields()
		{
			if (viewModel.ReceiptTypeInfo().BMV ?? false)
			{
				toolTip1.SetToolTip(lblControl1, "Use the format YYD#######");
				toolTip1.SetToolTip(lblControl2, "Use the format MMD#######");
				toolTip1.SetToolTip(txtControl1, "Use the format YYD#######");
				toolTip1.SetToolTip(txtControl2, "Use the format MMD#######");
			}
			else
			{
				toolTip1.SetToolTip(lblControl1, "");
				toolTip1.SetToolTip(lblControl2, "");
				toolTip1.SetToolTip(txtControl1, "");
				toolTip1.SetToolTip(txtControl2, "");
			}

			txtReference.Text = "";
			txtControl1.Text = "";
			txtControl2.Text = "";
			txtControl3.Text = "";

			if (viewModel.ReferenceVisible())
			{
				lblReference.Text = viewModel.ReceiptTypeInfo().Reference + ":";
				if (viewModel.ReceiptTypeInfo().ReferenceMandatory ?? false)
				{
					txtReference.Tag = "True";
					txtReference.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
				else
				{
					txtReference.Tag = "False";
					txtReference.BackColor = Color.White;
				}
			}
			else
			{
				lblReference.Visible = false;
				txtReference.Visible = false;
			}

			if (viewModel.ControlVisible(1))
			{
				lblControl1.Text = viewModel.ReceiptTypeInfo().Control1 + ":";
				if (viewModel.ReceiptTypeInfo().Control1Mandatory ?? false)
				{
					txtControl1.Tag = "True";
					txtControl1.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
				else
				{
					txtControl1.Tag = "False";
					txtControl1.BackColor = Color.White;
				}
			}
			else
			{
				lblControl1.Visible = false;
				txtControl1.Visible = false;
			}

			if (viewModel.ControlVisible(2))
			{
				lblControl2.Text = viewModel.ReceiptTypeInfo().Control2 + ":";
				if (viewModel.ReceiptTypeInfo().Control2Mandatory ?? false)
				{
					txtControl2.Tag = "True";
					txtControl2.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
				else
				{
					txtControl2.Tag = "False";
					txtControl2.BackColor = Color.White;
				}
			}
			else
			{
				lblControl2.Visible = false;
				txtControl2.Visible = false;
			}

			if (viewModel.ControlVisible(3))
			{
				lblControl3.Text = viewModel.ReceiptTypeInfo().Control3 + ":";
				if (viewModel.ReceiptTypeInfo().Control3Mandatory ?? false)
				{
					txtControl3.Tag = "True";
					txtControl3.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
				else
				{
					txtControl3.Tag = "False";
					txtControl3.BackColor = Color.White;
				}
			}
			else
			{
				lblControl3.Visible = false;
				txtControl3.Visible = false;
			}
		}

		private void SetUpView()
		{
			FormatGrid();
			FillResCodeCombo();
		}

		private void FormatGrid()
		{
			int Width = 0;
			vsFees.Cols = 5;
			vsFees.EditMaxLength = 12;
			Width = vsFees.WidthOriginal;
			vsFees.ColWidth(0, 0);
			vsFees.ColWidth(1, FCConvert.ToInt32(Width * 0.61));
			vsFees.ColWidth(2, FCConvert.ToInt32(Width * 0.37));
			vsFees.ColWidth(3, 0);
			vsFees.ColWidth(4, 0);
			vsFees.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFees.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsFees.TextMatrix(0, 0, "Fee");
			vsFees.TextMatrix(0, 1, "Title");
			vsFees.TextMatrix(0, 2, "Amount");
			vsFees.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
		}

		private void FillResCodeCombo()
		{
			try
			{
				boolAddingResCodes = true;
				cmbResCode.Clear();
				
				foreach (var region in viewModel.Regions())
				{
					cmbResCode.AddItem(((int)(region.TownNumber)).ToString("00") + " - " + region.TownName.Trim());
				}
				
				cmbResCode.Visible = cmbResCode.Items.Count > 1;
				if (cmbResCode.Visible)
				{
					cmbResCode.SelectedIndex = 0;
				}
					
				boolAddingResCodes = false;
			}
			catch (Exception ex)
			{
				boolAddingResCodes = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Town ID Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtComment_Enter(object sender, EventArgs e)
		{
			txtComment.SelectionStart = 0;
			txtComment.SelectionLength = txtComment.Text.Length;
		}

		private void txtPercentageAmount_Enter(object sender, EventArgs e)
		{
			txtPercentageAmount.SelectionStart = 0;
			txtPercentageAmount.SelectionLength = txtPercentageAmount.Text.Length;
		}

		private void txtPercentageAmount_KeyPress(object sender, KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert))
			{
			}
			else if ((KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Delete))
			{
				if (Strings.InStr(1, txtPercentageAmount.Text, ".", CompareConstants.vbBinaryCompare) != 0)
				{
					// block the second decimal
					KeyAscii = (Keys)0;
				}
				else
				{
					// allow this
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPercentageAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtPercentageAmount.Text = Strings.Format(txtPercentageAmount.Text, "#,##0.00");
		}

		private void txtMultiple_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int rowIndex;
			decimal lineAmount = 0;

			if (txtQuantity.Text.ToIntegerValue() < 1)
				txtQuantity.Text = "1";

			if (lngQuanity > 1)
			{
				for (rowIndex = 1; rowIndex <= 6; rowIndex++)
				{
					if (!vsFees.TextMatrix(rowIndex, 2).Contains("%"))
					{
						if (vsFees.TextMatrix(rowIndex, 2).ToDecimalValue() != 0)
						{
							lineAmount = vsFees.TextMatrix(rowIndex, 2).ToDecimalValue();
						}
						else
						{
							lineAmount = 0;
						}
						if (lineAmount != 0)
						{
							lineAmount /= lngQuanity;
						}
						vsFees.TextMatrix(rowIndex, 2, Strings.Format(Math.Round(lineAmount, 2, MidpointRounding.AwayFromZero), "#,##0.00"));
					}
				}
			}

			lngQuanity = txtQuantity.Text.ToIntegerValue();
			
			if (lngQuanity > 1)
			{
				for (rowIndex = 1; rowIndex <= 6; rowIndex++)
				{
					if (!vsFees.TextMatrix(rowIndex, 2).Contains("%"))
					{
						if (vsFees.TextMatrix(rowIndex, 2).ToDecimalValue() != 0)
						{
							lineAmount = vsFees.TextMatrix(rowIndex, 2).ToDecimalValue();
						}
						else
						{
							lineAmount = 0;
						}
						if (lineAmount != 0)
						{
							lineAmount *= lngQuanity;
						}
						vsFees.TextMatrix(rowIndex, 2, Strings.Format(Math.Round(lineAmount, 2, MidpointRounding.AwayFromZero), "#,##0.00"));
					}
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			FillTransactionInformation();
			if (ValidateTransaction())
			{
				viewModel.CompleteTransaction();
				cancelling = false;
				this.Close();
			}
		}

		private void FillTransactionInformation()
		{
			viewModel.Transaction.TownCode = viewModel.TownKey;
			viewModel.Transaction.TransactionTypeCode = viewModel.ReceiptTypeInfo().TypeCode ?? 0;
			viewModel.Transaction.TransactionTypeDescription = viewModel.ReceiptTypeInfo().TypeTitle ?? "";
            viewModel.Transaction.DefaultCashAccount = viewModel.ReceiptTypeInfo().DefaultAccount ?? "";
			viewModel.Transaction.GlAccount = txtAcct1.TextMatrix(0, 0);
			viewModel.Transaction.Quantity = txtQuantity.Text.ToIntegerValue();
			viewModel.Transaction.PercentageAmount = txtPercentageAmount.Text.ToDecimalValue();
			viewModel.Transaction.Comment = txtComment.Text.Trim();
			viewModel.Transaction.AffectCashDrawer = chkAffectCash.Checked;
			viewModel.Transaction.AffectCash = true;
			viewModel.Transaction.Reference = txtReference.Text.Trim();
			viewModel.Transaction.PaidBy = txtName.Text.Trim();
			if (viewModel.ControlVisible(1))
			{
				viewModel.UpdateControlItem("Control1",  viewModel.ReceiptTypeInfo().Control1, txtControl1.Text.Trim());
			}
			if (viewModel.ControlVisible(2))
			{
				viewModel.UpdateControlItem("Control2", viewModel.ReceiptTypeInfo().Control2, txtControl2.Text.Trim());
			}
			if (viewModel.ControlVisible(3))
			{
				viewModel.UpdateControlItem("Control3", viewModel.ReceiptTypeInfo().Control3, txtControl3.Text.Trim());
			}

			for (int line = 1; line <= 6; line++)
			{
				if (viewModel.ShowLine(line))
				{
					decimal amount = vsFees.TextMatrix(line, 2).ToDecimalValue();
					if (viewModel.LinePercentage(line))
					{
						viewModel.UpdateAmountItem("Amount" + line, vsFees.TextMatrix(line, 1), 0, amount);
					}
					else
					{
						viewModel.UpdateAmountItem("Amount" + line, vsFees.TextMatrix(line, 1), amount);
					}
				}
			}
		}

		private bool ValidateTransaction()
		{
			try
			{

				if (viewModel.IsRegionalTown())
				{
					if (cmbResCode.Items[cmbResCode.SelectedIndex].ToString().Left(3).ToIntegerValue() == 0)
					{
						MessageBox.Show("This receipt type needs to have a town code associated.  Please enter a valid town code.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						if (cmbResCode.Visible)
						{
							cmbResCode.Focus();
						}
						return false;
					}
				}

				if (txtName.Text.Trim() == "")
				{
					MessageBox.Show("The name field is required to be filled in before processing.", "Data Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtName.Focus();
					return false;
				}

				if (viewModel.ReceiptTypeInfo().ReferenceMandatory ?? false)
				{
					if (txtReference.Text.Trim() == "")
					{
						MessageBox.Show("The " + lblReference.Text.Left(lblReference.Text.Length - 1) + " field is required before processing.", "Data Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtReference.Focus();
						return false;
					}
				}

				if (viewModel.ReceiptTypeInfo().Control1Mandatory ?? false)
				{
					if (txtControl1.Text.Trim() == "")
					{
						MessageBox.Show("The " + lblControl1.Text.Left(lblControl1.Text.Length - 1) + " field is required before processing.", "Data Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtControl1.Focus();
						return false;
					}
				}
				if (viewModel.ReceiptTypeInfo().Control2Mandatory ?? false)
				{
					if (txtControl2.Text.Trim() == "")
					{
						MessageBox.Show("The " + lblControl2.Text.Left(lblControl2.Text.Length - 1) + " field is required before processing.", "Data Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtControl2.Focus();
						return false;
					}
				}
				if (viewModel.ReceiptTypeInfo().Control3Mandatory ?? false)
				{
					if (txtControl3.Text.Trim() == "")
					{
						MessageBox.Show("The " + lblControl3.Text.Left(lblControl3.Text.Length - 1) + " field is required before processing.", "Data Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtControl3.Focus();
						return false;
					}
				}

				if (viewModel.IsZeroAmountReceipt())
				{
					DialogResult result = MessageBox.Show("This transaction will result in a zero amount transaction.  Do you wish to continue?", "Zero Amount Receipt", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
					if (result == DialogResult.No)
					{
						return false;
					}
				}

				
				if (!viewModel.IsValidPercentageTotal())
				{
					MessageBox.Show("Make sure that your percentages add up to 100%.", "Invalid Percentage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (viewModel.ShowAccountEntry())
				{
					if (txtReference.Visible)
					{
						txtReference.Focus();
					}

					if (!viewModel.IsValidAccount(txtAcct1.TextMatrix(0, 0)))
					{
						MessageBox.Show("Please enter a valid account number in the box provided.",
							"Missing Account Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtAcct1.Focus();
						return false;
					}
				}

				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Receipt", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return false;
		}

		private void frmMiscReceiptInput_Load(object sender, EventArgs e)
		{
			clsAcctBox.GRID7Light = txtAcct1;
			clsAcctBox.DefaultAccountType = "R";
		}

		private void vsFees_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int row = vsFees.GetFlexRowIndex(e.RowIndex);
			if (vsFees.TextMatrix(row, 4) == "%")
			{
				// this checks the hidden col to check the type
				vsFees.EditText = Strings.Format(Conversion.Val(vsFees.EditText.Replace("%", "")), "#0.00") + "%";
			}
			else
			{
				//FC:FINAL:DSE #i511 Format does not take count of the last "-" character
				if (vsFees.EditText.EndsWith("-"))
				{
					vsFees.EditText = "-" + vsFees.EditText.Substring(0, vsFees.EditText.Length - 1);
				}

                decimal test;

                if (decimal.TryParse(vsFees.EditText, out test))
                {
                    vsFees.EditText = test.ToString("#,##0.00");
                }
                else
                {
                    vsFees.EditText = "0.00";
                }

                e.FormattedValue = vsFees.EditText;
                vsFees.Refresh();
			}
        }

		private void vsFees_CurrentCellChanged(object sender, EventArgs e)
		{
			try
			{
				if (vsFees.RowHidden(vsFees.Row))
				{
					if (vsFees.Row < vsFees.Rows - 1)
					{
						vsFees.Row += 1;
					}
				}
				else
				{
					switch (vsFees.Col)
					{
						case 1:
							{
								vsFees.Col = 2;
								return;
							}
						case 2:
							{
								vsFees.EditMaxLength = 13;
								vsFees.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsFees.EditCell();
								break;
							}
						default:
							{
								vsFees.Editable = FCGrid.EditableSettings.flexEDNone;
								break;
							}
					}

					int lngBotRow;
					for (lngBotRow = vsFees.Rows - 1; lngBotRow >= 1; lngBotRow--)
					{
						if (vsFees.RowHeight(lngBotRow) != 0)
							break;
					}
					if (vsFees.Col == 2 && vsFees.Row == lngBotRow)
					{
						vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
					}
				}
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fee Grid Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
