﻿using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace TWCR0000.ReceiptProcess
{
    public class GetTellerIdHandler : CommandHandler<GetTellerId,(bool Success, string TellerId)>
    {
        private IModalView<IGetTellerIdViewModel> getTellerView;
        public GetTellerIdHandler(IModalView<IGetTellerIdViewModel> getTellerView)
        {
            this.getTellerView = getTellerView;
        }
        protected override (bool Success, string TellerId) Handle(GetTellerId command)
        {
            getTellerView.ShowModal();
            if (getTellerView.ViewModel.ChosenTeller != "")
            {
                return (Success: true, TellerId: getTellerView.ViewModel.ChosenTeller);
            }

            return (Success: false, TellerId: "");
        }
    }
}