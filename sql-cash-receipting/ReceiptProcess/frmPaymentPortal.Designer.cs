﻿namespace TWCR0000.ReceiptProcess
{
    partial class frmPaymentPortal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.wbPayPort = new Wisej.Web.WebBrowser();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 740);
            this.BottomPanel.Size = new System.Drawing.Size(824, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.ClientArea.Controls.Add(this.wbPayPort);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Location = new System.Drawing.Point(0, 13);
            this.ClientArea.Size = new System.Drawing.Size(844, 743);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.wbPayPort, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(844, 10);
            this.TopPanel.Visible = false;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // wbPayPort
            // 
            this.wbPayPort.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.wbPayPort.Location = new System.Drawing.Point(0, 4);
            this.wbPayPort.Name = "wbPayPort";
            this.wbPayPort.Size = new System.Drawing.Size(825, 736);
            this.wbPayPort.TabIndex = 1003;
            // 
            // frmPaymentPortal
            // 
            this.ClientSize = new System.Drawing.Size(844, 756);
            this.Name = "frmPaymentPortal";
            this.Text = "Enter Payment";
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.WebBrowser wbPayPort;
        private Wisej.Web.JavaScript javaScript1;
    }
}