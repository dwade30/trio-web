﻿namespace TWCR0000.ReceiptProcess
{
	partial class frmVoidReceipt
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Wisej Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtPaidBy = new fecherFoundation.FCTextBox();
            this.lblPaidBy = new fecherFoundation.FCLabel();
            this.SummaryGrid = new fecherFoundation.FCGrid();
            this.gridPayments = new fecherFoundation.FCGrid();
            this.cmdVoid = new fecherFoundation.FCButton();
            this.btnCancel = new fecherFoundation.FCButton();
            this.txtCustNum_PaidBy = new fecherFoundation.FCTextBox();
            this.lblCustNum_PaidBy = new fecherFoundation.FCLabel();
            this.panel2 = new Wisej.Web.Panel();
            this.lblTransactionTotal = new fecherFoundation.FCLabel();
            this.lblConvenienceFeeTotal = new fecherFoundation.FCLabel();
            this.lblCashTotal = new fecherFoundation.FCLabel();
            this.lblCheckTotal = new fecherFoundation.FCLabel();
            this.lblCreditTotal = new fecherFoundation.FCLabel();
            this.lblReceiptTotal = new fecherFoundation.FCLabel();
            this.lblTransactions = new fecherFoundation.FCLabel();
            this.lblConvenienceFee = new fecherFoundation.FCLabel();
            this.lblCashOutCashPaid = new fecherFoundation.FCLabel();
            this.lblCashOutCheckPaid = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.lblCashOutCreditPaid = new fecherFoundation.FCLabel();
            this.lblCashOutTotal = new fecherFoundation.FCLabel();
            this.lblCashOutChange = new fecherFoundation.FCLabel();
            this.lblCashOutTotalDue = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdVoid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnCancel);
            this.BottomPanel.Controls.Add(this.cmdVoid);
            this.BottomPanel.Location = new System.Drawing.Point(0, 529);
            this.BottomPanel.Size = new System.Drawing.Size(897, 109);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtCustNum_PaidBy);
            this.ClientArea.Controls.Add(this.lblCustNum_PaidBy);
            this.ClientArea.Controls.Add(this.txtPaidBy);
            this.ClientArea.Controls.Add(this.lblPaidBy);
            this.ClientArea.Controls.Add(this.SummaryGrid);
            this.ClientArea.Controls.Add(this.panel2);
            this.ClientArea.Size = new System.Drawing.Size(917, 652);
            this.ClientArea.Controls.SetChildIndex(this.panel2, 0);
            this.ClientArea.Controls.SetChildIndex(this.SummaryGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCustNum_PaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCustNum_PaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(917, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(140, 28);
            this.HeaderText.Text = "Void Receipt";
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaidBy.Location = new System.Drawing.Point(117, 241);
            this.txtPaidBy.LockedOriginal = true;
            this.txtPaidBy.MaxLength = 50;
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.ReadOnly = true;
            this.txtPaidBy.Size = new System.Drawing.Size(337, 40);
            this.txtPaidBy.TabIndex = 145;
            // 
            // lblPaidBy
            // 
            this.lblPaidBy.Location = new System.Drawing.Point(30, 255);
            this.lblPaidBy.Name = "lblPaidBy";
            this.lblPaidBy.Size = new System.Drawing.Size(52, 16);
            this.lblPaidBy.TabIndex = 147;
            this.lblPaidBy.Text = "PAID BY";
            // 
            // SummaryGrid
            // 
            this.SummaryGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.SummaryGrid.Cols = 3;
            this.SummaryGrid.FixedCols = 0;
            this.SummaryGrid.Location = new System.Drawing.Point(30, 23);
            this.SummaryGrid.Name = "SummaryGrid";
            this.SummaryGrid.RowHeadersVisible = false;
            this.SummaryGrid.Rows = 1;
            this.SummaryGrid.ShowFocusCell = false;
            this.SummaryGrid.Size = new System.Drawing.Size(852, 190);
            this.SummaryGrid.TabIndex = 146;
            // 
            // gridPayments
            // 
            this.gridPayments.Cols = 3;
            this.gridPayments.FixedCols = 0;
            this.gridPayments.Location = new System.Drawing.Point(347, 17);
            this.gridPayments.Name = "gridPayments";
            this.gridPayments.RowHeadersVisible = false;
            this.gridPayments.Rows = 1;
            this.gridPayments.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.gridPayments.ShowFocusCell = false;
            this.gridPayments.Size = new System.Drawing.Size(368, 190);
            this.gridPayments.TabIndex = 148;
            // 
            // cmdVoid
            // 
            this.cmdVoid.AppearanceKey = "acceptButton";
            this.cmdVoid.Location = new System.Drawing.Point(239, 30);
            this.cmdVoid.Name = "cmdVoid";
            this.cmdVoid.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdVoid.Size = new System.Drawing.Size(188, 48);
            this.cmdVoid.TabIndex = 102;
            this.cmdVoid.Text = "Void Receipt";
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "actionButton";
            this.btnCancel.Location = new System.Drawing.Point(490, 30);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(188, 48);
            this.btnCancel.TabIndex = 103;
            this.btnCancel.Text = "Cancel";
            // 
            // txtCustNum_PaidBy
            // 
            this.txtCustNum_PaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtCustNum_PaidBy.Location = new System.Drawing.Point(576, 241);
            this.txtCustNum_PaidBy.LockedOriginal = true;
            this.txtCustNum_PaidBy.Name = "txtCustNum_PaidBy";
            this.txtCustNum_PaidBy.ReadOnly = true;
            this.txtCustNum_PaidBy.Size = new System.Drawing.Size(85, 40);
            this.txtCustNum_PaidBy.TabIndex = 151;
            // 
            // lblCustNum_PaidBy
            // 
            this.lblCustNum_PaidBy.Location = new System.Drawing.Point(516, 255);
            this.lblCustNum_PaidBy.Name = "lblCustNum_PaidBy";
            this.lblCustNum_PaidBy.Size = new System.Drawing.Size(25, 16);
            this.lblCustNum_PaidBy.TabIndex = 152;
            this.lblCustNum_PaidBy.Text = "ID#";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblTransactionTotal);
            this.panel2.Controls.Add(this.lblConvenienceFeeTotal);
            this.panel2.Controls.Add(this.lblCashTotal);
            this.panel2.Controls.Add(this.lblCheckTotal);
            this.panel2.Controls.Add(this.lblCreditTotal);
            this.panel2.Controls.Add(this.gridPayments);
            this.panel2.Controls.Add(this.lblReceiptTotal);
            this.panel2.Controls.Add(this.lblTransactions);
            this.panel2.Controls.Add(this.lblConvenienceFee);
            this.panel2.Controls.Add(this.lblCashOutCashPaid);
            this.panel2.Controls.Add(this.lblCashOutCheckPaid);
            this.panel2.Controls.Add(this.fcLabel1);
            this.panel2.Controls.Add(this.fcLabel2);
            this.panel2.Controls.Add(this.lblCashOutCreditPaid);
            this.panel2.Controls.Add(this.lblCashOutTotal);
            this.panel2.Controls.Add(this.lblCashOutChange);
            this.panel2.Controls.Add(this.lblCashOutTotalDue);
            this.panel2.Location = new System.Drawing.Point(49, 305);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(739, 224);
            this.panel2.TabIndex = 153;
            // 
            // lblTransactionTotal
            // 
            this.lblTransactionTotal.Location = new System.Drawing.Point(168, 17);
            this.lblTransactionTotal.Name = "lblTransactionTotal";
            this.lblTransactionTotal.TabIndex = 142;
            this.lblTransactionTotal.Text = "0.00";
            this.lblTransactionTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblConvenienceFeeTotal
            // 
            this.lblConvenienceFeeTotal.Location = new System.Drawing.Point(168, 43);
            this.lblConvenienceFeeTotal.Name = "lblConvenienceFeeTotal";
            this.lblConvenienceFeeTotal.TabIndex = 141;
            this.lblConvenienceFeeTotal.Text = "0.00";
            this.lblConvenienceFeeTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCashTotal
            // 
            this.lblCashTotal.Location = new System.Drawing.Point(168, 95);
            this.lblCashTotal.Name = "lblCashTotal";
            this.lblCashTotal.TabIndex = 140;
            this.lblCashTotal.Text = "0.00";
            this.lblCashTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCheckTotal
            // 
            this.lblCheckTotal.Location = new System.Drawing.Point(168, 121);
            this.lblCheckTotal.Name = "lblCheckTotal";
            this.lblCheckTotal.TabIndex = 139;
            this.lblCheckTotal.Text = "0.00";
            this.lblCheckTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Location = new System.Drawing.Point(168, 147);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.TabIndex = 138;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblReceiptTotal
            // 
            this.lblReceiptTotal.Location = new System.Drawing.Point(168, 69);
            this.lblReceiptTotal.Name = "lblReceiptTotal";
            this.lblReceiptTotal.TabIndex = 137;
            this.lblReceiptTotal.Text = "0.00";
            this.lblReceiptTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTransactions
            // 
            this.lblTransactions.Location = new System.Drawing.Point(27, 17);
            this.lblTransactions.Name = "lblTransactions";
            this.lblTransactions.Size = new System.Drawing.Size(117, 16);
            this.lblTransactions.TabIndex = 136;
            this.lblTransactions.Text = "TRANSACTIONS";
            // 
            // lblConvenienceFee
            // 
            this.lblConvenienceFee.Location = new System.Drawing.Point(27, 43);
            this.lblConvenienceFee.Name = "lblConvenienceFee";
            this.lblConvenienceFee.Size = new System.Drawing.Size(157, 16);
            this.lblConvenienceFee.TabIndex = 135;
            this.lblConvenienceFee.Text = "CONVENIENCE FEE";
            // 
            // lblCashOutCashPaid
            // 
            this.lblCashOutCashPaid.Location = new System.Drawing.Point(27, 95);
            this.lblCashOutCashPaid.Name = "lblCashOutCashPaid";
            this.lblCashOutCashPaid.Size = new System.Drawing.Size(133, 16);
            this.lblCashOutCashPaid.TabIndex = 134;
            this.lblCashOutCashPaid.Text = "TOTAL CASH PAID";
            // 
            // lblCashOutCheckPaid
            // 
            this.lblCashOutCheckPaid.Location = new System.Drawing.Point(27, 121);
            this.lblCashOutCheckPaid.Name = "lblCashOutCheckPaid";
            this.lblCashOutCheckPaid.Size = new System.Drawing.Size(133, 16);
            this.lblCashOutCheckPaid.TabIndex = 133;
            this.lblCashOutCheckPaid.Text = "TOTAL CHECK PAID";
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(27, 360);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(57, 16);
            this.fcLabel1.TabIndex = 132;
            this.fcLabel1.Text = "CHANGE";
            this.fcLabel1.Visible = false;
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(27, 310);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(72, 16);
            this.fcLabel2.TabIndex = 131;
            this.fcLabel2.Text = "TOTAL DUE";
            // 
            // lblCashOutCreditPaid
            // 
            this.lblCashOutCreditPaid.Location = new System.Drawing.Point(27, 147);
            this.lblCashOutCreditPaid.Name = "lblCashOutCreditPaid";
            this.lblCashOutCreditPaid.Size = new System.Drawing.Size(140, 16);
            this.lblCashOutCreditPaid.TabIndex = 130;
            this.lblCashOutCreditPaid.Text = "TOTAL CREDIT PAID";
            // 
            // lblCashOutTotal
            // 
            this.lblCashOutTotal.Location = new System.Drawing.Point(27, 69);
            this.lblCashOutTotal.Name = "lblCashOutTotal";
            this.lblCashOutTotal.Size = new System.Drawing.Size(92, 16);
            this.lblCashOutTotal.TabIndex = 129;
            this.lblCashOutTotal.Text = "RECEIPT TOTAL";
            // 
            // lblCashOutChange
            // 
            this.lblCashOutChange.Location = new System.Drawing.Point(27, 364);
            this.lblCashOutChange.Name = "lblCashOutChange";
            this.lblCashOutChange.Size = new System.Drawing.Size(57, 16);
            this.lblCashOutChange.TabIndex = 128;
            this.lblCashOutChange.Text = "CHANGE";
            this.lblCashOutChange.Visible = false;
            // 
            // lblCashOutTotalDue
            // 
            this.lblCashOutTotalDue.Location = new System.Drawing.Point(27, 314);
            this.lblCashOutTotalDue.Name = "lblCashOutTotalDue";
            this.lblCashOutTotalDue.Size = new System.Drawing.Size(72, 16);
            this.lblCashOutTotalDue.TabIndex = 127;
            this.lblCashOutTotalDue.Text = "TOTAL DUE";
            // 
            // frmVoidReceipt
            // 
            this.ClientSize = new System.Drawing.Size(917, 712);
            this.Name = "frmVoidReceipt";
            this.Text = "Void Receipt";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdVoid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		public fecherFoundation.FCTextBox txtPaidBy;
		public fecherFoundation.FCLabel lblPaidBy;
		public fecherFoundation.FCGrid SummaryGrid;
		public fecherFoundation.FCGrid gridPayments;
		public fecherFoundation.FCButton btnCancel;
		public fecherFoundation.FCButton cmdVoid;
		public fecherFoundation.FCTextBox txtCustNum_PaidBy;
		public fecherFoundation.FCLabel lblCustNum_PaidBy;
		private Wisej.Web.Panel panel2;
		public fecherFoundation.FCLabel lblTransactionTotal;
		public fecherFoundation.FCLabel lblConvenienceFeeTotal;
		public fecherFoundation.FCLabel lblCashTotal;
		public fecherFoundation.FCLabel lblCheckTotal;
		public fecherFoundation.FCLabel lblCreditTotal;
		public fecherFoundation.FCLabel lblReceiptTotal;
		public fecherFoundation.FCLabel lblTransactions;
		public fecherFoundation.FCLabel lblConvenienceFee;
		public fecherFoundation.FCLabel lblCashOutCashPaid;
		public fecherFoundation.FCLabel lblCashOutCheckPaid;
		public fecherFoundation.FCLabel fcLabel1;
		public fecherFoundation.FCLabel fcLabel2;
		public fecherFoundation.FCLabel lblCashOutCreditPaid;
		public fecherFoundation.FCLabel lblCashOutTotal;
		public fecherFoundation.FCLabel lblCashOutChange;
		public fecherFoundation.FCLabel lblCashOutTotalDue;
	}
}
