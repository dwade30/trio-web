﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using GrapeCity.ActiveReports;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using TWCR0000.ReceiptProcess;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmNewReprint.
	/// </summary>
	public partial class frmNewReprint : BaseForm, IView<IReprintReceiptViewModel>
	{
		public frmNewReprint() 
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            this.SummaryGrid.CellMouseDown += SummaryGrid_CellMouseDown;
            this.Load += frmNewReprint_Load;
            this.cmdPrintReceipt.Click += CmdPrintReceipt_Click;
		}

        private void CmdPrintReceipt_Click(object sender, EventArgs e)
        {
            PrintReceipt();
        }

        private void PrintReceipt()
        {
            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
            {
                return;
            }
           
            var printResponse = ViewModel.PrintReceipt();
            if (!printResponse.Success)
            {
                FCMessageBox.Show(printResponse.Message, MsgBoxStyle.Critical | MsgBoxStyle.OkOnly,
                    printResponse.Title);
            }
        }
        public frmNewReprint(IReprintReceiptViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }


		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void frmNewReprint_Load(object sender, System.EventArgs e)
		{
			SetupGridPayments();
            SetupSummaryGrid();
            ShowPayerInfo();
            RefreshTransactions();
            lblSummaryTitle.Text = "Summary for Receipt #" + ViewModel.ReceiptNumber;
            // ShowPayments();
        }


        private void ShowPayerInfo()
        {
            txtPaidBy.Text = ViewModel.PaidBy;
           
        }
        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{

		}

		private void frmNewReprint_Resize(object sender, System.EventArgs e)
		{
            ResizePaymentsGrid();
            ResizeSummaryGrid();
        }

		private void mnuProcessPrint_Click(object sender, System.EventArgs e)
		{
			if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
			{
				return;
			}

			if (Conversion.Val(cmdPrintReceipt.Tag) == 0)
			{
				// show receipt
				//cmdPrintReceipt_Click();
			}
			else
			{
				// print receipt
				//PrintReceipt();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSplit_Click(object sender, System.EventArgs e)
		{

		}

        private void SummaryGrid_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {

            var row = SummaryGrid.MouseRow;
            if (e.Button == MouseButtons.Right && row > 0)
            {
                // right click
                ViewModel.ShowPaymentDetail((Guid)SummaryGrid.RowData(row));
            }
        }

        private void SetupSummaryGrid()
        {
            SummaryGrid.Cols = 9;
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.TransactionType, "Type");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.TypeDescription, "Type Description");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Account, "Account");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Name, "Name");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Info, "Info");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Amount, "Amount");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Split, "Split");
            SummaryGrid.ColHidden((int)SummaryColumns.Id, true);
            SummaryGrid.ColComboList((int)SummaryColumns.Split, "#1;1|#2;2|#3;3|#4;4|#5;5");
        }

        private void ResizeSummaryGrid()
        {
            SummaryGrid.Cols = 9;
            var gridWidth = SummaryGrid.WidthOriginal;
            SummaryGrid.ColWidth((int)SummaryColumns.TransactionType, Convert.ToInt32(gridWidth * 0.06));
            SummaryGrid.ColWidth((int)SummaryColumns.TypeDescription, Convert.ToInt32(gridWidth * 0.2));
            SummaryGrid.ColWidth((int)SummaryColumns.Account, Convert.ToInt32(gridWidth * 0.09));
            SummaryGrid.ColWidth((int)SummaryColumns.Name, Convert.ToInt32(gridWidth * .2));
            SummaryGrid.ColWidth((int)SummaryColumns.Info, Convert.ToInt32(gridWidth * .17));
            SummaryGrid.ColWidth((int)SummaryColumns.Amount, Convert.ToInt32(gridWidth * 0.11));
            SummaryGrid.Columns[(int)SummaryColumns.Split].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void ResizePaymentsGrid()
        {
            var gridWidth = gridPayments.WidthOriginal;
            gridPayments.ColWidth((int)PaymentCols.PaymentType, Convert.ToInt32(gridWidth * .2));
            gridPayments.ColWidth((int)PaymentCols.Amount, Convert.ToInt32(gridWidth * .35));
            gridPayments.Columns[(int)PaymentCols.Reference].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void SetupGridPayments()
        {
            gridPayments.Rows = 1;
            gridPayments.Cols = 4;
            gridPayments.ColHidden((int)PaymentCols.Id, true);
            gridPayments.TextMatrix(0, (int)PaymentCols.PaymentType, "Type");
            gridPayments.TextMatrix(0, (int)PaymentCols.Amount, "Amount");
            gridPayments.TextMatrix(0, (int)PaymentCols.Reference, "Reference");
        }

        private void RefreshTransactions()
        {
            FillSummaryGrid();
            ShowPayments();
            //SetPrintReceiptCopiesOptions();
        }

        private void FillSummaryGrid()
        {
            SummaryGrid.Rows = 1;
            var transactions = ViewModel.TransactionGroup.GetTransactions();

            var printReceipt = false;
            var copies = 0;

            foreach (var transaction in transactions)
            {
                AddTransactionToSummaryGrid(transaction);
            }
        }

        private void AddTransactionToSummaryGrid(TransactionBase transaction)
        {
            SummaryGrid.Rows += 1;
            var row = SummaryGrid.Rows - 1;
            var summaryItem = transaction.ToReceiptSummaryItem();
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Id, summaryItem.TransactionId);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.TransactionType, summaryItem.TransactionType);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Amount, summaryItem.Amount.FormatAsMoney());
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Name, summaryItem.Name);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Info, summaryItem.Info);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Split, summaryItem.Split);
            SummaryGrid.RowData(row, summaryItem.TransactionId);
            if (summaryItem.Account > 0)
            {
                SummaryGrid.TextMatrix(row, (int)SummaryColumns.Account, summaryItem.Account.ToString());
            }
            else
            {
                SummaryGrid.TextMatrix(row, (int)SummaryColumns.Account, "");
            }
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.TypeDescription, summaryItem.TypeDescription);
        }
        private void ShowPayments()
        {
            lblTransactionTotal.Text = ViewModel.TotalAmount.FormatAsMoney();
            lblConvenienceFeeTotal.Text = ViewModel.ConvenienceFee.FormatAsMoney();
            lblReceiptTotal.Text = ViewModel.TotalReceiptAmount.FormatAsMoney();
            lblCashTotal.Text = ViewModel.TotalCashPaid.FormatAsMoney();
            lblCheckTotal.Text = ViewModel.TotalCheckPaid.FormatAsMoney();
            lblCreditTotal.Text = ViewModel.TotalCreditPaid.FormatAsMoney();
            lblChangeDue.Text = ViewModel.ChangeDue.FormatAsMoney();
            gridPayments.Rows = 1;
            var payments = ViewModel.Payments;
            foreach (var payment in payments)
            {
                AddPaymentToGrid(payment);
            }
        }
        private void AddPaymentToGrid(ReceiptPayment payment)
        {
            gridPayments.Rows += 1;
            var row = gridPayments.Rows - 1;

            gridPayments.TextMatrix(row, (int)PaymentCols.Id, payment.Id);
            gridPayments.TextMatrix(row, (int)PaymentCols.Amount, payment.Amount.FormatAsMoney());
            gridPayments.TextMatrix(row, (int)PaymentCols.Reference, payment.Reference);
            switch (payment.PaymentType)
            {
                case PaymentMethod.Cash:
                    gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Cash");
                    break;
                case PaymentMethod.Check:
                    gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Check");
                    break;
                case PaymentMethod.Credit:
                    gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Credit Card");
                    break;
            }

            gridPayments.RowData(row, payment);
        }
        private enum SummaryColumns
        {
            Id = 0,
            TransactionType = 1,
            TypeDescription = 2,
            Account = 3,
            Name = 4,
            Info = 5,
            Amount = 6,
            Split = 7
        }
        private enum PaymentCols
        {
            Id = 0,
            PaymentType = 1,
            Amount = 2,
            Reference = 3
        }
        public IReprintReceiptViewModel ViewModel { get; set; }
    }
}
