﻿using Global;

namespace TWCR0000.ReceiptProcess
{
	partial class frmGetMultiTownId 
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Wisej Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmbTowns = new Wisej.Web.ComboBox();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 122);
			this.BottomPanel.Size = new System.Drawing.Size(272, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbTowns);
			this.ClientArea.Size = new System.Drawing.Size(272, 62);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(272, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(147, 30);
			this.HeaderText.Text = "Select Town";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(86, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "OK";
			// 
			// cmbTowns
			// 
			this.cmbTowns.AutoSize = false;
			this.cmbTowns.DisplayMember = "Description";
			this.cmbTowns.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTowns.Location = new System.Drawing.Point(30, 14);
			this.cmbTowns.Name = "cmbTowns";
			this.cmbTowns.Size = new System.Drawing.Size(207, 33);
			this.cmbTowns.TabIndex = 3;
			this.cmbTowns.ValueMember = "ID";
			// 
			// frmGetMultiTownId
			// 
			this.ClientSize = new System.Drawing.Size(272, 230);
			this.Name = "frmGetMultiTownId";
			this.Text = "frmGetMultiTownId";
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private fecherFoundation.FCButton cmdSave;
		private Wisej.Web.ComboBox cmbTowns;
	}
}