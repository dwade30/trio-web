﻿namespace TWCR0000.ReceiptProcess
{
    partial class frmReceiptTransactionSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiptTransactionSummary));
            this.SummaryGrid = new fecherFoundation.FCGrid();
            this.cmdContinue = new fecherFoundation.FCButton();
            this.btnAddTransaction = new fecherFoundation.FCButton();
            this.cmbTransactionType = new fecherFoundation.FCComboBox();
            this.panel2 = new Wisej.Web.Panel();
            this.lblChangeDue = new fecherFoundation.FCLabel();
            this.fcLabel8 = new fecherFoundation.FCLabel();
            this.lblTransactionTotal = new fecherFoundation.FCLabel();
            this.lblConvenienceFeeTotal = new fecherFoundation.FCLabel();
            this.lblCashTotal = new fecherFoundation.FCLabel();
            this.lblCheckTotal = new fecherFoundation.FCLabel();
            this.lblCreditTotal = new fecherFoundation.FCLabel();
            this.lblReceiptTotal = new fecherFoundation.FCLabel();
            this.lblTransactions = new fecherFoundation.FCLabel();
            this.lblConvenienceFee = new fecherFoundation.FCLabel();
            this.lblCashOutCashPaid = new fecherFoundation.FCLabel();
            this.lblCashOutCheckPaid = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.lblCashOutCreditPaid = new fecherFoundation.FCLabel();
            this.lblCashOutTotal = new fecherFoundation.FCLabel();
            this.lblCashOutChange = new fecherFoundation.FCLabel();
            this.lblCashOutTotalDue = new fecherFoundation.FCLabel();
            this.paymentPanel = new Wisej.Web.FlowLayoutPanel();
            this.tblPayment = new Wisej.Web.TableLayoutPanel();
            this.txtPayment = new fecherFoundation.FCTextBox();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.tblCashoutType = new Wisej.Web.TableLayoutPanel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.cmbPaymentType = new Wisej.Web.ComboBox();
            this.tblCardType = new Wisej.Web.TableLayoutPanel();
            this.cmbCardType = new Wisej.Web.ComboBox();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.tblBank = new Wisej.Web.TableLayoutPanel();
            this.cmbBank = new Wisej.Web.ComboBox();
            this.fcLabel7 = new fecherFoundation.FCLabel();
            this.tblCheckNumber = new Wisej.Web.TableLayoutPanel();
            this.txtCheckNumber = new fecherFoundation.FCTextBox();
            this.fcLabel6 = new fecherFoundation.FCLabel();
            this.tableLayoutPanel1 = new Wisej.Web.TableLayoutPanel();
            this.btnAddPayment = new fecherFoundation.FCButton();
            this.gridPayments = new fecherFoundation.FCGrid();
            this.txtCustNum_PaidBy = new fecherFoundation.FCTextBox();
            this.cmdSearch_PaidBy = new fecherFoundation.FCButton();
            this.txtPaidBy = new fecherFoundation.FCTextBox();
            this.lblCustNum_PaidBy = new fecherFoundation.FCLabel();
            this.lblPaidBy = new fecherFoundation.FCLabel();
            this.cmdEdit_PaidBy = new fecherFoundation.FCButton();
            this.imgMemo_PaidBy = new fecherFoundation.FCPictureBox();
            this.cmdVoidByReceipt = new fecherFoundation.FCButton();
            this.cmbCopies = new fecherFoundation.FCComboBox();
            this.chkPrint = new fecherFoundation.FCCheckBox();
            this.lblCopies = new fecherFoundation.FCLabel();
            this.lblPrint = new fecherFoundation.FCLabel();
            this.btnMyRecReceiptImport = new fecherFoundation.FCButton();
            this.btnImportMooringReceipts = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddTransaction)).BeginInit();
            this.panel2.SuspendLayout();
            this.paymentPanel.SuspendLayout();
            this.tblPayment.SuspendLayout();
            this.tblCashoutType.SuspendLayout();
            this.tblCardType.SuspendLayout();
            this.tblBank.SuspendLayout();
            this.tblCheckNumber.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch_PaidBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit_PaidBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMemo_PaidBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdVoidByReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMyRecReceiptImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportMooringReceipts)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 549);
            this.BottomPanel.Size = new System.Drawing.Size(1002, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbCopies);
            this.ClientArea.Controls.Add(this.chkPrint);
            this.ClientArea.Controls.Add(this.lblCopies);
            this.ClientArea.Controls.Add(this.lblPrint);
            this.ClientArea.Controls.Add(this.cmdEdit_PaidBy);
            this.ClientArea.Controls.Add(this.imgMemo_PaidBy);
            this.ClientArea.Controls.Add(this.txtCustNum_PaidBy);
            this.ClientArea.Controls.Add(this.cmdSearch_PaidBy);
            this.ClientArea.Controls.Add(this.txtPaidBy);
            this.ClientArea.Controls.Add(this.lblCustNum_PaidBy);
            this.ClientArea.Controls.Add(this.lblPaidBy);
            this.ClientArea.Controls.Add(this.cmbTransactionType);
            this.ClientArea.Controls.Add(this.btnAddTransaction);
            this.ClientArea.Controls.Add(this.SummaryGrid);
            this.ClientArea.Controls.Add(this.panel2);
            this.ClientArea.Size = new System.Drawing.Size(1022, 650);
            this.ClientArea.Controls.SetChildIndex(this.panel2, 0);
            this.ClientArea.Controls.SetChildIndex(this.SummaryGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.btnAddTransaction, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbTransactionType, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCustNum_PaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSearch_PaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCustNum_PaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgMemo_PaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdEdit_PaidBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCopies, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbCopies, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnImportMooringReceipts);
            this.TopPanel.Controls.Add(this.btnMyRecReceiptImport);
            this.TopPanel.Controls.Add(this.cmdVoidByReceipt);
            this.TopPanel.Size = new System.Drawing.Size(1022, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdVoidByReceipt, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnMyRecReceiptImport, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnImportMooringReceipts, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(145, 28);
            this.HeaderText.Text = "Receipt Input";
            // 
            // SummaryGrid
            // 
            this.SummaryGrid.Cols = 3;
            this.SummaryGrid.FixedCols = 0;
            this.SummaryGrid.Location = new System.Drawing.Point(30, 63);
            this.SummaryGrid.Name = "SummaryGrid";
            this.SummaryGrid.RowHeadersVisible = false;
            this.SummaryGrid.Rows = 1;
            this.SummaryGrid.ShowFocusCell = false;
            this.SummaryGrid.Size = new System.Drawing.Size(964, 162);
            this.SummaryGrid.TabIndex = 18;
            this.SummaryGrid.CurrentCellChanged += new System.EventHandler(this.SummaryGrid_CurrentCellChanged);
            this.SummaryGrid.DoubleClick += new System.EventHandler(this.SummaryGrid_DoubleClick);
            this.SummaryGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.SummaryGrid_KeyDown);
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "acceptButton";
            this.cmdContinue.Cursor = Wisej.Web.Cursors.Default;
            this.cmdContinue.Location = new System.Drawing.Point(413, 30);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdContinue.Size = new System.Drawing.Size(188, 48);
            this.cmdContinue.TabIndex = 101;
            this.cmdContinue.Text = "Save and Continue";
            // 
            // btnAddTransaction
            // 
            this.btnAddTransaction.AppearanceKey = "actionButton";
            this.btnAddTransaction.Cursor = Wisej.Web.Cursors.Default;
            this.btnAddTransaction.Location = new System.Drawing.Point(368, 4);
            this.btnAddTransaction.Name = "btnAddTransaction";
            this.btnAddTransaction.Size = new System.Drawing.Size(143, 36);
            this.btnAddTransaction.TabIndex = 2;
            this.btnAddTransaction.Text = "Add Transaction";
            // 
            // cmbTransactionType
            // 
            this.cmbTransactionType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
            this.cmbTransactionType.Location = new System.Drawing.Point(30, 6);
            this.cmbTransactionType.Name = "cmbTransactionType";
            this.cmbTransactionType.Size = new System.Drawing.Size(303, 34);
            this.cmbTransactionType.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblChangeDue);
            this.panel2.Controls.Add(this.fcLabel8);
            this.panel2.Controls.Add(this.lblTransactionTotal);
            this.panel2.Controls.Add(this.lblConvenienceFeeTotal);
            this.panel2.Controls.Add(this.lblCashTotal);
            this.panel2.Controls.Add(this.lblCheckTotal);
            this.panel2.Controls.Add(this.lblCreditTotal);
            this.panel2.Controls.Add(this.lblReceiptTotal);
            this.panel2.Controls.Add(this.lblTransactions);
            this.panel2.Controls.Add(this.lblConvenienceFee);
            this.panel2.Controls.Add(this.lblCashOutCashPaid);
            this.panel2.Controls.Add(this.lblCashOutCheckPaid);
            this.panel2.Controls.Add(this.fcLabel1);
            this.panel2.Controls.Add(this.fcLabel2);
            this.panel2.Controls.Add(this.lblCashOutCreditPaid);
            this.panel2.Controls.Add(this.lblCashOutTotal);
            this.panel2.Controls.Add(this.lblCashOutChange);
            this.panel2.Controls.Add(this.lblCashOutTotalDue);
            this.panel2.Controls.Add(this.paymentPanel);
            this.panel2.Controls.Add(this.gridPayments);
            this.panel2.Location = new System.Drawing.Point(3, 310);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(995, 239);
            this.panel2.TabIndex = 131;
            this.panel2.TabStop = true;
            // 
            // lblChangeDue
            // 
            this.lblChangeDue.Location = new System.Drawing.Point(168, 173);
            this.lblChangeDue.Name = "lblChangeDue";
            this.lblChangeDue.TabIndex = 146;
            this.lblChangeDue.Text = "0.00";
            this.lblChangeDue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // fcLabel8
            // 
            this.fcLabel8.Location = new System.Drawing.Point(27, 173);
            this.fcLabel8.Name = "fcLabel8";
            this.fcLabel8.Size = new System.Drawing.Size(140, 16);
            this.fcLabel8.TabIndex = 145;
            this.fcLabel8.Text = "CHANGE DUE";
            // 
            // lblTransactionTotal
            // 
            this.lblTransactionTotal.Location = new System.Drawing.Point(168, 17);
            this.lblTransactionTotal.Name = "lblTransactionTotal";
            this.lblTransactionTotal.TabIndex = 142;
            this.lblTransactionTotal.Text = "0.00";
            this.lblTransactionTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblConvenienceFeeTotal
            // 
            this.lblConvenienceFeeTotal.Location = new System.Drawing.Point(168, 43);
            this.lblConvenienceFeeTotal.Name = "lblConvenienceFeeTotal";
            this.lblConvenienceFeeTotal.TabIndex = 141;
            this.lblConvenienceFeeTotal.Text = "0.00";
            this.lblConvenienceFeeTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCashTotal
            // 
            this.lblCashTotal.Location = new System.Drawing.Point(168, 95);
            this.lblCashTotal.Name = "lblCashTotal";
            this.lblCashTotal.TabIndex = 140;
            this.lblCashTotal.Text = "0.00";
            this.lblCashTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCheckTotal
            // 
            this.lblCheckTotal.Location = new System.Drawing.Point(168, 121);
            this.lblCheckTotal.Name = "lblCheckTotal";
            this.lblCheckTotal.TabIndex = 139;
            this.lblCheckTotal.Text = "0.00";
            this.lblCheckTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Location = new System.Drawing.Point(168, 147);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.TabIndex = 138;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblReceiptTotal
            // 
            this.lblReceiptTotal.Location = new System.Drawing.Point(168, 69);
            this.lblReceiptTotal.Name = "lblReceiptTotal";
            this.lblReceiptTotal.TabIndex = 137;
            this.lblReceiptTotal.Text = "0.00";
            this.lblReceiptTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTransactions
            // 
            this.lblTransactions.Location = new System.Drawing.Point(27, 17);
            this.lblTransactions.Name = "lblTransactions";
            this.lblTransactions.Size = new System.Drawing.Size(117, 16);
            this.lblTransactions.TabIndex = 136;
            this.lblTransactions.Text = "TRANSACTIONS";
            // 
            // lblConvenienceFee
            // 
            this.lblConvenienceFee.Location = new System.Drawing.Point(27, 43);
            this.lblConvenienceFee.Name = "lblConvenienceFee";
            this.lblConvenienceFee.Size = new System.Drawing.Size(157, 16);
            this.lblConvenienceFee.TabIndex = 135;
            this.lblConvenienceFee.Text = "CONVENIENCE FEE";
            // 
            // lblCashOutCashPaid
            // 
            this.lblCashOutCashPaid.Location = new System.Drawing.Point(27, 95);
            this.lblCashOutCashPaid.Name = "lblCashOutCashPaid";
            this.lblCashOutCashPaid.Size = new System.Drawing.Size(133, 16);
            this.lblCashOutCashPaid.TabIndex = 134;
            this.lblCashOutCashPaid.Text = "TOTAL CASH PAID";
            // 
            // lblCashOutCheckPaid
            // 
            this.lblCashOutCheckPaid.Location = new System.Drawing.Point(27, 121);
            this.lblCashOutCheckPaid.Name = "lblCashOutCheckPaid";
            this.lblCashOutCheckPaid.Size = new System.Drawing.Size(133, 16);
            this.lblCashOutCheckPaid.TabIndex = 133;
            this.lblCashOutCheckPaid.Text = "TOTAL CHECK PAID";
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(27, 360);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(57, 16);
            this.fcLabel1.TabIndex = 132;
            this.fcLabel1.Text = "CHANGE";
            this.fcLabel1.Visible = false;
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(27, 310);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(72, 16);
            this.fcLabel2.TabIndex = 131;
            this.fcLabel2.Text = "TOTAL DUE";
            // 
            // lblCashOutCreditPaid
            // 
            this.lblCashOutCreditPaid.Location = new System.Drawing.Point(27, 147);
            this.lblCashOutCreditPaid.Name = "lblCashOutCreditPaid";
            this.lblCashOutCreditPaid.Size = new System.Drawing.Size(140, 16);
            this.lblCashOutCreditPaid.TabIndex = 130;
            this.lblCashOutCreditPaid.Text = "TOTAL CREDIT PAID";
            // 
            // lblCashOutTotal
            // 
            this.lblCashOutTotal.Location = new System.Drawing.Point(27, 69);
            this.lblCashOutTotal.Name = "lblCashOutTotal";
            this.lblCashOutTotal.Size = new System.Drawing.Size(92, 16);
            this.lblCashOutTotal.TabIndex = 129;
            this.lblCashOutTotal.Text = "RECEIPT TOTAL";
            // 
            // lblCashOutChange
            // 
            this.lblCashOutChange.Location = new System.Drawing.Point(27, 364);
            this.lblCashOutChange.Name = "lblCashOutChange";
            this.lblCashOutChange.Size = new System.Drawing.Size(57, 16);
            this.lblCashOutChange.TabIndex = 128;
            this.lblCashOutChange.Text = "CHANGE";
            this.lblCashOutChange.Visible = false;
            // 
            // lblCashOutTotalDue
            // 
            this.lblCashOutTotalDue.Location = new System.Drawing.Point(27, 314);
            this.lblCashOutTotalDue.Name = "lblCashOutTotalDue";
            this.lblCashOutTotalDue.Size = new System.Drawing.Size(72, 16);
            this.lblCashOutTotalDue.TabIndex = 127;
            this.lblCashOutTotalDue.Text = "TOTAL DUE";
            // 
            // paymentPanel
            // 
            this.paymentPanel.Controls.Add(this.tblPayment);
            this.paymentPanel.Controls.Add(this.tblCashoutType);
            this.paymentPanel.Controls.Add(this.tblCardType);
            this.paymentPanel.Controls.Add(this.tblBank);
            this.paymentPanel.Controls.Add(this.tblCheckNumber);
            this.paymentPanel.Controls.Add(this.tableLayoutPanel1);
            this.paymentPanel.FlowDirection = Wisej.Web.FlowDirection.TopDown;
            this.paymentPanel.Location = new System.Drawing.Point(305, 2);
            this.paymentPanel.Name = "paymentPanel";
            this.paymentPanel.Size = new System.Drawing.Size(298, 239);
            this.paymentPanel.TabIndex = 7;
            this.paymentPanel.TabStop = true;
            // 
            // tblPayment
            // 
            this.tblPayment.ColumnCount = 2;
            this.tblPayment.ColumnStyles.Clear();
            this.tblPayment.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 118F));
            this.tblPayment.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tblPayment.Controls.Add(this.txtPayment, 0, 0);
            this.tblPayment.Controls.Add(this.fcLabel3, 0, 0);
            this.tblPayment.Location = new System.Drawing.Point(3, 3);
            this.tblPayment.Margin = new Wisej.Web.Padding(3, 3, 3, 5);
            this.tblPayment.Name = "tblPayment";
            this.tblPayment.RowCount = 1;
            this.tblPayment.RowStyles.Clear();
            this.tblPayment.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tblPayment.Size = new System.Drawing.Size(295, 39);
            this.tblPayment.TabIndex = 0;
            this.tblPayment.TabStop = true;
            // 
            // txtPayment
            // 
            this.txtPayment.BackColor = System.Drawing.SystemColors.Window;
            this.txtPayment.Cursor = Wisej.Web.Cursors.Default;
            this.txtPayment.Location = new System.Drawing.Point(121, 3);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(171, 33);
            this.txtPayment.TabIndex = 1;
            this.txtPayment.Text = "0.00";
            this.txtPayment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel3
            // 
            this.fcLabel3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fcLabel3.Location = new System.Drawing.Point(3, 3);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(76, 33);
            this.fcLabel3.TabIndex = 137;
            this.fcLabel3.Text = "PAYMENT";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tblCashoutType
            // 
            this.tblCashoutType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.tblCashoutType.ColumnCount = 2;
            this.tblCashoutType.ColumnStyles.Clear();
            this.tblCashoutType.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 118F));
            this.tblCashoutType.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tblCashoutType.Controls.Add(this.fcLabel4, 0, 0);
            this.tblCashoutType.Controls.Add(this.cmbPaymentType, 1, 0);
            this.tblCashoutType.Location = new System.Drawing.Point(3, 52);
            this.tblCashoutType.Margin = new Wisej.Web.Padding(3, 5, 3, 5);
            this.tblCashoutType.Name = "tblCashoutType";
            this.tblCashoutType.RowCount = 1;
            this.tblCashoutType.RowStyles.Clear();
            this.tblCashoutType.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tblCashoutType.Size = new System.Drawing.Size(295, 39);
            this.tblCashoutType.TabIndex = 1;
            this.tblCashoutType.TabStop = true;
            // 
            // fcLabel4
            // 
            this.fcLabel4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fcLabel4.Location = new System.Drawing.Point(3, 3);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(97, 33);
            this.fcLabel4.TabIndex = 137;
            this.fcLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.AutoSize = false;
            this.cmbPaymentType.DisplayMember = "Description";
            this.cmbPaymentType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPaymentType.Location = new System.Drawing.Point(121, 3);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(171, 33);
            this.cmbPaymentType.TabIndex = 2;
            this.cmbPaymentType.ValueMember = "ID";
            // 
            // tblCardType
            // 
            this.tblCardType.ColumnCount = 2;
            this.tblCardType.ColumnStyles.Clear();
            this.tblCardType.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 118F));
            this.tblCardType.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tblCardType.Controls.Add(this.cmbCardType, 0, 0);
            this.tblCardType.Controls.Add(this.fcLabel5, 0, 0);
            this.tblCardType.Location = new System.Drawing.Point(3, 99);
            this.tblCardType.Name = "tblCardType";
            this.tblCardType.RowCount = 1;
            this.tblCardType.RowStyles.Clear();
            this.tblCardType.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tblCardType.Size = new System.Drawing.Size(295, 39);
            this.tblCardType.TabIndex = 2;
            this.tblCardType.TabStop = true;
            this.tblCardType.Visible = false;
            // 
            // cmbCardType
            // 
            this.cmbCardType.AutoSize = false;
            this.cmbCardType.DisplayMember = "Description";
            this.cmbCardType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbCardType.Location = new System.Drawing.Point(121, 3);
            this.cmbCardType.Name = "cmbCardType";
            this.cmbCardType.Size = new System.Drawing.Size(171, 33);
            this.cmbCardType.TabIndex = 139;
            this.cmbCardType.ValueMember = "ID";
            // 
            // fcLabel5
            // 
            this.fcLabel5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fcLabel5.Location = new System.Drawing.Point(3, 3);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(97, 33);
            this.fcLabel5.TabIndex = 137;
            this.fcLabel5.Text = "CARD TYPE";
            this.fcLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tblBank
            // 
            this.tblBank.ColumnCount = 2;
            this.tblBank.ColumnStyles.Clear();
            this.tblBank.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 118F));
            this.tblBank.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tblBank.Controls.Add(this.cmbBank, 0, 0);
            this.tblBank.Controls.Add(this.fcLabel7, 0, 0);
            this.tblBank.Location = new System.Drawing.Point(3, 144);
            this.tblBank.Name = "tblBank";
            this.tblBank.RowCount = 1;
            this.tblBank.RowStyles.Clear();
            this.tblBank.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tblBank.Size = new System.Drawing.Size(295, 39);
            this.tblBank.TabIndex = 5;
            this.tblBank.TabStop = true;
            this.tblBank.Visible = false;
            // 
            // cmbBank
            // 
            this.cmbBank.AutoSize = false;
            this.cmbBank.DisplayMember = "Description";
            this.cmbBank.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbBank.Location = new System.Drawing.Point(121, 3);
            this.cmbBank.Name = "cmbBank";
            this.cmbBank.Size = new System.Drawing.Size(171, 33);
            this.cmbBank.TabIndex = 138;
            this.cmbBank.ValueMember = "ID";
            // 
            // fcLabel7
            // 
            this.fcLabel7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fcLabel7.Location = new System.Drawing.Point(3, 3);
            this.fcLabel7.Name = "fcLabel7";
            this.fcLabel7.Size = new System.Drawing.Size(97, 33);
            this.fcLabel7.TabIndex = 137;
            this.fcLabel7.Text = "BANK";
            this.fcLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tblCheckNumber
            // 
            this.tblCheckNumber.ColumnCount = 2;
            this.tblCheckNumber.ColumnStyles.Clear();
            this.tblCheckNumber.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 118F));
            this.tblCheckNumber.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tblCheckNumber.Controls.Add(this.txtCheckNumber, 0, 0);
            this.tblCheckNumber.Controls.Add(this.fcLabel6, 0, 0);
            this.tblCheckNumber.Location = new System.Drawing.Point(3, 189);
            this.tblCheckNumber.Margin = new Wisej.Web.Padding(3, 3, 3, 5);
            this.tblCheckNumber.Name = "tblCheckNumber";
            this.tblCheckNumber.RowCount = 1;
            this.tblCheckNumber.RowStyles.Clear();
            this.tblCheckNumber.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tblCheckNumber.Size = new System.Drawing.Size(295, 39);
            this.tblCheckNumber.TabIndex = 3;
            this.tblCheckNumber.TabStop = true;
            this.tblCheckNumber.Visible = false;
            // 
            // txtCheckNumber
            // 
            this.txtCheckNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtCheckNumber.Cursor = Wisej.Web.Cursors.Default;
            this.txtCheckNumber.Location = new System.Drawing.Point(121, 3);
            this.txtCheckNumber.Name = "txtCheckNumber";
            this.txtCheckNumber.Size = new System.Drawing.Size(171, 33);
            this.txtCheckNumber.TabIndex = 4;
            this.txtCheckNumber.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // fcLabel6
            // 
            this.fcLabel6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fcLabel6.Location = new System.Drawing.Point(3, 3);
            this.fcLabel6.Name = "fcLabel6";
            this.fcLabel6.Size = new System.Drawing.Size(97, 33);
            this.fcLabel6.TabIndex = 137;
            this.fcLabel6.Text = "CHECK #";
            this.fcLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Clear();
            this.tableLayoutPanel1.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Absolute, 118F));
            this.tableLayoutPanel1.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.btnAddPayment, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(304, 3);
            this.tableLayoutPanel1.Margin = new Wisej.Web.Padding(3, 3, 3, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Clear();
            this.tableLayoutPanel1.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(290, 39);
            this.tableLayoutPanel1.TabIndex = 4;
            this.tableLayoutPanel1.TabStop = true;
            // 
            // btnAddPayment
            // 
            this.btnAddPayment.AppearanceKey = "actionButton";
            this.btnAddPayment.Cursor = Wisej.Web.Cursors.Default;
            this.btnAddPayment.Location = new System.Drawing.Point(121, 3);
            this.btnAddPayment.Name = "btnAddPayment";
            this.btnAddPayment.Size = new System.Drawing.Size(166, 33);
            this.btnAddPayment.TabIndex = 3;
            this.btnAddPayment.Text = "Add Payment";
            // 
            // gridPayments
            // 
            this.gridPayments.Cols = 3;
            this.gridPayments.FixedCols = 0;
            this.gridPayments.Location = new System.Drawing.Point(609, 8);
            this.gridPayments.Name = "gridPayments";
            this.gridPayments.RowHeadersVisible = false;
            this.gridPayments.Rows = 1;
            this.gridPayments.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.gridPayments.ShowFocusCell = false;
            this.gridPayments.Size = new System.Drawing.Size(382, 190);
            this.gridPayments.TabIndex = 144;
            this.gridPayments.KeyDown += new Wisej.Web.KeyEventHandler(this.gridPayments_KeyDown);
            // 
            // txtCustNum_PaidBy
            // 
            this.txtCustNum_PaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtCustNum_PaidBy.Cursor = Wisej.Web.Cursors.Default;
            this.txtCustNum_PaidBy.Location = new System.Drawing.Point(486, 250);
            this.txtCustNum_PaidBy.Name = "txtCustNum_PaidBy";
            this.txtCustNum_PaidBy.Size = new System.Drawing.Size(85, 40);
            this.txtCustNum_PaidBy.TabIndex = 4;
            this.txtCustNum_PaidBy.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustNum_PaidBy_Validating);
            this.txtCustNum_PaidBy.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustNum_PaidBy_KeyPress);
            // 
            // cmdSearch_PaidBy
            // 
            this.cmdSearch_PaidBy.AppearanceKey = "actionButton";
            this.cmdSearch_PaidBy.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSearch_PaidBy.ImageSource = "icon - search";
            this.cmdSearch_PaidBy.Location = new System.Drawing.Point(577, 250);
            this.cmdSearch_PaidBy.Name = "cmdSearch_PaidBy";
            this.cmdSearch_PaidBy.Size = new System.Drawing.Size(40, 40);
            this.cmdSearch_PaidBy.TabIndex = 5;
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaidBy.Cursor = Wisej.Web.Cursors.Default;
            this.txtPaidBy.Location = new System.Drawing.Point(117, 250);
            this.txtPaidBy.MaxLength = 50;
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.Size = new System.Drawing.Size(294, 40);
            this.txtPaidBy.TabIndex = 3;
            this.txtPaidBy.Validated += new System.EventHandler(this.txtPaidBy_Validated);
            // 
            // lblCustNum_PaidBy
            // 
            this.lblCustNum_PaidBy.Location = new System.Drawing.Point(441, 264);
            this.lblCustNum_PaidBy.Name = "lblCustNum_PaidBy";
            this.lblCustNum_PaidBy.Size = new System.Drawing.Size(25, 16);
            this.lblCustNum_PaidBy.TabIndex = 137;
            this.lblCustNum_PaidBy.Text = "ID#";
            // 
            // lblPaidBy
            // 
            this.lblPaidBy.Location = new System.Drawing.Point(30, 264);
            this.lblPaidBy.Name = "lblPaidBy";
            this.lblPaidBy.Size = new System.Drawing.Size(52, 16);
            this.lblPaidBy.TabIndex = 134;
            this.lblPaidBy.Text = "PAID BY";
            // 
            // cmdEdit_PaidBy
            // 
            this.cmdEdit_PaidBy.AppearanceKey = "actionButton";
            this.cmdEdit_PaidBy.Cursor = Wisej.Web.Cursors.Default;
            this.cmdEdit_PaidBy.Enabled = false;
            this.cmdEdit_PaidBy.ImageSource = "icon - edit";
            this.cmdEdit_PaidBy.Location = new System.Drawing.Point(621, 250);
            this.cmdEdit_PaidBy.Name = "cmdEdit_PaidBy";
            this.cmdEdit_PaidBy.Size = new System.Drawing.Size(40, 40);
            this.cmdEdit_PaidBy.TabIndex = 6;
            this.cmdEdit_PaidBy.Click += new System.EventHandler(this.cmdEdit_PaidBy_Click);
            // 
            // imgMemo_PaidBy
            // 
            this.imgMemo_PaidBy.Anchor = Wisej.Web.AnchorStyles.Left;
            this.imgMemo_PaidBy.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgMemo_PaidBy.Cursor = Wisej.Web.Cursors.Default;
            this.imgMemo_PaidBy.Image = ((System.Drawing.Image)(resources.GetObject("imgMemo_PaidBy.Image")));
            this.imgMemo_PaidBy.Location = new System.Drawing.Point(661, 304);
            this.imgMemo_PaidBy.Name = "imgMemo_PaidBy";
            this.imgMemo_PaidBy.Size = new System.Drawing.Size(40, 40);
            this.imgMemo_PaidBy.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgMemo_PaidBy.Visible = false;
            // 
            // cmdVoidByReceipt
            // 
            this.cmdVoidByReceipt.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdVoidByReceipt.Cursor = Wisej.Web.Cursors.Default;
            this.cmdVoidByReceipt.Location = new System.Drawing.Point(834, 26);
            this.cmdVoidByReceipt.Name = "cmdVoidByReceipt";
            this.cmdVoidByReceipt.Shortcut = Wisej.Web.Shortcut.F8;
            this.cmdVoidByReceipt.Size = new System.Drawing.Size(168, 24);
            this.cmdVoidByReceipt.TabIndex = 2;
            this.cmdVoidByReceipt.Text = "Void By Receipt Number";
            this.cmdVoidByReceipt.Click += new System.EventHandler(this.cmdVoidByReceipt_Click);
            // 
            // cmbCopies
            // 
            this.cmbCopies.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCopies.Location = new System.Drawing.Point(935, 250);
            this.cmbCopies.Name = "cmbCopies";
            this.cmbCopies.Size = new System.Drawing.Size(59, 40);
            this.cmbCopies.Sorted = true;
            this.cmbCopies.TabIndex = 141;
            // 
            // chkPrint
            // 
            this.chkPrint.Location = new System.Drawing.Point(837, 260);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(32, 22);
            this.chkPrint.TabIndex = 139;
            // 
            // lblCopies
            // 
            this.lblCopies.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCopies.Location = new System.Drawing.Point(874, 262);
            this.lblCopies.Name = "lblCopies";
            this.lblCopies.Size = new System.Drawing.Size(64, 20);
            this.lblCopies.TabIndex = 140;
            this.lblCopies.Text = "COPIES";
            this.lblCopies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPrint
            // 
            this.lblPrint.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblPrint.Location = new System.Drawing.Point(733, 262);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(103, 20);
            this.lblPrint.TabIndex = 138;
            this.lblPrint.Text = "PRINT RECEIPT";
            this.lblPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnMyRecReceiptImport
            // 
            this.btnMyRecReceiptImport.AccessibleDescription = "";
            this.btnMyRecReceiptImport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnMyRecReceiptImport.Cursor = Wisej.Web.Cursors.Default;
            this.btnMyRecReceiptImport.Location = new System.Drawing.Point(650, 26);
            this.btnMyRecReceiptImport.Name = "btnMyRecReceiptImport";
            this.btnMyRecReceiptImport.Shortcut = Wisej.Web.Shortcut.F8;
            this.btnMyRecReceiptImport.Size = new System.Drawing.Size(168, 24);
            this.btnMyRecReceiptImport.TabIndex = 3;
            this.btnMyRecReceiptImport.Text = "Import MyRec Receipts";
            // 
            // btnImportMooringReceipts
            // 
            this.btnImportMooringReceipts.AccessibleDescription = "";
            this.btnImportMooringReceipts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnImportMooringReceipts.Cursor = Wisej.Web.Cursors.Default;
            this.btnImportMooringReceipts.Location = new System.Drawing.Point(447, 26);
            this.btnImportMooringReceipts.Name = "btnImportMooringReceipts";
            this.btnImportMooringReceipts.Shortcut = Wisej.Web.Shortcut.F8;
            this.btnImportMooringReceipts.Size = new System.Drawing.Size(168, 24);
            this.btnImportMooringReceipts.TabIndex = 4;
            this.btnImportMooringReceipts.Text = "Import Mooring Receipts";
            // 
            // frmReceiptTransactionSummary
            // 
            this.ClientSize = new System.Drawing.Size(1022, 710);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.Name = "frmReceiptTransactionSummary";
            this.Text = "Receipt Input";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.frmReceiptTransactionSummary_QueryUnload);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddTransaction)).EndInit();
            this.panel2.ResumeLayout(false);
            this.paymentPanel.ResumeLayout(false);
            this.tblPayment.ResumeLayout(false);
            this.tblCashoutType.ResumeLayout(false);
            this.tblCardType.ResumeLayout(false);
            this.tblBank.ResumeLayout(false);
            this.tblCheckNumber.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnAddPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch_PaidBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEdit_PaidBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMemo_PaidBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdVoidByReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMyRecReceiptImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportMooringReceipts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCGrid SummaryGrid;
        public fecherFoundation.FCButton cmdContinue;
        private fecherFoundation.FCButton btnAddTransaction;
        private fecherFoundation.FCComboBox cmbTransactionType;
        private Wisej.Web.Panel panel2;
        public fecherFoundation.FCLabel lblTransactions;
        public fecherFoundation.FCLabel lblConvenienceFee;
        public fecherFoundation.FCLabel lblCashOutCashPaid;
        public fecherFoundation.FCLabel lblCashOutCheckPaid;
        public fecherFoundation.FCLabel fcLabel1;
        public fecherFoundation.FCLabel fcLabel2;
        public fecherFoundation.FCLabel lblCashOutCreditPaid;
        public fecherFoundation.FCLabel lblCashOutTotal;
        public fecherFoundation.FCLabel lblCashOutChange;
        public fecherFoundation.FCLabel lblCashOutTotalDue;
        public fecherFoundation.FCLabel lblTransactionTotal;
        public fecherFoundation.FCLabel lblConvenienceFeeTotal;
        public fecherFoundation.FCLabel lblCashTotal;
        public fecherFoundation.FCLabel lblCheckTotal;
        public fecherFoundation.FCLabel lblCreditTotal;
        public fecherFoundation.FCLabel lblReceiptTotal;
        private Wisej.Web.FlowLayoutPanel paymentPanel;
        private Wisej.Web.TableLayoutPanel tblPayment;
        public fecherFoundation.FCLabel fcLabel3;
        public fecherFoundation.FCTextBox txtPayment;
        private Wisej.Web.TableLayoutPanel tblCashoutType;
        public fecherFoundation.FCLabel fcLabel4;
        private Wisej.Web.TableLayoutPanel tblCardType;
        public fecherFoundation.FCLabel fcLabel5;
        private Wisej.Web.TableLayoutPanel tblCheckNumber;
        public fecherFoundation.FCTextBox txtCheckNumber;
        public fecherFoundation.FCLabel fcLabel6;
        public fecherFoundation.FCTextBox txtCustNum_PaidBy;
        public fecherFoundation.FCButton cmdSearch_PaidBy;
        public fecherFoundation.FCTextBox txtPaidBy;
        public fecherFoundation.FCLabel lblCustNum_PaidBy;
        public fecherFoundation.FCLabel lblPaidBy;
        public fecherFoundation.FCButton cmdEdit_PaidBy;
        public fecherFoundation.FCPictureBox imgMemo_PaidBy;
        public fecherFoundation.FCGrid gridPayments;
        private Wisej.Web.ComboBox cmbPaymentType;
        private Wisej.Web.TableLayoutPanel tableLayoutPanel1;
        private fecherFoundation.FCButton btnAddPayment;
        public fecherFoundation.FCLabel lblChangeDue;
        public fecherFoundation.FCLabel fcLabel8;
        private Wisej.Web.TableLayoutPanel tblBank;
        private Wisej.Web.ComboBox cmbBank;
        public fecherFoundation.FCLabel fcLabel7;
		private Wisej.Web.ComboBox cmbCardType;
		private fecherFoundation.FCButton cmdVoidByReceipt;
		public fecherFoundation.FCComboBox cmbCopies;
		public fecherFoundation.FCCheckBox chkPrint;
		public fecherFoundation.FCLabel lblCopies;
		public fecherFoundation.FCLabel lblPrint;
        private fecherFoundation.FCButton btnMyRecReceiptImport;
        private fecherFoundation.FCButton btnImportMooringReceipts;
    }
}