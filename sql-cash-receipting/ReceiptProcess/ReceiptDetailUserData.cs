﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting;

namespace TWCR0000.ReceiptProcess
{
	public class ReceiptDetailsUserData
	{
		public TransactionBase transaction { get; set; }
		public bool ShowFeeDetails { get; set; }
	}
}
