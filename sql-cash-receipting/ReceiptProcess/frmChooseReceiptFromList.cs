﻿using System;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCR0000.ReceiptProcess
{
    public partial class frmChooseReceiptFromList : BaseForm, IModalView<ISelectReceiptFromListViewModel>
    {
        private bool cancelling = true;
        public frmChooseReceiptFromList()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmChooseReceiptFromList(ISelectReceiptFromListViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        private void InitializeComponentEx()
        {
            this.cmdOk.Click += CmdOk_Click;
            this.Load += FrmChooseReceiptFromList_Load;
            ReceiptGrid.DoubleClick += ReceiptGrid_DoubleClick;
            this.Resize += FrmChooseReceiptFromList_Resize;
        }

        private void FrmChooseReceiptFromList_Resize(object sender, EventArgs e)
        {
            ResizeGrid();
        }

        private void ReceiptGrid_DoubleClick(object sender, EventArgs e)
        {
            SelectReceipt();
        }

        private void FrmChooseReceiptFromList_Load(object sender, EventArgs e)
        {
            SetupGrid();
            FillGrid();
        }

        private void CmdOk_Click(object sender, EventArgs e)
        {
            SelectReceipt();
        }

        private void CloseWithoutCancelling()
        {
            cancelling = false;
            ViewModel.Cancelled = false;
            Close();
        }

        private void SelectReceipt()
        {
            if (ReceiptGrid.Row < 1)
            {
                FCMessageBox.Show("You must select a receipt before continuing",
                    MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "No Receipt Selected");
                return;
            }

            ViewModel.SelectedReceiptId = (int)ReceiptGrid.RowData(ReceiptGrid.Row);
            CloseWithoutCancelling();
        }

        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }

        public ISelectReceiptFromListViewModel ViewModel { get; set; }

        private void FillGrid()
        {
            ReceiptGrid.Rows = 1;
            foreach (var receiptInfo in ViewModel.ReceiptList)
            {
                ReceiptGrid.Rows += 1;
                var row = ReceiptGrid.Rows - 1;
                ReceiptGrid.TextMatrix(row, (int) ReceiptColumns.PaidBy, receiptInfo.PaidBy);
                ReceiptGrid.TextMatrix(row, (int) ReceiptColumns.ReceiptDate,
                    receiptInfo.ReceiptDate.FormatAndPadShortDate());
                ReceiptGrid.TextMatrix(row, (int) ReceiptColumns.TellerID, receiptInfo.TellerID);
                ReceiptGrid.RowData(row, receiptInfo.Id);
            }
        }

        private void SetupGrid()
        {
            ReceiptGrid.Rows = 1;           
            ReceiptGrid.TextMatrix(0, (int) ReceiptColumns.ReceiptDate, "Date");
            ReceiptGrid.TextMatrix(0, (int) ReceiptColumns.PaidBy, "Paid By");
            ReceiptGrid.TextMatrix(0, (int) ReceiptColumns.TellerID, "Teller ID");
        }

        private void ResizeGrid()
        {
            int gridWidth = ReceiptGrid.WidthOriginal;
            ReceiptGrid.ColWidth((int) ReceiptColumns.ReceiptDate, (.25 * gridWidth).ToInteger());
            ReceiptGrid.ColWidth((int)ReceiptColumns.PaidBy,(.6 * gridWidth).ToInteger());
        }
        private enum ReceiptColumns
        {                        
            ReceiptDate = 0,
            PaidBy = 1,
            TellerID = 2
        }
    }
}
