﻿namespace TWCR0000.ReceiptProcess
{
    partial class frmReceiptTransactionDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.flexLayoutPanel1 = new Wisej.Web.FlexLayoutPanel();
			this.flexLayoutPanel2 = new Wisej.Web.FlexLayoutPanel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 499);
			this.BottomPanel.Size = new System.Drawing.Size(994, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.flexLayoutPanel2);
			this.ClientArea.Controls.Add(this.flexLayoutPanel1);
			this.ClientArea.Size = new System.Drawing.Size(1014, 518);
			this.ClientArea.Controls.SetChildIndex(this.flexLayoutPanel1, 0);
			this.ClientArea.Controls.SetChildIndex(this.flexLayoutPanel2, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// flexLayoutPanel1
			// 
			this.flexLayoutPanel1.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
			this.flexLayoutPanel1.Location = new System.Drawing.Point(21, 31);
			this.flexLayoutPanel1.Name = "flexLayoutPanel1";
			this.flexLayoutPanel1.Size = new System.Drawing.Size(382, 468);
			this.flexLayoutPanel1.TabIndex = 1;
			this.flexLayoutPanel1.TabStop = true;
			// 
			// flexLayoutPanel2
			// 
			this.flexLayoutPanel2.LayoutStyle = Wisej.Web.FlexLayoutStyle.Vertical;
			this.flexLayoutPanel2.Location = new System.Drawing.Point(436, 31);
			this.flexLayoutPanel2.Name = "flexLayoutPanel2";
			this.flexLayoutPanel2.Size = new System.Drawing.Size(382, 468);
			this.flexLayoutPanel2.TabIndex = 2;
			this.flexLayoutPanel2.TabStop = true;
			// 
			// frmReceiptTransactionDetail
			// 
			this.Name = "frmReceiptTransactionDetail";
			this.Text = "Transaction Detail";
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.FlexLayoutPanel flexLayoutPanel1;
        private Wisej.Web.FlexLayoutPanel flexLayoutPanel2;
    }
}