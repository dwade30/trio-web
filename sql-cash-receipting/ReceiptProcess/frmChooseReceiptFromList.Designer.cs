﻿namespace TWCR0000.ReceiptProcess
{
    partial class frmChooseReceiptFromList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbArchive = new fecherFoundation.FCComboBox();
            this.lblArchive = new fecherFoundation.FCLabel();
            this.cmdOk = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.ReceiptGrid = new fecherFoundation.FCGrid();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceiptGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdOk);
            this.BottomPanel.Location = new System.Drawing.Point(3, 212);
            this.BottomPanel.Size = new System.Drawing.Size(531, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.ReceiptGrid);
            this.ClientArea.Controls.Add(this.lblArchive);
            this.ClientArea.Controls.Add(this.cmbArchive);
            this.ClientArea.Size = new System.Drawing.Size(551, 324);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbArchive, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblArchive, 0);
            this.ClientArea.Controls.SetChildIndex(this.ReceiptGrid, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(551, 60);
            // 
            // cmbArchive
            // 
            this.cmbArchive.Items.AddRange(new object[] {
            "Current Year",
            "Archive Year"});
            this.cmbArchive.Location = new System.Drawing.Point(159, 104);
            this.cmbArchive.Name = "cmbArchive";
            this.cmbArchive.Size = new System.Drawing.Size(147, 40);
            this.cmbArchive.TabIndex = 2;
            // 
            // lblArchive
            // 
            this.lblArchive.Location = new System.Drawing.Point(21, 115);
            this.lblArchive.Name = "lblArchive";
            this.lblArchive.Size = new System.Drawing.Size(60, 16);
            this.lblArchive.TabIndex = 1001;
            this.lblArchive.Text = "YEAR";
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "acceptButton";
            this.cmdOk.Location = new System.Drawing.Point(224, 30);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdOk.Size = new System.Drawing.Size(100, 48);
            this.cmdOk.TabIndex = 2;
            this.cmdOk.Text = "OK";
            // 
            // ReceiptGrid
            // 
            this.ReceiptGrid.Cols = 3;
            this.ReceiptGrid.ExtendLastCol = true;
            this.ReceiptGrid.FixedCols = 0;
            this.ReceiptGrid.Location = new System.Drawing.Point(5, 6);
            this.ReceiptGrid.Name = "ReceiptGrid";
            this.ReceiptGrid.RowHeadersVisible = false;
            this.ReceiptGrid.Rows = 1;
            this.ReceiptGrid.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.ReceiptGrid.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
            this.ReceiptGrid.Size = new System.Drawing.Size(529, 206);
            this.ReceiptGrid.TabIndex = 1003;
            // 
            // frmChooseReceiptFromList
            // 
            this.ClientSize = new System.Drawing.Size(551, 384);
            this.ForeColor = System.Drawing.Color.FromName("@appWorkspace");
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChooseReceiptFromList";
            this.Text = "Choose Receipt";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceiptGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public fecherFoundation.FCComboBox cmbArchive;
        public fecherFoundation.FCLabel lblArchive;
        private fecherFoundation.FCButton cmdOk;
        private Wisej.Web.JavaScript javaScript1;
        public fecherFoundation.FCGrid ReceiptGrid;
    }
}