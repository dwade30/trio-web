﻿using Global;

namespace TWCR0000.ReceiptProcess
{
    partial class frmMiscReceiptInput : BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMiscReceiptInput));
            this.fraReceipt = new fecherFoundation.FCPanel();
            this.txtName = new fecherFoundation.FCTextBox();
            this.lblName = new fecherFoundation.FCLabel();
            this.txtQuantity = new fecherFoundation.FCTextBox();
            this.chkAffectCash = new fecherFoundation.FCCheckBox();
            this.cmbResCode = new fecherFoundation.FCComboBox();
            this.txtPercentageAmount = new fecherFoundation.FCTextBox();
            this.txtComment = new fecherFoundation.FCTextBox();
            this.vsFees = new fecherFoundation.FCGrid();
            this.txtAcct1 = new fecherFoundation.FCGrid();
            this.lblResCode = new fecherFoundation.FCLabel();
            this.lblPercentAmount = new fecherFoundation.FCLabel();
            this.lblMultiple = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblComment = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtReference = new fecherFoundation.FCTextBox();
            this.txtControl2 = new fecherFoundation.FCTextBox();
            this.txtControl1 = new fecherFoundation.FCTextBox();
            this.txtControl3 = new fecherFoundation.FCTextBox();
            this.lblControl2 = new fecherFoundation.FCLabel();
            this.lblControl1 = new fecherFoundation.FCLabel();
            this.lblReference = new fecherFoundation.FCLabel();
            this.lblControl3 = new fecherFoundation.FCLabel();
            this.toolTip1 = new Wisej.Web.ToolTip(this.components);
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceipt)).BeginInit();
            this.fraReceipt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAffectCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 614);
            this.BottomPanel.Size = new System.Drawing.Size(900, 102);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraReceipt);
            this.ClientArea.Size = new System.Drawing.Size(920, 518);
            this.ClientArea.Controls.SetChildIndex(this.fraReceipt, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(920, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // fraReceipt
            // 
            this.fraReceipt.Controls.Add(this.txtName);
            this.fraReceipt.Controls.Add(this.lblName);
            this.fraReceipt.Controls.Add(this.txtQuantity);
            this.fraReceipt.Controls.Add(this.chkAffectCash);
            this.fraReceipt.Controls.Add(this.cmbResCode);
            this.fraReceipt.Controls.Add(this.txtPercentageAmount);
            this.fraReceipt.Controls.Add(this.txtComment);
            this.fraReceipt.Controls.Add(this.vsFees);
            this.fraReceipt.Controls.Add(this.txtAcct1);
            this.fraReceipt.Controls.Add(this.lblResCode);
            this.fraReceipt.Controls.Add(this.lblPercentAmount);
            this.fraReceipt.Controls.Add(this.lblMultiple);
            this.fraReceipt.Controls.Add(this.lblAccount);
            this.fraReceipt.Controls.Add(this.lblComment);
            this.fraReceipt.Controls.Add(this.Frame1);
            this.fraReceipt.Location = new System.Drawing.Point(-6, 0);
            this.fraReceipt.Name = "fraReceipt";
            this.fraReceipt.Size = new System.Drawing.Size(909, 614);
            this.fraReceipt.TabIndex = 20;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Cursor = Wisej.Web.Cursors.Default;
            this.txtName.Location = new System.Drawing.Point(145, 19);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(329, 40);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(30, 33);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(109, 32);
            this.lblName.TabIndex = 101;
            this.lblName.Text = "NAME";
            // 
            // txtQuantity
            // 
            this.txtQuantity.BackColor = System.Drawing.SystemColors.Window;
            this.txtQuantity.Cursor = Wisej.Web.Cursors.Default;
            this.javaScript1.SetJavaScript(this.txtQuantity, resources.GetString("txtQuantity.JavaScript"));
            this.txtQuantity.Location = new System.Drawing.Point(123, 500);
            this.txtQuantity.MaxLength = 13;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(108, 40);
            this.txtQuantity.TabIndex = 9;
            this.txtQuantity.Text = "1";
            this.txtQuantity.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtQuantity.Validating += new System.ComponentModel.CancelEventHandler(this.txtMultiple_Validating);
            // 
            // chkAffectCash
            // 
            this.chkAffectCash.Checked = true;
            this.chkAffectCash.CheckState = Wisej.Web.CheckState.Checked;
            this.chkAffectCash.Location = new System.Drawing.Point(604, 507);
            this.chkAffectCash.Name = "chkAffectCash";
            this.chkAffectCash.Size = new System.Drawing.Size(108, 22);
            this.chkAffectCash.TabIndex = 99;
            this.chkAffectCash.Text = "Cash Drawer";
            // 
            // cmbResCode
            // 
            this.cmbResCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbResCode.Location = new System.Drawing.Point(617, 74);
            this.cmbResCode.Name = "cmbResCode";
            this.cmbResCode.Size = new System.Drawing.Size(98, 40);
            this.cmbResCode.Sorted = true;
            this.cmbResCode.TabIndex = 3;
            this.cmbResCode.Visible = false;
            // 
            // txtPercentageAmount
            // 
            this.txtPercentageAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtPercentageAmount.Cursor = Wisej.Web.Cursors.Default;
            this.txtPercentageAmount.Location = new System.Drawing.Point(441, 500);
            this.txtPercentageAmount.MaxLength = 13;
            this.txtPercentageAmount.Name = "txtPercentageAmount";
            this.txtPercentageAmount.Size = new System.Drawing.Size(108, 40);
            this.txtPercentageAmount.TabIndex = 10;
            this.txtPercentageAmount.Text = "0.00";
            this.txtPercentageAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPercentageAmount.Visible = false;
            this.txtPercentageAmount.Enter += new System.EventHandler(this.txtPercentageAmount_Enter);
            this.txtPercentageAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtPercentageAmount_Validating);
            this.txtPercentageAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPercentageAmount_KeyPress);
            // 
            // txtComment
            // 
            this.txtComment.BackColor = System.Drawing.SystemColors.Window;
            this.txtComment.Cursor = Wisej.Web.Cursors.Default;
            this.txtComment.Location = new System.Drawing.Point(123, 556);
            this.txtComment.MaxLength = 255;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(592, 40);
            this.txtComment.TabIndex = 11;
            this.txtComment.Enter += new System.EventHandler(this.txtComment_Enter);
            // 
            // vsFees
            // 
            this.vsFees.Cols = 3;
            this.vsFees.FixedCols = 2;
            this.vsFees.FrozenCols = 1;
            this.javaScript1.SetJavaScript(this.vsFees, resources.GetString("vsFees.JavaScript"));
            this.vsFees.Location = new System.Drawing.Point(30, 213);
            this.vsFees.Name = "vsFees";
            this.vsFees.Rows = 7;
            this.vsFees.ShowFocusCell = false;
            this.vsFees.Size = new System.Drawing.Size(685, 275);
            this.vsFees.StandardTab = false;
            this.vsFees.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsFees.TabIndex = 8;
            this.vsFees.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsFees_CellValidating);
            this.vsFees.CurrentCellChanged += new System.EventHandler(this.vsFees_CurrentCellChanged);
            // 
            // txtAcct1
            // 
            this.txtAcct1.Cols = 1;
            this.txtAcct1.ColumnHeadersVisible = false;
            this.txtAcct1.ExtendLastCol = true;
            this.txtAcct1.FixedCols = 0;
            this.txtAcct1.FixedRows = 0;
            this.txtAcct1.Location = new System.Drawing.Point(483, 156);
            this.txtAcct1.Name = "txtAcct1";
            this.txtAcct1.RowHeadersVisible = false;
            this.txtAcct1.Rows = 1;
            this.txtAcct1.ShowFocusCell = false;
            this.txtAcct1.Size = new System.Drawing.Size(232, 42);
            this.txtAcct1.TabIndex = 7;
            this.txtAcct1.Visible = false;
            // 
            // lblResCode
            // 
            this.lblResCode.Location = new System.Drawing.Point(534, 88);
            this.lblResCode.Name = "lblResCode";
            this.lblResCode.Size = new System.Drawing.Size(77, 16);
            this.lblResCode.TabIndex = 96;
            this.lblResCode.Text = "TOWN CODE";
            this.lblResCode.Visible = false;
            // 
            // lblPercentAmount
            // 
            this.lblPercentAmount.Location = new System.Drawing.Point(300, 514);
            this.lblPercentAmount.Name = "lblPercentAmount";
            this.lblPercentAmount.Size = new System.Drawing.Size(135, 16);
            this.lblPercentAmount.TabIndex = 90;
            this.lblPercentAmount.Text = "PERCENTAGE AMOUNT";
            this.lblPercentAmount.Visible = false;
            // 
            // lblMultiple
            // 
            this.lblMultiple.Location = new System.Drawing.Point(30, 514);
            this.lblMultiple.Name = "lblMultiple";
            this.lblMultiple.Size = new System.Drawing.Size(72, 16);
            this.lblMultiple.TabIndex = 89;
            this.lblMultiple.Text = "QUANTITY";
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(483, 133);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(75, 16);
            this.lblAccount.TabIndex = 34;
            this.lblAccount.Text = "ACCOUNT #";
            this.lblAccount.Visible = false;
            // 
            // lblComment
            // 
            this.lblComment.Location = new System.Drawing.Point(30, 568);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(63, 16);
            this.lblComment.TabIndex = 20;
            this.lblComment.Text = "COMMENT";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.txtReference);
            this.Frame1.Controls.Add(this.txtControl2);
            this.Frame1.Controls.Add(this.txtControl1);
            this.Frame1.Controls.Add(this.txtControl3);
            this.Frame1.Controls.Add(this.lblControl2);
            this.Frame1.Controls.Add(this.lblControl1);
            this.Frame1.Controls.Add(this.lblReference);
            this.Frame1.Controls.Add(this.lblControl3);
            this.Frame1.Location = new System.Drawing.Point(10, 65);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(489, 146);
            this.Frame1.TabIndex = 3;
            // 
            // txtReference
            // 
            this.txtReference.BackColor = System.Drawing.SystemColors.Window;
            this.txtReference.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtReference.Cursor = Wisej.Web.Cursors.Default;
            this.txtReference.Location = new System.Drawing.Point(135, 9);
            this.txtReference.MaxLength = 10;
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(329, 40);
            this.txtReference.TabIndex = 2;
            // 
            // txtControl2
            // 
            this.txtControl2.BackColor = System.Drawing.SystemColors.Window;
            this.txtControl2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtControl2.Cursor = Wisej.Web.Cursors.Default;
            this.txtControl2.Location = new System.Drawing.Point(170, 92);
            this.txtControl2.MaxLength = 10;
            this.txtControl2.Name = "txtControl2";
            this.txtControl2.Size = new System.Drawing.Size(145, 40);
            this.txtControl2.TabIndex = 5;
            // 
            // txtControl1
            // 
            this.txtControl1.BackColor = System.Drawing.SystemColors.Window;
            this.txtControl1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtControl1.Cursor = Wisej.Web.Cursors.Default;
            this.txtControl1.Location = new System.Drawing.Point(20, 92);
            this.txtControl1.MaxLength = 10;
            this.txtControl1.Name = "txtControl1";
            this.txtControl1.Size = new System.Drawing.Size(145, 40);
            this.txtControl1.TabIndex = 4;
            // 
            // txtControl3
            // 
            this.txtControl3.BackColor = System.Drawing.SystemColors.Window;
            this.txtControl3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtControl3.Cursor = Wisej.Web.Cursors.Default;
            this.txtControl3.Location = new System.Drawing.Point(322, 92);
            this.txtControl3.MaxLength = 10;
            this.txtControl3.Name = "txtControl3";
            this.txtControl3.Size = new System.Drawing.Size(145, 40);
            this.txtControl3.TabIndex = 6;
            // 
            // lblControl2
            // 
            this.lblControl2.Location = new System.Drawing.Point(170, 68);
            this.lblControl2.Name = "lblControl2";
            this.lblControl2.Size = new System.Drawing.Size(123, 17);
            this.lblControl2.TabIndex = 32;
            this.lblControl2.Text = "CONTROL2";
            // 
            // lblControl1
            // 
            this.lblControl1.Location = new System.Drawing.Point(20, 68);
            this.lblControl1.Name = "lblControl1";
            this.lblControl1.Size = new System.Drawing.Size(135, 17);
            this.lblControl1.TabIndex = 31;
            this.lblControl1.Text = "CONTROL1";
            // 
            // lblReference
            // 
            this.lblReference.Location = new System.Drawing.Point(20, 23);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(109, 32);
            this.lblReference.TabIndex = 30;
            this.lblReference.Text = "REF";
            // 
            // lblControl3
            // 
            this.lblControl3.Location = new System.Drawing.Point(322, 68);
            this.lblControl3.Name = "lblControl3";
            this.lblControl3.Size = new System.Drawing.Size(132, 17);
            this.lblControl3.TabIndex = 29;
            this.lblControl3.Text = "CONTROL3";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSave.Location = new System.Drawing.Point(454, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(188, 48);
            this.cmdSave.TabIndex = 101;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmMiscReceiptInput
            // 
            this.ClientSize = new System.Drawing.Size(920, 578);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.Name = "frmMiscReceiptInput";
            this.Text = "frmMiscReceiptInput";
            this.Load += new System.EventHandler(this.frmMiscReceiptInput_Load);
            this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmMiscReceiptInput_FormClosing);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceipt)).EndInit();
            this.fraReceipt.ResumeLayout(false);
            this.fraReceipt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAffectCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

        }

		#endregion

		public fecherFoundation.FCPanel fraReceipt;
		public fecherFoundation.FCGrid txtAcct1;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCComboBox cmbResCode;
		public fecherFoundation.FCTextBox txtPercentageAmount;
		public fecherFoundation.FCTextBox txtComment;
		public fecherFoundation.FCGrid vsFees;
		public fecherFoundation.FCLabel lblResCode;
		public fecherFoundation.FCLabel lblPercentAmount;
		public fecherFoundation.FCLabel lblMultiple;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtReference;
		public fecherFoundation.FCTextBox txtControl2;
		public fecherFoundation.FCTextBox txtControl1;
		public fecherFoundation.FCTextBox txtControl3;
		public fecherFoundation.FCLabel lblControl2;
		public fecherFoundation.FCLabel lblControl1;
		public fecherFoundation.FCLabel lblReference;
		public fecherFoundation.FCLabel lblControl3;
		public fecherFoundation.FCCheckBox chkAffectCash;
		private Wisej.Web.ToolTip toolTip1;
		public fecherFoundation.FCTextBox txtQuantity;
		private Wisej.Web.JavaScript javaScript1;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCLabel lblName;
	}
}