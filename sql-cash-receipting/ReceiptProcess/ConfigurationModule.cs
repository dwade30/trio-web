﻿using Autofac;
using SharedApplication;
using SharedApplication.CashReceipts;

namespace TWCR0000.ReceiptProcess
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmChooseTransactionType>().As <IView<IChooseTransactionTypeViewModel>>();

        }
    }
}