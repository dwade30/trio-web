﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using System.Linq;
using GrapeCity.ActiveReports.SectionReportModel;
using SharedApplication.CashReceipts;
using SharedApplication.Receipting;
using TWSharedLibrary;

namespace TWCR0000.ReceiptProcess
{
	/// <summary>
	/// Summary description for sarGenericReceiptDetail.
	/// </summary>
	public partial class sarNewGenericReceiptDetail : BaseSectionReport
	{
		
		float[] lngLeftCol = new float[5 + 1];
		float lngTotalWidth;
		int lngCurrentPayment;
		float[] lngTop = new float[6 + 1];
		int intRowsUsed;
		float lngFieldWid;
		string strNarrowInfo = "";
		clsDRWrapper rsBatch = new clsDRWrapper();
		bool boolOpened;
		int lngReceiptArrayIndex;
		float lngLineHeight;


		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private string townAbbreviation;
		private string comment;
		private string paidBy;
		private string collectionCode;
		private int wideFontSize;
		private ReceiptDetailsUserData receiptDetailsUserData;
		private bool blnFirstRecord = true;
		public sarNewGenericReceiptDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

		}

		public sarNewGenericReceiptDetail(GlobalCashReceiptSettings globalCashReceiptSettings, string townAbbreviation, string paidBy, int wideFontSize) : this()
		{
			this.globalCashReceiptSettings = globalCashReceiptSettings;
			this.townAbbreviation = townAbbreviation;
			this.paidBy = paidBy;
			this.wideFontSize = wideFontSize;

			comment = "";
			collectionCode = "";


		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			receiptDetailsUserData = (ReceiptDetailsUserData)this.UserData;
			var result = receiptDetailsUserData.transaction.ToReceiptDetailItem().TransactionSpecificDetails.FirstOrDefault(x => x.Title == "Comment");
			if (result.Value != null)
			{
				comment = result.Value;
			}

			result = receiptDetailsUserData.transaction.ToReceiptDetailItem().TransactionSpecificDetails.FirstOrDefault(x => x.Title == "Collection Code");
			if (result.Value != null)
			{
				collectionCode = result.Value;
			}

			lngLineHeight = 250 / 1440F;
			lngTop[0] = 0;
			lngTop[1] = lngLineHeight * 1;
			lngTop[2] = lngLineHeight * 2;
			lngTop[3] = lngLineHeight * 3;
			lngTop[4] = lngLineHeight * 4;
			lngTop[5] = lngLineHeight * 5;
			lngTop[6] = lngLineHeight * 6;
			SetArraySizes();
			SetupReceiptFormat();
			
			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				SetReceiptFontSize();
			}

            for (int i = 1; i <= 7; i++)
            {
                GrapeCity.ActiveReports.SectionReportModel.Label obNew;
                string strTemp;
                obNew = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(i));
                obNew.Visible = false;
            }
        }

		private void SetArraySizes()
		{
			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				// wide format
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 1170 / 1440F;
				lngLeftCol[2] = 2880 / 1440F;
				lngLeftCol[3] = 3060 / 1440F;
				lngLeftCol[4] = 4950 / 1440F;
				lngLeftCol[5] = 5130 / 1440F;
				lngTotalWidth = 7215 / 1440F;
				lngFieldWid = 2880 / 1440F;
				PageSettings.Margins.Left = 1440 / 1440F;
				PageSettings.Margins.Right = 1440 / 1440F;
			}
			else
			{
				// narrow format
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 100 / 1440F;
				lngLeftCol[2] = 0;
				lngLeftCol[3] = 200 / 1440F;
				lngLeftCol[4] = 500 / 1440F;
				lngLeftCol[5] = 300 / 1440F;
				lngTotalWidth = 5400 / 1440F;
				lngFieldWid = 1440 / 1440F;
				PageSettings.Margins.Left = 0;
				PageSettings.Margins.Right = 0;
			}
		}

		private void SetupReceiptFormat()
		{
			fldName.Left = lngLeftCol[0];
			lblComment.Left = lngLeftCol[0];

			this.PrintWidth = lngTotalWidth;
			fldNarrow.Width = lngTotalWidth;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			bool boolBatchDone;
			int intCT;
			bool boolREType = false;
			int intCommentLine = 0;

			if (receiptDetailsUserData.transaction != null)
			{
				intRowsUsed = 6;

				if (comment == "")
				{
					lblComment.Top = lngTop[0];
					lblComment.Visible = false;
					intRowsUsed -= 1;
				}
				else
				{
					lblComment.Visible = true;
					lblComment.Top = lngTop[2];
					intCommentLine = 2;
					lblComment.Text = new string(' ', globalCashReceiptSettings.ReceiptOffset) + "   " + comment.PadRight(37 - globalCashReceiptSettings.ReceiptOffset);
				}
				
				if (receiptDetailsUserData.transaction.ToReceiptSummaryItem().Name == paidBy)
				{
					intRowsUsed -= 1;
					fldNarrow.Top = lngTop[0];
					fldName.Text = "";
					fldName.Visible = false;
					if (Strings.Trim(lblComment.Text) != "")
					{
						lblComment.Top = lngTop[1];
						intCommentLine = 2;
					}
				}
				else
				{
					fldName.Visible = true;
					fldName.Top = 0;
					fldName.Text = "   **" + modGlobalFunctions.PadStringWithSpaces(modUseCR.Statics.ReceiptArray[lngReceiptArrayIndex].Name, 37, false);
					fldNarrow.Top = lngTop[1];
				}

				fldNarrow.Visible = true;
				if (receiptDetailsUserData.ShowFeeDetails)
				{
					strNarrowInfo = receiptDetailsUserData.transaction.ToReceiptSummaryItem().TypeDescription.PadRight(16)  + receiptDetailsUserData.transaction.Reference.PadRight(10);
				}
				else
				{
					strNarrowInfo = receiptDetailsUserData.transaction.ToReceiptSummaryItem().TypeDescription.PadRight(16) + receiptDetailsUserData.transaction.Reference.PadRight(9) + receiptDetailsUserData.transaction.Total.ToString("#,##0.00").PadRight(11);
				}

				fldNarrow.Text = strNarrowInfo;

				if (!globalCashReceiptSettings.ShowControlFields)
				{
					fldControlDetail1.Top = lngTop[0];
					fldControlDetail1.Visible = false;
					fldControlDetail2.Top = lngTop[0];
					fldControlDetail2.Visible = false;
					fldControlDetail3.Top = lngTop[0];
					fldControlDetail3.Visible = false;
					intRowsUsed -= 3;
				}
				else
				{
					FillControlFields(intCommentLine);
				}

				if (receiptDetailsUserData.ShowFeeDetails)
				{
					foreach (var amount in receiptDetailsUserData.transaction.TransactionAmounts)
					{
						if (amount.Total != 0)
						{
							AddFeeRow(amount.Description, amount.Total, intRowsUsed * lngLineHeight, intRowsUsed);
							intRowsUsed += 1;
						}
					}
				}

				Detail.Height = intRowsUsed * lngLineHeight;
			}
		}

		private void AddFeeRow(string strFeeDesc, decimal dblFee, float lngTop, int intNum, bool blnNoIndent = false)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			string strTemp;
			string strCaption;
			int intSpaces = 0;

			obNew = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(intNum));
			obNew.Visible = true;
			obNew.Top = lngTop;

			if (!blnNoIndent)
			{
				obNew.Left = fldNarrow.Left + 200 / 1440F;
			}
			else
			{
				obNew.Left = fldNarrow.Left;
			}
			obNew.Width = this.PrintWidth - 200 / 1440F;
			obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			obNew.Font = fldNarrow.Font;
			// this sets the font to the same as the field that is already created
			if (!blnNoIndent)
			{
				obNew.Text = strFeeDesc.PadRight(20) + dblFee.ToString("#,##0.00").PadRight(15);
			}
			else
			{
				obNew.Text = strFeeDesc.PadRight(20) + dblFee.ToString("#,##0.00").PadRight(17);
			}
		}

		private void SetReceiptFontSize()
		{
			try
			{
				if (wideFontSize <= 0)
				{
					return;
				}
				fldName.Font = new Font(fldName.Font.Name, wideFontSize);
				fldNarrow.Font = new Font(fldNarrow.Font.Name, wideFontSize);
				lblComment.Font = new Font(lblComment.Font.Name, wideFontSize);
				fldControlDetail1.Font = new Font(fldControlDetail1.Font.Name, wideFontSize);
				fldControlDetail2.Font = new Font(fldControlDetail2.Font.Name, wideFontSize);
				fldControlDetail3.Font = new Font(fldControlDetail3.Font.Name, wideFontSize);
			}
			catch (Exception ex)
			{

			}
		}

		private void ShowControlInformation(int controlLinesShown, int startLine, ControlItem controlItem)
		{
			TextBox control = null;
			switch (controlLinesShown)
			{
				case 0:
					control = fldControlDetail1;
					break;
				case 1:
					control = fldControlDetail2;
					break;
				case 2:
					control = fldControlDetail3;
					break;
			}

			control.Visible = true;
			control.Text = controlItem.Description + ":" + controlItem.Value;
			fldControlDetail1.Top = lngTop[startLine + controlLinesShown + 1];
		}

		private void FillControlFields(int intCommentLine)
		{
			int controlLinesShown = 0;

			fldControlDetail1.Visible = false;
			fldControlDetail2.Visible = false;
			fldControlDetail3.Visible = false;

			foreach (var controlItem in receiptDetailsUserData.transaction.Controls)
			{
				ShowControlInformation(controlLinesShown, intCommentLine, controlItem);
				controlLinesShown++;
			}
		}
	}
}
