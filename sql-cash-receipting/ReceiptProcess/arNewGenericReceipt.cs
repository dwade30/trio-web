﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using System.Drawing;
using System.Linq;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using TWSharedLibrary;
using Label = GrapeCity.ActiveReports.SectionReportModel.Label;

namespace TWCR0000.ReceiptProcess
{
	/// <summary>
	/// Summary description for arGenericReceipt.
	/// </summary>
	public partial class arNewGenericReceipt : BaseSectionReport
	{
		
		float[] lngLeftCol = new float[5 + 1];
		// this is an array of the column lefts used in the report
		// it will help with wide and narrow formats
		float lngTotalWidth;
		// total width of the report
		int intPaymentNumber;
		// this is the payment number in the grid
		int intTag;
		string strNarrowHeader = "";
		string strNarrowReceipt;
		string strNarrowTotal = "";
		int lngNumOfRows;
		int intIndex;
		bool boolTextExport;
		bool boolSmallVersion;
		bool bool1STPage;
		int lngMultiTown;
		bool blnShowSigLine;

		private IEnumerable<TransactionBase> transactions;
		private string clientName;
		private decimal convenienceFee;
		private bool reprint;
		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private IEnumerable<ReceiptPayment> payments;
		private string paidBy;
		private bool isBatchPayment = false;
		private ISettingService settingService;
		private int receiptNumber;
		private decimal change;
		private bool firstCopy;
		private string tellerId;
		private DateTime receiptDate;
		private IReceiptTypesService receiptTypesService;
		private string townAbbreviation;
		private string cashReceiptPrinter;
		private TransactionBase currentTransaction;

		public arNewGenericReceipt()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

		}

		public arNewGenericReceipt(IEnumerable<TransactionBase> transactions, IEnumerable<ReceiptPayment> payments,
			string clientName, decimal convenienceFee, bool reprint,
			GlobalCashReceiptSettings globalCashReceiptSettings, string paidBy,
			ISettingService settingService, int receiptNumber, decimal change, bool firstCopy, string tellerId,
			DateTime receiptDate, IReceiptTypesService receiptTypesService, string townAbbreviation,
			string cashReceiptPrinter) : this()
		{
			try
			{
				this.transactions = transactions;
				this.payments = payments;
				this.clientName = clientName;
				this.convenienceFee = convenienceFee;
				this.reprint = reprint;
				this.globalCashReceiptSettings = globalCashReceiptSettings;
				this.paidBy = paidBy;
				this.settingService = settingService;
				this.receiptNumber = receiptNumber;
				this.change = change;
				this.firstCopy = firstCopy;
				this.tellerId = tellerId;
				this.receiptDate = receiptDate;
				this.receiptTypesService = receiptTypesService;
				this.townAbbreviation = townAbbreviation;
				this.cashReceiptPrinter = cashReceiptPrinter;

				isBatchPayment = (this.transactions.FirstOrDefault()?.Split ?? 0) > 5;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void StartUp()
		{
			this.ReportFooter.Height = 3000 / 1440F + (globalCashReceiptSettings.ReceiptExtraLines * 550) / 1440F;
			lblBottomLine.Top = this.ReportFooter.Height - 250 / 1440F;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			sarReceiptDetailOb.Report = new sarNewGenericReceiptDetail();
			intPaymentNumber = 0;
			StartUp();
			SetMainLabels();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				if (intPaymentNumber >= transactions.Count())
				{
					eArgs.EOF = true;
				}
				else
				{
					currentTransaction = transactions.ToList()[intPaymentNumber];
					eArgs.EOF = false;
				}

				intPaymentNumber += 1;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Fetch Data ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetArraySizes()
		{
			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 1170 / 1440F;
				lngLeftCol[2] = 1170 / 1440F;
				lngLeftCol[3] = 3060 / 1440F;
				lngLeftCol[4] = 4950 / 1440F;
				lngLeftCol[5] = 5130 / 1440F;
				lngTotalWidth = 7215 / 1440F;
				this.PageSettings.PaperHeight = 255 * globalCashReceiptSettings.ReceiptLength / 1440F;
				PageSettings.Margins.Left = 1440 / 1440F;
				PageSettings.Margins.Right = 1440 / 1440F;
				PageSettings.Margins.Top = 1440 / 1440F;
				if (globalCashReceiptSettings.ReceiptLength != 66)
				{
                    this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("TWCRReceipt22", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
                }
			}
			else
			{
				// Printer.RenderMode = 1
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 100 / 1440F;
				lngLeftCol[2] = 0;
				lngLeftCol[3] = 200 / 1440F;
				lngLeftCol[4] = 1200 / 1440F;
				lngLeftCol[5] = 300 / 1440F;
				lngTotalWidth = 5400 / 1440F;
				PageSettings.Margins.Top = 720 / 1440F;
				PageSettings.Margins.Left = 0;
				PageSettings.Margins.Right = 0;
				strNarrowHeader = "";
				strNarrowReceipt = "";
				strNarrowTotal = "";
			}
        }

        private void SetMainLabels()
		{
			
			lblHeader.Text = "-----  R e c e i p t  -----";
			lblTopComment.Text = globalCashReceiptSettings.TopComment;
			// 3nd line
			lblTime.Text = receiptDate.ToString("h:mm tt");
			strNarrowReceipt = receiptDate.ToString("MM/dd/yy").PadRight(9);
			strNarrowReceipt += receiptDate.ToString("h:mm tt").PadRight(9);
			strNarrowReceipt += tellerId.PadRight(5);
			strNarrowReceipt += ("#" + receiptNumber).PadRight(8) + "-" + transactions.FirstOrDefault()?.Split;
			strNarrowHeader = "TYPE------".PadRight(17) + "REF---".PadRight(10) + "AMOUNT".PadRight(10);
			strNarrowTotal = isBatchPayment ? "": "              Total :" + transactions.Select(x => x.ToReceiptSummaryItem()).Sum(a => a.Amount).ToString("#,##0.00").PadRight(15) + "*";

			lblNarrowTitles.Width = this.PrintWidth;
			lblNarrowTitles.Text = strNarrowHeader;
			lblNarrowTitles.Visible = true;
			lblMuniName.Text = clientName;

			lblDate.Text = receiptDate.ToString("MM/dd/yyyy");
			lblReceiptNumber.Text = "Receipt #" + receiptNumber;

			lblTeller.Text = reprint || globalCashReceiptSettings.TellerIdUsage == TellerIDUsage.NotRequired ? "" : "Teller: " + tellerId;
			lblReprint.Visible = reprint;

			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				// wide
				fldTotal.Visible = true;
				lblTotal.Visible = true;
				lblTotal.Text = "Total: ";
			}
			else
			{
				// narrow
				lblTotal.Text = strNarrowTotal;
				lblTotal.Visible = true;
				fldTotal.Visible = false;
			}

			if (isBatchPayment)
			{
				lblOtherAccounts.Visible = false;
				lblPaidBy.Top = 450 / 1440F;
				lblBottomComment.Top = 810 / 1440F;
			}

			lblHeaderType.Text = " ";
			lblHeaderRef.Text = " ";
			lblHeaderAmt.Text = " ";
			if (!isBatchPayment)
			{
				fldTotal.Text = transactions.Select(x => x.ToReceiptSummaryItem()).Sum(a => a.Amount).ToString("#,##0.00") + "*";
			}
			lblBottomComment.Text = globalCashReceiptSettings.BottomComment;
		}

        private decimal TotalCashPaid
        {
	        get { return payments.Where(p => p.PaymentType == PaymentMethod.Cash).Sum(p => p.Amount); }
        }

        private decimal TotalCheckPaid
        {
	        get { return payments.Where(p => p.PaymentType == PaymentMethod.Check).Sum(p => p.Amount); }
        }

        private decimal TotalCreditPaid
        {
	        get { return payments.Where(p => p.PaymentType == PaymentMethod.Credit).Sum(p => p.Amount); }
        }
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			bool boolFound;
			boolFound = false;
			if (globalCashReceiptSettings.PrintSignature == PrintSignatureOption.Yes)
			{
				blnShowSigLine = true;
			}
			else if (globalCashReceiptSettings.PrintSignature == PrintSignatureOption.OnlyOnCreditCardPayments)
			{
				blnShowSigLine = TotalCreditPaid != 0;
			}
			else
			{
				blnShowSigLine = false;
			}
			SetArraySizes();

			if (cashReceiptPrinter == "")
			{
				MessageBox.Show("No Receipt Printer is setup, please check the setup of your receipt printer in the printer setup on General Entry.", "Printer Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}

			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				SetReceiptFontSize();
			}

			lblPaidBy.Text =  "Paid By: " + paidBy;

			lblHeader.Visible = true;
			lblHeaderAmt.Text = " ";
			lblHeaderRef.Text = " ";
			lblHeaderType.Text = " ";

            for (int i = 1; i <= 5; i++)
            {
                GrapeCity.ActiveReports.SectionReportModel.Label obNew;
                obNew = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(i));
                obNew.Visible = false;
            }

			var controlsNeeded = payments.Count() + (convenienceFee != 0 ? 1 : 0);
			for (int counter = 1; counter <= controlsNeeded; counter++)
			{
				GrapeCity.ActiveReports.SectionReportModel.Label obNew = ReportFooter.AddControlWithName<Label>("lblCheck" + FCConvert.ToString(counter));
				obNew.Visible = false;
			}
		}
		
		private void Detail_AfterPrint(object sender, EventArgs e)
		{
			sarReceiptDetailOb.Report = null;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				var wideFontSize = settingService.GetSettingValue( SettingOwnerType.User,"CRWideFontSize");
				sarReceiptDetailOb.Report = new sarNewGenericReceiptDetail(globalCashReceiptSettings, townAbbreviation, paidBy, wideFontSize == null ? 0 : wideFontSize.SettingValue.ToIntegerValue());

				//var showFeeDetail = receiptTypesService.GetReceiptTypeByCode(currentTransaction.TransactionTypeCode.ToIntegerValue()).ShowFeeDetailOnReceipt ?? false;
                var showFeeDetail = receiptTypesService.GetReceiptTypeFromTransaction(currentTransaction).ShowFeeDetailOnReceipt ?? false;
                var receiptDetailsUserData = new ReceiptDetailsUserData
				{
					transaction = currentTransaction,
					ShowFeeDetails = showFeeDetail
				};

				sarReceiptDetailOb.Report.UserData = receiptDetailsUserData;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddFooterFeeRow(string strFeeDesc, decimal dblFee, float lngTop, int intNum)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			string strTemp;
            obNew = ReportFooter.Controls["lblFeeD" + FCConvert.ToString(intNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
            obNew.Visible = true;
			obNew.Top = lngTop;
			obNew.Left = (this.PrintWidth / 4);
			obNew.Width = FCConvert.ToSingle(this.PrintWidth * 0.667);
			obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			obNew.Font = fldTotal.Font;

			if (dblFee != 0)
			{
				obNew.Text = strFeeDesc.PadRight(7) + ":" + dblFee.ToString("#,##0.00").PadRight(13);
			}
			else
			{
				obNew.Text = " ";
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			int intCT;
			// this will count all the rows used  in the footer
			float lngStart;
			string strTemp = "";
			float lngFieldHeight;
			string strRegValue = "";
			int intXLines = 0;
			lngFieldHeight = 240 / 1440f;

			fldTotal.Top = 0;
			lblTotal.Top = 0;
			if (!blnShowSigLine)
			{
				lblSignature.Visible = false;
				lblSignatureLine.Visible = false;
				lblSignature.Top = 0;
				lblSignatureLine.Top = 0;
			}

			if (reprint || isBatchPayment)
			{
				if (lngNumOfRows < 2 && !modGlobal.Statics.gboolDetail)
				{
					lblTotal.Text = "     ";
				}
				fldTotal.Visible = false;
				lblPaidBy.Top = 200 / 1440f;
				lblOtherAccounts.Top = 450 / 1440f;
				if (blnShowSigLine)
				{
					lblSignature.Top = 810 / 1440f;
					lblSignatureLine.Top = 1310 / 1440f;
					lblBottomComment.Top = lblSignatureLine.Top + lblSignatureLine.Height + 200 / 1440f;
				}
				else
				{
					lblBottomComment.Top = 810 / 1440f;
				}
			}
			else
			{
                //var showFeeDetail =
                //	receiptTypesService
                //		.GetReceiptTypeByCode(transactions.FirstOrDefault().TransactionTypeCode.ToIntegerValue())
                //		.ShowFeeDetailOnReceipt ?? false;
                var showFeeDetail =
                    receiptTypesService
                        .GetReceiptTypeFromTransaction(transactions.FirstOrDefault())
                        .ShowFeeDetailOnReceipt ?? false;
				//strTemp = modUseCR.GetInfoAboutOtherAccounts(transactions, firstCopy, reprint, isBatchPayment);
				strTemp = StaticSettings.GlobalCommandDispatcher.Send(new GetReceiptRemainingBalanceText
                {
                    Transactions = transactions,
                    IsFirstCopy = firstCopy,
                    IsReprint = reprint,
                    IsBatch = isBatchPayment
                }).Result;
				if (transactions.Count() == 1 && convenienceFee == 0 && !showFeeDetail)
				{
					// one entry
					if (Strings.Left(strTemp, 3) != "(0)" && strTemp != "")
					{
						// outstanding account balances
						lblTotal.Text = "   ";
						fldTotal.Visible = false;
						lblPaidBy.Top = 450 / 1440f;
						lblOtherAccounts.Top = 810 / 1440f;
						if (blnShowSigLine)
						{
							lblSignature.Top = 1080 / 1440f;
							lblSignatureLine.Top = 1580 / 1440f;
							lblBottomComment.Top = lblSignatureLine.Top + lblSignatureLine.Height + 200 / 1440f;
						}
						else
						{
							lblBottomComment.Top = 1080 / 1440f;
						}
					}
					else
					{
						// no other accounts/balances
						lblOtherAccounts.Visible = false;
						lblTotal.Text = "      ";
						fldTotal.Visible = false;
						lblPaidBy.Top = 200 / 1440f;
						if (blnShowSigLine)
						{
							lblSignature.Top = 450 / 1440f;
							lblSignatureLine.Top = 950 / 1440f;
							lblBottomComment.Top = lblSignatureLine.Top + lblSignatureLine.Height + 200 / 1440f;
						}
						else
						{
							lblBottomComment.Top = 450 / 1440f;
						}
					}
				}
				else
				{
					// more than one entry
					if (Strings.Left(strTemp, 3) != "(0)")
					{
						// outstanding account balances
					}
					else
					{
						// no other accounts/balances
						lblOtherAccounts.Visible = false;
						lblPaidBy.Top = 450 / 1440f;
						if (blnShowSigLine)
						{
							lblSignature.Top = 810 / 1440f;
							lblSignatureLine.Top = 1310 / 1440f;
							lblBottomComment.Top = lblSignatureLine.Top + lblSignatureLine.Height + 200 / 1440f;
						}
						else
						{
							lblBottomComment.Top = 810 / 1440f;
						}
					}
				}
			}

			intCT = 0;
			lngStart = lblBottomComment.Top + 360 / 1440f;
			// footer
			if (reprint)
			{
				lblPaidBy.Text = new string(' ', globalCashReceiptSettings.ReceiptOffset) + "Paid By: " +
				                 paidBy.Trim();

				lblReprint.Text = "***  REPRINT  ***";
				lblOtherAccounts.Text = "***  REPRINT  ***";

				var lastReceiptSetting = settingService.GetSettingValue(SettingOwnerType.Machine,"CRLastReceiptNumber");

				if (lastReceiptSetting != null)
				{
					if (lastReceiptSetting.SettingValue.ToIntegerValue() == receiptNumber)
					{
						var lastReceiptDetailsSetting = settingService.GetSettingValue(SettingOwnerType.Machine, "CRLastRemainingBalance");

						if (lastReceiptDetailsSetting != null)
						{
							lblOtherAccounts.Text = lastReceiptDetailsSetting.SettingValue;
						}
					}
				}
			}
			else
			{
				lblOtherAccounts.Text = strTemp + " ";
				settingService.SaveSetting(SettingOwnerType.Machine, "CRLastReceiptNumber", lblReceiptNumber.Text);
				settingService.SaveSetting(SettingOwnerType.Machine, "CRLastRemainingBalance", lblOtherAccounts.Text);
			}

			if (TotalCashPaid != 0)
			{
				AddFooterFeeRow("Cash", TotalCashPaid, lngStart, 1);
				intCT += 1;
			}
			if (TotalCheckPaid != 0)
			{
				// Check
				AddFooterFeeRow("Check", TotalCheckPaid, lngStart + (lngFieldHeight * intCT), 2);
				intCT += 1;
			}
			if (TotalCreditPaid != 0)
			{
				// Credit Card
				AddFooterFeeRow("Credit/Debit", TotalCreditPaid, lngStart + (lngFieldHeight * intCT), 3);
				intCT += 1;
			}
			if (change != 0)
			{
				// Change
				AddFooterFeeRow("Change", change, lngStart + (lngFieldHeight * intCT), 4);
				intCT += 1;
			}

			lblCheck1.Top = lngStart + (lngFieldHeight * intCT);
			intXLines += globalCashReceiptSettings.ReceiptExtraLines + AddCheckNumbers();
			if (globalCashReceiptSettings.NarrowReceipt)
			{
				ReportFooter.Height = lngStart + (180 / 1440f * (intCT + intXLines)) + 360 / 1440f;
				AddFooterFeeRow(" ", 0, lngStart + (180 / 1440f * (intCT + intXLines)), 5);
			}
			else
			{
				ReportFooter.Height = lngStart + (180 / 1440f * intCT) + 360 / 1440f;
				AddFooterFeeRow(" ", 0, lngStart + (180 / 1440f * intCT), 5);
			}
			lblBottomLine.Top = this.ReportFooter.Height - 250 / 1440f;
		}

		private Label GetCheckNumberLabel(int counter)
		{
			if (counter == 1)
			{
				lblCheck1.Visible = true;
				return lblCheck1;
			}
			else
			{
				var obNew = ReportFooter.Controls["lblCheck" + FCConvert.ToString(counter)] as GrapeCity.ActiveReports.SectionReportModel.Label;
				obNew.Visible = true;
				obNew.Top = lblCheck1.Top + ((counter - 1) * lblCheck1.Height);
				obNew.Left = lblCheck1.Left;
				obNew.Width = lblCheck1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				obNew.Font = lblCheck1.Font;
				obNew.MultiLine = false;
				obNew.WordWrap = false;
				return obNew;
			}
		}

		private int AddCheckNumbers()
		{
			try
			{
				Label obNew;
				int counter = 1;
				foreach (var payment in payments.Where(x => x.PaymentType == PaymentMethod.Check))
				{
					obNew = GetCheckNumberLabel(counter);
					obNew.Text = "Check #" + ((CheckReceiptPayment)payment).CheckNumber.ToString().PadRight(10) + " -" + payment.Amount.ToString("#,##0.00").PadLeft(15);
					counter += 1;
				}

				foreach (var payment in payments.Where(x => x.PaymentType == PaymentMethod.Credit))
				{
					obNew = GetCheckNumberLabel(counter);
					obNew.Text = ((CreditCardReceiptPayment)payment).TypeDescription + " -" + payment.Amount.ToString("#,##0.00").PadLeft(13);
					counter += 1;
				}

				if (convenienceFee != 0)
				{
					obNew = GetCheckNumberLabel(counter);

					if (globalCashReceiptSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort)
					{
						obNew.Text = "Portal Charge  -" + convenienceFee.ToString("#,##0.00").PadLeft(13);
					}
					else
					{
						obNew.Text = "Convenience Fee  -" + convenienceFee.ToString("#,##0.00").PadLeft(13);
					}
				}

				return counter - 1;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Check Numbers", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}

			return 0;
		}

		private void SetReceiptFontSize()
		{
			try
			{
				int lngFSize;

				var wideFontSize = settingService.GetSettingValue(SettingOwnerType.User, "CRWideFontSize");
				lngFSize = wideFontSize == null ? 0 : wideFontSize.SettingValue.ToIntegerValue();
				if (lngFSize <= 0)
				{
					return;
				}
				lblMuniName.Font = new Font(lblMuniName.Font.Name, lngFSize);
				lblHeader.Font = new Font(lblHeader.Font.Name, lngFSize);
				lblTopComment.Font = new Font(lblTopComment.Font.Name, lngFSize);
				lblReprint.Font = new Font(lblReprint.Font.Name, lngFSize);
				lblTeller.Font = new Font(lblTeller.Font.Name, lngFSize);
				lblDate.Font = new Font(lblDate.Font.Name, lngFSize);
				lblTime.Font = new Font(lblTime.Font.Name, lngFSize);
				lblReceiptNumber.Font = new Font(lblReceiptNumber.Font.Name, lngFSize);
				lblHeaderAmt.Font = new Font(lblHeaderAmt.Font.Name, lngFSize);
				lblHeaderType.Font = new Font(lblHeaderType.Font.Name, lngFSize);
				lblHeaderRef.Font = new Font(lblHeaderRef.Font.Name, lngFSize);
				lblTotal.Font = new Font(lblTotal.Font.Name, lngFSize);
				fldTotal.Font = new Font(fldTotal.Font.Name, lngFSize);
				lblPaidBy.Font = new Font(lblPaidBy.Font.Name, lngFSize);
				lblOtherAccounts.Font = new Font(lblOtherAccounts.Font.Name, lngFSize);
				lblBottomComment.Font = new Font(lblBottomComment.Font.Name, lngFSize);
			}
			catch (Exception ex)
			{

			}
		}
	}
}
