﻿using System;
using Global;
using SharedApplication;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWCR0000.ReceiptProcess
{
    public partial class frmReceiptTransactionDetail : BaseForm, IView<IReceiptTransactionDetailViewModel>
    {
        public frmReceiptTransactionDetail()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += FrmReceiptTransactionDetail_Load;
        }

        private void FrmReceiptTransactionDetail_Load(object sender, EventArgs e)
        {
            ShowDetails();
        }

        public frmReceiptTransactionDetail(IReceiptTransactionDetailViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        public IReceiptTransactionDetailViewModel ViewModel { get; set; }

        public  void Show()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            if (!this.Visible)
            {
                //FC:FINAL:MSH - i.issue #1839: sometimes new modal window can be overlapped by already opened modal window
                this.BringToFront();
                base.Show(FormShowEnum.Modeless,"",null);
            }
        }

        private void ShowDetails()
        {
            ShowPanel1Details();
            ShowPanel2Details();
        }
        private void ShowPanel1Details()
        {
            flexLayoutPanel1.Controls.Add(GetControl("Type", GetTypeDescription(ViewModel.DetailItem.TypeCode, ViewModel.DetailItem.TypeDescription)));
            if (!String.IsNullOrWhiteSpace(ViewModel.DetailItem.Reference))
            {
                flexLayoutPanel1.Controls.Add(GetControl("Reference", ViewModel.DetailItem.Reference));
            }
            foreach (var control in ViewModel.DetailItem.Controls)
            {
                flexLayoutPanel1.Controls.Add(GetControl(control.Description,control.Value));
            }
            flexLayoutPanel1.Controls.Add(GetControl(" "," "));
            decimal total = 0;
            foreach (var transactionAmount in ViewModel.DetailItem.TransactionAmounts)
            {
                flexLayoutPanel1.Controls.Add( GetControl(transactionAmount.Description, transactionAmount.Total.FormatAsMoney()));
                total = total + transactionAmount.Total;
            }
            flexLayoutPanel1.Controls.Add(GetControl("Total", total.FormatAsMoney()));
        }

        private void ShowPanel2Details()
        {
            foreach (var detail in ViewModel.DetailItem.TransactionSpecificDetails)
            {
                if (!String.IsNullOrWhiteSpace(detail.Value))
                {
                    flexLayoutPanel2.Controls.Add(GetControl(detail.Title,detail.Value));
                }
            }
        }

        private TableLayoutPanel GetControl(string description, string value)
        {
            var panel = new TableLayoutPanel();
            panel.ColumnCount = 2;
            panel.RowCount = 1;
            panel.AutoSize = true;
            panel.Dock = DockStyle.Left & DockStyle.Right;
            //panel.ColumnStyles[0].SizeType = SizeType.Percent;
            //panel.ColumnStyles[0].Width = 50;
            //panel.ColumnStyles[1].SizeType = SizeType.Percent;
            //panel.ColumnStyles[1].Width = 50;
            //panel.Anchor = AnchorStyles.Left &  AnchorStyles.Right & AnchorStyles.Top;
            panel.Controls.Add(new Label(){Text = description,AutoSize = false, Width = 120, Dock = DockStyle.Fill});
            panel.Controls.Add(new Label(){Text = value, AutoSize =  true,Dock = DockStyle.Fill});

            return panel;
        }

        private string GetTypeDescription(string typeCode, string typeDescription)
        {
            var description = "";
            var separator = "";
            if (!String.IsNullOrWhiteSpace(typeCode))
            {
                description = typeCode;
                separator = " - ";
            }

            return description + separator + typeDescription;

        }
    }
}
