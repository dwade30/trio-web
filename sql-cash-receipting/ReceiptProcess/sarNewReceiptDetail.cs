﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using System.Linq;
using System.Windows.Forms.VisualStyles;
using GrapeCity.ActiveReports.SectionReportModel;
using SharedApplication.CashReceipts;
using SharedApplication.Extensions;
using SharedApplication.Receipting;

namespace TWCR0000.ReceiptProcess
{
	/// <summary>
	/// Summary description for sarReceiptDetail.
	/// </summary>
	public partial class sarNewReceiptDetail : FCSectionReport
	{
		float[] lngLeftCol = new float[5 + 1];
		float lngTotalWidth;
		float[] lngTop = new float[6 + 1];
		int intRowsUsed;
		float lngFieldWid;
		string strNarrowInfo = "";
		bool boolOpened;
		int lngReceiptArrayIndex;
		float lngLineHeight;

		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private string townAbbreviation;
		private string comment;
		private string paidBy;
		private string collectionCode;
		private int wideFontSize;
		private ReceiptDetailsUserData receiptDetailsUserData;
		private bool blnFirstRecord = true;
		public sarNewReceiptDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public sarNewReceiptDetail(GlobalCashReceiptSettings globalCashReceiptSettings, string townAbbreviation, string paidBy, int wideFontSize) : this()
		{
			this.globalCashReceiptSettings = globalCashReceiptSettings;
			this.townAbbreviation = townAbbreviation;
			this.paidBy = paidBy;
			this.wideFontSize = wideFontSize;

			comment = "";
			collectionCode = "";
			

		}	

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			receiptDetailsUserData = (ReceiptDetailsUserData)this.UserData;
            if (receiptDetailsUserData == null)
            {
                return;
            }

            var receiptDetailItem = receiptDetailsUserData.transaction.ToReceiptDetailItem();

            var result = receiptDetailItem.TransactionSpecificDetails.FirstOrDefault(x => x.Title == "Comment");
			if (result.Value != null)
			{
				comment = result.Value;
			}

			result = receiptDetailItem.TransactionSpecificDetails.FirstOrDefault(x => x.Title == "Collection Code");
			if (result.Value != null)
			{
				collectionCode = result.Value;
			}
			SetArraySizes();
			SetupReceiptFormat();
			lngLineHeight = 270 / 1440F;
			lngTop[0] = 0;
			lngTop[1] = 270 / 1440F;
			lngTop[2] = 540 / 1440F;
			lngTop[3] = 810 / 1440F;
			lngTop[4] = 1080 / 1440F;
			lngTop[5] = 1350 / 1440F;
			lngTop[6] = 1620 / 1440F;
			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				SetReceiptFontSize();
			}
			
            Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblLocation");
            for (int i = 0; i < 15; i++)
            {
                Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(i));
            }
        }

		private void SetArraySizes()
		{
			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				lngLeftCol[0] = 0;
				lngLeftCol[1] = 1170 / 1440F;
				lngLeftCol[2] = 2880 / 1440F;
				lngLeftCol[3] = 3060 / 1440F;
				lngLeftCol[4] = 4950 / 1440F;
				lngLeftCol[5] = 5030 / 1440F;
				lngTotalWidth = 7215 / 1440F;
				lngFieldWid = 2880 / 1440F;
				PageSettings.Margins.Top = 1440 / 1440F;
				PageSettings.Margins.Bottom = 1440 / 1440F;
			}
			else
			{
                lngLeftCol[0] = 0;
                lngLeftCol[1] = 88.875f / 1440F;
                lngLeftCol[2] = 0;
                lngLeftCol[3] = 177.75f / 1440F;
                lngLeftCol[4] = 444.375f / 1440F;
                lngLeftCol[5] = 266.375f / 1440F;
                lngTotalWidth = 3555f / 1440F;
                lngFieldWid = 1440f / 1440F;
                PageSettings.Margins.Left = 319.95f / 1440F;
                PageSettings.Margins.Right = 0;
                PageSettings.Margins.Top = 0;
                PageSettings.Margins.Bottom = 0;
            }
		}

		private void SetupReceiptFormat()
		{
			fldName.Left = lngLeftCol[0];
			fldModule.Left = lngLeftCol[0];
			lblComment.Left = lngLeftCol[0];
			fldRef.Left = lngLeftCol[3];
			fldAmt.Left = lngLeftCol[5];

			this.PrintWidth = lngTotalWidth + 1200 / 1440F;
			if (!globalCashReceiptSettings.NarrowReceipt)
			{
				fldAmt.Width = 1440 / 1440F;
				fldRef.Width = 1890 / 1440F;
				fldName.Width = lngTotalWidth - fldName.Left;
			}
			else
			{
				fldAmt.Width = 1000 / 1440F;
				fldRef.Width = 1440 / 1440F;
			}

			fldModule.Width = lngFieldWid;
			fldNarrow.Width = lngTotalWidth;
			lblComment.Width = lngTotalWidth;
			fldControlDetail1.Width = lngTotalWidth - fldControlDetail1.Left;
			fldControlDetail2.Width = lngTotalWidth - fldControlDetail2.Left;
			fldControlDetail3.Width = lngTotalWidth - fldControlDetail3.Left;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
        {
            bool boolBatchDone;
            int intCT;
            bool boolREType = false;
            int intCommentLine = 0;
            if (receiptDetailsUserData?.transaction != null)
            {
                intRowsUsed = 6;
                
                if (string.IsNullOrWhiteSpace(comment))
                {
                    lblComment.Top = lngTop[0];
                    lblComment.Visible = false;
                    intCommentLine = 1;
                    intRowsUsed -= 1;
                }
                else
                {
                    lblComment.Visible = true;
                    lblComment.Top = lngTop[2];
                    intCommentLine = 2;
                    lblComment.Text = new string(' ',globalCashReceiptSettings.ReceiptOffset) + "   " + comment.PadRight(37 - globalCashReceiptSettings.ReceiptOffset);
                }

                switch (receiptDetailsUserData.transaction.TransactionTypeCode.ToIntegerValue())
                {
					case 90:
                    case 91:
                    case 92:
                    case 890:
                    case 891:
                    {
                        boolREType = true;
                        break;
                    }
                    default:
                    {
                        boolREType = false;
                        break;
                    }
                }

                var paidByName = receiptDetailsUserData.transaction.ToReceiptSummaryItem().Name;
                var receiptSummaryItem = receiptDetailsUserData.transaction.ToReceiptSummaryItem();
                if (boolREType)
                {
                    fldName.Visible = true;
                    fldName.Top = lngTop[0];
                    fldModule.Top = lngTop[1];
                    fldRef.Top = lngTop[1];
                    fldAmt.Top = lngTop[1];
                    fldName.Text = " " + paidByName.Left(37 - globalCashReceiptSettings.ReceiptOffset).PadRight(37 - globalCashReceiptSettings.ReceiptOffset);
                    fldNarrow.Top = lngTop[1];
                    intCommentLine = 1;
                }
                else if (paidByName == paidBy || paidByName.Trim() == "")
                {
                    intRowsUsed -= 1;
                    fldModule.Top = lngTop[0];
                    fldRef.Top = lngTop[0];
                    fldNarrow.Top = lngTop[0];
                    fldAmt.Top = lngTop[0];
                    fldName.Text = "";
                    fldName.Visible = false;
                    if (comment != "")
                    {
                        lblComment.Top = lngTop[1];
                        intCommentLine = 1;
                    }
                    else
                    {
                        intCommentLine = 0;
                    }
                }
                else
                {
                    fldName.Visible = true;
                    fldName.Top = lngTop[0];
                    fldModule.Top = lngTop[1];
                    fldRef.Top = lngTop[1];
                    fldAmt.Top = lngTop[1];
                    fldName.Text = "  **" + paidByName.PadRight(37 - globalCashReceiptSettings.ReceiptOffset);
                    fldNarrow.Top = lngTop[1];

                }
                if (!globalCashReceiptSettings.NarrowReceipt)
                {
                    fldModule.Visible = true;
                    fldRef.Visible = true;
                    fldNarrow.Visible = false;
                    if (receiptDetailsUserData.ShowFeeDetails)
                    {
                        fldAmt.Visible = false;
                    }
                    else
                    {
                        fldAmt.Visible = true;
                    }
                }
                else
                {
                    fldModule.Visible = false;
                    fldRef.Visible = false;
                    fldAmt.Visible = false;
                    fldNarrow.Visible = true;
                    if (receiptDetailsUserData.ShowFeeDetails)
                    {
                        strNarrowInfo = new string(' ', globalCashReceiptSettings.ReceiptOffset) + receiptSummaryItem.TypeDescription.PadRight(17 - globalCashReceiptSettings.ReceiptOffset) + (townAbbreviation + receiptDetailsUserData.transaction.Reference.PadRight(10)).PadRight(12);
                    }
                    else
                    {
                        strNarrowInfo = new string(' ', globalCashReceiptSettings.ReceiptOffset) + receiptSummaryItem.TypeDescription.PadRight(17 - globalCashReceiptSettings.ReceiptOffset) + (townAbbreviation + receiptDetailsUserData.transaction.Reference.PadRight(9 - globalCashReceiptSettings.ReceiptOffset)).PadRight(10) + receiptDetailsUserData.transaction.Total.ToString("#,##0.00").PadRight(12);
                    }
                    fldNarrow.Text = strNarrowInfo;
                }

                if (!globalCashReceiptSettings.ShowControlFields)
                {
                    fldControlDetail1.Top = lngTop[0];
                    fldControlDetail1.Visible = false;
                    fldControlDetail2.Top = lngTop[0];
                    fldControlDetail2.Visible = false;
                    fldControlDetail3.Top = lngTop[0];
                    fldControlDetail3.Visible = false;
                    intRowsUsed -= 3;
                }
                else
                {
                    FillControlFields(intCommentLine);
                }

                fldModule.Text = receiptSummaryItem.TypeDescription;
                fldRef.Text = townAbbreviation + receiptDetailsUserData.transaction.Reference;
                fldAmt.Text = receiptDetailsUserData.transaction.Total.ToString("#,##0.00");
                if (receiptDetailsUserData.ShowFeeDetails)
                {
	                foreach (var amount in receiptDetailsUserData.transaction.TransactionAmounts)
	                {
		                if (amount.Total != 0)
		                {
			                AddFeeRow(amount.Description, amount.Total, intRowsUsed * lngLineHeight, intRowsUsed);
			                intRowsUsed += 1;
						}
	                }
                }

                // this will create a location line for RE payments
                if (receiptDetailsUserData.transaction.TransactionTypeCode.ToIntegerValue() == 90 || receiptDetailsUserData.transaction.TransactionTypeCode.ToIntegerValue() == 91)
                {
                    AddLocationRow(receiptDetailsUserData.transaction.Controls.FirstOrDefault(x => x.Name == "Control3")?.Value, intRowsUsed * lngLineHeight);
                    intRowsUsed += 1;
                }
                Detail.Height = intRowsUsed * lngLineHeight;
            }
        }

		private void AddFeeRow(string strFeeDesc, decimal dblFee, float lngTop, int intNum, bool blnNoIndent = false)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			string strTemp;
			string strCaption;
			int intSpaces = 0;

            obNew = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(intNum));
			obNew.Top = lngTop;
			if (!blnNoIndent)
			{
				obNew.Left = fldModule.Left + 200 / 1440F;
			}
			else
			{
				obNew.Left = fldModule.Left;
			}

			obNew.Width = lngTotalWidth - obNew.Left;
			obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			obNew.Font = fldNarrow.Font;
			obNew.Font = new Font(obNew.Font.Name, fldNarrow.Font.Size);
			obNew.MultiLine = false;
			obNew.WordWrap = false;
			if (globalCashReceiptSettings.NarrowReceipt)
			{
				intSpaces = 17;
			}
			else
			{
				if (obNew.Font.Size >= 10)
				{
					intSpaces = 13;
				}
				else
				{
					intSpaces = 35;
				}
			}
			if (blnNoIndent)
			{
				intSpaces += 2;
			}
			strCaption = Strings.StrDup(globalCashReceiptSettings.ReceiptOffset, " ") + modGlobalFunctions.PadStringWithSpaces(strFeeDesc, (15 - globalCashReceiptSettings.ReceiptOffset), false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblFee, "#,##0.00"), intSpaces);
			obNew.Text = strCaption;
		}

		private void AddLocationRow(string strLocation, float lngTop)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			string strTemp;
            obNew = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblLocation");
			obNew.Top = lngTop;
			obNew.Left = fldModule.Left + 200 / 1440F;
			obNew.Width = this.PrintWidth;
			obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			obNew.Font = fldNarrow.Font;
			obNew.Font = new Font(obNew.Font.Name, fldNarrow.Font.Size);
			if (globalCashReceiptSettings.NarrowReceipt)
			{
				obNew.Text = Strings.Left(strLocation, 35);
			}
			else
			{
				obNew.Text = strLocation;
			}
		}

		private void SetReceiptFontSize()
		{
			try
			{
				if (wideFontSize <= 0)
				{
					return;
				}

				fldModule.Font = new Font(fldModule.Font.Name, wideFontSize);
				fldAmt.Font = new Font(fldAmt.Font.Name, wideFontSize);
				fldName.Font = new Font(fldName.Font.Name, wideFontSize);
				fldRef.Font = new Font(fldRef.Font.Name, wideFontSize);
				lblComment.Font = new Font(lblComment.Font.Name, wideFontSize);
				fldNarrow.Font = new Font(fldNarrow.Font.Name, wideFontSize);
				fldControlDetail1.Font = new Font(fldControlDetail1.Font.Name, wideFontSize);
				fldControlDetail2.Font = new Font(fldControlDetail2.Font.Name, wideFontSize);
				fldControlDetail3.Font = new Font(fldControlDetail3.Font.Name, wideFontSize);
			}
			catch (Exception ex)
			{

			}
		}

		private void ShowControlInformation(int controlLinesShown, int startLine, ControlItem controlItem)
		{
			TextBox control = null;
			switch (controlLinesShown)
			{
				case 0:
					control = fldControlDetail1;
					break;
				case 1:
					control = fldControlDetail2;
					break;
				case 2:
					control = fldControlDetail3;
					break;
			}

			control.Visible = true;
			control.Text = controlItem.Description + ":" + controlItem.Value;
			control.Top = lngTop[startLine + controlLinesShown + 1];
		}

		private void FillControlFields(int intCommentLine)
		{
			int controlLinesShown = 0;

			fldControlDetail1.Visible = false;
			fldControlDetail2.Visible = false;
			fldControlDetail3.Visible = false;

			foreach (var controlItem in receiptDetailsUserData.transaction.Controls.Where(x => x.Value.Trim() != ""))
			{
				ShowControlInformation(controlLinesShown, intCommentLine, controlItem);
				controlLinesShown++;
			}

			intRowsUsed -= (3 - controlLinesShown);
		}
	}
}
