﻿using System;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using Wisej.Web;

namespace TWCR0000.ReceiptProcess
{
	public partial class frmGetMultiTownId : BaseForm, IModalView<IMultiTownSelectionViewModel>
	{
		public frmGetMultiTownId()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		public frmGetMultiTownId(IMultiTownSelectionViewModel viewModel) : this()
		{
			ViewModel = viewModel;
		}
		
		private void InitializeComponentEx()
		{
			this.cmdSave.Click += CmdSave_Click;
			this.Load += FrmGetMultiTownId_Load;
			this.cmbTowns.KeyPress += CmbTowns_KeyPress;
		}

		private void CmbTowns_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				e.Handled = true;
				SaveTownId();
			}
		}

		private void FrmGetMultiTownId_Load(object sender, EventArgs e)
		{
			FillResCodeCombo();
		}

		private void FillResCodeCombo()
		{
			try
			{
				var towns = ViewModel.Regions();
				foreach (var town in towns)
				{
					cmbTowns.Items.Add(new DescriptionIDPair()
						{ ID = town.ID, Description = town.TownName});
				}

				if (cmbTowns.Items.Count > 0)
				{
					cmbTowns.SelectedIndex = 0;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + Information.Err(ex).Number + " - " + Information.Err(ex).Description + ".", "Error Filling Town ID Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CmdSave_Click(object sender, EventArgs e)
		{
			SaveTownId();
		}

		private void SaveTownId()
		{
			DescriptionIDPair selectedItem = (DescriptionIDPair)cmbTowns.SelectedItem;
			if (cmbTowns.SelectedIndex >= 0)
			{
				ViewModel.SelectedTownId = selectedItem.ID;
			}
		}

		public IMultiTownSelectionViewModel ViewModel { get; set; }
		public void ShowModal()
		{
			this.Show(FormShowEnum.Modal);
		}
	}
}
