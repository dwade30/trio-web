﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using GrapeCity.ActiveReports;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmNewReprint.
	/// </summary>
	partial class frmNewReprint : BaseForm
	{
		public fecherFoundation.FCPanel fraCashOut;
		public fecherFoundation.FCLabel lblEFT;
		public fecherFoundation.FCPanel fraSummary;
		public fecherFoundation.FCButton cmdPrintReceipt;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSplit;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;



		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.fraCashOut = new fecherFoundation.FCPanel();
            this.gridPayments = new fecherFoundation.FCGrid();
            this.lblChangeDue = new fecherFoundation.FCLabel();
            this.fcLabel8 = new fecherFoundation.FCLabel();
            this.lblTransactionTotal = new fecherFoundation.FCLabel();
            this.lblConvenienceFeeTotal = new fecherFoundation.FCLabel();
            this.lblCashTotal = new fecherFoundation.FCLabel();
            this.lblCheckTotal = new fecherFoundation.FCLabel();
            this.lblCreditTotal = new fecherFoundation.FCLabel();
            this.lblReceiptTotal = new fecherFoundation.FCLabel();
            this.lblTransactions = new fecherFoundation.FCLabel();
            this.lblConvenienceFee = new fecherFoundation.FCLabel();
            this.lblCashOutCashPaid = new fecherFoundation.FCLabel();
            this.lblCashOutCheckPaid = new fecherFoundation.FCLabel();
            this.lblCashOutCreditPaid = new fecherFoundation.FCLabel();
            this.lblCashOutTotal = new fecherFoundation.FCLabel();
            this.lblEFT = new fecherFoundation.FCLabel();
            this.fraSummary = new fecherFoundation.FCPanel();
            this.SummaryGrid = new fecherFoundation.FCGrid();
            this.txtPaidBy = new fecherFoundation.FCTextBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblSummaryTitle = new fecherFoundation.FCLabel();
            this.cmdPrintReceipt = new fecherFoundation.FCButton();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSplit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessPrint = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraCashOut)).BeginInit();
            this.fraCashOut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).BeginInit();
            this.fraSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReceipt)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintReceipt);
            this.BottomPanel.Location = new System.Drawing.Point(0, 505);
            this.BottomPanel.Size = new System.Drawing.Size(979, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraCashOut);
            this.ClientArea.Controls.Add(this.fraSummary);
            this.ClientArea.Size = new System.Drawing.Size(999, 635);
            this.ClientArea.Controls.SetChildIndex(this.fraSummary, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraCashOut, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(999, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(169, 28);
            this.HeaderText.Text = "Reprint Receipt";
            // 
            // fraCashOut
            // 
            this.fraCashOut.AppearanceKey = "groupBoxNoBorders";
            this.fraCashOut.Controls.Add(this.gridPayments);
            this.fraCashOut.Controls.Add(this.lblChangeDue);
            this.fraCashOut.Controls.Add(this.fcLabel8);
            this.fraCashOut.Controls.Add(this.lblTransactionTotal);
            this.fraCashOut.Controls.Add(this.lblConvenienceFeeTotal);
            this.fraCashOut.Controls.Add(this.lblCashTotal);
            this.fraCashOut.Controls.Add(this.lblCheckTotal);
            this.fraCashOut.Controls.Add(this.lblCreditTotal);
            this.fraCashOut.Controls.Add(this.lblReceiptTotal);
            this.fraCashOut.Controls.Add(this.lblTransactions);
            this.fraCashOut.Controls.Add(this.lblConvenienceFee);
            this.fraCashOut.Controls.Add(this.lblCashOutCashPaid);
            this.fraCashOut.Controls.Add(this.lblCashOutCheckPaid);
            this.fraCashOut.Controls.Add(this.lblCashOutCreditPaid);
            this.fraCashOut.Controls.Add(this.lblCashOutTotal);
            this.fraCashOut.Controls.Add(this.lblEFT);
            this.fraCashOut.Location = new System.Drawing.Point(0, 278);
            this.fraCashOut.Name = "fraCashOut";
            this.fraCashOut.Size = new System.Drawing.Size(979, 227);
            this.fraCashOut.TabIndex = 1;
            this.fraCashOut.Text = "Cash Out";
            // 
            // gridPayments
            // 
            this.gridPayments.Cols = 3;
            this.gridPayments.FixedCols = 0;
            this.gridPayments.Location = new System.Drawing.Point(383, 6);
            this.gridPayments.Name = "gridPayments";
            this.gridPayments.RowHeadersVisible = false;
            this.gridPayments.Rows = 1;
            this.gridPayments.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.gridPayments.ShowFocusCell = false;
            this.gridPayments.Size = new System.Drawing.Size(382, 190);
            this.gridPayments.TabIndex = 161;
            // 
            // lblChangeDue
            // 
            this.lblChangeDue.Location = new System.Drawing.Point(172, 172);
            this.lblChangeDue.Name = "lblChangeDue";
            this.lblChangeDue.TabIndex = 160;
            this.lblChangeDue.Text = "0.00";
            this.lblChangeDue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // fcLabel8
            // 
            this.fcLabel8.Location = new System.Drawing.Point(31, 172);
            this.fcLabel8.Name = "fcLabel8";
            this.fcLabel8.Size = new System.Drawing.Size(140, 16);
            this.fcLabel8.TabIndex = 159;
            this.fcLabel8.Text = "CHANGE DUE";
            // 
            // lblTransactionTotal
            // 
            this.lblTransactionTotal.Location = new System.Drawing.Point(172, 16);
            this.lblTransactionTotal.Name = "lblTransactionTotal";
            this.lblTransactionTotal.TabIndex = 158;
            this.lblTransactionTotal.Text = "0.00";
            this.lblTransactionTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblConvenienceFeeTotal
            // 
            this.lblConvenienceFeeTotal.Location = new System.Drawing.Point(172, 42);
            this.lblConvenienceFeeTotal.Name = "lblConvenienceFeeTotal";
            this.lblConvenienceFeeTotal.TabIndex = 157;
            this.lblConvenienceFeeTotal.Text = "0.00";
            this.lblConvenienceFeeTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCashTotal
            // 
            this.lblCashTotal.Location = new System.Drawing.Point(172, 94);
            this.lblCashTotal.Name = "lblCashTotal";
            this.lblCashTotal.TabIndex = 156;
            this.lblCashTotal.Text = "0.00";
            this.lblCashTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCheckTotal
            // 
            this.lblCheckTotal.Location = new System.Drawing.Point(172, 120);
            this.lblCheckTotal.Name = "lblCheckTotal";
            this.lblCheckTotal.TabIndex = 155;
            this.lblCheckTotal.Text = "0.00";
            this.lblCheckTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Location = new System.Drawing.Point(172, 146);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.TabIndex = 154;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblReceiptTotal
            // 
            this.lblReceiptTotal.Location = new System.Drawing.Point(172, 68);
            this.lblReceiptTotal.Name = "lblReceiptTotal";
            this.lblReceiptTotal.TabIndex = 153;
            this.lblReceiptTotal.Text = "0.00";
            this.lblReceiptTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTransactions
            // 
            this.lblTransactions.Location = new System.Drawing.Point(31, 16);
            this.lblTransactions.Name = "lblTransactions";
            this.lblTransactions.Size = new System.Drawing.Size(117, 16);
            this.lblTransactions.TabIndex = 152;
            this.lblTransactions.Text = "TRANSACTIONS";
            // 
            // lblConvenienceFee
            // 
            this.lblConvenienceFee.Location = new System.Drawing.Point(31, 42);
            this.lblConvenienceFee.Name = "lblConvenienceFee";
            this.lblConvenienceFee.Size = new System.Drawing.Size(157, 16);
            this.lblConvenienceFee.TabIndex = 151;
            this.lblConvenienceFee.Text = "CONVENIENCE FEE";
            // 
            // lblCashOutCashPaid
            // 
            this.lblCashOutCashPaid.Location = new System.Drawing.Point(31, 94);
            this.lblCashOutCashPaid.Name = "lblCashOutCashPaid";
            this.lblCashOutCashPaid.Size = new System.Drawing.Size(133, 16);
            this.lblCashOutCashPaid.TabIndex = 150;
            this.lblCashOutCashPaid.Text = "TOTAL CASH PAID";
            // 
            // lblCashOutCheckPaid
            // 
            this.lblCashOutCheckPaid.Location = new System.Drawing.Point(31, 120);
            this.lblCashOutCheckPaid.Name = "lblCashOutCheckPaid";
            this.lblCashOutCheckPaid.Size = new System.Drawing.Size(133, 16);
            this.lblCashOutCheckPaid.TabIndex = 149;
            this.lblCashOutCheckPaid.Text = "TOTAL CHECK PAID";
            // 
            // lblCashOutCreditPaid
            // 
            this.lblCashOutCreditPaid.Location = new System.Drawing.Point(31, 146);
            this.lblCashOutCreditPaid.Name = "lblCashOutCreditPaid";
            this.lblCashOutCreditPaid.Size = new System.Drawing.Size(140, 16);
            this.lblCashOutCreditPaid.TabIndex = 148;
            this.lblCashOutCreditPaid.Text = "TOTAL CREDIT PAID";
            // 
            // lblCashOutTotal
            // 
            this.lblCashOutTotal.Location = new System.Drawing.Point(31, 68);
            this.lblCashOutTotal.Name = "lblCashOutTotal";
            this.lblCashOutTotal.Size = new System.Drawing.Size(92, 16);
            this.lblCashOutTotal.TabIndex = 147;
            this.lblCashOutTotal.Text = "RECEIPT TOTAL";
            // 
            // lblEFT
            // 
            this.lblEFT.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblEFT.Location = new System.Drawing.Point(31, 200);
            this.lblEFT.Name = "lblEFT";
            this.lblEFT.Size = new System.Drawing.Size(171, 24);
            this.lblEFT.TabIndex = 17;
            this.lblEFT.Text = "EFT TRANSACTION";
            this.lblEFT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEFT.Visible = false;
            // 
            // fraSummary
            // 
            this.fraSummary.AppearanceKey = "groupBoxNoBorders";
            this.fraSummary.Controls.Add(this.SummaryGrid);
            this.fraSummary.Controls.Add(this.txtPaidBy);
            this.fraSummary.Controls.Add(this.Label7);
            this.fraSummary.Controls.Add(this.lblSummaryTitle);
            this.fraSummary.Location = new System.Drawing.Point(10, 0);
            this.fraSummary.Name = "fraSummary";
            this.fraSummary.Size = new System.Drawing.Size(969, 278);
            this.fraSummary.TabIndex = 2;
            // 
            // SummaryGrid
            // 
            this.SummaryGrid.Cols = 3;
            this.SummaryGrid.FixedCols = 0;
            this.SummaryGrid.Location = new System.Drawing.Point(21, 37);
            this.SummaryGrid.Name = "SummaryGrid";
            this.SummaryGrid.RowHeadersVisible = false;
            this.SummaryGrid.Rows = 1;
            this.SummaryGrid.ShowFocusCell = false;
            this.SummaryGrid.Size = new System.Drawing.Size(932, 162);
            this.SummaryGrid.TabIndex = 19;
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaidBy.Cursor = Wisej.Web.Cursors.Default;
            this.txtPaidBy.Location = new System.Drawing.Point(134, 217);
            this.txtPaidBy.LockedOriginal = true;
            this.txtPaidBy.MaxLength = 50;
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.ReadOnly = true;
            this.txtPaidBy.Size = new System.Drawing.Size(343, 40);
            this.txtPaidBy.TabIndex = 3;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(21, 231);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(50, 16);
            this.Label7.TabIndex = 2;
            this.Label7.Text = "PAID BY";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSummaryTitle
            // 
            this.lblSummaryTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblSummaryTitle.Location = new System.Drawing.Point(21, 15);
            this.lblSummaryTitle.Name = "lblSummaryTitle";
            this.lblSummaryTitle.Size = new System.Drawing.Size(604, 16);
            this.lblSummaryTitle.TabIndex = 4;
            this.lblSummaryTitle.Text = "SUMMARY";
            this.lblSummaryTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdPrintReceipt
            // 
            this.cmdPrintReceipt.AppearanceKey = "acceptButton";
            this.cmdPrintReceipt.Cursor = Wisej.Web.Cursors.Default;
            this.cmdPrintReceipt.Location = new System.Drawing.Point(440, 33);
            this.cmdPrintReceipt.Name = "cmdPrintReceipt";
            this.cmdPrintReceipt.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintReceipt.Size = new System.Drawing.Size(125, 48);
            this.cmdPrintReceipt.TabIndex = 0;
            this.cmdPrintReceipt.Text = "Print Receipt";
            this.cmdPrintReceipt.Click += new System.EventHandler(this.mnuProcessPrint_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSplit,
            this.mnuProcessPrint,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSplit
            // 
            this.mnuProcessSplit.Enabled = false;
            this.mnuProcessSplit.Index = 0;
            this.mnuProcessSplit.Name = "mnuProcessSplit";
            this.mnuProcessSplit.Text = "Print Split";
            this.mnuProcessSplit.Click += new System.EventHandler(this.mnuProcessSplit_Click);
            // 
            // mnuProcessPrint
            // 
            this.mnuProcessPrint.Index = 1;
            this.mnuProcessPrint.Name = "mnuProcessPrint";
            this.mnuProcessPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessPrint.Text = "Show Receipt";
            this.mnuProcessPrint.Click += new System.EventHandler(this.mnuProcessPrint_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmNewReprint
            // 
            this.ClientSize = new System.Drawing.Size(999, 695);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmNewReprint";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reprint Receipt";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Resize += new System.EventHandler(this.frmNewReprint_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraCashOut)).EndInit();
            this.fraCashOut.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).EndInit();
            this.fraSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReceipt)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
        public FCGrid SummaryGrid;
        public FCTextBox txtPaidBy;
        public FCLabel Label7;
        public FCLabel lblSummaryTitle;
        public FCLabel lblChangeDue;
        public FCLabel fcLabel8;
        public FCLabel lblTransactionTotal;
        public FCLabel lblConvenienceFeeTotal;
        public FCLabel lblCashTotal;
        public FCLabel lblCheckTotal;
        public FCLabel lblCreditTotal;
        public FCLabel lblReceiptTotal;
        public FCLabel lblTransactions;
        public FCLabel lblConvenienceFee;
        public FCLabel lblCashOutCashPaid;
        public FCLabel lblCashOutCheckPaid;
        public FCLabel lblCashOutCreditPaid;
        public FCLabel lblCashOutTotal;
        public FCGrid gridPayments;
    }
}
