﻿using System;
using Global;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using Wisej.Web;


namespace TWCR0000.ReceiptProcess
{
	public partial class frmVoidReceipt : BaseForm, IModalView<IVoidReceiptViewModel>
	{
		public frmVoidReceipt()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		public frmVoidReceipt(IVoidReceiptViewModel viewModel) : this()
		{
			ViewModel = viewModel;
            ViewModel.AlertAdded += ViewModel_AlertAdded;
		}

        private void ViewModel_AlertAdded(object sender, (string message, string alertType) e)
        {
            MessageBoxIcon alertIcon;
            switch (e.alertType.ToLower())
            {
				case "error":
                    alertIcon = MessageBoxIcon.Error;
                    break;
				case "warning":
                    alertIcon = MessageBoxIcon.Warning;
                    break;
				case "question":
                    alertIcon = MessageBoxIcon.Question;
                    break;
				case "information":
                    alertIcon = MessageBoxIcon.Information;
                    break;
				default:
                    alertIcon = MessageBoxIcon.None;
                    break;
            }

            MessageBox.Show(e.message, "", MessageBoxButtons.OK, alertIcon);
        }

        private void InitializeComponentEx()
		{
			this.Load += FrmVoidReceipt_Load;
			this.Resize += FrmVoidReceipt_Resize;
			this.btnCancel.Click += BtnCancel_Click;
			this.cmdVoid.Click += CmdVoid_Click;

		}

		private void CmdVoid_Click(object sender, EventArgs e)
		{
			ViewModel.CashOut();
			Close();
		}

		private void BtnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FrmVoidReceipt_Resize(object sender, EventArgs e)
		{
			ResizePaymentsGrid();
			ResizeSummaryGrid();
		}

		private void FrmVoidReceipt_Load(object sender, EventArgs e)
		{
			SetupGridPayments();
			SetupSummaryGrid();
			ShowPayerInfo();
			FillSummaryGrid();
			FillPaymentGrid();
		}

		private void ShowPayerInfo()
		{
			txtPaidBy.Text = ViewModel.TransactionGroup.PaidBy;
			txtCustNum_PaidBy.Text = ViewModel.TransactionGroup.PaidById > 0 ? ViewModel.TransactionGroup.PaidById.ToString() : "";
		}

		private void FillSummaryGrid()
		{
			SummaryGrid.Rows = 1;
			var transactions = ViewModel.TransactionGroup.GetTransactions();

			foreach (var transaction in transactions)
			{
				AddTransactionToSummaryGrid(transaction);
			}
		}

		private void AddTransactionToSummaryGrid(TransactionBase transaction)
		{
			SummaryGrid.Rows += 1;
			var row = SummaryGrid.Rows - 1;
			var summaryItem = transaction.ToReceiptSummaryItem();
			SummaryGrid.TextMatrix(row, (int)SummaryColumns.Id, summaryItem.TransactionId);
			SummaryGrid.TextMatrix(row, (int)SummaryColumns.TransactionType, summaryItem.TransactionType);
			SummaryGrid.TextMatrix(row, (int)SummaryColumns.Amount, summaryItem.Amount.FormatAsMoney());
			SummaryGrid.TextMatrix(row, (int)SummaryColumns.Name, summaryItem.Name);
			SummaryGrid.TextMatrix(row, (int)SummaryColumns.Info, summaryItem.Info);
			SummaryGrid.TextMatrix(row, (int)SummaryColumns.Split, summaryItem.Split);
			SummaryGrid.RowData(row, summaryItem.TransactionId);
			if (summaryItem.Account > 0)
			{
				SummaryGrid.TextMatrix(row, (int)SummaryColumns.Account, summaryItem.Account.ToString());
			}
			else
			{
				SummaryGrid.TextMatrix(row, (int)SummaryColumns.Account, "");
			}
			SummaryGrid.TextMatrix(row, (int)SummaryColumns.TypeDescription, summaryItem.TypeDescription);
		}


		private void FillPaymentGrid()
		{
			lblTransactionTotal.Text = ViewModel.TotalAmount.FormatAsMoney();
			lblConvenienceFeeTotal.Text = ViewModel.ConvenienceFee.FormatAsMoney();
			lblReceiptTotal.Text = ViewModel.TotalReceiptAmount.FormatAsMoney();
			lblCashTotal.Text = ViewModel.TotalCashPaid.FormatAsMoney();
			lblCheckTotal.Text = ViewModel.TotalCheckPaid.FormatAsMoney();
			lblCreditTotal.Text = ViewModel.TotalCreditPaid.FormatAsMoney();
			gridPayments.Rows = 1;
			var payments = ViewModel.Payments;
			foreach (var payment in payments)
			{
				AddPaymentToGrid(payment);
			}
		}

		private void AddPaymentToGrid(ReceiptPayment payment)
		{
			gridPayments.Rows += 1;
			var row = gridPayments.Rows - 1;

			gridPayments.TextMatrix(row, (int)PaymentCols.Id, payment.Id);
			gridPayments.TextMatrix(row, (int)PaymentCols.Amount, payment.Amount.FormatAsMoney());
			gridPayments.TextMatrix(row, (int)PaymentCols.Reference, payment.Reference);
			switch (payment.PaymentType)
			{
				case PaymentMethod.Cash:
					gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Cash");
					break;
				case PaymentMethod.Check:
					gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Check");
					break;
				case PaymentMethod.Credit:
					gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Credit Card");
					gridPayments.TextMatrix(row, (int)PaymentCols.EPaymentReferenceNumber, ((CreditCardReceiptPayment)payment).EPaymentReferenceNumber);
					break;
			}

			gridPayments.RowData(row, payment);
		}

		public IVoidReceiptViewModel ViewModel { get; set; }
		public void ShowModal()
		{
			this.Show(FormShowEnum.Modal);
		}

		private enum SummaryColumns
		{
			Id = 0,
			TransactionType = 1,
			TypeDescription = 2,
			Account = 3,
			Name = 4,
			Info = 5,
			Copies = 6,
			Amount = 7,
			Split = 8
		}

		private enum PaymentCols
		{
			Id = 0,
			EPaymentReferenceNumber = 1,
			PaymentType = 2,
			Amount = 3,
			Reference = 4
		}

		private void SetupSummaryGrid()
		{
			SummaryGrid.Cols = 9;
			SummaryGrid.TextMatrix(0, (int)SummaryColumns.TransactionType, "Type");
			SummaryGrid.TextMatrix(0, (int)SummaryColumns.TypeDescription, "Type Description");
			SummaryGrid.TextMatrix(0, (int)SummaryColumns.Account, "Account");
			SummaryGrid.TextMatrix(0, (int)SummaryColumns.Name, "Name");
			SummaryGrid.TextMatrix(0, (int)SummaryColumns.Info, "Info");
			SummaryGrid.TextMatrix(0, (int)SummaryColumns.Amount, "Amount");
			SummaryGrid.TextMatrix(0, (int)SummaryColumns.Split, "Split");
			SummaryGrid.ColHidden((int)SummaryColumns.Id, true);
		}
		private void ResizeSummaryGrid()
		{
			SummaryGrid.Cols = 9;
			var gridWidth = SummaryGrid.WidthOriginal;
			SummaryGrid.ColWidth((int)SummaryColumns.TransactionType, Convert.ToInt32(gridWidth * 0.06));
			SummaryGrid.ColWidth((int)SummaryColumns.TypeDescription, Convert.ToInt32(gridWidth * 0.2));
			SummaryGrid.ColWidth((int)SummaryColumns.Account, Convert.ToInt32(gridWidth * 0.09));
			SummaryGrid.ColWidth((int)SummaryColumns.Name, Convert.ToInt32(gridWidth * .2));
			SummaryGrid.ColWidth((int)SummaryColumns.Info, Convert.ToInt32(gridWidth * .17));
			SummaryGrid.ColWidth((int)SummaryColumns.Amount, Convert.ToInt32(gridWidth * 0.11));
			SummaryGrid.Columns[(int)SummaryColumns.Split].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
		}

		private void ResizePaymentsGrid()
		{
			var gridWidth = gridPayments.WidthOriginal;
			gridPayments.ColWidth((int)PaymentCols.PaymentType, Convert.ToInt32(gridWidth * .3));
			gridPayments.ColWidth((int)PaymentCols.Amount, Convert.ToInt32(gridWidth * .35));
			gridPayments.Columns[(int)PaymentCols.Reference].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
		}

		private void SetupGridPayments()
		{
			gridPayments.Rows = 1;
			gridPayments.Cols = 5;
			gridPayments.ColHidden((int)PaymentCols.Id, true);
			gridPayments.ColHidden((int)PaymentCols.EPaymentReferenceNumber, true);
			gridPayments.TextMatrix(0, (int)PaymentCols.PaymentType, "Type");
			gridPayments.TextMatrix(0, (int)PaymentCols.Amount, "Amount");
			gridPayments.TextMatrix(0, (int)PaymentCols.Reference, "Reference");
		}

	}
}
