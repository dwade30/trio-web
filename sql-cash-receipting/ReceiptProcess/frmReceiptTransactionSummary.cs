﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using fecherFoundation;
using Global;
//using Microsoft.Ajax.Utilities;
using SharedApplication;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.MyRecImport;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Commands;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWCR0000.ReceiptProcess
{
    public partial class frmReceiptTransactionSummary : BaseForm, IView<ICRCashOutViewModel>
    {
        public frmReceiptTransactionSummary()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmReceiptTransactionSummary(ICRCashOutViewModel viewModel) : this()
        {
            ViewModel = viewModel;
            ViewModel.PaymentsUpdated += ViewModel_PaymentsUpdated;
            ViewModel.TransactionsUpdated += ViewModel_TransactionsUpdated;
            ViewModel.PartyUpdated += ViewModel_PartyUpdated;
        }

        private void ViewModel_PartyUpdated(object sender, (int PartyId, string PartyName) e)
        {
            ShowPayerInfo();
        }

        public ICRCashOutViewModel ViewModel { get; set; }

        private void InitializeComponentEx()
        {
            this.Resize += FrmReceiptTransactionSummary_Resize;
            this.Load += FrmReceiptTransactionSummary_Load;
            SummaryGrid.CellMouseDown += SummaryGrid_CellMouseDown;
            btnAddTransaction.Click += BtnAddTransaction_Click;
            cmbTransactionType.KeyPress += CmbTransactionType_KeyPress;
            cmbPaymentType.SelectedItemChanged += CmbPaymentType_SelectedItemChanged;
            btnAddPayment.Click += BtnAddPayment_Click;
            cmdSearch_PaidBy.Click += CmdSearch_PaidBy_Click;
            cmdContinue.Click += CmdContinue_Click;
            btnMyRecReceiptImport.Click += BtnMyRecReceiptImport_Click;
            btnImportMooringReceipts.Click += BtnImportMooringReceipts_Click;
        }

        private void BtnMyRecReceiptImport_Click(object sender, EventArgs e)
        {
            var fileName = GetxFilePath("Select MyRec file to upload");
            if (!fileName.IsNullOrWhiteSpace())
            {
                this.Hide();
                ViewModel.StartMyRecTransaction(fileName);
                Close();
            }
        }

        private void BtnImportMooringReceipts_Click(object sender, EventArgs e)
        {
            var fileName = GetxFilePath("Select mooring file to upload");
            if (!fileName.IsNullOrWhiteSpace())
            {
                this.Hide();
                ViewModel.StartCamdenMooringTransaction(fileName);
                Close();
            }
        }

        private string GetxFilePath(string description)
        {
            return StaticSettings.GlobalCommandDispatcher.Send(new GetUploadFile(description)).Result;
        }

        private void CmdSearch_PaidBy_Click(object sender, EventArgs e)
        {
            ViewModel.ChooseParty();

            //int lngID;
            //lngID = frmCentralPartySearch.InstancePtr.Init();
            //if (lngID > 0)
            //{
            // txtCustNum_PaidBy.Text = FCConvert.ToString(lngID);
            // GetPartyInfo(lngID);
            //}
        }

        private void CmbPaymentType_SelectedItemChanged(object sender, EventArgs e)
        {
            GenericDescriptionPair<PaymentMethod> selectedItem = (GenericDescriptionPair<PaymentMethod>)cmbPaymentType.SelectedItem;
            if (selectedItem.ID == PaymentMethod.Check)
            {
                tblCheckNumber.Visible = true;
                tblBank.Visible = true;
                if (cmbBank.Items.Count > 1)
                {
                    cmbBank.SelectedIndex = 1;
                }
            }
            else
            {
                tblCheckNumber.Visible = false;
                tblBank.Visible = false;
            }

            tblCardType.Visible = selectedItem.ID == PaymentMethod.Credit;
        }

        private void BtnAddPayment_Click(object sender, EventArgs e)
        {
            AddPayment();
        }

        private void AddPayment()
        {
            var paymentAmt = txtPayment.Text.ToDecimalValue();

            if (paymentAmt == 0) return;

            var selectedItem = (GenericDescriptionPair<PaymentMethod>)cmbPaymentType.SelectedItem;
            switch (selectedItem.ID)
            {
                case PaymentMethod.Cash:
                    AddCashPayment(paymentAmt);
                    break;
                case PaymentMethod.Check:
                    AddCheckPayment(paymentAmt, txtCheckNumber.Text.Trim(), (DescriptionIDPair)cmbBank.SelectedItem);
                    break;
                case PaymentMethod.Credit:
                    //TODO Hit payport here first?
                    AddCreditPayment(paymentAmt, (DescriptionIDPair)cmbCardType.SelectedItem);
                    break;
            }
        }

        private void AddCheckPayment(decimal amount, string checkNumber, DescriptionIDPair selectedBank)
        {
            if (checkNumber  == "")
            {
                MessageBox.Show("You must supply a check number before adding this payment", "Invalid Check Number",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            var bankId = 0;

            if (selectedBank == null)
            {
                MessageBox.Show("You must select a bank before adding this payment", "No Bank Selected",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            bankId = selectedBank.ID == 0 ? ViewModel.AddBank() : selectedBank.ID;


            if (bankId <= 0) return;

            if (ViewModel.CheckAlreadyExistsForBank(checkNumber, bankId))
            {
                if (MessageBox.Show("This check is already in the system.  Would you like to continue?",
                                    "Input Error", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }

            var payment = new CheckReceiptPayment()
            {
                Amount = amount,
                CheckNumber = checkNumber,
                BankId = bankId
            };
            ViewModel.AddPayment(payment);
        }

        private void AddCreditPayment(decimal amount, DescriptionIDPair selectedCardType)
        {
            var typeId = 0;

            if (selectedCardType == null)
            {
                MessageBox.Show("You must select a card type before adding this payment", "No Card Type Selected",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            typeId = selectedCardType.ID;

            if (typeId <= 0) return;

            var payment = new CreditCardReceiptPayment
            {
                Amount = amount,
                TypeId = typeId,
                TypeDescription = selectedCardType.Description
            };

            //TODO 
            ViewModel.AddPayment(payment);
        }


        private void AddCashPayment(decimal amount)
        {
            var payment = new ReceiptPayment() { PaymentType = PaymentMethod.Cash, Amount = amount, Reference = "" };
            ViewModel.AddPayment(payment);
        }

        private void ViewModel_PaymentsUpdated(object sender, EventArgs e)
        {
            RefreshPayments();
        }

        private void ViewModel_TransactionsUpdated(object sender, EventArgs e)
        {
            RefreshTransactions();
        }

        private void RefreshTransactions()
        {
            FillSummaryGrid();
            ShowPayments();
            SetPrintReceiptCopiesOptions();
        }

        private void SetPrintReceiptCopiesOptions()
        {
            chkPrint.Checked = ViewModel.ShouldPrintReceipt;
            cmbCopies.ListIndex = ViewModel.Copies - 1;
        }

        private void RefreshPayments()
        {
            ShowPayments();
            DefaultPaymentSection();
        }

        private void ShowPayments()
        {
            lblTransactionTotal.Text = ViewModel.TotalAmount.FormatAsMoney();
            lblConvenienceFeeTotal.Text = ViewModel.ConvenienceFee.FormatAsMoney();
            lblReceiptTotal.Text = ViewModel.TotalReceiptAmount.FormatAsMoney();
            lblCashTotal.Text = ViewModel.TotalCashPaid.FormatAsMoney();
            lblCheckTotal.Text = ViewModel.TotalCheckPaid.FormatAsMoney();
            lblCreditTotal.Text = ViewModel.TotalCreditPaid.FormatAsMoney();
            lblChangeDue.Text = ViewModel.ChangeDue.FormatAsMoney();
            gridPayments.Rows = 1;
            var payments = ViewModel.Payments;
            foreach (var payment in payments)
            {
                AddPaymentToGrid(payment);
            }

            paymentPanel.Enabled = ViewModel.TotalReceiptAmount != 0;

            cmdContinue.Enabled = ViewModel.CanCashOut();
        }

        private void AddPaymentToGrid(ReceiptPayment payment)
        {
            gridPayments.Rows += 1;
            var row = gridPayments.Rows - 1;

            gridPayments.TextMatrix(row, (int)PaymentCols.Id, payment.Id);
            gridPayments.TextMatrix(row, (int)PaymentCols.Amount, payment.Total.FormatAsMoney());
            gridPayments.TextMatrix(row, (int)PaymentCols.Reference, payment.Reference);
            switch (payment.PaymentType)
            {
                case PaymentMethod.Cash:
                    gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Cash");
                    break;
                case PaymentMethod.Check:
                    gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Check");
                    break;
                case PaymentMethod.Credit:
                    gridPayments.TextMatrix(row, (int)PaymentCols.PaymentType, "Credit Card");
                    break;
            }

            gridPayments.RowData(row, payment);
        }


        private void SetupGridPayments()
        {
            gridPayments.Rows = 1;
            gridPayments.Cols = 4;
            gridPayments.ColHidden((int)PaymentCols.Id, true);
            gridPayments.TextMatrix(0, (int)PaymentCols.PaymentType, "Type");
            gridPayments.TextMatrix(0, (int)PaymentCols.Amount, "Amount");
            gridPayments.TextMatrix(0, (int)PaymentCols.Reference, "Reference");
        }

        private void CmbTransactionType_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                if (!ValidateTransactionChoice())
                {
                    cmbTransactionType.Text = "";
                }
                else
                {
                    NewTransaction();
                }
            }
        }

        private bool ValidateTransactionChoice()
        {
            var choice = cmbTransactionType.Text.Trim();
            if (String.IsNullOrWhiteSpace(choice))
            {
                return false;
            }

            var code = choice.Split(new char[] { ' ' }, 2)[0];
            if (code.ToIntegerValue() > 0)
            {
                return ViewModel.IsValidTransactionCode(code.ToIntegerValue());
            }
            else if (code.Length > 1)
            {
                return ViewModel.IsValidTransactionChoice(code);
            }

            return false;
        }

        private void BtnAddTransaction_Click(object sender, EventArgs e)
        {
            if (ValidateTransactionChoice())
            {
                NewTransaction();
            }
            else
            {
                MessageBox.Show("That is not a valid transaction type", "Invalid Transaction Type",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void NewTransaction()
        {
	        UpdateReceiptGroupInfo();
            var selectedItem = (DescriptionIDPair)cmbTransactionType.SelectedItem;
            if (selectedItem != null)
            {
                StartTransactionByCode(selectedItem.ID);
            }
            else
            {
                if (cmbTransactionType.Text.ToIntegerValue() > 0)
                {
                    StartTransactionByCode(cmbTransactionType.Text.ToIntegerValue());
                }
                else
                {
                    StartTransactionByAccount(cmbTransactionType.Text);
                }
            }
        }

        private void StartTransactionByAccount(string accountCode)
        {
            var account = accountCode.Substring(1).ToIntegerValue();
            switch (accountCode.Left(1).ToLower())
            {
                case "r":
                    ViewModel.StartRealEstateTransaction(account);
                    break;
                case "p":
                    ViewModel.StartPersonalPropertyTransaction(account);
                    break;
                case "u":
                    ViewModel.StartUtilityTransaction(account);
                    break;
                case "a":
                    ViewModel.StartAccountsReceivableTransaction(account);
                    break;
                default:
                    return;
            }

            Close();
        }

        private void UpdateReceiptGroupInfo()
        {
	        ViewModel.PaidBy = txtPaidBy.Text.Trim();
	        ViewModel.PaidById = txtCustNum_PaidBy.Text.ToIntegerValue();
	        ViewModel.Copies = cmbCopies.Text.ToIntegerValue();
	        ViewModel.ShouldPrintReceipt = chkPrint.Checked;

            for (int row = 1; row <= SummaryGrid.Rows - 1; row++)
	        {
		        ViewModel.UpdateTransactionSplit(new Guid(SummaryGrid.TextMatrix(row, (int)SummaryColumns.Id)), SummaryGrid.TextMatrix(row, (int)SummaryColumns.Split).ToIntegerValue());
            }
        }

        private void StartTransactionByCode(int code)
        {
            switch (code)
            {
                case 90:
                    ViewModel.StartRealEstateTransaction(0);
                    break;
                case 92:
                    ViewModel.StartPersonalPropertyTransaction(0);
                    break;
                case 93:
                    ViewModel.StartUtilityTransaction(0);
                    break;
                case 97:
                    ViewModel.StartAccountsReceivableTransaction(0);
                    break;
                case 98:
                    ViewModel.StartClerkTransaction();
                    break;
                case 99:
                    var mvSuccess = ViewModel.StartMotorVehicleTransaction();
                    if (!mvSuccess.success)
                    {
                        FCMessageBox.Show(mvSuccess.message, MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Error");
                        return;
                    }
                    break;
                case 190:
                    ViewModel.StartMosesTransaction();
                    break;
                default:
                    ViewModel.StartMiscReceiptTransaction(code, txtPaidBy.Text.Trim());
                    break;
            }

            Close();
        }
        private void CmdContinue_Click(object sender, EventArgs e)
        {
            CashOut();
        }

        private void CashOut()
        {
            
            if (!IsReadyToCashOut()) return;


            ViewModel.PaidBy = txtPaidBy.Text;
            ViewModel.PaidById = txtCustNum_PaidBy.Text.ToIntegerValue();

            var receiptNumber = ViewModel.CashOut();

            if (receiptNumber > 0)
            {
                MessageBox.Show("Receipt #" + receiptNumber + " was generated.", "Receipt Number", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                PrintReceipt();

                Close();

                ViewModel.StartNewTransaction();
            }
        }

        private void PrintReceipt()
        {
            if (!ViewModel.IsReceiptPrinterSetUp())
            {
                MessageBox.Show(
                    "You must select a receipt printer in Settings -> Printer Setup before you will be able to print a receipt.",
                    "Invalid Receipt Printer", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (chkPrint.Checked)
                {
                    var copies = cmbCopies.ListIndex + 1;

                    if (ViewModel.PrintExtraReceipt() && ViewModel.TotalCreditPaid != 0)
                    {
                        copies++;
                    }

                    ViewModel.PrintReceipt(copies);
                }
                else
                {
                    if (!ViewModel.PrintExtraReceipt() || ViewModel.TotalCreditPaid == 0) return;

                    var dialogResult = MessageBox.Show("Would you like to print your extra copy of the receipt?",
                                                       "Print Extra Copy?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dialogResult == DialogResult.Yes)
                    {
                        ViewModel.PrintReceipt(1);
                    }
                }
            }
        }

        private bool IsReadyToCashOut()
        {
            //TODO verify credit cards here?

            // viewmodel say you are ready?
            if (!ViewModel.CanCashOut())
            {
                MessageBox.Show("You cannot continue until the remaining due is zero and the receipt total is non-zero",
                                "Cannot continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return false;
            }

            // going to print and printer is ready/configured?
            if (chkPrint.Checked && !ClientPrintingSetup.InstancePtr.CheckConfiguration()) return false;

            // you be ready
            return true;
        }

        private void SummaryGrid_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {

            var row = SummaryGrid.MouseRow;
            if (e.Button == MouseButtons.Right && row > 0)
            {
                // right click
                ViewModel.ShowPaymentDetail((Guid)SummaryGrid.RowData(row));
            }
        }

        private void FrmReceiptTransactionSummary_Load(object sender, EventArgs e)
        {
            if (ViewModel.AsPendingTransaction)
            {
                this.Text = "Receipt Input for Pending Transactions";
            }

            ShowCustomOptions();
            FillBankCombo();
            FillCopiesCombo();
            SetupSummaryGrid();
            SetupGridPayments();
            FillTransactionTypeCombo();
            FillPaymentTypeCombo();
            FillCreditCardTypeCmbo();
            ShowErrors();
            ShowNotifications();
            ShowPayerInfo();
            RefreshTransactions();
            RefreshPayments();
        }

        private void ShowCustomOptions()
        {
            btnMyRecReceiptImport.Visible = ViewModel.MyRecImportEnabled;
            btnMyRecReceiptImport.Enabled = ViewModel.MyRecImportEnabled;
            btnImportMooringReceipts.Visible = ViewModel.CamdenMooringImportEnabled;
            btnImportMooringReceipts.Enabled = ViewModel.CamdenMooringImportEnabled;
        }

        private void FillCopiesCombo()
        {
            cmbCopies.AddItem("1");
            cmbCopies.AddItem("2");
            cmbCopies.AddItem("3");
            cmbCopies.AddItem("4");
        }

        private void FillCreditCardTypeCmbo()
        {
            foreach (var creditCardType in ViewModel.CreditCardTypes)
            {
                cmbCardType.Items.Add(new DescriptionIDPair
                {
                    ID = creditCardType.ID,
                    Description = creditCardType.Type
                });
            }
        }

        private void FillBankCombo()
        {

            var banks = ViewModel.Banks;
            cmbBank.Items.Add(new DescriptionIDPair() { ID = 0, Description = "-- Add New Bank --" });
            foreach (var bank in banks)
            {
                cmbBank.Items.Add(new DescriptionIDPair()
                    {ID = bank.ID, Description = bank.Name});
            }
        }

        private void ShowPayerInfo()
        {
            txtPaidBy.Text = ViewModel.PaidBy;
            txtCustNum_PaidBy.Text = ViewModel.PaidById > 0 ? ViewModel.PaidById.ToString() : "";
        }
        private void ShowNotifications()
        {
            if (!ViewModel.ValidationErrors.Any() || ViewModel.TransactionGroup.HasTransactions()) return;
            
            var messageDetails = "";
            foreach (var validationError in ViewModel.ValidationErrors)
            {
                messageDetails += validationError + "\r\n";
            }

            MessageBox.Show(
                "Issues found with receipt types.  Please go to Type Setup to correct the issues listed below. \r\n\r\n" + messageDetails, "Receipt Type Validation Errors", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void ShowErrors()
        {
            if (!ViewModel.ProcessErrors.Any()) return;
            var messageDetails = "";
            var lineEnd = "";
            foreach (var processError in ViewModel.ProcessErrors)
            {
                messageDetails += lineEnd + processError;
                lineEnd = "\r\n";
            }

            MessageBox.Show(messageDetails, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void FrmReceiptTransactionSummary_Resize(object sender, EventArgs e)
        {
            ResizeSummaryGrid();
            ResizePaymentsGrid();
        }

        private void FillSummaryGrid()
        {
            SummaryGrid.Rows = 1;
            var transactions = ViewModel.TransactionGroup.GetTransactions();

            var printReceipt = false;
            var copies = 0;

            foreach (var transaction in transactions)
            {
                AddTransactionToSummaryGrid(transaction);
            }
        }

        private void AddTransactionToSummaryGrid(TransactionBase transaction)
        {
            SummaryGrid.Rows += 1;
            var row = SummaryGrid.Rows - 1;
            var summaryItem = transaction.ToReceiptSummaryItem();
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Id, summaryItem.TransactionId);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.TransactionType, summaryItem.TransactionType);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Amount, summaryItem.Amount.FormatAsMoney());
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Name, summaryItem.Name);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Info, summaryItem.Info);
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.Split, summaryItem.Split);
            SummaryGrid.RowData(row, summaryItem.TransactionId);
            if (summaryItem.Account > 0)
            {
                SummaryGrid.TextMatrix(row, (int)SummaryColumns.Account, summaryItem.Account.ToString());
            }
            else
            {
                SummaryGrid.TextMatrix(row, (int)SummaryColumns.Account, "");
            }
            SummaryGrid.TextMatrix(row, (int)SummaryColumns.TypeDescription, summaryItem.TypeDescription);
        }



        private void SetupSummaryGrid()
        {
            SummaryGrid.Cols = 9;
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.TransactionType, "Type");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.TypeDescription, "Type Description");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Account, "Account");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Name, "Name");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Info, "Info");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Amount, "Amount");
            SummaryGrid.TextMatrix(0, (int)SummaryColumns.Split, "Split");
            SummaryGrid.ColHidden((int)SummaryColumns.Id, true);
            SummaryGrid.ColComboList((int)SummaryColumns.Split, "#1;1|#2;2|#3;3|#4;4|#5;5");
        }

        private void ResizeSummaryGrid()
        {
            SummaryGrid.Cols = 9;
            var gridWidth = SummaryGrid.WidthOriginal;
            SummaryGrid.ColWidth((int)SummaryColumns.TransactionType, Convert.ToInt32(gridWidth * 0.06));
            SummaryGrid.ColWidth((int)SummaryColumns.TypeDescription, Convert.ToInt32(gridWidth * 0.2));
            SummaryGrid.ColWidth((int)SummaryColumns.Account, Convert.ToInt32(gridWidth * 0.09));
            SummaryGrid.ColWidth((int)SummaryColumns.Name, Convert.ToInt32(gridWidth * .2));
            SummaryGrid.ColWidth((int)SummaryColumns.Info, Convert.ToInt32(gridWidth * .17));
            SummaryGrid.ColWidth((int)SummaryColumns.Amount, Convert.ToInt32(gridWidth * 0.11));
            SummaryGrid.Columns[(int)SummaryColumns.Split].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void ResizePaymentsGrid()
        {
            var gridWidth = gridPayments.WidthOriginal;
            gridPayments.ColWidth((int)PaymentCols.PaymentType, Convert.ToInt32(gridWidth * .2));
            gridPayments.ColWidth((int)PaymentCols.Amount, Convert.ToInt32(gridWidth * .35));
            gridPayments.Columns[(int)PaymentCols.Reference].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private enum SummaryColumns
        {
            Id = 0,
            TransactionType = 1,
            TypeDescription = 2,
            Account = 3,
            Name = 4,
            Info = 5,
            Amount = 6,
            Split = 7
        }

        private void SummaryGrid_DoubleClick(object sender, EventArgs e)
        {
            if (SummaryGrid.Row > 0)
            {
                if (ViewModel.EditTransaction(
                    new Guid(SummaryGrid.TextMatrix(SummaryGrid.Row, (int)SummaryColumns.Id))))
                {
                    Close();
                }
            }
        }

        private void FillTransactionTypeCombo()
        {
            var receiptTypes = ViewModel.GetTransactionTypes();

            cmbTransactionType.Items.AddRange(receiptTypes.Select(r => new DescriptionIDPair() { ID = r.ID, Description = r.ID + "  " + r.Description }).ToArray());
            cmbTransactionType.ValueMember = "ID";
            cmbTransactionType.DisplayMember = "Description";

        }

        private void FillPaymentTypeCombo()
        {
            cmbPaymentType.Items.Clear();
            var paymentMethods = ViewModel.PaymentMethods;
            foreach (var paymentMethod in paymentMethods)
            {
                cmbPaymentType.Items.Add(paymentMethod);
            }
        }

        private void DefaultPaymentSection()
        {
            SetPaymentTypeTo(PaymentMethod.Cash);
            txtPayment.Text = ViewModel.RemainingDue.FormatAsMoney();
            txtCheckNumber.Text = "";
            cmbBank.SelectedItem = null;
        }

        private void SetPaymentTypeTo(PaymentMethod paymentMethod)
        {
            foreach (GenericDescriptionPair<PaymentMethod> paymentType in cmbPaymentType.Items)
            {
                if (paymentType.ID == paymentMethod)
                {
                    cmbPaymentType.SelectedItem = paymentType;
                    return;
                }
            }
        }

        private enum PaymentCols
        {
            Id = 0,
            PaymentType = 1,
            Amount = 2,
            Reference = 3
        }

        private void gridPayments_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (gridPayments.Row > 0)
                {
                    if (MessageBox.Show("Are you sure that you want to delete this payment?", "Delete Payment", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        ViewModel.RemovePayment(new Guid(gridPayments.TextMatrix(gridPayments.Row, (int)PaymentCols.Id)));
                    }
                }
            }
        }

        private void SummaryGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (SummaryGrid.Row > 0)
                {
                    if (MessageBox.Show("Are you sure that you want to delete this transaction?", "Delete Transaction", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        ViewModel.RemoveTransaction(new Guid(SummaryGrid.TextMatrix(SummaryGrid.Row, (int)SummaryColumns.Id)));
                    }
                }
            }
        }

        private void cmdVoidByReceipt_Click(object sender, EventArgs e)
        {
            object returnVal = "";
            if (frmInput.InstancePtr.Init(ref returnVal, "Void Receipt", "Please input the receipt number you wish to void and press OK.", intDataType: modGlobalConstants.InputDTypes.idtWholeNumber, startWithBlank:true))
            {
                int receiptNumber;
                if (int.TryParse(returnVal.ToString(), out receiptNumber))
                {
                    if (receiptNumber < 1)
                    {
                        return;
                    }
                    var result = ViewModel.ValidReceiptToVoid(receiptNumber);
                    if (result == ValidReceiptToVoidResult.OkToVoid)
                    {
                        ViewModel.VoidReceipt(receiptNumber);
                    }
                    else if (result == ValidReceiptToVoidResult.ReceiptContainsRestrictedTypes)
                    {
                        MessageBox.Show("This receipt has a restricted receipt type.", "Restricted Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (result == ValidReceiptToVoidResult.ReceiptAlreadyVoided)
                    {
                        MessageBox.Show("This receipt has already been voided.", "Already Voided", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        MessageBox.Show("Receipt Information not found in the database.", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void SummaryGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (SummaryGrid.Row > 0)
            {
                if (SummaryGrid.Col == (int)SummaryColumns.Split)
                {
                    SummaryGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                }
                else
                {
                    SummaryGrid.Editable = FCGrid.EditableSettings.flexEDNone;
                }
            }
            else
            {
                SummaryGrid.Editable = FCGrid.EditableSettings.flexEDNone;
            }
        }

        private void txtPaidBy_Validated(object sender, EventArgs e)
        {

        }

        private void txtCustNum_PaidBy_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
            {
                KeyAscii = (Keys)0;
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtCustNum_PaidBy_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Information.IsNumeric(txtCustNum_PaidBy.Text) && Conversion.Val(txtCustNum_PaidBy.Text) > 0)
            {
                GetPartyInfo(txtCustNum_PaidBy.Text.ToIntegerValue());
            }
            else
            {
                ClearPartyInfo();
            }
        }

        private void cmdEdit_PaidBy_Click(object sender, EventArgs e)
        {
            int lngID = 0;
            cmdEdit_PaidBy.Enabled = false;
            if (Information.IsNumeric(txtCustNum_PaidBy.Text) && Conversion.Val(txtCustNum_PaidBy.Text) > 0)
            {
                lngID = FCConvert.ToInt32(txtCustNum_PaidBy.Text);
                lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
                if (lngID > 0)
                {
                    txtCustNum_PaidBy.Text = FCConvert.ToString(lngID);
                    GetPartyInfo(lngID);
                }
            }
            cmdEdit_PaidBy.Enabled = true;
        }

        public void ClearPartyInfo()
        {
            txtCustNum_PaidBy.Text = "";
            imgMemo_PaidBy.Visible = false;
            cmdEdit_PaidBy.Enabled = false;
        }

        public void GetPartyInfo(int partyId)
        {
            cPartyController pCont = new cPartyController();
            cParty pInfo = new cParty();
            FCCollection pComments = new FCCollection();
            pInfo = pCont.GetParty(partyId);
            if (!(pInfo == null))
            {
                txtPaidBy.Text = pInfo.FullName;
                pComments = pInfo.GetModuleSpecificComments();
                if (pComments.Count > 0)
                {
                    imgMemo_PaidBy.Visible = true;
                }
                else
                {
                    imgMemo_PaidBy.Visible = false;
                }
                cmdEdit_PaidBy.Enabled = true;
            }
        }

        private void frmReceiptTransactionSummary_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if (e.CloseReason == FCCloseReason.FormControlMenu)
            {
                if (SummaryGrid.Rows > 1)
                {
                    if (MessageBox.Show("There are pending transactions.  Do you want to exit without processing?",
                            "Exit Screen", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }


        #region Overrides of Form

        /// <inheritdoc />
        protected override void OnWebRender(dynamic config)
        {
            base.OnWebRender((object)config);
            config.wiredEvents.Add("SendMessage(value)");
        }

        /// <inheritdoc />
        protected override void OnWebEvent(WisejEventArgs e)
        {
            base.OnWebEvent(e);

            switch (e.Type)
            {
                case "SendMessage":
                    if (e.Parameters.value != null)
                    {
                        AlertBox.Show(e.Parameters.value.ToString());
                    }
                    break;
            }
        }



        #endregion

    }
}
