﻿using System;
using System.Xml.Linq;
using fecherFoundation;
using Global;
using GrapeCity.ActiveReports.Data;
using SharedApplication;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using Wisej.Core;
using Wisej.Web;

namespace TWCR0000.ReceiptProcess
{
    public partial class frmPaymentPortal : BaseForm , IModalView<IGetPaymentFromPortalViewModel>
    {
        private bool cancelling = true;
        public frmPaymentPortal()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        public frmPaymentPortal(IGetPaymentFromPortalViewModel viewModel) : this()
        {
            this.ViewModel = viewModel;
            //var tempUri = new Uri(Application.Url);
            //ViewModel.ResponseURL = tempUri.GetLeftPart(System.UriPartial.Authority) + @"/";
        }

        private void InitializeComponentEx()
        {
            this.Shown += FrmPaymentPortal_Shown;
            this.wbPayPort.DocumentCompleted += WbPayPort_DocumentCompleted;
            this.Load += FrmPaymentPortal_Load;
            this.Closing += FrmPaymentPortal_Closing;
        }

        protected override void OnWebRender(dynamic config)
        {
            base.OnWebRender((object)config);
            config.wiredEvents.Add("SendMessage(value)");
        }
        protected override void OnWebEvent(WisejEventArgs e)
        {
            base.OnWebEvent(e);           
            switch (e.Type.ToLower())
            {
                case "sendmessage":
                    if (e.Parameters.value != null)
                    {
                        SetResponse(e.Parameters.value.tranResult.ToString());
                        if (!ViewModel.PortalResponse.Success)
                        {
                            FCMessageBox.Show(ViewModel.PortalResponse.ErrorDescription,
                                MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "Payment Cancelled");
                        }
                        CloseWithoutCancelling();
                    }
                    break;
            }
        }

        private void SetResponse(string transactionResult)
        {
            var xmlResult = XElement.Parse(transactionResult);
            //throw new NotImplementedException();
            var saleResponse = xmlResult.Element("makeSaleResponse");
            var resultCode = saleResponse.Element("ResultCode").Value.ToIntegerValue();
            switch (resultCode)
            {
                case 1:
                    ViewModel.PortalResponse.Success = true;
                    ViewModel.PortalResponse.ReferenceNumber = saleResponse.Element("transactionID")?.Value.ToString();
                    ViewModel.PortalResponse.AuthorizationCode =
                        saleResponse.Element("transactionStatus")?.Value.ToString();
                    ViewModel.PortalResponse.ExpirationDate =
                        saleResponse.Element("ccExpirationMonth")?.Value.ToString() + @"\" +
                        saleResponse.Element("ccExpirationYear")?.Value.ToString();
                    break;
                default:
                    ViewModel.PortalResponse.Success = false;
                    var errorMessage = saleResponse.Element("ErrorMessage")?.Value.ToString();
                    var errorDescription = saleResponse.Element("ErrorDescription")?.Value.ToString();
                    ViewModel.PortalResponse.ErrorCode = errorMessage;
                    ViewModel.PortalResponse.ErrorDescription = errorDescription;
                    
                    break;
            }

            ViewModel.PortalResponse.ResponseData = saleResponse.ToString();

        }
        private void FrmPaymentPortal_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancelled = true;
            }
        }

        private void CloseWithoutCancelling()
        {
            cancelling = false;
            ViewModel.Cancelled = false;
            this.Close();
        }
        private void FrmPaymentPortal_Shown(object sender, EventArgs e)
        {
            if (ViewModel.ShowAccountId)
            {
                this.Text = "Enter Payment for service code: " + ViewModel.AccountId;
            }
            else
            {
                this.Text = "Enter Payment";
            }

            //if (ViewModel.TransactionAmount > 0)
            //{
                wbPayPort.Navigate(ViewModel.SaleUrl);
            //}
            //else
            //{
            //    wbPayPort.Navigate(ViewModel.CreditUrl);
            //}
        }

        private void FrmPaymentPortal_Load(object sender, EventArgs e)
        {
            
        }
        
        private void WbPayPort_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            
            //really need to switch on document title           
            switch (e.Url.LocalPath.ToLower())
            {
                case @"/paymentportalresponse.html":
                    this.Eval($"AttachToIframe({wbPayPort.Handle},{this.Handle})");
                    break;
            }
        }

        public IGetPaymentFromPortalViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            Show(FormShowEnum.Modal);
        }

        
    }
}
