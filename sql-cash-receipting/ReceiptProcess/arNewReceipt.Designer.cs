﻿namespace TWCR0000.ReceiptProcess
{
	/// <summary>
	/// Summary description for arReceipt.
	/// </summary>
	partial class arNewReceipt
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arNewReceipt));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.sarReceiptDetailOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.lblPaidBy = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOtherAccounts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBottomComment = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheck1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSignature = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSignatureLine = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblTeller = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderAmt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTopComment = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderReceipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReprint = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.lblPaidBy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOtherAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBottomComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignatureLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderAmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTopComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReprint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.sarReceiptDetailOb});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.AfterPrint += new System.EventHandler(this.Detail_AfterPrint);
			// 
			// sarReceiptDetailOb
			// 
			this.sarReceiptDetailOb.CloseBorder = false;
			this.sarReceiptDetailOb.Height = 0.1666667F;
			this.sarReceiptDetailOb.Left = 0F;
			this.sarReceiptDetailOb.Name = "sarReceiptDetailOb";
			this.sarReceiptDetailOb.Report = null;
			this.sarReceiptDetailOb.Top = 0F;
			this.sarReceiptDetailOb.Width = 4.45F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblPaidBy,
            this.lblOtherAccounts,
            this.lblBottomComment,
            this.fldTotal,
            this.lblTotal,
            this.lblCheck1,
            this.lblSignature,
            this.lblSignatureLine});
			this.ReportFooter.Height = 2.583333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// lblPaidBy
			// 
			this.lblPaidBy.Height = 0.125F;
			this.lblPaidBy.HyperLink = null;
			this.lblPaidBy.Left = 1F;
			this.lblPaidBy.MultiLine = false;
			this.lblPaidBy.Name = "lblPaidBy";
			this.lblPaidBy.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblPaidBy.Text = "Paid By:";
			this.lblPaidBy.Top = 0.337F;
			this.lblPaidBy.Width = 2.438F;
			// 
			// lblOtherAccounts
			// 
			this.lblOtherAccounts.Height = 0.125F;
			this.lblOtherAccounts.HyperLink = null;
			this.lblOtherAccounts.Left = 1F;
			this.lblOtherAccounts.MultiLine = false;
			this.lblOtherAccounts.Name = "lblOtherAccounts";
			this.lblOtherAccounts.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblOtherAccounts.Text = "Remaining Balances:";
			this.lblOtherAccounts.Top = 0.6666667F;
			this.lblOtherAccounts.Width = 2.438F;
			// 
			// lblBottomComment
			// 
			this.lblBottomComment.Height = 0.125F;
			this.lblBottomComment.HyperLink = null;
			this.lblBottomComment.Left = 0F;
			this.lblBottomComment.MultiLine = false;
			this.lblBottomComment.Name = "lblBottomComment";
			this.lblBottomComment.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; white-space: no" +
    "wrap";
			this.lblBottomComment.Text = "Bottom Comment";
			this.lblBottomComment.Top = 1.5625F;
			this.lblBottomComment.Width = 2.375F;
			// 
			// fldTotal
			// 
			this.fldTotal.CanGrow = false;
			this.fldTotal.Height = 0.125F;
			this.fldTotal.Left = 3.5F;
			this.fldTotal.MultiLine = false;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; white-space: now" +
    "rap";
			this.fldTotal.Text = "Total";
			this.fldTotal.Top = 0.1666667F;
			this.fldTotal.Width = 0.938F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.125F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 2.875F;
			this.lblTotal.MultiLine = false;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblTotal.Text = "Total:";
			this.lblTotal.Top = 0.1666667F;
			this.lblTotal.Width = 0.625F;
			// 
			// lblCheck1
			// 
			this.lblCheck1.Height = 0.177F;
			this.lblCheck1.HyperLink = null;
			this.lblCheck1.Left = 0F;
			this.lblCheck1.Name = "lblCheck1";
			this.lblCheck1.Style = "font-family: \'Courier New\'";
			this.lblCheck1.Text = null;
			this.lblCheck1.Top = 2.0625F;
			this.lblCheck1.Width = 4.781F;
			// 
			// lblSignature
			// 
			this.lblSignature.Height = 0.125F;
			this.lblSignature.HyperLink = null;
			this.lblSignature.Left = 0F;
			this.lblSignature.MultiLine = false;
			this.lblSignature.Name = "lblSignature";
			this.lblSignature.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblSignature.Text = "Signature";
			this.lblSignature.Top = 0.9375F;
			this.lblSignature.Width = 2.438F;
			// 
			// lblSignatureLine
			// 
			this.lblSignatureLine.Height = 0.1875F;
			this.lblSignatureLine.HyperLink = null;
			this.lblSignatureLine.Left = 0F;
			this.lblSignatureLine.MultiLine = false;
			this.lblSignatureLine.Name = "lblSignatureLine";
			this.lblSignatureLine.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblSignatureLine.Text = "________________________________________";
			this.lblSignatureLine.Top = 1.3125F;
			this.lblSignatureLine.Width = 2.96875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTeller,
            this.lblHeaderType,
            this.lblHeaderRef,
            this.lblHeaderAmt,
            this.lblTopComment,
            this.lblHeader,
            this.lblDate,
            this.lblTime,
            this.lnHeader,
            this.lblReceiptNumber,
            this.lblHeaderReceipt,
            this.lblHeaderTitle,
            this.lblMuniName,
            this.lblReprint});
			this.PageHeader.Height = 1.25F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.BeforePrint += new System.EventHandler(this.PageHeader_BeforePrint);
			// 
			// lblTeller
			// 
			this.lblTeller.Height = 0.125F;
			this.lblTeller.HyperLink = null;
			this.lblTeller.Left = 2.3125F;
			this.lblTeller.MultiLine = false;
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblTeller.Text = "Teller";
			this.lblTeller.Top = 0.875F;
			this.lblTeller.Width = 1.188F;
			// 
			// lblHeaderType
			// 
			this.lblHeaderType.Height = 0.125F;
			this.lblHeaderType.HyperLink = null;
			this.lblHeaderType.Left = 0F;
			this.lblHeaderType.MultiLine = false;
			this.lblHeaderType.Name = "lblHeaderType";
			this.lblHeaderType.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; white-space: no" +
    "wrap";
			this.lblHeaderType.Text = "Type";
			this.lblHeaderType.Top = 1.0625F;
			this.lblHeaderType.Width = 0.813F;
			// 
			// lblHeaderRef
			// 
			this.lblHeaderRef.Height = 0.125F;
			this.lblHeaderRef.HyperLink = null;
			this.lblHeaderRef.Left = 2.125F;
			this.lblHeaderRef.MultiLine = false;
			this.lblHeaderRef.Name = "lblHeaderRef";
			this.lblHeaderRef.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblHeaderRef.Text = "Reference";
			this.lblHeaderRef.Top = 1.0625F;
			this.lblHeaderRef.Width = 1.125F;
			// 
			// lblHeaderAmt
			// 
			this.lblHeaderAmt.Height = 0.125F;
			this.lblHeaderAmt.HyperLink = null;
			this.lblHeaderAmt.Left = 3.5625F;
			this.lblHeaderAmt.MultiLine = false;
			this.lblHeaderAmt.Name = "lblHeaderAmt";
			this.lblHeaderAmt.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; white-space: now" +
    "rap";
			this.lblHeaderAmt.Text = "Amount";
			this.lblHeaderAmt.Top = 1.0625F;
			this.lblHeaderAmt.Width = 0.875F;
			// 
			// lblTopComment
			// 
			this.lblTopComment.Height = 0.125F;
			this.lblTopComment.HyperLink = null;
			this.lblTopComment.Left = 0F;
			this.lblTopComment.MultiLine = false;
			this.lblTopComment.Name = "lblTopComment";
			this.lblTopComment.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; white-space: no" +
    "wrap";
			this.lblTopComment.Text = "TopComment";
			this.lblTopComment.Top = 0.53125F;
			this.lblTopComment.Width = 2.438F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1770833F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.MultiLine = false;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Courier New\'; text-align: center; white-space: nowrap";
			this.lblHeader.Text = "Header";
			this.lblHeader.Top = 0.1770833F;
			this.lblHeader.Width = 2.4375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.125F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 0F;
			this.lblDate.MultiLine = false;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.8854167F;
			this.lblDate.Width = 1F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.125F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 1.1875F;
			this.lblTime.MultiLine = false;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; white-space: no" +
    "wrap";
			this.lblTime.Text = "Time";
			this.lblTime.Top = 0.875F;
			this.lblTime.Width = 0.938F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 1.239583F;
			this.lnHeader.Width = 4.375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 4.375F;
			this.lnHeader.Y1 = 1.239583F;
			this.lnHeader.Y2 = 1.239583F;
			// 
			// lblReceiptNumber
			// 
			this.lblReceiptNumber.Height = 0.125F;
			this.lblReceiptNumber.HyperLink = null;
			this.lblReceiptNumber.Left = 3.5625F;
			this.lblReceiptNumber.MultiLine = false;
			this.lblReceiptNumber.Name = "lblReceiptNumber";
			this.lblReceiptNumber.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: right; white-space: now" +
    "rap";
			this.lblReceiptNumber.Text = "ReceiptNumber";
			this.lblReceiptNumber.Top = 0.8854167F;
			this.lblReceiptNumber.Width = 0.875F;
			// 
			// lblHeaderReceipt
			// 
			this.lblHeaderReceipt.Height = 0.125F;
			this.lblHeaderReceipt.HyperLink = null;
			this.lblHeaderReceipt.Left = 0F;
			this.lblHeaderReceipt.MultiLine = false;
			this.lblHeaderReceipt.Name = "lblHeaderReceipt";
			this.lblHeaderReceipt.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblHeaderReceipt.Text = null;
			this.lblHeaderReceipt.Top = 0.8854167F;
			this.lblHeaderReceipt.Visible = false;
			this.lblHeaderReceipt.Width = 2.938F;
			// 
			// lblHeaderTitle
			// 
			this.lblHeaderTitle.Height = 0.125F;
			this.lblHeaderTitle.HyperLink = null;
			this.lblHeaderTitle.Left = 0F;
			this.lblHeaderTitle.MultiLine = false;
			this.lblHeaderTitle.Name = "lblHeaderTitle";
			this.lblHeaderTitle.Style = "font-family: \'Courier New\'; font-size: 8.5pt; white-space: nowrap";
			this.lblHeaderTitle.Text = null;
			this.lblHeaderTitle.Top = 1.0625F;
			this.lblHeaderTitle.Visible = false;
			this.lblHeaderTitle.Width = 2.917F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1770833F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.MultiLine = false;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center; white-space: now" +
    "rap";
			this.lblMuniName.Text = "MuniName";
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.4375F;
			// 
			// lblReprint
			// 
			this.lblReprint.Height = 0.125F;
			this.lblReprint.HyperLink = null;
			this.lblReprint.Left = 0F;
			this.lblReprint.MultiLine = false;
			this.lblReprint.Name = "lblReprint";
			this.lblReprint.Style = "font-family: \'Courier New\'; font-size: 8.5pt; text-align: center; white-space: no" +
    "wrap";
			this.lblReprint.Text = "***  REPRINT  ***";
			this.lblReprint.Top = 0.7083333F;
			this.lblReprint.Visible = false;
			this.lblReprint.Width = 2.438F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// arNewReceipt
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 4.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			((System.ComponentModel.ISupportInitialize)(this.lblPaidBy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOtherAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBottomComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignatureLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderAmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTopComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReprint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarReceiptDetailOb;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		public GrapeCity.ActiveReports.SectionReportModel.Label lblPaidBy;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOtherAccounts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBottomComment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheck1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSignature;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSignatureLine;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTeller;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderRef;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderAmt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTopComment;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReprint;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
