﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTellerDetail.
	/// </summary>
	partial class sarTellerDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarTellerDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBill = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTeller = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTellerHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblConvenienceFeeTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTeller = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAffect = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldConvenienceFeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTellerHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConvenienceFeeTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAffect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldName,
				this.fldAcct,
				this.fldBillNumber,
				this.fldReceiptNumber,
				this.fldDate,
				this.fldTeller,
				this.fldAffect,
				this.fldPaymentType,
				this.fldCode,
				this.fldTotals1,
				this.fldTotals2,
				this.fldTotals3,
				this.fldTotals4,
				this.fldTotals5,
				this.fldTotals6,
				this.fldTotals7,
				this.fldAccountNumber,
				this.lblAccountNumber,
				this.fldConvenienceFeeTotal
			});
			this.Detail.Height = 0.3854167F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblName,
				this.lblAcct,
				this.lblBill,
				this.lblReceipt,
				this.lblDate,
				this.lblTeller,
				this.lblNC,
				this.lblPayType,
				this.lblCode,
				this.lblTitle1,
				this.lblTitle2,
				this.lblTitle3,
				this.lblTitle4,
				this.lblTitle5,
				this.lblTitle6,
				this.lblTotal,
				this.Line1,
				this.lblTellerHeader,
				this.lblConvenienceFeeTitle
			});
			this.GroupHeader1.Height = 0.5208333F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.3125F;
			this.lblName.Visible = false;
			this.lblName.Width = 0.75F;
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.1875F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0.75F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblAcct.Text = "Acct #";
			this.lblAcct.Top = 0.3125F;
			this.lblAcct.Width = 0.4375F;
			// 
			// lblBill
			// 
			this.lblBill.Height = 0.1875F;
			this.lblBill.HyperLink = null;
			this.lblBill.Left = 1.1875F;
			this.lblBill.Name = "lblBill";
			this.lblBill.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblBill.Text = "Bill";
			this.lblBill.Top = 0.3125F;
			this.lblBill.Width = 0.3125F;
			// 
			// lblReceipt
			// 
			this.lblReceipt.Height = 0.1875F;
			this.lblReceipt.HyperLink = null;
			this.lblReceipt.Left = 1.5F;
			this.lblReceipt.Name = "lblReceipt";
			this.lblReceipt.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblReceipt.Text = "Recpt";
			this.lblReceipt.Top = 0.3125F;
			this.lblReceipt.Width = 0.375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.3125F;
			this.lblDate.Visible = false;
			this.lblDate.Width = 0.4375F;
			// 
			// lblTeller
			// 
			this.lblTeller.Height = 0.1875F;
			this.lblTeller.HyperLink = null;
			this.lblTeller.Left = 2.3125F;
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTeller.Text = "TLR";
			this.lblTeller.Top = 0.3125F;
			this.lblTeller.Width = 0.3125F;
			// 
			// lblNC
			// 
			this.lblNC.Height = 0.1875F;
			this.lblNC.HyperLink = null;
			this.lblNC.Left = 2.5625F;
			this.lblNC.Name = "lblNC";
			this.lblNC.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.lblNC.Text = "Cd/C";
			this.lblNC.Top = 0.3125F;
			this.lblNC.Width = 0.4375F;
			// 
			// lblPayType
			// 
			this.lblPayType.Height = 0.1875F;
			this.lblPayType.HyperLink = null;
			this.lblPayType.Left = 2.9375F;
			this.lblPayType.Name = "lblPayType";
			this.lblPayType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblPayType.Text = "CC/CK";
			this.lblPayType.Top = 0.3125F;
			this.lblPayType.Width = 0.40625F;
			// 
			// lblCode
			// 
			this.lblCode.Height = 0.1875F;
			this.lblCode.HyperLink = null;
			this.lblCode.Left = 3.3125F;
			this.lblCode.Name = "lblCode";
			this.lblCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblCode.Text = "Code";
			this.lblCode.Top = 0.3125F;
			this.lblCode.Width = 0.3125F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.1875F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 3.5625F;
			this.lblTitle1.MultiLine = false;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTitle1.Text = null;
			this.lblTitle1.Top = 0.3125F;
			this.lblTitle1.Width = 0.5625F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1875F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 4.125F;
			this.lblTitle2.MultiLine = false;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-family: \'1\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTitle2.Text = null;
			this.lblTitle2.Top = 0.3125F;
			this.lblTitle2.Width = 0.5625F;
			// 
			// lblTitle3
			// 
			this.lblTitle3.Height = 0.1875F;
			this.lblTitle3.HyperLink = null;
			this.lblTitle3.Left = 4.6875F;
			this.lblTitle3.MultiLine = false;
			this.lblTitle3.Name = "lblTitle3";
			this.lblTitle3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTitle3.Text = null;
			this.lblTitle3.Top = 0.3125F;
			this.lblTitle3.Width = 0.5625F;
			// 
			// lblTitle4
			// 
			this.lblTitle4.Height = 0.1875F;
			this.lblTitle4.HyperLink = null;
			this.lblTitle4.Left = 5.25F;
			this.lblTitle4.MultiLine = false;
			this.lblTitle4.Name = "lblTitle4";
			this.lblTitle4.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTitle4.Text = null;
			this.lblTitle4.Top = 0.3125F;
			this.lblTitle4.Width = 0.5625F;
			// 
			// lblTitle5
			// 
			this.lblTitle5.Height = 0.1875F;
			this.lblTitle5.HyperLink = null;
			this.lblTitle5.Left = 5.8125F;
			this.lblTitle5.MultiLine = false;
			this.lblTitle5.Name = "lblTitle5";
			this.lblTitle5.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTitle5.Text = null;
			this.lblTitle5.Top = 0.3125F;
			this.lblTitle5.Width = 0.5625F;
			// 
			// lblTitle6
			// 
			this.lblTitle6.Height = 0.1875F;
			this.lblTitle6.HyperLink = null;
			this.lblTitle6.Left = 6.375F;
			this.lblTitle6.MultiLine = false;
			this.lblTitle6.Name = "lblTitle6";
			this.lblTitle6.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTitle6.Text = null;
			this.lblTitle6.Top = 0.3125F;
			this.lblTitle6.Width = 0.5625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6.9375F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.3125F;
			this.lblTotal.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// lblTellerHeader
			// 
			this.lblTellerHeader.Height = 0.1875F;
			this.lblTellerHeader.HyperLink = null;
			this.lblTellerHeader.Left = 0F;
			this.lblTellerHeader.Name = "lblTellerHeader";
			this.lblTellerHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblTellerHeader.Text = null;
			this.lblTellerHeader.Top = 0F;
			this.lblTellerHeader.Width = 1F;
			// 
			// lblConvenienceFeeTitle
			// 
			this.lblConvenienceFeeTitle.Height = 0.1875F;
			this.lblConvenienceFeeTitle.HyperLink = null;
			this.lblConvenienceFeeTitle.Left = 4.09375F;
			this.lblConvenienceFeeTitle.MultiLine = false;
			this.lblConvenienceFeeTitle.Name = "lblConvenienceFeeTitle";
			this.lblConvenienceFeeTitle.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.lblConvenienceFeeTitle.Text = null;
			this.lblConvenienceFeeTitle.Top = 0.3125F;
			this.lblConvenienceFeeTitle.Visible = false;
			this.lblConvenienceFeeTitle.Width = 0.5625F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.430556F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0.75F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0.1875F;
			this.fldAcct.Width = 0.4375F;
			// 
			// fldBillNumber
			// 
			this.fldBillNumber.Height = 0.1875F;
			this.fldBillNumber.Left = 1.1875F;
			this.fldBillNumber.Name = "fldBillNumber";
			this.fldBillNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldBillNumber.Text = null;
			this.fldBillNumber.Top = 0.1875F;
			this.fldBillNumber.Width = 0.3125F;
			// 
			// fldReceiptNumber
			// 
			this.fldReceiptNumber.Height = 0.1875F;
			this.fldReceiptNumber.Left = 1.5F;
			this.fldReceiptNumber.Name = "fldReceiptNumber";
			this.fldReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldReceiptNumber.Text = null;
			this.fldReceiptNumber.Top = 0.1875F;
			this.fldReceiptNumber.Width = 0.375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 2.4375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 1F;
			// 
			// fldTeller
			// 
			this.fldTeller.Height = 0.1875F;
			this.fldTeller.Left = 2.3125F;
			this.fldTeller.Name = "fldTeller";
			this.fldTeller.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTeller.Text = null;
			this.fldTeller.Top = 0.1875F;
			this.fldTeller.Width = 0.3125F;
			// 
			// fldAffect
			// 
			this.fldAffect.CanGrow = false;
			this.fldAffect.Height = 0.1875F;
			this.fldAffect.Left = 2.625F;
			this.fldAffect.MultiLine = false;
			this.fldAffect.Name = "fldAffect";
			this.fldAffect.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.fldAffect.Text = null;
			this.fldAffect.Top = 0.1875F;
			this.fldAffect.Width = 0.40625F;
			// 
			// fldPaymentType
			// 
			this.fldPaymentType.Height = 0.1875F;
			this.fldPaymentType.Left = 3F;
			this.fldPaymentType.Name = "fldPaymentType";
			this.fldPaymentType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldPaymentType.Text = null;
			this.fldPaymentType.Top = 0.1875F;
			this.fldPaymentType.Width = 0.40625F;
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 3.1875F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldCode.Text = null;
			this.fldCode.Top = 0.1875F;
			this.fldCode.Width = 0.375F;
			// 
			// fldTotals1
			// 
			this.fldTotals1.Height = 0.1875F;
			this.fldTotals1.Left = 3.5625F;
			this.fldTotals1.Name = "fldTotals1";
			this.fldTotals1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTotals1.Text = null;
			this.fldTotals1.Top = 0.1875F;
			this.fldTotals1.Width = 0.5625F;
			// 
			// fldTotals2
			// 
			this.fldTotals2.Height = 0.1875F;
			this.fldTotals2.Left = 4.125F;
			this.fldTotals2.Name = "fldTotals2";
			this.fldTotals2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTotals2.Text = null;
			this.fldTotals2.Top = 0.1875F;
			this.fldTotals2.Width = 0.5625F;
			// 
			// fldTotals3
			// 
			this.fldTotals3.Height = 0.1875F;
			this.fldTotals3.Left = 4.6875F;
			this.fldTotals3.Name = "fldTotals3";
			this.fldTotals3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTotals3.Text = null;
			this.fldTotals3.Top = 0.1875F;
			this.fldTotals3.Width = 0.5625F;
			// 
			// fldTotals4
			// 
			this.fldTotals4.Height = 0.1875F;
			this.fldTotals4.Left = 5.25F;
			this.fldTotals4.Name = "fldTotals4";
			this.fldTotals4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTotals4.Text = null;
			this.fldTotals4.Top = 0.1875F;
			this.fldTotals4.Width = 0.5625F;
			// 
			// fldTotals5
			// 
			this.fldTotals5.Height = 0.1875F;
			this.fldTotals5.Left = 5.8125F;
			this.fldTotals5.Name = "fldTotals5";
			this.fldTotals5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTotals5.Text = null;
			this.fldTotals5.Top = 0.1875F;
			this.fldTotals5.Width = 0.5625F;
			// 
			// fldTotals6
			// 
			this.fldTotals6.Height = 0.1875F;
			this.fldTotals6.Left = 6.375F;
			this.fldTotals6.Name = "fldTotals6";
			this.fldTotals6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTotals6.Text = null;
			this.fldTotals6.Top = 0.1875F;
			this.fldTotals6.Width = 0.5625F;
			// 
			// fldTotals7
			// 
			this.fldTotals7.Height = 0.1875F;
			this.fldTotals7.Left = 6.9375F;
			this.fldTotals7.Name = "fldTotals7";
			this.fldTotals7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldTotals7.Text = null;
			this.fldTotals7.Top = 0.1875F;
			this.fldTotals7.Width = 0.5625F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 5.125F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldAccountNumber.Text = null;
			this.fldAccountNumber.Top = 0F;
			this.fldAccountNumber.Visible = false;
			this.fldAccountNumber.Width = 1.375F;
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.Height = 0.1875F;
			this.lblAccountNumber.HyperLink = null;
			this.lblAccountNumber.Left = 3.9375F;
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblAccountNumber.Text = "Account Number:";
			this.lblAccountNumber.Top = 0F;
			this.lblAccountNumber.Visible = false;
			this.lblAccountNumber.Width = 1.1875F;
			// 
			// fldConvenienceFeeTotal
			// 
			this.fldConvenienceFeeTotal.Height = 0.1875F;
			this.fldConvenienceFeeTotal.Left = 0F;
			this.fldConvenienceFeeTotal.Name = "fldConvenienceFeeTotal";
			this.fldConvenienceFeeTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldConvenienceFeeTotal.Text = null;
			this.fldConvenienceFeeTotal.Top = 0.1875F;
			this.fldConvenienceFeeTotal.Visible = false;
			this.fldConvenienceFeeTotal.Width = 0.5625F;
			// 
			// sarTellerDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTellerHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConvenienceFeeTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAffect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTeller;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAffect;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConvenienceFeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBill;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTeller;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNC;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTellerHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConvenienceFeeTitle;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
