﻿using System;
using fecherFoundation;
using Global;
using SharedApplication.CashReceipts;
using SharedApplication.Messaging;

namespace TWCR0000
{
    public class ShowMortgageHolderInCRHandler : CommandHandler<ShowMortgageHolderInCR>
    {
        protected override void Handle(ShowMortgageHolderInCR command)
        {
            frmMortgageHolder.InstancePtr.Init(command.Account, Convert.ToInt16(command.AccountType));
            frmMortgageHolder.InstancePtr.Show(App.MainForm);
        }
    }
}