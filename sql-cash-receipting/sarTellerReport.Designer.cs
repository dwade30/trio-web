﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTellerReport.
	/// </summary>
	partial class sarTellerReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarTellerReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.sarTellerDetailOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.lblTellerID = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.TellerBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTellerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TellerBinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.sarTellerDetailOB});
            this.Detail.Height = 0.1875F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // sarTellerDetailOB
            // 
            this.sarTellerDetailOB.CloseBorder = false;
            this.sarTellerDetailOB.Height = 0.1875F;
            this.sarTellerDetailOB.Left = 0F;
            this.sarTellerDetailOB.Name = "sarTellerDetailOB";
            this.sarTellerDetailOB.Report = null;
            this.sarTellerDetailOB.Top = 0F;
            this.sarTellerDetailOB.Width = 10F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Visible = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanGrow = false;
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1});
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Visible = false;
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.0625F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.3125F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "";
            this.Label1.Text = null;
            this.Label1.Top = 0F;
            this.Label1.Width = 0.25F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.CanShrink = true;
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTellerID,
            this.TellerBinder});
            this.GroupHeader1.DataField = "TellerBinder";
            this.GroupHeader1.Height = 0.21875F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // lblTellerID
            // 
            this.lblTellerID.Height = 0.2F;
            this.lblTellerID.HyperLink = null;
            this.lblTellerID.Left = 0F;
            this.lblTellerID.Name = "lblTellerID";
            this.lblTellerID.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblTellerID.Text = null;
            this.lblTellerID.Top = 0F;
            this.lblTellerID.Visible = false;
            this.lblTellerID.Width = 10F;
            // 
            // TellerBinder
            // 
            this.TellerBinder.DataField = "ArticleBinder";
            this.TellerBinder.DistinctField = "";
            this.TellerBinder.Height = 0.125F;
            this.TellerBinder.Left = 5.03646F;
            this.TellerBinder.Name = "TellerBinder";
            this.TellerBinder.SummaryGroup = "";
            this.TellerBinder.Text = "Field1";
            this.TellerBinder.Top = 0.046875F;
            this.TellerBinder.Visible = false;
            this.TellerBinder.Width = 0.6875F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.CanShrink = true;
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Visible = false;
            // 
            // sarTellerReport
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.76042F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTellerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TellerBinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarTellerDetailOB;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTellerID;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox TellerBinder;
    }
}
