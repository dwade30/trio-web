﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Linq;
using Wisej.Core;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using TWSharedLibrary;

namespace TWCR0000
{
	public class modUseCR
	{

		public const int SPI_GETFOREGROUNDLOCKTIMEOUT = 0x2000;
		public const int SPI_SETFOREGROUNDLOCKTIMEOUT = 0x2001;
		public const int SPIF_UPDATEINIFILE = 0x1;
		public const int SPIF_SENDWININICHANGE = 0x2;
		public const int SPIF_SENDCHANGE = SPIF_SENDWININICHANGE;

		//[DllImport("user32.dll", EntryPoint = "SystemParametersInfoA")]
		//public static extern int SystemParametersInfo(int uiAction, int uiParam, ref int pvParam, int fWinIni);
		// ----------------------------------------------------------------------------------------------------------
		public struct Receipt
		{
			public bool Used;
			// if True then I know that this array index is currently being used
			// vbPorter upgrade warning: Type As int	OnWrite(string, int)
			public int Type;
			public string Module;
			// P = Personal Property, R = Real Estate, W = UT Water, S = UT Sewer, X = Cash Receipting
			public string CollectionCode;
			// this will bring back the type of transaction from the module
			// vbPorter upgrade warning: Account As string	OnWrite(int, string)
			public string Account;
			public string TypeDescription;
			public string Reference;
			// vbPorter upgrade warning: Control1 As string	OnWrite(string, int)
			public string Control1;
			public string Control2;
			public string Control3;
			public bool EFT;
			// vbPorter upgrade warning: Fee1 As double	OnWrite(short, double, Decimal)
			public double Fee1;
			// fee amounts
			// vbPorter upgrade warning: Fee2 As double	OnWrite(short, double, Decimal)
			public double Fee2;
			// vbPorter upgrade warning: Fee3 As double	OnWrite(short, double, Decimal)
			public double Fee3;
			// vbPorter upgrade warning: Fee4 As double	OnWrite(short, double, Decimal)
			public double Fee4;
			// vbPorter upgrade warning: Fee5 As double	OnWrite(short, double, Decimal)
			public double Fee5;
			public double Fee6;
			public string FeeD1;
			// descriptions for the fees
			public string FeeD2;
			public string FeeD3;
			public string FeeD4;
			public string FeeD5;
			public string FeeD6;
			public double Total;
			// total of all the fees
			public string Name;
			public int NamePartyID;
			public string PaidBy;
			public DateTime Date;
			public short Copies;
			public string Comment;
			public int RecordKey;
			// this is the ID from whichever database it came from 0 if not from another module
			public int ReverseKey;
			// this is the key of the reversed payment from whichever database it came from, 0 otherwise   kk12192013 troar-61
			public int ArrayIndex;
			public int BillingYear;
			public bool AffectCash;
			public bool AffectCashDrawer;
			public string CashAlternativeAcct;
			// this is the account where the monies come from instead of incoming cash
			public int Split;
			public string DefaultCashAccount;
			// this is so that the cash coming in does not go to the General Cash Account
			public bool PrintReceipt;
			// this will tell if the user wants to print this payment...if there are multiple payments, then this will still print, but if all are turned off, then none will print
			public bool ShowDetail;
			// true if this receipt will show a detailed breakdown of fees
			public int Quantity;
			// this is the quantity of the receipts
			public string DefaultMIAccount;
			// this account will hold the account that will replace any M I accounts in the fees grid
			public int ResCode;
			// This is the code from the residence table that will show that there is
			// a different town being used for this transaction
			public int ARBillType;
			// MAL@20080611: Add storage for BillType ID from AR tables ; Tracker Reference: 14057
			public Decimal ConvenienceFee;
			public string ConvenienceFeeAccount;
			public string SeperateCreditCardGLAccount;
			public Decimal CardPaidAmount;
			public Decimal CheckPaidAmount;
			public Decimal CashPaidAmount;
			public string ConvenienceFeeType;
			public Decimal ConvenienceFeeFactor;
			public string EPmtAccountID;

			public Receipt(int unusedParam)
			{
				this.Account = string.Empty;
				this.AffectCash = false;
				this.AffectCashDrawer = false;
				this.ARBillType = 0;
				this.ArrayIndex = 0;
				this.BillingYear = 0;
				this.CardPaidAmount = 0;
				this.CashAlternativeAcct = string.Empty;
				this.CashPaidAmount = 0;
				this.CheckPaidAmount = 0;
				this.CollectionCode = string.Empty;
				this.Comment = string.Empty;
				this.Control1 = string.Empty;
				this.Control2 = string.Empty;
				this.Control3 = string.Empty;
				this.ConvenienceFee = 0;
				this.ConvenienceFeeAccount = string.Empty;
				this.ConvenienceFeeFactor = 0;
				this.ConvenienceFeeType = string.Empty;
				this.Copies = 0;
				this.Date = DateTime.FromOADate(0);
				this.DefaultCashAccount = string.Empty;
				this.DefaultMIAccount = string.Empty;
				this.EFT = false;
				this.EPmtAccountID = string.Empty;
				this.Fee1 = 0;
				this.Fee2 = 0;
				this.Fee3 = 0;
				this.Fee4 = 0;
				this.Fee5 = 0;
				this.Fee6 = 0;
				this.FeeD1 = string.Empty;
				this.FeeD2 = string.Empty;
				this.FeeD3 = string.Empty;
				this.FeeD4 = string.Empty;
				this.FeeD5 = string.Empty;
				this.FeeD6 = string.Empty;
				this.Module = string.Empty;
				this.Name = string.Empty;
				this.NamePartyID = 0;
				this.PaidBy = string.Empty;
				this.PrintReceipt = false;
				this.Quantity = 0;
				this.RecordKey = 0;
				this.Reference = string.Empty;
				this.ResCode = 0;
				this.ReverseKey = 0;
				this.SeperateCreditCardGLAccount = string.Empty;
				this.ShowDetail = false;
				this.Split = 0;
				this.Total = 0;
				this.Type = 0;
				this.TypeDescription = string.Empty;
				this.Used = false;
			}
		};

		public struct PaymentBreakdownInfo
		{
			public short ReceiptIndex;
			public int PaymentIndex;
			// vbPorter upgrade warning: ConvenienceFee As Decimal	OnWrite(short, double, Decimal)
			public Decimal ConvenienceFee;
			public Decimal CashAmount;
			public Decimal CardAmount;
			public Decimal CheckAmount;
			public string EPmtAcctID;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public PaymentBreakdownInfo(int unusedParam)
			{
				this.ReceiptIndex = 0;
				this.PaymentIndex = 0;
				this.ConvenienceFee = 0;
				this.CashAmount = 0;
				this.CardAmount = 0;
				this.CheckAmount = 0;
				this.EPmtAcctID = string.Empty;
			}
		};
	

		public static int GetNextReceiptNumber_2(int intTries)
		{
			return GetNextReceiptNumber(intTries);
		}

		public static int GetNextReceiptNumber(int intTries = 10)
		{
			int GetNextReceiptNumberRet = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will lock the Lock Table and get the number, then increment it for the next person
				string strSQL = "";
				clsDRWrapper rsLock = new clsDRWrapper();
				bool boolLock = false;
				int lngOpID;
				string strOPID;
				int lngLen;
				lngLen = 30;
				strOPID = Strings.StrDup(lngLen, " ");
				lngOpID = modAPIsConst.GetComputerNameWrp(ref strOPID, ref lngLen);
				if (lngOpID > 0)
				{
					strOPID = Strings.Left(strOPID, lngLen);
					// Read the T/F on Lockfile
					strSQL = "SELECT * FROM RNumLock";
					rsLock.OpenRecordset(strSQL);
					if (rsLock.EndOfFile() != true && rsLock.BeginningOfFile() != true)
					{
						if (intTries > 0)
						{
							boolLock = FCConvert.ToBoolean(rsLock.Get_Fields_Boolean("ReceiptNumberLock"));
							if (boolLock == false)
							{
								// if False
								// Then Write T and OP ID
								if (rsLock.Edit())
								{
									rsLock.Set_Fields("ReceiptNumberLock", true);
									rsLock.Set_Fields("OpID", strOPID);
									if (rsLock.Update(false))
									{
										// Read Rec #
										GetNextReceiptNumberRet = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLock.Get_Fields_Int32("NextReceiptNumber"))));
										// Incr Rec #
										rsLock.Edit();
										rsLock.Set_Fields("NextReceiptNumber", GetNextReceiptNumberRet + 1);
										// Write F in LockFile
										rsLock.Set_Fields("ReceiptNumberLock", false);
										rsLock.Update(false);
									}
									else
									{
										GetNextReceiptNumberRet = GetNextReceiptNumber_2(intTries - 1);
									}
								}
								else
								{
									GetNextReceiptNumberRet = GetNextReceiptNumber_2(intTries - 1);
								}
							}
							else
							{
								GetNextReceiptNumberRet = GetNextReceiptNumber_2(intTries - 1);
							}
						}
						else
						{
							// number of tries is 0 or less
							if (MessageBox.Show("The Receipt Number Lock has timed out.  It is currently being locked by " + rsLock.Get_Fields_String("OpID") + ".  Would you like to try again?", "CR Lock Time Out", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								GetNextReceiptNumberRet = GetNextReceiptNumber_2(10);
							}
							else
							{
								if (MessageBox.Show("If you have been locked out for a considerable amount of time, it is possible to force the record to unlock.  This action will be logged and the computer number and user will be recorded.  Would you like to automatically unlock record?", "Force Unlock", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									ForceUnlockReceiptNumberLock();
									GetNextReceiptNumberRet = GetNextReceiptNumber_2(10);
								}
								else
								{
									GetNextReceiptNumberRet = -1;
								}
							}
						}
					}
					else
					{
						// if there are no records
						if (rsLock.RecordCount() == 0)
						{
							// add a record
							rsLock.AddNew();
							rsLock.Set_Fields("ReceiptNumberLock", false);
							rsLock.Update(false);
						}
						// try the function again
						GetNextReceiptNumberRet = GetNextReceiptNumber_2(intTries - 1);
					}
				}
				else
				{
					// there is an error getting the computer name
					GetNextReceiptNumberRet = GetNextReceiptNumber_2(intTries - 1);
				}
				return GetNextReceiptNumberRet;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + " has occured attempting to find the next Receipt Number.", "CR Lock ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetNextReceiptNumberRet;
		}

		private static bool ForceUnlockReceiptNumberLock()
		{
			bool ForceUnlockReceiptNumberLock = false;
			// this sub will forcibly unlock the lock file and log the actions...
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsF = new clsDRWrapper();
				string strLocker = "";
				int lngOpID;
				string strOPID;
				int lngLen;
				string strText = "";
				// get the computer's name
				lngLen = 30;
				strOPID = Strings.StrDup(lngLen, " ");
				lngOpID = modAPIsConst.GetComputerNameWrp(ref strOPID, ref lngLen);
				// if there is a computer name then
				if (lngOpID > 0)
				{
					strOPID = Strings.Left(strOPID, lngLen);
					// cut the extra spaces off
					// create the string to print to the log file
					strText = "User: " + strOPID + " forcibly unlocked the CR Receipt Number Lock at " + DateTime.Today.ToShortTimeString() + " on " + DateTime.Today.ToShortDateString() + ".  It was currently locked by ";
					rsF.OpenRecordset("SELECT * FROM Lock");
					if (rsF.EndOfFile() != true && rsF.BeginningOfFile() != true)
					{
						rsF.Edit();
						rsF.Set_Fields("ReceiptNumberLock", false);
						rsF.Set_Fields("NextReceiptNumber", FCConvert.ToInt16(rsF.Get_Fields_Int32("NextReceiptNumber")) + 1);
						if (rsF.Update())
						{
							// if the update is successful then print the info to the log file
							strText += rsF.Get_Fields_String("OpID") + ".";
							FCFileSystem.FileOpen(1, "TWCR0000.log", OpenMode.Append, (OpenAccess)(-1), (OpenShare)(-1), -1);
							FCFileSystem.PrintLine(1, strText);
							FCFileSystem.FileClose(1);
							ForceUnlockReceiptNumberLock = true;
						}
						else
						{
							MessageBox.Show("Unable to unlock file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							ForceUnlockReceiptNumberLock = false;
						}
					}
					else
					{
						MessageBox.Show("Error forcing an unlock.  This file may be corrupt or empty.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						ForceUnlockReceiptNumberLock = false;
					}
				}
				else
				{
					MessageBox.Show("Error forcing an unlock.  Unable to obtain computer ID.", "Get Computer Name Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ForceUnlockReceiptNumberLock = false;
				}
				return ForceUnlockReceiptNumberLock;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				ForceUnlockReceiptNumberLock = false;
			}
			return ForceUnlockReceiptNumberLock;
		}

		
        private static int ReceiptTypeToInteger(string receiptType)
        {
            if (receiptType.ToIntegerValue() > 0)
            {
                return receiptType.ToIntegerValue();
            }
            switch (receiptType.ToUpper())
            {
                case "RETAX":
                    return 90;
                case "RETAXLIEN":
                    return 91;
                case "PPTAX":
                    return 92;
                case "TARETAXLIEN":
                    return 891;
                case "TARETAX":
                    return 890;
                case "DOG":
                    return 800;
                case "DEA":
                    return 801;
                case "BIR":
                    return 802;
                case "MAR":
                    return 803;
                case "BUR":
                    return 804;
            }
            return 0;
        }

		public static string GetInfoAboutOtherAccounts(IEnumerable<TransactionBase> transactions, bool isFirstCopy, bool isReprint, bool isBatch)
		{
			string GetInfoAboutOtherAccounts = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of accounts and the outstanding totals for those accounts
				double dblTotal = 0;
				int intNumAccounts = 0;
				int intTempNumAccounts = 0;
				bool boolError = false;
				int intCT;
				int lngGrpNum = 0;
				bool boolNoGroup = false;
				// this will be set to true if there is a type 90-92, but none of them are setup in a group
				int lngAcctNumber = 0;
				bool boolUsedGroupsAlready = false;
				bool boolCalcGroup = false;
				double dblTemp = 0;
				int intIndex;
				List<int> lngGroupNumbers = null;
				List<int> accountsCalculated = null;
				bool boolNoCalculate = false;
				bool boolGroupType = false;

				if (isReprint || !isFirstCopy || isBatch)
				{
					lngGrpNum = 0;
				}
				else
				{
					lngGroupNumbers = new List<int>();
					accountsCalculated = new List<int>();
					foreach (var transaction in transactions)
					{
						boolGroupType = false;
						int vbPorterVar = ReceiptTypeToInteger(transaction.TransactionTypeCode);
						int account = transaction.ToReceiptSummaryItem().Account;
						if ((vbPorterVar == 90) || (vbPorterVar == 890))
						{
							// RE
							boolGroupType = true;
							lngGrpNum = modGlobalFunctions.GetGroupNumber_6(account, "RE");
							if (lngGrpNum == 0)
							{
								boolNoGroup = true;
								lngAcctNumber = account;
							}
							else
							{
								lngGroupNumbers.Add(lngGrpNum);
							}

						}
						else if ((vbPorterVar == 91) || (vbPorterVar == 891))
						{
							// RE Lien
							boolGroupType = true;

							lngGrpNum = modGlobalFunctions.GetGroupNumber_6(account, "RE");
							if (lngGrpNum == 0)
							{
								boolNoGroup = true;
								lngAcctNumber = account;
							}
							else
							{
								lngGroupNumbers.Add(lngGrpNum);
							}
						}
						else if (vbPorterVar == 92)
						{
							// PP
							boolGroupType = true;

							lngGrpNum = modGlobalFunctions.GetGroupNumber_6(account, "PP");
							if (lngGrpNum == 0)
							{
								boolNoGroup = true;
								lngAcctNumber = account;
							}
							else
							{
								lngGroupNumbers.Add(lngGrpNum);
							}
						}
						else if ((vbPorterVar == 93) || (vbPorterVar == 94) || (vbPorterVar == 95) || (vbPorterVar == 96) || (vbPorterVar == 893) || (vbPorterVar == 894) || (vbPorterVar == 895) || (vbPorterVar == 896))
						{
							boolGroupType = true;

							lngGrpNum = modGlobalFunctions.GetGroupNumber_6(account, "UT");
							if (lngGrpNum == 0)
							{
								boolNoGroup = true;
								lngAcctNumber = account;
							}
							else
							{
								lngGroupNumbers.Add(lngGrpNum);
							}
						}
						else if (vbPorterVar == 97)
						{
							boolGroupType = true;
							lngGrpNum = 0;
							boolNoGroup = true;
							lngAcctNumber = ((AccountsReceivableTransactionBase) transaction).AccountNumber;
						}
						else
						{
							boolNoGroup = false;
						}


						if (boolGroupType)
						{
							if (lngGrpNum > 0)
							{
								dblTemp = 0;
								boolError = !modGlobal.GroupInformation(ref intTempNumAccounts, ref dblTemp, ref lngGrpNum);
								if (boolError)
								{
									// do not do anything
								}
								else
								{
									if (boolUsedGroupsAlready)
									{
										// this will look back to see any of the other accounts have the same group number
										// if the same group number is found on prior rows, then skip calculating this again
										boolCalcGroup = true;

										if (lngGroupNumbers.Select(x => x == lngGrpNum).Count() > 1)
										{
											boolCalcGroup = false;
											break;
										}
									}
									else
									{
										boolCalcGroup = true;
									}

									if (boolCalcGroup)
									{
										// this adds the group information
										dblTotal += dblTemp;
										// sum the totals
										intNumAccounts += intTempNumAccounts;
										// add the accounts from the group
										boolUsedGroupsAlready = true;
									}
								}
							}
							else
							{
								if (boolNoGroup)
								{
									// this is when the account has not been set up in a group, but it still can have a remaining balance
									boolNoCalculate = false;
									if(accountsCalculated.Contains(lngAcctNumber))
									{ 
										boolNoCalculate = true;
										break;
									}

									if (!boolNoCalculate)
									{
										if ((vbPorterVar == 90) || (vbPorterVar == 91) || (vbPorterVar == 890) || (vbPorterVar == 891))
										{
											// RE
											dblTotal += modCLCalculations.CalculateAccountTotal(lngAcctNumber, true, true);
										}
										else if (vbPorterVar == 92)
										{
											// PP
											dblTotal += modCLCalculations.CalculateAccountTotal(lngAcctNumber, false, true);
										}
										else if ((vbPorterVar == 93) || (vbPorterVar == 96) || (vbPorterVar == 893) || (vbPorterVar == 896))
										{
											// UT    kgk 07-06-2011 trocr-276  Added 893, 896
											dblTotal += modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAcctNumber), true, true);
											dblTotal += modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAcctNumber), false, true);
										}
										else if ((vbPorterVar == 94) || (vbPorterVar == 95) || (vbPorterVar == 894) || (vbPorterVar == 895))
										{
											// UT    kgk 07-06-2011 trocr-276  Added 894, 895
											dblTotal += modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAcctNumber), true, true);
											dblTotal += modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAcctNumber), false, true);
										}
										else if (vbPorterVar == 97)
										{
											// AR
											dblTotal += modARCalculations.CalculateAccountARTotal_2(modARStatusPayments.GetAccountKeyFromAccountNumber(lngAcctNumber));
										}

										accountsCalculated.Add(lngAcctNumber);
										intNumAccounts += 1;
									}
								}
							}
						}
					}
				}
				if (isReprint)
				{
					GetInfoAboutOtherAccounts = "REPRINT";
				}
				else if (!isFirstCopy)
				{
					GetInfoAboutOtherAccounts = "COPY";
				}
				else
				{
					if (dblTotal == 0)
					{
						GetInfoAboutOtherAccounts = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + "Remaining Balance: 0.00";
					}
					else if (intNumAccounts <= 1)
					{
						GetInfoAboutOtherAccounts = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + "Remaining Balance: " + Strings.Format(dblTotal, "#,##0.00");
					}
					else
					{
						GetInfoAboutOtherAccounts = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + "Remaining Balance: (" + FCConvert.ToString(intNumAccounts) + ") " + Strings.Format(dblTotal, "#,##0.00");
					}
				}
				return GetInfoAboutOtherAccounts;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Getting Group Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetInfoAboutOtherAccounts;
		}

		

		public static bool BreakdownCKRecords()
		{
			bool BreakdownCKRecords = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsUC = new clsDRWrapper();
				rsUC.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
				if (!rsUC.EndOfFile())
				{
					BreakdownCKRecords = FCConvert.ToBoolean(rsUC.Get_Fields_Boolean("BreakdownCKTypes"));
				}
				else
				{
					BreakdownCKRecords = false;
				}
				return BreakdownCKRecords;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Settings", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BreakdownCKRecords;
		}

		

		public class StaticVariables
		{
			//public string[] strTypeDescriptions = new string[999 + 1];

            public Receipt[] ReceiptArray = new Receipt[modStatusPayments.MAX_PAYMENTS + 1];
			
           
            public StaticVariables()
			{
				for (int i = 0; i < modStatusPayments.MAX_PAYMENTS; i++)
				{
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					ReceiptArray[i] = new Receipt(0);
				}
			}
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}

    }
}
