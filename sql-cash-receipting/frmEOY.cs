﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.IO.Compression;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmEOY.
	/// </summary>
	public partial class frmEOY : BaseForm
	{
		public frmEOY()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.imgDone = new System.Collections.Generic.List<fecherFoundation.FCPictureBox>();
			this.imgNotDone = new System.Collections.Generic.List<fecherFoundation.FCPictureBox>();
			this.lblEOY = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.imgDone.AddControlArrayElement(imgDone_3, 3);
			this.imgDone.AddControlArrayElement(imgDone_2, 2);
			this.imgDone.AddControlArrayElement(imgDone_1, 1);
			this.imgDone.AddControlArrayElement(imgDone_0, 0);
			this.imgNotDone.AddControlArrayElement(imgNotDone_3, 3);
			this.imgNotDone.AddControlArrayElement(imgNotDone_2, 2);
			this.imgNotDone.AddControlArrayElement(imgNotDone_1, 1);
			this.imgNotDone.AddControlArrayElement(imgNotDone_0, 0);
			this.lblEOY.AddControlArrayElement(lblEOY_3, 3);
			this.lblEOY.AddControlArrayElement(lblEOY_2, 2);
			this.lblEOY.AddControlArrayElement(lblEOY_1, 1);
			this.lblEOY.AddControlArrayElement(lblEOY_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEOY InstancePtr
		{
			get
			{
				return (frmEOY)Sys.GetInstance(typeof(frmEOY));
			}
		}

		protected frmEOY _InstancePtr = null;
		//FC:FINAL:AM: replace with .NET classes
		//public AbaleZipLibrary.AbaleZip AbaleZip1 = new AbaleZipLibrary.AbaleZip();
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/13/2006              *
		// ********************************************************
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			mnuFileCancel_Click();
		}

		private void cmdGo_Click(object sender, System.EventArgs e)
		{
			int lngErrorNumber = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMove = new clsDRWrapper();
				string strSQL = "";
				string strFrom = "";
				// check for non closed out receipts
				rsMove.OpenRecordset("SELECT * FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0");
				if (!rsMove.EndOfFile())
				{
					// If MsgBox("Are you sure that you want to process your End Of Year now?", vbYesNoCancel+, "Process End Of Year?") = vbYes Then
					MessageBox.Show("There are receipts that have not been closed out.  Please run your daily audit before running the End Of Year process.", "Pending Receipts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					// move the EOF process frame to the center of the screen and show it
					//fraUpdate.Left = FCConvert.ToInt32((this.Width - fraUpdate.Width) / 2.0);
					fraUpdate.Top = FCConvert.ToInt32((this.Height - fraUpdate.Height) / 3.0);
					//fraChoice.Left = FCConvert.ToInt32((this.Width - fraChoice.Width) / 2.0);
					fraChoice.Top = FCConvert.ToInt32((this.Height - fraChoice.Height) / 3.0);
					fraUpdate.Visible = true;
					fraChoice.Visible = false;
					frmEOY.InstancePtr.Refresh();
					if (EndOfYearCheck())
					{
						lngErrorNumber = 1;
						frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing End Of Year" + "\r\n" + "This may take a few minutes.");
						// MAL@20071107: Unload form after warning of delays ; it obscures the processing form
						frmWait.InstancePtr.Unload();
						modGlobalFunctions.AddCYAEntry_8("CR", "End Of Year Process: " + FCConvert.ToString(DateTime.Now));
						rsMove.DefaultDB = modExtraModules.strCRDatabase;
						strFrom = "Receipt WHERE Receipt.ReceiptKey IN (SELECT Receipt.ReceiptKey FROM Receipt INNER JOIN Archive ON Receipt.ReceiptKey = Archive.ReceiptNumber WHERE ISNULL(DailyCloseOut,0) <> 0)";
						lngErrorNumber = 2;
						// this will move the closed out information into LastYearReceipt
						if (MoveReceiptRecords())
						{
							// .Execute(strSQL, , False)
							imgNotDone[0].Visible = false;
							imgDone[0].Visible = true;
							// this will move the closed out information into LastYearArchive
							lngErrorNumber = 3;
							if (MoveArchiveRecords())
							{
								// .Execute("INSERT INTO LastYearArchive SELECT * FROM Archive WHERE DailyCloseOut <> 0", , False) Then
								imgNotDone[1].Visible = false;
								imgDone[1].Visible = true;
								// this will delete the information that was moved from the Receipt Table
								lngErrorNumber = 4;
								strSQL = "DELETE FROM " + strFrom;
								if (rsMove.Execute(strSQL, "TWCR0000.vb1", false))
								{
									// this will delete the information that was moved from the Archive Table
									imgNotDone[2].Visible = false;
									imgDone[2].Visible = true;
									lngErrorNumber = 5;
									if (rsMove.Execute("DELETE FROM Archive WHERE ISNULL(DailyCloseOut,0) <> 0", "TWCR0000.vb1", false))
									{
										imgNotDone[3].Visible = false;
										imgDone[3].Visible = true;
										lngErrorNumber = 6;
										// this will Move the CheckMaster and the CCMaster record
										lngErrorNumber = 7;
										MoveCheckMasterRecords();
										lngErrorNumber = 8;
										MoveCCMasterRecords();
										ArchiveMosesReceipts();
										lngErrorNumber = 8;
										frmWait.InstancePtr.Unload();
										MessageBox.Show("End of Year process has completed.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
										// Success!
										Close();
									}
									else
									{
										MessageBox.Show("An error has occured while deleting records from your current Archive.  Archive and Receipt records were moved to Last Year's Archive, but not removed from your current Archive.", "Add Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									}
								}
								else
								{
									MessageBox.Show("An error has occured while deleting records from your current Receipt Table.  Archive and Receipt records were moved to Last Year's Archive, but not removed from either.", "Add Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								}
							}
							else
							{
								MessageBox.Show("An error has occured while adding records to Last Year's Archive.  Receipt records were moved, but not Archive.", "Add Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
						}
						else
						{
							MessageBox.Show("An error has occured while adding records to Last Year's Archive.  No records were moved.", "Add Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
						frmWait.InstancePtr.Unload();
						MessageBox.Show("If receipt numbers should be reset, this can be done now by selecting File Maintenance > Customize > Reset Receipt Number", "Reset Receipt Numbers", MessageBoxButtons.OK, MessageBoxIcon.Information);
						// MsgBox "If you want you receipt numbers to be reset then do it now.", vbInformation, "Receipt Receipt Numbers"
					}
					else
					{
						MessageBox.Show("End Of Year unsuccessful.", "Failed End Of Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
						fraUpdate.Visible = false;
						fraChoice.Visible = true;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + " has occured during the End Of Year process." + "\r\n" + "");
				modGlobalFunctions.AddCYAEntry_8("CR", "Error processing End Of Year: " + FCConvert.ToString(lngErrorNumber));
			}
		}

		public void cmdGo_Click()
		{
			cmdGo_Click(cmdGo, new System.EventArgs());
		}

		private void ArchiveMosesReceipts()
		{
			int lngWhatHappened;
			// vbPorter upgrade warning: zipreturn As abeError	OnWrite(AbaleZipLibrary.abeError)	OnReadFCConvert.ToInt32(
			AbaleZipLibrary.abeError zipreturn;
			string strDBPath = "";
			string strBackupPath = "";
			int counter;
			if (Directory.Exists(FCFileSystem.Statics.UserDataFolder + "\\MOSESArchive\\"))
			{
				if (FCFileSystem.Dir(FCFileSystem.Statics.UserDataFolder + "\\MOSESArchive\\*.txt", 0) != "")
				{
					strDBPath = Strings.Trim(FCFileSystem.Statics.UserDataFolder);
					strBackupPath = FCFileSystem.Statics.UserDataFolder + "\\MOSESArchive\\";
					//AbaleZip1.BasePath = strBackupPath;
					//AbaleZip1.PreservePaths = false;
					//AbaleZip1.FilesToProcess = "*.txt";
					//AbaleZip1.ProcessSubfolders = false;
					//AbaleZip1.SpanMultipleDisks = AbaleZipLibrary.abeDiskSpanning.adsAlways;
					string zipFileName = "";
					if (File.Exists(strBackupPath + "EOYMOSESArchive" + Strings.Format(DateTime.Today, "MMddyyyy") + ".zip") == false)
					{
						zipFileName = strBackupPath + "EOYMOSESArchive" + Strings.Format(DateTime.Today, "MMddyyyy") + ".zip";
					}
					else
					{
						for (counter = 1; counter <= 1000; counter++)
						{
							if (File.Exists(strBackupPath + "EOYMOSESArchive" + Strings.Format(DateTime.Today, "MMddyyyy") + ".zp" + FCConvert.ToString(counter)) == false)
							{
								zipFileName = strBackupPath + "EOYMOSESArchive" + Strings.Format(DateTime.Today, "MMddyyyy") + ".zp" + FCConvert.ToString(counter);
								break;
							}
						}
					}
					//zipreturn = AbaleZip1.Zip();
					try
					{
                        fecherFoundation.ZipFile.CreateFromDirectory(strBackupPath, zipFileName);
						FCUtils.Download(zipFileName);
						File.Delete(strBackupPath + "*.txt");
					}
					catch (Exception ex)
					{
						MessageBox.Show("Archive of MOSES Receipts not Successful" + "\r\n" + ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					//Application.StartupPath = strDBPath;
					//if (zipreturn != 0)
					//{
					//    lngWhatHappened = FCConvert.ToInt32(zipreturn;
					//    MessageBox.Show("Archive of MOSES Receipts not Successful" + "\r\n" + AbaleZip1.GetErrorDescription(AbaleZipLibrary.abeValueType.avtError, FCConvert.ToInt32(zipreturn), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					//}
					//else
					//{
					//    File.Delete(strBackupPath + "*.txt");
					//}
				}
			}
		}

		private void cmdNo_Click(object sender, System.EventArgs e)
		{
			mnuFileCancel_Click();
		}
		//private void frmEOY_Activated(object sender, System.EventArgs e)
		//{
		//    // lblInstructions.Text = "Are you sure that you want to process your End of Year now?"
		//    lblInstructions.Text = "This option will save your current receipt records into an archive file.  It should be run after all activity is processed on the last day of the year and before any activity is entered for the new year.  Please backup if you have not done so.  Begin End of Year?";
		//}
		private void frmEOY_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmEOY_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEOY.FillStyle	= 0;
			//frmEOY.ScaleWidth	= 5880;
			//frmEOY.ScaleHeight	= 4380;
			//frmEOY.LinkTopic	= "Form2";
			//frmEOY.LockControls	= -1  'True;
			//frmEOY.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "End Of Year";
			ShowFrame(fraChoice);
		}

		private void frmEOY_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//if (MDIParent.InstancePtr.Visible && MDIParent.InstancePtr.Enabled)
			//{
			//    MDIParent.InstancePtr.Focus();
			//}
		}

		private void frmEOY_Resize(object sender, System.EventArgs e)
		{
			if (fraChoice.Visible)
			{
				ShowFrame(fraChoice);
			}
			if (fraUpdate.Visible)
			{
				ShowFrame(fraUpdate);
			}
		}

		private void mnuFileCancel_Click()
		{
			MessageBox.Show("You have cancelled the End of Year process.", "Cancelled End of Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			Close();
		}

		private void mnuFileContinue_Click(object sender, System.EventArgs e)
		{
			cmdGo_Click();
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(FCFrame fraFrame)
		{
			// this will place the frame in the middle of the form
			fraFrame.Top = 20;
			//fraFrame.Left = 30;
			fraFrame.Visible = true;
		}

		private bool MoveArchiveRecords()
		{
			int intErr = 0;
			bool MoveArchiveRecords = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will move all of the records in the Archive Table into the LastYearArchive Table
				clsDRWrapper rsFrom = new clsDRWrapper();
				clsDRWrapper rsTo = new clsDRWrapper();
				intErr = 1;
				rsFrom.OpenRecordset("SELECT * FROM Archive", modExtraModules.strCRDatabase);
				intErr = 2;
				rsTo.OpenRecordset("SELECT * FROM LastYearArchive WHERE ID = -1", modExtraModules.strCRDatabase);
				intErr = 3;
				while (!rsFrom.EndOfFile())
				{
					intErr = 4;
					rsTo.AddNew();
					intErr = 5;
					// kk07092014 trocrs-24  LastYearArchive.ID is set to AutoNumber Primary Key.       'rsTo.Fields("ID") = rsFrom.Fields("ID")
					// I can't find any reason that this has to match the Archive record.
					intErr = 6;
					rsTo.Set_Fields("ReceiptType", rsFrom.Get_Fields_Int32("ReceiptType"));
					intErr = 7;
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					rsTo.Set_Fields("AccountNumber", rsFrom.Get_Fields("AccountNumber"));
					intErr = 8;
					rsTo.Set_Fields("ReceiptTitle", rsFrom.Get_Fields_String("ReceiptTitle"));
					intErr = 9;
					rsTo.Set_Fields("Account1", rsFrom.Get_Fields_String("Account1"));
					intErr = 10;
					rsTo.Set_Fields("Account2", rsFrom.Get_Fields_String("Account2"));
					intErr = 11;
					rsTo.Set_Fields("Account3", rsFrom.Get_Fields_String("Account3"));
					intErr = 12;
					rsTo.Set_Fields("Account4", rsFrom.Get_Fields_String("Account4"));
					intErr = 13;
					rsTo.Set_Fields("Account5", rsFrom.Get_Fields_String("Account5"));
					intErr = 14;
					rsTo.Set_Fields("Account6", rsFrom.Get_Fields_String("Account6"));
					intErr = 15;
					// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					rsTo.Set_Fields("Amount1", rsFrom.Get_Fields("Amount1"));
					intErr = 16;
					// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					rsTo.Set_Fields("Amount2", rsFrom.Get_Fields("Amount2"));
					intErr = 17;
					// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
					rsTo.Set_Fields("Amount3", rsFrom.Get_Fields("Amount3"));
					intErr = 18;
					// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					rsTo.Set_Fields("Amount4", rsFrom.Get_Fields("Amount4"));
					intErr = 19;
					// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
					rsTo.Set_Fields("Amount5", rsFrom.Get_Fields("Amount5"));
					intErr = 20;
					// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
					rsTo.Set_Fields("Amount6", rsFrom.Get_Fields("Amount6"));
					intErr = 21;
					rsTo.Set_Fields("Comment", rsFrom.Get_Fields_String("Comment"));
					intErr = 22;
					rsTo.Set_Fields("Ref", rsFrom.Get_Fields_String("Ref"));
					intErr = 23;
					rsTo.Set_Fields("Control1", rsFrom.Get_Fields_String("Control1"));
					intErr = 24;
					rsTo.Set_Fields("Control2", rsFrom.Get_Fields_String("Control2"));
					intErr = 25;
					rsTo.Set_Fields("Control3", rsFrom.Get_Fields_String("Control3"));
					intErr = 26;
					rsTo.Set_Fields("ReceiptNumber", FCConvert.ToString(Conversion.Val("0" + rsFrom.Get_Fields_Int32("ReceiptNumber"))));
					intErr = 27;
					rsTo.Set_Fields("Split", FCConvert.ToString(Conversion.Val(rsFrom.Get_Fields_Int16("Split"))));
					intErr = 28;
					rsTo.Set_Fields("TellerID", rsFrom.Get_Fields_String("TellerID"));
					intErr = 29;
					rsTo.Set_Fields("Name", rsFrom.Get_Fields_String("Name"));
					intErr = 30;
					rsTo.Set_Fields("LastYearArchiveDate", rsFrom.Get_Fields_DateTime("ArchiveDate"));
					intErr = 31;
					if (Information.IsDate(rsFrom.Get_Fields("ActualSystemDate")))
					{
						rsTo.Set_Fields("ActualSystemDate", rsFrom.Get_Fields_DateTime("ActualSystemDate"));
					}
					else
					{
						rsTo.Set_Fields("ActualSystemDate", null);
					}
					intErr = 32;
					rsTo.Set_Fields("CollectionCode", rsFrom.Get_Fields_String("CollectionCode"));
					intErr = 33;
					rsTo.Set_Fields("TellerCloseOut", rsFrom.Get_Fields_Int32("TellerCloseOut"));
					intErr = 34;
					rsTo.Set_Fields("DailyCloseOut", rsFrom.Get_Fields_Int32("DailyCloseOut"));
					intErr = 35;
					rsTo.Set_Fields("AffectCashDrawer", rsFrom.Get_Fields_Boolean("AffectCashDrawer"));
					intErr = 36;
					rsTo.Set_Fields("AffectCash", rsFrom.Get_Fields_Boolean("AffectCash"));
					intErr = 37;
					rsTo.Set_Fields("DefaultAccount", rsFrom.Get_Fields_String("DefaultAccount"));
					intErr = 38;
					rsTo.Set_Fields("ArchiveCode", FCConvert.ToString(Conversion.Val(rsFrom.Get_Fields_Int32("ArchiveCode") + "")));
					intErr = 39;
					rsTo.Set_Fields("DefaultCashAccount", rsFrom.Get_Fields_String("DefaultCashAccount"));
					intErr = 40;
					// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
					rsTo.Set_Fields("Quantity", FCConvert.ToString(Conversion.Val(rsFrom.Get_Fields("Quantity"))));
					intErr = 41;
					rsTo.Set_Fields("DefaultMIAccount", rsFrom.Get_Fields_String("DefaultMIAccount"));
					intErr = 42;
					rsTo.Set_Fields("EFT", rsFrom.Get_Fields_String("EFT"));
					rsTo.Set_Fields("TownKey", rsFrom.Get_Fields_String("TownKey"));
					rsTo.Set_Fields("Project1", rsFrom.Get_Fields_String("Project1"));
					rsTo.Set_Fields("Project2", rsFrom.Get_Fields_String("Project2"));
					rsTo.Set_Fields("Project3", rsFrom.Get_Fields_String("Project3"));
					rsTo.Set_Fields("Project4", rsFrom.Get_Fields_String("Project4"));
					rsTo.Set_Fields("Project5", rsFrom.Get_Fields_String("Project5"));
					rsTo.Set_Fields("Project6", rsFrom.Get_Fields_String("Project6"));
					rsTo.Set_Fields("ARBillType", rsFrom.Get_Fields_String("ARBillType"));
					rsTo.Set_Fields("SeperateCreditCardGLAccount", rsFrom.Get_Fields_String("SeperateCreditCardGLAccount"));
					rsTo.Set_Fields("ConvenienceFee", rsFrom.Get_Fields_String("ConvenienceFee"));
					rsTo.Set_Fields("ConvenienceFeeGLAccount", rsFrom.Get_Fields_String("ConvenienceFeeGLAccount"));
					rsTo.Set_Fields("CardPaidAmount", rsFrom.Get_Fields_String("CardPaidAmount"));
					rsTo.Set_Fields("CheckPaidAmount", rsFrom.Get_Fields_String("CheckPaidAmount"));
					rsTo.Set_Fields("CashPaidAmount", rsFrom.Get_Fields_String("CashPaidAmount"));
					rsTo.Set_Fields("NamePartyID", rsFrom.Get_Fields_String("NamePartyID"));
					if (!rsTo.Update())
					{
						MoveArchiveRecords = false;
						return MoveArchiveRecords;
					}
					intErr = 43;
					rsFrom.MoveNext();
				}
				MoveArchiveRecords = true;
				return MoveArchiveRecords;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MoveArchiveRecords = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Moving Archive Records - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MoveArchiveRecords;
		}

		private bool MoveReceiptRecords()
		{
			int intErr = 0;
			bool MoveReceiptRecords = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will move all of the records in the Receipt Table into the LastYearReceipt Table
				clsDRWrapper rsFrom = new clsDRWrapper();
				clsDRWrapper rsTo = new clsDRWrapper();
				intErr = 1;
				rsFrom.OpenRecordset("SELECT * FROM Receipt", modExtraModules.strCRDatabase);
				intErr = 2;
				rsTo.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ID = -1", modExtraModules.strCRDatabase);
				intErr = 3;
				while (!rsFrom.EndOfFile())
				{
					rsTo.AddNew();
					intErr = 10;
					rsTo.Set_Fields("ReceiptKey", rsFrom.Get_Fields_Int32("ReceiptKey"));
					intErr = 11;
					rsTo.Set_Fields("ReceiptNumber", FCConvert.ToString(Conversion.Val("0" + rsFrom.Get_Fields_Int32("ReceiptNumber"))));
					intErr = 12;
					rsTo.Set_Fields("TellerID", rsFrom.Get_Fields_String("TellerID"));
					intErr = 13;
					rsTo.Set_Fields("LastYearReceiptDate", rsFrom.Get_Fields_DateTime("ReceiptDate").ToString("MM/dd/yy h:mm:ss tt"));
					intErr = 14;
					rsTo.Set_Fields("PaidBy", rsFrom.Get_Fields_String("PaidBy"));
					intErr = 15;
					rsTo.Set_Fields("Change", rsFrom.Get_Fields_Double("Change"));
					intErr = 16;
					rsTo.Set_Fields("CashAmount", rsFrom.Get_Fields_Double("CashAmount"));
					intErr = 17;
					rsTo.Set_Fields("CheckAmount", rsFrom.Get_Fields_Double("CheckAmount"));
					intErr = 18;
					rsTo.Set_Fields("CardAmount", rsFrom.Get_Fields_Double("CardAmount"));
					intErr = 19;
					rsTo.Set_Fields("MultipleChecks", rsFrom.Get_Fields_Boolean("MultipleChecks"));
					intErr = 20;
					rsTo.Set_Fields("MultipleCC", rsFrom.Get_Fields_Boolean("MultipleCC"));
					intErr = 21;
					rsTo.Set_Fields("EFT", rsFrom.Get_Fields_String("EFT"));
					rsTo.Set_Fields("IsEPayment", rsFrom.Get_Fields_String("IsEPayment"));
					rsTo.Set_Fields("ConvenienceFee", rsFrom.Get_Fields_String("ConvenienceFee"));
					rsTo.Set_Fields("PaidByPartyID", rsFrom.Get_Fields_String("PaidByPartyID"));
					if (!rsTo.Update())
					{
						MoveReceiptRecords = false;
						return MoveReceiptRecords;
					}
					intErr = 22;
					rsFrom.MoveNext();
				}
				MoveReceiptRecords = true;
				return MoveReceiptRecords;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MoveReceiptRecords = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Moving Receipt Records - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MoveReceiptRecords;
		}

		private bool MoveCheckMasterRecords()
		{
			try
			{
				clsDRWrapper rsFrom = new clsDRWrapper();
				clsDRWrapper rsTo = new clsDRWrapper();
				int intErr = 1;
				rsFrom.OpenRecordset("SELECT * FROM CheckMaster", modExtraModules.strCRDatabase);
				intErr = 2;
				rsTo.OpenRecordset("SELECT * FROM LastYearCheckMaster WHERE ID = -1", modExtraModules.strCRDatabase);
				while (!rsFrom.EndOfFile())
				{
					intErr = 4;
					rsTo.AddNew();
					intErr = 5;
					rsTo.Set_Fields("ReceiptNumber", rsFrom.Get_Fields_Int32("ReceiptNumber"));
					intErr = 6;
					rsTo.Set_Fields("CheckNumber", rsFrom.Get_Fields_String("CheckNumber"));
					intErr = 7;
					rsTo.Set_Fields("BankNumber", rsFrom.Get_Fields_Int32("BankNumber"));
					intErr = 8;
					rsTo.Set_Fields("Amount", rsFrom.Get_Fields_Decimal( "Amount"));
					intErr = 9;
					rsTo.Set_Fields("EFT", rsFrom.Get_Fields_Boolean("EFT"));
					intErr = 10;
					rsTo.Set_Fields("EPymtRefNumber", rsFrom.Get_Fields_String("EPymtRefNumber"));
					intErr = 11;
					rsTo.Set_Fields("Last4Digits", rsFrom.Get_Fields_String("Last4Digits"));
					intErr = 12;
					rsTo.Set_Fields("AccountType", rsFrom.Get_Fields_String("AccountType"));
					intErr = 13;
					rsTo.Set_Fields("OriginalReceiptKey", rsFrom.Get_Fields_Int32("OriginalReceiptKey"));
					intErr = 14;

					if (!rsTo.Update())
					{
						return false;
					}

					intErr = 15;
					rsFrom.MoveNext();
				}

				intErr = 16;
				if (!rsFrom.Execute("DELETE FROM CheckMaster", modExtraModules.strCRDatabase, false))
				{
					return false;
				}
			}
			catch (Exception e)
			{
				// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Moving CheckMaster Records - " & intErr
				return false;
			}

			return true;
		}

		private bool MoveCCMasterRecords()
		{
			try
			{
				clsDRWrapper rsFrom = new clsDRWrapper();
				clsDRWrapper rsTo = new clsDRWrapper();
				int intErr = 1;
				rsFrom.OpenRecordset("SELECT * FROM CCMaster", modExtraModules.strCRDatabase);
				intErr = 2;
				rsTo.OpenRecordset("SELECT * FROM LastYearCCMaster WHERE ID = -1", modExtraModules.strCRDatabase);
				while (!rsFrom.EndOfFile())
				{
					intErr = 4;
					rsTo.AddNew();
					intErr = 5;
					rsTo.Set_Fields("ReceiptNumber", rsFrom.Get_Fields_Int32("ReceiptNumber"));
					intErr = 6;
					rsTo.Set_Fields("CCType", rsFrom.Get_Fields_String("CCType"));
					intErr = 7;
					rsTo.Set_Fields("Amount", rsFrom.Get_Fields_Decimal("Amount"));
					intErr = 8;
					rsTo.Set_Fields("EPymtRefNumber", rsFrom.Get_Fields_String("EPymtRefNumber"));
					intErr = 9;
					rsTo.Set_Fields("Last4Digits", rsFrom.Get_Fields_String("Last4Digits"));
					intErr = 10;
					rsTo.Set_Fields("OriginalReceiptKey", rsFrom.Get_Fields_Int32("OriginalReceiptKey"));
					intErr = 11;
					rsTo.Set_Fields("CryptCard", rsFrom.Get_Fields_String("CryptCard"));
					intErr = 12;
					rsTo.Set_Fields("CryptCardExpiration", rsFrom.Get_Fields_String("CryptCardExpiration"));
					intErr = 13;
					rsTo.Set_Fields("ConvenienceFee", rsFrom.Get_Fields_Decimal("ConvenienceFee"));
					intErr = 14;
					rsTo.Set_Fields("MaskedCreditCardNumber", rsFrom.Get_Fields_String("MaskedCreditCardNumber"));
					intErr = 15;
					rsTo.Set_Fields("AuthCode", rsFrom.Get_Fields_String("AuthCode"));
					intErr = 16;
					rsTo.Set_Fields("IsVISA", rsFrom.Get_Fields_Boolean("IsVISA"));
					intErr = 17;
					rsTo.Set_Fields("IsDebit", rsFrom.Get_Fields_Boolean("IsDebit"));
					intErr = 18;
					rsTo.Set_Fields("NonTaxAmount", rsFrom.Get_Fields_Decimal("NonTaxAmount"));
					intErr = 19;
					rsTo.Set_Fields("AuthCodeConvenienceFee", rsFrom.Get_Fields_String("AuthCodeConvenienceFee"));
					intErr = 20;
					rsTo.Set_Fields("AuthCodeVISANonTax", rsFrom.Get_Fields_String("AuthCodeVISANonTax"));
					intErr = 21;
					rsTo.Set_Fields("NonTaxConvenienceFee", rsFrom.Get_Fields_Decimal("NonTaxConvenienceFee"));
					intErr = 22;
					rsTo.Set_Fields("AuthCodeNonTaxConvenienceFee", rsFrom.Get_Fields_Decimal("AuthCodeNonTaxConvenienceFee"));
					intErr = 23;
					rsTo.Set_Fields("SecureGUID", rsFrom.Get_Fields_String("SecureGUID"));
					intErr = 24;
					rsTo.Set_Fields("EPmtAcctID", rsFrom.Get_Fields_String("EPmtAcctID"));
					intErr = 25;
					if (!rsTo.Update())
					{
						return false;
					}

					intErr = 26;
					rsFrom.MoveNext();
				}

				intErr = 27;
				if (!rsFrom.Execute("DELETE FROM CCMaster", modExtraModules.strCRDatabase, false))
				{
					return false;
				}
			}
			catch (Exception e)
			{
				// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Moving CCMaster Records - " & intErr
				return false;
			}

			return true;
		}

		private bool EndOfYearCheck()
		{
			bool EndOfYearCheck = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBD = new clsDRWrapper();
				DateTime dtDate;
				EndOfYearCheck = true;
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsBD.OpenRecordset("SELECT * FROM EOYProcess", modExtraModules.strBDDatabase);
					if (!rsBD.EndOfFile())
					{
						// MAL@20080630: Add check to make sure that a date exists
						if (Information.IsDate(rsBD.Get_Fields("EOYCompletedDate")))
						{
							dtDate = (DateTime)rsBD.Get_Fields_DateTime("EOYCompletedDate");
							if (DateAndTime.DateDiff("M", dtDate, DateTime.Today) < 9)
							{
								EndOfYearCheck = true;
							}
							else
							{
								MessageBox.Show("The Budgetary End Of Year must be completed before running the Cash Receipts End Of Year.", "End Of Year Check", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								EndOfYearCheck = false;
							}
						}
						else
						{
							MessageBox.Show("The Budgetary End Of Year must be completed before running the Cash Receipts End Of Year.", "End Of Year Check", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							EndOfYearCheck = false;
						}
					}
					else
					{
						EndOfYearCheck = false;
					}
				}
				return EndOfYearCheck;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				EndOfYearCheck = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error End Of Year Check", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return EndOfYearCheck;
		}
	}
}
