﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCreditCard.
	/// </summary>
	public partial class frmCreditCard : BaseForm
	{
		public frmCreditCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreditCard InstancePtr
		{
			get
			{
				return (frmCreditCard)Sys.GetInstance(typeof(frmCreditCard));
			}
		}

		protected frmCreditCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/12/2002              *
		// ********************************************************
		clsDRWrapper rsCard = new clsDRWrapper();
		bool boolLoaded;

		private void frmCreditCard_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				FormatGrid();
				FillGrid();
				boolLoaded = true;
			}
		}

		private void frmCreditCard_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
			// Case vbKeyN
			// If Shift = 2 Then       'control is pressed
			// KeyCode = 0
			// mnuFileAdd_Click
			// End If
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						// mnuFileSaveExit_Click
						// Case vbKeyF9
						// KeyCode = 0
						// mnuFileSave_Click
						break;
					}
			}
			//end switch
		}

		private void frmCreditCard_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreditCard.FillStyle	= 0;
			//frmCreditCard.ScaleWidth	= 3885;
			//frmCreditCard.ScaleHeight	= 2460;
			//frmCreditCard.LinkTopic	= "Form2";
			//frmCreditCard.LockControls	= -1  'True;
			//frmCreditCard.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// this will set the size of the form to the smaller TRIO size (1/2 normal)
			modGlobalFunctions.SetTRIOColors(this);
			// this will set all the TRIO colors
			// the gets all of the type from the CCType table
			rsCard.OpenRecordset("SELECT * FROM CCType WHERE SystemDefined = 0", modExtraModules.strCRDatabase);
			if (rsCard.EndOfFile())
			{
				// there are no cc types is the table so you can't delete any
				cmdDelete.Enabled = false;
			}
		}

		private void frmCreditCard_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int lngCT;
			bool boolDirty = false;
			for (lngCT = 0; lngCT <= vsCC.Rows - 1; lngCT++)
			{
				if (Conversion.Val(vsCC.TextMatrix(lngCT, 3)) == -1)
				{
					boolDirty = true;
					break;
				}
			}
			if (boolDirty)
			{
				switch (MessageBox.Show("Changes have been made.  Would you like to save them before you exit?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							// save the changes and exit
							SaveInfo();
							break;
						}
					case DialogResult.No:
						{
							// do not save the changes and exit
							break;
						}
					case DialogResult.Cancel:
						{
							// do not save and cancel the exit
							e.Cancel = true;
							break;
						}
				}
				//end switch
			}
			if (e.Cancel)
			{
				// do nothing
			}
			else
			{
				boolLoaded = false;
				//if (MDIParent.InstancePtr.Visible && MDIParent.InstancePtr.Enabled)
				//{
				//    MDIParent.InstancePtr.Focus();
				//}
			}
		}

		private void frmCreditCard_Resize(object sender, System.EventArgs e)
		{
			//FC:FINAL:SBE - grid already anchored
			//int lngRows;
			//int lngRowHt = 0;
			//lngRows = vsCC.Rows;
			//if (lngRows > 0)
			//{
			//    lngRowHt = vsCC.RowHeight(0);
			//    if ((lngRows * lngRowHt) + 70 > frmCreditCard.InstancePtr.Height * 0.9)
			//    {
			//        vsCC.Height = FCConvert.ToInt32(frmCreditCard.InstancePtr.Height * 0.9);
			//    }
			//    else
			//    {
			//        vsCC.Height = (lngRows * lngRowHt) + 70;
			//    }
			//}
		}

		private void mnuFileAdd_Click(object sender, System.EventArgs e)
		{
			AddCreditCard();
		}

		public void mnuFileAdd_Click()
		{
			mnuFileAdd_Click(mnuFileAdd, new System.EventArgs());
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// this will delete the card from the DB and remove the row
			DeleteCreditCard();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void AddCreditCard()
		{
			// this routine will add a new type to the table and allow editing
			vsCC.AddItem("");
			// this will add another row to the grid
			vsCC.Select(vsCC.Rows - 1, 1);
			cmdDelete.Enabled = true;
			AdjustGridHeight();
		}

		private void DeleteCreditCard()
		{
			// this routine will delete the selected credit card type
			if (vsCC.Row > 0)
			{
				if (MessageBox.Show("Are you sure that you would like to delete " + vsCC.TextMatrix(vsCC.Row, 1) + "?", "Delete Credit/Debit Card", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// delete the card
					if (Conversion.Val(vsCC.TextMatrix(vsCC.Row, 0)) != 0)
					{
						rsCard.FindFirstRecord("ID", vsCC.TextMatrix(vsCC.Row, 0));
						if (rsCard.NoMatch)
						{
							// there is no record that is associated with this card yet
						}
						else
						{
							rsCard.Delete();
						}
					}
					else
					{
						// there is no record that is associated with this card yet
					}
					// remove the row
					vsCC.RemoveItem(vsCC.Row);
					// this will remove the row selected
				}
				else
				{
					// do nothing
				}
			}
			if (vsCC.Rows == 1)
			{
				cmdDelete.Enabled = false;
			}
			AdjustGridHeight();
		}

		private void FormatGrid()
		{
			// this routine will size and align the grid
			int lngWid = 0;
			lngWid = vsCC.WidthOriginal;
			vsCC.Cols = 5;
			vsCC.ColWidth(0, 0);
			// ID Number
			vsCC.ColWidth(1, FCConvert.ToInt32(lngWid * 0.6));
			// Name       '0.97
			vsCC.ColWidth(2, FCConvert.ToInt32(lngWid * 0.2));
			// Is Debit
			vsCC.ColWidth(3, 0);
			// Dirty Field
			vsCC.ColWidth(4, FCConvert.ToInt32(lngWid * 0.2));
			// Is VISA
			vsCC.ExtendLastCol = true;
			vsCC.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			vsCC.TextMatrix(0, 0, "ID Number");
			vsCC.TextMatrix(0, 1, "Credit/Debit Card Type");
			vsCC.TextMatrix(0, 2, "Is Debit");
			vsCC.TextMatrix(0, 4, "Is VISA");
			vsCC.ColComboList(2, "Yes|No");
			vsCC.ColComboList(4, "Yes|No");
		}

		private void PrintCreditCardList()
		{
			// this will print out a list of all the credit card types that can be used in this town
			frmReportViewer.InstancePtr.Init(rptCreditCardList.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			PrintCreditCardList();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
			Close();
		}

		private void vsCC_ChangeEdit(object sender, System.EventArgs e)
		{
			// this will set the row to dirty = true
			if (vsCC.IsCurrentCellInEditMode)
			{
				vsCC.TextMatrix(vsCC.Row, 3, FCConvert.ToString(-1));
			}
		}

		private void vsCC_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteCreditCard();
						break;
					}
				case Keys.Insert:
					{
						KeyCode = 0;
						if (mnuFileAdd.Visible && mnuFileAdd.Enabled)
						{
							mnuFileAdd_Click();
						}
						break;
					}
			}
			//end switch
		}

		private void vsCC_RowColChange(object sender, System.EventArgs e)
		{
			if ((vsCC.Col == 1 || vsCC.Col == 2 || vsCC.Col == 4) && vsCC.Row > 0)
			{
				vsCC.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCC.EditCell();
			}
		}

		private void FillGrid()
		{
			string strAdd = "";
			// this will fill the grid with all of the credit card types
			vsCC.Rows = 1;
			// this will clear all the rows excepts for the very top row
			if (!rsCard.EndOfFile())
			{
				rsCard.MoveFirst();
				while (!rsCard.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					strAdd = rsCard.Get_Fields_Int32("ID") + "\t" + rsCard.Get_Fields("Type") + "\t";
					if (FCConvert.ToBoolean(rsCard.Get_Fields_Boolean("IsDebit")))
					{
						strAdd += "Yes" + "\t" + "0";
					}
					else
					{
						strAdd += "No" + "\t" + "0";
					}
					if (FCConvert.ToBoolean(rsCard.Get_Fields_Boolean("IsVISA")))
					{
						strAdd += "\t" + "Yes" + "\t";
					}
					else
					{
						strAdd += "\t" + "No" + "\t";
					}
					// vsCC.AddItem rsCard.Fields("ID") & vbTab & rsCard.Fields("Type") & vbTab & "0"
					vsCC.AddItem(strAdd);
					rsCard.MoveNext();
				}
			}
			AdjustGridHeight();
		}

		private void SaveInfo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save all of the credit card information
				int lngCT;
				if (vsCC.Rows > 0)
				{
					vsCC.Select(0, 0);
					// this will select another cell so that any cell being edited will become eligible to save
				}
				for (lngCT = 1; lngCT <= vsCC.Rows - 1; lngCT++)
				{
					if (Conversion.Val(vsCC.TextMatrix(lngCT, 0)) != 0)
					{
						// is this already stored in the DB
						rsCard.FindFirstRecord("ID", vsCC.TextMatrix(lngCT, 0));
						if (rsCard.NoMatch)
						{
							rsCard.AddNew();
						}
						else
						{
							rsCard.Edit();
						}
						rsCard.Set_Fields("Type", Strings.Trim(vsCC.TextMatrix(lngCT, 1)));
						rsCard.Set_Fields("IsDebit", FCConvert.CBool(Strings.Trim(vsCC.TextMatrix(lngCT, 2)) == "Yes"));
						rsCard.Set_Fields("IsVISA", FCConvert.CBool(Strings.Trim(vsCC.TextMatrix(lngCT, 4)) == "Yes"));
						rsCard.Update();
						vsCC.TextMatrix(lngCT, 0, FCConvert.ToString(rsCard.Get_Fields_Int32("ID")));
					}
					else
					{
						if (Strings.Trim(vsCC.TextMatrix(lngCT, 1)) != "")
						{
							rsCard.AddNew();
							rsCard.Set_Fields("Type", Strings.Trim(vsCC.TextMatrix(lngCT, 1)));
							rsCard.Set_Fields("IsDebit", FCConvert.CBool(Strings.Trim(vsCC.TextMatrix(lngCT, 2)) == "Yes"));
							rsCard.Set_Fields("IsVISA", FCConvert.CBool(Strings.Trim(vsCC.TextMatrix(lngCT, 4)) == "Yes"));
							rsCard.Update();
							vsCC.TextMatrix(lngCT, 0, FCConvert.ToString(rsCard.Get_Fields_Int32("ID")));
						}
					}
					vsCC.TextMatrix(lngCT, 3, FCConvert.ToString(0));
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Card", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AdjustGridHeight()
		{
			//FC:FINAL:SBE - grid already anchored
			//// this will resize the grid to fit all of the rows and not extend past the end of the form
			//if ((vsCC.Rows * vsCC.RowHeight(0)) + 70 > (this.Height * 0.73))
			//{
			//    vsCC.Height = FCConvert.ToInt32((this.Height * 0.73));
			//    vsCC.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsCC.Height = (vsCC.Rows * vsCC.RowHeight(0)) + 70;
			//    vsCC.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void vsCC_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// this will set the row to dirty = true
			vsCC.TextMatrix(vsCC.GetFlexRowIndex(e.RowIndex), 3, FCConvert.ToString(-1));
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			mnuFileAdd_Click(sender, e);
		}
	}
}
