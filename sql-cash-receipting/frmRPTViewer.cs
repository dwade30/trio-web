﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Diagnostics;
using Global;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmRPTViewer.
	/// </summary>
	public partial class frmRPTViewer : FCForm
	{
		bool boolUseFilename = false;
		int intCopies = 1;
		bool boolAutoPrintDuplex = false;
		bool boolCantChoosePages = false;
		bool boolDuplexForcePairs = false;
		bool boolShowPrintDialog = true;
		bool boolPrintDuplex = false;
		bool pboolOverRideDuplex = false;

		public frmRPTViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRPTViewer InstancePtr
		{
			get
			{
				return (frmRPTViewer)Sys.GetInstance(typeof(frmRPTViewer));
			}
		}

		protected frmRPTViewer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/06/2005              *
		// ********************************************************
		clsDRWrapper rsReport = new clsDRWrapper();
		bool boolLoaded;
		bool boolNoReports;
		public bool blnRecreate;
		public bool blnRecreateJournal;
		int lngJournalNumber;
		clsDRWrapper rsData = new clsDRWrapper();
		int intExtraLines;
		int intLvl;
		int intArrIndex;
		bool boolArrDone;
		modBudgetaryAccounting.FundType[] ftOverrideArray = null;
		modBudgetaryAccounting.FundType[] ftAccountArray = null;
		modBudgetaryAccounting.FundType[] ftEFTArray = null;
		modBudgetaryAccounting.FundType[] ftFundArray = null;
		modBudgetaryAccounting.FundType[] ftFundArray2 = null;
		modBudgetaryAccounting.FundType[] ftAltCashFundArray = null;
		modBudgetaryAccounting.FundType[] ftCheckRecEntries = null;
		int lngAcctCT;
		int lngFundCT;
		string strDefaultCashAccount = "";
		double dblM_DTotals;
		// this will keep the total of any M D Account to have its own entry at the end of the Accounting Summary
		bool boolM_DLine;
		double dblMFundTotal;
		public bool boolUseBDJournal;
		public bool boolFundArrayEmpty;
		modBudgetaryAccounting.FundType[] ftFunds = new modBudgetaryAccounting.FundType[999 + 1];
		// this will be the second array used for Due To / Due Froms
		double dblFTAccountTotals;
		double dblFTAccountTotalsEFT;
        private cBDAccountController acctCont;
        //   = new cBDAccountController();
        private cBDSettings bdSets;
        private Dictionary<int,cFundTotal> FTAccountFunds = new Dictionary<int, cFundTotal>();
        private Dictionary<int, cFundTotal> FTAccountFundsEFT = new Dictionary<int, cFundTotal>();

		private struct DueToFrom
		{
			public string Account;
			public double Amount;
			public string Type;
			// CashEntry, Due To, Due From...ect
			public string DbCr;
			// DB' or 'CR'
		};

		private void cmbNumber_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbNumber.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
			// this will make the dropdown area bigger
		}

		private void cmbNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will dropdown the combobox
						KeyCode = (Keys)0;
						if (modAPIsConst.SendMessageByNum(cmbNumber.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbNumber.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
						}
						break;
					}
			}
			//end switch
		}

		private void cmbRecreate_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbRecreate.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
			// this will make the dropdown area bigger
		}

		private void cmbRecreate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will dropdown the combobox
						KeyCode = (Keys)0;
						if (modAPIsConst.SendMessageByNum(cmbRecreate.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbRecreate.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
						}
						break;
					}
			}
			//end switch
		}

		private void cmbRecreateJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbRecreateJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
			// this will make the dropdown area bigger
		}

		private void cmbRecreateJournal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will dropdown the combobox
						KeyCode = (Keys)0;
						if (modAPIsConst.SendMessageByNum(cmbRecreateJournal.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbRecreateJournal.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
						}
						break;
					}
			}
			//end switch
		}

		private void frmRPTViewer_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				//arView.ReportSource.Top = 0;
				//arView.ReportSource.Width = this.Width - 100;
				//arView.ReportSource.Left = 0;
				//arView.ReportSource.Height = this.Height - 400;
				//arView.Zoom = -1; // set the zoom of the viewer
				if (!LoadRecreateCombo())
				{
					boolNoReports = true;
				}
				// MAL@20080718: Disable Recreate Journal if BD not activated
				// Tracker Reference: 12893
				if (modGlobalConstants.Statics.gboolBD)
				{
					mnuFileRecreateJournal.Enabled = true;
					if (!LoadRecreateJournalCombo())
					{
						boolNoReports = true;
					}
					FillJournalCombo();
					FillMonthCombo();
				}
				else
				{
					mnuFileRecreateJournal.Enabled = false;
				}
				if (cmbRecreate.Items.Count > 0)
				{
					// if there is at least one report in the combo then
					// cmbNumber.ListIndex = 0         'this should show the latest report
				}
				else
				{
					MessageBox.Show("You must run a Daily Audit Report before viewing it.", "No Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
					Close();
					return;
				}
				mnuFileChange_Click();
			}
		}

		private void frmRPTViewer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRPTViewer.FillStyle	= 0;
			//frmRPTViewer.ScaleWidth	= 9300;
			//frmRPTViewer.ScaleHeight	= 7395;
			//frmRPTViewer.LinkTopic	= "Form2";
			//frmRPTViewer.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
            if (StaticSettings.gGlobalActiveModuleSettings.BudgetaryIsActive)
            {
				acctCont = new cBDAccountController();
            }
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// Me.WindowState = vbMaximized        'this will force the screen to maximized
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Recreate Daily Audit Report";
		}

		private void frmRPTViewer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape ID
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuExit_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmRPTViewer_Resize(object sender, System.EventArgs e)
		{
			//arView.ReportSource.Top = 0;
			//arView.ReportSource.Width = this.Width - 100;
			//arView.ReportSource.Left = 0;
			//arView.ReportSource.Height = this.Height - 400;
			//fraRecreate.Left = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Width - fraRecreate.Width) / 2.0);
			//fraRecreate.Top = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Height - fraRecreate.Height) / 3.0);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void mnuEMailPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Name + ".pdf";
                //a.Export(arView.ReportSource.Document, fileName);
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".pdf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                a.Export(report.Document, fileName);
                if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(Path.GetTempFileName()) != string.Empty)
					{
						strList = fileName + ";" + Path.GetTempFileName() + ".pdf";
					}
					else
					{
						strList = fileName;
					}
					frmEMail.InstancePtr.Init(strList, string.Empty, string.Empty, string.Empty, false, false, true, showAsModalForm: true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailPDF_Click");
			}
		}

		private void mnuEMalRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Document.Name + ".rtf";
                //a.Export(arView.ReportSource.Document, fileName);
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".rtf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                a.Export(report.Document, fileName);
                if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(Path.GetRandomFileName()) != string.Empty)
					{
						strList = fileName + ";" + Path.GetRandomFileName() + ".rtf";
					}
					else
					{
						strList = fileName;
					}
					frmEMail.InstancePtr.Init(strList, string.Empty, string.Empty, string.Empty, false, false, true, showAsModalForm: true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailRTF_Click");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
			mnuFileExport.Enabled = arView.Visible;
		}

		private void mnuFileChange_Click(object sender, System.EventArgs e)
		{
			arView.Visible = false;
            arView.Parent = null;
			toolBar1.Visible = false;
			fraRecreate.Visible = true;
			btnFileSave.Top = fraNumber.Top + fraNumber.Height + 20;
			btnFileSave.Visible = true;
			//fraNumber.Left = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Width - fraNumber.Width) / 2.0);
			//fraNumber.Top = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Height - fraNumber.Height) / 3.0);
			this.Text = "Recreate Daily Audit Report";
		}

		public void mnuFileChange_Click()
		{
			mnuFileChange_Click(mnuFileChange, new System.EventArgs());
		}

		private void mnuFileExportExcel_Click(object sender, System.EventArgs e)
		{
			ExportExcel();
		}

		private void mnuFileExportHTML_Click(object sender, System.EventArgs e)
		{
			ExportHTML();
		}

		private void mnuFileExportPDF_Click(object sender, System.EventArgs e)
		{
			ExportPDF();
		}

		private void mnuFileExportRTF_Click(object sender, System.EventArgs e)
		{
			ExportRTF();
		}

		private void mnuFileRecreate_Click(object sender, System.EventArgs e)
		{
			arView.Visible = false;
            arView.Parent = null;
            toolBar1.Visible = false;
			fraRecreate.Visible = true;
			fraRecreateJournal.Visible = false;
			btnFileSave.Visible = true;
			btnFileSave.Top = fraRecreate.Top + fraRecreate.Height + 20;
            //fraRecreate.Left = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Width - fraRecreate.Width) / 2.0);
            //fraRecreate.Top = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Height - fraRecreate.Height) / 3.0);
            this.Text = "Recreate Daily Audit Report";
		}

		private bool LoadRecreateCombo()
		{
			bool LoadRecreateCombo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsArch = new clsDRWrapper();
				cmbRecreate.Clear();
				string strSQL = "SELECT DISTINCT DailyCloseOut, 'FALSE' AS PrevYear FROM Archive " +
				                "UNION ALL SELECT DISTINCT DailyCloseOut, 'TRUE' AS PrevYear FROM LastYearArchive " +
				                "ORDER BY DailyCloseOut DESC";
				rsArch.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				rsLoad.OpenRecordset("SELECT * FROM CloseOut WHERE Type = 'R' ORDER BY CloseoutDate desc");
				while (!rsLoad.EndOfFile())
				{
					// check to make sure this close out has any archive records associated with it, this will serve
					// to remove all closeouts that have been removed or are not valid
					rsArch.FindFirstRecord("DailyCloseOut", rsLoad.Get_Fields_Int32("ID"));
					if (!rsArch.NoMatch)
					{
						cmbRecreate.AddItem(modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")), 6) + " - " + rsLoad.Get_Fields_DateTime("CloseoutDate"));
						if (!rsArch.Get_Fields_Boolean("PrevYear"))
						{
							cmbRecreate.ItemData(cmbRecreate.ListCount -1, 0);
						}
						else
						{
							cmbRecreate.ItemData(cmbRecreate.ListCount - 1, 1);
						}
					}
					rsLoad.MoveNext();
				}
				LoadRecreateCombo = true;
				return LoadRecreateCombo;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				LoadRecreateCombo = false;
			}
			return LoadRecreateCombo;
		}

		private void RecreateReport(int lngNum, bool boolUsePrevYears)
		{
			try
			{
				frmDailyReceiptAudit.InstancePtr.Unload();
				frmDailyReceiptAudit.InstancePtr.Hide();
				// make sure that the file exists
				frmDailyReceiptAudit.InstancePtr.boolCloseOut = true;
				// set the close out ID
				frmDailyReceiptAudit.InstancePtr.lngCloseoutKey = lngNum;
				frmDailyReceiptAudit.InstancePtr.boolUsePrevYears = boolUsePrevYears;
				SetupSQL();
				// regular audit
				frmReportViewer.InstancePtr.Init(arDailyAuditReport.InstancePtr);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Recreating Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupSQL()
		{
			frmDailyReceiptAudit.InstancePtr.strCloseOutSQL = " ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(frmDailyReceiptAudit.InstancePtr.lngCloseoutKey);
		}

		private void mnuFileRecreateJournal_Click(object sender, System.EventArgs e)
		{
			// Tracker Reference: 12893
			arView.Visible = false;
            arView.Parent = null;
            toolBar1.Visible = false;
			fraRecreate.Visible = false;
			fraRecreateJournal.Visible = true;
			btnFileSave.Visible = true;
			btnFileSave.Top = fraRecreateJournal.Top + fraRecreateJournal.Height + 20;
            //fraRecreateJournal.Left = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Width - fraRecreateJournal.Width) / 2.0);
            //fraRecreateJournal.Top = FCConvert.ToInt32((frmRPTViewer.InstancePtr.Height - fraRecreateJournal.Height) / 3.0);
            this.Text = "Recreate Daily Journal";
		}

		private bool LoadRecreateJournalCombo()
		{
			bool LoadRecreateJournalCombo = false;
			// Tracker Reference: 12893
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsArch = new clsDRWrapper();
				cmbRecreateJournal.Clear();
				rsArch.OpenRecordset("SELECT Distinct DailyCloseOut FROM Archive", modExtraModules.strCRDatabase);
				rsLoad.OpenRecordset("SELECT * FROM CloseOut WHERE Type = 'R' ORDER BY CloseoutDate desc");
				while (!rsLoad.EndOfFile())
				{
					// check to make sure this close out has any archive records associated with it, this will serve
					// to remove all closeouts that have been removed or are not valid
					rsArch.FindFirstRecord("DailyCloseOut", rsLoad.Get_Fields_Int32("ID"));
					if (!rsArch.NoMatch)
					{
						cmbRecreateJournal.AddItem(modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")), 6) + " - " + rsLoad.Get_Fields_DateTime("CloseoutDate"));
					}
					rsLoad.MoveNext();
				}
				LoadRecreateJournalCombo = true;
				return LoadRecreateJournalCombo;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				LoadRecreateJournalCombo = false;
			}
			return LoadRecreateJournalCombo;
		}

		private void RecreateJournal(int lngNum)
		{
			int lngCode;
			if (DialogResult.Yes == MessageBox.Show("Creating new journal for close out " + FCConvert.ToString(lngNum) + ". Are you sure?", "Confirm Recreate Journal", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
			{
				//Application.DoEvents();
				if (cmbJournal.SelectedIndex != -1)
				{
					frmDailyReceiptAudit.InstancePtr.Unload();
					frmDailyReceiptAudit.InstancePtr.Hide();
					// make sure that the file exists
					frmDailyReceiptAudit.InstancePtr.boolCloseOut = true;
					// set the close out ID
					frmDailyReceiptAudit.InstancePtr.lngCloseoutKey = lngNum;
					SetupSQL();
					if (cmbJournal.SelectedIndex == 0)
					{
						// this will get a new journal number from BD and use it
						lngJournalNumber = 0;
					}
					else
					{
						lngJournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbJournal.Items[cmbJournal.SelectedIndex].ToString())));
					}
					if (CheckBDDatabase() < 0)
					{
						if (GenerateJournal(lngNum))
						{
							MessageBox.Show("Recreated Journal Successfully", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
							Close();
						}
					}
				}
				else
				{
					MessageBox.Show("Please enter a Journal number before proceeding.", "Journal Number Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbJournal.Focus();
				}
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			int lngNum = 0;
			bool boolUsePrevYears = false;
			if (fraRecreate.Visible == true)
			{
				if (cmbRecreate.SelectedIndex != -1)
				{
					// kk01302015  Don't try if nothing is selected
					lngNum = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbRecreate.Items[cmbRecreate.SelectedIndex].ToString(), 6))));
					boolUsePrevYears = cmbRecreate.ItemData(cmbRecreate.ListIndex) == 1;
					blnRecreate = true;
					RecreateReport(lngNum, boolUsePrevYears);
				}
				else
				{
					MessageBox.Show("Please select a Close Out before proceeding.", "Close Out Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbRecreate.Focus();
				}
			}
			else if (fraRecreateJournal.Visible == true)
			{
				if (cmbRecreateJournal.SelectedIndex != -1)
				{
					// kk01302015  Don't try if nothing is selected
					lngNum = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbRecreateJournal.Items[cmbRecreateJournal.SelectedIndex].ToString(), 6))));
					blnRecreateJournal = true;
					RecreateJournal(lngNum);
				}
				else
				{
					MessageBox.Show("Please select a Close Out before proceeding.", "Close Out Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbRecreateJournal.Focus();
				}
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
            //int intStartPage = 0;
            //int intEndPage = 0;
            //string strDuplex = "";
            //int intStart = 0;
            //// VBto upgrade warning: intPgs As short --> As int	OnWriteFCConvert.ToDouble(
            //int intPgs = 0;
            //int intEnd = 0;
            //int intIndex;
            //DialogResult intReturn = 0;
            //string strPrinter = "";
            //string strOldPrinter;
            //// VBto upgrade warning: intOrientation As short --> As int	OnWrite(DDActiveReportsViewer2.PrtOrientation)
            //int intOrientation;
            //int x;
            //FCPrinter tPrinter = new FCPrinter();
            //strOldPrinter = "";
            try
            {
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                }
                if (report.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                {
                    FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                    return;
                }
                report.ExportToPDFAndPrint();
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuPrint", MsgBoxStyle.Critical, "Error");
            }
            //try
            //{
            //	// On Error GoTo ErrorHandler
            //	intOrientation = FCConvert.ToInt32(FCCommonDialog.PrinterOrientationConstants.cdlPortrait);
            //	if (!boolPrintDuplex)
            //	{
            //		// this is the automatic duplex printing that uses the GN settings
            //		if (boolShowPrintDialog)
            //		{
            //			// show the dialog box manually
            //			// strOldPrinter = ARViewer21.Document.Printer.PrinterName
            //			tPrinter = modCustomPageSize.GetDefaultPrinter();
            //			if (!(tPrinter == null))
            //			{
            //				strOldPrinter = modCustomPageSize.GetDefaultPrinter().DeviceName;
            //			}
            //			strPrinter = modDuplexPrinting.GetPrinterManually(report.Document.Pages.Count, intStartPage, intEndPage, intCopies, boolCantChoosePages, intOrientation);
            //			if (strPrinter == "")
            //			{
            //				return;
            //			}
            //			else
            //			{
            //				arView.Printer.PrinterName = strPrinter;
            //			}
            //		}
            //		else
            //		{
            //			intStartPage = 1;
            //			intEndPage = report.Document.Pages.Count;
            //			if (!boolCantChoosePages)
            //			{
            //				if (frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
            //				{
            //				}
            //				else
            //				{
            //					return;
            //				}
            //			}
            //		}

            //		arView.Printer.PrinterSettings.FromPage = intStartPage;
            //		arView.Printer.PrinterSettings.ToPage = intEndPage;

            //		arView.Printer.PrinterSettings.Copies = 1;
            //		for (x = 1; x <= intCopies; x++)
            //		{
            //                     report.PrintReport();
            //		}

            //		modCustomPageSize.Statics.boolChangedDefault = true;
            //		modCustomPageSize.Statics.strDefaultPrinter = strOldPrinter;
            //		modCustomPageSize.SetDefaultPrinterWinNT();
            //		modCustomPageSize.Statics.boolChangedDefault = false;
            //	}
            //	else
            //	{
            //		// this is the forced duplex printing, so that the user can print on a non duplex printer
            //		// this is used in RE and PP to print property cards
            //		if (boolShowPrintDialog)
            //		{
            //			//FC:TODO:AM
            //			//if (!ARViewer21.Printer.SetupDialog(App.MainForm.Handle.ToInt32()))
            //			//{
            //			//    return;
            //			//}
            //		}
            //		// have to see how many pages and check duplex info
            //		if (!boolAutoPrintDuplex)
            //		{
            //			strDuplex = frmPrintDuplex.InstancePtr.Init();
            //		}
            //		else
            //		{
            //			strDuplex = "FULL";
            //		}
            //		if (strDuplex != string.Empty)
            //		{
            //			if (Strings.UCase(Strings.Trim(strDuplex)) == "NONE")
            //			{
            //				// print regular
            //				arView.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
            //				intStartPage = 1;
            //				intEndPage = report.Document.Pages.Count;
            //				if (!boolCantChoosePages)
            //				{
            //					if (frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
            //					{
            //						arView.Printer.PrinterSettings.FromPage = intStartPage;
            //						arView.Printer.PrinterSettings.ToPage = intEndPage;
            //                                 //FC:FINAL:SBE - use Print() extension for Document
            //                                 //ARViewer21.Printer.Print();
            //                                 report.PrintReport();
            //					}
            //				}
            //				else
            //				{
            //                             //FC:FINAL:SBE - use Print() extension for Document
            //                             //ARViewer21.Printer.Print();
            //                             report.PrintReport();
            //				}
            //				return;
            //			}
            //			else if (Strings.UCase(Strings.Trim(strDuplex)) == "FULL")
            //			{
            //				// printer can handle duplex by itself
            //				// print regular but set printer to duplex
            //				arView.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
            //				arView.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;
            //				intStartPage = 1;
            //				intEndPage = report.Document.Pages.Count;
            //				if (!boolCantChoosePages)
            //				{
            //					if (frmNumPages.InstancePtr.Init(ref intEndPage, ref intStartPage))
            //					{
            //						arView.Printer.PrinterSettings.FromPage = intStartPage;
            //						arView.Printer.PrinterSettings.ToPage = intEndPage;
            //                                 //FC:FINAL:SBE - use Print() extension for Document
            //                                 //ARViewer21.Printer.Print();
            //                                 report.PrintReport();
            //					}
            //				}
            //				else
            //				{
            //                             //FC:FINAL:SBE - use Print() extension for Document
            //                             //ARViewer21.Printer.Print();
            //                             report.PrintReport();
            //				}
            //				return;
            //			}
            //			else if (Strings.UCase(Strings.Trim(strDuplex)) == "MANUALTOPISLAST")
            //			{
            //				// when flipped, must reverse pages
            //				// move all even pages behind all odd pages and reverse even pages order
            //				arView.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
            //				intStart = 1;
            //				if (boolDuplexForcePairs)
            //				{
            //					intPgs = FCConvert.ToInt32(report.Document.Pages.Count / 2);
            //					if (!boolCantChoosePages)
            //					{
            //						if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
            //						{
            //							// these will be the pairs to print (such as cards etc), not pages
            //							intEnd = intPgs;
            //							intStart = (intStart * 2) - 1;
            //							// front side of first card to print
            //							intEnd = (intEnd * 2);
            //							// second side of last card to print
            //						}
            //					}
            //					else
            //					{
            //						intEnd = intPgs;
            //						intStart = (intStart * 2) - 1;
            //						// front side of first card to print
            //						intEnd = (intEnd * 2);
            //						// second side of last card to print
            //					}
            //				}
            //				else
            //				{
            //					intPgs = report.Document.Pages.Count;
            //					if (!boolCantChoosePages)
            //					{
            //						if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
            //						{
            //							// these will be the pages to print
            //							intEnd = intPgs;
            //						}
            //					}
            //					else
            //					{
            //						intEnd = intPgs;
            //					}
            //				}
            //                         // print odds first
            //                         //FC:FINAL:SBE - use Print() extension for Document
            //                         //ARViewer21.Printer.Print();
            //                         report.PrintReport();
            //				//FC:TODO:AM
            //				//for (intIndex = intStart; intIndex <= intEnd; intIndex += 2)
            //				//{
            //				//    // this will print odds
            //				//    ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
            //				//} // intIndex
            //				//ARViewer21.Printer.EndJob();
            //				//if (intEnd > intStart)
            //				//{
            //				//    // check to make sure there is more than one page
            //				//    // now print evens
            //				//    intReturn = FCMessageBox.Show("When " + strDuplexLabel + " are done printing, replace them in the printer so the back sides will print and click OK.", MsgBoxStyle.OkCancel, "Print second side(s)");
            //				//    if (intReturn != DialogResult.Cancel)
            //				//    {
            //				//        ARViewer21.Printer.StartJob(this.Text);
            //				//        for (intIndex = intEnd; intIndex >= intStart; intIndex += -2)
            //				//        {
            //				//            // this will print evens
            //				//            ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
            //				//        } // intIndex
            //				//        ARViewer21.Printer.EndJob();
            //				//    }
            //				//}
            //			}
            //			else if (Strings.UCase(Strings.Trim(strDuplex)) == "MANUALTOPISFIRST")
            //			{
            //				// no need to reverse pages
            //				// move all even pages behind all odd pages
            //				arView.Printer.PrinterSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
            //				intStart = 1;
            //				if (boolDuplexForcePairs)
            //				{
            //					intPgs = FCConvert.ToInt32(report.Document.Pages.Count / 2);
            //					if (!boolCantChoosePages)
            //					{
            //						if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
            //						{
            //							// these will be the *CARDS* to print, not pages
            //							intEnd = intPgs;
            //							intStart = (intStart * 2) - 1;
            //							// front side of first card to print
            //							intEnd = (intEnd * 2);
            //							// second side of last card to print
            //						}
            //					}
            //					else
            //					{
            //						intEnd = intPgs;
            //						intStart = (intStart * 2) - 1;
            //						// front side of first card to print
            //						intEnd = (intEnd * 2);
            //						// second side of last card to print
            //					}
            //				}
            //				else
            //				{
            //					intPgs = report.Document.Pages.Count;
            //					if (!boolCantChoosePages)
            //					{
            //						if (frmNumPages.InstancePtr.Init(ref intPgs, ref intStart, null, "Print Pages"))
            //						{
            //							// These will the the pages to print
            //							intEnd = intPgs;
            //						}
            //					}
            //					else
            //					{
            //						intEnd = intPgs;
            //					}
            //				}
            //                         // print odds first
            //                         //FC:FINAL:SBE - use Print() extension for Document
            //                         //ARViewer21.Printer.Print();
            //                         report.PrintReport();
            //				//FC:TODO:AM
            //				//for (intIndex = intStart; intIndex <= intEnd; intIndex += 2)
            //				//{
            //				//    // this will print odds
            //				//    ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
            //				//} // intIndex
            //				//ARViewer21.Printer.EndJob();
            //				//if (intEnd > intStart)
            //				//{
            //				//    // only do this if there is more than one page
            //				//    // now print evens
            //				//    intReturn = FCMessageBox.Show("When " + strDuplexLabel + " are done printing, replace them in the printer so the back sides will print and click OK.", MsgBoxStyle.OkCancel, "Print second side(s)");
            //				//    if (intReturn != DialogResult.Cancel)
            //				//    {
            //				//        ARViewer21.Printer.StartJob(this.Text);
            //				//        for (intIndex = intStart + 1; intIndex <= intEnd; intIndex += 2)
            //				//        {
            //				//            // this will print evens
            //				//            ARViewer21.Printer.PrintPage(ARViewer21.ReportSource.Document.Pages[intIndex - 1]);
            //				//        } // intIndex
            //				//        ARViewer21.Printer.EndJob();
            //				//    }
            //				//}
            //			}
            //		}
            //	}
            //	return;
            //}
            //catch (Exception ex)
            //{
            //	// ErrorHandler:
            //	FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuPrint", MsgBoxStyle.Critical, "Error");
            //}
        }

        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
		{
			if (e.Button == toolBarButtonEmailPDF)
			{
				this.mnuEMailPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonEmailRTF)
			{
				this.mnuEMalRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportPDF)
			{
				this.mnuFileExportPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportRTF)
			{
				this.mnuFileExportRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportHTML)
			{
				this.mnuFileExportHTML_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportExcel)
			{
				this.mnuFileExportExcel_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == this.toolBarButtonPrint)
			{
				this.mnuPrint_Click(e.Button, EventArgs.Empty);
			}
		}

		private void ExportHTML()
		{
			try
			{
				//string strFileName = "";
				////FileSystemObject fso = new FileSystemObject();
				//// vbPorter upgrade warning: intResp As short, int --> As DialogResult
				//DialogResult intResp;
				//string strOldDir;
				//string strFullName = "";
				//strOldDir = Application.StartupPath;
				//Information.Err(ex).Clear();
				//// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- App.MainForm.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//App.MainForm.CommonDialog1.FileName = "";
				//App.MainForm.CommonDialog1.Filter = "*.htm";
				//App.MainForm.CommonDialog1.DefaultExt = "htm";
				//App.MainForm.CommonDialog1.InitialDirectory = Application.StartupPath;
				//App.MainForm.CommonDialog1.ShowDialog();
				//if (Information.Err(ex).Number == 0)
				//{
				//    ActiveReportsHTMLExport.HTMLexport a = new ActiveReportsHTMLExport.HTMLexport();
				//    a = new ActiveReportsHTMLExport.HTMLexport;
				//    strFileName = fso.GetBaseName(App.MainForm.CommonDialog1.FileName);
				//    a.FileNamePrefix = strFileName;
				//    strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
				//    a.HTMLOutputPath = Path.GetDirectoryName(App.MainForm.CommonDialog1.FileName);
				//    a.MHTOutput = false;
				//    a.MultiPageOutput = false;
				//    if (arView.Pages.Count > 1)
				//    {
				//        intResp = MessageBox.Show("Do you want to save all pages as one HTML page?", "Single or multiple files?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				//        if (intResp == DialogResult.No) a.MultiPageOutput = true;
				//    }
				//    if (!Directory.Exists(a.HTMLOutputPath + strFileName))
				//    {
				//        Directory.CreateDirectory(a.HTMLOutputPath + strFileName);
				//        a.HTMLOutputPath = a.HTMLOutputPath + strFileName;
				//    }
				//    a.Export(arView.Pages);
				//    MessageBox.Show("Export to file  " + a.HTMLOutputPath + strFullName + "  was completed successfully." + "\r\n" + "Please note that supporting files may also have been created in the same directory.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//}
				//else
				//{
				//    MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//ChDrive(strOldDir);
				//Application.StartupPath = strOldDir;
				GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".html";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".html";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".html";
                }
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE Exception when ReportSource is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HTML export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ExportRTF()
		{
			try
			{
				string strOldDir;
				strOldDir = FCFileSystem.Statics.UserDataFolder;
				//Information.Err(ex).Clear();
				//// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- App.MainForm.CommonDialog1.CancelError = true;
				// /*? On Error Resume Next  */
				//App.MainForm.CommonDialog1.FileName = "";
				//App.MainForm.CommonDialog1.Filter = "*.rtf";
				//App.MainForm.CommonDialog1.DefaultExt = "rtf";
				//App.MainForm.CommonDialog1.InitialDirectory = Application.StartupPath;
				//App.MainForm.CommonDialog1.ShowDialog();
				//if (Information.Err(ex).Number==0) {
				//	ActiveReportsRTFExport.ARExportRTF a = new ActiveReportsRTFExport.ARExportRTF();
				//	a = new ActiveReportsRTFExport.ARExportRTF;
				//	a.FileName = App.MainForm.CommonDialog1.FileName;
				//	a.Export(arView.Pages);
				//	MessageBox.Show("Export to file  "+App.MainForm.CommonDialog1.FileName+"  was completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//} else {
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//ChDrive(strOldDir);
				//Application.StartupPath = strOldDir;
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".rtf";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".rtf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE Exception when report source is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In RTF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ExportPDF()
		{
			try
			{
				string strOldDir;
				strOldDir = FCFileSystem.Statics.UserDataFolder;
				Information.Err().Clear();
				// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- App.MainForm.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  *///App.MainForm.CommonDialog1.FileName = "";
				//App.MainForm.CommonDialog1.Filter = "*.pdf";
				//App.MainForm.CommonDialog1.DefaultExt = "pdf";
				//App.MainForm.CommonDialog1.InitialDirectory = Application.StartupPath;
				//App.MainForm.CommonDialog1.ShowDialog();
				//if (Information.Err(ex).Number==0) {
				//	ActiveReportsPDFExport.ARExportPDF a = new ActiveReportsPDFExport.ARExportPDF();
				//	a = new ActiveReportsPDFExport.ARExportPDF;
				//	a.FileName = App.MainForm.CommonDialog1.FileName;
				//	a.Export(arView.Pages);
				//	MessageBox.Show("Export to file  "+App.MainForm.CommonDialog1.FileName+"  was completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//} else {
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				//ChDrive(strOldDir);
				//Application.StartupPath = strOldDir;
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".pdf";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".pdf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE Exception when ReportSource is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In PDF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ExportExcel()
		{
			try
			{
				string strOldDir;
				string strFileName = "";
				string strFullName = "";
				string strPathName = "";
				////FileSystemObject fso = new FileSystemObject();
				//strOldDir = Application.StartupPath;
				//Information.Err(ex).Clear();
				//// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////- App.MainForm.CommonDialog1.CancelError = true;
				// /*? On Error Resume Next  */
				//App.MainForm.CommonDialog1.FileName = "";
				//App.MainForm.CommonDialog1.Filter = "*.xls";
				//App.MainForm.CommonDialog1.DefaultExt = "xls";
				//App.MainForm.CommonDialog1.InitialDirectory = Application.StartupPath;
				//App.MainForm.CommonDialog1.ShowDialog();
				//if (Information.Err(ex).Number==0) {
				//	strFileName = fso.GetBaseName(App.MainForm.CommonDialog1.FileName); // name without extension
				//	strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName); // name with extension
				//	strPathName = Path.GetDirectoryName(App.MainForm.CommonDialog1.FileName);
				//	if (Strings.Right(strPathName, 1)!="\\") strPathName += "\\";
				//	ActiveReportsExcelExport.ARExportExcel a = new ActiveReportsExcelExport.ARExportExcel();
				//	a = new ActiveReportsExcelExport.ARExportExcel;
				//	if (!Directory.Exists(strPathName+strFileName)) {
				//		Directory.CreateDirectory(strPathName+strFileName);
				//		strPathName += strFileName+"\\";
				//	}
				//	// a.FileName = .FileName
				//	a.FileName = strPathName+strFullName;
				//	a.Version = 8;
				//	a.Export(arView.Pages);
				//	MessageBox.Show("Export to file  "+strPathName+strFullName+"  was completed successfully."+"\r\n"+"Please note that supporting files may also have been created in the same directory.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//} else {
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//ChDrive(strOldDir);
				//Application.StartupPath = strOldDir;
				GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".xls";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".xls";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".xls";
                }
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE Exception when ReportSource is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Excel export.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillMonthCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				// adds the months to the combo box
				cmbMonth.Clear();
				cmbMonth.AddItem("01 - January");
				cmbMonth.AddItem("02 - February");
				cmbMonth.AddItem("03 - March");
				cmbMonth.AddItem("04 - April");
				cmbMonth.AddItem("05 - May");
				cmbMonth.AddItem("06 - June");
				cmbMonth.AddItem("07 - July");
				cmbMonth.AddItem("08 - August");
				cmbMonth.AddItem("09 - September");
				cmbMonth.AddItem("10 - October");
				cmbMonth.AddItem("11 - November");
				cmbMonth.AddItem("12 - December");
				rsM.OpenRecordset("SELECT ActualSystemDate FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 ORDER BY ActualSystemDate desc", modExtraModules.strCRDatabase);
				if (!rsM.EndOfFile())
				{
					cmbMonth.SelectedIndex = ((DateTime)rsM.Get_Fields_DateTime("ActualSystemDate")).Month - 1;
				}
				else
				{
					// default month
					cmbMonth.SelectedIndex = DateTime.Now.Month - 1;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Period Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillJournalCombo()
		{
			// adds all of the active journals to the combo box
			clsDRWrapper rsBD = new clsDRWrapper();
			rsBD.DefaultDB = "TWBD0000.vb1";
			cmbJournal.Clear();
			cmbJournal.Items.Insert(0, "New Journal");
			rsBD.OpenRecordset("SELECT JournalNumber, Period, Description FROM JournalMaster WHERE Type = 'CW' AND Status = 'E' ORDER BY JournalNumber");
			if (rsBD.BeginningOfFile() != true && rsBD.EndOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cmbJournal.AddItem(modGlobal.PadToString_8(rsBD.Get_Fields("JournalNumber"), 4) + " - " + modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsBD.Get_Fields("Period")), 2, false) + " - " + rsBD.Get_Fields_String("Description"));
					rsBD.MoveNext();
				}
				while (!rsBD.EndOfFile());
			}
			if (cmbJournal.Items.Count > 0)
				cmbJournal.SelectedIndex = 0;
		}

		private void cmbJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 80, 0);
		}

		private void cmbJournal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						//FC:FINAL:AM
						//if (modAPIsConst.SendMessageByNumWrp(cmbJournal.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, string.Empty) == 0)
						//{
						//    modAPIsConst.SendMessageByNumWrp(cmbJournal.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, string.Empty);
						//    KeyCode = (Keys)0;
						//}
						if (!cmbJournal.DroppedDown)
						{
							cmbJournal.DroppedDown = true;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbJournal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cmbJournal.SelectedIndex == -1)
			{
				if (cmbJournal.Items.Count > 1)
				{
					// Cancel = True
					MessageBox.Show("Please enter a Journal Number.", "Input", MessageBoxButtons.OK, MessageBoxIcon.Question);
				}
				else
				{
					MessageBox.Show("You have no Journals to choose, you must Add a New Journal.", "Input", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = false;
					cmbRecreateJournal.Focus();
				}
			}
		}

		private void cmbMonth_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbMonth.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 100, 0);
		}

		private void cmbMonth_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						//FC:FINAL:AM
						//if (modAPIsConst.SendMessageByNumWrp(cmbMonth.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, string.Empty) == 0)
						//{
						//    modAPIsConst.SendMessageByNumWrp(cmbMonth.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, string.Empty);
						//    KeyCode = (Keys)0;
						//}
						if (!cmbMonth.DroppedDown)
						{
							cmbMonth.DroppedDown = true;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMonth_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cmbMonth.SelectedIndex == -1)
			{
				e.Cancel = true;
				MessageBox.Show("Please enter an Accounting Month.", "Input", MessageBoxButtons.OK, MessageBoxIcon.Question);
			}
		}

		private bool GenerateJournal(int lngNum)
		{
			bool GenerateJournal = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL;
				clsDRWrapper rsTemp = new clsDRWrapper();
				int lngJN = 0;
				// this is the journal number that is returned
				bool boolAccountAdded = false;
				int lngCT;
				int lngNegCT;
				bool boolAltCashAccts;
				DateTime dtDate;
				// this is the latest date on any receipt that I am processing
				string strJDesc = "";
				// this is to pass the jounral desc back from a form
				string strTempAccount = "";
				string strTempDate = "";
				bool boolAddJournal = false;
				bool boolJournalEntryDesc = false;
				int lngInitialCashIndex = 0;
				bool boolContinue;
				string strBO = "";
				int lngEFT = 0;
				double dblCashFundCCAltCashTotal;
                var bdSetCont = new cBDSettingsController();
                cStandardAccounts stdAccts;

				// kk09092014 trocr-430
				// initialize the arrays
				FCUtils.EraseSafe(modGlobal.Statics.garrFundInfo);
                FTAccountFunds.Clear();
                FTAccountFundsEFT.Clear();
                stdAccts = acctCont.GetStandardAccounts();
                bdSets = bdSetCont.GetBDSettings();
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				for (int i = 0; i < 99; i++)
				{
					modGlobal.Statics.garrFundInfo[i] = new modGlobal.FundMonies(0);
				}
				Array.Resize(ref ftEFTArray, 0 + 1);
				dtDate = GetLastDate_2(DateTime.FromOADate(0), lngNum);
                dblFTAccountTotals = 0;
                dblFTAccountTotalsEFT = 0;
				// this will get the last date of a receipt that is being processed
				// **********************************************************************
				// This is True True with no alt cash accounts
				// this will get the SQL Statement for the T T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_2(0);
				boolUseBDJournal = false;
				boolFundArrayEmpty = true;
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase, 0, false);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolFundArrayEmpty = false;
					ftAccountArray = new modBudgetaryAccounting.FundType[rsData.RecordCount() - 1 + 1];
					FillFundInfo_6(rsData, 0);
					// this function will actually add the accounts
					rsData.MoveFirst();
					if (lngAcctCT != Information.UBound(ftAccountArray, 1) + 1)
					{
						// check to make sure that there are no blank elements in the array
						if (lngAcctCT != 0)
						{
							Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						}
					}
					else
					{
					}
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				else
				{
					boolAccountAdded = true;
				}
				// **********************************************************************
				// This is False True with no alt cash accounts non EFT
				// this will get the SQL Statement for the F T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_2(1);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolFundArrayEmpty = false;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
					}
					FillFundInfo_6(rsData, 1);
					rsData.MoveFirst();
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				
				strSQL = GetSQL_2(7);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolFundArrayEmpty = false;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
					}
					FillFundInfo_204(rsData, 1, true);
					rsData.MoveFirst();
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				// **********************************************************************
				// This will find the cash entry
				if (!boolFundArrayEmpty)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						// this sets up the cash accounts for the funds if the user has BD
						ftFundArray = modBudgetaryAccounting.CalcFundCash(ref ftAccountArray);
					}
					else
					{
						// create a cash entry into the account M CASH for these accounts
						ftFundArray = CashAccountForM(ref ftAccountArray);
					}
				}
				else
				{
					ftFundArray = new modBudgetaryAccounting.FundType[999 + 1];
				}
				// **********************************************************************
				// This is True True with alt cash accounts
				// this will get the SQL Statement for the T T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_68(0, true);
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase, 0, false);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolAltCashAccts = true;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
					}
					FillFundInfo_69(rsData, 0, true);
					// this function will actually add the accounts
					rsData.MoveFirst();
					if (lngAcctCT != Information.UBound(ftAccountArray, 1) + 1)
					{
						// check to make sure that there are no blank elements in the array
						if (lngAcctCT != 0)
						{
							Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						}
					}
					else
					{
					}
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				else
				{
					// boolAccountAdded = True
				}
				// **********************************************************************
				// This is False True with alt cash accounts
				// this will get the SQL Statement for the F T Accounts
				// this will fill the ftAccountArray
				strSQL = GetSQL_68(1, true);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolAltCashAccts = true;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
					}
					FillFundInfo_69(rsData, 1, true);
					rsData.MoveFirst();
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				// **********************************************************************
				// This is False False with no alt cash accounts
				// these are not included in the fund cash accounts...they will have thier own entry
				// this will get the accounts for the F F Accounts to be placed above the divider
				strSQL = GetSQL_2(2);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
					}
					FillFundInfo_6(rsData, 2);
					rsData.MoveFirst();
					boolFundArrayEmpty = false;
				}
				if (boolFundArrayEmpty)
				{
					intLvl = 0;
					Array.Resize(ref ftAccountArray, 0 + 1);
				}
				else
				{
					intLvl = Information.UBound(ftAccountArray, 1);
				}
				
				double dblTemp;
				// This will create the Account (top) entries for the EFT entries that have Alt Cash Accounts
				strSQL = GetSQL_2(20);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					FillFundInfoWithAltCash_6(rsData, true);
				}
				if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Due")) == "Y")
				{
					strSQL = GetSQL_68(5, true);
					rsData.OpenRecordset(strSQL);
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						// this will find the default cash account and save it for
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsData.Get_Fields("SumTotal")) != 0)
						{
							rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
							if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								strDefaultCashAccount = "G " + modGlobal.PadToString_8(FCConvert.ToInt16(modGlobal.Statics.glngCashFund), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsTemp.Get_Fields("Account") + "-" + "00";
							}
							else
							{
								MessageBox.Show("You must set up an Misc Cash Account before you may continue.", "Missing Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
					}
					rsData.MoveFirst();
				}
				// this will get the accounts for the F F Accounts grouped by Default Account to be put at the bottom of the account list
				strSQL = GetSQL_2(3);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					FillFundInfo_6(rsData, 3);
					rsData.MoveFirst();
				}
				// check for any accounts that have a default cash account and append them at the end of the array...
				// also subtract the amounts from the fund that they are associated with
				strSQL = GetSQL_2(4);
				rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					AddDefaultCashAccounts();
				}
				// this will set up the Due To/Froms
				// strBO =  GetBankOverride (
				boolContinue = modBudgetaryAccounting.CalcDueToFrom(ref ftAccountArray, ref ftFundArray, false, Strings.Format(dtDate, "MM/dd/yyyy") + " C/R", boolFundArrayEmpty, true, "CR", string.Empty, false, lngInitialCashIndex);

				// kk10252016 trocr-467  Move this later in the process so we can adjust the cash accounts
				// **********************************************************************
				// This is the Seperate Credit Card Cash Account amounts
				// this will get the SQL Statement for the Seperate Credit Card Cash Account amounts
				// this will fill the ftAccountArray
				lngAcctCT = Information.UBound(ftAccountArray, 1) + 1;
				// kk10262016 trocr-467  CalcDueToFrom adds elements to the array - refresh the count
				strSQL = GetSQL_2(40);
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					boolFundArrayEmpty = false;
					if (!boolAccountAdded)
					{
						// keep rockin
					}
					else
					{
						ftAccountArray = new modBudgetaryAccounting.FundType[0 + 1];
					}
					AddSeperateCreditCardGLAccounts();
					// kk10262016 trocr-467 Make this a procedure call    'kk09092014 trocr-430
					rsData.MoveFirst();
					boolAccountAdded = false;
					boolFundArrayEmpty = false;
				}
				// **********************************************************************
				if (boolContinue)
				{
					if (!(lngInitialCashIndex > Information.UBound(ftAccountArray, 1)))
                    {
                        //cFundTotal ftItem;
                        string strAcct = "";
                        string strCshAccount = "";
                        int intBound = 0;
                        int intTempIndex = 0;
						// this will find the last cash entry that was added and adjust it by the amount in dblFTAccountTotals + dblFTAccountTotalsEFT
						// kk10262016 trocr-467  Take the Separate Credit Card account out
						// kk09092014 trocr-430  Reduce the cash entry by the total amount of credit card payments that went to the Alt Account
						if (dblFTAccountTotals != 0 || dblFTAccountTotalsEFT != 0)
                        {
                            intBound = FTAccountFunds.Count;
                            if (intBound > 0)
							{
                                foreach (var ftItem in FTAccountFunds.Values)
                                {
                                    if (ftItem.Amount != 0)
                                    {
                                        strAcct = ftFundArray[ftItem.Fund].Account;
                                        if (ftFundArray[ftItem.Fund].UseDueToFrom)
                                        {
                                            strCshAccount = MakeCashAccount(Convert.ToInt32(ftFundArray[ftItem.Fund].CashFund),
                                                Convert.ToInt32(stdAccts.MiscCash), bdSets.Ledger);
                                        }
                                        else
                                        {
                                            strCshAccount = MakeCashAccount(ftItem.Fund,Convert.ToInt32(stdAccts.MiscCash),bdSets.Ledger);
                                        }

                                        intTempIndex = FindFundAccount(ref strCshAccount);
                                        ftAccountArray[intTempIndex].Amount =
                                            ftAccountArray[intTempIndex].Amount + Convert.ToDecimal(ftItem.Amount);
                                        Array.Resize(ref ftAccountArray, Information.UBound(ftAccountArray, 1) + 1 + 1);
                                        ftAccountArray[ftAccountArray.UBound()].Amount = Convert.ToDecimal(ftItem.Amount * -1);
                                        ftAccountArray[ftAccountArray.UBound()].Description =
                                            dtDate.FormatAndPadShortDate() + @" C/R - Prev Csh";
                                        ftAccountArray[ftAccountArray.UBound()].Account = strCshAccount;
                                        ftAccountArray[ftAccountArray.UBound()].AcctType = "C";
                                    }
                                }
								Array.Resize(ref ftAccountArray, Information.UBound(ftAccountArray, 1) + 1 + 1);
								ftAccountArray[Information.UBound(ftAccountArray)].Amount = FCConvert.ToDecimal(dblFTAccountTotals * -1);
								ftAccountArray[Information.UBound(ftAccountArray)].Description = Strings.Format(dtDate, "MM/dd/yyyy") + " C/R - Prev Csh";
								ftAccountArray[Information.UBound(ftAccountArray)].Account = ftAccountArray[lngInitialCashIndex].Account;
								ftAccountArray[Information.UBound(ftAccountArray)].AcctType = "C";
							}

                            if (ftEFTArray.UBound() >= 0)
                            {
                                int intTempFund = 0;
                                while (!(lngEFT >= ftEFTArray.UBound()))
                                {
                                    if (ftEFTArray[lngEFT].Amount != 0)
                                    {
                                        if (strCshAccount == "")
                                        {
                                            intTempFund = modBudgetaryAccounting.GetFundFromAccount(ftEFTArray[lngEFT].Account);
                                            strAcct = ftFundArray[intTempFund].Account;
                                            if (ftFundArray[intTempFund].UseDueToFrom)
                                            {
                                                strCshAccount = MakeCashAccount(
                                                    Convert.ToInt32(ftFundArray[intTempFund].CashFund),
                                                    Convert.ToInt32(stdAccts.MiscCash), bdSets.Ledger);
                                            }
                                            else
                                            {
                                                strCshAccount = MakeCashAccount(intTempFund,
                                                    Convert.ToInt32(stdAccts.MiscCash), bdSets.Ledger);
                                            }

                                            intTempIndex = FindFundAccount(ref strCshAccount);
                                        }
                                        else
                                        {
                                            intTempIndex = FindFundAccount(ref strCshAccount);
                                        }

                                        ftAccountArray[intTempIndex].Amount += ftEFTArray[lngEFT].Amount;
                                        Array.Resize(ref ftAccountArray, Information.UBound(ftAccountArray, 1) + 1 + 1);
                                        ftAccountArray[ftAccountArray.UBound()].Amount = ftEFTArray[lngEFT].Amount * -1;
                                        ftAccountArray[ftAccountArray.UBound()].Description =
                                            dtDate.FormatAndPadShortDate() + @" C/R - Cash EFT";
                                        ftAccountArray[ftAccountArray.UBound()].Account =
                                            ftAccountArray[intTempIndex].Account;
                                        ftAccountArray[ftAccountArray.UBound()].AcctType = "C";
                                        lngEFT += 1;
                                    }
                                    
                                }
                            }
						
						}
					}
					// check to make sure the count of how many entries in the array are correct
					if (lngAcctCT - 1 != Information.UBound(ftAccountArray, 1))
					{
						lngAcctCT += 1;
					}
					if (dblMFundTotal != 0)
					{
						// only run this if the M total is not zero
						if (AddCashAccountforM_9(ref ftAccountArray, ftFundArray, dblMFundTotal))
						{
						}
					}
					lngJN = lngJournalNumber;
					// set all of the values in the array to negative
					for (lngNegCT = 0; lngNegCT <= Information.UBound(ftAccountArray, 1); lngNegCT++)
					{
						ftAccountArray[lngNegCT].Amount *= -1;
					}
					strJDesc = Strings.Format(dtDate, "MM/dd/yyyy") + " C/R";
					// set the mouse pointer to the arrow
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
					// show the date and description form
					frmGetJournalDesc.InstancePtr.Init(ref dtDate, ref strJDesc, ref boolJournalEntryDesc);
					// this will check with the user to make sure that the info is correct
					// set it back to the hourglass
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
					if (lngJN != 0)
					{
						// this is when the user does not want to add a new journal
						modGlobalFunctions.AddCYAEntry_8("CR", "Daily Audit added to Journal " + FCConvert.ToString(lngJN) + ".");
					}
					for (lngCT = 0; lngCT <= Information.UBound(ftAccountArray, 1); lngCT++)
					{
						if (boolJournalEntryDesc)
						{
							if (Strings.Left(ftAccountArray[lngCT].Account, 1) != "M" && ftAccountArray[lngCT].Account != "")
							{
								// Add this journal
								if (ftAccountArray[lngCT].Amount != 0)
								{
									boolAddJournal = true;
								}
							}
							if (ftAccountArray[lngCT].Description == "")
							{
								ftAccountArray[lngCT].Description = strJDesc;
							}
							else if (ftAccountArray[lngCT].AcctType == "C")
							{
							}
						}
						else
						{
							if (Strings.Left(ftAccountArray[lngCT].Account, 1) != "M" && ftAccountArray[lngCT].Account != "")
							{
								// Add this journal
								if (ftAccountArray[lngCT].Amount != 0)
								{
									boolAddJournal = true;
								}
							}
							if (ftAccountArray[lngCT].Description == "")
							{
								ftAccountArray[lngCT].Description = Strings.Format(dtDate, "MM/dd/yyyy") + " C/R";
							}
							else if (ftAccountArray[lngCT].AcctType == "C")
							{
								if (Strings.Left(ftAccountArray[lngCT].Account, 1) != "M" && ftAccountArray[lngCT].Account != "")
								{
									// Add this journal
									boolAddJournal = true;
									if (ftAccountArray[lngCT].Description.Length > 11)
									{
										ftAccountArray[lngCT].Description = Strings.Format(dtDate, "MM/dd/yyyy") + Strings.Right(ftAccountArray[lngCT].Description, 11);
									}
								}
							}
						}
					}
					if (boolAddJournal)
					{
						// journal needs to be posted, this routine will do it
						lngJN = modBudgetaryAccounting.AddToJournal(ref ftAccountArray, "CW", lngJN, dtDate, strJDesc, FCConvert.ToInt32(Strings.Left(cmbMonth.Items[cmbMonth.SelectedIndex].ToString(), 2)), !boolUseBDJournal, frmDailyReceiptAudit.InstancePtr.lngCloseoutKey);
						lngJournalNumber = lngJN;
						// kk01282015 trocrs-32  Add option to automatically post system generated journals
						if (modSecurity.ValidPermissions_8(null, modGlobal.CRSECURITYAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptCRDailyAudit))
						{
							if (lngJN > 0)
							{
								if (MessageBox.Show("The receipt entries have been saved into journal " + Strings.Format(lngJN, "0000") + ".  Would you like to post the journal?", "Post Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									modGlobal.Statics.tPostInfo = new clsBudgetaryPosting();
									// Instantiate the collection.  The journal is added in sarAccountingSummary
									clsPostingJournalInfo tJournalInfo = new clsPostingJournalInfo();
									tJournalInfo.JournalNumber = lngJN;
									tJournalInfo.JournalType = "CW";
									tJournalInfo.Period = FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cmbMonth.Items[cmbMonth.SelectedIndex].ToString(), 2))));
									tJournalInfo.CheckDate = "";
									modGlobal.Statics.tPostInfo.AddJournal(tJournalInfo);
									modGlobal.Statics.tPostInfo.AllowPreview = false;
									modGlobal.Statics.tPostInfo.PostJournals();
									modGlobal.Statics.tPostInfo = null;
								}
							}
						}
					}
					// set all of the values in the array to negative
					for (lngNegCT = 0; lngNegCT <= Information.UBound(ftAccountArray, 1); lngNegCT++)
					{
						ftAccountArray[lngNegCT].Amount *= -1;
					}
					GenerateJournal = true;
				}
				else
				{
					MessageBox.Show("Error during the calculation of Due To/From entries.  No Journal has been created and will have to be manually entered.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					GenerateJournal = false;
				}
				return GenerateJournal;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				if (Information.Err(ex).Number != 9)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + " has occured.", "Accounting Summary", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					// this is the subscript out of range from the UBound on an empty array
				}
				GenerateJournal = false;
			}
			return GenerateJournal;
		}

		private string GetSQL_2(short I, bool boolAffectCashDrawer = true, bool boolAffectCash = true, bool boolUseDefaultCashAccount = false, bool boolShowETF = false)
		{
			return GetSQL(ref I, boolAffectCashDrawer, boolAffectCash, boolUseDefaultCashAccount, boolShowETF);
		}

		private string GetSQL_68(short I, bool boolUseDefaultCashAccount = false, bool boolShowETF = false)
		{
			return GetSQL(ref I, true, true, boolUseDefaultCashAccount, boolShowETF);
		}

		private string GetSQL(ref short I, bool boolAffectCashDrawer = true, bool boolAffectCash = true, bool boolUseDefaultCashAccount = false, bool boolShowETF = false)
		{
			string GetSQL = "";
			// this will create a query def if needed, but will return the correct sql statement either way
			int j;
			string strTemp = "";
			string strAltCashAcct = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strEFT = "";
			if (boolShowETF)
			{
				strEFT = " AND EFT = 1";
			}
			else
			{
				strEFT = " AND EFT <> 1";
			}
			if (boolUseDefaultCashAccount)
			{
				strAltCashAcct = " AND rTRIM(isnull(DefaultCashAccount, '')) <> ''";
			}
			else
			{
				strAltCashAcct = " AND rTRIM(isnull(DefaultCashAccount, '')) = ''";
			}
			// this will get ALL accounts that are not closed out and group them by the account number
			switch (I)
			{
				case 0:
					{
						// Get all accounts where Cash Drawer = True        'XXXX    'this will get all accounts that are not a from RE, PP or UT or that Affect Cash = True
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, rtrim(Project1) as Proj FROM Archive WHERE AffectCashDrawer = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										// XXXX  'ReceiptType NOT BETWEEN 90 AND 95) OR
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM Archive WHERE AffectCashDrawer = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										// XXXX    ' (ReceiptType NOT BETWEEN 90 AND 95) OR
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, Proj, sum(Amt) as SumTotal FROM (" + strTemp + ") as CashDrawerAccounts WHERE Amt <> 0 GROUP BY Account, Proj";
						break;
					}
				case 1:
					{
						// Get all accounts where Affect Cash = True and Cash Drawer = False         'XXXX    'this will get all the accounts that are between 90-95 and Affect Cash = False and Cash Drawer = True
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1" + strEFT + ") ";
										// XXXX 'ReceiptType BETWEEN 90 AND 95 AND
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1" + strEFT + ") ";
										// XXXX   'ReceiptType BETWEEN 90 AND 95 AND
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, Proj, sum(Amt) as SumTotal FROM (" + strTemp + ") as NonCashDrawerAccounts WHERE Amt <> 0 GROUP BY Account, Proj";
						break;
					}
				case 2:
					{
						// this will get all the accounts that are between 90-96 or 890 or 891 and Affect Cash = False and Cash Drawer = False
						// kgk trocr-277 04-25-11  include 893-896
						// kgk trocrs-21 09-20-13  include 97
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj FROM Archive WHERE (ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 0) ";
										break;
									}
								case 2:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM Archive WHERE (ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 0 ) ";
										// AND Amount2 < 0
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM Archive WHERE (ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 0) ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, Proj, sum(Amt) as SumTotal FROM (" + strTemp + ") as NonCashDrawerAccounts WHERE Amt <> 0 GROUP BY Account, Proj";
						break;
					}
				case 3:
					{
						// this will get all accounts that are between 90-96 or 890 or 891 AND Affect Cash = False AND Cash Drawer = False and have a DefaultAccount to use to pay with
						// kgk trocr-277 04-25-11  include 893-896
						// kgk trocrs-21 09-20-13  include 97
						// this needs to know which amount is the interest/principal amount to see if there is any other type of money coming in/out
						strTemp = "SELECT Sum(Amount1 + Amount3 + Amount4 + Amount5 + Amount6) as SumTotal , SUM(Amount2) AS Interest, DefaultAccount as Account, '' as Proj FROM Archive WHERE ((ReceiptType BETWEEN 90 AND 97 OR ReceiptType = 890 OR ReceiptType = 891 OR ReceiptType BETWEEN 893 AND 896) AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND AffectCash = 0) AND (Amount1 <> 0 OR Amount2 <> 0 OR Amount3 <> 0 OR Amount4 <> 0 OR Amount5 <> 0 OR Amount6 <> 0) AND DefaultAccount <> '' GROUP BY DefaultAccount";
						GetSQL = strTemp;
						break;
					}
				case 4:
					{
						// get any accounts that have a default cash account
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj, DefaultCashAccount FROM Archive WHERE DefaultCashAccount <> '' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL;
										break;
									}
								default:
									{
										strTemp += " UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj, DefaultCashAccount FROM Archive WHERE DefaultCashAccount <> '' AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL;
										break;
									}
							}
							//end switch
						}
						strTemp += "))))))";
						GetSQL = "SELECT sum(Amt) as SumTotal, DefaultCashAccount FROM (" + strTemp + ") as AlternativeCashAccounts WHERE Amt <> 0 GROUP BY DefaultCashAccount";
						break;
					}
				case 5:
					{
						// , Account1, Account2, Account3, Account4, Account5, Account6
						GetSQL = "SELECT SUM(Amount1 + Amount2 + Amount3 + Amount4 + Amount5 + Amount6) AS SumTotal FROM (SELECT * FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1 AND EFT = 0) as temp";
						break;
					}
				case 6:
					{
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT DefaultCashAccount AS Account, Amount1 AS Amt FROM Archive WHERE ((ReceiptType NOT BETWEEN 90 AND 97) OR (AffectCashDrawer = 1 OR AffectCashDrawer = 0 AND AffectCash = 1)) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT DefaultCashAccount AS Account, Amount" + FCConvert.ToString(j) + " AS Amt FROM Archive WHERE ((ReceiptType NOT BETWEEN 90 AND 97) OR (AffectCashDrawer = 1 OR AffectCashDrawer = 0 AND AffectCash = 1)) AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + ") ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT Account, sum(Amt) as SumTotal FROM (" + strTemp + ") as AltCashAccounts WHERE Amt <> 0 GROUP BY Account";
						break;
					}
				case 7:
					{
						// Get all accounts with Affect Cash = False and Cash Drawer = True EFT     'XXXX   'this will get all the accounts that are between 90-95 and Affect Cash = False and Cash Drawer = True EFT
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account1 as Account, Amount1 as Amt, Project1 as Proj FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1 AND EFT = 1) ";
										// XXXX    'ReceiptType BETWEEN 90 AND 95 AND
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strAltCashAcct + " AND AffectCashDrawer = 0 AND AffectCash = 1 AND EFT = 1) ";
										// XXXX  'ReceiptType BETWEEN 90 AND 95 AND
										break;
									}
							}
							//end switch
						}
						// GetSQL = "SELECT Account, sum(Amt) as SumTotal FROM NonCashDrawerAccounts WHERE Amt <> 0 GROUP BY Account"
						GetSQL = "SELECT Account, Proj, Amt as SumTotal FROM (" + strTemp + ") as NonCashDrawerAccounts WHERE Amt <> 0";
						break;
					}
				case 10:
					{
						// gets the SQL for EFTs and CheckRecEntries
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account" + FCConvert.ToString(j) + " AS Account, Amount1 AS Amt, Project1 as Proj, EFT, Name FROM Archive WHERE (ReceiptType NOT BETWEEN 90 AND 99) AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj, EFT, Name FROM Archive WHERE (ReceiptType NOT BETWEEN 90 AND 99) AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT * FROM (" + strTemp + ") as CheckRecEntries WHERE Amt <> 0";
						break;
					}
				case 20:
					{
						// gets the SQL for EFTs that have alt cash accounts
						for (j = 1; j <= 6; j++)
						{
							switch (j)
							{
								case 1:
									{
										strTemp = "(SELECT Account" + FCConvert.ToString(j) + " AS Account, Amount1 AS Amt, Project1 as Proj, EFT, Name FROM Archive WHERE DefaultCashAccount <> '' AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
								default:
									{
										strTemp += "UNION ALL (SELECT Account" + FCConvert.ToString(j) + " as Account, Amount" + FCConvert.ToString(j) + " as Amt, Project" + FCConvert.ToString(j) + " as Proj, EFT, Name FROM Archive WHERE DefaultCashAccount <> '' AND EFT = 1 AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") ";
										break;
									}
							}
							//end switch
						}
						GetSQL = "SELECT * FROM (" + strTemp + ") as AltCashEFTEntries WHERE Amt <> 0";
						// Case 30     'gets the SQL for convenience fees with no seperate credit card cash account
						// GetSQL = "SELECT SUM(ConvenienceFee) AS SumTotal, ConvenienceFeeGLAccount as Account FROM (SELECT * FROM Archive WHERE " & frmDailyReceiptAudit.strCloseOutSQL & ") GROUP BY ConvenienceFeeGLAccount"
						break;
					}
				case 40:
					{
						// gets the SQL for conveneince fees and other credit card amounts that go to a seperate credit card cash account
						// GetSQL = "SELECT SUM(ConvenienceFee) AS ConvenienceFeeTotal, SUM(CardPaidAmount) as SumTotal, SeperateCreditCardGLAccount, ConvenienceFeeGLAccount FROM (SELECT * FROM Archive WHERE " & frmDailyReceiptAudit.strCloseOutSQL & " AND SeperateCreditCardGLAccount <> '') GROUP BY SeperateCreditCardGLAccount, ConvenienceFeeGLAccount HAVING SUM(CardPaidAmount) <> 0"
						GetSQL = "SELECT SUM(CardPaidAmount) as SumTotal, SeperateCreditCardGLAccount FROM (SELECT * FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND SeperateCreditCardGLAccount <> '') as temp GROUP BY SeperateCreditCardGLAccount HAVING SUM(CardPaidAmount) <> 0";
						break;
					}
			}
			//end switch
			return GetSQL;
		}

		private short CheckBDDatabase()
		{
			short CheckBDDatabase = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function is just checking to see if I can make a good connection to the BD database
				// it will return true if it makes a good connection and can access data
				clsDRWrapper rsBD = new clsDRWrapper();
				TextReader ts;
				// vbPorter upgrade warning: lngCT As int	OnWrite(short, double)
				int lngCT = 0;
				string strTemp = "";
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsBD.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
					if (!rsBD.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsBD.Get_Fields("Account")) != 0 || FCConvert.ToInt32(rsBD.Get_Fields("Account")) == 0)
						{
							CheckBDDatabase = -1;
						}
					}
					if (modGlobal.Statics.gboolBDExclusive)
					{
						// open the BD DB exclusively so noone else can be in it at this time
						if (File.Exists(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TWBD0000.LDB")))
						{
							// Open Path.Combine(CurDir, "TWBD0000.LDB") For Output As #10
							lngCT = 0;
							ts = File.OpenText(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TWBD0000.LDB"));
							while ((strTemp = ts.ReadLine()) != null)
							{
								lngCT += strTemp.Length / 64;
							}
							if (lngCT > 1)
							{
								MessageBox.Show("Please get all other users out of these modules before processing the Daily Audit." + "\r\n" + "Budgetary" + "\r\n" + "Cash Receipting" + "\r\n" + "Payroll", "Single User", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								CheckBDDatabase = -1;
								// allow the user to process
								return CheckBDDatabase;
							}
						}
					}
				}
				else
				{
					CheckBDDatabase = -1;
					return CheckBDDatabase;
				}
				return CheckBDDatabase;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckBDDatabase = FCConvert.ToInt16(Information.Err(ex).Number);
				// MsgBox "Error accessing the Budgetary database.  Please run a compact and repair on the Budgetary Database and try again.", vbCritical, "Database Access"
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking BD Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckBDDatabase;
		}
		// vbPorter upgrade warning: dtMaxDate As DateTime	OnWrite(short, DateTime)
		private DateTime GetLastDate_2(DateTime dtMaxDate, int lngCloseOut)
		{
			return GetLastDate(ref dtMaxDate, lngCloseOut);
		}

		private DateTime GetLastDate(ref DateTime dtMaxDate, int lngCloseOut)
		{
			DateTime GetLastDate = System.DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				string strSQL;
				clsDRWrapper rsTemp = new clsDRWrapper();
				strSQL = "SELECT ArchiveDate FROM Archive WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(lngCloseOut) + " GROUP BY ArchiveDate ORDER BY ArchiveDate desc";
				rsTemp.OpenRecordset(strSQL);
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					rsTemp.MoveFirst();
					while (!rsTemp.EndOfFile())
					{
						//FC:FINAL:JEI:IT717 rsTemp.Get_Fields("ArchiveDate") doesn't return an OADate
						if (Conversion.Val(rsTemp.Get_Fields_DateTime("ArchiveDate")) > dtMaxDate.ToOADate())
							dtMaxDate = (DateTime)rsTemp.Get_Fields_DateTime("ArchiveDate");
						rsTemp.MoveNext();
					}
				}
				if (dtMaxDate.ToOADate() == 0)
					dtMaxDate = DateTime.Today;
				GetLastDate = dtMaxDate;
				return GetLastDate;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetLastDate = dtMaxDate;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Last Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return GetLastDate;
		}

		private void FillFundInfo_6(clsDRWrapper rsF, int lngTemp, bool boolOverWrite = false, bool boolAltCashAcct = false, bool boolEFT = false)
		{
			FillFundInfo(ref rsF, ref lngTemp, boolOverWrite, boolAltCashAcct, boolEFT);
		}

		private void FillFundInfo_69(clsDRWrapper rsF, int lngTemp, bool boolAltCashAcct = false, bool boolEFT = false)
		{
			FillFundInfo(ref rsF, ref lngTemp, false, boolAltCashAcct, boolEFT);
		}

		private void FillFundInfo_204(clsDRWrapper rsF, int lngTemp, bool boolEFT = false)
		{
			FillFundInfo(ref rsF, ref lngTemp, false, false, boolEFT);
		}

		private void FillFundInfo(ref clsDRWrapper rsF, ref int lngTemp, bool boolOverWrite = false, bool boolAltCashAcct = false, bool boolEFT = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsWOInt = new clsDRWrapper();
                int intFund = 0;
                cFundTotal ftItem;
                string strTemp;

				// this will fill the recordset's Department field with the fund to be grouped on
				while (!(rsF.EndOfFile() || rsF.BeginningOfFile()))
				{
					if (lngTemp != 4)
					{
						if (lngTemp == 1)
                        {
                            intFund = 0;
                            strTemp = acctCont.GetFundForAccount(rsF.Get_Fields_String("Account"));
                            intFund = Convert.ToInt32(strTemp);
							// if it is a F T
							if (boolEFT)
							{
								Array.Resize(ref ftEFTArray, Information.UBound(ftEFTArray, 1) + 1 + 1);
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								ftEFTArray[Information.UBound(ftEFTArray) - 1].Amount = rsF.Get_Fields("SumTotal");
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								ftEFTArray[Information.UBound(ftEFTArray) - 1].Account = rsF.Get_Fields_String("Account");
								// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
								ftEFTArray[Information.UBound(ftEFTArray) - 1].Project = rsF.Get_Fields("Proj");
                                ftEFTArray[ftEFTArray.UBound()].CashFund = intFund.ToString();
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								dblFTAccountTotalsEFT += rsF.Get_Fields("SumTotal");
								// this will save the total of the F T that should be subracted from the cash account and added to the second entry of the cash account
							}
							else
							{
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								dblFTAccountTotals += rsF.Get_Fields("SumTotal");
                                if (FTAccountFunds.ContainsKey(intFund))
                                {
                                    FTAccountFunds.TryGetValue(intFund,out ftItem);
                                    ftItem.Amount += rsF.Get_Fields_Double("SumTotal");
                                }
                                else
                                {
                                    ftItem = new cFundTotal();
                                    ftItem.Amount = rsF.Get_Fields_Double("SumTotal");
                                    ftItem.Fund = intFund;
                                    FTAccountFunds.Add(intFund,ftItem);
                                }
							}
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.InStr(1, rsF.Get_Fields("Account"), "_", CompareConstants.vbBinaryCompare) == 0 && rsF.Get_Fields("Account").Length > 2)
						{
							// check to make sure that this is not an "M D" account
							// if it is...do not allow it to add its totals to the accounts...
							// it needs to have its own cash account and no entries in the BD system
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Trim(rsF.Get_Fields("Account")) == "M D")
							{
								// add the M D total to be added at the end of the accounting summary
								// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
								dblM_DTotals = rsF.Get_Fields("SumTotal");
								// and skip the rest of this function for this type of account only
							}
							else
							{
								// this will add the account summary to ftAccountArray
								if (lngTemp == 3)
								{
									// if it is a default cash account, it has to have a negative balance so that it is set to DB
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
									// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
									AddAccount_24(rsF.Get_Fields("Account"), (Conversion.Val(rsF.Get_Fields("SumTotal")) + Conversion.Val(rsF.Get_Fields("Interest"))) * -1, Strings.Trim(rsF.Get_Fields("Proj")), lngTemp, boolOverWrite);
								}
								else if (lngTemp == 30)
								{
									// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
									if (Conversion.Val(rsF.Get_Fields("SumTotal")) != 0)
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
										AddAccount_24(rsF.Get_Fields("Account"), Conversion.Val(rsF.Get_Fields("SumTotal")), "", 0, boolOverWrite);
										// this should collect just the M accounts so that I can calculate the end fund balance
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (Strings.Left(rsF.Get_Fields("Account"), 1) == "M" && lngTemp != 2 && !boolAltCashAcct)
										{
											// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
											dblMFundTotal += rsF.Get_Fields("SumTotal");
										}
									}
								}
								else
								{
									// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
									if (Conversion.Val(rsF.Get_Fields("SumTotal")) != 0)
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
										AddAccount_24(rsF.Get_Fields("Account"), Conversion.Val(rsF.Get_Fields("SumTotal")), Strings.Trim(rsF.Get_Fields("Proj")), lngTemp, boolOverWrite);
										// this should collect just the M accounts so that I can calculate the end fund balance
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (Strings.Left(rsF.Get_Fields("Account"), 1) == "M" && lngTemp != 2 && !boolAltCashAcct)
										{
											// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
											dblMFundTotal += rsF.Get_Fields("SumTotal");
										}
									}
								}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (Strings.Left(rsF.Get_Fields("Account"), 1) != "M")
								{
									boolUseBDJournal = true;
								}
								boolOverWrite = false;
							}
						}
						else
						{
							// Stop
						}
					}
					else
					{
						// this should always have only one entry...F T Payments
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						if (rsF.Get_Fields("SumTotal") != "" && modGlobalConstants.Statics.gboolBD)
						{
							// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
							AddAccount_24(strDefaultCashAccount, Conversion.Val(rsF.Get_Fields("SumTotal")) * -1, Strings.Trim(rsF.Get_Fields("Proj")), lngTemp, boolOverWrite);
						}
					}
					rsF.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Fund Info Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillFundInfoWithAltCash_6(clsDRWrapper rsF, bool boolAltCashAcct = false)
		{
			FillFundInfoWithAltCash(ref rsF, boolAltCashAcct);
		}

		private void FillFundInfoWithAltCash(ref clsDRWrapper rsF, bool boolAltCashAcct = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngFund;
				bool boolAddDTDFEntries;
				clsDRWrapper rsFundInfo = new clsDRWrapper();
				int lngMainFund;
				// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
				Decimal curTotal;
				// new fund array for alt cash accounts
				ftAltCashFundArray = new modBudgetaryAccounting.FundType[999 + 1];
				// this will fill the recordset's Department field with the fund to be grouped on
				curTotal = 0;
				while (!(rsF.EndOfFile() || rsF.BeginningOfFile()))
				{
					boolAddDTDFEntries = false;
					// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsF.Get_Fields("Amt")) != 0)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [Proj] not found!! (maybe it is an alias?)
						AddAccount_24(rsF.Get_Fields("Account"), Conversion.Val(rsF.Get_Fields("Amt")), Strings.Trim(rsF.Get_Fields("Proj")), 0);
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(rsF.Get_Fields("Account"), 1) != "M")
						{
							boolUseBDJournal = true;
						}
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						curTotal += Conversion.Val(rsF.Get_Fields("Amt"));
					}
					rsF.MoveNext();
				}
				if (curTotal != 0)
				{
					intLvl = Information.UBound(ftAccountArray, 1);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Fund Info Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private modBudgetaryAccounting.FundType[] CashAccountForM(ref modBudgetaryAccounting.FundType[] Accounts)
		{
			modBudgetaryAccounting.FundType[] CashAccountForM = null;
			// this function will setup the cash account for any user that does not have the BD module
			// it will return an array of FundTypes with only one fund which is the default cash account
			try
			{
				// On Error GoTo ERROR_HANDLER
				modBudgetaryAccounting.FundType[] ftFunds = new modBudgetaryAccounting.FundType[99 + 1];
				// this will be the array that is returned
				int lngCT;
				// total number of accounts in the array
				int lngUB;
				// this is the upper bound of the array passed in
				lngUB = Information.UBound(Accounts, 1);
				// reset all of the funds
				FCUtils.EraseSafe(ftFunds);
				ftFunds[1].Account = "M CASH";
				ftFunds[1].Description = "CASH ACCOUNT";
				for (lngCT = 0; lngCT <= lngUB; lngCT++)
				{
					// check all of the accounts
					// check to see if there is an invalid account number
					if (Strings.InStr(1, Accounts[lngCT].Account, "_", CompareConstants.vbBinaryCompare) == 0 && Accounts[lngCT].Account.Length > 2)
					{
						// extract the fund number and add the amount
						ftFunds[1].Amount -= Accounts[lngCT].Amount;
					}
					else
					{
						// invalid account format
					}
				}
				CashAccountForM = ftFunds;
				return CashAccountForM;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + " has occured.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CashAccountForM;
		}

		private bool AddCashAccountforM_9(ref modBudgetaryAccounting.FundType[] Accounts, modBudgetaryAccounting.FundType[] Cash, double dblMAmount = 0)
		{
			return AddCashAccountforM(ref Accounts, ref Cash, false, dblMAmount);
		}

		private bool AddCashAccountforM(ref modBudgetaryAccounting.FundType[] Accounts, ref modBudgetaryAccounting.FundType[] Cash, bool bolNoAccounts = false, double dblMAmount = 0)
		{
			bool AddCashAccountforM = false;
			// PRE:    'Cash() is an array of 99 funds with amounts and fund numbers in account field and
			// Accounts() is a dynamic array of any accounts you want posted to the budgetary system
			// POST:   'Accounts will have all of the original accounts and thier totals with one cash entry
			try
			{
				// On Error GoTo ErrorTag
				string strCashAccount = "";
				int lngUB;
				// If Accounts(UBound(Accounts)).Account = "" And Accounts(UBound(Accounts)).Amount = 0 And Accounts(UBound(Accounts)).AcctType = "" Then
				// AddCashAccountforM = True
				// lngUB = UBound(Accounts)
				// lngAcctCT = lngAcctCT + 1
				// Else
				AddCashAccountforM = true;
				lngUB = Information.UBound(Accounts, 1) + 1;
				lngAcctCT += 1;
				Array.Resize(ref Accounts, lngUB + 1);
				lngUB = lngUB;
				// End If
				if (dblMAmount != 0)
				{
					Accounts[lngUB].Account = "M CASH";
					Accounts[lngUB].AcctType = "F";
					Accounts[lngUB].Amount = FCConvert.ToDecimal(dblMAmount * -1);
					// this has to be multiplied by -1 so that it will be a DB not a CR
					Accounts[lngUB].Description = "CASH ACCOUNT";
				}
				else
				{
					Accounts[lngUB].Account = Cash[1].Account;
					Accounts[lngUB].AcctType = "F";
					Accounts[lngUB].Amount = Cash[1].Amount;
					Accounts[lngUB].Description = Cash[1].Description;
				}
				return AddCashAccountforM;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Add Cash Account (M)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				AddCashAccountforM = false;
			}
			return AddCashAccountforM;
		}

		private double AddDefaultCashAccounts()
		{
			double AddDefaultCashAccounts = 0;
			// add all the accounts to the end of the array, assuming that the accounts are summed in this
			// this function will return a value equal to the amount changed in all of the funds
			int intFund = 0;
			int intIndex;
			double dblSum = 0;
			string strGLAcct = "";
			string strGLAltAcct = "";
			rsData.MoveFirst();
			while (!rsData.EndOfFile())
			{
				// subtract the amount from each associated fund total
				if (Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount")), 1) == "M")
				{
					intFund = modGlobal.Statics.glngCashFund;
				}
				else
				{
					intFund = modBudgetaryAccounting.GetFundFromAccount(rsData.Get_Fields_String("DefaultCashAccount"), false);
				}
				if (intFund > 0)
				{
					strGLAcct = "G " + modGlobal.PadToString_8(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modGlobal.PadToString_8(modGlobal.Statics.glngCashAccount, FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modGlobal.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2))));
					strGLAltAcct = FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount"));
					// add the account and amount to the array
					// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
					AddAccount_15(strGLAltAcct, Conversion.Val(rsData.Get_Fields("SumTotal")) * -1, 4);
					if (modGlobal.Statics.glngCashAccount == Conversion.Val(Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("DefaultCashAccount")), 3, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))))
					{
						// this will find the index of the fund in the array
						// intIndex = FindFundAccount(strGLAcct)
						// If intIndex > 0 Then
						// If AdjustFundCashAccount(intIndex, Val(rsData.Fields("SumTotal"))) Then
						// dblSum = dblSum + Val(rsData.Fields("SumTotal"))
						// Else
						// MsgBox "Fund not found, the Fund totals are incorrect. (" & Format(rsData.Fields("SumTotal"), "#,##0.00") & ")", vbCritical, "Fund Error"
						// End If
						// End If
					}
				}
				rsData.MoveNext();
			}
			AddDefaultCashAccounts = dblSum;
			return AddDefaultCashAccounts;
		}
		// kk10262016 trocr-467  Change this to a procedure and handle all redistribution here
		private void AddSeperateCreditCardGLAccounts()
		{
			// kk09092014 trocr-430  Rewriting this function to:
			// 1 - If the fund uses DTDF then transfer the amount to the credit card alt cash account in the cash fund (per Brenda)
			// We don't reduce the fund amount because we want the DTDF code to transfer the entire amount
			// We will reduce the cash account by the amount after the DTDF entries are added
			// Otherwise add an entry for the credit card alt cash account and reduce the fund cash by the amount
			// 2 - Sum all the amounts to transfer and create one entry
			// 3 - Return the amount to reduce the cash account of the cash fund
			// 
			// add all the accounts to the end of the array, assuming that the accounts are summed in the array
			// this function will return a value equal to the amount changed in all of the funds
			int intFund = 0;
			string strGLAltAcct = "";
			string strGLAltAcctCash = "";
			double dblCashFundAmt = 0;
			string strGLCashAcct = "";
			int intAcctIndx = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsData.MoveFirst();
			while (!rsData.EndOfFile())
			{
				intFund = modBudgetaryAccounting.GetFundFromAccount(rsData.Get_Fields_String("SeperateCreditCardGLAccount"), false);
				if (intFund > 0)
				{
					if (ftFundArray[intFund].UseDueToFrom || intFund == modGlobal.Statics.glngCashFund)
					{
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						dblCashFundAmt += Conversion.Val(rsData.Get_Fields("SumTotal"));
						if (strGLAltAcctCash == "")
						{
							strGLAltAcctCash = "G " + modGlobal.PadToString_6(modGlobal.Statics.glngCashFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))));
							strGLAltAcctCash += Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("SeperateCreditCardGLAccount")), strGLAltAcctCash.Length + 1);
						}
					}
					else
					{
						strGLAltAcct = FCConvert.ToString(rsData.Get_Fields_String("SeperateCreditCardGLAccount"));
						// kk10252016 trocr-467  Adjust the Fund cash account amount
						if (modGlobalConstants.Statics.gboolBD)
						{
							rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
							if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
							{
								// NEED THE FIND THE CASH ACCOUNT HERE
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								strGLCashAcct = "G " + modGlobal.PadToString_6(intFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsTemp.Get_Fields("Account") + "-" + "00";
							}
						}
						intAcctIndx = FindFundAccount(ref strGLCashAcct);
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						AdjustFundCashAccount_6(intAcctIndx, rsData.Get_Fields("SumTotal"));
						// add the account and amount to the array
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						AddAccount_15(strGLAltAcct, Conversion.Val(rsData.Get_Fields("SumTotal")) * -1, 4);
						// subtract the amount from the associated fund total
						// TODO Get_Fields: Field [SumTotal] not found!! (maybe it is an alias?)
						ftFundArray[intFund].Amount -= rsData.Get_Fields("SumTotal");
					}
				}
				rsData.MoveNext();
			}
			if (dblCashFundAmt != 0)
			{
				// kk10252016 trocr-467  Adjust the Fund cash account amount
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'", "TWBD0000.vb1");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						// NEED THE FIND THE CASH ACCOUNT HERE
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strGLCashAcct = "G " + modGlobal.PadToString_6(modGlobal.Statics.glngCashFund, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsTemp.Get_Fields("Account") + "-" + "00";
					}
				}
				intAcctIndx = FindFundAccount(ref strGLCashAcct);
				AdjustFundCashAccount(ref intAcctIndx, ref dblCashFundAmt);
				// Should we still see if it already exists?
				AddAccount_15(strGLAltAcctCash, dblCashFundAmt * -1, 4);
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short FindFundAccount(ref string strAcct)
		{
			short FindFundAccount = 0;
			// this will search through the array and find the account that matches the one passed in
			// if found it will return the index of the account, if not then it returns a -1
			int intCT;
			for (intCT = 0; intCT <= Information.UBound(ftAccountArray, 1); intCT++)
			{
				if (ftAccountArray[intCT].Account == strAcct)
				{
					FindFundAccount = FCConvert.ToInt16(intCT);
					return FindFundAccount;
				}
				Debug.WriteLine(FCConvert.ToString(intCT) + " " + ftAccountArray[intCT].Account);
			}
			FindFundAccount = -1;
			return FindFundAccount;
		}
		// vbPorter upgrade warning: intX As short	OnWriteFCConvert.ToInt32(
		private bool AdjustFundCashAccount_6(int intX, double dblAmt)
		{
			return AdjustFundCashAccount(ref intX, ref dblAmt);
		}

		private bool AdjustFundCashAccount(ref int intX, ref double dblAmt)
		{
			bool AdjustFundCashAccount = false;
			// this function will change the amount of the fund cash entry
			try
			{
				// On Error GoTo ERROR_HANDLER
				ftAccountArray[intX].Amount += FCConvert.ToDecimal(dblAmt);
				AdjustFundCashAccount = true;
				return AdjustFundCashAccount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				AdjustFundCashAccount = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Adjust Fund Cash Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AdjustFundCashAccount;
		}
		// vbPorter upgrade warning: dblAmt As double	OnReadFCConvert.ToDecimal(
		private void AddAccount_15(string strAcct, double dblAmt, int intType = 0, bool boolOverWrite = false)
		{
			AddAccount(ref strAcct, ref dblAmt, "", intType, boolOverWrite);
		}

		private void AddAccount_24(string strAcct, double dblAmt, string strProj = "", int intType = 0, bool boolOverWrite = false)
		{
			AddAccount(ref strAcct, ref dblAmt, strProj, intType, boolOverWrite);
		}

		private void AddAccount(ref string strAcct, ref double dblAmt, string strProj = "", int intType = 0, bool boolOverWrite = false)
		{
			// this will redim the array if needed and add the account to the next available index
			int lngTemp = 0;
			lngAcctCT += 1;
			if (!boolOverWrite)
			{
				if (intType == 4 || intType == 3)
				{
					if (lngAcctCT >= Information.UBound(ftAccountArray, 1))
					{
						// if the number elements is greater than the upper bound of the array then
						Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						// this will reset the size of the array
					}
					lngTemp = lngAcctCT - 1;
				}
				else
				{
					if (lngAcctCT > Information.UBound(ftAccountArray, 1))
					{
						// if the number elements is greater than the upper bound of the array then
						Array.Resize(ref ftAccountArray, lngAcctCT - 1 + 1);
						// this will reset the size of the array
					}
					lngTemp = lngAcctCT - 1;
				}
			}
			else
			{
				lngTemp = 0;
			}
			// use the -1 to account for the zero based array
			ftAccountArray[lngTemp].Account = strAcct;
			ftAccountArray[lngTemp].Amount = FCConvert.ToDecimal(dblAmt);
			ftAccountArray[lngTemp].Project = strProj;
			switch (intType)
			{
				case 0:
				case 1:
				case 2:
					{
						ftAccountArray[lngTemp].AcctType = "A";
						break;
					}
				case 3:
					{
						ftAccountArray[lngTemp].AcctType = "F";
						break;
					}
				case 4:
					{
						ftAccountArray[lngTemp].AcctType = "C";
						break;
					}
			}
			//end switch
		}

        private string MakeCashAccount(int intFund, int intAcct, string strLedg)
        {
            var fundLen = Convert.ToInt32(strLedg.Substring(0, 2));
            var acctLen = Convert.ToInt32(strLedg.Substring(2, 2));
            var suffixLen = Convert.ToInt32(strLedg.Substring(4, 2));
            return "G " + intFund.ToString().PadLeft(fundLen, '0') + "-" + intAcct.ToString().PadLeft(acctLen, '0') +
                   "-" + String.Empty.PadLeft(suffixLen, '0');
        }
	}
}
