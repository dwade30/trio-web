﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarRSReceiptDetail.
	partial class sarRSReceiptDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarRSReceiptDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldChange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblChange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCredit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCHK1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCHKAmt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldChange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCHK1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCHKAmt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldCash,
            this.fldCheck,
            this.fldCredit,
            this.fldChange,
            this.lblChange,
            this.lblCash,
            this.lblCheck,
            this.lblCredit,
            this.lblCHK1,
            this.lblCHKAmt1,
            this.lblCheckHeader});
			this.Detail.Height = 0.90625F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldCash
			// 
			this.fldCash.Height = 0.1875F;
			this.fldCash.Left = 5.3125F;
			this.fldCash.Name = "fldCash";
			this.fldCash.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCash.Text = "0.00";
			this.fldCash.Top = 0.1875F;
			this.fldCash.Width = 0.75F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 5.3125F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCheck.Text = "0.00";
			this.fldCheck.Top = 0.375F;
			this.fldCheck.Width = 0.75F;
			// 
			// fldCredit
			// 
			this.fldCredit.Height = 0.1875F;
			this.fldCredit.Left = 5.3125F;
			this.fldCredit.Name = "fldCredit";
			this.fldCredit.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCredit.Text = "0.00";
			this.fldCredit.Top = 0.5625F;
			this.fldCredit.Width = 0.75F;
			// 
			// fldChange
			// 
			this.fldChange.Height = 0.1875F;
			this.fldChange.Left = 5.3125F;
			this.fldChange.Name = "fldChange";
			this.fldChange.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldChange.Text = "0.00";
			this.fldChange.Top = 0F;
			this.fldChange.Width = 0.75F;
			// 
			// lblChange
			// 
			this.lblChange.Height = 0.1875F;
			this.lblChange.HyperLink = null;
			this.lblChange.Left = 4.5625F;
			this.lblChange.Name = "lblChange";
			this.lblChange.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblChange.Text = "Change";
			this.lblChange.Top = 0F;
			this.lblChange.Width = 0.75F;
			// 
			// lblCash
			// 
			this.lblCash.Height = 0.1875F;
			this.lblCash.HyperLink = null;
			this.lblCash.Left = 4.5625F;
			this.lblCash.Name = "lblCash";
			this.lblCash.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblCash.Text = "Cash";
			this.lblCash.Top = 0.1875F;
			this.lblCash.Width = 0.75F;
			// 
			// lblCheck
			// 
			this.lblCheck.Height = 0.1875F;
			this.lblCheck.HyperLink = null;
			this.lblCheck.Left = 4.5625F;
			this.lblCheck.Name = "lblCheck";
			this.lblCheck.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblCheck.Text = "Check";
			this.lblCheck.Top = 0.375F;
			this.lblCheck.Width = 0.75F;
			// 
			// lblCredit
			// 
			this.lblCredit.Height = 0.1875F;
			this.lblCredit.HyperLink = null;
			this.lblCredit.Left = 4.5625F;
			this.lblCredit.Name = "lblCredit";
			this.lblCredit.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblCredit.Text = "Credit";
			this.lblCredit.Top = 0.5625F;
			this.lblCredit.Width = 0.75F;
			// 
			// lblCHK1
			// 
			this.lblCHK1.Height = 0.1875F;
			this.lblCHK1.HyperLink = null;
			this.lblCHK1.Left = 0.6875F;
			this.lblCHK1.Name = "lblCHK1";
			this.lblCHK1.Style = "font-family: \'Tahoma\'";
			this.lblCHK1.Text = null;
			this.lblCHK1.Top = 0.1875F;
			this.lblCHK1.Visible = false;
			this.lblCHK1.Width = 1.3125F;
			// 
			// lblCHKAmt1
			// 
			this.lblCHKAmt1.Height = 0.1875F;
			this.lblCHKAmt1.HyperLink = null;
			this.lblCHKAmt1.Left = 2F;
			this.lblCHKAmt1.Name = "lblCHKAmt1";
			this.lblCHKAmt1.Style = "font-family: \'Tahoma\'";
			this.lblCHKAmt1.Text = null;
			this.lblCHKAmt1.Top = 0.1875F;
			this.lblCHKAmt1.Visible = false;
			this.lblCHKAmt1.Width = 1F;
			// 
			// lblCheckHeader
			// 
			this.lblCheckHeader.Height = 0.1875F;
			this.lblCheckHeader.HyperLink = null;
			this.lblCheckHeader.Left = 0.6875F;
			this.lblCheckHeader.Name = "lblCheckHeader";
			this.lblCheckHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblCheckHeader.Text = "Check List";
			this.lblCheckHeader.Top = 0.0625F;
			this.lblCheckHeader.Visible = false;
			this.lblCheckHeader.Width = 2.3125F;
			// 
			// sarRSReceiptDetail
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldChange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCHK1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCHKAmt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldChange;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblChange;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCredit;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCHK1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCHKAmt1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckHeader;
	}
}
