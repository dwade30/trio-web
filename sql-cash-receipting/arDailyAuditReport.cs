﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for arDailyAuditReport.
	/// </summary>
	public partial class arDailyAuditReport : BaseSectionReport
    {
		public static arDailyAuditReport InstancePtr
		{
			get
			{
				return (arDailyAuditReport)Sys.GetInstance(typeof(arDailyAuditReport));
			}
		}

		protected arDailyAuditReport _InstancePtr = null;
		
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
        double[] arrTotals = new double[7 + 1];
		public string strTellerID = string.Empty;
		public int intReportHeader;
		clsDRWrapper rsTotals = new clsDRWrapper();
		clsDRWrapper rsReceipts = new clsDRWrapper();
		int lngStart;
		int lngEnd;
		public bool blnFullAuditPreview;
		public bool boolUsePrevYears = false;
		
		public arDailyAuditReport()
		{
            InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit Report";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            try
            {
                if (eArgs.EOF) return;

                string strSQL = "";
                if (boolUsePrevYears)
                {
	                strSQL =
		                "SELECT COUNT(*) AS ReceiptCount, sum(Amount1) as Total1, sum(Amount2) as Total2, sum(Amount3) as Total3, sum(Amount4) as Total4, sum(Amount5) as Total5, sum(Amount6) as Total6, sum(ConvenienceFee) as ConvenienceFeeTotal " +
		                "FROM LastYearArchive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
                }
                else
                {
	                strSQL =
		                "SELECT COUNT(*) AS ReceiptCount, sum(Amount1) as Total1, sum(Amount2) as Total2, sum(Amount3) as Total3, sum(Amount4) as Total4, sum(Amount5) as Total5, sum(Amount6) as Total6, sum(ConvenienceFee) as ConvenienceFeeTotal " +
		                "FROM Archive WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
                }

				// , sum(ConvenienceFee) as ConvenienceFeeTotal
				rsTotals.OpenRecordset(strSQL);

                //if (modGlobal.Statics.gstrEPaymentPortal == "P" || modGlobal.Statics.gstrEPaymentPortal == "I")
                //{
                //    lblConvenienceFees.Visible = true;
                //    fldConvenienceFees.Visible = true;
                //    fldConvenienceFees.Text = Strings.Format(rsTotals.Get_Fields("ConvenienceFeeTotal"), "#,##0.00");
                //}
                //else
                //{
                    fldConvenienceFees.Visible = false;
                    lblConvenienceFees.Visible = false;
                    fldConvenienceFeeTotal.Text = "0.00";
                //}

                fldTotals1.Text = rsTotals.Get_Fields_Double("Total1").FormatAsCurrencyNoSymbol();
                fldTotals2.Text = rsTotals.Get_Fields_Double("Total2").FormatAsCurrencyNoSymbol();
                fldTotals3.Text = rsTotals.Get_Fields_Double("Total3").FormatAsCurrencyNoSymbol();
                fldTotals4.Text = rsTotals.Get_Fields_Double("Total4").FormatAsCurrencyNoSymbol();
                fldTotals5.Text = rsTotals.Get_Fields_Double("Total5").FormatAsCurrencyNoSymbol();
                fldTotals6.Text = rsTotals.Get_Fields_Double("Total6").FormatAsCurrencyNoSymbol();
                fldTotals7.Text = (rsTotals.Get_Fields_Double("Total1") + rsTotals.Get_Fields_Double("Total2") + rsTotals.Get_Fields_Double("Total3") + rsTotals.Get_Fields_Double("Total4") + rsTotals.Get_Fields_Double("Total5") + rsTotals.Get_Fields_Double("Total6")).FormatAsCurrencyNoSymbol();
                lblTotalsTotal.Text = rsTotals.Get_Fields_Int32("ReceiptCount") + " Receipts      Net Received:";
                if (fldTotals1.Text.IsNullOrWhiteSpace())  fldTotals1.Text = "0.00";
                if (fldTotals2.Text.IsNullOrWhiteSpace()) fldTotals2.Text = "0.00";
                if (fldTotals3.Text.IsNullOrWhiteSpace()) fldTotals3.Text = "0.00";
                if (fldTotals4.Text.IsNullOrWhiteSpace()) fldTotals4.Text = "0.00";
                if (fldTotals5.Text.IsNullOrWhiteSpace()) fldTotals5.Text = "0.00";
                if (fldTotals6.Text.IsNullOrWhiteSpace()) fldTotals6.Text = "0.00";
                if (fldTotals7.Text.IsNullOrWhiteSpace()) fldTotals7.Text = "0.00";
                if (fldConvenienceFeeTotal.Text.IsNullOrWhiteSpace()) fldConvenienceFeeTotal.Text = "0.00";
                FillSumTotals();

            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "FetchData Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsTotals.DisposeOf();
                // set up a new one after you clear out the existing one
                rsTotals = new clsDRWrapper();
            }
		}

		private void ActiveReport_Initialize()
		{
			try
			{
				boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
				if (frmDailyReceiptAudit.InstancePtr.cmbAudit.SelectedIndex == 0)
				{
					blnFullAuditPreview = true;
				}
				else
				{
					blnFullAuditPreview = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Initialize", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
            try
            {
                // On Error GoTo ERROR_HANDLER
                int intNum = 0;

                if (frmDailyReceiptAudit.InstancePtr.boolCloseOut && !frmRPTViewer.InstancePtr.blnRecreate)
                {
                    // IncrementSavedReports "LastCRDailyAudit"
                    // Use the ReportDates table to find out which number the report should use
                    intNum = modGlobal.SaveReportNumber_2("LastCRDailyAudit");
                    Document.Save(
                        System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCRDailyAudit") +
                        FCConvert.ToString(intNum) + ".RDF");
                    if (frmDailyReceiptAudit.InstancePtr.boolPerformingPartialCO)
                    {
                        // do not unload it
                    }
                    else
                    {
                        frmDailyReceiptAudit.InstancePtr.Unload();
                    }
                }

                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                frmWait.InstancePtr.Unload();
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly,
                    "Error Saving Report");
            }
            finally
            {
                rsTotals.DisposeOf();
                rsReceipts.DisposeOf();
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            clsDRWrapper rsCO = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                ActiveReport_Initialize();
                int lngMRN;
                int lngCT;
                string strSQL = "";
                lblMuniName.Text = modGlobalConstants.Statics.MuniName;
                lblDate.Text = DateTime.Now.FormatAndPadShortDate();
                lblTime.Text = DateAndTime.TimeOfDay.FormatAndPadTime();

                frmDailyReceiptAudit.InstancePtr.boolTellerOnly = frmDailyReceiptAudit.InstancePtr.cmbAudit.Text == "Teller Preview" || frmDailyReceiptAudit.InstancePtr.cmbAudit.Text == "Close Out Individual Teller";
                strTellerID = frmDailyReceiptAudit.InstancePtr.txtTellerID.Text;

                if (modGlobal.Statics.gdblAuditMarginAdjustmentTop != 0)
                {
                    this.PageSettings.Margins.Top += FCConvert.ToSingle(modGlobal.Statics.gdblAuditMarginAdjustmentTop * 240 / 1440f);
                }

                if (!modGlobal.Statics.gboolDailyAuditLandscape)
                {
	                PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
                }
                else
                {
	                PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                }

                SetupForm(PageSettings.Orientation);
                frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Loading...", true, 15);

                if (frmDailyReceiptAudit.InstancePtr.boolPerformingPartialCO)
                {
                    if (!frmDailyReceiptAudit.InstancePtr.boolCloseOut)
                    {
                        lblTellerID.Text = "PARTIAL PREVIEW";
                    }
                    else
                    {
                        lblTellerID.Text = "PARTIAL";
                    }
                }
                else
                {
                    if (!frmDailyReceiptAudit.InstancePtr.boolCloseOut)
                    {
                        lblTellerID.Text = "PREVIEW";
                    }
                }

                // this will setup the titles on each page
                if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
                {
                    lblTellerID.Text = lblTellerID.Text + "\r\n" + "Teller ID : " + strTellerID;
                    lblHeader.Text = "Cash Receipting Teller Report";
                }
                else
                {
                    lblHeader.Text = "Cash Receipting Report";
                }

                if (frmRPTViewer.InstancePtr.blnRecreate)
                {
                    rsCO.OpenRecordset("SELECT CloseoutDate FROM CloseOut WHERE ID = " + frmDailyReceiptAudit.InstancePtr.lngCloseoutKey.ToString(), modExtraModules.strCRDatabase);

                    if (rsCO.RecordCount() > 0)
                    {
                        lblRecreateOrigDate.Text = "Close Out Date: " + rsCO.Get_Fields_DateTime("CloseoutDate").FormatAndPadShortDate();
                    }
                    else
                    {
                        lblRecreateOrigDate.Text = "Close Out Date: Unknown";
                    }

                    lblRecreateOrigDate.Visible = true;
                }
                else
                {
                    lblRecreateOrigDate.Visible = false;
                }

                sarTypesOb.Report = new sarTypes();
                sarCRSummaryOB.Report = new sarAccountingSummary();
                // Set sarCRSummaryOB.Object = New sarCashReceiptsSummary
	            sarCheckReportOb.Report = new sarCheckReport();
	            if (modGlobal.Statics.gblnPrintCCReport)
	            {
		            sarCCReportOB.Report = new sarCCReport();
	            }
	            else
	            {
		            sarCCReportOB.Visible = false;
	            }

                if (modGlobal.Statics.gboolTellerReportWithAudit && modGlobal.Statics.gstrTeller != "N")
                {
                    sarTellerReportOb.Report = new sarTellerReport();
                }
                else
                {
                    sarTellerReportOb.Visible = false;
                }

 
                frmWait.InstancePtr.IncrementProgress();

            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsCO.DisposeOf();
            }
		}

		private void SetupForm(GrapeCity.ActiveReports.Document.Section.PageOrientation intStyle)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!(intStyle == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape))
				{
					// normal
					lblHeader.Width = 10800 / 1440f;
					lblTellerID.Width = 10800 / 1440f;
					sarTypesOb.Width = 10800 / 1440f;
					sarCRSummaryOB.Width = 10800 / 1440f;
					sarCheckReportOb.Width = 10800 / 1440f;
					sarTellerReportOb.Width = 10800 / 1440f;
					this.PrintWidth = 10800 / 1440f;
					fldConvenienceFeeTotal.Width = 1500 / 1440f;
					fldTotals1.Width = 1500 / 1440f;
					fldTotals2.Width = 1500 / 1440f;
					fldTotals3.Width = 1500 / 1440f;
					fldTotals4.Width = 1500 / 1440f;
					fldTotals5.Width = 1500 / 1440f;
					fldTotals6.Width = 1500 / 1440f;
					fldTotals7.Width = 1500 / 1440f;
					lblRecreateOrigDate.Width = 10800 / 1440f;
                    fldConvenienceFeeTotal.Left = 3630 / 1440f;
					fldTotals1.Left = 4290 / 1440f;
					// + 40  ' 4440 + 40
					fldTotals2.Left = 5250 / 1440f;
					// + 40
					fldTotals3.Left = 6060 / 1440f;
					// + 40
					fldTotals4.Left = 6870 / 1440f;
					// + 40
					fldTotals5.Left = 7680 / 1440f;
					// + 40
					fldTotals6.Left = 8490 / 1440f;
					// + 40
					fldTotals7.Left = 9300 / 1440f;
					// + 40
				}
				else
				{
					// landscape
					lblHeader.Width = 15120 / 1440f;
					lblTellerID.Width = 15120 / 1440f;
					sarTypesOb.Width = 15120 / 1440f;
					sarCRSummaryOB.Width = 15120 / 1440f;
					sarCheckReportOb.Width = 15120 / 1440f;
					sarTellerReportOb.Width = 15120 / 1440f;
					this.PrintWidth = 15120 / 1440f;
					fldTotals1.Width = 1440 / 1440f;
					fldTotals2.Width = 1440 / 1440f;
					fldTotals3.Width = 1440 / 1440f;
					fldTotals4.Width = 1440 / 1440f;
					fldTotals5.Width = 1440 / 1440f;
					fldTotals6.Width = 1440 / 1440f;
					fldTotals7.Width = 1440 / 1440f;
					lblTotalsTotal.Left = 1370 / 1440f;
					lnSubtotal.X2 = 15120 / 1440f;
					fldTotals1.Left = 4320 / 1440f;
					fldTotals2.Left = 5760 / 1440f;
					fldTotals3.Left = 7200 / 1440f;
					fldTotals4.Left = 8640 / 1440f;
					fldTotals5.Left = 10080 / 1440f;
					fldTotals6.Left = 11520 / 1440f;
					fldTotals7.Left = 13740 / 1440f;
					fldTotals2.Top = fldTotals1.Top;
					fldTotals3.Top = fldTotals1.Top;
					fldTotals4.Top = fldTotals1.Top;
					fldTotals5.Top = fldTotals1.Top;
					fldTotals6.Top = fldTotals1.Top;
					fldTotals7.Top = fldTotals1.Top;
					lblRecreateOrigDate.Width = 15120 / 1440f;
				}
				lblDate.Left = lblHeader.Width - (lblDate.Width + 70 / 1440f);
				lblPageNumber.Left = lblDate.Left;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Setup Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillSumTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the totals at the bottom of the first report
				clsDRWrapper rsTotals = new clsDRWrapper();

                try
                {
                    string strSQL;
                    string strTellerList;
                    string strTellerCO = "";
                    decimal dblCash = 0;
                    decimal dblCheck = 0;
                    decimal dblCredit = 0;
                    // vbPorter upgrade warning: dblPrevious As double	OnWrite(string, short)
                    decimal dblPrevious = 0;
                    decimal dblOther;
                    // vbPorter upgrade warning: dblTemp As double	OnWrite(double, string)
                    decimal dblTemp = 0;
                    string strTable = "";
                    // clear all of the fields so they can be filled with the correct values
                    fldTotalReceivedToday.Text = "0.00";
                    fldTotalReversalsToday.Text = "0.00";
                    fldTotalNonCashToday.Text = "0.00";
                    fldTotalReceivedAll.Text = "0.00";
                    fldTotalReversalsAll.Text = "0.00";
                    fldTotalNonCashAll.Text = "0.00";
                    fldNetActivityToday.Text = "0.00";
                    fldNetActivityAll.Text = "0.00";
                    fldTotalCash.Text = "0.00";
                    fldTotalCheck.Text = "0.00";
                    fldTotalCredit.Text = "0.00";
                    fldTotalOther.Text = "0.00";
                    fldTotalPrevious.Text = "0.00";
                    fldTotalTotal.Text = "0.00";
                    
                    // this will create a list of tellers that have already closed out today
                    strSQL = "SELECT TellerID FROM CloseOut WHERE Type = 'T' AND CloseoutDate = '" + DateTime.Today.ToShortDateString() + "' ORDER BY CloseoutDate";
                    rsTotals.OpenRecordset(strSQL);
                    
                    strTellerList = "";
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        do
                        {
                            if (FCConvert.ToString(rsTotals.Get_Fields_String("TellerID")) != "")
                            {
                                strTellerList += rsTotals.Get_Fields_String("TellerID") + ",";
                            }
                            rsTotals.MoveNext();
                        }
                        while (!rsTotals.EndOfFile());
                        if (Strings.InStr(1, strTellerList, ",", CompareConstants.vbBinaryCompare) != 0)
                        {
                            strTellerList = Strings.Left(strTellerList, strTellerList.Length - 1);
                        }
                    }

                    if (strTellerList != "")
                    {
                        // these are the arguements that will determine if the teller has closed out today
                        strTellerCO = " AND ISNULL(TellerCloseOut,0) <> 0 OR (ISNULL(TellerCloseOut,0) = 0 AND NOT (TellerID IN (" + strTellerList + ")))";
                    }
                    else
                    {
                        strTellerCO = "";
                    }

                    if (boolUsePrevYears)
                    {
	                    strTable = "LastYearArchive";
                    }
                    else
                    {
	                    strTable = "Archive";
                    }

                    // TODAY-----------------------------------------------------------------------------------------
                    // Total Received Today
                    // first find out any eft that will have to be subtracted
                    strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1 GROUP BY Account1) ";
                    strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1 GROUP BY Account2) ";
                    strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1 GROUP BY Account3) ";
                    strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1 GROUP BY Account4) ";
                    strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1 GROUP BY Account5) ";
                    strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1 GROUP BY Account6)";
                    strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalEFT WHERE Account <> ''";
                    rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        if (rsTotals.Get_Fields_Double("SumAmount").ToDecimal() != 0)
                        {
                            dblTemp = rsTotals.Get_Fields_Double("SumAmount").ToDecimal();
                        }
                        else
                        {
                            dblTemp = 0;
                        }
                    }
                    else
                    {
                        dblTemp = 0;
                    }
                    strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount1 > 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account1) ";
                    strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount2 > 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account2) ";
                    strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount3 > 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account3) ";
                    strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount4 > 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account4) ";
                    strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount5 > 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account5) ";
                    strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount6 > 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account6) ";
                    modGlobal.Statics.strTotalPositiveReceiptsSQL = strSQL;
                    strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalPositiveReceipts";
                    
                    rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        fldTotalReceivedToday.Text = rsTotals.Get_Fields_Double("SumAmount") != 0 
                            ? rsTotals.Get_Fields_Double("SumAmount").FormatAsCurrencyNoSymbol() 
                            :  "0.00";
                    }
                    else
                    {
                        fldTotalReceivedToday.Text = "0.00";
                    }
                    // Total Reversals Today
                    strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount1 < 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account1) ";
                    strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount2 < 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account2) ";
                    strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount3 < 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account3) ";
                    strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount4 < 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account4) ";
                    strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount5 < 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account5) ";
                    strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount6 < 0 AND AffectCashDrawer = 1" + strTellerCO + " GROUP BY Account6) ";
                    strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalNegativeReceipts";
                    rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        fldTotalReversalsToday.Text = (rsTotals.Get_Fields_Double("SumAmount") * -1).FormatAsCurrencyNoSymbol();
                    }
                    else
                    {
                        fldTotalReversalsToday.Text = "0.00";
                    }
                    // Total Non-Cash Activity Today      '06/28/2004 - Taken out because there should be no non cash for today  that would have to be a True False which we do not allow. JB
                    fldTotalNonCashToday.Text = "0.00";
                    strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount1 > 0 GROUP BY Account1) ";
                    strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount2 > 0 GROUP BY Account2) ";
                    strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount3 > 0 GROUP BY Account3) ";
                    strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount4 > 0 GROUP BY Account4) ";
                    strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount5 > 0 GROUP BY Account5) ";
                    strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount6 > 0 GROUP BY Account6) ";
                    strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalPositiveReceipts";
                    rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        fldTotalReceivedAll.Text = (rsTotals.Get_Fields_Double("SumAmount").ToDecimal() + dblTemp).FormatAsCurrencyNoSymbol();
                    }
                    else
                    {
                        fldTotalReceivedAll.Text = dblTemp.FormatAsCurrencyNoSymbol();
                    }
                    // Total Reversals All
                    strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount1 < 0 GROUP BY Account1) ";
                    strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount2 < 0 GROUP BY Account2) ";
                    strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount3 < 0 GROUP BY Account3) ";
                    strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount4 < 0 GROUP BY Account4) ";
                    strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount5 < 0 GROUP BY Account5) ";
                    strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND EFT = 0 AND Amount6 < 0 GROUP BY Account6) ";
                    strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalNegativeReceipts";
                    rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        fldTotalReversalsAll.Text = (rsTotals.Get_Fields_Double("SumAmount") * -1).FormatAsCurrencyNoSymbol();
                    }
                    else
                    {
                        fldTotalReversalsAll.Text = "0.00";
                    }
                    // Total Non-Cash Activity All
                    strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 0 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account1) ";
                    strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 0 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account2) ";
                    strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 0 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account3) ";
                    strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 0 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account4) ";
                    strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 0 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account5) ";
                    strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 0 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account6) ";
                    strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalNonCash WHERE Account <> ''";
                    rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        fldTotalNonCashAll.Text = (rsTotals.Get_Fields_Double("SumAmount") * -1).FormatAsCurrencyNoSymbol();
                    }
                    else
                    {
                        fldTotalNonCashAll.Text = "0.00";
                    }
                    // Net Activity
                    // this will make sure all fields have values
                    if (fldTotalReceivedToday.Text == "")
                        fldTotalReceivedToday.Text = "0.00";
                    if (fldTotalReversalsToday.Text == "")
                        fldTotalReversalsToday.Text = "0.00";
                    if (fldTotalNonCashToday.Text == "")
                        fldTotalNonCashToday.Text = "0.00";
                    if (fldTotalReceivedAll.Text == "")
                        fldTotalReceivedAll.Text = "0.00";
                    if (fldTotalReversalsAll.Text == "")
                        fldTotalReversalsAll.Text = "0.00";
                    if (fldTotalNonCashAll.Text == "")
                        fldTotalNonCashAll.Text = "0.00";
                    // this adds them up
                    fldNetActivityToday.Text = (fldTotalReceivedToday.Text.ToDecimalValue() - (fldTotalReversalsToday.Text.ToDecimalValue() - fldTotalNonCashToday.Text.ToDecimalValue())).FormatAsCurrencyNoSymbol();
                    fldNetActivityAll.Text = (fldTotalReceivedAll.Text.ToDecimalValue() - (fldTotalReversalsAll.Text.ToDecimalValue() - fldTotalNonCashAll.Text.ToDecimalValue())).FormatAsCurrencyNoSymbol();
                    // Previous Period Activity
                    strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account1) ";
                    strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account2) ";
                    strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account3) ";
                    strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account4) ";
                    strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account5) ";
                    strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account6) ";
                    strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as PreviousPeriodActivity WHERE Account <> ''";
                    rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
                    {
                        dblPrevious = rsTotals.Get_Fields_Double("SumAmount").ToDecimal() + dblTemp;
                    }
                    else
                    {
                        dblPrevious = 0;
                    }

                    if (boolUsePrevYears)
                    {
	                    strSQL =
		                    "SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC , SUM(ConvenienceFee) as Convenience " +
		                    "FROM (SELECT DISTINCT LastYearArchive.ReceiptNumber, LastYearReceipt.CardAmount, LastYearReceipt.CheckAmount, LastYearReceipt.CashAmount, LastYearReceipt.ConvenienceFee  " +
		                    "FROM LastYearArchive INNER JOIN LastYearReceipt ON LastYearArchive.ReceiptNumber = LastYearReceipt.ReceiptKey "+
		                    "WHERE LastYearReceipt.EFT = 0 AND AffectCash = 1 AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") as temp";
                    }
                    else
                    {
						strSQL = 
							"SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC , SUM(ConvenienceFee) as Convenience " +
							"FROM (SELECT DISTINCT Archive.ReceiptNumber, Receipt.CardAmount, Receipt.CheckAmount, Receipt.CashAmount, Receipt.ConvenienceFee " +
							"FROM Archive INNER JOIN Receipt ON Archive.ReceiptNumber = Receipt.ReceiptKey " +
							"WHERE Receipt.EFT = 0 AND AffectCash = 1 AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") as temp";
                    }

					rsTotals.OpenRecordset(strSQL);
                    if (rsTotals.EndOfFile() != true)
                    {
                        if (boolUsePrevYears)
                        {
                            dblCash = rsTotals.Get_Fields_Decimal("CSH");
                            dblCheck = rsTotals.Get_Fields_Decimal("CHK");
                            dblCredit = rsTotals.Get_Fields_Decimal("CC");// + rsTotals.Get_Fields_Decimal("ConvenienceFee");
                        }
                        else
                        {
                            dblCash = rsTotals.Get_Fields_Double("CSH").ToDecimal() ;
                            dblCheck = rsTotals.Get_Fields_Double("CHK").ToDecimal();
                            dblCredit = rsTotals.Get_Fields_Double("CC").ToDecimal();// + rsTotals.Get_Fields_Decimal("ConvenienceFee") : 0;
                        }
                        

                        // Total Cash
                        fldTotalCash.Text = dblCash.FormatAsCurrencyNoSymbol();
                        // Total Checks
                        fldTotalCheck.Text = dblCheck.FormatAsCurrencyNoSymbol();
                        // Total Credit Card
                        fldTotalCredit.Text = dblCredit.FormatAsCurrencyNoSymbol();
                        // Previous Period Value
                        fldTotalPrevious.Text = dblPrevious.FormatAsCurrencyNoSymbol();
                        // Total Other
                        fldTotalOther.Text = (fldNetActivityAll.Text.ToDecimalValue() - (dblCash + dblCheck + dblCredit + dblPrevious)).FormatAsCurrencyNoSymbol();
                        // Total
                        fldTotalTotal.Text = fldNetActivityAll.Text.ToDecimalValue().FormatAsCurrencyNoSymbol();
                    }
                    return;
                }
                catch (Exception ex)
                {
                    // ERROR_HANDLER:
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Fill Sum Totals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            finally
            {
                rsTotals.DisposeOf();
            }
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			rsReceipts.Dispose();
            rsTotals.Dispose();
			frmDailyReceiptAudit.InstancePtr.Close();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will switch the heading on each page to suit which report is being run
				switch (intReportHeader)
				{
					case 0:
						{
							lblHeader.Text = "Cash Receipting - Audit Report";
							break;
						}
					case 1:
						{
							lblHeader.Text = "Cash Receipting - Accounting Summary";
							break;
						}
					case 2:
						{
							lblHeader.Text = "Cash Receipting - Check Report";
							break;
						}
					case 3:
						{
							if (modGlobal.Statics.gboolTellerReportWithAudit && modGlobal.Statics.gstrTeller != "N")
							{
								lblHeader.Text = "Cash Receipting - Teller Report";
								// Else
								// lblHeader.Text = "Cash Receipting - Missing Receipts Report"
							}
							// Case 4
							// lblHeader.Text = "Cash Receipting - Missing Receipts Report"
							break;
						}
					case 5:
						{
							// Tracker Reference: 14953
							lblHeader.Text = "Cash Receipting - Credit/Debit Card Report";
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Page Header Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
    }
}
