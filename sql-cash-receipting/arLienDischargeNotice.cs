using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for arLienDischargeNotice.
	/// </summary>
	public partial class arLienDischargeNotice : FCSectionReport
	{

		public static arLienDischargeNotice InstancePtr
		{
			get
			{
				return (arLienDischargeNotice)Sys.GetInstance(typeof(arLienDischargeNotice));
			}
		}

		protected arLienDischargeNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public arLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
		}
	}
}
