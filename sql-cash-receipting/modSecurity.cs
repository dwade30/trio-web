﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	public class modSecurity
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/26/2002              *
		// ********************************************************
		// vbPorter upgrade warning: obForm As object	OnWrite(MDIParent, frmReceiptInput)
		public static bool ValidPermissions_6(object obForm, int lngFuncID, bool boolHandlePartials = true)
		{
			return ValidPermissions(obForm, lngFuncID, boolHandlePartials);
		}

		public static bool ValidPermissions_8(object obForm, int lngFuncID, bool boolHandlePartials = true)
		{
			return ValidPermissions(obForm, lngFuncID, boolHandlePartials);
		}

		public static bool ValidPermissions(object obForm, int lngFuncID, bool boolHandlePartials = true)
		{
			bool ValidPermissions = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strPerm;
				// checks the permissions and returns false if there are no permissions
				// if there are partial permissions then it calls the function for the form
				// if false is passed in then partial permissions will not be handled by the function
				// This function is for MDI menu options.  At the time this is called the child
				// menu options wont be present, so you cant disable them yet
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngFuncID));
				if (strPerm == "F")
				{
					ValidPermissions = true;
				}
				else if (strPerm == "N")
				{
					ValidPermissions = false;
				}
				else if (strPerm == "P")
				{
					ValidPermissions = true;
					if (boolHandlePartials)
					{
						FCUtils.CallByName(obForm, "HandlePartialPermission", CallType.Method, lngFuncID);
					}
				}
				return ValidPermissions;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description);
			}
			return ValidPermissions;
		}
	}
}
