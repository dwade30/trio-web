﻿using Global;
using SharedApplication.CashReceipts;
using SharedApplication.Messaging;

namespace TWCR0000
{
    public class ShowREBookPageReportFromCRHandler : CommandHandler<ShowREBookPageReportFromCR>
    {
        protected override void Handle(ShowREBookPageReportFromCR command)
        {
            arBookPageAccount.InstancePtr.Init(command.Account, command.Account);
        }
    }
}