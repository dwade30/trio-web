﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for arMVR3Listing.
	/// </summary>
	partial class arMVR3Listing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arMVR3Listing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateNow = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMVR3Number = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExcise = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPlate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTLR = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMVR3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTeller = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFooterExcise = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateNow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMVR3Number)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTLR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.AcrossDown;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldName,
				this.fldMVR3,
				this.fldExcise,
				this.fldPlate,
				this.fldReceipt,
				this.fldTeller,
				this.fldDate
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooterTotal,
				this.lblFooterExcise,
				this.Line2
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1,
				this.lblDateRange,
				this.lblDateNow,
				this.lblName,
				this.lblMVR3Number,
				this.lblExcise,
				this.lblPlate,
				this.lblReceipt,
				this.lblTLR,
				this.lblDate,
				this.lblMuniName,
				this.lblTime,
				this.lblPage
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "MVR3 Listing";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.8125F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0.8125F;
			this.Line1.Y2 = 0.8125F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.25F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 0F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblDateRange.Text = null;
			this.lblDateRange.Top = 0.25F;
			this.lblDateRange.Width = 7.5F;
			// 
			// lblDateNow
			// 
			this.lblDateNow.Height = 0.1875F;
			this.lblDateNow.HyperLink = null;
			this.lblDateNow.Left = 6.375F;
			this.lblDateNow.Name = "lblDateNow";
			this.lblDateNow.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblDateNow.Text = null;
			this.lblDateNow.Top = 0F;
			this.lblDateNow.Width = 1.125F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0F;
			this.lblName.MultiLine = false;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.625F;
			this.lblName.Width = 1.5F;
			// 
			// lblMVR3Number
			// 
			this.lblMVR3Number.Height = 0.1875F;
			this.lblMVR3Number.HyperLink = null;
			this.lblMVR3Number.Left = 2.3125F;
			this.lblMVR3Number.MultiLine = false;
			this.lblMVR3Number.Name = "lblMVR3Number";
			this.lblMVR3Number.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblMVR3Number.Text = "MVR3 Number";
			this.lblMVR3Number.Top = 0.625F;
			this.lblMVR3Number.Width = 1.0625F;
			// 
			// lblExcise
			// 
			this.lblExcise.Height = 0.1875F;
			this.lblExcise.HyperLink = null;
			this.lblExcise.Left = 3.4375F;
			this.lblExcise.MultiLine = false;
			this.lblExcise.Name = "lblExcise";
			this.lblExcise.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblExcise.Text = "Excise";
			this.lblExcise.Top = 0.625F;
			this.lblExcise.Width = 0.75F;
			// 
			// lblPlate
			// 
			this.lblPlate.Height = 0.1875F;
			this.lblPlate.HyperLink = null;
			this.lblPlate.Left = 4.375F;
			this.lblPlate.MultiLine = false;
			this.lblPlate.Name = "lblPlate";
			this.lblPlate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblPlate.Text = "Plate";
			this.lblPlate.Top = 0.625F;
			this.lblPlate.Width = 0.5F;
			// 
			// lblReceipt
			// 
			this.lblReceipt.Height = 0.1875F;
			this.lblReceipt.HyperLink = null;
			this.lblReceipt.Left = 5.1875F;
			this.lblReceipt.MultiLine = false;
			this.lblReceipt.Name = "lblReceipt";
			this.lblReceipt.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblReceipt.Text = "Receipt";
			this.lblReceipt.Top = 0.625F;
			this.lblReceipt.Width = 0.5625F;
			// 
			// lblTLR
			// 
			this.lblTLR.Height = 0.1875F;
			this.lblTLR.HyperLink = null;
			this.lblTLR.Left = 6F;
			this.lblTLR.MultiLine = false;
			this.lblTLR.Name = "lblTLR";
			this.lblTLR.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTLR.Text = "Teller";
			this.lblTLR.Top = 0.625F;
			this.lblTLR.Width = 0.4375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.6875F;
			this.lblDate.MultiLine = false;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.625F;
			this.lblDate.Width = 0.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.6875F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.125F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.3125F;
			// 
			// fldMVR3
			// 
			this.fldMVR3.Height = 0.1875F;
			this.fldMVR3.Left = 2.3125F;
			this.fldMVR3.Name = "fldMVR3";
			this.fldMVR3.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldMVR3.Text = null;
			this.fldMVR3.Top = 0F;
			this.fldMVR3.Width = 1.0625F;
			// 
			// fldExcise
			// 
			this.fldExcise.Height = 0.1875F;
			this.fldExcise.Left = 3.4375F;
			this.fldExcise.Name = "fldExcise";
			this.fldExcise.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldExcise.Text = null;
			this.fldExcise.Top = 0F;
			this.fldExcise.Width = 0.8125F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 4.375F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldPlate.Text = null;
			this.fldPlate.Top = 0F;
			this.fldPlate.Width = 0.6875F;
			// 
			// fldReceipt
			// 
			this.fldReceipt.Height = 0.1875F;
			this.fldReceipt.Left = 5.1875F;
			this.fldReceipt.Name = "fldReceipt";
			this.fldReceipt.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldReceipt.Text = null;
			this.fldReceipt.Top = 0F;
			this.fldReceipt.Width = 0.6875F;
			// 
			// fldTeller
			// 
			this.fldTeller.Height = 0.1875F;
			this.fldTeller.Left = 6F;
			this.fldTeller.Name = "fldTeller";
			this.fldTeller.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldTeller.Text = null;
			this.fldTeller.Top = 0F;
			this.fldTeller.Width = 0.5625F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 6.6875F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.75F;
			// 
			// lblFooterTotal
			// 
			this.lblFooterTotal.Height = 0.1875F;
			this.lblFooterTotal.HyperLink = null;
			this.lblFooterTotal.Left = 0F;
			this.lblFooterTotal.MultiLine = false;
			this.lblFooterTotal.Name = "lblFooterTotal";
			this.lblFooterTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblFooterTotal.Text = "Total:";
			this.lblFooterTotal.Top = 0.0625F;
			this.lblFooterTotal.Width = 2.625F;
			// 
			// lblFooterExcise
			// 
			this.lblFooterExcise.Height = 0.1875F;
			this.lblFooterExcise.HyperLink = null;
			this.lblFooterExcise.Left = 3.0625F;
			this.lblFooterExcise.MultiLine = false;
			this.lblFooterExcise.Name = "lblFooterExcise";
			this.lblFooterExcise.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblFooterExcise.Text = "0.00";
			this.lblFooterExcise.Top = 0.0625F;
			this.lblFooterExcise.Width = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.3125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.0625F;
			this.Line2.Width = 0.9375F;
			this.Line2.X1 = 4.25F;
			this.Line2.X2 = 3.3125F;
			this.Line2.Y1 = 0.0625F;
			this.Line2.Y2 = 0.0625F;
			// 
			// arMVR3Listing
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateNow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMVR3Number)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTLR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMVR3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExcise;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTeller;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterExcise;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateNow;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMVR3Number;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExcise;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPlate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTLR;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
