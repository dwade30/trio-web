﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTellerCheckReport.
	/// </summary>
	public partial class sarTellerCheckReport : FCSectionReport
	{
		public static sarTellerCheckReport InstancePtr
		{
			get
			{
				return (sarTellerCheckReport)Sys.GetInstance(typeof(sarTellerCheckReport));
			}
		}

		protected sarTellerCheckReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTellerCheckReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/17/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/17/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL;
		string strTeller = "";

		public sarTellerCheckReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarTellerCheckReport_ReportEnd;
		}

        private void SarTellerCheckReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [BankKey] not found!! (maybe it is an alias?)
				sarCheckReportDetailOB.Report.UserData = rsData.Get_Fields("BankKey") + "||" + strTeller;
				// SetData
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// This sql statement will find the totals for each bank and display them at the bottom of each detail
			strTeller = FCConvert.ToString(this.UserData);
			strSQL = "SELECT DISTINCT ReceiptNumber FROM Archive WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND TellerID = '" + strTeller + "'";
			rsData.OpenRecordset("SELECT * FROM Bank WHERE ID = 0");
			strSQL = "SELECT * FROM Bank INNER JOIN (SELECT sum(CheckMaster.Amount) as sumAmt, Bank.ID as BankKey FROM (CheckMaster INNER JOIN Bank ON CheckMaster.BankNumber = Bank.ID) INNER JOIN (" + strSQL + ") as NonClosedOutReceiptNumbers ON CheckMaster.ReceiptNumber = NonClosedOutReceiptNumbers.ReceiptNumber WHERE ISNULL(EFT,0) = 0 GROUP BY Bank.ID ) AS BK ON Bank.ID = BK.Bankkey ORDER BY Name";
			sarCheckReportDetailOB.Report = new sarTellerCheckReportDetail();
			rsData.OpenRecordset(strSQL);
			if (modGlobal.Statics.gintCheckReportOrder == 2)
			{
				rsData.MoveLast();
			}
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [BankKey] not found!! (maybe it is an alias?)
				sarCheckReportDetailOB.Report.UserData = rsData.Get_Fields("BankKey") + "||" + strTeller;
			}
			else
			{
				// this will leave a blank page
				// HideGroupFooter
				ShowFakeHeaders();
			}
		}

		private void GroupFooter1_AfterPrint(object sender, EventArgs e)
		{
			if (modGlobal.Statics.gboolTellerReportWithAudit)
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 3;
			}
			else
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 4;
			}
		}

		private void ShowFakeHeaders()
		{
			// this routine will show a display of zero checks and zero check amount
			Line2.Visible = true;
			lblBankNumber.Visible = true;
			lblBankNumberTotal.Visible = true;
			lblCheckNumber.Visible = true;
			lblReceiptNumber.Visible = true;
			lblAmount.Visible = true;
			fldBankTotal.Visible = true;
		}

		private void sarTellerCheckReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarTellerCheckReport.Caption	= "Check Report";
			//sarTellerCheckReport.Icon	= "sarTellerCheckReport.dsx":0000";
			//sarTellerCheckReport.Left	= 0;
			//sarTellerCheckReport.Top	= 0;
			//sarTellerCheckReport.Width	= 11880;
			//sarTellerCheckReport.Height	= 8580;
			//sarTellerCheckReport.StartUpPosition	= 3;
			//sarTellerCheckReport.SectionData	= "sarTellerCheckReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
