﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmMVR3Listing.
	/// </summary>
	partial class frmMVR3Listing : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkPrint;
		public fecherFoundation.FCFrame fraMVCO;
		public fecherFoundation.FCComboBox cmbMVCOStart;
		public fecherFoundation.FCComboBox cmbMVCOEnd;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCFrame fraOptions;
		public fecherFoundation.FCCheckBox chkPrint_4;
		public fecherFoundation.FCCheckBox chkPrint_3;
		public fecherFoundation.FCCheckBox chkPrint_2;
		public fecherFoundation.FCCheckBox chkPrint_1;
		public fecherFoundation.FCCheckBox chkPrint_0;
		public fecherFoundation.FCFrame fraChoice;
		public fecherFoundation.FCFrame fraRange;
		public Global.T2KDateBox txtStart;
		public Global.T2KDateBox txtEnd;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraDate;
		public fecherFoundation.FCComboBox cmbMonth;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileBack;
		public fecherFoundation.FCToolStripMenuItem mnuFileNext;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMVR3Listing));
			this.fraMVCO = new fecherFoundation.FCFrame();
			this.cmbMVCOStart = new fecherFoundation.FCComboBox();
			this.cmbMVCOEnd = new fecherFoundation.FCComboBox();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.fraOptions = new fecherFoundation.FCFrame();
			this.chkPrint_4 = new fecherFoundation.FCCheckBox();
			this.chkPrint_3 = new fecherFoundation.FCCheckBox();
			this.chkPrint_2 = new fecherFoundation.FCCheckBox();
			this.chkPrint_1 = new fecherFoundation.FCCheckBox();
			this.chkPrint_0 = new fecherFoundation.FCCheckBox();
			this.fraChoice = new fecherFoundation.FCFrame();
			this.fraRange = new fecherFoundation.FCFrame();
			this.txtStart = new Global.T2KDateBox();
			this.txtEnd = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.fraDate = new fecherFoundation.FCFrame();
			this.cmbMonth = new fecherFoundation.FCComboBox();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileBack = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileNext = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdPreview = new fecherFoundation.FCButton();
			this.cmdBack = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMVCO)).BeginInit();
			this.fraMVCO.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraOptions)).BeginInit();
			this.fraOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChoice)).BeginInit();
			this.fraChoice.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).BeginInit();
			this.fraDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBack)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 298);
			this.BottomPanel.Size = new System.Drawing.Size(715, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraMVCO);
			this.ClientArea.Controls.Add(this.fraOptions);
			this.ClientArea.Controls.Add(this.fraChoice);
			this.ClientArea.Size = new System.Drawing.Size(735, 570);
			this.ClientArea.Controls.SetChildIndex(this.fraChoice, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraOptions, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraMVCO, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdBack);
			this.TopPanel.Controls.Add(this.cmdPreview);
			this.TopPanel.Size = new System.Drawing.Size(735, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPreview, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdBack, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(86, 28);
			this.HeaderText.Text = "Options";
			// 
			// fraMVCO
			// 
			this.fraMVCO.Controls.Add(this.cmbMVCOStart);
			this.fraMVCO.Controls.Add(this.cmbMVCOEnd);
			this.fraMVCO.Controls.Add(this.Label7);
			this.fraMVCO.Controls.Add(this.Label8);
			this.fraMVCO.Location = new System.Drawing.Point(30, 30);
			this.fraMVCO.Name = "fraMVCO";
			this.fraMVCO.Size = new System.Drawing.Size(360, 150);
			this.fraMVCO.TabIndex = 1;
			this.fraMVCO.Text = "Include Close Outs";
			this.fraMVCO.Visible = false;
			// 
			// cmbMVCOStart
			// 
			this.cmbMVCOStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMVCOStart.Location = new System.Drawing.Point(93, 30);
			this.cmbMVCOStart.Name = "cmbMVCOStart";
			this.cmbMVCOStart.Size = new System.Drawing.Size(241, 40);
			this.cmbMVCOStart.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.cmbMVCOStart, "Select the correct month and press F12.");
			this.cmbMVCOStart.SelectedIndexChanged += new System.EventHandler(this.cmbMVCOStart_SelectedIndexChanged);
			// 
			// cmbMVCOEnd
			// 
			this.cmbMVCOEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMVCOEnd.Location = new System.Drawing.Point(93, 90);
			this.cmbMVCOEnd.Name = "cmbMVCOEnd";
			this.cmbMVCOEnd.Size = new System.Drawing.Size(241, 40);
			this.cmbMVCOEnd.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cmbMVCOEnd, "Select the correct year and press F12.");
			// 
			// Label7
			// 
			this.Label7.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label7.Location = new System.Drawing.Point(20, 104);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(33, 16);
			this.Label7.TabIndex = 2;
			this.Label7.Text = "LAST";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label8
			// 
			this.Label8.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label8.Location = new System.Drawing.Point(20, 44);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(37, 16);
			this.Label8.TabIndex = 4;
			this.Label8.Text = "FIRST";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraOptions
			// 
			this.fraOptions.Controls.Add(this.chkPrint_4);
			this.fraOptions.Controls.Add(this.chkPrint_3);
			this.fraOptions.Controls.Add(this.chkPrint_2);
			this.fraOptions.Controls.Add(this.chkPrint_1);
			this.fraOptions.Controls.Add(this.chkPrint_0);
			this.fraOptions.Location = new System.Drawing.Point(30, 30);
			this.fraOptions.Name = "fraOptions";
			this.fraOptions.Size = new System.Drawing.Size(245, 268);
			this.fraOptions.TabIndex = 2;
			this.fraOptions.Text = "Enter Your Selection";
			// 
			// chkPrint_4
			// 
			this.chkPrint_4.Location = new System.Drawing.Point(20, 222);
			this.chkPrint_4.Name = "chkPrint_4";
			this.chkPrint_4.Size = new System.Drawing.Size(77, 22);
			this.chkPrint_4.TabIndex = 4;
			this.chkPrint_4.Text = "All Files";
			this.chkPrint_4.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
			// 
			// chkPrint_3
			// 
			this.chkPrint_3.Location = new System.Drawing.Point(20, 174);
			this.chkPrint_3.Name = "chkPrint_3";
			this.chkPrint_3.Size = new System.Drawing.Size(172, 22);
			this.chkPrint_3.TabIndex = 3;
			this.chkPrint_3.Text = "Motor Vehicle Close Out";
			this.chkPrint_3.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
			// 
			// chkPrint_2
			// 
			this.chkPrint_2.Location = new System.Drawing.Point(20, 126);
			this.chkPrint_2.Name = "chkPrint_2";
			this.chkPrint_2.Size = new System.Drawing.Size(101, 22);
			this.chkPrint_2.TabIndex = 2;
			this.chkPrint_2.Text = "Date Range";
			this.chkPrint_2.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
			// 
			// chkPrint_1
			// 
			this.chkPrint_1.Location = new System.Drawing.Point(20, 78);
			this.chkPrint_1.Name = "chkPrint_1";
			this.chkPrint_1.Size = new System.Drawing.Size(117, 22);
			this.chkPrint_1.TabIndex = 1;
			this.chkPrint_1.Text = "Specific Month";
			this.chkPrint_1.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
			// 
			// chkPrint_0
			// 
			this.chkPrint_0.Location = new System.Drawing.Point(20, 30);
			this.chkPrint_0.Name = "chkPrint_0";
			this.chkPrint_0.Size = new System.Drawing.Size(108, 22);
			this.chkPrint_0.TabIndex = 5;
			this.chkPrint_0.Text = "Specific Year";
			this.chkPrint_0.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
			// 
			// fraChoice
			// 
			this.fraChoice.Controls.Add(this.fraRange);
			this.fraChoice.Controls.Add(this.fraDate);
			this.fraChoice.Location = new System.Drawing.Point(30, 30);
			this.fraChoice.Name = "fraChoice";
			this.fraChoice.Size = new System.Drawing.Size(305, 150);
			this.fraChoice.TabIndex = 2;
			this.fraChoice.Text = "Enter Range";
			this.fraChoice.Visible = false;
			// 
			// fraRange
			// 
			this.fraRange.AppearanceKey = "groupBoxNoBorders";
			this.fraRange.Controls.Add(this.txtStart);
			this.fraRange.Controls.Add(this.txtEnd);
			this.fraRange.Controls.Add(this.Label2);
			this.fraRange.Controls.Add(this.Label3);
			this.fraRange.Location = new System.Drawing.Point(1, 1);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(250, 149);
			this.fraRange.TabIndex = 1;
			this.fraRange.Visible = false;
			// 
			// txtStart
			// 
			this.txtStart.Location = new System.Drawing.Point(115, 29);
			this.txtStart.Mask = "##/##/####";
			this.txtStart.MaxLength = 10;
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(115, 22);
			this.txtStart.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtStart, "Enter the range and press F12.");
			// 
			// txtEnd
			// 
			this.txtEnd.Location = new System.Drawing.Point(115, 89);
			this.txtEnd.Mask = "##/##/####";
			this.txtEnd.MaxLength = 10;
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(115, 22);
			this.txtEnd.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtEnd, "Enter the range and press F12.");
			// 
			// Label2
			// 
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label2.Location = new System.Drawing.Point(20, 43);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(60, 16);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "START AT";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 103);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(50, 16);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "END AT";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraDate
			// 
			this.fraDate.AppearanceKey = "groupBoxNoBorders";
			this.fraDate.Controls.Add(this.cmbMonth);
			this.fraDate.Controls.Add(this.cmbYear);
			this.fraDate.Controls.Add(this.Label5);
			this.fraDate.Controls.Add(this.Label4);
			this.fraDate.Location = new System.Drawing.Point(1, 1);
			this.fraDate.Movable = true;
			this.fraDate.Name = "fraDate";
			this.fraDate.Size = new System.Drawing.Size(300, 149);
			this.fraDate.TabIndex = 2;
			this.fraDate.Visible = false;
			// 
			// cmbMonth
			// 
			this.cmbMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMonth.Enabled = false;
			this.cmbMonth.Location = new System.Drawing.Point(105, 29);
			this.cmbMonth.Name = "cmbMonth";
			this.cmbMonth.Size = new System.Drawing.Size(160, 40);
			this.cmbMonth.Sorted = true;
			this.cmbMonth.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.cmbMonth, "Select the correct month.");
			this.cmbMonth.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMonth_KeyDown);
			// 
			// cmbYear
			// 
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.Enabled = false;
			this.cmbYear.Location = new System.Drawing.Point(105, 89);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(80, 40);
			this.cmbYear.Sorted = true;
			this.cmbYear.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cmbYear, "Select the correct year.");
			this.cmbYear.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbYear_KeyDown);
			// 
			// Label5
			// 
			this.Label5.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label5.Location = new System.Drawing.Point(20, 103);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(35, 16);
			this.Label5.TabIndex = 2;
			this.Label5.Text = "YEAR";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label4
			// 
			this.Label4.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label4.Location = new System.Drawing.Point(20, 43);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(50, 16);
			this.Label4.TabIndex = 4;
			this.Label4.Text = "MONTH";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(290, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(154, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save & Continue";
			this.cmdSave.Click += new System.EventHandler(this.mnuFileNext_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileBack,
            this.mnuFileNext,
            this.mnuFileSeperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileBack
			// 
			this.mnuFileBack.Index = 0;
			this.mnuFileBack.Name = "mnuFileBack";
			this.mnuFileBack.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuFileBack.Text = "Previous Screen";
			this.mnuFileBack.Visible = false;
			this.mnuFileBack.Click += new System.EventHandler(this.mnuFileBack_Click);
			// 
			// mnuFileNext
			// 
			this.mnuFileNext.Index = 1;
			this.mnuFileNext.Name = "mnuFileNext";
			this.mnuFileNext.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileNext.Text = "Save & Continue";
			this.mnuFileNext.Click += new System.EventHandler(this.mnuFileNext_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 2;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPreview
			// 
			this.cmdPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPreview.Location = new System.Drawing.Point(626, 27);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Size = new System.Drawing.Size(65, 24);
			this.cmdPreview.TabIndex = 3;
			this.cmdPreview.Text = "Preview";
			this.cmdPreview.Visible = false;
			this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// cmdBack
			// 
			this.cmdBack.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdBack.Location = new System.Drawing.Point(508, 27);
			this.cmdBack.Name = "cmdBack";
			this.cmdBack.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdBack.Size = new System.Drawing.Size(112, 24);
			this.cmdBack.TabIndex = 4;
			this.cmdBack.Text = "Previous Screen";
			this.cmdBack.Visible = false;
			this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
			// 
			// frmMVR3Listing
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(735, 630);
			this.KeyPreview = true;
			this.Name = "frmMVR3Listing";
			this.Text = "MVR3 Listing";
			this.Load += new System.EventHandler(this.frmMVR3Listing_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMVR3Listing_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMVCO)).EndInit();
			this.fraMVCO.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraOptions)).EndInit();
			this.fraOptions.ResumeLayout(false);
			this.fraOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChoice)).EndInit();
			this.fraChoice.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			this.fraRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).EndInit();
			this.fraDate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBack)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdPreview;
        public FCButton cmdBack;
    }
}
