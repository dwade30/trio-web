﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for arDailyAuditReport.
	/// </summary>
	partial class arDailyAuditReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arDailyAuditReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldConvenienceFeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.sarTypesOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.fldTotals2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotals3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotals4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotals5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotals6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotals7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotals1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotalsTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalReceived = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalReceivedToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.sarCRSummaryOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lblTotalNonCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalNonCashToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblHead1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHead2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalReceivedAll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTotalReversals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalReversalsToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalReversalsAll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalNonCashAll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblNetActivity = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldNetActivityToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldNetActivityAll = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblLess1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLess2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lnSubtotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.sarCheckReportOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.sarTellerReportOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lblTotalCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalCredit = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalOther = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCredit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
            this.lblTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTotalPrevious = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalPrevious = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.sarCCReportOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lblConvenienceFees = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldConvenienceFees = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTellerID = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRecreateOrigDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalsTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReceivedToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalNonCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalNonCashToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHead1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHead2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReceivedAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalReversals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReversalsToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReversalsAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalNonCashAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNetActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetActivityToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetActivityAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLess1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLess2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalPrevious)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrevious)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConvenienceFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTellerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecreateOrigDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldConvenienceFeeTotal,
            this.sarTypesOb,
            this.fldTotals2,
            this.fldTotals3,
            this.fldTotals4,
            this.fldTotals5,
            this.fldTotals6,
            this.fldTotals7,
            this.fldTotals1,
            this.lblTotalsTotal,
            this.lblTotalReceived,
            this.fldTotalReceivedToday,
            this.sarCRSummaryOB,
            this.lblTotalNonCash,
            this.fldTotalNonCashToday,
            this.lblHead1,
            this.lblHead2,
            this.fldTotalReceivedAll,
            this.Line1,
            this.lblTotalReversals,
            this.fldTotalReversalsToday,
            this.fldTotalReversalsAll,
            this.fldTotalNonCashAll,
            this.lblNetActivity,
            this.fldNetActivityToday,
            this.fldNetActivityAll,
            this.lblLess1,
            this.lblLess2,
            this.Line2,
            this.lnSubtotal,
            this.sarCheckReportOb,
            this.sarTellerReportOb,
            this.lblTotalCash,
            this.lblTotalCheck,
            this.lblTotalCredit,
            this.lblTotalOther,
            this.fldTotalCash,
            this.fldTotalCheck,
            this.fldTotalCredit,
            this.fldTotalOther,
            this.PageBreak1,
            this.lblTotalTotal,
            this.fldTotalTotal,
            this.Line3,
            this.lblTotalPrevious,
            this.fldTotalPrevious,
            this.sarCCReportOB,
            this.lblConvenienceFees,
            this.fldConvenienceFees});
            this.Detail.Height = 3.625F;
            this.Detail.Name = "Detail";
            // 
            // fldConvenienceFeeTotal
            // 
            this.fldConvenienceFeeTotal.Height = 0.1875F;
            this.fldConvenienceFeeTotal.Left = 2.6F;
            this.fldConvenienceFeeTotal.Name = "fldConvenienceFeeTotal";
            this.fldConvenienceFeeTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldConvenienceFeeTotal.Text = "0.00";
            this.fldConvenienceFeeTotal.Top = 0.4479167F;
            this.fldConvenienceFeeTotal.Visible = false;
            this.fldConvenienceFeeTotal.Width = 1F;
            // 
            // sarTypesOb
            // 
            this.sarTypesOb.CloseBorder = false;
            this.sarTypesOb.Height = 0.15F;
            this.sarTypesOb.Left = 0F;
            this.sarTypesOb.Name = "sarTypesOb";
            this.sarTypesOb.Report = null;
            this.sarTypesOb.Top = 0F;
            this.sarTypesOb.Width = 7.5F;
            // 
            // fldTotals2
            // 
            this.fldTotals2.Height = 0.2F;
            this.fldTotals2.Left = 3.7F;
            this.fldTotals2.Name = "fldTotals2";
            this.fldTotals2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldTotals2.Text = "0.00";
            this.fldTotals2.Top = 0.45F;
            this.fldTotals2.Visible = false;
            this.fldTotals2.Width = 1F;
            // 
            // fldTotals3
            // 
            this.fldTotals3.Height = 0.2F;
            this.fldTotals3.Left = 4.25F;
            this.fldTotals3.Name = "fldTotals3";
            this.fldTotals3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldTotals3.Text = "0.00";
            this.fldTotals3.Top = 0.25F;
            this.fldTotals3.Visible = false;
            this.fldTotals3.Width = 1F;
            // 
            // fldTotals4
            // 
            this.fldTotals4.Height = 0.2F;
            this.fldTotals4.Left = 4.8F;
            this.fldTotals4.Name = "fldTotals4";
            this.fldTotals4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldTotals4.Text = "0.00";
            this.fldTotals4.Top = 0.45F;
            this.fldTotals4.Visible = false;
            this.fldTotals4.Width = 1F;
            // 
            // fldTotals5
            // 
            this.fldTotals5.Height = 0.2F;
            this.fldTotals5.Left = 5.4F;
            this.fldTotals5.Name = "fldTotals5";
            this.fldTotals5.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldTotals5.Text = "0.00";
            this.fldTotals5.Top = 0.25F;
            this.fldTotals5.Visible = false;
            this.fldTotals5.Width = 1F;
            // 
            // fldTotals6
            // 
            this.fldTotals6.Height = 0.2F;
            this.fldTotals6.Left = 5.95F;
            this.fldTotals6.Name = "fldTotals6";
            this.fldTotals6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldTotals6.Text = "0.00";
            this.fldTotals6.Top = 0.45F;
            this.fldTotals6.Visible = false;
            this.fldTotals6.Width = 1F;
            // 
            // fldTotals7
            // 
            this.fldTotals7.Height = 0.2F;
            this.fldTotals7.Left = 6.5F;
            this.fldTotals7.Name = "fldTotals7";
            this.fldTotals7.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldTotals7.Text = "0.00";
            this.fldTotals7.Top = 0.25F;
            this.fldTotals7.Width = 1F;
            // 
            // fldTotals1
            // 
            this.fldTotals1.Height = 0.2F;
            this.fldTotals1.Left = 3F;
            this.fldTotals1.Name = "fldTotals1";
            this.fldTotals1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.fldTotals1.Text = "0.00";
            this.fldTotals1.Top = 0.25F;
            this.fldTotals1.Visible = false;
            this.fldTotals1.Width = 1F;
            // 
            // lblTotalsTotal
            // 
            this.lblTotalsTotal.Height = 0.1875F;
            this.lblTotalsTotal.HyperLink = null;
            this.lblTotalsTotal.Left = 1F;
            this.lblTotalsTotal.MultiLine = false;
            this.lblTotalsTotal.Name = "lblTotalsTotal";
            this.lblTotalsTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalsTotal.Text = "Net Received:";
            this.lblTotalsTotal.Top = 0.25F;
            this.lblTotalsTotal.Width = 2F;
            // 
            // lblTotalReceived
            // 
            this.lblTotalReceived.Height = 0.1875F;
            this.lblTotalReceived.HyperLink = null;
            this.lblTotalReceived.Left = 1.6875F;
            this.lblTotalReceived.MultiLine = false;
            this.lblTotalReceived.Name = "lblTotalReceived";
            this.lblTotalReceived.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalReceived.Text = "Total Activity:";
            this.lblTotalReceived.Top = 0.9375F;
            this.lblTotalReceived.Width = 1.3125F;
            // 
            // fldTotalReceivedToday
            // 
            this.fldTotalReceivedToday.Height = 0.1875F;
            this.fldTotalReceivedToday.Left = 3F;
            this.fldTotalReceivedToday.Name = "fldTotalReceivedToday";
            this.fldTotalReceivedToday.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalReceivedToday.Text = "0.00";
            this.fldTotalReceivedToday.Top = 0.9375F;
            this.fldTotalReceivedToday.Width = 0.875F;
            // 
            // sarCRSummaryOB
            // 
            this.sarCRSummaryOB.CloseBorder = false;
            this.sarCRSummaryOB.Height = 0.0625F;
            this.sarCRSummaryOB.Left = 0F;
            this.sarCRSummaryOB.Name = "sarCRSummaryOB";
            this.sarCRSummaryOB.Report = null;
            this.sarCRSummaryOB.Top = 3.1875F;
            this.sarCRSummaryOB.Width = 7.5F;
            // 
            // lblTotalNonCash
            // 
            this.lblTotalNonCash.Height = 0.1875F;
            this.lblTotalNonCash.HyperLink = null;
            this.lblTotalNonCash.Left = 1.6875F;
            this.lblTotalNonCash.MultiLine = false;
            this.lblTotalNonCash.Name = "lblTotalNonCash";
            this.lblTotalNonCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalNonCash.Text = "Total Non-Cash:";
            this.lblTotalNonCash.Top = 1.3125F;
            this.lblTotalNonCash.Width = 1.3125F;
            // 
            // fldTotalNonCashToday
            // 
            this.fldTotalNonCashToday.Height = 0.1875F;
            this.fldTotalNonCashToday.Left = 3F;
            this.fldTotalNonCashToday.Name = "fldTotalNonCashToday";
            this.fldTotalNonCashToday.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalNonCashToday.Text = "0.00";
            this.fldTotalNonCashToday.Top = 1.3125F;
            this.fldTotalNonCashToday.Width = 0.875F;
            // 
            // lblHead1
            // 
            this.lblHead1.Height = 0.1875F;
            this.lblHead1.HyperLink = null;
            this.lblHead1.Left = 3.375F;
            this.lblHead1.Name = "lblHead1";
            this.lblHead1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblHead1.Text = "Today";
            this.lblHead1.Top = 0.75F;
            this.lblHead1.Width = 0.5F;
            // 
            // lblHead2
            // 
            this.lblHead2.Height = 0.1875F;
            this.lblHead2.HyperLink = null;
            this.lblHead2.Left = 4.25F;
            this.lblHead2.Name = "lblHead2";
            this.lblHead2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblHead2.Text = "Total";
            this.lblHead2.Top = 0.75F;
            this.lblHead2.Width = 0.5625F;
            // 
            // fldTotalReceivedAll
            // 
            this.fldTotalReceivedAll.Height = 0.1875F;
            this.fldTotalReceivedAll.Left = 3.875F;
            this.fldTotalReceivedAll.Name = "fldTotalReceivedAll";
            this.fldTotalReceivedAll.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalReceivedAll.Text = "0.00";
            this.fldTotalReceivedAll.Top = 0.9375F;
            this.fldTotalReceivedAll.Width = 0.9375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 2.9375F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.9375F;
            this.Line1.Width = 1.875F;
            this.Line1.X1 = 2.9375F;
            this.Line1.X2 = 4.8125F;
            this.Line1.Y1 = 0.9375F;
            this.Line1.Y2 = 0.9375F;
            // 
            // lblTotalReversals
            // 
            this.lblTotalReversals.Height = 0.1875F;
            this.lblTotalReversals.HyperLink = null;
            this.lblTotalReversals.Left = 1.6875F;
            this.lblTotalReversals.MultiLine = false;
            this.lblTotalReversals.Name = "lblTotalReversals";
            this.lblTotalReversals.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalReversals.Text = "Total Reversals:";
            this.lblTotalReversals.Top = 1.125F;
            this.lblTotalReversals.Width = 1.3125F;
            // 
            // fldTotalReversalsToday
            // 
            this.fldTotalReversalsToday.Height = 0.1875F;
            this.fldTotalReversalsToday.Left = 3F;
            this.fldTotalReversalsToday.Name = "fldTotalReversalsToday";
            this.fldTotalReversalsToday.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalReversalsToday.Text = "0.00";
            this.fldTotalReversalsToday.Top = 1.125F;
            this.fldTotalReversalsToday.Width = 0.875F;
            // 
            // fldTotalReversalsAll
            // 
            this.fldTotalReversalsAll.Height = 0.1875F;
            this.fldTotalReversalsAll.Left = 3.875F;
            this.fldTotalReversalsAll.Name = "fldTotalReversalsAll";
            this.fldTotalReversalsAll.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalReversalsAll.Text = "0.00";
            this.fldTotalReversalsAll.Top = 1.125F;
            this.fldTotalReversalsAll.Width = 0.9375F;
            // 
            // fldTotalNonCashAll
            // 
            this.fldTotalNonCashAll.Height = 0.1875F;
            this.fldTotalNonCashAll.Left = 3.875F;
            this.fldTotalNonCashAll.Name = "fldTotalNonCashAll";
            this.fldTotalNonCashAll.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalNonCashAll.Text = "0.00";
            this.fldTotalNonCashAll.Top = 1.3125F;
            this.fldTotalNonCashAll.Width = 0.9375F;
            // 
            // lblNetActivity
            // 
            this.lblNetActivity.Height = 0.1875F;
            this.lblNetActivity.HyperLink = null;
            this.lblNetActivity.Left = 1.6875F;
            this.lblNetActivity.MultiLine = false;
            this.lblNetActivity.Name = "lblNetActivity";
            this.lblNetActivity.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblNetActivity.Text = "Net Received:";
            this.lblNetActivity.Top = 1.5F;
            this.lblNetActivity.Width = 1.3125F;
            // 
            // fldNetActivityToday
            // 
            this.fldNetActivityToday.Height = 0.1875F;
            this.fldNetActivityToday.Left = 3F;
            this.fldNetActivityToday.Name = "fldNetActivityToday";
            this.fldNetActivityToday.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldNetActivityToday.Text = "0.00";
            this.fldNetActivityToday.Top = 1.5F;
            this.fldNetActivityToday.Width = 0.875F;
            // 
            // fldNetActivityAll
            // 
            this.fldNetActivityAll.Height = 0.1875F;
            this.fldNetActivityAll.Left = 3.875F;
            this.fldNetActivityAll.Name = "fldNetActivityAll";
            this.fldNetActivityAll.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldNetActivityAll.Text = "0.00";
            this.fldNetActivityAll.Top = 1.5F;
            this.fldNetActivityAll.Width = 0.9375F;
            // 
            // lblLess1
            // 
            this.lblLess1.Height = 0.1875F;
            this.lblLess1.HyperLink = null;
            this.lblLess1.Left = 1.25F;
            this.lblLess1.Name = "lblLess1";
            this.lblLess1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblLess1.Text = "Less:";
            this.lblLess1.Top = 1.125F;
            this.lblLess1.Width = 0.4375F;
            // 
            // lblLess2
            // 
            this.lblLess2.Height = 0.1875F;
            this.lblLess2.HyperLink = null;
            this.lblLess2.Left = 1.25F;
            this.lblLess2.Name = "lblLess2";
            this.lblLess2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblLess2.Text = "Less:";
            this.lblLess2.Top = 1.3125F;
            this.lblLess2.Width = 0.4375F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 2.9375F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 1.5F;
            this.Line2.Width = 1.875F;
            this.Line2.X1 = 2.9375F;
            this.Line2.X2 = 4.8125F;
            this.Line2.Y1 = 1.5F;
            this.Line2.Y2 = 1.5F;
            // 
            // lnSubtotal
            // 
            this.lnSubtotal.Height = 0F;
            this.lnSubtotal.Left = 2.95F;
            this.lnSubtotal.LineWeight = 3F;
            this.lnSubtotal.Name = "lnSubtotal";
            this.lnSubtotal.Top = 0.2F;
            this.lnSubtotal.Width = 4.55F;
            this.lnSubtotal.X1 = 2.95F;
            this.lnSubtotal.X2 = 7.5F;
            this.lnSubtotal.Y1 = 0.2F;
            this.lnSubtotal.Y2 = 0.2F;
            // 
            // sarCheckReportOb
            // 
            this.sarCheckReportOb.CloseBorder = false;
            this.sarCheckReportOb.Height = 0.0625F;
            this.sarCheckReportOb.Left = 0F;
            this.sarCheckReportOb.Name = "sarCheckReportOb";
            this.sarCheckReportOb.Report = null;
            this.sarCheckReportOb.Top = 3.3125F;
            this.sarCheckReportOb.Width = 7.5F;
            // 
            // sarTellerReportOb
            // 
            this.sarTellerReportOb.CloseBorder = false;
            this.sarTellerReportOb.Height = 0.0625F;
            this.sarTellerReportOb.Left = 0F;
            this.sarTellerReportOb.Name = "sarTellerReportOb";
            this.sarTellerReportOb.Report = null;
            this.sarTellerReportOb.Top = 3.5625F;
            this.sarTellerReportOb.Width = 7.5F;
            // 
            // lblTotalCash
            // 
            this.lblTotalCash.Height = 0.1875F;
            this.lblTotalCash.HyperLink = null;
            this.lblTotalCash.Left = 2.5F;
            this.lblTotalCash.MultiLine = false;
            this.lblTotalCash.Name = "lblTotalCash";
            this.lblTotalCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalCash.Text = "Cash:";
            this.lblTotalCash.Top = 1.8125F;
            this.lblTotalCash.Width = 0.9375F;
            // 
            // lblTotalCheck
            // 
            this.lblTotalCheck.Height = 0.1875F;
            this.lblTotalCheck.HyperLink = null;
            this.lblTotalCheck.Left = 2.5F;
            this.lblTotalCheck.MultiLine = false;
            this.lblTotalCheck.Name = "lblTotalCheck";
            this.lblTotalCheck.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalCheck.Text = "Check:";
            this.lblTotalCheck.Top = 2F;
            this.lblTotalCheck.Width = 0.9375F;
            // 
            // lblTotalCredit
            // 
            this.lblTotalCredit.Height = 0.1875F;
            this.lblTotalCredit.HyperLink = null;
            this.lblTotalCredit.Left = 2.5F;
            this.lblTotalCredit.MultiLine = false;
            this.lblTotalCredit.Name = "lblTotalCredit";
            this.lblTotalCredit.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalCredit.Text = "Credit/Debit:";
            this.lblTotalCredit.Top = 2.1875F;
            this.lblTotalCredit.Width = 1F;
            // 
            // lblTotalOther
            // 
            this.lblTotalOther.Height = 0.1875F;
            this.lblTotalOther.HyperLink = null;
            this.lblTotalOther.Left = 2.5F;
            this.lblTotalOther.MultiLine = false;
            this.lblTotalOther.Name = "lblTotalOther";
            this.lblTotalOther.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalOther.Text = "Other:";
            this.lblTotalOther.Top = 2.5625F;
            this.lblTotalOther.Width = 0.9375F;
            // 
            // fldTotalCash
            // 
            this.fldTotalCash.Height = 0.1875F;
            this.fldTotalCash.Left = 3.875F;
            this.fldTotalCash.Name = "fldTotalCash";
            this.fldTotalCash.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalCash.Text = "0.00";
            this.fldTotalCash.Top = 1.8125F;
            this.fldTotalCash.Width = 0.9375F;
            // 
            // fldTotalCheck
            // 
            this.fldTotalCheck.Height = 0.1875F;
            this.fldTotalCheck.Left = 3.875F;
            this.fldTotalCheck.Name = "fldTotalCheck";
            this.fldTotalCheck.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalCheck.Text = "0.00";
            this.fldTotalCheck.Top = 2F;
            this.fldTotalCheck.Width = 0.9375F;
            // 
            // fldTotalCredit
            // 
            this.fldTotalCredit.Height = 0.1875F;
            this.fldTotalCredit.Left = 3.875F;
            this.fldTotalCredit.Name = "fldTotalCredit";
            this.fldTotalCredit.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalCredit.Text = "0.00";
            this.fldTotalCredit.Top = 2.1875F;
            this.fldTotalCredit.Width = 0.9375F;
            // 
            // fldTotalOther
            // 
            this.fldTotalOther.Height = 0.1875F;
            this.fldTotalOther.Left = 3.875F;
            this.fldTotalOther.Name = "fldTotalOther";
            this.fldTotalOther.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalOther.Text = "0.00";
            this.fldTotalOther.Top = 2.5625F;
            this.fldTotalOther.Width = 0.9375F;
            // 
            // PageBreak1
            // 
            this.PageBreak1.Height = 0.05555556F;
            this.PageBreak1.Left = 0F;
            this.PageBreak1.Name = "PageBreak1";
            this.PageBreak1.Size = new System.Drawing.SizeF(6.5F, 0.05555556F);
            this.PageBreak1.Top = 3F;
            this.PageBreak1.Width = 6.5F;
            // 
            // lblTotalTotal
            // 
            this.lblTotalTotal.Height = 0.1875F;
            this.lblTotalTotal.HyperLink = null;
            this.lblTotalTotal.Left = 2.5F;
            this.lblTotalTotal.MultiLine = false;
            this.lblTotalTotal.Name = "lblTotalTotal";
            this.lblTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalTotal.Text = "Total:";
            this.lblTotalTotal.Top = 2.75F;
            this.lblTotalTotal.Width = 0.9375F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.1875F;
            this.fldTotalTotal.Left = 3.875F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalTotal.Text = "0.00";
            this.fldTotalTotal.Top = 2.75F;
            this.fldTotalTotal.Width = 0.9375F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 3.875F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 2.75F;
            this.Line3.Width = 0.9375F;
            this.Line3.X1 = 3.875F;
            this.Line3.X2 = 4.8125F;
            this.Line3.Y1 = 2.75F;
            this.Line3.Y2 = 2.75F;
            // 
            // lblTotalPrevious
            // 
            this.lblTotalPrevious.Height = 0.1875F;
            this.lblTotalPrevious.HyperLink = null;
            this.lblTotalPrevious.Left = 2.5F;
            this.lblTotalPrevious.MultiLine = false;
            this.lblTotalPrevious.Name = "lblTotalPrevious";
            this.lblTotalPrevious.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.lblTotalPrevious.Text = "Previous:";
            this.lblTotalPrevious.Top = 2.375F;
            this.lblTotalPrevious.Width = 0.9375F;
            // 
            // fldTotalPrevious
            // 
            this.fldTotalPrevious.Height = 0.1875F;
            this.fldTotalPrevious.Left = 3.875F;
            this.fldTotalPrevious.Name = "fldTotalPrevious";
            this.fldTotalPrevious.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.fldTotalPrevious.Text = "0.00";
            this.fldTotalPrevious.Top = 2.375F;
            this.fldTotalPrevious.Width = 0.9375F;
            // 
            // sarCCReportOB
            // 
            this.sarCCReportOB.CloseBorder = false;
            this.sarCCReportOB.Height = 0.0625F;
            this.sarCCReportOB.Left = 0F;
            this.sarCCReportOB.Name = "sarCCReportOB";
            this.sarCCReportOB.Report = null;
            this.sarCCReportOB.Top = 3.4375F;
            this.sarCCReportOB.Width = 7.5F;
            // 
            // lblConvenienceFees
            // 
            this.lblConvenienceFees.Height = 0.1875F;
            this.lblConvenienceFees.HyperLink = null;
            this.lblConvenienceFees.Left = 5.5F;
            this.lblConvenienceFees.MultiLine = false;
            this.lblConvenienceFees.Name = "lblConvenienceFees";
            this.lblConvenienceFees.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; te" +
    "xt-decoration: underline";
            this.lblConvenienceFees.Text = "Convenience Fees";
            this.lblConvenienceFees.Top = 1.03125F;
            this.lblConvenienceFees.Width = 1.53125F;
            // 
            // fldConvenienceFees
            // 
            this.fldConvenienceFees.Height = 0.1875F;
            this.fldConvenienceFees.Left = 5.5F;
            this.fldConvenienceFees.Name = "fldConvenienceFees";
            this.fldConvenienceFees.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.fldConvenienceFees.Text = "0.00";
            this.fldConvenienceFees.Top = 1.21875F;
            this.fldConvenienceFees.Width = 1.53125F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDate,
            this.lblMuniName,
            this.lblTime,
            this.lblTellerID,
            this.lblHeader,
            this.lblPageNumber,
            this.lblRecreateOrigDate});
            this.PageHeader.Height = 0.5416667F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.25F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.25F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.MultiLine = false;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.625F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.375F;
            // 
            // lblTellerID
            // 
            this.lblTellerID.Height = 0.3125F;
            this.lblTellerID.HyperLink = null;
            this.lblTellerID.Left = 0F;
            this.lblTellerID.Name = "lblTellerID";
            this.lblTellerID.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblTellerID.Text = null;
            this.lblTellerID.Top = 0.25F;
            this.lblTellerID.Width = 7.5F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.25F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Cash Receipting Detail Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblPageNumber
            // 
            this.lblPageNumber.Height = 0.1875F;
            this.lblPageNumber.HyperLink = null;
            this.lblPageNumber.Left = 6.25F;
            this.lblPageNumber.Name = "lblPageNumber";
            this.lblPageNumber.Style = "font-family: \'Tahoma\'; font-size: 9.75pt; text-align: right";
            this.lblPageNumber.Text = null;
            this.lblPageNumber.Top = 0.1875F;
            this.lblPageNumber.Width = 1.25F;
            // 
            // lblRecreateOrigDate
            // 
            this.lblRecreateOrigDate.Height = 0.1875F;
            this.lblRecreateOrigDate.HyperLink = null;
            this.lblRecreateOrigDate.Left = 0F;
            this.lblRecreateOrigDate.Name = "lblRecreateOrigDate";
            this.lblRecreateOrigDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.lblRecreateOrigDate.Text = null;
            this.lblRecreateOrigDate.Top = 0.25F;
            this.lblRecreateOrigDate.Visible = false;
            this.lblRecreateOrigDate.Width = 7.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // arDailyAuditReport
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
            ((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotals1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalsTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReceivedToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalNonCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalNonCashToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHead1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHead2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReceivedAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalReversals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReversalsToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalReversalsAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalNonCashAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNetActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetActivityToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetActivityAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLess1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLess2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalPrevious)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrevious)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConvenienceFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTellerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecreateOrigDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConvenienceFeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarTypesOb;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalReceivedToday;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarCRSummaryOB;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalNonCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNonCashToday;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHead1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHead2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalReceivedAll;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalReversals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalReversalsToday;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalReversalsAll;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNonCashAll;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNetActivity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetActivityToday;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetActivityAll;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLess1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLess2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSubtotal;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarCheckReportOb;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarTellerReportOb;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCash;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCredit;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalOther;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCash;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCheck;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCredit;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOther;
		private GrapeCity.ActiveReports.SectionReportModel.PageBreak PageBreak1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalTotal;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPrevious;
		public GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrevious;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarCCReportOB;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConvenienceFees;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConvenienceFees;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		public GrapeCity.ActiveReports.SectionReportModel.Label lblTellerID;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRecreateOrigDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
