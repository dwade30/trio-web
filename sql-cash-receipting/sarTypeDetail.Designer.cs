﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTypeDetail.
	/// </summary>
	partial class sarTypeDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarTypeDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTeller = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPayType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAltCashAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldConvenienceFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAltCashAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldName,
				this.fldAcct,
				this.fldBill,
				this.fldReceipt,
				this.fldDate,
				this.fldTeller,
				this.fldNC,
				this.fldPayType,
				this.fldCode,
				this.fldTitle1,
				this.fldTitle2,
				this.fldTitle3,
				this.fldTitle4,
				this.fldTitle5,
				this.fldTitle6,
				this.fldTotal,
				this.fldAccountNumber,
				this.lblAccountNumber,
				this.fldAltCashAccount,
				this.fldConvenienceFee
			});
			this.Detail.Height = 0.5625F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.430556F;
			// 
			// fldAcct
			// 
			this.fldAcct.CanGrow = false;
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0.4375F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0.1875F;
			this.fldAcct.Width = 0.4375F;
			// 
			// fldBill
			// 
			this.fldBill.CanGrow = false;
			this.fldBill.Height = 0.1875F;
			this.fldBill.Left = 0.875F;
			this.fldBill.Name = "fldBill";
			this.fldBill.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldBill.Text = null;
			this.fldBill.Top = 0.1875F;
			this.fldBill.Width = 0.3125F;
			// 
			// fldReceipt
			// 
			this.fldReceipt.CanGrow = false;
			this.fldReceipt.Height = 0.1875F;
			this.fldReceipt.Left = 1.1875F;
			this.fldReceipt.Name = "fldReceipt";
			this.fldReceipt.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldReceipt.Text = null;
			this.fldReceipt.Top = 0.1875F;
			this.fldReceipt.Width = 0.625F;
			// 
			// fldDate
			// 
			this.fldDate.CanGrow = false;
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 2.430556F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 1.0625F;
			// 
			// fldTeller
			// 
			this.fldTeller.CanGrow = false;
			this.fldTeller.Height = 0.1875F;
			this.fldTeller.Left = 1.8125F;
			this.fldTeller.Name = "fldTeller";
			this.fldTeller.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTeller.Text = null;
			this.fldTeller.Top = 0.1875F;
			this.fldTeller.Width = 0.375F;
			// 
			// fldNC
			// 
			this.fldNC.CanGrow = false;
			this.fldNC.Height = 0.1875F;
			this.fldNC.Left = 2.125F;
			this.fldNC.MultiLine = false;
			this.fldNC.Name = "fldNC";
			this.fldNC.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldNC.Text = null;
			this.fldNC.Top = 0.1875F;
			this.fldNC.Width = 0.5F;
			// 
			// fldPayType
			// 
			this.fldPayType.CanGrow = false;
			this.fldPayType.Height = 0.1875F;
			this.fldPayType.Left = 0.0625F;
			this.fldPayType.Name = "fldPayType";
			this.fldPayType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldPayType.Text = null;
			this.fldPayType.Top = 0.1875F;
			this.fldPayType.Width = 0.375F;
			// 
			// fldCode
			// 
			this.fldCode.CanGrow = false;
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 2.5625F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldCode.Text = null;
			this.fldCode.Top = 0.1875F;
			this.fldCode.Width = 0.4375F;
			// 
			// fldTitle1
			// 
			this.fldTitle1.CanGrow = false;
			this.fldTitle1.Height = 0.1875F;
			this.fldTitle1.Left = 3F;
			this.fldTitle1.Name = "fldTitle1";
			this.fldTitle1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTitle1.Text = "0.00";
			this.fldTitle1.Top = 0.1875F;
			this.fldTitle1.Width = 1F;
			// 
			// fldTitle2
			// 
			this.fldTitle2.CanGrow = false;
			this.fldTitle2.Height = 0.1875F;
			this.fldTitle2.Left = 3.6875F;
			this.fldTitle2.Name = "fldTitle2";
			this.fldTitle2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTitle2.Text = "0.00";
			this.fldTitle2.Top = 0.375F;
			this.fldTitle2.Width = 1F;
			// 
			// fldTitle3
			// 
			this.fldTitle3.CanGrow = false;
			this.fldTitle3.Height = 0.1875F;
			this.fldTitle3.Left = 4.25F;
			this.fldTitle3.Name = "fldTitle3";
			this.fldTitle3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTitle3.Text = "0.00";
			this.fldTitle3.Top = 0.1875F;
			this.fldTitle3.Width = 1F;
			// 
			// fldTitle4
			// 
			this.fldTitle4.CanGrow = false;
			this.fldTitle4.Height = 0.1875F;
			this.fldTitle4.Left = 4.8125F;
			this.fldTitle4.Name = "fldTitle4";
			this.fldTitle4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTitle4.Text = "0.00";
			this.fldTitle4.Top = 0.375F;
			this.fldTitle4.Width = 1F;
			// 
			// fldTitle5
			// 
			this.fldTitle5.CanGrow = false;
			this.fldTitle5.Height = 0.1875F;
			this.fldTitle5.Left = 5.375F;
			this.fldTitle5.Name = "fldTitle5";
			this.fldTitle5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTitle5.Text = "0.00";
			this.fldTitle5.Top = 0.1875F;
			this.fldTitle5.Width = 1F;
			// 
			// fldTitle6
			// 
			this.fldTitle6.CanGrow = false;
			this.fldTitle6.Height = 0.1875F;
			this.fldTitle6.Left = 5.9375F;
			this.fldTitle6.Name = "fldTitle6";
			this.fldTitle6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTitle6.Text = "0.00";
			this.fldTitle6.Top = 0.375F;
			this.fldTitle6.Width = 1F;
			// 
			// fldTotal
			// 
			this.fldTotal.CanGrow = false;
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.5F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 0.1875F;
			this.fldTotal.Width = 1F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.CanGrow = false;
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 4.6875F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left";
			this.fldAccountNumber.Text = null;
			this.fldAccountNumber.Top = 0F;
			this.fldAccountNumber.Visible = false;
			this.fldAccountNumber.Width = 1.3125F;
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.Height = 0.1875F;
			this.lblAccountNumber.HyperLink = null;
			this.lblAccountNumber.Left = 3.75F;
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold";
			this.lblAccountNumber.Text = "Account:";
			this.lblAccountNumber.Top = 0F;
			this.lblAccountNumber.Visible = false;
			this.lblAccountNumber.Width = 0.9375F;
			// 
			// fldAltCashAccount
			// 
			this.fldAltCashAccount.CanGrow = false;
			this.fldAltCashAccount.Height = 0.1875F;
			this.fldAltCashAccount.Left = 6F;
			this.fldAltCashAccount.Name = "fldAltCashAccount";
			this.fldAltCashAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left";
			this.fldAltCashAccount.Text = null;
			this.fldAltCashAccount.Top = 0F;
			this.fldAltCashAccount.Visible = false;
			this.fldAltCashAccount.Width = 1.5F;
			// 
			// fldConvenienceFee
			// 
			this.fldConvenienceFee.CanGrow = false;
			this.fldConvenienceFee.Height = 0.1875F;
			this.fldConvenienceFee.Left = 2.53125F;
			this.fldConvenienceFee.Name = "fldConvenienceFee";
			this.fldConvenienceFee.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldConvenienceFee.Text = "0.00";
			this.fldConvenienceFee.Top = 0.375F;
			this.fldConvenienceFee.Visible = false;
			this.fldConvenienceFee.Width = 1F;
			// 
			// sarTypeDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.4722222F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAltCashAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTeller;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAltCashAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConvenienceFee;
	}
}
