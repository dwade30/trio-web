﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmBackup.
	/// </summary>
	partial class frmBackup : BaseForm
	{
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 371);
			this.BottomPanel.Size = new System.Drawing.Size(610, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Size = new System.Drawing.Size(610, 311);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(282, 30);
			this.HeaderText.Text = "Cash Receipting Backup";
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcess
			});
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSave,
				this.mnuProcessSaveExit,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			// 
			// mnuProcessSaveExit
			// 
			this.mnuProcessSaveExit.Index = 1;
			this.mnuProcessSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSaveExit.Text = "Save & Exit";
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmBackup
			// 
			this.ClientSize = new System.Drawing.Size(610, 467);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmBackup";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Cash Receipting Backup";
			this.WindowState = Wisej.Web.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.frmBackup_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBackup_KeyPress);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
