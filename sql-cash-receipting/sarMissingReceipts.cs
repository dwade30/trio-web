﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarMissingReceipts.
	/// </summary>
	public partial class sarMissingReceipts : FCSectionReport
	{
		public static sarMissingReceipts InstancePtr
		{
			get
			{
				return (sarMissingReceipts)Sys.GetInstance(typeof(sarMissingReceipts));
			}
		}

		protected sarMissingReceipts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarMissingReceipts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/12/2005              *
		// ********************************************************
		int lngCT;
		bool boolDone;
		string strTempString;
		// comma delimited string with all of the missing receipt numbers in it
		public sarMissingReceipts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
			// Or frmDailyReceiptAudit.boolMissingReceipts
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			strTempString = frmDailyReceiptAudit.InstancePtr.strMissingReceipts;
			if (strTempString == "")
			{
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngCheck;
			lngCheck = GetNextMissingReceipt();
			if (lngCheck != -1)
			{
				// show this number
				fldRN.Text = FCConvert.ToString(lngCheck);
			}
			else
			{
				fldRN.Text = "";
				boolDone = true;
			}
		}

		private int GetNextMissingReceipt()
		{
			int GetNextMissingReceipt = 0;
			// this function will return the next missing receipt number from the comma delimited string
			if (strTempString.Length > 0)
			{
				if (Strings.InStr(1, strTempString, ",", CompareConstants.vbBinaryCompare) > 0)
				{
					GetNextMissingReceipt = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTempString, Strings.InStr(1, strTempString, ",", CompareConstants.vbBinaryCompare) - 1))));
					// this will remove the number just used
					strTempString = Strings.Right(strTempString, strTempString.Length - Strings.InStr(1, strTempString, ",", CompareConstants.vbBinaryCompare));
				}
				else
				{
					// last one
					GetNextMissingReceipt = FCConvert.ToInt32(Math.Round(Conversion.Val(strTempString)));
					strTempString = "";
				}
			}
			else
			{
				GetNextMissingReceipt = -1;
			}
			return GetNextMissingReceipt;
		}

		private void sarMissingReceipts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarMissingReceipts.Caption	= "Missing Receipts";
			//sarMissingReceipts.Icon	= "sarMissingReceipts.dsx":0000";
			//sarMissingReceipts.Left	= 0;
			//sarMissingReceipts.Top	= 0;
			//sarMissingReceipts.Width	= 11880;
			//sarMissingReceipts.Height	= 7170;
			//sarMissingReceipts.StartUpPosition	= 3;
			//sarMissingReceipts.SectionData	= "sarMissingReceipts.dsx":058A;
			//End Unmaped Properties
		}
	}
}
