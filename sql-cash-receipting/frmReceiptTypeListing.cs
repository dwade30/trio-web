﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmReceiptTypeListing.
	/// </summary>
	public partial class frmReceiptTypeListing : BaseForm
	{
		public frmReceiptTypeListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbRange.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReceiptTypeListing InstancePtr
		{
			get
			{
				return (frmReceiptTypeListing)Sys.GetInstance(typeof(frmReceiptTypeListing));
			}
		}

		protected frmReceiptTypeListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/17/2006              *
		// ********************************************************
		public int StartNumber;
		public int EndNumber;
		public bool blnSortAlpha;

		private void cmbEnd_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbEnd.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmbEnd_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbEnd.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbEnd.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbStart_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbStart.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmbStart_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbStart.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbStart.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmdPreview_Click(object sender, System.EventArgs e)
		{
			StartNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbStart.Items[cmbStart.SelectedIndex].ToString(), 3))));
			EndNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbEnd.Items[cmbEnd.SelectedIndex].ToString(), 3))));
			blnSortAlpha = FCConvert.CBool(chkSortAlpha.CheckState == Wisej.Web.CheckState.Checked);
			// set the range for the arctive report
			// If chk2Across.Value = vbUnchecked Then
			frmReportViewer.InstancePtr.Init(arTypeListing.InstancePtr);
			// .Show , MDIParent
			// Else
			// arTypeListing2.Show , MDIParent
			// End If
			//mnuProcessQuit_Click();
		}

		public void cmdPreview_Click()
		{
			cmdPreview_Click(cmdPreview, new System.EventArgs());
		}

		private void frmReceiptTypeListing_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						Close();
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						// cmdPreview_Click
						break;
					}
			}
			//end switch
		}

		private void frmReceiptTypeListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReceiptTypeListing.ScaleWidth	= 4230;
			//frmReceiptTypeListing.ScaleHeight	= 2790;
			//frmReceiptTypeListing.LinkTopic	= "Form1";
			//End Unmaped Properties
			// this will size the form to the MDIParent
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// fraOptions.Left = (Me.Width - fraOptions.Width) / 2
			// fraOptions.Top = (Me.Height - fraOptions.Height) / 3
			//fraChoice.Left = FCConvert.ToInt32((this.Width - fraChoice.Width) / 2.0);
			//fraChoice.Top = 0;
			SetRanges();
			modGlobalFunctions.SetTRIOColors(this);
			// MAL@20080123: Set default selection based on option
			if (modGlobalConstants.Statics.gblnSortTypeAlpha)
			{
				chkSortAlpha.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkSortAlpha.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void SetRanges()
		{
			// this sub will fill the range combo boxes for the user to pick the range
			// and the starting combo will be set the first code and the ending combo will be set to the last
			clsDRWrapper rsTemp = new clsDRWrapper();
			// clear the combo boxes before filling them
			cmbStart.Clear();
			cmbEnd.Clear();
			// open all types with a type name
			rsTemp.OpenRecordset("SELECT * FROM Type WHERE TypeTitle IS NOT NULL ORDER BY TypeCode");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				do
				{
					cmbStart.AddItem(modGlobal.PadToString_8(rsTemp.Get_Fields_Int32("Typecode"), 3) + " - " + rsTemp.Get_Fields_String("TypeTitle"));
					cmbEnd.AddItem(modGlobal.PadToString_8(rsTemp.Get_Fields_Int32("Typecode"), 3) + " - " + rsTemp.Get_Fields_String("TypeTitle"));
					rsTemp.MoveNext();
				}
				while (!rsTemp.EndOfFile());
				cmbStart.SelectedIndex = 0;
				cmbEnd.SelectedIndex = cmbEnd.Items.Count - 1;
			}
			else
			{
				MessageBox.Show("No types found.  Please go to the Type Setup screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		//private void frmReceiptTypeListing_Resize(object sender, System.EventArgs e)
		//{
		//    fraChoice.Left = FCConvert.ToInt32((this.Width - fraChoice.Width) / 2.0);
		//    fraChoice.Top = 0;
		//}
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			cmdPreview_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbRange.SelectedIndex == 1)
			{
				cmbStart.SelectedIndex = 0;
				cmbEnd.SelectedIndex = cmbEnd.Items.Count - 1;
				cmbStart.Enabled = true;
				cmbEnd.Enabled = true;
				cmbStart.BackColor = Color.White;
				cmbEnd.BackColor = Color.White;
			}
			else
			{
				cmbStart.Enabled = false;
				cmbEnd.Enabled = false;
				cmbStart.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				cmbEnd.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}
	}
}
