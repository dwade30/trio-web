﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;
using GrapeCity.ActiveReports;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmReprint.
	/// </summary>
	partial class frmReprint : BaseForm
	{
		public fecherFoundation.FCLabel lblArchive;
		public fecherFoundation.FCComboBox cmbArchive;
		public fecherFoundation.FCFrame fraReceiptList;
		public FCGrid vsReceiptList;
		public fecherFoundation.FCFrame fraPaymentInfo;
		public FCGrid vsPaymentInfo;
		public fecherFoundation.FCFrame fraSplit;
		public fecherFoundation.FCButton cmdSplitExit;
		public fecherFoundation.FCButton cmdSplitPrint;
		public fecherFoundation.FCComboBox cmbSplit;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCPanel fraCashOut;
		public fecherFoundation.FCTextBox txtTransactions;
		public fecherFoundation.FCTextBox txtConvenienceFee;
		public fecherFoundation.FCTextBox txtCashOutTotal;
		public fecherFoundation.FCTextBox txtCashOutCreditPaid;
		public fecherFoundation.FCTextBox txtCashOutTotalDue;
		public fecherFoundation.FCTextBox txtCashOutChange;
		public fecherFoundation.FCTextBox txtCashOutCheckPaid;
		public fecherFoundation.FCTextBox txtCashOutCashPaid;
		public FCGrid vsPayments;
		public fecherFoundation.FCLabel lblEFT;
		public fecherFoundation.FCLabel lblTransactions;
		public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCLabel lblConvenienceFee;
		public fecherFoundation.FCLabel lblCashOutTotal;
		public fecherFoundation.FCLabel lblCashOutCreditPaid;
		public fecherFoundation.FCLabel lblCashOutTotalDue;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel lblCashOutChange;
		public fecherFoundation.FCLabel lblCashOutCheckPaid;
		public fecherFoundation.FCLabel lblCashOutCashPaid;
		public fecherFoundation.FCPanel fraSummary;
		public fecherFoundation.FCTextBox txtPaidBy;
		public FCGrid vsSummary;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblSummaryTitle;
		public fecherFoundation.FCFrame fraReceiptNumber;
		public fecherFoundation.FCTextBox txtTellerID;
		public fecherFoundation.FCButton cmdPrintReceipt;
		public fecherFoundation.FCTextBox txtReceipt;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSplit;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.lblArchive = new fecherFoundation.FCLabel();
            this.cmbArchive = new fecherFoundation.FCComboBox();
            this.fraReceiptList = new fecherFoundation.FCFrame();
            this.vsReceiptList = new fecherFoundation.FCGrid();
            this.fraPaymentInfo = new fecherFoundation.FCFrame();
            this.vsPaymentInfo = new fecherFoundation.FCGrid();
            this.fraSplit = new fecherFoundation.FCFrame();
            this.cmdSplitExit = new fecherFoundation.FCButton();
            this.cmdSplitPrint = new fecherFoundation.FCButton();
            this.cmbSplit = new fecherFoundation.FCComboBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.fraCashOut = new fecherFoundation.FCPanel();
            this.txtTransactions = new fecherFoundation.FCTextBox();
            this.txtConvenienceFee = new fecherFoundation.FCTextBox();
            this.txtCashOutTotal = new fecherFoundation.FCTextBox();
            this.txtCashOutCreditPaid = new fecherFoundation.FCTextBox();
            this.txtCashOutTotalDue = new fecherFoundation.FCTextBox();
            this.txtCashOutChange = new fecherFoundation.FCTextBox();
            this.txtCashOutCheckPaid = new fecherFoundation.FCTextBox();
            this.txtCashOutCashPaid = new fecherFoundation.FCTextBox();
            this.vsPayments = new fecherFoundation.FCGrid();
            this.lblEFT = new fecherFoundation.FCLabel();
            this.lblTransactions = new fecherFoundation.FCLabel();
            this.Line2 = new fecherFoundation.FCLine();
            this.lblConvenienceFee = new fecherFoundation.FCLabel();
            this.lblCashOutTotal = new fecherFoundation.FCLabel();
            this.lblCashOutCreditPaid = new fecherFoundation.FCLabel();
            this.lblCashOutTotalDue = new fecherFoundation.FCLabel();
            this.Line1 = new fecherFoundation.FCLine();
            this.lblCashOutChange = new fecherFoundation.FCLabel();
            this.lblCashOutCheckPaid = new fecherFoundation.FCLabel();
            this.lblCashOutCashPaid = new fecherFoundation.FCLabel();
            this.fraSummary = new fecherFoundation.FCPanel();
            this.txtPaidBy = new fecherFoundation.FCTextBox();
            this.vsSummary = new fecherFoundation.FCGrid();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblSummaryTitle = new fecherFoundation.FCLabel();
            this.fraReceiptNumber = new fecherFoundation.FCFrame();
            this.txtTellerID = new fecherFoundation.FCTextBox();
            this.txtReceipt = new fecherFoundation.FCTextBox();
            this.lblLastAccount = new fecherFoundation.FCLabel();
            this.cmdPrintReceipt = new fecherFoundation.FCButton();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSplit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessPrint = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).BeginInit();
            this.fraReceiptList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsReceiptList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPaymentInfo)).BeginInit();
            this.fraPaymentInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPaymentInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSplit)).BeginInit();
            this.fraSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSplitExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSplitPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCashOut)).BeginInit();
            this.fraCashOut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).BeginInit();
            this.fraSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptNumber)).BeginInit();
            this.fraReceiptNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReceipt)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintReceipt);
            this.BottomPanel.Location = new System.Drawing.Point(0, 852);
            this.BottomPanel.Size = new System.Drawing.Size(979, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraCashOut);
            this.ClientArea.Controls.Add(this.fraPaymentInfo);
            this.ClientArea.Controls.Add(this.fraReceiptList);
            this.ClientArea.Controls.Add(this.fraSplit);
            this.ClientArea.Controls.Add(this.fraReceiptNumber);
            this.ClientArea.Controls.Add(this.fraSummary);
            this.ClientArea.Size = new System.Drawing.Size(999, 635);
            this.ClientArea.Controls.SetChildIndex(this.fraSummary, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraReceiptNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraSplit, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraReceiptList, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPaymentInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraCashOut, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(999, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(182, 30);
            this.HeaderText.Text = "Reprint Receipt";
            // 
            // lblArchive
            // 
            this.lblArchive.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblArchive.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblArchive.Location = new System.Drawing.Point(20, 104);
            this.lblArchive.Name = "lblArchive";
            this.lblArchive.Size = new System.Drawing.Size(60, 16);
            this.lblArchive.TabIndex = 5;
            this.lblArchive.Text = "ARCHIVE";
            // 
            // cmbArchive
            // 
            this.cmbArchive.Appearance = fecherFoundation.FCComboBox.AppearanceConstants.dbl3D;
            this.cmbArchive.Items.AddRange(new object[] {
            "Current Year",
            "Archive Year"});
            this.cmbArchive.Location = new System.Drawing.Point(115, 90);
            this.cmbArchive.Name = "cmbArchive";
            this.cmbArchive.Size = new System.Drawing.Size(164, 40);
            this.cmbArchive.TabIndex = 6;
            // 
            // fraReceiptList
            // 
            this.fraReceiptList.Controls.Add(this.vsReceiptList);
            this.fraReceiptList.Location = new System.Drawing.Point(800, 518);
            this.fraReceiptList.Name = "fraReceiptList";
            this.fraReceiptList.Size = new System.Drawing.Size(747, 334);
            this.fraReceiptList.TabIndex = 5;
            this.fraReceiptList.Text = "Select A Receipt";
            this.fraReceiptList.Visible = false;
            // 
            // vsReceiptList
            // 
            this.vsReceiptList.Cols = 4;
            this.vsReceiptList.Location = new System.Drawing.Point(20, 30);
            this.vsReceiptList.Name = "vsReceiptList";
            this.vsReceiptList.Rows = 1;
            this.vsReceiptList.Size = new System.Drawing.Size(717, 294);
            this.vsReceiptList.DoubleClick += new System.EventHandler(this.vsReceiptList_DblClick);
            // 
            // fraPaymentInfo
            // 
            this.fraPaymentInfo.AppearanceKey = "groupBoxNoBorders";
            this.fraPaymentInfo.Controls.Add(this.vsPaymentInfo);
            this.fraPaymentInfo.Location = new System.Drawing.Point(0, 524);
            this.fraPaymentInfo.Name = "fraPaymentInfo";
            this.fraPaymentInfo.Size = new System.Drawing.Size(590, 322);
            this.fraPaymentInfo.TabIndex = 2;
            this.fraPaymentInfo.Visible = false;
            // 
            // vsPaymentInfo
            // 
            this.vsPaymentInfo.Cols = 4;
            this.vsPaymentInfo.ColumnHeadersVisible = false;
            this.vsPaymentInfo.FixedCols = 0;
            this.vsPaymentInfo.FixedRows = 0;
            this.vsPaymentInfo.Location = new System.Drawing.Point(30, 30);
            this.vsPaymentInfo.Name = "vsPaymentInfo";
            this.vsPaymentInfo.RowHeadersVisible = false;
            this.vsPaymentInfo.Rows = 1;
            this.vsPaymentInfo.Size = new System.Drawing.Size(539, 276);
            // 
            // fraSplit
            // 
            this.fraSplit.Controls.Add(this.cmdSplitExit);
            this.fraSplit.Controls.Add(this.cmdSplitPrint);
            this.fraSplit.Controls.Add(this.cmbSplit);
            this.fraSplit.Controls.Add(this.Label3);
            this.fraSplit.Location = new System.Drawing.Point(1120, 296);
            this.fraSplit.Name = "fraSplit";
            this.fraSplit.Size = new System.Drawing.Size(365, 134);
            this.fraSplit.TabIndex = 4;
            this.fraSplit.Text = "Choose Split";
            this.fraSplit.Visible = false;
            // 
            // cmdSplitExit
            // 
            this.cmdSplitExit.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdSplitExit.Location = new System.Drawing.Point(439, 65);
            this.cmdSplitExit.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdSplitExit.Name = "cmdSplitExit";
            this.cmdSplitExit.Size = new System.Drawing.Size(92, 28);
            this.cmdSplitExit.TabIndex = 29;
            this.cmdSplitExit.Text = "Exit";
            this.cmdSplitExit.Visible = false;
            this.cmdSplitExit.Click += new System.EventHandler(this.cmdSplitExit_Click);
            // 
            // cmdSplitPrint
            // 
            this.cmdSplitPrint.AppearanceKey = "actionButton";
            this.cmdSplitPrint.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdSplitPrint.Location = new System.Drawing.Point(20, 66);
            this.cmdSplitPrint.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdSplitPrint.Name = "cmdSplitPrint";
            this.cmdSplitPrint.Size = new System.Drawing.Size(100, 40);
            this.cmdSplitPrint.TabIndex = 1;
            this.cmdSplitPrint.Text = "Print";
            this.cmdSplitPrint.Visible = false;
            this.cmdSplitPrint.Click += new System.EventHandler(this.cmdSplitPrint_Click);
            // 
            // cmbSplit
            // 
            this.cmbSplit.Appearance = fecherFoundation.FCComboBox.AppearanceConstants.dbl3D;
            this.cmbSplit.BackColor = System.Drawing.SystemColors.Window;
            this.cmbSplit.Location = new System.Drawing.Point(140, 66);
            this.cmbSplit.Name = "cmbSplit";
            this.cmbSplit.Size = new System.Drawing.Size(178, 40);
            this.cmbSplit.TabIndex = 2;
            this.cmbSplit.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbSplit_KeyDown);
            // 
            // Label3
            // 
            this.Label3.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.Label3.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.Label3.Location = new System.Drawing.Point(20, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(310, 16);
            this.Label3.TabIndex = 30;
            this.Label3.Text = "CHOOSE THE SPLIT THAT YOU WOULD LIKE TO PRINT";
            // 
            // fraCashOut
            // 
            this.fraCashOut.AppearanceKey = "groupBoxNoBorders";
            this.fraCashOut.Controls.Add(this.txtTransactions);
            this.fraCashOut.Controls.Add(this.txtConvenienceFee);
            this.fraCashOut.Controls.Add(this.txtCashOutTotal);
            this.fraCashOut.Controls.Add(this.txtCashOutCreditPaid);
            this.fraCashOut.Controls.Add(this.txtCashOutTotalDue);
            this.fraCashOut.Controls.Add(this.txtCashOutChange);
            this.fraCashOut.Controls.Add(this.txtCashOutCheckPaid);
            this.fraCashOut.Controls.Add(this.txtCashOutCashPaid);
            this.fraCashOut.Controls.Add(this.vsPayments);
            this.fraCashOut.Controls.Add(this.lblEFT);
            this.fraCashOut.Controls.Add(this.lblTransactions);
            this.fraCashOut.Controls.Add(this.Line2);
            this.fraCashOut.Controls.Add(this.lblConvenienceFee);
            this.fraCashOut.Controls.Add(this.lblCashOutTotal);
            this.fraCashOut.Controls.Add(this.lblCashOutCreditPaid);
            this.fraCashOut.Controls.Add(this.lblCashOutTotalDue);
            this.fraCashOut.Controls.Add(this.Line1);
            this.fraCashOut.Controls.Add(this.lblCashOutChange);
            this.fraCashOut.Controls.Add(this.lblCashOutCheckPaid);
            this.fraCashOut.Controls.Add(this.lblCashOutCashPaid);
            this.fraCashOut.Location = new System.Drawing.Point(0, 278);
            this.fraCashOut.Name = "fraCashOut";
            this.fraCashOut.Size = new System.Drawing.Size(768, 509);
            this.fraCashOut.TabIndex = 1;
            this.fraCashOut.Text = "Cash Out";
            this.fraCashOut.Visible = false;
            // 
            // txtTransactions
            // 
            this.txtTransactions.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactions.Location = new System.Drawing.Point(191, 0);
            this.txtTransactions.LockedOriginal = true;
            this.txtTransactions.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtTransactions.Name = "txtTransactions";
            this.txtTransactions.ReadOnly = true;
            this.txtTransactions.Size = new System.Drawing.Size(104, 40);
            this.txtTransactions.TabIndex = 1;
            this.txtTransactions.TabStop = false;
            this.txtTransactions.Text = "0.00";
            this.txtTransactions.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtConvenienceFee
            // 
            this.txtConvenienceFee.BackColor = System.Drawing.SystemColors.Window;
            this.txtConvenienceFee.Location = new System.Drawing.Point(191, 60);
            this.txtConvenienceFee.LockedOriginal = true;
            this.txtConvenienceFee.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtConvenienceFee.Name = "txtConvenienceFee";
            this.txtConvenienceFee.ReadOnly = true;
            this.txtConvenienceFee.Size = new System.Drawing.Size(104, 40);
            this.txtConvenienceFee.TabIndex = 3;
            this.txtConvenienceFee.TabStop = false;
            this.txtConvenienceFee.Text = "0.00";
            this.txtConvenienceFee.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCashOutTotal
            // 
            this.txtCashOutTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashOutTotal.Location = new System.Drawing.Point(191, 120);
            this.txtCashOutTotal.LockedOriginal = true;
            this.txtCashOutTotal.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCashOutTotal.Name = "txtCashOutTotal";
            this.txtCashOutTotal.ReadOnly = true;
            this.txtCashOutTotal.Size = new System.Drawing.Size(104, 40);
            this.txtCashOutTotal.TabIndex = 5;
            this.txtCashOutTotal.TabStop = false;
            this.txtCashOutTotal.Text = "0.00";
            this.txtCashOutTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCashOutCreditPaid
            // 
            this.txtCashOutCreditPaid.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashOutCreditPaid.Location = new System.Drawing.Point(191, 300);
            this.txtCashOutCreditPaid.LockedOriginal = true;
            this.txtCashOutCreditPaid.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCashOutCreditPaid.Name = "txtCashOutCreditPaid";
            this.txtCashOutCreditPaid.ReadOnly = true;
            this.txtCashOutCreditPaid.Size = new System.Drawing.Size(104, 40);
            this.txtCashOutCreditPaid.TabIndex = 11;
            this.txtCashOutCreditPaid.TabStop = false;
            this.txtCashOutCreditPaid.Text = "0.00";
            this.txtCashOutCreditPaid.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCashOutTotalDue
            // 
            this.txtCashOutTotalDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashOutTotalDue.Location = new System.Drawing.Point(191, 360);
            this.txtCashOutTotalDue.LockedOriginal = true;
            this.txtCashOutTotalDue.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCashOutTotalDue.Name = "txtCashOutTotalDue";
            this.txtCashOutTotalDue.ReadOnly = true;
            this.txtCashOutTotalDue.Size = new System.Drawing.Size(104, 40);
            this.txtCashOutTotalDue.TabIndex = 13;
            this.txtCashOutTotalDue.TabStop = false;
            this.txtCashOutTotalDue.Text = "0.00";
            this.txtCashOutTotalDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCashOutChange
            // 
            this.txtCashOutChange.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashOutChange.Location = new System.Drawing.Point(191, 420);
            this.txtCashOutChange.LockedOriginal = true;
            this.txtCashOutChange.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCashOutChange.Name = "txtCashOutChange";
            this.txtCashOutChange.ReadOnly = true;
            this.txtCashOutChange.Size = new System.Drawing.Size(104, 40);
            this.txtCashOutChange.TabIndex = 15;
            this.txtCashOutChange.TabStop = false;
            this.txtCashOutChange.Text = "0.00";
            this.txtCashOutChange.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCashOutChange.Visible = false;
            // 
            // txtCashOutCheckPaid
            // 
            this.txtCashOutCheckPaid.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashOutCheckPaid.Location = new System.Drawing.Point(191, 240);
            this.txtCashOutCheckPaid.LockedOriginal = true;
            this.txtCashOutCheckPaid.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCashOutCheckPaid.Name = "txtCashOutCheckPaid";
            this.txtCashOutCheckPaid.ReadOnly = true;
            this.txtCashOutCheckPaid.Size = new System.Drawing.Size(104, 40);
            this.txtCashOutCheckPaid.TabIndex = 9;
            this.txtCashOutCheckPaid.TabStop = false;
            this.txtCashOutCheckPaid.Text = "0.00";
            this.txtCashOutCheckPaid.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCashOutCashPaid
            // 
            this.txtCashOutCashPaid.BackColor = System.Drawing.SystemColors.Window;
            this.txtCashOutCashPaid.Location = new System.Drawing.Point(191, 180);
            this.txtCashOutCashPaid.LockedOriginal = true;
            this.txtCashOutCashPaid.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCashOutCashPaid.Name = "txtCashOutCashPaid";
            this.txtCashOutCashPaid.ReadOnly = true;
            this.txtCashOutCashPaid.Size = new System.Drawing.Size(104, 40);
            this.txtCashOutCashPaid.TabIndex = 7;
            this.txtCashOutCashPaid.TabStop = false;
            this.txtCashOutCashPaid.Text = "0.00";
            this.txtCashOutCashPaid.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // vsPayments
            // 
            this.vsPayments.Cols = 4;
            this.vsPayments.FixedCols = 0;
            this.vsPayments.Location = new System.Drawing.Point(315, 0);
            this.vsPayments.Name = "vsPayments";
            this.vsPayments.RowHeadersVisible = false;
            this.vsPayments.Rows = 1;
            this.vsPayments.Size = new System.Drawing.Size(435, 224);
            this.vsPayments.TabIndex = 16;
            this.vsPayments.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsPayments_MouseMoveEvent);
            // 
            // lblEFT
            // 
            this.lblEFT.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblEFT.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblEFT.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblEFT.Location = new System.Drawing.Point(315, 245);
            this.lblEFT.Name = "lblEFT";
            this.lblEFT.Size = new System.Drawing.Size(171, 24);
            this.lblEFT.TabIndex = 17;
            this.lblEFT.Text = "EFT TRANSACTION";
            this.lblEFT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEFT.Visible = false;
            // 
            // lblTransactions
            // 
            this.lblTransactions.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblTransactions.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblTransactions.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblTransactions.Location = new System.Drawing.Point(30, 14);
            this.lblTransactions.Name = "lblTransactions";
            this.lblTransactions.Size = new System.Drawing.Size(95, 16);
            this.lblTransactions.TabIndex = 18;
            this.lblTransactions.Text = "TRANSACTIONS";
            // 
            // Line2
            // 
            this.Line2.BorderColor = System.Drawing.Color.Empty;
            this.Line2.LineWidth = 1;
            this.Line2.Location = new System.Drawing.Point(185, 110);
            this.Line2.Name = "Line2";
            this.Line2.Size = new System.Drawing.Size(120, 1);
            this.Line2.X1 = 2775F;
            this.Line2.X2 = 4450F;
            this.Line2.Y1 = 1650F;
            this.Line2.Y2 = 1650F;
            // 
            // lblConvenienceFee
            // 
            this.lblConvenienceFee.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblConvenienceFee.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblConvenienceFee.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblConvenienceFee.Location = new System.Drawing.Point(30, 74);
            this.lblConvenienceFee.Name = "lblConvenienceFee";
            this.lblConvenienceFee.Size = new System.Drawing.Size(115, 16);
            this.lblConvenienceFee.TabIndex = 2;
            this.lblConvenienceFee.Text = "CONVENIENCE FEE";
            // 
            // lblCashOutTotal
            // 
            this.lblCashOutTotal.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblCashOutTotal.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblCashOutTotal.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCashOutTotal.Location = new System.Drawing.Point(30, 134);
            this.lblCashOutTotal.Name = "lblCashOutTotal";
            this.lblCashOutTotal.Size = new System.Drawing.Size(95, 16);
            this.lblCashOutTotal.TabIndex = 4;
            this.lblCashOutTotal.Text = "RECEIPT TOTAL";
            // 
            // lblCashOutCreditPaid
            // 
            this.lblCashOutCreditPaid.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblCashOutCreditPaid.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblCashOutCreditPaid.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCashOutCreditPaid.Location = new System.Drawing.Point(30, 314);
            this.lblCashOutCreditPaid.Name = "lblCashOutCreditPaid";
            this.lblCashOutCreditPaid.Size = new System.Drawing.Size(117, 16);
            this.lblCashOutCreditPaid.TabIndex = 10;
            this.lblCashOutCreditPaid.Text = "TOTAL CREDIT PAID";
            // 
            // lblCashOutTotalDue
            // 
            this.lblCashOutTotalDue.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblCashOutTotalDue.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblCashOutTotalDue.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCashOutTotalDue.Location = new System.Drawing.Point(30, 374);
            this.lblCashOutTotalDue.Name = "lblCashOutTotalDue";
            this.lblCashOutTotalDue.Size = new System.Drawing.Size(73, 16);
            this.lblCashOutTotalDue.TabIndex = 12;
            this.lblCashOutTotalDue.Text = "TOTAL DUE";
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.Empty;
            this.Line1.LineWidth = 1;
            this.Line1.Location = new System.Drawing.Point(185, 350);
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(120, 1);
            this.Line1.X1 = 2775F;
            this.Line1.X2 = 4450F;
            this.Line1.Y1 = 5250F;
            this.Line1.Y2 = 5250F;
            // 
            // lblCashOutChange
            // 
            this.lblCashOutChange.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblCashOutChange.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblCashOutChange.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCashOutChange.Location = new System.Drawing.Point(30, 434);
            this.lblCashOutChange.Name = "lblCashOutChange";
            this.lblCashOutChange.Size = new System.Drawing.Size(60, 16);
            this.lblCashOutChange.TabIndex = 14;
            this.lblCashOutChange.Text = "CHANGE";
            this.lblCashOutChange.Visible = false;
            // 
            // lblCashOutCheckPaid
            // 
            this.lblCashOutCheckPaid.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblCashOutCheckPaid.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblCashOutCheckPaid.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCashOutCheckPaid.Location = new System.Drawing.Point(30, 254);
            this.lblCashOutCheckPaid.Name = "lblCashOutCheckPaid";
            this.lblCashOutCheckPaid.Size = new System.Drawing.Size(115, 16);
            this.lblCashOutCheckPaid.TabIndex = 8;
            this.lblCashOutCheckPaid.Text = "TOTAL CHECK PAID";
            // 
            // lblCashOutCashPaid
            // 
            this.lblCashOutCashPaid.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblCashOutCashPaid.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblCashOutCashPaid.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCashOutCashPaid.Location = new System.Drawing.Point(30, 194);
            this.lblCashOutCashPaid.Name = "lblCashOutCashPaid";
            this.lblCashOutCashPaid.Size = new System.Drawing.Size(110, 16);
            this.lblCashOutCashPaid.TabIndex = 6;
            this.lblCashOutCashPaid.Text = "TOTAL CASH PAID";
            // 
            // fraSummary
            // 
            this.fraSummary.AppearanceKey = "groupBoxNoBorders";
            this.fraSummary.Controls.Add(this.txtPaidBy);
            this.fraSummary.Controls.Add(this.vsSummary);
            this.fraSummary.Controls.Add(this.Label7);
            this.fraSummary.Controls.Add(this.lblSummaryTitle);
            this.fraSummary.Location = new System.Drawing.Point(10, 0);
            this.fraSummary.Name = "fraSummary";
            this.fraSummary.Size = new System.Drawing.Size(758, 278);
            this.fraSummary.TabIndex = 2;
            this.fraSummary.Visible = false;
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaidBy.Location = new System.Drawing.Point(133, 214);
            this.txtPaidBy.LockedOriginal = true;
            this.txtPaidBy.MaxLength = 50;
            this.txtPaidBy.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.ReadOnly = true;
            this.txtPaidBy.Size = new System.Drawing.Size(343, 40);
            this.txtPaidBy.TabIndex = 3;
            // 
            // vsSummary
            // 
            this.vsSummary.AutoSizeColumnsMode = Wisej.Web.DataGridViewAutoSizeColumnsMode.AllCells;
            this.vsSummary.Cols = 3;
            this.vsSummary.FixedCols = 0;
            this.vsSummary.Location = new System.Drawing.Point(20, 66);
            this.vsSummary.Name = "vsSummary";
            this.vsSummary.RowHeadersVisible = false;
            this.vsSummary.Rows = 1;
            this.vsSummary.Size = new System.Drawing.Size(720, 128);
            this.vsSummary.TabIndex = 1;
            this.vsSummary.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsSummary_MouseDownEvent);
            // 
            // Label7
            // 
            this.Label7.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.Label7.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.Label7.Location = new System.Drawing.Point(20, 228);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(50, 16);
            this.Label7.TabIndex = 2;
            this.Label7.Text = "PAID BY";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSummaryTitle
            // 
            this.lblSummaryTitle.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblSummaryTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblSummaryTitle.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblSummaryTitle.Location = new System.Drawing.Point(20, 30);
            this.lblSummaryTitle.Name = "lblSummaryTitle";
            this.lblSummaryTitle.Size = new System.Drawing.Size(604, 16);
            this.lblSummaryTitle.TabIndex = 4;
            this.lblSummaryTitle.Text = "SUMMARY";
            this.lblSummaryTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraReceiptNumber
            // 
            this.fraReceiptNumber.Anchor = Wisej.Web.AnchorStyles.Left;
            this.fraReceiptNumber.Controls.Add(this.txtTellerID);
            this.fraReceiptNumber.Controls.Add(this.lblArchive);
            this.fraReceiptNumber.Controls.Add(this.cmbArchive);
            this.fraReceiptNumber.Controls.Add(this.txtReceipt);
            this.fraReceiptNumber.Controls.Add(this.lblLastAccount);
            this.fraReceiptNumber.Location = new System.Drawing.Point(20, 34);
            this.fraReceiptNumber.Name = "fraReceiptNumber";
            this.fraReceiptNumber.Size = new System.Drawing.Size(299, 150);
            this.fraReceiptNumber.TabIndex = 3;
            this.fraReceiptNumber.Text = "Enter Receipt Number";
            this.fraReceiptNumber.Visible = false;
            // 
            // txtTellerID
            // 
            this.txtTellerID.BackColor = System.Drawing.SystemColors.Window;
            this.txtTellerID.Location = new System.Drawing.Point(213, 30);
            this.txtTellerID.MaxLength = 3;
            this.txtTellerID.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtTellerID.Name = "txtTellerID";
            this.txtTellerID.Size = new System.Drawing.Size(66, 40);
            this.txtTellerID.TabIndex = 1;
            this.txtTellerID.Visible = false;
            // 
            // txtReceipt
            // 
            this.txtReceipt.BackColor = System.Drawing.SystemColors.Window;
            this.txtReceipt.Location = new System.Drawing.Point(115, 30);
            this.txtReceipt.MaxLength = 9;
            this.txtReceipt.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtReceipt.Name = "txtReceipt";
            this.txtReceipt.Size = new System.Drawing.Size(78, 40);
            this.txtReceipt.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtReceipt, "Default number is last receipt entered.");
            this.txtReceipt.KeyDown += new Wisej.Web.KeyEventHandler(this.txtReceipt_KeyDown);
            this.txtReceipt.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReceipt_KeyPress);
            // 
            // lblLastAccount
            // 
            this.lblLastAccount.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblLastAccount.AutoSize = true;
            this.lblLastAccount.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblLastAccount.Location = new System.Drawing.Point(206, 43);
            this.lblLastAccount.Name = "lblLastAccount";
            this.lblLastAccount.Size = new System.Drawing.Size(4, 16);
            this.lblLastAccount.TabIndex = 2;
            this.lblLastAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdPrintReceipt
            // 
            this.cmdPrintReceipt.AppearanceKey = "acceptButton";
            this.cmdPrintReceipt.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdPrintReceipt.Location = new System.Drawing.Point(265, 30);
            this.cmdPrintReceipt.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdPrintReceipt.Name = "cmdPrintReceipt";
            this.cmdPrintReceipt.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintReceipt.Size = new System.Drawing.Size(125, 48);
            this.cmdPrintReceipt.Text = "Show Receipt";
            this.cmdPrintReceipt.Click += new System.EventHandler(this.mnuProcessPrint_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSplit,
            this.mnuProcessPrint,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSplit
            // 
            this.mnuProcessSplit.Enabled = false;
            this.mnuProcessSplit.Index = 0;
            this.mnuProcessSplit.Name = "mnuProcessSplit";
            this.mnuProcessSplit.Text = "Print Split";
            this.mnuProcessSplit.Click += new System.EventHandler(this.mnuProcessSplit_Click);
            // 
            // mnuProcessPrint
            // 
            this.mnuProcessPrint.Index = 1;
            this.mnuProcessPrint.Name = "mnuProcessPrint";
            this.mnuProcessPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessPrint.Text = "Show Receipt";
            this.mnuProcessPrint.Click += new System.EventHandler(this.mnuProcessPrint_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmReprint
            // 
            this.BorderStyleOriginal = fecherFoundation.FCForm.BorderStyleConstants.vbSizable;
            this.ClientSize = new System.Drawing.Size(999, 695);
            this.FillStyle = fecherFoundation.FillStyleConstants.vbFSTransparent;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.Name = "frmReprint";
            this.PaletteMode = fecherFoundation.FCForm.PalleteModeConstants.vbPaletteModeHalfTone;
            this.ScaleMode = fecherFoundation.ScaleModeConstants.vbTwips;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reprint Receipt";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmReprint_Load);
            this.Activated += new System.EventHandler(this.frmReprint_Activated);
            this.Resize += new System.EventHandler(this.frmReprint_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReprint_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReprint_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).EndInit();
            this.fraReceiptList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsReceiptList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPaymentInfo)).EndInit();
            this.fraPaymentInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsPaymentInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSplit)).EndInit();
            this.fraSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSplitExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSplitPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCashOut)).EndInit();
            this.fraCashOut.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).EndInit();
            this.fraSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptNumber)).EndInit();
            this.fraReceiptNumber.ResumeLayout(false);
            this.fraReceiptNumber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintReceipt)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
