﻿namespace TWCR0000
{
    public class cFundTotal
    {
        public int Fund { get; set; } = 0;
        public double Amount { get; set; } = 0;
    }
}