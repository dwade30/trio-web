﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmMultiTownSelect.
	/// </summary>
	public partial class frmMultiTownSelect : BaseForm
	{
		public frmMultiTownSelect()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMultiTownSelect InstancePtr
		{
			get
			{
				return (frmMultiTownSelect)Sys.GetInstance(typeof(frmMultiTownSelect));
			}
		}

		protected frmMultiTownSelect _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Kevin Kelly             *
		// DATE           :               06/19/2012              *
		// *
		// MODIFIED BY    :               Kevin Kelly             *
		// LAST UPDATED   :               06/19/2012              *
		// ********************************************************
		bool boolCanceled;
		int lngTownIndex;
		int lngTownCode;
		bool boolAddingResCodes;

		public int GetTownCode()
		{
			int GetTownCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				boolCanceled = true;
				this.Show(FormShowEnum.Modal, App.MainForm);
				if (boolCanceled)
				{
					GetTownCode = 0;
				}
				else
				{
					GetTownCode = lngTownCode;
				}
				return GetTownCode;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Town Selection Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetTownCode;
		}

		public int GetTownIndex()
		{
			int GetTownIndex = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				boolCanceled = true;
				this.Show(FormShowEnum.Modal, App.MainForm);
				if (boolCanceled)
				{
					GetTownIndex = -1;
				}
				else
				{
					GetTownIndex = lngTownIndex;
					// cmbResCode.ListIndex
				}
				return GetTownIndex;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Town Selection Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetTownIndex;
		}

		private void cmbResCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lngTownIndex = cmbResCode.SelectedIndex;
			lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString())));
		}

		private void cmdContinue_Click()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				boolCanceled = false;
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Town Selection Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdCancel_Click()
		{
			boolCanceled = true;
			Close();
		}

		private void frmMultiTownSelect_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmMultiTownSelect_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMultiTownSelect.FillStyle	= 0;
			//frmMultiTownSelect.ScaleWidth	= 4965;
			//frmMultiTownSelect.ScaleHeight	= 1320;
			//frmMultiTownSelect.LinkTopic	= "Form2";
			//frmMultiTownSelect.LockControls	= -1  'True;
			//frmMultiTownSelect.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strMessage = "";
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Select Town Code";
			FillResCodeCombo();
		}

		private void frmMultiTownSelect_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileCancel_Click()
		{
			boolCanceled = true;
			Close();
		}

		private void mnuFileContinue_Click(object sender, System.EventArgs e)
		{
			boolCanceled = false;
			cmdContinue_Click();
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			boolCanceled = true;
			Close();
		}

		private void FillResCodeCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRC = new clsDRWrapper();
				cmbResCode.Clear();
				rsRC.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber > 0 ORDER BY TownNumber", "CentralData");
				while (!rsRC.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					cmbResCode.AddItem(Strings.Format(rsRC.Get_Fields("TownNumber"), "00") + " - " + Strings.Trim(FCConvert.ToString(rsRC.Get_Fields_String("TownName"))));
					rsRC.MoveNext();
				}
				//Application.DoEvents();
				if (cmbResCode.Items.Count > 0)
				{
					cmbResCode.SelectedIndex = 0;
				}
				if (cmbResCode.Items.Count <= 1)
				{
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Town ID Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
