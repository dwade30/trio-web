﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTypes.
	/// </summary>
	partial class sarTypes
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarTypes));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBill = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceipt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTeller = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTypeHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblConvenienceFeeTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.sarTypeDetailOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldConvenienceFeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitleTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotals1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotals7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConvenienceFeeTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.sarTypeDetailOb
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooter
			});
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblName,
				this.lblAcct,
				this.lblBill,
				this.lblReceipt,
				this.lblDate,
				this.lblTeller,
				this.lblNC,
				this.lblPayType,
				this.lblCode,
				this.lblTitle1,
				this.lblTitle2,
				this.lblTitle3,
				this.lblTitle4,
				this.lblTitle5,
				this.lblTitle6,
				this.lblTotal,
				this.lblTypeHeader,
				this.lblConvenienceFeeTitle
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.46875F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldConvenienceFeeTotal,
				this.lblTitleTotal,
				this.fldTotals1,
				this.fldTotals2,
				this.fldTotals3,
				this.fldTotals4,
				this.fldTotals5,
				this.fldTotals6,
				this.fldTotals7,
				this.Line2
			});
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 4.9375F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.125F;
			this.lblName.Visible = false;
			this.lblName.Width = 0.75F;
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.1875F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0.6875F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblAcct.Text = "Acct #";
			this.lblAcct.Top = 0.3125F;
			this.lblAcct.Visible = false;
			this.lblAcct.Width = 0.375F;
			// 
			// lblBill
			// 
			this.lblBill.Height = 0.1875F;
			this.lblBill.HyperLink = null;
			this.lblBill.Left = 1.0625F;
			this.lblBill.Name = "lblBill";
			this.lblBill.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblBill.Text = "Bill";
			this.lblBill.Top = 0.3125F;
			this.lblBill.Visible = false;
			this.lblBill.Width = 0.25F;
			// 
			// lblReceipt
			// 
			this.lblReceipt.Height = 0.1875F;
			this.lblReceipt.HyperLink = null;
			this.lblReceipt.Left = 1.3125F;
			this.lblReceipt.Name = "lblReceipt";
			this.lblReceipt.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblReceipt.Text = "Recpt";
			this.lblReceipt.Top = 0.3125F;
			this.lblReceipt.Visible = false;
			this.lblReceipt.Width = 0.5625F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 4.4375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.125F;
			this.lblDate.Visible = false;
			this.lblDate.Width = 0.4375F;
			// 
			// lblTeller
			// 
			this.lblTeller.Height = 0.1875F;
			this.lblTeller.HyperLink = null;
			this.lblTeller.Left = 1.875F;
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTeller.Text = "TLR";
			this.lblTeller.Top = 0.3125F;
			this.lblTeller.Visible = false;
			this.lblTeller.Width = 0.3125F;
			// 
			// lblNC
			// 
			this.lblNC.Height = 0.1875F;
			this.lblNC.HyperLink = null;
			this.lblNC.Left = 2.1875F;
			this.lblNC.Name = "lblNC";
			this.lblNC.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblNC.Text = "CD/AC";
			this.lblNC.Top = 0.3125F;
			this.lblNC.Visible = false;
			this.lblNC.Width = 0.5625F;
			// 
			// lblPayType
			// 
			this.lblPayType.Height = 0.1875F;
			this.lblPayType.HyperLink = null;
			this.lblPayType.Left = 0.1875F;
			this.lblPayType.Name = "lblPayType";
			this.lblPayType.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblPayType.Text = "CC/CK";
			this.lblPayType.Top = 0.3125F;
			this.lblPayType.Visible = false;
			this.lblPayType.Width = 0.5F;
			// 
			// lblCode
			// 
			this.lblCode.Height = 0.1875F;
			this.lblCode.HyperLink = null;
			this.lblCode.Left = 2.75F;
			this.lblCode.Name = "lblCode";
			this.lblCode.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblCode.Text = "Code";
			this.lblCode.Top = 0.3125F;
			this.lblCode.Visible = false;
			this.lblCode.Width = 0.375F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.1875F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 3F;
			this.lblTitle1.MultiLine = false;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTitle1.Text = null;
			this.lblTitle1.Top = 0.3125F;
			this.lblTitle1.Width = 1F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1875F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 4F;
			this.lblTitle2.MultiLine = false;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTitle2.Text = null;
			this.lblTitle2.Top = 0.3125F;
			this.lblTitle2.Width = 1F;
			// 
			// lblTitle3
			// 
			this.lblTitle3.Height = 0.1875F;
			this.lblTitle3.HyperLink = null;
			this.lblTitle3.Left = 5F;
			this.lblTitle3.MultiLine = false;
			this.lblTitle3.Name = "lblTitle3";
			this.lblTitle3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTitle3.Text = null;
			this.lblTitle3.Top = 0.3125F;
			this.lblTitle3.Width = 1F;
			// 
			// lblTitle4
			// 
			this.lblTitle4.Height = 0.1875F;
			this.lblTitle4.HyperLink = null;
			this.lblTitle4.Left = 6F;
			this.lblTitle4.MultiLine = false;
			this.lblTitle4.Name = "lblTitle4";
			this.lblTitle4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTitle4.Text = null;
			this.lblTitle4.Top = 0.3125F;
			this.lblTitle4.Width = 1F;
			// 
			// lblTitle5
			// 
			this.lblTitle5.Height = 0.1875F;
			this.lblTitle5.HyperLink = null;
			this.lblTitle5.Left = 7F;
			this.lblTitle5.MultiLine = false;
			this.lblTitle5.Name = "lblTitle5";
			this.lblTitle5.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTitle5.Text = null;
			this.lblTitle5.Top = 0.3125F;
			this.lblTitle5.Width = 1F;
			// 
			// lblTitle6
			// 
			this.lblTitle6.Height = 0.1875F;
			this.lblTitle6.HyperLink = null;
			this.lblTitle6.Left = 8F;
			this.lblTitle6.MultiLine = false;
			this.lblTitle6.Name = "lblTitle6";
			this.lblTitle6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblTitle6.Text = null;
			this.lblTitle6.Top = 0.3125F;
			this.lblTitle6.Width = 1F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 9F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 9.75pt; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.3125F;
			this.lblTotal.Width = 1F;
			// 
			// lblTypeHeader
			// 
			this.lblTypeHeader.Height = 0.25F;
			this.lblTypeHeader.HyperLink = null;
			this.lblTypeHeader.Left = 0F;
			this.lblTypeHeader.Name = "lblTypeHeader";
			this.lblTypeHeader.Style = "font-family: \'Tahoma\'; font-size: 11.5pt; font-weight: bold";
			this.lblTypeHeader.Text = null;
			this.lblTypeHeader.Top = 0.125F;
			this.lblTypeHeader.Width = 3.625F;
			// 
			// lblConvenienceFeeTitle
			// 
			this.lblConvenienceFeeTitle.Height = 0.1875F;
			this.lblConvenienceFeeTitle.HyperLink = null;
			this.lblConvenienceFeeTitle.Left = 6.53125F;
			this.lblConvenienceFeeTitle.MultiLine = false;
			this.lblConvenienceFeeTitle.Name = "lblConvenienceFeeTitle";
			this.lblConvenienceFeeTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblConvenienceFeeTitle.Text = "CONV. FEE";
			this.lblConvenienceFeeTitle.Top = 0.3125F;
			this.lblConvenienceFeeTitle.Width = 1F;
			// 
			// sarTypeDetailOb
			// 
			this.sarTypeDetailOb.CloseBorder = false;
			this.sarTypeDetailOb.Height = 0.125F;
			this.sarTypeDetailOb.Left = 0F;
			this.sarTypeDetailOb.Name = "sarTypeDetailOb";
			this.sarTypeDetailOb.Report = null;
			this.sarTypeDetailOb.Top = 0F;
			this.sarTypeDetailOb.Width = 10F;
			// 
			// fldConvenienceFeeTotal
			// 
			this.fldConvenienceFeeTotal.Height = 0.1875F;
			this.fldConvenienceFeeTotal.Left = 3.6875F;
			this.fldConvenienceFeeTotal.Name = "fldConvenienceFeeTotal";
			this.fldConvenienceFeeTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldConvenienceFeeTotal.Text = null;
			this.fldConvenienceFeeTotal.Top = 0.0625F;
			this.fldConvenienceFeeTotal.Width = 1F;
			// 
			// lblTitleTotal
			// 
			this.lblTitleTotal.Height = 0.1875F;
			this.lblTitleTotal.HyperLink = null;
			this.lblTitleTotal.Left = 0F;
			this.lblTitleTotal.Name = "lblTitleTotal";
			this.lblTitleTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.lblTitleTotal.Text = null;
			this.lblTitleTotal.Top = 0.0625F;
			this.lblTitleTotal.Width = 3F;
			// 
			// fldTotals1
			// 
			this.fldTotals1.Height = 0.1875F;
			this.fldTotals1.Left = 3F;
			this.fldTotals1.Name = "fldTotals1";
			this.fldTotals1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotals1.Text = null;
			this.fldTotals1.Top = 0.0625F;
			this.fldTotals1.Width = 1F;
			// 
			// fldTotals2
			// 
			this.fldTotals2.Height = 0.1875F;
			this.fldTotals2.Left = 4F;
			this.fldTotals2.Name = "fldTotals2";
			this.fldTotals2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotals2.Text = null;
			this.fldTotals2.Top = 0.0625F;
			this.fldTotals2.Width = 1F;
			// 
			// fldTotals3
			// 
			this.fldTotals3.Height = 0.1875F;
			this.fldTotals3.Left = 5F;
			this.fldTotals3.Name = "fldTotals3";
			this.fldTotals3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotals3.Text = null;
			this.fldTotals3.Top = 0.0625F;
			this.fldTotals3.Width = 1F;
			// 
			// fldTotals4
			// 
			this.fldTotals4.Height = 0.1875F;
			this.fldTotals4.Left = 6F;
			this.fldTotals4.Name = "fldTotals4";
			this.fldTotals4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotals4.Text = null;
			this.fldTotals4.Top = 0.0625F;
			this.fldTotals4.Width = 1F;
			// 
			// fldTotals5
			// 
			this.fldTotals5.Height = 0.1875F;
			this.fldTotals5.Left = 7F;
			this.fldTotals5.Name = "fldTotals5";
			this.fldTotals5.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotals5.Text = null;
			this.fldTotals5.Top = 0.0625F;
			this.fldTotals5.Width = 1F;
			// 
			// fldTotals6
			// 
			this.fldTotals6.Height = 0.1875F;
			this.fldTotals6.Left = 8F;
			this.fldTotals6.Name = "fldTotals6";
			this.fldTotals6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotals6.Text = null;
			this.fldTotals6.Top = 0.0625F;
			this.fldTotals6.Width = 1F;
			// 
			// fldTotals7
			// 
			this.fldTotals7.Height = 0.1875F;
			this.fldTotals7.Left = 9F;
			this.fldTotals7.Name = "fldTotals7";
			this.fldTotals7.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.fldTotals7.Text = null;
			this.fldTotals7.Top = 0.0625F;
			this.fldTotals7.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.0625F;
			this.Line2.Width = 7F;
			this.Line2.X1 = 3F;
			this.Line2.X2 = 10F;
			this.Line2.Y1 = 0.0625F;
			this.Line2.Y2 = 0.0625F;
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.2F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.lblFooter.Text = null;
			this.lblFooter.Top = 0F;
			this.lblFooter.Width = 6F;
			// 
			// sarTypes
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.4722222F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConvenienceFeeTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConvenienceFeeTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotals7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarTypeDetailOb;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBill;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTeller;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNC;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTypeHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConvenienceFeeTitle;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConvenienceFeeTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitleTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotals7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
