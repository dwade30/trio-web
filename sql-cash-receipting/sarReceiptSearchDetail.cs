﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using Wisej.Web;

namespace TWCR0000
{
    /// <summary>
    /// Summary description for sarReceiptSearchDetail.
    /// </summary>
    public partial class sarReceiptSearchDetail : FCSectionReport
    {
	    private bool firstRecord = true;
	    private int counter = 0;
	    private List<ReceiptSearchReportResult> reportData = new List<ReceiptSearchReportResult>();
	    private bool showDetail = false;

		public sarReceiptSearchDetail()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
			if (firstRecord)
			{
				firstRecord = false;
				counter = 0;
			}
			else
			{
				counter++;
			}

			if (counter < reportData.Count)
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {

	        reportData = ((arReceiptSearch) ParentReport).reportData.ToList();
	        showDetail = ((arReceiptSearch) ParentReport).reportService.ReportSetup.ReceiptDetailLevel ==
	                     SummaryDetailOption.Detail;


			firstRecord = true;

	        if (reportData.Count() == 0)
	        {
		        ((arReceiptSearch)ParentReport).NoRecords();
	        }

			SetupFields();
		}

        private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
            if (showDetail)
            {
	            SetupDetailFields();
            }
        }

        private void SetupFields()
        {
            // this sub will move/hide the fields needed to create the report that the user wants
            if (showDetail)
            {
                fldAcct1.Visible = true;
                fldAcct2.Visible = true;
                fldAcct3.Visible = true;
                fldAcct4.Visible = true;
                fldAcct5.Visible = true;
                fldAcct6.Visible = true;
                //if (modGlobal.Statics.gstrEPaymentPortal == "P")
                //{
                //    fldConvenienceFeeAccount.Visible = true;
                //}
                //else
                //{
                    fldConvenienceFeeAccount.Visible = false;
                //}
                fldAmt1.Visible = true;
                fldAmt2.Visible = true;
                fldAmt3.Visible = true;
                fldAmt4.Visible = true;
                fldAmt5.Visible = true;
                fldAmt6.Visible = true;
                //if (modGlobal.Statics.gstrEPaymentPortal == "P")
                //{
                //    fldConvenienceFeeAmount.Visible = true;
                //}
                //else
                //{
                    fldConvenienceFeeAmount.Visible = false;
                //}
                fldControl1.Visible = true;
                fldControl2.Visible = true;
                fldControl3.Visible = true;
                lblControlTitle1.Visible = true;
                lblControlTitle2.Visible = true;
                lblControlTitle3.Visible = true;
                fldComment.Visible = true;
            }
            else
            {
                fldAcct1.Visible = false;
                fldAcct2.Visible = false;
                fldAcct3.Visible = false;
                fldAcct4.Visible = false;
                fldAcct5.Visible = false;
                fldAcct6.Visible = false;
                fldConvenienceFeeAccount.Visible = false;
                fldAmt1.Visible = false;
                fldAmt2.Visible = false;
                fldAmt3.Visible = false;
                fldAmt4.Visible = false;
                fldAmt5.Visible = false;
                fldAmt6.Visible = false;
                fldConvenienceFeeAmount.Visible = false;
                fldControl1.Visible = false;
                fldControl2.Visible = false;
                fldControl3.Visible = false;
                lblControlTitle1.Visible = false;
                lblControlTitle2.Visible = false;
                lblControlTitle3.Visible = false;
                fldAcct1.Top = 0;
                fldAcct2.Top = 0;
                fldAcct3.Top = 0;
                fldAcct4.Top = 0;
                fldAcct5.Top = 0;
                fldAcct6.Top = 0;
                fldConvenienceFeeAccount.Top = 0;
                fldAmt1.Top = 0;
                fldAmt2.Top = 0;
                fldAmt3.Top = 0;
                fldAmt4.Top = 0;
                fldAmt5.Top = 0;
                fldAmt6.Top = 0;
                fldConvenienceFeeAmount.Top = 0;
                fldControl1.Top = 0;
                fldControl2.Top = 0;
                fldControl3.Top = 0;
                lblControlTitle1.Top = 0;
                lblControlTitle2.Top = 0;
                lblControlTitle3.Top = 0;
                fldComment.Visible = true;
                fldComment.Top = 0;
                //fldComment.Height = 270 / 1440f;
                // also hide the "Account" header title because none of the accounts are going to be shown
                lblAccount.Visible = false;
                // set the name field to fit in the space left by the account missing
                fldName.Width = fldRef.Left - fldName.Left;
                sarRSReceiptDetailOB.Visible = false;
                sarRSReceiptDetailOB.Top = 0;
            }
        }

        private void BindFields()
        {
            frmWait.InstancePtr.IncrementProgress();
            fldControl1.Text = "";
            fldControl2.Text = "";
            fldControl3.Text = "";
            fldControl1.Top = 270 / 1440f;
            fldControl2.Top = 270 / 1440f;
            fldControl3.Top = 270 / 1440f;
            lblControlTitle1.Top = 270 / 1440f;
            lblControlTitle2.Top = 270 / 1440f;
            lblControlTitle3.Top = 270 / 1440f;
            // check to make sure that this is the correct receipt rather than a duplicate ID receipt
            // this sub will fill the fields in the detail section
            if (((arReceiptSearch)ParentReport).reportService.ReportSetup.DateToShow == DateToShowOptions.ActualSystemDate)
            {
	            fldDate.Text = reportData[counter].ActualSystemDate.Value.ToShortDateString();
                lblTime.Visible = true;
                fldTransTime.Text = reportData[counter].ActualSystemDate.Value.ToShortTimeString();
            }
            else
            {
                fldDate.Text = reportData[counter].RecordedTransactionDate.Value.ToShortDateString();
				lblTime.Visible = false;
                fldTransTime.Text = "";
            }
            fldReceipt.Text = reportData[counter].ReceiptNumber.ToString();
			fldType.Text = reportData[counter].ReceiptType.ToString().PadLeft(3,'0');
            fldTeller.Text = reportData[counter].TellerID;
            fldName.Text = reportData[counter].Name;
            fldRef.Text = reportData[counter].Reference;
            fldAmount.Text =
	            (reportData[counter].Amount1 + reportData[counter].Amount2 + reportData[counter].Amount3 +
	             reportData[counter].Amount4 + reportData[counter].Amount5 + reportData[counter].Amount6)
	            .ToString("#,##0.00");

			
            lblControlTitle1.Text = reportData[counter].Control1Label + ":";
            lblControlTitle2.Text = reportData[counter].Control2Label + ":";
			lblControlTitle3.Text = reportData[counter].Control3Label + ":";

			fldControl1.Text = reportData[counter].Control1.Trim();
            if (Strings.Trim(fldControl1.Text) == "")
            {
                fldControl1.Visible = false;
                lblControlTitle1.Visible = false;
                fldControl2.Top = fldControl1.Top;
                lblControlTitle2.Top = lblControlTitle1.Top;
            }
            else
            {
                fldControl1.Visible = true;
                lblControlTitle1.Visible = true;
                fldControl2.Top = fldControl1.Top + 270 / 1440f;
                lblControlTitle2.Top = lblControlTitle1.Top + 270 / 1440f;
            }
            fldControl2.Text = reportData[counter].Control2.Trim();
			if (Strings.Trim(fldControl2.Text) == "")
            {
                fldControl2.Visible = false;
                lblControlTitle2.Visible = false;
                fldControl3.Top = fldControl2.Top;
                lblControlTitle3.Top = lblControlTitle2.Top;
            }
            else
            {
                fldControl2.Visible = true;
                lblControlTitle2.Visible = true;
                fldControl3.Top = fldControl2.Top + 270 / 1440f;
                lblControlTitle3.Top = lblControlTitle2.Top + 270 / 1440f;
            }
            fldControl3.Text = reportData[counter].Control3.Trim();
			if (Strings.Trim(fldControl3.Text) == "")
            {
                fldControl3.Visible = false;
                lblControlTitle3.Visible = false;
            }
            else
            {
                fldControl3.Visible = true;
                lblControlTitle3.Visible = true;
            }

			if (reportData[counter].Amount1 != 0 || reportData[counter].Account1 != "")
            {
                fldAcct1.Text = reportData[counter].Account1;
                fldAmt1.Text = reportData[counter].Amount1.ToString("#,##0.00");
            }
            else
            {
                fldAcct1.Text = "";
                fldAmt1.Text = "";
            }
			if (reportData[counter].Amount2 != 0 || reportData[counter].Account2 != "")
			{
				fldAcct2.Text = reportData[counter].Account2;
				fldAmt2.Text = reportData[counter].Amount2.ToString("#,##0.00");
			}
			else
			{
				fldAcct2.Text = "";
				fldAmt2.Text = "";
			}
			if (reportData[counter].Amount3 != 0 || reportData[counter].Account3 != "")
			{
				fldAcct3.Text = reportData[counter].Account3;
				fldAmt3.Text = reportData[counter].Amount3.ToString("#,##0.00");
			}
			else
			{
				fldAcct3.Text = "";
				fldAmt3.Text = "";
			}
			if (reportData[counter].Amount4 != 0 || reportData[counter].Account4 != "")
			{
				fldAcct4.Text = reportData[counter].Account4;
				fldAmt4.Text = reportData[counter].Amount4.ToString("#,##0.00");
			}
			else
			{
				fldAcct4.Text = "";
				fldAmt4.Text = "";
			}
			if (reportData[counter].Amount5 != 0 || reportData[counter].Account5 != "")
			{
				fldAcct5.Text = reportData[counter].Account5;
				fldAmt5.Text = reportData[counter].Amount5.ToString("#,##0.00");
			}
			else
			{
				fldAcct5.Text = "";
				fldAmt5.Text = "";
			}
			if (reportData[counter].Amount6 != 0 || reportData[counter].Account6 != "")
			{
				fldAcct6.Text = reportData[counter].Account6;
				fldAmt6.Text = reportData[counter].Amount6.ToString("#,##0.00");
			}
			else
			{
				fldAcct6.Text = "";
				fldAmt6.Text = "";
			}


			if (reportData[counter].ConvenienceFee != 0 || reportData[counter].ConvenienceFeeGLAccount != "")
            {
                fldConvenienceFeeAccount.Text = reportData[counter].ConvenienceFeeGLAccount;
                fldConvenienceFeeAmount.Text = reportData[counter].ConvenienceFee.ToString("#,##0.00");
            }
            else
            {
                fldConvenienceFeeAccount.Text = "";
                fldConvenienceFeeAmount.Text = "";
            }
            if (reportData[counter].Comment.Trim() != "" && showDetail)
            {
	            fldComment.Text = "Comment : " + reportData[counter].Comment.Trim();
				fldComment.Visible = true;
				fldComment.Top = fldControl3.Visible ? fldControl3.Top + fldControl3.Height :
					fldControl2.Visible ? fldControl2.Top + fldControl2.Height :
					fldControl3.Visible ? fldControl1.Top + fldControl1.Height : fldRef.Top + fldRef.Height;
				//fldComment.Height = 720 / 1440f;
            }
            else
            {
                fldComment.Text = "";
                fldComment.Visible = false;
                fldComment.Top = 0;
                //fldComment.Height = 0;
            }

            if (((arReceiptSearch)ParentReport).reportService.ReportSetup.ShowTenderedAmounts)
            {
                sarRSReceiptDetailOB.Report = new sarRSReceiptDetail();
                if (reportData[counter].MultipleLineItemsOnReceipt)
                {
					((arReceiptSearch)ParentReport).lblDisclaimer.Visible = true;
				}

                sarRSReceiptDetailOB.Report.UserData = reportData[counter];
            }
        }

        private void SetupDetailFields()
        {
			float currentTop = fldRef.Top + fldRef.Height;
			float currentDescriptionTop = fldComment.Visible ? fldComment.Top + fldComment.Height :
				fldControl3.Visible ? fldControl3.Top + fldControl3.Height :
				fldControl2.Visible ? fldControl2.Top + fldControl2.Height :
				fldControl3.Visible ? fldControl1.Top + fldControl1.Height : 0;

			if (Conversion.Val(fldAmt1.Text) != 0)
            {
                fldAmt1.Visible = true;
                fldAcct1.Visible = true;
                fldAmt1.Top = currentTop;
                fldAcct1.Top = currentTop;
				currentTop = fldAcct1.Top + fldAcct1.Height;
			}
            else
            {
                fldAmt1.Visible = false;
                fldAcct1.Visible = false;
                fldAmt1.Top = 0;
                fldAcct1.Top = 0;
            }
            if (Conversion.Val(fldAmt2.Text) != 0)
            {
                fldAmt2.Visible = true;
                fldAcct2.Visible = true;
                fldAmt2.Top = currentTop;
                fldAcct2.Top = currentTop;
				currentTop = fldAcct2.Top + fldAcct2.Height;
			}
            else
            {
                fldAmt2.Visible = false;
                fldAcct2.Visible = false;
                fldAmt2.Top = 0;
                fldAcct2.Top = 0;
            }
            if (Conversion.Val(fldAmt3.Text) != 0)
            {
                fldAmt3.Visible = true;
                fldAcct3.Visible = true;
                fldAmt3.Top = currentTop;
                fldAcct3.Top = currentTop;
				currentTop = fldAcct3.Top + fldAcct3.Height;
			}
            else
            {
                fldAmt3.Visible = false;
                fldAcct3.Visible = false;
                fldAmt3.Top = 0;
                fldAcct3.Top = 0;
            }
            if (Conversion.Val(fldAmt4.Text) != 0)
            {
                fldAmt4.Visible = true;
                fldAcct4.Visible = true;
                fldAmt4.Top = currentTop;
                fldAcct4.Top = currentTop;
				currentTop = fldAcct4.Top + fldAcct4.Height;
			}
            else
            {
                fldAmt4.Visible = false;
                fldAcct4.Visible = false;
                fldAmt4.Top = 0;
                fldAcct4.Top = 0;
            }
            if (Conversion.Val(fldAmt5.Text) != 0)
            {
                fldAmt5.Visible = true;
                fldAcct5.Visible = true;
                fldAmt5.Top = currentTop;
                fldAcct5.Top = currentTop;
				currentTop = fldAcct5.Top + fldAcct5.Height;
			}
            else
            {
                fldAmt5.Visible = false;
                fldAcct5.Visible = false;
                fldAmt5.Top = 0;
                fldAcct5.Top = 0;
            }
            if (Conversion.Val(fldAmt6.Text) != 0)
            {
                fldAmt6.Visible = true;
                fldAcct6.Visible = true;
                fldAmt6.Top = currentTop;
                fldAcct6.Top = currentTop;
				currentTop = fldAcct6.Top + fldAcct6.Height;
			}
            else
            {
                fldAmt6.Visible = false;
                fldAcct6.Visible = false;
                fldAmt6.Top = 0;
                fldAcct6.Top = 0;
            }
            if (Conversion.Val(fldConvenienceFeeAmount.Text) != 0)
            {
                fldConvenienceFeeAmount.Visible = true;
                fldConvenienceFeeAccount.Visible = true;
                fldConvenienceFeeAmount.Top = currentTop;
                fldConvenienceFeeAccount.Top = currentTop;
				currentTop = fldConvenienceFeeAccount.Top + fldConvenienceFeeAccount.Height;
			}
            else
            {
                fldConvenienceFeeAmount.Visible = false;
                fldConvenienceFeeAccount.Visible = false;
                fldConvenienceFeeAmount.Top = 0;
                fldConvenienceFeeAccount.Top = 0;
            }

			sarRSReceiptDetailOB.Top = (currentTop > currentDescriptionTop ? currentTop : currentDescriptionTop) ;
		}
    }
}
