﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTellerCheckReportDetail.
	/// </summary>
	partial class sarTellerCheckReportDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarTellerCheckReportDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblBankNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBankNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblBankNumberTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBankTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCheckCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumberTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBankNumber,
				this.fldCheckNumber,
				this.fldAmount,
				this.fldReceiptNumber
			});
			this.Detail.Height = 0.125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblBankNumber,
				this.lblCheckNumber,
				this.lblAmount,
				this.lblReceiptNumber
			});
			this.GroupHeader1.Height = 0.21875F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line1,
				this.lblBankNumberTotal,
				this.fldBankTotal,
				this.Line2,
				this.fldCheckCount
			});
			this.GroupFooter1.Height = 0.3958333F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblBankNumber
			// 
			this.lblBankNumber.Height = 0.1875F;
			this.lblBankNumber.HyperLink = null;
			this.lblBankNumber.Left = 0.0625F;
			this.lblBankNumber.Name = "lblBankNumber";
			this.lblBankNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblBankNumber.Text = "Bank Number";
			this.lblBankNumber.Top = 0F;
			this.lblBankNumber.Width = 2.3125F;
			// 
			// lblCheckNumber
			// 
			this.lblCheckNumber.Height = 0.1875F;
			this.lblCheckNumber.HyperLink = null;
			this.lblCheckNumber.Left = 2.375F;
			this.lblCheckNumber.Name = "lblCheckNumber";
			this.lblCheckNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblCheckNumber.Text = "Check Number";
			this.lblCheckNumber.Top = 0F;
			this.lblCheckNumber.Width = 1.25F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 3.625F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0F;
			this.lblAmount.Width = 1.4375F;
			// 
			// lblReceiptNumber
			// 
			this.lblReceiptNumber.Height = 0.1875F;
			this.lblReceiptNumber.HyperLink = null;
			this.lblReceiptNumber.Left = 5.0625F;
			this.lblReceiptNumber.Name = "lblReceiptNumber";
			this.lblReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblReceiptNumber.Text = "Receipt Number";
			this.lblReceiptNumber.Top = 0F;
			this.lblReceiptNumber.Width = 1.4375F;
			// 
			// fldBankNumber
			// 
			this.fldBankNumber.Height = 0.125F;
			this.fldBankNumber.Left = 0.5F;
			this.fldBankNumber.MultiLine = false;
			this.fldBankNumber.Name = "fldBankNumber";
			this.fldBankNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; white-space: nowrap";
			this.fldBankNumber.Text = null;
			this.fldBankNumber.Top = 0F;
			this.fldBankNumber.Width = 1.5F;
			// 
			// fldCheckNumber
			// 
			this.fldCheckNumber.Height = 0.125F;
			this.fldCheckNumber.Left = 2.1875F;
			this.fldCheckNumber.MultiLine = false;
			this.fldCheckNumber.Name = "fldCheckNumber";
			this.fldCheckNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; white-space: nowrap";
			this.fldCheckNumber.Text = null;
			this.fldCheckNumber.Top = 0F;
			this.fldCheckNumber.Width = 1.4375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.125F;
			this.fldAmount.Left = 3.625F;
			this.fldAmount.MultiLine = false;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; white-space: nowrap";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.4375F;
			// 
			// fldReceiptNumber
			// 
			this.fldReceiptNumber.Height = 0.125F;
			this.fldReceiptNumber.Left = 5.0625F;
			this.fldReceiptNumber.MultiLine = false;
			this.fldReceiptNumber.Name = "fldReceiptNumber";
			this.fldReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; white-space: nowrap";
			this.fldReceiptNumber.Text = null;
			this.fldReceiptNumber.Top = 0F;
			this.fldReceiptNumber.Width = 1.4375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.5625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 1.625F;
			this.Line1.X1 = 3.5625F;
			this.Line1.X2 = 5.1875F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// lblBankNumberTotal
			// 
			this.lblBankNumberTotal.Height = 0.1875F;
			this.lblBankNumberTotal.HyperLink = null;
			this.lblBankNumberTotal.Left = 0.75F;
			this.lblBankNumberTotal.MultiLine = false;
			this.lblBankNumberTotal.Name = "lblBankNumberTotal";
			this.lblBankNumberTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; white-space: nowrap";
			this.lblBankNumberTotal.Text = "Bank Total:";
			this.lblBankNumberTotal.Top = 0F;
			this.lblBankNumberTotal.Width = 2.875F;
			// 
			// fldBankTotal
			// 
			this.fldBankTotal.Height = 0.1875F;
			this.fldBankTotal.Left = 3.625F;
			this.fldBankTotal.MultiLine = false;
			this.fldBankTotal.Name = "fldBankTotal";
			this.fldBankTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; whi" + "te-space: nowrap";
			this.fldBankTotal.Text = "0.00";
			this.fldBankTotal.Top = 0F;
			this.fldBankTotal.Width = 1.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.3125F;
			this.Line2.Width = 7F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7F;
			this.Line2.Y1 = 0.3125F;
			this.Line2.Y2 = 0.3125F;
			// 
			// fldCheckCount
			// 
			this.fldCheckCount.Height = 0.1875F;
			this.fldCheckCount.Left = 5.5F;
			this.fldCheckCount.MultiLine = false;
			this.fldCheckCount.Name = "fldCheckCount";
			this.fldCheckCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; whit" + "e-space: nowrap";
			this.fldCheckCount.Text = "0";
			this.fldCheckCount.Top = 0F;
			this.fldCheckCount.Width = 1F;
			// 
			// sarTellerCheckReportDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumberTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBankNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBankNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBankNumberTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBankTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckCount;
	}
}
