﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmGetFocus.
	/// </summary>
	public partial class frmGetFocus : BaseForm
	{
		public frmGetFocus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetFocus InstancePtr
		{
			get
			{
				return (frmGetFocus)Sys.GetInstance(typeof(frmGetFocus));
			}
		}

		protected frmGetFocus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/21/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/22/2006              *
		// ********************************************************
		int lngWait;
		// MAL@20080414: Add API calls to find and show open programs
		// Tracker Reference: 12912
		[DllImport("user32", EntryPoint = "FindWindowA")]
		private static extern int FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32")]
		private static extern int ShowWindow(int hwnd, int nCmdShow);

		const int SW_NORMAL = 1;
		const int SW_MAXIMIZE = 3;
		const short SW_SHOW = 5;
		const int SW_MINIMIZE = 6;
		const int SW_RESTORE = 9;
		int lngResult;

		public bool Init()
		{
			bool Init = true;
			string strTemp = "";
			int lngTemp;
			// MsgBox "Entering GotFocus Init function"
			try
			{
				// On Error GoTo ErrorHandler
				//Application.DoEvents();
				if (modGlobalConstants.Statics.gboolMV)
				{
                    //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                    //lngWait = Interaction.Shell("TWMV0000.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                    TWSharedLibrary.Variables.Statics.ShowOpIDForm = false;
                    lngWait = App.MainForm.OpenModule("TWMV0000", true, true);
                    TWSharedLibrary.Variables.Statics.ShowOpIDForm = true;
                    modGlobalConstants.Statics.gblnMVIsOpen = true;
				}
				else
				{
                    // dblRBValue = 0
                    //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                    //lngWait = Interaction.Shell("TWRB0000.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                    lngWait = App.MainForm.OpenModule("TWRB0000");
                }
				lngTemp = FCConvert.ToInt32(modGlobal.GetFocusBehavior(0));
				modGlobal.ChangeFocusBehavior(0);
                // GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CR\", "MVCheck", strTemp
                // If strTemp = "TRUE" Then
                // lblOut.Text = "You have successfully completed your Motor Vehicle transaction."
                // Me.Show vbModal, frmReceiptInput
                // Else
                
                if (lngWait != 0)
				{
					//modExtraModules.WaitForTerm(ref lngWait);
                    App.MainForm.LockModuleSwitch();
                    App.MainForm.WaitForMVModule();
                    App.MainForm.UnlockModuleSwitch();
                    //switch back to Cash Receipt
                    App.MainForm.OpenModule("TWCR0000");
                    // MAL@20080414: Activate application and set global value to indicate still open
                    // Tracker Reference: 12912
                    if (IsMVStillOpen(ref lngResult))
					{
						ShowWindow(lngResult, SW_SHOW);
						modGlobalConstants.Statics.gblnMVIsOpen = true;
					}
					else
					{
						modGlobalConstants.Statics.gblnMVIsOpen = false;
						//Application.DoEvents();
						Close();
						//MDIParent.InstancePtr.GRID.Focus();
					}
				}
                else
                {
                    App.MainForm.OpenModule("TWCR0000");
                    Init = false;
                }
				// MAL@20080414: Move to more appropriate place in code
				// Tracker Reference: 12912
				// DoEvents
				// Unload Me
				// MDIParent.Grid.SetFocus
				// End If
				// ChangeFocusBehavior lngtemp
				// Unload Me
				return Init;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Error in GotFocus Init", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return Init;
		}

		private void cmdGo_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmGetFocus_Activated(object sender, System.EventArgs e)
		{
			// If lngWait <> 0 Then
			// WaitForTerm lngWait
			// End If
			// DoEvents
			// Unload Me
			// MDIParent.Grid.SetFocus
		}

		private void frmGetFocus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetFocus.FillStyle	= 0;
			//frmGetFocus.ScaleWidth	= 3885;
			//frmGetFocus.ScaleHeight	= 2115;
			//frmGetFocus.LinkTopic	= "Form2";
			//frmGetFocus.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuProcessSaveExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private bool IsMVStillOpen(ref int lngResult)
		{
			bool IsMVStillOpen = false;
			// Tracker Reference: 12912
			bool blnResult;
			lngResult = FindWindow(string.Empty, "TRIO Software - Motor Vehicle [Main]");
			blnResult = (lngResult > 0);
			IsMVStillOpen = blnResult;
			return IsMVStillOpen;
		}
	}
}
