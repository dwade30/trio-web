﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCreditCard.
	/// </summary>
	partial class frmCreditCard : BaseForm
	{
		public FCGrid vsCC;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileAdd;
		public fecherFoundation.FCToolStripMenuItem mnuFileDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsCC = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileAdd = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrintBankList = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.btnSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintBankList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 388);
			this.BottomPanel.Size = new System.Drawing.Size(849, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsCC);
			this.ClientArea.Size = new System.Drawing.Size(849, 328);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrintBankList);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(849, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintBankList, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(351, 30);
			this.HeaderText.Text = "Credit/Debit Card Maintenance";
			// 
			// vsCC
			// 
			this.vsCC.AllowSelection = false;
			this.vsCC.AllowUserToResizeColumns = false;
			this.vsCC.AllowUserToResizeRows = false;
			this.vsCC.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsCC.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsCC.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsCC.BackColorBkg = System.Drawing.Color.Empty;
			this.vsCC.BackColorFixed = System.Drawing.Color.Empty;
			this.vsCC.BackColorSel = System.Drawing.Color.Empty;
			this.vsCC.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsCC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsCC.ColumnHeadersHeight = 30;
			this.vsCC.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsCC.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsCC.DragIcon = null;
			this.vsCC.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsCC.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsCC.FixedCols = 0;
			this.vsCC.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsCC.FrozenCols = 0;
			this.vsCC.GridColor = System.Drawing.Color.Empty;
			this.vsCC.GridColorFixed = System.Drawing.Color.Empty;
			this.vsCC.Location = new System.Drawing.Point(30, 30);
			this.vsCC.Name = "vsCC";
			this.vsCC.ReadOnly = true;
			this.vsCC.RowHeadersVisible = false;
			this.vsCC.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsCC.RowHeightMin = 0;
			this.vsCC.Rows = 1;
			this.vsCC.ScrollTipText = null;
			this.vsCC.ShowColumnVisibilityMenu = false;
			this.vsCC.Size = new System.Drawing.Size(789, 268);
			this.vsCC.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsCC.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsCC.TabIndex = 0;
			this.vsCC.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsCC_ChangeEdit);
			this.vsCC.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsCC_ValidateEdit);
			this.vsCC.CurrentCellChanged += new System.EventHandler(this.vsCC_RowColChange);
			this.vsCC.KeyDown += new Wisej.Web.KeyEventHandler(this.vsCC_KeyDownEvent);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileAdd,
				this.mnuFileDelete,
				this.mnuFilePrint,
				this.mnuFileSeperator2,
				this.mnuFileSave,
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileAdd
			// 
			this.mnuFileAdd.Index = 0;
			this.mnuFileAdd.Name = "mnuFileAdd";
			this.mnuFileAdd.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuFileAdd.Text = "New";
			this.mnuFileAdd.Click += new System.EventHandler(this.mnuFileAdd_Click);
			// 
			// mnuFileDelete
			// 
			this.mnuFileDelete.Index = 1;
			this.mnuFileDelete.Name = "mnuFileDelete";
			this.mnuFileDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuFileDelete.Text = "Delete";
			this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 2;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print Preview Credit/Debit Card List";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 3;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 4;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 5;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 6;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 7;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdPrintBankList
			// 
			this.cmdPrintBankList.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintBankList.AppearanceKey = "toolbarButton";
			this.cmdPrintBankList.Location = new System.Drawing.Point(694, 29);
			this.cmdPrintBankList.Name = "cmdPrintBankList";
			this.cmdPrintBankList.Size = new System.Drawing.Size(119, 24);
			this.cmdPrintBankList.TabIndex = 6;
			this.cmdPrintBankList.Text = "Print Preview List";
			this.cmdPrintBankList.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(633, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.cmdDelete.Size = new System.Drawing.Size(56, 24);
			this.cmdDelete.TabIndex = 5;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(582, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.cmdNew.Size = new System.Drawing.Size(45, 24);
			this.cmdNew.TabIndex = 4;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// btnSave
			// 
			this.btnSave.AppearanceKey = "acceptButton";
			this.btnSave.Location = new System.Drawing.Point(155, 30);
			this.btnSave.Name = "btnSave";
			this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSave.Size = new System.Drawing.Size(90, 48);
			this.btnSave.TabIndex = 1;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmCreditCard
			// 
			this.ClientSize = new System.Drawing.Size(849, 484);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCreditCard";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Credit/Debit Card Maintenance";
			this.Load += new System.EventHandler(this.frmCreditCard_Load);
			this.Activated += new System.EventHandler(this.frmCreditCard_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCreditCard_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreditCard_KeyPress);
			this.Resize += new System.EventHandler(this.frmCreditCard_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintBankList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdPrintBankList;
		private FCButton cmdDelete;
		private FCButton cmdNew;
		private FCButton btnSave;
	}
}
