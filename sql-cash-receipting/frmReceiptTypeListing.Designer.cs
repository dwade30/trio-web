﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmReceiptTypeListing.
	/// </summary>
	partial class frmReceiptTypeListing : BaseForm
	{
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCPanel fraChoice;
		public fecherFoundation.FCCheckBox chkSortAlpha;
		public fecherFoundation.FCButton cmdPreview;
		public fecherFoundation.FCCheckBox chk2Across;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCComboBox cmbEnd;
		public fecherFoundation.FCComboBox cmbStart;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.fraChoice = new fecherFoundation.FCPanel();
			this.chkSortAlpha = new fecherFoundation.FCCheckBox();
			this.chk2Across = new fecherFoundation.FCCheckBox();
			this.fraRange = new fecherFoundation.FCFrame();
			this.cmbEnd = new fecherFoundation.FCComboBox();
			this.cmbStart = new fecherFoundation.FCComboBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraChoice)).BeginInit();
			this.fraChoice.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSortAlpha)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk2Across)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 457);
			this.BottomPanel.Size = new System.Drawing.Size(536, 30);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdPreview);
			this.ClientArea.Controls.Add(this.fraChoice);
			this.ClientArea.Size = new System.Drawing.Size(536, 397);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(536, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(235, 30);
			this.HeaderText.Text = "Receipt Type Listing";
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(30, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 0;
			this.lblRange.Text = "RANGE";
			this.lblRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All Types",
				"Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(122, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(167, 40);
			this.cmbRange.TabIndex = 1;
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// fraChoice
			// 
			this.fraChoice.AppearanceKey = "groupBoxNoBorders";
			this.fraChoice.Controls.Add(this.chkSortAlpha);
			this.fraChoice.Controls.Add(this.lblRange);
			this.fraChoice.Controls.Add(this.cmbRange);
			this.fraChoice.Controls.Add(this.chk2Across);
			this.fraChoice.Controls.Add(this.fraRange);
			this.fraChoice.Location = new System.Drawing.Point(0, 0);
			this.fraChoice.Name = "fraChoice";
			this.fraChoice.Size = new System.Drawing.Size(490, 316);
			this.fraChoice.TabIndex = 0;
			// 
			// chkSortAlpha
			// 
			this.chkSortAlpha.Location = new System.Drawing.Point(30, 220);
			this.chkSortAlpha.Name = "chkSortAlpha";
			this.chkSortAlpha.Size = new System.Drawing.Size(129, 26);
			this.chkSortAlpha.TabIndex = 3;
			this.chkSortAlpha.Text = "Sort By Name";
			// 
			// chk2Across
			// 
			this.chk2Across.Location = new System.Drawing.Point(30, 268);
			this.chk2Across.Name = "chk2Across";
			this.chk2Across.Size = new System.Drawing.Size(92, 26);
			this.chk2Across.TabIndex = 4;
			this.chk2Across.Text = "2-Across";
			// 
			// fraRange
			// 
			this.fraRange.AppearanceKey = "groupBoxNoBorders";
			this.fraRange.Controls.Add(this.cmbEnd);
			this.fraRange.Controls.Add(this.cmbStart);
			this.fraRange.Controls.Add(this.Label3);
			this.fraRange.Controls.Add(this.Label2);
			this.fraRange.Location = new System.Drawing.Point(0, 73);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(465, 140);
			this.fraRange.TabIndex = 2;
			// 
			// cmbEnd
			// 
			this.cmbEnd.AutoSize = false;
			this.cmbEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbEnd.Enabled = false;
			this.cmbEnd.FormattingEnabled = true;
			this.cmbEnd.Location = new System.Drawing.Point(122, 76);
			this.cmbEnd.Name = "cmbEnd";
			this.cmbEnd.Size = new System.Drawing.Size(329, 40);
			this.cmbEnd.Sorted = true;
			this.cmbEnd.TabIndex = 3;
			this.cmbEnd.DropDown += new System.EventHandler(this.cmbEnd_DropDown);
			this.cmbEnd.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbEnd_KeyDown);
			// 
			// cmbStart
			// 
			this.cmbStart.AutoSize = false;
			this.cmbStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbStart.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbStart.Enabled = false;
			this.cmbStart.FormattingEnabled = true;
			this.cmbStart.Location = new System.Drawing.Point(122, 16);
			this.cmbStart.Name = "cmbStart";
			this.cmbStart.Size = new System.Drawing.Size(329, 40);
			this.cmbStart.Sorted = true;
			this.cmbStart.TabIndex = 1;
			this.cmbStart.DropDown += new System.EventHandler(this.cmbStart_DropDown);
			this.cmbStart.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbStart_KeyDown);
			// 
			// Label3
			// 
			this.Label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label3.Location = new System.Drawing.Point(30, 90);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(32, 24);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "END";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(45, 24);
			this.Label2.TabIndex = 0;
			this.Label2.Text = "START";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Location = new System.Drawing.Point(30, 330);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Size = new System.Drawing.Size(122, 40);
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.TabIndex = 5;
			this.cmdPreview.Text = "Print Preview";
			this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePreview,
				this.mnuFileSeperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 0;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 1;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmReceiptTypeListing
			// 
			this.ClientSize = new System.Drawing.Size(536, 487);
			this.KeyPreview = true;
			this.Name = "frmReceiptTypeListing";
			this.Text = "Receipt Type Listing";
			this.Load += new System.EventHandler(this.frmReceiptTypeListing_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReceiptTypeListing_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraChoice)).EndInit();
			this.fraChoice.ResumeLayout(false);
			this.fraChoice.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSortAlpha)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk2Across)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
