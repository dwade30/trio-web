﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication.CashReceipts.Models;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for arDailyTellerAuditReport.
	/// </summary>
	public partial class arDailyTellerAuditReport : BaseSectionReport
	{
		public static arDailyTellerAuditReport InstancePtr
		{
			get
			{
				return (arDailyTellerAuditReport)Sys.GetInstance(typeof(arDailyTellerAuditReport));
			}
		}

		protected arDailyTellerAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            rsTotals_AutoInitialized?.Dispose();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arDailyTellerAuditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/19/2005              *
		// ********************************************************
		double[] arrTotals = new double[7 + 1];
		public string strTellerID = "";
		public int intReportHeader;
		private clsDRWrapper rsTotals_AutoInitialized;
		private bool boolUsePrevYears = false;
        
		private clsDRWrapper rsTotals
		{
			get
			{
				if (rsTotals_AutoInitialized == null)
				{
					rsTotals_AutoInitialized = new clsDRWrapper();
				}
				return rsTotals_AutoInitialized;
			}
		}

		public arDailyTellerAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public arDailyTellerAuditReport(bool isSubReport) : this()
        {
            GroupHeader1.Visible = isSubReport;
        }

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Teller Audit Report";
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			rsTotals.Reset();
            rsTotals_AutoInitialized.DisposeOf();
			rsTotals_AutoInitialized = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTellerAuditType = "";

				boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;

				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				lblDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				frmDailyReceiptAudit.InstancePtr.txtTellerID.Text = Strings.Trim(FCConvert.ToString(this.UserData));
                lblGroupTellerId.Text = "Teller ID: " + this.UserData;
				if (modGlobal.Statics.gboolTellerCheckReportWithAudit && !boolUsePrevYears)
				{
					sarTellerCheckReportOB.Report = new sarTellerCheckReport();
					sarTellerCheckReportOB.Report.UserData = Strings.Trim(FCConvert.ToString(this.UserData));
					sarTellerCheckReportOB.Visible = true;
				}
				else
				{
					sarTellerCheckReportOB.Visible = false;
				}
				// this will setup the orientation
				if (!modGlobal.Statics.gboolDailyAuditLandscape)
				{
					// If frmDailyReceiptAudit.optAlignment(0).Value Then
					arDailyTellerAuditReport.InstancePtr.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
				}
				else
				{
					arDailyTellerAuditReport.InstancePtr.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				}
				SetupForm(arDailyTellerAuditReport.InstancePtr.PageSettings.Orientation);
				//this.Zoom = -1;
				// Post CloseOut
				// Pre CloseOut
				// Both
				if (frmDailyReceiptAudit.InstancePtr.cmbTellerType.SelectedIndex == 0)
				{
					strTellerAuditType = "Post Close Out";
				}
				else if (frmDailyReceiptAudit.InstancePtr.cmbTellerType.SelectedIndex == 1)
				{
					strTellerAuditType = "Pre Close Out";
				}
				else
				{
					strTellerAuditType = "Post and Pre Close Out";
				}
				if (Strings.Trim(frmDailyReceiptAudit.InstancePtr.txtTellerID.Text) != "")
				{
					if (!frmDailyReceiptAudit.InstancePtr.boolCloseOut)
					{
						lblTellerID.Text = "PREVIEW" + "\r\n" + "Teller ID : " + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + " - " + strTellerAuditType;
					}
					else
					{
						lblTellerID.Text = "\r\n" + "Teller ID : " + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text;
					}
				}
				else
				{
					if (!frmDailyReceiptAudit.InstancePtr.boolCloseOut)
					{
						lblTellerID.Text = "PREVIEW" + "\r\n" + "Teller ID : NONE - " + strTellerAuditType;
					}
					else
					{
						lblTellerID.Text = "\r\n" + "Teller ID : NONE";
					}
				}
				// this will setup the titles on each page
				lblHeader.Text = "Cash Receipting - Teller Audit Report";
				if (!modGlobal.Statics.gboolAuditSeqByType)
				{
					sarTypesOb.Report = new sarTypes();
					sarTypesOb.Report.UserData = frmDailyReceiptAudit.InstancePtr.txtTellerID.Text;
				}
				else
				{
					sarTypesOb.Report = new sarTellerDetail();
					sarTypesOb.Report.UserData = frmDailyReceiptAudit.InstancePtr.txtTellerID.Text;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Teller Report Start Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupForm(GrapeCity.ActiveReports.Document.Section.PageOrientation intStyle)
		{
			if (!(intStyle == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape))
			{
				// normal
				lblHeader.Width = 10800 / 1440f;
				lblTellerID.Width = 10800 / 1440f;
                lblGroupTellerId.Width = 10800 / 1440f;
				sarTypesOb.Width = 10800 / 1440f;
				this.PrintWidth = 10800 / 1440f;
				fldTotals1.Width = 1500 / 1440f;
				fldTotals2.Width = 1500 / 1440f;
				fldTotals3.Width = 1500 / 1440f;
				fldTotals4.Width = 1500 / 1440f;
				fldTotals5.Width = 1500 / 1440f;
				fldTotals6.Width = 1500 / 1440f;
				fldTotals7.Width = 1500 / 1440f;
				fldConvenienceFeeTotal.Width = 1500 / 1440f;
				fldTotals1.Left = 4290 / 1440f;
				// 4440 + 40
				fldTotals2.Left = (5250 + 40) / 1440f;
				fldTotals3.Left = (6060 + 40) / 1440f;
				fldTotals4.Left = (6870 + 40) / 1440f;
				fldTotals5.Left = (7680 + 40) / 1440f;
				fldTotals6.Left = (8490 + 40) / 1440f;
				fldTotals7.Left = (9300 + 40) / 1440f;
			}
			else
			{
				// landscape
				lblHeader.Width = 15120 / 1440f;
				lblTellerID.Width = 15120 / 1440f;
                lblGroupTellerId.Width = 15120 / 1440f;
				sarTypesOb.Width = 15120 / 1440f;
				this.PrintWidth = 15120 / 1440f;
				lblTotalsTotal.Left = 3200 / 1440f;
				lnSubtotal.X2 = 15120 / 1440f;
				fldTotals1.Width = 1440 / 1440f;
				fldTotals2.Width = 1440 / 1440f;
				fldTotals3.Width = 1440 / 1440f;
				fldTotals4.Width = 1440 / 1440f;
				fldTotals5.Width = 1440 / 1440f;
				fldTotals6.Width = 1440 / 1440f;
				fldTotals7.Width = 1440 / 1440f;
				lblTotalsTotal.Left = 1370 / 1440f;
				lnSubtotal.X2 = 15120 / 1440f;
				fldTotals1.Left = 4320 / 1440f;
				fldTotals2.Left = 5760 / 1440f;
				fldTotals3.Left = 7200 / 1440f;
				fldTotals4.Left = 8640 / 1440f;
				fldTotals5.Left = 10080 / 1440f;
				fldTotals6.Left = 11520 / 1440f;
				fldTotals7.Left = 13530 / 1440f;
				fldTotals2.Top = fldTotals1.Top;
				fldTotals3.Top = fldTotals1.Top;
				fldTotals4.Top = fldTotals1.Top;
				fldTotals5.Top = fldTotals1.Top;
				fldTotals6.Top = fldTotals1.Top;
				fldTotals7.Top = fldTotals1.Top;
			}
			lblDate.Left = lblHeader.Width - lblDate.Width;
			lblPage.Left = lblDate.Left;
		}

		private void FillSumTotals()
		{
			// this will fill the totals at the bottom of the first report
			clsDRWrapper rsTotals = new clsDRWrapper();
			string strSQL;
			string strTellerList = "";
			string strTellerCO = "";
			string strOnlyTeller = "";
			// vbPorter upgrade warning: dblPrevious As string	OnWrite(string, short)
			string dblPrevious = "";
			// vbPorter upgrade warning: dblTemp As Decimal	OnWrite(double, string)
			double dblTemp;
			string strArchiveTable = "";

			// clear all of the fields so they can be filled with the correct values
			fldTotalReceivedToday.Text = "0.00";
			fldTotalReversalsToday.Text = "0.00";
			fldTotalNonCashToday.Text = "0.00";
			fldTotalReceivedAll.Text = "0.00";
			fldTotalReversalsAll.Text = "0.00";
			fldTotalNonCashAll.Text = "0.00";
			fldNetActivityToday.Text = "0.00";
			fldNetActivityAll.Text = "0.00";
			fldTotalCash.Text = "0.00";
			fldTotalCheck.Text = "0.00";
			fldTotalCredit.Text = "0.00";
			fldTotalPrevious.Text = "0.00";
			fldTotalOther.Text = "0.00";
			fldTotalTotal.Text = "0.00";
			// this will create a list of tellers that have already closed out today
			strSQL = "SELECT * FROM Closeout WHERE Type = 'T' AND CloseoutDate = '" + DateTime.Today.ToShortDateString() + "' ORDER BY CloseoutDate";
			rsTotals.OpenRecordset(strSQL);
			if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly && Strings.Trim(FCConvert.ToString(this.UserData)) != "")
			{
				strOnlyTeller = " AND TellerID = '" + this.UserData + "'";
				// strOnlyTeller = " AND TellerID = '" & frmDailyReceiptAudit.txtTellerID.Text & "'"
			}
			else
			{
				strOnlyTeller = "";
			}
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				strTellerList = "TellerID IN (";
				do
				{
					if (FCConvert.ToString(rsTotals.Get_Fields_String("TellerID")) != "")
					{
						strTellerList += rsTotals.Get_Fields_String("TellerID") + ",";
					}
					rsTotals.MoveNext();
				}
				while (!rsTotals.EndOfFile());
				if (Strings.InStr(1, strTellerList, ",", CompareConstants.vbBinaryCompare) != 0)
				{
					strTellerList = Strings.Left(strTellerList, strTellerList.Length - 1);
				}
				strTellerList += ")";
			}
			if (strTellerList != "TellerID IN ()" && Strings.Trim(strTellerList) != "")
			{
				// these are the arguements that will determine if the teller has closed out today
				strTellerCO = " AND ISNULL(TellerCloseOut,0) <> 0 OR (ISNULL(TellerCloseOut,0) = 0 AND NOT (" + strTellerList + "))";
			}
			else
			{
				strTellerCO = "";
			}

			if (boolUsePrevYears)
			{
				strArchiveTable = "LastYearArchive";
			}
			else
			{
				strArchiveTable = "Archive";
			}

			// TODAY-----------------------------------------------------------------------------------------
			// Total Received Today
			strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1" + strTellerCO + strOnlyTeller + "  GROUP BY Account1) ";
			strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1" + strTellerCO + strOnlyTeller + "  GROUP BY Account2) ";
			strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1" + strTellerCO + strOnlyTeller + "  GROUP BY Account3) ";
			strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1" + strTellerCO + strOnlyTeller + "  GROUP BY Account4) ";
			strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1" + strTellerCO + strOnlyTeller + "  GROUP BY Account5) ";
			strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND AffectCashDrawer = 0 AND EFT = 1" + strTellerCO + strOnlyTeller + "  GROUP BY Account6)";
			strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalEFT WHERE Account <> ''";
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsTotals.Get_Fields("SumAmount")) != 0)
				{
					// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
					dblTemp = FCConvert.ToDouble(rsTotals.Get_Fields("SumAmount"));
				}
				else
				{
					dblTemp = 0;
				}
			}
			else
			{
				dblTemp = 0;
			}
			strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount1 > 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account1) ";
			strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount2 > 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account2) ";
			strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount3 > 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account3) ";
			strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount4 > 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account4) ";
			strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount5 > 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account5) ";
			strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount6 > 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account6)";
			strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalPositiveReceipts";
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
				fldTotalReceivedToday.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("SumAmount")), "#,##0.00");
			}
			else
			{
				fldTotalReceivedToday.Text = "0.00";
			}
			// Total Reversals Today
			strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount1 < 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account1) ";
			strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount2 < 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account2) ";
			strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount3 < 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account3) ";
			strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount4 < 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account4) ";
			strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount5 < 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account5) ";
			strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND Amount6 < 0 AND AffectCashDrawer = 1" + strTellerCO + strOnlyTeller + " GROUP BY Account6)";
			strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalNegativeReceipts";
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
				fldTotalReversalsToday.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("SumAmount")) * -1, "#,##0.00");
			}
			else
			{
				fldTotalReversalsToday.Text = "0.00";
			}
			// Total Non-Cash Activity Today
			fldTotalNonCashToday.Text = "0.00";
			// ALL-------------------------------------------------------------------------------------------
			// Total Received All
			strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND EFT = 0 AND Amount1 > 0 GROUP BY Account1) ";
			strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND EFT = 0 AND Amount2 > 0 GROUP BY Account2) ";
			strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND EFT = 0 AND Amount3 > 0 GROUP BY Account3) ";
			strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND EFT = 0 AND Amount4 > 0 GROUP BY Account4) ";
			strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND EFT = 0 AND Amount5 > 0 GROUP BY Account5) ";
			strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND EFT = 0 AND Amount6 > 0 GROUP BY Account6)";
			strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalPositiveReceipts";
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
				fldTotalReceivedAll.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("SumAmount")) + dblTemp, "#,##0.00");
			}
			else
			{
				fldTotalReceivedAll.Text = "0.00";
			}
			// Total Reversals All
			strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND Amount1 < 0 GROUP BY Account1) ";
			strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND Amount2 < 0 GROUP BY Account2) ";
			strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND Amount3 < 0 GROUP BY Account3) ";
			strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND Amount4 < 0 GROUP BY Account4) ";
			strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND Amount5 < 0 GROUP BY Account5) ";
			strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND Amount6 < 0 GROUP BY Account6)";
			strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalNegativeReceipts";
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
				fldTotalReversalsAll.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("SumAmount")) * -1, "#,##0.00");
			}
			else
			{
				fldTotalReversalsAll.Text = "0.00";
			}
			// Total Non-Cash Activity All
			strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCashDrawer = 0 AND AffectCash = 0 AND EFT = 0 GROUP BY Account1) ";
			strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCashDrawer = 0 AND AffectCash = 0 AND EFT = 0  GROUP BY Account2) ";
			strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCashDrawer = 0 AND AffectCash = 0 AND EFT = 0  GROUP BY Account3) ";
			strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCashDrawer = 0 AND AffectCash = 0 AND EFT = 0  GROUP BY Account4) ";
			strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCashDrawer = 0 AND AffectCash = 0 AND EFT = 0  GROUP BY Account5) ";
			strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCashDrawer = 0 AND AffectCash = 0 AND EFT = 0  GROUP BY Account6)";
			strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as TotalNonCash WHERE Account <> ''";
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
				fldTotalNonCashAll.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("SumAmount")) * -1, "#,##0.00");
			}
			else
			{
				fldTotalNonCashAll.Text = "0.00";
			}
			// Net Activity
			// this will make sure all fields have values
			if (fldTotalReceivedToday.Text == "")
				fldTotalReceivedToday.Text = "0.00";
			if (fldTotalReversalsToday.Text == "")
				fldTotalReversalsToday.Text = "0.00";
			if (fldTotalNonCashToday.Text == "")
				fldTotalNonCashToday.Text = "0.00";
			if (fldTotalReceivedAll.Text == "")
				fldTotalReceivedAll.Text = "0.00";
			if (fldTotalReversalsAll.Text == "")
				fldTotalReversalsAll.Text = "0.00";
			if (fldTotalNonCashAll.Text == "")
				fldTotalNonCashAll.Text = "0.00";
			// this adds them up
			fldNetActivityToday.Text = Strings.Format(FCConvert.ToDouble(fldTotalReceivedToday.Text) - (FCConvert.ToDouble(fldTotalReversalsToday.Text) - FCConvert.ToDouble(fldTotalNonCashToday.Text)), "#,##0.00");
			fldNetActivityAll.Text = Strings.Format(FCConvert.ToDouble(fldTotalReceivedAll.Text) - (FCConvert.ToDouble(fldTotalReversalsAll.Text) - FCConvert.ToDouble(fldTotalNonCashAll.Text)), "#,##0.00");
			// Previoud Period Activity
			strSQL = "(SELECT Account1 as Account, sum (Amount1) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account1) ";
			strSQL += "UNION ALL (SELECT Account2 as Account, sum(Amount2) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account2) ";
			strSQL += "UNION ALL (SELECT Account3 as Account, sum(Amount3) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account3) ";
			strSQL += "UNION ALL (SELECT Account4 as Account, sum(Amount4) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account4) ";
			strSQL += "UNION ALL (SELECT Account5 as Account, sum(Amount5) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account5) ";
			strSQL += "UNION ALL (SELECT Account6 as Account, sum(Amount6) as Total FROM " + strArchiveTable + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + " AND AffectCash = 1 AND AffectCashDrawer = 0 AND EFT = 0 GROUP BY Account6)";
			strSQL = "SELECT SUM(Total) as SumAmount FROM (" + strSQL + ") as PreviousPeriodTellerActivity WHERE Account <> ''";
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [SumAmount] not found!! (maybe it is an alias?)
				dblPrevious = Strings.Format(FCConvert.ToString(Conversion.Val(rsTotals.Get_Fields("SumAmount"))), "#,##0.00");
				fldTotalPrevious.Text = Strings.Format(FCConvert.ToDouble(dblPrevious) + dblTemp, "#,##0.00");
			}
			else
			{
				dblPrevious = FCConvert.ToString(0);
				fldTotalPrevious.Text = "0.00";
			}
			if (strOnlyTeller != "")
			{
				strOnlyTeller = " AND a.TellerID = " + Strings.Right(strOnlyTeller, 5);
			}

			if (boolUsePrevYears)
			{
				strSQL =
					"SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC " +
					"FROM (SELECT DISTINCT a.ReceiptNumber, r.CardAmount, r.CheckAmount, r.CashAmount " +
					"FROM LastYearArchive a INNER JOIN LastYearReceipt r ON a.ReceiptNumber = r.ReceiptKey " +
					"WHERE AffectCash = 1 AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + ") as temp";
			}
			else
			{
				strSQL =
					"SELECT SUM(CheckAmount) as CHK, SUM(CashAmount) as CSH, SUM(CardAmount) as CC " +
					"FROM (SELECT DISTINCT a.ReceiptNumber, r.CardAmount, r.CheckAmount, r.CashAmount " +
					"FROM Archive a INNER JOIN Receipt r ON a.ReceiptNumber = r.ReceiptKey " +
					"WHERE AffectCash = 1 AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOnlyTeller + ") as temp";
			}
			rsTotals.OpenRecordset(strSQL);
			if (!rsTotals.EndOfFile())
			{
				// Cash
				// TODO Get_Fields: Field [CSH] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsTotals.Get_Fields("CSH")) != 0)
				{
					// TODO Get_Fields: Field [CSH] not found!! (maybe it is an alias?)
					fldTotalCash.Text = Strings.Format(rsTotals.Get_Fields("CSH"), "#,##0.00");
				}
				else
				{
					fldTotalCash.Text = "0.00";
				}
				// Checks
				// TODO Get_Fields: Field [CHK] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsTotals.Get_Fields("CHK")) != 0)
				{
					// TODO Get_Fields: Field [CHK] not found!! (maybe it is an alias?)
					fldTotalCheck.Text = Strings.Format(rsTotals.Get_Fields("CHK"), "#,##0.00");
				}
				else
				{
					fldTotalCheck.Text = "0.00";
				}
				// Credit Card
				if (Conversion.Val(rsTotals.Get_Fields_String("CC")) != 0)
				{
					fldTotalCredit.Text = Strings.Format(rsTotals.Get_Fields_String("CC"), "#,##0.00");
				}
				else
				{
					fldTotalCredit.Text = "0.00";
				}
			}
			else
			{
				fldTotalCash.Text = "0.00";
				fldTotalCheck.Text = "0.00";
				fldTotalCredit.Text = "0.00";
			}
			// Other
			// TODO Get_Fields: Field [CSH] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [CHK] not found!! (maybe it is an alias?)
			fldTotalOther.Text = Strings.Format(FCConvert.ToDouble(fldNetActivityAll.Text) - (Conversion.Val(rsTotals.Get_Fields("CSH")) + Conversion.Val(rsTotals.Get_Fields("CHK")) + Conversion.Val(rsTotals.Get_Fields_String("CC")) + FCConvert.ToDouble(dblPrevious) + dblTemp), "#,##0.00");
			// Total
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldNetActivityAll.Text), "#,##0.00");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strSQL = "";
			string strArchiveTable = "";

			if (boolUsePrevYears)
			{
				strArchiveTable = "LastYearArchive";
			}
			else
			{
				strArchiveTable = "Archive";
			}
			if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
			{
				strSQL = "SELECT COUNT(*) AS ReceiptCount, sum(Amount1) as Total1, sum(Amount2) as Total2, sum(Amount3) as Total3, sum(Amount4) as Total4, sum(Amount5) as Total5, sum(Amount6) as Total6, sum(ConvenienceFee) as ConvenienceFeeTotal FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND " + strArchiveTable + ".TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "'" + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
			}
			else
			{
				strSQL = "SELECT COUNT(*) AS ReceiptCount, sum(Amount1) as Total1, sum(Amount2) as Total2, sum(Amount3) as Total3, sum(Amount4) as Total4, sum(Amount5) as Total5, sum(Amount6) as Total6, sum(ConvenienceFee) as ConvenienceFeeTotal FROM " + strArchiveTable + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
			}
			rsTotals.OpenRecordset(strSQL);
			if (rsTotals.EndOfFile() != true)
			{
				// TODO Get_Fields: Field [Total1] not found!! (maybe it is an alias?)
				fldTotals1.Text = Strings.Format(rsTotals.Get_Fields("Total1"), "#,##0.00");
				// TODO Get_Fields: Field [Total2] not found!! (maybe it is an alias?)
				fldTotals2.Text = Strings.Format(rsTotals.Get_Fields("Total2"), "#,##0.00");
				// TODO Get_Fields: Field [Total3] not found!! (maybe it is an alias?)
				fldTotals3.Text = Strings.Format(rsTotals.Get_Fields("Total3"), "#,##0.00");
				// TODO Get_Fields: Field [Total4] not found!! (maybe it is an alias?)
				fldTotals4.Text = Strings.Format(rsTotals.Get_Fields("Total4"), "#,##0.00");
				// TODO Get_Fields: Field [Total5] not found!! (maybe it is an alias?)
				fldTotals5.Text = Strings.Format(rsTotals.Get_Fields("Total5"), "#,##0.00");
				// TODO Get_Fields: Field [Total6] not found!! (maybe it is an alias?)
				fldTotals6.Text = Strings.Format(rsTotals.Get_Fields("Total6"), "#,##0.00");
				// fldConvenienceFeeTotal.Text = Format(rsTotals.Fields("ConvenienceFeeTotal"), "#,##0.00")
				//if (modGlobal.Statics.gstrEPaymentPortal == "P" || modGlobal.Statics.gstrEPaymentPortal == "I")
				//{
				//	lblConvenienceFees.Visible = true;
				//	fldConvenienceFees.Visible = true;
				//	// TODO Get_Fields: Field [ConvenienceFeeTotal] not found!! (maybe it is an alias?)
				//	fldConvenienceFees.Text = Strings.Format(rsTotals.Get_Fields("ConvenienceFeeTotal"), "#,##0.00");
				//}
				//else
				//{
					fldConvenienceFees.Visible = false;
					lblConvenienceFees.Visible = false;
					fldConvenienceFeeTotal.Text = "0.00";
				//}

				fldTotals7.Text = Strings.Format(rsTotals.Get_Fields("Total1") + rsTotals.Get_Fields("Total2") + rsTotals.Get_Fields("Total3") + rsTotals.Get_Fields("Total4") + rsTotals.Get_Fields("Total5") + rsTotals.Get_Fields("Total6"), "#,##0.00");
				// TODO Get_Fields: Field [ReceiptCount] not found!! (maybe it is an alias?)
				lblTotalsTotal.Text = FCConvert.ToString(rsTotals.Get_Fields("ReceiptCount")) + " Receipts      Net Received:";
				if (fldTotals1.Text == "")
					fldTotals1.Text = "0.00";
				if (fldTotals2.Text == "")
					fldTotals2.Text = "0.00";
				if (fldTotals3.Text == "")
					fldTotals3.Text = "0.00";
				if (fldTotals4.Text == "")
					fldTotals4.Text = "0.00";
				if (fldTotals5.Text == "")
					fldTotals5.Text = "0.00";
				if (fldTotals6.Text == "")
					fldTotals6.Text = "0.00";
				if (fldTotals7.Text == "")
					fldTotals7.Text = "0.00";
				// If fldConvenienceFeeTotal.Text = "" Then fldConvenienceFeeTotal.Text = "0.00"
				FillSumTotals();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
            
		}
	}
}
