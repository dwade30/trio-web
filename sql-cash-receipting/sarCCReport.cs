﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarCCReport.
	/// </summary>
	public partial class sarCCReport : FCSectionReport
	{
		public static sarCCReport InstancePtr
		{
			get
			{
				return (sarCCReport)Sys.GetInstance(typeof(sarCCReport));
			}
		}

		protected sarCCReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarCCReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/01/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int intCallCount;                         // MAL@20071023: Counts number of time detail report is called
		string strSQL;
		bool boolUsePrevYears = false;

		public sarCCReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarCCReport_ReportEnd;
		}

        private void SarCCReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				switch (modGlobal.Statics.gintCCReportOrder)
				{
					case 0:
						{
							// Don't Check Call Count
							break;
						}
					case 1:
						{
							if (intCallCount > 0)
							{
								return;
							}
							else
							{
								// Do Nothing
							}
							break;
						}
				}
				//end switch
				// TODO Get_Fields: Field [CardKey] not found!! (maybe it is an alias?)
				sarCCReportDetailOB.Report.UserData = rsData.Get_Fields("CardKey");
				intCallCount += 1;
				// SetData
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strMaster = "";
			string strArchive = "";

			boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
			if (boolUsePrevYears)
			{
				strMaster = "LastYearCCMaster";
				strArchive = "LastYearArchive";
			}
			else
			{
				strMaster = "CCMaster";
				strArchive = "Archive";
			}

			// This sql statement will find the totals for each bank and display them at the bottom of each detail
			strSQL = "SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL;
            strSQL = "SELECT * FROM CCType INNER JOIN (SELECT sum(ISNULL(ccmast.Amount,0)) as amt , CCType.ID as CardKey FROM ("+ strMaster + " ccmast INNER JOIN CCType ON ccmast.CCType = CCType.ID) INNER JOIN (" + strSQL + ") as NonClosedOutReceiptNumbers ON ccmast.ReceiptNumber = NonClosedOutReceiptNumbers.ReceiptNumber GROUP BY CCType.ID ) AS BK ON CCType.ID = BK.CardKey ORDER BY Type";
			sarCCReportDetailOB.Report = new sarCCReportDetail();
			rsData.OpenRecordset(strSQL);
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
                sarCCReportDetailOB.Report.UserData = rsData.Get_Fields("CardKey");
			}
			else
			{
                ShowFakeHeaders();
			}
		}

		private void GroupFooter1_AfterPrint(object sender, EventArgs e)
		{
			if (modGlobal.Statics.gboolTellerReportWithAudit)
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 3;
			}
			else
			{
				arDailyAuditReport.InstancePtr.intReportHeader = 4;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (arDailyAuditReport.InstancePtr.fldTotalCash.Text != "")
			{
				fldTotalCash.Text = arDailyAuditReport.InstancePtr.fldTotalCash.Text;
			}
			else
			{
				fldTotalCash.Text = "0.00";
			}
			if (arDailyAuditReport.InstancePtr.fldTotalCheck.Text != "")
			{
				fldTotalCheck.Text = arDailyAuditReport.InstancePtr.fldTotalCheck.Text;
			}
			else
			{
				fldTotalCheck.Text = "0.00";
			}
			if (arDailyAuditReport.InstancePtr.fldTotalCredit.Text != "")
			{
				fldTotalCredit.Text = arDailyAuditReport.InstancePtr.fldTotalCredit.Text;
			}
			else
			{
				fldTotalCredit.Text = "0.00";
			}
			if (arDailyAuditReport.InstancePtr.fldTotalOther.Text != "")
			{
				fldTotalOther.Text = arDailyAuditReport.InstancePtr.fldTotalOther.Text;
			}
			else
			{
				fldTotalOther.Text = "0.00";
			}
			fldTotal.Text = (fldTotalCash.Text.ToDecimalValue() + fldTotalCheck.Text.ToDecimalValue() + fldTotalCredit.Text.ToDecimalValue() + fldTotalOther.Text.ToDecimalValue()).FormatAsCurrencyNoSymbol();
		}

		private void ShowFakeHeaders()
		{
			// this routine will show a display of zero checks and zero check amount
			Line2.Visible = true;
			lblBankNumber.Visible = true;
			lblBankNumberTotal.Visible = true;
			lblCheckNumber.Visible = true;
			lblReceiptNumber.Visible = true;
			lblAmount.Visible = true;
			fldBankTotal.Visible = true;
		}
    }
}
