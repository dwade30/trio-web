﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Diagnostics;
using Global;
using System.IO;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmRPTViewer.
	/// </summary>
	partial class frmRPTViewer : FCForm
	{
		public fecherFoundation.FCFrame fraRecreate;
		public fecherFoundation.FCComboBox cmbRecreate;
		public fecherFoundation.FCFrame fraNumber;
		public fecherFoundation.FCComboBox cmbNumber;
		public fecherFoundation.FCFrame fraRecreateJournal;
		public fecherFoundation.FCComboBox cmbJournal;
		public fecherFoundation.FCComboBox cmbMonth;
		public fecherFoundation.FCComboBox cmbRecreateJournal;
		public fecherFoundation.FCLabel lblMonth;
		public fecherFoundation.FCLabel lblJournal;
		public Global.ARViewer arView;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEMail;
		public fecherFoundation.FCToolStripMenuItem mnuEMalRTF;
		public fecherFoundation.FCToolStripMenuItem mnuEMailPDF;
		public fecherFoundation.FCToolStripMenuItem mnuFileExport;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportRTF;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportPDF;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportHTML;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportExcel;
		public fecherFoundation.FCToolStripMenuItem mnuFileSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuFileRecreate;
		public fecherFoundation.FCToolStripMenuItem mnuFileRecreateJournal;
		public fecherFoundation.FCToolStripMenuItem mnuFileChange;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCButton btnFileSave;
		private ToolBar toolBar1;
		private ToolBarButton toolBarButtonEmailPDF;
		private ToolBarButton toolBarButtonExportPDF;
		private ToolBarButton toolBarButtonPrint;
		private ToolBarButton toolBarButtonEmailRTF;
		private ToolBarButton toolBarButtonExportRTF;
		private ToolBarButton toolBarButtonExportHTML;
		private ToolBarButton toolBarButtonExportExcel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraRecreate = new fecherFoundation.FCFrame();
			this.cmbRecreate = new fecherFoundation.FCComboBox();
			this.fraNumber = new fecherFoundation.FCFrame();
			this.cmbNumber = new fecherFoundation.FCComboBox();
			this.fraRecreateJournal = new fecherFoundation.FCFrame();
			this.cmbJournal = new fecherFoundation.FCComboBox();
			this.cmbMonth = new fecherFoundation.FCComboBox();
			this.cmbRecreateJournal = new fecherFoundation.FCComboBox();
			this.lblMonth = new fecherFoundation.FCLabel();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.arView = new Global.ARViewer();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFileRecreate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileRecreateJournal = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileChange = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEMail = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEMalRTF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEMailPDF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExportRTF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExportPDF = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExportHTML = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExportExcel = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSpacer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.toolBar1 = new Wisej.Web.ToolBar();
			this.toolBarButtonEmailPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonEmailRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportPDF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportRTF = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportHTML = new Wisej.Web.ToolBarButton();
			this.toolBarButtonExportExcel = new Wisej.Web.ToolBarButton();
			this.toolBarButtonPrint = new Wisej.Web.ToolBarButton();
			this.btnFileSave = new fecherFoundation.FCButton();
			((System.ComponentModel.ISupportInitialize)(this.fraRecreate)).BeginInit();
			this.fraRecreate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraNumber)).BeginInit();
			this.fraNumber.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRecreateJournal)).BeginInit();
			this.fraRecreateJournal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
			this.SuspendLayout();
			// 
			// fraRecreate
			// 
			this.fraRecreate.Controls.Add(this.cmbRecreate);
			this.fraRecreate.Location = new System.Drawing.Point(30, 30);
			this.fraRecreate.Name = "fraRecreate";
			this.fraRecreate.Size = new System.Drawing.Size(358, 90);
			this.fraRecreate.TabIndex = 3;
			this.fraRecreate.Text = "Select Close Out To Recreate";
			this.fraRecreate.Visible = false;
			// 
			// cmbRecreate
			// 
			this.cmbRecreate.BackColor = System.Drawing.SystemColors.Window;
			this.cmbRecreate.Location = new System.Drawing.Point(20, 30);
			this.cmbRecreate.Name = "cmbRecreate";
			this.cmbRecreate.Size = new System.Drawing.Size(313, 40);
			this.cmbRecreate.TabIndex = 4;
			this.cmbRecreate.DropDown += new System.EventHandler(this.cmbRecreate_DropDown);
			this.cmbRecreate.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbRecreate_KeyDown);
			// 
			// fraNumber
			// 
			this.fraNumber.Controls.Add(this.cmbNumber);
			this.fraNumber.Location = new System.Drawing.Point(30, 30);
			this.fraNumber.Name = "fraNumber";
			this.fraNumber.Size = new System.Drawing.Size(440, 90);
			this.fraNumber.TabIndex = 1;
			this.fraNumber.Text = "Select Report To Display";
			this.fraNumber.Visible = false;
			// 
			// cmbNumber
			// 
			this.cmbNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbNumber.Location = new System.Drawing.Point(20, 30);
			this.cmbNumber.Name = "cmbNumber";
			this.cmbNumber.Size = new System.Drawing.Size(400, 40);
			this.cmbNumber.TabIndex = 2;
			this.cmbNumber.DropDown += new System.EventHandler(this.cmbNumber_DropDown);
			this.cmbNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbNumber_KeyDown);
			// 
			// fraRecreateJournal
			// 
			this.fraRecreateJournal.Controls.Add(this.cmbJournal);
			this.fraRecreateJournal.Controls.Add(this.cmbMonth);
			this.fraRecreateJournal.Controls.Add(this.cmbRecreateJournal);
			this.fraRecreateJournal.Controls.Add(this.lblMonth);
			this.fraRecreateJournal.Controls.Add(this.lblJournal);
			this.fraRecreateJournal.Location = new System.Drawing.Point(30, 30);
			this.fraRecreateJournal.Name = "fraRecreateJournal";
			this.fraRecreateJournal.Size = new System.Drawing.Size(480, 206);
			this.fraRecreateJournal.TabIndex = 5;
			this.fraRecreateJournal.Text = "Select Close Out For Journal";
			this.fraRecreateJournal.Visible = false;
			// 
			// cmbJournal
			// 
			this.cmbJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cmbJournal.Location = new System.Drawing.Point(213, 90);
			this.cmbJournal.Name = "cmbJournal";
			this.cmbJournal.Size = new System.Drawing.Size(247, 40);
			this.cmbJournal.TabIndex = 8;
			this.cmbJournal.DropDown += new System.EventHandler(this.cmbJournal_DropDown);
			this.cmbJournal.Validating += new System.ComponentModel.CancelEventHandler(this.cmbJournal_Validating);
			this.cmbJournal.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbJournal_KeyDown);
			// 
			// cmbMonth
			// 
			this.cmbMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMonth.Location = new System.Drawing.Point(213, 150);
			this.cmbMonth.Name = "cmbMonth";
			this.cmbMonth.Size = new System.Drawing.Size(247, 40);
			this.cmbMonth.Sorted = true;
			this.cmbMonth.TabIndex = 7;
			this.cmbMonth.DropDown += new System.EventHandler(this.cmbMonth_DropDown);
			this.cmbMonth.Validating += new System.ComponentModel.CancelEventHandler(this.cmbMonth_Validating);
			this.cmbMonth.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMonth_KeyDown);
			// 
			// cmbRecreateJournal
			// 
			this.cmbRecreateJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cmbRecreateJournal.Location = new System.Drawing.Point(20, 30);
			this.cmbRecreateJournal.Name = "cmbRecreateJournal";
			this.cmbRecreateJournal.Size = new System.Drawing.Size(440, 40);
			this.cmbRecreateJournal.TabIndex = 6;
			this.cmbRecreateJournal.DropDown += new System.EventHandler(this.cmbRecreateJournal_DropDown);
			this.cmbRecreateJournal.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbRecreateJournal_KeyDown);
			// 
			// lblMonth
			// 
			this.lblMonth.Location = new System.Drawing.Point(20, 164);
			this.lblMonth.Name = "lblMonth";
			this.lblMonth.Size = new System.Drawing.Size(160, 22);
			this.lblMonth.TabIndex = 10;
			this.lblMonth.Text = "ACCOUNTING MONTH";
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(20, 104);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(148, 22);
			this.lblJournal.TabIndex = 9;
			this.lblJournal.Text = "JOURNAL NUMBER";
			// 
			// arView
			// 
			this.arView.Dock = Wisej.Web.DockStyle.Fill;
			this.arView.Location = new System.Drawing.Point(0, 40);
			this.arView.Name = "arView";
			this.arView.Size = new System.Drawing.Size(627, 459);
			this.arView.Visible = false;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileRecreate,
            this.mnuFileRecreateJournal,
            this.mnuFileChange});
			this.MainMenu1.Name = null;
			// 
			// mnuFileRecreate
			// 
			this.mnuFileRecreate.Index = 0;
			this.mnuFileRecreate.Name = "mnuFileRecreate";
			this.mnuFileRecreate.Text = "Recreate Report";
			this.mnuFileRecreate.Click += new System.EventHandler(this.mnuFileRecreate_Click);
			// 
			// mnuFileRecreateJournal
			// 
			this.mnuFileRecreateJournal.Index = 1;
			this.mnuFileRecreateJournal.Name = "mnuFileRecreateJournal";
			this.mnuFileRecreateJournal.Text = "Recreate Journal";
			this.mnuFileRecreateJournal.Click += new System.EventHandler(this.mnuFileRecreateJournal_Click);
			// 
			// mnuFileChange
			// 
			this.mnuFileChange.Index = 2;
			this.mnuFileChange.Name = "mnuFileChange";
			this.mnuFileChange.Text = "Change Report";
			this.mnuFileChange.Visible = false;
			this.mnuFileChange.Click += new System.EventHandler(this.mnuFileChange_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEMail,
            this.mnuFileExport,
            this.mnuFileSpacer,
            this.mnuFileSave,
            this.mnuFileSeperator,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
			// 
			// mnuEMail
			// 
			this.mnuEMail.Index = 0;
			this.mnuEMail.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEMalRTF,
            this.mnuEMailPDF});
			this.mnuEMail.Name = "mnuEMail";
			this.mnuEMail.Text = "E-mail";
			// 
			// mnuEMalRTF
			// 
			this.mnuEMalRTF.Index = 0;
			this.mnuEMalRTF.Name = "mnuEMalRTF";
			this.mnuEMalRTF.Text = "E-mail as Rich Text";
			this.mnuEMalRTF.Click += new System.EventHandler(this.mnuEMalRTF_Click);
			// 
			// mnuEMailPDF
			// 
			this.mnuEMailPDF.Index = 1;
			this.mnuEMailPDF.Name = "mnuEMailPDF";
			this.mnuEMailPDF.Text = "E-mail as PDF";
			this.mnuEMailPDF.Click += new System.EventHandler(this.mnuEMailPDF_Click);
			// 
			// mnuFileExport
			// 
			this.mnuFileExport.Index = 1;
			this.mnuFileExport.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileExportRTF,
            this.mnuFileExportPDF,
            this.mnuFileExportHTML,
            this.mnuFileExportExcel});
			this.mnuFileExport.Name = "mnuFileExport";
			this.mnuFileExport.Text = "Export";
			// 
			// mnuFileExportRTF
			// 
			this.mnuFileExportRTF.Index = 0;
			this.mnuFileExportRTF.Name = "mnuFileExportRTF";
			this.mnuFileExportRTF.Text = "Export as Rich Text";
			this.mnuFileExportRTF.Click += new System.EventHandler(this.mnuFileExportRTF_Click);
			// 
			// mnuFileExportPDF
			// 
			this.mnuFileExportPDF.Index = 1;
			this.mnuFileExportPDF.Name = "mnuFileExportPDF";
			this.mnuFileExportPDF.Text = "Export as PDF";
			this.mnuFileExportPDF.Click += new System.EventHandler(this.mnuFileExportPDF_Click);
			// 
			// mnuFileExportHTML
			// 
			this.mnuFileExportHTML.Index = 2;
			this.mnuFileExportHTML.Name = "mnuFileExportHTML";
			this.mnuFileExportHTML.Text = "Export as HTML";
			this.mnuFileExportHTML.Click += new System.EventHandler(this.mnuFileExportHTML_Click);
			// 
			// mnuFileExportExcel
			// 
			this.mnuFileExportExcel.Index = 3;
			this.mnuFileExportExcel.Name = "mnuFileExportExcel";
			this.mnuFileExportExcel.Text = "Export as Excel";
			this.mnuFileExportExcel.Click += new System.EventHandler(this.mnuFileExportExcel_Click);
			// 
			// mnuFileSpacer
			// 
			this.mnuFileSpacer.Index = 2;
			this.mnuFileSpacer.Name = "mnuFileSpacer";
			this.mnuFileSpacer.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 3;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Save & Continue";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 4;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 5;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// toolBar1
			// 
			this.toolBar1.AutoSize = false;
			this.toolBar1.BackColor = System.Drawing.Color.FromArgb(244, 247, 249);
			this.toolBar1.Buttons.AddRange(new Wisej.Web.ToolBarButton[] {
            this.toolBarButtonEmailPDF,
            this.toolBarButtonEmailRTF,
            this.toolBarButtonExportPDF,
            this.toolBarButtonExportRTF,
            this.toolBarButtonExportHTML,
            this.toolBarButtonExportExcel,
            this.toolBarButtonPrint});
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.Size = new System.Drawing.Size(627, 40);
			this.toolBar1.TabIndex = 0;
			this.toolBar1.TabStop = false;
			this.toolBar1.Visible = false;
			this.toolBar1.ButtonClick += new Wisej.Web.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
			// 
			// toolBarButtonEmailPDF
			// 
			this.toolBarButtonEmailPDF.ImageSource = "icon-report-email-pdf";
			this.toolBarButtonEmailPDF.Name = "toolBarButtonEmailPDF";
			this.toolBarButtonEmailPDF.ToolTipText = "Email as PDF";
			// 
			// toolBarButtonEmailRTF
			// 
			this.toolBarButtonEmailRTF.ImageSource = "icon-report-email-rtf";
			this.toolBarButtonEmailRTF.Name = "toolBarButtonEmailRTF";
			this.toolBarButtonEmailRTF.ToolTipText = "Email as RTF";
			// 
			// toolBarButtonExportPDF
			// 
			this.toolBarButtonExportPDF.ImageSource = "icon-report-export-pdf";
			this.toolBarButtonExportPDF.Name = "toolBarButtonExportPDF";
			this.toolBarButtonExportPDF.ToolTipText = "Export as PDF";
			// 
			// toolBarButtonExportRTF
			// 
			this.toolBarButtonExportRTF.ImageSource = "icon-report-export-rtf";
			this.toolBarButtonExportRTF.Name = "toolBarButtonExportRTF";
			this.toolBarButtonExportRTF.ToolTipText = "Export as RTF";
			// 
			// toolBarButtonExportHTML
			// 
			this.toolBarButtonExportHTML.ImageSource = "icon-report-export-html";
			this.toolBarButtonExportHTML.Name = "toolBarButtonExportHTML";
			this.toolBarButtonExportHTML.ToolTipText = "Export as HTML";
			// 
			// toolBarButtonExportExcel
			// 
			this.toolBarButtonExportExcel.ImageSource = "icon-report-export-excel";
			this.toolBarButtonExportExcel.Name = "toolBarButtonExportExcel";
			this.toolBarButtonExportExcel.ToolTipText = "Export as Excel";
			// 
			// toolBarButtonPrint
			// 
			this.toolBarButtonPrint.ImageSource = "icon-report-print";
			this.toolBarButtonPrint.Name = "toolBarButtonPrint";
			this.toolBarButtonPrint.ToolTipText = "Print";
			// 
			// btnFileSave
			// 
			this.btnFileSave.AppearanceKey = "acceptButton";
			this.btnFileSave.Location = new System.Drawing.Point(30, 400);
			this.btnFileSave.Name = "btnFileSave";
			this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileSave.Size = new System.Drawing.Size(180, 48);
			this.btnFileSave.TabIndex = 6;
			this.btnFileSave.Text = "Save & Continue";
			this.btnFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmRPTViewer
			// 
			this.ClientSize = new System.Drawing.Size(627, 499);
			this.Controls.Add(this.btnFileSave);
			this.Controls.Add(this.fraRecreate);
			this.Controls.Add(this.fraNumber);
			this.Controls.Add(this.fraRecreateJournal);
			this.Controls.Add(this.arView);
			this.Controls.Add(this.toolBar1);
			this.FillColor = 16777215;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmRPTViewer";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Recreate Daily Audit Report";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmRPTViewer_Load);
			this.Activated += new System.EventHandler(this.frmRPTViewer_Activated);
			this.Resize += new System.EventHandler(this.frmRPTViewer_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRPTViewer_KeyPress);
			((System.ComponentModel.ISupportInitialize)(this.fraRecreate)).EndInit();
			this.fraRecreate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraNumber)).EndInit();
			this.fraNumber.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRecreateJournal)).EndInit();
			this.fraRecreateJournal.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
