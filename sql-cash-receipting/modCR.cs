﻿//Fecher vbPorter - Version 1.0.0.27
#define CURRENTMODULE_1
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using Wisej.Core;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using Autofac;
using Microsoft.Ajax.Utilities;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.Enums;
using TWSharedLibrary;

namespace TWCR0000
{
	//FC:FINAL:AM: renamed modCR to modGlobal
	public class modGlobal
	{
		// this is to pass the answer from a question form back to form asking
		/*! #const int CURRENTMODULE = 1; */
public const string EXECONST = "CR";
		// these arrays are for reporting the Budgetary information and creating Journal Entries in the BD Database
		public struct FundMonies
		{
			public double CashYY;
			public double CashNY;
			public string Account;
			public double Amount;
			public double ExpControl;
			public double RevControl;
			public string Departments;
			public bool Dirty;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public FundMonies(int unusedParam)
			{
				this.CashNY = 0;
				this.CashYY = 0;
				this.Account = string.Empty;
				this.Amount = 0;
				this.ExpControl = 0;
				this.RevControl = 0;
				this.Departments = string.Empty;
				this.Dirty = false;
			}
		};
		// *****************************************************************************************
		// Security Function Constants                                                            '*
		public const int CRSECURITYMAINENTRY = 1;
		// *
		public const int CRSECURITYRECEIPTINPUT = 2;
		// *
		public const int CRSECURITYREDISPLAYLASTRECEIPT = 3;
		// *
		public const int CRSECURITYDAILYRECEIPTAUDIT = 4;
		// *
		public const int CRSECURITYPERFORMDAILYAUDIT = 16;
		// *
		public const int CRSECURITYREPRINTDAILYAUDIT = 5;
		// *
		public const int CRSECURITYFILEMAINETENACE = 6;
		// *
		public const int CRSECURITYSETUPRECEIPTTYPES = 7;
		// *
		public const int CRSECURITYSEARCHROUTINES = 8;
		// *
		public const int CRSECURITYOPENCASHDRAWER = 9;
		// *
		public const int CRSECURITYENDOFYEAR = 10;
		// *
		public const int CRSECURITYREPRINTRECEIPT = 11;
		// *
		public const int CRSECURITYTYPELISTING = 12;
		// *
		public const int CRSECURITYMVR3LISTING = 13;
		// *
		public const int CRSECURITYCUSTOMIZE = 14;
		// *
		public const int CRSECURITYRESETRECEIPTNUMBER = 15;
		// *
		public const int CRSECURITYPASSWORDPROTECTAUDIT = 17;
		// *
		public const int CRSECURITYCASHDRAWERCODES = 18;
		// *
		public const int CRSECURITYCHANGECOMMENTS = 19;
		// *
		public const int CRSECURITYREPORTALIGNMENT = 20;
		// *
		public const int CRSECURITYTELLERDEFAULTS = 21;
		// *
		public const int CRSECURITYBANKMAINTENANCE = 22;
		// *
		public const int CRSECURITYREMOVELIENMATNOTICE = 23;
		// kk01282015 Not sure why this is CR '*
		public const int CRSECURITYAUTOPOSTJOURNAL = 24;
		// kk01282015 trocrs-32               '*
		// *
		public const int AUTOPOSTBILLINGJOURNAL = 24;
		// kk07152014 For AR                  '*
		// *****************************************************************************************
		public const string DEFAULTDATABASE = "TWCR0000.VB1";

		public static void Main(IContainer container)
		{
			int intTest = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTest = "";
				string strTemp = "";
				string strTempSelect = "";
				// kk04082015 troges-39  Change to single config file
				if (!modGlobalFunctions.LoadSQLConfig("TWCR0000.VB1"))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				clsDRWrapper rsMain = new clsDRWrapper();
				clsDRWrapper rsBank = new clsDRWrapper();

				Statics.strDbRE = rsMain.Get_GetFullDBName(modExtraModules.strREDatabase) + ".dbo.";
				Statics.strDbPP = rsMain.Get_GetFullDBName(modExtraModules.strPPDatabase) + ".dbo.";
				Statics.strDbUT = rsMain.Get_GetFullDBName(modExtraModules.strUTDatabase) + ".dbo.";
				Statics.strDbGNC = rsMain.Get_GetFullDBName("CentralData") + ".dbo.";
				Statics.strDbGNS = rsMain.Get_GetFullDBName("SystemSettings") + ".dbo.";
				Statics.strDbCP = rsMain.Get_GetFullDBName("CentralParties") + ".dbo.";
				intTest = 1;

				intTest = 2;
				modGlobalFunctions.GetGlobalRegistrySetting();
				modReplaceWorkFiles.EntryFlagFile();
				// Write #12, "01 - " & Now & "    - Global Registry"
				intTest = 3;
				// use the security class to make sure that the user is allowed into each option
				modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
				modGlobalConstants.Statics.clsSecurityClass.Init("CR");
				modGlobalConstants.Statics.gstrUserID = modGlobalConstants.Statics.clsSecurityClass.Get_OpID();
				// save the user name
				// Write #12, "02 - " & Now & "    - Security"
				intTest = 4;
				modGlobalFunctions.LoadTRIOColors();
				// this will set all of the color variables
				// Write #12, "03 - " & Now & "    - Colors"
				intTest = 5;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking Database", true, 16);
				frmWait.InstancePtr.Top = FCConvert.ToInt32((FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2.0);
				frmWait.InstancePtr.Left = FCConvert.ToInt32((FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2.0);
				frmWait.InstancePtr.Visible = true;
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Checking Database";
				frmWait.InstancePtr.Refresh();
				// XXX SQL Server Conversion
				//         CheckDatabaseStructure();
				// Write #12, "04 - " & Now & "    - Wait Screen"
				intTest = 6;
				if (NewVersion())
				{
					// this will check for a new version of the program and update the database accordingly
					// MAL@20080514: Add new registry entries for audit report orientation
					// Tracker Reference: 13318
					//Application.DoEvents();
					CheckOrientationRegistryEntries();
					frmWait.InstancePtr.IncrementProgress();
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Loading Settings";
                    frmWait.InstancePtr.Refresh();
                    // Write #12, "06 - " & Now & "    - Compact Date Check"
                    intTest = 7;
					// open the general entry database
					if (rsMain.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings"))
					{
						// MuniName = Trim(.Fields("MuniName"))
						// Write #12, "07 - " & Now
						frmWait.InstancePtr.IncrementProgress();
						modReplaceWorkFiles.GetGlobalVariables();
						
						// Write #12, "09 - " & Now
						frmWait.InstancePtr.IncrementProgress();
						// use this only when debugging!!!
						// gstrTrioSDrive = "C:\VBTRIO\PACKAGE\"
						// gstrTrioSDrive = Trim(.Fields("TrioSDrive"))
						modGlobalConstants.Statics.gstrCityTown = FCConvert.ToString(rsMain.Get_Fields_String("CityTown"));
						Statics.gboolMultipleTowns = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("UseMultipleTown"));
						frmWait.InstancePtr.IncrementProgress();
						intTest = 8;
						//Application.DoEvents();
						// Write #12, "10 - " & Now
						CheckMultipleTownTypes();
						// this will open the type table for use
						Statics.grsType.OpenRecordset("SELECT * FROM Type", DEFAULTDATABASE);
						frmWait.InstancePtr.IncrementProgress();
						// Write #12, "11 - " & Now
						intTest = 9;
						// open the modules table
						modGlobalFunctions.UpdateUsedModules();
						// this will update the Module Initials variables (ie. gboolBD, gboolMV)
						// Write #12, "12 - " & Now
						if (!modGlobalConstants.Statics.gboolCR)
						{
							MessageBox.Show("Cash Receipting has expired.  Please call TRIO!", "Module Expired", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							Application.Exit();
						}
						// Write #12, "13 - " & Now
						if (modGlobalConstants.Statics.gboolBD)
						{
							// if the user has the BD module, then connect to the database so that a slowdown does not occur
							intTest = 911;
							strTemp = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("UseProjectNumber"));
							if (strTemp == "Y")
							{
								Statics.ProjectFlag = true;
							}
							else
							{
								Statics.ProjectFlag = false;
							}
						}
						frmWait.InstancePtr.IncrementProgress();
						// Write #12, "14 - " & Now
						intTest = 10;
						// open the security table
						// .OpenRecordset "SELECT * FROM Security"
						rsMain.Reset();
						// open the cash receipting table and get the default values
						rsMain.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
						if (rsMain.EndOfFile())
						{
							// if there is no record
							rsMain.AddNew();
							// then add one and try looking at it
							rsMain.Update();
							//Application.DoEvents();
							rsMain.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
						}
						// Write #12, "15 - " & Now
						frmWait.InstancePtr.IncrementProgress();
						intTest = 11;
                        var settingsController = new cSettingsController();
                        StaticSettings.gGlobalCashReceiptSettings.ShowPaymentPortalCodeOnPaymentScreen =  settingsController.GetSettingValue("ShowServiceCodeOnPaymentScreen", "CashReceipts", "", "", "") == "true";

						if (!rsMain.EndOfFile())
						{
							// gstrAuditReportFormat = .Fields("AuditReportFormat")       'MAL@20080609 ; Tracker Reference: 14038
							intTest = 12;
							frmWait.InstancePtr.IncrementProgress();
							// Write #12, "16 - " & Now
							// get the wide no margin flag
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideNoMargin", ref strTemp);
							Statics.gboolWideReceiptNoMargin = FCConvert.CBool(strTemp == "TRUE");
							StaticSettings.gGlobalCashReceiptSettings.WideReceiptNoMargins =
								Statics.gboolWideReceiptNoMargin;
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptLength", ref strTemp);
							Statics.gintReceiptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							StaticSettings.gGlobalCashReceiptSettings.ReceiptLength = Statics.gintReceiptLength;
							intTest = 13;
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRExtraLength", ref strTemp);
							Statics.gintExtraLines = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							StaticSettings.gGlobalCashReceiptSettings.ReceiptExtraLines = Statics.gintExtraLines;
							// get the compacted length from the registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideCompact", ref strTemp);
							Statics.gboolRecieptCompacted = FCConvert.CBool(Strings.UCase(strTemp) == "TRUE");
							StaticSettings.gGlobalCashReceiptSettings.CompactedReceipt = Statics.gboolRecieptCompacted;
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideThin", ref strTemp);
							Statics.gboolRecieptThin = FCConvert.CBool(Strings.UCase(strTemp) == "TRUE");
							StaticSettings.gGlobalCashReceiptSettings.ThinReceipt = Statics.gboolRecieptThin;
							// Write #12, "17 - " & Now
							frmWait.InstancePtr.IncrementProgress();
							intTest = 14;
							Statics.gstrTopComments = FCConvert.ToString(rsMain.Get_Fields_String("TopComment"));
							StaticSettings.gGlobalCashReceiptSettings.TopComment = Statics.gstrTopComments;
							intTest = 15;
							Statics.gstrBottomComments = FCConvert.ToString(rsMain.Get_Fields_String("BottomComment"));
							StaticSettings.gGlobalCashReceiptSettings.BottomComment = Statics.gstrBottomComments;
							intTest = 16;
							frmWait.InstancePtr.IncrementProgress();
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDConnected", ref strTemp);
							Statics.gboolCDConnected = FCConvert.CBool(strTemp == "TRUE");
							StaticSettings.gGlobalCashReceiptSettings.CashDrawerConnected = Statics.gboolCDConnected;
							// gboolCDConnected = .Fields("CDConnected")
							intTest = 17;
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDCode", ref strTemp);
							Statics.gstrCDCode = Strings.Trim(strTemp);
							StaticSettings.gGlobalCashReceiptSettings.CashDrawerCode = Statics.gstrCDCode;
							// gstrCDCode = .Fields("CDCode")
							intTest = 18;
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRRepeatCount", ref strTemp);
							Statics.gintRepeatCount = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							StaticSettings.gGlobalCashReceiptSettings.CashDrawerRepeatCount = Statics.gintRepeatCount;
							// gintRepeatCount = .Fields("RepeatCount")
							intTest = 19;
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDPortID", ref strTemp);
							Statics.gstrCDPortID = Strings.Trim(strTemp);
							StaticSettings.gGlobalCashReceiptSettings.CashDrawerPortId = Statics.gstrCDPortID;
							// gstrCDPortID = .Fields("PortID")
							intTest = 20;
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRNarrowReceipt", ref strTemp);
							Statics.gboolNarrowReceipt = FCConvert.CBool(strTemp == "TRUE");
							StaticSettings.gGlobalCashReceiptSettings.NarrowReceipt = Statics.gboolNarrowReceipt;
							// gboolNarrowReceipt = .Fields("NarrowReceipt")
							intTest = 21;
							frmWait.InstancePtr.IncrementProgress();
							// Write #12, "18 - " & Now
							if (modGlobalConstants.Statics.gboolBD)
							{
								Statics.gboolAutoCheckRecEntry = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("AutoCheckRecEntry"));
							}
							else
							{
								Statics.gboolAutoCheckRecEntry = false;
							}
							// Write #12, "19 - " & Now
							Statics.gstrPrintSigLine = FCConvert.ToString(rsMain.Get_Fields_String("PrintSigLine"));
							StaticSettings.gGlobalCashReceiptSettings.PrintSignature = Statics.gstrPrintSigLine == "A"
								?
								PrintSignatureOption.Yes
								: Statics.gstrPrintSigLine == "P"
									? PrintSignatureOption.OnlyOnCreditCardPayments
									: PrintSignatureOption.No;
							intTest = 22;
							Statics.gstrTeller = FCConvert.ToString(rsMain.Get_Fields_String("TellerIDUsed"));
                            switch (Statics.gstrTeller)
                            {
                                case "M":
                                    StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage =
                                        TellerIDUsage.RequireEachTime;
                                    break;
                                case "Y":
                                    StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage =
                                        TellerIDUsage.RequireButDefault;
                                    break;
                                case "P":
                                    StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage =
                                        TellerIDUsage.RequirePassword;
                                    break;
                                case "N":
                                    StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage = TellerIDUsage.NotRequired;
                                    break;
                            }
                            intTest = 23;
							Statics.gboolTellerReportWithAudit = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("TellerReportWithAudit"));
							intTest = 24;
							Statics.gboolTellerCheckReportWithAudit = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("gboolTellerCheckReportWithAudit"));
							intTest = 25;
							Statics.gboolSkipPageForTeller = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("SkipPageForTeller"));
							Statics.gboolCashTellersOutSeperately = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("CashTellersOutSeperately"));
							intTest = 26;
							Statics.gboolAuditSeqByType = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("AuditSeqByRecNumber"));
							intTest = 27;
							// glngCurrentYear = Val(.Fields("CurrentAccountYear"))
							intTest = 28;
							// gstrCurrentYearAccount = .Fields("CurrentYearAccount")
							intTest = 29;
							Statics.gboolPasswordProtectAudit = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("PasswordProtectAudit"));
							Statics.gboolClearPaidBy = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("ClearPaidBy"));
							// Tracker Reference: 17448
							Statics.gstrClearPaidByOption = FCConvert.ToString(rsMain.Get_Fields_String("ClearPaidByOption"));
							modGlobalConstants.Statics.gblnShowControlFields = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("ShowControlFields"));
							StaticSettings.gGlobalCashReceiptSettings.ShowControlFields =
								modGlobalConstants.Statics.gblnShowControlFields;
							modGlobalConstants.Statics.gblnSortTypeAlpha = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("SortReceiptTypeAlpha"));
							// Tracker Reference: 16641
							Statics.gblnEPaymentActivated = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("EPymtActivated"));
							if (Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("EPaymentClientIDVISA"))) == "")
							{
								Statics.gblnNoVISANONTaxPayments = true;
							}
							else
							{
								Statics.gblnNoVISANONTaxPayments = false;
							}
							if (Strings.Trim(FCConvert.ToString(rsMain.Get_Fields_String("EPaymentClientID"))) == "")
							{
								Statics.gblnNoVISAPayments = true;
							}
							else
							{
								Statics.gblnNoVISAPayments = false;
							}
							Statics.gstrEPaymentPortal = FCConvert.ToString(rsMain.Get_Fields_String("EPaymentPortal"));
							
                            var paymentPortalSetting = settingsController.GetSettingValue("ElectronicPaymentPortal", "CashReceipts", "", "", "SystemSettings") ;
                            switch (paymentPortalSetting.ToLower())
                            {
								case "payport":
                                    StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType =
                                        ElectronicPaymentPortalOption.PayPort;
                                    Statics.gstrEPaymentPortal = "E";
                                    Statics.gblnEPaymentActivated = true;
                                    break;
								case "":
                                    StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType = Statics.gstrEPaymentPortal == "E" ? ElectronicPaymentPortalOption.PayPort : ElectronicPaymentPortalOption.None;
                                    Statics.gblnEPaymentActivated = StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType != ElectronicPaymentPortalOption.None;
                                    switch (StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType)
                                    {
										case ElectronicPaymentPortalOption.PayPort:
                                            settingsController.SaveSetting("PayPort", "ElectronicPaymentPortal", "CashReceipts", "", "", "SystemSettings");
											break;
										default:
											settingsController.SaveSetting("None","ElectronicPaymentPortal","CashReceipts","","","SystemSettings");
                                            break;
                                    }
									break;
								default:
                                    StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType =
                                        ElectronicPaymentPortalOption.None;
                                    Statics.gstrEPaymentPortal = "N";
                                    Statics.gblnEPaymentActivated = false;
									break;
                            }
							StaticSettings.gGlobalCashReceiptSettings.EPaymentInTestMode =
                                rsMain.Get_Fields_Boolean("EPaymentTestMode");
                            StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeMethod = rsMain.Get_Fields_String("ConvenienceFeeMethod");
							StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeGLAccount = rsMain.Get_Fields_String("ConvenienceFeeGLAccount");
							StaticSettings.gGlobalCashReceiptSettings.SeparateCreditCardGLAccount = rsMain.Get_Fields_String("SeperateCreditCardGLAccount");
                            StaticSettings.gGlobalCashReceiptSettings.UseSeparateCreditCardGLAccount =
                                rsMain.Get_Fields_Boolean("UseSeperateCreditCardGLAccount");
							StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeFlatAmount = rsMain.Get_Fields_Decimal("ConvenienceFlatAmount");
							StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeePercentage = rsMain.Get_Fields_Decimal("ConveniencePercentage");
							StaticSettings.gGlobalCashReceiptSettings.EPaymentClientID = rsMain.Get_Fields_String("EPaymentClientID");
                            StaticSettings.gGlobalCashReceiptSettings.PaymentResponseURL = Wisej.Web.Application.Uri.GetComponents(UriComponents.SchemeAndServer,
                                                                                               UriFormat.UriEscaped) + @"/";
							SetupSystemDefinedCCTypes();
							Statics.gblnAcceptEChecks = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("AcceptEChecks"));
							Statics.gblnEPymtTestMode = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("EPaymentTestMode"));
							Statics.gblnPrintExtraReceipt = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("PrintExtraReceipt"));
							StaticSettings.gGlobalCashReceiptSettings.PrintExtraReceipt = Statics.gblnPrintExtraReceipt;
							// Tracker Reference: 14953
							Statics.gblnPrintCCReport = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("PrintCreditCardReport"));
							Statics.gintCCReportOrder = FCConvert.ToInt32(rsMain.Get_Fields_Int16("CreditCardReportOrder"));
							// Tracker Reference: 17676
							Statics.gblnReceiptPrinterPrompt = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("ReceiptPrinterPrompt"));
							intTest = 30;
							frmWait.InstancePtr.IncrementProgress();
							// Write #12, "20 - " & Now
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRSP200Printer", ref strTemp);
							Statics.gboolSP200Printer = FCConvert.CBool(strTemp == "TRUE");
							StaticSettings.gGlobalCashReceiptSettings.SP200Printer = Statics.gboolSP200Printer;
							// gboolSP200Printer = CBool(.Fields("SP200Printer"))
							intTest = 31;
							// get from registry
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptOffset", ref strTemp);
							Statics.gintReceiptOffSet = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							StaticSettings.gGlobalCashReceiptSettings.ReceiptOffset = Statics.gintReceiptOffSet;
							// gintReceiptOffSet = Val(.Fields("ReceiptPrinterOffset"))
							intTest = 32;
							frmWait.InstancePtr.IncrementProgress();
							Statics.gboolCloseMVPeriod = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("CloseMVPeriod"));
							intTest = 33;
							Statics.gboolDailyAuditByAlpha = FCConvert.CBool(rsMain.Get_Fields_Boolean("DailyAuditByAlpha"));
							intTest = 34;
							Statics.gboolAllowPartialAudit = FCConvert.CBool(rsMain.Get_Fields_Boolean("AllowPartialAudit"));
							intTest = 134;
							Statics.gintCheckReportOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMain.Get_Fields_Int16("CheckReportOrder"))));
							intTest = 135;
							if (Statics.gboolPasswordProtectAudit)
							{
								intTest = 136;
								Statics.gstrAuditPassword = modEncrypt.ToggleEncryptCode(rsMain.Get_Fields_String("AuditPassword"));
							}
							else
							{
								Statics.gstrAuditPassword = "";
							}
							// Write #12, "21 - " & Now
							Statics.gboolUseUSLExport = FCConvert.CBool(rsMain.Get_Fields_Boolean("USLExport"));
							intTest = 137;
							// Write #12, "100 - " & Now
							// this is the adjustment for the receipts
							// If Val(.Fields("gdblReceiptLenAdjust")) <> 0 Then
							// gdblReceiptLenAdjust = .Fields("gdblReceiptLenAdjust")
							// Else
							// gdblReceiptLenAdjust = 0
							// End If
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "AuditMarginAdjustmentTop", ref strTemp);
							if (Conversion.Val(strTemp) != 0)
							{
								Statics.gdblAuditMarginAdjustmentTop = FCConvert.ToDouble(strTemp);
							}
							else
							{
								Statics.gdblAuditMarginAdjustmentTop = 0;
							}
							// Write #12, "101 - " & Now
							Statics.gboolUseUSLExport = FCConvert.ToBoolean(rsMain.Get_Fields_Boolean("USLExport"));
							// Write #12, "102 - " & Now
							frmWait.InstancePtr.IncrementProgress();
							// Write #12, "103 - " & Now
						}
						else
						{
							frmWait.InstancePtr.Unload();
							MessageBox.Show("Default Cash receipting values were not loaded.  Please restart your TRIO Cash Receipting.", "Missing Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						// Write #12, "30 - " & Now
						intTest = 36;
						modNewAccountBox.GetFormats();
						if (modGlobalConstants.Statics.gboolBD)
						{
							if (!modValidateAccount.SetBDFormats())
							{
								frmWait.InstancePtr.Unload();
								MessageBox.Show("Budgetary Account Formats were not loaded, please go to File Maintenance to check your settings.", "Account Format Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							if (!GetBDAccounts())
							{
								frmWait.InstancePtr.Unload();
								MessageBox.Show("Generic Budgetary Account numbers not found.  Please make sure that they are set up correctly.", "Account Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						//Application.DoEvents();
						frmWait.InstancePtr.IncrementProgress();
						// Write #12, "40 - " & Now
						intTest = 37;
						if (modGlobalConstants.Statics.gboolCL)
						{
							if (!setupCL())
							{
								frmWait.InstancePtr.Unload();
								MessageBox.Show("RE/PP Collections global information is not loaded, please go to File Maintenance to check your settings.", "RE/PP Variable Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						if (modGlobalConstants.Statics.gboolUT)
						{
							modUTCalculations.UTSetup();
						}
						// MAL@20080611: Add support for AR
						// Tracker Reference: 14057
						if (modGlobalConstants.Statics.gboolAR)
						{
							modARCalculations.ARSetup();
						}
						frmWait.InstancePtr.IncrementProgress();
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("An error occured while opening System Settings.", "Open Database Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					// Write #12, "50 - " & Now
					intTest = 38;
					Statics.gboolPendingTransactions = CheckForPendingTransactions();
					// this will check to see if there are any pending transactions in the db already
					intTest = 39;
					frmWait.InstancePtr.Unload();
					// this will create query defs to show the new and old information that will be used with some of the sub reports
					// MAL@20080611: Add new AR Bill Type Field
					// Tracker Reference: 14057
					// MAL@20081105: Add Actual System Time Field
					// Tracker Reference: 13124
					strTempSelect = "LastYearArchive.ID, DailyCloseOut, ActualSystemDate, CONVERT(VARCHAR(8),ActualSystemDate,108) as ActualSystemTime, LastYearReceipt.ReceiptNumber AS ReceiptNumber, ReceiptType, ReceiptKey, AccountNumber, Ref, Control1, Control2, Control3, LastYearReceipt.TellerID AS TellerID, Name, LastYearArchive.LastYearArchiveDate AS RecordedTransactionDate, LastYearReceipt.LastYearReceiptDate AS [Date], DefaultCashAccount, Change, CashAmount, CheckAmount, CardAmount, Amount1, Amount2, Amount3, Amount4, Amount5, Amount6, Account1, Account2, Account3, Account4, Account5, Account6, Comment, PaidBy, DefaultAccount, ARBillType, LastYearArchive.ConvenienceFeeGLAccount as ConvenienceFeeGLAccount, LastYearArchive.ConvenienceFee as ConvenienceFee, CashPaidAmount, CardPaidAmount, CheckPaidAmount";
					// kk01092014        strOldArchiveSQL = "SELECT " & strTempSelect & " FROM LastYearArchive INNER JOIN LastYearReceipt ON  (LastYearArchive.ReceiptNumber = LastYearReceipt.ReceiptKey AND Year(LastYearArchive.ActualSystemDate) = Year(LastYearReceipt.LastYearReceiptDate) AND Month(LastYearArchive.ActualSystemDate) = Month(LastYearReceipt.LastYearReceiptDate) AND Day(LastYearArchive.ActualSystemDate) = Day(LastYearReceipt.LastYearReceiptDate))"
					Statics.strOldArchiveSQL = "SELECT " + strTempSelect + " FROM LastYearArchive INNER JOIN LastYearReceipt ON  (LastYearArchive.ReceiptNumber = LastYearReceipt.ReceiptKey AND Convert(Date,LastYearArchive.ActualSystemDate,101) = Convert(Date,LastYearReceipt.LastYearReceiptDate,101))";
					strTempSelect = "Archive.ID, DailyCloseOut, ActualSystemDate, CONVERT(VARCHAR(8),ActualSystemDate,108) as ActualSystemTime, Receipt.ReceiptNumber AS ReceiptNumber, ReceiptType, ReceiptKey, AccountNumber, Ref, Control1, Control2, Control3, Receipt.TellerID AS TellerID, Name, Archive.ArchiveDate AS RecordedTransactionDate, Receipt.ReceiptDate AS [Date], DefaultCashAccount, Change, CashAmount, CheckAmount, CardAmount, Amount1, Amount2, Amount3, Amount4, Amount5, Amount6, Account1, Account2, Account3, Account4, Account5, Account6, Comment, PaidBy, DefaultAccount, ARBillType, Archive.ConvenienceFeeGLAccount as ConvenienceFeeGLAccount, Archive.ConvenienceFee as ConvenienceFee, CashPaidAmount, CardPaidAmount, CheckPaidAmount";
					// kk01092014        strNewArchiveSQL = "SELECT " & strTempSelect & " FROM Archive INNER JOIN Receipt ON (Archive.ReceiptNumber = Receipt.ReceiptKey AND Year(Archive.ActualSystemDate) = Year(Receipt.ReceiptDate) AND Month(Archive.ActualSystemDate) = Month(Receipt.ReceiptDate) AND Day(Archive.ActualSystemDate) = Day(Receipt.ReceiptDate))"
					Statics.strNewArchiveSQL = "SELECT " + strTempSelect + " FROM Archive INNER JOIN Receipt ON (Archive.ReceiptNumber = Receipt.ReceiptKey AND Convert(Date,Archive.ActualSystemDate,101) = Convert(Date,Receipt.ReceiptDate,101))";
					Statics.strCurrentDailyAuditSQL = "SELECT DailyCloseOut FROM Archive WHERE ( ISNULL(DailyCloseOut,0) = 0 OR DailyCloseOut = -100 )";
					if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) == "\\")
					{
						App.HelpFile = FCFileSystem.Statics.UserDataFolder + "TWCR0000.HLP";
					}
					else
					{
						App.HelpFile = FCFileSystem.Statics.UserDataFolder + "\\" + "TWCR0000.HLP";
					}
					// Write #12, "55 - " & Now
					intTest = 50;
					modReplaceWorkFiles.EntryFlagFile(true, "  ");
					intTest = 51;
					MDIParent.InstancePtr.Init(container);
				}
				else
				{
					// new version failed and I do not want the user to enter
					MessageBox.Show("Fields were not added to the database.  Make sure that everyone is out of TRIO and try again.", "Error Adding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Main CR Error - " + FCConvert.ToString(intTest), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				FCFileSystem.FileClose(12);
				Application.Exit();
			}
		}

		public static bool NewVersion_2(bool boolOverride)
		{
			return NewVersion(boolOverride);
		}

		public static bool NewVersion(bool boolOverride = false)
		{
			bool NewVersion = false;

			try
			{
				bool boolUpdate;
				int lngMajor;
				int lngMinor;
				int lngRevsion;
				string strRevision = "";
				int lngTemp;
				string strTypeTemp = "";
				string strTempSelect = "";
				bool boolNoEntryError = false;
				bool boolUpdateAuditPrintOptions;

				if (boolOverride)
				{
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking Database", true, 100);
				}
				else
				{
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Updating Database";
                    frmWait.InstancePtr.Refresh();
                }

				var cddb = new cCashReceiptsDB();
				cddb.CheckVersion();

				frmWait.InstancePtr.IncrementProgress();

				NewVersion = !boolNoEntryError;
				frmWait.InstancePtr.Unload();
				if (!boolNoEntryError)
				{
					if (boolOverride)
					{
						MessageBox.Show("Check database structure successful.", "Check Database Structure", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}

                var sSet = new cSystemSettings();
                sSet.CheckVersion();
                var reDb = new cRealEstateDB();
                reDb.CheckVersion();
                var ppDb = new cPPDatabase();
                ppDb.CheckVersion();
                var cdb = new cCollectionsDB();
                cdb.CheckVersion();
                var ardb = new cAccountsReceivableDB();
                ardb.CheckVersion();
                var ubdb = new cUtilityBillingDB();
                ubdb.CheckVersion();
                
				return NewVersion;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + "." + "\r\n" + "Please make sure that everyone is out of the database and run File Maintenance > Check Database Structure.", "Update Cash Receipting Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return NewVersion;
			}
		}

		

		public static void SetupSystemDefinedCCTypes()
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM CCType", "TWCR0000.vb1");
			if (rs.FindFirstRecord2("Type,SystemDefined", "American Express,True", ","))
			{
				rs.Edit();
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("SystemDefined", true);
			rs.Set_Fields("Type", "American Express");
			rs.Set_Fields("IsDebit", false);
			rs.Set_Fields("IsVISA", false);
			rs.Update();
			if (rs.FindFirstRecord2("Type,SystemDefined", "Discover,True", ","))
			{
				rs.Edit();
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("SystemDefined", true);
			rs.Set_Fields("Type", "Discover");
			rs.Set_Fields("IsDebit", false);
			rs.Set_Fields("IsVISA", false);
			rs.Update();
			if (rs.FindFirstRecord2("Type,SystemDefined", "MasterCard,True", ","))
			{
				rs.Edit();
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("SystemDefined", true);
			rs.Set_Fields("Type", "MasterCard");
			rs.Set_Fields("IsDebit", false);
			rs.Set_Fields("IsVISA", false);
			rs.Update();
			if (rs.FindFirstRecord2("Type,SystemDefined", "VISA,True", ","))
			{
				rs.Edit();
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("SystemDefined", true);
			rs.Set_Fields("Type", "VISA");
			rs.Set_Fields("IsDebit", false);
			rs.Set_Fields("IsVISA", true);
			rs.Update();
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmReceiptInput, frmTypeSetup, frmDateChange, frmLoadValidAccounts, frmGetMasterAccount, frmUTDateChange, frmARGetMasterAccount, frmARDateChange, frmCentralPartySearch, frmEditCentralParties, frmSelectBankNumber, frmSelectPostingJournal)
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		public static bool IsFormLoaded_2(string strFormName)
		{
			return IsFormLoaded(ref strFormName);
		}

		public static bool IsFormLoaded(ref string strFormName)
		{
			bool IsFormLoaded = false;
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == strFormName)
				{
					IsFormLoaded = true;
					return IsFormLoaded;
				}
			}
			return IsFormLoaded;
		}

		public static short ValidateTellerID_21(string strID, string strPass = "")
		{
			return ValidateTellerID(strID, 0, strPass);
		}

		public static short ValidateTellerID(string strID, short intSecLvl = 0, string strPass = "")
		{
			short ValidateTellerID = 0;
			// checks the teller table and returns true if the TellerID is found otherwise false
			clsDRWrapper rsVal = new clsDRWrapper();
			rsVal.DefaultDB = modExtraModules.strCRDatabase;
			rsVal.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + Strings.UCase(strID) + "'", "SystemSettings");
			if (rsVal.BeginningOfFile() != true && rsVal.EndOfFile() != true)
			{
				if (!FCConvert.ToBoolean(rsVal.Get_Fields_Boolean("LoggedIn")))
				{
					if (Statics.gstrTeller == "P")
					{
						if (Strings.UCase(strPass) == modEncrypt.ToggleEncryptCode(rsVal.Get_Fields_String("Password")))
						{
							ValidateTellerID = 0;
							// fine to log in
						}
						else
						{
							ValidateTellerID = 3;
							// wrong password
						}
					}
					else
					{
						ValidateTellerID = 0;
						// fine to log in
					}
				}
				else
				{
					ValidateTellerID = 1;
					// already logged in
				}
			}
			else
			{
				ValidateTellerID = 2;
				// no teller
			}
			return ValidateTellerID;
		}

		public static void TellerLogin_6(string strID, bool boolLogin)
		{
			TellerLogin(ref strID, ref boolLogin);
		}

		public static void TellerLogin(ref string strID, ref bool boolLogin)
		{
			// this will log the teller in or out
			clsDRWrapper rsVal = new clsDRWrapper();
			rsVal.DefaultDB = modExtraModules.strCRDatabase;
			rsVal.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + Strings.UCase(strID) + "'", "SystemSettings");
			if (rsVal.BeginningOfFile() != true && rsVal.EndOfFile() != true)
			{
				if (rsVal.Edit())
				{
					if (boolLogin)
					{
						rsVal.Set_Fields("LoggedIn", true);
					}
					else
					{
						rsVal.Set_Fields("LoggedIn", false);
					}
					rsVal.Update(false);
				}
			}
			else
			{
				if (boolLogin)
				{
					MessageBox.Show("Error logging " + strID + " into the system.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					MessageBox.Show("Error logging " + strID + " out of the system.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		public static void LogOutTeller_2(string strID)
		{
			LogOutTeller(ref strID);
		}

		public static void LogOutTeller(ref string strID)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTID = new clsDRWrapper();
				if (strID != "")
				{
					rsTID.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + strID + "'", "SystemSettings");
					if (rsTID.EndOfFile() != true && rsTID.BeginningOfFile() != true)
					{
						rsTID.Set_Fields("LoggedIn", false);
						rsTID.Update(false);
					}
				}
				rsTID.Reset();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Log Out Teller Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intTotalAccounts As short	OnWriteFCConvert.ToInt32(
		public static bool GroupInformation(ref int intTotalAccounts, ref double dblTotalOutstandingBalance, ref int lngGroupNumber)
		{
			bool GroupInformation = false;
			// this function will return true if a group number has been found and will pass the number of accounts and balance back in the parameters
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsGroup = new clsDRWrapper();
				clsDRWrapper rsGrpMst = new clsDRWrapper();
				string strSQL = "";
				int intTemp = 0;
				bool boolCL = false;
				bool boolUT = false;
				// kgk trocr-302 10-24-2011  Straighten out confusion between GroupMaster.GroupNumber and  ModuleAssociation.GroupNumber
				rsGrpMst.OpenRecordset("SELECT * FROM GroupMaster WHERE GroupMaster.GroupNumber = " + FCConvert.ToString(lngGroupNumber), "CentralData");
				if (!rsGrpMst.EndOfFile())
				{
					// strSQL = "SELECT * FROM ModuleAssociation WHERE GroupNumber = " & lngGroupNumber
					strSQL = "SELECT * FROM ModuleAssociation WHERE GroupNumber = " + rsGrpMst.Get_Fields_Int32("ID");
					if (rsGroup.OpenRecordset(strSQL, "CentralData"))
					{
						if (!rsGroup.EndOfFile())
						{
							GroupInformation = true;
							// this will loop through all of the transactions on the receipt and check for groups
							while (!rsGroup.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								dblTotalOutstandingBalance += GetRemainingBalance_8(FCConvert.ToInt32(rsGroup.Get_Fields("Account")), rsGroup.Get_Fields_String("Module"), FCConvert.ToInt16(intTemp));
								// this will decide if the accounts have already been counted or not
								string vbPorterVar = rsGroup.Get_Fields_String("Module");
								if ((vbPorterVar == "RE") || (vbPorterVar == "PP"))
								{
									if (boolCL)
									{
									}
									else
									{
										boolCL = true;
										// intTotalAccounts = intTotalAccounts + intTemp
									}
								}
								else if (vbPorterVar == "UT")
								{
									if (boolUT)
									{
									}
									else
									{
										boolUT = true;
										// intTotalAccounts = intTotalAccounts + intTemp
									}
								}
								rsGroup.MoveNext();
							}
							intTotalAccounts = FCConvert.ToInt16(rsGroup.RecordCount());
						}
						else
						{
							intTotalAccounts = 0;
							dblTotalOutstandingBalance = 0;
							GroupInformation = false;
						}
					}
				}
				return GroupInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				intTotalAccounts = 0;
				dblTotalOutstandingBalance = 0;
				GroupInformation = false;
			}
			return GroupInformation;
		}
		// vbPorter upgrade warning: intNumAccts As short	OnWriteFCConvert.ToInt32(
		public static double GetRemainingBalance_8(int lngAccountNumber, string strModule, short intNumAccts)
		{
			return GetRemainingBalance(ref lngAccountNumber, ref strModule, ref intNumAccts);
		}

		public static double GetRemainingBalance(ref int lngAccountNumber, ref string strModule, ref short intNumAccts)
		{
			double GetRemainingBalance = 0;
			// this function will return the remaining balance from one account
			clsDRWrapper rsMod = new clsDRWrapper();
			clsDRWrapper rsTempCL = new clsDRWrapper();
			clsDRWrapper rsTempLien = new clsDRWrapper();
			string strSQL = "";
			// set the default database and the sql string for the
			if ((strModule == "PP") || (strModule == "RE"))
			{
				rsMod.DefaultDB = modExtraModules.strCLDatabase;
				rsTempCL.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = '" + strModule + "' ORDER BY BillingYear", modExtraModules.strCLDatabase);
				if (rsTempCL.EndOfFile() != true && rsTempCL.BeginningOfFile() != true)
				{
					do
					{
						if (FCConvert.ToInt32(rsTempCL.Get_Fields_Int32("LienRecordNumber")) != 0)
						{
							rsTempLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + rsTempCL.Get_Fields_Int32("LienRecordNumber"), modExtraModules.strCLDatabase);
							if (!rsTempLien.EndOfFile())
							{
								double dblNonIntTemp = 0;
								GetRemainingBalance += modCLCalculations.CalculateAccountCLLien(rsTempLien, DateTime.Today, ref dblNonIntTemp);
							}
							else
							{
								// keep going
							}
						}
						else
						{
							double dblCurIntTemp = 0;
							GetRemainingBalance += modCLCalculations.CalculateAccountCL(ref rsTempCL, lngAccountNumber, DateTime.Today, ref dblCurIntTemp, true);
						}
						rsTempCL.MoveNext();
					}
					while (!rsTempCL.EndOfFile());
				}
				else
				{
					GetRemainingBalance = 0;
				}
			}
			else if (strModule == "UT")
			{
				rsMod.DefaultDB = modExtraModules.strUTDatabase;
				rsTempCL.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.GetAccountKeyUT(ref lngAccountNumber)) + " ORDER BY BillingRateKey", modExtraModules.strUTDatabase);
				if (rsTempCL.EndOfFile() != true && rsTempCL.BeginningOfFile() != true)
				{
					do
					{
						// Water
						if (FCConvert.ToInt32(rsTempCL.Get_Fields_Int32("WLienRecordNumber")) != 0)
						{
							rsTempLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTempCL.Get_Fields_Int32("WLienRecordNumber"), modExtraModules.strUTDatabase);
							if (!rsTempLien.EndOfFile())
							{
								double tempParam = 0;
								GetRemainingBalance += modUTCalculations.CalculateAccountUTLien(rsTempLien, DateTime.Today, ref tempParam, true);
							}
							else
							{
								// keep going
							}
						}
						else
						{
							double tempParam = 0;
							GetRemainingBalance += modUTCalculations.CalculateAccountUTLien(rsTempCL, DateTime.Today, ref tempParam, true);
						}
						// Sewer
						if (FCConvert.ToInt32(rsTempCL.Get_Fields_Int32("SLienRecordNumber")) != 0)
						{
							rsTempLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTempCL.Get_Fields_Int32("SLienRecordNumber"), modExtraModules.strUTDatabase);
							if (!rsTempLien.EndOfFile())
							{
								double tempParam = 0;
								GetRemainingBalance += modUTCalculations.CalculateAccountUTLien(rsTempLien, DateTime.Today, ref tempParam, false);
							}
							else
							{
								// keep going
							}
						}
						else
						{
							double tempParam = 0;
							GetRemainingBalance += modUTCalculations.CalculateAccountUT(rsTempCL, DateTime.Today, ref tempParam, false);
						}
						rsTempCL.MoveNext();
					}
					while (!rsTempCL.EndOfFile());
				}
				else
				{
					GetRemainingBalance = 0;
				}
			}
			else
			{
			}
			return GetRemainingBalance;
			ERROR_HANDLER:
			;
			GetRemainingBalance = 0;
			return GetRemainingBalance;
		}

		private static bool setupCL()
		{
			bool setupCL = false;
			// this will load the global CL variables
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCL = new clsDRWrapper();
				string strTemp = "";
				// Write #12, "41 - " & Now & "    - Start Setup CL"
				rsCL.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
				if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
				{
					Statics.gboolDefaultYear = FCConvert.CBool(rsCL.Get_Fields_Boolean("ShowDefaultYear"));
					Statics.gboolShowLastCLAccountInCR = FCConvert.CBool(rsCL.Get_Fields_Boolean("ShowLastCLAccountInCR"));
					modStatusPayments.Statics.gboolDefaultPaymentsToAuto = FCConvert.CBool(rsCL.Get_Fields_Boolean("DefaultPaymentsToAuto"));
					// MAL@20080218
					// gstrTreasSigPath = Trim(rsCL.Fields("TreasurerSig") & " ")
					// 
					Statics.gstrLastYearBilled = Strings.Trim(rsCL.Get_Fields_Int32("LastBilledYear") + " ");
					// MAL@20080110: Reactivated this line
					// Write #12, "42 - " & Now & "    - Start CLSetup Function"
					modCLCalculations.CLSetup();
					// Write #12, "43 - " & Now & "    - End CLSetup Function"
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", ref strTemp);
                    StaticSettings.TaxCollectionValues.LastAccountRE = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", ref strTemp);
                    StaticSettings.TaxCollectionValues.LastAccountPP = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
					// kgk 11-4-2011 trocl-823  Printer adjustments not working from CR
					GetGlobalPrintAdjustments();
					setupCL = true;
				}
				else
				{
					rsCL.AddNew();
					rsCL.Update();
					setupCL = false;
				}
				// Write #12, "44 - " & Now & "    - End Setup CL"
				return setupCL;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				setupCL = false;
			}
			return setupCL;
		}
		// vbPorter upgrade warning: intValue As object	OnWrite(int, string, object)
		// vbPorter upgrade warning: intLength As short	OnWrite(short, double, string)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string PadToString_6(object intValue, short intLength)
		{
			return PadToString(intValue, intLength);
		}

		public static string PadToString_6(int intValue, short intLength)
		{
			object intValueObject = intValue;
			return PadToString(intValueObject, intLength);
		}

		public static string PadToString_8(object intValue, short intLength)
		{
			return PadToString(intValue, intLength);
		}

		public static string PadToString(object intValue, short intLength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			if ((intLength - FCConvert.ToString(intValue).Length) > 0)
			{
				PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			}
			else
			{
				PadToString = intValue.ToString();
			}
			return PadToString;
		}

		public static string Quotes(ref string strValue)
		{
			string Quotes = "";
			Quotes = FCConvert.ToString(Convert.ToChar(34)) + strValue + FCConvert.ToString(Convert.ToChar(34));
			return Quotes;
		}

		public static void CloseChildForms()
		{
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name != "MDIParent")
				{
					if (!Statics.boolLoadHide)
						frm.Close();
				}
			}
		}

		private static bool GetBDAccounts()
		{
			bool GetBDAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBD = new clsDRWrapper();
				GetBDAccounts = false;
				if (rsBD.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase))
				{
					if (rsBD.EndOfFile() != true && rsBD.BeginningOfFile() != true)
					{
						// find the Cash Fund from the budgetary system
						if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Due")) == "Y")
						{
							rsBD.FindFirstRecord("Code", "CF");
							if (rsBD.NoMatch)
							{
								MessageBox.Show("No Cash Fund in the Budgetary System was found.  Please make sure that this account is setup.", "BD Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (Conversion.Val(rsBD.Get_Fields("Account")) == 0)
								{
									frmWait.InstancePtr.Unload();
									MessageBox.Show("No Cash Fund in the Budgetary System was found.  Please make sure that this fund is entered.");
								}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								Statics.glngCashFund = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBD.Get_Fields("Account"))));
							}
							// find the Cash Account for Cash Receipting
							rsBD.FindFirstRecord("Code", "CM");
							if (rsBD.NoMatch)
							{
								MessageBox.Show("No Misc Account for Cash Receipting in the Budgetary System was found.  Please make sure that this account is setup.", "BD Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								Statics.glngCashAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBD.Get_Fields("Account"))));
							}
							GetBDAccounts = true;
							// find the Due To Account from the budgetary system
							rsBD.FindFirstRecord("Code", "DT");
							if (rsBD.NoMatch)
							{
								MessageBox.Show("No Due To Account in the Budgetary System was found.  Please make sure that this account is setup.", "BD Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								Statics.glngDueTo = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBD.Get_Fields("Account"))));
							}
							rsBD.FindFirstRecord("Code", "DF");
							if (rsBD.NoMatch)
							{
								MessageBox.Show("No Due From Account in the Budgetary System was found.  Please make sure that this account is setup.", "BD Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								Statics.glngDueFrom = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBD.Get_Fields("Account"))));
							}
							GetBDAccounts = true;
						}
						else
						{
							GetBDAccounts = true;
						}
					}
				}
				return GetBDAccounts;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetBDAccounts = false;
			}
			return GetBDAccounts;
		}

		public static bool OpenCashDrawer(short intType = 0)
		{
			bool OpenCashDrawer = false;
			// this function will open the cash drawer
			string strPrinterName = "";
			// Dim strPrinterPath  As String
			string strSendChar = "";
			int lngTemp;
			clsPrinterFunctions pfPrinter = new clsPrinterFunctions();
			int intN;
			if (Statics.gboolCDConnected)
			{
				OpenCashDrawer = true;
				strPrinterName = Statics.gstrCDPortID;
				strSendChar = Statics.gstrCDCode;
				strPrinterName = StaticSettings.GlobalSettingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName")?.SettingValue ?? "";
				if (strPrinterName != "")
				{
					// strPrinterName = gstrCDPortID
				}
				else
				{
					MessageBox.Show("No cash drawer is set up. See Cash Drawer under Cash Receipting Help.", "Printer Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				// End If
				if (Strings.UCase(strPrinterName) != "PRNT.TXT")
				{
					if (pfPrinter.OpenPrinterObject(strPrinterName))
					{
						for (intN = 1; intN <= Statics.gintRepeatCount; intN++)
						{
							pfPrinter.PrintText(FCConvert.ToString(Convert.ToChar(FCConvert.ToInt32(strSendChar))));
						}
						pfPrinter.CloseSession(StaticSettings.gGlobalSettings.MachineOwnerID);
					}
					else
					{
						MessageBox.Show("Error opening Port.", "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						OpenCashDrawer = false;
					}
				}
				else
				{
					MessageBox.Show("This printer is setup as \"PRNT.TXT\".  A printer must be set up in order to use the cash drawer.", "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					OpenCashDrawer = false;
				}
			}
			else
			{
				MessageBox.Show("No cash drawer is set up. For information on this topic, go to the index of Cash Receipting Help and type Cash Drawer.", "No Cash Drawer", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return OpenCashDrawer;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public static short SaveReportNumber_2(string strReportName)
		{
			return SaveReportNumber(ref strReportName);
		}

		public static short SaveReportNumber(ref string strReportName)
		{
			short SaveReportNumber = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will take a report name and find the others saved with the same name
				// then it will find the oldest and delete it and return the number of the report so
				// it can be saved in the deleted reports place
				clsDRWrapper rsReport = new clsDRWrapper();
				string strFile = "";
				int intNum;
				if (modGlobalFunctions.ReportDirectoryStatus())
				{
					rsReport.OpenRecordset("SELECT * FROM ReportDates WHERE Description = '" + strReportName + "' ORDER BY ReportDate", modExtraModules.strCRDatabase);
					if (rsReport.EndOfFile() && rsReport.BeginningOfFile() || rsReport.RecordCount() <= 9)
					{
						// if there are no records, then make one
						// find the next unused number
						for (intNum = 1; intNum <= 9; intNum++)
						{
							rsReport.FindFirstRecord("ReportNumber", intNum);
							if (rsReport.NoMatch)
							{
								break;
							}
						}
						rsReport.AddNew();
						rsReport.Set_Fields("Description", strReportName);
						rsReport.Set_Fields("ReportDate", DateTime.Now);
						rsReport.Set_Fields("ReportNumber", intNum);
						rsReport.Update(true);
						SaveReportNumber = FCConvert.ToInt16(rsReport.RecordCount());
					}
					else
					{
						// a file has been found
						SaveReportNumber = FCConvert.ToInt16(rsReport.Get_Fields_Int32("ReportNumber"));
						// update the date
						rsReport.Edit();
						rsReport.Set_Fields("ReportDate", DateTime.Now);
						rsReport.Update(true);
						// delete the old one so that the new one can be added
						strFile = Path.Combine("RPT", strReportName + FCConvert.ToString(SaveReportNumber) + ".RDF");
						// build the path correctly
						if (File.Exists(strFile))
						{
							File.Delete(strFile);
							// delete the file
						}
					}
				}
				else
				{
					MessageBox.Show("The report was not saved.", "Error Creating Directory", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return SaveReportNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Report Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveReportNumber;
		}

		public static int ARCloseOut_2(bool boolPartial, bool boolFinalize = false, string strDeptDiv = "")
		{
			return ARCloseOut(boolPartial, boolFinalize, strDeptDiv);
		}

		public static int ARCloseOut(bool boolPartial = false, bool boolFinalize = false, string strDeptDiv = "")
		{
			int ARCloseOut = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will close out all of the payments
				string strSQL = "";
				clsDRWrapper rsUpdate = new clsDRWrapper();
				clsDRWrapper rs = new clsDRWrapper();
				string strDeptSQL = "";
				string strExclude = "";
				// kk 103112 trocr-348  Close Out by Dept
				if (strDeptDiv != "")
				{
					rs.OpenRecordset("SELECT Code FROM Operators WHERE DeptDiv = '" + strDeptDiv + "'", "SystemSettings");
					while (!rs.EndOfFile())
					{
						if (strDeptSQL == "")
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL = " AND (ISNULL(Teller, '') = '' OR Teller IN ('" + rs.Get_Fields("Code") + "'";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL += ",'" + rs.Get_Fields("Code") + "'";
						}
						rs.MoveNext();
					}
					strDeptSQL += "))";
				}
				if (boolPartial)
				{
					// if a partial close out then do not create a close out record for these entries
					modGlobalFunctions.AddCYAEntry_8("AR", "Partial Close Out");
					ARCloseOut = -100;
				}
				else
				{
					// this will find the next close out number
					rsUpdate.OpenRecordset("SELECT * FROM CloseOut", modExtraModules.strARDatabase);
					rsUpdate.AddNew();
					rsUpdate.Set_Fields("Type", "AR");
					rsUpdate.Update();
					ARCloseOut = FCConvert.ToInt32(rsUpdate.Get_Fields_Int32("ID"));
				}
				if (boolFinalize)
				{
					// if finalizing the close out then use the -100 code to find the records rather than 0
					strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(ARCloseOut) + " WHERE DailyCloseOut = -100" + strDeptSQL;
					modGlobalFunctions.AddCYAEntry_8("AR", "Finalize Partial Close Out");
				}
				else
				{
					// kk10292014 trocr-437  If CR only closeout payments that are closed out in CR
					if (modExtraModules.IsThisCR())
					{
						strExclude = "";
						rs.OpenRecordset("SELECT DISTINCT ReceiptNumber FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 AND ReceiptType = 97 ORDER BY ReceiptNumber", modExtraModules.strCRDatabase);
						while (!rs.EndOfFile())
						{
							strExclude += rs.Get_Fields_Int32("ReceiptNumber") + ",";
							rs.MoveNext();
						}
						if (strExclude != "")
						{
							strExclude = Strings.Left(strExclude, strExclude.Length - 1);
							strExclude = " AND ReceiptNumber NOT IN (" + strExclude + ")";
						}
						strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(ARCloseOut) + " WHERE ISNULL(DailyCloseOut,0) = 0 AND ISNULL(ReceiptNumber,0) >= 0" + strExclude + strDeptSQL;
					}
					else
					{
						strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(ARCloseOut) + " WHERE ISNULL(DailyCloseOut,0) = 0" + strDeptSQL;
					}
				}
				if (rsUpdate.Execute(strSQL, modExtraModules.strARDatabase))
				{
					if (!modExtraModules.IsThisCR())
					{
						MessageBox.Show("Records Updated.", "AR Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					MessageBox.Show("Records not updated.", "AR Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				// MAL@20080918: Set deliberate connection reset to try to avoid error
				// Tracker Reference: 15407
				rsUpdate.DisposeOf();
				rs.DisposeOf();
				return ARCloseOut;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In AR Close Out", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ARCloseOut;
		}

		public static int CLCloseOut_2(bool boolPartial, bool boolFinalize = false, string strDeptDiv = "")
		{
			return CLCloseOut(boolPartial, boolFinalize, strDeptDiv);
		}

		public static int CLCloseOut_3(bool boolPartial, string strDeptDiv = "")
		{
			return CLCloseOut(boolPartial, false, strDeptDiv);
		}

		public static int CLCloseOut(bool boolPartial = false, bool boolFinalize = false, string strDeptDiv = "")
		{
			int CLCloseOut = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will close out all of the payments
				string strSQL = "";
				clsDRWrapper rsUpdate = new clsDRWrapper();
				clsDRWrapper rs = new clsDRWrapper();
				string strDeptSQL = "";
				string strExclude = "";
				// kk 103112 trocr-348  Close Out by Dept
				if (strDeptDiv != "")
				{
					rs.OpenRecordset("SELECT Code FROM Operators WHERE DeptDiv = '" + strDeptDiv + "'", "SystemSettings");
					while (!rs.EndOfFile())
					{
						if (strDeptSQL == "")
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL = " AND (ISNULL(Teller, '') = '' OR Teller IN ('" + rs.Get_Fields("Code") + "'";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL += ",'" + rs.Get_Fields("Code") + "'";
						}
						rs.MoveNext();
					}
					strDeptSQL += "))";
				}

				if (boolPartial)
				{
					// if a partial close out then do not create a close out record for these entries
					modGlobalFunctions.AddCYAEntry_8("CL", "Partial Close Out");
					CLCloseOut = -100;
				}
				else
				{
					rsUpdate.OpenRecordset("SELECT * FROM CloseOut", modExtraModules.strCLDatabase);
					rsUpdate.AddNew();
					rsUpdate.Set_Fields("Type", "CL");
					rsUpdate.Set_Fields("CloseOutDate", DateTime.Now);
					rsUpdate.Update();
					CLCloseOut = FCConvert.ToInt32(rsUpdate.Get_Fields_Int32("ID"));
				}
				if (boolFinalize)
				{
					// if finalizing the close out then use the -100 code to find the records rather than 0
					strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(CLCloseOut) + " WHERE DailyCloseOut = -100" + strDeptSQL;
					modGlobalFunctions.AddCYAEntry_8("CL", "Finalize Partial Close Out");
				}
				else
				{
					// kk10292014 trocr-437  If CR only closeout payments that are closed out in CR
					if (modExtraModules.IsThisCR())
					{
						strExclude = "";
						//FC:FINAL:DSE #i580 reinitialize rs before using it
						rs = new clsDRWrapper();
						rs.OpenRecordset("SELECT DISTINCT ReceiptNumber FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 AND ReceiptType IN (90,91,92,890,891) ORDER BY ReceiptNumber", modExtraModules.strCRDatabase);
						while (!rs.EndOfFile())
						{
							strExclude += rs.Get_Fields_Int32("ReceiptNumber") + ",";
							rs.MoveNext();
						}
						if (strExclude != "")
						{
							strExclude = Strings.Left(strExclude, strExclude.Length - 1);
							strExclude = " AND ReceiptNumber NOT IN (" + strExclude + ")";
						}
						strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(CLCloseOut) + " WHERE ISNULL(DailyCloseOut,0) = 0 AND ISNULL(ReceiptNumber,0) >= 0" + strExclude + strDeptSQL;
					}
					else
					{
						strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(CLCloseOut) + " WHERE ISNULL(DailyCloseOut,0) = 0" + strDeptSQL;
					}
				}
				if (rsUpdate.Execute(strSQL, modExtraModules.strCLDatabase))
				{
					if (!modExtraModules.IsThisCR())
					{
						MessageBox.Show("Records Updated.", "CL Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					MessageBox.Show("Records not updated.", "CL Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				// MAL@20080918: Set deliberate connection reset to try to avoid error
				// Tracker Reference: 15407
				rsUpdate.DisposeOf();
				rs.DisposeOf();
				return CLCloseOut;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In CL Close Out", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CLCloseOut;
		}

		public static int UTCloseOut_2(bool boolPartial, bool boolFinalize = false, string strDeptDiv = "")
		{
			return UTCloseOut(boolPartial, boolFinalize, strDeptDiv);
		}

		public static int UTCloseOut(bool boolPartial = false, bool boolFinalize = false, string strDeptDiv = "")
		{
			int UTCloseOut = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will close out all of the payments
				string strSQL = "";
				clsDRWrapper rsUpdate = new clsDRWrapper();
				clsDRWrapper rs = new clsDRWrapper();
				string strDeptSQL = "";
				string strExclude = "";
				// kk 103112 trocr-348  Close Out by Dept
				if (strDeptDiv != "")
				{
					rs.OpenRecordset("SELECT Code FROM Operators WHERE DeptDiv = '" + strDeptDiv + "'", "SystemSettings");
					while (!rs.EndOfFile())
					{
						if (strDeptSQL == "")
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL = " AND (ISNULL(Teller, '') = '' OR Teller IN ('" + rs.Get_Fields("Code") + "'";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strDeptSQL += ",'" + rs.Get_Fields("Code") + "'";
						}
						rs.MoveNext();
					}
					strDeptSQL += "))";
				}
				rs = null;
				//FC:FINAl:SBE - in original rs is declared as new clsDRWrapper. It will be automatically instantiated on first use
				rs = new clsDRWrapper();
				if (boolPartial)
				{
					// if a partial close out then do not create a close out record for these entries
					modGlobalFunctions.AddCYAEntry_8("UT", "Partial Close Out");
					UTCloseOut = -100;
				}
				else
				{
					rsUpdate.OpenRecordset("SELECT * FROM CloseOut", modExtraModules.strUTDatabase);
					rsUpdate.AddNew();
					rsUpdate.Set_Fields("Type", "UT");
					rsUpdate.Set_Fields("CloseOutDate", DateTime.Now);
					rsUpdate.Update();
					UTCloseOut = FCConvert.ToInt32(rsUpdate.Get_Fields_Int32("ID"));
				}
				if (boolFinalize)
				{
					// if finalizing the close out then use the -100 code to find the records rather than 0
					strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(UTCloseOut) + " WHERE DailyCloseOut = -100" + strDeptSQL;
					modGlobalFunctions.AddCYAEntry_8("UT", "Finalize Partial Close Out");
				}
				else
				{
					// kk10292014 trocr-437  If CR only closeout payments that are closed out in CR
					if (modExtraModules.IsThisCR())
					{
						strExclude = "";
						rs.OpenRecordset("SELECT DISTINCT ReceiptNumber FROM Archive WHERE ISNULL(DailyCloseOut,0) = 0 AND ReceiptType IN (93,94,95,96,893,894,895,896) ORDER BY ReceiptNumber", modExtraModules.strCRDatabase);
						while (!rs.EndOfFile())
						{
							strExclude += rs.Get_Fields_Int32("ReceiptNumber") + ",";
							rs.MoveNext();
						}
						if (strExclude != "")
						{
							strExclude = Strings.Left(strExclude, strExclude.Length - 1);
							strExclude = " AND ReceiptNumber NOT IN (" + strExclude + ")";
						}
						strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(UTCloseOut) + " WHERE ISNULL(DailyCloseOut,0) = 0 AND ISNULL(ReceiptNumber,0) >= 0" + strExclude + strDeptSQL;
					}
					else
					{
						strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(UTCloseOut) + " WHERE ISNULL(DailyCloseOut,0) = 0" + strDeptSQL;
					}
				}
				if (rsUpdate.Execute(strSQL, modExtraModules.strUTDatabase))
				{
					if (!modExtraModules.IsThisCR())
					{
						MessageBox.Show("Records Updated.", "UT Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					MessageBox.Show("Records not updated.", "UT Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				// MAL@20080918: Set deliberate connection reset to try to avoid error
				// Tracker Reference: 15407
				rsUpdate.Reset();
				return UTCloseOut;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In UT Close Out", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UTCloseOut;
		}

		private static void SetPrinterRegistrySettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsReg = new clsDRWrapper();
				rsReg.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
				if (rsReg.EndOfFile())
				{
					rsReg.AddNew();
					rsReg.Update();
				}
				else
				{
					// set the registry entries
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptLength", FCConvert.ToString(Conversion.Val(rsReg.Get_Fields_Int32("ReceiptLength"))));
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRExtraLength", FCConvert.ToString(Conversion.Val(rsReg.Get_Fields_Int32("ExtraLines"))));
					if (FCConvert.ToBoolean(rsReg.Get_Fields_Boolean("CDConnected")))
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDConnected", "TRUE");
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDConnected", "FALSE");
					}
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDCode", rsReg.Get_Fields_String("CDCode"));
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRRepeatCount", FCConvert.ToString(Conversion.Val(rsReg.Get_Fields_Int32("RepeatCount"))));
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDPortID", rsReg.Get_Fields_String("PortID"));
					if (FCConvert.ToBoolean(rsReg.Get_Fields_Boolean("NarrowReceipt")))
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRNarrowReceipt", "TRUE");
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRNarrowReceipt", "FALSE");
					}
					if (FCConvert.ToBoolean(rsReg.Get_Fields_Boolean("SP200Printer")))
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRSP200Printer", "TRUE");
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRSP200Printer", "FALSE");
					}
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptOffset", FCConvert.ToString(Conversion.Val(rsReg.Get_Fields_Int32("ReceiptPrinterOffset"))));
				}
				// set the flag
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRAdjustPrinterRegistry", "TRUE");
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// let it go and the user would have to set it up anyway
			}
		}

		public static bool CheckPrinterRegistry()
		{
			bool CheckPrinterRegistry = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				// get the flag for this
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRAdjustPrinterRegistry", ref strTemp);
				if (Strings.UCase(strTemp) != "TRUE")
				{
					CheckPrinterRegistry = true;
				}
				else
				{
					// keep going this already has been run
				}
				return CheckPrinterRegistry;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Printer Registry", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckPrinterRegistry;
		}

		public static bool CheckForPendingTransactions()
		{
			bool CheckForPendingTransactions = false;
			// this function will return true if any records are found with a daily close out value of less then zero (Pending Transaction)
			clsDRWrapper rsPend = new clsDRWrapper();
			rsPend.OpenRecordset("SELECT ID FROM Archive WHERE ISNULL(DailyCloseOut,0) < -1");
			if (rsPend.EndOfFile())
			{
				CheckForPendingTransactions = false;
			}
			else
			{
				CheckForPendingTransactions = true;
			}
			return CheckForPendingTransactions;
		}

		public static void SetupRestrictedReceiptTypes()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRT = new clsDRWrapper();
				int lngTemp = 0;
				int lngType;
				bool boolUC = false;
				rsRT.DefaultDB = modExtraModules.strCRDatabase;
				// UppercaseTypes
				rsRT.OpenRecordset("SELECT UppercaseTypes FROM CashRec");
				if (!rsRT.EndOfFile())
				{
					boolUC = FCConvert.ToBoolean(rsRT.Get_Fields_Boolean("UppercaseTypes"));
				}
				// For lngTemp = 0 To 8
				// RE Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "90");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 90);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Real Estate Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Discount"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Disc"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Real Estate Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Discount");
					rsRT.Set_Fields("Title3Abbrev", "Disc");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Account-Year");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// RE Lien Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "91");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 91);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Tax Lien Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Lien Interest"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Tax Lien Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Lien Interest");
					rsRT.Set_Fields("Title3Abbrev", "Lien Int");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Account-Year");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Tax Acquired RE Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 890");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 890);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Tax Acquired Real Estate Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Discount"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Disc"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Tax Acquired Real Estate Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Discount");
					rsRT.Set_Fields("Title3Abbrev", "Disc");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Account-Year");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Tax Acquired RE Lien Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 891");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 891);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Tax Acquired Lien Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Lien Interest"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Tax Acquired Lien Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Lien Interest");
					rsRT.Set_Fields("Title3Abbrev", "Lien Int");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Account-Year");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				//Application.DoEvents();
				frmWait.InstancePtr.IncrementProgress();
				// PP Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "92");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 92);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Personal Property Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Discount"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Disc"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Personal Property Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Discount");
					rsRT.Set_Fields("Title3Abbrev", "Disc");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Account-Year");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Water Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "93");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 93);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Utility - Water Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Discount"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Disc"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Utility - Water Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Discount");
					rsRT.Set_Fields("Title3Abbrev", "Disc");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Water TA Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 893");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 893);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Tax Acquired Water Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Discount"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Disc"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Tax Acquired Water Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Discount");
					rsRT.Set_Fields("Title3Abbrev", "Disc");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Sewer Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "94");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 94);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Utility - Sewer Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Discount"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Disc"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Utility - Sewer Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Discount");
					rsRT.Set_Fields("Title3Abbrev", "Disc");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				//Application.DoEvents();
				frmWait.InstancePtr.IncrementProgress();
				// Sewer TA Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 894");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 894);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Tax Acquired Sewer Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Discount"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Disc"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Tax Acquired Sewer Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Discount");
					rsRT.Set_Fields("Title3Abbrev", "Disc");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Sewer Lien
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "95");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 95);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Utility - Sewer Lien"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Lien Interest"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Utility - Sewer Lien");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Lien Interest");
					rsRT.Set_Fields("Title3Abbrev", "Lien Int");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Account-Year");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Water TA Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 895");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 895);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Tax Acquired Sewer Lien Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Tax Acquired Sewer Lien Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Lien Int");
					rsRT.Set_Fields("Title3Abbrev", "Lien Int");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Water Lien
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "96");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 96);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Utility - Water Lien"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Lien Interest"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Utility - Water Lien");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Lien Interest");
					rsRT.Set_Fields("Title3Abbrev", "Lien Int");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				//Application.DoEvents();
				frmWait.InstancePtr.IncrementProgress();
				// Water TA Payment
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 896");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 896);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Tax Acquired Water Lien Payment"));
					rsRT.Set_Fields("Title1", Strings.UCase("Principal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Lien Int"));
					rsRT.Set_Fields("Title4", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Costs"));
					rsRT.Set_Fields("Title5", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title6", Strings.UCase("Abatement"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Abate"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Tax Acquired Water Lien Payment");
					rsRT.Set_Fields("Title1", "Principal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Lien Int");
					rsRT.Set_Fields("Title3Abbrev", "Lien Int");
					rsRT.Set_Fields("Title4", "Costs");
					rsRT.Set_Fields("Title4Abbrev", "Costs");
					rsRT.Set_Fields("Title5", "Tax");
					rsRT.Set_Fields("Title5Abbrev", "Tax");
					rsRT.Set_Fields("Title6", "Abatement");
					rsRT.Set_Fields("Title6Abbrev", "Abate");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Accounts Receivable
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "97");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 97);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Accounts Receivable"));
					rsRT.Set_Fields("Title1", Strings.UCase("Prinicpal"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Prin"));
					rsRT.Set_Fields("Title2", Strings.UCase("Interest"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Int"));
					rsRT.Set_Fields("Title3", Strings.UCase("Sales Tax"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Tax"));
					rsRT.Set_Fields("Title4", "");
					rsRT.Set_Fields("Title4Abbrev", "");
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", "");
					rsRT.Set_Fields("Title6Abbrev", "");
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Accounts Receivable");
					rsRT.Set_Fields("Title1", "Prinicpal");
					rsRT.Set_Fields("Title1Abbrev", "Prin");
					rsRT.Set_Fields("Title2", "Interest");
					rsRT.Set_Fields("Title2Abbrev", "Int");
					rsRT.Set_Fields("Title3", "Sales Tax");
					rsRT.Set_Fields("Title3Abbrev", "Tax");
					rsRT.Set_Fields("Title4", "");
					rsRT.Set_Fields("Title4Abbrev", "");
					rsRT.Set_Fields("Title5", "");
					rsRT.Set_Fields("Title5Abbrev", "");
					rsRT.Set_Fields("Title6", "");
					rsRT.Set_Fields("Title6Abbrev", "");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "Control3");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// add the clerk type if it is needed
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "98");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(lngTemp) + 98);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Clerk"));
					rsRT.Set_Fields("Title1", Strings.UCase("Vital Records"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Vital"));
					rsRT.Set_Fields("Title2", Strings.UCase("Town Fee"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Town"));
					rsRT.Set_Fields("Title3", Strings.UCase("State Fee"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("State"));
					rsRT.Set_Fields("Title4", Strings.UCase("Clerk Fee"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Clerk"));
					rsRT.Set_Fields("Title5", Strings.UCase("Late Fee"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Late"));
					rsRT.Set_Fields("Title6", Strings.UCase("Misc Fee"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Misc"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Clerk");
					rsRT.Set_Fields("Title1", "Vital Records");
					rsRT.Set_Fields("Title1Abbrev", "Vital");
					rsRT.Set_Fields("Title2", "Town Fee");
					rsRT.Set_Fields("Title2Abbrev", "Town");
					rsRT.Set_Fields("Title3", "State Fee");
					rsRT.Set_Fields("Title3Abbrev", "State");
					rsRT.Set_Fields("Title4", "Clerk Fee");
					rsRT.Set_Fields("Title4Abbrev", "Clerk");
					rsRT.Set_Fields("Title5", "Late Fee");
					rsRT.Set_Fields("Title5Abbrev", "Late");
					rsRT.Set_Fields("Title6", "Misc Fee");
					rsRT.Set_Fields("Title6Abbrev", "Misc");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Fix the MV type if it is needed
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "99");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(Conversion.Val(FCConvert.ToString(lngTemp) + "99")));
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Motor Vehicle"));
					rsRT.Set_Fields("Title1", Strings.UCase("Excise Tax"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Excise"));
					rsRT.Set_Fields("Title2", Strings.UCase("State Fee"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("State"));
					rsRT.Set_Fields("Title3", Strings.UCase("Agent Fee"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Agent"));
					rsRT.Set_Fields("Title4", Strings.UCase("Sales Tax"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Sales"));
					rsRT.Set_Fields("Title5", Strings.UCase("Title Fee"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Title"));
					rsRT.Set_Fields("Title6", Strings.UCase("Other Excise"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Other"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Motor Vehicle");
					rsRT.Set_Fields("Title1", "Excise Tax");
					rsRT.Set_Fields("Title1Abbrev", "Excise");
					rsRT.Set_Fields("Title2", "State Fee");
					rsRT.Set_Fields("Title2Abbrev", "State");
					rsRT.Set_Fields("Title3", "Agent Fee");
					rsRT.Set_Fields("Title3Abbrev", "Agent");
					rsRT.Set_Fields("Title4", "Sales Tax");
					rsRT.Set_Fields("Title4Abbrev", "Sales");
					rsRT.Set_Fields("Title5", "Title Fee");
					rsRT.Set_Fields("Title5Abbrev", "Title");
					rsRT.Set_Fields("Title6", "Other Excise");
					rsRT.Set_Fields("Title6Abbrev", "Other");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Plate");
				rsRT.Set_Fields("Control1", "Year Sticker");
				rsRT.Set_Fields("Control2", "Month Sticker");
				rsRT.Set_Fields("Control3", "MVR3");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Next
				//Application.DoEvents();
				frmWait.InstancePtr.IncrementProgress();
				// MOSES Type
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 190", modExtraModules.strCRDatabase);
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", FCConvert.ToString(Conversion.Val("190")));
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("MOSES"));
					rsRT.Set_Fields("Title1", Strings.UCase("Excise Tax"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Excise"));
					rsRT.Set_Fields("Title2", Strings.UCase("State Fee"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("State"));
					rsRT.Set_Fields("Title3", Strings.UCase("Agent Fee"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("Agent"));
					rsRT.Set_Fields("Title4", Strings.UCase("Sales Tax"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Sales"));
					rsRT.Set_Fields("Title5", "");
					// UCase("Title Fee")
					rsRT.Set_Fields("Title5Abbrev", "");
					// UCase("Title")
					rsRT.Set_Fields("Title6", "");
					// UCase("Other Excise")
					rsRT.Set_Fields("Title6Abbrev", "");
					// UCase("Other")
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Moses");
					rsRT.Set_Fields("Title1", "Excise Tax");
					rsRT.Set_Fields("Title1Abbrev", "Excise");
					rsRT.Set_Fields("Title2", "State Fee");
					rsRT.Set_Fields("Title2Abbrev", "State");
					rsRT.Set_Fields("Title3", "Agent Fee");
					rsRT.Set_Fields("Title3Abbrev", "Agent");
					rsRT.Set_Fields("Title4", "Sales Tax");
					rsRT.Set_Fields("Title4Abbrev", "Sales");
					rsRT.Set_Fields("Title5", "");
					// "Title Fee"
					rsRT.Set_Fields("Title5Abbrev", "");
					// "Title"
					rsRT.Set_Fields("Title6", "");
					// "Other Excise"
					rsRT.Set_Fields("Title6Abbrev", "");
					// "Other"
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Document");
				rsRT.Set_Fields("Control1", "Receipt");
				rsRT.Set_Fields("Control2", "Registration");
				rsRT.Set_Fields("Control3", "Inventory Serial");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				//Application.DoEvents();
				frmWait.InstancePtr.IncrementProgress();
				// Clerk Types - Dog
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 800");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 800);
				rsRT.Set_Fields("TypeTitle", "Dog Registration");
				if (boolUC)
				{
					rsRT.Set_Fields("Title1", Strings.UCase("Custom Fee 2"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Custom Fee 2"));
					// UCase("Vital")
					rsRT.Set_Fields("Title2", Strings.UCase("Town Fee"));
					rsRT.Set_Fields("Title2Abbrev", Strings.UCase("Town"));
					rsRT.Set_Fields("Title3", Strings.UCase("State Fee"));
					rsRT.Set_Fields("Title3Abbrev", Strings.UCase("State"));
					rsRT.Set_Fields("Title4", Strings.UCase("Clerk Fee"));
					rsRT.Set_Fields("Title4Abbrev", Strings.UCase("Clerk"));
					rsRT.Set_Fields("Title5", Strings.UCase("Late Fee"));
					rsRT.Set_Fields("Title5Abbrev", Strings.UCase("Late"));
					rsRT.Set_Fields("Title6", Strings.UCase("Misc Fee"));
					rsRT.Set_Fields("Title6Abbrev", Strings.UCase("Misc"));
				}
				else
				{
					rsRT.Set_Fields("Title1", "Custom Fee 2");
					rsRT.Set_Fields("Title1Abbrev", "Custom Fee 2");
					rsRT.Set_Fields("Title2", "Town Fee");
					rsRT.Set_Fields("Title2Abbrev", "Town");
					rsRT.Set_Fields("Title3", "State Fee");
					rsRT.Set_Fields("Title3Abbrev", "State");
					rsRT.Set_Fields("Title4", "Clerk Fee");
					rsRT.Set_Fields("Title4Abbrev", "Clerk");
					rsRT.Set_Fields("Title5", "Late Fee");
					rsRT.Set_Fields("Title5Abbrev", "Late");
					rsRT.Set_Fields("Title6", "Misc Fee");
					rsRT.Set_Fields("Title6Abbrev", "Misc");
				}
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Clerk Types - Death
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 801");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 801);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Death Certificate"));
					rsRT.Set_Fields("Title1", Strings.UCase("Vital Records"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Vital"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Death Certificate");
					rsRT.Set_Fields("Title1", "Vital Records");
					rsRT.Set_Fields("Title1Abbrev", "Vital");
				}
				rsRT.Set_Fields("Title2", "State Fee");
				rsRT.Set_Fields("Title2Abbrev", "State");
				rsRT.Set_Fields("Title3", "");
				rsRT.Set_Fields("Title3Abbrev", "");
				rsRT.Set_Fields("Title4", "");
				rsRT.Set_Fields("Title4Abbrev", "");
				rsRT.Set_Fields("Title5", "");
				rsRT.Set_Fields("Title5Abbrev", "");
				rsRT.Set_Fields("Title6", "");
				rsRT.Set_Fields("Title6Abbrev", "");
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Clerk Types - Birth
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 802");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 802);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Birth Certificate"));
					rsRT.Set_Fields("Title1", Strings.UCase("Vital Records"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Vital"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Birth Certificate");
					rsRT.Set_Fields("Title1", "Vital Records");
					rsRT.Set_Fields("Title1Abbrev", "Vital");
				}
				rsRT.Set_Fields("Title2", "State Fee");
				rsRT.Set_Fields("Title2Abbrev", "State");
				rsRT.Set_Fields("Title3", "");
				rsRT.Set_Fields("Title3Abbrev", "");
				rsRT.Set_Fields("Title4", "");
				rsRT.Set_Fields("Title4Abbrev", "");
				rsRT.Set_Fields("Title5", "");
				rsRT.Set_Fields("Title5Abbrev", "");
				rsRT.Set_Fields("Title6", "");
				rsRT.Set_Fields("Title6Abbrev", "");
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Clerk Types - Marriage
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 803");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 803);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Marriage Certificate"));
					rsRT.Set_Fields("Title1", Strings.UCase("Vital Records"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Vital"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Marriage Certificate");
					rsRT.Set_Fields("Title1", "Vital Records");
					rsRT.Set_Fields("Title1Abbrev", "Vital");
				}
				rsRT.Set_Fields("Title2", "State Fee");
				rsRT.Set_Fields("Title2Abbrev", "State");
				rsRT.Set_Fields("Title3", "");
				rsRT.Set_Fields("Title3Abbrev", "");
				rsRT.Set_Fields("Title4", "");
				rsRT.Set_Fields("Title4Abbrev", "");
				rsRT.Set_Fields("Title5", "");
				rsRT.Set_Fields("Title5Abbrev", "");
				rsRT.Set_Fields("Title6", "");
				rsRT.Set_Fields("Title6Abbrev", "");
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// Clerk Types - Burial Permit
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 804");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 804);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Burial Permit"));
					rsRT.Set_Fields("Title1", Strings.UCase("Vital Records"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Vital"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Burial Permit");
					rsRT.Set_Fields("Title1", "Vital Records");
					rsRT.Set_Fields("Title1Abbrev", "Vital");
				}
				rsRT.Set_Fields("Title2", "State Fee");
				rsRT.Set_Fields("Title2Abbrev", "State");
				rsRT.Set_Fields("Title3", "");
				rsRT.Set_Fields("Title3Abbrev", "");
				rsRT.Set_Fields("Title4", "");
				rsRT.Set_Fields("Title4Abbrev", "");
				rsRT.Set_Fields("Title5", "");
				rsRT.Set_Fields("Title5Abbrev", "");
				rsRT.Set_Fields("Title6", "");
				rsRT.Set_Fields("Title6Abbrev", "");
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Reference");
				rsRT.Set_Fields("Control1", "Control1");
				rsRT.Set_Fields("Control2", "Control2");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// VISA Service Fee
				rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 901");
				if (rsRT.EndOfFile())
				{
					rsRT.AddNew();
				}
				else
				{
					rsRT.Edit();
				}
				rsRT.Set_Fields("TypeCode", 901);
				if (boolUC)
				{
					rsRT.Set_Fields("TypeTitle", Strings.UCase("Visa Service Fee"));
					rsRT.Set_Fields("Title1", Strings.UCase("Service Fee"));
					rsRT.Set_Fields("Title1Abbrev", Strings.UCase("Serv Fee"));
				}
				else
				{
					rsRT.Set_Fields("TypeTitle", "Visa Service Fee");
					rsRT.Set_Fields("Title1", "Service Fee");
					rsRT.Set_Fields("Title1Abbrev", "Serv Fee");
				}
				rsRT.Set_Fields("Title2", "");
				rsRT.Set_Fields("Title2Abbrev", "");
				rsRT.Set_Fields("Title3", "");
				rsRT.Set_Fields("Title3Abbrev", "");
				rsRT.Set_Fields("Title4", "");
				rsRT.Set_Fields("Title4Abbrev", "");
				rsRT.Set_Fields("Title5", "");
				rsRT.Set_Fields("Title5Abbrev", "");
				rsRT.Set_Fields("Title6", "");
				rsRT.Set_Fields("Title6Abbrev", "");
				rsRT.Set_Fields("BMV", false);
				rsRT.Set_Fields("ChangeType", false);
				rsRT.Set_Fields("Reference", "Original Rcpt");
				rsRT.Set_Fields("Control1", "");
				rsRT.Set_Fields("Control2", "");
				rsRT.Set_Fields("Control3", "");
				rsRT.Set_Fields("ShowFeeDetailOnReceipt", true);
				rsRT.Update();
				// this will remove all the excess receipts types made during the multi town transition phase
				for (lngTemp = 1; lngTemp <= 8; lngTemp++)
				{
					if (lngTemp < 8)
					{
						// 1-7 for these 890 - tax acquired and 891 - liened tax acquired
						if (lngTemp != 1)
						{
							rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "90");
							rsRT.Delete();
						}
						rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "91");
						rsRT.Delete();
						rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "93");
						rsRT.Delete();
						rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "94");
						rsRT.Delete();
						rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "95");
						rsRT.Delete();
						rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "96");
						rsRT.Delete();
					}
					rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "92");
					rsRT.Delete();
					rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "97");
					rsRT.Delete();
					rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "98");
					rsRT.Delete();
					rsRT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTemp) + "99");
					rsRT.Delete();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Restricted Type Setup", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void UpdateCLAppliedThroughDate(ref int lngKey)
		{
			// dummy function from CL
		}

		public static void PrintCLReceipt()
		{
			// dummy function from CL
		}

		private static void AddFakeTellerForBen()
		{
			clsDRWrapper rsBen = new clsDRWrapper();
			rsBen.OpenRecordset("SELECT * FROM Operators WHERE Code = '617'", "SystemSettings");
			if (!rsBen.EndOfFile())
			{
				// this is ok, let it go
			}
			else
			{
				rsBen.AddNew();
				rsBen.Set_Fields("Code", "617");
				rsBen.Set_Fields("Name", "TRIO Staff");
				rsBen.Set_Fields("Level", 3);
				rsBen.Update();
			}
		}

		public static void GetMasterAccount(int lngAcctKey, int lngAccountNumber)
		{
			// This is a dummy function for UT
		}

		public static void CheckMultipleTownTypes()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This routine will check to make sure all of the additional types have the correct titles, ctrl, ref and settings
				clsDRWrapper rsType = new clsDRWrapper();
				clsDRWrapper rsMT = new clsDRWrapper();
				clsDRWrapper rsMO = new clsDRWrapper();
				clsDRWrapper rsTowns = new clsDRWrapper();
				if (Statics.gboolMultipleTowns)
				{
					rsTowns.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber > 1 ORDER BY TownNumber", "CentralData");
					// rsType.OpenRecordset "SELECT TypeCode FROM (SELECT COUNT(*) AS CT, TypeCode FROM Type GROUP BY TypeCode) AS Temp WHERE CT > 1", strCRDatabase
					if (!rsTowns.EndOfFile())
					{
						rsType.OpenRecordset("SELECT * FROM Type WHERE TownKey = 1", modExtraModules.strCRDatabase);
						while (!rsType.EndOfFile())
						{
							rsMT.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + rsType.Get_Fields_Int32("TypeCode") + " AND TownKey = 1", modExtraModules.strCRDatabase);
							if (!rsMT.EndOfFile())
							{
								rsTowns.MoveFirst();
								while (!rsTowns.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
									rsMO.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + rsType.Get_Fields_Int32("TypeCode") + " AND TownKey = " + rsTowns.Get_Fields("TownNumber"), modExtraModules.strCRDatabase);
									if (rsMO.EndOfFile())
									{
										rsMO.AddNew();
									}
									else
									{
										rsMO.Edit();
									}
									// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
									rsMO.Set_Fields("TownKey", rsTowns.Get_Fields("TownNumber"));
									rsMO.Set_Fields("TypeCode", rsMT.Get_Fields_Int32("TypeCode"));
									rsMO.Set_Fields("TypeTitle", rsMT.Get_Fields_String("TypeTitle"));
									rsMO.Set_Fields("Title1", rsMT.Get_Fields_String("Title1"));
									rsMO.Set_Fields("Title1Abbrev", rsMT.Get_Fields_String("Title1Abbrev"));
									rsMO.Set_Fields("Title2", rsMT.Get_Fields_String("Title2"));
									rsMO.Set_Fields("Title2Abbrev", rsMT.Get_Fields_String("Title2Abbrev"));
									rsMO.Set_Fields("Title3", rsMT.Get_Fields_String("Title3"));
									rsMO.Set_Fields("Title3Abbrev", rsMT.Get_Fields_String("Title3Abbrev"));
									rsMO.Set_Fields("Title4", rsMT.Get_Fields_String("Title4"));
									rsMO.Set_Fields("Title4Abbrev", rsMT.Get_Fields_String("Title4Abbrev"));
									rsMO.Set_Fields("Title5", rsMT.Get_Fields_String("Title5"));
									rsMO.Set_Fields("Title5Abbrev", rsMT.Get_Fields_String("Title5Abbrev"));
									rsMO.Set_Fields("Title6", rsMT.Get_Fields_String("Title6"));
									rsMO.Set_Fields("Title6Abbrev", rsMT.Get_Fields_String("Title6Abbrev"));
									rsMO.Set_Fields("BMV", rsMT.Get_Fields_Boolean("BMV"));
									rsMO.Set_Fields("Reference", rsMT.Get_Fields_String("Reference"));
									rsMO.Set_Fields("Control1", rsMT.Get_Fields_String("Control1"));
									rsMO.Set_Fields("Control2", rsMT.Get_Fields_String("Control2"));
									rsMO.Set_Fields("Control3", rsMT.Get_Fields_String("Control3"));
									rsMO.Set_Fields("ReferenceMandatory", rsMT.Get_Fields_Boolean("ReferenceMandatory"));
									rsMO.Set_Fields("Control1Mandatory", rsMT.Get_Fields_Boolean("Control1Mandatory"));
									rsMO.Set_Fields("Control2Mandatory", rsMT.Get_Fields_Boolean("Control2Mandatory"));
									rsMO.Set_Fields("Control3Mandatory", rsMT.Get_Fields_Boolean("Control3Mandatory"));
									// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
									rsMO.Set_Fields("Copies", rsMT.Get_Fields("Copies"));
									rsMO.Set_Fields("Print", rsMT.Get_Fields_Boolean("Print"));
									rsMO.Set_Fields("ShowFeeDetailOnReceipt", rsMT.Get_Fields_Boolean("ShowFeeDetailOnReceipt"));
									rsMO.Set_Fields("EFT", rsMT.Get_Fields_Boolean("EFT"));
									rsMO.Update();
									rsTowns.MoveNext();
								}
								rsType.MoveNext();
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Multiple Town Types", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static int FindExcludeYear()
		{
			int FindExcludeYear = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// vbPorter upgrade warning: lngTemp As int	OnWrite(string)
				int lngTemp = 0;
				frmWait.InstancePtr.Unload();
				TryAgain:
				;
				switch (MessageBox.Show("Have you manually applied the new registry of deeds fee to your current year liens?", "Adding Lien Fee", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
				{
					case DialogResult.Yes:
						{
							// find out what year
							lngTemp = FCConvert.ToInt32(Interaction.InputBox("What year were these fees applied to?", "Year To Exclude From Update", null));
							if (lngTemp > 2000 && lngTemp < 2006)
							{
								// return the lear for the SQL statement
								FindExcludeYear = lngTemp;
								modGlobalFunctions.AddCYAEntry_26("CL", "Adding New Lien Fees", "User did apply new fees for the year " + FCConvert.ToString(lngTemp) + " .");
							}
							else
							{
								MessageBox.Show("This year is not your most current liened year.", "Try again.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								goto TryAgain;
							}
							break;
						}
					case DialogResult.No:
						{
							// let this rip
							modGlobalFunctions.AddCYAEntry_26("CL", "Adding New Lien Fees", "User did not apply new fees yet.");
							FindExcludeYear = 0;
							break;
						}
				}
				//end switch
				return FindExcludeYear;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Extra Lien Costs", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindExcludeYear;
		}
		// vbPorter upgrade warning: boolCheckAll As object	OnWrite(bool)
		private static bool CheckRestrictedAccountNumbers_8(int lngType, bool boolCheckAll = true, bool boolAllowBlankAccounts = false)
		{
			return CheckRestrictedAccountNumbers(ref lngType, boolCheckAll, boolAllowBlankAccounts);
		}

		private static bool CheckRestrictedAccountNumbers_26(int lngType, bool boolCheckAll = true, bool boolAllowBlankAccounts = false)
		{
			return CheckRestrictedAccountNumbers(ref lngType, boolCheckAll, boolAllowBlankAccounts);
		}

		private static bool CheckRestrictedAccountNumbers(ref int lngType, bool boolCheckAll = true, bool boolAllowBlankAccounts = false)
		{
			bool CheckRestrictedAccountNumbersRet = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function has a receipt type number passed into it, it will check the receipt number
				// to make sure that all of the accounts needed are valid.  If all accounts are valid, then
				// it will return True, else it will return a false
				clsDRWrapper rsType;
				int intCT;
				CheckRestrictedAccountNumbersRet = true;
				rsType = new clsDRWrapper();
				rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType) + " AND TownKey = 0", modExtraModules.strCRDatabase);
				if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
				{
					for (intCT = 1; intCT <= 6; intCT++)
					{
						if (FCConvert.ToString(rsType.Get_Fields_String("Title" + FCConvert.ToString(intCT))) != "")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields("Account" + FCConvert.ToString(intCT)))) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields("Account" + FCConvert.ToString(intCT))))
									{
										// remove validation
										CheckRestrictedAccountNumbersRet = false;
										return CheckRestrictedAccountNumbersRet;
									}
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									if (Strings.Left(FCConvert.ToString(rsType.Get_Fields("Account" + FCConvert.ToString(intCT))), 1) != "M")
									{
										CheckRestrictedAccountNumbersRet = false;
										return CheckRestrictedAccountNumbersRet;
									}
								}
							}
							else
							{
								// if the string is empty then return false
								if (boolAllowBlankAccounts)
								{
									// keep going, this is for the type 97 account in the range of 901 to 999
								}
								else
								{
									CheckRestrictedAccountNumbersRet = false;
									return CheckRestrictedAccountNumbersRet;
								}
							}
						}
					}
					if (FCConvert.ToBoolean(boolCheckAll) && CheckRestrictedAccountNumbersRet)
					{
						if (lngType == 90)
						{
							// check the 91
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_8(91, false);
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_8(890, false);
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_8(891, false);
						}
						else if (lngType == 91)
						{
							// check the 90
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_8(90, false);
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_8(890, false);
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_8(891, false);
						}
						else if (lngType == 97)
						{
							// This will start recursion at 901 and check all 99 AR types or until a failure
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_26(901, false, true);
						}
						else if (lngType == 98)
						{
							if (modUseCR.BreakdownCKRecords())
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_8(800, false);
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_8(801, false);
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_8(802, false);
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_8(803, false);
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_8(804, false);
							}
						}
						else if (lngType >= 901 && lngType <= 999)
						{
							if (lngType < 999)
							{
								// recursion = coding magic!
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_26(lngType + 1, false, true);
							}
						}
					}
					// If rsType.Fields("Print") Then
					// chkPrint.Value = vbChecked
					// Else
					// chkPrint.Value = vbUnchecked
					// End If
				}
				else
				{
					CheckRestrictedAccountNumbersRet = false;
				}
				return CheckRestrictedAccountNumbersRet;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckRestrictedAccountNumbersRet = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Check Restricted Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckRestrictedAccountNumbersRet;
		}
		// vbPorter upgrade warning: lngValue As object	OnWriteFCConvert.ToInt16(
		public static object ChangeFocusBehavior(object lngValue)
		{
			object ChangeFocusBehavior = null;
			int lngWin32apiResultCode;
			int lngValueTemp = FCConvert.ToInt32(lngValue);
			//lngWin32apiResultCode = modUseCR.SystemParametersInfo(modUseCR.SPI_SETFOREGROUNDLOCKTIMEOUT, 0, ref lngValueTemp, modUseCR.SPIF_SENDWININICHANGE | modUseCR.SPIF_UPDATEINIFILE);
			return ChangeFocusBehavior;
		}
		// vbPorter upgrade warning: lngValue As object	OnWriteFCConvert.ToInt16(
		public static object GetFocusBehavior(object lngValue)
		{
			object GetFocusBehavior = null;
			int lngWin32apiResultCode;
			int lngValueTemp = FCConvert.ToInt32(lngValue);
			//lngWin32apiResultCode = modUseCR.SystemParametersInfo(modUseCR.SPI_GETFOREGROUNDLOCKTIMEOUT, 0, ref lngValueTemp, 0);
			GetFocusBehavior = lngValue;
			return GetFocusBehavior;
		}

		private static void AddMOSESFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				rsM.OpenRecordset("SELECT * FROM MOSESTypes WHERE MOSESType = 1917");
				if (rsM.EndOfFile())
				{
					rsM.AddNew();
					rsM.Set_Fields("MOSESType", 1917);
					rsM.Set_Fields("MOSESTypeDescription", "Boat Permanent Sticker");
					rsM.EndOfFile();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding MOSES Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void AddMOSESFields_20071201()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				Dictionary<object, object> objDictionary = new Dictionary<object, object>();
				int lngCode = 0;
				int intCount;
				// vbPorter upgrade warning: strDescription As string	OnWrite(object)
				string strDescription = "";
				// vbPorter upgrade warning: varKey As Variant --> As int
				int varKey;
				for (intCount = 1; intCount <= 20; intCount++)
				{
					switch (intCount)
					{
						case 1:
							{
								objDictionary.Add(1925, "Boat 0-10 Hsp Reg Ren w/Milfoil");
								break;
							}
						case 2:
							{
								objDictionary.Add(1926, "Boat 11-50 Hsp Reg Ren w/Milfoil");
								break;
							}
						case 3:
							{
								objDictionary.Add(1927, "Boat 51-115 Hsp Reg Ren w/Milfoil");
								break;
							}
						case 4:
							{
								objDictionary.Add(1928, "Boat Over 115 Hsp Reg Ren w/Milfoil");
								break;
							}
						case 5:
							{
								objDictionary.Add(1929, "Boat PWC Reg Ren w/Milfoil");
								break;
							}
						case 6:
							{
								objDictionary.Add(1930, "Boat 0-10 Hsp Reg New/Roll w/Milfoil");
								break;
							}
						case 7:
							{
								objDictionary.Add(1931, "Boat 11-50 Hsp Reg New/Roll w/Milfoil");
								break;
							}
						case 8:
							{
								objDictionary.Add(1932, "Boat 51-115 Hsp Reg New/Roll w/Milfoil");
								break;
							}
						case 9:
							{
								objDictionary.Add(1933, "Boat Over 115 Hsp Reg New/Roll w/Milfoil");
								break;
							}
						case 10:
							{
								objDictionary.Add(1934, "Boat PWC Reg New/Roll w/Milfoil");
								break;
							}
						case 11:
							{
								objDictionary.Add(1939, "Milfoil Upgrade");
								break;
							}
						case 12:
							{
								objDictionary.Add(1840, "Resident Apprentice Hunt");
								break;
							}
						case 13:
							{
								objDictionary.Add(1841, "Nonresident Apprentice Hunt");
								break;
							}
						case 14:
							{
								objDictionary.Add(1842, "Nonresident Apprentice Small Game");
								break;
							}
						case 15:
							{
								objDictionary.Add(1843, "Resident Apprentice Archery");
								break;
							}
						case 16:
							{
								objDictionary.Add(1844, "Nonresident Apprentice Archery");
								break;
							}
						case 17:
							{
								objDictionary.Add(1845, "Resident Apprentice Crossbow");
								break;
							}
						case 18:
							{
								objDictionary.Add(1846, "Nonresident Apprentice Crossbow");
								break;
							}
						case 19:
							{
								objDictionary.Add(1847, "Nonresident November Bear Permit");
								break;
							}
						case 20:
							{
								objDictionary.Add(1850, "Complimentary Spring Turkey");
								break;
							}
					}
					//end switch
				}
				// intCount
				foreach (int varKey_foreach in objDictionary.Keys)
				{
					varKey = varKey_foreach;
					lngCode = varKey;
					strDescription = FCConvert.ToString(objDictionary[varKey]);
					rsM.OpenRecordset("SELECT * FROM MOSESTypes WHERE MOSESType = " + FCConvert.ToString(lngCode), "TWCR0000.vb1");
					if (rsM.EndOfFile())
					{
						rsM.AddNew();
						rsM.Set_Fields("MOSESType", lngCode);
						rsM.Set_Fields("MOSESTypeDescription", strDescription);
						rsM.Set_Fields("CRType", 0);
						rsM.Update();
						rsM.EndOfFile();
					}
				}
				// varKey
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Dec., 2007 MOSES Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void AddMOSESFields_20091110()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				Dictionary<object, object> objDictionary = new Dictionary<object, object>();
				int lngCode = 0;
				int intCount;
				// vbPorter upgrade warning: strDescription As string	OnWrite(object)
				string strDescription = "";
				// vbPorter upgrade warning: varKey As Variant --> As int
				int varKey;
				for (intCount = 1; intCount <= 3; intCount++)
				{
					switch (intCount)
					{
						case 1:
							{
								objDictionary.Add(1852, "Resident Spring & Fall Turkey");
								break;
							}
						case 2:
							{
								objDictionary.Add(1853, "Non-Resident Spring & Fall Turkey");
								break;
							}
						case 3:
							{
								objDictionary.Add(1854, "Second Spring Turkey");
								break;
							}
					}
					//end switch
				}
				// intCount
				foreach (int varKey_foreach in objDictionary.Keys)
				{
					varKey = varKey_foreach;
					lngCode = varKey;
					strDescription = FCConvert.ToString(objDictionary[varKey]);
					rsM.OpenRecordset("SELECT * FROM MOSESTypes WHERE MOSESType = " + FCConvert.ToString(lngCode), "TWCR0000.vb1");
					if (rsM.EndOfFile())
					{
						rsM.AddNew();
						rsM.Set_Fields("MOSESType", lngCode);
						rsM.Set_Fields("MOSESTypeDescription", strDescription);
						rsM.Set_Fields("CRType", 0);
						rsM.Update();
						rsM.EndOfFile();
					}
				}
				// varKey
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Dec., 2007 MOSES Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void VerifyMOSESFields_20080122()
		{
			// Tracker Reference: 12066
			try
			{
				// On Error GoTo ERROR_HANDLER
				Dictionary<object, object> objDictionary = new Dictionary<object, object>();
				clsDRWrapper rsM = new clsDRWrapper();
				int lngCode = 0;
				int intCount;
				// vbPorter upgrade warning: strDescription As string	OnWrite(object)
				string strDescription = "";
				// vbPorter upgrade warning: varKey As Variant --> As int
				int varKey;
				objDictionary.Add(1472, "Boat Registration Transfer");
				objDictionary.Add(1478, "Expanded Archery Antlerless Deer");
				objDictionary.Add(1479, "Expanded Archery Antlered Deer");
				objDictionary.Add(1480, "Boat Up to 10 Hsp. Registration");
				objDictionary.Add(1481, "Boat 11-50 Hsp. Registration");
				objDictionary.Add(1483, "Boat PWC Registration");
				objDictionary.Add(1487, "Boat Duplicate Stickers");
				objDictionary.Add(1488, "Boat Duplicate Registration");
				objDictionary.Add(1508, "Nonresident ATV Duplicate Stickers");
				objDictionary.Add(1509, "Nonresident ATV Duplicate Registration and Stickers");
				objDictionary.Add(1511, "Resident Snowmobile Duplicate Registration");
				objDictionary.Add(1513, "Resident Snowmobile Duplicate Sticker");
				objDictionary.Add(1515, "Nonresident ATV Transfer Registration");
				objDictionary.Add(1525, "Nonresident ATV Duplicate Registration");
				objDictionary.Add(1533, "Resident Snowmobile Transfer Registration");
				objDictionary.Add(1553, "Nonresident ATV Registration");
				objDictionary.Add(1564, "Nonresident Snowmobile Transfer Registration");
				objDictionary.Add(1565, "Nonresident 3-Day Snowmobile Registration");
				objDictionary.Add(1566, "Nonresident 10-Day Snowmobile Registration");
				objDictionary.Add(1567, "Nonresident Season Snowmobile Registration");
				objDictionary.Add(1568, "Nonresident Snowmobile Duplicate Registration and Sticker");
				objDictionary.Add(1575, "Resident Snowmobile Registration");
				objDictionary.Add(1580, "Resident ATV Registration");
				objDictionary.Add(1582, "Resident ATV Duplicate Registration");
				objDictionary.Add(1583, "Resident ATV Duplicate Stickers");
				objDictionary.Add(1584, "Resident ATV Transfer Registration");
				objDictionary.Add(1589, "Boat Duplicate Registration & Stickers");
				objDictionary.Add(1590, "Snowmobile Duplicate Registration & Stickers");
				objDictionary.Add(1591, "Resident ATV Duplicate Registration & Stickers");
				objDictionary.Add(1688, "Resident Atlantic Salmon");
				objDictionary.Add(1689, "Non-Resident Atlantic Salmon");
				objDictionary.Add(1690, "Non-Resident 3-Day Atlantic Salmon");
				objDictionary.Add(1691, "Non-Resident Junior Atlantic Salmon");
				objDictionary.Add(1692, "Resident Crossbow Hunting");
				objDictionary.Add(1693, "Nonresident Crossbow Hunting");
				objDictionary.Add(1694, "Alien Crossbow Hunting");
				objDictionary.Add(1695, "Resident Fall Turkey Hunting Permit");
				objDictionary.Add(1696, "Nonresident Fall Turkey Hunting Permit");
				objDictionary.Add(1697, "Resident Spring Turkey");
				objDictionary.Add(1698, "Nonresident Spring Turkey");
				objDictionary.Add(1699, "Resident Superpack");
				objDictionary.Add(1704, "Pheasant Hunting Permit");
				objDictionary.Add(1705, "Resident Hunting & Fishing Combo (Exchanged 1-Day Fish)");
				objDictionary.Add(1706, "Resident Hunting & Fishing Combo (Exchanged 3-Day Fish)");
				objDictionary.Add(1708, "Nonresident Small Game Hunting");
				objDictionary.Add(1710, "Res Service Depend Hunting and Fishing Combo");
				objDictionary.Add(1713, "Migratory Waterfowl Hunting Permit");
				objDictionary.Add(1720, "Alien Hunting & Fishing Combo");
				objDictionary.Add(1721, "Alien Season Fishing");
				objDictionary.Add(1722, "Alien Small Game Hunting");
				objDictionary.Add(1723, "Alien Archery Hunting");
				objDictionary.Add(1729, "Resident Serviceman Dependent Fishing");
				objDictionary.Add(1730, "Resident Serviceman Dependent Hunting");
				objDictionary.Add(1744, "Resident Archery Hunting & Fishing Combo");
				objDictionary.Add(1745, "Supersport License");
				objDictionary.Add(1748, "Nonresident 3-Day Small Game Hunting");
				objDictionary.Add(1749, "Resident Small Game Hunting");
				objDictionary.Add(1750, "Resident Junior Hunting");
				objDictionary.Add(1751, "Resident Hunting & Fishing Combo");
				objDictionary.Add(1752, "Resident Hunting");
				objDictionary.Add(1753, "Resident Fishing");
				objDictionary.Add(1754, "Resident Archery Hunting");
				objDictionary.Add(1755, "Nonresident Junior Hunting");
				objDictionary.Add(1757, "Nonresident Big Game Hunting");
				objDictionary.Add(1758, "Nonresident Archery Hunting");
				objDictionary.Add(1759, "Alien Big Game Hunting");
				objDictionary.Add(1762, "1-Day Fishing");
				objDictionary.Add(1763, "Resident Fishing (Exchanged 1-Day Fish)");
				objDictionary.Add(1765, "Nonresident 7-Day Fishing");
				objDictionary.Add(1766, "Nonresident Season Fishing");
				objDictionary.Add(1767, "Nonresident 15-Day Fishing");
				objDictionary.Add(1768, "Nonresident Season Fishing (Exchanged 15-Day Fish)");
				objDictionary.Add(1769, "3-Day Fishing");
				objDictionary.Add(1771, "Nonresident Junior Fishing");
				objDictionary.Add(1772, "Nonresident Hunting & Fishing Combo");
				objDictionary.Add(1780, "Lake & River Protection sticker - Maine Registered Boats");
				objDictionary.Add(1782, "Coyote Night Hunting Permit");
				objDictionary.Add(1783, "Lake & River Protection sticker - Other Registered Boats");
				objDictionary.Add(1786, "Resident Muzzleloader Hunting");
				objDictionary.Add(1787, "Nonresident Muzzleloader Hunting");
				objDictionary.Add(1788, "Alien Muzzleloader Hunting");
				objDictionary.Add(1790, "Resident Serviceman Hunt and Fish Combo");
				objDictionary.Add(1796, "Resident Bear Hunting");
				objDictionary.Add(1797, "Nonresident Bear Hunting");
				objDictionary.Add(1800, "Resident Fishing (Exchanged 3-Day Fish)");
				objDictionary.Add(1801, "Duplicate Recreational License");
				objDictionary.Add(1809, "Complimentary Resident Muzzleloader Hunting Permit");
				objDictionary.Add(1810, "Complimentary Pheasant Hunting Permit");
				objDictionary.Add(1813, "Complimentary Bear Hunt");
				objDictionary.Add(1821, "Complimentary Migratory Waterfowl Hunting Permit");
				objDictionary.Add(1825, "Migratory Waterfowl Upgrade Card");
				objDictionary.Add(1901, "Resident Over-70 Lifetime License");
				objDictionary.Add(1917, "Boat 51-115 Hsp Registration");
				objDictionary.Add(1918, "Boat 51-115 Hsp Reg (Rollover to Spouse)");
				objDictionary.Add(1921, "Boat Over 115 Hsp Registration");
				objDictionary.Add(1922, "Boat Over 115 Hsp Reg (Rollover to Spouse)");
				objDictionary.Add(2304, "Boat Up to 10 Hsp. Registration (Rollover to Spouse)");
				objDictionary.Add(2305, "Boat 11-50 Hsp. Registration (Rollover to Spouse)");
				objDictionary.Add(2306, "Boat PWC Registration (Rollover to Spouse)");
				objDictionary.Add(2315, "Nonresident ATV Registration (Rollover to Spouse)");
				objDictionary.Add(2319, "Nonresident Season Snowmobile Registration (Rollover to Spouse)");
				objDictionary.Add(2321, "Resident Snowmobile Registration (Rollover to Spouse)");
				objDictionary.Add(2322, "Resident ATV Registration (Rollover to Spouse)");
				foreach (int varKey_foreach in objDictionary.Keys)
				{
					varKey = varKey_foreach;
					lngCode = varKey;
					strDescription = FCConvert.ToString(objDictionary[varKey]);
					rsM.OpenRecordset("SELECT * FROM MOSESTypes WHERE MOSESType = " + FCConvert.ToString(lngCode), modExtraModules.strCRDatabase);
					if (rsM.EndOfFile())
					{
						rsM.AddNew();
						rsM.Set_Fields("MOSESType", lngCode);
						rsM.Set_Fields("MOSESTypeDescription", strDescription);
						rsM.Set_Fields("CRType", 0);
						rsM.Update();
					}
					else
					{
						if (FCConvert.ToString(rsM.Get_Fields_String("MOSESTypeDescription")) != strDescription)
						{
							rsM.Edit();
							rsM.Set_Fields("MOSESTypeDescription", strDescription);
							rsM.Update();
						}
					}
				}
				// varKey
				objDictionary = null;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Verifying MOSES Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void AddMOSESFields_20120210()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				Dictionary<object, object> objDictionary = new Dictionary<object, object>();
				int lngCode = 0;
				int intCount;
				// vbPorter upgrade warning: strDescription As string	OnWrite(object)
				string strDescription = "";
				// vbPorter upgrade warning: varKey As Variant --> As int
				int varKey;
				for (intCount = 1; intCount <= 2; intCount++)
				{
					switch (intCount)
					{
						case 1:
							{
								objDictionary.Add(1871, "Saltwater Fishing Registry");
								break;
							}
						case 2:
							{
								objDictionary.Add(1430, "Resident Over 70 Lifetime License Upgrade");
								break;
							}
					}
					//end switch
				}
				// intCount
				foreach (int varKey_foreach in objDictionary.Keys)
				{
					varKey = varKey_foreach;
					lngCode = varKey;
					strDescription = FCConvert.ToString(objDictionary[varKey]);
					rsM.OpenRecordset("SELECT * FROM MOSESTypes WHERE MOSESType = " + FCConvert.ToString(lngCode), "TWCR0000.vb1");
					if (rsM.EndOfFile())
					{
						rsM.AddNew();
						rsM.Set_Fields("MOSESType", lngCode);
						rsM.Set_Fields("MOSESTypeDescription", strDescription);
						rsM.Set_Fields("CRType", 0);
						rsM.Update();
						rsM.EndOfFile();
					}
				}
				// varKey
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Dec., 2011 MOSES Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void AddMOSESFields_20120423()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				Dictionary<object, object> objDictionary = new Dictionary<object, object>();
				int lngCode = 0;
				int intCount;
				// vbPorter upgrade warning: strDescription As string	OnWrite(object)
				string strDescription = "";
				// vbPorter upgrade warning: varKey As Variant --> As int
				int varKey;
				for (intCount = 1; intCount <= 2; intCount++)
				{
					switch (intCount)
					{
						case 1:
							{
								objDictionary.Add(1884, "NR 7-Day ATV Registration Renewal");
								break;
							}
						case 2:
							{
								objDictionary.Add(1885, "NR 7-Day ATV Registration New and Rollover");
								break;
							}
					}
					//end switch
				}
				// intCount
				foreach (int varKey_foreach in objDictionary.Keys)
				{
					varKey = varKey_foreach;
					lngCode = varKey;
					strDescription = FCConvert.ToString(objDictionary[varKey]);
					rsM.OpenRecordset("SELECT * FROM MOSESTypes WHERE MOSESType = " + FCConvert.ToString(lngCode), "TWCR0000.vb1");
					if (rsM.EndOfFile())
					{
						rsM.AddNew();
						rsM.Set_Fields("MOSESType", lngCode);
						rsM.Set_Fields("MOSESTypeDescription", strDescription);
						rsM.Set_Fields("CRType", 0);
						rsM.Update();
						rsM.EndOfFile();
					}
					varKey = 0;
				}
				// varKey
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Dec., 2012 MOSES Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void CheckOrientationRegistryEntries()
		{
			// Tracker Reference: 13318
			if (modRegistry.GetRegistryKey("CLAuditLandscape", "CR") == "")
			{
				modRegistry.SaveRegistryKey("CLAuditLandscape", "True", "CR");
				Statics.gblnPrintCLAuditLandscape = true;
			}
			else
			{
				Statics.gblnPrintCLAuditLandscape = FCConvert.CBool(modRegistry.GetRegistryKey("CLAuditLandscape", "CR") == "True");
			}
			if (modRegistry.GetRegistryKey("UTAuditLandscape", "CR") == "")
			{
				modRegistry.SaveRegistryKey("UTAuditLandscape", "True", "CR");
				Statics.gblnPrintUTAuditLandscape = true;
			}
			else
			{
				Statics.gblnPrintUTAuditLandscape = FCConvert.CBool(modRegistry.GetRegistryKey("UTAuditLandscape", "CR") == "True");
			}
			if (modRegistry.GetRegistryKey("MainAuditLandscape", "CR") == "")
			{
				modRegistry.SaveRegistryKey("MainAuditLandscape", "True", "CR");
				Statics.gboolDailyAuditLandscape = true;
			}
			else
			{
				Statics.gboolDailyAuditLandscape = FCConvert.CBool(modRegistry.GetRegistryKey("MainAuditLandscape", "CR") == "True");
			}
			if (modRegistry.GetRegistryKey("ARAuditLandscape", "CR") == "")
			{
				modRegistry.SaveRegistryKey("ARAuditLandscape", "True", "CR");
				Statics.gblnPrintARAuditLandscape = true;
			}
			else
			{
				Statics.gblnPrintARAuditLandscape = FCConvert.CBool(modRegistry.GetRegistryKey("ARAuditLandscape", "CR") == "True");
			}
		}
		//FC:FINAL:AM: code not needed
		//public static string BrowseForFolder(Form owner, ref string Title, ref string StartDir)
		//{
		//	string BrowseForFolder = "";
		//	// Opens a Treeview control that displays the directories in a computer
		//	// Borrowed from http://www.tek-tips.com/faqs.cfm?fid=867
		//	int lpIDList;
		//	string szTitle;
		//	string sBuffer = "";
		//	modGlobalConstants.BrowseInfo tBrowseInfo = new modGlobalConstants.BrowseInfo();
		//	Statics.gstrCurrentDirectory = StartDir + "\0";
		//	szTitle = Title;
		//	tBrowseInfo.hWndOwner = owner.Handle.ToInt32();
		//	tBrowseInfo.lpszTitle = modAPIsConst.lstrcatWrp(ref szTitle, "");
		//	tBrowseInfo.ulFlags = modAPIsConst.BIF_RETURNONLYFSDIRS + modAPIsConst.BIF_DONTGOBELOWDOMAIN + modAPIsConst.BIF_STATUSTEXT;
		//	tBrowseInfo.lpfnCallback = GetAddressofFunction_2(BrowseCallbackProc);
		//	// get address of function.
		//	lpIDList = modAPIsConst.SHBrowseForFolder(ref tBrowseInfo);
		//	if (FCConvert.ToBoolean((lpIDList)))
		//	{
		//		sBuffer = Strings.Space(modAPIsConst.MAX_PATH);
		//		modAPIsConst.SHGetPathFromIDListWrp(lpIDList, ref sBuffer);
		//		sBuffer = Strings.Left(sBuffer, Strings.InStr(sBuffer, "\0", CompareConstants.vbBinaryCompare) - 1);
		//		BrowseForFolder = sBuffer;
		//	}
		//	else
		//	{
		//		BrowseForFolder = "";
		//	}
		//	return BrowseForFolder;
		//}
		//public delegate int Address_BrowseCallbackProc(int hwnd, int uMsg, int lp, int pData);
		//private static int BrowseCallbackProc(int hwnd, int uMsg, int lp, int pData)
		//{
		//	int BrowseCallbackProc = 0;
		//	int lpIDList;
		//	int ret = 0;
		//	string sBuffer = "";
		//	/*? On Error Resume Next  */// Sugested by MS to prevent an error from
		//	// propagating back into the calling process.
		//	switch (uMsg)
		//	{
		//		case modAPIsConst.BFFM_INITIALIZED:
		//			{
		//				modAPIsConst.SendMessage(hwnd, modAPIsConst.BFFM_SETSELECTION, 1, Statics.gstrCurrentDirectory);
		//				break;
		//			}
		//		case modAPIsConst.BFFM_SELCHANGED:
		//			{
		//				sBuffer = Strings.Space(modAPIsConst.MAX_PATH);
		//				ret = modAPIsConst.SHGetPathFromIDListWrp(lp, ref sBuffer);
		//				if (ret == 1)
		//				{
		//					modAPIsConst.SendMessage(hwnd, modAPIsConst.BFFM_SETSTATUSTEXT, 0, sBuffer);
		//				}
		//				break;
		//			}
		//	}
		//	//end switch
		//	BrowseCallbackProc = 0;
		//	return BrowseCallbackProc;
		//}
		// This function allows you to assign a function pointer to a vaiable.
		// vbPorter upgrade warning: Add As int	OnWrite(void*)
		private static int GetAddressofFunction_2(int Add)
		{
			return GetAddressofFunction(ref Add);
		}

		private static int GetAddressofFunction(ref int Add)
		{
			int GetAddressofFunction = 0;
			GetAddressofFunction = Add;
			return GetAddressofFunction;
		}

		private static void GetGlobalPrintAdjustments()
		{
			// kgk 11-4-2011 trocl-823  Printer adjustments not working from CR
			string strTemp = "";
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentCMFLaser", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblCMFAdjustment = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblCMFAdjustment = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentLabels", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLabelsAdjustment = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLabelsAdjustment = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentLabelsDMH", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLabelsAdjustmentDMH = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLabelsAdjustmentDMH = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "AdjustmentLabelsDMV", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLabelsAdjustmentDMV = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLabelsAdjustmentDMV = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLienAdjustmentTop", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLienAdjustmentTop = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLienAdjustmentTop = 0;
			}
			// kk04222014   trocl-1156
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLienAdjustmentBottom", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLienAdjustmentBottom = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLienAdjustmentBottom = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLDNAdjustmentTop", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLDNAdjustmentTop = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLDNAdjustmentTop = 0;
			}
			// kk04222014   trocl-1156
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "gdblLDNAdjustmentBottom", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblLDNAdjustmentBottom = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblLDNAdjustmentBottom = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "ReminderFormAdjustH", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblReminderFormAdjustH = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblReminderFormAdjustH = 0;
			}
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL", "ReminderFormAdjustV", ref strTemp);
			if (Conversion.Val(strTemp) != 0)
			{
				Statics.gdblReminderFormAdjustV = FCConvert.ToDouble(strTemp);
			}
			else
			{
				Statics.gdblReminderFormAdjustV = 0;
			}
		}

		public static void CleanUpPaymentTypeMasters()
		{
			// this routine will remove entries from the CheckMaster table that are not linked to a receipt
			// this will stop the duplicate checks showing up on the Check Report when a Key is reused
			clsDRWrapper rsCheck = new clsDRWrapper();
			clsDRWrapper rsReceipt = new clsDRWrapper();
			int lngWaitAmount;
			lngWaitAmount = frmWait.InstancePtr.prgProgress.Maximum;
			// kk07092014 trocrs-24  Speed this up and don't delete in the rs in a loop
			// rsReceipt.OpenRecordset "SELECT ReceiptKey FROM Receipt", strCRDatabase
			rsCheck.OpenRecordset("SELECT * FROM CheckMaster", modExtraModules.strCRDatabase);
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Cleaning Check Master File", true, rsCheck.RecordCount());
			while (!rsCheck.EndOfFile())
			{
				//Application.DoEvents();
				frmWait.InstancePtr.IncrementProgress();
				// kk07092014 trocrs-24  Speed this up and don't delete in the rs in a loop
				// rsReceipt.FindFirstRecord "ReceiptKey", rsCheck.Fields("ReceiptNumber")
				// If rsReceipt.NoMatch Then
				// if there is no matching receipt then remove this entry
				// rsCheck.Delete
				// End If
				rsReceipt.OpenRecordset("SELECT ReceiptKey FROM Receipt WHERE ReceiptKey = " + rsCheck.Get_Fields_Int32("ReceiptNumber"), modExtraModules.strCRDatabase);
				if (rsReceipt.EndOfFile())
				{
					// if there is no matching receipt then remove this entry
					rsCheck.Execute("DELETE FROM CheckMaster WHERE ID = " + rsCheck.Get_Fields_Int32("ID"), modExtraModules.strCRDatabase);
				}
				rsCheck.MoveNext();
			}
			// clears the CC Master
			// kk07092014 trocrs-24  Speed this up and don't delete in the rs in a loop
			// rsReceipt.OpenRecordset "SELECT ReceiptKey FROM Receipt", strCRDatabase
			rsCheck.OpenRecordset("SELECT * FROM CCMaster", modExtraModules.strCRDatabase);
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Cleaning CC Master File", true, rsCheck.RecordCount());
			while (!rsCheck.EndOfFile())
			{
				frmWait.InstancePtr.IncrementProgress();
				//Application.DoEvents();
				// kk07092014 trocrs-24  Speed this up and don't delete in the rs in a loop
				// rsReceipt.FindFirstRecord "ReceiptKey", rsCheck.Fields("ReceiptNumber")
				// If rsReceipt.NoMatch Then
				// if there is no matching receipt then remove this entry
				// rsCheck.Delete
				// End If
				rsReceipt.OpenRecordset("SELECT ReceiptKey FROM Receipt WHERE ReceiptKey = " + rsCheck.Get_Fields_Int32("ReceiptNumber"), modExtraModules.strCRDatabase);
				if (rsReceipt.EndOfFile())
				{
					// if there is no matching receipt then remove this entry
					rsCheck.Execute("DELETE FROM CCMaster WHERE ID = " + rsCheck.Get_Fields_Int32("ID"), modExtraModules.strCRDatabase);
				}
				rsCheck.MoveNext();
			}
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Cleaning CheckMaster", true, lngWaitAmount);
		}

		private static void CheckDatabaseStructure()
		{
			clsDRWrapper rsDB = new clsDRWrapper();
			//Application.DoEvents();
			// Make sure Receipt table has ReceiptKey instead of ID
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ReceiptKey' AND TABLE_NAME = 'Receipt'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("EXEC sp_rename 'dbo.Receipt.ID', 'ReceiptKey', 'COLUMN'", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure Version is in the CashRec table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Version' AND TABLE_NAME = 'CashRec'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[CashRec] ADD [Version] nvarchar(50) NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure NamePartyID is in the Archive table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'NamePartyID' AND TABLE_NAME = 'Archive'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[Archive] ADD [NamePartyID] int NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure NamePartyID is in the LastYearArchive table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'NamePartyID' AND TABLE_NAME = 'LastYearArchive'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[LastYearArchive] ADD [NamePartyID] int NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure PaidByPartyID is in the Receipt table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PaidByPartyID' AND TABLE_NAME = 'Receipt'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[Receipt] ADD [PaidByPartyID] int NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure PaidByPartyID is in the LastYearReceipt table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PaidByPartyID' AND TABLE_NAME = 'LastYearReceipt'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[LastYearReceipt] ADD [PaidByPartyID] int NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure EPmtAcctID is in the Type table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'EPmtAcctID' AND TABLE_NAME = 'Type'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[Type] ADD [EPmtAcctID] nvarchar(100) NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure EPaymentDemoMode is in the CashRec table
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'EPaymentDemoMode' AND TABLE_NAME = 'CashRec'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[CashRec] ADD [EPaymentDemoMode] bit NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// kk 111212 trout-348  Add Close Out by Department option
			rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'CloseOutByDept' AND TABLE_NAME = 'CashRec'", modExtraModules.strCRDatabase);
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("ALTER TABLE.[dbo].[CashRec] ADD [CloseOutByDept] int NULL", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure Archive is indexed on ReceiptNumber
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_Archive_ReceiptNumber'");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_Archive_ReceiptNumber] ON [dbo].[Archive] " + "([ReceiptNumber] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure Archive is indexed on TellerCloseout
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_Archive_TellerCloseout'");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_Archive_TellerCloseout] ON [dbo].[Archive] " + "([TellerCloseout] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure Archive is indexed on DailyCloseout
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_Archive_DailyCloseout'");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_Archive_DailyCloseout] ON [dbo].[Archive] " + "([DailyCloseout] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// kk 01092014 Add Indexes to Receipt, LastYearArchive and LastYearReceipt tables
			// Make sure Receipt is indexed on ReceiptNumber
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_Receipt_ReceiptNumber'");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_Receipt_ReceiptNumber] ON [dbo].[Receipt] " + "([ReceiptNumber] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure LastYearArchive is indexed on ReceiptNumber
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_LastYearArchive_ReceiptNumber'");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_LastYearArchive_ReceiptNumber] ON [dbo].[LastYearArchive] " + "([ReceiptNumber] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure LastYearReceipt is indexed on ReceiptKey
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_LastYearReceipt_ReceiptKey'");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_LastYearReceipt_ReceiptKey] ON [dbo].[LastYearReceipt] " + "([ReceiptKey] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// Make sure LastYearReceipt is indexed on ReceiptNumber
			rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_LastYearReceipt_ReceiptNumber'");
			if (rsDB.EndOfFile())
			{
				rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_LastYearReceipt_ReceiptNumber] ON [dbo].[LastYearReceipt] " + "([ReceiptNumber] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", modExtraModules.strCRDatabase);
			}
			//Application.DoEvents();
			// 04012015 trocr-419 Make sure non-AR receipts don't have ARBillType set
			rsDB.OpenRecordset("SELECT TOP 1 ID FROM Archive WHERE DailyCloseOut > 0 AND ReceiptType <> 97 AND ARBillType > 0", modExtraModules.strCRDatabase);
			if (!rsDB.EndOfFile())
			{
				rsDB.Execute("UPDATE Archive SET ARBillType = 0 WHERE DailyCloseOut > 0 AND ReceiptType <> 97 AND ARBillType > 0", modExtraModules.strCRDatabase);
			}
		}

		public static double Round(double dValue, short iDigits)
		{
			double Round = 0;
			// Round = Int(dValue * (10 ^ iDigits) + 0.5) / (10 ^ iDigits)
			Round = Conversion.Int(FCConvert.ToDouble(dValue * (Math.Pow(10, iDigits)) + 0.5)) / (Math.Pow(10, iDigits));
			return Round;
		}

		public class StaticVariables
		{
            // ********************************************************
            // LAST UPDATED   :               02/27/2007              *
            // MODIFIED BY    :               Jim Bertolino           *
            // *
            // DATE           :               10/01/2002              *
            // WRITTEN BY     :               Jim Bertolino           *
            // *
            // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
            // ********************************************************
            //=========================================================
            public bool moduleInitialized = false;
            public string DatabaseName = "";
			public bool Networked;
			public bool gboolDetail;
			// this is to show the total even if there is only one line on the receipt
			public int gintPassQuestion;
			// kk01282015 trocrs-32  Classes to Automatically post CR Journals
			public clsBudgetaryPosting tPostInfo;
			// these variables are all of the information from the CR Database
			// Collection of journals
			public string gstrAuditReportFormat = "";
			public int gintReceiptLength;
			public int gintExtraLines;
			public string gstrTopComments = "";
			public string gstrBottomComments = "";
			public bool gboolCDConnected;
			public string gstrCDCode = "";
			public int gintRepeatCount;
			public string gstrCDPortID = "";
			public bool gboolNarrowReceipt;
			public bool gboolAutoCheckRecEntry;
			public string gstrTeller = "";
			public bool gboolTellerReportWithAudit;
			public bool gboolTellerCheckReportWithAudit;
			public bool gboolSkipPageForTeller;
			public bool gboolCashTellersOutSeperately;
			public bool gboolAuditSeqByType;
			public bool gboolDefaultYear;
			public string gstrPrintSigLine = "";
			public bool gboolReprint;
			// Public gstrCurrentYearAccount           As String       'this is the account that the monies are entered into until the end of the FY
			// Public glngCurrentYear                  As Long         'this is the current FY, if it is set to zero then do not use this function
			// this tells the Receipt Report that it is a reprinted receipt
			public bool gboolPasswordProtectAudit;
			public string gstrAuditPassword = "";
			public bool gboolSP200Printer;
			// this will be set to true when the user is setup with SP200 printers
			public string gstrLastYearBilled = "";
			public int gintReceiptOffSet;
			// this is how many spaces will be added to the left side of the receipt to make sure that the sp200 printer will work correctly
			public bool gboolCloseMVPeriod;
			// true if a daily audit will close out a MV period
			public bool gboolDailyAuditByAlpha;
			// false if the user wants the breakdown of receipts in receipt order under the type breakdown, true if they want alphanumeric
			public bool gboolAllowPartialAudit;
			public bool gboolPendingRI;
			// if this is true, then frmReceiptInput will act differently
			public bool gboolPendingTransactions;
			// if this is true, then the pending RI option will show, the daily audit options will be different, ect
			public bool gboolWideReceiptNoMargin;
			// this is true when the user is using a wide receipt format and does not want a top margin
			public int gintCheckReportOrder;
			// if this is true then change the order of the check report to check number rather than receipt number
			public bool gboolShowLastCLAccountInCR;
			// if this is true then the last account accessed in CL will show in the account box by default through CR
			public double gdblReceiptLenAdjust;
			// this will hold the adjustment of the receipt length
			public bool gboolRecieptCompacted;
			// this will compact a wide receipt format so that more can fit in a smaller space
			public bool gboolRecieptThin;
			// this will compact a wide receipt format so that more can fit in a smaller space AND has margins on the top and bottom
			public double gdblAuditMarginAdjustmentTop;
			// this will adjust the top margin of the cr daily audit
			public bool gboolBDExclusive;
			// if this is true, then it will force everyone out of the BD DB before running an audit
			public bool gboolUseUSLExport;
			// if this is True, then the Audit Summary will export a file to the data directory so the USL system can import it
			public bool gboolDailyAuditLandscape;
			// if this is True then the audit will show up in landscape orinetation else in portrait
			public bool gboolMultipleTowns;
			// This is true if the town is a grouping of multiple towns using one system
			public bool gboolCLNarrowReceipt;
			// Dummy variable for CL to create receipts by itself
			public bool gboolClearPaidBy;
			// Hold preference for clearing Paid By field during multiple transactions (non-batch)
			public string gstrClearPaidByOption = "";
			// Hold preference for clearing Paid By field for All Tax payments or just multiple transactions (S = All ; M = Multiple)
			public bool gblnPrintCLAuditLandscape;
			// This is true if user selected Landscape as the last orientation when printing CL Audit
			public bool gblnPrintUTAuditLandscape;
			// This is true if user selected Landscape as the last orientation when printing UT Audit
			public bool gblnPrintARAuditLandscape;
			// This is true if user selected Landscape as the last orientation when printing AR Audit
			public bool gblnEPaymentActivated;
			// This is true if E-Payments have been activated
			public bool gblnNoVISANONTaxPayments;
			// This is true if VISA Payments are not allowed on any type of non Real Estate Tax transactions
			public bool gblnNoVISAPayments;
			// This is true if VISA Payments are not allowed at all
			public string gstrEPaymentPortal = "";
			// This is the Portal Type for electronic payments
			public bool gblnAcceptEChecks;
			// This is true if user selects a portal to allows echecks and chooses to accept e-checks
			public bool gblnEPymtTestMode;
			// This is true if the user has selected to be in test mode for the CSI portal
			public bool gblnPrintExtraReceipt;
			// This is true if the user wants to print an extra copy of the receipt any time there is a CC transaction
			public string gstrCurrentDirectory = string.Empty;
			// The current directory for browse for folder option
			public string gstrCCOwnerName = "";
			// For E-Payment Credit Cards - Owner Name
			public string gstrCCOwnerStreet = "";
			// For E-Payment Credit Cards - Owner Billing Address
			public string gstrCCOwnerCity = "";
			// For E-Payment Credit Cards - Owner Billing City
			public string gstrCCOwnerState = "";
			// For E-Payment Credit Cards - Owner Billing State
			public string gstrCCOwnerZip = "";
			// For E-Payment Credit Cards - Owner Billing Zip
			public string gstrCCOwnerCountry = "";
			// For E-Payment Credit Cards - Owner Billing Country
			public bool gblnPrintCCReport;
			// This is true if user selects to print the Credit Card report with the Daily Audit
			public int gintCCReportOrder;
			// Which order the Credit Card Report should print in ; 0 = By Receipt Within Type, 1 = By Amount
			public bool gblnReceiptPrinterPrompt;
			// If True, User will be prompted to select a Receipt Printer each time receipt is printed ; Tracker Reference: 17676
			public string strOldArchiveSQL = "";
			public string strNewArchiveSQL = "";
			public string strCurrentDailyAuditSQL = "";
			public string strTotalPositiveReceiptsSQL = string.Empty;
			// Thsi is a global variable letting us knwo if the use projects flag is set in Budgetary
			public bool ProjectFlag;
			public int glngCurrentCloseOut;
			public int glngARCurrentCloseOut;
			// kgk 08-21-2012  Add full db paths for joins
			public string strDbRE = string.Empty;
			public string strDbPP = string.Empty;
			public string strDbUT = string.Empty;
			public string strDbGNC = string.Empty;
			public string strDbGNS = string.Empty;
			public string strDbCP = string.Empty;
			// this is a global recordset for sarReceiptDetail
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			//public clsDRWrapper grsType = new clsDRWrapper();
			public clsDRWrapper grsType_AutoInitialized;

			public clsDRWrapper grsType
			{
				get
				{
					if (grsType_AutoInitialized == null)
					{
						grsType_AutoInitialized = new clsDRWrapper();
					}
					return grsType_AutoInitialized;
				}
				set
				{
					grsType_AutoInitialized = value;
				}
			}
			// these variables are from modGNbas
			public bool boolLoadHide;
			public int gintSecurityID;
			public FundMonies[] garrFundInfo = new FundMonies[99 + 1];
			// collecions variable
			public int gintBasis;
			// There is only on Interest Basis based on CL appies to UT also
			public bool gboolUseSigFile;
			// This is never initialized in CR
			public double gdblLDNAdjustmentTop;
			public double gdblLDNAdjustmentBottom;
			// kk04222014  trocl-1156
			public double gdblLDNAdjustmentSide;
			// kk02092016  trout-1197
			public double gdblLienSigAdjust;
			// Thess are defined here but never used in CR; included to prevent compile errors
			// Collections and Utility Billing Globals
			// Public gdblUTLDNAdjustmentBottom        As Double
			// Public gdblUTLDNAdjustmentTop           As Double
			// Utility Billing variables - these are declared in modUTCalculations
			// kk060112014  Defined in modSignatureFile    Public gstrTreasurerSigPassword         As String
			// kk02092016
			public double gdblCMFAdjustment;
			public double gdblLabelsAdjustment;
			public double gdblLabelsAdjustmentDMH;
			public double gdblLabelsAdjustmentDMV;
			public double gdblLienAdjustmentTop;
			public double gdblLienAdjustmentBottom;
			// kk04222014  trocl-1156
			public double gdblLienAdjustmentSide;
			// kk02172016  trout-1197
			public double gdblReminderFormAdjustH;
			public double gdblReminderFormAdjustV;
			// budgetary variables
			public int glngCashFund;
			public int glngDueTo;
			public int glngDueFrom;
			public int glngExpControl;
			public int glngRevControl;
			public int glngCashAccount;
			//public Address_BrowseCallbackProc Ptr_BrowseCallbackProc = new Address_BrowseCallbackProc(BrowseCallbackProc);
			//FC:FINAL:AM
			public cSettingsController setCont = new cSettingsController();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
