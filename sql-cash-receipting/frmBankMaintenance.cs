﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmBankMaintenance.
	/// </summary>
	public partial class frmBankMaintenance : BaseForm
	{
		public frmBankMaintenance()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBankMaintenance InstancePtr
		{
			get
			{
				return (frmBankMaintenance)Sys.GetInstance(typeof(frmBankMaintenance));
			}
		}

		protected frmBankMaintenance _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/20/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/07/2003              *
		// ********************************************************
		clsDRWrapper rsBank = new clsDRWrapper();
		bool boolLoaded;
		int lngColName;
		int lngColRT;
		int lngColKey;
		int lngColCheck;
		int lngColDirty;

		private void frmBankMaintenance_Activated(object sender, System.EventArgs e)
		{
			// if this form has not been loaded then format and fill the grid
			if (!boolLoaded)
			{
				FormatGrid();
				FillGrid();
				boolLoaded = true;
			}
			SetGridHeight();
			// set the height/position of the grid
		}

		private void frmBankMaintenance_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmBankMaintenance_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			// this sets the columns
			lngColName = 1;
			lngColRT = 2;
			lngColKey = 3;
			lngColDirty = 4;
			lngColCheck = 0;
		}

		private void frmBankMaintenance_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int lngCT;
			for (lngCT = vsBank.Rows - 1; lngCT >= 1; lngCT--)
			{
				if (lngCT <= vsBank.Rows - 1)
				{
					// double check in case of deletions
					if (Strings.Trim(vsBank.TextMatrix(lngCT, lngColName)) != "" || Strings.Trim(vsBank.TextMatrix(lngCT, lngColRT)) != "")
					{
						if (Conversion.Val(vsBank.TextMatrix(lngCT, lngColDirty)) != 0)
						{
							if (Strings.Trim(vsBank.TextMatrix(lngCT, lngColName)) == "" || Strings.Trim(vsBank.TextMatrix(lngCT, lngColRT)) == "")
							{
								switch (MessageBox.Show("No bank name or routing transit number have been specified, continue without saving?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
								{
									case DialogResult.Yes:
										{
											// delete the bank
											DeleteBank_6(lngCT, true);
											lngCT -= 1;
											// this will back lngCT back to the row that it was just on, because a row has been removed
											break;
										}
									case DialogResult.No:
										{
											vsBank.Select(lngCT, lngColName);
											e.Cancel = true;
											break;
											break;
										}
									case DialogResult.Cancel:
										{
											e.Cancel = true;
											return;
										}
								}
								//end switch
							}
							else
							{
								switch (MessageBox.Show("There is data that has been changed, would you like to exit without saving?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
								{
									case DialogResult.Yes:
										{
											break;
											break;
										}
									case DialogResult.No:
										{
											SaveAllDirtyRows();
											break;
											break;
										}
									case DialogResult.Cancel:
										{
											e.Cancel = true;
											return;
										}
								}
								//end switch
							}
						}
					}
					else
					{
						DeleteBank_6(lngCT, true);
						// lngCT = lngCT - 1
					}
				}
			}
			boolLoaded = false;
			//MDIParent.InstancePtr.Focus();
		}

		private void frmBankMaintenance_Resize(object sender, System.EventArgs e)
		{
			// this will resize all of the frames/grids whenever the form is resized
			SetGridHeight();
		}

		private void mnuFileAddBank_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will add a record in the bank table and the user is responsible for inputting the data correctly
				if (Strings.Trim(rsBank.Name()) == "")
				{
					rsBank.OpenRecordset("SELECT * FROM Bank", modExtraModules.strCRDatabase);
				}
				rsBank.AddNew();
				vsBank.AddItem("");
				vsBank.TextMatrix(vsBank.Rows - 1, lngColKey, FCConvert.ToString(rsBank.Get_Fields_Int32("ID")));
				vsBank.TextMatrix(vsBank.Rows - 1, lngColDirty, FCConvert.ToString(1));
				rsBank.Update(true);
				// resize the grid
				SetGridHeight();
				vsBank.Select(vsBank.Rows - 1, lngColName);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Bank", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuFileAddBank_Click()
		{
			mnuFileAddBank_Click(mnuFileAddBank, new System.EventArgs());
		}

		private void mnuFileDeleteBank_Click(object sender, System.EventArgs e)
		{
			if (vsBank.Row > 0)
			{
				DeleteBank(vsBank.Row);
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			vsBank.Select(0, 0);
			Close();
		}

		private void FormatGrid()
		{
			int wid = 0;
			wid = vsBank.WidthOriginal;
			vsBank.Cols = 5;
			vsBank.ColWidth(lngColCheck, 0);
			// checkbox
			vsBank.ColWidth(lngColName, FCConvert.ToInt32(wid * 0.5));
			// name
			vsBank.ColWidth(lngColDirty, 0);
			// 0 data did not changed, <> 0 data changed
			vsBank.ColWidth(lngColKey, 0);
			// ID
			vsBank.ColWidth(lngColRT, FCConvert.ToInt32(wid * 0.45));
			// routing transit number
			vsBank.TextMatrix(0, lngColName, "Name");
			vsBank.TextMatrix(0, lngColRT, "Routing Transit Number");
			vsBank.ExtendLastCol = true;
			vsBank.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsBank.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsBank.ColAlignment(lngColRT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBank.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void FillGrid()
		{
			vsBank.Rows = 1;
			rsBank.OpenRecordset("SELECT * FROM Bank", modExtraModules.strCRDatabase);
			while (!rsBank.EndOfFile())
			{
				// .AddItem rsBank.Fields("Name") & vbTab & "0" & vbTab & rsBank.Fields("ID") & vbTab & rsBank.Fields("RoutingTransit")
				vsBank.AddItem("");
				vsBank.TextMatrix(vsBank.Rows - 1, lngColName, FCConvert.ToString(rsBank.Get_Fields_String("Name")));
				vsBank.TextMatrix(vsBank.Rows - 1, lngColDirty, "0");
				vsBank.TextMatrix(vsBank.Rows - 1, lngColKey, FCConvert.ToString(rsBank.Get_Fields_Int32("ID")));
				vsBank.TextMatrix(vsBank.Rows - 1, lngColRT, FCConvert.ToString(rsBank.Get_Fields_String("RoutingTransit")));
				rsBank.MoveNext();
			}
			SetGridHeight();
		}

		private void SetGridHeight()
		{
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			//rptBankSummary.InstancePtr.Show(App.MainForm);
			frmReportViewer.InstancePtr.Init(rptBankSummary.InstancePtr);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			vsBank.Select(0, 0);
			SaveAllRows();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			vsBank.Select(0, 0);
			SaveAllRows();
			Close();
		}

		private void vsBank_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						// the user hits the delete ID when a bank is selected
						DeleteBank(vsBank.Row);
						break;
					}
				case Keys.Space:
					{

						break;
					}
				case Keys.Insert:
					{
						KeyCode = 0;
						if (mnuFileAddBank.Visible && mnuFileAddBank.Enabled)
						{
							mnuFileAddBank_Click();
						}
						break;
					}
			}
			//end switch
			// HighlightSelectedLines
		}

		private void vsBank_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (vsBank.Col == lngColDirty)
			{
			}
			else
			{
				vsBank.TextMatrix(vsBank.Row, lngColDirty, FCConvert.ToString(1));
				// this will tell me that the row changed
			}
			// HighlightSelectedLines
		}

		private void vsBank_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (vsBank.Row > 0)
			{
				vsBank.EditCell();
			}
			// HighlightSelectedLines
		}

		private void vsBank_RowColChange(object sender, System.EventArgs e)
		{
			if (vsBank.Row > 0)
			{
				vsBank.EditCell();
			}
			// HighlightSelectedLines
		}

		private void vsBank_StartEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsBank.Col == lngColDirty)
			{
				return;
			}
			else
			{
				vsBank.EditSelStart = 0;
				vsBank.EditSelLength = vsBank.EditText.Length;
			}
		}

		private void vsBank_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsBank.GetFlexRowIndex(e.RowIndex);
			int col = vsBank.GetFlexColIndex(e.ColumnIndex);
			if (col == lngColDirty)
			{
			}
			else
			{
				vsBank.TextMatrix(row, lngColDirty, FCConvert.ToString(1));
				// this will tell me that the row changed
			}
		}

		private bool SaveRow(ref int lngRW)
		{
			bool SaveRow = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will save the row passed in even if it was not marked as changed
				int lngKey;
				lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBank.TextMatrix(lngRW, lngColKey))));
				if (lngKey != 0)
				{
					if (ValidateBankInformation(ref lngRW))
					{
						// update the record
						rsBank.DefaultDB = modExtraModules.strCRDatabase;
						if (rsBank.Execute("UPDATE Bank SET Name = '" + vsBank.TextMatrix(lngRW, lngColName) + "', RoutingTransit = '" + vsBank.TextMatrix(lngRW, lngColRT) + "' WHERE ID = " + vsBank.TextMatrix(lngRW, lngColKey), "TWCR0000.vb1"))
						{
							SaveRow = true;
						}
						else
						{
							SaveRow = false;
						}
					}
					else
					{
						SaveRow = false;
					}
				}
				return SaveRow;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				SaveRow = false;
			}
			return SaveRow;
		}

		private bool ValidateBankInformation(ref int lngRow)
		{
			bool ValidateBankInformation = false;
			// this will just check to see if there is information in the fields
			if (Strings.Trim(vsBank.TextMatrix(lngRow, lngColName)) != "")
			{
				// checks the first field (name)
				if (Strings.Trim(vsBank.TextMatrix(lngRow, lngColRT)) != "")
				{
					// checks the last field (routing transit)
					ValidateBankInformation = true;
				}
				else
				{
					ValidateBankInformation = false;
				}
			}
			else
			{
				ValidateBankInformation = false;
			}
			return ValidateBankInformation;
		}

		private void SaveAllRows()
		{
			int lngCT;
			bool boolFail = false;
			for (lngCT = 1; lngCT <= vsBank.Rows - 1; lngCT++)
			{
				if (SaveRow(ref lngCT))
				{
					vsBank.TextMatrix(lngCT, lngColDirty, FCConvert.ToString(0));
				}
				else
				{
					boolFail = true;
				}
			}
			if (!boolFail)
			{
				MessageBox.Show("All records saved correctly.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("Some of the records were not saved.", "Save All Rows", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveAllDirtyRows()
		{
			SaveAllRows();
			// Dim lngCT                   As Long
			// Dim boolFail                As Boolean
			// 
			// For lngCT = 1 To vsBank.rows - 1
			// If Val(vsBank.TextMatrix(lngCT, lngColDirty)) <> 0 Then
			// If SaveRow(lngCT) Then
			// 
			// Else
			// boolFail = True
			// End If
			// End If
			// Next
			// If Not boolFail Then
			// MsgBox "All records saved correctly.", vbInformation, "TRIO Software"
			// Else
			// MsgBox "Some of the records were not saved.", vbCritical, "Save Rows"
			// End If
		}

		private void DeleteSelected()
		{
			int lngKey = 0;
			string strDesc = "";
			clsDRWrapper rsDelBank = new clsDRWrapper();
			if (vsBank.Row > 1)
			{
				if (MessageBox.Show("Are you sure that you want to delete " + vsBank.TextMatrix(vsBank.Row, lngColName) + ".", null, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// get the ID value
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBank.TextMatrix(vsBank.Row, lngColKey))));
					strDesc = vsBank.TextMatrix(vsBank.Row, lngColName) + " " + vsBank.TextMatrix(vsBank.Row, lngColRT);
					// delete this line
					vsBank.RemoveItem(vsBank.Row);
					// delete this bank from the database
					if (lngKey > 0)
					{
						rsDelBank.DefaultDB = modExtraModules.strCRDatabase;
						rsDelBank.Execute("DELETE FROM Bank WHERE ID = " + FCConvert.ToString(lngKey), "TWCR0000.vb1");
					}
					// write this in the CYA table
					modGlobalFunctions.AddCYAEntry_8("CR", "Deleted from Bank table: " + FCConvert.ToString(lngKey) + " - " + strDesc);
				}
				else
				{
					// do nothing
				}
				SetGridHeight();
			}
		}

		private void DeleteBank_6(int lngRow, bool boolEmptyRow = false)
		{
			DeleteBank(lngRow, boolEmptyRow);
		}

		private void DeleteBank(int lngRow, bool boolEmptyRow = false)
		{
			int lngKey = 0;
			if (vsBank.Rows > 0 && lngRow > 0)
			{
				if (!boolEmptyRow)
				{
					if (MessageBox.Show("Are you sure that you want to delete " + vsBank.TextMatrix(lngRow, lngColName) + ".", null, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBank.TextMatrix(lngRow, lngColKey))));
						if (lngKey > 0)
						{
							// this will delete the bank
							rsBank.FindFirstRecord("ID", lngKey);
							if (rsBank.NoMatch)
							{
								// do nothing, the bank is not in the DB
							}
							else
							{
								// add an entry to the CYA table
								modGlobalFunctions.AddCYAEntry_8("CR", "Deleted from Bank table: " + rsBank.Get_Fields_Int32("ID") + " - " + vsBank.TextMatrix(lngRow, lngColName) + " " + vsBank.TextMatrix(lngRow, lngColRT));
								// delete this line
								vsBank.RemoveItem(vsBank.Row);
								// delete the bank
								rsBank.Delete();
								rsBank.OpenRecordset("SELECT * FROM Bank", modExtraModules.strCRDatabase);
							}
						}
						else
						{
							// delete this line
							vsBank.RemoveItem(vsBank.Row);
						}
						SetGridHeight();
					}
				}
				else
				{
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBank.TextMatrix(lngRow, lngColKey))));
					if (lngKey > 0)
					{
						// this will delete the bank
						rsBank.FindFirstRecord("ID", lngKey);
						if (rsBank.NoMatch)
						{
							// do nothing, the bank is not in the DB
						}
						else
						{
							// add an entry to the CYA table
							modGlobalFunctions.AddCYAEntry_8("CR", "Deleted blank line from Bank table: " + rsBank.Get_Fields_Int32("ID"));
							// delete this line
							vsBank.RemoveItem(vsBank.Row);
							// delete the bank
							rsBank.Delete();
							rsBank.OpenRecordset("SELECT * FROM Bank", modExtraModules.strCRDatabase);
						}
					}
					else
					{
						// delete this line
						vsBank.RemoveItem(vsBank.Row);
					}
				}
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, e);
		}
	}
}
