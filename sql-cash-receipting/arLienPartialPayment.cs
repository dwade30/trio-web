using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for arLienPartialPayment.
	/// </summary>
	public partial class arLienPartialPayment : FCSectionReport
	{

		public static arLienPartialPayment InstancePtr
		{
			get
			{
				return (arLienPartialPayment)Sys.GetInstance(typeof(arLienPartialPayment));
			}
		}

		protected arLienPartialPayment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public arLienPartialPayment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
		}
	}
}
