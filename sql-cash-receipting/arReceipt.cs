﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports.SectionReportModel;
using SharedApplication.Enums;
using System;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Web;
using Label = GrapeCity.ActiveReports.SectionReportModel.Label;

namespace TWCR0000
{
    /// <summary>
    /// Summary description for arReceipt.
    /// </summary>
    public partial class arReceipt : FCSectionReport
    {
        public static arReceipt InstancePtr
        {
            get
            {
                return (arReceipt)Sys.GetInstance(typeof(arReceipt));
            }
        }

        protected arReceipt _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	arReceipt	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               06/17/2002              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               02/01/2006              *
        // ********************************************************
        float[] lngLeftCol = new float[5 + 1];
        // this is an array of the column lefts used in the report
        // it will help with wide and narrow formats
        float lngTotalWidth;
        // total width of the report
        int intPaymentNumber;
        // this is the payment number in the grid
        int intTag;
        string strNarrowHeader = "";
        string strNarrowReceipt;
        string strNarrowTotal = "";
        int lngNumOfRows;
        int intindex;
        bool boolTextExport;
        //Printer prn = new Printer();
        bool boolSmallVersion;
        bool bool1STPage;
        bool stopReport = false;
        int lngMultiTown;
        // vbPorter upgrade warning: curConvenienceFee As Decimal	OnWrite(Decimal, short)
        Decimal curConvenienceFee;
        bool blnShowSigLine;

        public arReceipt()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

        public void StartUp()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int counter;
                if (modGlobal.Statics.gboolReprint)
                {
                    lngMultiTown = frmReprint.InstancePtr.lngMultiTown;
                    intTag = FCConvert.ToInt32(Math.Round(Conversion.Val(frmReprint.InstancePtr.strPassTag)));
                    // strPassTag is the split number
                    if (intTag == 0)
                        intTag = 1;
                    if (FCConvert.ToInt16(intTag) > 5)
                    {
                        intindex = FCConvert.ToInt16(frmReprint.InstancePtr.intRAIndex);
                        // this is the index in the ReceiptArray (for batch updates)
                        lngNumOfRows = 1;
                        curConvenienceFee = modUseCR.Statics.ReceiptArray[intindex].ConvenienceFee;
                    }
                    else
                    {
                        lngNumOfRows = frmReprint.InstancePtr.vsSummary.Rows - 1;
                        curConvenienceFee = 0;
                        for (counter = 1; counter <= frmReprint.InstancePtr.vsSummary.Rows - 1; counter++)
                        {
                            curConvenienceFee += modUseCR.Statics.ReceiptArray[counter].ConvenienceFee;
                        }
                    }
                }
                else
                {
                    lngMultiTown = frmReceiptInput.InstancePtr.lngMultiTown;
                    intTag = FCConvert.ToInt32(Math.Round(Conversion.Val(frmReceiptInput.InstancePtr.strPassTag)));
                    // strPassTag is the split number
                    if (intTag == 0)
                        intTag = 1;
                    if (FCConvert.ToInt16(intTag) > 5)
                    {
                        intindex = FCConvert.ToInt16(frmReceiptInput.InstancePtr.intRAIndex);
                        // this is the index in the ReceiptArray (for batch updates)
                        lngNumOfRows = 1;
                        curConvenienceFee = modUseCR.Statics.ReceiptArray[intindex].ConvenienceFee;
                    }
                    else
                    {
                        lngNumOfRows = frmReceiptInput.InstancePtr.vsSummary.Rows - 1;
                        for (counter = 1; counter <= frmReceiptInput.InstancePtr.vsSummary.Rows - 1; counter++)
                        {
                            curConvenienceFee += modUseCR.Statics.ReceiptArray[counter].ConvenienceFee;
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            int lngError = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                lngError = 1;
            TRYAGAIN:
                ;
                lngError = 2;
                intPaymentNumber += 1;
                if (intPaymentNumber > lngNumOfRows)
                {
                    lngError = 3;
                    eArgs.EOF = true;
                    stopReport = true;
                }
                else if (intTag > 5)
                {
                    // if this is a batch update then just sent the intIndex
                    lngError = 4;
                    // sarReceiptDetailOb.Report.UserData = CStr(intIndex)        'this is the receiptarray index for the detail section to get the info from
                }
                else
                {
                    lngError = 5;
                    if (modGlobal.Statics.gboolReprint)
                    {
                        lngError = 6;
                        if (Conversion.Val(frmReprint.InstancePtr.vsSummary.TextMatrix(intPaymentNumber, 7)) == intTag)
                        {
                            lngError = 7;
                            // sarReceiptDetailOb.Report.UserData = frmReprint.vsSummary.TextMatrix(intPaymentNumber, 8) 'this is the receiptarray index for the detail section to get the info from
                            eArgs.EOF = false;
                            lngError = 8;
                        }
                        else
                        {
                            lngError = 9;
                            // try the next row
                            goto TRYAGAIN;
                        }
                    }
                    else
                    {
                        lngError = 10;
                        if (Conversion.Val(frmReceiptInput.InstancePtr.vsSummary.TextMatrix(intPaymentNumber, 7)) == intTag)
                        {
                            lngError = 11;
                            // sarReceiptDetailOb.Report.UserData = frmReceiptInput.vsSummary.TextMatrix(intPaymentNumber, 8) 'this is the receiptarray index for the detail section to get the info from
                            eArgs.EOF = false;
                            lngError = 12;
                        }
                        else
                        {
                            lngError = 13;
                            // try the next row
                            goto TRYAGAIN;
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Fetch Data - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private void SetArraySizes()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this sub will set the array elements to the left of each column and it will set
                // the total size of the report depending on the size of the receipt
                int intReturn = 0;
                string strOldPrinter = "";
                string strPrinter = "";
                boolSmallVersion = false;
                modUseCR.Statics.strPrinterName = StaticSettings.GlobalSettingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName")?.SettingValue ?? "";

                if (modUseCR.Statics.strPrinterName != "")
                {
                    if (SetupAsPrinterFile(modUseCR.Statics.strPrinterName))
                    {
                        boolTextExport = true;
                    }
                    else
                    {
                        boolTextExport = false;
                        //arReceipt.InstancePtr.Document.Printer.PrinterName = modUseCR.Statics.strDeviceName;
                    }
                }
                if (!modGlobal.Statics.gboolNarrowReceipt)
                {
                    lngLeftCol[0] = 0;
                    lngLeftCol[1] = 1170 / 1440f;
                    lngLeftCol[2] = 1170 / 1440f;
                    lngLeftCol[3] = 3060 / 1440f;
                    lngLeftCol[4] = 4950 / 1440f;
                    lngLeftCol[5] = 5130 / 1440f;
                    lngTotalWidth = 7215 / 1440f;
                    // Me.Printer.PaperHeight = 1440 * gintReceiptLength
                    //FC:FINAL:DDU:#
                    if (modGlobal.Statics.gintReceiptLength > 0)
                    {
                        this.PageSettings.PaperHeight = 247 * modGlobal.Statics.gintReceiptLength / 1440f;
                    }
                    // this should set the paper height
                    PageSettings.Margins.Left = 720 / 1440f;
                    PageSettings.Margins.Right = 720 / 1440f;
                    if (modGlobal.Statics.gboolWideReceiptNoMargin)
                    {
                        PageSettings.Margins.Top = 0;
                    }
                    else
                    {
                        PageSettings.Margins.Top = 360 / 1440f;
                    }
                    if (modGlobal.Statics.gboolRecieptThin)
                    {
                        // PageFooter.Height = 1440
                        PageSettings.Margins.Bottom = 1440 / 1440f;
                    }
                    else
                    {
                        PageSettings.Margins.Bottom = 0;
                    }
                    // Me.Printer.PaperSize = 255
                    if (modGlobal.Statics.gintReceiptLength != 66)
                    {
                        //modCustomPageSize.CheckDefaultPrinter(arReceipt.InstancePtr.Document.Printer.PrinterName);
                        //FC:TODO:AM
                        //if (modGlobal.Statics.gintReceiptLength == 22)
                        //{
                        //	// PageSettings.Gutter = 0
                        //	intReturn = modCustomPageSize.SelectForm("TWCRReceipt22", this.Handle.ToInt32(), 205000, FCConvert.ToInt32(93133 - modGlobal.Statics.gdblReceiptLenAdjust));
                        //	boolSmallVersion = true;
                        //}
                        //else
                        //{
                        //	intReturn = modCustomPageSize.SelectForm("TWCRReceipt33", this.Handle.ToInt32(), 205000, FCConvert.ToInt32(139700 - modGlobal.Statics.gdblReceiptLenAdjust));
                        //	boolSmallVersion = true;
                        //}
                        //if (intReturn > 0)
                        //{
                        //	this.Document.Printer.PaperSize = intReturn;
                        //}
                        //this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("TWCRReceipt22", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
                    }
                    else
                    {
                        this.PageSettings.PaperWidth = 11520 / 1440f;
                    }
                }
                else
                {
                    // Printer.RenderMode = 1
                    //FC:FINAL:SBE - #3235 - adjust report width based on printer paper width
                    //lngLeftCol[0] = 0;
                    //lngLeftCol[1] = 100 / 1440f;
                    //lngLeftCol[2] = 0;
                    //lngLeftCol[3] = 200 / 1440f;
                    //lngLeftCol[4] = 1200 / 1440f;
                    //lngLeftCol[5] = 300 / 1440f;
                    //lngTotalWidth = 4000 / 1440f;
                    lngLeftCol[0] = 0;
                    lngLeftCol[1] = 88.875f / 1440f;
                    lngLeftCol[2] = 0;
                    lngLeftCol[3] = 177.75f / 1440f;
                    lngLeftCol[4] = 1066.5f / 1440f;
                    lngLeftCol[5] = 266.625f / 1440f;
                    lngTotalWidth = 3555 / 1440f;
                    //this.Document.Printer.PaperSize = 255;
                    this.PageSettings.PaperHeight = 1440 * 10 / 1440f; // 22
                    //FC:FINAL:SBE - #3235 - adjust report width based on printer paper width
                    //this.PageSettings.PaperWidth = 4002 / 1440f;
                    this.PageSettings.PaperWidth = (lngTotalWidth + 2) / 1440f;
                    //this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("Custom", (1440 * 10 * 100) / 1440, (4002 * 100) / 1440);
                    //this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("Custom", Convert.ToInt32(this.PageSettings.PaperWidth * 100), Convert.ToInt32(this.PageSettings.PaperHeight * 100));
                    PageSettings.Margins.Top = 0;
                    PageSettings.Margins.Bottom = 0;
                    PageSettings.Margins.Left = 0;
                    PageSettings.Margins.Right = 0;
                    strNarrowHeader = "";
                    strNarrowReceipt = "";
                    strNarrowTotal = "";
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Array Size", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private void SetupReceiptFormat()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // set the lefts
                // 0 column
                lblHeader.Left = lngLeftCol[0];
                lblTopComment.Left = lngLeftCol[0];
                lblDate.Left = lngLeftCol[0];
                lblHeaderType.Left = lngLeftCol[0];
                sarReceiptDetailOb.Left = lngLeftCol[0];
                lblBottomComment.Left = lngLeftCol[0];
                lblMuniName.Left = lngLeftCol[0];
                // 1 column
                lblTime.Left = lngLeftCol[1];
                // 2 column
                // DJW@10/19/2009
                // lblPaidBy.Left = lngLeftCol(2)
                lblPaidBy.Left = 0;
                // lblCashPaid.Left = lngLeftCol(2)
                // lblCheckPaid.Left = lngLeftCol(2)
                // lblCardPaid.Left = lngLeftCol(2)
                // lblChange.Left = lngLeftCol(2)
                if (modGlobal.Statics.gboolReprint)
                {
                    lblOtherAccounts.Left = 0;
                    lblReprint.Left = 0;
                }
                else
                {
                    if (!modGlobal.Statics.gboolNarrowReceipt)
                    {
                        lblOtherAccounts.Left = lblPaidBy.Left;
                        // + 100 ' + lblPaidBy.Width
                        lblOtherAccounts.Width = this.PrintWidth - lblOtherAccounts.Left;
                        // DJW @ 10/17/2009 'Commented out
                        // lblOtherAccounts.Top = lblPaidBy.Top + lblPaidBy.Height
                        // lblBottomComment.Top = lblPaidBy.Top + lblPaidBy.Height + lblOtherAccounts.Height
                    }
                    else
                    {
                        lblOtherAccounts.Left = lngLeftCol[2];
                    }
                }
                // 3 column
                lblTeller.Left = lngLeftCol[3];
                lblHeaderRef.Left = lngLeftCol[3];
                if (modGlobal.Statics.gboolNarrowReceipt)
                    lblTeller.Left = lblTime.Left + lblTime.Width;
                // 4 column
                // 5 column
                lblHeaderAmt.Left = lngLeftCol[5];
                //FC:FINAL:SBE - #3235 - adjust report width based on printer paper width
                //lblReceiptNumber.Left = lngLeftCol[5] - 500 / 1440f;
                lblReceiptNumber.Left = lngLeftCol[5] - 444.375f / 1440f;
                fldTotal.Left = lngLeftCol[5] - (modGlobal.Statics.gintReceiptOffSet * 140 / 1440f);
                if (fldTotal.Left - lblTotal.Width < 0)
                {
                    lblTotal.Left = 0;
                    //FC:FINAL:SBE - #3235 - adjust report width based on printer paper width
                    fldTotal.Left = lblTotal.Width;
                }
                else
                {
                    lblTotal.Left = fldTotal.Left - lblTotal.Width;
                }
                // set the widths
                if (!modGlobal.Statics.gboolNarrowReceipt)
                {
                    // wide
                    lblHeaderAmt.Visible = true;
                    lblHeaderType.Visible = true;
                    lblHeaderRef.Visible = true;
                    lblDate.Visible = true;
                    lblTime.Visible = true;
                    lblTeller.Visible = true;
                    lblReceiptNumber.Visible = true;
                    lblHeaderReceipt.Visible = false;
                    lblHeaderTitle.Visible = false;
                    lblHeaderAmt.Width = 1440 / 1440f;
                    // MAL@20080602: Adjust teller width to accomodate larger fonts
                    // Tracker Reference: 13603
                    // lblTeller.Width = 1540
                    lblTeller.Width = 2100 / 1440f;
                    lblReceiptNumber.Width = 2100 / 1440f;
                    fldTotal.Width = 1440 / 1440f;
                    lblMuniName.Width = lngTotalWidth;
                    lblHeader.Width = lngTotalWidth;
                    lblTopComment.Width = lngTotalWidth;
                    lblBottomComment.Width = lngTotalWidth;
                    lblPaidBy.Width = lngTotalWidth - lblPaidBy.Left;
                    lblCheck1.Width = lngTotalWidth - lblCheck1.Left;
                    lblTime.Left = lblDate.Left + lblDate.Width;
                    if (modGlobal.Statics.gboolRecieptCompacted || (modGlobal.Statics.gintReceiptLength == 22 && !modGlobal.Statics.gboolNarrowReceipt))
                    {
                        // this will shrink the header
                        lblMuniName.Top = 0;
                        lblHeader.Top = 240 / 1440f;
                        lblTopComment.Top = 480 / 1440f;
                        lblReprint.Top = 720 / 1440f;
                        lblHeaderReceipt.Top = 960 / 1440f;
                        lblTeller.Top = 960 / 1440f;
                        lblTime.Top = 960 / 1440f;
                        lblDate.Top = 960 / 1440f;
                        lblReceiptNumber.Top = 960 / 1440f;
                        lblHeaderTitle.Top = 1200 / 1440f;
                        lblHeaderAmt.Top = 1200 / 1440f;
                        lblHeaderRef.Top = 1200 / 1440f;
                        lblHeaderType.Top = 1200 / 1440f;
                        lnHeader.Y1 = 1440 / 1440f;
                        lnHeader.Y2 = 1440 / 1440f;
                        PageHeader.Height = 1440 / 1440f;
                        // this will shrink the footer
                        lblTotal.Top = 0;
                        fldTotal.Top = 0;
                        lblPaidBy.Top = 240 / 1440f;
                        lblOtherAccounts.Top = 480 / 1440f;
                        if (modGlobal.Statics.gintReceiptLength == 22)
                        {
                            if (blnShowSigLine)
                            {
                                lblSignature.Left = lblPaidBy.Left + lblPaidBy.Width + 100 / 1440f;
                                lblSignature.Top = lblPaidBy.Top;
                                lblSignatureLine.Left = lblSignature.Left;
                                lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                lblBottomComment.Top = 720 / 1440f;
                                lblCheck1.Top = 960 / 1440f;
                                ReportFooter.Height = 1200 / 1440f;
                            }
                            else
                            {
                                lblBottomComment.Top = 720 / 1440f;
                                lblCheck1.Top = 960 / 1440f;
                                ReportFooter.Height = 1200 / 1440f;
                            }
                        }
                        else
                        {
                            if (blnShowSigLine)
                            {
                                lblSignature.Top = 720 / 1440f;
                                lblSignatureLine.Top = 1220 / 1440f;
                                lblBottomComment.Top = 1460 / 1440f;
                                lblCheck1.Top = 1700 / 1440f;
                                ReportFooter.Height = 1940 / 1440f;
                            }
                            else
                            {
                                lblBottomComment.Top = 720 / 1440f;
                                lblCheck1.Top = 960 / 1440f;
                                ReportFooter.Height = 1200 / 1440f;
                            }
                        }
                    }
                    // Me.PageSettings.TopMargin = 1440
                    // If gboolRecieptThin Then
                    // Me.PageSettings.BottomMargin = 1440
                    // End If
                }
                else
                {
                    // narrow
                    lblHeaderAmt.Visible = false;
                    lblHeaderType.Visible = false;
                    lblHeaderRef.Visible = false;
                    lblDate.Visible = false;
                    lblTime.Visible = false;
                    lblTeller.Visible = false;
                    lblReceiptNumber.Visible = false;
                    lblHeaderReceipt.Visible = true;
                    lblHeaderTitle.Visible = true;
                    lblTotal.Width = lngTotalWidth;
                    lblTotal.Left = 0;
                    //FC:FINAL:SBE - #3235 - adjust report width based on printer paper width
                    //lblHeaderAmt.Width = 600 / 1440f;
                    //lblTeller.Width = 600 / 1440f;
                    //lblReceiptNumber.Width = 600 / 1440f;
                    //lblMuniName.Width = lngTotalWidth - 200 / 1440f;
                    //lblHeader.Width = lngTotalWidth - 200 / 1440f;
                    //lblTopComment.Width = lngTotalWidth - 200 / 1440f;
                    //lblBottomComment.Width = lngTotalWidth - 200 / 1440f;
                    lblHeaderAmt.Width = 533.25f / 1440f;
                    lblTeller.Width = 533.25f / 1440f;
                    lblReceiptNumber.Width = 533.25f / 1440f;
                    lblMuniName.Width = lngTotalWidth - 177.75f / 1440f;
                    lblHeader.Width = lngTotalWidth - 177.75f / 1440f;
                    lblTopComment.Width = lngTotalWidth - 177.75f / 1440f;
                    lblBottomComment.Width = lngTotalWidth - 177.75f / 1440f;
                }
                lblHeaderTitle.Width = lngTotalWidth;
                lblHeaderReceipt.Width = lngTotalWidth;
                if (modGlobal.Statics.gboolReprint)
                {
                    //FC:FINAL:SBE - #3235 - adjust report width based on printer paper width
                    //lblReprint.Width = lngTotalWidth - 200 / 1440f;
                    //lblOtherAccounts.Width = lngTotalWidth - 200 / 1440f;
                    lblReprint.Width = lngTotalWidth - 177.75f / 1440f;
                    lblOtherAccounts.Width = lngTotalWidth - 177.75f / 1440f;
                }
                else
                {
                }
                sarReceiptDetailOb.Width = lngTotalWidth;
                this.PrintWidth = lngTotalWidth + 2 / 1440f;
                this.PageSettings.PaperWidth = this.PrintWidth + this.PageSettings.Margins.Left + this.PageSettings.Margins.Right;
                lnHeader.X1 = 0;
                lnHeader.X2 = lngTotalWidth - 200 / 1440f;
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Setting Receipt Format");
            }
        }

        private void SetMainLabels()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will put in all of the information on the top and bottom of the receipt..ie comments, date/time, and teller
                DateTime dtReceiptDate;
                if (modGlobal.Statics.gboolReprint)
                {
                    dtReceiptDate = frmReprint.InstancePtr.dtReceiptDate;
                }
                else
                {
                    dtReceiptDate = DateTime.Now;
                }
                // 1st line
                lblHeader.Text = "-----  R e c e i p t  -----";
                // 2nd line
                if (modGlobal.Statics.gboolSP200Printer)
                {
                    lblTopComment.Text = Strings.Trim(modGlobal.Statics.gstrTopComments) + "     ";
                }
                else
                {
                    lblTopComment.Text = Strings.Trim(modGlobal.Statics.gstrTopComments);
                }
                // 3nd line
                lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
                strNarrowReceipt = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dtReceiptDate, "MM/dd/yy"), 9, false);
                strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dtReceiptDate, "h:mm tt"), 9, false);
                if (intTag > 5)
                {
                    if (modGlobal.Statics.gboolReprint)
                    {
                        if (modGlobal.Statics.gintReceiptOffSet < 3)
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("ID:" + frmReprint.InstancePtr.strTellerID, FCConvert.ToInt16(7 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        else
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces(" " + frmReprint.InstancePtr.strTellerID, FCConvert.ToInt16(2 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("#" + FCConvert.ToString(frmReprint.InstancePtr.lngReceiptNumber), 8) + "-" + FCConvert.ToString(intTag);
                        strNarrowHeader = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces("TYPE------", FCConvert.ToInt16(18 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("REF---", FCConvert.ToInt16(9 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("AMOUNT", 7);
                        strNarrowTotal = "";
                    }
                    else
                    {
                        if (modGlobal.Statics.gintReceiptOffSet < 3)
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("ID:" + frmReceiptInput.InstancePtr.strBatchTellerID, FCConvert.ToInt16(7 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        else
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces(" " + frmReceiptInput.InstancePtr.strBatchTellerID, FCConvert.ToInt16(2 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("#" + FCConvert.ToString(frmReceiptInput.InstancePtr.lngReceiptCreated), 8) + "-" + FCConvert.ToString(intTag);
                        strNarrowHeader = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces("TYPE------", FCConvert.ToInt16(18 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("REF---", FCConvert.ToInt16(9 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("AMOUNT", 7);
                        strNarrowTotal = "";
                    }
                    // lblOtherAccounts.Text = ""
                }
                else
                {
                    if (modGlobal.Statics.gboolReprint)
                    {
                        if (modGlobal.Statics.gintReceiptOffSet < 3)
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("ID:" + frmReprint.InstancePtr.strTellerID, FCConvert.ToInt16(7 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        else
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces(" " + frmReprint.InstancePtr.strTellerID, FCConvert.ToInt16(2 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("#" + FCConvert.ToString(frmReprint.InstancePtr.lngReceiptNumber), 8) + "-" + FCConvert.ToString(intTag);
                        strNarrowHeader = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces("TYPE------", FCConvert.ToInt16(18 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("REF---", FCConvert.ToInt16(9 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("AMOUNT", 7);
                        strNarrowTotal = "              Total :" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modUseCR.Statics.dblSplitTotals[intTag], "#,##0.00"), 17) + "*";
                    }
                    else
                    {
                        if (modGlobal.Statics.gintReceiptOffSet < 3)
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("ID:" + frmReceiptInput.InstancePtr.txtTellerID.Text, FCConvert.ToInt16(7 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        else
                        {
                            strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces(" " + frmReceiptInput.InstancePtr.txtTellerID.Text, FCConvert.ToInt16(2 - modGlobal.Statics.gintReceiptOffSet), false);
                        }
                        strNarrowReceipt += modGlobalFunctions.PadStringWithSpaces("#" + FCConvert.ToString(frmReceiptInput.InstancePtr.lngReceiptCreated), 8) + "-" + FCConvert.ToString(intTag);
                        strNarrowHeader = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces("TYPE------", FCConvert.ToInt16(18 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("REF---", FCConvert.ToInt16(9 - modGlobal.Statics.gintReceiptOffSet), false) + modGlobalFunctions.PadStringWithSpaces("AMOUNT", 7);
                        strNarrowTotal = "              Total :" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(modUseCR.Statics.dblSplitTotals[intTag], "#,##0.00"), 17) + "*";
                    }
                }
                lblHeaderTitle.Text = strNarrowHeader;
                lblHeaderReceipt.Text = strNarrowReceipt;
                if (modGlobal.Statics.gboolMultipleTowns)
                {
                    lblMuniName.Text = GetMultiTownMuniName();
                }
                else if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) == "")
                {
                    lblMuniName.Text = modGlobalConstants.Statics.MuniName;
                }
                else
                {
                    lblMuniName.Text = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
                }
                if (!modGlobal.Statics.gboolNarrowReceipt)
                {
                    // wide
                    lblDate.Text = Strings.Format(dtReceiptDate, "MM/dd/yyyy");
                    if (modGlobal.Statics.gboolReprint)
                    {
                        lblReceiptNumber.Text = "#" + FCConvert.ToString(frmReprint.InstancePtr.lngReceiptNumber);
                    }
                    else
                    {
                        lblReceiptNumber.Text = "#" + FCConvert.ToString(frmReceiptInput.InstancePtr.lngReceiptCreated);
                    }
                    if (modGlobal.Statics.gboolReprint)
                    {
                        lblTeller.Text = "";
                        lblReprint.Visible = true;
                    }
                    else
                    {
                        if (modGlobal.Statics.gstrTeller == "N")
                        {
                            lblTeller.Text = "";
                        }
                        else
                        {
                            lblTeller.Text = "Teller: " + frmReceiptInput.InstancePtr.txtTellerID.Text;
                        }
                    }
                    fldTotal.Visible = true;
                    lblTotal.Visible = true;
                    lblTotal.Text = "Total: ";
                }
                else
                {
                    // narrow
                    lblDate.Text = Strings.Format(dtReceiptDate, "MM/dd/yy");
                    if (modGlobal.Statics.gboolReprint)
                    {
                        lblReceiptNumber.Text = "#" + FCConvert.ToString(frmReprint.InstancePtr.lngReceiptNumber);
                        lblReprint.Visible = true;
                        if (modGlobal.Statics.gstrTeller == "N")
                        {
                            lblTeller.Text = "";
                        }
                        else
                        {
                            lblTeller.Text = "ID:" + frmReprint.InstancePtr.txtTellerID.Text;
                        }
                    }
                    else
                    {
                        lblReceiptNumber.Text = "#" + FCConvert.ToString(frmReceiptInput.InstancePtr.lngReceiptCreated);
                        lblReprint.Visible = false;
                        if (modGlobal.Statics.gstrTeller == "N")
                        {
                            lblTeller.Text = "";
                        }
                        else
                        {
                            lblTeller.Text = "ID:" + frmReceiptInput.InstancePtr.txtTellerID.Text;
                        }
                    }
                    fldTotal.Visible = true;
                    lblTotal.Visible = true;
                    lblTotal.Text = "Total: ";
                    // DJW@10/17/2009
                    // lblTotal.Text = strNarrowTotal
                    // lblTotal.Visible = True
                    // fldTotal.Visible = False
                }
                if (modGlobal.Statics.gboolReprint)
                {
                    if (frmReprint.InstancePtr.vsSummary.FindRow("***", -1, 2) != -1)
                    {
                        // hide lblOtherAccounts when there is
                        lblOtherAccounts.Visible = false;
                        // a batch line in the summary
                        lblSignature.Visible = false;
                        lblSignatureLine.Visible = false;
                        lblPaidBy.Top = 450 / 1440F;
                        lblBottomComment.Top = 810 / 1440F;
                    }
                }
                else
                {
                    if (frmReceiptInput.InstancePtr.vsSummary.FindRow("***", -1, 2) != -1)
                    {
                        // hide lblOtherAccounts when there is
                        lblOtherAccounts.Visible = false;
                        // a batch line in the summary
                        lblSignature.Visible = false;
                        lblSignatureLine.Visible = false;
                        lblPaidBy.Top = 450 / 1440F;
                        lblBottomComment.Top = 810 / 1440F;
                    }
                }
                // 4rd line
                lblHeaderType.Text = "Type";
                lblHeaderRef.Text = "Reference";
                lblHeaderAmt.Text = "Amount";
                if (intTag <= 5)
                {
                    fldTotal.Text = Strings.Format(modUseCR.Statics.dblSplitTotals[intTag], "#,##0.00") + "*";
                }
                if (modGlobal.Statics.gboolSP200Printer)
                {
                    lblBottomComment.Text = Strings.Trim(modGlobal.Statics.gstrBottomComments) + "      ";
                }
                else
                {
                    lblBottomComment.Text = Strings.Trim(modGlobal.Statics.gstrBottomComments);
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Setting Main Labels");
            }
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {

        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // use the printers collection to set the printer settings
                if (modGlobal.Statics.gstrPrintSigLine == "A")
                {
                    blnShowSigLine = true;
                }
                else
                {
                    if (modGlobal.Statics.gstrPrintSigLine == "P")
                    {
                        blnShowSigLine = false;

                        if (modGlobal.Statics.gboolReprint)
                        {
                            if (Conversion.Val(frmReprint.InstancePtr.txtCashOutCreditPaid.Text) != 0)
                            {
                                // Credit Card
                                blnShowSigLine = true;
                            }
                        }
                        else
                        {
                            if (Conversion.Val(frmReceiptInput.InstancePtr.txtCashOutCreditPaid.Text) != 0)
                            {
                                // Credit Card
                                blnShowSigLine = true;
                            }
                        }
                    }
                    else
                    {
                        blnShowSigLine = false;
                    }
                }

                SetArraySizes();
                SetupReceiptFormat();
                intPaymentNumber = 0;
                StartUp();
                SetMainLabels();
                if (modGlobal.Statics.gboolReprint)
                {
                    lblPaidBy.Text = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + "Paid By: " + Strings.Trim(frmReprint.InstancePtr.txtPaidBy.Text);
                }
                else
                {
                    lblPaidBy.Text = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + "Paid By: " + Strings.Trim(frmReceiptInput.InstancePtr.txtPaidBy.Text);
                }
                if (!modGlobal.Statics.gboolNarrowReceipt)
                {
                    SetReceiptFontSize();
                }
                if (boolSmallVersion || modGlobal.Statics.gboolNarrowReceipt)
                {
                    boolSmallVersion = true;
                    bool1STPage = true;
                }
                if (modUseCR.Statics.strPrinterName != "")
                {

                }
                else
                {
                    MessageBox.Show("No Receipt Printer is setup, please check the setup of your receipt printer in the printer setup on General Entry.", "Printer Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
                    this.Close();
                    return;
                }
                //FC:FINAL:SBE - #3235 - add report controls in ReportStart
                int lngNum = 2;
                if (modGlobal.Statics.gboolReprint)
                {
                    for (int repeat = 0; repeat <= 1; repeat++)
                    {
                        for (int lngRW = 0; lngRW <= frmReprint.InstancePtr.vsPayments.Rows; lngRW++)
                        {
                            //GrapeCity.ActiveReports.SectionReportModel.Label obNew;
                            //ReportFooter.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
                            //obNew.Name = "lblCheck" + FCConvert.ToString(lngNum);
                            GrapeCity.ActiveReports.SectionReportModel.Label obNew = ReportFooter.AddControlWithName<Label>("lblCheck" + FCConvert.ToString(lngNum));
                            obNew.Visible = false;
                            lngNum++;
                        }
                    }
                }
                else
                {
                    for (int lngRW = 1; lngRW <= frmReceiptInput.InstancePtr.vsPayments.Rows; lngRW++)
                    {
                        GrapeCity.ActiveReports.SectionReportModel.Label obNew = ReportFooter.AddControlWithName<Label>("lblCheck" + FCConvert.ToString(lngNum));
                        //ReportFooter.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
                        //obNew.Name = "lblCheck" + FCConvert.ToString(lngNum);
                        obNew.Visible = false;
                        lngNum++;
                    }
                }
                //FC:FINAL:DDU:#2108 - moved reportfooter method here
                ReportFooter_Format();
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Receipt", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private bool SetupAsPrinterFile(string strName)
        {
            bool SetupAsPrinterFile = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will return true if the string passed int is "PRNT.txt"
                if (Strings.UCase(strName) == Strings.UCase("PRNT.TXT"))
                {
                    SetupAsPrinterFile = true;
                }
                else
                {
                    SetupAsPrinterFile = false;
                }
                return SetupAsPrinterFile;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Printer File", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
            return SetupAsPrinterFile;
        }

        private void AddFooterFeeRow_62(string strFeeDesc, double dblFee, float lngTop, short intNum, bool boolOverride = false)
        {
            AddFooterFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, boolOverride);
        }

        private void AddFooterFeeRow_80(string strFeeDesc, double dblFee, float lngTop, short intNum, bool boolOverride = false)
        {
            AddFooterFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, boolOverride);
        }

        private void AddFooterFeeRow_242(string strFeeDesc, double dblFee, float lngTop, short intNum, bool boolOverride = false)
        {
            AddFooterFeeRow(ref strFeeDesc, ref dblFee, ref lngTop, ref intNum, boolOverride);
        }

        private void AddFooterFeeRow(ref string strFeeDesc, ref double dblFee, ref float lngTop, ref short intNum, bool boolOverride = false)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                GrapeCity.ActiveReports.SectionReportModel.Label obNew = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblFeeD" + FCConvert.ToString(intNum));
                string strTemp;
                // add a label for a description
                //obNew.Name = "lblFeeD" + FCConvert.ToString(intNum);
                obNew.Top = lngTop;
                obNew.Left = lngLeftCol[2];
                obNew.Width = this.PrintWidth - lngLeftCol[2];
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                //strTemp = obNew.Font;
                obNew.Font = fldTotal.Font;
                // this sets the font to the same as the field that is already created
                if (dblFee != 0 || boolOverride)
                {
                    if (boolOverride)
                    {
                        obNew.Text = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces(strFeeDesc, 7, false);
                    }
                    else
                    {
                        obNew.Text = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + modGlobalFunctions.PadStringWithSpaces(strFeeDesc, 7, false) + ":" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblFee, "#,##0.00"), 13);
                    }
                }
                else
                {
                    obNew.Text = " ";
                }
                //ReportFooter.Controls.Add(obNew);
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Footer Row", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {
            // Dim expPDF As New ActiveReportsPDFExport.ARExportPDF
            // expPDF.FileName = "c:\PrintTest.PDF"
            // expPDF.Export arReceipt.Pages
            // arReceipt.Pages.Save "C:\PrintTest.RDF"
        }

        private void Detail_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                sarReceiptDetailOb.Report = null;
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error After Detail", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                sarReceiptDetailOb.Report = new sarReceiptDetail();
                if (intPaymentNumber > lngNumOfRows)
                {
                }
                else if (intTag > 5)
                {
                    // if this is a batch update then just sent the intIndex
                    sarReceiptDetailOb.Report.UserData = intindex;
                    // this is the receiptarray index for the detail section to get the info from
                }
                else
                {
                    if (modGlobal.Statics.gboolReprint)
                    {
                        if (Conversion.Val(frmReprint.InstancePtr.vsSummary.TextMatrix(intPaymentNumber, 7)) == intTag)
                        {
                            sarReceiptDetailOb.Report.UserData = frmReprint.InstancePtr.vsSummary.TextMatrix(intPaymentNumber, 8);
                            // this is the receiptarray index for the detail section to get the info from
                        }
                    }
                    else
                    {
                        if (Conversion.Val(frmReceiptInput.InstancePtr.vsSummary.TextMatrix(intPaymentNumber, 7)) == intTag)
                        {
                            sarReceiptDetailOb.Report.UserData = frmReceiptInput.InstancePtr.vsSummary.TextMatrix(intPaymentNumber, 8);
                            // this is the receiptarray index for the detail section to get the info from
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private void PageHeader_BeforePrint(object sender, EventArgs e)
        {
            //FC:FINAL:SBE - stop report if ther is no other data to be processed. Empty pages are generated at export for not a known reason.
            if (stopReport)
            {
                this.Stop();
            }
            // If boolSmallVersion Then
            // If bool1STPage Then
            // bool1STPage = False
            // Else
            // PageHeader.Height = 0
            // PageHeader.Visible = False
            // End If
            // End If
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                if (boolSmallVersion)
                {
                    if (bool1STPage)
                    {
                        //FC:FINAL:SBE - #3235 - for unknown reason export to TIFF generates multiple pages, and the Visible and Height remains with previous values
                        PageHeader.Height = 1.229167F;
                        PageHeader.Visible = true;
                        bool1STPage = false;
                    }
                    else
                    {
                        PageHeader.Height = 0;
                        PageHeader.Visible = false;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Page Header", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }

        private void ReportFooter_Format()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int intCT;
                // this will count all the rows used in the footer
                float lngStart = 0;
                string strTemp = "";
                bool boolShowTotalLine;
                int intXLines;
                float lngFieldHeight;
                string strRegValue = "";
                lngFieldHeight = 240 / 1440f;
                fldTotal.Top = 0;
                lblTotal.Top = 0;
                if (!blnShowSigLine)
                {
                    lblSignature.Visible = false;
                    lblSignatureLine.Visible = false;
                    lblSignature.Top = 0;
                    lblSignatureLine.Top = 0;
                }
                if (modGlobal.Statics.gboolReprint || intTag > 5)
                {
                    // If lngNumOfRows < 2 And curConvenienceFee = 0 And Not gboolDetail Then
                    // lblTotal.Text = "     "
                    // Else
                    // this will keep the total line above the paid by line
                    // fldTotal.Top = 0
                    // lblTotal.Top = 0
                    // End If
                    // DJW@10/17/2009 Show Total line on reprint
                    // If gboolNarrowReceipt Then
                    // fldTotal.Visible = False
                    // Else
                    // If lngNumOfRows >= 2 Or curConvenienceFee > 0 Then
                    fldTotal.Visible = true;
                    // show the actual total of the receipt
                    lblTotal.Visible = true;
                    // this will keep the total line above the paid by line
                    fldTotal.Top = 0;
                    lblTotal.Top = 0;
                    // End If
                    if (modGlobal.Statics.gboolRecieptCompacted && !modGlobal.Statics.gboolNarrowReceipt)
                    {
                        lblPaidBy.Top = 240 / 1440f;
                        lblOtherAccounts.Top = 480 / 1440f;
                        if (modGlobal.Statics.gintReceiptLength == 22)
                        {
                            if (blnShowSigLine)
                            {
                                lblSignature.Left = 4320 / 1440f;
                                lblSignature.Top = lblPaidBy.Top + 240 / 1440f;
                                lblSignatureLine.Left = lblSignature.Left;
                                lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                lblBottomComment.Top = 720 / 1440f;
                            }
                            else
                            {
                                lblBottomComment.Top = 720 / 1440f;
                            }
                        }
                        else
                        {
                            if (blnShowSigLine)
                            {
                                lblSignature.Top = 720 / 1440f;
                                lblSignatureLine.Top = 1220 / 1440f;
                                lblBottomComment.Top = 1460 / 1440f;
                            }
                            else
                            {
                                lblBottomComment.Top = 720 / 1440f;
                            }
                        }
                    }
                    else
                    {
                        if (modGlobal.Statics.gintReceiptLength == 22)
                        {
                            lblPaidBy.Top = 240 / 1440f;
                            lblOtherAccounts.Top = 480 / 1440f;
                            if (blnShowSigLine)
                            {
                                lblSignature.Top = 960 / 1440f;
                                lblSignatureLine.Left = lblSignature.Left;
                                lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                lblBottomComment.Top = 1700 / 1440f;
                            }
                            else
                            {
                                lblBottomComment.Top = 720 / 1440f;
                            }
                        }
                        else
                        {
                            lblPaidBy.Top = 480 / 1440f;
                            lblOtherAccounts.Top = 960 / 1440f;
                            if (blnShowSigLine)
                            {
                                lblSignature.Top = 1440 / 1440f;
                                lblSignatureLine.Top = 1940 / 1440f;
                                lblBottomComment.Top = 2180 / 1440f;
                            }
                            else
                            {
                                lblBottomComment.Top = 1440 / 1440f;
                            }
                        }
                    }
                    lblOtherAccounts.Width = lblBottomComment.Width - lblOtherAccounts.Left;
                    // lblOtherAccounts.Alignment = ddTXCenter
                    lblCheck1.Visible = false;
                }
                else
                {
                    strTemp = modUseCR.GetInfoAboutOtherAccounts(FCConvert.ToInt16(intTag), lngNumOfRows);
                    if (lngNumOfRows < 2 && curConvenienceFee == 0 && !modGlobal.Statics.gboolDetail)
                    {
                        // one entry
                        if (Strings.Left(strTemp, 3) != "(0)")
                        {
                            // outstanding account balances
                            lblTotal.Text = "   ";
                            fldTotal.Visible = false;
                            if (modGlobal.Statics.gboolRecieptCompacted && !modGlobal.Statics.gboolNarrowReceipt)
                            {
                                lblPaidBy.Top = 240 / 1440f;
                                lblOtherAccounts.Top = 480 / 1440f;
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Left = 4320 / 1440f;
                                        lblSignature.Top = lblPaidBy.Top + 240 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                }
                                else
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 720 / 1440f;
                                        lblSignatureLine.Top = 1220 / 1440f;
                                        lblBottomComment.Top = 1460 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                }
                            }
                            else
                            {
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    lblPaidBy.Top = 240 / 1440f;
                                    lblOtherAccounts.Top = 480 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        // lblSignature.Left = 4320
                                        lblSignature.Top = 960 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 1700 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                }
                                else
                                {
                                    lblPaidBy.Top = 480 / 1440f;
                                    lblOtherAccounts.Top = 960 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 1440 / 1440f;
                                        lblSignatureLine.Top = 1940 / 1440f;
                                        lblBottomComment.Top = 2180 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 1440 / 1440f;
                                    }
                                }
                            }
                            // If gboolRecieptCompacted And Not gboolNarrowReceipt Then
                            // lblPaidBy.Top = lngFieldHeight
                            // lblOtherAccounts.Top = lngFieldHeight * 2
                            // lblBottomComment.Top = lngFieldHeight * 3
                            // Else
                            // lblPaidBy.Top = lngFieldHeight * 2
                            // lblOtherAccounts.Top = lngFieldHeight * 4
                            // lblBottomComment.Top = lngFieldHeight * 5
                            // End If
                        }
                        else
                        {
                            // no other accounts/balances
                            lblOtherAccounts.Visible = false;
                            lblTotal.Text = "      ";
                            fldTotal.Visible = false;
                            // lblPaidBy.Top = lngFieldHeight
                            // lblBottomComment.Top = lngFieldHeight * 2
                            if (modGlobal.Statics.gboolRecieptCompacted && !modGlobal.Statics.gboolNarrowReceipt)
                            {
                                lblPaidBy.Top = 240 / 1440F;
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Left = 4320 / 1440f;
                                        lblSignature.Top = lblPaidBy.Top + 240 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                }
                                else
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 480 / 1440f;
                                        lblSignatureLine.Top = 980 / 1440f;
                                        lblBottomComment.Top = 1220 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                }
                            }
                            else
                            {
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    lblPaidBy.Top = 240 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        // lblSignature.Left = 4320
                                        lblSignature.Top = lblPaidBy.Top + 480 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 1220 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                }
                                else
                                {
                                    lblPaidBy.Top = 480 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 960 / 1440f;
                                        lblSignatureLine.Top = 1460 / 1440f;
                                        lblBottomComment.Top = 1700 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 960 / 1440f;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // more than one entry
                        if (Strings.Left(strTemp, 3) != "(0)")
                        {
                            // outstanding account balances
                            if (modGlobal.Statics.gboolRecieptCompacted && !modGlobal.Statics.gboolNarrowReceipt)
                            {
                                lblPaidBy.Top = 240 / 1440f;
                                lblOtherAccounts.Top = 480 / 1440f;
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Left = 4320 / 1440f;
                                        lblSignature.Top = lblPaidBy.Top + 240 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                }
                                else
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 720 / 1440f;
                                        lblSignatureLine.Top = 1220 / 1440f;
                                        lblBottomComment.Top = 1460 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                }
                            }
                            else
                            {
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    lblPaidBy.Top = 240 / 1440f;
                                    lblOtherAccounts.Top = 480 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        // lblSignature.Left = 4320
                                        lblSignature.Top = lblOtherAccounts.Top + 480 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 1700 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 720 / 1440f;
                                    }
                                }
                                else
                                {
                                    lblPaidBy.Top = 480 / 1440f;
                                    lblOtherAccounts.Top = 960 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 1440 / 1440f;
                                        lblSignatureLine.Top = 1940 / 1440f;
                                        lblBottomComment.Top = 2180 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 1440 / 1440f;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // no other accounts/balances
                            lblOtherAccounts.Visible = false;
                            // lblPaidBy.Top = lngFieldHeight * 2
                            // lblBottomComment.Top = lngFieldHeight * 4
                            if (modGlobal.Statics.gboolRecieptCompacted && !modGlobal.Statics.gboolNarrowReceipt)
                            {
                                lblPaidBy.Top = 240 / 1440f;
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Left = 4320 / 1440f;
                                        lblSignature.Top = lblPaidBy.Top + 240 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                }
                                else
                                {
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 480 / 1440f;
                                        lblSignatureLine.Top = 980 / 1440f;
                                        lblBottomComment.Top = 1220 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                }
                            }
                            else
                            {
                                if (modGlobal.Statics.gintReceiptLength == 22)
                                {
                                    lblPaidBy.Top = 240 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        // lblSignature.Left = 4320
                                        lblSignature.Top = lblPaidBy.Top + 480 / 1440f;
                                        lblSignatureLine.Left = lblSignature.Left;
                                        lblSignatureLine.Top = lblSignature.Top + 500 / 1440f;
                                        lblBottomComment.Top = 1220 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 480 / 1440f;
                                    }
                                }
                                else
                                {
                                    lblPaidBy.Top = 480 / 1440f;
                                    if (blnShowSigLine)
                                    {
                                        lblSignature.Top = 960 / 1440f;
                                        lblSignatureLine.Top = 1460 / 1440f;
                                        lblBottomComment.Top = 1700 / 1440f;
                                    }
                                    else
                                    {
                                        lblBottomComment.Top = 960 / 1440f;
                                    }
                                }
                            }
                        }
                    }
                }
                intCT = 0;
                if (modGlobal.Statics.gboolRecieptCompacted && !modGlobal.Statics.gboolNarrowReceipt)
                {
                    lngStart = lblBottomComment.Top + lngFieldHeight;
                }
                else
                {
                    if (modGlobal.Statics.gintReceiptLength == 22)
                    {
                        lngStart = lblBottomComment.Top + lngFieldHeight;
                    }
                    else
                    {
                        lngStart = lblBottomComment.Top + (lngFieldHeight * 2);
                    }
                }
                // footer
                if (modGlobal.Statics.gboolReprint)
                {
                    lblPaidBy.Text = Strings.StrDup(modGlobal.Statics.gintReceiptOffSet, " ") + "Paid By: " + Strings.Trim(frmReprint.InstancePtr.txtPaidBy.Text);
                    // get the last receipt number
                    modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRLastReceiptNumber", ref strRegValue);
                    if (strRegValue == lblReceiptNumber.Text)
                    {
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRLastRemainingBalance", ref strRegValue);
                        lblOtherAccounts.Text = strRegValue;
                        // lblOtherAccounts.Top = 720  'set it back to the original top
                    }
                    else
                    {
                        lblOtherAccounts.Text = "***  REPRINT  ***";
                        lblOtherAccounts.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
                    }
                    lblReprint.Text = "***  REPRINT  ***";
                    if (Conversion.Val(frmReprint.InstancePtr.txtCashOutCashPaid.Text) != 0)
                    {
                        // Cash
                        AddFooterFeeRow_62("Cash", FCConvert.ToDouble(frmReprint.InstancePtr.txtCashOutCashPaid.Text), lngStart, 1);
                        intCT += 1;
                    }
                    if (Conversion.Val(frmReprint.InstancePtr.txtCashOutCheckPaid.Text) != 0)
                    {
                        // Check
                        AddFooterFeeRow_80("Check", FCConvert.ToDouble(frmReprint.InstancePtr.txtCashOutCheckPaid.Text), lngStart + (lngFieldHeight * intCT), 2);
                        intCT += 1;
                    }
                    if (Conversion.Val(frmReprint.InstancePtr.txtCashOutCreditPaid.Text) != 0)
                    {
                        // Credit Card
                        AddFooterFeeRow_80("Credit/Debit", FCConvert.ToDouble(frmReprint.InstancePtr.txtCashOutCreditPaid.Text), lngStart + (lngFieldHeight * intCT), 3);
                        intCT += 1;
                    }
                    if (Conversion.Val(frmReprint.InstancePtr.txtCashOutChange.Text) != 0)
                    {
                        // Change
                        AddFooterFeeRow_80("Change", FCConvert.ToDouble(frmReprint.InstancePtr.txtCashOutChange.Text), lngStart + (lngFieldHeight * intCT), 4);
                        intCT += 1;
                    }
                }
                else
                {
                    lblOtherAccounts.Text = strTemp + " ";
                    // save the last receipt information
                    string lblReceiptNumberTemp = lblReceiptNumber.Text;
                    string lblOtherAccountsTemp = lblOtherAccounts.Text;
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRLastReceiptNumber", lblReceiptNumberTemp);
                    lblReceiptNumber.Text = lblReceiptNumberTemp;
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRLastRemainingBalance", lblOtherAccountsTemp);
                    lblOtherAccounts.Text = lblOtherAccountsTemp;
                    // fldCashPaid.Text = frmReceiptInput.txtCashOutCashPaid.Text
                    // fldCheckPaid.Text = frmReceiptInput.txtCashOutCheckPaid.Text
                    // fldCardPaid.Text = frmReceiptInput.txtCashOutCreditPaid.Text
                    // fldChange.Text = frmReceiptInput.txtCashOutChange.Text
                    if (Conversion.Val(frmReceiptInput.InstancePtr.txtCashOutCashPaid.Text) != 0)
                    {
                        // Cash
                        AddFooterFeeRow_62("Cash", FCConvert.ToDouble(frmReceiptInput.InstancePtr.txtCashOutCashPaid.Text), lngStart, 1);
                        intCT += 1;
                    }
                    if (Conversion.Val(frmReceiptInput.InstancePtr.txtCashOutCheckPaid.Text) != 0)
                    {
                        // Check
                        AddFooterFeeRow_80("Check", FCConvert.ToDouble(frmReceiptInput.InstancePtr.txtCashOutCheckPaid.Text), lngStart + (lngFieldHeight * intCT), 2);
                        intCT += 1;
                    }
                    if (Conversion.Val(frmReceiptInput.InstancePtr.txtCashOutCreditPaid.Text) != 0)
                    {
                        // Credit Card
                        AddFooterFeeRow_80("Credit/Debit", FCConvert.ToDouble(frmReceiptInput.InstancePtr.txtCashOutCreditPaid.Text), lngStart + (lngFieldHeight * intCT), 3);
                        intCT += 1;
                    }
                    if (Conversion.Val(frmReceiptInput.InstancePtr.txtCashOutChange.Text) != 0)
                    {
                        // Change
                        AddFooterFeeRow_80("Change", FCConvert.ToDouble(frmReceiptInput.InstancePtr.txtCashOutChange.Text), lngStart + (lngFieldHeight * intCT), 4);
                        intCT += 1;
                    }
                }
                // this will push the check number to start lower than the
                lblCheck1.Top = lngStart + (lngFieldHeight * intCT);
                // add the check numbers
                intXLines = modGlobal.Statics.gintExtraLines + AddCheckNumbers();
                if (modGlobal.Statics.gboolNarrowReceipt)
                {
                    ReportFooter.Height = lngStart + (lngFieldHeight * (intCT + intXLines)) + 240;
                    // MAL@20081229: Changed to use a space instead of a period. XP SP3 is showing the period
                    // Tracker Reference: 16631
                    // AddFooterFeeRow ".", 0, lngStart + (lngFieldHeight * (intCT + intXLines)), 5, True
                    AddFooterFeeRow_242(" ", 0, lngStart + (lngFieldHeight * (intCT + intXLines)), 5, true);
                }
                else
                {
                    if (modGlobal.Statics.gboolRecieptCompacted || (modGlobal.Statics.gintReceiptLength == 22 && !modGlobal.Statics.gboolNarrowReceipt))
                    {
                        ReportFooter.Height = lngStart + (lngFieldHeight * (intCT + intXLines));
                        // AddFooterFeeRow ".", 0, lngStart + (lngFieldHeight * (intCT + intXLines)), 5
                        AddFooterFeeRow_80(" ", 0, lngStart + (lngFieldHeight * (intCT + intXLines)), 5);
                    }
                    else
                    {
                        ReportFooter.Height = lngStart + (lngFieldHeight * (intCT + intXLines + 2));
                        // AddFooterFeeRow ".", 0, lngStart + (lngFieldHeight * (intCT + intXLines + 1)), 5
                        AddFooterFeeRow_80(" ", 0, lngStart + (lngFieldHeight * (intCT + intXLines + 1)), 5);
                    }
                }
                // lblbottom.Top = ReportFooter.Height - lblbottom.Height
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Footer Format", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
        }
        // vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
        private short AddCheckNumbers()
        {
            short AddCheckNumbers = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will add the check numbers at the bottom of the receipt
                GrapeCity.ActiveReports.SectionReportModel.Label obNew;
                string strTemp = "";
                int lngNum;
                double dblAmount = 0;
                int lngRW;
                // vbPorter upgrade warning: dblConvenienceFee As Decimal	OnWrite(short, Decimal)
                Decimal dblConvenienceFee;
                lngNum = 1;
                dblConvenienceFee = 0;
                if (modGlobal.Statics.gboolReprint)
                {
                    for (lngRW = 0; lngRW <= frmReprint.InstancePtr.vsPayments.Rows - 1; lngRW++)
                    {
                        if (Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 0)) == "CHK")
                        {
                            dblAmount = FCConvert.ToDouble(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 1));
                            if (lngNum == 1)
                            {
                                lblCheck1.Text = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 2)), 10) + " - " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 15, true);
                                lblCheck1.Visible = true;
                            }
                            else
                            {
                                // add a label for a description
                                //FC:FINAL:SBE - #3235 - add report controls in ReportStart
                                //ReportFooter.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
                                //obNew.Name = "lblCheck" + FCConvert.ToString(lngNum);
                                obNew = ReportFooter.Controls["lblCheck" + FCConvert.ToString(lngNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                obNew.Visible = true;
                                obNew.Top = lblCheck1.Top + ((lngNum - 1) * lblCheck1.Height);
                                obNew.Left = lblCheck1.Left;
                                obNew.Width = lblCheck1.Width;
                                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                                //strTemp = obNew.Font;
                                obNew.Font = lblCheck1.Font;
                                // this sets the font to the same as the field that is already created
                                obNew.Text = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 2)), 10) + " - " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 15, true);
                            }
                            lngNum += 1;
                        }
                    }
                    for (lngRW = 0; lngRW <= frmReprint.InstancePtr.vsPayments.Rows - 1; lngRW++)
                    {
                        if (Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 0)) == "CC")
                        {
                            dblAmount = FCConvert.ToDouble(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 1)) - FCConvert.ToDouble(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 4));
                            dblConvenienceFee += FCConvert.ToDecimal(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 4));

                            if (lngNum == 1)
                            {
                                lblCheck1.Text = Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 2)) + " " + Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 5)) + " -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 13, true);
                                lblCheck1.Visible = true;
                            }
                            else
                            {
                                // add a label for a description
                                //FC:FINAL:SBE - #3235 - add report controls in ReportStart
                                //ReportFooter.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
                                //obNew.Name = "lblCheck" + FCConvert.ToString(lngNum);
                                obNew = ReportFooter.Controls["lblCheck" + FCConvert.ToString(lngNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                obNew.Visible = true;
                                obNew.Top = lblCheck1.Top + ((lngNum - 1) * lblCheck1.Height);
                                obNew.Left = lblCheck1.Left;
                                obNew.Width = lblCheck1.Width;
                                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                                //strTemp = obNew.Font;
                                obNew.Font = lblCheck1.Font;
                                // this sets the font to the same as the field that is already created
                                obNew.Text = Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 2)) + " " + Strings.Trim(frmReprint.InstancePtr.vsPayments.TextMatrix(lngRW, 5)) + " -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 13, true);
                            }
                            lngNum += 1;
                        }
                    }
                }
                else
                {
                    for (lngRW = 1; lngRW <= frmReceiptInput.InstancePtr.vsPayments.Rows - 1; lngRW++)
                    {
                        if (Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 0)) == "CHK")
                        {
                            dblAmount = FCConvert.ToDouble(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 1));
                            if (lngNum == 1)
                            {
                                lblCheck1.Text = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 2)), 10) + " -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 13, true);
                                lblCheck1.Visible = true;
                            }
                            else
                            {
                                // add a label for a description
                                //FC:FINAL:SBE - #3235 - add report controls in ReportStart
                                //ReportFooter.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
                                //obNew.Name = "lblCheck" + FCConvert.ToString(lngNum);
                                obNew = ReportFooter.Controls["lblCheck" + FCConvert.ToString(lngNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                obNew.Visible = true;
                                obNew.Top = lblCheck1.Top + ((lngNum - 1) * lblCheck1.Height);
                                obNew.Left = lblCheck1.Left;
                                obNew.Width = lblCheck1.Width;
                                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                                //strTemp = obNew.Font;
                                obNew.Font = lblCheck1.Font;
                                // this sets the font to the same as the field that is already created
                                obNew.MultiLine = false;
                                obNew.WordWrap = false;
                                obNew.Text = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 2)), 10) + " -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 13, true);
                            }
                            lngNum += 1;
                        }
                    }
                    for (lngRW = 1; lngRW <= frmReceiptInput.InstancePtr.vsPayments.Rows - 1; lngRW++)
                    {
                        if (Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 0)) == "CC")
                        {
                            dblAmount = FCConvert.ToDouble(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 1)) - FCConvert.ToDouble(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 4));
                            dblConvenienceFee += FCConvert.ToDecimal(FCConvert.ToDouble(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 4)));
                            if (lngNum == 1)
                            {
                                lblCheck1.Text = Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 2)) + " " + Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 5)) + " -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 13, true);
                                lblCheck1.Visible = true;
                            }
                            else
                            {
                                // add a label for a description
                                //FC:FINAL:SBE - #3235 - add report controls in ReportStart
                                //ReportFooter.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
                                //obNew.Name = "lblCheck" + FCConvert.ToString(lngNum);
                                obNew = ReportFooter.Controls["lblCheck" + FCConvert.ToString(lngNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                obNew.Visible = true;
                                obNew.Top = lblCheck1.Top + ((lngNum - 1) * lblCheck1.Height);
                                obNew.Left = lblCheck1.Left;
                                obNew.Width = lblCheck1.Width;
                                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                                //strTemp = obNew.Font;
                                obNew.Font = lblCheck1.Font;
                                // this sets the font to the same as the field that is already created
                                obNew.MultiLine = false;
                                obNew.WordWrap = false;
                                obNew.Text = Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 2)) + " " + Strings.Trim(frmReceiptInput.InstancePtr.vsPayments.TextMatrix(lngRW, 5)) + " -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblAmount, "#,##0.00"), 13, true);
                            }
                            lngNum += 1;
                        }
                    }
                }
                if (dblConvenienceFee != 0)
                {
                    //FC:FINAL:SBE - #3235 - add report controls in ReportStart
                    //ReportFooter.Controls.Add(obNew = new GrapeCity.ActiveReports.SectionReportModel.Label());
                    //obNew.Name = "lblCheck" + FCConvert.ToString(lngNum);
                    obNew = ReportFooter.Controls["lblCheck" + FCConvert.ToString(lngNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                    obNew.Visible = true;
                    obNew.Top = lblCheck1.Top + ((lngNum - 1) * lblCheck1.Height);
                    obNew.Left = lblCheck1.Left;
                    obNew.Width = lblCheck1.Width;
                    obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                    //strTemp = obNew.Font;
                    obNew.Font = lblCheck1.Font;
                    // this sets the font to the same as the field that is already created
                    obNew.MultiLine = false;
                    obNew.WordWrap = false;
                    // kgk 02-02-2012  InforME uses a Portal Charge
                    if (modGlobal.Statics.gstrEPaymentPortal == "E")
                    {
                        obNew.Text = "Portal Charge  -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblConvenienceFee, "#,##0.00"), 13, true);
                    }
                    else
                    {
                        obNew.Text = "Convenience Fee  -" + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblConvenienceFee, "#,##0.00"), 13, true);
                    }
                    lngNum += 1;
                }
                if (lngNum > 1)
                {
                    AddCheckNumbers = FCConvert.ToInt16(lngNum - 1);
                }
                else
                {
                    AddCheckNumbers = 0;
                }
                return AddCheckNumbers;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Check Numbers", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal: false);
            }
            return AddCheckNumbers;
        }

        private void SetReceiptFontSize()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngFSize;
                string strTemp = "";
                // this function will get the font size set for wide receipts and adjust the fields accordingly
                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideFontSize", ref strTemp);
                lngFSize = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
                if (lngFSize <= 0)
                {
                    return;
                }
                lblMuniName.Font = new Font(lblMuniName.Font.Name, lngFSize);
                lblHeader.Font = new Font(lblHeader.Font.Name, lngFSize);
                lblTopComment.Font = new Font(lblTopComment.Font.Name, lngFSize);
                lblReprint.Font = new Font(lblReprint.Font.Name, lngFSize);
                lblTeller.Font = new Font(lblTeller.Font.Name, lngFSize);
                lblDate.Font = new Font(lblDate.Font.Name, lngFSize);
                lblTime.Font = new Font(lblTime.Font.Name, lngFSize);
                lblReceiptNumber.Font = new Font(lblReceiptNumber.Font.Name, lngFSize);
                lblHeaderAmt.Font = new Font(lblHeaderAmt.Font.Name, lngFSize);
                lblHeaderType.Font = new Font(lblHeaderType.Font.Name, lngFSize);
                lblHeaderRef.Font = new Font(lblHeaderRef.Font.Name, lngFSize);
                lblTotal.Font = new Font(lblTotal.Font.Name, lngFSize);
                fldTotal.Font = new Font(fldTotal.Font.Name, lngFSize);
                lblPaidBy.Font = new Font(lblPaidBy.Font.Name, lngFSize);
                lblOtherAccounts.Font = new Font(lblOtherAccounts.Font.Name, lngFSize);
                lblBottomComment.Font = new Font(lblBottomComment.Font.Name, lngFSize);
                lblCheck1.Font = new Font(lblCheck1.Font.Name, lngFSize);
                lblMuniName.Height = 300 / 1440F;
                lblHeader.Height = 300 / 1440F;
                lblTopComment.Height = 300 / 1440F;
                lblReprint.Height = 300 / 1440F;
                lblTeller.Height = 300 / 1440F;
                lblDate.Height = 300 / 1440F;
                lblTime.Height = 300 / 1440F;
                lblReceiptNumber.Height = 300 / 1440F;
                lblHeaderAmt.Height = 300 / 1440F;
                lblHeaderType.Height = 300 / 1440F;
                lblHeaderRef.Height = 300 / 1440F;
                lblTotal.Height = 300 / 1440F;
                fldTotal.Height = 300 / 1440F;
                lblPaidBy.Height = 300 / 1440F;
                lblOtherAccounts.Height = 300 / 1440F;
                lblBottomComment.Height = 300 / 1440F;
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                // do nothing
            }
        }

        private string GetMultiTownMuniName()
        {
            string GetMultiTownMuniName = "";
            // Check the last archive amount
            if (lngMultiTown == 0)
            {
                GetMultiTownMuniName = modGlobalConstants.Statics.MuniName;
            }
            else
            {
                GetMultiTownMuniName = modRegionalTown.GetTownKeyName(ref lngMultiTown);
            }
            return GetMultiTownMuniName;
        }

        private void arReceipt_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //arReceipt.Caption	= "Receipt";
            //arReceipt.Icon	= "arReceipt.dsx":0000";
            //arReceipt.Left	= 0;
            //arReceipt.Top	= 0;
            //arReceipt.Width	= 11880;
            //arReceipt.Height	= 8220;
            //arReceipt.SectionData	= "arReceipt.dsx":058A;
            //End Unmaped Properties
        }
    }
}
