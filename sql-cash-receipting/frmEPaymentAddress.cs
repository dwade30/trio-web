﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmEPaymentAddress.
	/// </summary>
	public partial class frmEPaymentAddress : BaseForm
	{
		public frmEPaymentAddress()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEPaymentAddress InstancePtr
		{
			get
			{
				return (frmEPaymentAddress)Sys.GetInstance(typeof(frmEPaymentAddress));
			}
		}

		protected frmEPaymentAddress _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolReturn;
		private string strName = "";
		private string strAddress1 = "";
		private string strAddress2 = "";
		private string strCity = "";
		private string strZip = "";
		private string strZip4 = "";
		private string strState = "";

		public bool Init(bool boolEditable = true)
		{
			bool Init = false;
			txtName.Text = modGlobal.Statics.gstrCCOwnerName;
			txtAddress1.Text = modGlobal.Statics.gstrCCOwnerStreet;
			txtCity.Text = modGlobal.Statics.gstrCCOwnerCity;
			txtState.Text = modGlobal.Statics.gstrCCOwnerState;
			txtZip.Text = Strings.Left(modGlobal.Statics.gstrCCOwnerZip, 5);
			if (modGlobal.Statics.gstrCCOwnerZip.Length > 5)
			{
				txtZip4.Text = Strings.Mid(modGlobal.Statics.gstrCCOwnerZip, 7, 4);
			}
			else
			{
				txtZip4.Text = "";
			}
			txtCountry.Text = modGlobal.Statics.gstrCCOwnerCountry;
			if (!boolEditable)
			{
				mnuSaveContinue.Visible = false;
				Seperator.Visible = false;
			}
			this.Show(FormShowEnum.Modal, App.MainForm);
			Init = true;
			return Init;
		}

		private void frmEPaymentAddress_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmEPaymentAddress_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEPaymentAddress.FillStyle	= 0;
			//frmEPaymentAddress.ScaleWidth	= 5880;
			//frmEPaymentAddress.ScaleHeight	= 4140;
			//frmEPaymentAddress.LinkTopic	= "Form2";
			//frmEPaymentAddress.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			boolReturn = true;
			if (modGlobal.Statics.gstrCCOwnerName == "" || modGlobal.Statics.gstrCCOwnerStreet == "" || modGlobal.Statics.gstrCCOwnerCity == "" || modGlobal.Statics.gstrCCOwnerState == "" || FCConvert.CBool(modGlobal.Statics.gstrCCOwnerZip) || modGlobal.Statics.gstrCCOwnerCountry == "")
			{
				if (MessageBox.Show("ALL address information is required for Credit Card transactions!" + "\r\n" + "Are you sure you want to exit?", "Required Information!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					modGlobal.Statics.gstrCCOwnerName = "";
					modGlobal.Statics.gstrCCOwnerStreet = "";
					modGlobal.Statics.gstrCCOwnerCity = "";
					modGlobal.Statics.gstrCCOwnerState = "";
					modGlobal.Statics.gstrCCOwnerZip = "";
					modGlobal.Statics.gstrCCOwnerCountry = "";
					Close();
				}
				else
				{
					txtName.Focus();
				}
			}
			else
			{
				modGlobal.Statics.gstrCCOwnerName = Strings.Trim(txtName.Text);
				modGlobal.Statics.gstrCCOwnerStreet = Strings.Trim(txtAddress1.Text);
				modGlobal.Statics.gstrCCOwnerCity = Strings.Trim(txtCity.Text);
				modGlobal.Statics.gstrCCOwnerStreet = Strings.Trim(txtState.Text);
				if (txtZip4.Text != "")
				{
					modGlobal.Statics.gstrCCOwnerZip = txtZip.Text + "-" + txtZip4.Text;
				}
				else
				{
					modGlobal.Statics.gstrCCOwnerZip = txtZip.Text;
				}
				modGlobal.Statics.gstrCCOwnerCountry = txtCountry.Text;
				Close();
			}
		}
	}
}
