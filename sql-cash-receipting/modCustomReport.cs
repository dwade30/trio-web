﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using Wisej.Web;

namespace TWCR0000
{
    public class modCustomReport
    {
        public const int GRIDNONE = -1;
        // This will not show the field in the where grid
        public const int GRIDTEXT = 1;
        public const int GRIDDATE = 2;
        public const int GRIDCOMBOIDTEXT = 3;
        public const int GRIDNUMRANGE = 4;
        public const int GRIDCOMBOIDNUM = 5;
        public const int GRIDCOMBOTEXT = 6;
        public const int GRIDTEXTRANGE = 7;
        public const int GRIDCOMBOANDNUM = 8;
        // THIS HAS A COMBO BOX AND NUMERIC FIELD
        public const int GRIDCOMBOANDTEXT = 9;
        // THIS HAS A COMBO BOX AND TEXT FIELD
        public const int GRIDBDACCOUNT = 10;
        // THIS HAS A COMBO BOX AND TEXT FIELD, THE COMBO HAVING E,R,G
        public const int GRIDLISTTEXT = 11;
        // ALLOWS A COMMA DELIMITED STRING OF CONSTRAINTS
        public const int GRIDCOMBORANGE = 12;
        // This is two combo boxes for text
        public const int GRIDCOMBONUMRANGE = 13;
        // This is two combo boxes for numbers
        public struct CustomReportField
        {
            public string DatabaseFieldName;
            // This is the field name that is in the database
            public string SelectName;
            // this is the text that will be put into the SELECT area of the SQL statement
            public string NameToShowUser;
            // his will be how the user sees the field name in the grid "Owner Name"
            public string ReportName;
            // This is the name of the variable that will show on the report
            public bool Sort;
            public string ComboList;
            // Variable - it is the list of answers in the combo box
            public int WhereType;
            // This is the type of variable. ex(Date, Text, Number) using the Custom Report
            // 0 = Text, 1 = Number, 2 = Date, 3 = Do NOT show in Where grid, 4 = Formatted Year, 5 = BD Account Number
            public string WhereString;
            // this will be set to the Where string that will search for the correct receipt
            public int RowNumber;
            // this will be set when loaded into the grid, this is the row of the answer in the grid
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public CustomReportField(int unusedParam)
            {
                this.DatabaseFieldName = string.Empty;
                this.SelectName = string.Empty;
                this.NameToShowUser = string.Empty;
                this.ReportName = string.Empty;
                this.Sort = false;
                this.ComboList = string.Empty;
                this.WhereType = 0;
                this.WhereString = string.Empty;
                this.RowNumber = 0;
            }
        };
        // vbPorter upgrade warning: 'Return' As Variant --> As string
        public static string FixQuotes(string strValue)
        {
            string FixQuotes = "";
            FixQuotes = strValue.Replace("'", "''");
            return FixQuotes;
        }

        public static void GetNumberOfFields(string strSQL)
        {
            // THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
            // ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
            // TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
            // ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
            // vbPorter upgrade warning: strTemp As Variant --> As string()
            string[] strTemp = null;
            object strTemp2;
            int intCount;
            int intCounter;
            if (Strings.Trim(strSQL) == string.Empty)
            {
                Statics.intNumberOfSQLFields = -1;
                return;
            }
            strTemp = Strings.Split(strSQL, "FROM", -1, CompareConstants.vbBinaryCompare);
            strSQL = Strings.Mid(strTemp[0], 7, FCConvert.ToString(strTemp[0]).Length - 6);
            strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
            Statics.intNumberOfSQLFields = Information.UBound((object[])strTemp, 1);
            for (intCounter = 0; intCounter <= Statics.intNumberOfSQLFields; intCounter++)
            {
                if (Strings.Trim(strTemp[intCounter]) != string.Empty)
                {
                    // CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
                    Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
                    // NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
                    // REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
                    for (intCount = 0; intCount <= Statics.intNumberOfSQLFields; intCount++)
                    {
                        if (Statics.strFields[intCount] == Strings.Trim(strTemp[intCounter]))
                        {
                            Statics.strFieldCaptions[intCounter] = Statics.arrCustomReport[intCount].NameToShowUser;
                            break;
                        }
                    }
                }
            }
        }
        // vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
        public static object CheckForAS(string strFieldName, short intSegment = 2)
        {
            object CheckForAS = null;
            // THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
            // IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
            // DATABASE FIELD NAME
            // vbPorter upgrade warning: strTemp As Variant --> As string()
            string[] strTemp = null;
            if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
            {
                // SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
                strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
                if (Strings.UCase(strTemp[1]) == "AS")
                {
                    // RETURN THE SQL REFERENCE FIELD NAME
                    if (intSegment == 1)
                    {
                        CheckForAS = strTemp[0];
                    }
                    else
                    {
                        CheckForAS = strTemp[2];
                    }
                }
                else
                {
                    // RETURN THE ACTUAL DATABASE FIELD NAME
                    CheckForAS = strTemp[0];
                }
            }
            else
            {
                // AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
                strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
                CheckForAS = Strings.Trim(strTemp[0]);
            }
            return CheckForAS;
        }

        public static void ClearComboListArray()
        {
            // CLEAR THE COMBO LIST ARRAY.
            // THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
            int intCount;
            for (intCount = 0; intCount <= 50; intCount++)
            {
                Statics.strComboList[intCount, 0] = string.Empty;
                Statics.strComboList[intCount, 1] = string.Empty;
            }
        }
        // vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomReport)
        public static void SetFormFieldCaptions(Form FormName, string ReportType)
        {
            // THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
            // 
            // ****************************************************************
            // THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
            // TO ADD A NEW REPORT TYPE.  Maybe the BuildWhereParameters sub.
            // ****************************************************************
            Statics.strBinaryReportType = Strings.UCase(ReportType);
            if (!frmCustomReport.InstancePtr.boolSavingAnswers)
            {
                // CLEAR THE COMBO LIST ARRAY
                ClearComboListArray();
                SetReceiptSearch(FormName);
                // POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
                LoadSortList(FormName);
                // POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
                LoadWhereGrid(FormName);
                // LOAD THE SAVED REPORT COMBO ON THE FORM
                FCUtils.CallByName(FormName, "LoadCombo", CallType.Method);
            }
        }
        // vbPorter upgrade warning: FormName As Form	OnWrite(Form)
        public static void SetReceiptSearch(dynamic FormName)
        {
            int lngCT;
            // WHAT IS THE CAPTION TO SHOW ON THE TOP OF THE REPORT
            Statics.strCustomTitle = " Receipt Search Report ";
            // fill the array for this report
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptDate].NameToShowUser = "Actual Date";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptDate].ReportName = "Date";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptDate].DatabaseFieldName = "Date";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptDate].SelectName = "Date";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptDate].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptDate].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptDate].WhereType = GRIDDATE;
            // MAL@20081105: Add Time to Sort options
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTime].NameToShowUser = "Actual Time";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTime].ReportName = "ActualSystemTime";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTime].DatabaseFieldName = "ActualSystemTime";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTime].SelectName = "Actual System Time";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTime].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTime].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTime].WhereType = GRIDTEXTRANGE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCloseOut].NameToShowUser = "Close Out Date";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCloseOut].ReportName = "Close Out";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCloseOut].DatabaseFieldName = "DailyCloseOut";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCloseOut].SelectName = "Close Out";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCloseOut].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCloseOut].ComboList = GetCloseOutComboList();
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCloseOut].WhereType = GRIDCOMBORANGE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTellerID].NameToShowUser = "Teller ID";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTellerID].ReportName = "Teller";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTellerID].DatabaseFieldName = "TellerID";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTellerID].SelectName = "TellerID";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTellerID].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTellerID].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTellerID].WhereType = GRIDCOMBOTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptType].NameToShowUser = "Receipt Type";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptType].ReportName = "Type";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptType].DatabaseFieldName = "ReceiptType";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptType].SelectName = "ReceiptType";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptType].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptType].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptType].WhereType = GRIDCOMBONUMRANGE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptNumber].NameToShowUser = "Receipt Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptNumber].ReportName = "Receipt";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptNumber].DatabaseFieldName = "ReceiptNumber";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptNumber].SelectName = "ReceiptNumber";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptNumber].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptNumber].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptNumber].WhereType = GRIDNUMRANGE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReference].NameToShowUser = "Reference";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReference].ReportName = "Ref";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReference].DatabaseFieldName = "Ref";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReference].SelectName = "Ref";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReference].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReference].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReference].WhereType = GRIDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowName].NameToShowUser = "Name";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowName].ReportName = "Name";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowName].DatabaseFieldName = "NewArchive.Name";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowName].SelectName = "Archive.Name";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowName].Sort = true;
            // kk09212017 trocrs-59  Allow choice of search by name that Begins With or Contains criteria
            // arrCustomReport(frmCustomReport.lngRowName).ComboList = ""
            // arrCustomReport(frmCustomReport.lngRowName).WhereType = GRIDTEXT
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowName].ComboList = "#0;Begins With|#1;Contains";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowName].WhereType = GRIDCOMBOANDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl1].NameToShowUser = "Control 1";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl1].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl1].DatabaseFieldName = "Control1";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl1].SelectName = "Control1";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl1].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl1].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl1].WhereType = GRIDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl2].NameToShowUser = "Control 2";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl2].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl2].DatabaseFieldName = "Control2";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl2].SelectName = "Control2";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl2].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl2].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl2].WhereType = GRIDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl3].NameToShowUser = "Control 3";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl3].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl3].DatabaseFieldName = "Control3";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl3].SelectName = "Control3";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl3].Sort = true;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl3].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowControl3].WhereType = GRIDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber1].NameToShowUser = "Account Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber1].ReportName = "Account";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber1].DatabaseFieldName = "Account1";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber1].SelectName = "Account1";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber1].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber1].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber1].WhereType = GRIDBDACCOUNT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber2].NameToShowUser = "Account Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber2].ReportName = "Account";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber2].DatabaseFieldName = "Account2";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber2].SelectName = "Account2";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber2].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber2].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber2].WhereType = GRIDNONE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber3].NameToShowUser = "Account Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber3].ReportName = "Account";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber3].DatabaseFieldName = "Account3";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber3].SelectName = "Account3";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber3].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber3].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber3].WhereType = GRIDNONE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber4].NameToShowUser = "Account Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber4].ReportName = "Account";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber4].DatabaseFieldName = "Account4";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber4].SelectName = "Account4";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber4].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber4].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber4].WhereType = GRIDNONE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber5].NameToShowUser = "Account Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber5].ReportName = "Account";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber5].DatabaseFieldName = "Account5";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber5].SelectName = "Account5";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber5].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber5].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber5].WhereType = GRIDNONE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber6].NameToShowUser = "Account Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber6].ReportName = "Account";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber6].DatabaseFieldName = "Account6";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber6].SelectName = "Account6";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber6].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber6].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowAccountNumber6].WhereType = GRIDNONE;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowMVType].NameToShowUser = "Motor Vehicle Type";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowMVType].ReportName = "MV";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowMVType].DatabaseFieldName = "Comment";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowMVType].SelectName = "Comment";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowMVType].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowMVType].ComboList = "#0;All Types w/Summary|#1;DPR|#2;DPS|#3;ECO|#4;GVW|#5;LPS|#6;NRR|#7;NRT|#8;RRR|#9;RRT";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowMVType].WhereType = GRIDCOMBOTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTotal].NameToShowUser = "Receipt Total";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTotal].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTotal].DatabaseFieldName = "(CardAmount + CheckAmount + CashAmount)";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTotal].SelectName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTotal].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTotal].ComboList = "#0;<" + "\t" + "Less Than|#1;>" + "\t" + "Greater Than|#2;=" + "\t" + "Equals|#3;<>" + "\t" + "Not Equal";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowReceiptTotal].WhereType = GRIDCOMBOANDNUM;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTenderedTotal].NameToShowUser = "Tendered Total";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTenderedTotal].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTenderedTotal].DatabaseFieldName = "(CardAmount + CheckAmount + CashAmount + Change)";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTenderedTotal].SelectName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTenderedTotal].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTenderedTotal].ComboList = "#0;<" + "\t" + "Less Than|#1;>" + "\t" + "Greater Than|#2;=" + "\t" + "Equals|#3;<>" + "\t" + "Not Equal";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowTenderedTotal].WhereType = GRIDCOMBOANDNUM;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaidBy].NameToShowUser = "Paid By";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaidBy].ReportName = "Paid By";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaidBy].DatabaseFieldName = "PaidBy";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaidBy].SelectName = "PaidBy";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaidBy].Sort = false;
            // kk09212017 trocrs-59  Allow choice of search by paid by that Begins With or Contains criteria
            // arrCustomReport(frmCustomReport.lngRowPaidBy).ComboList = ""
            // arrCustomReport(frmCustomReport.lngRowPaidBy).WhereType = GRIDTEXT
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaidBy].ComboList = "#0;Begins With|#1;Contains";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaidBy].WhereType = GRIDCOMBOANDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCheckNumber].NameToShowUser = "Check Number";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCheckNumber].ReportName = "Check";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCheckNumber].DatabaseFieldName = "CheckNumber";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCheckNumber].SelectName = "CheckNumber";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCheckNumber].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCheckNumber].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowCheckNumber].WhereType = GRIDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowDateToShow].NameToShowUser = "Date To Show";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowDateToShow].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowDateToShow].DatabaseFieldName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowDateToShow].SelectName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowDateToShow].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowDateToShow].ComboList = "#0;Actual System Date|#1;Recorded Transaction Date";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowDateToShow].WhereType = GRIDCOMBOTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowBank].NameToShowUser = "Bank";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowBank].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowBank].DatabaseFieldName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowBank].SelectName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowBank].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowBank].ComboList = GetBankComboList();
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowBank].WhereType = GRIDCOMBOTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowComment].NameToShowUser = "Comment";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowComment].ReportName = "Comment";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowComment].DatabaseFieldName = "Comment";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowComment].SelectName = "Comment";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowComment].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowComment].ComboList = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowComment].WhereType = GRIDTEXT;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaymentType].NameToShowUser = "Payment Type";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaymentType].ReportName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaymentType].DatabaseFieldName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaymentType].SelectName = "";
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaymentType].Sort = false;
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaymentType].ComboList = GetPaymentTypeComboList();
            Statics.arrCustomReport[frmCustomReport.InstancePtr.lngRowPaymentType].WhereType = GRIDCOMBOTEXT;
            // this will fill the fields listbox
            FormName.lstFields.Clear();
            for (lngCT = 0; lngCT <= frmCustomReport.InstancePtr.MAX_VARIABLES; lngCT++)
            {
                if (Statics.arrCustomReport[lngCT].Sort)
                {
                    FormName.lstFields.AddItem(Statics.arrCustomReport[lngCT].NameToShowUser);
                }
            }
            // IF ONE OF THE FIELDS IS OF TYPE GRIDCOMBOIDTEXT THEN YOU NEED
            // TO CREATE A LIST TO SHOW WHEN THE USER CLICKS ON THAT CELL
            // IN THE GRID
            // MAL@20071010
            LoadGridCellAsCombo(FormName, frmCustomReport.InstancePtr.lngRowReceiptType, "SELECT TypeCode, TypeTitle FROM TYPE WHERE TypeTitle <> '' AND Deleted = 0 ORDER BY TypeCode", "TypeCode", "TypeCode", "Type");
            // LoadGridCellAsCombo FormName, frmCustomReport.lngRowReceiptType, "SELECT TypeCode, TypeTitle FROM TYPE WHERE TypeTitle <> '' ORDER BY TypeCode", "TypeCode", "TypeCode", "Type"
            // Call LoadGridCellAsCombo(FormName, 9, "#0;<" & vbTab & "Less Than|#1;>" & vbTab & "Greater Than|#2;=" & vbTab & "Equals")
            // Call LoadGridCellAsCombo(FormName, 10, "#0;<" & vbTab & "Less Than|#1;>" & vbTab & "Greater Than|#2;=" & vbTab & "Equals")
            // Call LoadGridCellAsCombo(FormName, 11, "#0;<" & vbTab & "Less Than|#1;>" & vbTab & "Greater Than|#2;=" & vbTab & "Equals")
            // Call LoadGridCellAsCombo(FormName, 12, "#0;<" & vbTab & "Less Than|#1;>" & vbTab & "Greater Than|#2;=" & vbTab & "Equals")
            // Call LoadGridCellAsCombo(FormName, 14, "#0;A - All Periods|#1;1|#2;2|#3;3|#4;4")
            // Call LoadGridCellAsCombo(FormName, 15, "#0;A" & vbTab & "Abatement|#1;C" & vbTab & "Correction|#2;D" & vbTab & "Discount|#3;P" & vbTab & "Regular Payment|#4;R" & vbTab & "Refunded Abatement|#5;S" & vbTab & "Supplemental|#6;U" & vbTab & "Tax Club Payment|#7;Y" & vbTab & "Pre Payments")
            // FINISH THE MID SECTION OF THE SQL STATEMENT        'XXXXXXX
            FormName.fraFields.Tag = "FROM (Receipt INNER JOIN Archive ON Receipt.ReceiptKey = Archive.ReceiptNumber)";
        }
        // vbPorter upgrade warning: FormName As Form	OnWrite(Form)
        private static void LoadSortList(dynamic FormName)
        {
            // LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
            int lngCT;
            // fill the sort listbox
            FormName.lstSort.Clear();
            for (lngCT = 0; lngCT <= frmCustomReport.InstancePtr.MAX_VARIABLES; lngCT++)
            {
                if (Statics.arrCustomReport[lngCT].Sort)
                {
                    FormName.lstSort.AddItem(Statics.arrCustomReport[lngCT].NameToShowUser);
                    FormName.lstSort.ItemData(FormName.lstSort.NewIndex, lngCT);
                }
            }
            // Dim intCounter As Integer
            // FormName.lstSort.Clear
            // For intCounter = 0 To FormName.lstFields.ListCount - 1
            // FormName.lstSort.AddItem FormName.lstFields.List(intCounter)
            // FormName.lstSort.ItemData(FormName.lstSort.NewIndex) = FormName.lstFields.ItemData(intCounter)
            // Next
        }
        // vbPorter upgrade warning: FormName As Form	OnWrite(Form)
        private static void LoadWhereGrid(dynamic FormName)
        {
            // LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
            int intCounter;
            int intpos;
            FormName.vsWhere.Rows = 0;
            FormName.vsWhere.Cols = 5;
            // For intCounter = 0 To FormName.lstFields.ListCount - 1
            for (intCounter = 0; intCounter <= frmCustomReport.InstancePtr.MAX_VARIABLES; intCounter++)
            {
                if (Statics.arrCustomReport[intCounter].WhereType != GRIDNONE)
                {
                    // FormName.vsWhere.AddItem FormName.lstFields.List(intCounter)
                    FormName.vsWhere.AddItem(Statics.arrCustomReport[intCounter].NameToShowUser);
                    // this will save the row that this field was added to
                    Statics.arrCustomReport[intCounter].RowNumber = FormName.vsWhere.Rows - 1;
                    FormName.vsWhere.TextMatrix(FormName.vsWhere.Rows - 1, 4, intCounter);
                    // IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
                    // THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
                    // THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
                    // Select Case strWhereType(intCounter)
                    switch (Statics.arrCustomReport[intCounter].WhereType)
                    {
                        case GRIDTEXT:
                            {
                                //FC:FINAL:CHN: Change using Form BackColor to set Grid cell no editable on Web version.
                                // FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.BackColor);
                                FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.vsWhere.BackColorFixed);
                                break;
                            }
                        case GRIDDATE:
                            {
                                break;
                            }
                        case GRIDCOMBOIDTEXT:
                            {
                                //FC:FINAL:CHN: Change using Form BackColor to set Grid cell no editable on Web version.
                                // FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.BackColor);
                                FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.vsWhere.BackColorFixed);
                                break;
                            }
                        case GRIDNUMRANGE:
                            {
                                break;
                            }
                        case GRIDCOMBOIDNUM:
                            {
                                //FC:FINAL:CHN: Change using Form BackColor to set Grid cell no editable on Web version.
                                // FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.BackColor);
                                FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.vsWhere.BackColorFixed);
                                break;
                            }
                        case GRIDCOMBOTEXT:
                            {
                                //FC:FINAL:CHN: Change using Form BackColor to set Grid cell no editable on Web version.
                                // FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.BackColor);
                                FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.vsWhere.BackColorFixed);
                                break;
                            }
                        case GRIDTEXTRANGE:
                            {
                                break;
                            }
                        case GRIDCOMBOANDNUM:
                            {
                                break;
                            }
                        case GRIDCOMBOANDTEXT:
                            {
                                break;
                            }
                        case GRIDBDACCOUNT:
                            {
                                //FC:FINAL:CHN: Change using Form BackColor to set Grid cell no editable on Web version.
                                // FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.BackColor);
                                FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.vsWhere.BackColorFixed);
                                break;
                            }
                        case GRIDLISTTEXT:
                            {
                                break;
                            }
                        case GRIDCOMBONUMRANGE:
                            {
                                break;
                            }
                        case GRIDCOMBORANGE:
                            {
                                // FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.rows - 1, 2) = FormName.BackColor
                                break;
                            }
                        default:
                            {
                                //FC:FINAL:CHN: Change using Form BackColor to set Grid cell no editable on Web version.
                                // FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.BackColor);
                                FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, FormName.vsWhere.BackColorFixed);
                                break;
                            }
                    }
                    //end switch
                }
                else
                {
                    Statics.arrCustomReport[intCounter].RowNumber = -1;
                }
            }
            // set the row of the other accounts to be pointed at the same info as the first account field
            Statics.arrCustomReport[10].RowNumber = Statics.arrCustomReport[2].RowNumber;
            // account 2
            Statics.arrCustomReport[11].RowNumber = Statics.arrCustomReport[2].RowNumber;
            // account 3
            Statics.arrCustomReport[12].RowNumber = Statics.arrCustomReport[2].RowNumber;
            // account 4
            Statics.arrCustomReport[13].RowNumber = Statics.arrCustomReport[2].RowNumber;
            // account 5
            Statics.arrCustomReport[14].RowNumber = Statics.arrCustomReport[2].RowNumber;
            // account 6
            // MAL@20081105: Changed to get the Receipt Number position here since the position can change as items as added
            // Tracker Reference: 13124
            intpos = GetReceiptNumberSortPosition(FormName);
            // FormName.lstSort.Selected(5) = True     'this is hard coded to select "Receipt Number" automatically
            //FormName.lstSort.Selected(intpos, true);
            // FC:FINAL:VGE - #833 'Selected' changed to 'SetSelected'
            FormName.lstSort.SetSelected(intpos, true);
            // SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
            // THE LAST COLUMN IS THERE TO HOLD ID VALUES IF THE TYPE IS OF
            // A COMBO BOX BECAUSE THE LIST DOES NOT STAY WITH THAT CELL ONCE
            // THE USER MOVES TO A NEW ONE.
            int wid;
            wid = FormName.vsWhere.WidthOriginal;
            FormName.vsWhere.ColWidth(0, FCConvert.ToInt32(wid * 0.35));
            FormName.vsWhere.ColWidth(1, FCConvert.ToInt32(wid * 0.3));
            FormName.vsWhere.ColWidth(2, FCConvert.ToInt32(wid * 0.3));
            FormName.vsWhere.ColWidth(3, 0);
            FormName.vsWhere.ColWidth(4, 0);
            FormName.vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
        }
        // vbPorter upgrade warning: FormName As Form	OnWrite(Form)
        private static void LoadGridCellAsCombo(Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
        {
            // THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
            // COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
            // ROW THAT IT IS IN THE GRID.
            // THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
            // WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
            if (Information.IsNothing(FieldName) || FieldName == string.Empty)
            {
                Statics.strComboList[intRowNumber, 0] = strSQL;
            }
            else
            {
                int intCounter;
                clsDRWrapper rsCombo = new clsDRWrapper();

                try
                {
                    rsCombo.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
                    if (intRowNumber == 3)
                    {
                        // type combo
                        while (!rsCombo.EndOfFile())
                        {
                            Statics.strComboList[intRowNumber, 0] += "|";
                            Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName) + "\t" + rsCombo.Get_Fields_String("TypeTitle");
                            Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
                            rsCombo.MoveNext();
                        }
                        Statics.arrCustomReport[3].ComboList = Statics.strComboList[intRowNumber, 0];
                    }
                    else
                    {
                        while (!rsCombo.EndOfFile())
                        {
                            Statics.strComboList[intRowNumber, 0] += "|";
                            Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
                            Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
                            rsCombo.MoveNext();
                        }
                    }
                }
                finally
                {
                    rsCombo.DisposeOf();
                }
            }
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public static bool IsValidDate(string DateToCheck)
        {
            bool IsValidDate = false;
            string strAnnTemp;
            // vbPorter upgrade warning: intMonth As short --> As int	OnWrite(string)
            int intMonth;
            // hold month
            // vbPorter upgrade warning: intDay As short --> As int	OnWrite(string)
            int intDay;
            // hold day
            int intYear;
            // hold year
            int intCheckFormat;
            // make sure date is format 'mm/dd'
            strAnnTemp = DateToCheck;
            intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
            intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
            switch (intMonth)
            {
                case 2:
                    {
                        // feb can't be more then 29 days
                        if (intDay > 29)
                        {
                            MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return IsValidDate;
                        }
                        break;
                    }
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    {
                        // these months no more then 31 days
                        if (intDay > 31)
                        {
                            MessageBox.Show("Invalid day on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return IsValidDate;
                        }
                        break;
                    }
                case 4:
                case 6:
                case 9:
                case 11:
                    {
                        // these months no more then 30 days
                        if (intDay > 30)
                        {
                            MessageBox.Show("Invalid day on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return IsValidDate;
                        }
                        break;
                    }
                default:
                    {
                        // not even a month
                        MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return IsValidDate;
                    }
            }
            //end switch
            IsValidDate = true;
            return IsValidDate;
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As string
        private static string GetBankComboList()
        {
            string GetBankComboList = "";
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this routine will return the list of banks in a combo list form
                clsDRWrapper rsBank = new clsDRWrapper();
                int lngCT = 0;
                string strTemp = "";

                try
                {
                    rsBank.OpenRecordset("SELECT * FROM Bank ORDER BY Name", modExtraModules.strCRDatabase);
                    while (!rsBank.EndOfFile())
                    {
                        lngCT = FCConvert.ToInt32(rsBank.Get_Fields_Int32("ID"));
                        strTemp += "#" + FCConvert.ToString(lngCT) + ";" + rsBank.Get_Fields_String("Name") + "  -  " + Strings.Format(rsBank.Get_Fields_Int32("ID"), "00000") + "|";
                        rsBank.MoveNext();
                    }
                }
                finally
                {
                    rsBank.DisposeOf();
                }
                GetBankComboList = strTemp;
                return GetBankComboList;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Bank Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetBankComboList;
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As string
        private static string GetCloseOutComboList()
        {
            string GetCloseOutComboList = "";
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this routine will return the list of banks in a combo list form
                clsDRWrapper rsCloseOut = new clsDRWrapper();
                int lngCT = 0;
                string strTemp = "";

                try
                {
                    rsCloseOut.OpenRecordset("SELECT * FROM CloseOut WHERE Type = 'R' ORDER BY ID desc", modExtraModules.strCRDatabase);
                    while (!rsCloseOut.EndOfFile())
                    {
                        lngCT = FCConvert.ToInt32(rsCloseOut.Get_Fields_Int32("ID"));
                        strTemp += "#" + FCConvert.ToString(lngCT) + ";" + Strings.Format(rsCloseOut.Get_Fields_DateTime("CloseoutDate"), "MM/dd/yyyy hh:mm tt") + "  -  " + Strings.Format(rsCloseOut.Get_Fields_Int32("ID"), "000000") + "|";
                        rsCloseOut.MoveNext();
                    }
                }
                finally
                {
                    rsCloseOut.DisposeOf();
                }
                GetCloseOutComboList = strTemp;
                return GetCloseOutComboList;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Close Out Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetCloseOutComboList;
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As string
        private static string GetPaymentTypeComboList()
        {
            string GetPaymentTypeComboList = "";
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this routine will return the list of payment types in a combo list form
                string strTemp;
                strTemp = "#0;CASH|#1;CHECK|#2;CREDIT/DEBIT CARD";
                GetPaymentTypeComboList = strTemp;
                return GetPaymentTypeComboList;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Payment Type Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetPaymentTypeComboList;
        }
        // vbPorter upgrade warning: FormName As Form	OnWrite(Form)
        // vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
        private static short GetReceiptNumberSortPosition(dynamic FormName)
        {
            short GetReceiptNumberSortPosition = 0;
            int intResult;
            for (intResult = 0; intResult <= FormName.lstSort.ListCount - 1; intResult++)
            {
                if (FormName.lstSort.List(intResult) == "Receipt Number")
                {
                    break;
                }
            }
            // intResult
            GetReceiptNumberSortPosition = FCConvert.ToInt16(intResult);
            return GetReceiptNumberSortPosition;
        }

        public class StaticVariables
        {
            // ********************************************************
            // LAST UPDATED   :               10/11/2006              *
            // MODIFIED BY    :               Jim Bertolino           *
            // *
            // DATE           :                                       *
            // WRITTEN BY     :                                       *
            // *
            // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
            // ********************************************************
            //=========================================================
            public int intNumberOfSQLFields;
            public int[] strFieldWidth = new int[50 + 1];
            public string strCustomSQL = string.Empty;
            public string strColumnCaptions = "";
            public string[] strFields = new string[50 + 1];
            public string[] strFieldNames = new string[50 + 1];
            public string[] strFieldCaptions = new string[50 + 1];
            public string strCustomTitle = string.Empty;
            public string strReportType = "";
            public string[,] strComboList = new string[50 + 1, 50 + 1];
            public string[] strWhereType = new string[50 + 1];
            public string strBinaryReportType = string.Empty;
            public CustomReportField[] arrCustomReport = new CustomReportField[50 + 1];
            public bool boolRSMVSummary;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
