﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarCCReportDetail.
	/// </summary>
	public partial class sarCCReportDetail : FCSectionReport
	{
		public static sarCCReportDetail InstancePtr
		{
			get
			{
				return (sarCCReportDetail)Sys.GetInstance(typeof(sarCCReportDetail));
			}
		}

		protected sarCCReportDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
        clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsData2 = new clsDRWrapper();
		string strSQL;
		bool boolUsePrevYears = false;

		public sarCCReportDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarCCReportDetail_ReportEnd;
		}

        private void SarCCReportDetail_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
			rsData2.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				FillDetail();
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strMaster;
			string strArchive;
			string strReceipt;
			string strDateField;
			DateTime dateOfCloseOutEnd;
			DateTime dateOfCloseOutStart;
			string strDateRange;

			// We need a date range for Prev Year Receipts because ReceiptKey isn't unique
			boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
			if (boolUsePrevYears)
			{
				strMaster = "LastYearCCMaster";
				strArchive = "LastYearArchive";
				strReceipt = "LastYearReceipt";
				strDateField = "LastYearReceiptDate,";
				dateOfCloseOutEnd = GetCloseOutDate(frmDailyReceiptAudit.InstancePtr.lngCloseoutKey);
				dateOfCloseOutStart = GetPrevCloseOutDate(frmDailyReceiptAudit.InstancePtr.lngCloseoutKey);
				strDateRange = "AND LastYearReceiptDate < '" + dateOfCloseOutEnd.ToString("MM/dd/yyyy h:mm:ss tt") + "'";
				if (dateOfCloseOutStart != DateTime.MinValue)
				{
					strDateRange += " AND LastYearReceiptDate > '" + dateOfCloseOutStart.ToString("MM/dd/yyyy h:mm:ss tt") + "'";
				}
			}
			else
			{
				strMaster = "CCMaster";
				strArchive = "Archive";
				strReceipt = "Receipt";
				strDateField = "";
				dateOfCloseOutEnd = DateTime.MinValue;
				dateOfCloseOutStart = DateTime.MinValue;
				strDateRange = "";
			}
			strSQL = "SELECT sum(ISNULL(ccmast.Amount,0))  as sumAmt, CCType.ID as CardKey " +
			         "FROM (" + strMaster + " ccmast INNER JOIN CCType ON ccmast.CCType = CCType.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM "+ strArchive + " " +
			         "WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") as Arch ON ccmast.ReceiptNumber = Arch.ReceiptNumber GROUP BY CCType.ID";
			rsData2.OpenRecordset(strSQL);
			switch (modGlobal.Statics.gintCCReportOrder)
			{
				case 0:
					{
						// by receipt
						strSQL = "SELECT rcpt.ReceiptNumber as RNum, " + strDateField + " ccmast.ReceiptNumber, ccmast.Last4Digits as Last4, ISNULL(ccmast.Amount,0)  as Amt, CCType.Type, CCType.IsDebit as Debit, CCType.ID as CKey " +
						         "FROM ((" + strMaster + " ccmast INNER JOIN CCType ON ccmast.CCType = CCType.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON ccmast.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN " + strReceipt + " rcpt ON Arch.ReceiptNumber = rcpt.ReceiptKey " + strDateRange + " " + 
						         "WHERE CCType.ID = " + FCConvert.ToString(Conversion.Val(this.UserData));
						break;
					}
				case 1:
					{
						// by amount
						strSQL = "SELECT sum(ISNULL(ccmast.Amount,0)  as sumAmt " +
						         "FROM " + strMaster + " ccmast INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " " + 
						         "WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") as Arch ON ccmast.ReceiptNumber = Arch.ReceiptNumber ";
						// do not group
						rsData2.OpenRecordset(strSQL);
						strSQL = "SELECT rcpt.ReceiptNumber as RNum, " + strDateField + " ccmast.ReceiptNumber, ccmast.Last4Digits as Last4, ISNULL(ccmast.Amount,0)  as Amt, CCType.Type, CCType.IsDebit as Debit, CCType.ID as CKey " + 
						         "FROM ((" + strMaster + " ccmast INNER JOIN CCType ON ccmast.CCType = CCType.ID) INNER JOIN (SELECT DISTINCT ReceiptNumber FROM " + strArchive + " WHERE " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + ") AS Arch ON ccmast.ReceiptNumber = Arch.ReceiptNumber) INNER JOIN " + strReceipt + " rcpt ON Arch.ReceiptNumber = rcpt.ReceiptKey ORDER BY Amt";
						// no bank splits  'kk 06062013  ORDER BY convert(double, Amount - CCMaster.ConvenienceFee)
						break;
					}
			}
			//end switch
			rsData.OpenRecordset(strSQL);
			SetData();
		}

		private void FillDetail()
		{
			clsDRWrapper rsOwner = new clsDRWrapper();
            fldAmount.Text = rsData.Get_Fields_Double("Amt").FormatAsCurrencyNoSymbol();
			fldBankNumber.Text = "";
   //         if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Last4"))) != "")
			//{
   //             fldCardNum.Text = "************" + FCConvert.ToString(rsData.Get_Fields("Last4"));
			//}
			//else
			//{
				fldCardNum.Text = "";
			//}
            fldReceiptNumber.Text = rsData.Get_Fields_Int32("RNum").ToString();
            if (boolUsePrevYears)
			{
				rsOwner.OpenRecordset("SELECT * FROM LastYearReceipt " + 
				                      "WHERE ReceiptKey = " + rsData.Get_Fields_Int32("ReceiptNumber") + " AND LastYearReceiptDate = '" + rsData.Get_Fields_DateTime("LastYearReceiptDate").ToString("MM/dd/yyyy h:mm:ss tt") + "'", "TWCR0000.vb1");
			}
			else
			{
				rsOwner.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + rsData.Get_Fields_Int32("ReceiptNumber"), "TWCR0000.vb1");
			}

			if (rsOwner.EndOfFile() != true && rsOwner.BeginningOfFile() != true)
			{
				fldName.Text = rsOwner.Get_Fields_String("PaidBy");
			}
			else
			{
				fldName.Text = "";
			}
			rsOwner.DisposeOf();
		}

		private void SetData()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			switch (modGlobal.Statics.gintCCReportOrder)
			{
				case 0:
					{
						if (rsData2.FindFirstRecord("CardKey", Conversion.Val(this.UserData)))
						{
							if (!rsData2.NoMatch)
							{
                                fldBankTotal.Text = rsData2.Get_Fields_Double("sumAmt").FormatAsCurrencyNoSymbol();
                                rsTemp.OpenRecordset("SELECT * FROM CCType WHERE ID = " + rsData2.Get_Fields_Int32("CardKey"));
								if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
								{
                                    if (rsTemp.Get_Fields_String("Type").HasText())
									{
                                        lblBankNumberTotal.Text = rsTemp.Get_Fields_String("Type") + " Total:";
                                        lblBankNumber.Text = rsTemp.Get_Fields_String("Type");
									}
									else
									{
										lblBankNumberTotal.Text = "Card Total:";
										lblBankNumber.Text = "Card Number";
									}
								}
								else
								{
									lblBankNumberTotal.Text = "Card Total:";
									lblBankNumber.Text = "Card Number";
								}
							}
							else
							{
								lblBankNumberTotal.Text = "Card Total:";
								lblBankNumber.Text = "Card Type";
								fldBankTotal.Text = "ERROR";
							}
						}
						else
						{
							lblBankNumberTotal.Text = "Card Total:";
							lblBankNumber.Text = "Card Type";
							fldBankTotal.Text = "ERROR";
						}
						break;
					}
				default:
					{
						if (!rsData2.EndOfFile())
						{
                            fldBankTotal.Text = rsData2.Get_Fields_Double("sumAmt").FormatAsCurrencyNoSymbol();
						}
						else
						{
							fldBankTotal.Text = "ERROR";
						}
						lblBankNumberTotal.Text = "All Card Types - Total:";
						lblBankNumber.Text = "All Card Types";
						break;
					}
			}
			rsTemp.DisposeOf();
		}

		private DateTime GetPrevCloseOutDate(int lngDCO)
		{
			clsDRWrapper rsCO = new clsDRWrapper();
			DateTime closeOutDate = DateTime.MinValue;

			try
			{
				rsCO.OpenRecordset("SELECT TOP 1 ID FROM CloseOut WHERE ID < " + lngDCO + " AND Type  = 'R' ORDER BY ID DESC", modExtraModules.strCRDatabase);
				if (!rsCO.EndOfFile())
				{
					closeOutDate = GetCloseOutDate(rsCO.Get_Fields_Int32("ID"));
				}
			}
			catch (Exception e)
			{
				//  MsgBox("ERROR #" + Err.Number + " - " + e.Message + ".")    //   "Error Getting Previous Close Out Date
			}
			rsCO.DisposeOf();
			return closeOutDate;
		}

		private DateTime GetCloseOutDate(int lngDCO)
		{
			clsDRWrapper rsD = new clsDRWrapper();
			DateTime closeOutDate = DateTime.MinValue;

			try
			{
				rsD.OpenRecordset("SELECT CloseOutDate FROM CloseOut WHERE ID = " + lngDCO, modExtraModules.strCRDatabase);
				if (!rsD.EndOfFile())
                {
                    closeOutDate = rsD.Get_Fields_DateTime("CloseOutDate");
                }
			}
			catch (Exception e)
			{
            }
			rsD.DisposeOf();
			return closeOutDate;
		}

		private void sarCCReportDetail_Load(object sender, System.EventArgs e)
		{

		}
	}
}
