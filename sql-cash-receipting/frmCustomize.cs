﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	public partial class frmCustomize : BaseForm
	{
		public frmCustomize()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbOptions.SelectedIndex = 0;
			this.txtPymtPortal.Capitalize = false;
            ServiceCodesGrid.CellMouseClick += ServiceCodesGrid_CellMouseClick;
            btnAddServiceCode.Click += BtnAddServiceCode_Click;
            btnRemoveServiceCode.Click += BtnRemoveServiceCode_Click;
		}

        private void BtnRemoveServiceCode_Click(object sender, EventArgs e)
        {
            RemoveServiceCode();
        }

        private void BtnAddServiceCode_Click(object sender, EventArgs e)
        {
            AddServiceCode();
        }

        private void ServiceCodesGrid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == (int) ServiceCodeColumns.Default)
            {
                ValidateDefaultServiceCodes(e.RowIndex + 1);
            }
        }

        

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCustomize InstancePtr
		{
			get
			{
				return (frmCustomize)Sys.GetInstance(typeof(frmCustomize));
			}
		}

		protected frmCustomize _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/29/2006              *
		// ********************************************************
		bool boolLoaded;
		int lngReceiptNumber;
		string strPassword = "";
		bool boolDirty;
		string strBankString = "";
		int lngColFund;
		int lngColDesc;
		int lngColKey;
		int lngColBank;
        private bool loading = true;
        private cSettingsController settingCont = new cSettingsController();
        private clsGridAccount vsReceivableGrid_AutoInitialized;
        private List<int> RemovedServiceCodes = new List<int>();

		private clsGridAccount vsReceivableGrid
		{
			get
			{
				if (vsReceivableGrid_AutoInitialized == null)
				{
					vsReceivableGrid_AutoInitialized = new clsGridAccount();
				}
				return vsReceivableGrid_AutoInitialized;
			}
			set
			{
				vsReceivableGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsSeperateCreditCardCashGrid_AutoInitialized;

		private clsGridAccount vsSeperateCreditCardCashGrid
		{
			get
			{
				if (vsSeperateCreditCardCashGrid_AutoInitialized == null)
				{
					vsSeperateCreditCardCashGrid_AutoInitialized = new clsGridAccount();
				}
				return vsSeperateCreditCardCashGrid_AutoInitialized;
			}
			set
			{
				vsSeperateCreditCardCashGrid_AutoInitialized = value;
			}
		}

		string strEPaymentClientID = "";
		string strEPaymentClientPassword = "";
		string strEPaymentClientGateway = "";
		string strEPaymentUserID = "";
		string strEPaymentClientIDConvenienceFee = "";
		string strEPaymentClientPasswordConvenienceFee = "";
		string strEPaymentClientGatewayConvenienceFee = "";
		string strEPaymentUserIDConvenienceFee = "";
		string strEPaymentClientIDVISA = "";
		string strEPaymentClientPasswordVISA = "";
		string strEPaymentClientGatewayVISA = "";
		string strEPaymentUserIDVISA = "";
		string strEPaymentClientIDMCDiscover = "";
		string strEPaymentClientPasswordMCDiscover = "";
		string strEPaymentClientGatewayMCDiscover = "";
		string strEPaymentUserIDMCDiscover = "";
		string strEPaymentClientIDAmEx = "";
		string strEPaymentClientPasswordAmEx = "";
		string strEPaymentClientGatewayAmEx = "";
		string strEPaymentUserIDAmEx = "";
		string strEPaymentClientIDMCDiscoverConvenienceFee = "";
		string strEPaymentClientPasswordMCDiscoverConvenienceFee = "";
		string strEPaymentClientGatewayMCDiscoverConvenienceFee = "";
		string strEPaymentUserIDMCDiscoverConvenienceFee = "";
		string strEPaymentClientIDAmExConvenienceFee = "";
		string strEPaymentClientPasswordAmExConvenienceFee = "";
		string strEPaymentClientGatewayAmExConvenienceFee = "";
		string strEPaymentUserIDAmExConvenienceFee = "";

		

		private void chkAuditPassword_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			// something has changed
			if (chkAuditPassword.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtAuditPassword.Enabled = true;
			}
			else
			{
				txtAuditPassword.Enabled = false;
			}
		}

		private void chkCDConnected_CheckedChanged(object sender, System.EventArgs e)
		{
			// this will show/hide the options that are associated with the cash drawer
			boolDirty = true;
			if (chkCDConnected.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtCDCode.Enabled = true;
				cmbPrinterPort.Enabled = true;
				cmbRepeat.Enabled = true;
				lblCDCode.Enabled = true;
				lblPortID.Enabled = true;
				lblCDRepeat.Enabled = true;
			}
			else
			{
				txtCDCode.Enabled = false;
				cmbPrinterPort.Enabled = false;
				cmbRepeat.Enabled = false;
				lblCDCode.Enabled = false;
				lblPortID.Enabled = false;
				lblCDRepeat.Enabled = false;
			}
		}

		private void chkCLAuditPortrait_Click()
		{
			boolDirty = true;
		}

		private void chkClearPaidBy_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			if (chkClearPaidBy.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblClearPaidBy.Enabled = true;
				cmbClearPaidBy.Enabled = true;
			}
			else
			{
				lblClearPaidBy.Enabled = false;
				cmbClearPaidBy.Enabled = false;
			}
		}

		private void chkCloseMV_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkCloseMV_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (chkPartialAudit.CheckState == Wisej.Web.CheckState.Checked)
			{
				MessageBox.Show("Automatic Motor Vehicle close out is not allowed with use of partial audits.", "Partial Audit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				chkCloseMV.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void chkCloseMV_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			if (chkPartialAudit.CheckState == Wisej.Web.CheckState.Checked)
			{
				MessageBox.Show("Automatic Motor Vehicle close out is not allowed with use of partial audits.", "Partial Audit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				chkCloseMV.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

	
		private void chkCrossCheck_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!(chkCrossCheck.CheckState == Wisej.Web.CheckState.Checked))
			{
				// MAL@20071107: Changed to not clear the combobox
				// cmbCrossCheck.ListIndex = -1
				cmbCrossCheck.SelectedIndex = 0;
				cmbCrossCheck.Enabled = false;
			}
			else
			{
				cmbCrossCheck.SelectedIndex = 0;
				cmbCrossCheck.Enabled = true;
			}
		}

		private void chkDailyAuditByAlpha_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkDepositEntry_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkExclusive_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkNoMargin_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			if (chkNoMargin.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkThinReceipt.CheckState = Wisej.Web.CheckState.Unchecked;
				chkThinReceipt.Enabled = false;
			}
			else
			{
				chkThinReceipt.Enabled = true;
			}
		}

		private void chkPartialAudit_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			if (chkCloseMV.CheckState == Wisej.Web.CheckState.Checked)
			{
				MessageBox.Show("Automatic Motor Vehicle close out is not allowed with use of partial audits.", "Partial Audit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				chkCloseMV.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void chkPrintAuditRE_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkPrintAuditUT_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkPrintCCReport_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			if (chkPrintCCReport.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblCCReportSort.Enabled = true;
				cmbCCReportSort.Enabled = true;
			}
			else
			{
				lblCCReportSort.Enabled = false;
				cmbCCReportSort.Enabled = false;
			}
		}

		private void chkRoutNumsInBnkLst_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkSeperateCreditCardCashAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkSeperateCreditCardCashAccount.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraSeperateCreditCardCashAccount.Enabled = true;
				txtSeperateCreditCardCashAcct.Enabled = true;
			}
			else
			{
				fraSeperateCreditCardCashAccount.Enabled = false;
				txtSeperateCreditCardCashAcct.Text = "";
				txtSeperateCreditCardCashAcct.Enabled = false;
			}
		}

		private void chkSortTypeAlpha_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkSP200_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkTellerCheckReport_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkTellerNewPage_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkTellerReportSeq_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkTellerSeparate_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkTellerWithAudit_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkUTAuditPortrait_Click()
		{
			boolDirty = true;
		}

		private void cmbExtraLines_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbFontSize_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbPrinterPort_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbReceiptOffset_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbRepeat_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbTellerUsed_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			if (cmbTellerUsed.Items[cmbTellerUsed.SelectedIndex].ToString() == "No")
			{
				chkTellerNewPage.Enabled = false;
				chkTellerReportSeq.Enabled = false;
				chkTellerSeparate.Enabled = false;
				chkTellerWithAudit.Enabled = false;
				chkTellerCheckReport.Enabled = false;
			}
			else
			{
				chkTellerNewPage.Enabled = true;
				chkTellerReportSeq.Enabled = true;
				chkTellerSeparate.Enabled = true;
				chkTellerWithAudit.Enabled = true;
				chkTellerCheckReport.Enabled = true;
			}
		}

		private void cmbWideLines_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void frmCustomize_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				StartUp();
				SetDefaults();
				HandlePartialPermission_2(modGlobal.CRSECURITYCUSTOMIZE);
				boolLoaded = true;
				boolDirty = false;
			}
		}

		private void frmCustomize_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// capture the escape ID
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuProcessExit_Click();
						// Case vbKeyF9
						// KeyCode = 0
						// mnuProcessSave_Click
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						// mnuProcessSaveExit_Click
						break;
					}
			}
			//end switch

		}

		private void frmCustomize_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
            lngColFund = 0;
			lngColDesc = 1;
			lngColKey = 2;
			lngColBank = 3;
			if (modGlobal.Statics.gboolUseUSLExport)
			{
				chkUSLExport.Enabled = true;
			}
			else
			{
				chkUSLExport.Enabled = false;
			}

			vsReceivableGrid.AccountCol = -1;
		}

		private void StartUp()
        {
            loading = true;
			SetupServiceCodesGrid();
			// fill combo boxes with hardcoded options
			cmbExtraLines.Clear();
			cmbExtraLines.AddItem("0");
			cmbExtraLines.AddItem("1");
			cmbExtraLines.AddItem("2");
			cmbExtraLines.AddItem("3");
			cmbExtraLines.AddItem("4");
			cmbExtraLines.AddItem("5");
			cmbExtraLines.AddItem("6");
			cmbTellerUsed.Clear();
			cmbTellerUsed.AddItem("Yes");
			cmbTellerUsed.AddItem("No");
			cmbTellerUsed.AddItem("Mandatory");
			cmbTellerUsed.AddItem("Password");
			cmbWideLines.Clear();
			cmbWideLines.AddItem("22");
			cmbWideLines.AddItem("33");
			cmbWideLines.AddItem("66");
			cmbRepeat.Clear();
			cmbRepeat.AddItem("1");
			cmbRepeat.AddItem("2");
			cmbRepeat.AddItem("3");
			cmbRepeat.AddItem("4");
			cmbRepeat.AddItem("5");
			cmbRepeat.AddItem("6");
			cmbRepeat.AddItem("7");
			cmbRepeat.AddItem("8");
			cmbReceiptOffset.Clear();
			cmbReceiptOffset.AddItem("0");
			cmbReceiptOffset.AddItem("1");
			cmbReceiptOffset.AddItem("2");
			cmbReceiptOffset.AddItem("3");
			cmbReceiptOffset.AddItem("4");
			cmbReceiptOffset.AddItem("5");
			cmbFontSize.Clear();
			cmbFontSize.AddItem("8");
			cmbFontSize.AddItem("9");
			cmbFontSize.AddItem("10");
			cmbFontSize.AddItem("11");
			cmbFontSize.AddItem("12");
			cmbCrossCheck.Clear();
			cmbCrossCheck.AddItem("Never");
			cmbCrossCheck.AddItem("Daily");
			cmbCrossCheck.AddItem("Weekly");
			cmbCrossCheck.AddItem("Monthly");

			if (modGlobalConstants.Statics.gboolMV)
			{
				chkCloseMV.Enabled = true;
			}
			else
			{
				chkCloseMV.Enabled = false;
			}
			txtSeperateCreditCardCashAcct.MaxLength = FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)));
			FillPrinterPortCombo();
        }

		private void SetDefaults()
        {
            loading = true;
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will get all of the current default information from the database
				clsDRWrapper rsDef = new clsDRWrapper();
				clsDRWrapper rsIME = new clsDRWrapper();
				string strSQL;
				int intCT;
				string strTemp = "";

                strTemp = settingCont.GetSettingValue("ShowServiceCodeOnPaymentScreen", "CashReceipts", "", "", "");

                if (strTemp == "")
                {
                    chkShowServiceCode.Checked = false;
                    settingCont.SaveSetting("false", "ShowServiceCodeOnPaymentScreen", "CashReceipts", "", "", "");
                }
                else
                {
                    chkShowServiceCode.Checked = FCConvert.CBool(strTemp);
                }
				strSQL = "SELECT * FROM CashRec";
				rsDef.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
				if (rsDef.EndOfFile() != true)
				{
					intErr = 1;
					if (modGlobalConstants.Statics.gboolBD)
					{
						chkExclusive.Enabled = true;
						if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("gboolBDExclusive")))
						{
							chkExclusive.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkExclusive.CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
					else
					{
						chkExclusive.CheckState = Wisej.Web.CheckState.Unchecked;
						chkExclusive.Enabled = false;
					}
					intErr = 2;
					intErr = 3;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("DailyAuditByAlpha")))
					{
						chkDailyAuditByAlpha.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDailyAuditByAlpha.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 4;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("AllowPartialAudit")))
					{
						chkPartialAudit.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPartialAudit.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (modGlobal.Statics.gstrPrintSigLine == "P")
					{
						cmbSignatureNo.SelectedIndex = 1;
					}
					else if (modGlobal.Statics.gstrPrintSigLine == "A")
					{
						cmbSignatureNo.SelectedIndex = 2;
					}
					else
					{
						cmbSignatureNo.SelectedIndex = 0;
					}
					intErr = 5;
					cmbWideLines.Enabled = true;
					cmbFontSize.Enabled = true;
					cmbExtraLines.Enabled = true;
					// get the default receipt length
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideNoMargin", ref strTemp);
					intErr = 6;
					if (Strings.UCase(strTemp) == "TRUE")
					{
						chkNoMargin.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkNoMargin.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 7;
					// get the default receipt length
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideCompact", ref strTemp);
					if (Strings.UCase(strTemp) == "TRUE")
					{
						chkCompactReceipt.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkCompactReceipt.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 8;
					// get the default receipt length
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideThin", ref strTemp);
					if (Strings.UCase(strTemp) == "TRUE")
					{
						chkThinReceipt.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkThinReceipt.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "MVDelay", ref strTemp);
					if (Information.IsNumeric(strTemp))
					{
						txtMVDelay.Text = strTemp;
					}
					else
					{
						txtMVDelay.Text = "0";
					}
					intErr = 9;
					// get the default receipt length
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptLength", ref strTemp);
					intErr = 10;
					for (intCT = 0; intCT <= cmbWideLines.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbWideLines.Items[intCT].ToString()) == Conversion.Val(strTemp))
						{
							cmbWideLines.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT == cmbWideLines.Items.Count)
						cmbWideLines.SelectedIndex = 0;
					intErr = 11;
					// Extra Lines
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRExtraLength", ref strTemp);
					for (intCT = 0; intCT <= cmbExtraLines.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbExtraLines.Items[intCT].ToString()) == Conversion.Val(strTemp))
						{
							cmbExtraLines.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT == cmbExtraLines.Items.Count)
						cmbExtraLines.SelectedIndex = 0;
					intErr = 12;
					if (cmbReceiptWidth.SelectedIndex != 1)
					{
						cmbWideLines.Enabled = false;
					}
					else
					{
						cmbExtraLines.Enabled = false;
					}
					intErr = 13;
					// Wide Receipt Font Size
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideFontSize", ref strTemp);
					for (intCT = 0; intCT <= cmbFontSize.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbFontSize.Items[intCT].ToString()) == Conversion.Val(strTemp))
						{
							cmbFontSize.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT == cmbFontSize.Items.Count)
						cmbFontSize.SelectedIndex = 0;
					intErr = 14;
					// Receipt Offset
					// get from registry
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptOffset", ref strTemp);
					intErr = 15;
					for (intCT = 0; intCT <= cmbReceiptOffset.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbReceiptOffset.Items[intCT].ToString()) == Conversion.Val(strTemp))
						{
							cmbReceiptOffset.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT == cmbReceiptOffset.Items.Count && cmbReceiptOffset.Items.Count > 0)
						cmbReceiptOffset.SelectedIndex = 0;
					intErr = 16;
					txtTopComment.Text = Strings.Trim(rsDef.Get_Fields_String("TopComment") + " ");
					txtBottomComment.Text = Strings.Trim(rsDef.Get_Fields_String("BottomComment") + " ");
					intErr = 17;
					// Is a cash drawer connected
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDConnected", ref strTemp);
					intErr = 18;
					if (Strings.UCase(strTemp) == "TRUE")
					{
						chkCDConnected.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkCDConnected.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 19;
					// Cash Drawer Code
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDCode", ref strTemp);
					txtCDCode.Text = Strings.Trim(strTemp + " ");
					// txtCDCode.Text = Trim(.Fields("CDCode") & " ")
					intErr = 20;
					// Repeat Count
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRRepeatCount", ref strTemp);
					intErr = 21;
					for (intCT = 0; intCT <= cmbRepeat.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbRepeat.Items[intCT].ToString()) == Conversion.Val(strTemp))
						{
							cmbRepeat.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT == cmbRepeat.Items.Count)
						cmbRepeat.SelectedIndex = 0;
					intErr = 22;
					// Cash Drawer Port
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDPortID", ref strTemp);
					intErr = 23;
					if (Strings.Trim(strTemp) != "")
					{
						for (intCT = 0; intCT < cmbPrinterPort.Items.Count; intCT++)
						{
							if (Strings.Trim(cmbPrinterPort.Items[intCT].ToString()) == Strings.Trim(strTemp))
							{
								cmbPrinterPort.SelectedIndex = intCT;
								break;
							}
						}
					}
					else
					{
						cmbPrinterPort.SelectedIndex = -1;
					}
					intErr = 24;
					// Narrow Receipt
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRNarrowReceipt", ref strTemp);
					if (FCConvert.CBool(strTemp == "TRUE"))
					{
						// If .Fields("NarrowReceipt") Then
						cmbReceiptWidth.SelectedIndex = 0;
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRSP200Printer", ref strTemp);
						if (FCConvert.CBool(strTemp == "TRUE"))
						{
							// If CBool(.Fields("SP200Printer")) Then
							chkSP200.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkSP200.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						ReceiptWidthLock_2(0);
					}
					else
					{
						cmbReceiptWidth.SelectedIndex = 1;
						chkSP200.CheckState = Wisej.Web.CheckState.Unchecked;
						ReceiptWidthLock_2(1);
					}
					intErr = 25;
					// automatic check rec entries
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("AutoCheckRecEntry")))
					{
						chkDepositEntry.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDepositEntry.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 26;
					for (intCT = 0; intCT <= cmbTellerUsed.Items.Count - 1; intCT++)
					{
						if (Strings.Left(cmbTellerUsed.Items[intCT].ToString(), 1) == FCConvert.ToString(rsDef.Get_Fields_String("TellerIDUsed")))
						{
							cmbTellerUsed.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT == cmbTellerUsed.Items.Count)
						cmbTellerUsed.SelectedIndex = 0;
					intErr = 27;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("TellerReportWithAudit")))
					{
						chkTellerWithAudit.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkTellerWithAudit.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 28;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("gboolTellerCheckReportWithAudit")))
					{
						chkTellerCheckReport.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkTellerCheckReport.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 29;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("SkipPageForTeller")))
					{
						chkTellerNewPage.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkTellerNewPage.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 30;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("CashTellersOutSeperately")))
					{
						chkTellerSeparate.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkTellerSeparate.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 31;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("AuditSeqbyRecNumber")))
					{
						chkTellerReportSeq.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkTellerReportSeq.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 32;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("PasswordProtectAudit")))
					{
						chkAuditPassword.CheckState = Wisej.Web.CheckState.Checked;
						txtAuditPassword.Text = modEncrypt.ToggleEncryptCode(rsDef.Get_Fields_String("AuditPassword"));
					}
					else
					{
						chkAuditPassword.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 33;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("CloseMVPeriod")) && modGlobalConstants.Statics.gboolMV)
					{
						chkCloseMV.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkCloseMV.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 34;
					cmbCheckReportOrder.SelectedIndex = FCConvert.ToInt16(Conversion.Val(rsDef.Get_Fields_Int16("CheckReportOrder")));
					intErr = 35;
					// Audit Top Margin Adjustment
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "AuditMarginAdjustmentTop", ref strTemp);
					txtAuditMarginTop.Text = Strings.Trim(strTemp + " ");
					intErr = 36;
					// USL Export
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("USLExport")) && modGlobal.Statics.gboolUseUSLExport)
					{
						chkUSLExport.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkUSLExport.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 37;
					// Print the RE, PP, UT or AR Audits
					if (modGlobalConstants.Statics.gboolCL)
					{
						chkPrintAuditRE.Enabled = true;
						if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("PrintCLAudit")))
						{
							chkPrintAuditRE.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkPrintAuditRE.CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
					else
					{
						chkPrintAuditRE.Enabled = false;
					}
					if (modGlobalConstants.Statics.gboolUT)
					{
						chkPrintAuditUT.Enabled = true;
						if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("PrintUTAudit")))
						{
							chkPrintAuditUT.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkPrintAuditUT.CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
					else
					{
						chkPrintAuditUT.Enabled = false;
					}
					if (modGlobalConstants.Statics.gboolAR)
					{
						chkPrintAuditUT.Enabled = true;
						if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("PrintARAudit")))
						{
							chkPrintAuditAR.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkPrintAuditAR.CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
					else
					{
						chkPrintAuditAR.Enabled = false;
					}
					intErr = 38;
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("UppercaseTypes")))
					{
						chkUppercaseTypes.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkUppercaseTypes.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (modGlobalConstants.Statics.gboolCK)
					{
						if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("BreakdownCKTypes")))
						{
							chkBreakdownCK.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkBreakdownCK.CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
					else
					{
						chkBreakdownCK.Enabled = false;
					}
					switch (FCConvert.ToInt32(rsDef.Get_Fields_Int32("CrossCheckDefault")))
					{
						case 0:
							{
								// use last EOY Date
								cmbCrossCheckDate.SelectedIndex = 0;
								txtCrossCheckDate.Enabled = false;
								txtCrossCheckDate.Text = "";
								break;
							}
						case 1:
							{
								// use last Cross Check Date
								cmbCrossCheckDate.SelectedIndex = 1;
								txtCrossCheckDate.Enabled = false;
								txtCrossCheckDate.Text = "";
								break;
							}
						case 2:
							{
								// specific date
								cmbCrossCheckDate.SelectedIndex = 2;
								txtCrossCheckDate.Enabled = true;
								txtCrossCheckDate.Text = FCConvert.ToString(rsDef.Get_Fields_DateTime("CrossCheckDefaultDate"));
								break;
							}
					}
					//end switch
					if (modGlobalConstants.Statics.gboolMV)
					{
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "MVCheck", ref strTemp);
						if (strTemp == "TRUE")
						{
							chkMVCheck.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkMVCheck.CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
					else
					{
						chkMVCheck.Enabled = false;
					}
					intErr = 39;
					// Auto Cross Check Report
					string vbPorterVar = rsDef.Get_Fields_String("AutoRunCrossCheck");
					if (vbPorterVar == "D")
					{
						chkCrossCheck.CheckState = Wisej.Web.CheckState.Checked;
						cmbCrossCheck.SelectedIndex = 1;
						cmbCrossCheck.Enabled = true;
					}
					else if (vbPorterVar == "W")
					{
						chkCrossCheck.CheckState = Wisej.Web.CheckState.Checked;
						cmbCrossCheck.SelectedIndex = 2;
						cmbCrossCheck.Enabled = true;
					}
					else if (vbPorterVar == "M")
					{
						chkCrossCheck.CheckState = Wisej.Web.CheckState.Checked;
						cmbCrossCheck.SelectedIndex = 3;
						cmbCrossCheck.Enabled = true;
					}
					else
					{
						chkCrossCheck.CheckState = Wisej.Web.CheckState.Unchecked;
						cmbCrossCheck.SelectedIndex = 0;
						cmbCrossCheck.Enabled = false;
					}
					// Clear Paid By
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("ClearPaidBy")))
					{
						chkClearPaidBy.CheckState = Wisej.Web.CheckState.Checked;
						// Tracker Reference: 17448
						lblClearPaidBy.Enabled = true;
						cmbClearPaidBy.Enabled = true;
						if (FCConvert.ToString(rsDef.Get_Fields_String("ClearPaidByOption")) == "M")
						{
							cmbClearPaidBy.SelectedIndex = 1;
						}
						else
						{
							cmbClearPaidBy.SelectedIndex = 0;
						}
					}
					else
					{
						chkClearPaidBy.CheckState = Wisej.Web.CheckState.Unchecked;
						lblClearPaidBy.Enabled = false;
						cmbClearPaidBy.Enabled = false;
						cmbClearPaidBy.SelectedIndex = 0;
					}
					// Show Control Fields
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("ShowControlFields")))
					{
						chkShowControlFields.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkShowControlFields.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// Sort Receipt Type Alpha
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("SortReceiptTypeAlpha")))
					{
						chkSortTypeAlpha.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSortTypeAlpha.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// kgk 11-10-2011 trocr-284  Add option to display routing numbers in bank list
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("ShowRoutingNumbers")))
					{
						chkRoutNumsInBnkLst.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkRoutNumsInBnkLst.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// MAL@20081223: Elec Payment Options
					// Tracker Reference: 16641
					if (StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType!= ElectronicPaymentPortalOption.None)
					{
						if (!cmbOptions.Items.Contains("Electronic Payment Processing"))
						{
							this.cmbOptions.AddItem("Electronic Payment Processing");
						}
					}
					else
					{
						if (cmbOptions.Items.Contains("Electronic Payment Processing"))
						{
							this.cmbOptions.Items.RemoveAt(cmbOptions.Items.IndexOf("Electronic Payment Processing"));
						}
					}
					string vbPorterVar1 = rsDef.Get_Fields_String("EPaymentPortal");
					if (vbPorterVar1 == "N")
					{
						txtPymtPortal.Text = "None";
                        chkEPymtTestMode.Enabled = false;
                    }
                    else if (vbPorterVar1 == "I")
					{
						// kgk 09-29-2011  Add Invoice Cloud Gateway
						txtPymtPortal.Text = "Invoice Cloud";
                        chkEPymtTestMode.Enabled = true;
                        txtBillerGUID_ICL.Text = FCConvert.ToString(rsDef.Get_Fields_String("EPaymentClientID"));

						if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("UseSeperateCreditCardGLAccount")))
						{
							chkSeperateCreditCardCashAccount.CheckState = Wisej.Web.CheckState.Checked;
							txtSeperateCreditCardCashAcct.Text = FCConvert.ToString(rsDef.Get_Fields_String("SeperateCreditCardGLAccount"));
						}
						else
						{
							chkSeperateCreditCardCashAccount.CheckState = Wisej.Web.CheckState.Unchecked;
						}
                    }
					else if (vbPorterVar1 == "E")
					{
						// kgk 01-26-2012  Add InforME Gateway
						txtPymtPortal.Text = "InforME";
                        chkEPymtTestMode.Enabled = true;
						// kgk 03122012 trocr-325  Change to allow multiple service codes
                        var defaultServiceCode = FCConvert.ToString(rsDef.Get_Fields_String("EPaymentClientID"));
						LoadServiceCodesGrid(defaultServiceCode);
                        if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("UseSeperateCreditCardGLAccount")))
						{
							chkSeperateCreditCardCashAccount.CheckState = Wisej.Web.CheckState.Checked;
							txtSeperateCreditCardCashAcct.Text = FCConvert.ToString(rsDef.Get_Fields_String("SeperateCreditCardGLAccount"));
						}
						else
						{
							chkSeperateCreditCardCashAccount.CheckState = Wisej.Web.CheckState.Unchecked;
						}

					}
					else
					{
						txtPymtPortal.Text = "None";
                        chkEPymtTestMode.Enabled = false;
                    }
					ShowEPymtFrame(txtPymtPortal.Text);

					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("EPaymentTestMode")))
					{
						chkEPymtTestMode.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkEPymtTestMode.CheckState = Wisej.Web.CheckState.Unchecked;
					}


					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("PrintCreditCardReport")))
					{
						chkPrintCCReport.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPrintCCReport.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					switch (FCConvert.ToInt32(rsDef.Get_Fields_Int16("CreditCardReportOrder")))
					{
						case 0:
							{
								cmbCCReportSort.SelectedIndex = 0;
								break;
							}
						case 1:
							{
								cmbCCReportSort.SelectedIndex = 1;
								break;
							}
						default:
							{
								cmbCCReportSort.SelectedIndex = 0;
								break;
							}
					}
					//end switch
					if (chkPrintCCReport.CheckState == Wisej.Web.CheckState.Checked)
					{
						lblCCReportSort.Enabled = true;
						cmbCCReportSort.Enabled = true;
					}
					else
					{
						lblCCReportSort.Enabled = false;
						cmbCCReportSort.Enabled = false;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("ReceiptPrinterPrompt")))
					{
						chkReceiptPrinterPrompt.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkReceiptPrinterPrompt.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// kk trocr-348  111212  Add Close Out By Dept option
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Int32("CloseOutByDept")))
					{
						chkDeptCloseOut.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDeptCloseOut.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					intErr = 45;
                    intErr = 50;
					// show the next receipt number
					strSQL = "SELECT * FROM RNumLock";
					intErr = 61;
					rsDef.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
					if (rsDef.EndOfFile() != true && rsDef.BeginningOfFile() != true)
					{
						lngReceiptNumber = FCConvert.ToInt32(rsDef.Get_Fields_Int32("NextReceiptNumber"));
						txtResetReceipt.Text = FCConvert.ToString(lngReceiptNumber);
					}
				}
				else
				{
					intErr = 100;
					rsDef.AddNew();
					rsDef.Update(false);
					SetDefaults();
				}

                loading = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Defaults - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDirty)
			{
				if (MessageBox.Show("Would you like to save the changes to your settings before exiting?", "Save Settings", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (chkSeperateCreditCardCashAccount.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (txtSeperateCreditCardCashAcct.Text != "")
						{
							if (txtSeperateCreditCardCashAcct.Text.Length < txtSeperateCreditCardCashAcct.MaxLength)
							{
								MessageBox.Show("Invalid account number for seperate credit card cash account.", "Invalid Seperate Credit Card Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
						}
						else if (modGlobalConstants.Statics.gboolBD)
						{
							MessageBox.Show("You must enter a seperate credit card cash account before you may save the type.", "Invalid Seperate Credit Card Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtSeperateCreditCardCashAcct.Focus();
							return;
						}
					}
					SaveSettings();
				}
			}
			boolLoaded = false;
        }

		private void frmCustomize_Resize(object sender, System.EventArgs e)
		{
			AdjustAllFrames();
			ResizeServiceCodesGrid();
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessExit_Click()
		{
			mnuProcessExit_Click(mnuProcessExit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (cmbOptions.SelectedIndex == 8)
			{
				cmbOptions.Focus();
			}
			if (chkSeperateCreditCardCashAccount.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (txtSeperateCreditCardCashAcct.Text != "")
				{
					if (txtSeperateCreditCardCashAcct.Text.Length < txtSeperateCreditCardCashAcct.MaxLength)
					{
						MessageBox.Show("Invalid account number for seperate credit card cash account.", "Invalid Seperate Credit Card Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				else if (modGlobalConstants.Statics.gboolBD)
				{
					MessageBox.Show("You must enter a seperate credit card cash account before you may save.", "Invalid Seperate Credit Card Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtSeperateCreditCardCashAcct.Focus();
					return;
				}
			}
			SaveSettings();
		}

		private void mnuProcessSaveExit_Click(object sender, System.EventArgs e)
		{
			if (cmbOptions.SelectedIndex == 8)
			{
				cmbOptions.Focus();
			}
			if (chkSeperateCreditCardCashAccount.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (txtSeperateCreditCardCashAcct.Text != "")
				{
					if (txtSeperateCreditCardCashAcct.Text.Length < txtSeperateCreditCardCashAcct.MaxLength)
					{
						MessageBox.Show("Invalid account number for seperate credit card cash account.", "Invalid Seperate Credit Card Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				else if (modGlobalConstants.Statics.gboolBD)
				{
					MessageBox.Show("You must enter a seperate credit card cash account before you may save.", "Invalid Seperate Credit Card Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtSeperateCreditCardCashAcct.Focus();
					return;
				}
			}
			SaveSettings();
			//FC:FINAL:DDU:#2909 - don't close form
			//Close();
		}

		private void optCrossCheckDate_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
				case 1:
					{
						txtCrossCheckDate.Enabled = false;
						txtCrossCheckDate.Text = "";
						break;
					}
				case 2:
					{
						txtCrossCheckDate.Enabled = true;
						if (txtCrossCheckDate.Visible && txtCrossCheckDate.Enabled)
						{
							txtCrossCheckDate.Focus();
						}
						break;
					}
			}
			//end switch
		}

		private void optCrossCheckDate_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbCrossCheckDate.SelectedIndex;
			optCrossCheckDate_CheckedChanged(index, sender, e);
		}

		private void optOptions_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			ShowFrame(Index);
		}

		private void optOptions_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbOptions.SelectedIndex;
			optOptions_CheckedChanged(index, sender, e);
		}

		private void optReceiptWidth_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
			ReceiptWidthLock(ref Index);
		}

		private void optReceiptWidth_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReceiptWidth.SelectedIndex;
			optReceiptWidth_CheckedChanged(index, sender, e);
		}

		private void ReceiptWidthLock_2(int intIndex)
		{
			ReceiptWidthLock(ref intIndex);
		}

		private void ReceiptWidthLock(ref int intIndex)
		{
			// this will lock or unlock the correct combo boxes and labels
			if (intIndex == 0)
			{
				// narrow
				cmbWideLines.Enabled = false;
				lblWideLines.Enabled = false;
				cmbFontSize.Enabled = false;
				lblFontSize.Enabled = false;
				cmbExtraLines.Enabled = true;
				lblExtraLines.Enabled = true;
				chkSP200.Enabled = true;
				lblReceiptOffset.Enabled = true;
				cmbReceiptOffset.Enabled = true;
				chkNoMargin.Enabled = false;
				chkCompactReceipt.Enabled = false;
				chkThinReceipt.Enabled = false;
			}
			else
			{
				// wide
				cmbWideLines.Enabled = true;
				lblWideLines.Enabled = true;
				cmbFontSize.Enabled = true;
				lblFontSize.Enabled = true;
				cmbExtraLines.Enabled = false;
				lblExtraLines.Enabled = false;
				chkSP200.Enabled = false;
				lblReceiptOffset.Enabled = false;
				cmbReceiptOffset.Enabled = false;
				chkNoMargin.Enabled = true;
				chkCompactReceipt.Enabled = true;
				chkThinReceipt.Enabled = true;
			}
		}

		private void SaveSettings()
		{
			int intErr = 0;
			try
			{
                string defaultServiceCode = "";
				// On Error GoTo ERROR_HANDLER
				// this routine will save all of the default settings for this town into the CR database
				if (StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType != ElectronicPaymentPortalOption.None)
                {
                    if (!ValidateServiceCodes())
                    {
                        return;
                    }
                    foreach (var removedId in RemovedServiceCodes)
                    {
                        StaticSettings.GlobalCommandDispatcher.Send(new DeleteServiceCode(removedId));
                    }

                    var serviceCodes = getServiceCodesFromGrid();
                    if (serviceCodes.Any())
                    {
                        
                        foreach (var serviceCodeItem in serviceCodes)
                        {
                            if (serviceCodeItem.IsDefault)
                            {
                                defaultServiceCode = serviceCodeItem.Code;
                            }
                            StaticSettings.GlobalCommandDispatcher.Send(new SaveServiceCode(serviceCodeItem.ToServiceCode()));
                        }
						LoadServiceCodesGrid(defaultServiceCode);
                    }
                }
                RemovedServiceCodes.Clear();
                StaticSettings.gGlobalCashReceiptSettings.EPaymentClientID = defaultServiceCode;
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsBank = new clsDRWrapper();
				int lngCT;
				
                
                bool boolUpdateRecTypes = false;

                

				clsDRWrapper rsType = new clsDRWrapper();
				rsSave.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
				intErr = 1;
                if (chkShowServiceCode.Checked)
                {
                    settingCont.SaveSetting("true", "ShowServiceCodeOnPaymentScreen", "CashReceipts", "", "", "");
                }
                else
                {
                    settingCont.SaveSetting("false", "ShowServiceCodeOnPaymentScreen", "CashReceipts", "", "", "");
                }

                StaticSettings.gGlobalCashReceiptSettings.ShowPaymentPortalCodeOnPaymentScreen =
                    chkShowServiceCode.Checked;
               
              
				if (rsSave.EndOfFile() != true)
				{
					rsSave.Edit();

					intErr = 2;
					rsSave.Set_Fields("EPaymentClientID",defaultServiceCode);
                    rsSave.Set_Fields("DailyAuditByAlpha", FCConvert.CBool(chkDailyAuditByAlpha.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gboolDailyAuditByAlpha = FCConvert.CBool(rsSave.Get_Fields_Boolean("DailyAuditByAlpha"));
					intErr = 3;
					rsSave.Set_Fields("AllowPartialAudit", FCConvert.CBool(chkPartialAudit.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gboolAllowPartialAudit = FCConvert.CBool(rsSave.Get_Fields_Boolean("AllowPartialAudit"));
					intErr = 4;
					// .Fields("NarrowReceipt") = CBool(optReceiptWidth(0).Value = True)
					if (FCConvert.CBool(cmbReceiptWidth.SelectedIndex == 0))
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRNarrowReceipt", "TRUE");
						// if this is a narrow receipt, then save the wide options as false
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideNoMargin", "FALSE");
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideCompact", "FALSE");
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideThin", "FALSE");
						modGlobal.Statics.gboolWideReceiptNoMargin = false;
						modGlobal.Statics.gboolRecieptCompacted = false;
						modGlobal.Statics.gboolNarrowReceipt = true;
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRNarrowReceipt", "FALSE");
						if (chkNoMargin.CheckState == Wisej.Web.CheckState.Checked)
						{
							// save the default receipt length
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideNoMargin", "TRUE");
							modGlobal.Statics.gboolWideReceiptNoMargin = true;
						}
						else
						{
							// save the default receipt length
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideNoMargin", "FALSE");
							modGlobal.Statics.gboolWideReceiptNoMargin = false;
						}
						if (chkCompactReceipt.CheckState == Wisej.Web.CheckState.Checked)
						{
							// save the default receipt length
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideCompact", "TRUE");
							modGlobal.Statics.gboolRecieptCompacted = true;
						}
						else
						{
							// save the default receipt length
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideCompact", "FALSE");
							modGlobal.Statics.gboolRecieptCompacted = false;
						}
						if (chkThinReceipt.CheckState == Wisej.Web.CheckState.Checked)
						{
							// save the default receipt length
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideThin", "TRUE");
							modGlobal.Statics.gboolRecieptThin = true;
						}
						else
						{
							// save the default receipt length
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideThin", "FALSE");
							modGlobal.Statics.gboolRecieptThin = false;
						}
						modGlobal.Statics.gboolNarrowReceipt = false;
					}

					StaticSettings.gGlobalCashReceiptSettings.NarrowReceipt = modGlobal.Statics.gboolNarrowReceipt;
					StaticSettings.gGlobalCashReceiptSettings.WideReceiptNoMargins =
						modGlobal.Statics.gboolWideReceiptNoMargin;
					StaticSettings.gGlobalCashReceiptSettings.CompactedReceipt =
						modGlobal.Statics.gboolRecieptCompacted;
					StaticSettings.gGlobalCashReceiptSettings.ThinReceipt = modGlobal.Statics.gboolRecieptThin;
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "MVDelay", FCConvert.ToString(Conversion.Val(Strings.Trim(txtMVDelay.Text))));
					intErr = 5;
					// .Fields("ReceiptLength") = Val(cmbWideLines.List(cmbWideLines.ListIndex))
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptLength", FCConvert.ToString(Conversion.Val(cmbWideLines.Items[cmbWideLines.SelectedIndex].ToString())));
					modGlobal.Statics.gintReceiptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbWideLines.Items[cmbWideLines.SelectedIndex].ToString())));
					StaticSettings.gGlobalCashReceiptSettings.ReceiptLength = modGlobal.Statics.gintReceiptLength;
					intErr = 6;
					// .Fields("ExtraLines") = Val(cmbExtraLines.List(cmbExtraLines.ListIndex))
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRExtraLength", Conversion.Str(Conversion.Val(cmbExtraLines.Items[cmbExtraLines.SelectedIndex].ToString())));
					modGlobal.Statics.gintExtraLines = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbExtraLines.Items[cmbExtraLines.SelectedIndex].ToString())));
					StaticSettings.gGlobalCashReceiptSettings.ReceiptExtraLines = modGlobal.Statics.gintExtraLines;
					intErr = 7;
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRWideFontSize", Conversion.Str(Conversion.Val(cmbFontSize.Items[cmbFontSize.SelectedIndex].ToString())));
					rsSave.Set_Fields("TopComment", txtTopComment.Text);
					modGlobal.Statics.gstrTopComments = FCConvert.ToString(rsSave.Get_Fields_String("TopComment"));
					StaticSettings.gGlobalCashReceiptSettings.TopComment = modGlobal.Statics.gstrTopComments;
					rsSave.Set_Fields("BottomComment", txtBottomComment.Text);
					modGlobal.Statics.gstrBottomComments = FCConvert.ToString(rsSave.Get_Fields_String("BottomComment"));
					StaticSettings.gGlobalCashReceiptSettings.BottomComment = modGlobal.Statics.gstrBottomComments;
					if (cmbSignatureNo.SelectedIndex == 2)
					{
						rsSave.Set_Fields("PrintSigLine", "A");
					}
					else if (cmbSignatureNo.SelectedIndex == 1)
					{
						rsSave.Set_Fields("PrintSigLine", "P");
					}
					else
					{
						rsSave.Set_Fields("PrintSigLine", "N");
					}
					modGlobal.Statics.gstrPrintSigLine = FCConvert.ToString(rsSave.Get_Fields_String("PrintSigLine"));

					StaticSettings.gGlobalCashReceiptSettings.PrintSignature = 
						modGlobal.Statics.gstrPrintSigLine == "A" 
							? PrintSignatureOption.Yes
							: modGlobal.Statics.gstrPrintSigLine == "P"
								? PrintSignatureOption.OnlyOnCreditCardPayments
								: PrintSignatureOption.No;
					// .Fields("CDConnected") = CBool(chkCDConnected.Value = 1)
					intErr = 8;
					if (FCConvert.CBool(chkCDConnected.CheckState == Wisej.Web.CheckState.Checked))
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDConnected", "TRUE");
						modGlobal.Statics.gboolCDConnected = true;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDCode", FCConvert.ToString(Conversion.Val(txtCDCode.Text)));
						modGlobal.Statics.gstrCDCode = FCConvert.ToString(Conversion.Val(txtCDCode.Text));
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRRepeatCount", FCConvert.ToString(Conversion.Val(cmbRepeat.Items[cmbRepeat.SelectedIndex].ToString())));
						modGlobal.Statics.gintRepeatCount = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbRepeat.Items[cmbRepeat.SelectedIndex].ToString())));
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDPortID", cmbPrinterPort.Text);
						modGlobal.Statics.gstrCDPortID = cmbPrinterPort.Text;
						StaticSettings.gGlobalCashReceiptSettings.CashDrawerConnected =
							modGlobal.Statics.gboolCDConnected;
						StaticSettings.gGlobalCashReceiptSettings.CashDrawerCode =
							modGlobal.Statics.gstrCDCode;
						StaticSettings.gGlobalCashReceiptSettings.CashDrawerRepeatCount =
							modGlobal.Statics.gintRepeatCount;
						StaticSettings.gGlobalCashReceiptSettings.CashDrawerPortId =
							modGlobal.Statics.gstrCDPortID;

					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRCDConnected", "FALSE");
						modGlobal.Statics.gboolCDConnected = false;
						StaticSettings.gGlobalCashReceiptSettings.CashDrawerConnected =
							modGlobal.Statics.gboolCDConnected;
					}
					intErr = 9;
					rsSave.Set_Fields("AutoCheckRecEntry", FCConvert.CBool(chkDepositEntry.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gboolAutoCheckRecEntry = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("AutoCheckRecEntry"));
					rsSave.Set_Fields("TellerIDUsed", Strings.Left(cmbTellerUsed.Items[cmbTellerUsed.SelectedIndex].ToString(), 1));
                    switch (cmbTellerUsed.Items[cmbTellerUsed.SelectedIndex].ToString().Left(1))
                    {
                        case "M":
                            StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage =
                                TellerIDUsage.RequireEachTime;
                            break;
                        case "Y":
                            StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage =
                                TellerIDUsage.RequireButDefault;
                            break;
                        case "P":
                            StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage =
                                TellerIDUsage.RequirePassword;
                            break;
                        case "N":
                            StaticSettings.gGlobalCashReceiptSettings.TellerIdUsage = TellerIDUsage.NotRequired;
                            break;
                    }
                    modGlobal.Statics.gstrTeller = FCConvert.ToString(rsSave.Get_Fields_String("TellerIDUsed"));
					intErr = 10;
					if (FCConvert.ToString(rsSave.Get_Fields_String("TellerIDUsed")) == "N")
					{
						rsSave.Set_Fields("TellerReportWithAudit", false);
						rsSave.Set_Fields("SkipPageForTeller", false);
						rsSave.Set_Fields("CashTellersOutSeperately", false);
						rsSave.Set_Fields("AuditSeqbyRecNumber", false);
						rsSave.Set_Fields("gboolTellerCheckReportWithAudit", false);
						modGlobal.Statics.gboolTellerReportWithAudit = false;
						modGlobal.Statics.gboolSkipPageForTeller = false;
						modGlobal.Statics.gboolCashTellersOutSeperately = false;
						modGlobal.Statics.gboolAuditSeqByType = false;
						modGlobal.Statics.gboolTellerCheckReportWithAudit = false;
					}
					else
					{
						rsSave.Set_Fields("TellerReportWithAudit", FCConvert.CBool(chkTellerWithAudit.CheckState == Wisej.Web.CheckState.Checked));
						rsSave.Set_Fields("SkipPageForTeller", FCConvert.CBool(chkTellerNewPage.CheckState == Wisej.Web.CheckState.Checked));
						rsSave.Set_Fields("CashTellersOutSeperately", FCConvert.CBool(chkTellerSeparate.CheckState == Wisej.Web.CheckState.Checked));
						rsSave.Set_Fields("AuditSeqbyRecNumber", FCConvert.CBool(chkTellerReportSeq.CheckState == Wisej.Web.CheckState.Checked));
						rsSave.Set_Fields("gboolTellerCheckReportWithAudit", FCConvert.CBool(chkTellerCheckReport.CheckState == Wisej.Web.CheckState.Checked));
						modGlobal.Statics.gboolTellerReportWithAudit = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("TellerReportWithAudit"));
						modGlobal.Statics.gboolSkipPageForTeller = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("SkipPageForTeller"));
						modGlobal.Statics.gboolCashTellersOutSeperately = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("CashTellersOutSeperately"));
						modGlobal.Statics.gboolAuditSeqByType = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("AuditSeqByRecNumber"));
						modGlobal.Statics.gboolTellerCheckReportWithAudit = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("gboolTellerCheckReportWithAudit"));
					}
					intErr = 11;
					rsSave.Set_Fields("PasswordProtectAudit", FCConvert.CBool(chkAuditPassword.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gboolPasswordProtectAudit = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("PasswordProtectAudit"));
					rsSave.Set_Fields("AuditPassword", modEncrypt.ToggleEncryptCode(txtAuditPassword.Text));
					modGlobal.Statics.gstrAuditPassword = modEncrypt.ToggleEncryptCode(rsSave.Get_Fields_String("AuditPassword"));
					intErr = 12;
					modGlobal.Statics.gboolBDExclusive = FCConvert.CBool(chkExclusive.CheckState == Wisej.Web.CheckState.Checked);
					rsSave.Set_Fields("gboolBDExclusive", modGlobal.Statics.gboolBDExclusive);
					intErr = 13;
					// .Fields("SP200Printer") = CBool(chkSP200.Value = vbChecked)
					if (FCConvert.CBool(chkSP200.CheckState == Wisej.Web.CheckState.Checked))
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRSP200Printer", "TRUE");
						modGlobal.Statics.gboolSP200Printer = true;
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRSP200Printer", "FALSE");
						modGlobal.Statics.gboolSP200Printer = false;
					}

					StaticSettings.gGlobalCashReceiptSettings.SP200Printer = modGlobal.Statics.gboolSP200Printer;
					intErr = 14;
					// .Fields("ReceiptPrinterOffset") = Val(cmbReceiptOffset.List(cmbReceiptOffset.ListIndex))
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "CRReceiptOffset", FCConvert.ToString(Conversion.Val(cmbReceiptOffset.Items[cmbReceiptOffset.SelectedIndex].ToString())));
					modGlobal.Statics.gintReceiptOffSet = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbReceiptOffset.Items[cmbReceiptOffset.SelectedIndex].ToString())));
					StaticSettings.gGlobalCashReceiptSettings.ReceiptOffset = modGlobal.Statics.gintReceiptOffSet;
					intErr = 15;
					rsSave.Set_Fields("CloseMVPeriod", FCConvert.CBool(chkCloseMV.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gboolCloseMVPeriod = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("CloseMVPeriod"));
					intErr = 16;
					if (cmbCheckReportOrder.SelectedIndex == 0)
					{
						rsSave.Set_Fields("CheckReportOrder", 0);
					}
					else if (cmbCheckReportOrder.SelectedIndex == 1)
					{
						rsSave.Set_Fields("CheckReportOrder", 1);
					}
					else
					{
						rsSave.Set_Fields("CheckReportOrder", 2);
					}
					// MAL@20071023: Update global variable
					if (FCConvert.ToInt32(rsSave.Get_Fields_Int16("CheckReportOrder")) != modGlobal.Statics.gintCheckReportOrder)
					{
						modGlobal.Statics.gintCheckReportOrder = FCConvert.ToInt32(rsSave.Get_Fields_Int16("CheckReportOrder"));
					}
					modGlobal.Statics.gboolCloseMVPeriod = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("CloseMVPeriod"));
					intErr = 17;
					rsSave.Set_Fields("USLExport", FCConvert.CBool(chkUSLExport.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gboolUseUSLExport = FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("USLExport"));
					rsSave.Set_Fields("PrintCLAudit", FCConvert.CBool(chkPrintAuditRE.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("PrintUTAudit", FCConvert.CBool(chkPrintAuditUT.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("PrintARAudit", FCConvert.CBool(chkPrintAuditAR.CheckState == Wisej.Web.CheckState.Checked));
					if ((!FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("UppercaseTypes")) && FCConvert.CBool(chkUppercaseTypes.CheckState == Wisej.Web.CheckState.Checked)) || (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("UppercaseTypes")) && FCConvert.CBool(chkUppercaseTypes.CheckState == Wisej.Web.CheckState.Unchecked)))
					{
						boolUpdateRecTypes = true;
					}
					rsSave.Set_Fields("UppercaseTypes", FCConvert.CBool(chkUppercaseTypes.CheckState == Wisej.Web.CheckState.Checked));
					if (modGlobalConstants.Statics.gboolCK)
					{
						rsSave.Set_Fields("BreakdownCKTypes", FCConvert.CBool(chkBreakdownCK.CheckState == Wisej.Web.CheckState.Checked));
					}
					if (cmbCrossCheckDate.SelectedIndex == 0)
					{
						rsSave.Set_Fields("CrossCheckDefault", 0);
						rsSave.Set_Fields("CrossCheckDefaultDate", 0);
					}
					else if (cmbCrossCheckDate.SelectedIndex == 1)
					{
						rsSave.Set_Fields("CrossCheckDefault", 1);
						rsSave.Set_Fields("CrossCheckDefaultDate", 0);
					}
					else
					{
						if (Information.IsDate(txtCrossCheckDate.Text))
						{
							rsSave.Set_Fields("CrossCheckDefault", 2);
							rsSave.Set_Fields("CrossCheckDefaultDate", DateAndTime.DateValue(txtCrossCheckDate.Text));
						}
						else
						{
							MessageBox.Show("Cross check date is not valid.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					if (modGlobalConstants.Statics.gboolMV)
					{
						if (chkMVCheck.CheckState == Wisej.Web.CheckState.Checked)
						{
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "MVCheck", "TRUE");
						}
						else
						{
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "MVCheck", "FALSE");
						}
					}
					// Auto Cross Check Report
					rsSave.Set_Fields("AutoRunCrossCheck", Strings.Left(cmbCrossCheck.Items[cmbCrossCheck.SelectedIndex].ToString(), 1));
					if (rsSave.IsFieldNull("LastCrossCheck"))
					{
						rsSave.Set_Fields("LastCrossCheck", DateTime.Today);
					}
					intErr = 18;
					// Clear Paid By Field
					rsSave.Set_Fields("ClearPaidBy", FCConvert.CBool(chkClearPaidBy.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gboolClearPaidBy = FCConvert.CBool(chkClearPaidBy.CheckState == Wisej.Web.CheckState.Checked);
					// Tracker Reference: 17448
					if (modGlobal.Statics.gboolClearPaidBy)
					{
						if (cmbClearPaidBy.SelectedIndex == 0)
						{
							rsSave.Set_Fields("ClearPaidByOption", "S");
							modGlobal.Statics.gstrClearPaidByOption = "S";
						}
						else
						{
							rsSave.Set_Fields("ClearPaidByOption", "M");
							modGlobal.Statics.gstrClearPaidByOption = "M";
						}
					}
					else
					{
						rsSave.Set_Fields("ClearPaidByOption", "S");
						modGlobal.Statics.gstrClearPaidByOption = "S";
					}
					intErr = 19;
                    rsSave.Set_Fields("ShowControlFields", FCConvert.CBool(chkShowControlFields.CheckState == Wisej.Web.CheckState.Checked));
					modGlobalConstants.Statics.gblnShowControlFields = FCConvert.CBool(chkShowControlFields.CheckState == Wisej.Web.CheckState.Checked);
					StaticSettings.gGlobalCashReceiptSettings.ShowControlFields =
						modGlobalConstants.Statics.gblnShowControlFields;
                    rsSave.Set_Fields("SortReceiptTypeAlpha", FCConvert.CBool(chkSortTypeAlpha.CheckState == Wisej.Web.CheckState.Checked));
					modGlobalConstants.Statics.gblnSortTypeAlpha = FCConvert.CBool(chkSortTypeAlpha.CheckState == Wisej.Web.CheckState.Checked);
                    rsSave.Set_Fields("ShowRoutingNumbers", FCConvert.CBool(chkRoutNumsInBnkLst.CheckState == Wisej.Web.CheckState.Checked));
                    rsSave.Set_Fields("EPaymentTestMode", FCConvert.CBool(chkEPymtTestMode.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gblnEPymtTestMode = FCConvert.CBool(chkEPymtTestMode.CheckState == Wisej.Web.CheckState.Checked);
                    StaticSettings.gGlobalCashReceiptSettings.PrintExtraReceipt = modGlobal.Statics.gblnPrintExtraReceipt;

					//if (chkConvenienceFee.CheckState == Wisej.Web.CheckState.Checked)
					//{
					//	if (cboConvenienceFeeMethod.SelectedIndex == 0)
					//	{
					//		rsSave.Set_Fields("ConvenienceFeeMethod", "F");
     //                       StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeMethod = "F";
     //                       StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeGLAccount =
     //                           txtConvenienceFeeAccount.TextMatrix(0, 0);
     //                       rsSave.Set_Fields("ConvenienceFlatAmount", FCConvert.ToDecimal(txtFlatAmount.Text));
     //                       StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeFlatAmount =
     //                           txtFlatAmount.Text.ToDecimalValue();

     //                       rsSave.Set_Fields("ConvenienceFeeGLAccount", txtConvenienceFeeAccount.TextMatrix(0, 0));
					//		rsType.Execute("UPDATE Type SET ConvenienceFeeOverride = 0, ConvenienceFeeMethod = 'N', ConvenienceFlatAmount = 0, ConveniencePercentage = 0, ConvenienceFeeGLAccount = ''", modExtraModules.strCRDatabase);
					//	}
					//	else
     //                   {
     //                       StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeMethod = "P";
     //                       StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeGLAccount =
     //                           txtConvenienceFeeAccount.TextMatrix(0, 0);
     //                       StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeePercentage =
     //                           txtPerDiem.Text.ToDecimalValue() / 100;
     //                       rsSave.Set_Fields("ConvenienceFeeMethod", "P");
					//		rsSave.Set_Fields("ConveniencePercentage", FCConvert.ToDouble(txtPerDiem.Text) / 100);
					//		rsSave.Set_Fields("ConvenienceFeeGLAccount", txtConvenienceFeeAccount.TextMatrix(0, 0));
					//	}
					//}
					//else
     //               {
                        StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeMethod = "N";
                        StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeGLAccount = "";
                        StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeePercentage = 0;
                        rsSave.Set_Fields("ConvenienceFeeMethod", "N");
						rsSave.Set_Fields("ConvenienceFlatAmount", 0);
                        StaticSettings.gGlobalCashReceiptSettings.ConvenienceFeeFlatAmount = 0;

                        rsSave.Set_Fields("ConveniencePercentage", 0);
						rsSave.Set_Fields("ConvenienceFeeGLAccount", "");
					//}
					if (chkSeperateCreditCardCashAccount.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsSave.Set_Fields("UseSeperateCreditCardGLAccount", true);
						rsSave.Set_Fields("SeperateCreditCardGLAccount", txtSeperateCreditCardCashAcct.Text);
                        StaticSettings.gGlobalCashReceiptSettings.SeparateCreditCardGLAccount =
                            txtSeperateCreditCardCashAcct.Text;
                        StaticSettings.gGlobalCashReceiptSettings.UseSeparateCreditCardGLAccount = true;

                    }
					else
					{
						rsSave.Set_Fields("UseSeperateCreditCardGLAccount", false);
						rsSave.Set_Fields("SeperateCreditCardGLAccount", "");
                        StaticSettings.gGlobalCashReceiptSettings.SeparateCreditCardGLAccount = "";
                        StaticSettings.gGlobalCashReceiptSettings.UseSeparateCreditCardGLAccount = false;
                    }


					rsSave.Set_Fields("PrintCreditCardReport", FCConvert.CBool(chkPrintCCReport.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gblnPrintCCReport = FCConvert.CBool(chkPrintCCReport.CheckState == Wisej.Web.CheckState.Checked);
					if (cmbCCReportSort.SelectedIndex == 0)
					{
						rsSave.Set_Fields("CreditCardReportOrder", 0);
					}
					else if (cmbCCReportSort.SelectedIndex == 1)
					{
						rsSave.Set_Fields("CreditCardReportOrder", 1);
					}
					else
					{
						rsSave.Set_Fields("CreditCardReportOrder", 0);
					}
					modGlobal.Statics.gintCCReportOrder = FCConvert.ToInt32(rsSave.Get_Fields_Int16("CreditCardReportOrder"));
					// MAL@20090306: Receipt Printer Prompt
					// Tracker Reference: 17676
					rsSave.Set_Fields("ReceiptPrinterPrompt", FCConvert.CBool(chkReceiptPrinterPrompt.CheckState == Wisej.Web.CheckState.Checked));
					modGlobal.Statics.gblnReceiptPrinterPrompt = FCConvert.CBool(chkReceiptPrinterPrompt.CheckState == Wisej.Web.CheckState.Checked);
                    // kk 111212 trocr-348  Add Close Out By Department
                    //FC:FINAL:AM:#4586 - convert to int
                    //rsSave.Set_Fields("CloseOutByDept", FCConvert.CBool(chkDeptCloseOut.CheckState));
                    rsSave.Set_Fields("CloseOutByDept", FCConvert.ToInt32(chkDeptCloseOut.CheckState));

                    StaticSettings.gGlobalCashReceiptSettings.ElectronicPaymentPortalType = modGlobal.Statics.gstrEPaymentPortal == "E" ? ElectronicPaymentPortalOption.PayPort : ElectronicPaymentPortalOption.None;
                    StaticSettings.gGlobalCashReceiptSettings.EPaymentInTestMode = modGlobal.Statics.gblnEPymtTestMode;
                    intErr = 20;
					if (rsSave.Update())
					{
						boolDirty = false;
					}
					intErr = 21;
					if (Conversion.Val(txtAuditMarginTop.Text) > 0)
					{
						if (Conversion.Val(txtAuditMarginTop.Text) <= 5)
						{
							// Audit Top Margin Adjustment
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "AuditMarginAdjustmentTop", txtAuditMarginTop.Text);
							modGlobal.Statics.gdblAuditMarginAdjustmentTop = FCConvert.ToDouble(txtAuditMarginTop.Text);
						}
						else
						{
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "AuditMarginAdjustmentTop", "5");
							modGlobal.Statics.gdblAuditMarginAdjustmentTop = 5;
						}
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "AuditMarginAdjustmentTop", "0");
						modGlobal.Statics.gdblAuditMarginAdjustmentTop = 0;
					}
					intErr = 25;
					if (lngReceiptNumber != Conversion.Val(txtResetReceipt.Text))
					{
						if (Conversion.Val(txtResetReceipt.Text) > 0)
						{
							lngReceiptNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtResetReceipt.Text)));
							if (MessageBox.Show("Please make sure that there is no other user creating receipts." + "\r\n" + "Click OK when there is no one else creating a receipt.", "Updating Receipt Number", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
							{
								modGlobalFunctions.AddCYAEntry_8("CR", "Reset Receipt Number - Old:" + FCConvert.ToString(lngReceiptNumber) + " New:" + txtResetReceipt.Text);
								rsSave.OpenRecordset("SELECT * FROM RNumLock");
								if (rsSave.EndOfFile() != true && rsSave.BeginningOfFile() != true)
								{
									rsSave.Edit();
								}
								else
								{
									rsSave.AddNew();
								}
								rsSave.Set_Fields("NextReceiptNumber", FCConvert.ToString(Conversion.Val(txtResetReceipt.Text)));
								rsSave.Update();
							}
							else
							{
								MessageBox.Show("The next receipt number has not updated.", "Cancel Receipt Number Update", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						else
						{
							MessageBox.Show("Please enter a receipt number greater than zero with no charaters.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				else
				{
					MessageBox.Show("Error while saving the default settings for Cash Receipting.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				if (boolUpdateRecTypes)
				{
					modGlobal.SetupRestrictedReceiptTypes();
					// this will force the Capitals in the titles
				}
				boolDirty = false;
				MessageBox.Show("Save Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                // MsgBox "You must exit and restart Cash Receipting for these changes to take affect.", vbInformation, "New Settings"
                // MsgBox "For these changes to take affect, each user will have to exit and restart the TRIO Cash Receipting module.", vbInformation, "New Settings"
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Save Error - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillPrinterPortCombo()
		{
			// this will load all of the basic lpt/com ports
			cmbPrinterPort.Clear();
			cmbPrinterPort.AddItem("LPT1");
			cmbPrinterPort.AddItem("LPT2");
			cmbPrinterPort.AddItem("LPT3");
			cmbPrinterPort.AddItem("COM1");
			cmbPrinterPort.AddItem("COM2");
			cmbPrinterPort.AddItem("AUTO");
		}

		private void optReportAlignment_Click(ref short Index)
		{
			boolDirty = true;
		}

		private void txtAuditMarginTop_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtAuditMarginTop_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{

			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtAuditMarginTop.Text = "0";
				txtAuditMarginTop.SelectionStart = 0;
				txtAuditMarginTop.SelectionLength = 1;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAuditPassword_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		public void HandlePartialPermission_2(int lngFuncID)
		{
			HandlePartialPermission(ref lngFuncID);
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case modGlobal.CRSECURITYCASHDRAWERCODES:
						{
							if (strPerm == "F")
							{
								chkCDConnected.Enabled = true;
							}
							else
							{
								chkCDConnected.Enabled = false;
							}
							break;
						}
					case modGlobal.CRSECURITYCHANGECOMMENTS:
						{
							if (strPerm == "F")
							{
								txtBottomComment.Enabled = true;
								txtTopComment.Enabled = true;
							}
							else
							{
								txtBottomComment.Enabled = false;
								txtTopComment.Enabled = false;
							}
							break;
						}
					case modGlobal.CRSECURITYPASSWORDPROTECTAUDIT:
						{
							if (strPerm == "F")
							{
								chkAuditPassword.Enabled = true;
								txtAuditPassword.Enabled = true;
							}
							else
							{
								chkAuditPassword.Enabled = false;
								txtAuditPassword.Enabled = false;
							}
							break;
						}
					case modGlobal.CRSECURITYREPORTALIGNMENT:
						{

							break;
						}
					case modGlobal.CRSECURITYRESETRECEIPTNUMBER:
						{
							if (strPerm == "F")
							{
								txtResetReceipt.Enabled = true;
							}
							else
							{
								txtResetReceipt.Enabled = false;
							}
							break;
						}
					case modGlobal.CRSECURITYTELLERDEFAULTS:
						{
							if (strPerm == "F")
							{
								cmbTellerUsed.Enabled = true;
							}
							else
							{
								cmbTellerUsed.Enabled = false;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void txtBottomComment_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCDCode_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtMVDelay_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || KeyAscii == Keys.Back)
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtResetReceipt_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtSeperateCreditCardCashAcct_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || KeyAscii == Keys.Back)
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTopComment_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void ShowFrame(int intFrameIndex)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				AdjustAllFrames_2(true);
				switch (intFrameIndex)
				{
					case 0:
						{
							// Receipt Information
							fraWidth.Visible = true;
							break;
						}
					case 1:
						{
							// Daily Audit Options
							fraAuditPassword.Visible = true;
							break;
						}
					case 2:
						{
							// Reciept Comments
							fraComment.Visible = true;
							break;
						}
					case 3:
						{
							// Report Alignment
							fraAlign.Visible = true;
							break;
						}
					case 4:
						{
							// Teller Optios
							fraTeller.Visible = true;
							break;
						}
					case 5:
						{
							// Cash Drawer
							fraCD.Visible = true;
							break;
						}
					case 6:
						{
							// Reset Reciept Number
							fraReset.Visible = true;
							// Case 7  'Check Rec Options
							// fraCheckRec.Visible = True
							break;
						}
					case 7:
						{
							// Other Options
							fraOther.Visible = true;
							break;
						}
					case 8:
						{
							fraEPayments.Visible = true;
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Frame - " + FCConvert.ToString(intFrameIndex), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AdjustAllFrames_2(bool boolHide)
		{
			AdjustAllFrames(boolHide);
		}

		private void AdjustAllFrames(bool boolHide = false)
		{

			if (boolHide)
			{
				// this routine will hide all of the frames
				fraAlign.Visible = false;
				fraAuditPassword.Visible = false;
				fraCD.Visible = false;
				fraComment.Visible = false;
				fraReset.Visible = false;
				fraTeller.Visible = false;
				fraWidth.Visible = false;
				// fraCheckRec.Visible = False
				fraOther.Visible = false;
				fraEPayments.Visible = false;
			}
		}

		private void ShowEPymtFrame(string strIndex)
		{
            if (strIndex == "Invoice Cloud")
			{
                fraGatewayInfo_ICL.Visible = true;
				fraGatewayInfo_IME.Visible = false;
            }
			else if (strIndex == "InforME")
			{ 
                fraGatewayInfo_IME.Visible = true;
            }
			else
			{
				// Hide All
                fraGatewayInfo_ICL.Visible = false;
				fraGatewayInfo_IME.Visible = false;
            }

		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}

        private void SetupServiceCodesGrid()
        {
            ServiceCodesGrid.Rows = 1;
            ServiceCodesGrid.ColDataType((int) ServiceCodeColumns.Default, FCGrid.DataTypeSettings.flexDTBoolean);
            ServiceCodesGrid.TextMatrix(0, (int)ServiceCodeColumns.Default, "Default");
            ServiceCodesGrid.TextMatrix(0, (int)ServiceCodeColumns.Code, "Code");
            ServiceCodesGrid.TextMatrix(0, (int)ServiceCodeColumns.Description, "Description");
        }

        private void ResizeServiceCodesGrid()
        {
            var gridWidth = ServiceCodesGrid.WidthOriginal;
            ServiceCodesGrid.ColWidth((int)ServiceCodeColumns.Default, (.1 * gridWidth).ToInteger());
            ServiceCodesGrid.ColWidth((int) ServiceCodeColumns.Code, (.55 * gridWidth).ToInteger());
            ServiceCodesGrid.ColWidth((int) ServiceCodeColumns.Description, (.3 * gridWidth).ToInteger());
        }

        private void ValidateDefaultServiceCodes(int row)
        {
            if (row > 0 && ServiceCodesGrid.Rows >= row)
            {
                if (ServiceCodesGrid.TextMatrix(row, (int)ServiceCodeColumns.Default).ToBoolean() == true)
                {
					for(int x = 1; x < ServiceCodesGrid.Rows; x++)
                    {
                        if (x != row)
                        {
                            if (ServiceCodesGrid.TextMatrix(x, (int) ServiceCodeColumns.Default).ToBoolean())
                            {
                                ServiceCodesGrid.TextMatrix(x, (int) ServiceCodeColumns.Default, false);
                            }
                        }
                    }
                }
            }
        }

		private enum ServiceCodeColumns
        {
			Default = 0,
			Code = 1,
			Description = 2
        }


        private void AddServiceCode()
        {
            ServiceCodesGrid.Rows = ServiceCodesGrid.Rows + 1;
            var row = ServiceCodesGrid.Rows - 1;
            ServiceCodesGrid.TextMatrix(row, (int) ServiceCodeColumns.Default, false);
            ServiceCodesGrid.TextMatrix(row, (int) ServiceCodeColumns.Code, "");
            ServiceCodesGrid.TextMatrix(row, (int) ServiceCodeColumns.Description, "");
            ServiceCodesGrid.RowData(row, 0);
        }

        private void RemoveServiceCode()
        {
            var row = ServiceCodesGrid.Row;
            if (row > 0 && row < ServiceCodesGrid.Rows)
            {
                var id =Convert.ToInt32(ServiceCodesGrid.RowData(row));
                if (id > 0)
                {
					RemovedServiceCodes.Add(id);
                }
				ServiceCodesGrid.RemoveItem(row);
            }
        }

        private bool ValidateServiceCodes()
        {
            var rowCount = ServiceCodesGrid.Rows - 1;
            var hasDefault = false;
            for (int row = 1; row <= rowCount; row++)
            {
                if (Convert.ToInt32(ServiceCodesGrid.RowData(row)) > 0)
                {
                    if (ServiceCodesGrid.TextMatrix(row, (int)ServiceCodeColumns.Code).IsNullOrWhiteSpace())
                    {
                        FCMessageBox.Show("Service codes cannot be blank", MsgBoxStyle.Exclamation,
                            "Invalid Service Code");
                        return false;
                    }

                    if (ServiceCodesGrid.TextMatrix(row, (int) ServiceCodeColumns.Description).IsNullOrWhiteSpace())
                    {
                        FCMessageBox.Show("Service code descriptions cannot be blank", MsgBoxStyle.Exclamation,
                            "Invalid Description");
                        return false;
                    }
                }

                if (ServiceCodesGrid.TextMatrix(row, (int)ServiceCodeColumns.Default).ToBoolean())
                {
                    hasDefault = true;
                }
			}

            if (!hasDefault && rowCount  > 1)
            {
                FCMessageBox.Show("You must select a default service code", MsgBoxStyle.Exclamation, "No Default Code");
                return false;
            }
            return true;
        }

        private IEnumerable<ServiceCodeGridItem> getServiceCodesFromGrid()
        {
            var rowCount = ServiceCodesGrid.Rows - 1;
            var serviceCodes = new List<ServiceCodeGridItem>();
            for (int row = 1; row <= rowCount; row++)
            {
				var serviceCode = new ServiceCodeGridItem();
                serviceCode.Code = ServiceCodesGrid.TextMatrix(row, (int) ServiceCodeColumns.Code).Trim();
                serviceCode.Description = ServiceCodesGrid.TextMatrix(row, (int) ServiceCodeColumns.Description).Trim();
                serviceCode.Id = Convert.ToInt32(ServiceCodesGrid.RowData(row));
                serviceCode.IsDefault = ServiceCodesGrid.TextMatrix(row, (int) ServiceCodeColumns.Default).ToBoolean() || rowCount == 1;
                if (!serviceCode.Code.IsNullOrWhiteSpace() && !serviceCode.Code.IsNullOrWhiteSpace())
                {
					serviceCodes.Add(serviceCode);
                }
            }

            return serviceCodes;
        }

        private void LoadServiceCodesGrid(string defaultServiceCode)
        {
            ServiceCodesGrid.Rows = 1;
            
			var rsIME = new clsDRWrapper();
            rsIME.OpenRecordset("SELECT Id, AcctID, AcctDescription FROM EPmtAccounts ORDER BY AcctID", "CashReceipts");
            while (!rsIME.EndOfFile())
            {
                ServiceCodesGrid.Rows += 1;
                var row = ServiceCodesGrid.Rows - 1;
                ServiceCodesGrid.TextMatrix(row, (int)ServiceCodeColumns.Code,
                    rsIME.Get_Fields_String("AcctID"));
                ServiceCodesGrid.TextMatrix(row, (int)ServiceCodeColumns.Description,
                    rsIME.Get_Fields_String("AcctDescription"));
                if (rsIME.Get_Fields_String("AcctID") == defaultServiceCode)
                {
                    ServiceCodesGrid.TextMatrix(row, (int)ServiceCodeColumns.Default, true.ToString());
                }
                else
                {
                    ServiceCodesGrid.TextMatrix(row, (int)ServiceCodeColumns.Default, false.ToString());
                }

                ServiceCodesGrid.RowData(row, rsIME.Get_Fields_Int32("Id"));
                rsIME.MoveNext();
            }
		}
        private class ServiceCodeGridItem : ServiceCode
        {
            public bool IsDefault { get; set; }

            public ServiceCode ToServiceCode()
            {
				return new ServiceCode()
                {
					Code = this.Code,
					Description = this.Description,
					Id = this.Id
                };
            }
        }
    }

   
}
