﻿namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarCCReportDetail.
	/// </summary>
	partial class sarCCReportDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarCCReportDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblBankNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCardNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBankNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceiptNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCardNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblBankNumberTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBankTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCardNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCardNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumberTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBankNumber,
				this.fldAmount,
				this.fldReceiptNumber,
				this.fldName,
				this.fldCardNum
			});
			this.Detail.Height = 0.1354167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblBankNumber,
				this.lblAmount,
				this.lblReceiptNumber,
				this.lblName,
				this.lblCardNum
			});
			this.GroupHeader1.Height = 0.2291667F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line1,
				this.lblBankNumberTotal,
				this.fldBankTotal,
				this.Line2
			});
			this.GroupFooter1.Height = 0.3958333F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblBankNumber
			// 
			this.lblBankNumber.Height = 0.1875F;
			this.lblBankNumber.HyperLink = null;
			this.lblBankNumber.Left = 0.0625F;
			this.lblBankNumber.Name = "lblBankNumber";
			this.lblBankNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblBankNumber.Text = "Card Type";
			this.lblBankNumber.Top = 0F;
			this.lblBankNumber.Width = 1.375F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 5F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0F;
			this.lblAmount.Width = 0.875F;
			// 
			// lblReceiptNumber
			// 
			this.lblReceiptNumber.Height = 0.1875F;
			this.lblReceiptNumber.HyperLink = null;
			this.lblReceiptNumber.Left = 5.9375F;
			this.lblReceiptNumber.Name = "lblReceiptNumber";
			this.lblReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.lblReceiptNumber.Text = "Receipt #";
			this.lblReceiptNumber.Top = 0F;
			this.lblReceiptNumber.Width = 0.9375F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 1.5F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblName.Text = "Name";
			this.lblName.Top = 0F;
			this.lblName.Width = 0.75F;
			// 
			// lblCardNum
			// 
			this.lblCardNum.Height = 0.1875F;
			this.lblCardNum.HyperLink = null;
			this.lblCardNum.Left = 3.875F;
			this.lblCardNum.Name = "lblCardNum";
			this.lblCardNum.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
			this.lblCardNum.Text = "Card #";
			this.lblCardNum.Top = 0F;
			this.lblCardNum.Width = 0.875F;
			// 
			// fldBankNumber
			// 
			this.fldBankNumber.Height = 0.125F;
			this.fldBankNumber.Left = 0.0625F;
			this.fldBankNumber.MultiLine = false;
			this.fldBankNumber.Name = "fldBankNumber";
			this.fldBankNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; white-space: nowrap";
			this.fldBankNumber.Text = null;
			this.fldBankNumber.Top = 0F;
			this.fldBankNumber.Width = 1.375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.125F;
			this.fldAmount.Left = 4.90625F;
			this.fldAmount.MultiLine = false;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; white-space: nowrap";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.96875F;
			// 
			// fldReceiptNumber
			// 
			this.fldReceiptNumber.Height = 0.125F;
			this.fldReceiptNumber.Left = 5.9375F;
			this.fldReceiptNumber.MultiLine = false;
			this.fldReceiptNumber.Name = "fldReceiptNumber";
			this.fldReceiptNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; white-space: nowrap";
			this.fldReceiptNumber.Text = null;
			this.fldReceiptNumber.Top = 0F;
			this.fldReceiptNumber.Width = 0.9375F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.125F;
			this.fldName.Left = 1.5F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.3125F;
			// 
			// fldCardNum
			// 
			this.fldCardNum.Height = 0.125F;
			this.fldCardNum.Left = 3.875F;
			this.fldCardNum.MultiLine = false;
			this.fldCardNum.Name = "fldCardNum";
			this.fldCardNum.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; white-space: nowrap";
			this.fldCardNum.Text = null;
			this.fldCardNum.Top = 0F;
			this.fldCardNum.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 4.375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 1.625F;
			this.Line1.X1 = 4.375F;
			this.Line1.X2 = 6F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// lblBankNumberTotal
			// 
			this.lblBankNumberTotal.Height = 0.1875F;
			this.lblBankNumberTotal.HyperLink = null;
			this.lblBankNumberTotal.Left = 1.5625F;
			this.lblBankNumberTotal.MultiLine = false;
			this.lblBankNumberTotal.Name = "lblBankNumberTotal";
			this.lblBankNumberTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap";
			this.lblBankNumberTotal.Text = "Card Type Total:";
			this.lblBankNumberTotal.Top = 0.03125F;
			this.lblBankNumberTotal.Width = 2.875F;
			// 
			// fldBankTotal
			// 
			this.fldBankTotal.Height = 0.1875F;
			this.fldBankTotal.Left = 4.4375F;
			this.fldBankTotal.MultiLine = false;
			this.fldBankTotal.Name = "fldBankTotal";
			this.fldBankTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; whit" + "e-space: nowrap";
			this.fldBankTotal.Text = "0.00";
			this.fldBankTotal.Top = 0.03125F;
			this.fldBankTotal.Width = 1.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.3125F;
			this.Line2.Width = 7F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7F;
			this.Line2.Y1 = 0.3125F;
			this.Line2.Y2 = 0.3125F;
			// 
			// sarCCReportDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCardNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceiptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCardNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankNumberTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBankNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCardNum;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBankNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCardNum;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBankNumberTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBankTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
