﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Web.Management;
using Autofac;
using Autofac.Core.Lifetime;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Messaging;
using TWCR0000.ReceiptProcess;
using TWSharedLibrary;

namespace TWCR0000
{
	public class MDIParent
	//: BaseForm
	{
        public FCCommonDialog CommonDialog1;
        public MDIParent()
		{
			//
			// Required for Windows Form Designer support
			//
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/19/2005              *
		// ********************************************************
		private int[] LabelNumber = new int[200 + 1];
		int CurRow;
		public bool boolLockReceiptForms;
		const string strTrio = "TRIO Software";
        private IContainer diContainer;
        public void Init(IContainer container)
        {
            this.diContainer = container;
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			App.MainForm.NavigationMenu.Owner = this;
			App.MainForm.menuTree.ImageList = CreateImageList();
			GetCRMenu();
		}

		public ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-open-cash-drawer", "open-cash-drawer"),
				new ImageListEntry("menutree-end-of-year", "end-of-year"),
				new ImageListEntry("menutree-daily-audit-report", "audit"),
				new ImageListEntry("menutree-setup-projects", "setup-projects"),
				new ImageListEntry("menutree-receipt-input", "receipt-input"),
				new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
				new ImageListEntry("menutree-printing-active", "printing-active"),
				new ImageListEntry("menutree-open-cash-drawer-active", "open-cash-drawer-active"),
				new ImageListEntry("menutree-end-of-year-active", "end-of-year-active"),
				new ImageListEntry("menutree-daily-audit-report-active", "audit-active"),
				new ImageListEntry("menutree-setup-projects-active", "setup-projects-active"),
				new ImageListEntry("menutree-receipt-input-active", "receipt-input-active"),
			});
			return imageList;
		}

		public void ClearLabels()
		{

		}

		private void SetMenuOptions(string strMenu)
		{
			ClearLabels();
			modGlobal.CloseChildForms();
			//GRID.Text = strMenu;
			GetCRMenu();
		}

		public void Menu1()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            CommandDispatcher commandDispatcher;
            switch (vbPorterVar)
            {
                // Receipt Input
                case "Main" when !boolLockReceiptForms:
                    //modGlobal.Statics.gboolPendingRI = false;
                    //boolLockReceiptForms = true;
                    ////! Load frmReceiptInput;
                    //frmReceiptInput.InstancePtr.Show(App.MainForm);
                     commandDispatcher = diContainer.Resolve<CommandDispatcher>();
                    var crSettings = diContainer.Resolve<GlobalCashReceiptSettings>();
                    (bool Success, string TellerId) result = (Success: true, TellerId: "");

                    if (crSettings.TellerIdUsage != TellerIDUsage.NotRequired)
                    {
                        result = commandDispatcher.Send(new GetTellerId()).Result;
                    }

                    if (result.Success)
                    {
                        commandDispatcher.Send(new StartCRTransaction(result.TellerId,false));
                    }
                    break;
                case "Main":
                    MessageBox.Show("You cannot process a receipt while Receipt Input or Reprint Receipt is open.", "Multiple Window Error", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    break;
                case "PRINT":

	                commandDispatcher = diContainer.Resolve<CommandDispatcher>();
	                commandDispatcher.Send(new ShowReceiptSearch());

					//// Receipt Search Routines
					//frmCustomReport.InstancePtr.Show(App.MainForm);
     //               //Application.DoEvents();
     //               frmCustomReport.InstancePtr.Focus();

                    break;
                case "File":

                    //! Load frmCustomize;
                    frmCustomize.InstancePtr.Show(App.MainForm);
                    //Application.DoEvents();
                    frmCustomize.InstancePtr.Focus();

                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

        public  void CloseoutPerformed()
        {
            GetCRMenu();
        }
		public void Menu2()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":

                    // Daily Receipt Audit
                    //FC:FINAL:MSH - close form before showing for correct initializing fields(Close() in form was replaced by Hide() 
                    // to avoid missing data in reports, after reopening form fields will be disabled)
                    frmDailyReceiptAudit.InstancePtr.Close();
                    frmDailyReceiptAudit.InstancePtr.Init(CloseoutPerformed);
                    frmDailyReceiptAudit.InstancePtr.Show(App.MainForm);
                    //Application.DoEvents();
                    // frmDailyReceiptAudit.SetFocus
                    break;
                case "PRINT":

                    // Receipt Type Listing
                    frmReceiptTypeListing.InstancePtr.Show(App.MainForm);
                    //Application.DoEvents();
                    frmReceiptTypeListing.InstancePtr.Focus();

                    break;
                case "File":

                    // Check Database Structure
                    frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking Structure");
                    modGlobal.NewVersion_2(true);
                    frmWait.InstancePtr.Unload();

                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu3()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":

                    // Print Menu
                    //GetPrintMenu();
                    break;
                case "PRINT":

                    // MVR3 Listing
                    frmMVR3Listing.InstancePtr.Show(App.MainForm);
                    //Application.DoEvents();
                    frmMVR3Listing.InstancePtr.Init();

                    break;
                case "File":

                    // Bank Maintenance
                    frmBankMaintenance.InstancePtr.Show(App.MainForm);

                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu4()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                // Open Cash Drawer
                case "Main" when modGlobal.OpenCashDrawer():
                    break;
                case "Main":

                    // error will be shown in the OpenCashDrawer function
                    // MsgBox "There has been an error opening the Cash Drawer.", vbOKOnly + vbCritical, "Open Cash Drawer Error"
                    break;
                case "PRINT":

                    // Reprint Last Daily Audit
                    frmRPTViewer.InstancePtr.Show(App.MainForm);
                    //Application.DoEvents();
                    break;
                case "File":

                    // Credit Card Maintenance
                    frmCreditCard.InstancePtr.Show(App.MainForm);

                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu5()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":

                    // Setup Receipt Types
                    frmTypeSetup.InstancePtr.Show(App.MainForm);
                    //Application.DoEvents();
                    frmTypeSetup.InstancePtr.Focus();

                    break;

                // Reprint Any Receipt
                case "PRINT" when !boolLockReceiptForms:
                    //boolLockReceiptForms = true;
                    //frmReprint.InstancePtr.intType = 0;
                    //frmReprint.InstancePtr.Show(App.MainForm);
                    var commandDispatcher = diContainer.Resolve<CommandDispatcher>();
                    var receiptId = commandDispatcher.Send(new ChooseExistingReceipt()).Result;
                    if (receiptId > 0)
                    {
                        commandDispatcher.Send(new ReprintReceipt(receiptId));
                    }
                    break;
                case "PRINT":
                    MessageBox.Show("You cannot reprint while Receipt Input is open.", "Multiple Window Error", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    break;
                case "File":

                    // Purge
                    frmPurge.InstancePtr.Show(App.MainForm);

                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu6()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":

                    // EndOfYear Procedure
                    frmEOY.InstancePtr.Show(App.MainForm);
                    //Application.DoEvents();
                    frmEOY.InstancePtr.Focus();

                    break;

                // Redisplay Last Receipt
                case "PRINT" when !boolLockReceiptForms:
                    //boolLockReceiptForms = true;
                    ////! Load frmReprint;
                    //frmReprint.InstancePtr.intType = 1;
                    //frmReprint.InstancePtr.Show(App.MainForm);
                    var commandDispatcher = diContainer.Resolve<CommandDispatcher>();
                    var receiptId = commandDispatcher.Send(new GetLastReceiptId()).Result;
                    if (receiptId > 0)
                    {
                        commandDispatcher.Send(new ReprintReceipt(receiptId));
                    }
                    break;
                case "PRINT":
                    MessageBox.Show("You cannot reprint while Receipt Input is open.", "Multiple Window Error", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    break;
                case "File":
                    GetCRMenu();

                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu7()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
            CommandDispatcher commandDispatcher;
            switch (vbPorterVar)
            {
                case "Main":
                {
                    if (modGlobal.Statics.gboolPendingTransactions)
                    {
                        // run frmReceiptInput with parameters to use pending transactions
                        //modGlobal.Statics.gboolPendingRI = true;
                        //frmReceiptInput.InstancePtr.Show(App.MainForm);

                        commandDispatcher = diContainer.Resolve<CommandDispatcher>();
                        var crSettings = diContainer.Resolve<GlobalCashReceiptSettings>();
                        (bool Success, string TellerId) result = (Success: true, TellerId: "");

                        if (crSettings.TellerIdUsage != TellerIDUsage.NotRequired)
                        {
                            result = commandDispatcher.Send(new GetTellerId()).Result;
                        }

                        if (result.Success)
                        {
                            commandDispatcher.Send(new StartCRTransaction(result.TellerId,true));
                        }
                        break;
                        }
                    //else
                    //{
                    //    // File Maintenance
                    //    GetFileMenu();
                    //}
                    break;
                }

                case "PRINT":

                    // Cross Check Payments
                    frmReportViewer.InstancePtr.Init(rptPaymentCrossCheck.InstancePtr);

                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu8()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                {
                    //var commandDispatcher = diContainer.Resolve<CommandDispatcher>();
                    //var crSettings = diContainer.Resolve<GlobalCashReceiptSettings>();
                    //(bool Success, string TellerId) result = (Success: true, TellerId: "");
                    
                    //if (crSettings.TellerIdUsage != TellerIDUsage.NotRequired)
                    //{
                    //    result = commandDispatcher.Send(new GetTellerId()).Result;
                    //}

                    //if (result.Success)
                    //{
                    //    commandDispatcher.Send(new StartCRTransaction(result.TellerId));
                    //}

                    break;
                }

                case "PRINT":
                    GetCRMenu();

                    break;
                case "File":
                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu9()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				if (modGlobal.Statics.gboolPendingTransactions)
				{
					// exit CR back to General Entry Menu
					//Close();
				}
			}
			else
			{
				GetCRMenu();
			}
		}

		public void Menu10()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    break;
                case "File":
                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu11()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    break;
                case "File":
                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu12()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    break;
                case "File":
                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu13()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    break;
                case "File":
                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu14()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            switch (vbPorterVar)
            {
                case "Main":
                    break;
                case "File":
                    break;
                default:
                    GetCRMenu();

                    break;
            }
        }

		public void Menu15()
        {
            var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;

            if (vbPorterVar != "Main")
            {
                GetCRMenu();
            }
            
        }

		public void Menu16()
		{
			//switch (GRID.Tag)
			{
				//default:
				//{
				GetCRMenu();
				//break;
				//}
				//end switch
			}
		}

		public void Menu17()
		{
			//switch (GRID.Tag)
			//{
			//	default:
			//		{
			GetCRMenu();
			//			break;
			//		}
			//}
			//end switch
		}

		public void Menu18()
		{
			//switch (GRID.Tag)
			//{
			//	default:
			//		{
			GetCRMenu();
			//			break;
			//		}
			//}
			//end switch
		}
		
		//private void MDIForm_QueryUnload(object sender, FCFormClosingEventArgs e)
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		bool boolClose;
		//		int intTypeFound = 0;
		//		boolClose = true;
		//		foreach (Form frm in FCGlobal.Statics.Forms)
		//		{
		//			string vbPorterVar = frm.Name;
		//			if (vbPorterVar == "frmRECLStatus")
		//			{
		//				if (frmRECLStatus.InstancePtr.vsPayments.Rows <= 1)
		//				{
		//					boolClose = true;
		//				}
		//				else
		//				{
		//					boolClose = false;
		//					intTypeFound = 2;
		//					break;
		//				}
		//			}
		//			else if (vbPorterVar == "frmReceiptInput")
		//			{
		//				if (frmReceiptInput.InstancePtr.vsSummary.Rows <= 1)
		//				{
		//					boolClose = true;
		//					frmReceiptInput.InstancePtr.Unload();
		//				}
		//				else
		//				{
		//					boolClose = false;
		//					intTypeFound = 1;
		//					break;
		//				}
		//				if (Strings.Trim(frmReceiptInput.InstancePtr.txtTellerID.Text + "") != "")
		//				{
		//					//Application.DoEvents();
		//					modGlobal.LogOutTeller_2(Strings.Trim(frmReceiptInput.InstancePtr.txtTellerID.Text + ""));
		//				}
		//			}
		//		}
		//		if (boolClose)
		//		{
		//			foreach (Form frm in FCGlobal.Statics.Forms)
		//			{
		//				frm.Close();
		//			}
		//			//Application.DoEvents();
		//			FCFileSystem.FileClose();
		//			modBlockEntry.WriteYY();
  //                  //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
  //                  //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
  //                  App.MainForm.OpenModule("TWGNENTY");
		//			Application.Exit();
		//		}
		//		else
		//		{
		//			e.Cancel = true;
		//			//Application.DoEvents();
		//			switch (intTypeFound)
		//			{
		//				case 1:
		//					{
		//						MessageBox.Show("You cannot close the program while the Receipt Input screen is open and transactions are pending.", "Receipt Pending", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//						break;
		//					}
		//				case 2:
		//					{
		//						MessageBox.Show("You cannot close the program while the CL screen is open and transactions are pending.", "Receipt Pending", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//						break;
		//					}
		//			}
		//			//end switch
		//		}
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Unloading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		Application.Exit();
		//	}
		//}

		private void mnuFExit_Click(object sender, System.EventArgs e)
		{
			//Close();
		}

		private void mnuFileCentralParties_Click(object sender, System.EventArgs e)
		{
			frmCentralPartySearch.InstancePtr.Show(App.MainForm);
		}

		private void mnuFixedAssetsHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWFA0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuFormsPrint_Click(object sender, System.EventArgs e)
		{
			frmScreenPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
			//this.ZOrder(ZOrderConstants.SendToBack);
		}

		private void mnuGeneralEntryHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWGENTRY.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuHAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout.InstancePtr.Show();
		}

		private void mnuMotorVehicleRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWMV0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		public void GetCRMenu()
		{
			string strTemp = "";
			int lngFCode = 0;
			bool boolDisable = false;
			//ClearLabels();
			string menu = "Main";
			string imageKey = "";
			App.MainForm.Text = strTrio + " - Cash Receipting   [Main]";
			App.MainForm.Caption = "CASH RECEIPTING";
			App.MainForm.ApplicationIcon = "icon-cash-receipting";
            App.MainForm.NavigationMenu.OriginName = "Cash Receipting";
            App.MainForm.NavigationMenu.Clear();
			string assemblyName = this.GetType().Assembly.GetName().Name;
			if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
			{
				App.MainForm.ApplicationIcons.Add(assemblyName, "icon-cash-receipting");
			}
			modGlobal.Statics.gboolPendingTransactions = modGlobal.CheckForPendingTransactions();
			// this will check to see if there are any pending transactions in the db already
			for (int CurRow = 1; CurRow <= 8; CurRow++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Receipt Input";
							imageKey = "receipt-input";
							lngFCode = modGlobal.CRSECURITYRECEIPTINPUT;
							break;
						}
					case 2:
						{
							strTemp = "Daily Receipt Audit";
							imageKey = "audit";
							lngFCode = modGlobal.CRSECURITYDAILYRECEIPTAUDIT;
							break;
						}
					case 3:
						{
							strTemp = "Printing";
							imageKey = "printing";
							break;
						}
					case 4:
						{
							strTemp = "Open Cash Drawer";
							lngFCode = modGlobal.CRSECURITYOPENCASHDRAWER;
							imageKey = "open-cash-drawer";
							break;
						}
					case 5:
						{
							strTemp = "Type Setup";
							imageKey = "setup-projects";
							lngFCode = modGlobal.CRSECURITYSETUPRECEIPTTYPES;
							break;
						}
					case 6:
						{
							strTemp = "End Of Year";
							lngFCode = modGlobal.CRSECURITYENDOFYEAR;
							imageKey = "end-of-year";
							break;
						}
					case 7:
						{
							if (modGlobal.Statics.gboolPendingTransactions)
							{
								strTemp = "Pending Transaction Input";
								lngFCode = 0;
							}
							else
							{
								strTemp = "File Maintenance";
								imageKey = "file-maintenance";
								lngFCode = modGlobal.CRSECURITYFILEMAINETENACE;
							}
							break;
						}
					case 8:
						{
							if (modGlobal.Statics.gboolPendingTransactions)
							{
								strTemp = "File Maintenance";
								imageKey = "file-maintenance";
								lngFCode = modGlobal.CRSECURITYFILEMAINETENACE;
							}
							break;
						}
				}

				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				if (modGlobal.Statics.gboolPendingTransactions || CurRow < 8)
				{
					FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 1, imageKey);
					switch (CurRow)
					{
						case 3:
							{
								GetPrintMenu(newItem);
								break;
							}
						case 7:
							{
								if (!modGlobal.Statics.gboolPendingTransactions)
								{
									GetFileMenu(newItem);
								}
								break;
							}
						case 8:
							{
								if (modGlobal.Statics.gboolPendingTransactions)
								{
									GetFileMenu(newItem);
								}
								break;
							}
					}
				}
			}
            //FCMenuItem item = App.MainForm.NavigationMenu.Add("New Receipt Input", "Menu8" , menu, true, 1, "receipt-input");
            LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			if (modGlobal.Statics.gboolPendingTransactions)
			{
				LabelNumber[Strings.Asc("7")] = 7;
				LabelNumber[Strings.Asc("M")] = 8;
				LabelNumber[Strings.Asc("X")] = 9;
			}
			else
			{
				LabelNumber[Strings.Asc("M")] = 7;
				LabelNumber[Strings.Asc("X")] = 8;
			}
		}

		private void GetFileMenu(FCMenuItem parent)
		{
			//ClearLabels();
			//GRID.Text = "File";
			int lngFCode = 0;
			string menu = "File";
			bool boolDisable = false;
			App.MainForm.Text = strTrio + " - Cash Receipting   [File Maintenance]";
			string strTemp = "";
			for (int CurRow = 1; CurRow <= 5; CurRow++)
			{
				lngFCode = 0;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Customize";
							lngFCode = modGlobal.CRSECURITYCUSTOMIZE;
							break;
						}
					case 2:
						{
							strTemp = "Check Database Structure";
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Bank Maintenance";
							lngFCode = modGlobal.CRSECURITYBANKMAINTENANCE;
							break;
						}
					case 4:
						{
							strTemp = "Credit/Debit Card Maint.";
							lngFCode = 0;
							break;
						}
					case 5:
						{
							strTemp = "Purge Receipts";
							lngFCode = 0;
							break;
						}
				//case 6:
				//    {
				//        strTemp = "X. Return to Main Menu";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				//if (CurRow == 4)
				//{
				//	if (modGlobal.Statics.gstrEPaymentPortal == "P")
				//	{
				//		boolDisable = true;
				//	}
				//}
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("X")] = 6;
		}

		private void GetPrintMenu(FCMenuItem parent)
		{
			int lngFCode = 0;
			string strTemp = "";
			//ClearLabels();
			//GRID.Text = "PRINT";
			string menu = "PRINT";
			bool boolDisable = false;
			App.MainForm.Text = strTrio + " - Cash Receipting   [Printing]";
			for (int CurRow = 1; CurRow <= 7; CurRow++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (CurRow)
				{
					case 1:
						{
							strTemp = "Receipt Search";
							lngFCode = modGlobal.CRSECURITYSEARCHROUTINES;
							break;
						}
					case 2:
						{
							strTemp = "Receipt Type Listing";
							lngFCode = modGlobal.CRSECURITYTYPELISTING;
							break;
						}
					case 3:
						{
							strTemp = "MVR3 Listing";
							lngFCode = modGlobal.CRSECURITYMVR3LISTING;
							break;
						}
					case 4:
						{
							strTemp = "Redisplay Daily Audit Report";
							lngFCode = modGlobal.CRSECURITYREPRINTDAILYAUDIT;
							break;
						}
					case 5:
						{
							strTemp = "Redisplay Any Receipt";
							lngFCode = modGlobal.CRSECURITYREPRINTRECEIPT;
							break;
						}
					case 6:
						{
							strTemp = "Redisplay Last Receipt";
							lngFCode = modGlobal.CRSECURITYREDISPLAYLASTRECEIPT;
							break;
						}
					case 7:
						{
							strTemp = "Cross Check Payment Report";
							break;
						}
				//case 8:
				//    {
				//        strTemp = "X. Return to Main Menu";
				//        lngFCode = 0;
				//        break;
				//    }
				//default:
				//    {
				//        strTemp = "";
				//        break;
				//    }
				}
				//end switch
				//GRID.TextMatrix((CurRow), 1, strTemp);
				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, lngFCode, false);
				}
				parent.SubItems.Add(strTemp, "Menu" + CurRow, menu, !boolDisable, 2);
			}
			LabelNumber[Strings.Asc("1")] = 1;
			LabelNumber[Strings.Asc("2")] = 2;
			LabelNumber[Strings.Asc("3")] = 3;
			LabelNumber[Strings.Asc("4")] = 4;
			LabelNumber[Strings.Asc("5")] = 5;
			LabelNumber[Strings.Asc("6")] = 6;
			LabelNumber[Strings.Asc("7")] = 7;
			LabelNumber[Strings.Asc("X")] = 8;
		}

		private void DisableMenuOption_2(bool boolState, int intRow)
		{
			DisableMenuOption(ref boolState, ref intRow);
		}

		private void DisableMenuOption(ref bool boolState, ref int intRow)
		{
			//if (boolState)
			//{
			//	GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, modGlobalFunctions.SetMenuOptionColor(ref boolState));
			//}
			//else
			//{
			//	GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, modGlobalFunctions.SetMenuOptionColor(ref boolState));
			//}
		}

		private void mnuOptionsChangeUser_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("Are you sure that you would like to change the current user?", "Change User", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				frmTrioSplash.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
			}
		}

		public void ChangeUser()
		{
			// this sub uses the security class to refresh the menu options reflecting the new user's security level
			modGlobalConstants.Statics.secUser = new clsTrioSecurity();
			modGlobalConstants.Statics.secUser.Init("CR");
			// refresh the grid
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "Main")
			{
				GetCRMenu();
			}
			else if (vbPorterVar == "File")
			{
				//GetFileMenu();
			}
			else
			{
				GetCRMenu();
			}
			modGlobalConstants.Statics.gstrUserID = modGlobalConstants.Statics.secUser.Get_OpID();
		}
		// *******************************************************************************************************
		// Help File Subroutines
		// *******************************************************************************************************
		private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWPY0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWPP0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWRE0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuRedbookHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWRB0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWBL0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWCL0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWUT0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWVR0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWBD0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWCR0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuClerkHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWCK0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWCE0000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		private void mnuEnhanced911Help_Click(object sender, System.EventArgs e)
		{
			string strDir;
			strDir = Application.StartupPath;
			if (Strings.Right(strDir, 1) != "\\")
			{
				strDir += "\\";
			}
			modGlobalFunctions.HelpShell("TWE90000.HLP", AppWinStyle.MaximizedFocus, strDir);
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
		}

		public class StaticVariables
		{
			/// </summary>
			/// Summary description for MDIParent.
			/// <summary>
			public int R = 0, c;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
