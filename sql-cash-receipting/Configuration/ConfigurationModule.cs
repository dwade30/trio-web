﻿using System;
using System.Web.Compilation;
using Autofac;
using Autofac.Core.Activators.Reflection;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using TWCR0000.ReceiptProcess;

namespace TWCR0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmMiscReceiptInput>().As <IView<IMiscTransactionViewModel>>();
            //builder.RegisterType<frmChooseTransactionType>().As<IView<IChooseTransactionTypeViewModel>>();
            builder.RegisterType<frmReceiptTransactionSummary>().As<IView<ICRCashOutViewModel>>(); //.UsingConstructor(new MostParametersConstructorSelector());
            builder.RegisterType<frmReceiptTransactionDetail>().As<IView<IReceiptTransactionDetailViewModel>>();
            //builder.Register<Func<IReceiptTransactionSummary, IView<IReceiptTransactionSummary>>>(c => (value) =>
            //{
            //    return new frmReceiptTransactionSummary(value);
            //});
            builder.RegisterType<frmGetTellerID>().As<IModalView<IGetTellerIdViewModel>>();
            builder.RegisterType<frmProcessCreditCardTransaction>().As<IModalView<IProcessCreditCardTransactionViewModel>>();
			builder.RegisterType<frmGetNewCRBank>().As<IModalView<INewCRBankViewModel>>();
            builder.RegisterType<frmGetMultiTownId>().As<IModalView<IMultiTownSelectionViewModel>>();
            builder.RegisterType<frmVoidReceipt>().As<IModalView<IVoidReceiptViewModel>>();
            builder.RegisterType<frmReceiptSearch>().As<IView<IReceiptSearchViewModel>>();
            builder.RegisterType<frmChooseReceipt>().As<IModalView<IChooseExistingReceiptViewModel>>();
            builder.RegisterType<frmChooseReceiptFromList>().As<IModalView<ISelectReceiptFromListViewModel>>();
            builder.RegisterType<frmNewReprint>().As<IView<IReprintReceiptViewModel>>();
            builder.RegisterType<frmPaymentPortal>().As<IModalView<IGetPaymentFromPortalViewModel>>();
        }
    }
}