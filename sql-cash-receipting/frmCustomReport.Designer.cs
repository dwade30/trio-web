//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmCustomReport : BaseForm
	{
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCFrame Frame3;
		public FCGrid vsSetup;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCListBox lstTypes;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCLabel lblReportName;
		public fecherFoundation.FCFrame fraWhere;
		public FCGrid txtAcct;
		public fecherFoundation.FCButton cmdClear;
		public FCGrid vsWhere;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileNew;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReport));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.lblReport = new fecherFoundation.FCLabel();
			this.cmbReport = new fecherFoundation.FCComboBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.vsSetup = new fecherFoundation.FCGrid();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.fraFields = new fecherFoundation.FCFrame();
			this.lstTypes = new fecherFoundation.FCListBox();
			this.lstFields = new fecherFoundation.FCDraggableListBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboSavedReport = new fecherFoundation.FCComboBox();
			this.cmdAdd = new fecherFoundation.FCButton();
			this.lblReportName = new fecherFoundation.FCLabel();
			this.fraWhere = new fecherFoundation.FCFrame();
			this.txtAcct = new fecherFoundation.FCGrid();
			this.vsWhere = new fecherFoundation.FCGrid();
			this.cmdClear = new fecherFoundation.FCButton();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.fcButton1 = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsSetup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
			this.fraFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 681);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.fraFields);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Size = new System.Drawing.Size(1078, 606);
			this.ClientArea.Controls.SetChildIndex(this.fraWhere, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraSort, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraFields, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.fcButton1);
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.fcButton1, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(180, 30);
			this.HeaderText.Text = "Receipt Search";
			// 
			// lblReport
			// 
			this.lblReport.AutoSize = true;
			this.lblReport.Location = new System.Drawing.Point(5, 19);
			this.lblReport.Name = "lblReport";
			this.lblReport.Size = new System.Drawing.Size(87, 17);
			this.lblReport.Text = "LBLREPORT";
			// 
			// cmbReport
			// 
			this.cmbReport.Items.AddRange(new object[] {
            "Save Custom Report",
            "Show Saved Report",
            "Delete Saved Report"});
			this.cmbReport.Location = new System.Drawing.Point(20, 30);
			this.cmbReport.Name = "cmbReport";
			this.cmbReport.Size = new System.Drawing.Size(250, 40);
			this.cmbReport.TabIndex = 11;
			this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
			// 
			// Frame3
			// 
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.vsSetup);
			this.Frame3.Controls.Add(this.lblInstructions);
			this.Frame3.Location = new System.Drawing.Point(10, 10);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(1045, 262);
			this.Frame3.TabIndex = 17;
			// 
			// vsSetup
			// 
			this.vsSetup.Cols = 3;
			this.vsSetup.Location = new System.Drawing.Point(30, 55);
			this.vsSetup.Name = "vsSetup";
			this.vsSetup.Rows = 1;
			this.vsSetup.Size = new System.Drawing.Size(1002, 185);
			this.vsSetup.TabIndex = 18;
			this.vsSetup.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsSetup_ChangeEdit);
			this.vsSetup.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsSetup_BeforeEdit);
			this.vsSetup.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsSetup_ValidateEdit);
			this.vsSetup.CurrentCellChanged += new System.EventHandler(this.vsSetup_RowColChange);
			this.vsSetup.CellMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsSetup_CellChanged);
			this.vsSetup.Enter += new System.EventHandler(this.vsSetup_Enter);
			this.vsSetup.Validating += new System.ComponentModel.CancelEventHandler(this.vsSetup_Validating);
			this.vsSetup.MouseMove += new Wisej.Web.MouseEventHandler(this.vsSetup_MouseMoveEvent);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(1002, 16);
			this.lblInstructions.TabIndex = 19;
			this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.lstTypes);
			this.fraFields.Controls.Add(this.lstFields);
			this.fraFields.Location = new System.Drawing.Point(30, 278);
			this.fraFields.Name = "fraFields";
			this.fraFields.Size = new System.Drawing.Size(422, 189);
			this.fraFields.TabIndex = 14;
			this.fraFields.Text = "Receipt Types";
			this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
			// 
			// lstTypes
			// 
			this.lstTypes.BackColor = System.Drawing.SystemColors.Window;
			this.lstTypes.CheckBoxes = true;
			this.lstTypes.Location = new System.Drawing.Point(20, 30);
			this.lstTypes.Name = "lstTypes";
			this.lstTypes.Size = new System.Drawing.Size(382, 140);
			this.lstTypes.Sorted = true;
			this.lstTypes.Sorting = Wisej.Web.SortOrder.Ascending;
			this.lstTypes.Style = 1;
			this.lstTypes.TabIndex = 22;
			// 
			// lstFields
			// 
			this.lstFields.BackColor = System.Drawing.SystemColors.Window;
			this.lstFields.Enabled = false;
			this.lstFields.Location = new System.Drawing.Point(20, 30);
			this.lstFields.Name = "lstFields";
			this.lstFields.Size = new System.Drawing.Size(173, 126);
			this.lstFields.TabIndex = 15;
			this.lstFields.Visible = false;
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(407, 28);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(126, 40);
			this.cmdPrint.TabIndex = 13;
			this.cmdPrint.Text = "Print Preview";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Location = new System.Drawing.Point(477, 278);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(247, 189);
			this.fraSort.TabIndex = 11;
			this.fraSort.Text = "Fields To Sort By";
			this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
			// 
			// lstSort
			// 
			this.lstSort.BackColor = System.Drawing.SystemColors.Window;
			this.lstSort.CheckBoxes = true;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(207, 140);
			this.lstSort.Sorted = true;
			this.lstSort.Sorting = Wisej.Web.SortOrder.Ascending;
			this.lstSort.Style = 1;
			this.lstSort.TabIndex = 12;
			this.lstSort.SelectedIndexChanged += new System.EventHandler(this.lstSort_SelectedIndexChanged);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboSavedReport);
			this.Frame2.Controls.Add(this.cmbReport);
			this.Frame2.Controls.Add(this.cmdAdd);
			this.Frame2.Controls.Add(this.lblReportName);
			this.Frame2.Location = new System.Drawing.Point(752, 278);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(290, 189);
			this.Frame2.TabIndex = 5;
			this.Frame2.Text = "Report";
			// 
			// cboSavedReport
			// 
			this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
			this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
			this.cboSavedReport.Name = "cboSavedReport";
			this.cboSavedReport.Size = new System.Drawing.Size(250, 40);
			this.cboSavedReport.TabIndex = 10;
			this.cboSavedReport.Visible = false;
			this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
			// 
			// cmdAdd
			// 
			this.cmdAdd.AppearanceKey = "actionButton";
			this.cmdAdd.Location = new System.Drawing.Point(20, 130);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(250, 40);
			this.cmdAdd.TabIndex = 9;
			this.cmdAdd.Text = "Add Custom Report to Library";
			this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
			// 
			// lblReportName
			// 
			this.lblReportName.Location = new System.Drawing.Point(20, 94);
			this.lblReportName.Name = "lblReportName";
			this.lblReportName.Size = new System.Drawing.Size(263, 22);
			this.lblReportName.TabIndex = 20;
			this.lblReportName.Visible = false;
			// 
			// fraWhere
			// 
			this.fraWhere.Controls.Add(this.txtAcct);
			this.fraWhere.Controls.Add(this.vsWhere);
			this.fraWhere.Location = new System.Drawing.Point(30, 490);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(1012, 191);
			this.fraWhere.TabIndex = 2;
			this.fraWhere.Text = "Select Search Criteria";
			this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
			// 
			// txtAcct
			// 
			this.txtAcct.AutoSizeColumnsMode = Wisej.Web.DataGridViewAutoSizeColumnsMode.Fill;
			this.txtAcct.Cols = 1;
			this.txtAcct.ColumnHeadersVisible = false;
			this.txtAcct.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.txtAcct.FixedCols = 0;
			this.txtAcct.FixedRows = 0;
			this.txtAcct.Location = new System.Drawing.Point(20, 196);
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.ReadOnly = false;
			this.txtAcct.RowHeadersVisible = false;
			this.txtAcct.Rows = 1;
			this.txtAcct.Size = new System.Drawing.Size(106, 42);
			this.txtAcct.TabIndex = 21;
			this.txtAcct.Enter += new System.EventHandler(this.txtAcct_Enter);
			this.txtAcct.Leave += new System.EventHandler(this.txtAcct_Leave);
			this.txtAcct.Validating += new System.ComponentModel.CancelEventHandler(this.txtAcct_Validating);
			this.txtAcct.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAcct_KeyDownEvent);
			// 
			// vsWhere
			// 
			this.vsWhere.BackColorFixed = System.Drawing.Color.Black;
			this.vsWhere.Cols = 10;
			this.vsWhere.ColumnHeadersVisible = false;
			this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsWhere.FixedRows = 0;
			this.vsWhere.Location = new System.Drawing.Point(20, 30);
			this.vsWhere.Name = "vsWhere";
			this.vsWhere.ReadOnly = false;
			this.vsWhere.Rows = 0;
			this.vsWhere.Size = new System.Drawing.Size(972, 141);
			this.vsWhere.StandardTab = false;
			this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsWhere.TabIndex = 3;
			this.vsWhere.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEdit);
			this.vsWhere.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsWhere_KeyPressEdit);
			this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
			this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
			this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
			this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
			this.vsWhere.Scroll += new Wisej.Web.ScrollEventHandler(this.vsWhere_BeforeScroll);
			this.vsWhere.Enter += new System.EventHandler(this.vsWhere_Enter);
			this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(958, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(141, 24);
			this.cmdClear.TabIndex = 4;
			this.cmdClear.Text = "Clear Search Criteria";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileNew,
            this.mnuLayout,
            this.mnuFileSeperator2,
            this.mnuClear,
            this.mnuPrint,
            this.mnuSP1,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileNew
			// 
			this.mnuFileNew.Index = 0;
			this.mnuFileNew.Name = "mnuFileNew";
			this.mnuFileNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuFileNew.Text = "New";
			this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// mnuLayout
			// 
			this.mnuLayout.Enabled = false;
			this.mnuLayout.Index = 1;
			this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
			this.mnuLayout.Name = "mnuLayout";
			this.mnuLayout.Text = "Options";
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = 0;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow.Text = "Add Row";
			// 
			// mnuAddColumn
			// 
			this.mnuAddColumn.Index = 1;
			this.mnuAddColumn.Name = "mnuAddColumn";
			this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddColumn.Text = "Add Column";
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = 2;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow.Text = "Delete Row";
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = 3;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn.Text = "Delete Column";
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 2;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 3;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear Search Criteria";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 4;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Print Preview";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 5;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			// 
			// fcButton1
			// 
			this.fcButton1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.fcButton1.Location = new System.Drawing.Point(912, 29);
			this.fcButton1.Name = "fcButton1";
			this.fcButton1.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.fcButton1.Size = new System.Drawing.Size(42, 24);
			this.fcButton1.TabIndex = 17;
			this.fcButton1.Text = "New";
			this.fcButton1.Click += new System.EventHandler(this.fcButton1_Click);
			// 
			// frmCustomReport
			// 
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.KeyPreview = true;
			this.Name = "frmCustomReport";
			this.Text = "Receipt Search";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmCustomReport_Load);
			this.Activated += new System.EventHandler(this.frmCustomReport_Activated);
			this.Resize += new System.EventHandler(this.frmCustomReport_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsSetup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
			this.fraFields.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton fcButton1;
	}
}