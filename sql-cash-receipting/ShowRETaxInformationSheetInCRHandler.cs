﻿using Global;
using SharedApplication.CashReceipts;
using SharedApplication.Messaging;

namespace TWCR0000
{
    public class ShowRETaxInformationSheetInCRHandler : CommandHandler<ShowRETaxInformationSheetInCR>
    {
        protected override void Handle(ShowRETaxInformationSheetInCR command)
        {
            rptAccountInformationSheet.InstancePtr.Init(command.Account, command.EffectiveDate);
        }
    }
}