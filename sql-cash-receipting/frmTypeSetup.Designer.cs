﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmTypeSetup.
	/// </summary>
	partial class frmTypeSetup : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkMandatory;
		public fecherFoundation.FCFrame fraNewType;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdNewCode;
		public fecherFoundation.FCTextBox txtCode;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCFrame fraResCode;
		public fecherFoundation.FCComboBox cmbNewResCode;
		public fecherFoundation.FCButton cmdNewResCode;
		public fecherFoundation.FCButton cmdCancelResCode;
		public fecherFoundation.FCLabel lblNewResCode;
		public fecherFoundation.FCPanel fraReceiptList;
		public fecherFoundation.FCCheckBox chkAltEPmtAccount;
		public fecherFoundation.FCFrame fraAltEPmtAccount;
		public fecherFoundation.FCComboBox cboAltEPmtAccount;
		public fecherFoundation.FCCheckBox chkConvenienceFee;
		public fecherFoundation.FCCheckBox chkDeleted;
		public FCGrid vsReceipt;
		public fecherFoundation.FCFrame fraOptions;
		public fecherFoundation.FCComboBox cmbResCode;
		public fecherFoundation.FCCheckBox chkEFT;
		public fecherFoundation.FCCheckBox chkRB;
		public fecherFoundation.FCCheckBox chkBMV;
		public fecherFoundation.FCCheckBox chkDetail;
		public fecherFoundation.FCComboBox cmbCopies;
		public fecherFoundation.FCTextBox txtTitle;
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCCheckBox chkPrint;
		public FCGrid cmbCode;
		public fecherFoundation.FCLabel lblResCode;
		public fecherFoundation.FCLabel lblEFT;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblCopies;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCFrame fraDefaultAccount;
		public FCGrid txtDefaultAccount;
		public fecherFoundation.FCLabel lblDefaultAccount;
		public fecherFoundation.FCCheckBox chkMandatory_3;
		public fecherFoundation.FCCheckBox chkMandatory_2;
		public fecherFoundation.FCCheckBox chkMandatory_1;
		public fecherFoundation.FCCheckBox chkMandatory_0;
		public fecherFoundation.FCTextBox txtCTRL3Title;
		public fecherFoundation.FCTextBox txtRefTitle;
		public fecherFoundation.FCTextBox txtCTRL1Title;
		public fecherFoundation.FCTextBox txtCTRL2Title;
		public fecherFoundation.FCFrame fraConvenienceFee;
		public fecherFoundation.FCFrame fraPerDiem;
		public Global.T2KBackFillDecimal txtPerDiem;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCComboBox cboConvenienceFeeMethod;
		public FCGrid txtConvenienceFeeAccount;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel lblDeleted;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessNew;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcessReactivate;
		public fecherFoundation.FCToolStripMenuItem mnuFileNewResCode;
		public fecherFoundation.FCToolStripMenuItem mnuFileMOSESSetup;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator3;
		public fecherFoundation.FCToolStripMenuItem mnuFileBack;
		public fecherFoundation.FCToolStripMenuItem mnuFileForward;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.fraNewType = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdNewCode = new fecherFoundation.FCButton();
			this.txtCode = new fecherFoundation.FCTextBox();
			this.Label15 = new fecherFoundation.FCLabel();
			this.fraResCode = new fecherFoundation.FCFrame();
			this.cmbNewResCode = new fecherFoundation.FCComboBox();
			this.cmdNewResCode = new fecherFoundation.FCButton();
			this.cmdCancelResCode = new fecherFoundation.FCButton();
			this.lblNewResCode = new fecherFoundation.FCLabel();
			this.fraReceiptList = new fecherFoundation.FCPanel();
			this.chkAltEPmtAccount = new fecherFoundation.FCCheckBox();
			this.fraAltEPmtAccount = new fecherFoundation.FCFrame();
			this.cboAltEPmtAccount = new fecherFoundation.FCComboBox();
			this.chkConvenienceFee = new fecherFoundation.FCCheckBox();
			this.chkDeleted = new fecherFoundation.FCCheckBox();
			this.vsReceipt = new fecherFoundation.FCGrid();
			this.fraOptions = new fecherFoundation.FCFrame();
			this.cmbCopies = new fecherFoundation.FCComboBox();
			this.cmbResCode = new fecherFoundation.FCComboBox();
			this.chkEFT = new fecherFoundation.FCCheckBox();
			this.chkRB = new fecherFoundation.FCCheckBox();
			this.chkBMV = new fecherFoundation.FCCheckBox();
			this.chkDetail = new fecherFoundation.FCCheckBox();
			this.txtTitle = new fecherFoundation.FCTextBox();
			this.cmbType = new fecherFoundation.FCComboBox();
			this.chkPrint = new fecherFoundation.FCCheckBox();
			this.cmbCode = new fecherFoundation.FCGrid();
			this.lblResCode = new fecherFoundation.FCLabel();
			this.lblEFT = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.lblCopies = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblPrint = new fecherFoundation.FCLabel();
			this.fraDefaultAccount = new fecherFoundation.FCFrame();
			this.txtDefaultAccount = new fecherFoundation.FCGrid();
			this.lblDefaultAccount = new fecherFoundation.FCLabel();
			this.chkMandatory_3 = new fecherFoundation.FCCheckBox();
			this.chkMandatory_2 = new fecherFoundation.FCCheckBox();
			this.chkMandatory_1 = new fecherFoundation.FCCheckBox();
			this.chkMandatory_0 = new fecherFoundation.FCCheckBox();
			this.txtCTRL3Title = new fecherFoundation.FCTextBox();
			this.txtRefTitle = new fecherFoundation.FCTextBox();
			this.txtCTRL1Title = new fecherFoundation.FCTextBox();
			this.txtCTRL2Title = new fecherFoundation.FCTextBox();
			this.fraConvenienceFee = new fecherFoundation.FCFrame();
			this.fraPerDiem = new fecherFoundation.FCFrame();
			this.txtPerDiem = new Global.T2KBackFillDecimal();
			this.Label14 = new fecherFoundation.FCLabel();
			this.cboConvenienceFeeMethod = new fecherFoundation.FCComboBox();
			this.txtConvenienceFeeAccount = new fecherFoundation.FCGrid();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.lblAccountTitle = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.lblDeleted = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessReactivate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileNewResCode = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMOSESSetup = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileBack = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileForward = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSaveAdvance = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdNext = new fecherFoundation.FCButton();
			this.cmdPrevious = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdMoses = new fecherFoundation.FCButton();
			this.cmdReactivate = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraNewType)).BeginInit();
			this.fraNewType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraResCode)).BeginInit();
			this.fraResCode.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewResCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelResCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).BeginInit();
			this.fraReceiptList.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAltEPmtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAltEPmtAccount)).BeginInit();
			this.fraAltEPmtAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkConvenienceFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraOptions)).BeginInit();
			this.fraOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBMV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).BeginInit();
			this.fraDefaultAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraConvenienceFee)).BeginInit();
			this.fraConvenienceFee.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPerDiem)).BeginInit();
			this.fraPerDiem.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPerDiem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConvenienceFeeAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAdvance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNext)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdMoses)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReactivate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveAdvance);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(948, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraResCode);
			this.ClientArea.Controls.Add(this.fraReceiptList);
			this.ClientArea.Controls.Add(this.fraNewType);
			this.ClientArea.Size = new System.Drawing.Size(948, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdReactivate);
			this.TopPanel.Controls.Add(this.cmdMoses);
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdPrevious);
			this.TopPanel.Controls.Add(this.cmdNext);
			this.TopPanel.Controls.Add(this.cmdSave);
			this.TopPanel.Controls.Add(this.lblDeleted);
			this.TopPanel.Size = new System.Drawing.Size(948, 60);
			this.TopPanel.Controls.SetChildIndex(this.lblDeleted, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNext, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrevious, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdMoses, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdReactivate, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(138, 30);
			this.HeaderText.Text = "Type Setup";
			// 
			// fraNewType
			// 
			this.fraNewType.Controls.Add(this.cmdCancel);
			this.fraNewType.Controls.Add(this.cmdNewCode);
			this.fraNewType.Controls.Add(this.txtCode);
			this.fraNewType.Controls.Add(this.Label15);
			this.fraNewType.Location = new System.Drawing.Point(30, 30);
			this.fraNewType.Name = "fraNewType";
			this.fraNewType.Size = new System.Drawing.Size(221, 80);
			this.fraNewType.TabIndex = 26;
			this.fraNewType.Text = "New Type Code";
			this.fraNewType.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.Location = new System.Drawing.Point(90, 90);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(60, 23);
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Visible = false;
			this.cmdCancel.Enter += new System.EventHandler(this.cmdCancel_Enter);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdNewCode
			// 
			this.cmdNewCode.Location = new System.Drawing.Point(20, 90);
			this.cmdNewCode.Name = "cmdNewCode";
			this.cmdNewCode.Size = new System.Drawing.Size(60, 23);
			this.cmdNewCode.TabIndex = 2;
			this.cmdNewCode.Text = "Save";
			this.cmdNewCode.Visible = false;
			this.cmdNewCode.Enter += new System.EventHandler(this.cmdNewCode_Enter);
			this.cmdNewCode.Click += new System.EventHandler(this.cmdNewCode_Click);
			// 
			// txtCode
			// 
			this.txtCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtCode.Location = new System.Drawing.Point(80, 30);
			this.txtCode.MaxLength = 3;
			this.txtCode.Name = "txtCode";
			this.txtCode.Size = new System.Drawing.Size(116, 40);
			this.txtCode.TabIndex = 1;
			this.txtCode.Enter += new System.EventHandler(this.txtCode_Enter);
			this.txtCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCode_KeyDown);
			this.txtCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCode_KeyPress);
			// 
			// Label15
			// 
			this.Label15.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label15.Location = new System.Drawing.Point(20, 44);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(33, 20);
			this.Label15.TabIndex = 4;
			this.Label15.Text = "CODE";
			this.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraResCode
			// 
			this.fraResCode.Controls.Add(this.cmbNewResCode);
			this.fraResCode.Controls.Add(this.cmdNewResCode);
			this.fraResCode.Controls.Add(this.cmdCancelResCode);
			this.fraResCode.Controls.Add(this.lblNewResCode);
			this.fraResCode.Location = new System.Drawing.Point(-159, 128);
			this.fraResCode.Name = "fraResCode";
			this.fraResCode.Size = new System.Drawing.Size(159, 67);
			this.fraResCode.TabIndex = 48;
			this.fraResCode.Text = "New Town Key Type";
			this.fraResCode.Visible = false;
			// 
			// cmbNewResCode
			// 
			this.cmbNewResCode.BackColor = System.Drawing.SystemColors.Window;
			this.cmbNewResCode.Location = new System.Drawing.Point(81, 25);
			this.cmbNewResCode.Name = "cmbNewResCode";
			this.cmbNewResCode.Size = new System.Drawing.Size(50, 22);
			this.cmbNewResCode.Sorted = true;
			this.cmbNewResCode.TabIndex = 18;
			this.cmbNewResCode.DropDown += new System.EventHandler(this.cmbNewResCode_DropDown);
			this.cmbNewResCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbNewResCode_KeyDown);
			// 
			// cmdNewResCode
			// 
			this.cmdNewResCode.Location = new System.Drawing.Point(18, 40);
			this.cmdNewResCode.Name = "cmdNewResCode";
			this.cmdNewResCode.Size = new System.Drawing.Size(60, 23);
			this.cmdNewResCode.TabIndex = 19;
			this.cmdNewResCode.Text = "Save";
			this.cmdNewResCode.Visible = false;
			this.cmdNewResCode.Enter += new System.EventHandler(this.cmdNewResCode_Enter);
			this.cmdNewResCode.Click += new System.EventHandler(this.cmdNewResCode_Click);
			// 
			// cmdCancelResCode
			// 
			this.cmdCancelResCode.Location = new System.Drawing.Point(93, 40);
			this.cmdCancelResCode.Name = "cmdCancelResCode";
			this.cmdCancelResCode.Size = new System.Drawing.Size(60, 23);
			this.cmdCancelResCode.TabIndex = 20;
			this.cmdCancelResCode.Text = "Cancel";
			this.cmdCancelResCode.Visible = false;
			this.cmdCancelResCode.Enter += new System.EventHandler(this.cmdCancelResCode_Enter);
			this.cmdCancelResCode.Click += new System.EventHandler(this.cmdCancelResCode_Click);
			// 
			// lblNewResCode
			// 
			this.lblNewResCode.Location = new System.Drawing.Point(32, 25);
			this.lblNewResCode.Name = "lblNewResCode";
			this.lblNewResCode.Size = new System.Drawing.Size(33, 20);
			this.lblNewResCode.TabIndex = 49;
			this.lblNewResCode.Text = "CODE";
			// 
			// fraReceiptList
			// 
			this.fraReceiptList.AppearanceKey = "groupBoxNoBorders";
			this.fraReceiptList.Controls.Add(this.chkAltEPmtAccount);
			this.fraReceiptList.Controls.Add(this.fraAltEPmtAccount);
			this.fraReceiptList.Controls.Add(this.chkConvenienceFee);
			this.fraReceiptList.Controls.Add(this.chkDeleted);
			this.fraReceiptList.Controls.Add(this.vsReceipt);
			this.fraReceiptList.Controls.Add(this.fraOptions);
			this.fraReceiptList.Controls.Add(this.fraDefaultAccount);
			this.fraReceiptList.Controls.Add(this.chkMandatory_3);
			this.fraReceiptList.Controls.Add(this.chkMandatory_2);
			this.fraReceiptList.Controls.Add(this.chkMandatory_1);
			this.fraReceiptList.Controls.Add(this.chkMandatory_0);
			this.fraReceiptList.Controls.Add(this.txtCTRL3Title);
			this.fraReceiptList.Controls.Add(this.txtRefTitle);
			this.fraReceiptList.Controls.Add(this.txtCTRL1Title);
			this.fraReceiptList.Controls.Add(this.txtCTRL2Title);
			this.fraReceiptList.Controls.Add(this.fraConvenienceFee);
			this.fraReceiptList.Controls.Add(this.Label12);
			this.fraReceiptList.Controls.Add(this.lblAccountTitle);
			this.fraReceiptList.Controls.Add(this.Label8);
			this.fraReceiptList.Controls.Add(this.Label6);
			this.fraReceiptList.Controls.Add(this.Label7);
			this.fraReceiptList.Controls.Add(this.Label13);
			this.fraReceiptList.Controls.Add(this.Label11);
			this.fraReceiptList.Controls.Add(this.Label10);
			this.fraReceiptList.Controls.Add(this.Label9);
			this.fraReceiptList.Name = "fraReceiptList";
			this.fraReceiptList.Size = new System.Drawing.Size(918, 793);
			this.fraReceiptList.TabIndex = 49;
			// 
			// chkAltEPmtAccount
			// 
			this.chkAltEPmtAccount.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.chkAltEPmtAccount.Location = new System.Drawing.Point(30, 702);
			this.chkAltEPmtAccount.Name = "chkAltEPmtAccount";
			this.chkAltEPmtAccount.Size = new System.Drawing.Size(161, 23);
			this.chkAltEPmtAccount.TabIndex = 24;
			this.chkAltEPmtAccount.Text = "Override Service Code";
			this.chkAltEPmtAccount.Visible = false;
			this.chkAltEPmtAccount.CheckedChanged += new System.EventHandler(this.chkAltEPmtAccount_CheckedChanged);
			// 
			// fraAltEPmtAccount
			// 
			this.fraAltEPmtAccount.AppearanceKey = "groupBoxNoBorders";
			this.fraAltEPmtAccount.Controls.Add(this.cboAltEPmtAccount);
			this.fraAltEPmtAccount.Enabled = false;
			this.fraAltEPmtAccount.Location = new System.Drawing.Point(0, 718);
			this.fraAltEPmtAccount.Name = "fraAltEPmtAccount";
			this.fraAltEPmtAccount.Size = new System.Drawing.Size(358, 61);
			this.fraAltEPmtAccount.TabIndex = 23;
			this.fraAltEPmtAccount.Visible = false;
			// 
			// cboAltEPmtAccount
			// 
			this.cboAltEPmtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.cboAltEPmtAccount.Enabled = false;
			this.cboAltEPmtAccount.Location = new System.Drawing.Point(30, 19);
			this.cboAltEPmtAccount.Name = "cboAltEPmtAccount";
			this.cboAltEPmtAccount.Size = new System.Drawing.Size(316, 40);
			// 
			// chkConvenienceFee
			// 
			this.chkConvenienceFee.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.chkConvenienceFee.Location = new System.Drawing.Point(571, 588);
			this.chkConvenienceFee.Name = "chkConvenienceFee";
			this.chkConvenienceFee.Size = new System.Drawing.Size(186, 23);
			this.chkConvenienceFee.TabIndex = 21;
			this.chkConvenienceFee.Text = "Override Convenience Fee";
			this.chkConvenienceFee.Visible = false;
			this.chkConvenienceFee.CheckedChanged += new System.EventHandler(this.chkConvenienceFee_CheckedChanged);
			// 
			// chkDeleted
			// 
			this.chkDeleted.Location = new System.Drawing.Point(868, 241);
			this.chkDeleted.Name = "chkDeleted";
			this.chkDeleted.Size = new System.Drawing.Size(22, 22);
			this.chkDeleted.TabIndex = 17;
			this.chkDeleted.Visible = false;
			// 
			// vsReceipt
			// 
			this.vsReceipt.Cols = 6;
			this.vsReceipt.Location = new System.Drawing.Point(30, 276);
			this.vsReceipt.Name = "vsReceipt";
			this.vsReceipt.Rows = 7;
			this.vsReceipt.Size = new System.Drawing.Size(865, 287);
			this.vsReceipt.TabIndex = 18;
			this.vsReceipt.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsReceipt_KeyPressEdit);
			this.vsReceipt.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsReceipt_ChangeEdit);
			this.vsReceipt.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsReceipt_StartEdit);
			this.vsReceipt.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsReceipt_ValidateEdit);
			this.vsReceipt.CurrentCellChanged += new System.EventHandler(this.vsReceipt_RowColChange);
			this.vsReceipt.Enter += new System.EventHandler(this.vsReceipt_Enter);
			this.vsReceipt.Click += new System.EventHandler(this.vsReceipt_ClickEvent);
			this.vsReceipt.KeyDown += new Wisej.Web.KeyEventHandler(this.vsReceipt_KeyDownEvent);
			// 
			// fraOptions
			// 
			this.fraOptions.AppearanceKey = "groupBoxNoBorders";
			this.fraOptions.Controls.Add(this.cmbCopies);
			this.fraOptions.Controls.Add(this.cmbResCode);
			this.fraOptions.Controls.Add(this.chkEFT);
			this.fraOptions.Controls.Add(this.chkRB);
			this.fraOptions.Controls.Add(this.chkBMV);
			this.fraOptions.Controls.Add(this.chkDetail);
			this.fraOptions.Controls.Add(this.txtTitle);
			this.fraOptions.Controls.Add(this.cmbType);
			this.fraOptions.Controls.Add(this.chkPrint);
			this.fraOptions.Controls.Add(this.cmbCode);
			this.fraOptions.Controls.Add(this.lblResCode);
			this.fraOptions.Controls.Add(this.lblEFT);
			this.fraOptions.Controls.Add(this.Label4);
			this.fraOptions.Controls.Add(this.Label5);
			this.fraOptions.Controls.Add(this.lblCopies);
			this.fraOptions.Controls.Add(this.Label1);
			this.fraOptions.Controls.Add(this.Label2);
			this.fraOptions.Controls.Add(this.Label3);
			this.fraOptions.Controls.Add(this.lblPrint);
			this.fraOptions.Location = new System.Drawing.Point(0, 30);
			this.fraOptions.Name = "fraOptions";
			this.fraOptions.Size = new System.Drawing.Size(564, 225);
			this.fraOptions.TabIndex = 1;
			// 
			// cmbCopies
			// 
			this.cmbCopies.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCopies.Location = new System.Drawing.Point(316, 141);
			this.cmbCopies.Name = "cmbCopies";
			this.cmbCopies.Size = new System.Drawing.Size(64, 40);
			this.cmbCopies.Sorted = true;
			this.cmbCopies.TabIndex = 15;
			this.cmbCopies.Enter += new System.EventHandler(this.cmbCopies_Enter);
			this.cmbCopies.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCopies_KeyDown);
			// 
			// cmbResCode
			// 
			this.cmbResCode.BackColor = System.Drawing.SystemColors.Window;
			this.cmbResCode.Location = new System.Drawing.Point(459, 1);
			this.cmbResCode.Name = "cmbResCode";
			this.cmbResCode.Size = new System.Drawing.Size(91, 40);
			this.cmbResCode.Sorted = true;
			this.cmbResCode.TabIndex = 3;
			this.cmbResCode.SelectedIndexChanged += new System.EventHandler(this.cmbResCode_SelectedIndexChanged);
			this.cmbResCode.DropDown += new System.EventHandler(this.cmbResCode_DropDown);
			this.cmbResCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbResCode_KeyDown);
			// 
			// chkEFT
			// 
			this.chkEFT.Location = new System.Drawing.Point(358, 203);
			this.chkEFT.Name = "chkEFT";
			this.chkEFT.Size = new System.Drawing.Size(22, 22);
			this.chkEFT.TabIndex = 17;
			// 
			// chkRB
			// 
			this.chkRB.Location = new System.Drawing.Point(138, 203);
			this.chkRB.Name = "chkRB";
			this.chkRB.Size = new System.Drawing.Size(22, 22);
			this.chkRB.TabIndex = 11;
			// 
			// chkBMV
			// 
			this.chkBMV.Location = new System.Drawing.Point(138, 103);
			this.chkBMV.Name = "chkBMV";
			this.chkBMV.Size = new System.Drawing.Size(22, 22);
			this.chkBMV.TabIndex = 7;
			this.chkBMV.CheckedChanged += new System.EventHandler(this.chkBMV_CheckedChanged);
			// 
			// chkDetail
			// 
			this.chkDetail.Location = new System.Drawing.Point(138, 153);
			this.chkDetail.Name = "chkDetail";
			this.chkDetail.Size = new System.Drawing.Size(22, 22);
			this.chkDetail.TabIndex = 9;
			// 
			// txtTitle
			// 
			this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle.Location = new System.Drawing.Point(140, 54);
			this.txtTitle.MaxLength = 30;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Size = new System.Drawing.Size(410, 40);
			this.txtTitle.TabIndex = 5;
			this.txtTitle.Enter += new System.EventHandler(this.txtTitle_Enter);
			this.txtTitle.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
			// 
			// cmbType
			// 
			this.cmbType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbType.Location = new System.Drawing.Point(140, 1);
			this.cmbType.Name = "cmbType";
			this.cmbType.Size = new System.Drawing.Size(206, 40);
			this.cmbType.Sorted = true;
			this.cmbType.TabIndex = 1;
			this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
			this.cmbType.DropDown += new System.EventHandler(this.cmbType_DropDown);
			this.cmbType.DoubleClick += new System.EventHandler(this.cmbType_DoubleClick);
			this.cmbType.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbType_KeyDown);
			// 
			// chkPrint
			// 
			this.chkPrint.Location = new System.Drawing.Point(358, 103);
			this.chkPrint.Name = "chkPrint";
			this.chkPrint.Size = new System.Drawing.Size(22, 22);
			this.chkPrint.TabIndex = 13;
			this.chkPrint.CheckedChanged += new System.EventHandler(this.chkPrint_CheckedChanged);
			// 
			// cmbCode
			// 
			this.cmbCode.Cols = 1;
			this.cmbCode.ColumnHeadersVisible = false;
			this.cmbCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.cmbCode.FixedCols = 0;
			this.cmbCode.FixedRows = 0;
			this.cmbCode.Location = new System.Drawing.Point(166, 126);
			this.cmbCode.Name = "cmbCode";
			this.cmbCode.ReadOnly = false;
			this.cmbCode.RowHeadersVisible = false;
			this.cmbCode.Rows = 1;
			this.cmbCode.Size = new System.Drawing.Size(52, 15);
			this.cmbCode.TabIndex = 18;
			this.cmbCode.Visible = false;
			this.cmbCode.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDownEdit);
			this.cmbCode.ComboDropDown += new System.EventHandler(this.cmbCode_ComboDropDown);
			this.cmbCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.cmbCode_ChangeEdit);
			this.cmbCode.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.cmbCode_ValidateEdit);
			this.cmbCode.Enter += new System.EventHandler(this.cmbCode_Enter);
			this.cmbCode.Click += new System.EventHandler(this.cmbCode_ClickEvent);
			this.cmbCode.DoubleClick += new System.EventHandler(this.cmbCode_DblClick);
			this.cmbCode.Validating += new System.ComponentModel.CancelEventHandler(this.cmbCode_Validating);
			this.cmbCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDownEvent);
			// 
			// lblResCode
			// 
			this.lblResCode.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.lblResCode.Location = new System.Drawing.Point(361, 15);
			this.lblResCode.Name = "lblResCode";
			this.lblResCode.Size = new System.Drawing.Size(74, 20);
			this.lblResCode.TabIndex = 2;
			this.lblResCode.Text = "TOWN CODE";
			this.lblResCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblEFT
			// 
			this.lblEFT.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.lblEFT.Location = new System.Drawing.Point(236, 205);
			this.lblEFT.Name = "lblEFT";
			this.lblEFT.Size = new System.Drawing.Size(82, 20);
			this.lblEFT.TabIndex = 16;
			this.lblEFT.Text = "EFT CHECK";
			this.lblEFT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label4
			// 
			this.Label4.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label4.Location = new System.Drawing.Point(30, 205);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(102, 20);
			this.Label4.TabIndex = 10;
			this.Label4.Text = "BLUE BOOK TYPE";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label5
			// 
			this.Label5.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label5.Location = new System.Drawing.Point(30, 155);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(82, 20);
			this.Label5.TabIndex = 8;
			this.Label5.Text = "SHOW DETAIL";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCopies
			// 
			this.lblCopies.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.lblCopies.Location = new System.Drawing.Point(236, 155);
			this.lblCopies.Name = "lblCopies";
			this.lblCopies.Size = new System.Drawing.Size(43, 20);
			this.lblCopies.TabIndex = 14;
			this.lblCopies.Text = "COPIES";
			this.lblCopies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label1.Location = new System.Drawing.Point(30, 15);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(37, 20);
			this.Label1.TabIndex = 19;
			this.Label1.Text = "CODE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label2.Location = new System.Drawing.Point(30, 65);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(82, 18);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "DESCRIPTION";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label3
			// 
			this.Label3.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label3.Location = new System.Drawing.Point(30, 105);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(43, 20);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "BMV";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblPrint
			// 
			this.lblPrint.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.lblPrint.Location = new System.Drawing.Point(236, 105);
			this.lblPrint.Name = "lblPrint";
			this.lblPrint.Size = new System.Drawing.Size(94, 20);
			this.lblPrint.TabIndex = 12;
			this.lblPrint.Text = "PRINT RECEIPT";
			this.lblPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraDefaultAccount
			// 
			this.fraDefaultAccount.AppearanceKey = "groupBoxNoBorders";
			this.fraDefaultAccount.Controls.Add(this.txtDefaultAccount);
			this.fraDefaultAccount.Controls.Add(this.lblDefaultAccount);
			this.fraDefaultAccount.Location = new System.Drawing.Point(0, 623);
			this.fraDefaultAccount.Name = "fraDefaultAccount";
			this.fraDefaultAccount.Size = new System.Drawing.Size(419, 69);
			this.fraDefaultAccount.TabIndex = 22;
			// 
			// txtDefaultAccount
			// 
			this.txtDefaultAccount.Cols = 1;
			this.txtDefaultAccount.ColumnHeadersVisible = false;
			this.txtDefaultAccount.ExtendLastCol = true;
			this.txtDefaultAccount.FixedCols = 0;
			this.txtDefaultAccount.FixedRows = 0;
			this.txtDefaultAccount.Location = new System.Drawing.Point(220, 20);
			this.txtDefaultAccount.Name = "txtDefaultAccount";
			this.txtDefaultAccount.RowHeadersVisible = false;
			this.txtDefaultAccount.Rows = 1;
			this.txtDefaultAccount.Size = new System.Drawing.Size(185, 42);
			this.txtDefaultAccount.TabIndex = 1;
			this.txtDefaultAccount.Enter += new System.EventHandler(this.txtDefaultAccount_Enter);
			this.txtDefaultAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtDefaultAccount_Validating);
			this.txtDefaultAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDefaultAccount_KeyDownEvent);
			// 
			// lblDefaultAccount
			// 
			this.lblDefaultAccount.Location = new System.Drawing.Point(30, 20);
			this.lblDefaultAccount.Name = "lblDefaultAccount";
			this.lblDefaultAccount.Size = new System.Drawing.Size(181, 42);
			this.lblDefaultAccount.TabIndex = 2;
			// 
			// chkMandatory_3
			// 
			this.chkMandatory_3.Location = new System.Drawing.Point(868, 187);
			this.chkMandatory_3.Name = "chkMandatory_3";
			this.chkMandatory_3.Size = new System.Drawing.Size(22, 22);
			this.chkMandatory_3.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.chkMandatory_3, "Check if this field is required for this type.");
			this.chkMandatory_3.CheckedChanged += new System.EventHandler(this.chkMandatory_CheckedChanged);
			this.chkMandatory_3.Enter += new System.EventHandler(this.chkMandatory_Enter);
			this.chkMandatory_3.Leave += new System.EventHandler(this.chkMandatory_Leave);
			// 
			// chkMandatory_2
			// 
			this.chkMandatory_2.Location = new System.Drawing.Point(868, 137);
			this.chkMandatory_2.Name = "chkMandatory_2";
			this.chkMandatory_2.Size = new System.Drawing.Size(22, 22);
			this.chkMandatory_2.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.chkMandatory_2, "Check if this field is required for this type.");
			this.chkMandatory_2.CheckedChanged += new System.EventHandler(this.chkMandatory_CheckedChanged);
			this.chkMandatory_2.Enter += new System.EventHandler(this.chkMandatory_Enter);
			this.chkMandatory_2.Leave += new System.EventHandler(this.chkMandatory_Leave);
			// 
			// chkMandatory_1
			// 
			this.chkMandatory_1.Location = new System.Drawing.Point(868, 87);
			this.chkMandatory_1.Name = "chkMandatory_1";
			this.chkMandatory_1.Size = new System.Drawing.Size(22, 22);
			this.chkMandatory_1.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.chkMandatory_1, "Check if this field is required for this type.");
			this.chkMandatory_1.CheckedChanged += new System.EventHandler(this.chkMandatory_CheckedChanged);
			this.chkMandatory_1.Enter += new System.EventHandler(this.chkMandatory_Enter);
			this.chkMandatory_1.Leave += new System.EventHandler(this.chkMandatory_Leave);
			// 
			// chkMandatory_0
			// 
			this.chkMandatory_0.Location = new System.Drawing.Point(868, 37);
			this.chkMandatory_0.Name = "chkMandatory_0";
			this.chkMandatory_0.Size = new System.Drawing.Size(22, 22);
			this.chkMandatory_0.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.chkMandatory_0, "Check if this field is required for this type.");
			this.chkMandatory_0.CheckedChanged += new System.EventHandler(this.chkMandatory_CheckedChanged);
			this.chkMandatory_0.Enter += new System.EventHandler(this.chkMandatory_Enter);
			this.chkMandatory_0.Leave += new System.EventHandler(this.chkMandatory_Leave);
			// 
			// txtCTRL3Title
			// 
			this.txtCTRL3Title.BackColor = System.Drawing.SystemColors.Window;
			this.txtCTRL3Title.Location = new System.Drawing.Point(669, 180);
			this.txtCTRL3Title.MaxLength = 15;
			this.txtCTRL3Title.Name = "txtCTRL3Title";
			this.txtCTRL3Title.Size = new System.Drawing.Size(178, 40);
			this.txtCTRL3Title.TabIndex = 14;
			this.txtCTRL3Title.Enter += new System.EventHandler(this.txtCTRL3Title_Enter);
			this.txtCTRL3Title.TextChanged += new System.EventHandler(this.txtCTRL3Title_TextChanged);
			// 
			// txtRefTitle
			// 
			this.txtRefTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtRefTitle.Location = new System.Drawing.Point(669, 30);
			this.txtRefTitle.MaxLength = 15;
			this.txtRefTitle.Name = "txtRefTitle";
			this.txtRefTitle.Size = new System.Drawing.Size(178, 40);
			this.txtRefTitle.TabIndex = 5;
			this.txtRefTitle.Enter += new System.EventHandler(this.txtRefTitle_Enter);
			this.txtRefTitle.TextChanged += new System.EventHandler(this.txtRefTitle_TextChanged);
			this.txtRefTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtRefTitle_Validating);
			// 
			// txtCTRL1Title
			// 
			this.txtCTRL1Title.BackColor = System.Drawing.SystemColors.Window;
			this.txtCTRL1Title.Location = new System.Drawing.Point(669, 80);
			this.txtCTRL1Title.MaxLength = 15;
			this.txtCTRL1Title.Name = "txtCTRL1Title";
			this.txtCTRL1Title.Size = new System.Drawing.Size(178, 40);
			this.txtCTRL1Title.TabIndex = 8;
			this.txtCTRL1Title.Enter += new System.EventHandler(this.txtCTRL1Title_Enter);
			this.txtCTRL1Title.TextChanged += new System.EventHandler(this.txtCTRL1Title_TextChanged);
			// 
			// txtCTRL2Title
			// 
			this.txtCTRL2Title.BackColor = System.Drawing.SystemColors.Window;
			this.txtCTRL2Title.Location = new System.Drawing.Point(669, 130);
			this.txtCTRL2Title.MaxLength = 15;
			this.txtCTRL2Title.Name = "txtCTRL2Title";
			this.txtCTRL2Title.Size = new System.Drawing.Size(178, 40);
			this.txtCTRL2Title.TabIndex = 11;
			this.txtCTRL2Title.Enter += new System.EventHandler(this.txtCTRL2Title_Enter);
			this.txtCTRL2Title.TextChanged += new System.EventHandler(this.txtCTRL2Title_TextChanged);
			// 
			// fraConvenienceFee
			// 
			this.fraConvenienceFee.AppearanceKey = "groupBoxNoBorders";
			this.fraConvenienceFee.Controls.Add(this.fraPerDiem);
			this.fraConvenienceFee.Controls.Add(this.cboConvenienceFeeMethod);
			this.fraConvenienceFee.Controls.Add(this.txtConvenienceFeeAccount);
			this.fraConvenienceFee.Controls.Add(this.Label16);
			this.fraConvenienceFee.Enabled = false;
			this.fraConvenienceFee.Location = new System.Drawing.Point(571, 620);
			this.fraConvenienceFee.Name = "fraConvenienceFee";
			this.fraConvenienceFee.Size = new System.Drawing.Size(279, 159);
			this.fraConvenienceFee.TabIndex = 25;
			this.fraConvenienceFee.Visible = false;
			// 
			// fraPerDiem
			// 
			this.fraPerDiem.Controls.Add(this.txtPerDiem);
			this.fraPerDiem.Controls.Add(this.Label14);
			this.fraPerDiem.Location = new System.Drawing.Point(149, 24);
			this.fraPerDiem.Name = "fraPerDiem";
			this.fraPerDiem.Size = new System.Drawing.Size(130, 72);
			this.fraPerDiem.TabIndex = 3;
			this.fraPerDiem.Text = "Per Diem Rate";
			this.fraPerDiem.Visible = false;
			// 
			// txtPerDiem
			// 
			this.txtPerDiem.Location = new System.Drawing.Point(20, 30);
			this.txtPerDiem.MaxLength = 8;
			this.txtPerDiem.Name = "txtPerDiem";
			this.txtPerDiem.Size = new System.Drawing.Size(76, 22);
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(108, 36);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(19, 20);
			this.Label14.TabIndex = 1;
			this.Label14.Text = "%";
			this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboConvenienceFeeMethod
			// 
			this.cboConvenienceFeeMethod.BackColor = System.Drawing.SystemColors.Window;
			this.cboConvenienceFeeMethod.Enabled = false;
			this.cboConvenienceFeeMethod.Items.AddRange(new object[] {
            "Percentage",
            "No Fee"});
			this.cboConvenienceFeeMethod.Location = new System.Drawing.Point(20, 30);
			this.cboConvenienceFeeMethod.Name = "cboConvenienceFeeMethod";
			this.cboConvenienceFeeMethod.Size = new System.Drawing.Size(108, 40);
			this.cboConvenienceFeeMethod.TabIndex = 4;
			this.cboConvenienceFeeMethod.SelectedIndexChanged += new System.EventHandler(this.cboConvenienceFeeMethod_SelectedIndexChanged);
			// 
			// txtConvenienceFeeAccount
			// 
			this.txtConvenienceFeeAccount.Cols = 1;
			this.txtConvenienceFeeAccount.ColumnHeadersVisible = false;
			this.txtConvenienceFeeAccount.Enabled = false;
			this.txtConvenienceFeeAccount.FixedCols = 0;
			this.txtConvenienceFeeAccount.FixedRows = 0;
			this.txtConvenienceFeeAccount.Location = new System.Drawing.Point(149, 112);
			this.txtConvenienceFeeAccount.Name = "txtConvenienceFeeAccount";
			this.txtConvenienceFeeAccount.RowHeadersVisible = false;
			this.txtConvenienceFeeAccount.Rows = 1;
			this.txtConvenienceFeeAccount.Size = new System.Drawing.Size(119, 42);
			this.txtConvenienceFeeAccount.TabIndex = 2;
			// 
			// Label16
			// 
			this.Label16.Enabled = false;
			this.Label16.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label16.Location = new System.Drawing.Point(20, 126);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(128, 18);
			this.Label16.TabIndex = 1;
			this.Label16.Text = "CONVENIENCE FEE ACCOUNT";
			this.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label12
			// 
			this.Label12.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label12.Location = new System.Drawing.Point(781, 243);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(64, 20);
			this.Label12.TabIndex = 16;
			this.Label12.Text = "DELETED";
			this.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Label12.Visible = false;
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.Location = new System.Drawing.Point(144, 590);
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Size = new System.Drawing.Size(406, 31);
			this.lblAccountTitle.TabIndex = 20;
			this.lblAccountTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(30, 590);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(104, 31);
			this.Label8.TabIndex = 19;
			this.Label8.Text = "ACCOUNT TITLE";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label6
			// 
			this.Label6.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label6.Location = new System.Drawing.Point(857, 2);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(43, 18);
			this.Label6.TabIndex = 3;
			this.Label6.Text = "REQ";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.ToolTip1.SetToolTip(this.Label6, "Check if this field is required for this type.");
			// 
			// Label7
			// 
			this.Label7.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label7.Location = new System.Drawing.Point(570, 194);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(75, 16);
			this.Label7.TabIndex = 13;
			this.Label7.Text = "CONTROL 3";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label13
			// 
			this.Label13.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label13.Location = new System.Drawing.Point(669, 2);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(116, 18);
			this.Label13.TabIndex = 2;
			this.Label13.Text = "DEFAULT TITLES";
			this.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Label11
			// 
			this.Label11.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label11.Location = new System.Drawing.Point(569, 44);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(75, 16);
			this.Label11.TabIndex = 4;
			this.Label11.Text = "REFERENCE";
			this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label10
			// 
			this.Label10.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label10.Location = new System.Drawing.Point(569, 94);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(75, 16);
			this.Label10.TabIndex = 7;
			this.Label10.Text = "CONTROL 1";
			this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label9
			// 
			this.Label9.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.Label9.Location = new System.Drawing.Point(570, 144);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(75, 16);
			this.Label9.TabIndex = 10;
			this.Label9.Text = "CONTROL 2";
			this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDeleted
			// 
			this.lblDeleted.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
			this.lblDeleted.Location = new System.Drawing.Point(174, 31);
			this.lblDeleted.Name = "lblDeleted";
			this.lblDeleted.Size = new System.Drawing.Size(80, 23);
			this.lblDeleted.TabIndex = 8;
			this.lblDeleted.Text = "INACTIVE";
			this.lblDeleted.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblDeleted.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessNew,
            this.mnuProcessDelete,
            this.mnuProcessReactivate,
            this.mnuFileNewResCode,
            this.mnuFileMOSESSetup,
            this.mnuFileSeperator3,
            this.mnuFileBack,
            this.mnuFileForward,
            this.mnuFileSeperator2,
            this.mnuProcessSave,
            this.mnuProcessSaveExit,
            this.mnuProcessSeperator,
            this.mnuProcessExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			// 
			// mnuProcessNew
			// 
			this.mnuProcessNew.Index = 0;
			this.mnuProcessNew.Name = "mnuProcessNew";
			this.mnuProcessNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuProcessNew.Text = "New";
			this.mnuProcessNew.Click += new System.EventHandler(this.mnuProcessNew_Click);
			// 
			// mnuProcessDelete
			// 
			this.mnuProcessDelete.Index = 1;
			this.mnuProcessDelete.Name = "mnuProcessDelete";
			this.mnuProcessDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuProcessDelete.Text = "Delete";
			this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// mnuProcessReactivate
			// 
			this.mnuProcessReactivate.Enabled = false;
			this.mnuProcessReactivate.Index = 2;
			this.mnuProcessReactivate.Name = "mnuProcessReactivate";
			this.mnuProcessReactivate.Shortcut = Wisej.Web.Shortcut.CtrlR;
			this.mnuProcessReactivate.Text = "Reactivate";
			this.mnuProcessReactivate.Click += new System.EventHandler(this.mnuProcessReactivate_Click);
			// 
			// mnuFileNewResCode
			// 
			this.mnuFileNewResCode.Enabled = false;
			this.mnuFileNewResCode.Index = 3;
			this.mnuFileNewResCode.Name = "mnuFileNewResCode";
			this.mnuFileNewResCode.Text = "New Town Key";
			this.mnuFileNewResCode.Visible = false;
			this.mnuFileNewResCode.Click += new System.EventHandler(this.mnuFileNewResCode_Click);
			// 
			// mnuFileMOSESSetup
			// 
			this.mnuFileMOSESSetup.Index = 4;
			this.mnuFileMOSESSetup.Name = "mnuFileMOSESSetup";
			this.mnuFileMOSESSetup.Text = "MOSES Setup";
			this.mnuFileMOSESSetup.Click += new System.EventHandler(this.mnuFileMOSESSetup_Click);
			// 
			// mnuFileSeperator3
			// 
			this.mnuFileSeperator3.Index = 5;
			this.mnuFileSeperator3.Name = "mnuFileSeperator3";
			this.mnuFileSeperator3.Text = "-";
			// 
			// mnuFileBack
			// 
			this.mnuFileBack.Index = 6;
			this.mnuFileBack.Name = "mnuFileBack";
			this.mnuFileBack.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuFileBack.Text = "Previous";
			this.mnuFileBack.Click += new System.EventHandler(this.mnuFileBack_Click);
			// 
			// mnuFileForward
			// 
			this.mnuFileForward.Index = 7;
			this.mnuFileForward.Name = "mnuFileForward";
			this.mnuFileForward.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuFileForward.Text = "Next";
			this.mnuFileForward.Click += new System.EventHandler(this.mnuFileForward_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 8;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 9;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuProcessSave.Text = "Save";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// mnuProcessSaveExit
			// 
			this.mnuProcessSaveExit.Index = 10;
			this.mnuProcessSaveExit.Name = "mnuProcessSaveExit";
			this.mnuProcessSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSaveExit.Text = "Save & Advance";
			this.mnuProcessSaveExit.Click += new System.EventHandler(this.mnuProcessSaveExit_Click);
			// 
			// mnuProcessSeperator
			// 
			this.mnuProcessSeperator.Index = 11;
			this.mnuProcessSeperator.Name = "mnuProcessSeperator";
			this.mnuProcessSeperator.Text = "-";
			// 
			// mnuProcessExit
			// 
			this.mnuProcessExit.Index = 12;
			this.mnuProcessExit.Name = "mnuProcessExit";
			this.mnuProcessExit.Text = "Exit";
			this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
			// 
			// cmdSaveAdvance
			// 
			this.cmdSaveAdvance.AppearanceKey = "acceptButton";
			this.cmdSaveAdvance.Location = new System.Drawing.Point(401, 15);
			this.cmdSaveAdvance.Name = "cmdSaveAdvance";
			this.cmdSaveAdvance.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveAdvance.Size = new System.Drawing.Size(147, 40);
			this.cmdSaveAdvance.Text = "Save & Advance";
			this.cmdSaveAdvance.Click += new System.EventHandler(this.cmdSaveAdvance_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.Location = new System.Drawing.Point(872, 29);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdSave.Size = new System.Drawing.Size(42, 24);
			this.cmdSave.TabIndex = 1;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdNext
			// 
			this.cmdNext.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNext.Location = new System.Drawing.Point(827, 29);
			this.cmdNext.Name = "cmdNext";
			this.cmdNext.Shortcut = Wisej.Web.Shortcut.F8;
			this.cmdNext.Size = new System.Drawing.Size(40, 24);
			this.cmdNext.TabIndex = 2;
			this.cmdNext.Text = "Next";
			this.cmdNext.Click += new System.EventHandler(this.cmdNext_Click);
			// 
			// cmdPrevious
			// 
			this.cmdPrevious.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrevious.Location = new System.Drawing.Point(757, 29);
			this.cmdPrevious.Name = "cmdPrevious";
			this.cmdPrevious.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdPrevious.Size = new System.Drawing.Size(65, 24);
			this.cmdPrevious.TabIndex = 3;
			this.cmdPrevious.Text = "Previous";
			this.cmdPrevious.Click += new System.EventHandler(this.cmdPrevious_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.Location = new System.Drawing.Point(686, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.cmdDelete.Size = new System.Drawing.Size(67, 24);
			this.cmdDelete.TabIndex = 4;
			this.cmdDelete.Text = "Inactivate";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.Location = new System.Drawing.Point(641, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.cmdNew.Size = new System.Drawing.Size(42, 24);
			this.cmdNew.TabIndex = 5;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// cmdMoses
			// 
			this.cmdMoses.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdMoses.Location = new System.Drawing.Point(538, 29);
			this.cmdMoses.Name = "cmdMoses";
			this.cmdMoses.Size = new System.Drawing.Size(100, 24);
			this.cmdMoses.TabIndex = 6;
			this.cmdMoses.Text = "MOSES Setup";
			this.cmdMoses.Click += new System.EventHandler(this.cmdMoses_Click);
			// 
			// cmdReactivate
			// 
			this.cmdReactivate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdReactivate.Location = new System.Drawing.Point(456, 29);
			this.cmdReactivate.Name = "cmdReactivate";
			this.cmdReactivate.Size = new System.Drawing.Size(78, 24);
			this.cmdReactivate.TabIndex = 7;
			this.cmdReactivate.Text = "Reactivate";
			this.cmdReactivate.Click += new System.EventHandler(this.cmdReactivate_Click);
			// 
			// frmTypeSetup
			// 
			this.ClientSize = new System.Drawing.Size(948, 688);
			this.KeyPreview = true;
			this.Name = "frmTypeSetup";
			this.Text = "Type Setup";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmTypeSetup_Load);
			this.Activated += new System.EventHandler(this.frmTypeSetup_Activated);
			this.Resize += new System.EventHandler(this.frmTypeSetup_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTypeSetup_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraNewType)).EndInit();
			this.fraNewType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraResCode)).EndInit();
			this.fraResCode.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdNewResCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelResCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).EndInit();
			this.fraReceiptList.ResumeLayout(false);
			this.fraReceiptList.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAltEPmtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAltEPmtAccount)).EndInit();
			this.fraAltEPmtAccount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkConvenienceFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraOptions)).EndInit();
			this.fraOptions.ResumeLayout(false);
			this.fraOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBMV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmbCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).EndInit();
			this.fraDefaultAccount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMandatory_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraConvenienceFee)).EndInit();
			this.fraConvenienceFee.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraPerDiem)).EndInit();
			this.fraPerDiem.ResumeLayout(false);
			this.fraPerDiem.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtPerDiem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConvenienceFeeAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveAdvance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNext)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdMoses)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReactivate)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveAdvance;
		private FCButton cmdSave;
		private FCButton cmdNext;
		private FCButton cmdPrevious;
		private FCButton cmdDelete;
		private FCButton cmdNew;
		private FCButton cmdMoses;
		private FCButton cmdReactivate;
	}
}
