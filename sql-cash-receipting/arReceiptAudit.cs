﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTypes.
	/// </summary>
	public partial class sarTypes : BaseSectionReport
	{
        //FC:FINAL:IPI - #1626 - the subreport should not be created in Report_FetchData; instead create it in Detail_Format 
        private bool isARreport = false;
        private bool setReport = false;
		public sarTypes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Daily Receipt Audit";
		}

		public static sarTypes InstancePtr
		{
			get
			{
				return (sarTypes)Sys.GetInstance(typeof(sarTypes));
			}
		}

		protected sarTypes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (rsTypeInfo != null)
                rsTypeInfo.Dispose();
            if (rsAR != null)
				rsAR.Dispose();
            if (rsTypeTotals != null)
				rsTypeTotals.Dispose();
            if (rsTypes != null)
				rsTypes.Dispose();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTypes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/08/2005              *
		// ********************************************************
		clsDRWrapper rsTypes = new clsDRWrapper();
		clsDRWrapper rsTypeTotals = new clsDRWrapper();
		int intType;
		int lngResCode;
		int intOldType;
		int lngOldResCode;
		bool BoolFirstType;
		bool boolNoRecords;
		public string strTID = string.Empty;
		int intSubType;
		clsDRWrapper rsAR = new clsDRWrapper();
		int intOldSubType;
		clsDRWrapper rsTypeInfo = new clsDRWrapper();
		bool boolUsePrevYears = false;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("Binder");
			Fields["Binder"].Value = Strings.Format(intType, "000") + "-" + Strings.Format(lngResCode, "00");
			BoolFirstType = true;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            //FC:FINAL:IPI - #1626 - the subreport should not be created in Report_FetchData; instead create it in Detail_Format 
            setReport = false;
			// Dim strTypes As String
			string strArchiveTable = "";
			bool executeNextType = false;

			if (boolUsePrevYears)
			{
				strArchiveTable = "LastYearArchive";
			}
			else
			{
				strArchiveTable = "Archive";
			}
			if (intType == 97)
			{
				intOldSubType = intSubType;
				// kgk 03-12-11 trocr-279   Wrong Sub Type on group footers
				rsTypeInfo.MoveNext();
				if (rsTypeInfo.EndOfFile())
				{
					rsTypes.MoveNext();
					// boolNoRecords = True
					FindNextFullRecord();
					executeNextType = true;
					goto NextType;
				}
				else
				{
					intSubType = FCConvert.ToInt32(rsTypeInfo.Get_Fields_Int32("ARBillType"));
                    //FC:FINAL:IPI - #1626 - the subreport should not be created in Report_FetchData; instead create it in Detail_Format 
                    //sarTypeDetailOb.Report = new sarTypeDetailAR();
                    //sarTypeDetailOb.Report.UserData = Strings.Format(intSubType, "000");
                    setReport = true;
                    isARreport = true;
					SetupTitles();
					eArgs.EOF = false;
					intOldType = intType;
					// kgk 03-12-11 trocr-279   RTE if type 97 is first because intOldType = 0
					Fields["Binder"].Value = Strings.Format(intType, "000") + "-" + Strings.Format(lngResCode, "00") + "-" + Strings.Format(intSubType, "000");
				}
			}
			else
			{
				executeNextType = true;
				goto NextType;
			}
			NextType:
			;
			if (executeNextType)
			{
				if (rsTypes.EndOfFile() != true && rsTypes.BeginningOfFile() != true)
				{
					intOldType = intType;
					lngOldResCode = lngResCode;
					intType = FCConvert.ToInt32(rsTypes.Get_Fields_Int32("TypeCode"));
					lngResCode = FCConvert.ToInt32(rsTypes.Get_Fields_Int32("TownKey"));
					// MAL@20080611: Tracker Reference: 14057
					if (intType == 97)
					{
						if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
						{
							rsTypeInfo.OpenRecordset("SELECT DISTINCT ARBillType FROM " + strArchiveTable + " WHERE ReceiptType = 97 AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
						}
						else
						{
							rsTypeInfo.OpenRecordset("SELECT DISTINCT ARBillType FROM " + strArchiveTable + " WHERE ReceiptType = 97 AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
						}
						if (rsTypeInfo.EndOfFile() != true && rsTypeInfo.BeginningOfFile() != true)
						{
							// strTypes = "("
							// Do
							// strTypes = strTypes & rsTypeInfo.Fields("ARBillType") & ", "
							// rsTypeInfo.MoveNext
							// Loop While rsTypeInfo.EndOfFile <> True
							// If strTypes <> "(" Then
							// strTypes = Left(strTypes, Len(strTypes) - 2) & ")"
							// End If
							// rsAR.OpenRecordset "SELECT * FROM DefaultBillTypes WHERE TypeCode IN " & strTypes, "TWAR0000.vb1"
						}
						else
						{
							rsTypes.MoveNext();
							goto NextType;
						}
						intSubType = FCConvert.ToInt32(rsTypeInfo.Get_Fields_Int32("ARBillType"));
                        //FC:FINAL:IPI - #1626 - the subreport should not be created in Report_FetchData; instead create it in Detail_Format 
                        //sarTypeDetailOb.Report = new sarTypeDetailAR();
                        //sarTypeDetailOb.Report.UserData = Strings.Format(intSubType, "000");
                        setReport = true;
                        isARreport = true;
                    }
					else
					{
                        //FC:FINAL:IPI - #1626 - the subreport should not be created in Report_FetchData; instead create it in Detail_Format 
                        //sarTypeDetailOb.Report = new sarTypeDetail();
                        //sarTypeDetailOb.Report.UserData = Strings.Format(intType, "000") + "-" + Strings.Format(lngResCode, "00");
                        setReport = true;
                        isARreport = false;
                    }
					SetupTitles();
					eArgs.EOF = false;
				}
				else
				{
					intOldType = intType;
					lngOldResCode = lngResCode;
					eArgs.EOF = true;
				}
				if (eArgs.EOF != true)
				{
					Fields["Binder"].Value = Strings.Format(intType, "000") + "-" + Strings.Format(lngResCode, "00");
				}
			}
		}

		private void ActiveReport_Initialize()
		{
			SetupForm();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			rsTypeInfo.DisposeOf();
			rsTypes.DisposeOf();
			rsTypeTotals.DisposeOf();
			rsAR.DisposeOf();
			rsAR = null;
			rsTypes = null;
			rsTypeInfo = null;
			rsTypeTotals = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			ActiveReport_Initialize();
			string strSQL;
			boolUsePrevYears = frmDailyReceiptAudit.InstancePtr.boolUsePrevYears;
			strTID = Strings.Trim(FCConvert.ToString(this.UserData));
			boolNoRecords = true;
			strSQL = "SELECT TypeCode, TownKey FROM Type WHERE TypeTitle <> '' ORDER BY TownKey, TypeCode";
			rsTypes.OpenRecordset(strSQL);
			FindNextFullRecord();
		}

		private void FindNextFullRecord()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strArchiveTable = "";

			if (boolUsePrevYears)
			{
				strArchiveTable = "LastYearArchive";
			}
			else
			{
				strArchiveTable = "Archive";
			}
			do
			{
				if (!boolNoRecords)
				{
					rsTypes.MoveNext();
				}
				if (rsTypes.EndOfFile() != true)
				{
					// if there are no more to get then exit this loop/sub    'check how many records are of the next type
					if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
					{
						rsTemp.OpenRecordset("SELECT * FROM " + strArchiveTable + " WHERE ReceiptType = " + rsTypes.Get_Fields_Int32("TypeCode") + " AND TownKey = " + rsTypes.Get_Fields_Int32("TownKey") + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
					}
					else
					{
						rsTemp.OpenRecordset("SELECT * FROM " + strArchiveTable + " WHERE ReceiptType = " + rsTypes.Get_Fields_Int32("TypeCode") + " AND TownKey = " + rsTypes.Get_Fields_Int32("TownKey") + " AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL, modGlobal.DEFAULTDATABASE);
					}
					if (rsTemp.EndOfFile() != true)
					{
						// if there are records then
						boolNoRecords = false;
						break;
						// exit the loop and continue with reporting
					}
					if (boolNoRecords)
						rsTypes.MoveNext();
					// check the next record
				}
			}
			while (!rsTypes.EndOfFile());
		}

		private void SetupTitles()
		{
			// this will show the basic titles once and the dynamic titles for each type
			string strSQL;
			clsDRWrapper rsHeader = new clsDRWrapper();
			strSQL = "SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(intType) + " AND TownKey = " + FCConvert.ToString(lngResCode) + " ORDER BY TownKey, TypeCode";
			rsHeader.OpenRecordset(strSQL);
			if (BoolFirstType)
			{
				lblName.Visible = false;
				lblAcct.Visible = true;
				lblBill.Visible = true;
				lblReceipt.Visible = true;
				lblDate.Visible = false;
				lblTeller.Visible = true;
				lblNC.Visible = true;
				lblPayType.Visible = true;
				lblCode.Visible = true;
				BoolFirstType = false;
			}
			else
			{
				lblName.Visible = false;
				lblAcct.Visible = false;
				lblBill.Visible = false;
				lblReceipt.Visible = false;
				lblDate.Visible = false;
				lblTeller.Visible = false;
				lblNC.Visible = false;
				lblPayType.Visible = false;
				lblCode.Visible = false;
			}
			if (intType != 97)
			{
				if (rsHeader.EndOfFile() != true && rsHeader.BeginningOfFile() != true)
				{
					lblTypeHeader.Text = modGlobal.PadToString_8(rsHeader.Get_Fields_Int32("TypeCode"), 3) + " - " + rsHeader.Get_Fields_String("TypeTitle");
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						lblTypeHeader.Text = lblTypeHeader.Text + " - " + Strings.Format(lngResCode, "00");
					}
					lblTitle1.Text = rsHeader.Get_Fields_String("Title1Abbrev");
					lblTitle2.Text = rsHeader.Get_Fields_String("Title2Abbrev");
					lblTitle3.Text = rsHeader.Get_Fields_String("Title3Abbrev");
					lblTitle4.Text = rsHeader.Get_Fields_String("Title4Abbrev");
					lblTitle5.Text = rsHeader.Get_Fields_String("Title5Abbrev");
					lblTitle6.Text = rsHeader.Get_Fields_String("Title6Abbrev");
					if (Strings.Trim(lblTitle1.Text) == "")
					{
						lblTitle1.Text = ".......";
					}
					if (Strings.Trim(lblTitle2.Text) == "")
					{
						lblTitle2.Text = ".......";
					}
					if (Strings.Trim(lblTitle3.Text) == "")
					{
						lblTitle3.Text = ".......";
					}
					if (Strings.Trim(lblTitle4.Text) == "")
					{
						lblTitle4.Text = ".......";
					}
					if (Strings.Trim(lblTitle5.Text) == "")
					{
						lblTitle5.Text = ".......";
					}
					if (Strings.Trim(lblTitle6.Text) == "")
					{
						lblTitle6.Text = ".......";
					}
				}
			}
			else
			{
				if (intSubType != 0)
				{
					rsHeader.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intSubType), "TWAR0000.vb1");
					lblTypeHeader.Text = "097(" + modGlobal.PadToString_8(rsHeader.Get_Fields_Int32("TypeCode"), 3) + ") - " + rsHeader.Get_Fields_String("TypeTitle");
				}
				else
				{
					lblTypeHeader.Text = "097(000) - PRE PAYMENT";
				}
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					lblTypeHeader.Text = lblTypeHeader.Text + " - " + Strings.Format(lngResCode, "00");
				}
				lblTitle1.Text = "Principal";
				lblTitle2.Text = "Interest";
				lblTitle3.Text = "Sales Tax";
				lblTitle4.Text = ".......";
				lblTitle5.Text = ".......";
				lblTitle6.Text = ".......";
			}
			// fldConvenienceFeeTotal.Text = 0
			fldTotals1.Text = FCConvert.ToString(0);
			fldTotals2.Text = FCConvert.ToString(0);
			fldTotals3.Text = FCConvert.ToString(0);
			fldTotals4.Text = FCConvert.ToString(0);
			fldTotals5.Text = FCConvert.ToString(0);
			fldTotals6.Text = FCConvert.ToString(0);
			fldTotals7.Text = FCConvert.ToString(0);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intType != 97)
			{
				FindNextFullRecord();
			}
			frmWait.InstancePtr.IncrementProgress();

            //FC:FINAL:IPI - #1626 - the subreport should not be created in Report_FetchData; instead create it in Detail_Format 
            if (setReport)
            {
                if (isARreport)
                {
                    sarTypeDetailOb.Report = new sarTypeDetailAR();
                    sarTypeDetailOb.Report.UserData = Strings.Format(intSubType, "000");
                }
                else
                {
                    sarTypeDetailOb.Report = new sarTypeDetail();
                    sarTypeDetailOb.Report.UserData = Strings.Format(intType, "000") + "-" + Strings.Format(lngResCode, "00");
                }
            }
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strSQL = "";
			string strArchiveTable = "";
			if (boolUsePrevYears)
			{
				strArchiveTable = "LastYearArchive";
			}
			else
			{
				strArchiveTable = "Archive";
			}
			if (boolNoRecords == false)
			{
				// DJW 5/26/2009 Changed intOldType to intType in if statement
				if (intOldType != 97)
				{
					if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
					{
						strSQL = "SELECT COUNT(*) AS ReceiptCount, Sum(Amount1) as Total1, Sum(Amount2) as Total2, Sum(Amount3) as Total3, Sum(Amount4) as Total4, Sum(Amount5) as Total5, Sum(Amount6) as Total6 FROM " + strArchiveTable + " WHERE ReceiptType = " + FCConvert.ToString(intOldType) + " AND TownKey = " + FCConvert.ToString(lngOldResCode) + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
					}
					else
					{
						strSQL = "SELECT COUNT(*) AS ReceiptCount, Sum(Amount1) as Total1, Sum(Amount2) as Total2, Sum(Amount3) as Total3, Sum(Amount4) as Total4, Sum(Amount5) as Total5, Sum(Amount6) as Total6 FROM " + strArchiveTable + " WHERE ReceiptType = " + FCConvert.ToString(intOldType) + " AND TownKey = " + FCConvert.ToString(lngOldResCode) + " AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
					}
					rsTypeTotals.OpenRecordset(strSQL);
					rsTemp.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(intOldType));
					// TODO Get_Fields: Field [ReceiptCount] not found!! (maybe it is an alias?)
					lblTitleTotal.Text = rsTemp.Get_Fields_String("TypeTitle") + " - (" + FCConvert.ToString(rsTypeTotals.Get_Fields("ReceiptCount")) + ")";
				}
				else
				{
					// kgk 03-12-11 trocr-279  changed all intSubType to intOldSubType in this Else section
					if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
					{
						strSQL = "SELECT COUNT(*) AS ReceiptCount, Sum(Amount1) as Total1, Sum(Amount2) as Total2, Sum(Amount3) as Total3, Sum(Amount4) as Total4, Sum(Amount5) as Total5, Sum(Amount6) as Total6 FROM " + strArchiveTable + " WHERE ReceiptType = 97 AND ARBillType = " + FCConvert.ToString(intOldSubType) + " AND TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
					}
					else
					{
						strSQL = "SELECT COUNT(*) AS ReceiptCount, Sum(Amount1) as Total1, Sum(Amount2) as Total2, Sum(Amount3) as Total3, Sum(Amount4) as Total4, Sum(Amount5) as Total5, Sum(Amount6) as Total6 FROM " + strArchiveTable + " WHERE ReceiptType = 97 AND ARBillType = " + FCConvert.ToString(intOldSubType) + " AND " + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + " AND (AffectCash = 1 OR AffectCashDrawer = 1)";
					}
					rsTypeTotals.OpenRecordset(strSQL);
					if (intOldSubType != 0)
					{
						rsTemp.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(intOldSubType), "TWAR0000.vb1");
						// TODO Get_Fields: Field [ReceiptCount] not found!! (maybe it is an alias?)
						lblTitleTotal.Text = "A/R (" + rsTemp.Get_Fields_String("TypeTitle") + ") - (" + FCConvert.ToString(rsTypeTotals.Get_Fields("ReceiptCount")) + ")";
					}
					else
					{
						// TODO Get_Fields: Field [ReceiptCount] not found!! (maybe it is an alias?)
						lblTitleTotal.Text = "A/R (PRE PAYMENT) - (" + FCConvert.ToString(rsTypeTotals.Get_Fields("ReceiptCount")) + ")";
					}
				}
				// fldConvenienceFeeTotal.Text = Format(rsTypeTotals.Fields("ConvenienceFeeTotal"), "#,##0.00")
				// TODO Get_Fields: Field [Total1] not found!! (maybe it is an alias?)
				fldTotals1.Text = Strings.Format(rsTypeTotals.Get_Fields("Total1"), "#,##0.00");
				// TODO Get_Fields: Field [Total2] not found!! (maybe it is an alias?)
				fldTotals2.Text = Strings.Format(rsTypeTotals.Get_Fields("Total2"), "#,##0.00");
				// TODO Get_Fields: Field [Total3] not found!! (maybe it is an alias?)
				fldTotals3.Text = Strings.Format(rsTypeTotals.Get_Fields("Total3"), "#,##0.00");
				// TODO Get_Fields: Field [Total4] not found!! (maybe it is an alias?)
				fldTotals4.Text = Strings.Format(rsTypeTotals.Get_Fields("Total4"), "#,##0.00");
				// TODO Get_Fields: Field [Total5] not found!! (maybe it is an alias?)
				fldTotals5.Text = Strings.Format(rsTypeTotals.Get_Fields("Total5"), "#,##0.00");
				// TODO Get_Fields: Field [Total6] not found!! (maybe it is an alias?)
				fldTotals6.Text = Strings.Format(rsTypeTotals.Get_Fields("Total6"), "#,##0.00");
				if (Strings.Trim(fldTotals1.Text) == "")
				{
					fldTotals1.Text = "0.00";
				}
				if (Strings.Trim(fldTotals2.Text) == "")
				{
					fldTotals2.Text = "0.00";
				}
				if (Strings.Trim(fldTotals3.Text) == "")
				{
					fldTotals3.Text = "0.00";
				}
				if (Strings.Trim(fldTotals4.Text) == "")
				{
					fldTotals4.Text = "0.00";
				}
				if (Strings.Trim(fldTotals5.Text) == "")
				{
					fldTotals5.Text = "0.00";
				}
				if (Strings.Trim(fldTotals6.Text) == "")
				{
					fldTotals6.Text = "0.00";
				}
				// If Trim(fldConvenienceFeeTotal.Text) = "" Then
				// fldConvenienceFeeTotal.Text = "0.00"
				// End If
				// TODO Get_Fields: Field [Total1] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total2] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total3] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total4] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total5] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [Total6] not found!! (maybe it is an alias?)
				fldTotals7.Text = Strings.Format(rsTypeTotals.Get_Fields("Total1") + rsTypeTotals.Get_Fields("Total2") + rsTypeTotals.Get_Fields("Total3") + rsTypeTotals.Get_Fields("Total4") + rsTypeTotals.Get_Fields("Total5") + rsTypeTotals.Get_Fields("Total6"), "#,##0.00");
				if (Strings.Trim(fldTotals7.Text) == "")
				{
					fldTotals7.Text = "0.00";
				}
				Line2.Visible = true;
			}
		}

		private void SetupForm()
		{
			bool boolLand = false;
			if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
			{
				boolLand = FCConvert.CBool(arDailyTellerAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			else
			{
				boolLand = FCConvert.CBool(arDailyAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			// If gstrEPaymentPortal <> "P" Then
			lblConvenienceFeeTitle.Visible = false;
			fldConvenienceFeeTotal.Visible = false;
			// Else
			// lblConvenienceFeeTitle.Visible = True
			// fldConvenienceFeeTotal.Visible = True
			// End If
			if (!boolLand)
			{
				// portrait
				lblName.Width = 3500 / 1440f;
				lblAcct.Width = 720 / 1440f;
				lblBill.Width = 540 / 1440f;
				lblReceipt.Width = 930 / 1440f;
				lblDate.Width = 1530 / 1440f;
				lblTeller.Width = 540 / 1440f;
				lblNC.Width = 720 / 1440f;
				lblPayType.Width = 630 / 1440f;
				lblCode.Width = 630 / 1440f;
				lblTitle1.Width = 1390 / 1440f;
				lblTitle2.Width = 1390 / 1440f;
				lblTitle3.Width = 1390 / 1440f;
				lblTitle4.Width = 1390 / 1440f;
				lblTitle5.Width = 1390 / 1440f;
				lblTitle6.Width = 1390 / 1440f;
				lblTotal.Width = 1440 / 1440f;
				fldTotals1.Width = 1390 / 1440f;
				fldTotals2.Width = 1390 / 1440f;
				fldTotals3.Width = 1390 / 1440f;
				fldTotals4.Width = 1390 / 1440f;
				fldTotals5.Width = 1390 / 1440f;
				fldTotals6.Width = 1390 / 1440f;
				fldTotals7.Width = 1440 / 1440f;
				lblTitleTotal.Width = 4230 / 1440f;
				sarTypeDetailOb.Width = 10800 / 1440f;
				this.PrintWidth = 10800 / 1440f;
				// Line1.X2 = 10800
				Line2.X2 = 10800 / 1440f;
				lblName.Left = 0;
				lblAcct.Left = 720 / 1440f;
				lblBill.Left = 1440 / 1440f;
				lblReceipt.Left = 1980 / 1440f;
				lblDate.Left = 3500 / 1440f;
				lblTeller.Left = 2910 / 1440f;
				lblNC.Left = 3360 / 1440f;
				lblPayType.Left = 90 / 1440f;
				lblCode.Left = 3990 / 1440f;
				lblConvenienceFeeTitle.Left = 4040 / 1440f;
				lblTitle1.Left = 4620 / 1440f;
				lblTitle2.Left = 5560 / 1440f;
				lblTitle3.Left = 6320 / 1440f;
				lblTitle4.Left = 7080 / 1440f;
				lblTitle5.Left = 7840 / 1440f;
				lblTitle6.Left = 8600 / 1440f;
				lblTotal.Left = 9360 / 1440f;
				fldConvenienceFeeTotal.Left = 4040 / 1440f;
				fldTotals1.Left = 4620 / 1440f;
				fldTotals2.Left = 5560 / 1440f;
				fldTotals3.Left = 6320 / 1440f;
				fldTotals4.Left = 7080 / 1440f;
				fldTotals5.Left = 7840 / 1440f;
				fldTotals6.Left = 8600 / 1440f;
				fldTotals7.Left = 9360 / 1440f;
				fldTotals2.Top = fldTotals1.Top + 270 / 1440f;
				fldTotals4.Top = fldTotals2.Top;
				fldTotals6.Top = fldTotals2.Top;
				fldConvenienceFeeTotal.Top = fldTotals2.Top;
				lblTitle2.Top = lblTitle1.Top + 270 / 1440f;
				lblTitle4.Top = lblTitle2.Top;
				lblTitle6.Top = lblTitle2.Top;
				lblConvenienceFeeTitle.Top = lblTitle2.Top;
				this.GroupHeader1.Height = lblTitle2.Top + lblTitle2.Height;
			}
			else
			{
				// landscape
				lblName.Width = 3500 / 1440f;
				lblAcct.Width = 720 / 1440f;
				lblBill.Width = 540 / 1440f;
				lblReceipt.Width = 930 / 1440f;
				lblDate.Width = 1530 / 1440f;
				lblTeller.Width = 540 / 1440f;
				lblNC.Width = 720 / 1440f;
				lblPayType.Width = 630 / 1440f;
				lblCode.Width = 630 / 1440f;
				lblTitle1.Width = 1320 / 1440f;
				lblTitle2.Width = 1320 / 1440f;
				lblTitle3.Width = 1320 / 1440f;
				lblTitle4.Width = 1320 / 1440f;
				lblTitle5.Width = 1320 / 1440f;
				lblTitle6.Width = 1320 / 1440f;
				lblTotal.Width = 1370 / 1440f;
				lblConvenienceFeeTitle.Width = 1200 / 1440f;
				fldTotals1.Width = 1320 / 1440f;
				fldTotals2.Width = 1320 / 1440f;
				fldTotals3.Width = 1320 / 1440f;
				fldTotals4.Width = 1320 / 1440f;
				fldTotals5.Width = 1320 / 1440f;
				fldTotals6.Width = 1320 / 1440f;
				fldConvenienceFeeTotal.Width = 1200 / 1440f;
				fldTotals7.Width = 1370 / 1440f;
				this.PrintWidth = 15120 / 1440f;
				lblTitleTotal.Width = 4230 / 1440f;
				sarTypeDetailOb.Width = 15120 / 1440f;
				lblName.Left = 0;
				lblAcct.Left = 720 / 1440f;
				lblBill.Left = 1440 / 1440f;
				lblReceipt.Left = 1980 / 1440f;
				lblDate.Left = 3500 / 1440f;
				lblTeller.Left = 2910 / 1440f;
				lblNC.Left = 3360 / 1440f;
				lblPayType.Left = 90 / 1440f;
				lblCode.Left = 3990 / 1440f;
				lblConvenienceFeeTitle.Left = 4620 / 1440f;
				lblTitle1.Left = 5820 / 1440f;
				lblTitle2.Left = 7140 / 1440f;
				lblTitle3.Left = 8460 / 1440f;
				lblTitle4.Left = 9780 / 1440f;
				lblTitle5.Left = 11100 / 1440f;
				lblTitle6.Left = 12420 / 1440f;
				lblTotal.Left = 13740 / 1440f;
				fldConvenienceFeeTotal.Left = 4620 / 1440f;
				fldTotals1.Left = 5820 / 1440f;
				fldTotals2.Left = 7140 / 1440f;
				fldTotals3.Left = 8460 / 1440f;
				fldTotals4.Left = 9780 / 1440f;
				fldTotals5.Left = 11100 / 1440f;
				fldTotals6.Left = 12420 / 1440f;
				fldTotals7.Left = 13740 / 1440f;
				lblTitleTotal.Left = 0;
				lblTypeHeader.Left = 720 / 1440f;
				Line2.X2 = 15120 / 1440f;
				fldTotals2.Top = fldTotals1.Top;
				fldTotals4.Top = fldTotals2.Top;
				fldTotals6.Top = fldTotals2.Top;
				fldConvenienceFeeTotal.Top = fldTotals2.Top;
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (frmDailyReceiptAudit.InstancePtr.boolNonEntry)
			{
				lblFooter.Left = 0;
				frmDailyReceiptAudit.InstancePtr.boolNonEntry = false;
			}
			else
			{
				lblFooter.Visible = false;
				ReportFooter.Height = 0;
			}
		}

		private void sarTypes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarTypes.Caption	= "Daily Receipt Audit";
			//sarTypes.Icon	= "arReceiptAudit.dsx":0000";
			//sarTypes.Left	= 0;
			//sarTypes.Top	= 0;
			//sarTypes.Width	= 11880;
			//sarTypes.Height	= 8595;
			//sarTypes.StartUpPosition	= 3;
			//sarTypes.SectionData	= "arReceiptAudit.dsx":058A;
			//End Unmaped Properties
		}
	}
}
