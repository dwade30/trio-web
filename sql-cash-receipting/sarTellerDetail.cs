﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for sarTellerDetail.
	/// </summary>
	public partial class sarTellerDetail : FCSectionReport
	{
		public static sarTellerDetail InstancePtr
		{
			get
			{
				return (sarTellerDetail)Sys.GetInstance(typeof(sarTellerDetail));
			}
		}

		protected sarTellerDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTellerDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/01/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/09/2005              *
		// ********************************************************
		string strSQL = "";
		clsDRWrapper rsData = new clsDRWrapper();

		public sarTellerDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarTellerDetail_ReportEnd;
		}

        private void SarTellerDetail_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				BindFields();
				rsData.MoveNext();
				eArgs.EOF = false;
			}
			else
			{
				ClearFields();
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strOrderBy = "";
			SetupForm();
			if (modGlobal.Statics.gboolDailyAuditByAlpha)
			{
				strOrderBy = " ORDER BY Name, Receipt.ReceiptNumber, ID asc";
			}
			else
			{
				strOrderBy = " ORDER BY Receipt.ReceiptNumber, ID asc, Name";
			}
			if (FCConvert.ToString(this.UserData) != "")
			{
				strSQL = "SELECT * FROM Receipt INNER JOIN Archive ON Receipt.ReceiptKey = Archive.ReceiptNumber WHERE Archive.TellerID = '" + this.UserData + "' AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOrderBy;
				// " ORDER BY Receipt.ReceiptNumber"
			}
			else
			{
				strSQL = "SELECT * FROM Receipt INNER JOIN Archive ON Receipt.ReceiptKey = Archive.ReceiptNumber WHERE Archive.TellerID = '" + frmDailyReceiptAudit.InstancePtr.txtTellerID.Text + "' AND" + frmDailyReceiptAudit.InstancePtr.strCloseOutSQL + strOrderBy;
				// " ORDER BY Receipt.ReceiptNumber"
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strCRDatabase);
		}

		private void BindFields()
		{
			string strTemp = "";
			// this will fill the data into the fields
			fldName.Text = rsData.Get_Fields_String("Name");
			//FC:FINAL:MSH - The same as in issue #706: system can't implicitly convert int to string
			//fldAcct.Text = rsData.Get_Fields("AccountNumber");
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
			int vbPorterVar = rsData.Get_Fields_Int32("ReceiptType");
			if (vbPorterVar >= 90 && vbPorterVar <= 96)
			{
				fldBillNumber.Text = rsData.Get_Fields_String("Control1") + rsData.Get_Fields_String("Control2");
			}
			// TODO Get_Fields: Field [Receipt.ReceiptNumber] not found!! (maybe it is an alias?)
			fldReceiptNumber.Text = FCConvert.ToString(rsData.Get_Fields("Receipt.ReceiptNumber"));
			fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("ReceiptDate"), "MM/dd/yy");
			fldTeller.Text = rsData.Get_Fields_String("TellerID");
			// Cash Drawer/Affect Cash
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AffectCash")))
			{
				strTemp = "AC/";
			}
			else
			{
				strTemp = "NC/";
			}
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("AffectCashDrawer")))
			{
				strTemp += "C";
			}
			else
			{
				strTemp += "N";
			}
			if (strTemp == "NC/N")
			{
				Detail.Height = 450 / 1440F;
				lblAccountNumber.Visible = true;
				fldAccountNumber.Visible = true;
				fldAccountNumber.Text = rsData.Get_Fields_String("DefaultAccount");
			}
			else
			{
				Detail.Height = 270 / 1440F;
				lblAccountNumber.Visible = false;
				lblAccountNumber.Top = 0;
				fldAccountNumber.Visible = false;
				fldAccountNumber.Top = 0;
			}
			fldAffect.Text = strTemp;
			// PayType
			// rsRec.FindFirstRecord "ReceiptKey", rsData.Fields("ReceiptNumber")
			// If rsRec.EndOfFile <> True And rsRec.BeginningOfFile <> True Then
			if (rsData.Get_Fields_Double("CashAmount") != 0)
			{
				if (rsData.Get_Fields_Double("CardAmount") != 0)
				{
					if (rsData.Get_Fields_Double("CheckAmount") != 0)
					{
						strTemp = "CCC";
					}
					else
					{
						strTemp = "CS/CC";
					}
				}
				else
				{
					if (rsData.Get_Fields_Double("CheckAmount") != 0)
					{
						strTemp = "CS/CK";
					}
					else
					{
						strTemp = "CS";
					}
				}
			}
			else
			{
				if (rsData.Get_Fields_Double("CardAmount") != 0)
				{
					if (rsData.Get_Fields_Double("CheckAmount") != 0)
					{
						strTemp = "CC/CK";
					}
					else
					{
						strTemp = "CC";
					}
				}
				else
				{
					if (rsData.Get_Fields_Double("CheckAmount") != 0)
					{
						strTemp = "CK";
					}
					else
					{
						strTemp = "";
					}
				}
			}
			// End If
			fldPaymentType.Text = strTemp;
			fldCode.Text = rsData.Get_Fields_String("CollectionCode");
			// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
			fldTotals1.Text = Strings.Format(rsData.Get_Fields("Amount1"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
			fldTotals2.Text = Strings.Format(rsData.Get_Fields("Amount2"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
			fldTotals3.Text = Strings.Format(rsData.Get_Fields("Amount3"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
			fldTotals4.Text = Strings.Format(rsData.Get_Fields("Amount4"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
			fldTotals5.Text = Strings.Format(rsData.Get_Fields("Amount5"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
			fldTotals6.Text = Strings.Format(rsData.Get_Fields("Amount6"), "#,##0.00");
			// fldConvenienceFeeTotal.Text = Format(.Fields("Archive.ConvenienceFee"), "#,##0.00")
			// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
			fldTotals7.Text = Strings.Format(rsData.Get_Fields("Amount1") + rsData.Get_Fields("Amount2") + rsData.Get_Fields("Amount3") + rsData.Get_Fields("Amount4") + rsData.Get_Fields("Amount5") + rsData.Get_Fields("Amount6"), "#,##0.00");
		}
		// Private Sub Detail_Format()
		// If rsData.EndOfFile <> True Then
		// BindFields
		// Else
		// ClearFields
		// End If
		// End Sub
		private void ClearFields()
		{
			// this will clear all the fields just in case...so that no bad information is shown
			fldAccountNumber.Text = "";
			fldAcct.Text = "";
			fldAffect.Text = "";
			fldBillNumber.Text = "";
			fldCode.Text = "";
			fldDate.Text = "";
			fldTotals1.Text = "";
			fldTotals2.Text = "";
			fldTotals3.Text = "";
			fldTotals4.Text = "";
			fldTotals5.Text = "";
			fldTotals6.Text = "";
			fldConvenienceFeeTotal.Text = "";
			fldName.Text = "";
			fldPaymentType.Text = "";
			fldReceiptNumber.Text = "";
			fldTeller.Text = "";
			fldTotals7.Text = "";
		}
		// Private Sub SetupForm()
		// Dim boolLand As Boolean
		//
		// If frmDailyReceiptAudit.boolTellerOnly Then
		// boolLand = CBool(arDailyTellerAuditReport.Printer.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape)
		// Else
		// boolLand = CBool(arDailyAuditReport.Printer.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape)
		// End If
		//
		// If Not boolLand Then   'normal
		// Line1.X2 = 10800
		// fldName.Width = 1080
		// fldAcct.Width = 630
		// fldBillNumber.Width = 450
		// fldReceiptNumber.Width = 540
		// fldDate.Width = 630
		// fldTeller.Width = 450
		// fldAffect.Width = 400
		// fldPaymentType.Width = 540
		// fldCode.Width = 450
		// fldFee1.Width = 800
		// fldFee2.Width = 800
		// fldFee3.Width = 800
		// fldFee4.Width = 800
		// fldFee5.Width = 800
		// fldFee6.Width = 800
		// fldTotal.Width = 810
		// Me.PrintWidth = 10800
		// Else                'landscape
		// Line1.X2 = 14400
		// fldName.Width = 1440
		// fldAcct.Width = 720
		// fldBillNumber.Width = 540
		// fldReceiptNumber.Width = 630
		// fldDate.Width = 720
		// fldTeller.Width = 540
		// fldAffect.Width = 450
		// fldPaymentType.Width = 630
		// fldCode.Width = 540
		// fldFee1.Width = 1170
		// fldFee2.Width = 1170
		// fldFee3.Width = 1170
		// fldFee4.Width = 1170
		// fldFee5.Width = 1170
		// fldFee6.Width = 1170
		// fldTotal.Width = 1170
		// Me.PrintWidth = 14400
		// End If
		//
		// fldAcct.Left = fldName.Width
		// fldBillNumber.Left = fldAcct.Left + fldAcct.Width
		// fldReceiptNumber.Left = fldBillNumber.Left + fldBillNumber.Width
		// fldDate.Left = fldReceiptNumber.Left + fldReceiptNumber.Width
		// fldTeller.Left = fldDate.Left + fldDate.Width
		// fldAffect.Left = fldTeller.Left + fldTeller.Width
		// fldPaymentType.Left = fldAffect.Left + fldAffect.Width
		// fldCode.Left = fldPaymentType.Left + fldPaymentType.Width
		// fldFee1.Left = fldCode.Left + fldCode.Width
		// fldFee2.Left = fldFee1.Left + fldFee1.Width
		// fldFee3.Left = fldFee2.Left + fldFee2.Width
		// fldFee4.Left = fldFee3.Left + fldFee3.Width
		// fldFee5.Left = fldFee4.Left + fldFee4.Width
		// fldFee6.Left = fldFee5.Left + fldFee5.Width
		// fldTotal.Left = fldFee6.Left + fldFee6.Width
		//
		// lblTitle1.Left = fldFee1.Left
		// lblTitle2.Left = fldFee2.Left
		// lblTitle3.Left = fldFee3.Left
		// lblTitle4.Left = fldFee4.Left
		// lblTitle5.Left = fldFee5.Left
		// lblTitle6.Left = fldFee6.Left
		// lblTotal.Left = fldTotal.Left
		// End Sub
		private void SetupForm()
		{
			bool boolLand = false;
			if (frmDailyReceiptAudit.InstancePtr.boolTellerOnly)
			{
				boolLand = FCConvert.CBool(arDailyTellerAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			else
			{
				boolLand = FCConvert.CBool(arDailyAuditReport.InstancePtr.PageSettings.Orientation == GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape);
			}
			// If gstrEPaymentPortal = "P" Then
			// lblConvenienceFeeTitle.Visible = True
			// fldConvenienceFeeTotal.Visible = True
			// Else
			lblConvenienceFeeTitle.Visible = false;
			fldConvenienceFeeTotal.Visible = false;
			// End If
			if (!boolLand)
			{
				// portrait
				lblName.Width = 3500 / 1440f;
				lblAcct.Width = 720 / 1440f;
				lblBill.Width = 540 / 1440f;
				fldBillNumber.Width = 540 / 1440f;
				lblReceipt.Width = 630 / 1440f;
				fldReceiptNumber.Width = 630 / 1440f;
				lblDate.Width = 1530 / 1440f;
				lblTeller.Width = 540 / 1440f;
				lblNC.Width = 720 / 1440f;
				fldAffect.Width = 720 / 1440f;
				lblPayType.Width = 630 / 1440f;
				fldPaymentType.Width = 630 / 1440f;
				lblCode.Width = 630 / 1440f;
				fldCode.Width = 630 / 1440f;
				lblTitle1.Width = 1440 / 1440f;
				lblTitle2.Width = 1440 / 1440f;
				lblTitle3.Width = 1440 / 1440f;
				lblTitle4.Width = 1440 / 1440f;
				lblTitle5.Width = 1440 / 1440f;
				lblTitle6.Width = 1440 / 1440f;
				lblTotal.Width = 1440 / 1440f;
				lblConvenienceFeeTitle.Width = 1440 / 1440f;
				fldTotals1.Width = 1440 / 1440f;
				fldTotals2.Width = 1440 / 1440f;
				fldTotals3.Width = 1440 / 1440f;
				fldTotals4.Width = 1440 / 1440f;
				fldTotals5.Width = 1440 / 1440f;
				fldTotals6.Width = 1440 / 1440f;
				fldTotals7.Width = 1440 / 1440f;
				fldConvenienceFeeTotal.Width = 1440 / 1440f;
				// lblTitleTotal.Width = 2620
				// sarTypeDetailOb.Width = 10800
				this.PrintWidth = 10800 / 1440f;
				Line1.X2 = 10800 / 1440f;
				// Line2.X2 = 10800
				lblName.Left = 0;
				lblAcct.Left = 720 / 1440f;
				lblBill.Left = 1440 / 1440f;
				fldBillNumber.Left = 1440 / 1440f;
				lblReceipt.Left = 1980 / 1440f;
				fldReceiptNumber.Left = 1980 / 1440f;
				lblDate.Left = 3500 / 1440f;
				lblTeller.Left = 2610 / 1440f;
				fldTeller.Left = 2610 / 1440f;
				lblNC.Left = 3060 / 1440f;
				fldAffect.Left = 3060 / 1440f;
				lblPayType.Left = 90 / 1440f;
				fldPaymentType.Left = 90 / 1440f;
				lblCode.Left = 3690 / 1440f;
				fldCode.Left = 3690 / 1440f;
				lblConvenienceFeeTitle.Left = 3690 / 1440f;
				lblTitle1.Left = 4320 / 1440f;
				lblTitle2.Left = 5310 / 1440f;
				lblTitle3.Left = 6120 / 1440f;
				lblTitle4.Left = 6930 / 1440f;
				lblTitle5.Left = 7740 / 1440f;
				lblTitle6.Left = 8550 / 1440f;
				lblTotal.Left = 9360 / 1440f;
				fldConvenienceFeeTotal.Left = 3690 / 1440f;
				fldTotals1.Left = 4320 / 1440f;
				fldTotals2.Left = 5310 / 1440f;
				fldTotals3.Left = 6120 / 1440f;
				fldTotals4.Left = 6930 / 1440f;
				fldTotals5.Left = 7740 / 1440f;
				fldTotals6.Left = 8550 / 1440f;
				fldTotals7.Left = 9360 / 1440f;
				fldTotals2.Top = fldTotals1.Top + 270 / 1440f;
				fldTotals4.Top = fldTotals2.Top;
				fldTotals6.Top = fldTotals2.Top;
				fldConvenienceFeeTotal.Top = fldTotals2.Top;
				lblTitle2.Top = lblTitle1.Top + 270 / 1440f;
				lblTitle4.Top = lblTitle2.Top;
				lblTitle6.Top = lblTitle2.Top;
				lblConvenienceFeeTitle.Top = lblTitle2.Top;
				this.GroupHeader1.Height = lblTitle2.Top + lblTitle2.Height;
			}
			else
			{
				// landscape
				lblName.Width = 3500 / 1440f;
				lblAcct.Width = 720 / 1440f;
				lblBill.Width = 540 / 1440f;
				lblReceipt.Width = 630 / 1440f;
				lblDate.Width = 1530 / 1440f;
				lblTeller.Width = 540 / 1440f;
				lblNC.Width = 720 / 1440f;
				lblPayType.Width = 630 / 1440f;
				lblCode.Width = 630 / 1440f;
				lblTitle1.Width = 1440 / 1440f;
				lblTitle2.Width = 1440 / 1440f;
				lblTitle3.Width = 1440 / 1440f;
				lblTitle4.Width = 1440 / 1440f;
				lblTitle5.Width = 1440 / 1440f;
				lblTitle6.Width = 1440 / 1440f;
				lblTotal.Width = 1440 / 1440f;
				lblConvenienceFeeTitle.Width = 1200 / 1440f;
				fldTotals1.Width = 1440 / 1440f;
				fldTotals2.Width = 1440 / 1440f;
				fldTotals3.Width = 1440 / 1440f;
				fldTotals4.Width = 1440 / 1440f;
				fldTotals5.Width = 1440 / 1440f;
				fldTotals6.Width = 1440 / 1440f;
				fldTotals7.Width = 1440 / 1440f;
				fldConvenienceFeeTotal.Width = 1200 / 1440f;
				// lblTitleTotal.Width = 3630
				// sarTypeDetailOb.Width = 14400
				this.PrintWidth = 15120 / 1440f;
				lblName.Left = 0;
				fldName.Left = 0;
				lblAcct.Left = 720 / 1440f;
				// fldAccountNumber.Left = 720
				lblBill.Left = 1440 / 1440f;
				fldBillNumber.Left = 1440 / 1440f;
				lblReceipt.Left = 1980 / 1440f;
				fldReceiptNumber.Left = 1980 / 1440f;
				lblDate.Left = 3500 / 1440f;
				fldDate.Left = 3500 / 1440f;
				lblTeller.Left = 2610 / 1440f;
				fldTeller.Left = 2610 / 1440f;
				lblNC.Left = 3060 / 1440f;
				fldAffect.Left = 3060 / 1440f;
				lblPayType.Left = 90 / 1440f;
				fldPaymentType.Left = 90 / 1440f;
				lblCode.Left = 3690 / 1440f;
				fldCode.Left = 3690 / 1440f;
				lblConvenienceFeeTitle.Left = 3690 / 1440f;
				lblTitle1.Left = 4890 / 1440f;
				lblTitle2.Left = 6330 / 1440f;
				lblTitle3.Left = 7770 / 1440f;
				lblTitle4.Left = 9210 / 1440f;
				lblTitle5.Left = 10650 / 1440f;
				lblTitle6.Left = 12090 / 1440f;
				lblTotal.Left = 13530 / 1440f;
				fldConvenienceFeeTotal.Left = 3690 / 1440f;
				fldTotals1.Left = 4890 / 1440f;
				fldTotals2.Left = 6330 / 1440f;
				fldTotals3.Left = 7770 / 1440f;
				fldTotals4.Left = 9210 / 1440f;
				fldTotals5.Left = 10650 / 1440f;
				fldTotals6.Left = 12090 / 1440f;
				fldTotals7.Left = 13530 / 1440f;
				// lblTitleTotal.Left = 400
				// lblTypeHeader.Left = 720
				// 
				Line1.X2 = 15120 / 1440f;
				fldTotals2.Top = fldTotals1.Top;
				fldTotals4.Top = fldTotals2.Top;
				fldTotals6.Top = fldTotals2.Top;
				fldConvenienceFeeTotal.Top = fldTotals2.Top;
			}
		}

		private void sarTellerDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarTellerDetail.Caption	= "Teller Detail";
			//sarTellerDetail.Icon	= "sarTellerDetail.dsx":0000";
			//sarTellerDetail.Left	= 0;
			//sarTellerDetail.Top	= 0;
			//sarTellerDetail.Width	= 11850;
			//sarTellerDetail.Height	= 8010;
			//sarTellerDetail.StartUpPosition	= 3;
			//sarTellerDetail.SectionData	= "sarTellerDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
