﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using GrapeCity.ActiveReports;
using System.Linq;
using SharedApplication.Extensions;
using TWSharedLibrary;
using SharedApplication.Enums;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmReceiptInput.
	/// </summary>
	public partial class frmReceiptInput : BaseForm
	{
		public frmReceiptInput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtTitle = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.lblTitle = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.txtTitle.AddControlArrayElement(txtTitle_2, FCConvert.ToInt16(2));
			this.txtTitle.AddControlArrayElement(txtTitle_1, FCConvert.ToInt16(1));
			this.txtTitle.AddControlArrayElement(txtTitle_0, FCConvert.ToInt16(0));
			this.txtTitle.AddControlArrayElement(txtTitle_3, FCConvert.ToInt16(3));
			this.lblTitle.AddControlArrayElement(lblTitle_2, FCConvert.ToInt16(2));
			this.lblTitle.AddControlArrayElement(lblTitle_1, FCConvert.ToInt16(1));
			this.lblTitle.AddControlArrayElement(lblTitle_0, FCConvert.ToInt16(0));
			this.lblTitle.AddControlArrayElement(lblTitle_3, FCConvert.ToInt16(3));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReceiptInput InstancePtr
		{
			get
			{
				return (frmReceiptInput)Sys.GetInstance(typeof(frmReceiptInput));
			}
		}

		protected frmReceiptInput _InstancePtr = null;

		bool boolLoaded;
		// True if form has been loaded already, False otherwise
		bool boolLoad;
		// True if this form should load
		string strTellerID = "";
		// string the the teller input to identify who is using the system
		int[] vsSummaryPointers = new int[10 + 1];
		// this array will hold pointers to the correct column...this makes for easy manipulation of columns
		bool boolBMV;
		string strPassword = "";
		public int intCurFrame;
		// 0 = Teller; 1 = Payment Input; 2 = Summary; 3 = Cash Out
		bool boolQuit;
		int lngCurType;
		// vbPorter upgrade warning: lngQuanity As int	OnWrite(short, string)
		int lngQuanity;
		bool boolDoNotCheckMultiples;
		bool boolAccountBoxClear;
		int lngLastTypeChecked;
		bool boolSetAction;
		// This will be set to True if the user is in Autoaction mode
		string strActionKey = "";
		// (R)eal Estate, (P)ersonal Property, (U)tility Billing, (A)ccounts Recievable
		string strActionCode;
		// This is the info that the user is typing after selecting an autoaction
		bool boolAddingResCodes;
		clsGridAccount clsAcctBox = new clsGridAccount();
		clsGridAccount clsDefaultAcctBox = new clsGridAccount();
		bool boolNoKeystroke;
		string strRefNum = "";
		// Reference Number returned from e-payment processing
		string strReturnedCNum = "";
		// Returned Crypt Card Number
		string strReturnedAuthCode = "";
		// Returned Auth Code
		string strReturnedExpDate = "";
		// Returned Expiration Date of Card returned from CryptPAY
		string strReturnedCryptCard = "";
		string strReturnedZipCode = "";
		string[] strData = null;
		bool blnDebit;
		// If CC Type is Debit Card
		bool blnVISA;
		// If CC Type is VISA
		bool blnIsSale;
		// True if Amount of Receipt is > 0
		int lngReceiptNum;
		// Receipt Number for Voids/Refunds
		// vbPorter upgrade warning: lngMultiAcct As int	OnWrite(string)
		int lngMultiAcct;
		// If Multiple Payments of the Same Type this is the Account chosen to receive the credit
		bool blnIsVoid;
		// Flag to indicate whether refund is a Void
		bool blnIsEPymt;
		// Flag to indicate that payment was made electronically
		bool blnIsSwipe;
		// Flag to indicate that the credit card number was received through card swipe rather than manual entry
		bool boolInValidate;
		// Flag to prevent multiple simultaneous passes through cmbType_Validate  trocr-286
		bool boolShowRoutingNumbers;
		// Flag to show routing numbers in bank list - 11-10-2011 trocr-284
		bool boolMosesTown;
		// Flag for fix to MultiTown / MOSES problem trobl-106
		// vbPorter upgrade warning: strPassTag As string	OnWriteFCConvert.ToInt32(
		public string strPassTag = "";
		public int lngReceiptCreated;
		// True if the receipt has already been created in the Receipt Table
		public string strBatchTellerID = "";
		public int intRAIndex;
		public bool boolUnloadOK;
		public bool boolMulitpleCopy;
		public bool boolShowDetail;
		public double dblRBValue;
		public bool boolReturnBB;
		public bool boolEFTType;
		public int lngMultiTown;
		public bool boolLoadingUTsearch;
		// Public NamePartyID              As Long
		// Public PaidByPartyID            As Long
		const int SWP_NOMOVE = 2;
		const int SWP_NOSIZE = 1;
		const int FLAGS = SWP_NOMOVE | SWP_NOSIZE;
		private string cmbTypeItem = "";
        private const int constBatchTypeCol = 9;
        public string strBatchNumber = "";
        private cSettingsController setCont = new cSettingsController();

		private struct EPmtAcctDistr
		{
			// vbPorter upgrade warning: Amount As double	OnWriteFCConvert.ToDecimal(
			public double Amount;
			public string AcctID;
			public int GridRow;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public EPmtAcctDistr(int unusedParam)
			{
				this.Amount = 0;
				this.AcctID = string.Empty;
				this.GridRow = 0;
			}
		};

		private struct CashOut_Payment
		{
			// vbPorter upgrade warning: Amount As double	OnWrite(short, double, Decimal)
			public double Amount;
			// Amount of Payment
			public short Type;
			// Type of Payment 0 - Cash, 1 - Check, 2 - Card
			public string Bank;
			// Bank Number for Check
			// vbPorter upgrade warning: CType As string	OnWrite(string, int)
			public string CType;
			// Credit Card Type or Check Number
			public string CNum;
			// Credit Card Number (E-Pymt Options) ; Tracker Reference: 16641
			public string CVV2;
			// CVV2/Security Code
			public string ExpDate;
			// Credit Card Expiration Date
			public string ChkAcctNum;
			// Checking Acct Number (E-Checks)
			// vbPorter upgrade warning: ChkAcctType As string	OnWriteFCConvert.ToInt32(
			public string ChkAcctType;
			// Checking Acct Type
			// vbPorter upgrade warning: ChkCheckType As string	OnWriteFCConvert.ToInt32(
			public string ChkCheckType;
			// Check Type
			public string ChkAcctName;
			// Name of Bank Acct Holder
			public string OwnerName;
			// CC Name
			public string OwnerStreet;
			// CC Address
			public string OwnerCity;
			// CC City
			public string OwnerState;
			// CC State
			public string OwnerZip;
			// CC Zip
			public string OwnerCountry;
			// CC Country
			public string RefNumber;
			// Returned Reference Number when Payment is Processed
			public string OrigReceipt;
			// Original Receipt ID for Voids/Refunds
			public string CryptCard;
			// vbPorter upgrade warning: ConvenienceFee As Decimal	OnWrite(Decimal, short)
			public Decimal ConvenienceFee;
			public string AuthCode;
			public bool IsVISA;
			public bool IsDebit;
			// vbPorter upgrade warning: NonTaxAmount As Decimal	OnWrite(Decimal, short)
			public Decimal NonTaxAmount;
			// Only used for VISA transacitons if the CryptPAY interface is enabled
			public string AuthCodeConvenienceFee;
			public string AuthCodeVISANonTax;
			// vbPorter upgrade warning: NonTaxConvenienceFee As Decimal	OnWrite(Decimal, short)
			public Decimal NonTaxConvenienceFee;
			public string AuthCodeNonTaxConvenienceFee;
			public string EPmtAcctID;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CashOut_Payment(int unusedParam)
			{
				this.Amount = 0;
				this.AuthCode = string.Empty;
				this.AuthCodeConvenienceFee = string.Empty;
				this.AuthCodeNonTaxConvenienceFee = string.Empty;
				this.AuthCodeVISANonTax = string.Empty;
				this.Bank = string.Empty;
				this.ChkAcctName = string.Empty;
				this.ChkAcctNum = string.Empty;
				this.ChkAcctType = string.Empty;
				this.ChkCheckType = string.Empty;
				this.CNum = string.Empty;
				this.ConvenienceFee = 0;
				this.CryptCard = string.Empty;
				this.CType = string.Empty;
				this.CVV2 = string.Empty;
				this.EPmtAcctID = string.Empty;
				this.ExpDate = string.Empty;
				this.IsDebit = false;
				this.IsVISA = false;
				this.NonTaxAmount = 0;
				this.NonTaxConvenienceFee = 0;
				this.OrigReceipt = string.Empty;
				this.OwnerCity = string.Empty;
				this.OwnerCountry = string.Empty;
				this.OwnerName = string.Empty;
				this.OwnerState = string.Empty;
				this.OwnerStreet = string.Empty;
				this.OwnerZip = string.Empty;
				this.RefNumber = string.Empty;
				this.Type = 0;
			}
		};

		private CashOut_Payment[] arr = new CashOut_Payment[modStatusPayments.MAX_PAYMENTS + 1];

		private void StartUp()
		{
			// this runs the Input Routine
			// load the multiple combo
			cmbMultiple.Clear();
			cmbMultiple.AddItem("1");
			cmbMultiple.AddItem("2");
			cmbMultiple.AddItem("3");
			cmbMultiple.AddItem("4");
			cmbMultiple.AddItem("5");
			cmbMultiple.AddItem("6");
			cmbMultiple.AddItem("7");
			cmbMultiple.AddItem("8");
			cmbMultiple.AddItem("9");
			FCUtils.EraseSafe(modStatusPayments.Statics.CHGINTYearDate);
			// this will clear the last receipts dates
			if (modGlobal.Statics.gboolPendingRI)
			{
				this.Text = "Receipt Input for Pending Transactions";
			}
			else
			{
				this.Text = "Receipt Input";
			}
			this.HeaderText.Text = this.Text;
			// format the grid
			FormatGrid();
			// fill the main labels
			FillMainLabels();
			// show the frame
			ShowFrame(fraReceipt);
			ShowFrame(fraTotal);
			// set the height of the account box frame
			fraAcct.Height = chkAffectCash.Top + chkAffectCash.Height + 100;
			// MAL@20080729: Reset variable every time form is opened
			// Tracker Reference: 14841
			modGlobalConstants.Statics.gblnMVIsOpen = false;
			lngReceiptNum = 0;
		}

		private void ShowFrame(Control fraFrame)
		{
			// displays a frame
			//fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			//FC:FINAL:DDU:Prevent going below 0
			if (fraFrame.Left < 0)
			{
				fraFrame.Left = 0;
			}
			string vbPorterVar = fraFrame.GetName();
			switch (vbPorterVar)
            {
                case "fraTeller":

                    //fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
                    SetMenuOptions_2(0);
                    // this will set the captions correctly
                    fraBank.Visible = false;
                    fraPaidBy.Visible = false;
                    fraPaymentInfo.Visible = false;
                    fraVoid.Visible = false;
                    fraCreditReceiptNum.Visible = false;

                    break;
                case "fraReceipt":

                    //fraFrame.Top = 0;
                    SetMenuOptions_2(1);
                    // this will set the captions correctly
                    fraBank.Visible = false;
                    fraPaidBy.Visible = false;
                    fraPaymentInfo.Visible = false;
                    fraVoid.Visible = false;
                    fraCreditReceiptNum.Visible = false;

                    break;

                case "fraSummary":
                {
                    // adjust the frame
                    //fraFrame.Top = 0;
                    //FC:FINAL:DDU:#i1049 - fix show of pending transaction
                    if (intCurFrame == 3)
                    {
                        // this is the cash out screen
                        if (this.Height - (fraCashOut.Height * 1.5) - fraPaidBy.Height >= 0)
                        {
                            fraFrame.Height = FCConvert.ToInt32(this.Height - (fraCashOut.Height * 1.5) - fraPaidBy.Height);
                        }
                        if (fraFrame.Height < 200)
                        {
                            fraFrame.Height = 200;
                        }
                        // show the supporting frames
                        ShowFrame(fraPaidBy);
                    }
                    else
                    {
                        if (0.35 * (this.Height - fraPaidBy.Height) > 0)
                        {
                            fraFrame.Height = FCConvert.ToInt32(0.35 * (this.Height - fraPaidBy.Height));
                        }
                        if (fraFrame.Height < 200)
                        {
                            fraFrame.Height = 200;
                        }
                        SetMenuOptions_2(2);
                        // this will set the captions correctly
                        // show the supporting frames
                        ShowFrame(fraPaidBy);
                        ShowFrame(fraTotal);
                    }
                    fraVoid.Visible = false;
                    fraCreditReceiptNum.Visible = false;
                    // set the grid height for the frame height
                    //vsSummary.Height = FCConvert.ToInt32(fraFrame.Height - 40);
                    modUseCR.CalculateSummaryTotal();
                    // this will recalculate the running total
                    break;
                }

                case "fraPaidBy":
                    fraFrame.Left = fraSummary.Left;
                    fraFrame.Top = fraSummary.Top + fraSummary.Height;

                    break;

                case "fraTotal":
                {
                    if (fraReceipt.Visible)
                    {
                        fraFrame.Top = fraReceipt.Top + fraReceipt.Height;
                    }
                    else
                    {
                        fraFrame.Top = fraPaidBy.Top + fraPaidBy.Height;
                        if (fraFrame.Top + fraFrame.Height > this.Height)
                        {
                            fraFrame.Top = this.Height - fraFrame.Height;
                        }
                    }
                    fraBank.Visible = false;
                    fraPaymentInfo.Visible = false;
                    fraVoid.Visible = false;
                    fraCreditReceiptNum.Visible = false;

                    break;
                }

                case "fraCashOut":
                    SetMenuOptions_2(3);
                    // this will set the captions correctly
                    ShowFrame(fraSummary);
                    // this is to resize the summary frame
                    fraPaidBy.Top = fraSummary.Top + fraSummary.Height;
                    fraFrame.Top = FCConvert.ToInt32(fraPaidBy.Top + fraPaidBy.Height);

                    break;
                case "fraBank":
                    fraFrame.Top = txtCashOutCheckNumber.Top;
                    // fraFrame.Left = txtCashOutCheckNumber.Left
                    fraBank.Visible = false;
                    fraSummary.Visible = false;
                    fraPaymentInfo.Visible = false;
                    fraVoid.Visible = false;
                    fraCreditReceiptNum.Visible = false;

                    break;
                case "fraPaymentInfo":
                    fraFrame.Height = vsPaymentInfo.Rows * 28 + 40;
                    vsPaymentInfo.Height = vsPaymentInfo.Rows * 28;
                    fraFrame.CenterToContainer(this);
                    fraBank.Visible = false;
                    fraPaidBy.Visible = false;
                    fraVoid.Visible = false;
                    fraCreditReceiptNum.Visible = false;

                    break;
                case "fraVoid":

                    //FC:FINAL:RPU: #i527 Set this directly in designer and show also fraTotal corectly
                    //fraFrame.Top = FCConvert.ToInt32((this.Height - fraVoid.Height) / 2.0);
                    fraTotal.Top = FCConvert.ToInt32(fraVoid.Height);
                    fraReceipt.Visible = false;
                    fraBank.Visible = false;
                    fraPaidBy.Visible = false;
                    fraPaymentInfo.Visible = false;
                    fraCreditReceiptNum.Visible = false;
                    cmdSummary.Enabled = false;
                    cmdAddReceipt.Enabled = false;

                    break;
                case "fraCreditReceiptNum":
                    fraFrame.Top = FCConvert.ToInt32((this.Height - fraCreditReceiptNum.Height) / 2.0);
                    fraVoid.Visible = false;
                    fraReceipt.Visible = false;
                    fraBank.Visible = false;
                    fraPaidBy.Visible = false;
                    fraPaymentInfo.Visible = false;
                    fraCashOut.Visible = false;
                    fraSummary.Visible = false;
                    cmdSummary.Enabled = false;
                    cmdAddReceipt.Enabled = false;

                    break;
                case "fraMultiAccts":
                    fraFrame.Top = FCConvert.ToInt32((this.Height - fraMultiAccts.Height) / 2.0);
                    fraVoid.Visible = false;
                    fraReceipt.Visible = false;
                    fraBank.Visible = false;
                    fraPaidBy.Visible = false;
                    fraPaymentInfo.Visible = false;
                    fraCashOut.Visible = false;
                    fraSummary.Visible = false;
                    fraCreditReceiptNum.Visible = false;
                    cmdSummary.Enabled = false;
                    cmdAddReceipt.Enabled = false;

                    break;
            }
			fraFrame.Visible = true;
			fraFrame.ZOrder(ZOrderConstants.BringToFront);
		}

		private void FillMainLabels()
		{
			// this sub will fill all of the Labels and Textboxes above the grid on the form
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// loads the combo information for the Type combobox
			FillComboType();
			// loads the info into cmbCopies
			cmbCopies.Clear();
			cmbCopies.AddItem("1");
			cmbCopies.AddItem("2");
			cmbCopies.AddItem("3");
			cmbCopies.AddItem("4");
			chkPrint.CheckState = Wisej.Web.CheckState.Checked;
		}

		private void cboExpMonth_Enter(object sender, System.EventArgs e)
		{
			if (!cboExpMonth.Enabled)
			{
				txtCVV2.Focus();
			}
		}

		private void chkZeroReceipt_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			Decimal curVal;
			if (RestrictedCode(ref lngCurType) || boolEFTType)
			{
				chkZeroReceipt.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void cmbBank_TextChanged(object sender, System.EventArgs e)
		{
			string strRoutingNum = "";
			if (cmbBank.SelectedIndex > 0)
			{
				strRoutingNum = GetBankRoutingNumber(cmbBank.Items[cmbBank.SelectedIndex].ToString());
				if (strRoutingNum != "0" && strRoutingNum != "")
				{
					ToolTip1.SetToolTip(cmbBank, "Routing/Transit #: " + strRoutingNum);
				}
				else
				{
					ToolTip1.SetToolTip(cmbBank, "");
				}
			}
			else
			{
				ToolTip1.SetToolTip(cmbBank, "");
			}
		}

		private void cmbBank_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strRoutingNum = "";
			if (cmbBank.SelectedIndex > 0)
			{
				strRoutingNum = GetBankRoutingNumber(cmbBank.Items[cmbBank.SelectedIndex].ToString());
				if (strRoutingNum != "0" && strRoutingNum != "")
				{
					ToolTip1.SetToolTip(cmbBank, "Routing/Transit #: " + strRoutingNum);
				}
				else
				{
					ToolTip1.SetToolTip(cmbBank, "");
				}
			}
			else
			{
				ToolTip1.SetToolTip(cmbBank, "");
			}
			if (blnIsSale)
			{
				// Do Nothing
			}
			else
			{
				// MAL@20090325: Added check for portal before prompting
				// Tracker Reference: 17890
				// kgk 01-27-2012  Add InforME and Invoice Cloud
				if (lngReceiptNum == 0 && modGlobal.Statics.gstrEPaymentPortal != "N" && modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
				{
					ShowFrame(fraCreditReceiptNum);
				}
			}
		}

		private void cmbBank_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbBank.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, FCConvert.ToInt16(boolShowRoutingNumbers ? 400 : 300), 0);
			// kgk 11-10-2011  300, 0)
		}

		private void cmbBank_Enter(object sender, System.EventArgs e)
		{
			if (cmbBank.Items.Count > 1)
			{
				cmbBank.SelectedIndex = 1;
			}
			else if (cmbBank.Items.Count == 1)
			{
				cmbBank.SelectedIndex = 0;
			}
		}

		private void cmbBank_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbBank.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbBank.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCashOutCC_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// MAL@20090128 ; Tracker Reference: 16641
			clsDRWrapper rsCC = new clsDRWrapper();
			if (blnIsSale)
			{
				// Check if Debit Card Type
				if (cmbCashOutCC.SelectedIndex != -1)
				{
					rsCC.OpenRecordset("SELECT * FROM CCType WHERE ID = " + FCConvert.ToString(cmbCashOutCC.ItemData(cmbCashOutCC.SelectedIndex)), modExtraModules.strCRDatabase);
					if (rsCC.RecordCount() > 0)
					{
						blnDebit = FCConvert.ToBoolean(rsCC.Get_Fields_Boolean("IsDebit"));
						blnVISA = FCConvert.ToBoolean(rsCC.Get_Fields_Boolean("IsVISA"));
					}
					if (!blnDebit)
					{
						// MAL@20081224: Add options for electronic payments
						// Tracker Reference: 16641
						// kgk 01-27-2012  Add InforME Portal   'kgk 09-30-2011  Add Invoice Cloud
						if (modGlobal.Statics.gstrEPaymentPortal != "N" && modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
						{
							fraTransDetailsCC.Visible = true;
							fraTransDetailsCheck.Visible = false;
							if (modGlobal.Statics.gstrEPaymentPortal == "G")
							{
								// Go e-Merchant requires address for validation
								if (frmEPaymentAddress.InstancePtr.Init())
								{
									frmEPaymentAddress.InstancePtr.Show();
								}
								// Validate Return Values
								if (CCAddressValid())
								{
									// Continue
								}
								else
								{
									MessageBox.Show("You cannot select Credit as a transaction type without entering address information", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
									cmbCashOutType.SelectedIndex = 0;
								}
							}
						}
						else
						{
							fraTransDetailsCC.Visible = false;
							fraTransDetailsCheck.Visible = false;
						}
					}
					else
					{
						fraTransDetailsCC.Visible = false;
						fraTransDetailsCheck.Visible = false;
					}
				}
				else
				{
					fraTransDetailsCC.Visible = false;
					fraTransDetailsCheck.Visible = false;
				}
			}
			else
			{
				// kgk 01-27-2012  Add InforME and Invoice Cloud
				if (lngReceiptNum == 0 && modGlobal.Statics.gstrEPaymentPortal != "N" && modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
				{
					ShowFrame(fraCreditReceiptNum);
				}
			}
		}

		private void cmbCashOutCC_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbCashOutCC.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cmbCashOutCC_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbCashOutCC.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbCashOutCC.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCopies_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbCopies.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbCopies.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMultiple_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// this will adjust the values of the fee fields
			int lngRW;
			// vbPorter upgrade warning: dblval As double	OnWrite(string, short, double)
			double dblval = 0;
			if (lngQuanity == 0)
				lngQuanity = 1;
			if (!boolDoNotCheckMultiples)
			{
				// take the old value and divide the fees by it
				if (lngQuanity > 1)
				{
					// no need to divide by 1
					for (lngRW = 1; lngRW <= 6; lngRW++)
					{
						// check all the fees
						if (Strings.InStr(1, vsFees.TextMatrix(lngRW, 2), "%", CompareConstants.vbBinaryCompare) == 0)
						{
							if (Conversion.Val(vsFees.TextMatrix(lngRW, 2)) != 0)
							{
								dblval = FCConvert.ToDouble(vsFees.TextMatrix(lngRW, 2));
							}
							else
							{
								dblval = 0;
							}
							if (dblval != 0)
							{
								dblval /= lngQuanity;
							}
							vsFees.TextMatrix(lngRW, 2, Strings.Format(modGlobal.Round(dblval, 2), "#,##0.00"));
						}
						else
						{
							// do not multiply a percentage
						}
					}
				}
				lngQuanity = FCConvert.ToInt32(cmbMultiple.Items[cmbMultiple.SelectedIndex].ToString());
				if (lngQuanity == 0)
					lngQuanity = 1;
				// take the old value and divide the fees by it
				if (lngQuanity > 1)
				{
					// no need to multiply by 1
					for (lngRW = 1; lngRW <= 6; lngRW++)
					{
						// check all the fees
						if (Strings.InStr(1, vsFees.TextMatrix(lngRW, 2), "%", CompareConstants.vbBinaryCompare) == 0)
						{
							if (Conversion.Val(vsFees.TextMatrix(lngRW, 2)) != 0)
							{
								dblval = FCConvert.ToDouble(vsFees.TextMatrix(lngRW, 2));
							}
							else
							{
								dblval = 0;
							}
							if (dblval != 0)
							{
								dblval *= lngQuanity;
							}
							vsFees.TextMatrix(lngRW, 2, Strings.Format(modGlobal.Round(dblval, 2), "#,##0.00"));
						}
						else
						{
							// do not multiply a percentage
						}
					}
				}
			}
		}

		private void cmbResCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolAddingResCodes && !boolMosesTown)
			{
				// kgk 06-19-2012 trobl-106  Let us reset the town code
				ProcessType_2(true);
			}
		}

		private void cmbResCode_DoubleClick(object sender, System.EventArgs e)
		{
			ProcessType_2(true);
		}

		private void cmbType_ClickEvent(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbType_DblClick(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbType_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (boolSetAction)
			{
				switch (e.KeyCode)
				{
					case Keys.Return:
						{
							UseActionCode();
							break;
						}
					case (Keys)27:
						{
							ResetAutoAction();
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (e.KeyCode)
				{
					case Keys.Return:
						{
							ProcessType();
							// FC:FINAL:VGE - #383 Validation is used to open required tabs (in some other places as well)
							cmbType_Validate(false);
							break;
						}
				}
				//end switch
			}
		}

		private void cmbType_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys keyCode = e.KeyCode;
			if (boolSetAction)
			{
				switch (keyCode)
				{
					case Keys.Return:
						{
							UseActionCode();
							break;
						}
					case Keys.Escape:
						{
							ResetAutoAction();
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (keyCode)
				{
					case Keys.Return:
						{
							//if (cmbType.SelectedIndex != -1)
                            if (cmbType.SelectedIndex != -1)
                            {
								ProcessType();
								cmbType_Validate(false);
								if (boolNoKeystroke)
								{
									keyCode = 0;
									boolNoKeystroke = false;
									return;
								}
							}
							else
							{
								//cmbType.ShowCell(0, 0);
							}
							break;
						}
					case Keys.Space:
						{
							//if (cmbType.EditingControl is ComboBox)
							//{
							//	(cmbType.EditingControl as ComboBox).DroppedDown = true;
							//}
                            cmbType.DroppedDown = true;
							break;
						}
				}
				//end switch
			}
		}

		private void cmbType_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			// FC:FINAL:MW converting works for letters and numbers 
			Keys keyCode = (Keys)char.ToUpper(e.KeyChar);
			if (boolSetAction)
			{
				if (keyCode >= Keys.D0 && keyCode <= Keys.D9)
				{
					strActionKey += FCConvert.ToString(Convert.ToChar(keyCode));
					ShowActionPane();
				}
				keyCode = 0;
			}
			else
			{
				switch (keyCode)
				{
					case Keys.R:
					case Keys.R + 32:
						{
							SetActionCode_2("R");
							break;
						}
					case Keys.P:
					case Keys.P + 32:
						{
							SetActionCode_2("P");
							break;
						}
					case Keys.U:
					case Keys.U + 32:
						{
							SetActionCode_2("U");
							break;
						}
					case Keys.A:
					case Keys.A + 32:
						{
							SetActionCode_2("A");
							break;
						}
				}
				//end switch
			}
		}

		private void cmbType_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			// FC:FINAL:MW converting works for letters and numbers 
			Keys keyAscii = (Keys)char.ToUpper(e.KeyChar);
			if (boolSetAction)
			{
				if (keyAscii >= Keys.D0 && keyAscii <= Keys.D9)
				{
					strActionKey += FCConvert.ToString(Convert.ToChar(keyAscii));
					ShowActionPane();
					keyAscii = 0;
				}
			}
			else
			{
				switch (keyAscii)
				{
					case Keys.R:
					case Keys.R + 32:
						{
							SetActionCode_2("R");
							break;
						}
					case Keys.P:
					case Keys.P + 32:
						{
							SetActionCode_2("P");
							break;
						}
					case Keys.U:
					case Keys.U + 32:
						{
							SetActionCode_2("U");
							break;
						}
					case Keys.A:
					case Keys.A + 32:
						{
							SetActionCode_2("A");
							break;
						}
				}
				//end switch
			}
		}

		public void cmbType_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			bool cancel = e.Cancel;
			cmbType_Validate(ref cancel);
			e.Cancel = cancel;
		}

		public void cmbType_Validate(bool Cancel)
		{
			cmbType_Validate(ref Cancel);
		}

		public void cmbType_Validate(ref bool cancel)
		{
			int lngTypeNumber = 0;
			int lngTownCode = 0;
			int lngWait = 0;
			bool boolAllow = false;
			clsDRWrapper rsCheck = new clsDRWrapper();
			string strTemp = "";

			if (boolInValidate)
			{
				return;
			}
			else
			{
				boolInValidate = true;
				// kgk set the flag to indicate that this process is running
				// It MUST be set back to False when exiting the procedure
			}
			if (intCurFrame < 2 && cmbType.Visible == true)
			{
				boolAllow = true;
                //if (cmbType.Text != "")
				if (cmbType.Text != "")
				{
					//lngTypeNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbType.Text)));
                    lngTypeNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbType.Text)));
                    if (cmbResCode.SelectedIndex == -1 && modGlobal.Statics.gboolMultipleTowns)
					{
						if (cmbResCode.Items.Count > 0)
						{
							cmbResCode.SelectedIndex = 0;
							lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbResCode.Items[0].ToString(), 3))));
						}
						else
						{
							lngTownCode = 1;
						}
					}
					else
					{
						if (cmbResCode.SelectedIndex != -1)
						{
							lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbResCode.Items[cmbResCode.SelectedIndex].ToString(), 3))));
						}
						else
						{
							lngTownCode = 0;
						}
					}
					if (RestrictedCode(ref lngTypeNumber))
					{
						// Print #14, "03 - " & Now
						ProcessType();
						switch (lngTypeNumber)
						{
							case 93:
							case 94:
							case 95:
							case 96:
								{
									// is this a UT type
									if (CheckRestrictedAccountNumbers_14(93, lngTownCode) && CheckRestrictedAccountNumbers_14(94, lngTownCode) && CheckRestrictedAccountNumbers_14(95, lngTownCode) && CheckRestrictedAccountNumbers_14(96, lngTownCode))
									{
									}
									else
									{
										// tell the user that they need to setup the account number in thier receipt type
										MessageBox.Show("One of the Utility Billing receipt types (93 - 96) and (893 - 896) has an invalid account.  Please return to Receipt type Setup and enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										boolInValidate = false;
										return;
									}
									break;
								}
							case 97:
								{
									if (modGlobalConstants.Statics.gboolBD)
									{
										rsCheck.OpenRecordset("SELECT * FROM Customize", "TWAR0000.vb1");
										if (!modValidateAccount.AccountValidate(rsCheck.Get_Fields_String("PrePayReceivableaccount")))
										{
											MessageBox.Show("You must enter a Pre-Pay Receivable Account in Accounts Receivable before you may proceed.", "Invalid Pre-Pay Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
											boolInValidate = false;
											return;
										}
										// kgk troar-25 09-19-2011  Check the accounts for AR Bill Types
										if (!ValidateARAccounts())
										{
											boolInValidate = false;
											return;
										}
									}
									break;
								}
							default:
								{
									// this is a slow point
									if (CheckRestrictedAccountNumbers_12(lngTypeNumber, lngTownCode))
									{
										// this will check to make sure that this account has valid account numbers
										// if it does not, then do not allow the user to continue with this transaction
									}
									else
									{
										// tell the user that they need to setup the account number in thier receipt type
										MessageBox.Show("This receipt type (" + FCConvert.ToString(lngLastTypeChecked) + ") has an invalid account.  Please return to Receipt type Setup and enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										boolInValidate = false;
										return;
									}
									break;
								}
						}

                        switch (lngTypeNumber)
                        {
                            case 90:
                            case 91:
                            {
                                // RE
                                if (modGlobalConstants.Statics.gboolCL)
                                {
                                    // check to see if the user has permissions to open CL
                                    if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(1, "CL") == "N")
                                    {
                                        MessageBox.Show("You do not have permissions to perform this function.", "Security", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        boolInValidate = false;
                                        return;
                                    }
                                    // check to see if the user has permissions to make payments in CL
                                    if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(2, "CL") == "N")
                                    {
                                        MessageBox.Show("You do not have permissions to perform this function.", "Security", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        boolInValidate = false;
                                        return;
                                    }
                                    // Print #14, "05 - " & Now
                                    modCLCalculations.CLSetup();
                                    // Print #14, "06 - " & Now
                                    modStatusPayments.Statics.boolRE = true;
                                    frmCLGetAccount.InstancePtr.Show(App.MainForm);
                                    frmCLGetAccount.InstancePtr.txtHold.Text = "P";
                                }
                                else
                                {
                                    modStatusPayments.Statics.boolRE = true;
                                    boolAllow = false;
                                    MessageBox.Show("You are not authorized to use this receipt type.", "TRIO Tax Collections", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                            case 92:
                            {
                                // PP
                                if (modGlobalConstants.Statics.gboolCL)
                                {
                                    // check to see if the user has permissions to open CL
                                    if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(1, "CL") == "N")
                                    {
                                        MessageBox.Show("You do not have permissions to perform this function.", "Security", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        boolInValidate = false;
                                        return;
                                    }
                                    // check to see if the user has permissions to make payments in CL
                                    if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(2, "CL") == "N")
                                    {
                                        MessageBox.Show("You do not have permissions to perform this function.", "Security", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        boolInValidate = false;
                                        return;
                                    }
                                    modCLCalculations.CLSetup();
                                    modStatusPayments.Statics.boolRE = false;
                                    frmCLGetAccount.InstancePtr.Show(App.MainForm);
                                    frmCLGetAccount.InstancePtr.txtHold.Text = "P";
                                }
                                else
                                {
                                    boolAllow = false;
                                    MessageBox.Show("You are not authorized to use this receipt type.", "TRIO Tax Collections", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                            case 93:
                            case 94:
                            case 95:
                            case 96:
                            {
                                // UT
                                boolReturnBB = false;
                                if (modGlobalConstants.Statics.gboolUT)
                                {
                                    boolNoKeystroke = true;
                                    // this is to disable the keystroke in the grid to prevent the flickering of the screen
                                    // DoEvents
                                    modUTCalculations.UTSetup();
                                    //Application.DoEvents();
                                    //PPJ:FINAL:AM:#382 - don't cancel the validation
                                    //cancel = true;
                                    frmGetMasterAccount.InstancePtr.Init(3, true);
                                    // this will open the get account screen
                                    //Application.DoEvents();
                                    boolInValidate = false;
                                    return;
                                }
                                else
                                {
                                    boolAllow = false;
                                    MessageBox.Show("You are not authorized to use this receipt type.", "TRIO Utility Billing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                            case 97:
                            {
                                if (modGlobalConstants.Statics.gboolAR)
                                {
                                    boolNoKeystroke = true;
                                    // this is to disable the keystroke in the grid to prevent the flickering of the screen
                                    modARCalculations.ARSetup();
                                    frmARGetMasterAccount.InstancePtr.Init(3);
                                    boolInValidate = false;
                                    return;
                                }
                                else
                                {
                                    boolAllow = false;
                                    MessageBox.Show("You are not authorized to use this receipt type.", "TRIO Utility Billing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                            case 98:
                            {
                                // CK
                                if (modGlobalConstants.Statics.gboolCK)
                                {
                                    // check to see if the user has permissions to open CK
                                    if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(1, "CK") == "N")
                                    {
                                        MessageBox.Show("You do not have permissions to perform this function.", "Security", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        boolInValidate = false;
                                        return;
                                    }
                                    ProcessType();
                                    modBlockEntry.WriteYY();
                                    //Application.DoEvents();
                                    lngWait = 0;
                                    //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                                    //lngWait = Interaction.Shell("TWCK0000.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                                    lngWait = App.MainForm.OpenModule("TWCK0000");
                                    if (lngWait != 0)
                                    {
                                        //modExtraModules.WaitForTerm(ref lngWait);
                                        App.MainForm.LockModuleSwitch();
                                        App.MainForm.WaitForCKModule();
                                        App.MainForm.UnlockModuleSwitch();
                                        //switch back to Cash Receipt
                                        App.MainForm.OpenModule("TWCR0000");
                                    }
                                    modUseCR.FillCKInfo();
                                    //FC:FINAL:SBE - Modeless forms should be displayed as MDI child of MainForm
                                    //this.Show(FCForm.FormShowEnum.Modeless);
                                    this.Show(App.MainForm);
                                }
                                else
                                {
                                    boolAllow = false;
                                    MessageBox.Show("You are not authorized to use this receipt type.", "TRIO Clerk", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                modReplaceWorkFiles.EntryFlagFile(true, "  ", true);
                                break;
                            }
                            case 99:
                            {
                                // MV
                                if (modGlobalConstants.Statics.gboolMV || modGlobalConstants.Statics.gboolRB)
                                {
                                    // check to see if the user has permissions to open MV
                                    if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(1, "MV") == "N")
                                    {
                                        MessageBox.Show("You do not have permissions to perform this function.", "Security", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        boolInValidate = false;
                                        return;
                                    }
                                    ProcessType();
                                    modBlockEntry.WriteYY();
                                    lngWait = 0;
                                    bool result = frmGetFocus.InstancePtr.Init();
                                    if (result == false)
                                    {
                                        //frmReceiptInput.InstancePtr.cmbType.TextMatrix(0, 0, "");
                                        //frmReceiptInput.InstancePtr.cmbType.EditText = "";
                                        //frmReceiptInput.InstancePtr.cmbType.SelectedIndex = 0;
                                        frmReceiptInput.InstancePtr.cmbType.Text = "";
                                        frmReceiptInput.InstancePtr.cmbType.SelectedIndex = 0;
                                        boolInValidate = false;
                                        return;
                                    }
                                    modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "MVDelay", ref strTemp);
                                    if (Information.IsNumeric(strTemp))
                                    {
                                        if (Conversion.Val(strTemp) > 0)
                                        {
                                            modAPIsConst.Sleep(FCConvert.ToInt32(Conversion.Val(strTemp) * 1000));
                                        }
                                    }
                                    else
                                    {
                                        // do nothing
                                    }

                                    if (modGlobalConstants.Statics.gboolMV && !modGlobalConstants.Statics.gblnMVIsOpen)
                                    {
                                        // MsgBox "Fill in MV Data"
                                        modUseCR.FillMVInfo();
                                        boolReturnBB = false;

                                        this.Show(App.MainForm);
                                    }
                                    else if (modGlobalConstants.Statics.gboolMV && modGlobalConstants.Statics.gblnMVIsOpen)
                                    {

                                    }
                                    else
                                    {
                                        // This is a town with only RB and not MV so only pull some of the information back into the RI screen
                                        modUseCR.FillRBInfo();

                                        this.Show(App.MainForm);
                                    }

                                }
                                else
                                {
                                    boolAllow = false;
                                    MessageBox.Show("You are not authorized to use this receipt type.", "TRIO Motor Vehicle Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                modReplaceWorkFiles.EntryFlagFile(true, "  ");
                                break;
                            }
                            case 190:
                            {
                                if (modGlobalConstants.Statics.gboolMS)
                                {
                                    if (intCurFrame == 1)
                                    {
                                        // MOSES Type    'kgk 06-19-2012 trobl-106  Get the town code and pass it to the MOSES routine
                                        if (modGlobal.Statics.gboolMultipleTowns)
                                        {
                                            lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbResCode.Items[cmbResCode.SelectedIndex].ToString(), 3))));
                                        }
                                        modUseCR.FillMSInfo(ref lngTownCode);
                                    }
                                }
                                else
                                {
                                    boolAllow = false;
                                    MessageBox.Show("You are not authorized to use this receipt type.", "TRIO MOSES", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                break;
                            }
                            case 191:
                            {
                                break;
                            }
                            case 192:
                            {
                                break;
                            }
                            case 193:
                            {
                                break;
                            }
                            case 194:
                            {
                                break;
                            }
                            case 195:
                            {
                                break;
                            }
                            case 196:
                            {
                                break;
                            }
                            case 197:
                            {
                                break;
                            }
                            case 198:
                            {
                                break;
                            }
                            case 199:
                            {
                                break;
                            }
                            default:
                            {
                                // user input
                                break;
                            }
                        }
						//end switch
					}
					if (!boolAllow)
					{
						cancel = true;
					}
					else
					{
						if (!boolQuit && !boolReturnBB)
						{
							if (!RestrictedCode(ref lngTypeNumber))
							{
								ProcessType();
							}
						}
					}
				}
			}
			boolInValidate = false;
			// kgk 06-07-11  trocr-286  ---- XXXXX TRY MAKING THIS A GLOBAL
			boolReturnBB = false;
		}

		private void cmdBankCancel_Click(object sender, System.EventArgs e)
		{
			fraBank.Visible = false;
			ShowFrame(fraSummary);
			fraSummary.Top = 0;
		}

		private void cmdBankDone_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBank = new clsDRWrapper();
			int I;
			bool blnContinue = false;
			if (Strings.Trim(txtBankName.Text) != "" && Strings.Trim(txtBankRT.Text) != "")
			{
				// MAL@20090309: Change to warn user it is a duplicate routing transit number but don't stop insert automoatically
				// Tracker Reference:17449
				if (BankRTValidate_2(txtBankRT.Text))
				{
					blnContinue = true;
				}
				else
				{
					// MsgBox "This Routing-Transit number is already in the system.", vbInformation, "Input Error"
					if (MessageBox.Show("This Routing-Transit number is already in the system." + "\r\n" + "Are you sure you want to continue?", "Duplicate Routing/Transit Number", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
						blnContinue = true;
					}
					else
					{
						// Stop Add
						blnContinue = false;
					}
				}
				if (blnContinue)
				{
					rsBank.OpenRecordset("SELECT * FROM Bank WHERE RoutingTransit = '0'");
					if (rsBank.Execute("INSERT INTO Bank (RoutingTransit, Name) VALUES ('" + txtBankRT.Text + "', '" + txtBankName.Text + "')", "TWCR0000.vb1"))
					{
					}
					else
					{
						MessageBox.Show("The bank, " + txtBankName.Text + ", was not added.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					FillBankCombo_6(ref rsBank, true);
					for (I = 1; I <= cmbBank.Items.Count - 1; I++)
					{
						if (Strings.Trim(Strings.Left(txtBankName.Text, 10)) == Strings.Trim(Strings.Left(cmbBank.Items[I].ToString(), 10)))
						{
							cmbBank.SelectedIndex = I;
						}
					}
					fraBank.Visible = false;
					fraSummary.Visible = true;
					fraCashOut.Visible = true;
					txtBankName.Text = "";
					txtBankRT.Text = "";
					txtPayment.Focus();
					// cmdCashOut.SetFocus
				}
			}
			else
			{
				MessageBox.Show("Please fill in the bank information.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtBankName.Focus();
			}
		}

		private void cmdCashOut_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngRecKey = 0;
				string strTemp = "";
				int intCopies;
				int intIndex = 0;
				int intPrintNum = 0;
				bool blnSuccess = false;
				int intRow;
				// vbPorter upgrade warning: curConvenienceFee As Decimal	OnWriteFCConvert.ToInt16(
				Decimal curConvenienceFee = 0;
				// vbPorter upgrade warning: curNonTaxPayment As Decimal	OnWriteFCConvert.ToInt16(
				Decimal curNonTaxPayment = 0;
				// vbPorter upgrade warning: curCardAmount As Decimal	OnWrite(short, Decimal)
				Decimal curCardAmount = 0;
				int counter;
				Decimal curAvailableTax = 0;
				// vbPorter upgrade warning: curNonTaxConvenienceFee As Decimal	OnWriteFCConvert.ToInt16(
				Decimal curNonTaxConvenienceFee = 0;
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				bool blnNoTaxAmount = false;
				// hide the payment info screen if it is being shown
				if (fraPaymentInfo.Visible)
				{
					fraPaymentInfo.Visible = false;
					fraPaidBy.Visible = true;
					// kgk 12-29-2011  trocr-311
				}
				if (cmbCashOutType.SelectedIndex == 0 || cmbCashOutType.SelectedIndex == 1)
				{
					blnVISA = false;
					blnDebit = false;
				}
				if (vsSummary.Rows > 1)
				{
					if (FCConvert.ToDouble(txtCashOutTotalDue.Text) != 0)
					{
						// reset some of the boxes
						if (Conversion.Val(txtPayment.Text) == 0)
						{
							txtPayment.Text = "0.00";
						}
						if (FCConvert.ToDouble(txtPayment.Text) != 0)
						{
							if (cmbCashOutType.SelectedIndex == 0)
							{
								// Cash Transaction
								// kgk 02-02-2012  The audit report uses these the type totals, they should always be filled
								// kgk 01-27-2012  Add InforME     'kgk 09-30-2011  Add Invoice Cloud
								// If gstrEPaymentPortal = "P" Or gstrEPaymentPortal = "I" Or gstrEPaymentPortal = "E" Then
								CalculatePaymentTypeTotals();
								// End If
								CashOut();
							}
							else if (cmbCashOutType.SelectedIndex == 1)
							{
								// Check Transaction
								if (cmbBank.SelectedIndex != -1)
								{
									if (cmbBank.SelectedIndex == 0)
									{
										ShowFrame(fraBank);
										txtBankName.Focus();
									}
									else
									{
										if (Strings.Trim(txtCashOutCheckNumber.Text) != "")
										{
											strTemp = cmbBank.Items[cmbBank.SelectedIndex].ToString();
											if (Strings.InStr(1, txtCashOutCheckNumber.Text, "'", CompareConstants.vbBinaryCompare) != 0)
											{
												MessageBox.Show("You may not enter an apostrophe in the check number field.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
											}
											else
											{
												if (CheckNumberValidate_8(txtCashOutCheckNumber.Text, Strings.Right(strTemp, strTemp.Length - Strings.InStr(1, strTemp, "|", CompareConstants.vbBinaryCompare))))
												{
													// kgk 02-02-2012  The audit report uses these the type totals, they should always be filled
													// kgk 01-27-2012  Add InforME     'kgk 09-30-2011  Add Invoice Cloud
													// If gstrEPaymentPortal = "P" Or gstrEPaymentPortal = "I" Or gstrEPaymentPortal = "E" Then
													CalculatePaymentTypeTotals();
													// End If
													CashOut();
												}
												else
												{
													if (MessageBox.Show("This check is already in the system.  Would you like to continue?", "Input Error", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
													{
														// kgk 02-02-2012  The audit report uses these the type totals, they should always be filled
														// kgk 01-27-2012  Add InforME   'kgk 09-30-2011 Add Invoice Cloud
														// If gstrEPaymentPortal = "P" Or gstrEPaymentPortal = "I" Or gstrEPaymentPortal = "E" Then
														CalculatePaymentTypeTotals();
														// End If
														CashOut();
													}
												}
											}
										}
										else
										{
											MessageBox.Show("Please enter a check number.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
										}
									}
								}
								else
								{
									MessageBox.Show("Please select a bank.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
							}
							else if (cmbCashOutType.SelectedIndex == 2)
							{
								// Credit Card Transaction
								if (cmbCashOutCC.SelectedIndex != -1)
								{
									// kgk 09-29-2011 Add Invoice Cloud
									if (Conversion.Val(txtPayment.Text) > 0 && (modGlobal.Statics.gstrEPaymentPortal == "P" || modGlobal.Statics.gstrEPaymentPortal == "I"))
									{
										curAvailableTax = CalculateTaxAmountAvailable();
										if (modGlobal.Statics.gblnNoVISANONTaxPayments && (curAvailableTax < FCConvert.ToDecimal(txtPayment.Text)) && blnVISA)
										{
											if (TaxPaidAmountExists())
											{
												ans = MessageBox.Show("You may only charge an additional " + Strings.Format(curAvailableTax, "#,##0.00") + " to a VISA card.  Do you wish to process this payment amount?", "VISA Card Limitation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
											}
											else
											{
												ans = MessageBox.Show("You may only charge " + Strings.Format(curAvailableTax, "#,##0.00") + " to a VISA card.  Do you wish to process this payment amount?", "VISA Card Limitation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
											}
											if (ans == DialogResult.Yes)
											{
												txtPayment.Text = Strings.Format(curAvailableTax, "#,##0.00");
												CalculateConvenienceFee(ref curConvenienceFee, ref curNonTaxPayment, ref curNonTaxConvenienceFee);
											}
											else
											{
												return;
											}
										}
										else
										{
											CalculateConvenienceFee(ref curConvenienceFee, ref curNonTaxPayment, ref curNonTaxConvenienceFee);
										}
										// kgk 02-01-2012  Retrieve Convenience Fee from InforME
									}
									else if (Conversion.Val(txtPayment.Text) > 0 && modGlobal.Statics.gstrEPaymentPortal == "E")
									{
										// kgk 02-02-2012  The audit report uses the type totals, they should always be filled
										CalculatePaymentTypeTotals();
										DistributePaymentInforME();
										// kgk 03082012 trocr-325  Type Specific Service Codes
										// curConvenienceFee = RetrievePortalFee(CCur(txtPayment.Text))
										curNonTaxPayment = 0;
										curNonTaxConvenienceFee = 0;
									}
									else
									{
										// kgk 02-02-2012  The audit report uses the type totals, they should always be filled
										CalculatePaymentTypeTotals();
										curConvenienceFee = 0;
										curNonTaxPayment = 0;
										curNonTaxConvenienceFee = 0;
									}

									if (modGlobal.Statics.gstrEPaymentPortal != "E")
									{
										// Cash out is called by DistributePaymentInforME()
										CashOut(curConvenienceFee, blnVISA, blnDebit, curNonTaxPayment, curNonTaxConvenienceFee);
									}
								}
								else
								{
									MessageBox.Show("Please select a credit/debit card type.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
							}
							else
							{
								MessageBox.Show("Please select a payment type.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							MessageBox.Show("Please enter a value greater than zero.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						cmdCashOut.Enabled = false;
						cmdCashOut.Text = "Enter";
						mnuFileCashOut.Text = cmdCashOut.Text;
						curCardAmount = 0;
						if (lngReceiptCreated == 0)
						{
							if (vsPayments.Rows > 1)
							{
								for (intRow = 1; intRow <= vsPayments.Rows - 1; intRow++)
								{
									intIndex = FCConvert.ToInt16(FCConvert.ToDouble(vsPayments.TextMatrix(intRow, 3)));
									// CInt(vsSummary.TextMatrix(intRow, 8))
									bool executeSkipTaxConvenience = false;
									bool executeTreatAsCC = false;
									if (arr[intIndex].Type == 0)
									{
										// Cash - Continue
										blnSuccess = true;
									}
									else if (arr[intIndex].Type == 1 && !modGlobal.Statics.gblnAcceptEChecks)
									{
										// Standard Check - Continue
										blnSuccess = true;
									}
									else if (arr[intIndex].Type == 1 && modGlobal.Statics.gblnAcceptEChecks)
									{
										// Electronic Check - Test
										blnIsEPymt = true;
										blnSuccess = ProcessEPayment(intIndex);
									}
									else if (arr[intIndex].Type == 2 && modGlobal.Statics.gstrEPaymentPortal == "" || modGlobal.Statics.gstrEPaymentPortal == "N")
									{
										// Standard CC - Continue
										blnSuccess = true;
									}
									else if (arr[intIndex].Type == 2 && modGlobal.Statics.gstrEPaymentPortal != "" && modGlobal.Statics.gstrEPaymentPortal != "N" && blnDebit)
									{
										if (modGlobal.Statics.gstrEPaymentPortal == "P" && blnVISA)
										{
											executeTreatAsCC = true;
											goto TreatAsCC;
										}
										// Debit Card - Continue
										blnSuccess = true;
									}
									else if (arr[intIndex].Type == 2 && modGlobal.Statics.gstrEPaymentPortal != "" && modGlobal.Statics.gstrEPaymentPortal != "N" && !blnDebit)
									{
										// Electronic CC - Test
										executeTreatAsCC = true;
										goto TreatAsCC;
									}
									TreatAsCC:
									;
									if (executeTreatAsCC)
									{
										#region TreatAsCC
										blnNoTaxAmount = false;
										if (FCUtils.CBool(vsPayments.TextMatrix(intRow, 10)) == false)
										{
											if (!modGlobal.Statics.gblnNoVISAPayments)
											{
												if (FCConvert.ToDecimal(arr[intIndex].Amount) - arr[intIndex].NonTaxAmount - arr[intIndex].NonTaxConvenienceFee == 0)
												{
													if (modGlobal.Statics.gstrEPaymentPortal == "P")
													{
														// kgk 09-30-2011  Don't do for Inv Cld
														blnNoTaxAmount = true;
														executeSkipTaxConvenience = true;
														goto SkipTaxConvenience;
													}
													else
													{
														blnSuccess = ProcessEPayment(intIndex);
														blnIsEPymt = true;
														curCardAmount += FCConvert.ToDecimal(arr[intIndex].Amount);
													}
												}
												else
												{
													blnSuccess = ProcessEPayment(intIndex);
													blnIsEPymt = true;
													curCardAmount += FCConvert.ToDecimal(arr[intIndex].Amount);
												}
											}
											else
											{
												blnSuccess = ProcessEPayment(intIndex);
												blnIsEPymt = true;
												curCardAmount += FCConvert.ToDecimal(arr[intIndex].Amount);
											}
										}
										else
										{
											// MsgBox "Card was previously processed so we will skip authorization"
											blnSuccess = true;
											blnIsEPymt = true;
										}
										#endregion
									}
									if (blnSuccess)
									{
										// Continue Processing
										// Add Reference Number to Payment Array
										if (FCUtils.CBool(vsPayments.TextMatrix(intRow, 10)) == false)
										{
											arr[intIndex].RefNumber = strRefNum;
											// kgk 01-27-2012  Add InforME
											if (modGlobal.Statics.gstrEPaymentPortal == "E")
											{
												if (!blnIsVoid)
												{
													arr[intIndex].CNum = strReturnedCNum;
												}
												arr[intIndex].ExpDate = strReturnedExpDate;
												// arr(intindex).CryptCard = strReturnedCryptCard    ' no cryptcard for InforME
												arr[intIndex].AuthCode = strReturnedAuthCode;
												vsPayments.TextMatrix(intRow, 5, arr[intIndex].CNum);
												vsPayments.TextMatrix(intRow, 6, arr[intIndex].ExpDate);
												vsPayments.TextMatrix(intRow, 10, FCConvert.ToString(true));
											}
											// kgk 09-29-2011  Add Invoice Cloud
											if (modGlobal.Statics.gstrEPaymentPortal == "I")
											{
												// If Trim(strReturnedCryptCard) = "" Then
												// MsgBox "No SecureGUID returned"
												// End If
												if (!blnIsVoid)
												{
													arr[intIndex].CNum = strReturnedCNum;
												}
												arr[intIndex].ExpDate = strReturnedExpDate;
												arr[intIndex].CryptCard = strReturnedCryptCard;
												arr[intIndex].AuthCode = strReturnedAuthCode;
												vsPayments.TextMatrix(intRow, 5, arr[intIndex].CNum);
												vsPayments.TextMatrix(intRow, 6, arr[intIndex].ExpDate);
												vsPayments.TextMatrix(intRow, 10, FCConvert.ToString(true));
											}
											if (modGlobal.Statics.gstrEPaymentPortal == "P")
											{
												if (!blnIsVoid)
												{
													arr[intIndex].CNum = strReturnedCNum;
												}
												arr[intIndex].ExpDate = strReturnedExpDate;
												arr[intIndex].CryptCard = strReturnedCryptCard;
												arr[intIndex].AuthCode = strReturnedAuthCode;
												vsPayments.TextMatrix(intRow, 5, arr[intIndex].CNum);
												vsPayments.TextMatrix(intRow, 6, arr[intIndex].ExpDate);
												vsPayments.TextMatrix(intRow, 7, FCConvert.ToString(arr[intIndex].IsVISA));
												vsPayments.TextMatrix(intRow, 8, FCConvert.ToString(arr[intIndex].IsDebit));
												vsPayments.TextMatrix(intRow, 10, FCConvert.ToString(true));
												if (blnIsEPymt)
												{
													// If there is convenience fee
													if (arr[intIndex].ConvenienceFee > 0)
													{
														// Authorize the convenience fee
														if (!modGlobal.Statics.gblnNoVISAPayments)
														{
															if (arr[intIndex].ConvenienceFee - arr[intIndex].NonTaxConvenienceFee == 0)
															{
																executeSkipTaxConvenience = true;
																goto SkipTaxConvenience;
															}
															else
															{
																blnSuccess = ProcessEPayment_1836(intIndex, arr[intIndex].CryptCard, arr[intIndex].ExpDate, true, true, strReturnedZipCode);
															}
														}
														else
														{
															blnSuccess = ProcessEPayment_54(intIndex, arr[intIndex].CryptCard, arr[intIndex].ExpDate, true);
														}
														// If successful
														if (blnSuccess)
														{
															arr[intIndex].AuthCodeConvenienceFee = strReturnedAuthCode;
															// If there is a non tax amount
															if (arr[intIndex].NonTaxAmount > 0 && !modGlobal.Statics.gblnNoVISAPayments)
															{
																// Attempt to authorize this amount
																blnSuccess = ProcessEPayment_1188(intIndex, arr[intIndex].CryptCard, arr[intIndex].ExpDate, false, true, strReturnedZipCode);
																if (blnSuccess)
																{
																	arr[intIndex].AuthCodeVISANonTax = strReturnedAuthCode;
																	if (arr[intIndex].NonTaxConvenienceFee > 0 && !modGlobal.Statics.gblnNoVISAPayments)
																	{
																		// Attempt to authorize this amount
																		blnSuccess = ProcessEPayment_1107(intIndex, arr[intIndex].CryptCard, arr[intIndex].ExpDate, true, strReturnedZipCode);
																		if (blnSuccess)
																		{
																			arr[intIndex].AuthCodeNonTaxConvenienceFee = strReturnedAuthCode;
																		}
																		else
																		{
																			RemoveCreditCardAmounts_78(intRow, false, false, true);
																			blnSuccess = true;
																		}
																	}
																}
																else
																{
																	if (arr[intIndex].NonTaxConvenienceFee > 0 && !modGlobal.Statics.gblnNoVISAPayments)
																	{
																		RemoveCreditCardAmounts_78(intRow, false, true, true);
																		cmdCashOut.Enabled = true;
																		return;
																	}
																	else
																	{
																		RemoveCreditCardAmounts_78(intRow, false, true, false);
																		cmdCashOut.Enabled = true;
																		return;
																	}
																}
															}
														}
														else
														{
															if (arr[intIndex].NonTaxAmount > 0 && !modGlobal.Statics.gblnNoVISAPayments)
															{
																if (arr[intIndex].NonTaxConvenienceFee > 0 && !modGlobal.Statics.gblnNoVISAPayments)
																{
																	RemoveCreditCardAmounts_78(intRow, true, true, true);
																	cmdCashOut.Enabled = true;
																	return;
																}
																else
																{
																	RemoveCreditCardAmounts_78(intRow, true, true, false);
																	cmdCashOut.Enabled = true;
																	return;
																}
															}
															else
															{
																RemoveCreditCardAmounts_78(intRow, true, false, false);
																blnSuccess = true;
															}
														}
													}
													else
													{
														executeSkipTaxConvenience = true;
														goto SkipTaxConvenience;
													}
												}
											}
										}
									}
									else
									{
										break;
									}
									SkipTaxConvenience:
									;
									if (executeSkipTaxConvenience)
									{
										#region SkipTaxConvenience
										// If there is a non tax amount
										if (arr[intIndex].NonTaxAmount > 0 && !modGlobal.Statics.gblnNoVISAPayments)
										{
											// Attempt to authorize this amount
											blnSuccess = ProcessEPayment_1188(intIndex, arr[intIndex].CryptCard, arr[intIndex].ExpDate, false, true, strReturnedZipCode);
											if (blnSuccess)
											{
												if (blnNoTaxAmount)
												{
													blnIsEPymt = true;
													curCardAmount += FCConvert.ToDecimal(arr[intIndex].Amount);
													arr[intIndex].RefNumber = strRefNum;
													if (!blnIsVoid)
													{
														arr[intIndex].CNum = strReturnedCNum;
													}
													arr[intIndex].ExpDate = strReturnedExpDate;
													arr[intIndex].CryptCard = strReturnedCryptCard;
													vsPayments.TextMatrix(intRow, 5, arr[intIndex].CNum);
													vsPayments.TextMatrix(intRow, 6, arr[intIndex].ExpDate);
													vsPayments.TextMatrix(intRow, 7, FCConvert.ToString(arr[intIndex].IsVISA));
													vsPayments.TextMatrix(intRow, 8, FCConvert.ToString(arr[intIndex].IsDebit));
													vsPayments.TextMatrix(intRow, 10, FCConvert.ToString(true));
												}
												arr[intIndex].AuthCodeVISANonTax = strReturnedAuthCode;
												if (arr[intIndex].NonTaxConvenienceFee > 0 && !modGlobal.Statics.gblnNoVISAPayments)
												{
													// Attempt to authorize this amount
													blnSuccess = ProcessEPayment_1107(intIndex, arr[intIndex].CryptCard, arr[intIndex].ExpDate, true, strReturnedZipCode);
													if (blnSuccess)
													{
														arr[intIndex].AuthCodeNonTaxConvenienceFee = strReturnedAuthCode;
													}
													else
													{
														RemoveCreditCardAmounts_78(intRow, false, false, true);
														blnSuccess = true;
													}
												}
											}
											else
											{
												if (!modGlobal.Statics.gblnNoVISAPayments)
												{
													if (FCConvert.ToDecimal(arr[intIndex].Amount) - arr[intIndex].NonTaxAmount - arr[intIndex].NonTaxConvenienceFee == 0)
													{
														goto FailedTransaction;
													}
												}
												if (arr[intIndex].NonTaxConvenienceFee > 0 && !modGlobal.Statics.gblnNoVISAPayments)
												{
													RemoveCreditCardAmounts_78(intRow, false, true, true);
													cmdCashOut.Enabled = true;
													return;
												}
												else
												{
													RemoveCreditCardAmounts_78(intRow, false, true, false);
													cmdCashOut.Enabled = true;
													return;
												}
											}
										}
										#endregion
									}
								}
								// intRow
							}
							else
							{
								blnSuccess = true;
							}
							FailedTransaction:
							;
							if (blnSuccess)
							{
								lngReceiptCreated = CreateReceiptRecord(lngRecKey);
								if (modGlobal.Statics.gboolCDConnected)
								{
									// this will open then cash drawer if there is one connected
									if (modSecurity.ValidPermissions_6(this, modGlobal.CRSECURITYOPENCASHDRAWER, false))
									{
										modGlobal.OpenCashDrawer();
									}
								}
								// MAL@20090305: Moved from EndReceipt process to help with incorrect interest calculations
								// Tracker Reference: 16656/16808
								UpdateNonCRRecords();
								// show the user what receipt was generated
								MessageBox.Show("Receipt #" + FCConvert.ToString(lngReceiptCreated) + " was generated.", "Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
								// store the last receipt number in the registry
								modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\", "LastReceiptGenerated", lngReceiptCreated.ToString());
								for (intCopies = 1; intCopies <= Information.UBound(modUseCR.Statics.ReceiptArray, 1); intCopies++)
								{
									if (modUseCR.Statics.ReceiptArray[intCopies].Copies > intPrintNum)
										intPrintNum = modUseCR.Statics.ReceiptArray[intCopies].Copies;
								}
								// MAL@20080610: Have it check to see if user wants to Print the Receipt before continuing
								// Tracker Reference: 14155
								if (this.chkPrint.CheckState == Wisej.Web.CheckState.Checked)
								{
									if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
									{
										return;
									}

									if (intPrintNum > 0)
									{
										// is a number selected
										for (intCopies = 1; intCopies <= intPrintNum; intCopies++)
										{
											// if so, then print the receipt that many times
											PrintReceipt();
											boolMulitpleCopy = true;
										}
									}
									else
									{
										// else only do it once
										// PrintReceipt
									}
									if (modGlobal.Statics.gblnPrintExtraReceipt && curCardAmount != 0)
									{
										MessageBox.Show("Please click OK to print your extra copy of the receipt.", "Print Extra Copy", MessageBoxButtons.OK, MessageBoxIcon.Information);
										PrintReceipt();
									}
									// kgk 01-27-2012 Update for InforME
								}
								else if ((modGlobal.Statics.gstrEPaymentPortal == "P" || modGlobal.Statics.gstrEPaymentPortal == "I" || modGlobal.Statics.gstrEPaymentPortal == "E") && modGlobal.Statics.gblnPrintExtraReceipt && curCardAmount != 0)
								{
									if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
									{
										return;
									}

									PrintReceipt();
								}
								EndReceipt();
								//Application.DoEvents();
								if (modGlobal.Statics.gboolPendingRI)
								{
									boolUnloadOK = true;
									Close();
								}
								else
								{
									modUseCR.GoBackToTellerLogin();
								}
								boolMulitpleCopy = false;
								//JEI:IT257: cannot enable a disposed control
								if (!cmdCashOut.IsDisposed)
								{
									cmdCashOut.Enabled = true;
								}
								return;
							}
							else
							{
								// Back Out - Reset buttons, etc to pre-Cash Out button pressing
								// Clear Payment Grid
								if (blnIsVoid && blnIsEPymt)
								{
									cmdCashOut.Text = "Process";
								}
								else
								{
									for (intRow = vsPayments.Rows - 1; intRow >= 1; intRow--)
									{
										if (FCUtils.CBool(vsPayments.TextMatrix(intRow, 10)) != true)
										{
											DeletePaymentFromGrid_2(intRow);
										}
									}
									// intRow
								}
								cmdCashOut.Enabled = true;
								return;
							}
						}
					}
				}
				else
				{
					// no money is due so there should not be a receipt created
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Cash Out Click Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				{
					/*? Resume Next; */}
				cmdCashOut.Enabled = true;
			}
		}

		public void cmdCashOut_Click()
		{
			cmdCashOut_Click(cmdCashOut, new System.EventArgs());
		}

		private void AddPaymentInfoToArray_54(object intTransacitonIndex, object intPaymentIndex, object intPaymentType, object curAmount, string strEPmtAcctID)
		{
			AddPaymentInfoToArray(ref intTransacitonIndex, ref intPaymentIndex, ref intPaymentType, ref curAmount, ref strEPmtAcctID);
		}

		private void AddPaymentInfoToArray(ref object intTransacitonIndex, ref object intPaymentIndex, ref object intPaymentType, ref object curAmount, ref string strEPmtAcctID)
		{
			Array.Resize(ref modUseCR.Statics.PaymentBreakdownInfoArray, modUseCR.Statics.intPaymentBreakdownInfoCounter + 1);
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			modUseCR.Statics.PaymentBreakdownInfoArray[modUseCR.Statics.intPaymentBreakdownInfoCounter] = new modUseCR.PaymentBreakdownInfo(0);
			modUseCR.Statics.PaymentBreakdownInfoArray[modUseCR.Statics.intPaymentBreakdownInfoCounter].ReceiptIndex = FCConvert.ToInt16(intTransacitonIndex);
			modUseCR.Statics.PaymentBreakdownInfoArray[modUseCR.Statics.intPaymentBreakdownInfoCounter].PaymentIndex = FCConvert.ToInt32(intPaymentIndex);
			switch (FCConvert.ToInt32(intPaymentType))
			{
				case 0:
					{
						// Cash
						modUseCR.Statics.PaymentBreakdownInfoArray[modUseCR.Statics.intPaymentBreakdownInfoCounter].CashAmount = FCConvert.ToDecimal(curAmount);
						break;
					}
				case 1:
					{
						// Check
						modUseCR.Statics.PaymentBreakdownInfoArray[modUseCR.Statics.intPaymentBreakdownInfoCounter].CheckAmount = FCConvert.ToDecimal(curAmount);
						break;
					}
				case 2:
					{
						// Credit Card
						modUseCR.Statics.PaymentBreakdownInfoArray[modUseCR.Statics.intPaymentBreakdownInfoCounter].CardAmount = FCConvert.ToDecimal(curAmount);
						modUseCR.Statics.PaymentBreakdownInfoArray[modUseCR.Statics.intPaymentBreakdownInfoCounter].EPmtAcctID = strEPmtAcctID;
						break;
					}
			}
			//end switch
			modUseCR.Statics.intPaymentBreakdownInfoCounter += 1;
		}

		private void CalculatePaymentTypeTotals()
		{

			int counter;
			// vbPorter upgrade warning: curPayment As Decimal	OnWrite(Decimal, short)
			Decimal curPayment;
			bool blnPositiveEntry;
			bool blnNegativeEntry;
			int intType = 0;
			if (cmbCashOutType.SelectedIndex == 0)
			{
				intType = 0;
			}
			else if (cmbCashOutType.SelectedIndex == 1)
			{
				intType = 1;
			}
			else
			{
				intType = 2;
			}
			// Set the payment amount
			curPayment = FCConvert.ToDecimal(txtPayment.Text);
			if (blnVISA)
			{
				if (curPayment > 0)
				{
					// Loop through all receipt items looking for items that have a total that is positive if the payment is positive or negative if the payment is negative and apply the payment
					for (counter = 1; counter <= vsSummary.Rows - 1; counter++)
					{
						if (modUseCR.Statics.ReceiptArray[counter].Type == 90 || modUseCR.Statics.ReceiptArray[counter].Type == 91 || modUseCR.Statics.ReceiptArray[counter].Type == 92 || modUseCR.Statics.ReceiptArray[counter].Type == 890 || modUseCR.Statics.ReceiptArray[counter].Type == 891)
						{
							if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount != 0)
							{
								// If this is a positive receipt item
								if (modUseCR.Statics.ReceiptArray[counter].Total > 0)
								{
									// Check to see if the payment can fully pay this item off
									if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount - curPayment <= 0)
									{
										// If so subtract the amount from the payment
										AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount), modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
										curPayment -= (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount);
										// Set the proper payment type total for the receipt item
										switch (intType)
										{
											case 0:
												{
													// Cash
													modUseCR.Statics.ReceiptArray[counter].CashPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount;
													break;
												}
											case 1:
												{
													// Check
													modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
													break;
												}
											case 2:
												{
													// Credit
													modUseCR.Statics.ReceiptArray[counter].CardPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
													break;
												}
										}
										//end switch
										if (curPayment == 0)
										{
											break;
										}
									}
									else
									{
										// If the payment does not fully pay off the receipt item then add the payment to the proper type total
										AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, curPayment, modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
										switch (intType)
										{
											case 0:
												{
													// Cash
													modUseCR.Statics.ReceiptArray[counter].CashPaidAmount += curPayment;
													break;
												}
											case 1:
												{
													// Check
													modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount += curPayment;
													break;
												}
											case 2:
												{
													// Credit
													modUseCR.Statics.ReceiptArray[counter].CardPaidAmount += curPayment;
													break;
												}
										}
										//end switch
										// Set the reminaing payment amount to 0
										curPayment = 0;
										// Move to next payment
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				if (curPayment > 0)
				{
					// Loop through all receipt items looking for items that have a total that is positive if the payment is positive or negative if the payment is negative and apply the payment
					for (counter = 1; counter <= vsSummary.Rows - 1; counter++)
					{
						if (modUseCR.Statics.ReceiptArray[counter].Type != 90 && modUseCR.Statics.ReceiptArray[counter].Type != 91 && modUseCR.Statics.ReceiptArray[counter].Type != 92 && modUseCR.Statics.ReceiptArray[counter].Type != 890 && modUseCR.Statics.ReceiptArray[counter].Type != 891)
						{
							if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount != 0)
							{
								// If this is a positive receipt item
								if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) > 0)
								{
									// Check to see if the payment can fully pay this item off
									if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount - curPayment <= 0)
									{
										// If so subtract the amount from the payment
										AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount), modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
										curPayment -= (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount);
										// Set the proper payment type total for the receipt item
										switch (intType)
										{
											case 0:
												{
													// Cash
													modUseCR.Statics.ReceiptArray[counter].CashPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount;
													break;
												}
											case 1:
												{
													// Check
													modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
													break;
												}
											case 2:
												{
													// Credit
													modUseCR.Statics.ReceiptArray[counter].CardPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
													break;
												}
										}
										//end switch
										if (curPayment == 0)
										{
											break;
										}
									}
									else
									{
										// If the payment does not fully pay off the receipt item then add the payment to the proper type total
										AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, curPayment, modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
										switch (intType)
										{
											case 0:
												{
													// Cash
													modUseCR.Statics.ReceiptArray[counter].CashPaidAmount += curPayment;
													break;
												}
											case 1:
												{
													// Check
													modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount += curPayment;
													break;
												}
											case 2:
												{
													// Credit
													modUseCR.Statics.ReceiptArray[counter].CardPaidAmount += curPayment;
													break;
												}
										}
										//end switch
										// Set the reminaing payment amount to 0
										curPayment = 0;
										// Move to next payment
										break;
									}
								}
							}
						}
					}
				}
			}
			if (curPayment != 0)
			{
				// Loop through all receipt items looking for items that have a total that is positive if the payment is positive or negative if the payment is negative and apply the payment
				for (counter = 1; counter <= vsSummary.Rows - 1; counter++)
				{
					if ((curPayment > 0 && modUseCR.Statics.ReceiptArray[counter].Total > 0) || (curPayment < 0 && modUseCR.Statics.ReceiptArray[counter].Total < 0))
					{
						// If this item has not been fully paid off
						if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount != 0)
						{
							// If this is a positive receipt item
							if (modUseCR.Statics.ReceiptArray[counter].Total > 0)
							{
								// Check to see if the payment can fully pay this item off
								if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount - curPayment <= 0)
								{
									// If so subtract the amount from the payment
									AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount), modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
									curPayment -= (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount);
									// Set the proper payment type total for the receipt item
									switch (intType)
									{
										case 0:
											{
												// Cash
												modUseCR.Statics.ReceiptArray[counter].CashPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount;
												break;
											}
										case 1:
											{
												// Check
												modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
												break;
											}
										case 2:
											{
												// Credit
												modUseCR.Statics.ReceiptArray[counter].CardPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
												break;
											}
									}
									//end switch
									if (curPayment == 0)
									{
										break;
									}
								}
								else
								{
									// If the payment does not fully pay off the receipt item then add the payment to the proper type total
									AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, curPayment, modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
									switch (intType)
									{
										case 0:
											{
												// Cash
												modUseCR.Statics.ReceiptArray[counter].CashPaidAmount += curPayment;
												break;
											}
										case 1:
											{
												// Check
												modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount += curPayment;
												break;
											}
										case 2:
											{
												// Credit
												modUseCR.Statics.ReceiptArray[counter].CardPaidAmount += curPayment;
												break;
											}
									}
									//end switch
									// Set the reminaing payment amount to 0
									curPayment = 0;
									// Move to next payment
									break;
								}
							}
							else
							{
								// If this is a negative receipt item
								// Check to see if the payment can fully pay this item off
								if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount - curPayment >= 0)
								{
									AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount), modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
									// If so subtract the amount from the payment
									curPayment -= (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount);
									// Set the proper payment type total for the receipt item
									switch (intType)
									{
										case 0:
											{
												// Cash
												modUseCR.Statics.ReceiptArray[counter].CashPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount;
												break;
											}
										case 1:
											{
												// Check
												modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
												break;
											}
										case 2:
											{
												// Credit
												modUseCR.Statics.ReceiptArray[counter].CardPaidAmount = FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount;
												break;
											}
									}
									//end switch
									if (curPayment == 0)
									{
										break;
									}
								}
								else
								{
									// If the payment does not fully pay off the receipt item then add the payment to the proper type total
									AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, curPayment, modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
									switch (intType)
									{
										case 0:
											{
												// Cash
												modUseCR.Statics.ReceiptArray[counter].CashPaidAmount += curPayment;
												break;
											}
										case 1:
											{
												// Check
												modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount += curPayment;
												break;
											}
										case 2:
											{
												// Credit
												modUseCR.Statics.ReceiptArray[counter].CardPaidAmount += curPayment;
												break;
											}
									}
									//end switch
									// Set the reminaing payment amount to 0
									curPayment = 0;
									// Move to next payment
									break;
								}
							}
						}
					}
				}
			}
			if (curPayment != 0)
			{
				for (counter = 1; counter <= vsSummary.Rows - 1; counter++)
				{
					// If this item has not been fully paid off
					if (FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total) - modUseCR.Statics.ReceiptArray[counter].CardPaidAmount - modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount - modUseCR.Statics.ReceiptArray[counter].CashPaidAmount != 0)
					{
						// If the payment does not fully pay off the receipt item then add the payment to the proper type total
						AddPaymentInfoToArray_54(counter, vsPayments.Rows, intType, curPayment, modUseCR.Statics.ReceiptArray[counter].EPmtAccountID);
						switch (intType)
						{
							case 0:
								{
									// Cash
									modUseCR.Statics.ReceiptArray[counter].CashPaidAmount += curPayment;
									break;
								}
							case 1:
								{
									// Check
									modUseCR.Statics.ReceiptArray[counter].CheckPaidAmount += curPayment;
									break;
								}
							case 2:
								{
									// Credit
									modUseCR.Statics.ReceiptArray[counter].CardPaidAmount += curPayment;
									break;
								}
						}
						//end switch
						// Set the reminaing payment amount to 0
						curPayment = 0;
						// Move to next payment
						break;
					}
				}
			}
		}

		private Decimal CalculateTaxAmountAvailable()
		{
			Decimal CalculateTaxAmountAvailable = 0;
			// vbPorter upgrade warning: curTotalTax As Decimal	OnWrite(short, Decimal)
			Decimal curTotalTax;
			// vbPorter upgrade warning: curTotalTaxPayments As Decimal	OnWrite(short, Decimal)
			Decimal curTotalTaxPayments;
			int counter;
			curTotalTax = 0;
			curTotalTaxPayments = 0;
			for (counter = 1; counter <= vsSummary.Rows - 1; counter++)
			{
				int vbPorterVar = modUseCR.Statics.ReceiptArray[counter].Type;
				// Real Estate or Property Tax Receipt Types
				if ((vbPorterVar >= 90 && vbPorterVar <= 92) || (vbPorterVar == 891) || (vbPorterVar == 892))
				{
					// Add in Total
					curTotalTax += FCConvert.ToDecimal(modUseCR.Statics.ReceiptArray[counter].Total);
					// All Other Receipt Types
				}
				else
				{
					// do nothing this is a non tax receipt type
				}
			}
			for (counter = 1; counter <= modStatusPayments.MAX_PAYMENTS; counter++)
			{
				if (arr[counter].Amount == 0)
					break;
				switch (arr[counter].Type)
				{
					case 0:
						{
							// cash
							// do nothing
							break;
						}
					case 1:
						{
							// check
							// do nothing
							break;
						}
					case 2:
						{
							// card
							if (arr[counter].IsVISA)
							{
								// if part of the payment is non tax then there is no tax amount left to charge a VISA convenience fee against
								if (arr[counter].NonTaxAmount != 0)
								{
									curTotalTaxPayments += (FCConvert.ToDecimal(arr[counter].Amount) - arr[counter].ConvenienceFee - arr[counter].NonTaxAmount);
									break;
								}
								else
								{
									curTotalTaxPayments += FCConvert.ToDecimal(arr[counter].Amount) - arr[counter].ConvenienceFee;
								}
							}
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
			CalculateTaxAmountAvailable = curTotalTax - curTotalTaxPayments;
			return CalculateTaxAmountAvailable;
		}

		private bool TaxPaidAmountExists()
		{
			bool TaxPaidAmountExists = false;
			int counter;
			TaxPaidAmountExists = false;
			for (counter = 1; counter <= modStatusPayments.MAX_PAYMENTS; counter++)
			{
				if (arr[counter].Amount == 0)
					break;
				switch (arr[counter].Type)
				{
					case 0:
						{
							// cash
							// do nothing
							break;
						}
					case 1:
						{
							// check
							// do nothing
							break;
						}
					case 2:
						{
							// card
							if (arr[counter].IsVISA)
							{
								// if part of the payment is non tax then there is no tax amount left to charge a VISA convenience fee against
								if (arr[counter].NonTaxAmount != 0)
								{
									if ((FCConvert.ToDecimal(arr[counter].Amount) - arr[counter].ConvenienceFee - arr[counter].NonTaxAmount) > 0)
									{
										TaxPaidAmountExists = true;
										break;
									}
								}
								else
								{
									if ((FCConvert.ToDecimal(arr[counter].Amount) - arr[counter].ConvenienceFee) > 0)
									{
										TaxPaidAmountExists = true;
										break;
									}
								}
							}
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
			return TaxPaidAmountExists;
		}

		private void CalculateConvenienceFee(ref Decimal curConv, ref Decimal curNonTaxAmt, ref Decimal curNonTaxConvenienceFee)
		{
			// vbPorter upgrade warning: curFee As Decimal	OnWrite(short, Decimal)
			Decimal curFee;
			int counter;
			bool blnFlatFeeCharged;
			clsDRWrapper rsCashRec = new clsDRWrapper();
			// vbPorter upgrade warning: curTaxAmt As Decimal	OnWrite(Decimal, short)
			Decimal curTaxAmt;
			Decimal curMinFee;
			int intLastPaymentIndex;
			// vbPorter upgrade warning: curTaxConvenienceFee As Decimal	OnWrite(short, Decimal)
			Decimal curTaxConvenienceFee;
			curConv = 0;
			curNonTaxAmt = 0;
			curNonTaxConvenienceFee = 0;
			curFee = 0;
			blnFlatFeeCharged = false;
			intLastPaymentIndex = -1;
			curTaxConvenienceFee = 0;
			rsCashRec.OpenRecordset("SELECT * FROM CashRec", "TWCR0000.vb1");
			curMinFee = FCConvert.ToDecimal(rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount"));
			CalculatePaymentTypeTotals();
			// If blnVISA Then
			curTaxAmt = CalculateTaxAmountAvailable();
			// End If
			for (counter = 0; counter <= modUseCR.Statics.intPaymentBreakdownInfoCounter - 1; counter++)
			{
				if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].PaymentIndex == vsPayments.Rows)
				{
					if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount > 0)
					{
						if (blnDebit && blnVISA)
						{
							if (curTaxAmt > 0)
							{
								if (!blnFlatFeeCharged)
								{
									if (rsCashRec.EndOfFile() != true && rsCashRec.BeginningOfFile() != true)
									{
										modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = FCConvert.ToDecimal(rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount"));
									}
									else
									{
										modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = 0;
									}
									if (modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891)
									{
										curTaxConvenienceFee += modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
										if (intLastPaymentIndex == -1)
										{
											intLastPaymentIndex = counter;
										}
									}
									blnFlatFeeCharged = true;
								}
							}
							if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount <= curTaxAmt)
							{
								curTaxAmt -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
							}
							else
							{
								curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
								curTaxAmt = 0;
							}
						}
						else
						{
							if (blnVISA)
							{
								if (curTaxAmt > 0)
								{
									if (FCConvert.ToString(rsCashRec.Get_Fields_String("ConvenienceFeeMethod")) == "F")
									{
										// ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeType = "F" Then
										if (!blnFlatFeeCharged)
										{
											modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = FCConvert.ToDecimal(rsCashRec.Get_Fields_Decimal("ConvenienceFlatAmount"));
											// ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeFactor
											if (modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891)
											{
												curTaxConvenienceFee += modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
												if (intLastPaymentIndex == -1)
												{
													intLastPaymentIndex = counter;
												}
											}
											if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount <= curTaxAmt)
											{
												curTaxAmt -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
											}
											else
											{
												curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
												curTaxAmt = 0;
											}
											blnFlatFeeCharged = true;
										}
										else
										{
											if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount <= curTaxAmt)
											{
												curTaxAmt -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
											}
											else
											{
												curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
												curTaxAmt = 0;
											}
										}
									}
									else if (rsCashRec.Get_Fields_String("ConvenienceFeeMethod") == "P")
									{
										// If ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeType = "P" Then
										if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount <= curTaxAmt)
										{
											modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = FCConvert.ToDecimal(modGlobal.Round(FCConvert.ToDouble(modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount * FCConvert.ToDecimal(rsCashRec.Get_Fields_Double("ConveniencePercentage"))), 2));
											// ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeFactor
											curTaxAmt -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
										}
										else
										{
											modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = FCConvert.ToDecimal(modGlobal.Round(FCConvert.ToDouble(curTaxAmt * modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].ConvenienceFeeFactor), 2));
											curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
											curTaxAmt = 0;
										}
										if (modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891)
										{
											curTaxConvenienceFee += modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
											if (intLastPaymentIndex == -1)
											{
												intLastPaymentIndex = counter;
											}
										}
									}
									else
									{
										curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
										modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = 0;
									}
								}
								else
								{
									curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
									modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = 0;
								}
							}
							else
							{
								if (FCConvert.ToString(rsCashRec.Get_Fields_String("ConvenienceFeeMethod")) == "F")
								{
									// If ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeType = "F" Then
									if (!blnFlatFeeCharged)
									{
										modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = FCConvert.ToDecimal(rsCashRec.Get_Fields_Decimal("ConvenienceFlatAmount"));
										// ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeFactor
										if (modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891)
										{
											curTaxConvenienceFee += modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
											if (intLastPaymentIndex == -1)
											{
												intLastPaymentIndex = counter;
											}
											if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount <= curTaxAmt)
											{
												curTaxAmt -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
											}
											else
											{
												curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
												curTaxAmt = 0;
											}
										}
										else
										{
											curNonTaxAmt += modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
										}
										blnFlatFeeCharged = true;
									}
								}
								else if (rsCashRec.Get_Fields_String("ConvenienceFeeMethod") == "P")
								{
									// If ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeType = "P" Then
									modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = FCConvert.ToDecimal(modGlobal.Round(FCConvert.ToDouble(modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount * FCConvert.ToDecimal(rsCashRec.Get_Fields_Double("ConveniencePercentage"))), 2));
									// ReceiptArray(PaymentBreakdownInfoArray(counter).ReceiptIndex).ConvenienceFeeFactor
									if (modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891)
									{
										curTaxConvenienceFee += modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
										if (intLastPaymentIndex == -1)
										{
											intLastPaymentIndex = counter;
										}
										if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount <= curTaxAmt)
										{
											curTaxAmt -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
										}
										else
										{
											curNonTaxAmt += (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount - curTaxAmt);
											curTaxAmt = 0;
										}
									}
									else
									{
										curNonTaxAmt += modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
									}
								}
								else
								{
									modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = 0;
								}
							}
						}
						curFee += modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
						modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].ConvenienceFee += modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
					}
				}
			}
			if (rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount") != 0 && !modGlobal.Statics.gblnNoVISAPayments)
			{
				if (curTaxConvenienceFee > 0 && curTaxConvenienceFee < rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount"))
				{
					modUseCR.Statics.PaymentBreakdownInfoArray[intLastPaymentIndex].ConvenienceFee += (rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount") - curTaxConvenienceFee);
					curFee += (rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount") - curTaxConvenienceFee);
					modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[intLastPaymentIndex].ReceiptIndex].ConvenienceFee += (rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount") - curTaxConvenienceFee);
					curNonTaxConvenienceFee = curFee - rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount");
				}
				else
				{
					curNonTaxConvenienceFee = curFee - curTaxConvenienceFee;
				}
			}
			else
			{
				curNonTaxConvenienceFee = curFee - curTaxConvenienceFee;
			}
			curConv = curFee;
		}

		private void cmdTeller_Click_2(short Index)
		{
			cmdTeller_Click(ref Index);
		}

		private void cmdTeller_Click(ref short Index)
		{
			// this is a control array of two buttons, Done and Exit on the teller login frame
			int intTemp;
			if (Index == 0)
			{
				// done
				strTellerID = Strings.Left(Strings.Trim(txtTellerID.Text), 3);
				intTemp = modGlobal.ValidateTellerID_21(strTellerID, Strings.Trim(txtPassword.Text));
				switch (intTemp)
				{
					case 0:
						{
							// valid
							break;
						}
					case 1:
						{
							// Logged in
							if (MessageBox.Show("Teller " + strTellerID + " is already logged in, would you like to continue?", "Teller Logged In", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
							{
								// if they want to continue then ok, otherwise do not let them use this Teller ID
								return;
							}
							break;
						}
					case 2:
						{
							// not a valid ID
							MessageBox.Show("Please enter a valid Teller ID.", "Invalid Teller", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtTellerID.SelectionStart = 0;
							txtTellerID.SelectionLength = txtTellerID.Text.Length;
							txtTellerID.Focus();
							return;
						}
					case 3:
						{
							// incorrect password
							MessageBox.Show("Please enter a valid password for the Teller ID supplied.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
							txtPassword.SelectionStart = 0;
							txtPassword.SelectionLength = txtPassword.Text.Length;
							txtPassword.Focus();
							return;
						}
				}
				//end switch
				modGlobal.TellerLogin_6(strTellerID, true);
				// then save it is the registry
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "CRLastTellerID", strTellerID);
				StartUp();
				// and start the Input Screen
				lblTellerNumber.Text = modGlobal.PadToString_6(strTellerID, 3);
				fraTeller.Visible = false;
				// hide the teller frame
				//Application.DoEvents();
				if (cmbType.Visible && cmbType.Enabled)
				{
					cmbType.Focus();
				}
				else
				{
					txtFirst.Focus();
				}
			}
			else
			{
				// exit
				EndReceipt();
				Close();
			}
		}

		private void cmdClearCC_Click(object sender, System.EventArgs e)
		{
			txtTrack2.Text = "";
			txtCCNum.Enabled = true;
			txtCCNum.Text = "";
			cboExpMonth.Enabled = true;
			cboExpYear.Enabled = true;
			cboExpMonth.SelectedIndex = -1;
			cboExpYear.SelectedIndex = -1;
			txtCVV2.Text = "";
			txtCCNum.Focus();
		}

		private void cmdCreditReceiptCancel_Click(object sender, System.EventArgs e)
		{
			ShowFrame(fraCashOut);
			cmdAddReceipt.Enabled = true;
			cmdSummary.Enabled = true;
		}

		private void cmdCreditReceiptOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDetail = new clsDRWrapper();
			clsDRWrapper rsReceipt = new clsDRWrapper();
			bool blnIsCheck = false;
			int intIndex;
			string strLast = "";
			if (txtCreditReceiptNum.Text != "" || txtCreditReceiptNum.Text != "0")
			{
				lngReceiptNum = FCConvert.ToInt32(FCConvert.ToDouble(txtCreditReceiptNum.Text));
				blnIsCheck = FCConvert.CBool(cmbCashOutType.SelectedIndex == 1);
				rsReceipt.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptNumber = " + FCConvert.ToString(lngReceiptNum), modExtraModules.strCRDatabase);
				if (rsReceipt.RecordCount() > 0)
				{
					if (FCConvert.ToBoolean(rsReceipt.Get_Fields_Boolean("IsEPayment")))
					{
						blnIsEPymt = true;
					}
					else
					{
						blnIsEPymt = false;
					}
					if (blnIsCheck)
					{
						// Check for Multiple E-Checks
						if (FCConvert.ToBoolean(rsReceipt.Get_Fields_Boolean("MultipleChecks")))
						{
							rsDetail.OpenRecordset("SELECT * FROM CheckMaster WHERE BankNumber = " + Strings.Right(cmbBank.Items[cmbBank.SelectedIndex].ToString(), cmbBank.Items[cmbBank.SelectedIndex].ToString().Length - Strings.InStr(1, cmbBank.Items[cmbBank.SelectedIndex].ToString(), "|", CompareConstants.vbBinaryCompare)) + " AND ReceiptNumber = " + rsReceipt.Get_Fields_Int32("ID"), modExtraModules.strCRDatabase);
							if (rsDetail.RecordCount() > 1)
							{
								MessageBox.Show("There are multiple e-check records for this Bank for this receipt." + "\r\n" + "Please select which account you wish to apply the refund to", "Multiple Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
								LoadMultiAcctGrid(blnIsCheck, rsDetail);
								ShowFrame(fraMultiAccts);
							}
							else if (rsDetail.RecordCount() == 1)
							{
								lngMultiAcct = FCConvert.ToInt32(rsDetail.Get_Fields_Int32("ID"));
								AddCreditAmounttoArray(rsDetail, lngMultiAcct, blnIsCheck);
								CashOut();
								ShowFrame(fraCashOut);
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
							else
							{
								MessageBox.Show("There are no check records for this bank for this receipt." + "\r\n" + "Please select the correct Bank and try again", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								ShowFrame(fraCashOut);
								cmbBank.Focus();
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
						}
						else
						{
							rsDetail.OpenRecordset("SELECT * FROM CheckMaster WHERE ReceiptNumber = " + rsReceipt.Get_Fields_Int32("ID"), modExtraModules.strCRDatabase);
							if (rsDetail.RecordCount() > 0)
							{
								lngMultiAcct = FCConvert.ToInt32(rsDetail.Get_Fields_Int32("ID"));
								AddCreditAmounttoArray(rsDetail, lngMultiAcct, blnIsCheck);
								CashOut();
								ShowFrame(fraCashOut);
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
							else
							{
								MessageBox.Show("There are no check records for this receipt." + "\r\n" + "Please select a different payment type and try again", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								ShowFrame(fraCashOut);
								cmbCashOutType.Focus();
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
						}
					}
					else
					{
						// Credit Cards
						if (FCConvert.ToBoolean(rsReceipt.Get_Fields_Boolean("MultipleCC")))
						{
							rsDetail.OpenRecordset("SELECT * FROM CCMaster WHERE CCType = " + FCConvert.ToString(cmbCashOutCC.ItemData(cmbCashOutCC.SelectedIndex)) + " AND ReceiptNumber = " + rsReceipt.Get_Fields_Int32("ID"), modExtraModules.strCRDatabase);
							if (rsDetail.RecordCount() > 1)
							{
								MessageBox.Show("There are multiple credit cards of the same type for this receipt." + "\r\n" + "Please select which credit card account you wish to apply the refund to", "Multiple Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
								LoadMultiAcctGrid(blnIsCheck, rsDetail);
								ShowFrame(fraMultiAccts);
							}
							else if (rsDetail.RecordCount() == 1)
							{
								// There's Only One - Continue
								lngMultiAcct = FCConvert.ToInt32(rsDetail.Get_Fields_Int32("ID"));
								AddCreditAmounttoArray(rsDetail, lngMultiAcct, blnIsCheck);
								CashOut();
								ShowFrame(fraCashOut);
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
							else
							{
								MessageBox.Show("There are no credit card records for this credit card type for this receipt." + "\r\n" + "Please select the correct credit card type and try again", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								ShowFrame(fraCashOut);
								cmbCashOutCC.Focus();
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
						}
						else
						{
							rsDetail.OpenRecordset("SELECT * FROM CCMaster WHERE ReceiptNumber = " + rsReceipt.Get_Fields_Int32("ID"), modExtraModules.strCRDatabase);
							if (rsDetail.RecordCount() > 0)
							{
								lngMultiAcct = FCConvert.ToInt32(rsDetail.Get_Fields_Int32("ID"));
								AddCreditAmounttoArray(rsDetail, lngMultiAcct, blnIsCheck);
								CashOut();
								ShowFrame(fraCashOut);
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
							else
							{
								MessageBox.Show("There are no credit card records for this receipt." + "\r\n" + "Please select a different payment type and try again", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								ShowFrame(fraCashOut);
								cmbCashOutType.Focus();
								cmdAddReceipt.Enabled = true;
								cmdSummary.Enabled = true;
							}
						}
					}
				}
				else
				{
					MessageBox.Show("Receipt Number not found in the database.", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtCreditReceiptNum.Text = "";
					txtCreditReceiptNum.Focus();
				}
			}
			else
			{
				MessageBox.Show("You must enter a receipt number to process.", "No Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtCreditReceiptNum.Focus();
			}
		}

		private void cmdEdit_Name_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID = 0;
			cmdEdit_Name.Enabled = false;
			// kk01232015 trouts-134  Prevent the code from running twice
			if (Information.IsNumeric(txtCustNum_Name.Text) && Conversion.Val(txtCustNum_Name.Text) > 0)
			{
				lngID = FCConvert.ToInt32(txtCustNum_Name.Text);
				lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
				if (lngID > 0)
				{
					txtCustNum_Name.Text = FCConvert.ToString(lngID);
					GetPartyInfo_6(lngID, true);
				}
			}
			cmdEdit_Name.Enabled = true;
		}

		private void cmdSearch_Name_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtCustNum_Name.Text = FCConvert.ToString(lngID);
				GetPartyInfo_6(lngID, true);
			}
		}

		private void txtCustNum_Name_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCustNum_Name_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtCustNum_Name.Text) && Conversion.Val(txtCustNum_Name.Text) > 0)
			{
				GetPartyInfo_8(FCConvert.ToInt32(FCConvert.ToDouble(txtCustNum_Name.Text)), true);
			}
			else
			{
				ClearPartyInfo_2(true);
			}
		}

		private void cmdEdit_PaidBy_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID = 0;
			cmdEdit_PaidBy.Enabled = false;
			// kk01232015 trouts-134  Prevent the code from running twice
			if (Information.IsNumeric(txtCustNum_PaidBy.Text) && Conversion.Val(txtCustNum_PaidBy.Text) > 0)
			{
				lngID = FCConvert.ToInt32(txtCustNum_PaidBy.Text);
				lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
				if (lngID > 0)
				{
					txtCustNum_PaidBy.Text = FCConvert.ToString(lngID);
					GetPartyInfo_6(lngID, false);
				}
			}
			cmdEdit_PaidBy.Enabled = true;
		}

		private void cmdSearch_PaidBy_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtCustNum_PaidBy.Text = FCConvert.ToString(lngID);
				GetPartyInfo_6(lngID, false);
			}
		}

		private void txtCustNum_PaidBy_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCustNum_PaidBy_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtCustNum_PaidBy.Text) && Conversion.Val(txtCustNum_PaidBy.Text) > 0)
			{
				GetPartyInfo_8(FCConvert.ToInt32(FCConvert.ToDouble(txtCustNum_PaidBy.Text)), false);
			}
			else
			{
				ClearPartyInfo_2(false);
			}
		}

		private void cmdMultiAcctsOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDetail = new clsDRWrapper();
			bool blnIsCheck = false;
			int intRow;
			for (intRow = 1; intRow <= vsMultiAccts.Rows - 1; intRow++)
			{
				if (FCConvert.ToDouble(vsMultiAccts.TextMatrix(intRow, 0)) == -1)
				{
					// This is the selected row
					lngMultiAcct = FCConvert.ToInt32(vsMultiAccts.TextMatrix(intRow, 3));
					blnIsCheck = FCConvert.CBool(vsMultiAccts.TextMatrix(intRow, 4) == "Y");
					break;
				}
			}
			// intRow
			if (lngMultiAcct > 0)
			{
				if (blnIsCheck)
				{
					rsDetail.OpenRecordset("SELECT * FROM Checkmaster WHERE ID = " + FCConvert.ToString(lngMultiAcct), modExtraModules.strCRDatabase);
				}
				else
				{
					rsDetail.OpenRecordset("SELECT * FROM CCMaster WHERE ID = " + FCConvert.ToString(lngMultiAcct), modExtraModules.strCRDatabase);
				}
				AddCreditAmounttoArray(rsDetail, lngMultiAcct, blnIsCheck);
				CashOut();
				ShowFrame(fraCashOut);
				cmdAddReceipt.Enabled = true;
				cmdSummary.Enabled = true;
			}
			else
			{
				MessageBox.Show("You must select an account to apply the credit to", "Required Info", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
				vsMultiAccts.Focus();
			}
		}

		private void cmdVoidCancel_Click(object sender, System.EventArgs e)
		{
			blnIsSale = true;
			blnIsVoid = false;
			ShowFrame(fraReceipt);
		}

		private void cmdVoidProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsArchive = new clsDRWrapper();
			clsDRWrapper rsReceipt = new clsDRWrapper();
			clsDRWrapper rsCRecord = new clsDRWrapper();
			int intCount = 0;
			int intIndex = 0;
			int lngType = 0;
			int lngReceiptKey = 0;
			string strBank = "";
			string strRouteNum = "";
			if (txtReceiptNumber.Text != "" || txtReceiptNumber.Text == "0")
			{
				lngReceiptNum = FCConvert.ToInt32(FCConvert.ToDouble(txtReceiptNumber.Text));
				rsReceipt.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptNumber = " + FCConvert.ToString(lngReceiptNum), modExtraModules.strCRDatabase);
				if (rsReceipt.RecordCount() > 0)
				{
					// Receipt Found - Get the values
					lngReceiptKey = FCConvert.ToInt32(rsReceipt.Get_Fields_Int32("ReceiptKey"));
					// kk trocrs-8
					blnIsEPymt = FCConvert.ToBoolean(rsReceipt.Get_Fields_Boolean("IsEPayment"));
					rsArchive.OpenRecordset("SELECT * FROM Archive WHERE ReceiptNumber = " + FCConvert.ToString(lngReceiptKey), modExtraModules.strCRDatabase);
					if (rsArchive.RecordCount() > 0)
					{
						while (!rsArchive.EndOfFile())
						{
							lngType = FCConvert.ToInt32(rsArchive.Get_Fields_Int32("ReceiptType"));
							if (RestrictedCode(ref lngType))
							{
								MessageBox.Show("This receipt has a restricted receipt type.", "Restricted Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtReceiptNumber.Text = "";
								txtReceiptNumber.Focus();
								goto EndProcess;
							}
							rsArchive.MoveNext();
						}
						rsArchive.MoveFirst();
						blnIsSale = false;
						// MAL@20090202: Make sure its pegged as a credit ; Tracker Reference: 16641
						intIndex = 1;
						while (!rsArchive.EndOfFile())
						{
							intCount += 1;
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Account = FCConvert.ToString(rsArchive.Get_Fields("AccountNumber"));
							modUseCR.Statics.ReceiptArray[intCount].AffectCash = FCConvert.ToBoolean(rsArchive.Get_Fields_Boolean("AffectCash"));
							if (Conversion.Val(rsArchive.Get_Fields_Int32("DailyCloseOut")) > 0)
							{
								modUseCR.Statics.ReceiptArray[intCount].AffectCashDrawer = FCConvert.ToBoolean(0);
							}
							else
							{
								modUseCR.Statics.ReceiptArray[intCount].AffectCashDrawer = FCConvert.ToBoolean(rsArchive.Get_Fields_Boolean("AffectCashDrawer"));
							}
							modUseCR.Statics.ReceiptArray[intCount].CollectionCode = FCConvert.ToString(rsArchive.Get_Fields_String("CollectionCode"));
							modUseCR.Statics.ReceiptArray[intCount].Comment = "Void of Receipt Number " + FCConvert.ToString(lngReceiptNum);
							modUseCR.Statics.ReceiptArray[intCount].Control1 = FCConvert.ToString(rsArchive.Get_Fields_String("Control1"));
							modUseCR.Statics.ReceiptArray[intCount].Control2 = FCConvert.ToString(rsArchive.Get_Fields_String("Control2"));
							modUseCR.Statics.ReceiptArray[intCount].Control3 = FCConvert.ToString(rsArchive.Get_Fields_String("Control3"));
							modUseCR.Statics.ReceiptArray[intCount].Copies = 1;
							modUseCR.Statics.ReceiptArray[intCount].Date = DateTime.Today;
							modUseCR.Statics.ReceiptArray[intCount].DefaultCashAccount = FCConvert.ToString(rsArchive.Get_Fields_String("DefaultCashAccount"));
							modUseCR.Statics.ReceiptArray[intCount].DefaultMIAccount = FCConvert.ToString(rsArchive.Get_Fields_String("DefaultMIAccount"));
							modUseCR.Statics.ReceiptArray[intCount].EFT = FCConvert.ToBoolean(rsArchive.Get_Fields_Boolean("EFT"));
							// TODO Get_Fields: Check the table for the column [Amount1] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Fee1 = FCConvert.ToDouble(rsArchive.Get_Fields("Amount1")) * -1;
							// TODO Get_Fields: Check the table for the column [Amount2] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Fee2 = FCConvert.ToDouble(rsArchive.Get_Fields("Amount2")) * -1;
							// TODO Get_Fields: Check the table for the column [Amount3] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Fee3 = FCConvert.ToDouble(rsArchive.Get_Fields("Amount3")) * -1;
							// TODO Get_Fields: Check the table for the column [Amount4] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Fee4 = FCConvert.ToDouble(rsArchive.Get_Fields("Amount4")) * -1;
							// TODO Get_Fields: Check the table for the column [Amount5] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Fee5 = FCConvert.ToDouble(rsArchive.Get_Fields("Amount5")) * -1;
							// TODO Get_Fields: Check the table for the column [Amount6] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Fee6 = FCConvert.ToDouble(rsArchive.Get_Fields("Amount6")) * -1;
							modUseCR.Statics.ReceiptArray[intCount].FeeD1 = FCConvert.ToString(rsArchive.Get_Fields_String("Account1"));
							modUseCR.Statics.ReceiptArray[intCount].FeeD2 = FCConvert.ToString(rsArchive.Get_Fields_String("Account2"));
							modUseCR.Statics.ReceiptArray[intCount].FeeD3 = FCConvert.ToString(rsArchive.Get_Fields_String("Account3"));
							modUseCR.Statics.ReceiptArray[intCount].FeeD4 = FCConvert.ToString(rsArchive.Get_Fields_String("Account4"));
							modUseCR.Statics.ReceiptArray[intCount].FeeD5 = FCConvert.ToString(rsArchive.Get_Fields_String("Account5"));
							modUseCR.Statics.ReceiptArray[intCount].FeeD6 = FCConvert.ToString(rsArchive.Get_Fields_String("Account6"));
							modUseCR.Statics.ReceiptArray[intCount].Name = FCConvert.ToString(rsArchive.Get_Fields_String("Name"));
							modUseCR.Statics.ReceiptArray[intCount].NamePartyID = FCConvert.ToInt32(rsArchive.Get_Fields_Int32("NamePartyID"));
							modUseCR.Statics.ReceiptArray[intCount].PaidBy = FCConvert.ToString(rsReceipt.Get_Fields_String("PaidBy"));
							modUseCR.Statics.ReceiptArray[intCount].PrintReceipt = true;
							// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intCount].Quantity = FCConvert.ToInt32(rsArchive.Get_Fields("Quantity"));
							modUseCR.Statics.ReceiptArray[intCount].RecordKey = FCConvert.ToInt32(rsArchive.Get_Fields_Int32("RecordKey"));
							modUseCR.Statics.ReceiptArray[intCount].ResCode = FCConvert.ToInt32(rsArchive.Get_Fields_Int32("TownKey"));
							modUseCR.Statics.ReceiptArray[intCount].Reference = FCConvert.ToString(rsArchive.Get_Fields_String("Ref"));
							modUseCR.Statics.ReceiptArray[intCount].Split = FCConvert.ToInt32(rsArchive.Get_Fields_Int16("Split"));
							modUseCR.Statics.ReceiptArray[intCount].ARBillType = FCConvert.ToInt32(rsArchive.Get_Fields_Int32("ARBillType"));
							// MAL@20080729: Corrected to pull the right type in
							// Tracker Reference: 14811
							// ReceiptArray(intCount).Type = lngType
							modUseCR.Statics.ReceiptArray[intCount].Type = FCConvert.ToInt32(rsArchive.Get_Fields_Int32("ReceiptType"));
							modUseCR.Statics.ReceiptArray[intCount].Total = modUseCR.CalculateReceiptTotal(ref intCount);
							modUseCR.Statics.ReceiptArray[intCount].Used = true;
							// kgk 03-19-2012 trocr-328  Setup to use separate credit card account
							modUseCR.Statics.ReceiptArray[intCount].CardPaidAmount = FCConvert.ToDecimal(rsArchive.Get_Fields_Decimal("CardPaidAmount")) * -1;
							modUseCR.Statics.ReceiptArray[intCount].CashPaidAmount = FCConvert.ToDecimal(rsArchive.Get_Fields_Decimal("CashPaidAmount")) * -1;
							modUseCR.Statics.ReceiptArray[intCount].CheckPaidAmount = FCConvert.ToDecimal(rsArchive.Get_Fields_Decimal("CheckPaidAmount")) * -1;
							modUseCR.Statics.ReceiptArray[intCount].ConvenienceFee = FCConvert.ToDecimal(rsArchive.Get_Fields_Decimal("ConvenienceFee")) * -1;
							modUseCR.Statics.ReceiptArray[intCount].ConvenienceFeeAccount = FCConvert.ToString(rsArchive.Get_Fields_String("ConvenienceFeeGLAccount"));
							modUseCR.Statics.ReceiptArray[intCount].SeperateCreditCardGLAccount = FCConvert.ToString(rsArchive.Get_Fields_String("SeperateCreditCardGLAccount"));
							rsArchive.MoveNext();
						}
						// MAL@20090417: Recreate fix for Tracker #17964 - Run Only for E-Pymt
						if (blnIsEPymt)
						{
							// MAL@20090130: Add to Payment Array
							if (rsReceipt.Get_Fields_Double("CashAmount") != 0)
							{
								// Add Cash Payment Line
								arr[intIndex].Amount = FCConvert.ToDouble(rsReceipt.Get_Fields_Double("CashAmount")) * -1;
								arr[intIndex].Type = 0;
								intIndex += 1;
							}
							if (rsReceipt.Get_Fields_Double("CheckAmount") != 0)
							{
								// Add Check Payment Line(s)
								rsCRecord.OpenRecordset("SELECT * FROM CheckMaster WHERE ReceiptNumber = " + FCConvert.ToString(lngReceiptKey), modExtraModules.strCRDatabase);
								if (rsCRecord.RecordCount() > 0)
								{
									rsCRecord.MoveFirst();
									while (!rsCRecord.EndOfFile())
									{
										AddCreditAmounttoArray(rsCRecord, FCConvert.ToInt32(rsCRecord.Get_Fields_Int32("ID")), true);
										rsCRecord.MoveNext();
									}
								}
							}
							if (rsReceipt.Get_Fields_Double("CardAmount") != 0)
							{
								// Add Credit Payment Line(s)
								rsCRecord.OpenRecordset("SELECT * FROM CCMaster WHERE ReceiptNumber = " + FCConvert.ToString(lngReceiptKey), modExtraModules.strCRDatabase);
								if (rsCRecord.RecordCount() > 0)
								{
									rsCRecord.MoveFirst();
									while (!rsCRecord.EndOfFile())
									{
										AddCreditAmounttoArray(rsCRecord, FCConvert.ToInt32(rsCRecord.Get_Fields_Int32("ID")), false);
										rsCRecord.MoveNext();
									}
								}
							}
						}
						if (modUseCR.ProcessPayment_Void(ref intCount))
						{
							ShowFrame(fraSummary);
						}
						else
						{
							MessageBox.Show("Unable to process void", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else
					{
						MessageBox.Show("Receipt Information not found in the database.", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtReceiptNumber.Text = "";
						txtReceiptNumber.Focus();
					}
				}
				else
				{
					MessageBox.Show("Receipt Number not found in the database.", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtReceiptNumber.Text = "";
					txtReceiptNumber.Focus();
				}
			}
			else
			{
				MessageBox.Show("You must enter a receipt number to process.", "No Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtReceiptNumber.Focus();
			}
			EndProcess:
			;
			cmdAddReceipt.Enabled = true;
			cmdSummary.Enabled = true;
		}

		private void frmReceiptInput_Activated(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (boolLoadingUTsearch)
			{
				frmGetMasterAccount.InstancePtr.Show(App.MainForm);
				frmGetMasterAccount.InstancePtr.txtGetAccountNumber.Focus();
			}
			if (modGlobal.FormExist(this))
				return;
			if (boolLoad)
			{
				if (!boolLoaded)
				{
					// checks to see if the form is already loaded
					if (modGlobal.Statics.gstrTeller != "N")
					{
						// if the town uses the teller information
						Check_Teller();
						// this shows the teller frame and prompts the user to input thier ID
					}
					else
					{
						// town does not use the teller information
						StartUp();
						// start the Input Screen
					}
					boolLoaded = true;
					//FC:FINAL:AM:#195 - don't focus the form
					//this.Focus();
				}
				else
				{
					if (intCurFrame == 1)
					{
						if (!RestrictedCode(ref lngCurType))
						{
							if (cmbType.Visible && cmbType.Enabled)
							{
								cmbType.Focus();
							}
							else
							{
								txtFirst.Focus();
							}
						}
						else
						{
							this.Focus();
						}
					}
				}
				blnIsSale = true;
				// Default Setting ; Tracker Reference: 16641
			}
			else
			{
				Close();
			}
		}

		private void frmReceiptInput_Click(object sender, System.EventArgs e)
		{
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
		}

		private void frmReceiptInput_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			bool boolFound;
			int I;
			string strTemp = "";
			switch (KeyCode)
			{

				case Keys.Escape:
					{
						KeyCode = (Keys)0;

						if (fraPaymentInfo.Visible)
						{
							fraPaymentInfo.Visible = false;
							fraPaidBy.Visible = true;
							// kgk 12-29-2011  trocr-311
						}
						else
						{
							boolUnloadOK = false;
							Close();
						}
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmReceiptInput_Load(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			boolInValidate = false;
			// make sure that frmReprint is not loaded
			// MsgBox "Begin Form Load"
			boolLoad = true;
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == "frmReprint")
				{
					if (frm.Visible == true)
					{
						MessageBox.Show("You cannot reprint while Reprint Receipt is open.", "Multiple Window Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						boolLoad = false;
					}
					break;
				}
			}
			modGlobalFunctions.SetTRIOColors(this);
			txtFirst.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			//txtTotalDue.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			if (boolLoad)
			{
				// this will size the form to the MDIParent
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modUseCR.Statics.lngRunningCount = 0;
				lngReceiptCreated = 0;
				FCUtils.EraseSafe(modUseCR.Statics.ReceiptArray);
				modUseCR.Statics.PaymentBreakdownInfoArray = new modUseCR.PaymentBreakdownInfo[0 + 1];
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				modUseCR.Statics.PaymentBreakdownInfoArray[0] = new TWCR0000.modUseCR.PaymentBreakdownInfo(0);
				modUseCR.Statics.intPaymentBreakdownInfoCounter = 0;
				clsDefaultAcctBox.GRID7Light = txtDefaultCashAccount;
				clsAcctBox.GRID7Light = txtAcct1;
				clsAcctBox.DefaultAccountType = "R";
				boolSetAction = false;
				cboExpYear.Clear();
				for (counter = DateTime.Now.Year; counter <= DateTime.Now.Year + 5; counter++)
				{
					cboExpYear.AddItem(Strings.Format(counter - 2000, "00"));
				}
				// kgk 11-10-2011 trocr-284  Option to show routing numbers in bank list
				rsTemp.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
				boolShowRoutingNumbers = FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("ShowRoutingNumbers"));
                if (setCont.GetSettingValue("PayPortInterface","","","","SystemSettings").ToLower() == "true")
                {
                    modGlobalConstants.Statics.gboolPayPort = true;
                }
                else
                {
                    modGlobalConstants.Statics.gboolPayPort = false;
                }
			}
			//FC:FINAL:SBE - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vsSummary.ColAllowedKeys(5, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
			//FC:FINAL:SBE - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vsFees.ColAllowedKeys(2, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
			//FC:FINAL:SBE - #875 - disable autoformat of Frame caption
			this.fraAutoAction.FormatCaption = false;
			for (int i = 0; i < modStatusPayments.MAX_PAYMENTS; i++)
			{
				this.arr[i] = new CashOut_Payment(0);
				modUseCR.Statics.ReceiptArray[i] = new modUseCR.Receipt(0);
			}
		}

		private void Check_Teller()
		{
			// this sub will show the teller frame, display the last teller logged in and ask for the correct teller
			string strID = "";
			fraTeller.Visible = true;
			if (modGlobal.Statics.gstrTeller == "Y")
			{
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "CRLastTellerID", ref strID);
				txtTellerID.Text = strID;
				txtTellerID.SelectionStart = 0;
				txtTellerID.SelectionLength = txtTellerID.Text.Length;
			}
			else if (modGlobal.Statics.gstrTeller == "M")
			{
				// no default, no password
			}
			else
			{
				// no default, use password
				//txtPassword.Top = txtTellerID.Top + txtTellerID.Height + 200;
				txtPassword.Visible = true;
				fraTeller.Height = txtPassword.Top + txtPassword.Height + 20;
			}
			ShowFrame(fraTeller);
			txtTellerID.Focus();
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int intRow;
			if (boolLoad)
			{
				// do not let the user keep the form open that they shelled to from the window they are now closing
				// check to see if there are any reversals from RE/PP
				if (vsSummary.Rows > 1)
				{
					if (boolUnloadOK)
					{
						EndReceipt();
						modGlobal.LogOutTeller_2(Strings.Trim(txtTellerID.Text + ""));
						if (modGlobal.IsFormLoaded_2("frmRECLStatus"))
						{
							frmRECLStatus.InstancePtr.boolUnloadOK = true;
							frmRECLStatus.InstancePtr.Close();
						}
						if (modGlobal.IsFormLoaded_2("frmCLGetAccount"))
						{
							frmRECLStatus.InstancePtr.boolUnloadOK = true;
							frmCLGetAccount.InstancePtr.Close();
						}
						if (modGlobal.IsFormLoaded_2("frmUTCLStatus"))
						{
							frmUTCLStatus.InstancePtr.boolUnloadOK = true;
							frmUTCLStatus.InstancePtr.Close();
						}
						if (modGlobal.IsFormLoaded_2("frmGetMasterAccount"))
						{
							frmUTCLStatus.InstancePtr.boolUnloadOK = true;
							frmGetMasterAccount.InstancePtr.Close();
						}
						// MAL@20081119: Add check for AR status form
						// Tracker Reference: 14057
						if (modGlobal.IsFormLoaded_2("frmARCLStatus"))
						{
							frmARCLStatus.InstancePtr.boolUnloadOK = true;
							frmARCLStatus.InstancePtr.Close();
						}
						if (modGlobal.IsFormLoaded_2("frmARGetMasterAccount"))
						{
							frmARCLStatus.InstancePtr.boolUnloadOK = true;
							frmARGetMasterAccount.InstancePtr.Close();
						}
						lngReceiptCreated = 0;
						// cmdCashOut.Caption = "Enter"
						// sets the unload boolean
						boolLoaded = false;
					}
					else
					{
						// kgk 02-16-11 TROCR-271  Don't allow user to cancel if EPayments have been processed
						if (vsPayments.Rows > 1)
						{
							for (intRow = 1; intRow <= vsPayments.Rows - 1; intRow++)
							{
								if (FCConvert.CBool(vsPayments.TextMatrix(intRow, 10)))
								{
									MessageBox.Show("Credit card payments have been processed. The receipt cannot be canceled.");
									e.Cancel = true;
									break;
								}
							}
							// intRow
						}
						if (e.Cancel != true)
						{
							// kgk Skip if Canceling unload
							if (MessageBox.Show("There are pending transactions.  Do you want to exit without processing?", "Exit Screen", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								modGlobal.LogOutTeller_2(Strings.Trim(txtTellerID.Text + ""));
								RemoveNONCRPayments();
								EndReceipt();
								if (modGlobal.IsFormLoaded_2("frmRECLStatus"))
								{
									frmRECLStatus.InstancePtr.boolUnloadOK = true;
									frmRECLStatus.InstancePtr.Close();
								}
								if (modGlobal.IsFormLoaded_2("frmCLGetAccount"))
								{
									frmRECLStatus.InstancePtr.boolUnloadOK = true;
									frmCLGetAccount.InstancePtr.Close();
								}
								if (modGlobal.IsFormLoaded_2("frmUTCLStatus"))
								{
									frmUTCLStatus.InstancePtr.boolUnloadOK = true;
									frmUTCLStatus.InstancePtr.Close();
								}
								if (modGlobal.IsFormLoaded_2("frmGetMasterAccount"))
								{
									frmUTCLStatus.InstancePtr.boolUnloadOK = true;
									frmGetMasterAccount.InstancePtr.Close();
								}
								// MAL@20081119: Add check for AR status form
								// Tracker Reference: 14057
								if (modGlobal.IsFormLoaded_2("frmARCLStatus"))
								{
									frmARCLStatus.InstancePtr.boolUnloadOK = true;
									frmARCLStatus.InstancePtr.Close();
								}
								if (modGlobal.IsFormLoaded_2("frmARGetMasterAccount"))
								{
									frmARCLStatus.InstancePtr.boolUnloadOK = true;
									frmARGetMasterAccount.InstancePtr.Close();
								}
								lngReceiptCreated = 0;
								// cmdCashOut.Caption = "Enter"
								// sets the unload boolean
								boolLoaded = false;
							}
							else
							{
								e.Cancel = true;
							}
						}
					}
				}
				else
				{
					EndReceipt();
					modGlobal.LogOutTeller_2(Strings.Trim(txtTellerID.Text + ""));
					if (modGlobal.IsFormLoaded_2("frmRECLStatus"))
					{
						// frmRECLStatus.boolUnloadOK = True
						frmRECLStatus.InstancePtr.Close();
					}
					if (modGlobal.IsFormLoaded_2("frmCLGetAccount"))
					{
						// frmRECLStatus.boolUnloadOK = True
						frmCLGetAccount.InstancePtr.Close();
					}
					if (modGlobal.IsFormLoaded_2("frmUTCLStatus"))
					{
						frmUTCLStatus.InstancePtr.boolUnloadOK = true;
						frmUTCLStatus.InstancePtr.Close();
					}
					if (modGlobal.IsFormLoaded_2("frmGetMasterAccount"))
					{
						frmUTCLStatus.InstancePtr.boolUnloadOK = true;
						frmGetMasterAccount.InstancePtr.Close();
					}
					// MAL@20081119: Add check for AR status form
					// Tracker Reference: 14057
					if (modGlobal.IsFormLoaded_2("frmARCLStatus"))
					{
						frmARCLStatus.InstancePtr.boolUnloadOK = true;
						frmARCLStatus.InstancePtr.Close();
					}
					if (modGlobal.IsFormLoaded_2("frmARGetMasterAccount"))
					{
						frmARCLStatus.InstancePtr.boolUnloadOK = true;
						frmARGetMasterAccount.InstancePtr.Close();
					}
					lngReceiptCreated = 0;
					// cmdCashOut.Caption = "Enter"
					// sets the unload boolean
					boolLoaded = false;
				}
			}
			if (e.Cancel == false)
			{
				// LogOutTeller (Trim$(txtTellerID.Text & ""))
				boolBMV = false;
				MDIParent.InstancePtr.boolLockReceiptForms = false;
				lngCurType = 0;
			}
		}

		private void frmReceiptInput_Resize(object sender, System.EventArgs e)
		{
			if (fraTotal.Visible)
			{
				ShowFrame(fraTotal);
			}
			if (fraBank.Visible)
			{
				ShowFrame(fraBank);
			}
			if (fraCashOut.Visible)
			{
				ShowFrame(fraCashOut);
			}
			if (fraReceipt.Visible)
			{
				ShowFrame(fraReceipt);
				// fraTotal.Top = fraReceipt.Top + fraReceipt.Height
			}
			if (fraSummary.Visible)
			{
				ShowFrame(fraSummary);
				// fraTotal.Top = fraSummary.Top + fraSummary.Height
			}
			if (fraTeller.Visible)
			{
				ShowFrame(fraTeller);
			}
			if (fraPaymentInfo.Visible)
			{
				ShowFrame(fraPaymentInfo);
				FormatPaymentInfoGrid_2(false);
			}
			if (fraVoid.Visible)
			{
				ShowFrame(fraVoid);
			}
			if (fraCreditReceiptNum.Visible)
			{
				ShowFrame(fraCreditReceiptNum);
			}
		}

		private void fraCashOut_Click(object sender, System.EventArgs e)
		{
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
		}

		private void fraPaymentInfo_Click(object sender, System.EventArgs e)
		{
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
		}

		private void fraSummary_Click(object sender, System.EventArgs e)
		{
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
		}

		private void lblAcctDescription_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			ToolTip1.SetToolTip(lblAcctDescription, lblAcctDescription.Text);
		}

		private void mnuFileBack_Click(object sender, System.EventArgs e)
		{
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
			MoveBack();
		}

		private void mnuFileCashOut_Click(object sender, System.EventArgs e)
		{
			if (cmdCashOut.Visible && cmdCashOut.Enabled)
			{
				cmdCashOut_Click();
			}
		}

		private void mnuFileEdit_Click(object sender, System.EventArgs e)
		{
			EditReceiptInformation();
		}

		private void mnuFileNext_Click(object sender, System.EventArgs e)
		{
			cmdSummary.Enabled = false;
			cmdAddReceipt.Enabled = false;
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
			MoveNext();
			cmdSummary.Enabled = true;
			cmdAddReceipt.Enabled = true;
		}

		public void mnuFileNext_Click()
		{
			mnuFileNext_Click(cmdSummary, new System.EventArgs());
		}

		private void mnuFileVoidByReceipt_Click(object sender, System.EventArgs e)
		{
			// Call Reference: 92037
			blnIsSale = false;
			blnIsVoid = true;
			ShowFrame(fraVoid);
			txtReceiptNumber.Focus();
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			boolUnloadOK = false;
			Close();
			// this will call the unload event
		}

		private void mnuProcessPayment_Click(object sender, System.EventArgs e)
		{
			bool boolClear = false;
			cmdSummary.Enabled = false;
			cmdAddReceipt.Enabled = false;
			cmdVoidByReceipt.Enabled = false;
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
			// this will add the current receipt information to the summary table
			if (fraReceipt.Visible)
			{
				boolClear = modUseCR.ProcessPayment_2(FCConvert.ToInt16(FCConvert.ToBoolean(Conversion.Val(Strings.Left(FCConvert.ToString(cmdAddReceipt.Tag), 3)))));
				// the mnuProcessPayment tag will be 0 if this is a new tag and the index otherwise
				ProcessType_2(true);
			}
			else
			{
			}
			if (boolClear)
			{
				// clear the receipt information
				ClearReceiptInput();
			}
			cmdSummary.Enabled = true;
			cmdAddReceipt.Enabled = true;
		}

		public void MoveBack()
		{
			try
			{
				// On Error GoTo ERROR_HANLDER
				int lngRW;
				switch (intCurFrame)
				{
					case 0:
						{
							break;
						}
					case 1:
						{
							SetMenuOptions_2(0);
							break;
						}
					case 2:
						{
							// check to make sure that there are no batch payments
							for (lngRW = 1; lngRW <= vsSummary.Rows - 1; lngRW++)
							{
								if (vsSummary.TextMatrix(lngRW, 2) == "***" && Conversion.Val(vsSummary.TextMatrix(lngRW, 0)) >= 90 && Conversion.Val(vsSummary.TextMatrix(lngRW, 0)) < 99)
								{
									// do not allow the move back
									MessageBox.Show("Cannot add more payments after a batch payment.", "Move Back Not Seccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							SetMenuOptions_2(1);
							ShowFrame(fraReceipt);
							ShowFrame(fraTotal);
							fraSummary.Visible = false;
							fraCashOut.Visible = false;
							ClearReceiptInput();
							break;
						}
					case 3:
						{
							// go back to the summary screen
							fraCashOut.Visible = false;
							intCurFrame = 2;
							SetMenuOptions_2(2);
							ShowFrame(fraSummary);
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANLDER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Moving Back", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void MoveNext()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool boolFound = false;
				int intCT;
				string strTemp = "";
				int lngTemp = 0;
				bool boolZeroReceipt = false;
				// test to see if this is at the cash out screen
				if (fraCashOut.Visible)
				{
					cmdCashOut_Click();
					return;
				}
				// open the test output file
				modRegistry.GetUserNameWrp(ref strTemp, ref lngTemp);
				if (frmReceiptInput.InstancePtr.vsFees.Rows > 0)
				{
					// this will force any changed fees to be accepted
					frmReceiptInput.InstancePtr.vsFees.Select(0, 0);
				}
				modUseCR.CalculateSummaryTotal();
				if (intCurFrame == 1)
				{
					boolFound = false;
					cmdBack.Enabled = true;
					cmdBack.Visible = true;

					cmdAddReceipt.Visible = false;
					boolZeroReceipt = FCConvert.CBool(chkZeroReceipt.CheckState == Wisej.Web.CheckState.Checked);
					// Print #11, "MoveNext - 05 - " & Time & " - Menu options ON"
					if (!boolZeroReceipt)
					{
						for (intCT = 1; intCT <= vsFees.Rows - 1; intCT++)
						{
							if (Conversion.Val(vsFees.TextMatrix(intCT, 2).Replace("%", "")) != 0)
							{
								boolFound = true;
								break;
							}
						}
					}
					else
					{
						for (intCT = 1; intCT <= vsFees.Rows - 1; intCT++)
						{
							if (Conversion.Val(vsFees.TextMatrix(intCT, 2).Replace("%", "")) != 0)
							{
								MessageBox.Show("You may not have any amounts entered and also have zero receipt checked off.  You will need to correct this before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
						}
						boolFound = true;
					}
                    //if (cmbType.Text != "")
					if (cmbType.Text != "")
					{
      //                  cmbTypeItem = cmbType.Text;
						//if (Strings.Trim(cmbType.Text) != "" && boolFound == true)
						cmbTypeItem = cmbType.Text;
						if (Strings.Trim(cmbType.Text) != "" && boolFound == true)
						{
                            //if (fraReceipt.Visible && !RestrictedCode_2(FCConvert.ToInt32(Conversion.Val(cmbType.Text))))
							if (fraReceipt.Visible && !RestrictedCode_2(FCConvert.ToInt32(Conversion.Val(cmbType.Text))))
							{
								if (modUseCR.ProcessPayment_2(FCConvert.ToInt16(Conversion.Val(Strings.Left(FCConvert.ToString(cmdAddReceipt.Tag), 3)))) != true)
								{
									// the mnuProcessPayment tag will be 0 if this is a new tag and the index otherwise
									return;
									// did not process....
								}
							}
							else
							{
								// Print #11, "MoveNext - 10 - " & Time & " - Start Show/Hide Frames"
								ShowFrame(fraReceipt);
								ShowFrame(fraTotal);
								fraSummary.Visible = false;
								fraCashOut.Visible = false;
								// Print #11, "MoveNext - 11 - " & Time & " - End Show/Hide Frames"
							}

							strTemp = "";
							if (vsSummary.Rows == 2 && txtFirst.Text != "")
							{
								// is there only one row with a name in the Name field (base case)
								txtPaidBy.Text = txtFirst.Text;
								// then just take the name from the Name field
								txtCustNum_PaidBy.Text = txtCustNum_Name.Text;
								// TAKE THE DEFAULT Party ID FROM HERE!!
								if (Information.IsNumeric(txtCustNum_PaidBy.Text) && Conversion.Val(txtCustNum_PaidBy.Text) > 0)
								{
									// kk01232015 trouts-134 Make sure the central party stuff is setup correctly
									GetPartyInfo_8(FCConvert.ToInt32(FCConvert.ToDouble(txtCustNum_PaidBy.Text)), false);
								}
							}
							else
							{
								if (vsSummary.FindRow("***", 2) != -1)
								{
									// is there a batch in the list
									txtPaidBy.Text = vsSummary.TextMatrix(vsSummary.FindRow("***", 2), 4);
									// if so, then take the name from that
								}
								else
								{
									if (vsSummary.Rows > 1)
									{
										txtPaidBy.Text = vsSummary.TextMatrix(1, 4);
										// else take the first rows name
									}
								}
							}

							fraReceipt.Visible = false;
							ShowFrame(fraSummary);
							ShowFrame(fraPaidBy);
							// Print #11, "MoveNext - 14 - " & Time & " - Summary Screen Shown"
						}
						else
						{
							// move to the summary screen w/o saving any info
							fraReceipt.Visible = false;
							ShowFrame(fraSummary);
							ShowFrame(fraPaidBy);
							// Print #11, "MoveNext - 21 - " & Time
						}
						// MAL@20071210: Tracker Reference 11157
						// MAL@20090309: Change to add option for all tax payments or just multiple ; Tracker Reference: 17448
						if (modGlobal.Statics.gboolClearPaidBy && modGlobal.Statics.gstrClearPaidByOption == "S")
						{
							//FC:FINAL:AM:#i500- the text is cleared out; use instead the saved variable
							//switch (FCConvert.ToInt32(cmbType.Text))
							if (!String.IsNullOrEmpty(cmbTypeItem))
							{
								switch (FCConvert.ToInt32(cmbTypeItem))
								{
									case 90:
									case 91:
									case 92:
									case 890:
									case 891:
										{
											txtPaidBy.Text = "";
											txtCustNum_PaidBy.Text = "";
											// k 10032012
											txtPaidBy.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
											break;
										}
									default:
										{
											// Do Nothing
											break;
										}
								}
							}
							//end switch
						}
						else if (modGlobal.Statics.gboolClearPaidBy && modGlobal.Statics.gstrClearPaidByOption == "M")
						{
							// DJW@20100623: Jira Reference TROCR-244
							// Check to see if there is just 1 payment with a discount and if so don't clear out the Paid By field
							if (vsSummary.Rows > 2)
							{
								//FC:FINAL:AM:#i500- the text is cleared out; use instead the saved variable
								//switch (FCConvert.ToInt32(cmbType.Text))
								if (!String.IsNullOrEmpty(cmbTypeItem))
								{
									switch (FCConvert.ToInt32(cmbTypeItem))
									{
										case 90:
										case 91:
										case 92:
										case 890:
										case 891:
											{
												if (vsSummary.Rows == 3)
												{
													if (modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(1, 8))].BillingYear == modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(2, 8))].BillingYear && (modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(1, 8))].CollectionCode == "D" || modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(2, 8))].CollectionCode == "D"))
													{
														// do nothing
													}
													else
													{
														txtPaidBy.Text = "";
														txtCustNum_PaidBy.Text = "";
														// k 10032012
														txtPaidBy.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
													}
												}
												else
												{
													txtPaidBy.Text = "";
													txtCustNum_PaidBy.Text = "";
													// k 10032012
													txtPaidBy.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
												}
												break;
											}
										default:
											{
												// Do Nothing
												break;
											}
									}
								}
								//end switch
							}
						}
					}
					else
					{
						fraReceipt.Visible = false;
						ShowFrame(fraSummary);
						ShowFrame(fraPaidBy);
						// Print #11, "MoveNext - 22 - " & Time
					}
					intCurFrame = 2;
				}
				else if (intCurFrame == 3)
				{
					// do nothing when the Cash Out screen is shown
				}
				else if (intCurFrame == 2)
				{
					// when the summary screen is being shown, then change to the Cash Out screen
					// Print #11, "MoveNext - 30 - " & Time & " - Receipt Summary To Cash Out Screen"
					if (modGlobal.Statics.gboolClearPaidBy)
					{
						//switch (FCConvert.ToInt32(cmbType.Text))
						//FC:FINAL:AM: #i496 - the ComboItem property in VB6 returns a valid value even if not in edit mode and the text has been cleared
						if (!String.IsNullOrEmpty(cmbTypeItem))
						{
							switch (FCConvert.ToInt32(cmbTypeItem))
							{
								case 90:
								case 91:
								case 92:
								case 890:
								case 891:
									{
										if (txtPaidBy.Text == "")
										{
											MessageBox.Show("The Paid By field is required", "Required Field", MessageBoxButtons.OK, MessageBoxIcon.Information);
											return;
										}
										break;
									}
								default:
									{
										// Do Nothing
										break;
									}
							}
						}
						//end switch
					}
					// make sure that the summary grid is not editable
					cmdBack.Enabled = true;
					cmdBack.Visible = true;
					// mnuProcessPayment.Enabled = False
					cmdAddReceipt.Visible = false;
					vsSummary.Editable = FCGrid.EditableSettings.flexEDNone;
					vsSummary.Select(0, 0);
					// Print #11, "MoveNext - 31 - " & Time
					// save the receipts
					Setup_CashOut();
					intCurFrame = 3;
					// Print #11, "MoveNext - 32 - " & Time & " - Cash Out Shown"
				}
				else if (intCurFrame == 0)
				{
					// the user hit F12 when on the Teller ID screen
					if (fraTeller.Visible)
					{
						cmdTeller_Click_2(0);
					}
					else
					{
						intCurFrame = 1;
					}
				}
				// Close #11
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// Close #11
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In MoveNext", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuProcessReceipt_Click()
		{
			mnuFileNext_Click();
		}

		private void optCashOutType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// this shows the type of payment
			switch (Index)
			{
				case 0:
					{
						lblCashOutType.Visible = false;
						lblCardBank.Visible = false;
						txtCashOutCheckNumber.Visible = false;
						cmbCashOutCC.Visible = false;
						cmbBank.Visible = false;
						// MAL@20081224: Add options for electronic payments
						// Tracker Reference: 16641
						fraTransDetailsCC.Visible = false;
						fraTransDetailsCheck.Visible = false;
						break;
					}
				case 1:
					{
						if (cmbBank.Items.Count < 1)
						{
							MessageBox.Show("There are no banks setup in this system.  Please go to bank setup.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							cmbCashOutType.SelectedIndex = 0;
							txtPayment.Focus();
						}
						else
						{
							if (cmbBank.Items.Count >= 2)
							{
								// there is only one bank loaded so make that the default
								cmbBank.SelectedIndex = 1;
							}
							else
							{
								cmbBank.SelectedIndex = 0;
							}
							lblCashOutType.Text = "Check #:";
							lblCardBank.Text = "Bank:";
							// DJW 02022010 Commented out section because it does not seem like the check field should ever be invisible
							// If gstrEPaymentPortal = "N" Then
							lblCardBank.Visible = true;
							txtCashOutCheckNumber.Visible = true;
							lblCashOutType.Visible = true;
							cmbBank.Visible = true;
							cmbCashOutCC.Visible = false;
							// MAL@20081224: Add options for electronic payments
							// Tracker Reference: 16641
							if (modGlobal.Statics.gstrEPaymentPortal != "N" && modGlobal.Statics.gblnAcceptEChecks)
							{
								if (blnIsSale)
								{
									cmbCheckType.Clear();
									cmbCheckType.AddItem("Business");
									cmbCheckType.AddItem("Personal");
									cmbCheckAcctType.Clear();
									cmbCheckAcctType.AddItem("Checking");
									cmbCheckAcctType.AddItem("Savings");
									fraTransDetailsCC.Visible = false;
									fraTransDetailsCheck.Visible = true;
								}
								else
								{
									// Continue
								}
							}
							else
							{
								fraTransDetailsCC.Visible = false;
								fraTransDetailsCheck.Visible = false;
							}
						}
						break;
					}
				case 2:
					{
						lblCardBank.Text = "Card Type:";
						lblCardBank.Visible = true;
						lblCashOutType.Visible = false;
						txtCashOutCheckNumber.Visible = false;
						cmbCashOutCC.Visible = true;
						cmbBank.Visible = false;
						// MAL@20081224: Add options for electronic payments
						// Tracker Reference: 16641
						// kgk 01-27-2012  Add InforME   'kgk 09-29-2011  Add Invoice Cloud
						if (modGlobal.Statics.gstrEPaymentPortal != "N" && modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
						{
							if (blnIsSale)
							{
								fraTransDetailsCC.Visible = true;
								fraTransDetailsCheck.Visible = false;
								if (modGlobal.Statics.gstrEPaymentPortal == "G")
								{
									// Go e-Merchant requires address for validation
									if (frmEPaymentAddress.InstancePtr.Init())
									{
										frmEPaymentAddress.InstancePtr.Show();
									}
									// Validate Return Values
									if (CCAddressValid())
									{
										// Continue
									}
									else
									{
										MessageBox.Show("You cannot select Credit as a transaction type without entering address information", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
										cmbCashOutType.SelectedIndex = 0;
									}
								}
							}
							else
							{
								// Continue
							}
						}
						else
						{
							fraTransDetailsCC.Visible = false;
							fraTransDetailsCheck.Visible = false;
						}
						break;
					}
			}
			//end switch
		}

		private void optCashOutType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbCashOutType.SelectedIndex;
			optCashOutType_CheckedChanged(index, sender, e);
		}

		private void optCashOutType_KeyPress(int Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.Up:
					{
						KeyAscii = (Keys)0;
						if (cmbCashOutType.SelectedIndex == ((Index + 1) % 3))
						{
							cmbCashOutType.Focus();
						}
						break;
					}
				case Keys.Down:
					{
						KeyAscii = (Keys)0;
						if (cmbCashOutType.SelectedIndex == (3 - ((3 - Index) % 3)))
						{
							cmbCashOutType.Focus();
						}
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void optCashOutType_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int index = cmbCashOutType.SelectedIndex;
			optCashOutType_KeyPress(index, sender, e);
		}

		private void txtAcct1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (!boolAccountBoxClear && modGlobalConstants.Statics.gboolBD)
			{
				modAccountTitle.DetermineAccountTitle(txtAcct1.EditText, ref lblAcctDescription);
			}
		}

		private void txtBankName_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.Right:
					{
						// single quote
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtBankRT_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.Right:
					{
						// single quote
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCCNum_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// MAL@20090505: Add check for swiped credit card
			// Tracker Reference: 16641
			if (txtCCNum.Text.Length > 19)
			{
				// Swiped Card
				txtTrack2.Text = txtCCNum.Text;
				txtCCNum.Text = "";
				blnIsSwipe = true;
				ProcessSwipedCard();
			}
			else
			{
				// Manually Entered Card Number
				blnIsSwipe = false;
			}
		}

		private void txtComment_Enter(object sender, System.EventArgs e)
		{
			txtComment.SelectionStart = 0;
			txtComment.SelectionLength = txtComment.Text.Length;
		}

		private void txtFirst_Enter(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			txtFirst.SelectionStart = 0;
			txtFirst.SelectionLength = txtFirst.Text.Length;
		}

		private void txtFirst_Leave(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtCustNum_Name.Text) != 0)
			{
				Validate_Party_2(true);
			}
		}

		private void txtPaidBy_Enter(object sender, System.EventArgs e)
		{
			txtPaidBy.SelectionStart = 0;
			txtPaidBy.SelectionLength = txtPaidBy.Text.Length;
		}

		private void txtPaidBy_Leave(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtCustNum_PaidBy.Text) != 0)
			{
				Validate_Party_2(false);
			}
		}

		private void txtPassword_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
			// check for the return and escape ID
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						cmdTeller_Click_2(0);
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						cmdTeller_Click_2(1);
						break;
					}
			}
			//end switch
		}

		private void txtPayment_Enter(object sender, System.EventArgs e)
		{
			txtPayment.SelectionStart = 0;
			txtPayment.SelectionLength = txtPayment.Text.Length;
		}

		private void txtPayment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert))
			{
			}
			else if ((KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Delete))
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtPayment.Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtPayment.SelectionStart < lngDecPlace && txtPayment.SelectionLength + txtPayment.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPercentageAmount_Enter(object sender, System.EventArgs e)
		{
			txtPercentageAmount.SelectionStart = 0;
			txtPercentageAmount.SelectionLength = txtPercentageAmount.Text.Length;
		}

		private void txtPercentageAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert))
			{
			}
			else if ((KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Delete))
			{
				if (Strings.InStr(1, txtPercentageAmount.Text, ".", CompareConstants.vbBinaryCompare) != 0)
				{
					// block the second decimal
					KeyAscii = (Keys)0;
				}
				else
				{
					// allow this
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPercentageAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtPercentageAmount.Text = Strings.Format(txtPercentageAmount.Text, "#,##0.00");
			modUseCR.CalculateSummaryTotal();
		}

		private void txtTellerID_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
			// check for the return and escape ID
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						cmdTeller_Click_2(0);
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						cmdTeller_Click_2(1);
						break;
					}
			}
			//end switch
		}

		private void txtTellerID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// change the letters to uppercase
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillComboType()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill all of the types except for the ones w/o titles into cmbType
				int Number = 0;
				string Name = "";
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intCT;
				string strOrderBy = "";
				// MAL@20080123: Add check for sort type
				// Call Reference: 93837
				if (modGlobalConstants.Statics.gblnSortTypeAlpha)
				{
					strOrderBy = "ORDER BY TypeTitle";
				}
				else
				{
					strOrderBy = "ORDER BY TypeCode";
				}
				//cmbType.ColWidth(0, 100);
				//cmbType.ExtendLastCol = true;
				intCT = 0;
				rsTemp.DefaultDB = modGlobal.Statics.DatabaseName;
				FCUtils.EraseSafe(modUseCR.Statics.strTypeDescriptions);
				// type
				if (!modGlobal.Statics.gboolMultipleTowns)
				{
					// MAL@20071010
					rsTemp.OpenRecordset("SELECT * FROM Type WHERE isnull(TypeTitle, 'NULL') <> 'NULL' AND TypeTitle <> ''AND Townkey = 0 AND Deleted = 0 " + strOrderBy);
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM Type WHERE isnull(TypeTitle, 'NULL') <> 'NULL' AND TypeTitle <> ''AND Townkey = 1 AND Deleted = 0 " + strOrderBy);
				}
				if (rsTemp.BeginningOfFile() != true && rsTemp.EndOfFile() != true)
				{
					rsTemp.MoveFirst();
					strTemp += "#" + FCConvert.ToString(intCT) + ";" + "" + "\t" + "" + "|";
					intCT += 1;
					do
					{
						// this fills all of the types into the combobox
						Number = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("TypeCode"));
						Name = FCConvert.ToString(rsTemp.Get_Fields_String("TypeTitle"));
						// kgk 01-04-2012 trocr-146   Add logic to hide types for unauth modules
						switch (Number)
						{
							case 90:
							case 91:
							case 92:
							case 890:
							case 891:
								{
									// Tax Collections
									if (!modGlobalConstants.Statics.gboolCL)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 93:
							case 94:
							case 95:
							case 96:
							case 893:
							case 894:
							case 895:
							case 896:
								{
									// Utility Billing
									if (!modGlobalConstants.Statics.gboolUT)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 97:
								{
									// Accounts Receivable
									if (!modGlobalConstants.Statics.gboolAR)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 98:
							case 800:
							case 801:
							case 802:
							case 803:
							case 804:
								{
									// Clerk
									if (!modGlobalConstants.Statics.gboolCK)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 99:
								{
									// Motor Vehicles
									if (!modGlobalConstants.Statics.gboolMV && !modGlobalConstants.Statics.gboolRB)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
							case 190:
								{
									// MOSES
									if (!modGlobalConstants.Statics.gboolMS)
									{
										Number = 0;
										Name = "";
									}
									break;
								}
						}
						//end switch
						if (Number != 0)
						{
							modUseCR.Statics.strTypeDescriptions[Number] = Name;
							strTemp += "#" + FCConvert.ToString(intCT) + ";" + FCConvert.ToString(Number) + "\t" + Name + "|";
							intCT += 1;
						}
						rsTemp.MoveNext();
					}
					while (!rsTemp.EndOfFile());
					if (strTemp.Length > 0)
					{
						strTemp = Strings.Left(strTemp, strTemp.Length - 1);
					}
                    //cmbType.ComboList = strTemp;
                    #region datasource
                    string[] tokens = strTemp.Split('|');
                    var comboListItems = new System.Collections.Generic.List<string>();

                    for (int i = 0; i < tokens.Length; i++)
                    {
                        string token = tokens[i];
                        if (i == 0)
                        {
                            if (string.IsNullOrEmpty(token))
                            {
                                continue;
                            }
                        }
                        comboListItems.Add(token);

                    }

                    cmbType.DropDownStyle = strTemp.StartsWith("|") ? ComboBoxStyle.DropDown : ComboBoxStyle.DropDownList;
                    cmbType.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    cmbType.DataSource = comboListItems;
                    
                    if (cmbType.Items.Count > 0)
                    {
                        int itemsHeight = cmbType.ListView.Items.FirstOrDefault().RowHeight * cmbType.Items.Count + 5;
                        if (itemsHeight < cmbType.DropDownHeight)
                        {
                            cmbType.DropDownHeight = itemsHeight;
                        }
                        else
                        {
                            cmbType.DropDownHeight -= 10;
                        }
                    }

                    int comboColsWidth = cmbType.Columns.Sum(x => x.Width);
                    if (comboColsWidth < cmbType.Width)
                    {
                        cmbType.DropDownWidth = cmbType.Width + 20;
                        if (cmbType.Columns.Count == 1)
                        {
                            cmbType.Columns[0].Width = cmbType.Width;
                        }
                    }
                    else
                    {
                        cmbType.DropDownWidth = comboColsWidth + 20;
                    }
                    if (cmbType.Columns.Count > 1)
                    {
                        for (int i = 1; i < cmbType.Columns.Count; i++)
                        {
                            cmbType.Columns[i].Resizable = false;
                        }
                    }
                    #endregion
                }
				else
				{
					MessageBox.Show("No types were loaded into the combobox, please go to 'Receipt Type Setup' and create one.", "No Receipt Types Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatGrid()
		{
			// sets all of the widths, datatypes and titles for the grid
			int Width = 0;
			vsFees.Cols = 5;
			//vsFees.Select(0, 0);
			vsFees.EditMaxLength = 12;
			Width = vsFees.WidthOriginal;
			vsFees.ColWidth(0, 0);
			// Category Number
			vsFees.ColWidth(1, FCConvert.ToInt32(Width * 0.61));
			// Title
			vsFees.ColWidth(2, FCConvert.ToInt32(Width * 0.37));
			// Amount
			vsFees.ColWidth(3, 0);
			// Year: -1 = True or 0 = False
			vsFees.ColWidth(4, 0);
			// this will have a "%" in it if this is a percentage and a "$" if it is a dollar amount
			// .ColFormat(2) = "#,##0.00"
			//vsFees.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//FC:FINAL:DDU:#1193 - aligned column headers
			//vsFees.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsFees.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFees.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			// sets the titles for the flexgrid
			vsFees.TextMatrix(0, 0, "Fee");
			vsFees.TextMatrix(0, 1, "Title");
			vsFees.TextMatrix(0, 2, "Amount");
			vsFees.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 2, true);
			vsSummary.Cols = 10;
			Width = vsSummary.WidthOriginal;
			vsSummary.ColWidth(0, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.06));
			// Type Code
			vsSummary.ColWidth(1, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.22));
			// Type Name
			vsSummary.ColWidth(2, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.11));
			// Account
			vsSummary.ColWidth(3, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.11));
			vsSummary.ColWidth(4, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.21));
			// Name
			vsSummary.ColWidth(5, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.08));
			// Copies
			// .ColWidth(6) = .Width * 0.15    'Total
			vsSummary.ColWidth(6, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.13));
			// Total
			// .ColWidth(7) = .Width * 0.07    'Combo box for receipt split up
			vsSummary.ColWidth(7, FCConvert.ToInt32(vsSummary.WidthOriginal * 0.06));
			// Combo box for receipt split up
			vsSummary.ColWidth(8, 0);
            vsSummary.ColWidth(9, 0);
			// Hidden Field - this is the index of the element in the receipt array
			vsSummary.ColFormat(6, "#,##0.00");
			vsSummary.ColComboList(7, "#1;1|#2;2|#3;3|#4;4|#5;5");
			//FC:FINAL:DDU:#1183 - aligned grid columns
			vsSummary.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsSummary.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSummary.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 7, true);
			// sets the titles for the flexgrid
			vsSummary.TextMatrix(0, 0, "Type");
			vsSummary.TextMatrix(0, 1, "Type Description");
			vsSummary.TextMatrix(0, 2, "Account");
			vsSummary.TextMatrix(0, 3, "Info");
			vsSummary.TextMatrix(0, 4, "Name");
			vsSummary.TextMatrix(0, 5, "Copies");
			vsSummary.TextMatrix(0, 6, "Amount");
			vsSummary.TextMatrix(0, 7, "Split");
			vsSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			vsSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			vsPayments.Cols = 12;
			Width = vsPayments.WidthOriginal;
			vsPayments.ColWidth(0, FCConvert.ToInt32(vsPayments.WidthOriginal * 0.21));
			// Type
			vsPayments.ColWidth(1, FCConvert.ToInt32(vsPayments.WidthOriginal * 0.41));
			// Amount
			vsPayments.ColWidth(2, FCConvert.ToInt32(vsPayments.WidthOriginal * 0.35));
			// If Check then Check Number, if Credit Card then Card Type
			vsPayments.ColWidth(3, 0);
			// Hidden Field --> Array Index
			vsPayments.ColWidth(4, 0);
			// Hidden Field --> Convenience Fee
			vsPayments.ColWidth(5, 0);
			// Hidden Field --> Credit Card Number
			vsPayments.ColWidth(6, 0);
			// Hidden Field --> Expiration Date
			vsPayments.ColWidth(7, 0);
			// Hidden Field --> Is VISA
			vsPayments.ColWidth(8, 0);
			// Hidden Field --> Is Debit Card
			vsPayments.ColWidth(9, 0);
			// Hidden Field --> Non Tax Payment Amount
			vsPayments.ColWidth(10, 0);
			// Hidden Field --> EPayment Already Processed
			vsPayments.ColWidth(11, 0);
			// Hidden Field --> Non Tax Convenience Fee
			vsPayments.ColFormat(1, "#,##0.00");
			//vsPayments.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsPayments.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 6, true);
			vsPayments.ExtendLastCol = true;
			// sets the titles for the flexgrid
			vsPayments.TextMatrix(0, 0, "Type");
			vsPayments.TextMatrix(0, 1, "Amount");
			vsPayments.TextMatrix(0, 2, "Ref");
			vsPayments.TextMatrix(0, 3, "");
			vsPayments.TextMatrix(0, 4, "");
			vsPayments.TextMatrix(0, 5, "");
			vsPayments.TextMatrix(0, 6, "");
			vsPayments.TextMatrix(0, 7, "");
			vsPayments.TextMatrix(0, 8, "");
			vsPayments.TextMatrix(0, 9, "");
			vsPayments.TextMatrix(0, 10, "");
			vsPayments.TextMatrix(0, 11, "");
		}

		private void txtTitle_Enter(short Index, object sender, System.EventArgs e)
		{
			txtTitle[Index].SelectionStart = 0;
			txtTitle[Index].SelectionLength = txtTitle[Index].Text.Length;
		}

		private void txtTitle_Enter(object sender, System.EventArgs e)
		{
			short index = txtTitle.GetIndex((FCTextBox)sender);
			txtTitle_Enter(index, sender, e);
		}

		private void txtTitle_KeyDown(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int intTotBoxes;
			int intNextBox = 0;
			bool boolBad = false;
			intTotBoxes = 4;
			switch (KeyCode)
			{
				case Keys.Right:
					{
						if (txtTitle[Index].SelectionStart == txtTitle[Index].Text.Length)
						{
							KeyCode = (Keys)0;
							txtTitle_Validate(Index, ref boolBad);
							if (boolBad)
							{
							}
							else
							{
								intNextBox = (Index + 1) % intTotBoxes;
								if (txtTitle[FCConvert.ToInt16(intNextBox)].Visible == true)
								{
									txtTitle[FCConvert.ToInt16(intNextBox)].Focus();
								}
								else
								{
									if (txtTitle[0].Visible == true)
									{
										txtTitle[0].Focus();
									}
									else
									{
										txtTitle[Index].Focus();
									}
								}
							}
						}
						break;
					}
				case Keys.Left:
					{
						if (txtTitle[Index].SelectionStart == 0)
						{
							KeyCode = (Keys)0;
							txtTitle_Validate(Index, ref boolBad);
							if (boolBad)
							{
							}
							else
							{
								intNextBox = ((Index - 1) + intTotBoxes) % intTotBoxes;
								if (txtTitle[FCConvert.ToInt16(intNextBox)].Visible == true)
								{
									txtTitle[FCConvert.ToInt16(intNextBox)].Focus();
								}
								else
								{
									if (txtTitle[0].Visible == true)
									{
										txtTitle[0].Focus();
									}
									else
									{
										txtTitle[Index].Focus();
									}
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void txtTitle_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txtTitle.GetIndex((FCTextBox)sender);
			txtTitle_KeyDown(index, sender, e);
		}

		private void txtTitle_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTitle_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			short index = txtTitle.GetIndex((FCTextBox)sender);
			txtTitle_KeyPress(index, sender, e);
		}

		private void txtTitle_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strTemp = "";
			if (intCurFrame < 2)
			{
				if (RestrictedCode_2(FCConvert.ToInt32(Conversion.Val(cmbType.Text))) == false)
				{
					if (FCConvert.ToString(txtTitle[Index].Tag) == "True" && !boolBMV)
					{
						// check to see if this field is required
						if (Strings.Trim(txtTitle[Index].Text) != "")
						{
						}
						else
						{
							MessageBox.Show("Please enter a value into the " + Strings.Trim(lblTitle[Index].Text) + " field.", "Input Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
						}
					}
					else if (boolBMV && Index != 2)
					{
						strTemp = txtTitle[Index].Text;
						if (Strings.Trim(strTemp) == "")
						{
							MessageBox.Show("Please enter a value into the " + Strings.Trim(lblTitle[Index].Text) + " field.", "Input Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
						}
						else
						{
							if (strTemp == "")
								return;
							if (Index == 1)
							{
								// Year Sticker validation          'kk07132015 trocr-332  Add C to sticker validation
								if ((Strings.Mid(strTemp, 3, 1) == "D" || Strings.Mid(strTemp, 3, 1) == "S" || Strings.Mid(strTemp, 3, 1) == "C") && strTemp.Length == 10)
								{
									// this checks for the (D)ouble or (S)ingle character and the total len of the string
									if (Information.IsNumeric(Strings.Mid(strTemp, 1, 2)) == true && Information.IsNumeric(Strings.Mid(strTemp, 4, 7)) == true)
									{
										e.Cancel = false;
									}
									else
									{
										e.Cancel = true;
									}
								}
								else
								{
									e.Cancel = true;
								}
							}
							else if (Index == 3)
							{
								// MVR3#
								if (CheckBMVType(ref strTemp))
								{
									e.Cancel = false;
								}
								else
								{
									e.Cancel = true;
									MessageBox.Show("Please enter an MVR3 number in the MVR3# box.", "Data Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
							}
							if (e.Cancel == true && (Index == 1))
							{
								if (Index == 1)
								{
									if (MessageBox.Show("Please enter a valid Year Sticker Number in the format YY(C, D or S)#######.", "Validation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
									{
										e.Cancel = true;
									}
									else
									{
										txtTitle[Index].Text = "";
										e.Cancel = false;
									}
								}
								else
								{
									e.Cancel = false;
								}
							}
						}
					}
				}
			}
		}

		public void txtTitle_Validate(short Index, ref bool Cancel)
		{
			txtTitle_Validating(Index, txtTitle[Index], new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTitle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txtTitle.GetIndex((FCTextBox)sender);
			txtTitle_Validating(index, sender, e);
		}

		private void txtTotalDue_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			KeyCode = (Keys)0;
		}

		private void vsFees_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int row = vsFees.Row, col = vsFees.Col;
			if (row > 0 && col > 0)
			{
				if (vsFees.RowHidden(row))
				{
					vsFees.EditText = "0.00";
					// this will check to see if this row has been hidden
					vsFees.TextMatrix(row, col, "0.00");
				}
				else
				{
					if (!modUseCR.Statics.boolClearingPaymentBoxes)
					{
						modUseCR.CalculateSummaryTotal();
					}
				}
			}
		}

		private void vsFees_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsFees.Col == 2)
			{
				vsFees.EditCell();
			}
		}

		private void vsFees_DblClick(object sender, System.EventArgs e)
		{
			if (vsFees.Col == 2)
			{
				vsFees.EditCell();
			}
		}

		private void vsFees_Enter(object sender, System.EventArgs e)
		{
			vsFees.Row = 1;
			vsFees.Col = 1;
		}

		private void vsFees_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsFees.Col == 2)
			{
				switch (e.KeyCode)
				{
					case Keys.Return:
						{
							// if return is pressed
							if (vsFees.Row < vsFees.Rows - 1)
							{
								// then move down a row
								vsFees.Row = vsFees.Row + 1;
								KeyCode = 0;
							}
							break;
						}
				}
				//end switch
			}
		}

		private void vsFees_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int lngDecPlace = 0;
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vsFees.Col == 2)
			{
				if ((keyAscii >= FCConvert.ToInt32(Keys.D0) && keyAscii <= FCConvert.ToInt32(Keys.D9)) || (keyAscii == FCConvert.ToInt32(Keys.Back)) || (keyAscii == 45))
				{
					// do nothing
				}
				else if (keyAscii == 46)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, vsFees.EditText, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (vsFees.EditSelStart < lngDecPlace && vsFees.EditSelLength + vsFees.EditSelStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							keyAscii = 0;
						}
					}
				}
				else
				{
					keyAscii = 0;
				}
			}
		}

		private void vsFees_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will change the edit property when the correct row is clicked...
				if (!modUseCR.Statics.boolClearingPaymentBoxes)
				{
					int lngType = 0;
					int lngBotRow;
					if (cmbType.Text != "")
					{
						lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbType.Text)));
						if (RestrictedCode(ref lngType))
						{
							vsFees.Editable = FCGrid.EditableSettings.flexEDNone;
							vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
						}
						else
						{
							if (vsFees.RowHidden(vsFees.Row))
							{
								if (vsFees.Row < vsFees.Rows - 1)
								{
									vsFees.Row += 1;
								}
							}
							else
							{
								switch (vsFees.Col)
								{
									case 1:
										{
											// .EditMaxLength = 30
											// .Editable = True
											vsFees.Col = 2;
											return;
										}
									case 2:
										{
											vsFees.EditMaxLength = 13;
											vsFees.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
											vsFees.EditCell();
											break;
										}
									default:
										{
											vsFees.Editable = FCGrid.EditableSettings.flexEDNone;
											break;
										}
								}
								//end switch
								// find the lowest row that is shown
								for (lngBotRow = vsFees.Rows - 1; lngBotRow >= 1; lngBotRow--)
								{
									if (vsFees.RowHeight(lngBotRow) != 0)
										break;
								}
								if (vsFees.Col == 2 && vsFees.Row == lngBotRow)
								{
									vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
								}
								else
								{
									vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
								}
							}
						}
					}
					else
					{
						vsFees.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fee Grid Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsFees_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsFees.GetFlexRowIndex(e.RowIndex);
			int col = vsFees.GetFlexColIndex(e.ColumnIndex);
			if (vsFees.TextMatrix(row, 4) == "%")
			{
				// this checks the hidden col to check the type
				vsFees.EditText = Strings.Format(Conversion.Val(vsFees.EditText.Replace("%", "")), "#0.00") + "%";
			}
			else
			{
				//FC:FINAL:DSE #i511 Format does not take count of the last "-" character
				if (vsFees.EditText.EndsWith("-"))
				{
					vsFees.EditText = "-" + vsFees.EditText.Substring(0, vsFees.EditText.Length - 1);
				}
				vsFees.EditText = Strings.Format(Conversion.Val(vsFees.EditText), "#,##0.00");
			}
			//FC:FINAL:DSE Return modified value
			e.FormattedValue = vsFees.EditText;
		}

		private void vsPayments_Enter(object sender, System.EventArgs e)
		{
			// hide the payment info screen if it is being shown
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
		}

		private void vsPayments_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						if (vsPayments.Row > 0)
						{
							if (MessageBox.Show("Are you sure that you want to delete this payment?", "Delete Payment", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
							{
								DeletePaymentFromGrid(vsPayments.Row);
							}
						}
						break;
					}
			}
			//end switch
		}

		private void vsPayments_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsPayments[e.ColumnIndex, e.RowIndex];
            int lngR;
			FCGrid.CollapsedSettings lngC;
			int lngCol;
			lngR = vsPayments.GetFlexRowIndex(e.RowIndex);
			lngCol = vsPayments.GetFlexColIndex(e.ColumnIndex);
            if (lngR < 0 || lngCol < 0)
            {
                return;
            }
			if (lngCol != 1)
			{
				//ToolTip1.SetToolTip(vsPayments, vsPayments.TextMatrix(lngR, lngCol));
				cell.ToolTipText =  vsPayments.TextMatrix(lngR, lngCol);
			}
			else
			{
				//ToolTip1.SetToolTip(vsPayments, Strings.Format(vsPayments.TextMatrix(lngR, lngCol), "#,##0.00"));
				cell.ToolTipText =  Strings.Format(vsPayments.TextMatrix(lngR, lngCol), "#,##0.00");
			}
		}

		private void vsSummary_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int row = vsSummary.Row, col = vsSummary.Col;
			if (row > 0)
			{
				if (col == 7)
				{
					modUseCR.CalculateMaxCopies();
				}
				if (col == 5)
				{
					// this is the copies col
					if (FCConvert.ToDouble(vsSummary.TextMatrix(row, col)) == 0)
					{
						modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(row, 8))].PrintReceipt = false;
						modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(row, 8))].Copies = 0;
					}
					else
					{
						modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(row, 8))].PrintReceipt = true;
						modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(row, 8))].Copies = FCConvert.ToInt16(Math.Round(Conversion.Val(vsSummary.TextMatrix(row, col))));
					}
					modUseCR.CalculateMaxCopies();
				}
			}
		}

		private void vsSummary_DblClick(object sender, System.EventArgs e)
		{
			if (intCurFrame == 2)
			{
				if (fraPaymentInfo.Visible)
				{
					fraPaymentInfo.Visible = false;
					fraPaidBy.Visible = true;
					// kgk 12-29-2011  trocr-311
				}
				if (vsSummary.MouseCol != 5)
				{
					if (!blnIsVoid)
					{
						EditReceiptInformation();
						//FC:FINAL:CHN - issue #1302: Incorrect showing element after returning. 
						// relocate fraTotal component
						ShowFrame(fraTotal);
					}
				}
				else
				{
					vsSummary.EditCell();
				}
			}
		}

		private void vsSummary_Enter(object sender, System.EventArgs e)
		{
			modUseCR.CalculateSummaryTotal();
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
		}

		private void Setup_CashOut()
		{
			int intCT;
			double dblCash = 0;
			double dblCheck = 0;
			double dblCC = 0;
			// vbPorter upgrade warning: dblConvenienceFee As double	OnWriteFCConvert.ToDecimal(
			double dblConvenienceFee = 0;
			// this will transfer the info to the cash out frame and show the frame
			txtCashOutTotal.Text = txtTotalDue.Text;
			txtTransactions.Text = txtTotalDue.Text;
			if (vsPayments.Rows == 1)
			{
				txtCashOutCashPaid.Text = "0.00";
				txtCashOutCheckPaid.Text = "0.00";
				txtCashOutCreditPaid.Text = "0.00";
				txtConvenienceFee.Text = "0.00";
				txtCashOutTotal.Text = txtTotalDue.Text;
			}
			else
			{
				for (intCT = 1; intCT <= 500; intCT++)
				{
					switch (arr[intCT].Type)
					{
						case 0:
							{
								// Cash
								dblCash += arr[intCT].Amount;
								break;
							}
						case 1:
							{
								// Check
								dblCheck += arr[intCT].Amount;
								break;
							}
						case 2:
							{
								// Credit
								dblCC += arr[intCT].Amount;
								dblConvenienceFee += FCConvert.ToDouble(arr[intCT].ConvenienceFee);
								break;
							}
					}
					//end switch
				}
				txtCashOutCashPaid.Text = Strings.Format(dblCash, "#,##0.00");
				txtCashOutCheckPaid.Text = Strings.Format(dblCheck, "#,##0.00");
				txtCashOutCreditPaid.Text = Strings.Format(dblCC, "#,##0.00");
				txtConvenienceFee.Text = Strings.Format(dblConvenienceFee, "#,##0.00");
				txtCashOutTotal.Text = Strings.Format(FCConvert.ToDouble(txtTotalDue.Text) + dblConvenienceFee, "#,##0.00");
				if (blnIsVoid && blnIsEPymt)
				{
					txtTransactions.Text = Strings.Format(dblCash + dblCheck + dblCC + dblConvenienceFee, "#,##0.00");
					txtConvenienceFee.Text = Strings.Format(dblConvenienceFee * -1, "#,##0.00");
					txtTotalDue.Text = Strings.Format(FCConvert.ToDecimal(txtTransactions.Text) + FCConvert.ToDecimal(txtConvenienceFee.Text), "#,##0.00");
					txtCashOutTotal.Text = Strings.Format(FCConvert.ToDecimal(txtTransactions.Text) + FCConvert.ToDecimal(txtConvenienceFee.Text), "#,##0.00");
				}
			}
			if (boolEFTType)
			{
				// only allow checks when it is an EFT check
				cmbCashOutType.Items.Remove("Cash");
				cmbCashOutType.Items.Remove("Credit Card");
			}
			else
			{
				if (!cmbCashOutType.Items.Contains("Cash"))
				{
					cmbCashOutType.Items.Insert(0, "Cash");
				}
				if (FillCCCombo() && !cmbCashOutType.Items.Contains("Credit Card"))
				{
					cmbCashOutType.Items.Insert(2, "Credit Card");
				}
				else if (!FillCCCombo() && cmbCashOutType.Items.Contains("Credit Card"))
				{
					cmbCashOutType.Items.Remove("Credit Card");
				}
			}
			if (blnIsVoid && blnIsEPymt && lngReceiptNum != 0)
			{
				txtCashOutTotalDue.Text = "0.00";
				// Format(CDbl(txtTotalDue.Text), "#,##0.00")
				txtPayment.ReadOnly = true;
				lblCashOutType.Enabled = false;
				cmbCashOutType.Enabled = false;
				txtCashOutCashPaid.Enabled = false;
				txtCashOutCheckPaid.Enabled = false;
				txtCashOutCreditPaid.Enabled = false;
				txtPayment.Enabled = false;
			}
			else
			{
				if (FCConvert.ToDouble(txtCashOutTotal.Text) < 0 || (FCConvert.ToDouble(txtCashOutTotal.Text) - FCConvert.ToDouble(txtCashOutCashPaid.Text) - FCConvert.ToDouble(txtCashOutCheckPaid.Text) - FCConvert.ToDouble(txtCashOutCreditPaid.Text) >= 0))
				{
					txtCashOutChange.Text = "0.00";
					txtCashOutChange.Visible = false;
					lblCashOutChange.Visible = false;
					txtCashOutTotalDue.Text = Strings.Format(FCConvert.ToDouble(txtCashOutTotal.Text) - FCConvert.ToDouble(txtCashOutCashPaid.Text) - FCConvert.ToDouble(txtCashOutCheckPaid.Text) - FCConvert.ToDouble(txtCashOutCreditPaid.Text), "#,##0.00");
				}
				else
				{
					txtCashOutChange.Text = Strings.Format((FCConvert.ToDouble(txtCashOutTotal.Text) - FCConvert.ToDouble(txtCashOutCashPaid.Text) - FCConvert.ToDouble(txtCashOutCheckPaid.Text) - FCConvert.ToDouble(txtCashOutCreditPaid.Text)) * -1, "#,##0.00");
					txtCashOutChange.Visible = true;
					lblCashOutChange.Visible = true;
					txtCashOutTotalDue.Text = "0.00";
				}
				txtPayment.ReadOnly = false;
			}
			if (Conversion.Val(txtCashOutTotalDue.Text) == 0)
			{
				cmdCashOut.Text = "Process";
			}
			else
			{
				cmdCashOut.Text = "Enter";
			}
			mnuFileCashOut.Text = cmdCashOut.Text;
			txtPayment.Text = txtCashOutTotalDue.Text;
			fraSummary.Top = 0;
			fraTotal.Visible = false;
			FillBankCombo();
			// kgk 01-27-2012  Add InforME   'kgk 10-02-2011  Add Invoice Cloud
			if (modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
			{
				txtConvenienceFee.Enabled = false;
				lblConvenienceFee.Enabled = false;
			}
			else
			{
				txtConvenienceFee.Enabled = true;
				lblConvenienceFee.Enabled = true;
				if (modGlobal.Statics.gstrEPaymentPortal == "E")
				{
					lblConvenienceFee.Text = "Portal Charge:";
				}
			}
			ShowFrame(fraCashOut);
			ShowFrame(fraPaidBy);
			fraPaidBy.Top = fraSummary.Height;
			// MAL@20090114: E-Payment Options
			// Tracker Reference: 16641
			if (modGlobal.Statics.gblnAcceptEChecks)
			{
				int index = cmbCashOutType.Items.IndexOf("E-Check");
				if (index == -1)//doesn't exist
				{
					index = cmbCashOutType.Items.IndexOf("Check");
					if (index != -1)//change Check to E-Check
					{
						cmbCashOutType.Items.Remove("Check");
						cmbCashOutType.Items.Insert(1, "E-Check");
					}
					else if (index == -1)//neither one exists
					{
						cmbCashOutType.Items.Insert(1, "E-Check");
					}
				}
				else if (index != -1 && index != 1)// not in correct place
				{
					cmbCashOutType.Items.Remove("E-Check");
					cmbCashOutType.Items.Insert(1, "E-Check");
				}
			}
			else
			{
				int index = cmbCashOutType.Items.IndexOf("Check");
				if (index == -1)//doesn't exist
				{
					index = cmbCashOutType.Items.IndexOf("E-Check");
					if (index != -1)//change E-Check to Check
					{
						cmbCashOutType.Items.Remove("E-Check");
						cmbCashOutType.Items.Insert(1, "Check");
					}
					else if (index == -1)//neither one exists
					{
						cmbCashOutType.Items.Insert(1, "Check");
					}
				}
				else if (index != -1 && index != 1)// not in correct place
				{
					cmbCashOutType.Items.Remove("Check");
					cmbCashOutType.Items.Insert(1, "Check");
				}
			}
			// Determine if this is a sale or credit
			blnIsSale = (Conversion.Val(txtPayment.Text) > 0);
			if (frmReceiptInput.InstancePtr.cmdCashOut.Visible && frmReceiptInput.InstancePtr.cmdCashOut.Enabled)
			{
				frmReceiptInput.InstancePtr.cmdCashOut.Refresh();
			}
			if (txtPayment.Enabled == true)
			{
				txtPayment.Focus();
			}
		}

		private bool FillCCCombo()
		{
			bool FillCCCombo = false;
			// this loads the credit card types into the combo box with the correct ID as the item data
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsCashRec = new clsDRWrapper();
			rsCashRec.OpenRecordset("SELECT * FROM CashRec");
			// kgk 01-27-2012  Add InforME  'kgk 09-30-2011  Add Invoice Cloud
			if (modGlobal.Statics.gstrEPaymentPortal == "P" || modGlobal.Statics.gstrEPaymentPortal == "I" || modGlobal.Statics.gstrEPaymentPortal == "E")
			{
				rsTemp.OpenRecordset("SELECT * FROM CCType WHERE SystemDefined = 1 ORDER BY Type");
			}
			else
			{
				rsTemp.OpenRecordset("SELECT * FROM CCType WHERE SystemDefined = 0 ORDER BY Type");
			}
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				cmbCashOutCC.Clear();
				do
				{
					// kgk 01-27-2012 Add InforME  'kgk 09-30-2011  Add Invoice Cloud
					if (modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
					{
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						cmbCashOutCC.AddItem(FCConvert.ToString(rsTemp.Get_Fields("Type")));
						cmbCashOutCC.ItemData(cmbCashOutCC.NewIndex, FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID")));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsTemp.Get_Fields("Type")), 4) == "VISA" && (modGlobal.Statics.gblnNoVISAPayments || rsCashRec.Get_Fields_Boolean("AcceptVISA") != true) && modGlobal.Statics.gstrEPaymentPortal != "E")
						{
							// kgk 02-16-2012  Allow VISA on InforME
							// do nothing
						}
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (FCConvert.ToString(rsTemp.Get_Fields("Type")) == "VISA Debit" && rsCashRec.Get_Fields_Decimal("VISADebitConvenienceFlatAmount") == 0)
						{
							// do nothing
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							cmbCashOutCC.AddItem(FCConvert.ToString(rsTemp.Get_Fields("Type")));
							cmbCashOutCC.ItemData(cmbCashOutCC.NewIndex, FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID")));
						}
					}
					rsTemp.MoveNext();
				}
				while (!rsTemp.EndOfFile());
				FillCCCombo = true;
			}
			else
			{
				FillCCCombo = false;
			}
			return FillCCCombo;
		}

		private void FillBankCombo_6(ref clsDRWrapper rsBank, bool boolUseParam = false)
		{
			FillBankCombo(rsBank, boolUseParam);
		}

		private void FillBankCombo(clsDRWrapper rsBank = null, bool boolUseParam = false)
		{
			// this will fill the bank combo box
			// Dim rsBank          As New clsDRWrapper
			string strTemp = "";
			cmbBank.Clear();
			cmbBank.AddItem("-- Add New Bank --");
			if (!boolUseParam)
			{
				rsBank = new clsDRWrapper();
			}
			rsBank.OpenRecordset("SELECT * FROM Bank ORDER BY Name");
			if (rsBank.EndOfFile() != true && rsBank.BeginningOfFile() != true)
			{
				do
				{
					// kgk 11-10-2011 trocr-284  Make routing number visible
					if (boolShowRoutingNumbers)
					{
						strTemp = rsBank.Get_Fields_String("Name") + " (" + rsBank.Get_Fields_String("RoutingTransit") + ")";
						cmbBank.AddItem(strTemp + Strings.StrDup(100 - strTemp.Length, " ") + "|" + rsBank.Get_Fields_String("RoutingTransit"));
					}
					else
					{
						cmbBank.AddItem(rsBank.Get_Fields_String("Name") + "                                                                   |" + rsBank.Get_Fields_String("RoutingTransit"));
					}
					rsBank.MoveNext();
				}
				while (!rsBank.EndOfFile());
			}
		}

		private void CashOut_120(Decimal curConvenienceFee, string strEPmtAcctID = "")
		{
			CashOut(curConvenienceFee, false, false, 0, 0, strEPmtAcctID);
		}

		private void CashOut(Decimal curConvenienceFee = 0, bool blnIsVisaCard = false, bool blnIsDebitCard = false, Decimal curNonTaxAmt = 0, Decimal curNonTaxConvenienceFee = 0, string strEPmtAcctID = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will add the money paid to the total paid and decrease amount due
				double dblDue = 0;
				double dblPayment = 0;
				double dblPaid = 0;
				double dblChange = 0;
				int intIndex;
				bool blnContinue = false;
				for (intIndex = 1; intIndex <= modStatusPayments.MAX_PAYMENTS; intIndex++)
				{
					if (arr[intIndex].Amount == 0)
					{
						break;
					}
				}
				// Tracker Reference: 16641
				if ((modGlobal.Statics.gstrEPaymentPortal == "N") || (modGlobal.Statics.gstrEPaymentPortal == ""))
				{
					// Do Nothing - Regular Processing
					blnContinue = true;
				}
				else
				{
					// Active E-Payment Portal
					if (cmbCashOutType.SelectedIndex == 0)
					{
						// Cash Payment
						blnContinue = true;
					}
					else if (cmbCashOutType.SelectedIndex == 1)
					{
						// Check
						if (!modGlobal.Statics.gblnAcceptEChecks)
						{
							// Regular Check Process
							blnContinue = true;
						}
						else
						{
							blnContinue = ValidateECheckOptions();
						}
					}
					else
					{
						// Credit Card
						if (modGlobal.Statics.gstrEPaymentPortal != "N")
						{
							blnContinue = ValidateEPymtOptions();
						}
						else
						{
							blnContinue = true;
						}
					}
				}
				if (blnContinue)
				{
					if (intIndex <= modStatusPayments.MAX_PAYMENTS)
					{
						dblDue = FCConvert.ToDouble(txtCashOutTotalDue.Text);
						dblPaid = FCConvert.ToDouble(txtCashOutCashPaid.Text) + FCConvert.ToDouble(txtCashOutCheckPaid.Text) + FCConvert.ToDouble(txtCashOutCreditPaid.Text);
						dblPayment = FCConvert.ToDouble(txtPayment.Text);
						if (cmbCashOutType.SelectedIndex == 0)
						{
							arr[intIndex].Type = 0;
							txtCashOutCashPaid.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCashPaid.Text) + dblPayment, "#,##0.00");
							arr[intIndex].Amount = dblPayment;
						}
						else if (cmbCashOutType.SelectedIndex == 1)
						{
							if (blnIsSale)
							{
								arr[intIndex].Type = 1;
								arr[intIndex].CType = txtCashOutCheckNumber.Text;
								arr[intIndex].Bank = Strings.Right(cmbBank.Items[cmbBank.SelectedIndex].ToString(), cmbBank.Items[cmbBank.SelectedIndex].ToString().Length - Strings.InStr(1, cmbBank.Items[cmbBank.SelectedIndex].ToString(), "|", CompareConstants.vbBinaryCompare));
								// MAL@20080120 ; Tracker Reference: 16641
								if (modGlobal.Statics.gstrEPaymentPortal != "N" && modGlobal.Statics.gblnAcceptEChecks)
								{
									arr[intIndex].ChkAcctNum = txtCheckAcctNum.Text;
									arr[intIndex].ChkAcctType = FCConvert.ToString(cmbCheckAcctType.ItemData(cmbCheckAcctType.SelectedIndex));
									arr[intIndex].ChkCheckType = FCConvert.ToString(cmbCheckType.ItemData(cmbCheckType.SelectedIndex));
									arr[intIndex].ChkAcctName = txtChkAcctName.Text;
									arr[intIndex].Amount = dblPayment;
								}
								else
								{
									arr[intIndex].Amount = dblPayment;
								}
							}
							else if (!blnIsSale && blnIsVoid && blnIsEPymt)
							{
								// MAL@20090427:Check for voids - this is the only place credits have already been added to array
								// Tracker Reference: 18367
								// Credits Have Already Been Added to the Array
							}
							else
							{
								arr[intIndex].Type = 1;
								arr[intIndex].CType = txtCashOutCheckNumber.Text;
								arr[intIndex].Bank = Strings.Right(cmbBank.Items[cmbBank.SelectedIndex].ToString(), cmbBank.Items[cmbBank.SelectedIndex].ToString().Length - Strings.InStr(1, cmbBank.Items[cmbBank.SelectedIndex].ToString(), "|", CompareConstants.vbBinaryCompare));
								if (modGlobal.Statics.gstrEPaymentPortal != "N" && modGlobal.Statics.gblnAcceptEChecks)
								{
									arr[intIndex].ChkAcctNum = txtCheckAcctNum.Text;
									arr[intIndex].ChkAcctType = FCConvert.ToString(cmbCheckAcctType.ItemData(cmbCheckAcctType.SelectedIndex));
									arr[intIndex].ChkCheckType = FCConvert.ToString(cmbCheckType.ItemData(cmbCheckType.SelectedIndex));
									arr[intIndex].ChkAcctName = txtChkAcctName.Text;
									arr[intIndex].Amount = dblPayment;
								}
								else
								{
									arr[intIndex].Amount = dblPayment;
								}
							}
							txtCashOutCheckPaid.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCheckPaid.Text) + dblPayment, "#,##0.00");
							txtCashOutCheckNumber.Text = "";
							cmbBank.SelectedIndex = -1;
							txtCheckAcctNum.Text = "";
							cmbCheckAcctType.SelectedIndex = -1;
							cmbCheckType.SelectedIndex = -1;
							txtChkAcctName.Text = "";
						}
						else
						{
							if (blnIsSale)
							{
								arr[intIndex].Type = 2;
								arr[intIndex].CType = FCConvert.ToString(cmbCashOutCC.ItemData(cmbCashOutCC.SelectedIndex));
								arr[intIndex].Bank = cmbCashOutCC.Items[cmbCashOutCC.SelectedIndex].ToString();
								// MAL@20080120 ; Tracker Reference: 16641
								arr[intIndex].CNum = txtCCNum.Text;
								arr[intIndex].CVV2 = txtCVV2.Text;
								arr[intIndex].ExpDate = cboExpMonth.Text + "/1/" + cboExpYear.Text;
								arr[intIndex].OwnerName = modGlobal.Statics.gstrCCOwnerName;
								arr[intIndex].OwnerStreet = modGlobal.Statics.gstrCCOwnerStreet;
								arr[intIndex].OwnerCity = modGlobal.Statics.gstrCCOwnerCity;
								arr[intIndex].OwnerState = modGlobal.Statics.gstrCCOwnerState;
								arr[intIndex].OwnerZip = modGlobal.Statics.gstrCCOwnerZip;
								arr[intIndex].OwnerCountry = modGlobal.Statics.gstrCCOwnerCountry;
								arr[intIndex].Amount = (dblPayment + FCConvert.ToDouble(curConvenienceFee));
								arr[intIndex].ConvenienceFee = curConvenienceFee;
								arr[intIndex].IsVISA = blnIsVisaCard;
								arr[intIndex].IsDebit = blnIsDebitCard;
								arr[intIndex].NonTaxAmount = curNonTaxAmt;
								arr[intIndex].NonTaxConvenienceFee = curNonTaxConvenienceFee;
								arr[intIndex].EPmtAcctID = strEPmtAcctID;
							}
							else if (!blnIsSale && blnIsVoid && blnIsEPymt)
							{
								// Credits Have Already Been Added to the Array
							}
							else
							{
								arr[intIndex].Type = 2;
								arr[intIndex].CType = FCConvert.ToString(cmbCashOutCC.ItemData(cmbCashOutCC.SelectedIndex));
								arr[intIndex].Bank = cmbCashOutCC.Items[cmbCashOutCC.SelectedIndex].ToString();
								// MAL@20080120 ; Tracker Reference: 16641
								arr[intIndex].CNum = txtCCNum.Text;
								arr[intIndex].CVV2 = txtCVV2.Text;
								arr[intIndex].ExpDate = cboExpMonth.Text + "/1/" + cboExpYear.Text;
								arr[intIndex].OwnerName = modGlobal.Statics.gstrCCOwnerName;
								arr[intIndex].OwnerStreet = modGlobal.Statics.gstrCCOwnerStreet;
								arr[intIndex].OwnerCity = modGlobal.Statics.gstrCCOwnerCity;
								arr[intIndex].OwnerState = modGlobal.Statics.gstrCCOwnerState;
								arr[intIndex].OwnerZip = modGlobal.Statics.gstrCCOwnerZip;
								arr[intIndex].OwnerCountry = modGlobal.Statics.gstrCCOwnerCountry;
								arr[intIndex].Amount = dblPayment;
								arr[intIndex].ConvenienceFee = 0;
								arr[intIndex].NonTaxAmount = 0;
								arr[intIndex].NonTaxConvenienceFee = 0;
								arr[intIndex].IsVISA = blnIsVisaCard;
								arr[intIndex].IsDebit = blnIsDebitCard;
								arr[intIndex].EPmtAcctID = strEPmtAcctID;
							}
							txtCashOutCreditPaid.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCreditPaid.Text) + dblPayment + FCConvert.ToDouble(curConvenienceFee), "#,##0.00");
							txtConvenienceFee.Text = Strings.Format(FCConvert.ToDecimal(txtConvenienceFee.Text) + curConvenienceFee, "#,##0.00");
							// CalculateTotals  -  kk03092015 trocr-393  Handle calculating change the same as cash and check, done below
							cmbCashOutCC.SelectedIndex = -1;
							txtCCNum.Text = "";
							txtCVV2.Text = "";
							cboExpMonth.SelectedIndex = -1;
							cboExpYear.SelectedIndex = -1;
						}
						if (blnIsSale || cmbCashOutType.SelectedIndex == 0)
						{
							// this will show the payment in the payment grid
							AddPaymentToGrid(ref intIndex);
						}
						else if (!blnIsSale && blnIsEPymt && blnIsVoid)
						{
							// Already Added for Credits
						}
						else
						{
							AddPaymentToGrid(ref intIndex);
						}
						dblDue -= dblPayment;
						dblPaid += dblPayment;
						if (dblDue < 0 && FCConvert.ToDouble(txtCashOutTotalDue.Text) > 0)
						{
							dblChange = dblDue;
							dblDue = 0;
							cmdCashOut.Text = "Process";
							txtCashOutChange.Text = Strings.Format(dblChange * -1, "#,##0.00");
							lblCashOutChange.Visible = true;
							txtCashOutChange.Visible = true;
							cmbCashOutType.SelectedIndex = 0;
							txtCashOutTotalDue.Text = "0.00";
							txtPayment.Text = "0.00";
							// MAL@20080110: Disable fields to avoid changes that cannot be saved
							// Tracker Reference: 11717
							lblCashOutType.Enabled = false;
							cmbCashOutType.Enabled = false;
							txtCashOutCashPaid.Enabled = false;
							txtCashOutCheckPaid.Enabled = false;
							txtCashOutCreditPaid.Enabled = false;
							txtPayment.Enabled = false;
						}
						else if (dblDue == 0)
						{
							cmdCashOut.Text = "Process";
							lblCashOutChange.Visible = false;
							txtCashOutChange.Visible = false;
							cmbCashOutType.SelectedIndex = 0;
							txtCashOutTotalDue.Text = "0.00";
							txtPayment.Text = "0.00";
							// MAL@20080110: Disable fields to avoid changes that cannot be saved
							// Tracker Reference: 11717
							lblCashOutType.Enabled = false;
							cmbCashOutType.Enabled = false;
							txtCashOutCashPaid.Enabled = false;
							txtCashOutCheckPaid.Enabled = false;
							txtCashOutCreditPaid.Enabled = false;
							txtPayment.Enabled = false;
						}
						else
						{
							cmdCashOut.Text = "Enter";
							lblCashOutChange.Visible = false;
							txtCashOutChange.Visible = false;
							cmbCashOutType.SelectedIndex = 0;
							txtCashOutTotalDue.Text = Strings.Format(dblDue, "#,##0.00");
							txtPayment.Text = Strings.Format(dblDue, "#,##0.00");
							txtPayment.Focus();
						}
						mnuFileCashOut.Text = cmdCashOut.Text;
					}
					else
					{
						MessageBox.Show("A maximum of " + FCConvert.ToString(modStatusPayments.MAX_PAYMENTS) + " payments may be made against one account.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Cash Out Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public int CreateReceiptRecord(int lngReceiptKey = 0)
		{
			int CreateReceiptRecord = 0;
			int intError = 0;
			try
			{
				/* On Error GoTo ERROR_HANDLER */// this will create a receipt record in the Receipt Table and add the Receipt # to the Archive Table record
				clsDRWrapper rsAdd = new clsDRWrapper();
				clsDRWrapper rsType = new clsDRWrapper();
				clsDRWrapper rsType2 = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				int I;
				double dblCash = 0;
				double dblCredit = 0;
				double dblCheck = 0;
				double dblChange = 0;
				int intCashNum = 0;
				int intCheckNum = 0;
				int intCreditNum = 0;
				int intReceiptIndex = 0;
				string strSQL;
				int lngArchivekey;
				double dblTempChange;
				bool boolEFTType = false;
				bool boolTA;
				string strCT = "";
				string strARAccount = "";
				// vbPorter upgrade warning: curConvenienceFee As Decimal	OnWrite(short, Decimal)
				Decimal curConvenienceFee;
				clsDRWrapper rsCashRec = new clsDRWrapper();
				rsCashRec.OpenRecordset("SELECT * FROM CashRec", "TWCR0000.vb1");
				intError = 0;
				curConvenienceFee = 0;
				// this adds a new receipt record
				rsAdd.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = 0");
				intError = 1;
				rsAdd.AddNew();
				intError = 2;
				rsAdd.Update();
				// kk 101612 True
				lngReceiptKey = FCConvert.ToInt32(rsAdd.Get_Fields_Int32("ReceiptKey"));
				intError = 3;
				// this will add up all the payments and sort them by type
				for (I = 1; I <= modStatusPayments.MAX_PAYMENTS; I++)
				{
					if (arr[I].Amount == 0)
						break;
					switch (arr[I].Type)
					{
						case 0:
							{
								// cash
								intCashNum += 1;
								dblCash += arr[I].Amount;
								break;
							}
						case 1:
							{
								// check
								intCheckNum += 1;
								dblCheck += arr[I].Amount;
								// add check to CheckMaster
								CreateCheckRecord(ref lngReceiptKey, ref I);
								break;
							}
						case 2:
							{
								// card
								intCreditNum += 1;
								dblCredit += arr[I].Amount;
								// add credit card to CCMaster
								CreateCreditCardRecord(ref lngReceiptKey, ref I);
								break;
							}
						default:
							{
								break;
							}
					}
					//end switch
					curConvenienceFee += arr[I].ConvenienceFee;
				}
				intError = 4;
				dblChange = FCConvert.ToDouble(txtCashOutChange.Text);
				intError = 5;
				dblCash -= dblChange;
				
				intError = 6;
				rsAdd.Set_Fields("CashAmount", dblCash);
				intError = 7;
				rsAdd.Set_Fields("CheckAmount", dblCheck);
				intError = 8;
				rsAdd.Set_Fields("CardAmount", dblCredit - FCConvert.ToDouble(curConvenienceFee));
				intError = 9;
				rsAdd.Set_Fields("ConvenienceFee", curConvenienceFee);
				rsAdd.Set_Fields("MultipleChecks", FCConvert.CBool(intCheckNum > 1));
				intError = 10;
				rsAdd.Set_Fields("MultipleCC", FCConvert.CBool(intCreditNum > 1));
				intError = 11;
				rsAdd.Set_Fields("TellerID", Strings.Trim(strTellerID + " "));
				intError = 12;
				rsAdd.Set_Fields("ReceiptDate", DateTime.Now.ToString("MM/dd/yy h:mm:ss tt"));
				intError = 13;
				rsAdd.Set_Fields("EFT", modUseCR.Statics.ReceiptArray[1].EFT);
				// just check the first one since this only one at a time is allowed
				intError = 14;
				if (Strings.Trim(txtPaidBy.Text).Length > 250)
				{
					rsAdd.Set_Fields("PaidBy", Strings.Left(Strings.Trim(txtPaidBy.Text), 250));
				}
				else
				{
					rsAdd.Set_Fields("PaidBy", Strings.Trim(txtPaidBy.Text));
				}
				rsAdd.Set_Fields("PaidByPartyID", FCConvert.ToString(Conversion.Val(txtCustNum_PaidBy.Text)));
				intError = 15;
				rsAdd.Set_Fields("Change", dblChange);
				intError = 16;
				rsAdd.Set_Fields("IsEPayment", blnIsEPymt);
				// MAL@20090417 ; Tracker Reference: 17964
				rsAdd.Update(true);
				// need lngReceiptKey earlier        lngReceiptKey = .Fields("ReceiptKey")
				intError = 17;
				rsAdd.OpenRecordset("SELECT * FROM Archive WHERE ID = 0");
				intError = 18;
				for (I = 1; I <= vsSummary.Rows - 1; I++)
				{
					// this creates the new archive records with the correct receipt number in it
					boolEFTType = false;
					if (vsSummary.TextMatrix(I,constBatchTypeCol) == "CL")
					{
						intError = 20;
						AddBatchToReceiptArray(ref I, ref lngReceiptKey);
						intError = 21;
					}
                    else if (vsSummary.TextMatrix(I, constBatchTypeCol) == "UT")
                    {
                        intError = 20;
                        AddUTBatchToReceiptArray(I, ref lngReceiptKey);
                        int Error = 21;
                    }
					else
					{
						intReceiptIndex = FCConvert.ToInt32(FCConvert.ToDouble(vsSummary.TextMatrix(I, 8)));
						intError = 23;
						rsAdd.AddNew();
						if (modGlobal.Statics.gboolMultipleTowns)
						{
							rsAdd.Set_Fields("TownKey", modUseCR.Statics.ReceiptArray[intReceiptIndex].ResCode);
						}
						else
						{
							rsAdd.Set_Fields("TownKey", 0);
							// k  need to make sure it's not NULL
						}
						intError = 25;
						// ID
						intError = 26;
						// ReceiptType
						rsAdd.Set_Fields("ReceiptType", modUseCR.Statics.ReceiptArray[intReceiptIndex].Type);
						// MAL@20080611: Add New AR Bill Type ; Tracker Reference: 14057
						rsAdd.Set_Fields("ARBillType", modUseCR.Statics.ReceiptArray[intReceiptIndex].ARBillType);
						intError = 27;
						// AccountNumber
						rsAdd.Set_Fields("AccountNumber", FCConvert.ToString(Conversion.Val(Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].Account + " "))));
						intError = 28;
						// ReceiptTitle
						// rsadd.Fields("ReceiptTitle") = .TypeDescription
						if (modGlobal.Statics.gboolMultipleTowns)
						{
							rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Type + " AND TownKey = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].ResCode);
						}
						else
						{
							rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Type);
						}
						intError = 29;
						if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
						{
							// find out if this is an EFT type
							/*? On Error Resume Next  */// this will allow the user to get by this problem until it is added to all DB then take this out
							rsAdd.Set_Fields("EFT", rsType.Get_Fields_Boolean("EFT"));
							/* On Error GoTo ERROR_HANDLER */// Account1
							strCT = "1";
							if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee1 != 0)
							{
								;
								var vbPorterVar = modUseCR.Statics.ReceiptArray[intReceiptIndex].Type;
								if ((vbPorterVar == 90) || (vbPorterVar == 91) || (vbPorterVar == 890) || (vbPorterVar == 891))
								{
									rsRE.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strREDatabase);
									if (!rsRE.EndOfFile())
									{
										if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired")) && modGlobalConstants.Statics.gboolBD)
										{

											rsAdd.Set_Fields("Account1", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account1"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 1));
										}
										else
										{
											if (FCConvert.ToString(rsType.Get_Fields_String("Account1")) != "M I")
											{
												rsAdd.Set_Fields("Account1", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account1"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 1));
											}
											else
											{
												rsAdd.Set_Fields("Account1", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 1));
											}
										}
									}
									else
									{
										if (FCConvert.ToString(rsType.Get_Fields_String("Account1")) != "M I")
										{
											rsAdd.Set_Fields("Account1", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account1"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 1));
										}
										else
										{
											rsAdd.Set_Fields("Account1", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 1));
										}
									}
								}
								else if ((vbPorterVar >= 93 && vbPorterVar <= 96) || (vbPorterVar >= 893 && vbPorterVar <= 896))
								{
									rsRE.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strUTDatabase);
									if (!rsRE.EndOfFile())
									{
										if (modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[intReceiptIndex].Account))) && modGlobalConstants.Statics.gboolBD)
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
											{
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
											else
											{
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
									}
								}
								else if ((vbPorterVar == 97) || (vbPorterVar >= 901 && vbPorterVar <= 999))
								{
									strARAccount = GetARAccountNumber(strCT, modUseCR.Statics.ReceiptArray[intReceiptIndex].ARBillType, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear), modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount);
									if (Strings.Trim(strARAccount) != "")
									{
										rsAdd.Set_Fields("Account" + strCT, strARAccount);
									}
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 1));
									}
									else
									{
										rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 1));
									}
									if (FCConvert.ToString(rsType.Get_Fields_String("Project" + strCT)) != "")
									{
										rsAdd.Set_Fields("Project" + strCT, rsType.Get_Fields_String("Project" + strCT));
									}
								}
							}
							else
							{
								rsAdd.Set_Fields("Account" + strCT, "");
								rsAdd.Set_Fields("Project" + strCT, "");
							}
							intError = 30;
							// Account2
							strCT = "2";
							if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee2 != 0)
							{
								;
								var vbPorterVar1 = modUseCR.Statics.ReceiptArray[intReceiptIndex].Type;
								if ((vbPorterVar1 == 90) || (vbPorterVar1 == 91) || (vbPorterVar1 == 890) || (vbPorterVar1 == 891))
								{
									rsRE.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strREDatabase);
									if (!rsRE.EndOfFile())
									{
										if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired")) && modGlobalConstants.Statics.gboolBD)
										{
											// kk08112015 trocls-60  Make TA receipts use year like non-TA
											// rsAdd.Fields("Account2") = rsType.Fields("Account2")
											rsAdd.Set_Fields("Account2", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account2"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 2));
										}
										else
										{
											if (FCConvert.ToString(rsType.Get_Fields_String("Account2")) != "M I")
											{
												rsAdd.Set_Fields("Account2", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account2"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 2));
											}
											else
											{
												rsAdd.Set_Fields("Account2", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 2));
											}
										}
									}
									else
									{
										if (FCConvert.ToString(rsType.Get_Fields_String("Account2")) != "M I")
										{
											rsAdd.Set_Fields("Account2", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account2"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 2));
										}
										else
										{
											rsAdd.Set_Fields("Account2", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 2));
										}
									}
								}
								else if ((vbPorterVar1 >= 93 && vbPorterVar1 <= 96) || (vbPorterVar1 >= 893 && vbPorterVar1 <= 896))
								{
									rsRE.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strUTDatabase);
									if (!rsRE.EndOfFile())
									{
										if (modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[intReceiptIndex].Account))) && modGlobalConstants.Statics.gboolBD)
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
											{
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
											else
											{
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
									}
								}
								else if ((vbPorterVar1 == 97) || (vbPorterVar1 >= 901 && vbPorterVar1 <= 999))
								{
									strARAccount = GetARAccountNumber(strCT, modUseCR.Statics.ReceiptArray[intReceiptIndex].ARBillType, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear), modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount);
									if (Strings.Trim(strARAccount) != "")
									{
										rsAdd.Set_Fields("Account" + strCT, strARAccount);
									}
								}
								else
								{
									if (FCConvert.ToString(rsType.Get_Fields_String("Account2")) != "M I")
									{
										rsAdd.Set_Fields("Account2", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account2"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 2));
									}
									else
									{
										rsAdd.Set_Fields("Account2", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 2));
									}
									if (FCConvert.ToString(rsType.Get_Fields_String("Project2")) != "")
									{
										rsAdd.Set_Fields("Project2", rsType.Get_Fields_String("Project2"));
									}
								}
							}
							else
							{
								rsAdd.Set_Fields("Account2", "");
								rsAdd.Set_Fields("Project2", "");
							}
							intError = 31;
							// Account3
							strCT = "3";
							if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee3 != 0)
							{
								;
								var vbPorterVar2 = modUseCR.Statics.ReceiptArray[intReceiptIndex].Type;
								if ((vbPorterVar2 == 90) || (vbPorterVar2 == 91) || (vbPorterVar2 == 890) || (vbPorterVar2 == 891))
								{
									rsRE.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strREDatabase);
									if (!rsRE.EndOfFile())
									{
										if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired")) && modGlobalConstants.Statics.gboolBD)
										{
											// kk08112015 trocls-60  Make TA receipts use year like non-TA
											// rsAdd.Fields("Account3") = rsType.Fields("Account3")
											rsAdd.Set_Fields("Account3", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account3"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 3));
										}
										else
										{
											if (FCConvert.ToString(rsType.Get_Fields_String("Account3")) != "M I")
											{
												rsAdd.Set_Fields("Account3", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account3"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 3));
											}
											else
											{
												rsAdd.Set_Fields("Account3", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 3));
											}
										}
									}
									else
									{
										if (FCConvert.ToString(rsType.Get_Fields_String("Account3")) != "M I")
										{
											rsAdd.Set_Fields("Account3", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account3"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 3));
										}
										else
										{
											rsAdd.Set_Fields("Account3", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 3));
										}
									}
								}
								else if ((vbPorterVar2 >= 93 && vbPorterVar2 <= 96) || (vbPorterVar2 >= 893 && vbPorterVar2 <= 896))
								{
									rsRE.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strUTDatabase);
									if (!rsRE.EndOfFile())
									{
										if (modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[intReceiptIndex].Account))) && modGlobalConstants.Statics.gboolBD)
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
											{
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
											else
											{
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
									}
								}
								else if ((vbPorterVar2 == 97) || (vbPorterVar2 >= 901 && vbPorterVar2 <= 999))
								{
									strARAccount = GetARAccountNumber(strCT, modUseCR.Statics.ReceiptArray[intReceiptIndex].ARBillType, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear), modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount);
									if (Strings.Trim(strARAccount) != "")
									{
										rsAdd.Set_Fields("Account" + strCT, strARAccount);
									}
								}
								else
								{
									if (FCConvert.ToString(rsType.Get_Fields_String("Account3")) != "M I")
									{
										rsAdd.Set_Fields("Account3", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account3"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 3));
									}
									else
									{
										rsAdd.Set_Fields("Account3", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 3));
									}
									if (FCConvert.ToString(rsType.Get_Fields_String("Project3")) != "")
									{
										rsAdd.Set_Fields("Project3", rsType.Get_Fields_String("Project3"));
									}
								}
							}
							else
							{
								rsAdd.Set_Fields("Account3", "");
								rsAdd.Set_Fields("Project3", "");
							}
							intError = 32;
							// Account4
							strCT = "4";
							if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee4 != 0)
							{
								;
								var vbPorterVar3 = modUseCR.Statics.ReceiptArray[intReceiptIndex].Type;
								if ((vbPorterVar3 == 90) || (vbPorterVar3 == 91) || (vbPorterVar3 == 890) || (vbPorterVar3 == 891))
								{
									rsRE.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strREDatabase);
									if (!rsRE.EndOfFile())
									{
										if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired")) && modGlobalConstants.Statics.gboolBD)
										{
											// kk08112015 trocls-60  Make TA receipts use year like non-TA
											// rsAdd.Fields("Account4") = rsType.Fields("Account4")
											rsAdd.Set_Fields("Account4", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account4"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 4));
										}
										else
										{
											if (FCConvert.ToString(rsType.Get_Fields_String("Account4")) != "M I")
											{
												rsAdd.Set_Fields("Account4", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account4"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 4));
											}
											else
											{
												rsAdd.Set_Fields("Account4", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 4));
											}
										}
									}
									else
									{
										if (FCConvert.ToString(rsType.Get_Fields_String("Account4")) != "M I")
										{
											rsAdd.Set_Fields("Account4", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account4"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 4));
										}
										else
										{
											rsAdd.Set_Fields("Account4", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 4));
										}
									}
								}
								else if ((vbPorterVar3 >= 93 && vbPorterVar3 <= 96) || (vbPorterVar3 >= 893 && vbPorterVar3 <= 896))
								{
									rsRE.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strUTDatabase);
									if (!rsRE.EndOfFile())
									{
										if (modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[intReceiptIndex].Account))) && modGlobalConstants.Statics.gboolBD)
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
											{
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
											else
											{
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
									}
								}
								else if ((vbPorterVar3 == 97) || (vbPorterVar3 >= 901 && vbPorterVar3 <= 999))
								{
									strARAccount = GetARAccountNumber(strCT, modUseCR.Statics.ReceiptArray[intReceiptIndex].ARBillType, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear), modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount);
									if (Strings.Trim(strARAccount) != "")
									{
										rsAdd.Set_Fields("Account" + strCT, strARAccount);
									}
								}
								else
								{
									if (FCConvert.ToString(rsType.Get_Fields_String("Account4")) != "M I")
									{
										rsAdd.Set_Fields("Account4", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account4"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 4));
									}
									else
									{
										rsAdd.Set_Fields("Account4", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 4));
									}
									if (FCConvert.ToString(rsType.Get_Fields_String("Project4")) != "")
									{
										rsAdd.Set_Fields("Project4", rsType.Get_Fields_String("Project4"));
									}
								}
							}
							else
							{
								rsAdd.Set_Fields("Account4", "");
								rsAdd.Set_Fields("Project4", "");
							}
							intError = 33;
							// Account5
							strCT = "5";
							if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee5 != 0)
							{
								;
								var vbPorterVar4 = modUseCR.Statics.ReceiptArray[intReceiptIndex].Type;
								if ((vbPorterVar4 == 90) || (vbPorterVar4 == 91) || (vbPorterVar4 == 890) || (vbPorterVar4 == 891))
								{
									rsRE.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strREDatabase);
									if (!rsRE.EndOfFile())
									{
										if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired")) && modGlobalConstants.Statics.gboolBD)
										{
											// kk08112015 trocls-60  Make TA receipts use year like non-TA
											// rsAdd.Fields("Account5") = rsType.Fields("Account5")
											rsAdd.Set_Fields("Account5", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account5"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 5));
										}
										else
										{
											if (FCConvert.ToString(rsType.Get_Fields_String("Account5")) != "M I")
											{
												rsAdd.Set_Fields("Account5", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account5"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 5));
											}
											else
											{
												rsAdd.Set_Fields("Account5", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 5));
											}
										}
									}
									else
									{
										if (FCConvert.ToString(rsType.Get_Fields_String("Account5")) != "M I")
										{
											rsAdd.Set_Fields("Account5", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account5"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 5));
										}
										else
										{
											rsAdd.Set_Fields("Account5", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 5));
										}
									}
								}
								else if ((vbPorterVar4 >= 93 && vbPorterVar4 <= 96) || (vbPorterVar4 >= 893 && vbPorterVar4 <= 896))
								{
									rsRE.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strUTDatabase);
									if (!rsRE.EndOfFile())
									{
										if (modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[intReceiptIndex].Account))) && modGlobalConstants.Statics.gboolBD)
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
											{
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
											else
											{
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
									}
								}
								else if ((vbPorterVar4 == 97) || (vbPorterVar4 >= 901 && vbPorterVar4 <= 999))
								{
									strARAccount = GetARAccountNumber(strCT, modUseCR.Statics.ReceiptArray[intReceiptIndex].ARBillType, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear), modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount);
									if (Strings.Trim(strARAccount) != "")
									{
										rsAdd.Set_Fields("Account" + strCT, strARAccount);
									}
								}
								else
								{
									if (FCConvert.ToString(rsType.Get_Fields_String("Account5")) != "M I")
									{
										rsAdd.Set_Fields("Account5", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account5"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 5));
									}
									else
									{
										rsAdd.Set_Fields("Account5", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 5));
									}
									if (FCConvert.ToString(rsType.Get_Fields_String("Project5")) != "")
									{
										rsAdd.Set_Fields("Project5", rsType.Get_Fields_String("Project5"));
									}
								}
							}
							else
							{
								rsAdd.Set_Fields("Account5", "");
								rsAdd.Set_Fields("Project5", "");
							}
							intError = 34;
							// Account6
							strCT = "6";
							if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee6 != 0)
							{
								;
								var vbPorterVar5 = modUseCR.Statics.ReceiptArray[intReceiptIndex].Type;
								if ((vbPorterVar5 == 90) || (vbPorterVar5 == 91) || (vbPorterVar5 == 890) || (vbPorterVar5 == 891))
								{
									rsRE.OpenRecordset("SELECT TaxAcquired FROM Master WHERE RSCard = 1 AND RSAccount = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strREDatabase);
									if (!rsRE.EndOfFile())
									{
										if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired")) && modGlobalConstants.Statics.gboolBD)
										{
											// kk08112015 trocls-60  Make TA receipts use year like non-TA
											// rsAdd.Fields("Account6") = rsType.Fields("Account6")
											rsAdd.Set_Fields("Account6", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account6"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 6));
										}
										else
										{
											if (FCConvert.ToString(rsType.Get_Fields_String("Account6")) != "M I")
											{
												rsAdd.Set_Fields("Account6", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account6"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 6));
											}
											else
											{
												rsAdd.Set_Fields("Account6", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 6));
											}
										}
									}
									else
									{
										if (FCConvert.ToString(rsType.Get_Fields_String("Account6")) != "M I")
										{
											rsAdd.Set_Fields("Account6", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account6"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 6));
										}
										else
										{
											rsAdd.Set_Fields("Account6", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 6));
										}
									}
								}
								else if ((vbPorterVar5 >= 93 && vbPorterVar5 <= 96) || (vbPorterVar5 >= 893 && vbPorterVar5 <= 896))
								{
									rsRE.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + modUseCR.Statics.ReceiptArray[intReceiptIndex].Account, modExtraModules.strUTDatabase);
									if (!rsRE.EndOfFile())
									{
										if (modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[intReceiptIndex].Account))) && modGlobalConstants.Statics.gboolBD)
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
											{
												// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
											else
											{
												rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
											}
										}
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsType.Get_Fields("Account" + strCT)) != "M I")
										{
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields("Account" + strCT), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
										else
										{
											rsAdd.Set_Fields("Account" + strCT, modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strCT)))));
										}
									}
								}
								else if ((vbPorterVar5 == 97) || (vbPorterVar5 >= 901 && vbPorterVar5 <= 999))
								{
									strARAccount = GetARAccountNumber(strCT, modUseCR.Statics.ReceiptArray[intReceiptIndex].ARBillType, FCConvert.ToString(modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear), modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount);
									if (Strings.Trim(strARAccount) != "")
									{
										rsAdd.Set_Fields("Account" + strCT, strARAccount);
									}
								}
								else
								{
									if (FCConvert.ToString(rsType.Get_Fields_String("Account6")) != "M I")
									{
										rsAdd.Set_Fields("Account6", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account6"), modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 6));
									}
									else
									{
										rsAdd.Set_Fields("Account6", modUseCR.CheckAccountForYear_72(rsType, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, modUseCR.Statics.ReceiptArray[intReceiptIndex].BillingYear, 6));
									}
									if (FCConvert.ToString(rsType.Get_Fields_String("Project6")) != "")
									{
										rsAdd.Set_Fields("Project6", rsType.Get_Fields_String("Project6"));
									}
								}
							}
							else
							{
								rsAdd.Set_Fields("Account6", "");
								rsAdd.Set_Fields("Project6", "");
							}
						}
						intError = 35;
						// Amount1
						rsAdd.Set_Fields("Amount1", modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee1);
						intError = 36;
						// Amount2
						rsAdd.Set_Fields("Amount2", modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee2);
						intError = 37;
						// Amount3
						rsAdd.Set_Fields("Amount3", modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee3);
						intError = 38;
						// Amount4
						rsAdd.Set_Fields("Amount4", modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee4);
						intError = 39;
						// Amount5
						rsAdd.Set_Fields("Amount5", modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee5);
						intError = 40;
						// Amount6
						rsAdd.Set_Fields("Amount6", modUseCR.Statics.ReceiptArray[intReceiptIndex].Fee6);
						intError = 41;
						// Comment
						if (Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].Comment + " ") != "")
						{
							if (Convert.ToByte(Strings.Left(modUseCR.Statics.ReceiptArray[intReceiptIndex].Comment, 1)[0]) != 0)
							{
								rsAdd.Set_Fields("Comment", modUseCR.Statics.ReceiptArray[intReceiptIndex].Comment);
							}
							else
							{
								rsAdd.Set_Fields("Comment", "");
							}
						}
						else
						{
							rsAdd.Set_Fields("Comment", "");
						}
						intError = 42;
						// Ref
						if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Reference.Length > 255)
						{
							rsAdd.Set_Fields("Ref", Strings.Left(modUseCR.Statics.ReceiptArray[intReceiptIndex].Reference, 255));
						}
						else
						{
							rsAdd.Set_Fields("Ref", Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].Reference + " ") + " ");
						}
						intError = 43;
						// Control1
						if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Control1.Length <= 255)
						{
							rsAdd.Set_Fields("Control1", Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].Control1));
						}
						else
						{
							rsAdd.Set_Fields("Control1", Strings.Left(modUseCR.Statics.ReceiptArray[intReceiptIndex].Control1, 255));
						}
						intError = 44;
						// Control2
						if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Control2.Length <= 255)
						{
							rsAdd.Set_Fields("Control2", Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].Control2));
						}
						else
						{
							rsAdd.Set_Fields("Control2", Strings.Left(modUseCR.Statics.ReceiptArray[intReceiptIndex].Control2, 255));
						}
						intError = 45;
						// Control3
						if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Control3.Length <= 255)
						{
							rsAdd.Set_Fields("Control3", Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].Control3));
						}
						else
						{
							rsAdd.Set_Fields("Control3", Strings.Left(modUseCR.Statics.ReceiptArray[intReceiptIndex].Control3, 255));
						}
						intError = 46;
						rsAdd.Set_Fields("CardPaidAmount", modUseCR.Statics.ReceiptArray[intReceiptIndex].CardPaidAmount);
						rsAdd.Set_Fields("CheckPaidAmount", modUseCR.Statics.ReceiptArray[intReceiptIndex].CheckPaidAmount);
						rsAdd.Set_Fields("CashPaidAmount", modUseCR.Statics.ReceiptArray[intReceiptIndex].CashPaidAmount);
						// kgk 01-27-2012 Add InforME  'kgk 09-30-2011  Add Invoice Cloud
						if (modGlobal.Statics.gstrEPaymentPortal == "P" || modGlobal.Statics.gstrEPaymentPortal == "I" || modGlobal.Statics.gstrEPaymentPortal == "E")
						{
							rsAdd.Set_Fields("ConvenienceFee", modUseCR.Statics.ReceiptArray[intReceiptIndex].ConvenienceFee);
							if (modGlobal.Statics.gstrEPaymentPortal == "P")
							{
								rsAdd.Set_Fields("ConvenienceFeeGLAccount", "");
								// .ConvenienceFeeAccount
							}
							else
							{
								rsAdd.Set_Fields("ConvenienceFeeGLAccount", modUseCR.Statics.ReceiptArray[intReceiptIndex].ConvenienceFeeAccount);
							}
							if (modUseCR.Statics.ReceiptArray[intReceiptIndex].CardPaidAmount != 0)
							{
								if (Strings.Trim(FCConvert.ToString(rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount"))) != "")
								{
									if (Strings.Trim(FCConvert.ToString(rsAdd.Get_Fields_String("Account1"))) != "")
									{
										if (modAccountTitle.Statics.YearFlag)
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account1")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount"));
										}
										else
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account1")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount") + "-00");
										}
									}
									else if (Strings.Trim(FCConvert.ToString(rsAdd.Get_Fields_String("Account2"))) != "")
									{
										if (modAccountTitle.Statics.YearFlag)
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account2")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount"));
										}
										else
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account2")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount") + "-00");
										}
									}
									else if (Strings.Trim(FCConvert.ToString(rsAdd.Get_Fields_String("Account3"))) != "")
									{
										if (modAccountTitle.Statics.YearFlag)
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account3")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount"));
										}
										else
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account3")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount") + "-00");
										}
									}
									else if (Strings.Trim(FCConvert.ToString(rsAdd.Get_Fields_String("Account4"))) != "")
									{
										if (modAccountTitle.Statics.YearFlag)
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account4")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount"));
										}
										else
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account4")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount") + "-00");
										}
									}
									else if (Strings.Trim(FCConvert.ToString(rsAdd.Get_Fields_String("Account5"))) != "")
									{
										if (modAccountTitle.Statics.YearFlag)
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account5")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount"));
										}
										else
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account5")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount") + "-00");
										}
									}
									else if (Strings.Trim(FCConvert.ToString(rsAdd.Get_Fields_String("Account6"))) != "")
									{
										if (modAccountTitle.Statics.YearFlag)
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account6")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount"));
										}
										else
										{
											rsAdd.Set_Fields("SeperateCreditCardGLAccount", "G " + modGlobal.PadToString_8(modBudgetaryAccounting.GetFundFromAccount(rsAdd.Get_Fields_String("Account6")), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + rsCashRec.Get_Fields_String("SeperateCreditCardGLAccount") + "-00");
										}
									}
									else
									{
										rsAdd.Set_Fields("SeperateCreditCardGLAccount", "");
									}
								}
								else
								{
									rsAdd.Set_Fields("SeperateCreditCardGLAccount", "");
								}
							}
							else
							{
								rsAdd.Set_Fields("SeperateCreditCardGLAccount", "");
							}
						}
						else
						{
							rsAdd.Set_Fields("ConvenienceFee", 0);
							rsAdd.Set_Fields("ConvenienceFeeGLAccount", "");
							rsAdd.Set_Fields("SeperateCreditCardGLAccount", "");
						}
						// ReceiptNumber
						rsAdd.Set_Fields("ReceiptNumber", lngReceiptKey);
						intError = 50;
						// TellerID
						rsAdd.Set_Fields("TellerID", strTellerID);
						intError = 51;
						// Name
						if (Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].Name + " ") != "")
						{
							if (Convert.ToByte(Strings.Left(modUseCR.Statics.ReceiptArray[intReceiptIndex].Name, 1)[0]) != 0)
							{
								rsAdd.Set_Fields("Name", modUseCR.Statics.ReceiptArray[intReceiptIndex].Name);
							}
							else
							{
								rsAdd.Set_Fields("Name", "");
							}
						}
						else
						{
							rsAdd.Set_Fields("Name", "");
						}
						rsAdd.Set_Fields("NamePartyID", modUseCR.Statics.ReceiptArray[intReceiptIndex].NamePartyID);
						// XXXXX
						intError = 52;
						// Date
						rsAdd.Set_Fields("ArchiveDate", modUseCR.Statics.ReceiptArray[intReceiptIndex].Date);
						rsAdd.Set_Fields("ActualSystemDate", DateTime.Now);
						intError = 53;
						if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Split == 0)
							modUseCR.Statics.ReceiptArray[intReceiptIndex].Split = 1;
						// make sure that the split is not 0
						rsAdd.Set_Fields("Split", modUseCR.Statics.ReceiptArray[intReceiptIndex].Split);
						if (Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].CollectionCode + " ") != "")
						{
							if (Convert.ToByte(Strings.Left(modUseCR.Statics.ReceiptArray[intReceiptIndex].CollectionCode, 1)[0]) != 0)
							{
								rsAdd.Set_Fields("CollectionCode", modUseCR.Statics.ReceiptArray[intReceiptIndex].CollectionCode);
							}
							else
							{
								rsAdd.Set_Fields("CollectionCode", "");
							}
						}
						else
						{
							rsAdd.Set_Fields("CollectionCode", "");
						}
						intError = 54;
						// TellerCloseOut
						// DailyCloseOut
						if (modGlobal.Statics.gboolPendingRI)
						{
							rsAdd.Set_Fields("DailyCloseOut", -100);
							// pending audit code
							rsAdd.Set_Fields("TellerCloseOut", -100);
							intError = 56;
						}
						else
						{
							rsAdd.Set_Fields("DailyCloseOut", 0);
							rsAdd.Set_Fields("TellerCloseOut", 0);
							intError = 57;
						}
						intError = 58;
						// AffectCashDrawer
						if (boolEFTType)
						{
							rsAdd.Set_Fields("AffectCashDrawer", false);
							rsAdd.Set_Fields("AffectCash", true);
						}
						else
						{
							rsAdd.Set_Fields("AffectCashDrawer", modUseCR.Statics.ReceiptArray[intReceiptIndex].AffectCashDrawer);
							intError = 59;
							// AffectCash
							rsAdd.Set_Fields("AffectCash", modUseCR.Statics.ReceiptArray[intReceiptIndex].AffectCash);
							intError = 60;
						}
						// DefaultAccount (not the same as DefaultAccount from the Type Table)
						if (Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].CashAlternativeAcct) == "M" || Strings.InStr(1, modUseCR.Statics.ReceiptArray[intReceiptIndex].CashAlternativeAcct, "_", CompareConstants.vbBinaryCompare) != 0)
						{
							modUseCR.Statics.ReceiptArray[intReceiptIndex].CashAlternativeAcct = "";
						}
						intError = 61;
						rsAdd.Set_Fields("DefaultAccount", modUseCR.Statics.ReceiptArray[intReceiptIndex].CashAlternativeAcct);
						// this is used when the Cash is not affected and the Cash Drawer is not affected
						intError = 62;
						// DefaultCashAccount
						// @DJW 07302009 Added clause to say if there is no default acocunt in Receipt Array to check Receipt Type for a default account and if there is one put it in the receipt array
						if (FCConvert.ToString(rsAdd.Get_Fields_String("DefaultAccount")) == "")
						{
							if (Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultCashAccount) == "M" || Strings.InStr(1, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultCashAccount, "_", CompareConstants.vbBinaryCompare) != 0 || Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultCashAccount) == "")
							{
								if (FCConvert.ToString(rsType.Get_Fields_String("DefaultAccount")) != "")
								{
									modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultCashAccount = FCConvert.ToString(rsType.Get_Fields_String("DefaultAccount"));
								}
								else
								{
									modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultCashAccount = "";
								}
							}
						}
						else
						{
							modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultCashAccount = "";
						}
						// DefaultMIAccount
						intError = 63;
						if (Strings.Trim(modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount) == "M" || Strings.InStr(1, modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount, "_", CompareConstants.vbBinaryCompare) != 0)
						{
							modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount = "";
						}
						rsAdd.Set_Fields("DefaultMIAccount", modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultMIAccount);
						intError = 64;
						// Quantity
						if (modUseCR.Statics.ReceiptArray[intReceiptIndex].Quantity > 1)
						{
							rsAdd.Set_Fields("Quantity", modUseCR.Statics.ReceiptArray[intReceiptIndex].Quantity);
						}
						else
						{
							rsAdd.Set_Fields("Quantity", 1);
						}
						rsAdd.Set_Fields("RecordKey", modUseCR.Statics.ReceiptArray[intReceiptIndex].RecordKey);
						intError = 65;
						rsAdd.Set_Fields("DefaultCashAccount", modUseCR.Statics.ReceiptArray[intReceiptIndex].DefaultCashAccount);
						// this is used when the user does not want to use a seperate fund, but does not want the monies to be passed into the General Cash Account
						intError = 66;
						if (!rsAdd.Update(true))
						{
							MessageBox.Show("There has been an error in the creation of the Archive File.", "Receipt Record ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							lngArchivekey = FCConvert.ToInt32(rsAdd.Get_Fields_Int32("ID"));
						}
						// this will update the ReceiptNumber field from (P)ending to the receipt number of the payment
						// records of any payments from other modules
						;
						var vbPorterVar6 = modUseCR.Statics.ReceiptArray[intReceiptIndex].Type;
						if ((vbPorterVar6 >= 90 && vbPorterVar6 <= 92) || (vbPorterVar6 == 890) || (vbPorterVar6 == 891))
						{
							// CL
							modUseCR.UpdateCLPayment(ref modUseCR.Statics.ReceiptArray[intReceiptIndex].RecordKey, ref lngReceiptKey);
						}
						else if ((vbPorterVar6 >= 93 && vbPorterVar6 <= 96) || (vbPorterVar6 >= 893 && vbPorterVar6 <= 896))
						{
							// UT
							modUseCR.UpdateUTPayment(ref modUseCR.Statics.ReceiptArray[intReceiptIndex].RecordKey, ref lngReceiptKey);
						}
						else if ((vbPorterVar6 == 97) || (vbPorterVar6 >= 901 && vbPorterVar6 <= 999))
						{
							// AR
							modUseCR.UpdateARPayment(ref modUseCR.Statics.ReceiptArray[intReceiptIndex].RecordKey, ref lngReceiptKey);
						}
						else if (vbPorterVar6 == 98)
						{
							// CK
						}
						else if (vbPorterVar6 == 99)
						{
							// MV
						}
						else if (vbPorterVar6 >= 190 && vbPorterVar6 <= 199)
						{
							// Nothing
						}
					}
				}
				intError = 70;
				// this will get the next receipt number and not allow any other process to have the same number
				CreateReceiptRecord = modUseCR.GetNextReceiptNumber();
				strSQL = "UPDATE Receipt SET ReceiptNumber = " + FCConvert.ToString(CreateReceiptRecord) + " WHERE ReceiptKey = " + FCConvert.ToString(lngReceiptKey);
				intError = 71;
				if (modGlobal.Statics.gboolPendingRI)
				{
					modGlobalFunctions.AddCYAEntry_26("CR", "Partial Payment RI", "Receipt #" + FCConvert.ToString(CreateReceiptRecord));
				}
				rsAdd.Reset();
				intError = 72;
				rsAdd.Execute(strSQL, modExtraModules.strCRDatabase);
				intError = 73;

				return CreateReceiptRecord;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Create Receipt Error - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateReceiptRecord;
		}
		// vbPorter upgrade warning: Ind As short	OnWriteFCConvert.ToInt32(
		private void CreateCreditCardRecord(ref int RNum, ref int Ind)
		{
			// this will create the record in the CCMaster
			clsDRWrapper rsCRecord = new clsDRWrapper();
			rsCRecord.OpenRecordset("SELECT * FROM CCMaster");
			rsCRecord.AddNew();
			rsCRecord.Set_Fields("Amount", arr[Ind].Amount);
			rsCRecord.Set_Fields("CCType", FCConvert.ToString(Conversion.Val(arr[Ind].CType)));
			rsCRecord.Set_Fields("ReceiptNumber", RNum);
			// Tracker Reference: 16641
			rsCRecord.Set_Fields("EPymtRefNumber", arr[Ind].RefNumber);
			rsCRecord.Set_Fields("Last4Digits", Strings.Right(arr[Ind].CNum, 4));
			if (modGlobal.Statics.gstrEPaymentPortal == "P")
			{
				rsCRecord.Set_Fields("CryptCard", arr[Ind].CryptCard);
				rsCRecord.Set_Fields("CryptCardExpiration", arr[Ind].ExpDate);
				rsCRecord.Set_Fields("ConvenienceFee", arr[Ind].ConvenienceFee);
				rsCRecord.Set_Fields("MaskedCreditCardNumber", arr[Ind].CNum);
				rsCRecord.Set_Fields("AuthCode", arr[Ind].AuthCode);
				rsCRecord.Set_Fields("IsVISA", arr[Ind].IsVISA);
				rsCRecord.Set_Fields("IsDebit", arr[Ind].IsDebit);
				rsCRecord.Set_Fields("NonTaxAmount", arr[Ind].NonTaxAmount);
				rsCRecord.Set_Fields("AuthCodeConvenienceFee", arr[Ind].AuthCodeConvenienceFee);
				rsCRecord.Set_Fields("AuthCodeVISANonTax", arr[Ind].AuthCodeVISANonTax);
				rsCRecord.Set_Fields("NonTaxConvenienceFee", arr[Ind].NonTaxConvenienceFee);
				rsCRecord.Set_Fields("AuthCodeNonTaxConvenienceFee", arr[Ind].AuthCodeNonTaxConvenienceFee);
			}
			if (modGlobal.Statics.gstrEPaymentPortal == "I")
			{
				// kgk 09-30-2011  Add Invoice Cloud
				rsCRecord.Set_Fields("SecureGUID", arr[Ind].CryptCard);
				rsCRecord.Set_Fields("CryptCardExpiration", arr[Ind].ExpDate);
				rsCRecord.Set_Fields("ConvenienceFee", arr[Ind].ConvenienceFee);
				rsCRecord.Set_Fields("MaskedCreditCardNumber", arr[Ind].CNum);
				rsCRecord.Set_Fields("AuthCode", arr[Ind].AuthCode);
				rsCRecord.Set_Fields("IsVISA", arr[Ind].IsVISA);
				rsCRecord.Set_Fields("IsDebit", arr[Ind].IsDebit);
			}
			if (modGlobal.Statics.gstrEPaymentPortal == "E")
			{
				// kgk 01-27-2012  Add InforME
				// rsCRecord.Fields("SecureGUID") = arr(Ind).CryptCard     ' No cryptcard
				rsCRecord.Set_Fields("CryptCardExpiration", arr[Ind].ExpDate);
				rsCRecord.Set_Fields("ConvenienceFee", arr[Ind].ConvenienceFee);
				rsCRecord.Set_Fields("MaskedCreditCardNumber", arr[Ind].CNum);
				rsCRecord.Set_Fields("AuthCode", arr[Ind].AuthCode);
				rsCRecord.Set_Fields("IsVISA", arr[Ind].IsVISA);
				rsCRecord.Set_Fields("IsDebit", arr[Ind].IsDebit);
				rsCRecord.Set_Fields("EPmtAcctID", arr[Ind].EPmtAcctID);
			}
			if (arr[Ind].OrigReceipt == "")
			{
				rsCRecord.Set_Fields("OriginalReceiptKey", 0);
			}
			else
			{
				rsCRecord.Set_Fields("OriginalReceiptKey", arr[Ind].OrigReceipt);
			}
			rsCRecord.Update(false);
		}
		// vbPorter upgrade warning: Ind As short	OnWriteFCConvert.ToInt32(
		private void CreateCheckRecord(ref int RNum, ref int Ind)
		{
			// this will create the record in the CheckMaster
			clsDRWrapper rsCRecord = new clsDRWrapper();
			clsDRWrapper rsBank = new clsDRWrapper();
			rsCRecord.OpenRecordset("SELECT * FROM CheckMaster");
			rsCRecord.AddNew();
			rsCRecord.Set_Fields("Amount", arr[Ind].Amount);
			// this will take the name and the spacer hyphen out of the routing transit number and look it
			// up in the bank table in order to store the Bank.ID
			rsBank.OpenRecordset("SELECT * FROM Bank WHERE RoutingTransit = '" + arr[Ind].Bank + "'");
			if (rsBank.EndOfFile() != true && rsBank.BeginningOfFile() != true)
			{
				rsCRecord.Set_Fields("BankNumber", rsBank.Get_Fields_Int32("ID"));
			}
			rsCRecord.Set_Fields("CheckNumber", arr[Ind].CType);
			rsCRecord.Set_Fields("ReceiptNumber", RNum);
			if (modUseCR.Statics.ReceiptArray[1].EFT)
			{
				rsCRecord.Set_Fields("EFT", true);
			}
			// Tracker Reference: 16641
			rsCRecord.Set_Fields("EPymtRefNumber", arr[Ind].RefNumber);
			rsCRecord.Set_Fields("AccountType", arr[Ind].ChkAcctType);
			if (arr[Ind].OrigReceipt == "")
			{
				rsCRecord.Set_Fields("OriginalReceiptKey", 0);
			}
			else
			{
				rsCRecord.Set_Fields("OriginalReceiptKey", arr[Ind].OrigReceipt);
			}
			rsCRecord.Update(false);
		}

		private void vsSummary_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						if (fraCashOut.Visible != true)
						{
							// if the cashout screen is not showing, then attempt to delete it
							// delete the current item from the "Shopping List"
							modUseCR.DeletePaymentFromSummaryByKeystroke(vsSummary.Row);
							modUseCR.CalculateSummaryTotal();
						}
						break;
					}
			}
			//end switch
		}

		private void vsSummary_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vsSummary.Col == 5)
			{
				if ((keyAscii >= FCConvert.ToInt32(Keys.D0) && keyAscii <= FCConvert.ToInt32(Keys.D9)) || (keyAscii == 8))
				{
					// do nothing
				}
				else
				{
					keyAscii = 0;
				}
			}
		}

		private void vsSummary_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngMRow;
			if (fraPaymentInfo.Visible)
			{
				fraPaymentInfo.Visible = false;
				fraPaidBy.Visible = true;
				// kgk 12-29-2011  trocr-311
			}
			lngMRow = vsSummary.MouseRow;
			if (e.Button == MouseButtons.Right && lngMRow > 0)
			{
				// right click
				ViewPaymentRow(ref lngMRow);
			}
		}

		private void vsSummary_RowColChange(object sender, System.EventArgs e)
		{
			// this allows the drop down to occur and let the user choose a value
			if (vsSummary.Row > 0)
			{
				if (vsSummary.Col == 7 || vsSummary.Col == 5)
				{
					if (intCurFrame == 2)
					{
						vsSummary.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						vsSummary.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					vsSummary.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				vsSummary.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void PrintReceipt()
		{
			int lngError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will print the receipt(s) needed by
				// vbPorter upgrade warning: intCT As short --> As int	OnRead(string, int)
				int intCT;
				int lngRows;
				clsDRWrapper rsSplit = new clsDRWrapper();
				clsDRWrapper rsBK = new clsDRWrapper();
				bool boolPrint = false;
				bool boolBatch = false;
				int intSplit = 0;
				int lngBillKey;
				string strTemp = "";
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				int lngChkPrint;
				string strOldPrinter = "";
				string strPrinter = "";
				lngError = 1;
				modUseCR.Statics.strPrinterName = StaticSettings.GlobalSettingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName")?.SettingValue ?? "";
				lngError = 2;

				if (modUseCR.Statics.strPrinterName != "")
				{
					// check to see if the printer that is setup is a file rather than a printer
					lngError = 3;
					for (intCT = 1; intCT <= vsSummary.Rows - 1; intCT++)
					{
						if (vsSummary.TextMatrix(intCT, 2) != "***")
						{
							// this will update the split of any of the lines that are not a batch line
							// this is for regular payments only
							intSplit = FCConvert.ToInt16(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(Math.Round(Conversion.Val(vsSummary.TextMatrix(intCT, 8))))].Split);
							if (intSplit == 0)
							{
								intSplit = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSummary.TextMatrix(intCT, 7))));
								if (intSplit == 0)
									intSplit = 1;
							}
							modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(intCT, 8))].Split = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSummary.TextMatrix(intCT, 7))));
							lngMultiTown = modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(Math.Round(Conversion.Val(vsSummary.TextMatrix(intCT, 8))))].ResCode;
							rsSplit.Execute("UPDATE Archive SET Split = " + FCConvert.ToString(intSplit) + " WHERE ID = " + FCConvert.ToString(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(intCT, 8))].RecordKey), "TWCR0000.vb1");
						}
						else
						{
							// this is for batch updated only
							for (intSplit = 1; intSplit <= modStatusPayments.MAX_PAYMENTS; intSplit++)
							{
								if (modUseCR.Statics.ReceiptArray[intSplit].Split > 5)
								{
									rsSplit.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(modUseCR.Statics.ReceiptArray[intSplit].RecordKey), modExtraModules.strCLDatabase);
									if (rsSplit.EndOfFile() != true && rsSplit.BeginningOfFile() != true)
									{
										// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
										strTemp = "SELECT * FROM Archive WHERE ReceiptNumber = " + rsSplit.Get_Fields_Int32("ReceiptNumber") + " AND AccountNumber = " + rsSplit.Get_Fields("Account") + " AND Control1 = '" + rsSplit.Get_Fields("Year") + "'";
										rsSplit.OpenRecordset(strTemp, modExtraModules.strCRDatabase);
										if (rsSplit.EndOfFile() != true && rsSplit.BeginningOfFile() != true)
										{
											rsSplit.Set_Fields("Split", modUseCR.Statics.ReceiptArray[intSplit].Split);
											rsSplit.Update(false);
										}
										else
										{
											// could not find the Archive Record that corresponds to this payment
										}
									}
									else
									{
										// could not find the payment record
									}
								}
							}
						}
					}
					lngError = 4;
					// reinitialize the totals
					for (intCT = 1; intCT <= 5; intCT++)
					{
						modUseCR.Statics.dblSplitTotals[intCT] = 0;
					}
					// fill the totals
					for (intCT = 1; intCT <= 5; intCT++)
					{
						// this will total all of the splits
						if (vsSummary.FindRow(intCT, col:7) != -1)
						{
							// checks to see if there are any of this split number
							for (lngRows = 1; lngRows <= vsSummary.Rows - 1; lngRows++)
							{
								if (Conversion.Val(vsSummary.TextMatrix(lngRows, 7)) == intCT)
								{
									// is this on the same split
									if (modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(Conversion.Val(this.vsSummary.TextMatrix(lngRows, 8)))].CashAlternativeAcct == "")
									{
										// if this row should be counted  'And .TextMatrix(lngRows, 2) <> "***"
										modUseCR.Statics.dblSplitTotals[intCT] += FCConvert.ToDouble(vsSummary.TextMatrix(lngRows, 6)) + FCConvert.ToDouble(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(this.vsSummary.TextMatrix(lngRows, 8))].ConvenienceFee);
										// ElseIf .TextMatrix(lngRows, 2) <> "***" Then
									}
								}
							}
						}
					}
					lngError = 5;
                    boolBatch = (vsSummary.FindRow("***", col: 2) != -1);

					for (intCT = 1; intCT <= 5; intCT++)
					{
						if (vsSummary.FindRow(intCT, col:7) != -1)
						{
							// print only the splits that have payments
							strPassTag = FCConvert.ToString(intCT);
							lngError = 6;
							if (boolPrint)
							{
								if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
								{
									arReceipt.InstancePtr.StartUp();
								}
								else
								{
									arGenericReceipt.InstancePtr.StartUp();
								}
							}
							for (lngChkPrint = 1; lngChkPrint <= vsSummary.Rows - 1; lngChkPrint++)
							{
								if (modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(this.vsSummary.TextMatrix(lngChkPrint, 8))].PrintReceipt)
								{
									boolPrint = true;
								}
							}
							// boolPrint = True            'check to see if this split should be printed or not
							modGlobal.Statics.gboolReprint = false;
							lngError = 7;
							if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
							{
								lngError = 8;
								if (SetupAsPrinterFile(ref modUseCR.Statics.strPrinterName))
								{
									// if it is then export it to a file
									lngError = 9;
									ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
									arReceipt.InstancePtr.Run(false);
									string fileName = "PRNT" + FCConvert.ToString(intCT) + ".TXT";
									ARTemp.Export(arReceipt.InstancePtr.Document, fileName);
								}
								else
								{
									lngError = 10;
									if (boolPrint)
									{
										lngError = 11;
										// arReceipt.Run False
										// arReceipt.Show vbModal, MDIParent
										lngError = 12;
										arReceipt.InstancePtr.Unload();
                                        //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                                        //arReceipt.InstancePtr.PrintReport();
                                        arReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
										lngError = 13;
									}
								}
								lngError = 15;
								//arReceipt.InstancePtr.Hide();
								// make sure to uncomment this line when sending real code
								lngError = 16;
							}
							else
							{
								if (SetupAsPrinterFile(ref modUseCR.Statics.strPrinterName))
								{
									// if it is then export it to a file
									ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
									arGenericReceipt.InstancePtr.Run(false);
									string fileName = "PRNT" + FCConvert.ToString(intCT) + ".TXT";
									//Application.DoEvents();
									// This will wait until report is run before it exports
									ARTemp.Export(arGenericReceipt.InstancePtr.Document, fileName);
								}
								else
								{
									arGenericReceipt.InstancePtr.Unload();
									//FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                                    //arGenericReceipt.InstancePtr.PrintReport();
                                    arGenericReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
                                    // arGenericReceipt.Show
                                }
								//arGenericReceipt.InstancePtr.Hide();
							}
						}
					}
					// End If
					intCT = 1;
					if (boolBatch)
					{
						do
						{
							// this will assume that the receipt array has been filled with both the payments from
							// the grid and the payments from the batch update...ALL of the split numbers for a batch payment
							// will be greater than 5
							// individually print each batch line item
							if (modUseCR.Statics.ReceiptArray[intCT].Split > 5 && modUseCR.Statics.ReceiptArray[intCT].Used && modUseCR.Statics.ReceiptArray[intCT].Copies > 0)
							{
								strPassTag = FCConvert.ToString(modUseCR.Statics.ReceiptArray[intCT].Split);
								intRAIndex = intCT;
								if (boolPrint)
								{
									if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
									{
										arReceipt.InstancePtr.StartUp();
									}
									else
									{
										arGenericReceipt.InstancePtr.StartUp();
									}
								}
								boolPrint = true;
								modGlobal.Statics.gboolReprint = false;
								if (modGlobal.Statics.gboolSP200Printer || !modGlobal.Statics.gboolNarrowReceipt)
								{
									if (SetupAsPrinterFile(ref modUseCR.Statics.strPrinterName))
									{
										// if it is then export it to a file
										ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
										arReceipt.InstancePtr.Run(false);
										string fileName = "PRNT" + FCConvert.ToString(intCT) + ".TXT";
										//Application.DoEvents();
										// This will wait until report is run before it exports
										//arReceipt.InstancePtr.Export(ARTemp);
										ARTemp.Export(arReceipt.InstancePtr.Document, fileName);
									}
									else
									{
										arReceipt.InstancePtr.Unload();
                                        //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                                        //arReceipt.InstancePtr.PrintReport();
                                        arReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
                                    }
									//arReceipt.InstancePtr.Hide();
								}
								else
								{
									if (SetupAsPrinterFile(ref modUseCR.Statics.strPrinterName))
									{
										// if it is then export it to a file
										ARTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
										arGenericReceipt.InstancePtr.Run(false);
										string fileName = "PRNT" + FCConvert.ToString(intCT) + ".TXT";
										//Application.DoEvents();
										// This will wait until report is run before it exports
										ARTemp.Export(arGenericReceipt.InstancePtr.Document, fileName);
									}
									else
									{
										arGenericReceipt.InstancePtr.Unload();
                                        //FC:FINAL:SBE - print report on dot matrix printer (receipt report)
                                        //arGenericReceipt.InstancePtr.PrintReport();
                                        arGenericReceipt.InstancePtr.PrintReportOnDotMatrix("RCPTPrinterName");
                                        // arGenericReceipt.Show
                                    }
									//arGenericReceipt.InstancePtr.Hide();
								}
								intCT += 1;
							}
							else
							{
								intCT += 1;
							}
						}
						while (!(modUseCR.Statics.ReceiptArray[intCT].Used == false));
					}
				}
				else
				{
					MessageBox.Show("There is no Receipt Printer setup.  Please go to General Entry > System Maintenance > Custom Settings > Printer Setup and select a receipt printer and then reprint the last receipt.", "No Receipt Printer Setup", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Print Receipt Error - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RemoveNONCRPayments()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will delete the payments from the CL/UT database
				int lngRW;
				// this will clear the summary grid and make any deletions in other databases
				for (lngRW = vsSummary.Rows - 1; lngRW >= 1; lngRW--)
				{
					modUseCR.DeletePaymentFromSummaryByKeystroke_6(lngRW, true);
				}
				for (lngRW = 1; lngRW <= Information.UBound(modUseCR.Statics.ReceiptArray, 1) - 1; lngRW++)
				{
					if (modUseCR.Statics.ReceiptArray[lngRW].Used == true && modUseCR.Statics.ReceiptArray[lngRW].Module != "C")
					{
						modUseCR.DeletePaymentFromSummaryByKeystroke_24(lngRW, true, FCConvert.ToInt16(lngRW));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Print Receipt Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void EndReceipt()
		{
			int lngCT;
			// this will close out all arrays and variables and reset anything that can change in order
			// not to have any residue for the next receipt, it will also fill the paid by and tellID fields
			// just in case they did not fill correctly from any of the other modules
			// this will update the payments in the CL or UT system with teller id, paid by and other information from CR

			FCUtils.EraseSafe(arr);
			FCUtils.EraseSafe(modUseCR.Statics.ReceiptArray);
			modUseCR.Statics.PaymentBreakdownInfoArray = new modUseCR.PaymentBreakdownInfo[0 + 1];
			modUseCR.Statics.intPaymentBreakdownInfoCounter = 0;
			FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
			blnIsVoid = false;
		}

		private bool CheckNumberValidate_8(string strCkNum, string strRT)
		{
			return CheckNumberValidate(ref strCkNum, ref strRT);
		}

		private bool CheckNumberValidate(ref string strCkNum, ref string strRT)
		{
			bool CheckNumberValidate = false;
			// this function will check to see if there is an entry with the same check number and bank number
			clsDRWrapper rsVal = new clsDRWrapper();
			rsVal.OpenRecordset("SELECT * FROM ((CheckMaster INNER JOIN Bank ON CheckMaster.BankNumber = Bank.ID) INNER JOIN Archive ON CheckMaster.ReceiptNumber = Archive.ReceiptNumber) WHERE CheckNumber = '" + strCkNum + "' AND RoutingTransit = '" + Strings.Trim(strRT) + "' AND ISNULL(DailyCloseOut,0) = 0", modExtraModules.strCRDatabase);
			CheckNumberValidate = FCConvert.CBool(rsVal.EndOfFile() && rsVal.BeginningOfFile());
			return CheckNumberValidate;
		}

		private bool BankRTValidate_2(string strRT)
		{
			return BankRTValidate(ref strRT);
		}

		private bool BankRTValidate(ref string strRT)
		{
			bool BankRTValidate = false;
			// this function will check to see if there is an entry with the same check number and bank number
			clsDRWrapper rsVal = new clsDRWrapper();
			rsVal.OpenRecordset("SELECT * FROM Bank WHERE RoutingTransit = '" + strRT + "'");
			BankRTValidate = FCConvert.CBool(rsVal.EndOfFile() && rsVal.BeginningOfFile());
			return BankRTValidate;
		}
		// vbPorter upgrade warning: Index As short	OnWriteFCConvert.ToInt32(
		private void AddPaymentToGrid(ref int Index)
		{
			// this will take a payment from the payment array and add it to the payment grid
			if (arr[Index].Type == 0)
			{
				// cash
				vsPayments.AddItem("CSH" + "\t" + FCConvert.ToString(arr[Index].Amount) + "\t" + arr[Index].CType + "\t" + FCConvert.ToString(Index) + "\t" + FCConvert.ToString(arr[Index].ConvenienceFee) + "\t" + arr[Index].CNum + "\t" + arr[Index].ExpDate + "\t" + FCConvert.ToString(arr[Index].IsVISA) + "\t" + FCConvert.ToString(arr[Index].IsDebit) + "\t" + FCConvert.ToString(arr[Index].NonTaxAmount) + "\t" + false + "\t" + FCConvert.ToString(arr[Index].NonTaxConvenienceFee));
			}
			else if (arr[Index].Type == 1)
			{
				// check
				vsPayments.AddItem("CHK" + "\t" + FCConvert.ToString(arr[Index].Amount) + "\t" + arr[Index].CType + "\t" + FCConvert.ToString(Index) + "\t" + FCConvert.ToString(arr[Index].ConvenienceFee) + "\t" + arr[Index].CNum + "\t" + arr[Index].ExpDate + "\t" + FCConvert.ToString(arr[Index].IsVISA) + "\t" + FCConvert.ToString(arr[Index].IsDebit) + "\t" + FCConvert.ToString(arr[Index].NonTaxAmount) + "\t" + false + "\t" + FCConvert.ToString(arr[Index].NonTaxConvenienceFee));
			}
			else
			{
				// card
				vsPayments.AddItem("CC" + "\t" + FCConvert.ToString(arr[Index].Amount) + "\t" + arr[Index].Bank + "\t" + FCConvert.ToString(Index) + "\t" + FCConvert.ToString(arr[Index].ConvenienceFee) + "\t" + arr[Index].CNum + "\t" + arr[Index].ExpDate + "\t" + FCConvert.ToString(arr[Index].IsVISA) + "\t" + FCConvert.ToString(arr[Index].IsDebit) + "\t" + FCConvert.ToString(arr[Index].NonTaxAmount) + "\t" + false + "\t" + FCConvert.ToString(arr[Index].NonTaxConvenienceFee));
			}
			// MAL@20090120 ; Tracker Reference: 16641
			if (fraTransDetailsCheck.Visible)
			{
				fraTransDetailsCheck.Visible = false;
			}
			if (fraTransDetailsCC.Visible)
			{
				fraTransDetailsCC.Visible = false;
			}
		}

		private void DeletePaymentFromPaymentBreakdownArray(ref int intRow, ref short intType)
		{
			int counter;
			int counter2;
			int intDeleted;
			intDeleted = 0;
			for (counter = modUseCR.Statics.intPaymentBreakdownInfoCounter - 1; counter >= 0; counter--)
			{
				if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].PaymentIndex == intRow)
				{
					if (intType == 0)
					{
						modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].CashPaidAmount -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CashAmount;
					}
					else if (intType == 1)
					{
						modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].CheckPaidAmount = modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].CardPaidAmount - modUseCR.Statics.PaymentBreakdownInfoArray[counter].CheckAmount;
					}
					else
					{
						modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].CardPaidAmount -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
					}
					modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].ConvenienceFee -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
					for (counter2 = counter + 1; counter2 <= modUseCR.Statics.intPaymentBreakdownInfoCounter - 1; counter2++)
					{
						modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].CardAmount = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].CardAmount;
						modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].CashAmount = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].CashAmount;
						modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].CheckAmount = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].CheckAmount;
						modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].ConvenienceFee = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].ConvenienceFee;
						modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].PaymentIndex = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].PaymentIndex;
						modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].ReceiptIndex = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].ReceiptIndex;
					}
					intDeleted += 1;
				}
			}
			if (intDeleted > 0)
			{
				modUseCR.Statics.intPaymentBreakdownInfoCounter -= intDeleted;
				if (modUseCR.Statics.intPaymentBreakdownInfoCounter > 0)
				{
					Array.Resize(ref modUseCR.Statics.PaymentBreakdownInfoArray, modUseCR.Statics.intPaymentBreakdownInfoCounter - 1 + 1);
				}
				else
				{
					modUseCR.Statics.PaymentBreakdownInfoArray = new modUseCR.PaymentBreakdownInfo[1];
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					modUseCR.Statics.PaymentBreakdownInfoArray[0] = new modUseCR.PaymentBreakdownInfo(0);
				}
			}
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private void RemoveCreditCardAmounts_78(int intRow, bool blnRemoveConv, bool blnRemoveNonTax, bool blnRemoveNonTaxConvenienceFee)
		{
			RemoveCreditCardAmounts(ref intRow, ref blnRemoveConv, ref blnRemoveNonTax, ref blnRemoveNonTaxConvenienceFee);
		}

		private void RemoveCreditCardAmounts(ref int intRow, ref bool blnRemoveConv, ref bool blnRemoveNonTax, ref bool blnRemoveNonTaxConvenienceFee)
		{
			int Ind;
			// vbPorter upgrade warning: curConvenienceFee As Decimal	OnWrite(short, Decimal)
			Decimal curConvenienceFee;
			// vbPorter upgrade warning: curNonTaxAmount As Decimal	OnWrite(Decimal, short)
			Decimal curNonTaxAmount;
			Ind = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(intRow, 3))));
			curConvenienceFee = 0;
			if (modGlobal.Statics.gblnNoVISAPayments)
			{
				if (blnRemoveConv)
				{
					curConvenienceFee = arr[Ind].ConvenienceFee;
				}
			}
			else
			{
				if (blnRemoveConv)
				{
					if (blnRemoveNonTaxConvenienceFee)
					{
						curConvenienceFee = arr[Ind].ConvenienceFee;
					}
					else
					{
						curConvenienceFee = arr[Ind].ConvenienceFee - arr[Ind].NonTaxConvenienceFee;
					}
				}
				else
				{
					if (blnRemoveNonTaxConvenienceFee)
					{
						curConvenienceFee = arr[Ind].NonTaxConvenienceFee;
					}
				}
			}
			if (blnRemoveNonTax)
			{
				curNonTaxAmount = arr[Ind].NonTaxAmount;
			}
			else
			{
				curNonTaxAmount = 0;
			}
			vsPayments.TextMatrix(intRow, 1, FCConvert.ToString(FCConvert.ToDecimal(vsPayments.TextMatrix(intRow, 1)) - curNonTaxAmount - curConvenienceFee));
			if (blnRemoveNonTax)
			{
				vsPayments.TextMatrix(intRow, 9, FCConvert.ToString(0));
			}
			if (modGlobal.Statics.gblnNoVISAPayments)
			{
				if (blnRemoveConv)
				{
					vsPayments.TextMatrix(intRow, 4, FCConvert.ToString(0));
					vsPayments.TextMatrix(intRow, 11, FCConvert.ToString(0));
				}
			}
			else
			{
				if (blnRemoveConv)
				{
					if (blnRemoveNonTaxConvenienceFee)
					{
						vsPayments.TextMatrix(intRow, 4, FCConvert.ToString(0));
						vsPayments.TextMatrix(intRow, 11, FCConvert.ToString(0));
					}
					else
					{
						vsPayments.TextMatrix(intRow, 4, vsPayments.TextMatrix(intRow, 11));
					}
				}
				else
				{
					if (blnRemoveNonTaxConvenienceFee)
					{
						vsPayments.TextMatrix(intRow, 4, FCConvert.ToString(FCConvert.ToDecimal(vsPayments.TextMatrix(intRow, 4)) - FCConvert.ToDecimal(vsPayments.TextMatrix(intRow, 11))));
						vsPayments.TextMatrix(intRow, 11, FCConvert.ToString(0));
					}
				}
			}
			txtCashOutCreditPaid.Text = Strings.Format(FCConvert.ToDecimal(FCConvert.ToDouble(txtCashOutCreditPaid.Text)) - curConvenienceFee - curNonTaxAmount, "#,##0.00");
			txtConvenienceFee.Text = Strings.Format(FCConvert.ToDecimal(FCConvert.ToDouble(txtConvenienceFee.Text)) - curConvenienceFee, "#,##0.00");
			RemoveCreditCardAmountsFromPaymentBreakdownArray(ref intRow, ref blnRemoveConv, ref blnRemoveNonTax, ref blnRemoveNonTaxConvenienceFee);
			if (blnRemoveConv)
			{
				arr[Ind].ConvenienceFee = 0;
			}
			if (modGlobal.Statics.gblnNoVISAPayments)
			{
				if (blnRemoveConv)
				{
					arr[Ind].ConvenienceFee = 0;
					arr[Ind].NonTaxConvenienceFee = 0;
				}
			}
			else
			{
				if (blnRemoveConv)
				{
					if (blnRemoveNonTaxConvenienceFee)
					{
						arr[Ind].ConvenienceFee = 0;
						arr[Ind].NonTaxConvenienceFee = 0;
					}
					else
					{
						arr[Ind].ConvenienceFee = arr[Ind].NonTaxConvenienceFee;
					}
				}
				else
				{
					if (blnRemoveNonTaxConvenienceFee)
					{
						arr[Ind].ConvenienceFee -= arr[Ind].NonTaxConvenienceFee;
						arr[Ind].NonTaxConvenienceFee = 0;
					}
				}
			}
			if (blnRemoveNonTax)
			{
				arr[Ind].NonTaxAmount = 0;
			}
			arr[Ind].Amount += FCConvert.ToDouble(-curConvenienceFee - curNonTaxAmount);
			CalculateTotals();
			cmdCashOut.Text = "Enter";
			mnuFileCashOut.Text = cmdCashOut.Text;
			cmdCashOut.Refresh();
			txtPayment.Text = Strings.Format(FCConvert.ToDouble(txtCashOutTotalDue.Text), "#,##0.00");
			txtPayment.SelectionStart = 0;
			txtPayment.SelectionLength = txtPayment.Text.Length;
			// MAL@20080115: Re-enable disabled fields
			// Tracker Reference: 11717
			if (boolEFTType)
			{
				// only allow checks when it is an EFT check
				if (cmbCashOutType.Items.Contains("Cash"))
				{
					cmbCashOutType.Items.Remove("Cash");
				}
				if (modGlobal.Statics.gblnAcceptEChecks)
				{
					if (!cmbCashOutType.Items.Contains("E-Check"))
					{
						cmbCashOutType.Items.Insert(1, "E-Check");
					}
				}
				else
				{
					if (!cmbCashOutType.Items.Contains("Check"))
					{
						cmbCashOutType.Items.Insert(1, "Check");
					}
				}
				if (cmbCashOutType.Items.Contains("Credit Card"))
				{
					cmbCashOutType.Items.Remove("Credit Card");
				}
				txtCashOutCheckPaid.Enabled = true;
			}
			else
			{
				if (!cmbCashOutType.Items.Contains("Cash"))
				{
					cmbCashOutType.Items.Insert(0, "Cash");
				}
				if (modGlobal.Statics.gblnAcceptEChecks)
				{
					if (!cmbCashOutType.Items.Contains("E-Check"))
					{
						cmbCashOutType.Items.Insert(1, "E-Check");
					}
				}
				else
				{
					if (!cmbCashOutType.Items.Contains("Check"))
					{
						cmbCashOutType.Items.Insert(1, "Check");
					}
				}
				if (FillCCCombo() && !cmbCashOutType.Items.Contains("Credit Card"))
				{
					cmbCashOutType.Items.Insert(2, "Credit Card");
				}
				else if (!FillCCCombo() && cmbCashOutType.Items.Contains("Credit Card"))
				{
					cmbCashOutType.Items.Remove("Credit Card");
				}
				txtCashOutCashPaid.Enabled = true;
				txtCashOutCheckPaid.Enabled = true;
				txtCashOutCreditPaid.Enabled = FCConvert.CBool(cmbCashOutType.SelectedIndex == 2);
			}
			if (blnRemoveNonTax)
			{
				MessageBox.Show("We were only able to authorize a portion of the amount owed on the credit card.  You will have to work out some other form of payment for the remainder.", "Unable To Authorize", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("We were unable to authorize a portion or all of your convenience fees.  These unauthorized amounts have been removed from your transaction.", "Unable To Authorize", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			txtPayment.Enabled = true;
			txtPayment.Focus();
		}

		private void RemoveCreditCardAmountsFromPaymentBreakdownArray(ref int intRow, ref bool blnRemoveConv, ref bool blnRemoveNonTax, ref bool blnRemoveNonTaxConvenienceFee)
		{
			// - "AutoDim"
			int counter;
			int counter2;
			int intDeleted;
			int Ind;
			// vbPorter upgrade warning: curConvenienceFee As Decimal	OnWrite(Decimal, short)
			Decimal curConvenienceFee;
			// vbPorter upgrade warning: curNonTaxAmount As Decimal	OnWrite(Decimal, short)
			Decimal curNonTaxAmount;
			// vbPorter upgrade warning: curNonTaxConvenienceFee As Decimal	OnWrite(Decimal, short)
			Decimal curNonTaxConvenienceFee;
			Ind = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(intRow, 3))));
			if (blnRemoveNonTaxConvenienceFee && !modGlobal.Statics.gblnNoVISAPayments)
			{
				curNonTaxConvenienceFee = arr[Ind].NonTaxConvenienceFee;
			}
			else
			{
				curNonTaxConvenienceFee = 0;
			}
			if (blnRemoveConv)
			{
				curConvenienceFee = arr[Ind].ConvenienceFee - curNonTaxConvenienceFee;
			}
			else
			{
				curConvenienceFee = 0;
			}
			if (blnRemoveNonTax)
			{
				curNonTaxAmount = arr[Ind].NonTaxAmount;
			}
			else
			{
				curNonTaxAmount = 0;
			}
			intDeleted = 0;
			for (counter = 0; counter <= modUseCR.Statics.intPaymentBreakdownInfoCounter - 1; counter++)
			{
				if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].PaymentIndex == intRow)
				{
					if (blnRemoveConv)
					{
						if ((modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891) || modGlobal.Statics.gblnNoVISAPayments)
						{
							if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee >= curConvenienceFee)
							{
								modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].ConvenienceFee -= curConvenienceFee;
								modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee -= curConvenienceFee;
								curConvenienceFee = 0;
							}
							else
							{
								curConvenienceFee -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
								modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].ConvenienceFee -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
								modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = 0;
							}
						}
					}
					if (blnRemoveNonTaxConvenienceFee)
					{
						if ((modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891) || modGlobal.Statics.gblnNoVISAPayments)
						{
							// do nothing
						}
						else
						{
							if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee >= curNonTaxConvenienceFee)
							{
								modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].ConvenienceFee -= curNonTaxConvenienceFee;
								modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee -= curNonTaxConvenienceFee;
								curNonTaxConvenienceFee = 0;
							}
							else
							{
								curNonTaxConvenienceFee -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
								modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].ConvenienceFee -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee;
								modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee = 0;
							}
						}
					}
					if (blnRemoveNonTax)
					{
						if (modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 90 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 91 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 92 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 890 || modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].Type == 891)
						{
							// do nothing
						}
						else
						{
							if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount >= curNonTaxAmount)
							{
								modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].CardPaidAmount -= curNonTaxAmount;
								modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount -= curNonTaxAmount;
								curNonTaxAmount = 0;
							}
							else
							{
								curNonTaxAmount -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
								modUseCR.Statics.ReceiptArray[modUseCR.Statics.PaymentBreakdownInfoArray[counter].ReceiptIndex].CardPaidAmount -= modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount;
								modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount = 0;
							}
						}
					}
					if (modUseCR.Statics.PaymentBreakdownInfoArray[counter].CardAmount == 0 && modUseCR.Statics.PaymentBreakdownInfoArray[counter].CashAmount == 0 && modUseCR.Statics.PaymentBreakdownInfoArray[counter].CheckAmount == 0 && modUseCR.Statics.PaymentBreakdownInfoArray[counter].ConvenienceFee == 0)
					{
						for (counter2 = counter + 1; counter2 <= modUseCR.Statics.intPaymentBreakdownInfoCounter - 1; counter2++)
						{
							modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].CardAmount = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].CardAmount;
							modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].CashAmount = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].CashAmount;
							modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].CheckAmount = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].CheckAmount;
							modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].ConvenienceFee = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].ConvenienceFee;
							modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].PaymentIndex = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].PaymentIndex;
							modUseCR.Statics.PaymentBreakdownInfoArray[counter2 - 1].ReceiptIndex = modUseCR.Statics.PaymentBreakdownInfoArray[counter2].ReceiptIndex;
						}
						intDeleted += 1;
					}
				}
			}
			if (intDeleted > 0)
			{
				modUseCR.Statics.intPaymentBreakdownInfoCounter -= intDeleted;
				if (modUseCR.Statics.intPaymentBreakdownInfoCounter > 0)
				{
					Array.Resize(ref modUseCR.Statics.PaymentBreakdownInfoArray, modUseCR.Statics.intPaymentBreakdownInfoCounter - 1 + 1);
				}
				else
				{
					modUseCR.Statics.PaymentBreakdownInfoArray = new modUseCR.PaymentBreakdownInfo[1];
					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
					modUseCR.Statics.PaymentBreakdownInfoArray[0] = new modUseCR.PaymentBreakdownInfo(0);
				}
			}
		}

		private void DeletePaymentFromGrid_2(int Row)
		{
			DeletePaymentFromGrid(Row);
		}

		private void DeletePaymentFromGrid(int Row)
		{
			// this will delete a payment from the grid and from the underlying payment array
			int Ind = 0;
			if (Row > 0 && Row < vsPayments.Rows)
			{
				// kgk 03-09-2011 trocr-271 Don't allow delete payment if processed
				if (FCConvert.CBool(vsPayments.TextMatrix(Row, 10)))
				{
					MessageBox.Show("The cannot be deleted because it has already been processed.");
				}
				else
				{
					Ind = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(Row, 3))));
					vsPayments.RemoveItem(Row);
					switch (arr[Ind].Type)
					{
						case 0:
							{
								// cash
								txtCashOutCashPaid.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCashPaid.Text) - arr[Ind].Amount, "#,##0.00");
								break;
							}
						case 1:
							{
								// check
								txtCashOutCheckPaid.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCheckPaid.Text) - arr[Ind].Amount, "#,##0.00");
								break;
							}
						case 2:
							{
								// card
								txtCashOutCreditPaid.Text = Strings.Format(FCConvert.ToDouble(txtCashOutCreditPaid.Text) - arr[Ind].Amount, "#,##0.00");
								break;
							}
					}
					//end switch
					txtConvenienceFee.Text = Strings.Format(FCConvert.ToDecimal(txtConvenienceFee.Text) - arr[Ind].ConvenienceFee, "#,##0.00");
					DeletePaymentFromPaymentBreakdownArray(ref Row, ref arr[Ind].Type);
					CalculateTotals();

					arr[Ind].Amount = 0;
					arr[Ind].ConvenienceFee = 0;
					arr[Ind].Bank = "";
					arr[Ind].CType = "";
					arr[Ind].Type = 0;
					arr[Ind].IsVISA = false;
					arr[Ind].IsDebit = false;
					arr[Ind].NonTaxAmount = 0;
					arr[Ind].AuthCode = "";
					arr[Ind].AuthCodeConvenienceFee = "";
					arr[Ind].AuthCodeVISANonTax = "";
					arr[Ind].NonTaxConvenienceFee = 0;
					arr[Ind].AuthCodeNonTaxConvenienceFee = "";
					arr[Ind].EPmtAcctID = "";
					cmdCashOut.Text = "Enter";
					mnuFileCashOut.Text = cmdCashOut.Text;
					cmdCashOut.Refresh();
					txtPayment.Text = Strings.Format(FCConvert.ToDouble(txtCashOutTotalDue.Text), "#,##0.00");
					txtPayment.SelectionStart = 0;
					txtPayment.SelectionLength = txtPayment.Text.Length;
					// MAL@20080115: Re-enable disabled fields
					// Tracker Reference: 11717
					if (boolEFTType)
					{
						// only allow checks when it is an EFT check
						if (cmbCashOutType.Items.Contains("Cash"))
						{
							cmbCashOutType.Items.Remove("Cash");
						}
						if (modGlobal.Statics.gblnAcceptEChecks)
						{
							if (!cmbCashOutType.Items.Contains("E-Check"))
							{
								cmbCashOutType.Items.Insert(1, "E-Check");
							}
						}
						else
						{
							if (!cmbCashOutType.Items.Contains("Check"))
							{
								cmbCashOutType.Items.Insert(1, "Check");
							}
						}
						if (cmbCashOutType.Items.Contains("Credit Card"))
						{
							cmbCashOutType.Items.Remove("Credit Card");
						}
						txtCashOutCheckPaid.Enabled = true;
					}
					else
					{
						if (!cmbCashOutType.Items.Contains("Cash"))
						{
							cmbCashOutType.Items.Insert(0, "Cash");
						}
						if (modGlobal.Statics.gblnAcceptEChecks)
						{
							if (!cmbCashOutType.Items.Contains("E-Check"))
							{
								cmbCashOutType.Items.Insert(1, "E-Check");
							}
						}
						else
						{
							if (!cmbCashOutType.Items.Contains("Check"))
							{
								cmbCashOutType.Items.Insert(1, "Check");
							}
						}
						if (FillCCCombo() && !cmbCashOutType.Items.Contains("Credit Card"))
						{
							cmbCashOutType.Items.Insert(2, "Credit Card");
						}
						else if (!FillCCCombo() && cmbCashOutType.Items.Contains("Credit Card"))
						{
							cmbCashOutType.Items.Remove("Credit Card");
						}
						txtCashOutCashPaid.Enabled = true;
						txtCashOutCheckPaid.Enabled = true;
						txtCashOutCreditPaid.Enabled = FCConvert.CBool(cmbCashOutType.SelectedIndex == 2);
					}
					txtPayment.Enabled = true;
					// FC:FINAL:VGE - #279 Cash Out Type dropdown reenabled
					cmbCashOutType.Enabled = true;
					txtPayment.Focus();
					// XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
				}
			}
		}

		private void CalculateTotals()
		{
			txtCashOutTotal.Text = Strings.Format(FCConvert.ToDouble(txtConvenienceFee.Text) + FCConvert.ToDouble(txtTransactions.Text), "#,##0.00");
			txtCashOutTotalDue.Text = Strings.Format(FCConvert.ToDouble(txtCashOutTotal.Text) - FCConvert.ToDouble(txtCashOutCashPaid.Text) - FCConvert.ToDouble(txtCashOutCheckPaid.Text) - FCConvert.ToDouble(txtCashOutCreditPaid.Text), "#,##0.00");
			if (FCConvert.ToDouble(txtCashOutTotal.Text) >= 0 && FCConvert.ToDouble(txtCashOutTotalDue.Text) < 0)
			{
				txtCashOutChange.Text = Strings.Format(FCConvert.ToDouble(txtCashOutTotalDue.Text) * -1, "#,##0.00");
				txtCashOutTotalDue.Text = "0.00";
				txtCashOutChange.Visible = true;
				lblCashOutChange.Visible = true;
			}
			else
			{
				txtCashOutChange.Text = "0.00";
				txtCashOutChange.Visible = false;
				lblCashOutChange.Visible = false;
			}
		}

		private bool CheckBMVType(ref string strType)
		{
			bool CheckBMVType = false;

			if (Strings.Trim(strType) != "")
			{
				CheckBMVType = true;
			}
			else
			{
				CheckBMVType = false;
			}
			return CheckBMVType;
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private object AddBatchToReceiptArray(ref int intRow, ref int lngReceiptKey)
		{
			object AddBatchToReceiptArray = null;
			// this function will add the batch payments that go with the row # passed in to the receiptarray for processing
			clsDRWrapper rsBatch = new clsDRWrapper();
			clsDRWrapper rsArchive = new clsDRWrapper();
			clsDRWrapper rsType = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strSQL;
			int intIndex = 0;
			bool boolFound = false;
			int lngArchivekey;
			rsArchive.OpenRecordset("SELECT * FROM Archive WHERE ID = 0", modExtraModules.strCRDatabase);
			// initialize the recordset to add the batch to
			strSQL = "SELECT * FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(intRow, 8))].Date) + "' AND PaidBy = '" + modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(intRow, 8))].PaidBy + "' AND TellerID = '" + strBatchTellerID + "'";
			if (rsBatch.OpenRecordset(strSQL, modExtraModules.strCLDatabase))
			{
				if (rsBatch.EndOfFile() != true && rsBatch.BeginningOfFile() != true)
				{
					frmWait.InstancePtr.Init("Creating Batch Payments", true, rsBatch.RecordCount() - 1);
					intIndex = 0;
					do
					{
						frmWait.InstancePtr.IncrementProgress();
						//Application.DoEvents();
						intIndex += 1;
						do
						{
							// find a good place to put the payment
							//Application.DoEvents();
							if (modUseCR.Statics.ReceiptArray[intIndex].Used != true)
							{
								boolFound = true;
							}
							else
							{
								boolFound = false;
								intIndex += 1;
							}
						}
						while (!(intIndex > modStatusPayments.MAX_PAYMENTS || boolFound == true));
						if (boolFound)
						{
							modUseCR.Statics.ReceiptArray[intIndex].Used = true;
							// if True then I know that this array index is currently being used
							modUseCR.Statics.ReceiptArray[intIndex].Type = FCConvert.ToInt32("90");
							modUseCR.Statics.ReceiptArray[intIndex].Module = "R";
							// P = Personal Property, R = Real Estate, W = UT Water, S = UT Sewer, X = Cash Receipting
							modUseCR.Statics.ReceiptArray[intIndex].CollectionCode = "P";
							// this will bring back the type of transaction from the module
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intIndex].Account = FCConvert.ToString(rsBatch.Get_Fields("AccountNumber"));
							modUseCR.Statics.ReceiptArray[intIndex].TypeDescription = "Real Estate Batch";
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intIndex].Reference = modUseCR.Statics.ReceiptArray[intIndex].Account + "-" + rsBatch.Get_Fields("Year");
							modUseCR.Statics.ReceiptArray[intIndex].Control1 = "A";
							modUseCR.Statics.ReceiptArray[intIndex].Control2 = "P";
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intIndex].Control3 = rsBatch.Get_Fields("Year") + " Batch";
							modUseCR.Statics.ReceiptArray[intIndex].Fee1 = rsBatch.Get_Fields_Double("Prin");
							// fee amounts
							modUseCR.Statics.ReceiptArray[intIndex].Fee2 = rsBatch.Get_Fields_Double("Int");
							modUseCR.Statics.ReceiptArray[intIndex].Fee3 = 0;
							// MAL@20081016: Corrected to have costs be in column 4 as expected
							// Tracker Reference: 15537
							// ReceiptArray(intindex).Fee4 = 0
							// TODO Get_Fields: Check the table for the column [Cost] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intIndex].Fee4 = rsBatch.Get_Fields("Cost");
							modUseCR.Statics.ReceiptArray[intIndex].Fee5 = 0;
							// ReceiptArray(intindex).Fee6 = rsBatch.Fields("Cost")
							modUseCR.Statics.ReceiptArray[intIndex].Fee6 = 0;
							modUseCR.Statics.ReceiptArray[intIndex].FeeD1 = "Principal";
							modUseCR.Statics.ReceiptArray[intIndex].FeeD2 = "Interest";
							modUseCR.Statics.ReceiptArray[intIndex].FeeD3 = "";
							// MAL@20081016: Moved Costs to the Correct Column (4)
							// Tracker Reference: 15537
							// ReceiptArray(intindex).FeeD4 = ""
							modUseCR.Statics.ReceiptArray[intIndex].FeeD4 = "Costs";
							modUseCR.Statics.ReceiptArray[intIndex].FeeD5 = "";
							// ReceiptArray(intindex).FeeD6 = "Costs"
							modUseCR.Statics.ReceiptArray[intIndex].FeeD6 = "";
							modUseCR.Statics.ReceiptArray[intIndex].Total = modUseCR.Statics.ReceiptArray[intIndex].Fee3 + modUseCR.Statics.ReceiptArray[intIndex].Fee5 + modUseCR.Statics.ReceiptArray[intIndex].Fee6;
							modUseCR.Statics.ReceiptArray[intIndex].Name = Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields_String("Name")));
							// ReceiptArray(intIndex).NamePartyID = Val(rsBatch.Fields("NamePartyID"))
							modUseCR.Statics.ReceiptArray[intIndex].PaidBy = Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields_String("PaidBy")));
							modUseCR.Statics.ReceiptArray[intIndex].RecordKey = FCConvert.ToInt32(rsBatch.Get_Fields_Int32("PaymentKey"));
							// this is the ID from the paymentrec table in the CL database
							modUseCR.Statics.ReceiptArray[intIndex].Date = (DateTime)rsBatch.Get_Fields_DateTime("BatchRecoverDate");
							if (FCConvert.ToBoolean(rsBatch.Get_Fields_Boolean("PrintReceipt")))
							{
								// to print or not to print
								modUseCR.Statics.ReceiptArray[intIndex].Copies = 1;
							}
							else
							{
								modUseCR.Statics.ReceiptArray[intIndex].Copies = 0;
							}
							modUseCR.Statics.ReceiptArray[intIndex].Comment = "";
							modUseCR.Statics.ReceiptArray[intIndex].ArrayIndex = 0;
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							modUseCR.Statics.ReceiptArray[intIndex].BillingYear = FCConvert.ToInt32(rsBatch.Get_Fields("Year"));
							modUseCR.Statics.ReceiptArray[intIndex].CashAlternativeAcct = "";
							modUseCR.Statics.ReceiptArray[intIndex].Split = intIndex + 5;
							modUseCR.Statics.ReceiptArray[intIndex].PrintReceipt = true;
							modUseCR.Statics.ReceiptArray[intIndex].AffectCashDrawer = FCConvert.ToBoolean(1);
							modUseCR.Statics.ReceiptArray[intIndex].AffectCash = FCConvert.ToBoolean(1);
							modUseCR.Statics.ReceiptArray[intIndex].Quantity = 1;
							rsArchive.AddNew();
							// ID
							// ReceiptType
							rsArchive.Set_Fields("ReceiptType", modUseCR.Statics.ReceiptArray[intIndex].Type);
							// AccountNumber
							rsArchive.Set_Fields("AccountNumber", FCConvert.ToString(Conversion.Val(Strings.Trim(modUseCR.Statics.ReceiptArray[intIndex].Account + " "))));
							// Actual System Date
							rsArchive.Set_Fields("ActualSystemDate", DateTime.Now);
							// ReceiptTitle
							// rsarchive.Fields("ReceiptTitle") = .TypeDescription
							rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + modUseCR.Statics.ReceiptArray[intIndex].Type);
							if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
							{
								// Account1
								rsArchive.Set_Fields("Account1", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account1"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 1));
								// Account2
								rsArchive.Set_Fields("Account2", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account2"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 2));
								// Account3
								rsArchive.Set_Fields("Account3", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account3"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 3));
								// Account4
								rsArchive.Set_Fields("Account4", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account4"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 4));
								// Account5
								rsArchive.Set_Fields("Account5", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account5"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 5));
								// Account6
								rsArchive.Set_Fields("Account6", modUseCR.CheckAccountForYear_78(rsType, rsType.Get_Fields_String("Account6"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 6));
							}
							// Amount1
							rsArchive.Set_Fields("Amount1", modUseCR.Statics.ReceiptArray[intIndex].Fee1);
							// Amount2
							rsArchive.Set_Fields("Amount2", modUseCR.Statics.ReceiptArray[intIndex].Fee2);
							// Amount3
							rsArchive.Set_Fields("Amount3", modUseCR.Statics.ReceiptArray[intIndex].Fee3);
							// Amount4
							rsArchive.Set_Fields("Amount4", modUseCR.Statics.ReceiptArray[intIndex].Fee4);
							// Amount5
							rsArchive.Set_Fields("Amount5", modUseCR.Statics.ReceiptArray[intIndex].Fee5);
							// Amount6
							rsArchive.Set_Fields("Amount6", modUseCR.Statics.ReceiptArray[intIndex].Fee6);
							// Comment
							rsArchive.Set_Fields("Comment", modUseCR.Statics.ReceiptArray[intIndex].Comment);
							// Ref
							rsArchive.Set_Fields("Ref", modUseCR.Statics.ReceiptArray[intIndex].Reference);
							// Control1
							rsArchive.Set_Fields("Control1", modUseCR.Statics.ReceiptArray[intIndex].Control1);
							// Control2
							rsArchive.Set_Fields("Control2", modUseCR.Statics.ReceiptArray[intIndex].Control2);
							// Control3
							rsArchive.Set_Fields("Control3", modUseCR.Statics.ReceiptArray[intIndex].Control3);
							// ReceiptNumber
							rsArchive.Set_Fields("ReceiptNumber", lngReceiptKey);
							// TellerID
							rsArchive.Set_Fields("TellerID", strTellerID);
							// Name
							rsArchive.Set_Fields("Name", modUseCR.Statics.ReceiptArray[intIndex].Name);
							// Party ID
							rsArchive.Set_Fields("NamePartyID", modUseCR.Statics.ReceiptArray[intIndex].NamePartyID);
							// XXXXX
							// Date
							rsArchive.Set_Fields("ArchiveDate", modUseCR.Statics.ReceiptArray[intIndex].Date);

							// CollectionCode
							rsArchive.Set_Fields("CollectionCode", modUseCR.Statics.ReceiptArray[intIndex].CollectionCode);
							// TellerCloseOut
							rsArchive.Set_Fields("TellerCloseOut", 0);
							// DailyCloseOut
							rsArchive.Set_Fields("DailyCloseOut", 0);
							// AffectCashDrawer
							rsArchive.Set_Fields("AffectCashDrawer", modUseCR.Statics.ReceiptArray[intIndex].AffectCashDrawer);
							// DailyCloseOut
							rsArchive.Set_Fields("AffectCash", modUseCR.Statics.ReceiptArray[intIndex].AffectCash);
							// Quantity
							rsArchive.Set_Fields("Quantity", modUseCR.Statics.ReceiptArray[intIndex].Quantity);
							// DefaultAccount
							rsArchive.Set_Fields("DefaultAccount", modUseCR.Statics.ReceiptArray[intIndex].CashAlternativeAcct);
							// this is used when the Cash is not affected and the Cash Drawer is not affected
							// kk 02142013 trocrs-10  Need default values
							rsArchive.Set_Fields("Split", 1);
							rsArchive.Set_Fields("CardPaidAmount", 0);
							rsArchive.Set_Fields("CheckPaidAmount", 0);
							rsArchive.Set_Fields("CashPaidAmount", 0);
							rsArchive.Set_Fields("ConvenienceFee", 0);
							rsArchive.Set_Fields("ARBillType", 0);
							rsArchive.Set_Fields("TownKey", 0);
							rsArchive.Set_Fields("EFT", 0);
							if (!rsArchive.Update(true))
							{
								MessageBox.Show("There has been an error in the creation of the Archive File.", "Receipt Record ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
							else
							{
								lngArchivekey = FCConvert.ToInt32(rsArchive.Get_Fields_Int32("ID"));
							}
							// this will update the ReceiptNumber field from (P)ending to the receipt number of the payment
							// records of any payments from other modules
							;
							var vbPorterVar = modUseCR.Statics.ReceiptArray[intIndex].Type;
							if (vbPorterVar >= 90 && vbPorterVar <= 93)
							{
								modUseCR.UpdateCLPayment(ref modUseCR.Statics.ReceiptArray[intIndex].RecordKey, ref lngReceiptKey);
								// kk02282014 trocrs-26  Check for right clicks "on the fly"
								rsTemp.OpenRecordset("SELECT BillKey, BillCode FROM PaymentRec WHERE ID = " + FCConvert.ToString(modUseCR.Statics.ReceiptArray[intIndex].RecordKey), modExtraModules.strCLDatabase);
								if (rsTemp.RecordCount() > 0)
								{
									modStatusPayments.VerifyCLBillAmounts(FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey")), FCConvert.CBool(FCConvert.ToString(rsTemp.Get_Fields_String("BillCode")) == "L"));
								}
							}
							else if (vbPorterVar >= 94 && vbPorterVar <= 95)
							{
								// UT
							}
							else if (vbPorterVar == 96)
							{
							}
							else if (vbPorterVar == 97)
							{
							}
							else if (vbPorterVar == 98)
							{
							}
							else if (vbPorterVar == 99)
							{
								// MV
							}
							else if (vbPorterVar >= 190 && vbPorterVar <= 199)
							{
							}
						}
						else
						{
							MessageBox.Show("Exceeded maximum payments limit.", "Maximum Payment Limit", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						rsBatch.MoveNext();
					}
					while (!rsBatch.EndOfFile());
					// erase the records from the temp table
					strSQL = "DELETE" + Strings.Right(strSQL, strSQL.Length - 8);
					rsBatch.Execute(strSQL, modExtraModules.strCLDatabase);
				}
				else
				{
					MessageBox.Show("Error opening Batch Recover table.  No records found.", "Batch Recovery Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				MessageBox.Show("Error opening Batch Recover table.", "Batch Recovery Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			frmWait.InstancePtr.Unload();
			return AddBatchToReceiptArray;
		}

		private void SetMenuOptions_2(short intType)
		{
			SetMenuOptions(ref intType);
		}

		private void SetMenuOptions(ref short intType)
		{
			// this will adjust all of the menu options depending on what screen is showing at that moment
			// 0 = TellerID
			// 1 = Receipt Input Screen
			// 2 = Summary Screen
			// 3 = Cash Out Screen
            mnuImportMooringReceipts.Visible = false;
			intCurFrame = intType;
			switch (intType)
			{
				case 0:
					{
						// teller id
						cmdAddReceipt.Visible = false;
						cmdBack.Visible = false;
						cmdSummary.Visible = true;
						cmdEdit.Visible = false;
						cmdSummary.Text = "Process";
						cmdCashOut.Visible = false;
						frmReceiptInput.InstancePtr.Text = "Teller ID Screen";
						cmdVoidByReceipt.Visible = false;
						break;
					}
				case 1:
					{
						// receipt input
						cmdAddReceipt.Visible = true;
						cmdBack.Visible = false;
						cmdSummary.Visible = true;
						cmdCashOut.Visible = false;
						cmdEdit.Visible = false;
						cmdAddReceipt.Text = "Save";
						cmdSummary.Text = "Save and Continue";
						frmReceiptInput.InstancePtr.Text = "Receipt Input";
						cmdVoidByReceipt.Visible = true;
                        mnuImportMooringReceipts.Visible = (modGlobalConstants.Statics.MuniName.ToLower() + "      ").Substring(0,6) == "camden";
						break;
					}
				case 2:
					{
						// summary
						cmdAddReceipt.Visible = true;
						cmdSummary.Visible = true;
						cmdAddReceipt.Visible = false;
						if (!blnIsVoid)
						{
							cmdEdit.Visible = true;
							cmdBack.Text = "Previous Screen";
							mnuFileBack.Visible = true;
						}
						else
						{
							cmdEdit.Visible = false;
							cmdBack.Visible = false;
						}
						cmdAddReceipt.Text = "Save";
						cmdSummary.Text = "Save and Continue";
						cmdCashOut.Visible = false;
						frmReceiptInput.InstancePtr.Text = "Receipt Summary";
						cmdVoidByReceipt.Visible = false;
						break;
					}
				case 3:
					{
						// cash out
						cmdAddReceipt.Visible = true;
						cmdBack.Visible = true;
						cmdCashOut.Visible = true;
						cmdSummary.Visible = true;
						cmdAddReceipt.Visible = false;
						cmdEdit.Visible = false;
						cmdBack.Text = "Previous Screen";
						cmdAddReceipt.Text = "Process";
						cmdSummary.Text = "Save and Continue";
						frmReceiptInput.InstancePtr.Text = "Cash Out";
						cmdVoidByReceipt.Visible = false;
						break;
					}
			}
			//end switch
			this.HeaderText.Text = this.Text;
		}

		public void ProcessType_2(bool boolRefresh)
		{
			ProcessType(boolRefresh);
		}

		public void ProcessType(bool boolRefresh = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsType = new clsDRWrapper();
				int lngTypeNumber = 0;
				int lngResCode = 0;
				int intCT = 0;
				int I;

				rsType.DefaultDB = modGlobal.Statics.DatabaseName;
				// Print #15, "02 - " & Now
				if (cmbType.Text != "")
				{
					// Print #15, "03 - " & Now
					lngTypeNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbType.Text)));
					// check to see what type is in the combobox
					lblTypeDescription.Text = modUseCR.Statics.strTypeDescriptions[lngTypeNumber];
					// set the description
					boolRefresh = RestrictedCode(ref lngTypeNumber) || boolRefresh;
					// check to see if this is a restricted code...if it is, then you will want to refresh it even if it is the same
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						lngResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString())));
					}

					// Print #15, "06 - " & Now
					if (lngTypeNumber != 0 && (lngTypeNumber != lngCurType || boolRefresh))
					{
						if (lngTypeNumber != lngCurType && modGlobal.Statics.gboolMultipleTowns)
						{
							FillResCodeCombo(ref lngTypeNumber);
						}
						// Print #15, "07 - " & Now
						// kgk 06-19-2012 trobl-106  Force the user to select a town for MOSES trx
						if (lngTypeNumber == 190 && modGlobal.Statics.gboolMultipleTowns && lngResCode == 0)
						{
							boolMosesTown = true;
							cmbResCode.SelectedIndex = frmMultiTownSelect.InstancePtr.GetTownIndex();
							boolMosesTown = false;
							lngResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbResCode.Items[cmbResCode.SelectedIndex].ToString())));
						}
						// this will turn the account box off
						txtAcct1.Visible = false;
						lblAccount.Visible = false;
						lblAcctDescription.Visible = false;
						fraAcct.Height = chkAffectCash.Top + chkAffectCash.Height + 100;
						// Print #15, "08 - " & Now
						if (modGlobal.Statics.gboolMultipleTowns && lngResCode == 0)
						{
							lngResCode = 1;
						}
						// lngTypeNumber = Val(Left$(cmbType.List(cmbType.ListIndex), 3))
						rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngTypeNumber) + " AND TownKey = " + FCConvert.ToString(lngResCode));
						if (rsType.BeginningOfFile() != true && rsType.EndOfFile() != true)
						{

							lngCurType = lngTypeNumber;
							boolEFTType = FCConvert.ToBoolean(rsType.Get_Fields_Boolean("EFT"));
							// this will set the public variable
							if (boolEFTType)
							{
								chkZeroReceipt.Enabled = false;
							}
							else
							{
								chkZeroReceipt.Enabled = true;
							}
							// Print #15, "09 - " & Now
							// this checks for a special code in the type that needs to have an account number entered
							if (FCConvert.ToString(rsType.Get_Fields_String("Account1")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account2")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account3")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account4")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account5")) == "M I" || FCConvert.ToString(rsType.Get_Fields_String("Account6")) == "M I")
							{
								// if the code is present, then show the account box for the user
								// Print #15, "10 - " & Now & " Start Account Validation"
								txtAcct1.Visible = true;
								lblAccount.Visible = true;
								lblAcctDescription.Visible = true;
								fraAcct.Height = vsFees.RowHeight(1) * 8;
							}
							// Print #15, "11 - " & Now
							// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
							cmbCopies.SelectedIndex = FCConvert.ToInt32(rsType.Get_Fields("Copies")) - 1;
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Print")))
							{
								chkPrint.CheckState = Wisej.Web.CheckState.Checked;
							}
							else
							{
								chkPrint.CheckState = Wisej.Web.CheckState.Unchecked;
							}
							bool executeACCT2 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title1")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account1")) != "")
							{
								// remove validation
								if (modGlobalConstants.Statics.gboolBD)
								{
									// MAL@20081114: Ignore Account 1 for Type 97
									// Tracker Reference: 14057
									if (modGlobalConstants.Statics.gboolAR && lngTypeNumber == 97)
									{
										// Ignore
									}
									else
									{
										if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account1")))
										{
											executeACCT2 = true;
											goto ACCT2;
										}
									}
								}
								vsFees.TextMatrix(1, 1, FCConvert.ToString(rsType.Get_Fields_String("Title1")));
								if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee1")))
								{
									vsFees.TextMatrix(1, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount1"), "#0.00") + "%");
									vsFees.TextMatrix(1, 4, "%");
								}
								else
								{
									vsFees.TextMatrix(1, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount1"), "#,##0.00"));
									vsFees.TextMatrix(1, 4, "$");
								}
								vsFees.TextMatrix(1, 3, FCConvert.ToString(rsType.Get_Fields_Boolean("Year1")));
							}
							else
							{
								executeACCT2 = true;
								goto ACCT2;
							}
							ACCT2:
							;
							if (executeACCT2)
							{
								vsFees.TextMatrix(1, 1, "");
								vsFees.TextMatrix(1, 2, FCConvert.ToString(0));
								vsFees.TextMatrix(1, 3, FCConvert.ToString(false));
							}
							// if this is a redbook type coming back then correct the fee in the excise field
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("RBType")))
							{
								// Print #15, "15 - " & Now
								vsFees.TextMatrix(1, 2, Strings.Format(dblRBValue, "#,##0.00"));
								// MsgBox "Adding " & Format(dblRBValue, "#,##0.00") & " to the excise field."
							}
							bool executeACCT3 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title2")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account2")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account2")))
									{
										executeACCT3 = true;
										goto ACCT3;
									}
								}
								// Print #15, "16 - " & Now
								vsFees.TextMatrix(2, 1, FCConvert.ToString(rsType.Get_Fields_String("Title2")));
								if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee2")))
								{
									vsFees.TextMatrix(2, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount2"), "#0.00") + "%");
									vsFees.TextMatrix(2, 4, "%");
								}
								else
								{
									vsFees.TextMatrix(2, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount2"), "#,##0.00"));
									vsFees.TextMatrix(2, 4, "$");
								}
								vsFees.TextMatrix(2, 3, FCConvert.ToString(rsType.Get_Fields_Boolean("Year2")));
							}
							else
							{
								executeACCT3 = true;
								goto ACCT3;
							}
							// Print #15, "17 - " & Now
							ACCT3:
							;
							if (executeACCT3)
							{
								vsFees.TextMatrix(2, 1, "");
								vsFees.TextMatrix(2, 2, FCConvert.ToString(0));
								vsFees.TextMatrix(2, 3, FCConvert.ToString(false));
							}
							bool executeACCT4 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title3")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account3")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account3")))
									{
										executeACCT4 = true;
										goto ACCT4;
									}
								}
								// Print #15, "18 - " & Now
								vsFees.TextMatrix(3, 1, FCConvert.ToString(rsType.Get_Fields_String("Title3")));
								if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee3")))
								{
									vsFees.TextMatrix(3, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount3"), "#0.00") + "%");
									vsFees.TextMatrix(3, 4, "%");
								}
								else
								{
									vsFees.TextMatrix(3, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount3"), "#,##0.00"));
									vsFees.TextMatrix(3, 4, "$");
								}
								vsFees.TextMatrix(3, 3, FCConvert.ToString(rsType.Get_Fields_Boolean("Year3")));
							}
							else
							{
								executeACCT4 = true;
								goto ACCT4;
							}
							// Print #15, "19 - " & Now
							ACCT4:
							;
							if (executeACCT4)
							{
								vsFees.TextMatrix(3, 1, "");
								vsFees.TextMatrix(3, 2, FCConvert.ToString(0));
								vsFees.TextMatrix(3, 3, FCConvert.ToString(false));
							}
							bool executeACCT5 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title4")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account4")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account4")))
									{
										executeACCT5 = true;
										goto ACCT5;
									}
								}
								// Print #15, "20 - " & Now
								vsFees.TextMatrix(4, 1, FCConvert.ToString(rsType.Get_Fields_String("Title4")));
								if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee4")))
								{
									vsFees.TextMatrix(4, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount4"), "#0.00") + "%");
									vsFees.TextMatrix(4, 4, "%");
								}
								else
								{
									vsFees.TextMatrix(4, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount4"), "#,##0.00"));
									vsFees.TextMatrix(4, 4, "$");
								}
								vsFees.TextMatrix(4, 3, FCConvert.ToString(rsType.Get_Fields_Boolean("Year4")));
							}
							else
							{
								executeACCT5 = true;
								goto ACCT5;
							}
							// Print #15, "21 - " & Now
							ACCT5:
							;
							if (executeACCT5)
							{
								vsFees.TextMatrix(4, 1, "");
								vsFees.TextMatrix(4, 2, FCConvert.ToString(0));
								vsFees.TextMatrix(4, 3, FCConvert.ToString(false));
							}
							bool executeACCT6 = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title5")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account5")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account5")))
									{
										executeACCT6 = true;
										goto ACCT6;
									}
								}
								// Print #15, "22 - " & Now
								vsFees.TextMatrix(5, 1, FCConvert.ToString(rsType.Get_Fields_String("Title5")));
								if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee5")))
								{
									vsFees.TextMatrix(5, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount5"), "#0.00") + "%");
									vsFees.TextMatrix(5, 4, "%");
								}
								else
								{
									vsFees.TextMatrix(5, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount5"), "#,##0.00"));
									vsFees.TextMatrix(5, 4, "$");
								}
								vsFees.TextMatrix(5, 3, FCConvert.ToString(rsType.Get_Fields_Boolean("Year5")));
							}
							else
							{
								executeACCT6 = true;
								goto ACCT6;
							}
							ACCT6:
							;
							if (executeACCT6)
							{
								// Print #15, "23 - " & Now
								vsFees.TextMatrix(5, 1, "");
								vsFees.TextMatrix(5, 2, FCConvert.ToString(0));
								vsFees.TextMatrix(5, 3, FCConvert.ToString(false));
							}
							bool executeEXITACCT = false;
							if (FCConvert.ToString(rsType.Get_Fields_String("Title6")) != "" && FCConvert.ToString(rsType.Get_Fields_String("Account6")) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields_String("Account6")))
									{
										executeEXITACCT = true;
										goto EXITACCT;
									}
								}
								// Print #15, "24 - " & Now
								vsFees.TextMatrix(6, 1, FCConvert.ToString(rsType.Get_Fields_String("Title6")));
								if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee6")))
								{
									vsFees.TextMatrix(6, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount6"), "#0.00") + "%");
									vsFees.TextMatrix(6, 4, "%");
								}
								else
								{
									vsFees.TextMatrix(6, 2, Strings.Format(rsType.Get_Fields_Double("DefaultAmount6"), "#,##0.00"));
									vsFees.TextMatrix(6, 4, "$");
								}
								vsFees.TextMatrix(6, 3, FCConvert.ToString(rsType.Get_Fields_Boolean("Year6")));
							}
							else
							{
								executeEXITACCT = true;
								goto EXITACCT;
							}
							// Print #15, "25 - " & Now
							EXITACCT:
							;
							if (executeEXITACCT)
							{
								vsFees.TextMatrix(6, 1, "");
								vsFees.TextMatrix(6, 2, FCConvert.ToString(0));
								vsFees.TextMatrix(6, 3, FCConvert.ToString(false));
							}
							// Print #15, "26 - " & Now
							// check to see if any of the accounts need to be updated by the Year or need to be changed by the CurrentYear function
							// and at the same time set the heights of the rows
							intCT = 1;
							for (I = 1; I <= 6; I++)
							{
								if (Strings.Trim(vsFees.TextMatrix(I, 1)) == "" && Conversion.Val(vsFees.TextMatrix(I, 2).Replace("%", "")) == 0)
								{
									//vsFees.RowHeight(I, 0);
									vsFees.RowHidden(I, true);
								}
								else
								{
									intCT += 1;
									vsFees.RowHidden(I, false);
								}
							}
							boolShowDetail = FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ShowFeeDetailOnReceipt"));
							modGlobal.Statics.gboolDetail = boolShowDetail;
							boolBMV = FCConvert.ToBoolean(rsType.Get_Fields_Boolean("BMV"));
							// Print #15, "28 - " & Now
							if (boolBMV)
							{
								// Print #15, "29 - " & Now
								ToolTip1.SetToolTip(lblTitle[1], "Use the format YYD#######");
								ToolTip1.SetToolTip(lblTitle[2], "Use the format MMD#######");
								ToolTip1.SetToolTip(txtTitle[1], "Use the format YYD#######");
								ToolTip1.SetToolTip(txtTitle[2], "Use the format MMD#######");
							}
							else
							{
								ToolTip1.SetToolTip(lblTitle[1], "");
								ToolTip1.SetToolTip(lblTitle[2], "");
							}
							// Print #15, "30 - " & Now
							// fill in the labels names
							lblTitle[0].Text = rsType.Get_Fields_String("Reference") + ":";
							lblTitle[1].Text = rsType.Get_Fields_String("Control1") + ":";
							lblTitle[2].Text = rsType.Get_Fields_String("Control2") + ":";
							lblTitle[3].Text = rsType.Get_Fields_String("Control3") + ":";
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("ReferenceMandatory")))
							{
								txtTitle[0].Tag = "True";
								txtTitle[0].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[0].Tag = "False";
								txtTitle[0].BackColor = Color.White;
							}
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control1Mandatory")))
							{
								txtTitle[1].Tag = "True";
								txtTitle[1].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[1].Tag = "False";
								txtTitle[1].BackColor = Color.White;
							}
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control2Mandatory")))
							{
								txtTitle[2].Tag = "True";
								txtTitle[2].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[2].Tag = "False";
								txtTitle[2].BackColor = Color.White;
							}
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("Control3Mandatory")))
							{
								txtTitle[3].Tag = "True";
								txtTitle[3].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							}
							else
							{
								txtTitle[3].Tag = "False";
								txtTitle[3].BackColor = Color.White;
							}
							txtTitle[0].Text = "";
							txtTitle[1].Text = "";
							txtTitle[2].Text = "";
							txtTitle[3].Text = "";
							// Print #15, "31 - " & Now
							for (I = 0; I <= 3; I++)
							{
								if (Strings.Trim(lblTitle[FCConvert.ToInt16(I)].Text) == ":")
								{
									lblTitle[FCConvert.ToInt16(I)].Visible = false;
									txtTitle[FCConvert.ToInt16(I)].Visible = false;
								}
								else
								{
									lblTitle[FCConvert.ToInt16(I)].Visible = true;
									txtTitle[FCConvert.ToInt16(I)].Visible = true;
								}
							}
							// Print #15, "32 - " & Now
							if (FCConvert.ToString(rsType.Get_Fields_String("DefaultAccount")) != "")
							{
								// then this account will not be shown in the general cash account
								txtDefaultCashAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("DefaultAccount")));
							}
							else
							{
								// if it is blank already then leave it blank
								txtDefaultCashAccount.TextMatrix(0, 0, "");
							}
							// reset the quanity field
							lngQuanity = 1;
							boolDoNotCheckMultiples = true;
							if (cmbMultiple.Items.Count > 0)
							{
								cmbMultiple.SelectedIndex = 0;
							}
							boolDoNotCheckMultiples = false;
							// Print #15, "33 - " & Now
							// show the percentage box and label if it is being used by this type
							if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee1")) || FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee2")) || FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee3")) || FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee4")) || FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee5")) || FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee6")))
							{
								lblPercentAmount.Visible = true;
								txtPercentageAmount.Visible = true;
								txtPercentageAmount.Text = "0.00";
							}
							else
							{
								lblPercentAmount.Visible = false;
								txtPercentageAmount.Visible = false;
								txtPercentageAmount.Text = "0.00";
							}
						}
						else
						{
							// Print #15, "34 - " & Now
							boolShowDetail = false;
							MessageBox.Show("Could not find type #" + FCConvert.ToString(lngTypeNumber) + ".", "Error Loading Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);

						}
						// Print #15, "35 - " & Now
						if ((lngTypeNumber >= 90 && lngTypeNumber <= 99) || (lngTypeNumber >= 190 && lngTypeNumber <= 199))
						{
							modUseCR.LockBoxes_2(true);
						}
						else
						{
							modUseCR.LockBoxes_2(false);
						}
						// Print #15, "36 - " & Now
						if (boolEFTType)
						{
							chkAffectCash.CheckState = Wisej.Web.CheckState.Unchecked;
							chkAffectCash.Enabled = false;
						}
						else
						{
							chkAffectCash.CheckState = Wisej.Web.CheckState.Checked;
							chkAffectCash.Enabled = true;
						}
						cmbType.Text = FCConvert.ToString(lngTypeNumber);
					}
				}
				else
				{
					ClearReceiptInput();
				}
				// Print #15, "37 - " & Now
				// Close #15
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Processing Receipt Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngType As int	OnWrite(int, double)
		public bool RestrictedCode_2(int lngType)
		{
			return RestrictedCode(ref lngType);
		}

		public bool RestrictedCode(ref int lngType)
		{
			bool RestrictedCode = false;
			// this will return true if this is a restricted code
			int intTypeCode;
			intTypeCode = lngType;
			if ((intTypeCode >= 90 && intTypeCode <= 99) || (intTypeCode >= 190 && intTypeCode <= 199) || (intTypeCode >= 290 && intTypeCode <= 299) || (intTypeCode >= 390 && intTypeCode <= 399) || (intTypeCode >= 490 && intTypeCode <= 499) || (intTypeCode >= 590 && intTypeCode <= 599) || (intTypeCode >= 690 && intTypeCode <= 699) || (intTypeCode >= 790 && intTypeCode <= 799) || (intTypeCode >= 890 && intTypeCode <= 899) || (intTypeCode >= 901 && intTypeCode <= 999))
			{
				RestrictedCode = true;
			}
			else
			{
				RestrictedCode = false;
			}
			return RestrictedCode;
		}

		private bool RestrictForBMV()
		{
			bool RestrictForBMV = false;
			if (boolBMV)
			{
				RestrictForBMV = true;
			}
			else
			{
				RestrictForBMV = false;
			}
			return RestrictForBMV;
		}

		private bool SetupAsPrinterFile(ref string strName)
		{
			bool SetupAsPrinterFile = false;
			// this will return true if the string passed int is "PRNT.txt"
			if (Strings.UCase(strName) == Strings.UCase("PRNT.TXT"))
			{
				SetupAsPrinterFile = true;
			}
			else
			{
				SetupAsPrinterFile = false;
			}
			return SetupAsPrinterFile;
		}

		private void ClearReceiptInput()
		{
			// this sub will clear the input screen so that a new receipt can be entered
			//Application.DoEvents();
			lngCurType = 0;
			txtFirst.Focus();
			cmbType.SelectedIndex = 0;

            //FC:FINAL:SBE - #3908 - focus control only when form is active
            if (this.Active)
            {
                cmbType.Focus();
            }
			lblTypeDescription.Text = "";
			if (txtAcct1.Enabled == true)
			{
				boolAccountBoxClear = true;
				txtAcct1.TextMatrix(0, 0, "");
				txtAcct1.Visible = false;
				boolAccountBoxClear = false;
				// MAL@20071113: Reset Account Fields and Frame
				lblAccount.Visible = false;
				lblAcctDescription.Visible = true;
				fraAcct.Height = vsFees.RowHeight(1) * 4;
			}
			if (Strings.Trim(txtFirst.Text) == "" && Strings.Trim(txtPaidBy.Text) != "")
			{
				txtFirst.Text = Strings.Trim(txtPaidBy.Text);
			}
			boolMosesTown = false;

		}

		private void UpdateNonCRRecords()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				// vbPorter upgrade warning: strKey As string	OnWriteFCConvert.ToInt32(
				string strKey = "";
				// vbPorter upgrade warning: strRevKey As string	OnWriteFCConvert.ToInt32(
				string strRevKey = "";
				int lngCT;
				DateTime dtIntPaid = DateTime.FromOADate(0);
				int lngBillKey = 0;
				int lngBillNum;
				int lngAccount = 0;
				int lngCHGINT = 0;
				int lngLienKey = 0;
				int lngSLienKey;
				int lngWLienKey;
				int lngYear = 0;
				string strService = "";
				string strReference = "";
				string strCode = "";
				double dblPrin;
				double dblInt;
				double dblCost;
				double dblTax;
				DateTime dtTransDate;
				// vbPorter upgrade warning: dtPassDate As DateTime	OnWrite(DateTime, short)
				DateTime dtPassDate = DateTime.FromOADate(0);
				// find all CL, UT
				for (lngCT = 1; lngCT <= vsSummary.Rows - 1; lngCT++)
				{
					if (vsSummary.TextMatrix(lngCT, 2) != "***")
					{
						if ((Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 90) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 91) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 92) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 890) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 891))
						{
							// CL
							rsTemp.DefaultDB = modExtraModules.strCLDatabase;
							strKey = FCConvert.ToString(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(lngCT, 8))].RecordKey);
							// strKey = PaymentArray(vsSummary.TextMatrix(lngCT, 8)).ID
							if (Conversion.Val(strKey) > 0)
							{
								rsTemp.Execute("UPDATE PaymentRec SET Teller = '" + strTellerID + "' , PaidBy  = '" + Strings.Trim(modGlobalFunctions.RemoveApostrophe(txtPaidBy.Text)) + "' WHERE ID = " + strKey, modExtraModules.strCLDatabase);
								// k   "TWCR0000.vb1"   ', Comments = '" & Trim(RemoveApostrophe(txtComment.Text)) & "'
								// MAL@20080118: Update Interest Paid Through Date
								// Tracker Reference: 11743
								rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + strKey, modExtraModules.strCLDatabase);
								if (rsTemp.RecordCount() > 0)
								{
									dtTransDate = (DateTime)rsTemp.Get_Fields_DateTime("EffectiveInterestDate");
									lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									lngAccount = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
									lngCHGINT = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber"));
									// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
									lngYear = FCConvert.ToInt32(rsTemp.Get_Fields("Year"));
									strReference = FCConvert.ToString(rsTemp.Get_Fields_String("Reference"));
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									strCode = FCConvert.ToString(rsTemp.Get_Fields("Code"));
									// kk trocr-348 110212  Need to tag the chgint record
									if (lngCHGINT != 0)
									{
										rsTemp.Execute("UPDATE PaymentRec SET Teller = '" + strTellerID + "' WHERE ID = " + FCConvert.ToString(lngCHGINT), modExtraModules.strCLDatabase);
									}
									// Get Maximum Effective Interest Date
									if (lngAccount > 0)
									{
										if (FCConvert.ToString(rsTemp.Get_Fields_String("billcode")) != "P")
										{
											rsTemp.OpenRecordset("SELECT * FROM BillingMaster  WHERE billingtype = 'RE' and  Account = " + FCConvert.ToString(lngAccount) + " AND BillingYear = " + FCConvert.ToString(lngYear), modExtraModules.strCLDatabase);
										}
										else
										{
											rsTemp.OpenRecordset("SELECT * FROM BillingMaster  WHERE billingtype = 'PP' and  Account = " + FCConvert.ToString(lngAccount) + " AND BillingYear = " + FCConvert.ToString(lngYear), modExtraModules.strCLDatabase);
										}
										if (rsTemp.RecordCount() > 0)
										{
											lngLienKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("LienRecordNumber"));
											if (strReference != "REVERS")
											{
												// MAL@20081208: Add check for abatements - don't change Paid Through Date if it is
												// Tracker Reference: 16429
												if (strCode == "A" || strCode == "R")
												{
													dtPassDate = dtTransDate;
													if (lngLienKey > 0)
													{
														if (modStatusPayments.GetLastInterestDate_CLLien(lngAccount, lngYear, dtTransDate, 0).ToOADate() > dtPassDate.ToOADate())
														{
															dtIntPaid = dtPassDate;
														}
														else
														{
															dtIntPaid = modStatusPayments.GetLastInterestDate_CLLien(lngAccount, lngYear, dtPassDate, 0);
														}
													}
													else
													{
														if (modStatusPayments.GetLastInterestDate_CL(lngAccount, lngYear, dtTransDate, 0).ToOADate() > dtPassDate.ToOADate())
														{
															dtIntPaid = dtPassDate;
														}
														else
														{
															dtIntPaid = modStatusPayments.GetLastInterestDate_CL(lngAccount, lngYear, dtPassDate, 0);
														}
													}
												}
												else
												{
													dtIntPaid = dtTransDate;
												}
											}
											else
											{
												// MAL@20081015: Change to use the Last Interest Date as pass date to be sure to get the previous interest date correctly
												// Tracker Reference: 15461
												if (lngLienKey > 0)
												{
													rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(lngLienKey), modExtraModules.strCLDatabase);
													if (rsLien.RecordCount() > 0)
													{
														if (!rsLien.IsFieldNull("InterestAppliedThroughDate"))
														{
															dtPassDate = (DateTime)rsLien.Get_Fields_DateTime("InterestAppliedThroughDate");
														}
														else
														{
															dtPassDate = DateTime.FromOADate(0);
														}
													}
													else
													{
														dtPassDate = dtTransDate;
													}
													// MAL@20081104: Certain instances have been updating the interest date twice. Checks if Pass Date is already changed
													// Tracker Reference: 15528
													// If GetLastInterestDate_CLLien(lngAccount, lngYear, dtTransDate, 0) > dtPassDate Then
													if (modStatusPayments.GetLastInterestDateForReversal_CLLien(lngAccount, lngYear, dtTransDate, FCConvert.ToInt32(Conversion.Val(strKey))).ToOADate() > dtPassDate.ToOADate())
													{
														dtIntPaid = dtPassDate;
													}
													else
													{
														dtIntPaid = modStatusPayments.GetLastInterestDateForReversal_CLLien(lngAccount, lngYear, dtPassDate, 0);
													}
												}
												else
												{
													if (!rsTemp.IsFieldNull("InterestAppliedThroughDate"))
													{
														dtPassDate = (DateTime)rsTemp.Get_Fields_DateTime("InterestAppliedThroughDate");
													}
													else
													{
														dtPassDate = DateAndTime.DateValue(FCConvert.ToString(0));
													}
													// MAL@20081104: Certain instances have been updating the interest date twice. Checks if Pass Date is already changed
													// Tracker Reference: 15528
													if (modStatusPayments.GetLastInterestDateForReversal_CL(lngAccount, lngYear, dtTransDate, FCConvert.ToInt32(Conversion.Val(strKey))).ToOADate() > dtPassDate.ToOADate())
													{
														dtIntPaid = dtPassDate;
													}
													else
													{
														dtIntPaid = modStatusPayments.GetLastInterestDateForReversal_CL(lngAccount, lngYear, dtPassDate, FCConvert.ToInt32(Conversion.Val(strKey)));
													}
												}
											}
										}
										if (lngBillKey > 0 && lngLienKey == 0)
										{
											rsTemp.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strCLDatabase);
											if (rsTemp.RecordCount() > 0)
											{
												rsTemp.Edit();
												rsTemp.Set_Fields("InterestAppliedThroughDate", dtIntPaid);
												rsTemp.Update();
											}
										}
										if (lngLienKey > 0)
										{
											rsTemp.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(lngLienKey), modExtraModules.strCLDatabase);
											if (rsTemp.RecordCount() > 0)
											{
												rsTemp.Edit();
												rsTemp.Set_Fields("InterestAppliedThroughDate", dtIntPaid);
												rsTemp.Update();
											}
										}
									}
								}
							}
							// kk02282014 trocrs-26  Check for right-click "on the fly"
							rsTemp.OpenRecordset("SELECT BillKey, BillCode FROM PaymentRec WHERE ID = " + strKey, modExtraModules.strCLDatabase);
							if (rsTemp.RecordCount() > 0)
							{
								lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
								modStatusPayments.VerifyCLBillAmounts(lngBillKey, FCConvert.CBool(FCConvert.ToString(rsTemp.Get_Fields_String("BillCode")) == "L"));
							}
						}
						else if ((Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 93) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 94) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 95) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 96) || (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) >= 893 && Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) <= 896))
						{
							// UT
							rsTemp.DefaultDB = modExtraModules.strUTDatabase;
							strKey = FCConvert.ToString(modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(lngCT, 8))].RecordKey);
							// strKey = PaymentArray(vsSummary.TextMatrix(lngCT, 8)).ID
							if (Conversion.Val(strKey) > 0)
							{
								if (Strings.Trim(txtComment.Text) != "")
								{
									rsTemp.Execute("UPDATE PaymentRec SET Teller = '" + strTellerID + "' , PaidBy  = '" + Strings.Trim(modGlobalFunctions.RemoveApostrophe(txtPaidBy.Text)) + "', Comments = '" + Strings.Trim(modGlobalFunctions.RemoveApostrophe(txtComment.Text)) + "' WHERE ID = " + strKey, modExtraModules.strUTDatabase);
									// k,   "TWCR0000.vb1"
								}
								else
								{
									rsTemp.Execute("UPDATE PaymentRec SET Teller = '" + strTellerID + "' , PaidBy  = '" + Strings.Trim(modGlobalFunctions.RemoveApostrophe(txtPaidBy.Text)) + "' WHERE ID = " + strKey, modExtraModules.strUTDatabase);
									// k     , "TWCR0000.vb1"
								}
								// kk trocr-348 110712  Need to tag the chgint record
								rsTemp.OpenRecordset("SELECT CHGINTNumber FROM PaymentRec WHERE ID = " + strKey, modExtraModules.strUTDatabase);
								if (rsTemp.RecordCount() > 0)
								{
									lngCHGINT = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber"));
									if (lngCHGINT != 0)
									{
										rsTemp.Execute("UPDATE PaymentRec SET Teller = '" + strTellerID + "' WHERE ID = " + FCConvert.ToString(lngCHGINT), modExtraModules.strUTDatabase);
									}
								}
							}
							rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + strKey, modExtraModules.strUTDatabase);
							if (rsTemp.RecordCount() > 0)
							{
								lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
								strService = FCConvert.ToString(rsTemp.Get_Fields_String("Service"));
								modUTFunctions.VerifyUTBillAmounts_18(lngBillKey, strService, FCConvert.CBool(rsTemp.Get_Fields_Boolean("Lien")));
							}
						}
						else if (Conversion.Val(vsSummary.TextMatrix(lngCT, 0)) == 97)
						{
							// AR
							rsTemp.DefaultDB = modExtraModules.strARDatabase;
							int i = FCConvert.ToInt32(vsSummary.TextMatrix(lngCT, 8));
							strKey = FCConvert.ToString(modUseCR.Statics.ReceiptArray[i].RecordKey);
							strRevKey = FCConvert.ToString(modUseCR.Statics.ReceiptArray[i].ReverseKey);
							// kk12192013 troar-61
							if (Conversion.Val(strKey) > 0)
							{
								rsTemp.Execute("UPDATE PaymentRec SET Teller = '" + strTellerID + "' , PaidBy  = '" + Strings.Trim(modGlobalFunctions.RemoveApostrophe(txtPaidBy.Text)) + "', Comments = '" + Strings.Trim(modGlobalFunctions.RemoveApostrophe(txtComment.Text)) + "' WHERE ID = " + strKey, modExtraModules.strARDatabase);
								// k , "TWCR0000.vb1"
								// kk trocr-348 110712  Need to tag the chgint record
								rsTemp.OpenRecordset("SELECT CHGINTNumber FROM PaymentRec WHERE ID = " + strKey, modExtraModules.strARDatabase);
								if (rsTemp.RecordCount() > 0)
								{
									lngCHGINT = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber"));
									if (lngCHGINT != 0)
									{
										rsTemp.Execute("UPDATE PaymentRec SET Teller = '" + strTellerID + "' WHERE ID = " + FCConvert.ToString(lngCHGINT), modExtraModules.strARDatabase);
									}
								}
								rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + strKey);
								if (rsTemp.RecordCount() > 0)
								{
									dtTransDate = (DateTime)rsTemp.Get_Fields_DateTime("EffectiveInterestDate");
									lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
									lngAccount = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ActualAccountNumber"));
									lngCHGINT = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber"));
									lngYear = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
									strReference = FCConvert.ToString(rsTemp.Get_Fields_String("Reference"));
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									strCode = FCConvert.ToString(rsTemp.Get_Fields("Code"));
									// Get Maximum Effective Interest Date
									if (lngAccount > 0)
									{
										rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey));
										if (rsTemp.RecordCount() > 0)
										{
											if (strReference == "REVERS")
											{
												if (!rsTemp.IsFieldNull("IntPaidDate"))
												{
													// kk03262015 trocrs-21
													dtPassDate = (DateTime)rsTemp.Get_Fields_DateTime("IntPaidDate");
												}
												if (modARStatusPayments.GetLastARInterestDate(lngAccount, lngBillKey, dtTransDate, FCConvert.ToInt32(Conversion.Val(strKey))).ToOADate() > dtPassDate.ToOADate())
												{
													dtIntPaid = dtPassDate;
												}
												else
												{
													dtIntPaid = modARStatusPayments.GetLastARInterestDate(lngAccount, lngBillKey, dtPassDate, FCConvert.ToInt32(Conversion.Val(strKey)));
												}
											}
											else
											{
												dtIntPaid = dtTransDate;
											}
											if (lngBillKey > 0)
											{
												rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey));
												if (rsTemp.RecordCount() > 0)
												{
													rsTemp.Edit();
													rsTemp.Set_Fields("IntPaidDate", dtIntPaid);
													rsTemp.Update();
												}
											}
										}
									}
								}
							}
							rsTemp.OpenRecordset("SELECT BillKey FROM PaymentRec WHERE ID = " + strKey, modExtraModules.strARDatabase);
							if (rsTemp.RecordCount() > 0)
							{
								lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
								modARStatusPayments.VerifyARBillAmounts(lngBillKey);
							}
						}
						else
						{
						}
					}
					else
					{
						// batch payment - see AddBatchToReceiptArrary
					}
				}
				rsTemp.Reset();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Update Non-CR Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				{
					/*? Resume Next; */}
			}
		}
		// vbPorter upgrade warning: boolCheckAll As object	OnWrite(bool)
		private bool CheckRestrictedAccountNumbers_2(int lngType, bool boolCheckAll = true, bool boolAllowBlankAccounts = false, int lngTownCode = 1)
		{
			return CheckRestrictedAccountNumbers(ref lngType, boolCheckAll, boolAllowBlankAccounts, lngTownCode);
		}

		private bool CheckRestrictedAccountNumbers_12(int lngType, int lngTownCode = 1)
		{
			return CheckRestrictedAccountNumbers(ref lngType, true, false, lngTownCode);
		}

		private bool CheckRestrictedAccountNumbers_14(int lngType, int lngTownCode = 1)
		{
			return CheckRestrictedAccountNumbers(ref lngType, true, false, lngTownCode);
		}

		private bool CheckRestrictedAccountNumbers_17(int lngType, bool boolCheckAll = true, int lngTownCode = 1)
		{
			return CheckRestrictedAccountNumbers(ref lngType, boolCheckAll, false, lngTownCode);
		}

		private bool CheckRestrictedAccountNumbers_26(int lngType, bool boolCheckAll = true, bool boolAllowBlankAccounts = false, int lngTownCode = 1)
		{
			return CheckRestrictedAccountNumbers(ref lngType, boolCheckAll, boolAllowBlankAccounts, lngTownCode);
		}

		private bool CheckRestrictedAccountNumbers(ref int lngType, bool boolCheckAll = true, bool boolAllowBlankAccounts = false, int lngTownCode = 1)
		{
			bool CheckRestrictedAccountNumbersRet = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function has a receipt type number passed into it, it will check the receipt number
				// to make sure that all of the accounts needed are valid.  If all accounts are valid, then
				// it will return True, else it will return a false
				clsDRWrapper rsType;
				int intCT;
				lngLastTypeChecked = lngType;
				CheckRestrictedAccountNumbersRet = true;
				rsType = new clsDRWrapper();
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType) + " AND TownKey = " + FCConvert.ToString(lngTownCode), modExtraModules.strCRDatabase);
				}
				else
				{
					rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType) + " AND TownKey = 0", modExtraModules.strCRDatabase);
				}
				if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
				{
					for (intCT = 1; intCT <= 6; intCT++)
					{
						if (FCConvert.ToString(rsType.Get_Fields_String("Title" + FCConvert.ToString(intCT))) != "")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Trim(FCConvert.ToString(rsType.Get_Fields("Account" + FCConvert.ToString(intCT)))) != "")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									if (!modValidateAccount.AccountValidate(rsType.Get_Fields("Account" + FCConvert.ToString(intCT))))
									{
										// remove validation
										CheckRestrictedAccountNumbersRet = false;
										return CheckRestrictedAccountNumbersRet;
									}
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									if (Strings.Left(FCConvert.ToString(rsType.Get_Fields("Account" + FCConvert.ToString(intCT))), 1) != "M")
									{
										CheckRestrictedAccountNumbersRet = false;
										return CheckRestrictedAccountNumbersRet;
									}
								}
							}
							else
							{
								// if the string is empty then return false
								if (boolAllowBlankAccounts)
								{
									// keep going, this is for the type 97 account in the range of 901 to 999
								}
								else
								{
									CheckRestrictedAccountNumbersRet = false;
									return CheckRestrictedAccountNumbersRet;
								}
							}
						}
					}
					if (FCConvert.ToBoolean(boolCheckAll) && CheckRestrictedAccountNumbersRet)
					{
						if (lngType == 90)
						{
							// check the 91
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(91, false, lngTownCode);
							if (CheckRestrictedAccountNumbersRet)
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(890, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet)
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(891, false, lngTownCode);
							}
						}
						else if (lngType == 91)
						{
							// check the 90
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(90, false, lngTownCode);
							if (CheckRestrictedAccountNumbersRet)
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(890, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet)
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(891, false, lngTownCode);
							}
						}
						else if (lngType == 93)
						{
							// check the 94
							if (modUTFunctions.Statics.TownService != "W")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(94, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "S")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(893, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "W")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(894, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "W")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(895, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "S")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(896, false, lngTownCode);
							}
						}
						else if (lngType == 94)
						{
							// check the 93
							if (modUTFunctions.Statics.TownService != "S")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(93, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "S")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(893, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "W")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(894, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "W")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(895, false, lngTownCode);
							}
							if (CheckRestrictedAccountNumbersRet && modUTFunctions.Statics.TownService != "S")
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbersRet && CheckRestrictedAccountNumbers_17(896, false, lngTownCode);
							}
						}
						else if (lngType == 97)
						{
							// This will start recursion at 901 and check all 99 AR types or until a failure
							CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_26(901, false, true, lngTownCode);
						}
						else if (lngType == 98)
						{
							if (modUseCR.BreakdownCKRecords())
							{
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(800, false, lngTownCode);
								if (CheckRestrictedAccountNumbersRet)
								{
									CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(801, false, lngTownCode);
								}
								if (CheckRestrictedAccountNumbersRet)
								{
									CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(802, false, lngTownCode);
								}
								if (CheckRestrictedAccountNumbersRet)
								{
									CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(803, false, lngTownCode);
								}
								if (CheckRestrictedAccountNumbersRet)
								{
									CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_17(804, false, lngTownCode);
								}
							}
						}
						else if (lngType >= 901 && lngType <= 999)
						{
							if (lngType < 999)
							{
								// recursion = coding magic!
								CheckRestrictedAccountNumbersRet = CheckRestrictedAccountNumbers_26(lngType + 1, false, true, lngTownCode);
							}
						}
					}

				}
				else
				{
					CheckRestrictedAccountNumbersRet = false;
				}
				return CheckRestrictedAccountNumbersRet;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckRestrictedAccountNumbersRet = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Check Restricted Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckRestrictedAccountNumbersRet;
		}

		private void ViewPaymentRow(ref int lngRW)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				FormatPaymentInfoGrid_2(true);
				ShowFrame(fraPaymentInfo);
				int lngIndex;
				lngIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSummary.TextMatrix(lngRW, 8))));
				// this will get the index from the grid
				// fill the grid with the titles of each item
				// first col
				vsPaymentInfo.TextMatrix(0, 0, "Type");
				vsPaymentInfo.TextMatrix(1, 0, "Reference");
				vsPaymentInfo.TextMatrix(2, 0, "Control 1");
				vsPaymentInfo.TextMatrix(3, 0, "Control 2");
				vsPaymentInfo.TextMatrix(4, 0, "Control 3");
				vsPaymentInfo.TextMatrix(5, 0, "");
				vsPaymentInfo.TextMatrix(6, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD1);
				vsPaymentInfo.TextMatrix(7, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD2);
				vsPaymentInfo.TextMatrix(8, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD3);
				vsPaymentInfo.TextMatrix(9, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD4);
				vsPaymentInfo.TextMatrix(10, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD5);
				vsPaymentInfo.TextMatrix(11, 0, modUseCR.Statics.ReceiptArray[lngIndex].FeeD6);
				vsPaymentInfo.TextMatrix(12, 0, "Total");
				// second col
				vsPaymentInfo.TextMatrix(0, 1, modUseCR.Statics.ReceiptArray[lngIndex].Type + " - " + modUseCR.Statics.ReceiptArray[lngIndex].TypeDescription);
				vsPaymentInfo.TextMatrix(1, 1, modUseCR.Statics.ReceiptArray[lngIndex].Reference);
				vsPaymentInfo.TextMatrix(2, 1, modUseCR.Statics.ReceiptArray[lngIndex].Control1);
				vsPaymentInfo.TextMatrix(3, 1, modUseCR.Statics.ReceiptArray[lngIndex].Control2);
				vsPaymentInfo.TextMatrix(4, 1, modUseCR.Statics.ReceiptArray[lngIndex].Control3);
				vsPaymentInfo.TextMatrix(5, 1, "");
				vsPaymentInfo.TextMatrix(6, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee1, "#,##0.00"));
				vsPaymentInfo.TextMatrix(7, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee2, "#,##0.00"));
				vsPaymentInfo.TextMatrix(8, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee3, "#,##0.00"));
				vsPaymentInfo.TextMatrix(9, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee4, "#,##0.00"));
				vsPaymentInfo.TextMatrix(10, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee5, "#,##0.00"));
				vsPaymentInfo.TextMatrix(11, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Fee6, "#,##0.00"));
				vsPaymentInfo.TextMatrix(12, 1, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Total, "#,##0.00"));
				// fourth col
				vsPaymentInfo.TextMatrix(0, 3, "Name");
				vsPaymentInfo.TextMatrix(1, 3, "Date");
				vsPaymentInfo.TextMatrix(2, 3, "Module");
				vsPaymentInfo.TextMatrix(3, 3, "Alternative Cash Account");
				vsPaymentInfo.TextMatrix(4, 3, "Default Cash Account");
				vsPaymentInfo.TextMatrix(5, 3, "Manual Input Account");
				vsPaymentInfo.TextMatrix(6, 3, "Collection Code");
				vsPaymentInfo.TextMatrix(7, 3, "Quantity");
				vsPaymentInfo.TextMatrix(8, 3, "Show Detail");
				vsPaymentInfo.TextMatrix(9, 3, "Comment");
				vsPaymentInfo.TextMatrix(10, 3, "Record ID");
				vsPaymentInfo.TextMatrix(11, 3, "Affect Cash");
				vsPaymentInfo.TextMatrix(12, 3, "Affect Cash Drawer");
				vsPaymentInfo.TextMatrix(13, 3, "Teller ID");
				// fifth col
				vsPaymentInfo.TextMatrix(0, 4, modUseCR.Statics.ReceiptArray[lngIndex].Name);
				// XXXX PartyID here???
				vsPaymentInfo.TextMatrix(1, 4, Strings.Format(modUseCR.Statics.ReceiptArray[lngIndex].Date, "MM/dd/yyyy"));
				vsPaymentInfo.TextMatrix(2, 4, modUseCR.Statics.ReceiptArray[lngIndex].Module);
				vsPaymentInfo.TextMatrix(3, 4, modUseCR.Statics.ReceiptArray[lngIndex].CashAlternativeAcct);
				vsPaymentInfo.TextMatrix(4, 4, modUseCR.Statics.ReceiptArray[lngIndex].DefaultCashAccount);
				vsPaymentInfo.TextMatrix(5, 4, modUseCR.Statics.ReceiptArray[lngIndex].DefaultMIAccount);
				vsPaymentInfo.TextMatrix(6, 4, modUseCR.Statics.ReceiptArray[lngIndex].CollectionCode);
				vsPaymentInfo.TextMatrix(7, 4, FCConvert.ToString(modUseCR.Statics.ReceiptArray[lngIndex].Quantity));
				if (modUseCR.Statics.ReceiptArray[lngIndex].ShowDetail)
				{
					vsPaymentInfo.TextMatrix(8, 4, "True");
				}
				else
				{
					vsPaymentInfo.TextMatrix(8, 4, "False");
				}
				vsPaymentInfo.TextMatrix(9, 4, modUseCR.Statics.ReceiptArray[lngIndex].Comment);
				vsPaymentInfo.TextMatrix(10, 4, FCConvert.ToString(modUseCR.Statics.ReceiptArray[lngIndex].RecordKey));
				if (modUseCR.Statics.ReceiptArray[lngIndex].AffectCash)
				{
					vsPaymentInfo.TextMatrix(11, 4, "True");
				}
				else
				{
					vsPaymentInfo.TextMatrix(11, 4, "False");
				}
				if (modUseCR.Statics.ReceiptArray[lngIndex].AffectCashDrawer)
				{
					vsPaymentInfo.TextMatrix(12, 4, "True");
				}
				else
				{
					vsPaymentInfo.TextMatrix(12, 4, "False");
				}
				// get the teller ID
				vsPaymentInfo.TextMatrix(13, 4, strTellerID);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Viewing Payment Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatPaymentInfoGrid_2(bool boolReset)
		{
			FormatPaymentInfoGrid(ref boolReset);
		}

		private void FormatPaymentInfoGrid(ref bool boolReset)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngWid = 0;
				lngWid = vsPaymentInfo.WidthOriginal;
				if (boolReset)
				{
					// this will clear all the cells
					vsPaymentInfo.Rows = 0;
					vsPaymentInfo.Rows = 14;
				}
				//FC:FINAL:AM: decrease the height of rows
				vsPaymentInfo.RowHeight(-1, 400);
				// resize the columns
				vsPaymentInfo.ColWidth(0, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColWidth(1, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColWidth(2, FCConvert.ToInt32(lngWid * 0.032));
				vsPaymentInfo.ColWidth(3, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColWidth(4, FCConvert.ToInt32(lngWid * 0.24));
				vsPaymentInfo.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsPaymentInfo.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsPaymentInfo.Height = (vsPaymentInfo.Rows * 28) + 20;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formating Payment Info Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void EditReceiptInformation()
		{
			// this will show the information in the payment boxes and the payment can be edited and resaved only on the summary screen
			if (vsSummary.Row > 0 && fraCashOut.Visible == false)
			{
				fraPaymentInfo.Visible = false;
				int vbPorterVar1 = modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(vsSummary.Row, 8))].Type;
				if (vbPorterVar1 >= 901 && vbPorterVar1 <= 999)
				{
					// do not allow the edit
				}
				else
				{
					int vbPorterVar = modUseCR.Statics.ReceiptArray[FCConvert.ToInt32(vsSummary.TextMatrix(vsSummary.Row, 8))].Type % 100;
					if (vbPorterVar >= 90 && vbPorterVar <= 99)
					{
						// do not let the user edit any restricted codes
					}
					else
					{
						modUseCR.ShowPaymentInfo(vsSummary.Row);
					}
				}
			}
		}

		private void SetActionCode_2(string strType)
		{
			SetActionCode(ref strType);
		}

		private void SetActionCode(ref string strType)
		{
			// this routine will show a frame and the account number being typed for automovement
			boolSetAction = true;
			strActionKey = "";
			strActionCode = strType;
			// reset the code
			// Show the correct information in the Action Frame
			ShowActionPane();
		}

		private void ShowActionPane()
		{
			if (boolSetAction)
			{
				fraAutoAction.Visible = true;
				if (strActionCode == "R")
				{
					// Real Estate
					if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
					{
						fraAutoAction.Text = "RE Account";
					}
					else
					{
						ResetAutoAction();
					}
				}
				else if (strActionCode == "P")
				{
					// Personal Property
					if (modGlobalConstants.Statics.gboolPP || modGlobalConstants.Statics.gboolBL)
					{
						fraAutoAction.Text = "PP Account";
					}
					else
					{
						ResetAutoAction();
					}
				}
				else if (strActionCode == "U")
				{
					// Utility Billing
					if (modGlobalConstants.Statics.gboolUT)
					{
						fraAutoAction.Text = "UT Account";
					}
					else
					{
						ResetAutoAction();
					}
				}
				else if (strActionCode == "A")
				{
					// Account Recievable
					if (modGlobalConstants.Statics.gboolAR)
					{
						fraAutoAction.Text = "AR Account";
					}
					else
					{
						ResetAutoAction();
					}
				}
				lblAutoCode.Text = strActionKey;
			}
			else
			{
				ResetAutoAction();
			}
		}

		private void UseActionCode()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will call the next screen with the correct account number already processed
				if (boolSetAction)
				{
					if (strActionCode == "R")
					{
						if (CheckRestrictedAccountNumbers_2(90) && CheckRestrictedAccountNumbers_2(91))
						{
							cmbType.Text = "90";
							// MAL@20080903: Add check for printing options
							// Tracker Reference: 15228
							cmbType.SelectedIndex = GetTypeIndex(cmbType.Text);
							ProcessType();
							// MAL@20071206: Add call to setup to get correct globals such as Interest Basis
							modCLCalculations.CLSetup();
							frmCLGetAccount.InstancePtr.AutoShowAccount(true, FCConvert.ToInt32(Conversion.Val(strActionKey)));
						}
						else
						{
							MessageBox.Show("One of the Real Estate receipt types (90 - 91) has an invalid account.  Please return to Receipt type Setup and enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else if (strActionCode == "P")
					{
						if (CheckRestrictedAccountNumbers_2(92))
						{
							cmbType.Text = "92";
							// MAL@20080903: Add check for printing options
							// Tracker Reference: 15228
							cmbType.SelectedIndex = GetTypeIndex(cmbType.Text);
							ProcessType();
							modCLCalculations.CLSetup();
							// MAL@20071206
							frmCLGetAccount.InstancePtr.AutoShowAccount(false, FCConvert.ToInt32(Conversion.Val(strActionKey)));
						}
						else
						{
							MessageBox.Show("The Personal Property receipt type (92) has an invalid account.  Please return to Receipt type Setup and enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else if (strActionCode == "U")
					{
						if (CheckRestrictedAccountNumbers_2(93) && CheckRestrictedAccountNumbers_2(94) && CheckRestrictedAccountNumbers_2(95) && CheckRestrictedAccountNumbers_2(96))
						{
							cmbType.Text = "93";
							// MAL@20080903: Add check for printing options
							// Tracker Reference: 15228
							cmbType.SelectedIndex = GetTypeIndex(cmbType.Text);
							ProcessType();
							modUTCalculations.UTSetup();
							// MAL@20071206
							frmUTCLStatus.InstancePtr.Init(2, modUTFunctions.GetAccountKeyUT_2(FCConvert.ToInt32(Conversion.Val(strActionKey))), true);
						}
						else
						{
							// tell the user that they need to setup the account number in thier receipt type
							MessageBox.Show("One of the Utility Billing receipt types (93 - 96) has an invalid account.  Please return to Receipt type Setup and enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else if (strActionCode == "A")
					{
						if (CheckRestrictedAccountNumbers_2(97))
						{
							cmbType.Text = "97";
							// MAL@20080903: Add check for printing options
							// Tracker Reference: 15228
							cmbType.SelectedIndex = GetTypeIndex(cmbType.Text);
							ProcessType();
							modARCalculations.ARSetup();
							// MAL@20071206
							frmARCLStatus.InstancePtr.Init(2, FCConvert.ToInt32(Conversion.Val(strActionKey)), true);
						}
						else
						{
							// tell the user that they need to setup the account number in thier receipt type
							MessageBox.Show("One of the Accounts Receivable receipt types (97, 901 - 999) has an invalid account.  Please return to Receipt type Setup and enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				ResetAutoAction();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Using Action Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResetAutoAction()
		{
			boolSetAction = false;
			strActionCode = "";
			strActionKey = "";
			fraAutoAction.Visible = false;
		}

		private void FillResCodeCombo(ref int lngTypeNumber)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRC = new clsDRWrapper();
				boolAddingResCodes = true;
				cmbResCode.Clear();
				// rsRC.OpenRecordset "SELECT * FROM Type WHERE TypeCode = " & lngTypeNumber & " ORDER BY TownKey", strCRDatabase
				rsRC.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber > 0 ORDER BY TownNumber", "CentralData");
				while (!rsRC.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					cmbResCode.AddItem(Strings.Format(rsRC.Get_Fields("TownNumber"), "00") + " - " + Strings.Trim(FCConvert.ToString(rsRC.Get_Fields_String("TownName"))));
					rsRC.MoveNext();
				}
				//Application.DoEvents();
				cmbResCode.Visible = FCConvert.CBool(cmbResCode.Items.Count > 1);
				cmbResCode.SelectedIndex = 0;
				boolAddingResCodes = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				boolAddingResCodes = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Town ID Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string GetBankRoutingNumber(string strName)
		{
			string GetBankRoutingNumber = "";
			// Call Reference: 116984
			string strResult = "";
			// vbPorter upgrade warning: arrSplit As Variant --> As string()
			string[] arrSplit = null;
			arrSplit = Strings.Split(strName, "|", 2, CompareConstants.vbTextCompare);
			if (arrSplit[1] == null || arrSplit[1] == "")
			{
				strResult = "";
			}
			else
			{
				strResult = arrSplit[1];
			}
			GetBankRoutingNumber = strResult;
			return GetBankRoutingNumber;
		}
		// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
		private string GetARAccountNumber(string strCT, int lngARBillType, string strYear, string strDefaultMI)
		{
			string GetARAccountNumber = "";
			// Tracker Reference: 14057
			clsDRWrapper rsAR = new clsDRWrapper();
			string strResult = "";
			if (strCT == "1")
			{
				if (lngARBillType == 0)
				{
					rsAR.OpenRecordset("SELECT * FROM Customize", modExtraModules.strARDatabase);
					if (rsAR.RecordCount() > 0)
					{
						if (Strings.Trim(FCConvert.ToString(rsAR.Get_Fields_String("PrePayReceivableAccount"))) != "")
						{
							strResult = FCConvert.ToString(rsAR.Get_Fields_String("PrePayReceivableAccount"));
						}
						else
						{
							strResult = "";
						}
					}
					else
					{
						strResult = "";
					}
				}
				else
				{
					rsAR.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(lngARBillType), modExtraModules.strARDatabase);
					if (rsAR.RecordCount() > 0)
					{
						if (Strings.Trim(FCConvert.ToString(rsAR.Get_Fields_String("ReceivableAccount"))) != "")
						{
							strResult = FCConvert.ToString(rsAR.Get_Fields_String("ReceivableAccount"));
						}
						else
						{
							strResult = "";
						}
					}
					else
					{
						strResult = "";
					}
				}
			}
			else
			{
				// kgk 09-12-2011 TROAR-25  Allow override Tax and Interest accounts
				if (strCT == "2")
				{
					rsAR.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(lngARBillType), modExtraModules.strARDatabase);
					if (rsAR.RecordCount() > 0)
					{
						if (Strings.Trim(FCConvert.ToString(rsAR.Get_Fields_String("OverrideIntAccount"))) != "")
						{
							strResult = FCConvert.ToString(rsAR.Get_Fields_String("OverrideIntAccount"));
						}
						else
						{
							strResult = "";
						}
					}
					else
					{
						strResult = "";
					}
				}
				else if (strCT == "3")
				{
					rsAR.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + FCConvert.ToString(lngARBillType), modExtraModules.strARDatabase);
					if (rsAR.RecordCount() > 0)
					{
						if (Strings.Trim(FCConvert.ToString(rsAR.Get_Fields_String("OverrideTaxAccount"))) != "")
						{
							strResult = FCConvert.ToString(rsAR.Get_Fields_String("OverrideTaxAccount"));
						}
						else
						{
							strResult = "";
						}
					}
					else
					{
						strResult = "";
					}
				}
				if (strResult == "")
				{
					rsAR.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 97", modExtraModules.strCRDatabase);
					if (rsAR.RecordCount() > 0)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Trim(FCConvert.ToString(rsAR.Get_Fields("Account" + strCT))) != "")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strResult = modUseCR.CheckAccountForYear_78(rsAR, rsAR.Get_Fields("Account" + strCT), strYear.ToIntegerValue(), 1);
						}
						else
						{
							strResult = "";
						}
					}
					else
					{
						strResult = "";
					}
				}
			}
			GetARAccountNumber = strResult;
			return GetARAccountNumber;
		}

		private int GetTypeIndex(string strType)
		{
			int GetTypeIndex = 0;
			// Tracker Reference: 15228
			int lngResult = 0;
			int intCount = 0;
			int I;
			intCount = this.cmbType.Items.Count;
			for (I = 0; I <= intCount - 1; I++)
			{
				if (this.cmbType.ComboItem(I) == strType)
				{
					lngResult = I;
					break;
				}
			}
			// I
			GetTypeIndex = lngResult;
			return GetTypeIndex;
		}

		private bool ValidateECheckOptions()
		{
			bool ValidateECheckOptions = false;
			// Tracker Reference: 16641
			bool blnResult;
			bool blnMulti;
			clsDRWrapper rsCheck = new clsDRWrapper();
			clsDRWrapper rsDetail = new clsDRWrapper();
			string strLast = "";
			blnResult = true;
			// Optimistic
			if (blnIsSale)
			{
				// Routing Number (Bank Selection)
				if (cmbBank.SelectedIndex == -1)
				{
					blnResult = false;
					MessageBox.Show("You must select a bank before continuing!", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// ElseIf (cmbBank.Text) = "All" Then
					// blnResult = False
					// MsgBox "You cannot select this bank for e-check processing!", vbExclamation + vbOKOnly, "Invalid Selection"
				}
				else
				{
					if (GetBankRoutingNumber(Strings.Trim(cmbBank.Text)) == "")
					{
						blnResult = false;
						MessageBox.Show("You must specify the Bank Routing Number in order to complete e-check processing!", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						blnResult = true;
					}
				}
				if (blnResult)
				{
					// Account Number
					blnResult = FCConvert.CBool(txtCheckAcctNum.Text != "");
					if (blnResult)
					{
						// Account Type
						blnResult = FCConvert.CBool(cmbCheckAcctType.SelectedIndex != -1);
						if (blnResult)
						{
							// Check Type
							blnResult = FCConvert.CBool(cmbCheckType.SelectedIndex != -1);
							if (blnResult)
							{
								// Check Number
								blnResult = FCConvert.CBool(txtCashOutCheckNumber.Text != "");
								if (blnResult)
								{
									// Done
								}
								else
								{
									MessageBox.Show("You must enter a Check Number", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							else
							{
								MessageBox.Show("You must specify a Check Type", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						else
						{
							MessageBox.Show("You must specify an Account Type", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else
					{
						MessageBox.Show("You must enter the Account Number", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				blnResult = true;
			}
			ValidateECheckOptions = blnResult;
			return ValidateECheckOptions;
		}

		private bool ValidateEPymtOptions()
		{
			bool ValidateEPymtOptions = false;
			// Tracker Reference: 16641
			bool blnResult;
			blnResult = true;
			// Optimistic
			if (blnIsSale)
			{
				// Credit Card Type
				blnResult = FCConvert.CBool(cmbCashOutCC.SelectedIndex != -1);
				if (blnResult)
				{
					if (!blnDebit)
					{
						// CC Number
						// kgk 01-27-2012  Add InforME   'kgk 09-29-2011  Add Invoice Cloud
						if (modGlobal.Statics.gstrEPaymentPortal != "P" && modGlobal.Statics.gstrEPaymentPortal != "I" && modGlobal.Statics.gstrEPaymentPortal != "E")
						{
							blnResult = FCConvert.CBool(txtCCNum.Text != "");
							if (blnResult)
							{
								// Expiration Date
								blnResult = FCConvert.CBool(cboExpMonth.SelectedIndex != -1 && cboExpYear.SelectedIndex != -1);
								if (blnResult)
								{
									// CVV2
									blnResult = FCConvert.CBool(txtCVV2.Text != "");
									if (blnResult)
									{
										// Done
									}
									else
									{
										MessageBox.Show("You must enter the Card Security Code (CVV2)", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
								}
								else
								{
									MessageBox.Show("You must enter an Expiration Date", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							else
							{
								MessageBox.Show("You must enter a Credit Card Number", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						else
						{
							blnResult = true;
						}
					}
					else
					{
						// Debit Card Types do not require the CC number, etc.
						blnResult = true;
					}
				}
				else
				{
					MessageBox.Show("You must select a Credit Card Type", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				blnResult = FCConvert.CBool(cmbCashOutCC.SelectedIndex != -1);
				if (blnResult)
				{
					// Continue
				}
				else
				{
					MessageBox.Show("You must select a Credit Card Type", "Required Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}

			}
			ValidateEPymtOptions = blnResult;
			return ValidateEPymtOptions;
		}

		private bool ProcessEPayment_54(int intIndex, string strCryptCard = "", string strExpDate = "", bool blnConvenienceFee = false, bool blnVISANonTax = false, bool blnCancelTransaction = false, bool blnTaxTransaction = false, string strZipCode = "")
		{
			return ProcessEPayment(intIndex, strCryptCard, strExpDate, blnConvenienceFee, blnVISANonTax, blnCancelTransaction, blnTaxTransaction, strZipCode);
		}

		private bool ProcessEPayment_1107(int intIndex, string strCryptCard = "", string strExpDate = "", bool blnConvenienceFee = false, string strZipCode = "")
		{
			return ProcessEPayment(intIndex, strCryptCard, strExpDate, blnConvenienceFee, false, false, false, strZipCode);
		}

		private bool ProcessEPayment_1188(int intIndex, string strCryptCard = "", string strExpDate = "", bool blnConvenienceFee = false, bool blnVISANonTax = false, string strZipCode = "")
		{
			return ProcessEPayment(intIndex, strCryptCard, strExpDate, blnConvenienceFee, blnVISANonTax, false, false, strZipCode);
		}

		private bool ProcessEPayment_1836(int intIndex, string strCryptCard = "", string strExpDate = "", bool blnConvenienceFee = false, bool blnTaxTransaction = false, string strZipCode = "")
		{
			return ProcessEPayment(intIndex, strCryptCard, strExpDate, blnConvenienceFee, false, false, blnTaxTransaction, strZipCode);
		}

		private bool ProcessEPayment(int intIndex, string strCryptCard = "", string strExpDate = "", bool blnConvenienceFee = false, bool blnVISANonTax = false, bool blnCancelTransaction = false, bool blnTaxTransaction = false, string strZipCode = "")
		{
			bool ProcessEPayment = false;
			// Tracker Reference: 16641
			clsDRWrapper rsCR = new clsDRWrapper();
			bool blnResult = false;
			string strError = "";
			string strFile;
			// Full Path to Pass/Result File
			string strParam;
			string strPass = "";
			string strExePath;
			string strResult = "";
			object objExe;
			string strLogFile;
			string strLogData = "";

			/* XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX THIS HAS TO BE REPLACED XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

try
{
// On Error GoTo ErrorHandler
rsCR.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
if (!blnCancelTransaction)
{
	if (blnIsSale)
	{
		// Create Parameter String
		if (arr[intIndex].Type == 1)
		{
			strPass = BuildECheckString(intIndex);
		}
		else
		{
			strPass = BuildECCString(intIndex, strCryptCard, strExpDate, blnConvenienceFee, blnVISANonTax, blnTaxTransaction, strZipCode);
		}
	}
	else
	{
		if (blnIsVoid)
		{
			// Create Parameter String
			if (arr[intIndex].Type == 1)
			{
				strPass = BuildECheckString_Credit(intIndex);
			}
			else
			{
				if (arr[intIndex].IsVISA)
				{
					// If VISA Card the amount will be non tax because tax receipt types are restricted and can't be voided
					strPass = BuildECCString_Credit_6(intIndex, true);
				}
				else
				{
					// If not VISA then use the normal amount because we don't account for non tax amounts for anythign but VISA
					strPass = BuildECCString_Credit_6(intIndex, false);
				}
			}
		}
		else
		{
			// Create Parameter String
			if (arr[intIndex].Type == 1)
			{
				strPass = BuildECheckString_Credit(intIndex);
			}
			else
			{
				strPass = BuildECCString_Credit(intIndex, blnVISANonTax);
			}
		}
	}
}
else
{
	// Create Parameter String
	if (arr[intIndex].Type == 1)
	{
		strPass = BuildECheckString_Credit(intIndex);
	}
	else
	{
		strPass = BuildECCString_Credit(intIndex, blnVISANonTax);
	}
}
// Build File
strFile = modRegistry.GetRegistryKey("EPymtFilePath", "CR");
if (strFile == "")
{
	// Set default path
	strFile = FCFileSystem.Statics.UserDataFolder + "\\";
}
else if (Strings.Right(strFile, 1) != "\\")
{
	strFile += "\\";
}
strFile += "EPaymentData.txt";
FCFileSystem.FileOpen(1, strFile, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
FCFileSystem.PrintLine(1, strPass);
FCFileSystem.FileClose(1);
// Determine Portal Parameter
strParam = ",";
if (modGlobal.Statics.gstrEPaymentPortal == "A")
{
	strParam += "AIM";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "C")
{
	strParam += "CSI";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "G")
{
	strParam += "GOE";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "M")
{
	strParam += "MON";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "R")
{
	strParam += "RBS";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "P")
{
	strParam += "CRP";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "I")
{
	// kgk 09-29-2011   Invoice Cloud
	strParam += "ICL";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "E")
{
	// kgk 01-27-2012   InforME
	strParam += "IME";
}
if (blnIsSale)
{
	strParam += ",S";
}
else
{
	strParam += ",C";
}
// Call Executable
objExe = Interaction.CreateObject("WScript.Shell");
strExePath = Application.StartupPath;
if (Strings.Right(strExePath, 1) != "\\")
{
	strExePath += "\\";
}
//FC:TODO:AM
//objExe.Run(strExePath + "HarrisEPayments.exe " + strFile + strParam, AppWinStyle.NormalFocus, 1);
objExe = null;
// Get Result String
FCFileSystem.FileOpen(1, strFile, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
strResult = FCFileSystem.LineInput(1);
// Build File
strLogFile = modRegistry.GetRegistryKey("EPymtFilePath", "CR");
if (strLogFile == "")
{
	// Set default path
	strLogFile = FCFileSystem.Statics.UserDataFolder + "\\";
}
else if (Strings.Right(strLogFile, 1) != "\\")
{
	strLogFile += "\\";
}
strLogFile += "CCLog.txt";
FCFileSystem.FileOpen(2, strLogFile, OpenMode.Append, (OpenAccess)(-1), (OpenShare)(-1), -1);
FCFileSystem.PrintLine(2, fecherFoundation.Strings.StrP(DateTime.Now));
FCFileSystem.PrintLine(2, "Passed In: " + strPass);
FCFileSystem.PrintLine(2, "Returned:  " + strResult);
while (!FCFileSystem.EOF(1))
{
	strLogData = FCFileSystem.LineInput(1);
	FCFileSystem.PrintLine(2, strLogData);
}
FCFileSystem.PrintLine(2, "");
FCFileSystem.PrintLine(2, "");
FCFileSystem.PrintLine(2, "");
FCFileSystem.FileClose(2);
FCFileSystem.FileClose(1);
// Parse Result
strResult = Strings.Replace(strResult, "\"", "", 1, -1, CompareConstants.vbTextCompare);
if (modGlobal.Statics.gstrEPaymentPortal == "R")
{
	strData = Strings.Split(strResult, "&", -1, CompareConstants.vbTextCompare);
}
else
{
	strData = Strings.Split(strResult, "|", -1, CompareConstants.vbTextCompare);
}
if (blnIsSale)
{
	if (modGlobal.Statics.gstrEPaymentPortal == "A")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[1]) == 1);
		strRefNum = strData[4];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "C")
	{
		blnResult = FCConvert.CBool(strData[2] == "Y");
		strRefNum = strData[0];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "G")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
		strRefNum = strData[1];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "M")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[2]) < 50);
		strRefNum = strData[0] + "|" + strData[1];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "R")
	{
		blnResult = FCConvert.CBool(Strings.InStr(1, strData[0], "0", CompareConstants.vbTextCompare) > 0);
		strRefNum = strData[2];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "P")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
		strRefNum = strData[3];
		strReturnedAuthCode = strData[6];
		strReturnedCNum = strData[7];
		strReturnedExpDate = strData[15];
		strReturnedCryptCard = strData[16];
		strReturnedZipCode = strData[14];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "I")
	{
		// kgk  09-29-2011  Add Invoice Cloud
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
		strRefNum = strData[3];
		strReturnedAuthCode = strData[6];
		strReturnedCNum = strData[7];
		strReturnedExpDate = strData[13];
		strReturnedCryptCard = strData[14];
		strReturnedZipCode = strData[12];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "E")
	{
		// kgk  01-27-2012  Add InforME
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
		strRefNum = strData[3];
		strReturnedAuthCode = strData[6];
		strReturnedCNum = strData[7];
		strReturnedExpDate = strData[13];
		// strReturnedCryptCard = strData(14)   ' no cryptcard
		strReturnedZipCode = strData[12];
		// strReturnedPortChg = strData(xx)???XXX
	}
	if (!blnResult)
	{
		if (modGlobal.Statics.gstrEPaymentPortal == "A")
		{
			strError = strData[2];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "C")
		{
			strError = strData[3];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "G")
		{
			strError = strData[2];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "M")
		{
			strError = strData[3];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "R")
		{
			strError = strData[5];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "P")
		{
			strError = strData[1] + "\r\n" + strData[2];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "I")
		{
			// kgk  09-29-2011  Add Invoice Cloud
			strError = strData[1] + "\r\n" + strData[2];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "E")
		{
			// kgk  01-27-2012  Add InforME
			strError = strData[1] + "\r\n" + strData[2];
		}
		// Display Error Message
		MessageBox.Show(strError, "Payment Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
	}
}
else
{
	// Get Credit Results
	if (modGlobal.Statics.gstrEPaymentPortal == "A")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[1]) == 1);
		strRefNum = strData[4];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "C")
	{
		blnResult = FCConvert.CBool(strData[5] == "Y");
		strRefNum = strData[0];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "G")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[1]) == 1);
		strRefNum = strData[2];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "M")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[2]) < 50);
		strRefNum = strData[0] + "|" + strData[1];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "R")
	{
		blnResult = FCConvert.CBool(Strings.InStr(1, strData[0], "0", CompareConstants.vbTextCompare) > 0);
		strRefNum = strData[2];
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "P")
	{
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
		strRefNum = strData[3];
		strReturnedCNum = strData[6];
		strReturnedExpDate = strData[7];
		strReturnedCryptCard = strData[8];
		strReturnedAuthCode = "";
		strReturnedZipCode = "";
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "I")
	{
		// kgk 09-29-2011   Add Invoice Cloud
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
		strRefNum = strData[3];
		strReturnedCNum = strData[6];
		strReturnedExpDate = strData[7];
		strReturnedCryptCard = strData[8];
		strReturnedAuthCode = "";
		strReturnedZipCode = "";
	}
	else if (modGlobal.Statics.gstrEPaymentPortal == "E")
	{
		// kgk 01-27-2012   Add InforME
		blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
		strRefNum = strData[3];
		strReturnedCNum = strData[6];
		strReturnedExpDate = strData[7];
		// strReturnedCryptCard = strData(8)  ' no cryptcard
		strReturnedAuthCode = "";
		strReturnedZipCode = "";
	}
	if (!blnResult)
	{
		if (modGlobal.Statics.gstrEPaymentPortal == "A")
		{
			strError = strData[2];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "C")
		{
			strError = strData[6];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "G")
		{
			strError = strData[4];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "M")
		{
			strError = strData[3];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "R")
		{
			strError = strData[2];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "P")
		{
			strError = strData[1];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "I")
		{
			// kgk 09-29-2011  Add Invoice Cloud
			strError = strData[1];
		}
		else if (modGlobal.Statics.gstrEPaymentPortal == "E")
		{
			// kgk 01-27-2012  Add Invoice Cloud
			strError = strData[1];
		}
		// Display Error Message
		MessageBox.Show(strError, "Payment Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
	}
}
ProcessEPayment = blnResult;
return ProcessEPayment;
}
catch (Exception ex)
{
// ErrorHandler:
MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + Information.Err(ex).Description, "Payment Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
ProcessEPayment = false;
}
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX THIS HAS TO BE REPLACED XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX */

return ProcessEPayment;
}

private string BuildECheckString(int intIndex)
{
string BuildECheckString = "";
// Tracker Reference: 16641
int lngPymtID = 0;
int lngTransID = 0;
string strResult = "";
clsDRWrapper rsInfo = new clsDRWrapper();
clsDRWrapper rsTrans = new clsDRWrapper();
rsTrans.OpenRecordset("SELECT MAX(ID) AS LastKey FROM CheckMaster", modExtraModules.strCRDatabase);
if (rsTrans.RecordCount() > 0)
{
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
lngPymtID = FCConvert.ToInt32(rsTrans.Get_Fields("LastKey")) + 1;
}
rsTrans.OpenRecordset("SELECT MAX(ReceiptKey) AS LastKey FROM Receipt", modExtraModules.strCRDatabase);
if (rsTrans.RecordCount() > 0)
{
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
lngTransID = FCConvert.ToInt32(rsTrans.Get_Fields("LastKey")) + 1;
}
rsInfo.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
if (modGlobal.Statics.gstrEPaymentPortal == "A")
{
strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
// API Login ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// Transaction ID
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult += "|" + "Y";
}
else
{
	strResult += "|" + "N";
}
strResult += "|" + "Y";
// Is E-Check
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Amount
strResult += "|" + GetBankRoutingNumber(arr[intIndex].Bank);
// Bank ABA Code
strResult += "|" + arr[intIndex].ChkAcctNum;
// Bank Acct Num
if (arr[intIndex].ChkAcctType == "Business")
{
	// Bank Acct Type
	strResult += "|BUSINESSCHECKING";
}
else
{
	strResult += "|" + Strings.UCase(arr[intIndex].ChkAcctType);
}
strResult += "|" + arr[intIndex].Bank;
// Bank Name
strResult += "|" + arr[intIndex].ChkAcctName;
// Name of Acct Holder
strResult += "|" + "AUTH_CAPTURE";
// Echeck Transaction Type
if (arr[intIndex].ChkAcctType == "Business")
{
	// Check Type
	strResult += "|" + "CCD";
}
else
{
	strResult += "|" + "WEB";
}
strResult += "|" + "";
// Original Transaction ID - N/A
}
else if (modGlobal.Statics.gstrEPaymentPortal == "C")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Client ID - Different if Testing
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentTestKey"));
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
}
strResult += "|" + "1";
// Payment Method = ECheck
strResult += "|";
// Card Type - N/A
strResult += "|" + "2";
// Collection Mode (POS - Manual)
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Amount
strResult += "|" + "1";
// Static Customer Payment ID
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// ItemAmount
strResult += "|" + FCConvert.ToString(lngTransID);
// Transaction ID
strResult += "|" + "";
// Authorize/Force - N/A
strResult += "|" + "";
// Blank for initial
strResult += "|" + arr[intIndex].ChkAcctName;
// Acct Holder Name
strResult += "|" + "";
// Address - Optional
strResult += "|" + "";
// Address 2 - Optional
strResult += "|" + "";
// City - Optional
strResult += "|" + "";
// State - Optional
strResult += "|" + "";
// Zip - Optional
strResult += "|" + "";
// Country - Optional
strResult += "|" + "";
// Email - Optional
strResult += "|" + "";
// Phone - Optional
strResult += "|" + "";
// Shipping Name - Optional
strResult += "|" + "";
// Ship Address - Optional
strResult += "|" + "";
// Ship Addr2 - Optional
strResult += "|" + "";
// Ship City - Optional
strResult += "|" + "";
// Ship State - Optional
strResult += "|" + "";
// Ship Zip - Optional
strResult += "|" + "";
// Ship Country - Optional
strResult += "|" + "";
// Ship Email - Optional
strResult += "|" + "";
// Ship Phone - Optional
strResult += "|" + arr[intIndex].Bank;
// Bank Name
if (arr[intIndex].ChkAcctType == "Checking")
{
	// Acct Type
	strResult += "|" + "1";
}
else
{
	strResult += "|" + "2";
}
if (arr[intIndex].ChkCheckType == "Business")
{
	// Check Type
	strResult += "|" + "1";
}
else
{
	strResult += "|" + "2";
}
strResult += "|" + arr[intIndex].CType;
// Check Number
strResult += "|" + GetBankRoutingNumber(arr[intIndex].Bank);
// RoutingNum
strResult += "|" + arr[intIndex].ChkAcctNum;
// Account Num
strResult += "|" + "";
// Check Image Name - Optional
strResult += "|" + "";
// Effective Date - Optional
strResult += "|" + "";
// Notes - Optional
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
// CSI Org Unit ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// CSI Payer ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentUserID");
// CSI User ID
strResult += "|" + FCConvert.ToString(lngPymtID);
// Payment ID
strResult += "|" + FCConvert.ToString(lngPymtID);
// Payment ID Secondary
}
else if (modGlobal.Statics.gstrEPaymentPortal == "G")
{
strResult = "";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "M")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	strResult = "monusqa025";
	strResult += "|" + "qatoken";
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
	// StoreID
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
	// APIToken
}
strResult += "|" + "Y";
// Is E-Check
strResult += "|" + FCConvert.ToString(lngTransID);
// Order ID
strResult += "|" + "";
// Credit Card Num - Blank for E-Checks
strResult += "|" + "";
// Expiration Date - Blank for E-Checks
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Amount
strResult += "|" + "WEB";
// SEC Code
strResult += "|" + "";
// Customer ID - Optional
strResult += "|" + GetBankRoutingNumber(arr[intIndex].Bank);
// Routing Num
strResult += "|" + arr[intIndex].ChkAcctNum;
// AccountNum
strResult += "|" + Strings.LCase(arr[intIndex].ChkAcctType);
// Account Type
strResult += "|" + arr[intIndex].CType;
// CheckNum
}
else if (modGlobal.Statics.gstrEPaymentPortal == "R")
{
strResult = "";
}
rsTrans.Reset();
rsInfo.Reset();
BuildECheckString = strResult;
return BuildECheckString;
}

private string BuildECheckString_Credit(int intIndex)
{
string BuildECheckString_Credit = "";
// Tracker Reference: 16641
int lngPymtID;
int lngTransID;
string strResult = "";
clsDRWrapper rsInfo = new clsDRWrapper();
clsDRWrapper rsTrans = new clsDRWrapper();
rsInfo.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
if (modGlobal.Statics.gstrEPaymentPortal == "A")
{
strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
// API Login ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// Transaction ID
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult += "|" + "Y";
}
else
{
	strResult += "|" + "N";
}
strResult += "|" + "Y";
// Is E-Check
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Amount
strResult += "|" + GetBankRoutingNumber(arr[intIndex].Bank);
// Bank ABA Code
strResult += "|" + arr[intIndex].ChkAcctNum;
// Bank Acct Num
strResult += "|" + "";
// Bank Acct Type
strResult += "|" + arr[intIndex].Bank;
// Bank Name
strResult += "|" + "";
// Name of Acct Holder
strResult += "|" + "CREDIT";
// Echeck Transaction Type
strResult += "|" + "WEB";
// Check Type
strResult += "|" + arr[intIndex].RefNumber;
// Original Transaction ID
}
else if (modGlobal.Statics.gstrEPaymentPortal == "C")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Client ID - Different if Testing
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentTestKey"));
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
}
strResult += "|" + arr[intIndex].RefNumber;
// PRC
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Amount
strResult += "|" + "0";
// Internal Credit
strResult += "|" + "0";
// Chargeback
strResult += "|" + "";
// Notes
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
// CSI Org Unit ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentUserID");
// CSI User ID
}
else if (modGlobal.Statics.gstrEPaymentPortal == "G")
{
strResult = "";
}
else if (modGlobal.Statics.gstrEPaymentPortal == "M")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	strResult = "monusqa025";
	strResult += "|" + "qatoken";
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
	// StoreID
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
	// APIToken
}
strResult += "|" + "N";
// Is E-Check
strResult += "|" + Strings.Left(arr[intIndex].RefNumber, Strings.InStr(1, "|", arr[intIndex].RefNumber, CompareConstants.vbTextCompare) - 1);
// Order ID
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Amount
strResult += "|" + Strings.Right(arr[intIndex].RefNumber, arr[intIndex].RefNumber.Length - Strings.InStr(1, "|", arr[intIndex].RefNumber, CompareConstants.vbTextCompare));
// Transaction Number
strResult += "|" + "PPD";
// Crypt Type - SSL Enabled Merchant
strResult += "|" + "";
// Customer ID - Optional
strResult += "|" + GetBankRoutingNumber(arr[intIndex].Bank);
// Routing Num
strResult += "|" + arr[intIndex].ChkAcctNum;
// AccountNum
strResult += "|" + Strings.LCase(arr[intIndex].ChkAcctType);
// Account Type
strResult += "|" + "";
// CheckNum - N/A
}
else if (modGlobal.Statics.gstrEPaymentPortal == "R")
{
strResult = "";
}
rsTrans.Reset();
rsInfo.Reset();
BuildECheckString_Credit = strResult;
return BuildECheckString_Credit;
}

private string BuildECCString(int intIndex, string strCryptCard = "", string strExpDate = "", bool blnConvenienceFee = false, bool blnVISANonTax = false, bool blnTaxTransaction = false, string strZipCode = "")
{
string BuildECCString = "";
int lngPymtID = 0;
string strTransID = "";
string strCC = "";
string strResult = "";
clsDRWrapper rsInfo = new clsDRWrapper();
clsDRWrapper rsTrans = new clsDRWrapper();
rsTrans.OpenRecordset("SELECT MAX(ID) AS LastKey FROM CCMaster", modExtraModules.strCRDatabase);
if (rsTrans.RecordCount() > 0)
{
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
if (fecherFoundation.FCUtils.IsNull(rsTrans.Get_Fields("LastKey")) || FCConvert.ToString(rsTrans.Get_Fields("LastKey")) == "")
{
	lngPymtID = 1;
}
else
{
	// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
	lngPymtID = FCConvert.ToInt32(rsTrans.Get_Fields("LastKey")) + 1;
}
}
rsTrans.OpenRecordset("SELECT MAX(ReceiptKey) AS LastKey FROM Receipt", modExtraModules.strCRDatabase);
if (rsTrans.RecordCount() > 0)
{
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
if (fecherFoundation.FCUtils.IsNull(rsTrans.Get_Fields("LastKey")) || FCConvert.ToString(rsTrans.Get_Fields("LastKey")) == "")
{
	strTransID = Strings.Format(DateTime.Now, "mmddyyhhmmss") + "1";
}
else
{
	// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
	strTransID = Strings.Format(DateTime.Now, "mmddyyhhmmss") + FCConvert.ToString(FCConvert.ToInt32(rsTrans.Get_Fields("LastKey")) + 1);
}
}
rsInfo.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
if (modGlobal.Statics.gstrEPaymentPortal == "A")
{
strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
// API Login ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// Transaction ID
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult += "|" + "Y";
}
else
{
	strResult += "|" + "N";
}
strResult += "|" + "N";
// Is E-Check
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Amount
strResult += "|" + arr[intIndex].CNum;
// Card Number
strResult += "|" + arr[intIndex].ChkAcctNum;
// Exp Date
strResult += "|" + "AUTH_CAPTURE";
// Transaction Type
strResult += "|" + "";
// Bank Name - N/A
strResult += "|" + "";
// Name of Acct Holder - N/A
strResult += "|" + "";
// Echeck Transaction Type - N/A
strResult += "|" + "";
// Echeck Type - N/A
strResult += "|" + "";
// Original Transaction ID - N/A
}
else if (modGlobal.Statics.gstrEPaymentPortal == "C")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Client ID - Different if Testing
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentTestKey"));
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
}
strResult += "|" + "0";
// Payment Method = Credit Card
strCC = GetCreditCardName(FCConvert.ToInt32(arr[intIndex].CType));
// Card Type
if (Strings.UCase(strCC) == "AMEX")
{
	strResult += "|" + "3";
}
else if (Strings.UCase(strCC) == "VISA")
{
	strResult += "|" + "4";
}
else if (Strings.UCase(strCC) == "MASTERCARD")
{
	strResult += "|" + "5";
}
else if (Strings.UCase(strCC) == "DISCOVER")
{
	strResult += "|" + "6";
}
strResult += "|" + "2";
// Collection Mode (POS Manual)
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Amount
strResult += "|" + "1";
// Static Customer Payment ID
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// ItemAmount
strResult += "|" + strTransID;
// Transaction ID
strResult += "|" + "2";
// Authorize/Force
strResult += "|" + "";
// PRC - Blank for initial
strResult += "|" + "CSI_Customer";
// Acct Holder Name - Dummy data
strResult += "|" + "";
// Address - Optional
strResult += "|" + "";
// Address 2 - Optional
strResult += "|" + "";
// City - Optional
strResult += "|" + "";
// County - Optional
strResult += "|" + "";
// State - Optional
strResult += "|" + "";
// Zip - Optional
strResult += "|" + "";
// Country - Optional
strResult += "|" + "";
// Email - Optional
strResult += "|" + "";
// Phone - Optional
strResult += "|" + "";
// Shipping Name - Optional
strResult += "|" + "";
// Ship Address - Optional
strResult += "|" + "";
// Ship Addr2 - Optional
strResult += "|" + "";
// Ship City - Optional
strResult += "|" + "";
// Ship State - Optional
strResult += "|" + "";
// Ship Zip - Optional
strResult += "|" + "";
// Ship Country - Optional
strResult += "|" + "";
// Ship Email - Optional
strResult += "|" + "";
// Ship Phone - Optional
strResult += "|" + arr[intIndex].CVV2;
// CVV2
strResult += "|" + arr[intIndex].CNum;
// Credit Card Num
strResult += "|" + Strings.Format(arr[intIndex].ExpDate, "MMYY");
// Exp Date
strResult += "|" + "";
// Check Number - N/A
strResult += "|" + "";
// RoutingNum - N/A
strResult += "|" + "";
// Account Num - N/A
strResult += "|" + "";
// Check Image Name - Optional
strResult += "|" + "";
// Effective Date - Optional
strResult += "|" + "";
// Notes - Optional
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
// CSI Org Unit ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// CSI Payer ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentUserID");
// CSI User ID
strResult += "|" + FCConvert.ToString(lngPymtID);
// Payment ID
strResult += "|" + FCConvert.ToString(lngPymtID);
// Payment ID Secondary
}
else if (modGlobal.Statics.gstrEPaymentPortal == "G")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult = "1264";
	// Merchant ID
	strResult += "|" + "password";
	// Password
	strResult += "|" + "";
	// Gateway ID
	strResult += "|" + "Y";
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
	// Merchant ID
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
	// Password
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
	// Gateway ID
	strResult += "|" + "N";
}
strResult += "|" + "sale";
// Operation Type
strResult += "|" + strTransID;
// Order ID
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Total
strResult += "|" + arr[intIndex].CType;
// Card Name
strResult += "|" + arr[intIndex].CNum;
// Card Number
strResult += "|" + Strings.Format(arr[intIndex].ExpDate, "MMYY");
// Exp Date
strResult += "|" + arr[intIndex].CVV2;
// CVV2 / Security Code
strResult += "|" + arr[intIndex].OwnerName;
// Owner Name
strResult += "|" + arr[intIndex].OwnerStreet;
// Owner Street
strResult += "|" + arr[intIndex].OwnerCity;
// Owner City
strResult += "|" + arr[intIndex].OwnerState;
// Owner State
strResult += "|" + arr[intIndex].OwnerZip;
// Owner Zip
strResult += "|" + arr[intIndex].OwnerCountry;
// Owner Country
strResult += "|" + "";
// Owner E-mail - Optional
strResult += "|" + "";
// Owner Phone - Optional
strResult += "|" + "0";
// Recurring Flag - 0 = No
strResult += "|" + "";
// Recurring Type - Optional
strResult += "|" + "";
// Customer IP Address - Optional
}
else if (modGlobal.Statics.gstrEPaymentPortal == "M")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	strResult = "monusqa025";
	strResult += "|" + "qatoken";
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
	// StoreID
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
	// APIToken
}
strResult += "|" + "N";
// Is E-Check
strResult += "|" + strTransID;
// Order ID
strResult += "|" + arr[intIndex].CNum;
// Credit Card Num
strResult += "|" + Strings.Format(arr[intIndex].ExpDate, "YYMM");
// Expiration Date - Blank for E-Checks
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Amount
strResult += "|" + "7";
// Crypt Type - SSL Enabled Merchant
strResult += "|" + "";
// Customer ID - Optional
strResult += "|" + "";
// Routing Num - N/A
strResult += "|" + "";
// AccountNum - N/A
strResult += "|" + "";
// Account Type - N/A
strResult += "|" + "";
// CheckNum - N/A
if (blnIsSwipe)
{
	strResult += "|" + "Y";
	// Is Swiped Transaction
	strResult += "|" + Strings.Right(txtTrack2.Text, txtTrack2.Text.Length - 1);
	// Track 2 Data for Swiped Transactions
}
else
{
	strResult += "|" + "N";
	// Is Swiped Transaction
	strResult += "|" + "";
	// Track 2 Data for Swiped Transactions
}
strResult += "|" + "00";
// POS Code - Used for Swiped but Should Be '00' for all CC Transactions
}
else if (modGlobal.Statics.gstrEPaymentPortal == "R")
{
strResult = "HTTPS://tf.lynk-systems.com/LE0100082Pmt";
// Lynk Host Name
strResult += "," + "SvcType=Sale";
// Service Type
strResult += "&" + "StoreId=" + rsInfo.Get_Fields_String("EPaymentClientID");
// Store ID
strResult += "&" + "MerchantId=" + rsInfo.Get_Fields_String("EPaymentClientPassword");
// Merchant ID
strResult += "&" + "TerminalId=" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// Terminal ID
strResult += "&" + "CardNumber=" + arr[intIndex].CNum;
// Card Number
strResult += "&" + "ExpirationDate=" + Strings.Format(arr[intIndex].ExpDate, "MM/YY");
// Expiration Date
strResult += "&" + "EntryMode=" + "0";
// Entry Mode
strResult += "&" + "Signature=" + "1";
// Signature
strResult += "&" + "Amount=" + FCConvert.ToString(arr[intIndex].Amount);
// Amount
}
else if (modGlobal.Statics.gstrEPaymentPortal == "P")
{
if (blnConvenienceFee)
{
	if (blnTaxTransaction)
	{
		// MsgBox "Authorizing to Tax Convenience Account"
		strResult = rsInfo.Get_Fields_String("EPaymentClientIDConvenienceFee") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordConvenienceFee") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayConvenienceFee") + "|";
		// Alias
	}
	else
	{
		string vbPorterVar = arr[intIndex].Bank;
		if ((vbPorterVar == "VISA") || (vbPorterVar == "VISA Debit"))
		{
			// MsgBox "Authorizing to Tax Convenience Account"
			strResult = rsInfo.Get_Fields_String("EPaymentClientIDConvenienceFee") + "|";
			// Merchant ID
			strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordConvenienceFee") + "|";
			// Password
			strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayConvenienceFee") + "|";
			// Alias
		}
		else if ((vbPorterVar == "Discover") || (vbPorterVar == "MasterCard"))
		{
			// MsgBox "Authorizing to MasterCard / Discover Convenience Account"
			strResult = rsInfo.Get_Fields_String("EPaymentClientIDMCDiscoverConvenienceFee") + "|";
			// Merchant ID
			strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordMCDiscoverConvenienceFee") + "|";
			// Password
			strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayMCDiscoverConvenienceFee") + "|";
			// Alias
		}
		else if (vbPorterVar == "American Express")
		{
			// MsgBox "Authorizing to AmEx Convenience Account"
			strResult = rsInfo.Get_Fields_String("EPaymentClientIDAmExConvenienceFee") + "|";
			// Merchant ID
			strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordAmExConvenienceFee") + "|";
			// Password
			strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayAmExConvenienceFee") + "|";
			// Alias
		}
	}
}
else if (blnVISANonTax)
{
	string vbPorterVar1 = arr[intIndex].Bank;
	if ((vbPorterVar1 == "VISA") || (vbPorterVar1 == "VISA Debit"))
	{
		// MsgBox "Authorizing to VISA Non Tax Account"
		strResult = rsInfo.Get_Fields_String("EPaymentClientIDVISA") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordVISA") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayVISA") + "|";
		// Alias
	}
	else if ((vbPorterVar1 == "Discover") || (vbPorterVar1 == "MasterCard"))
	{
		// MsgBox "Authorizing to MasterCard / Discover Account"
		strResult = rsInfo.Get_Fields_String("EPaymentClientIDMCDiscover") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordMCDiscover") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayMCDiscover") + "|";
		// Alias
	}
	else if (vbPorterVar1 == "American Express")
	{
		// MsgBox "Authorizing to AmEx Account"
		strResult = rsInfo.Get_Fields_String("EPaymentClientIDAmEx") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordAmEx") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayAmEx") + "|";
		// Alias
	}
}
else
{
	if (!modGlobal.Statics.gblnNoVISAPayments)
	{
		// MsgBox "Authorizing to Tax Account"
		strResult = rsInfo.Get_Fields_String("EPaymentClientID") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPassword") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGateway") + "|";
		// Alias
	}
	else
	{
		string vbPorterVar2 = arr[intIndex].Bank;
		if ((vbPorterVar2 == "Discover") || (vbPorterVar2 == "MasterCard"))
		{
			// MsgBox "Authorizing to MasterCard / Discover Account"
			strResult = rsInfo.Get_Fields_String("EPaymentClientIDMCDiscover") + "|";
			// Merchant ID
			strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordMCDiscover") + "|";
			// Password
			strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayMCDiscover") + "|";
			// Alias
		}
		else if (vbPorterVar2 == "American Express")
		{
			// MsgBox "Authorizing to AmEx Account"
			strResult = rsInfo.Get_Fields_String("EPaymentClientIDAmEx") + "|";
			// Merchant ID
			strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordAmEx") + "|";
			// Password
			strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayAmEx") + "|";
			// Alias
		}
	}
}
strResult += strTransID + "|";
// Order ID
strResult += "Payment|";
// Instruction Type
strResult += "CreditCard|";
// Payment Mechanism
strResult += strExpDate + "|";
// Expiration Date in the format mm/yy
strResult += strCryptCard + "|";
// CryptCard
strResult += "|";
// First Name
strResult += "|";
// Last Name
strResult += "|";
// Address 1
strResult += "|";
// Address 2
strResult += "|";
// City
strResult += "|";
// State
strResult += strZipCode + "|";
// Zip
strResult += "Auth|";
// Transaciton Type
if (blnConvenienceFee)
{
	if (modGlobal.Statics.gblnNoVISAPayments)
	{
		strResult += FCConvert.ToString(arr[intIndex].ConvenienceFee * 100) + "|";
		// Transaction Amount
		// MsgBox "Amount " & arr(intindex).ConvenienceFee
	}
	else
	{
		if (blnTaxTransaction)
		{
			strResult += FCConvert.ToString((arr[intIndex].ConvenienceFee - arr[intIndex].NonTaxConvenienceFee) * 100) + "|";
			// Transaction Amount
			// MsgBox "Amount " & arr(intindex).ConvenienceFee - arr(intindex).NonTaxConvenienceFee
		}
		else
		{
			strResult += FCConvert.ToString(arr[intIndex].NonTaxConvenienceFee * 100) + "|";
			// Transaction Amount
			// MsgBox "Amount " & arr(intindex).NonTaxConvenienceFee
		}
	}
}
else if (blnVISANonTax)
{
	strResult += FCConvert.ToString(arr[intIndex].NonTaxAmount * 100) + "|";
	// Transaction Amount
	// MsgBox "Amount " & arr(intindex).NonTaxAmount
}
else
{
	if (!modGlobal.Statics.gblnNoVISAPayments)
	{
		strResult += FCConvert.ToString((FCConvert.ToDecimal(arr[intIndex].Amount) - arr[intIndex].ConvenienceFee - arr[intIndex].NonTaxAmount) * 100) + "|";
		// Transaction Amount
		// MsgBox "Amount " & arr(intindex).Amount - arr(intindex).ConvenienceFee - arr(intindex).NonTaxAmount
	}
	else
	{
		strResult += FCConvert.ToString((FCConvert.ToDecimal(arr[intIndex].Amount) - arr[intIndex].ConvenienceFee) * 100) + "|";
		// Transaction Amount
		// MsgBox "Amount " & arr(intindex).Amount - arr(intindex).ConvenienceFee
	}
}
if (blnConvenienceFee)
{
	if (blnTaxTransaction)
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserIDConvenienceFee") + "|" + "";
		// CryptPAY Domain
	}
	else
	{
		string vbPorterVar3 = arr[intIndex].Bank;
		if ((vbPorterVar3 == "VISA") || (vbPorterVar3 == "VISA Debit"))
		{
			strResult += rsInfo.Get_Fields_String("EPaymentUserIDConvenienceFee") + "|" + "";
			// CryptPAY Domain
		}
		else if ((vbPorterVar3 == "Discover") || (vbPorterVar3 == "MasterCard"))
		{
			strResult += rsInfo.Get_Fields_String("EPaymentUserIDMCDiscoverConvenienceFee") + "|" + "";
			// Alias
		}
		else if (vbPorterVar3 == "American Express")
		{
			strResult += rsInfo.Get_Fields_String("EPaymentUserIDAmExConvenienceFee") + "|" + "";
			// Alias
		}
	}
}
else if (blnVISANonTax)
{
	string vbPorterVar4 = arr[intIndex].Bank;
	if ((vbPorterVar4 == "VISA") || (vbPorterVar4 == "VISA Debit"))
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserIDVISA") + "|" + "";
		// Alias
	}
	else if ((vbPorterVar4 == "Discover") || (vbPorterVar4 == "MasterCard"))
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserIDMCDiscover") + "|" + "";
		// Alias
	}
	else if (vbPorterVar4 == "American Express")
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserIDAmEx") + "|" + "";
		// Alias
	}
}
else
{
	if (!modGlobal.Statics.gblnNoVISAPayments)
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserID") + "|" + "";
		// Alias
	}
	else
	{
		string vbPorterVar5 = arr[intIndex].Bank;
		if ((vbPorterVar5 == "Discover") || (vbPorterVar5 == "MasterCard"))
		{
			strResult += rsInfo.Get_Fields_String("EPaymentUserIDMCDiscover") + "|" + "";
			// Alias
		}
		else if (vbPorterVar5 == "American Express")
		{
			strResult += rsInfo.Get_Fields_String("EPaymentUserIDAmEx") + "|" + "";
			// Alias
		}
	}
}
if (modGlobal.Statics.gblnEPymtTestMode)
{
	strResult += "Y|";
	// Test
}
else
{
	strResult += "|";
}
strResult += arr[intIndex].Bank;
string vbPorterVar6 = arr[intIndex].Bank;
if ((vbPorterVar6 == "VISA") || (vbPorterVar6 == "VISA Debit"))
{
	strResult += "|Y";
}
else
{
	strResult += "|";
}
}
else if (modGlobal.Statics.gstrEPaymentPortal == "I")
{
// kgk 09-29-2011  Add Invoice Cloud
strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
// Biller GUID
strResult += "|" + strTransID;
// Order ID
strResult += "|" + strCryptCard;
// Secure GUID
strResult += "|" + arr[intIndex].OwnerName;
// Owner Name
strResult += "|" + arr[intIndex].OwnerStreet;
// Owner Street
strResult += "|" + arr[intIndex].OwnerCity;
// Owner City
strResult += "|" + arr[intIndex].OwnerState;
// Owner State
strResult += "|" + arr[intIndex].OwnerZip;
// Owner Zip
strResult += "|Sale";
// Trans Type?
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount);
// Total - Including Conv Fee if any
strResult += "|" + FCConvert.ToString(arr[intIndex].ConvenienceFee);
// Convenience Fee
strResult += "|";
// Sales Tax?
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult += "|Y";
}
else
{
	strResult += "|";
}
strResult += "|";
// Form Label
}
else if (modGlobal.Statics.gstrEPaymentPortal == "E")
{
// kgk 01-27-2012  Add InforME
// strResult = rsInfo.Fields("EPaymentClientID")                           'Merchant Code
strResult = arr[intIndex].EPmtAcctID;
// Service Code   '03122012
strResult += "|" + strTransID;
// Order ID
strResult += "|" + "";
// No CryptCard
strResult += "|" + arr[intIndex].OwnerName;
// Owner Name
strResult += "|" + arr[intIndex].OwnerStreet;
// Owner Street
strResult += "|" + arr[intIndex].OwnerCity;
// Owner City
strResult += "|" + arr[intIndex].OwnerState;
// Owner State
strResult += "|" + arr[intIndex].OwnerZip;
// Owner Zip
strResult += "|Sale";
// Trans Type?
strResult += "|" + FCConvert.ToString(FCConvert.ToDecimal(arr[intIndex].Amount) - arr[intIndex].ConvenienceFee);
// Total - Not ncluding Conv Fee if any
strResult += "|" + FCConvert.ToString(arr[intIndex].ConvenienceFee);
// Convenience Fee
strResult += "|";
// Sales Tax?
// kgk 08012012 trocr-350  Add demo mode
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentDemoMode")))
{
	// Demo Mode
	strResult += "|D";
}
else if (rsInfo.Get_Fields_Boolean("EPaymentTestMode"))
{
	// Test Mode
	strResult += "|Y";
}
else
{
	strResult += "|";
}
// Form Label
clsDRWrapper rsEPay = new clsDRWrapper();
rsEPay.OpenRecordset("SELECT AcctID, AcctDescription FROM EPmtAccounts WHERE AcctID = '" + arr[intIndex].EPmtAcctID + "'", modExtraModules.strCRDatabase);
if (rsEPay.RecordCount() > 0)
{
	strResult += "|" + GetCreditCardName(FCConvert.ToInt32(arr[intIndex].CType)) + " :: $" + Strings.Format(arr[intIndex].Amount, "0.00") + " :: " + rsEPay.Get_Fields_String("AcctDescription");
}
else
{
	strResult += "|" + GetCreditCardName(FCConvert.ToInt32(arr[intIndex].CType)) + " :: $" + Strings.Format(arr[intIndex].Amount, "0.00");
}
}
rsTrans.Reset();
rsInfo.Reset();
BuildECCString = strResult;
return BuildECCString;
}

private string BuildECCString_Credit_6(int intIndex, bool blnVISANonTax = false)
{
return BuildECCString_Credit(intIndex, blnVISANonTax);
}

private string BuildECCString_Credit(int intIndex, bool blnVISANonTax = false)
{
string BuildECCString_Credit = "";
int lngPymtID;
string strTransID = "";
string strCC = "";
string strResult = "";
clsDRWrapper rsInfo = new clsDRWrapper();
clsDRWrapper rsTrans = new clsDRWrapper();
rsTrans.OpenRecordset("SELECT MAX(ReceiptKey) AS LastKey FROM Receipt", modExtraModules.strCRDatabase);
if (rsTrans.RecordCount() > 0)
{
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
if (fecherFoundation.FCUtils.IsNull(rsTrans.Get_Fields("LastKey")) || FCConvert.ToString(rsTrans.Get_Fields("LastKey")) == "")
{
	strTransID = Strings.Format(DateTime.Now, "mmddyyhhmmss") + "1";
}
else
{
	// TODO Get_Fields: Field [LastKey] not found!! (maybe it is an alias?)
	strTransID = Strings.Format(DateTime.Now, "mmddyyhhmmss") + FCConvert.ToString(FCConvert.ToInt32(rsTrans.Get_Fields("LastKey")) + 1);
}
}
rsInfo.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
if (modGlobal.Statics.gstrEPaymentPortal == "A")
{
strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
// API Login ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// Transaction ID
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult += "|" + "Y";
}
else
{
	strResult += "|" + "N";
}
strResult += "|" + "N";
// Is E-Check
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Amount
strResult += "|" + arr[intIndex].CNum;
// Card Number
strResult += "|" + "";
// Exp Date - N/A
strResult += "|" + "CREDIT";
// Transaction Type
strResult += "|" + "";
// Bank Name - N/A
strResult += "|" + "";
// Name of Acct Holder - N/A
strResult += "|" + "";
// Echeck Transaction Type - N/A
strResult += "|" + "";
// Echeck Type - N/A
strResult += "|" + arr[intIndex].RefNumber;
// Original Transaction ID
}
else if (modGlobal.Statics.gstrEPaymentPortal == "C")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Client ID - Different if Testing
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentTestKey"));
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
}
strResult += "|" + arr[intIndex].RefNumber;
// PRC
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Amount
strResult += "|" + "0";
// Internal Credit
strResult += "|" + "0";
// Chargeback
strResult += "|" + "";
// Notes
strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
// CSI Org Unit ID
strResult += "|" + rsInfo.Get_Fields_String("EPaymentUserID");
// CSI User ID
}
else if (modGlobal.Statics.gstrEPaymentPortal == "G")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult = "1264";
	// Merchant ID
	strResult += "|" + "password";
	// Password
	strResult += "|" + "";
	// Gateway ID
	strResult += "|" + "Y";
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
	// Merchant ID
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
	// Password
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientGateway");
	// Gateway ID
	strResult += "|" + "N";
}
strResult += "|" + "credit";
// Operation Type
strResult += "|" + "1";
// Total Number of Trans
strResult += "|" + arr[intIndex].RefNumber;
// Original Trans Ref Number
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Credit Amount
}
else if (modGlobal.Statics.gstrEPaymentPortal == "M")
{
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	strResult = "monusqa025";
	strResult += "|" + "qatoken";
}
else
{
	strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
	// StoreID
	strResult += "|" + rsInfo.Get_Fields_String("EPaymentClientPassword");
	// APIToken
}
strResult += "|" + "N";
// Is E-Check
strResult += "|" + Strings.Left(arr[intIndex].RefNumber, Strings.InStr(1, "|", arr[intIndex].RefNumber, CompareConstants.vbTextCompare) - 1);
// Order ID
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Amount
strResult += "|" + Strings.Right(arr[intIndex].RefNumber, arr[intIndex].RefNumber.Length - Strings.InStr(1, "|", arr[intIndex].RefNumber, CompareConstants.vbTextCompare));
// Transaction Number
strResult += "|" + "7";
// Crypt Type - SSL Enabled Merchant
strResult += "|" + "";
// Customer ID - Optional
strResult += "|" + "";
// Routing Num - N/A
strResult += "|" + "";
// AccountNum - N/A
strResult += "|" + "";
// Account Type - N/A
strResult += "|" + "";
// CheckNum - N/A
strResult += "|" + "N";
// Independent Refund - Not Supported by TRIO
strResult += "|" + "";
// Ind Refund CC Numbers
strResult += "|" + "";
// Ind Refund Exp Date
if (blnIsSwipe)
{
	strResult += "|" + "Y";
	// Is Swiped Transaction
	strResult += "|" + Strings.Right(txtTrack2.Text, txtTrack2.Text.Length - 1);
	// Track 2 Data for Swiped Transactions
}
else
{
	strResult += "|" + "N";
	// Is Swiped Transaction
	strResult += "|" + "";
	// Track 2 Data for Swiped Transactions
}
strResult += "|" + "00";
// Track 2 Data for Swiped Transactions
strResult += "|" + "00";
// POS Code - Used for Swiped but Should Be '00' for all CC Transactions
}
else if (modGlobal.Statics.gstrEPaymentPortal == "R")
{
strResult = "HTTPS://tf.lynk-systems.com/LE0100082Pmt";
// Lynk Host Name
strResult += "," + "SvcType=Sale";
// Service Type
strResult += "&" + "StoreId=" + rsInfo.Get_Fields_String("EPaymentClientID");
// Store ID
strResult += "&" + "MerchantId=" + rsInfo.Get_Fields_String("EPaymentClientPassword");
// Merchant ID
strResult += "&" + "TerminalId=" + rsInfo.Get_Fields_String("EPaymentClientGateway");
// Terminal ID
strResult += "&" + "CardNumber=" + arr[intIndex].CNum;
// Card Number
strResult += "&" + "ExpirationDate=" + Strings.Format(arr[intIndex].ExpDate, "MM/YY");
// Expiration Date
strResult += "&" + "EntryMode=" + "0";
// Entry Mode
strResult += "&" + "Signature=" + "1";
// Signature
strResult += "&" + "Amount=" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Amount
}
else if (modGlobal.Statics.gstrEPaymentPortal == "P")
{
if (blnVISANonTax)
{
	string vbPorterVar = arr[intIndex].Bank;
	if ((vbPorterVar == "VISA") || (vbPorterVar == "VISA Debit"))
	{
		strResult = rsInfo.Get_Fields_String("EPaymentClientIDVISA") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordVISA") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayVISA") + "|";
		// Alias
	}
	else if ((vbPorterVar == "Discover") || (vbPorterVar == "MasterCard"))
	{
		strResult = rsInfo.Get_Fields_String("EPaymentClientIDMCDiscover") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordMCDiscover") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayMCDiscover") + "|";
		// Alias
	}
	else if (vbPorterVar == "American Express")
	{
		strResult = rsInfo.Get_Fields_String("EPaymentClientIDAmEx") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordAmEx") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayAmEx") + "|";
		// Alias
	}
}
else
{
	if (!modGlobal.Statics.gblnNoVISAPayments)
	{
		strResult = rsInfo.Get_Fields_String("EPaymentClientID") + "|";
		// Merchant ID
		strResult += rsInfo.Get_Fields_String("EPaymentClientPassword") + "|";
		// Password
		strResult += rsInfo.Get_Fields_String("EPaymentClientGateway") + "|";
		// Alias
	}
	else
	{
		string vbPorterVar1 = arr[intIndex].Bank;
		if ((vbPorterVar1 == "Discover") || (vbPorterVar1 == "MasterCard"))
		{
			strResult = rsInfo.Get_Fields_String("EPaymentClientIDMCDiscover") + "|";
			// Merchant ID
			strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordMCDiscover") + "|";
			// Password
			strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayMCDiscover") + "|";
			// Alias
		}
		else if (vbPorterVar1 == "American Express")
		{
			strResult = rsInfo.Get_Fields_String("EPaymentClientIDAmEx") + "|";
			// Merchant ID
			strResult += rsInfo.Get_Fields_String("EPaymentClientPasswordAmEx") + "|";
			// Password
			strResult += rsInfo.Get_Fields_String("EPaymentClientGatewayAmEx") + "|";
			// Alias
		}
	}
}
strResult += strTransID + "|";
// Order ID
strResult += "Payment|";
// Instruction Type
strResult += "CreditCard|";
// Payment Mechanism
strResult += arr[intIndex].ExpDate + "|";
// Expiration Date in the format mm/yy
strResult += arr[intIndex].CryptCard + "|";
// CryptCard
strResult += "Credit|";
// Transaciton Type
if (blnVISANonTax)
{
	strResult += FCConvert.ToString(arr[intIndex].NonTaxAmount * -100) + "|";
	// Transaction Amount
}
else
{
	if (!modGlobal.Statics.gblnNoVISAPayments)
	{
		strResult += FCConvert.ToString((FCConvert.ToDecimal(arr[intIndex].Amount) - arr[intIndex].ConvenienceFee - arr[intIndex].NonTaxAmount) * -100) + "|";
		// Transaction Amount
	}
	else
	{
		strResult += FCConvert.ToString((FCConvert.ToDecimal(arr[intIndex].Amount) - arr[intIndex].ConvenienceFee) * -100) + "|";
		// Transaction Amount
	}
}
if (blnVISANonTax)
{
	string vbPorterVar2 = arr[intIndex].Bank;
	if ((vbPorterVar2 == "VISA") || (vbPorterVar2 == "VISA Debit"))
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserIDVISA") + "|" + "";
		// Alias
	}
	else if ((vbPorterVar2 == "Discover") || (vbPorterVar2 == "MasterCard"))
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserIDMCDiscover") + "|" + "";
		// Alias
	}
	else if (vbPorterVar2 == "American Express")
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserIDAmEx") + "|" + "";
		// Alias
	}
}
else
{
	if (!modGlobal.Statics.gblnNoVISAPayments)
	{
		strResult += rsInfo.Get_Fields_String("EPaymentUserID") + "|" + "";
		// Alias
	}
	else
	{
		string vbPorterVar3 = arr[intIndex].Bank;
		if ((vbPorterVar3 == "Discover") || (vbPorterVar3 == "MasterCard"))
		{
			strResult += rsInfo.Get_Fields_String("EPaymentUserIDMCDiscover") + "|" + "";
			// Alias
		}
		else if (vbPorterVar3 == "American Express")
		{
			strResult += rsInfo.Get_Fields_String("EPaymentUserIDAmEx") + "|" + "";
			// Alias
		}
	}
}
if (modGlobal.Statics.gblnEPymtTestMode)
{
	strResult += "Y|";
	// Test
}
else
{
	strResult += "|";
}
strResult += arr[intIndex].Bank;
}
else if (modGlobal.Statics.gstrEPaymentPortal == "I")
{
// kgk 09-29-2011 Add Invoice Cloud
strResult = FCConvert.ToString(rsInfo.Get_Fields_String("EPaymentClientID"));
// Biller GUID
strResult += "|" + strTransID;
// Order ID
strResult += "|" + arr[intIndex].CryptCard;
// Secure GUID
// strResult = strResult & "|"                                             'Trans Type?
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Total
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentTestMode")))
{
	// Test Mode
	strResult += "|Y";
}
else
{
	strResult += "|";
}
strResult += "|";
// Form Label
}
else if (modGlobal.Statics.gstrEPaymentPortal == "E")
{
// kgk 01-27-2012 Add InforME
// strResult = rsInfo.Fields("EPaymentClientID")                           'Merchant Code
strResult = arr[intIndex].EPmtAcctID;
// AcctID/Service Code   '03122012
strResult += "|" + arr[intIndex].RefNumber;
// Order ID
strResult += "|" + "";
// No CryptCard
// strResult = strResult & "|"                                             'Trans Type?
strResult += "|" + FCConvert.ToString(arr[intIndex].Amount * -1);
// Total
// kgk 08012012 trocr-350  Add demo mode
if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("EPaymentDemoMode")))
{
	// Demo Mode
	strResult += "|D";
}
else if (rsInfo.Get_Fields_Boolean("EPaymentTestMode"))
{
	// Test Mode
	strResult += "|Y";
}
else
{
	strResult += "|";
}
strResult += "|";
// Form Label
}
rsTrans.Reset();
rsInfo.Reset();
BuildECCString_Credit = strResult;
return BuildECCString_Credit;
}

private bool CCAddressValid()
{
bool CCAddressValid = false;
bool blnResult;
blnResult = true;
// Optimistic
blnResult = !FCConvert.CBool(modGlobal.Statics.gstrCCOwnerName == "");
if (blnResult)
{
blnResult = !FCConvert.CBool(modGlobal.Statics.gstrCCOwnerStreet == "");
if (blnResult)
{
	blnResult = !FCConvert.CBool(modGlobal.Statics.gstrCCOwnerCity == "");
	if (blnResult)
	{
		blnResult = !FCConvert.CBool(modGlobal.Statics.gstrCCOwnerState == "");
		if (blnResult)
		{
			blnResult = !FCConvert.CBool(modGlobal.Statics.gstrCCOwnerZip == "");
			if (blnResult)
			{
				blnResult = !FCConvert.CBool(modGlobal.Statics.gstrCCOwnerCountry == "");
			}
		}
	}
}
}
CCAddressValid = blnResult;
return CCAddressValid;
}

private void GetBankDetails(int lngBankID, ref string strBankName, ref string strRouteNum)
{
// Tracker Reference: 16641
clsDRWrapper rsBank = new clsDRWrapper();
rsBank.OpenRecordset("SELECT * FROM Bank WHERE ID = " + FCConvert.ToString(lngBankID), modExtraModules.strCRDatabase);
if (rsBank.RecordCount() > 0)
{
strBankName = FCConvert.ToString(rsBank.Get_Fields_String("Name"));
strRouteNum = FCConvert.ToString(rsBank.Get_Fields_String("RoutingTransit"));
}
}
// vbPorter upgrade warning: lngCCID As int	OnWrite(string)
private string GetCreditCardName(int lngCCID)
{
string GetCreditCardName = "";
// Tracker Reference: 16641
clsDRWrapper rsTrans = new clsDRWrapper();
string strCC = "";
rsTrans.OpenRecordset("SELECT * FROM CCType WHERE ID = " + FCConvert.ToString(lngCCID), modExtraModules.strCRDatabase);
if (rsTrans.RecordCount() > 0)
{
// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
strCC = FCConvert.ToString(rsTrans.Get_Fields("Type"));
}
GetCreditCardName = strCC;
return GetCreditCardName;
}

private void LoadMultiAcctGrid(bool blnIsCheck, clsDRWrapper rsAccount)
{
// Tracker Reference: 16641
int Width;
int intRow = 0;
string strBankName = "";
string strRouteNum = "";
vsMultiAccts.Clear();
// MAL@20090202 ; Tracker Reference: 16641
vsMultiAccts.Cols = 5;
Width = vsMultiAccts.WidthOriginal;
vsMultiAccts.ColWidth(0, FCConvert.ToInt32(vsMultiAccts.WidthOriginal * 0.15));
// Select Column
vsMultiAccts.ColWidth(1, FCConvert.ToInt32(vsMultiAccts.WidthOriginal * 0.41));
// Bank Name / Card Type
vsMultiAccts.ColWidth(2, FCConvert.ToInt32(vsMultiAccts.WidthOriginal * 0.4));
// Last 4 Digits of Account
vsMultiAccts.ColWidth(3, 0);
// Hidden Field --> Record ID
vsMultiAccts.ColWidth(4, 0);
// Hidden Field - IsCheck
vsMultiAccts.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
vsMultiAccts.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
// sets col 0 checkboxes
vsMultiAccts.ExtendLastCol = true;
// sets the titles for the flexgrid
vsMultiAccts.TextMatrix(0, 0, "Select");
if (blnIsCheck)
{
vsMultiAccts.TextMatrix(0, 1, "Bank Name");
}
else
{
vsMultiAccts.TextMatrix(0, 1, "Card Type");
}
vsMultiAccts.TextMatrix(0, 2, "Account Num");
vsMultiAccts.TextMatrix(0, 3, "");
rsAccount.MoveFirst();
intRow = 1;
while (!rsAccount.EndOfFile())
{
if (blnIsCheck)
{
	// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
	GetBankDetails(rsAccount.Get_Fields("BankNumber"), ref strBankName, ref strRouteNum);
	vsMultiAccts.AddItem("\t" + strBankName + "\t" + rsAccount.Get_Fields_String("Last4Digits") + "\t" + rsAccount.Get_Fields_Int32("ID") + "\t" + "Y", intRow);
}
else
{
	vsMultiAccts.AddItem("\t" + GetCreditCardName(rsAccount.Get_Fields_Int32("CCType")) + "\t" + rsAccount.Get_Fields_String("Last4Digits") + "\t" + rsAccount.Get_Fields_Int32("ID") + "\t" + "N", intRow);
}
intRow += 1;
rsAccount.MoveNext();
}
}

private void AddCreditAmounttoArray(clsDRWrapper rsDetail, int lngRecordKey, bool blnIsCheck)
{
// Tracker Reference: 16641
int intIndex;
string strBank = "";
string strRouteNum = "";
for (intIndex = 1; intIndex <= modStatusPayments.MAX_PAYMENTS; intIndex++)
{
if (arr[intIndex].Amount == 0)
{
	break;
}
}
// intIndex
if (blnIsCheck)
{
if (blnIsVoid)
{
	// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
	arr[intIndex].Amount = rsDetail.Get_Fields("Amount") * -1;
}
else
{
	arr[intIndex].Amount = FCConvert.ToDouble(txtPayment.Text);
}
arr[intIndex].Type = 1;
arr[intIndex].RefNumber = rsDetail.Get_Fields_String("EPymtRefNumber");
// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
GetBankDetails(rsDetail.Get_Fields("BankNumber"), ref strBank, ref strRouteNum);
arr[intIndex].Bank = strBank;
arr[intIndex].ChkAcctNum = rsDetail.Get_Fields_String("Last4Digits");
// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
arr[intIndex].CType = rsDetail.Get_Fields("CheckNumber");
arr[intIndex].ChkAcctType = rsDetail.Get_Fields_String("AccountType");
arr[intIndex].OrigReceipt = GetReceiptKey(lngReceiptNum);
arr[intIndex].IsVISA = false;
arr[intIndex].IsDebit = false;
}
else
{
if (blnIsVoid)
{
	// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
	arr[intIndex].Amount = FCConvert.ToDouble((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("ConvenienceFee")) * -1);
}
else
{
	arr[intIndex].Amount = FCConvert.ToDouble(txtPayment.Text);
}
arr[intIndex].Type = 2;
arr[intIndex].CNum = rsDetail.Get_Fields_String("MaskedCreditCardNumber");
arr[intIndex].RefNumber = rsDetail.Get_Fields_String("EPymtRefNumber");
arr[intIndex].CType = rsDetail.Get_Fields_Int32("CCType").ToString();
arr[intIndex].Bank = GetCreditCardName(rsDetail.Get_Fields_Int32("CCType"));
arr[intIndex].OrigReceipt = GetReceiptKey(lngReceiptNum);
if (modGlobal.Statics.gstrEPaymentPortal != "I")
{
	arr[intIndex].CryptCard = rsDetail.Get_Fields_String("CryptCard");
}
else
{
	arr[intIndex].CryptCard = rsDetail.Get_Fields_String("SecureGUID");
}
arr[intIndex].AuthCode = rsDetail.Get_Fields_String("AuthCode");
arr[intIndex].ExpDate = rsDetail.Get_Fields_String("CryptCardExpiration");
arr[intIndex].ConvenienceFee = 0;
// rsDetail.Fields("ConvenienceFee")
arr[intIndex].IsVISA = rsDetail.Get_Fields_Boolean("IsVISA");
arr[intIndex].IsDebit = rsDetail.Get_Fields_Boolean("IsDebit");
arr[intIndex].NonTaxAmount = rsDetail.Get_Fields_Decimal("NonTaxAmount");
arr[intIndex].AuthCodeConvenienceFee = "";
// rsDetail.Fields("AuthCodeConvenienceFee")
arr[intIndex].AuthCodeVISANonTax = rsDetail.Get_Fields_String("AuthCodeVISANonTax");
arr[intIndex].NonTaxConvenienceFee = 0;
arr[intIndex].AuthCodeNonTaxConvenienceFee = "";
arr[intIndex].EPmtAcctID = rsDetail.Get_Fields_String("EPmtAcctID");
}
AddPaymentToGrid(ref intIndex);
}

private string GetReceiptKey(int lngRecNum)
{
string GetReceiptKey = "";
string strResult = "";
clsDRWrapper rsRec = new clsDRWrapper();
rsRec.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptNumber = " + FCConvert.ToString(lngRecNum), modExtraModules.strCRDatabase);
if (rsRec.RecordCount() > 0)
{
strResult = FCConvert.ToString(rsRec.Get_Fields_Int32("ID"));
}
else
{
strResult = "";
}
GetReceiptKey = strResult;
return GetReceiptKey;
}

private void ProcessSwipedCard()
{
// Tracker Reference: 16641
string strCCNum;
string strMonth;
string strDay;
string strYear;
string strTrack;
int intStart;
int intEnd = 0;
int counter;
string strExpDt;
// Get Track 2 Data
intStart = Strings.InStr(1, txtTrack2.Text, ";", CompareConstants.vbTextCompare);
if (Strings.InStr(1, txtTrack2.Text, "+", CompareConstants.vbTextCompare) > 0)
{
intEnd = Strings.InStr(intStart, txtTrack2.Text, "+", CompareConstants.vbTextCompare);
}
else
{
intEnd = Strings.InStr(intStart, txtTrack2.Text, "?", CompareConstants.vbTextCompare);
}
strTrack = Strings.Mid(txtTrack2.Text, intStart, intEnd - intStart);
// Break Out CC Num
intStart = 1;
intEnd = Strings.InStr(intStart, strTrack, "=", CompareConstants.vbTextCompare);
strCCNum = Strings.Mid(strTrack, intStart + 1, (intEnd - intStart) - 1);
// Break Out Exp Date
intStart = Strings.InStr(1, strTrack, "=", CompareConstants.vbTextCompare);
intEnd = intStart + 4;
strExpDt = Strings.Mid(strTrack, intStart + 1, (intEnd - intStart));
// Get Full Expiration Date
strYear = Strings.Left(strExpDt, 2);
strMonth = Strings.Right(strExpDt, 2);
strDay = GetEndofMonth(strMonth);
// Add Text to Fields
txtCCNum.Text = strCCNum;
cboExpMonth.SelectedIndex = FCConvert.ToInt32(Conversion.Val(strMonth) - 1);
for (counter = 0; counter <= cboExpYear.Items.Count - 1; counter++)
{
if (cboExpYear.Items[counter].ToString() == strYear)
{
	cboExpYear.SelectedIndex = counter;
}
}
txtCCNum.Enabled = false;
cboExpMonth.Enabled = false;
cboExpYear.Enabled = false;
txtCVV2.Focus();
}

private string GetEndofMonth(string strMonth)
{
string GetEndofMonth = "";
string strResult = "";
if ((strMonth == "01") || (strMonth == "03") || (strMonth == "05") || (strMonth == "07") || (strMonth == "08") || (strMonth == "10") || (strMonth == "12"))
{
strResult = "31";
}
else if ((strMonth == "04") || (strMonth == "06") || (strMonth == "09") || (strMonth == "11"))
{
strResult = "30";
}
else if (strMonth == "02")
{
strResult = "28";
}
else
{
strResult = "31";
}
GetEndofMonth = strResult;
return GetEndofMonth;
}
// vbPorter upgrade warning: 'Return' As bool	OnWriteFCConvert.ToInt32(
private bool ValidateARAccounts()
{
bool ValidateARAccounts = false;
// kgk troar-25 09-19-2011  Need to check that if alt cash is used that the
// interest and tax accounts used are from the same fund as the cash
clsDRWrapper rsType = new clsDRWrapper();
int lngFundInt = 0;
int lngFundTax = 0;
int lngFundAltCash = 0;
// vbPorter upgrade warning: lngRetVal As int	OnWrite(bool)
int lngRetVal;
// First get the default Int and Tax accounts from the type 97 setup
rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 97", "twcr0000.vb1");
if (!rsType.EndOfFile() && !rsType.BeginningOfFile())
{
// Default Interest account
if (FCConvert.ToString(rsType.Get_Fields_String("Account2")) != "")
{
	lngFundInt = modBudgetaryAccounting.GetFundFromAccount(rsType.Get_Fields_String("Account2"));
}
else
{
	lngFundInt = 0;
}
// Default Sales Tax account
if (FCConvert.ToString(rsType.Get_Fields_String("Account3")) != "")
{
	lngFundTax = modBudgetaryAccounting.GetFundFromAccount(rsType.Get_Fields_String("Account3"));
}
else
{
	lngFundTax = 0;
}
}
// Check for AR Bill Types with Alternate Cash account without Override Interest and Tax accounts
// Make sure that the Alt Cash account and the Default Interest and Tax accounts are the same fund
lngRetVal = (true ? -1 : 0);
rsType.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE DefaultAccount <> ''", "TWAR0000.vb1");
while (!rsType.EndOfFile())
{
if (FCConvert.ToString(rsType.Get_Fields_String("OverrideIntAccount")) == "" || FCConvert.ToString(rsType.Get_Fields_String("OverrideTaxAccount")) == "")
{
	lngFundAltCash = modBudgetaryAccounting.GetFundFromAccount(rsType.Get_Fields_String("DefaultAccount"));
	if (lngFundAltCash != lngFundInt || lngFundAltCash != lngFundTax)
	{
		MessageBox.Show("Error in AR Bill Type " + rsType.Get_Fields_Int32("TypeCode") + ": All of the accounts in categories 1 through 6 must be from the same fund as the Receivable, Alternate Cash, Interest and Sales Tax Accounts.");
		lngRetVal = (false ? -1 : 0);
	}
}
rsType.MoveNext();
}
ValidateARAccounts = FCConvert.ToBoolean(lngRetVal);
return ValidateARAccounts;
}
// vbPorter upgrade warning: strEPmtAcctID As object	OnWrite(string)	OnRead(string)
// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(Decimal, short)
private Decimal RetrievePortalFee_2(Decimal curPmtAmt, object strEPmtAcctID)
{
return RetrievePortalFee(ref curPmtAmt, ref strEPmtAcctID);
}

private Decimal RetrievePortalFee(ref Decimal curPmtAmt, ref object strEPmtAcctID)
{
Decimal RetrievePortalFee = 0;
clsDRWrapper rsCR = new clsDRWrapper();
bool blnResult;
string strError = "";
string strFile;
// Full Path to Pass/Result File
string strParam;
string strPass;
string strExePath;
string strResult = "";
object objExe;
string strLogFile;
string strLogData = "";
/* XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX THIS HAS TO BE REPLACED XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
try
{
// On Error GoTo ErrorHandler
// Make sure curPmtAmt is not zero?
rsCR.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
// Build File
strFile = modRegistry.GetRegistryKey("EPymtFilePath", "CR");
if (strFile == "")
{
// Set default path
strFile = FCFileSystem.Statics.UserDataFolder + "\\";
}
else if (Strings.Right(strFile, 1) != "\\")
{
strFile += "\\";
}
strFile += "EPaymentData.txt";
// Build string for retrieving the portal charge
strPass = FCConvert.ToString(strEPmtAcctID);
// Service Code
strPass += "|" + "";
// Order ID
strPass += "||||||";
// No CryptCard, Name, Street, City, State, Zip
strPass += "|Fee";
// Trans Type  (Do we need one?)
strPass += "|" + Strings.Format(curPmtAmt, "0.00");
// Total Not Including Conv Fee
strPass += "|" + "";
// Convenience Fee
strPass += "|";
// Sales Tax?
// kgk 08012012 trocr-350  Add demo mode
if (FCConvert.ToBoolean(rsCR.Get_Fields_Boolean("EPaymentDemoMode")))
{
// Demo Mode
strPass += "|D";
}
else if (rsCR.Get_Fields_Boolean("EPaymentTestMode"))
{
// Test Mode
strPass += "|Y";
}
else
{
strPass += "|";
}
strPass += "|";
// Form Label
FCFileSystem.FileOpen(1, strFile, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
FCFileSystem.PrintLine(1, strPass);
FCFileSystem.FileClose(1);
// This only applies to InforME at this time
strParam = ",IME,X";
// Call Executable
objExe = Interaction.CreateObject("WScript.Shell");
strExePath = Application.StartupPath;
if (Strings.Right(strExePath, 1) != "\\")
{
strExePath += "\\";
}
//FC:TODO:AM
//objExe.Run(strExePath + "HarrisEPayments.exe " + strFile + strParam, AppWinStyle.NormalFocus, 1);
objExe = null;
// Get Result String
FCFileSystem.FileOpen(1, strFile, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
strResult = FCFileSystem.LineInput(1);
// Build File
strLogFile = modRegistry.GetRegistryKey("EPymtFilePath", "CR");
if (strLogFile == "")
{
// Set default path
strLogFile = FCFileSystem.Statics.UserDataFolder + "\\";
}
else if (Strings.Right(strLogFile, 1) != "\\")
{
strLogFile += "\\";
}
strLogFile += "CCLog.txt";
FCFileSystem.FileOpen(2, strLogFile, OpenMode.Append, (OpenAccess)(-1), (OpenShare)(-1), -1);
FCFileSystem.PrintLine(2, fecherFoundation.Strings.StrP(DateTime.Now));
FCFileSystem.PrintLine(2, "Passed In: " + strPass);
FCFileSystem.PrintLine(2, "Returned:  " + strResult);
while (!FCFileSystem.EOF(1))
{
strLogData = FCFileSystem.LineInput(1);
FCFileSystem.PrintLine(2, strLogData);
}
FCFileSystem.PrintLine(2, "");
FCFileSystem.PrintLine(2, "");
FCFileSystem.PrintLine(2, "");
FCFileSystem.FileClose(2);
FCFileSystem.FileClose(1);
// Parse Result
strResult = Strings.Replace(strResult, "\"", "", 1, -1, CompareConstants.vbTextCompare);
strData = Strings.Split(strResult, "|", -1, CompareConstants.vbTextCompare);
blnResult = FCConvert.CBool(FCConvert.ToDouble(strData[0]) == 1);
if (blnResult)
{
RetrievePortalFee = FCConvert.ToDecimal(strData[14]);
}
else
{
strError = strData[1] + "\r\n" + strData[2];
// Display Error Message
MessageBox.Show(strError, "Error ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
}
return RetrievePortalFee;
}
catch (Exception ex)
{
// ErrorHandler:
MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + Information.Err(ex).Description, "Unable to retrieve portal fee", MessageBoxButtons.OK, MessageBoxIcon.Warning);
RetrievePortalFee = 0;
}
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX THIS HAS TO BE REPLACED XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX */

			return RetrievePortalFee;
		}

		private void DistributePaymentInforME()
		{
			// kgk 03082012 trocr-325  Because some towns use specific service codes (InforME accounts) for different receipt types
			// we need to be able to distribute the credit card charge between multiple accounts.
			// We'll do this by creating separate credit card payment lines for each service code.
			// 
			clsDRWrapper rsCR = new clsDRWrapper();
			clsDRWrapper rsStore = new clsDRWrapper();
			double dblPaymentAmt;
			int intPayTypeIdx;
			int intPaymentIdx;
			int intAddRows;
			Decimal curFeeAmount;
			int intDefAcct = 0;
			int I;
			int intCT;
			int lngNumStores;
			bool blnFound = false;
			EPmtAcctDistr[] arrEPmtAcct = null;
			// On Error GoTo ErrorHandler
			// frmWait.Show
			// frmWait.lblMessage.Text = "Please Wait..." & vbCrLf & "Contacting InforME"
			// frmWait.Refresh
			cmdCashOut.Enabled = false;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Contacting InforME");
			dblPaymentAmt = FCConvert.ToDouble(txtPayment.Text);
			intPayTypeIdx = cmbCashOutCC.SelectedIndex;
			intPaymentIdx = vsPayments.Rows;
			rsCR.OpenRecordset("SELECT * FROM CashRec", modExtraModules.strCRDatabase);
			// We need to loop through all of the payment breakdowns and check for a different Service Code
			rsStore.OpenRecordset("SELECT AcctID FROM EPmtAccounts", modExtraModules.strCRDatabase);
			rsStore.MoveLast();
			rsStore.MoveFirst();
			lngNumStores = rsStore.RecordCount();
			arrEPmtAcct = new EPmtAcctDistr[lngNumStores + 1];
			for (I = 1; I <= lngNumStores; I++)
			{
				//Application.DoEvents();
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				arrEPmtAcct[I] = new EPmtAcctDistr(0);
				arrEPmtAcct[I].AcctID = FCConvert.ToString(rsStore.Get_Fields_String("AcctID"));
				if (arrEPmtAcct[I].AcctID == FCConvert.ToString(rsCR.Get_Fields_String("EPaymentClientID")))
				{
					intDefAcct = I;
				}
				rsStore.MoveNext();
			}
			// I
			intAddRows = 0;
			// Loop through all of the pmt brkdwns and match up to a "store"
			// or set to the default "store"
			// Debug.Print "intPaymentBreakdownInfoCounter = " & intPaymentBreakdownInfoCounter
			for (intCT = 0; intCT <= Information.UBound(modUseCR.Statics.PaymentBreakdownInfoArray, 1); intCT++)
			{
				//Application.DoEvents();
				if (modUseCR.Statics.PaymentBreakdownInfoArray[intCT].PaymentIndex == intPaymentIdx)
				{
					if (modUseCR.Statics.PaymentBreakdownInfoArray[intCT].EPmtAcctID == "")
					{
						arrEPmtAcct[intDefAcct].Amount += FCConvert.ToDouble(modUseCR.Statics.PaymentBreakdownInfoArray[intCT].CardAmount);
						if (arrEPmtAcct[intDefAcct].GridRow == 0)
						{
							modUseCR.Statics.PaymentBreakdownInfoArray[intCT].EPmtAcctID = arrEPmtAcct[intDefAcct].AcctID;
							arrEPmtAcct[intDefAcct].GridRow = intPaymentIdx + intAddRows;
							intAddRows += 1;
						}
					}
					else
					{
						blnFound = false;
						for (I = 1; I <= lngNumStores; I++)
						{
							if (modUseCR.Statics.PaymentBreakdownInfoArray[intCT].EPmtAcctID == arrEPmtAcct[I].AcctID)
							{
								arrEPmtAcct[I].Amount += FCConvert.ToDouble(modUseCR.Statics.PaymentBreakdownInfoArray[intCT].CardAmount);
								blnFound = true;
								if (arrEPmtAcct[I].GridRow == 0)
								{
									arrEPmtAcct[I].GridRow = intPaymentIdx + intAddRows;
									intAddRows += 1;
								}
								break;
							}
						}
						// I
						if (!blnFound)
						{
							arrEPmtAcct[intDefAcct].Amount += FCConvert.ToDouble(modUseCR.Statics.PaymentBreakdownInfoArray[intCT].CardAmount);
							if (arrEPmtAcct[intDefAcct].GridRow == 0)
							{
								modUseCR.Statics.PaymentBreakdownInfoArray[intCT].EPmtAcctID = arrEPmtAcct[intDefAcct].AcctID;
								arrEPmtAcct[intDefAcct].GridRow = intPaymentIdx + intAddRows;
								intAddRows += 1;
							}
						}
					}
				}
			}
			// intCT
			// Now get the portal fee for each "store"
			for (I = 1; I <= lngNumStores; I++)
			{
				//Application.DoEvents();
				if (arrEPmtAcct[I].Amount > 0)
				{
					curFeeAmount = RetrievePortalFee_2(FCConvert.ToDecimal(arrEPmtAcct[I].Amount), arrEPmtAcct[I].AcctID);
					// Update the Payment Index to the payment row we're going to add for this AcctID/ServiceCode
					for (intCT = 0; intCT <= Information.UBound(modUseCR.Statics.PaymentBreakdownInfoArray, 1); intCT++)
					{
						if (modUseCR.Statics.PaymentBreakdownInfoArray[intCT].PaymentIndex == intPaymentIdx && modUseCR.Statics.PaymentBreakdownInfoArray[intCT].EPmtAcctID == arrEPmtAcct[I].AcctID)
						{
							modUseCR.Statics.PaymentBreakdownInfoArray[intCT].PaymentIndex = arrEPmtAcct[I].GridRow;
						}
					}
					// intCT
					cmbCashOutType.SelectedIndex = 2;
					txtPayment.Text = Strings.Format(arrEPmtAcct[I].Amount, "0.00");
					cmbCashOutCC.SelectedIndex = intPayTypeIdx;
					// Add a separate CC entry for each AcctID/Service Code
					CashOut_120(curFeeAmount, arrEPmtAcct[I].AcctID);
				}
			}
			// I
			frmWait.InstancePtr.Unload();
			cmdCashOut.Enabled = true;
		}

		public void ClearPartyInfo_2(bool boolNameFld)
		{
			ClearPartyInfo(ref boolNameFld);
		}

		public void ClearPartyInfo(ref bool boolNameFld)
		{
			if (boolNameFld)
			{
				txtCustNum_Name.Text = "";
				// txtFirst.Text  = ""
				imgMemo_Name.Visible = false;
				cmdEdit_Name.Enabled = false;
			}
			else
			{
				txtCustNum_PaidBy.Text = "";
				// txtFirst.Text  = ""
				imgMemo_PaidBy.Visible = false;
				cmdEdit_PaidBy.Enabled = false;
			}
		}

		public void GetPartyInfo_6(int intPartyID, bool boolNameFld)
		{
			GetPartyInfo(ref intPartyID, ref boolNameFld);
		}

		public void GetPartyInfo_8(int intPartyID, bool boolNameFld)
		{
			GetPartyInfo(ref intPartyID, ref boolNameFld);
		}

		public void GetPartyInfo(ref int intPartyID, ref bool boolNameFld)
		{
			cPartyController pCont = new cPartyController();
			// vbPorter upgrade warning: pInfo As CParty	OnWrite(cParty)
			cParty pInfo = new cParty();
			// Dim pAdd As cPartyAddress
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				if (boolNameFld)
				{
					txtFirst.Text = pInfo.FullName;
					pComments = pInfo.GetModuleSpecificComments();
					if (pComments.Count > 0)
					{
						imgMemo_Name.Visible = true;
					}
					else
					{
						imgMemo_Name.Visible = false;
					}
					cmdEdit_Name.Enabled = true;
				}
				else
				{
					txtPaidBy.Text = pInfo.FullName;
					pComments = pInfo.GetModuleSpecificComments();
					if (pComments.Count > 0)
					{
						imgMemo_PaidBy.Visible = true;
					}
					else
					{
						imgMemo_PaidBy.Visible = false;
					}
					cmdEdit_PaidBy.Enabled = true;
				}
			}
		}

		private void Validate_Party_2(bool boolNameFld)
		{
			Validate_Party(ref boolNameFld);
		}

		private void Validate_Party(ref bool boolNameFld)
		{
			cPartyController pCont = new cPartyController();
			// vbPorter upgrade warning: pInfo As CParty	OnWrite(cParty)
			cParty pInfo = new cParty();
			// Dim pAdd As cPartyAddress
			FCCollection pComments = new FCCollection();
			if (boolNameFld)
			{
				pInfo = pCont.GetParty(FCConvert.ToInt32(FCConvert.ToDouble(txtCustNum_Name.Text)));
				if (!(pInfo == null))
				{
					if (Strings.UCase(txtFirst.Text) != Strings.UCase(pInfo.FullName) && Strings.UCase(txtFirst.Text) != Strings.UCase(pInfo.FullNameLastFirst))
					{
						txtCustNum_Name.Text = "";
						imgMemo_Name.Visible = false;
						cmdEdit_Name.Enabled = false;
					}
				}
				else
				{
					txtCustNum_Name.Text = "";
					imgMemo_Name.Visible = false;
					cmdEdit_Name.Enabled = false;
				}
			}
			else
			{
				pInfo = pCont.GetParty(FCConvert.ToInt32(FCConvert.ToDouble(txtCustNum_PaidBy.Text)));
				if (!(pInfo == null))
				{
					if (Strings.UCase(txtPaidBy.Text) != Strings.UCase(pInfo.FullName) && Strings.UCase(txtPaidBy.Text) != Strings.UCase(pInfo.FullNameLastFirst))
					{
						txtCustNum_PaidBy.Text = "";
						imgMemo_PaidBy.Visible = false;
						cmdEdit_PaidBy.Enabled = false;
					}
				}
				else
				{
					txtCustNum_PaidBy.Text = "";
					imgMemo_PaidBy.Visible = false;
					cmdEdit_PaidBy.Enabled = false;
				}
			}
		}

		private void cmdBack_Click(object sender, EventArgs e)
		{
			mnuFileBack_Click(sender, e);
		}

		private void cmdEdit_Click(object sender, EventArgs e)
		{
			mnuFileEdit_Click(sender, e);
		}
		//// FC:FINAL:VGE - #383 Edit Control event for KeyDown
		//private void cmbType_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		//{
		//	if (e.Control != null)
		//	{
		//		e.Control.KeyDown -= Control_KeyDown;
		//		e.Control.KeyDown += Control_KeyDown;
		//		// FC:FINAL:VGE - #845 Restoring ARPU key press handler
		//		e.Control.KeyPress -= Control_KeyPress;
		//		e.Control.KeyPress += Control_KeyPress;
		//	}
		//}

		private void Control_KeyDown(object sender, KeyEventArgs e)
		{
			cmbType_KeyDownEvent(sender, e);
		}
		// FC:FINAL:VGE - #845 Restoring ARPU key press handler
		private void Control_KeyPress(object sender, KeyPressEventArgs e)
		{
			cmbType_KeyPressEvent(sender, e);
		}


        //private void cmbType_GotFocus(object sender, EventArgs e)
        //{
        //    if (!cmbType.IsCurrentCellInEditMode)
        //    {
        //        //FC:FINAL:SBE - #2556 - start edit cmbType when grid get the focus
        //        cmbType.Select(0, 0);
        //        cmbType.EditCell();
        //    }
        //}

        private void mnuImportMooringReceipts_Click(object sender, EventArgs e)
        {
           modUseCR.CustomMooringReceiptImport();
        }

        private object AddUTBatchToReceiptArray(int intRow, ref int lngReceiptKey)
        {
            object AddUTBatchToReceiptArray = null;
            // this function will add the batch payments that go with the row # passed in to the receiptarray for processing
            clsDRWrapper rsBatch = new/*AsNew*/
				clsDRWrapper();
            clsDRWrapper rsArchive = new/*AsNew*/ clsDRWrapper();
            clsDRWrapper rsType = new/*AsNew*/ clsDRWrapper();
            string strSQL = "";
            short intIndex = 0;
            bool boolFound = false;
            int lngArchivekey;
            string strDeleteSQL = "";
            string strRef = ""; // CODE FREEZE TROGES-88

            rsArchive.OpenRecordset("SELECT * FROM Archive WHERE id = 0", modExtraModules.strCRDatabase); // initialize the recordset to add the batch to

            // kk02082018 troges-88  If no batch number then handle as generic batch        'CODE FREEZE  TROGES-88
            if (strBatchNumber != "")
            {
                strSQL = "SELECT * FROM tblAutoPay WHERE ACHFlag = 1 AND ACHDone = 1 AND LineNumber > 0 AND NOT Posted = 1 AND BatchNumber = '" + strBatchNumber + "'";
            }
            else
            {
                strSQL = "SELECT * FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(modUseCR.Statics.ReceiptArray[Convert.ToInt32(vsSummary.TextMatrix(intRow, 8))].Date) + "' AND PaidBy = '" + modUseCR.Statics.ReceiptArray[Convert.ToInt32(vsSummary.TextMatrix(intRow, 8))].PaidBy + "' AND TellerID = '" + strBatchTellerID + "'";
                strDeleteSQL = "delete from batchrecover WHERE BatchRecoverDate = '" + FCConvert.ToString(modUseCR.Statics.ReceiptArray[Convert.ToInt32(vsSummary.TextMatrix(intRow, 8))].Date) + "' AND PaidBy = '" + modUseCR.Statics.ReceiptArray[Convert.ToInt32(vsSummary.TextMatrix(intRow, 8))].PaidBy + "' AND TellerID = '" + strBatchTellerID + "'";
            }

            if (rsBatch.OpenRecordset(strSQL, modExtraModules.strUTDatabase))
            {
                if (rsBatch.EndOfFile() != true && rsBatch.BeginningOfFile() != true)
                {
                    intIndex = 0;
                    do
                    {
                        intIndex += 1;
                        do
                        { // find a good place to put the payment
                            if (modUseCR.Statics.ReceiptArray[intIndex].Used != true)
                            {
                                boolFound = true;
                            }
                            else
                            {
                                boolFound = false;
                                intIndex += 1;
                            }
                        } while (!(intIndex > modStatusPayments.MAX_PAYMENTS || boolFound == true));

                        if (boolFound)
                        {
                            modUseCR.Statics.ReceiptArray[intIndex].Used = true; // if True then I know that this array index is currently being used
                                                                                // WATER 93 OR SEWER 94    Default to Sewer if nothing there             'kk02212018 troges-88  Handle W and S types    'CODE FREEZE  TROGES-88
                            if (FCConvert.ToString(rsBatch.Get_Fields("Service")) == "W")
                            {
                                modUseCR.Statics.ReceiptArray[intIndex].Type = Convert.ToInt32("93");
                            }
                            else
                            {
                                modUseCR.Statics.ReceiptArray[intIndex].Type = Convert.ToInt32("94");
                            }
                            modUseCR.Statics.ReceiptArray[intIndex].Module = "U"; // P = Personal Property, R = Real Estate, W = UT Water, S = UT Sewer, X = Cash Receipting
                            modUseCR.Statics.ReceiptArray[intIndex].CollectionCode = "P"; // this will bring back the type of transaction from the module
                            modUseCR.Statics.ReceiptArray[intIndex].Account = FCConvert.ToString(rsBatch.Get_Fields("AccountNumber"));
                            modUseCR.Statics.ReceiptArray[intIndex].TypeDescription = "Utility Billing Batch";
                            if (strBatchNumber != "")
                            { // kk02212018 troges-88  Set reference from ACH or Batch table   'CODE FREEZE  TROGES-88
                                modUseCR.Statics.ReceiptArray[intIndex].Reference = modUseCR.Statics.ReceiptArray[intIndex].Account + "-" + rsBatch.Get_Fields("BatchNumber");
                            }
                            else
                            {
                                modUseCR.Statics.ReceiptArray[intIndex].Reference = FCConvert.ToString(rsBatch.Get_Fields("Ref"));
                            }
                            modUseCR.Statics.ReceiptArray[intIndex].Control1 = "A";
                            modUseCR.Statics.ReceiptArray[intIndex].Control2 = "P";
                            modUseCR.Statics.ReceiptArray[intIndex].Control3 = "";
                            // fee amounts
                            modUseCR.Statics.ReceiptArray[intIndex].Fee1 = rsBatch.Get_Fields("Prin"); // Principal
                            modUseCR.Statics.ReceiptArray[intIndex].Fee2 = rsBatch.Get_Fields("Int"); // Interest
                            modUseCR.Statics.ReceiptArray[intIndex].Fee3 = 0; // Discount
                            modUseCR.Statics.ReceiptArray[intIndex].Fee4 = rsBatch.Get_Fields("Cost"); // Cost
                            modUseCR.Statics.ReceiptArray[intIndex].Fee5 = rsBatch.Get_Fields("Tax"); // Tax
                            modUseCR.Statics.ReceiptArray[intIndex].Fee6 = 0;
                            modUseCR.Statics.ReceiptArray[intIndex].FeeD1 = "Principal";
                            modUseCR.Statics.ReceiptArray[intIndex].FeeD2 = "Interest";
                            modUseCR.Statics.ReceiptArray[intIndex].FeeD3 = "";
                            modUseCR.Statics.ReceiptArray[intIndex].FeeD4 = "Costs";
                            modUseCR.Statics.ReceiptArray[intIndex].FeeD5 = "Tax";
                            modUseCR.Statics.ReceiptArray[intIndex].FeeD6 = "";
                            modUseCR.Statics.ReceiptArray[intIndex].Total = modUseCR.Statics.ReceiptArray[intIndex].Fee1 + modUseCR.Statics.ReceiptArray[intIndex].Fee2 + modUseCR.Statics.ReceiptArray[intIndex].Fee4 + modUseCR.Statics.ReceiptArray[intIndex].Fee5;
                            modUseCR.Statics.ReceiptArray[intIndex].Name = fecherFoundation.Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields("Name")));
                            modUseCR.Statics.ReceiptArray[intIndex].PaidBy = fecherFoundation.Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields("Name")));
                            if (strBatchNumber != "")
                            { // kk02212018 troges-88  Set the payment id from ACH or Batch table      'CODE FREEZE  TROGES-88
                                modUseCR.Statics.ReceiptArray[intIndex].RecordKey = Convert.ToInt32(rsBatch.Get_Fields("PaymentID")); // this is the key from the paymentrec table in the UT database
                            }
                            else
                            {
                                modUseCR.Statics.ReceiptArray[intIndex].RecordKey = Convert.ToInt32(rsBatch.Get_Fields("PaymentKey"));
                            }
                            if (strBatchNumber != "")
                            {
                                modUseCR.Statics.ReceiptArray[intIndex].Date = (DateTime)rsBatch.Get_Fields("EffPmtDate");
                                modUseCR.Statics.ReceiptArray[intIndex].Copies = 0;
                            }
                            else
                            {
                                modUseCR.Statics.ReceiptArray[intIndex].Date = (DateTime)rsBatch.Get_Fields("ETDate");
                                if (FCConvert.ToBoolean(rsBatch.Get_Fields("PrintReceipt")))
                                { // to print or not to print
                                    modUseCR.Statics.ReceiptArray[intIndex].Copies = 1;
                                }
                                else
                                {
                                    modUseCR.Statics.ReceiptArray[intIndex].Copies = 0;
                                }
                            }
                            modUseCR.Statics.ReceiptArray[intIndex].Comment = "";
                            modUseCR.Statics.ReceiptArray[intIndex].ArrayIndex = 0;
                            modUseCR.Statics.ReceiptArray[intIndex].BillingYear = 0; // XXX ??? Bill Number?
                            modUseCR.Statics.ReceiptArray[intIndex].CashAlternativeAcct = "";
                            modUseCR.Statics.ReceiptArray[intIndex].Split = intIndex + 5;
                            modUseCR.Statics.ReceiptArray[intIndex].PrintReceipt = true;
                            modUseCR.Statics.ReceiptArray[intIndex].AffectCashDrawer = true;
                            modUseCR.Statics.ReceiptArray[intIndex].AffectCash = true;
                            modUseCR.Statics.ReceiptArray[intIndex].Quantity = 1;


                            rsArchive.AddNew();
                            // Key

                            // ReceiptType
                            rsArchive.Set_Fields("ReceiptType",  modUseCR.Statics.ReceiptArray[intIndex].Type);
                            // AccountNumber
                            rsArchive.Set_Fields("AccountNumber", FCConvert.ToString(fecherFoundation.Conversion.Val(fecherFoundation.Strings.Trim(modUseCR.Statics.ReceiptArray[intIndex].Account + " "))));
                            // Actual System Date
                            rsArchive.Set_Fields("ActualSystemDate", DateTime.Now);
                            // ReceiptTitle
                            // rsarchive.Fields("ReceiptTitle") = .TypeDescription
                            rsType.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + modUseCR.Statics.ReceiptArray[intIndex]); /*? Type */
                            if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
                            {
                                // Account1
                                rsArchive.Set_Fields("Account1", modUseCR.CheckAccountForYear(ref rsType, rsType.Get_Fields("Account1"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 1));
                                // Account2
                                rsArchive.Set_Fields("Account2", modUseCR.CheckAccountForYear(ref rsType, rsType.Get_Fields("Account2"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 2));
                                // Account3
                                rsArchive.Set_Fields("Account3", modUseCR.CheckAccountForYear(ref rsType, rsType.Get_Fields("Account3"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 3));
                                // Account4
                                rsArchive.Set_Fields("Account4", modUseCR.CheckAccountForYear(ref rsType, rsType.Get_Fields("Account4"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 4));
                                // Account5
                                rsArchive.Set_Fields("Account5", modUseCR.CheckAccountForYear(ref rsType, rsType.Get_Fields("Account5"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 5));
                                // Account6
                                rsArchive.Set_Fields("Account6", modUseCR.CheckAccountForYear(ref rsType, rsType.Get_Fields("Account6"), modUseCR.Statics.ReceiptArray[intIndex].BillingYear, 6));
                            }
                            // Amount1
                            rsArchive.Set_Fields("Amount1", modUseCR.Statics.ReceiptArray[intIndex].Fee1);
                            // Amount2
                            rsArchive.Set_Fields("Amount2", modUseCR.Statics.ReceiptArray[intIndex].Fee2);
                            // Amount3
                            rsArchive.Set_Fields("Amount3", modUseCR.Statics.ReceiptArray[intIndex].Fee3);
                            // Amount4
                            rsArchive.Set_Fields("Amount4", modUseCR.Statics.ReceiptArray[intIndex].Fee4);
                            // Amount5
                            rsArchive.Set_Fields("Amount5", modUseCR.Statics.ReceiptArray[intIndex].Fee5);
                            // Amount6
                            rsArchive.Set_Fields("Amount6", modUseCR.Statics.ReceiptArray[intIndex].Fee6);
                            // Comment
                            rsArchive.Set_Fields("Comment", modUseCR.Statics.ReceiptArray[intIndex].Comment);
                            // Ref
                            rsArchive.Set_Fields("Ref", modUseCR.Statics.ReceiptArray[intIndex].Reference);
                            // Control1
                            rsArchive.Set_Fields("Control1", modUseCR.Statics.ReceiptArray[intIndex].Control1);
                            // Control2
                            rsArchive.Set_Fields("Control2", modUseCR.Statics.ReceiptArray[intIndex].Control2);
                            // Control3
                            rsArchive.Set_Fields("Control3", modUseCR.Statics.ReceiptArray[intIndex].Control3);
                            // ReceiptNumber
                            rsArchive.Set_Fields("ReceiptNumber", lngReceiptKey);
                            // TellerID
                            rsArchive.Set_Fields("TellerID", strTellerID);
                            // Name
                            rsArchive.Set_Fields("Name", modUseCR.Statics.ReceiptArray[intIndex].Name);
                            // Date
                            rsArchive.Set_Fields("ArchiveDate", modUseCR.Statics.ReceiptArray[intIndex].Date);

                            rsArchive.Set_Fields("CollectionCode", modUseCR.Statics.ReceiptArray[intIndex].CollectionCode);
                            // TellerCloseOut
                            rsArchive.Set_Fields("TellerCloseOut", 0);
                            // DailyCloseOut
                            rsArchive.Set_Fields("DailyCloseOut", 0);
                            // AffectCashDrawer
                            rsArchive.Set_Fields("AffectCashDrawer", modUseCR.Statics.ReceiptArray[intIndex].AffectCashDrawer);
                            // DailyCloseOut
                            rsArchive.Set_Fields("AffectCash", modUseCR.Statics.ReceiptArray[intIndex].AffectCash);
                            // Quantity
                            rsArchive.Set_Fields("Quantity", modUseCR.Statics.ReceiptArray[intIndex].Quantity);
                            // DefaultAccount
                            rsArchive.Set_Fields("DefaultAccount", modUseCR.Statics.ReceiptArray[intIndex].CashAlternativeAcct); // this is used when the Cash is not affected and the Cash Drawer is not affected

                            if (!rsArchive.Update(true))
                            {
                                MessageBox.Show("There has been an error in the creation of the Archive File.", "Receipt Record ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            }
                            lngArchivekey = Convert.ToInt32(rsArchive.Get_Fields("id"));

                            if (strBatchNumber != "")
                            { // kk02212018 troges-88  Update ACH record    'CODE FREEZE TROGES-88
                                rsBatch.Edit();
                                rsBatch.Set_Fields("Posted", true);
                                rsBatch.Set_Fields("PostedDate", DateTime.Now);
                                rsBatch.Update();
                            }

                            // this will update the ReceiptNumber field from (P)ending to the receipt number of the payment
                            // records of any payments from other modules
                            var vbPorterVar = modUseCR.Statics.ReceiptArray[intIndex].Type;
                            if (vbPorterVar >= 90 && vbPorterVar <= 92)
                            {
                                // kk02162018 troges-88  This was 90-93             'CODE FREEZE  TROGES-88
                            }
                            else if (vbPorterVar >= 93 && vbPorterVar <= 96)
                            {
                                // UT          '                      and this was 94-95
                                modUseCR.UpdateUTPayment(ref modUseCR.Statics.ReceiptArray[intIndex].RecordKey, ref lngReceiptKey);
                                // Case 96
                            }
                            else if (vbPorterVar == 97)
                            {
                            }
                            else if (vbPorterVar == 98)
                            {
                            }
                            else if (vbPorterVar == 99)
                            {
                                // MV
                            }
                            else if (vbPorterVar >= 190 && vbPorterVar <= 199)
                            {
                            }

                        }
                        else
                        {
                            MessageBox.Show("Exceeded maximum payments limit.", "Maximum Payment Limit", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        rsBatch.MoveNext();
                    } while (!rsBatch.EndOfFile());

                    if (strBatchNumber != "")
                    { // kk02212018 troges-88  Only check for unposted ACH and run the report if it's an ACH batch       'CODE FREEZE  TROGES-88
                      // kk11182014 trocr-434  If there are unposted payments, see if the user wants to keep the batch open
                      // Do we need to check for the service in the query??
                        strSQL = "SELECT * FROM tblAutoPay WHERE ACHDone AND NOT Posted AND BatchNumber = '" + strBatchNumber + "'";
                        rsBatch.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                        if (!rsBatch.EndOfFile())
                        {
                            if (DialogResult.Yes == MessageBox.Show("Some batch entries were not processed." + "\r\n" + "Do you want to mark this batch as completed?", "Unprocessed Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                            {
                                while (!rsBatch.EndOfFile())
                                {
                                    rsBatch.Edit();
                                    rsBatch.Set_Fields("Posted", true);
                                    rsBatch.Set_Fields("PostedDate", DateTime.Now);
                                    rsBatch.Update();
                                    rsBatch.MoveNext();
                                }
                            }
                        }

                        // print the list of accounts that have been affected
                        arUTAutoPayEditReport.InstancePtr.Init(autoPayRptType.autoPayRptTypePost, false,  strBatchNumber);
                    }
                    if (strDeleteSQL != "")
                    {
                        rsBatch.Execute(strDeleteSQL, modExtraModules.strUTDatabase);
                    }
                }
                else
                {
                    if (strBatchNumber != "")
                    { // kk02212018 troges-88  Only check for no posted payments if it's an ACH batch       'CODE FREEZE  TROGES-88
                      // kk11182014 trocr-434  If there are unposted payments, see if the user wants to keep the batch open
                      // Do we need to check for the service in the query??
                        strSQL = "SELECT * FROM tblAutoPay WHERE ACHDone AND NOT Posted AND BatchNumber = '" + strBatchNumber + "'";
                        rsBatch.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                        if (!rsBatch.EndOfFile())
                        {
                            if (DialogResult.Yes == MessageBox.Show("There were no payments recorded." + "\r\n" + "Do you want to mark this batch as completed?", "Unprocessed Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                            {
                                while (!rsBatch.EndOfFile())
                                {
                                    rsBatch.Edit();
                                    rsBatch.Set_Fields("Posted", true);
                                    rsBatch.Set_Fields("PostedDate", DateTime.Now);
                                    rsBatch.Update();
                                    rsBatch.MoveNext();
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (strBatchNumber != "")
                { // kk02212018 troges-88  Show appropriate message for ACH or payment batch       'CODE FREEZE  TROGES-88
                    MessageBox.Show("Error opening Auto-Pay table.", "Batch Recovery Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    MessageBox.Show("Error processing UT batch payments.", "Batch Recovery Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            return AddUTBatchToReceiptArray;
        }

		private void imgMemo_PaidBy_Click(object sender, EventArgs e)
		{

		}
	}
}
