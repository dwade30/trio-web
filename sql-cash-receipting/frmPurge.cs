﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWCR0000
{
	/// <summary>
	/// Summary description for frmPurge.
	/// </summary>
	public partial class frmPurge : BaseForm
	{
		public frmPurge()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurge InstancePtr
		{
			get
			{
				return (frmPurge)Sys.GetInstance(typeof(frmPurge));
			}
		}

		protected frmPurge _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/17/2004              *
		// ********************************************************
		private void cmdCancel_Click()
		{
			mnuFileCancel_Click();
		}

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strText;
				DateTime dtDate;
				int lngPurged = 0;
				strText = txtDate.Text;
				if (Strings.Trim(strText) != "")
				{
					if (Information.IsDate(strText))
					{
						dtDate = DateAndTime.DateValue(strText);
						if (dtDate.ToOADate() < DateTime.Now.ToOADate())
						{
							if (MessageBox.Show("Purge all receipts prior to " + dtDate.ToShortDateString() + "?", "Purge", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								lngPurged = PurgeArchive(ref dtDate);
								if (lngPurged >= 0)
								{
									MessageBox.Show("Sucessfully purged " + FCConvert.ToString(lngPurged) + " receipts.", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
									Close();
								}
								else
								{
									MessageBox.Show("There has been an error during the purge process.", "Purge Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									Close();
								}
							}
						}
						else
						{
							MessageBox.Show("Please enter a valid date.", "Invalid Purge Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
					}
					else
					{
						MessageBox.Show("Please enter a valid date.", "Invalid Purge Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid date.", "Invalid Purge Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Purge Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdPurge_Click()
		{
			cmdPurge_Click(cmdPurge, new System.EventArgs());
		}

		private void cmdPurgeCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmPurge_Activated(object sender, System.EventArgs e)
		{
			lblPurgeInstructions.Text = "All receipts dated before the date entered will be purged";
			txtDate.ToolTipText = "(MM/dd/yyyy)";
		}

		private void frmPurge_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmPurge_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurge.FillStyle	= 0;
			//frmPurge.ScaleWidth	= 3885;
			//frmPurge.ScaleHeight	= 2220;
			//frmPurge.LinkTopic	= "Form2";
			//frmPurge.LockControls	= -1  'True;
			//frmPurge.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strMessage;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Purge Receipts";
			//ShowFrame(fraPurge);
			strMessage = "Please make sure that you have a current permanent backup of the database before proceeding." + "\r\n";
			strMessage += "Make sure that everyone is out of TRIO when running this function." + "\r\n";
			strMessage += "This operation is memory intensive so please only purge one year at a time as to keep the load on your system down.";
			MessageBox.Show(strMessage, "Backup!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		private void frmPurge_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPurge_Resize(object sender, System.EventArgs e)
		{
			//ShowFrame(fraPurge);
		}

		private void mnuFileCancel_Click()
		{
			Close();
		}

		private void mnuFileContinue_Click(object sender, System.EventArgs e)
		{
			cmdPurge_Click();
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(FCFrame fraFrame)
		{
			// this will place the frame in the middle of the form
			fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private int PurgeArchive(ref DateTime dtDate)
		{
			int PurgeArchive = 0;
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will purge all records in the LastYearArchive table with a date before dtDate
				// and after completion will return the number of records purged
				int lngTemp;
				clsDRWrapper rsPurge = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				int lngKey;
				frmWait.InstancePtr.Init("Purging Records" + "\r\n" + "Please Wait...", true, 7);
				frmWait.InstancePtr.IncrementProgress();
				intError = 1;
				rsPurge.OpenRecordset("SELECT * FROM LastYearArchive WHERE LastYearArchiveDate < '" + FCConvert.ToString(dtDate) + "'");
				if (rsPurge.EndOfFile() != true && rsPurge.BeginningOfFile() != true)
				{
					intError = 2;
					lngTemp = rsPurge.RecordCount();
					intError = 3;
					// this does the receipts, but what about the other tables 'FIX THIS!!!
					// CYA Table
					// kgk 11-28-2011 trocr-309  Add CYA
					modGlobalFunctions.AddCYAEntry_26("CR", "Purge Prior Year Receipts", "Prior to " + Strings.Format(dtDate, "MM/dd/yyyy"));
					frmWait.InstancePtr.IncrementProgress();
					intError = 4;
					// CheckMaster
					// kgk 11-28-2011 trocr-309  Fix CheckMaster ID reference in Join
					// MAL@20071008: Changed the way it loops through and deletes the records
					// Call Reference: 117146
					// rsPurge.OpenRecordset "SELECT ID FROM CheckMaster INNER JOIN LastYearReceipt ON CheckMaster.ID = LastYearReceipt.ReceiptKey WHERE Date < '" & dtDate & "' ORDER BY Date"
					rsPurge.OpenRecordset("SELECT CheckMaster.ID as CheckMasterID FROM CheckMaster INNER JOIN LastYearReceipt ON CheckMaster.ReceiptNumber = LastYearReceipt.ReceiptKey WHERE LastYearReceiptDate < '" + FCConvert.ToString(dtDate) + "' ORDER BY LastYearReceiptDate");
					if (rsPurge.RecordCount() > 0)
					{
						rsPurge.MoveFirst();
						while (!rsPurge.EndOfFile())
						{
							//Application.DoEvents();
							// .Delete
							// TODO Get_Fields: Field [CheckMasterID] not found!! (maybe it is an alias?)
							rsTemp.Execute("delete from Checkmaster where id = " + rsPurge.Get_Fields("CheckMasterID"), "TWCR0000.vb1");
							intError = 44;
							rsPurge.MoveNext();
						}
					}
					frmWait.InstancePtr.IncrementProgress();
					intError = 5;
					// CCMaster
					rsPurge.OpenRecordset("SELECT ccmaster.ID as CCMasterID FROM CCMaster INNER JOIN LastYearReceipt ON CCMaster.ReceiptNumber = LastYearReceipt.ReceiptKey WHERE LastYearReceiptDate < '" + FCConvert.ToString(dtDate) + "' ORDER BY LastYearReceiptDate");
					if (rsPurge.RecordCount() > 0)
					{
						rsPurge.MoveFirst();
						while (!rsPurge.EndOfFile())
						{
							//Application.DoEvents();
							// .Delete
							// TODO Get_Fields: Field [CCMasterID] not found!! (maybe it is an alias?)
							rsTemp.Execute("delete from ccmaster where id = " + rsPurge.Get_Fields("CCMasterID"), "twcr0000.vb1");
							intError = 44;
							rsPurge.MoveNext();
						}
					}
					intError = 6;
					frmWait.InstancePtr.IncrementProgress();
					intError = 7;
					// Archive
					rsPurge.OpenRecordset("SELECT ID FROM LastYearArchive WHERE LastYearArchiveDate < '" + FCConvert.ToString(dtDate) + "' ORDER BY LastYearArchiveDate");
					if (!rsPurge.EndOfFile())
					{
						if (rsPurge.Execute("DELETE FROM LastYearArchive WHERE LastYearArchiveDate < '" + FCConvert.ToString(dtDate) + "'", "TWCR0000.vb1"))
						{
							intError = 8;
						}
					}
					frmWait.InstancePtr.IncrementProgress();
					// Receipts
					rsPurge.OpenRecordset("SELECT ReceiptKey FROM LastYearReceipt WHERE LastYearReceiptDate < '" + FCConvert.ToString(dtDate) + "' ORDER BY LastYearReceiptDate");
					if (!rsPurge.EndOfFile())
					{
						PurgeArchive = rsPurge.RecordCount();
						// this will give the count of receipts
						if (rsPurge.Execute("DELETE FROM LastYearReceipt WHERE LastYearReceiptDate < '" + FCConvert.ToString(dtDate) + "'", "TWCR0000.vb1"))
						{
							intError = 9;
						}
					}
					frmWait.InstancePtr.IncrementProgress();
				}
				else
				{
					PurgeArchive = 0;
				}
				frmWait.InstancePtr.Unload();
				return PurgeArchive;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				PurgeArchive = -1;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Purge Error - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return PurgeArchive;
		}
	}
}
